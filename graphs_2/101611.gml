graph [
  node [
    id 0
    label "znowu"
    origin "text"
  ]
  node [
    id 1
    label "dwa"
    origin "text"
  ]
  node [
    id 2
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 3
    label "ruda"
    origin "text"
  ]
  node [
    id 4
    label "walczy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "siebie"
    origin "text"
  ]
  node [
    id 6
    label "niegdy&#347;"
    origin "text"
  ]
  node [
    id 7
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 8
    label "bardzo"
    origin "text"
  ]
  node [
    id 9
    label "rozleg&#322;y"
    origin "text"
  ]
  node [
    id 10
    label "powoli"
    origin "text"
  ]
  node [
    id 11
    label "s&#261;siadek"
    origin "text"
  ]
  node [
    id 12
    label "wyprze&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zaj&#261;&#263;"
    origin "text"
  ]
  node [
    id 14
    label "miejsce"
    origin "text"
  ]
  node [
    id 15
    label "spojrze&#263;"
    origin "text"
  ]
  node [
    id 16
    label "zielony"
    origin "text"
  ]
  node [
    id 17
    label "siwy"
    origin "text"
  ]
  node [
    id 18
    label "zapuszcza&#263;"
    origin "text"
  ]
  node [
    id 19
    label "zagon"
    origin "text"
  ]
  node [
    id 20
    label "zielonem"
    origin "text"
  ]
  node [
    id 21
    label "t&#322;o"
    origin "text"
  ]
  node [
    id 22
    label "wida&#263;"
    origin "text"
  ]
  node [
    id 23
    label "szary"
    origin "text"
  ]
  node [
    id 24
    label "pasek"
    origin "text"
  ]
  node [
    id 25
    label "typ_mongoloidalny"
  ]
  node [
    id 26
    label "kolorowy"
  ]
  node [
    id 27
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 28
    label "ciep&#322;y"
  ]
  node [
    id 29
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 30
    label "jasny"
  ]
  node [
    id 31
    label "zabarwienie_si&#281;"
  ]
  node [
    id 32
    label "ciekawy"
  ]
  node [
    id 33
    label "r&#243;&#380;nofarbny"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "r&#243;&#380;nokolorowo"
  ]
  node [
    id 36
    label "weso&#322;y"
  ]
  node [
    id 37
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 38
    label "barwienie"
  ]
  node [
    id 39
    label "kolorowo"
  ]
  node [
    id 40
    label "barwnie"
  ]
  node [
    id 41
    label "kolorowanie"
  ]
  node [
    id 42
    label "barwisty"
  ]
  node [
    id 43
    label "przyjemny"
  ]
  node [
    id 44
    label "barwienie_si&#281;"
  ]
  node [
    id 45
    label "pi&#281;kny"
  ]
  node [
    id 46
    label "ubarwienie"
  ]
  node [
    id 47
    label "mi&#322;y"
  ]
  node [
    id 48
    label "ocieplanie_si&#281;"
  ]
  node [
    id 49
    label "ocieplanie"
  ]
  node [
    id 50
    label "grzanie"
  ]
  node [
    id 51
    label "ocieplenie_si&#281;"
  ]
  node [
    id 52
    label "zagrzanie"
  ]
  node [
    id 53
    label "ocieplenie"
  ]
  node [
    id 54
    label "korzystny"
  ]
  node [
    id 55
    label "ciep&#322;o"
  ]
  node [
    id 56
    label "dobry"
  ]
  node [
    id 57
    label "o&#347;wietlenie"
  ]
  node [
    id 58
    label "szczery"
  ]
  node [
    id 59
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 60
    label "jasno"
  ]
  node [
    id 61
    label "o&#347;wietlanie"
  ]
  node [
    id 62
    label "przytomny"
  ]
  node [
    id 63
    label "zrozumia&#322;y"
  ]
  node [
    id 64
    label "niezm&#261;cony"
  ]
  node [
    id 65
    label "bia&#322;y"
  ]
  node [
    id 66
    label "klarowny"
  ]
  node [
    id 67
    label "jednoznaczny"
  ]
  node [
    id 68
    label "pogodny"
  ]
  node [
    id 69
    label "odcinanie_si&#281;"
  ]
  node [
    id 70
    label "wypa&#322;ek"
  ]
  node [
    id 71
    label "atakamit"
  ]
  node [
    id 72
    label "ore"
  ]
  node [
    id 73
    label "aglomerownia"
  ]
  node [
    id 74
    label "kopalina_podstawowa"
  ]
  node [
    id 75
    label "pra&#380;alnia"
  ]
  node [
    id 76
    label "oddzia&#322;"
  ]
  node [
    id 77
    label "huta"
  ]
  node [
    id 78
    label "pra&#380;ak"
  ]
  node [
    id 79
    label "minera&#322;"
  ]
  node [
    id 80
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 81
    label "piec_hutniczy"
  ]
  node [
    id 82
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 83
    label "stara&#263;_si&#281;"
  ]
  node [
    id 84
    label "zmaga&#263;_si&#281;"
  ]
  node [
    id 85
    label "robi&#263;"
  ]
  node [
    id 86
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 87
    label "opowiada&#263;_si&#281;"
  ]
  node [
    id 88
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 89
    label "dzia&#322;a&#263;"
  ]
  node [
    id 90
    label "fight"
  ]
  node [
    id 91
    label "wrestle"
  ]
  node [
    id 92
    label "zawody"
  ]
  node [
    id 93
    label "cope"
  ]
  node [
    id 94
    label "contend"
  ]
  node [
    id 95
    label "argue"
  ]
  node [
    id 96
    label "my&#347;lenie"
  ]
  node [
    id 97
    label "organizowa&#263;"
  ]
  node [
    id 98
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 99
    label "czyni&#263;"
  ]
  node [
    id 100
    label "give"
  ]
  node [
    id 101
    label "stylizowa&#263;"
  ]
  node [
    id 102
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 103
    label "falowa&#263;"
  ]
  node [
    id 104
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 105
    label "peddle"
  ]
  node [
    id 106
    label "praca"
  ]
  node [
    id 107
    label "wydala&#263;"
  ]
  node [
    id 108
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 109
    label "tentegowa&#263;"
  ]
  node [
    id 110
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 111
    label "urz&#261;dza&#263;"
  ]
  node [
    id 112
    label "oszukiwa&#263;"
  ]
  node [
    id 113
    label "work"
  ]
  node [
    id 114
    label "ukazywa&#263;"
  ]
  node [
    id 115
    label "przerabia&#263;"
  ]
  node [
    id 116
    label "act"
  ]
  node [
    id 117
    label "post&#281;powa&#263;"
  ]
  node [
    id 118
    label "mie&#263;_miejsce"
  ]
  node [
    id 119
    label "istnie&#263;"
  ]
  node [
    id 120
    label "function"
  ]
  node [
    id 121
    label "determine"
  ]
  node [
    id 122
    label "bangla&#263;"
  ]
  node [
    id 123
    label "tryb"
  ]
  node [
    id 124
    label "powodowa&#263;"
  ]
  node [
    id 125
    label "reakcja_chemiczna"
  ]
  node [
    id 126
    label "commit"
  ]
  node [
    id 127
    label "dziama&#263;"
  ]
  node [
    id 128
    label "proces_my&#347;lowy"
  ]
  node [
    id 129
    label "domy&#347;lanie_si&#281;"
  ]
  node [
    id 130
    label "pilnowanie"
  ]
  node [
    id 131
    label "zjawisko"
  ]
  node [
    id 132
    label "s&#261;dzenie"
  ]
  node [
    id 133
    label "judgment"
  ]
  node [
    id 134
    label "troskanie_si&#281;"
  ]
  node [
    id 135
    label "wnioskowanie"
  ]
  node [
    id 136
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 137
    label "proces"
  ]
  node [
    id 138
    label "treatment"
  ]
  node [
    id 139
    label "robienie"
  ]
  node [
    id 140
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 141
    label "walczenie"
  ]
  node [
    id 142
    label "zinterpretowanie"
  ]
  node [
    id 143
    label "czynno&#347;&#263;"
  ]
  node [
    id 144
    label "skupianie_si&#281;"
  ]
  node [
    id 145
    label "reflection"
  ]
  node [
    id 146
    label "wzlecie&#263;"
  ]
  node [
    id 147
    label "wzlecenie"
  ]
  node [
    id 148
    label "impreza"
  ]
  node [
    id 149
    label "contest"
  ]
  node [
    id 150
    label "rywalizacja"
  ]
  node [
    id 151
    label "tysi&#281;cznik"
  ]
  node [
    id 152
    label "champion"
  ]
  node [
    id 153
    label "spadochroniarstwo"
  ]
  node [
    id 154
    label "kategoria_open"
  ]
  node [
    id 155
    label "professional_wrestling"
  ]
  node [
    id 156
    label "drzewiej"
  ]
  node [
    id 157
    label "niegdysiejszy"
  ]
  node [
    id 158
    label "kiedy&#347;"
  ]
  node [
    id 159
    label "ongi&#347;"
  ]
  node [
    id 160
    label "dawny"
  ]
  node [
    id 161
    label "partnerka"
  ]
  node [
    id 162
    label "aktorka"
  ]
  node [
    id 163
    label "kobieta"
  ]
  node [
    id 164
    label "partner"
  ]
  node [
    id 165
    label "kobita"
  ]
  node [
    id 166
    label "w_chuj"
  ]
  node [
    id 167
    label "szeroki"
  ]
  node [
    id 168
    label "rozlegle"
  ]
  node [
    id 169
    label "du&#380;y"
  ]
  node [
    id 170
    label "szeroko"
  ]
  node [
    id 171
    label "rozdeptanie"
  ]
  node [
    id 172
    label "rozdeptywanie"
  ]
  node [
    id 173
    label "lu&#378;no"
  ]
  node [
    id 174
    label "across_the_board"
  ]
  node [
    id 175
    label "rozci&#261;gle"
  ]
  node [
    id 176
    label "doros&#322;y"
  ]
  node [
    id 177
    label "znaczny"
  ]
  node [
    id 178
    label "niema&#322;o"
  ]
  node [
    id 179
    label "wiele"
  ]
  node [
    id 180
    label "rozwini&#281;ty"
  ]
  node [
    id 181
    label "dorodny"
  ]
  node [
    id 182
    label "wa&#380;ny"
  ]
  node [
    id 183
    label "prawdziwy"
  ]
  node [
    id 184
    label "du&#380;o"
  ]
  node [
    id 185
    label "niespiesznie"
  ]
  node [
    id 186
    label "spokojny"
  ]
  node [
    id 187
    label "wolny"
  ]
  node [
    id 188
    label "stopniowo"
  ]
  node [
    id 189
    label "bezproblemowo"
  ]
  node [
    id 190
    label "wolniej"
  ]
  node [
    id 191
    label "linearnie"
  ]
  node [
    id 192
    label "stopniowy"
  ]
  node [
    id 193
    label "udanie"
  ]
  node [
    id 194
    label "bezproblemowy"
  ]
  node [
    id 195
    label "niespieszny"
  ]
  node [
    id 196
    label "measuredly"
  ]
  node [
    id 197
    label "uspokajanie_si&#281;"
  ]
  node [
    id 198
    label "spokojnie"
  ]
  node [
    id 199
    label "uspokojenie_si&#281;"
  ]
  node [
    id 200
    label "cicho"
  ]
  node [
    id 201
    label "uspokojenie"
  ]
  node [
    id 202
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 203
    label "nietrudny"
  ]
  node [
    id 204
    label "uspokajanie"
  ]
  node [
    id 205
    label "rzedni&#281;cie"
  ]
  node [
    id 206
    label "zwalnianie_si&#281;"
  ]
  node [
    id 207
    label "wakowa&#263;"
  ]
  node [
    id 208
    label "rozwadnianie"
  ]
  node [
    id 209
    label "niezale&#380;ny"
  ]
  node [
    id 210
    label "rozwodnienie"
  ]
  node [
    id 211
    label "zrzedni&#281;cie"
  ]
  node [
    id 212
    label "swobodnie"
  ]
  node [
    id 213
    label "rozrzedzanie"
  ]
  node [
    id 214
    label "rozrzedzenie"
  ]
  node [
    id 215
    label "strza&#322;"
  ]
  node [
    id 216
    label "wolnie"
  ]
  node [
    id 217
    label "zwolnienie_si&#281;"
  ]
  node [
    id 218
    label "wolno"
  ]
  node [
    id 219
    label "ro&#347;liny"
  ]
  node [
    id 220
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 221
    label "sk&#243;ra"
  ]
  node [
    id 222
    label "zast&#261;pi&#263;"
  ]
  node [
    id 223
    label "usun&#261;&#263;"
  ]
  node [
    id 224
    label "wygin&#261;&#263;"
  ]
  node [
    id 225
    label "rozogni&#263;_si&#281;"
  ]
  node [
    id 226
    label "withdraw"
  ]
  node [
    id 227
    label "motivate"
  ]
  node [
    id 228
    label "wyrugowa&#263;"
  ]
  node [
    id 229
    label "go"
  ]
  node [
    id 230
    label "undo"
  ]
  node [
    id 231
    label "zabi&#263;"
  ]
  node [
    id 232
    label "spowodowa&#263;"
  ]
  node [
    id 233
    label "przenie&#347;&#263;"
  ]
  node [
    id 234
    label "przesun&#261;&#263;"
  ]
  node [
    id 235
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 236
    label "bomber"
  ]
  node [
    id 237
    label "zmieni&#263;"
  ]
  node [
    id 238
    label "zdecydowa&#263;"
  ]
  node [
    id 239
    label "die"
  ]
  node [
    id 240
    label "wymrze&#263;"
  ]
  node [
    id 241
    label "szczupak"
  ]
  node [
    id 242
    label "coating"
  ]
  node [
    id 243
    label "krupon"
  ]
  node [
    id 244
    label "harleyowiec"
  ]
  node [
    id 245
    label "tkanka_podsk&#243;rna"
  ]
  node [
    id 246
    label "kurtka"
  ]
  node [
    id 247
    label "metal"
  ]
  node [
    id 248
    label "p&#322;aszcz"
  ]
  node [
    id 249
    label "&#322;upa"
  ]
  node [
    id 250
    label "okrywa"
  ]
  node [
    id 251
    label "gruczo&#322;_kuprowy"
  ]
  node [
    id 252
    label "&#380;ycie"
  ]
  node [
    id 253
    label "gruczo&#322;_potowy"
  ]
  node [
    id 254
    label "lico"
  ]
  node [
    id 255
    label "wi&#243;rkownik"
  ]
  node [
    id 256
    label "mizdra"
  ]
  node [
    id 257
    label "dupa"
  ]
  node [
    id 258
    label "rockers"
  ]
  node [
    id 259
    label "&#322;uskowato&#347;&#263;"
  ]
  node [
    id 260
    label "surowiec"
  ]
  node [
    id 261
    label "czerniak_z&#322;o&#347;liwy"
  ]
  node [
    id 262
    label "gruczo&#322;_&#322;ojowy"
  ]
  node [
    id 263
    label "organ"
  ]
  node [
    id 264
    label "pow&#322;oka"
  ]
  node [
    id 265
    label "zdrowie"
  ]
  node [
    id 266
    label "wyprawa"
  ]
  node [
    id 267
    label "sk&#243;ra_w&#322;a&#347;ciwa"
  ]
  node [
    id 268
    label "hardrockowiec"
  ]
  node [
    id 269
    label "nask&#243;rek"
  ]
  node [
    id 270
    label "gestapowiec"
  ]
  node [
    id 271
    label "funkcja"
  ]
  node [
    id 272
    label "cia&#322;o"
  ]
  node [
    id 273
    label "shell"
  ]
  node [
    id 274
    label "j&#261;drowce"
  ]
  node [
    id 275
    label "kr&#243;lestwo"
  ]
  node [
    id 276
    label "zapanowa&#263;"
  ]
  node [
    id 277
    label "rozciekawi&#263;"
  ]
  node [
    id 278
    label "skorzysta&#263;"
  ]
  node [
    id 279
    label "komornik"
  ]
  node [
    id 280
    label "ulokowa&#263;_si&#281;"
  ]
  node [
    id 281
    label "klasyfikacja"
  ]
  node [
    id 282
    label "wype&#322;ni&#263;"
  ]
  node [
    id 283
    label "topographic_point"
  ]
  node [
    id 284
    label "obj&#261;&#263;"
  ]
  node [
    id 285
    label "seize"
  ]
  node [
    id 286
    label "interest"
  ]
  node [
    id 287
    label "anektowa&#263;"
  ]
  node [
    id 288
    label "employment"
  ]
  node [
    id 289
    label "zada&#263;"
  ]
  node [
    id 290
    label "prosecute"
  ]
  node [
    id 291
    label "dostarczy&#263;"
  ]
  node [
    id 292
    label "usytuowa&#263;_si&#281;"
  ]
  node [
    id 293
    label "bankrupt"
  ]
  node [
    id 294
    label "sorb"
  ]
  node [
    id 295
    label "zabra&#263;"
  ]
  node [
    id 296
    label "wzi&#261;&#263;"
  ]
  node [
    id 297
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 298
    label "do"
  ]
  node [
    id 299
    label "wzbudzi&#263;"
  ]
  node [
    id 300
    label "wywo&#322;a&#263;"
  ]
  node [
    id 301
    label "arouse"
  ]
  node [
    id 302
    label "doprowadzi&#263;"
  ]
  node [
    id 303
    label "z&#322;apa&#263;"
  ]
  node [
    id 304
    label "consume"
  ]
  node [
    id 305
    label "deprive"
  ]
  node [
    id 306
    label "abstract"
  ]
  node [
    id 307
    label "uda&#263;_si&#281;"
  ]
  node [
    id 308
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 309
    label "u&#380;y&#263;"
  ]
  node [
    id 310
    label "utilize"
  ]
  node [
    id 311
    label "uzyska&#263;"
  ]
  node [
    id 312
    label "zrobi&#263;"
  ]
  node [
    id 313
    label "odziedziczy&#263;"
  ]
  node [
    id 314
    label "ruszy&#263;"
  ]
  node [
    id 315
    label "take"
  ]
  node [
    id 316
    label "zaatakowa&#263;"
  ]
  node [
    id 317
    label "uciec"
  ]
  node [
    id 318
    label "receive"
  ]
  node [
    id 319
    label "nakaza&#263;"
  ]
  node [
    id 320
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 321
    label "obskoczy&#263;"
  ]
  node [
    id 322
    label "bra&#263;"
  ]
  node [
    id 323
    label "get"
  ]
  node [
    id 324
    label "wyrucha&#263;"
  ]
  node [
    id 325
    label "World_Health_Organization"
  ]
  node [
    id 326
    label "wyciupcia&#263;"
  ]
  node [
    id 327
    label "wygra&#263;"
  ]
  node [
    id 328
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 329
    label "wzi&#281;cie"
  ]
  node [
    id 330
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 331
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 332
    label "poczyta&#263;"
  ]
  node [
    id 333
    label "aim"
  ]
  node [
    id 334
    label "chwyci&#263;"
  ]
  node [
    id 335
    label "przyj&#261;&#263;"
  ]
  node [
    id 336
    label "pokona&#263;"
  ]
  node [
    id 337
    label "arise"
  ]
  node [
    id 338
    label "zacz&#261;&#263;"
  ]
  node [
    id 339
    label "otrzyma&#263;"
  ]
  node [
    id 340
    label "wej&#347;&#263;"
  ]
  node [
    id 341
    label "poruszy&#263;"
  ]
  node [
    id 342
    label "dosta&#263;"
  ]
  node [
    id 343
    label "powstrzyma&#263;"
  ]
  node [
    id 344
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 345
    label "manipulate"
  ]
  node [
    id 346
    label "rule"
  ]
  node [
    id 347
    label "wytworzy&#263;"
  ]
  node [
    id 348
    label "picture"
  ]
  node [
    id 349
    label "embrace"
  ]
  node [
    id 350
    label "assume"
  ]
  node [
    id 351
    label "podj&#261;&#263;"
  ]
  node [
    id 352
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 353
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 354
    label "skuma&#263;"
  ]
  node [
    id 355
    label "obejmowa&#263;"
  ]
  node [
    id 356
    label "zagarn&#261;&#263;"
  ]
  node [
    id 357
    label "obj&#281;cie"
  ]
  node [
    id 358
    label "involve"
  ]
  node [
    id 359
    label "dotkn&#261;&#263;"
  ]
  node [
    id 360
    label "follow_through"
  ]
  node [
    id 361
    label "perform"
  ]
  node [
    id 362
    label "play_along"
  ]
  node [
    id 363
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 364
    label "umie&#347;ci&#263;"
  ]
  node [
    id 365
    label "zainteresowa&#263;"
  ]
  node [
    id 366
    label "ut"
  ]
  node [
    id 367
    label "d&#378;wi&#281;k"
  ]
  node [
    id 368
    label "C"
  ]
  node [
    id 369
    label "his"
  ]
  node [
    id 370
    label "urz&#281;dnik"
  ]
  node [
    id 371
    label "podkomorzy"
  ]
  node [
    id 372
    label "ch&#322;op"
  ]
  node [
    id 373
    label "urz&#281;dnik_s&#261;dowy"
  ]
  node [
    id 374
    label "zajmowa&#263;"
  ]
  node [
    id 375
    label "bezrolny"
  ]
  node [
    id 376
    label "lokator"
  ]
  node [
    id 377
    label "sekutnik"
  ]
  node [
    id 378
    label "inkorporowa&#263;"
  ]
  node [
    id 379
    label "u&#380;ywa&#263;"
  ]
  node [
    id 380
    label "annex"
  ]
  node [
    id 381
    label "d&#378;wign&#261;&#263;"
  ]
  node [
    id 382
    label "zaszkodzi&#263;"
  ]
  node [
    id 383
    label "put"
  ]
  node [
    id 384
    label "deal"
  ]
  node [
    id 385
    label "set"
  ]
  node [
    id 386
    label "distribute"
  ]
  node [
    id 387
    label "nakarmi&#263;"
  ]
  node [
    id 388
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 389
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 390
    label "division"
  ]
  node [
    id 391
    label "plasowa&#263;_si&#281;"
  ]
  node [
    id 392
    label "wytw&#243;r"
  ]
  node [
    id 393
    label "uplasowa&#263;_si&#281;"
  ]
  node [
    id 394
    label "podzia&#322;"
  ]
  node [
    id 395
    label "plasowanie_si&#281;"
  ]
  node [
    id 396
    label "stopie&#324;"
  ]
  node [
    id 397
    label "poj&#281;cie"
  ]
  node [
    id 398
    label "kolejno&#347;&#263;"
  ]
  node [
    id 399
    label "uplasowanie_si&#281;"
  ]
  node [
    id 400
    label "competence"
  ]
  node [
    id 401
    label "ocena"
  ]
  node [
    id 402
    label "distribution"
  ]
  node [
    id 403
    label "warunek_lokalowy"
  ]
  node [
    id 404
    label "plac"
  ]
  node [
    id 405
    label "location"
  ]
  node [
    id 406
    label "uwaga"
  ]
  node [
    id 407
    label "przestrze&#324;"
  ]
  node [
    id 408
    label "status"
  ]
  node [
    id 409
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 410
    label "chwila"
  ]
  node [
    id 411
    label "cecha"
  ]
  node [
    id 412
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 413
    label "rz&#261;d"
  ]
  node [
    id 414
    label "charakterystyka"
  ]
  node [
    id 415
    label "m&#322;ot"
  ]
  node [
    id 416
    label "znak"
  ]
  node [
    id 417
    label "drzewo"
  ]
  node [
    id 418
    label "pr&#243;ba"
  ]
  node [
    id 419
    label "attribute"
  ]
  node [
    id 420
    label "marka"
  ]
  node [
    id 421
    label "wypowied&#378;"
  ]
  node [
    id 422
    label "stan"
  ]
  node [
    id 423
    label "nagana"
  ]
  node [
    id 424
    label "tekst"
  ]
  node [
    id 425
    label "upomnienie"
  ]
  node [
    id 426
    label "dzienniczek"
  ]
  node [
    id 427
    label "wzgl&#261;d"
  ]
  node [
    id 428
    label "gossip"
  ]
  node [
    id 429
    label "Rzym_Zachodni"
  ]
  node [
    id 430
    label "whole"
  ]
  node [
    id 431
    label "ilo&#347;&#263;"
  ]
  node [
    id 432
    label "element"
  ]
  node [
    id 433
    label "Rzym_Wschodni"
  ]
  node [
    id 434
    label "urz&#261;dzenie"
  ]
  node [
    id 435
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 436
    label "najem"
  ]
  node [
    id 437
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 438
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 439
    label "zak&#322;ad"
  ]
  node [
    id 440
    label "stosunek_pracy"
  ]
  node [
    id 441
    label "benedykty&#324;ski"
  ]
  node [
    id 442
    label "poda&#380;_pracy"
  ]
  node [
    id 443
    label "pracowanie"
  ]
  node [
    id 444
    label "tyrka"
  ]
  node [
    id 445
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 446
    label "zaw&#243;d"
  ]
  node [
    id 447
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 448
    label "tynkarski"
  ]
  node [
    id 449
    label "pracowa&#263;"
  ]
  node [
    id 450
    label "zmiana"
  ]
  node [
    id 451
    label "czynnik_produkcji"
  ]
  node [
    id 452
    label "zobowi&#261;zanie"
  ]
  node [
    id 453
    label "kierownictwo"
  ]
  node [
    id 454
    label "siedziba"
  ]
  node [
    id 455
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 456
    label "rozdzielanie"
  ]
  node [
    id 457
    label "bezbrze&#380;e"
  ]
  node [
    id 458
    label "punkt"
  ]
  node [
    id 459
    label "czasoprzestrze&#324;"
  ]
  node [
    id 460
    label "zbi&#243;r"
  ]
  node [
    id 461
    label "niezmierzony"
  ]
  node [
    id 462
    label "przedzielenie"
  ]
  node [
    id 463
    label "nielito&#347;ciwy"
  ]
  node [
    id 464
    label "rozdziela&#263;"
  ]
  node [
    id 465
    label "oktant"
  ]
  node [
    id 466
    label "przedzieli&#263;"
  ]
  node [
    id 467
    label "przestw&#243;r"
  ]
  node [
    id 468
    label "condition"
  ]
  node [
    id 469
    label "awansowa&#263;"
  ]
  node [
    id 470
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 471
    label "znaczenie"
  ]
  node [
    id 472
    label "awans"
  ]
  node [
    id 473
    label "podmiotowo"
  ]
  node [
    id 474
    label "awansowanie"
  ]
  node [
    id 475
    label "sytuacja"
  ]
  node [
    id 476
    label "time"
  ]
  node [
    id 477
    label "czas"
  ]
  node [
    id 478
    label "rozmiar"
  ]
  node [
    id 479
    label "liczba"
  ]
  node [
    id 480
    label "circumference"
  ]
  node [
    id 481
    label "leksem"
  ]
  node [
    id 482
    label "cyrkumferencja"
  ]
  node [
    id 483
    label "strona"
  ]
  node [
    id 484
    label "ekshumowanie"
  ]
  node [
    id 485
    label "jednostka_organizacyjna"
  ]
  node [
    id 486
    label "p&#322;aszczyzna"
  ]
  node [
    id 487
    label "odwadnia&#263;"
  ]
  node [
    id 488
    label "zabalsamowanie"
  ]
  node [
    id 489
    label "zesp&#243;&#322;"
  ]
  node [
    id 490
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 491
    label "odwodni&#263;"
  ]
  node [
    id 492
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 493
    label "staw"
  ]
  node [
    id 494
    label "ow&#322;osienie"
  ]
  node [
    id 495
    label "mi&#281;so"
  ]
  node [
    id 496
    label "zabalsamowa&#263;"
  ]
  node [
    id 497
    label "Izba_Konsyliarska"
  ]
  node [
    id 498
    label "unerwienie"
  ]
  node [
    id 499
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 500
    label "kremacja"
  ]
  node [
    id 501
    label "biorytm"
  ]
  node [
    id 502
    label "sekcja"
  ]
  node [
    id 503
    label "istota_&#380;ywa"
  ]
  node [
    id 504
    label "otworzy&#263;"
  ]
  node [
    id 505
    label "otwiera&#263;"
  ]
  node [
    id 506
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 507
    label "otworzenie"
  ]
  node [
    id 508
    label "materia"
  ]
  node [
    id 509
    label "pochowanie"
  ]
  node [
    id 510
    label "otwieranie"
  ]
  node [
    id 511
    label "ty&#322;"
  ]
  node [
    id 512
    label "szkielet"
  ]
  node [
    id 513
    label "tanatoplastyk"
  ]
  node [
    id 514
    label "odwadnianie"
  ]
  node [
    id 515
    label "Komitet_Region&#243;w"
  ]
  node [
    id 516
    label "odwodnienie"
  ]
  node [
    id 517
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 518
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 519
    label "nieumar&#322;y"
  ]
  node [
    id 520
    label "pochowa&#263;"
  ]
  node [
    id 521
    label "balsamowa&#263;"
  ]
  node [
    id 522
    label "tanatoplastyka"
  ]
  node [
    id 523
    label "temperatura"
  ]
  node [
    id 524
    label "ekshumowa&#263;"
  ]
  node [
    id 525
    label "balsamowanie"
  ]
  node [
    id 526
    label "uk&#322;ad"
  ]
  node [
    id 527
    label "prz&#243;d"
  ]
  node [
    id 528
    label "l&#281;d&#378;wie"
  ]
  node [
    id 529
    label "cz&#322;onek"
  ]
  node [
    id 530
    label "pogrzeb"
  ]
  node [
    id 531
    label "&#321;ubianka"
  ]
  node [
    id 532
    label "area"
  ]
  node [
    id 533
    label "Majdan"
  ]
  node [
    id 534
    label "pole_bitwy"
  ]
  node [
    id 535
    label "stoisko"
  ]
  node [
    id 536
    label "obszar"
  ]
  node [
    id 537
    label "pierzeja"
  ]
  node [
    id 538
    label "obiekt_handlowy"
  ]
  node [
    id 539
    label "zgromadzenie"
  ]
  node [
    id 540
    label "miasto"
  ]
  node [
    id 541
    label "targowica"
  ]
  node [
    id 542
    label "kram"
  ]
  node [
    id 543
    label "przybli&#380;enie"
  ]
  node [
    id 544
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 545
    label "kategoria"
  ]
  node [
    id 546
    label "szpaler"
  ]
  node [
    id 547
    label "lon&#380;a"
  ]
  node [
    id 548
    label "uporz&#261;dkowanie"
  ]
  node [
    id 549
    label "instytucja"
  ]
  node [
    id 550
    label "jednostka_systematyczna"
  ]
  node [
    id 551
    label "egzekutywa"
  ]
  node [
    id 552
    label "premier"
  ]
  node [
    id 553
    label "Londyn"
  ]
  node [
    id 554
    label "gabinet_cieni"
  ]
  node [
    id 555
    label "gromada"
  ]
  node [
    id 556
    label "number"
  ]
  node [
    id 557
    label "Konsulat"
  ]
  node [
    id 558
    label "tract"
  ]
  node [
    id 559
    label "klasa"
  ]
  node [
    id 560
    label "w&#322;adza"
  ]
  node [
    id 561
    label "zinterpretowa&#263;"
  ]
  node [
    id 562
    label "spoziera&#263;"
  ]
  node [
    id 563
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 564
    label "popatrze&#263;"
  ]
  node [
    id 565
    label "pojrze&#263;"
  ]
  node [
    id 566
    label "peek"
  ]
  node [
    id 567
    label "see"
  ]
  node [
    id 568
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 569
    label "oceni&#263;"
  ]
  node [
    id 570
    label "zagra&#263;"
  ]
  node [
    id 571
    label "illustrate"
  ]
  node [
    id 572
    label "zanalizowa&#263;"
  ]
  node [
    id 573
    label "read"
  ]
  node [
    id 574
    label "think"
  ]
  node [
    id 575
    label "visualize"
  ]
  node [
    id 576
    label "sta&#263;_si&#281;"
  ]
  node [
    id 577
    label "zobaczy&#263;"
  ]
  node [
    id 578
    label "spogl&#261;da&#263;"
  ]
  node [
    id 579
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 580
    label "dolar"
  ]
  node [
    id 581
    label "USA"
  ]
  node [
    id 582
    label "majny"
  ]
  node [
    id 583
    label "Ekwador"
  ]
  node [
    id 584
    label "Bonaire"
  ]
  node [
    id 585
    label "Sint_Eustatius"
  ]
  node [
    id 586
    label "zzielenienie"
  ]
  node [
    id 587
    label "zielono"
  ]
  node [
    id 588
    label "Portoryko"
  ]
  node [
    id 589
    label "dzia&#322;acz"
  ]
  node [
    id 590
    label "zazielenianie"
  ]
  node [
    id 591
    label "Panama"
  ]
  node [
    id 592
    label "Mikronezja"
  ]
  node [
    id 593
    label "zazielenienie"
  ]
  node [
    id 594
    label "niedojrza&#322;y"
  ]
  node [
    id 595
    label "pokryty"
  ]
  node [
    id 596
    label "socjalista"
  ]
  node [
    id 597
    label "Mariany_P&#243;&#322;nocne"
  ]
  node [
    id 598
    label "Saba"
  ]
  node [
    id 599
    label "zwolennik"
  ]
  node [
    id 600
    label "Timor_Wschodni"
  ]
  node [
    id 601
    label "polityk"
  ]
  node [
    id 602
    label "zieloni"
  ]
  node [
    id 603
    label "Wyspy_Marshalla"
  ]
  node [
    id 604
    label "naturalny"
  ]
  node [
    id 605
    label "Palau"
  ]
  node [
    id 606
    label "Zimbabwe"
  ]
  node [
    id 607
    label "blady"
  ]
  node [
    id 608
    label "zielenienie"
  ]
  node [
    id 609
    label "&#380;ywy"
  ]
  node [
    id 610
    label "ch&#322;odny"
  ]
  node [
    id 611
    label "&#347;wie&#380;y"
  ]
  node [
    id 612
    label "Salwador"
  ]
  node [
    id 613
    label "nowy"
  ]
  node [
    id 614
    label "&#347;wie&#380;o"
  ]
  node [
    id 615
    label "surowy"
  ]
  node [
    id 616
    label "orze&#378;wienie"
  ]
  node [
    id 617
    label "energiczny"
  ]
  node [
    id 618
    label "orze&#378;wianie"
  ]
  node [
    id 619
    label "rze&#347;ki"
  ]
  node [
    id 620
    label "zdrowy"
  ]
  node [
    id 621
    label "czysty"
  ]
  node [
    id 622
    label "oryginalnie"
  ]
  node [
    id 623
    label "o&#380;ywczy"
  ]
  node [
    id 624
    label "m&#322;ody"
  ]
  node [
    id 625
    label "orze&#378;wiaj&#261;co"
  ]
  node [
    id 626
    label "inny"
  ]
  node [
    id 627
    label "soczysty"
  ]
  node [
    id 628
    label "nowotny"
  ]
  node [
    id 629
    label "niedobry"
  ]
  node [
    id 630
    label "nie&#347;wie&#380;o"
  ]
  node [
    id 631
    label "nieoryginalnie"
  ]
  node [
    id 632
    label "brzydki"
  ]
  node [
    id 633
    label "przesta&#322;y"
  ]
  node [
    id 634
    label "zm&#281;czony"
  ]
  node [
    id 635
    label "u&#380;ywany"
  ]
  node [
    id 636
    label "unoriginal"
  ]
  node [
    id 637
    label "Asnyk"
  ]
  node [
    id 638
    label "Michnik"
  ]
  node [
    id 639
    label "Owsiak"
  ]
  node [
    id 640
    label "niegotowy"
  ]
  node [
    id 641
    label "pocz&#261;tkowy"
  ]
  node [
    id 642
    label "nieodpowiedzialny"
  ]
  node [
    id 643
    label "niedojrzale"
  ]
  node [
    id 644
    label "dziwny"
  ]
  node [
    id 645
    label "g&#322;upi"
  ]
  node [
    id 646
    label "socja&#322;"
  ]
  node [
    id 647
    label "lewicowiec"
  ]
  node [
    id 648
    label "Gorbaczow"
  ]
  node [
    id 649
    label "Korwin"
  ]
  node [
    id 650
    label "McCarthy"
  ]
  node [
    id 651
    label "Goebbels"
  ]
  node [
    id 652
    label "Miko&#322;ajczyk"
  ]
  node [
    id 653
    label "Ziobro"
  ]
  node [
    id 654
    label "Katon"
  ]
  node [
    id 655
    label "Moczar"
  ]
  node [
    id 656
    label "Gierek"
  ]
  node [
    id 657
    label "Arafat"
  ]
  node [
    id 658
    label "Vladimir_Me&#269;iar"
  ]
  node [
    id 659
    label "Naser"
  ]
  node [
    id 660
    label "Bre&#380;niew"
  ]
  node [
    id 661
    label "Mao"
  ]
  node [
    id 662
    label "Nixon"
  ]
  node [
    id 663
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 664
    label "Perykles"
  ]
  node [
    id 665
    label "Metternich"
  ]
  node [
    id 666
    label "Kuro&#324;"
  ]
  node [
    id 667
    label "Borel"
  ]
  node [
    id 668
    label "Juliusz_Cezar"
  ]
  node [
    id 669
    label "Bierut"
  ]
  node [
    id 670
    label "bezpartyjny"
  ]
  node [
    id 671
    label "Leszek_Miller"
  ]
  node [
    id 672
    label "Falandysz"
  ]
  node [
    id 673
    label "Fidel_Castro"
  ]
  node [
    id 674
    label "Winston_Churchill"
  ]
  node [
    id 675
    label "Sto&#322;ypin"
  ]
  node [
    id 676
    label "Putin"
  ]
  node [
    id 677
    label "J&#281;drzejewicz"
  ]
  node [
    id 678
    label "Chruszczow"
  ]
  node [
    id 679
    label "de_Gaulle"
  ]
  node [
    id 680
    label "Jaros&#322;aw_Kaczy&#324;ski"
  ]
  node [
    id 681
    label "Gomu&#322;ka"
  ]
  node [
    id 682
    label "prawy"
  ]
  node [
    id 683
    label "immanentny"
  ]
  node [
    id 684
    label "zwyczajny"
  ]
  node [
    id 685
    label "bezsporny"
  ]
  node [
    id 686
    label "organicznie"
  ]
  node [
    id 687
    label "pierwotny"
  ]
  node [
    id 688
    label "neutralny"
  ]
  node [
    id 689
    label "normalny"
  ]
  node [
    id 690
    label "rzeczywisty"
  ]
  node [
    id 691
    label "naturalnie"
  ]
  node [
    id 692
    label "szybki"
  ]
  node [
    id 693
    label "&#380;ywotny"
  ]
  node [
    id 694
    label "&#380;ywo"
  ]
  node [
    id 695
    label "o&#380;ywianie"
  ]
  node [
    id 696
    label "silny"
  ]
  node [
    id 697
    label "g&#322;&#281;boki"
  ]
  node [
    id 698
    label "wyra&#378;ny"
  ]
  node [
    id 699
    label "czynny"
  ]
  node [
    id 700
    label "aktualny"
  ]
  node [
    id 701
    label "zgrabny"
  ]
  node [
    id 702
    label "realistyczny"
  ]
  node [
    id 703
    label "jednostka_monetarna"
  ]
  node [
    id 704
    label "cent"
  ]
  node [
    id 705
    label "nieatrakcyjny"
  ]
  node [
    id 706
    label "blado"
  ]
  node [
    id 707
    label "mizerny"
  ]
  node [
    id 708
    label "niezabawny"
  ]
  node [
    id 709
    label "nienasycony"
  ]
  node [
    id 710
    label "s&#322;aby"
  ]
  node [
    id 711
    label "niewa&#380;ny"
  ]
  node [
    id 712
    label "oboj&#281;tny"
  ]
  node [
    id 713
    label "poszarzenie"
  ]
  node [
    id 714
    label "szarzenie"
  ]
  node [
    id 715
    label "bezbarwnie"
  ]
  node [
    id 716
    label "nieciekawy"
  ]
  node [
    id 717
    label "zi&#281;bienie"
  ]
  node [
    id 718
    label "niesympatyczny"
  ]
  node [
    id 719
    label "och&#322;odzenie"
  ]
  node [
    id 720
    label "opanowany"
  ]
  node [
    id 721
    label "ch&#322;odzenie_si&#281;"
  ]
  node [
    id 722
    label "rozs&#261;dny"
  ]
  node [
    id 723
    label "och&#322;odzenie_si&#281;"
  ]
  node [
    id 724
    label "sch&#322;adzanie"
  ]
  node [
    id 725
    label "ch&#322;odno"
  ]
  node [
    id 726
    label "covered"
  ]
  node [
    id 727
    label "pokrywanie"
  ]
  node [
    id 728
    label "organizacja"
  ]
  node [
    id 729
    label "partia"
  ]
  node [
    id 730
    label "lewica"
  ]
  node [
    id 731
    label "balboa"
  ]
  node [
    id 732
    label "Ameryka_Centralna"
  ]
  node [
    id 733
    label "Oceania"
  ]
  node [
    id 734
    label "Antyle_Holenderskie"
  ]
  node [
    id 735
    label "Karaiby"
  ]
  node [
    id 736
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 737
    label "Ohio"
  ]
  node [
    id 738
    label "P&#243;&#322;noc"
  ]
  node [
    id 739
    label "Nowy_York"
  ]
  node [
    id 740
    label "Illinois"
  ]
  node [
    id 741
    label "Po&#322;udnie"
  ]
  node [
    id 742
    label "Kalifornia"
  ]
  node [
    id 743
    label "Wirginia"
  ]
  node [
    id 744
    label "Teksas"
  ]
  node [
    id 745
    label "Waszyngton"
  ]
  node [
    id 746
    label "zielona_karta"
  ]
  node [
    id 747
    label "Alaska"
  ]
  node [
    id 748
    label "Massachusetts"
  ]
  node [
    id 749
    label "Hawaje"
  ]
  node [
    id 750
    label "NATO"
  ]
  node [
    id 751
    label "Maryland"
  ]
  node [
    id 752
    label "Michigan"
  ]
  node [
    id 753
    label "Arizona"
  ]
  node [
    id 754
    label "Georgia"
  ]
  node [
    id 755
    label "stan_wolny"
  ]
  node [
    id 756
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 757
    label "Pensylwania"
  ]
  node [
    id 758
    label "Luizjana"
  ]
  node [
    id 759
    label "Nowy_Meksyk"
  ]
  node [
    id 760
    label "Wuj_Sam"
  ]
  node [
    id 761
    label "Alabama"
  ]
  node [
    id 762
    label "Kansas"
  ]
  node [
    id 763
    label "Oregon"
  ]
  node [
    id 764
    label "Zach&#243;d"
  ]
  node [
    id 765
    label "Oklahoma"
  ]
  node [
    id 766
    label "Floryda"
  ]
  node [
    id 767
    label "Hudson"
  ]
  node [
    id 768
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 769
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 770
    label "Nauru"
  ]
  node [
    id 771
    label "Mariany"
  ]
  node [
    id 772
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 773
    label "dolar_Zimbabwe"
  ]
  node [
    id 774
    label "sadzenie"
  ]
  node [
    id 775
    label "bledni&#281;cie"
  ]
  node [
    id 776
    label "zabarwienie"
  ]
  node [
    id 777
    label "zasadzenie"
  ]
  node [
    id 778
    label "go&#322;&#261;b"
  ]
  node [
    id 779
    label "siwienie"
  ]
  node [
    id 780
    label "posiwienie"
  ]
  node [
    id 781
    label "siwo"
  ]
  node [
    id 782
    label "szymel"
  ]
  node [
    id 783
    label "ko&#324;"
  ]
  node [
    id 784
    label "jasnoszary"
  ]
  node [
    id 785
    label "pobielenie"
  ]
  node [
    id 786
    label "nieprzejrzysty"
  ]
  node [
    id 787
    label "niepewny"
  ]
  node [
    id 788
    label "m&#261;cenie"
  ]
  node [
    id 789
    label "trudny"
  ]
  node [
    id 790
    label "ciecz"
  ]
  node [
    id 791
    label "niejawny"
  ]
  node [
    id 792
    label "ci&#281;&#380;ki"
  ]
  node [
    id 793
    label "ciemny"
  ]
  node [
    id 794
    label "nieklarowny"
  ]
  node [
    id 795
    label "niezrozumia&#322;y"
  ]
  node [
    id 796
    label "zanieczyszczanie"
  ]
  node [
    id 797
    label "zanieczyszczenie"
  ]
  node [
    id 798
    label "nieprzejrzy&#347;cie"
  ]
  node [
    id 799
    label "z&#322;y"
  ]
  node [
    id 800
    label "k&#322;usowa&#263;"
  ]
  node [
    id 801
    label "nar&#243;w"
  ]
  node [
    id 802
    label "&#322;ykawo&#347;&#263;"
  ]
  node [
    id 803
    label "galopowa&#263;"
  ]
  node [
    id 804
    label "koniowate"
  ]
  node [
    id 805
    label "pogalopowanie"
  ]
  node [
    id 806
    label "zaci&#281;cie"
  ]
  node [
    id 807
    label "galopowanie"
  ]
  node [
    id 808
    label "osadza&#263;_si&#281;"
  ]
  node [
    id 809
    label "zar&#380;e&#263;"
  ]
  node [
    id 810
    label "k&#322;usowanie"
  ]
  node [
    id 811
    label "narowienie"
  ]
  node [
    id 812
    label "znarowi&#263;"
  ]
  node [
    id 813
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 814
    label "pok&#322;usowanie"
  ]
  node [
    id 815
    label "kawalerzysta"
  ]
  node [
    id 816
    label "pok&#322;usowa&#263;"
  ]
  node [
    id 817
    label "hipoterapia"
  ]
  node [
    id 818
    label "hipoterapeuta"
  ]
  node [
    id 819
    label "zebrula"
  ]
  node [
    id 820
    label "zaci&#261;&#263;"
  ]
  node [
    id 821
    label "lansada"
  ]
  node [
    id 822
    label "uje&#380;d&#380;anie"
  ]
  node [
    id 823
    label "narowi&#263;"
  ]
  node [
    id 824
    label "osadzi&#263;_si&#281;"
  ]
  node [
    id 825
    label "r&#380;enie"
  ]
  node [
    id 826
    label "figura"
  ]
  node [
    id 827
    label "osadzanie_si&#281;"
  ]
  node [
    id 828
    label "zebroid"
  ]
  node [
    id 829
    label "os&#322;omu&#322;"
  ]
  node [
    id 830
    label "r&#380;e&#263;"
  ]
  node [
    id 831
    label "przegalopowa&#263;"
  ]
  node [
    id 832
    label "podkuwanie"
  ]
  node [
    id 833
    label "karmiak"
  ]
  node [
    id 834
    label "podkuwa&#263;"
  ]
  node [
    id 835
    label "penis"
  ]
  node [
    id 836
    label "znarowienie"
  ]
  node [
    id 837
    label "czo&#322;dar"
  ]
  node [
    id 838
    label "zwierz&#281;_wierzchowe"
  ]
  node [
    id 839
    label "remuda"
  ]
  node [
    id 840
    label "przegalopowanie"
  ]
  node [
    id 841
    label "pogalopowa&#263;"
  ]
  node [
    id 842
    label "dosiad"
  ]
  node [
    id 843
    label "ko&#324;_dziki"
  ]
  node [
    id 844
    label "osadzenie_si&#281;"
  ]
  node [
    id 845
    label "jasnoszaro"
  ]
  node [
    id 846
    label "w&#322;osy"
  ]
  node [
    id 847
    label "stanie_si&#281;"
  ]
  node [
    id 848
    label "pomalowanie"
  ]
  node [
    id 849
    label "bielenie"
  ]
  node [
    id 850
    label "odbarwienie_si&#281;"
  ]
  node [
    id 851
    label "spowodowanie"
  ]
  node [
    id 852
    label "zabezpieczenie"
  ]
  node [
    id 853
    label "zabielenie_si&#281;"
  ]
  node [
    id 854
    label "ptak"
  ]
  node [
    id 855
    label "grucha&#263;"
  ]
  node [
    id 856
    label "zagrucha&#263;"
  ]
  node [
    id 857
    label "go&#322;&#281;bie"
  ]
  node [
    id 858
    label "gruchanie"
  ]
  node [
    id 859
    label "pigeon"
  ]
  node [
    id 860
    label "przesy&#322;ka"
  ]
  node [
    id 861
    label "whitening"
  ]
  node [
    id 862
    label "stawanie_si&#281;"
  ]
  node [
    id 863
    label "model"
  ]
  node [
    id 864
    label "siwek"
  ]
  node [
    id 865
    label "ma&#347;&#263;"
  ]
  node [
    id 866
    label "wyd&#322;u&#380;a&#263;"
  ]
  node [
    id 867
    label "opuszcza&#263;"
  ]
  node [
    id 868
    label "zaniedbywa&#263;"
  ]
  node [
    id 869
    label "za&#347;wieca&#263;"
  ]
  node [
    id 870
    label "reject"
  ]
  node [
    id 871
    label "umieszcza&#263;"
  ]
  node [
    id 872
    label "hodowa&#263;"
  ]
  node [
    id 873
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 874
    label "lengthen"
  ]
  node [
    id 875
    label "draw"
  ]
  node [
    id 876
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 877
    label "plasowa&#263;"
  ]
  node [
    id 878
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 879
    label "pomieszcza&#263;"
  ]
  node [
    id 880
    label "accommodate"
  ]
  node [
    id 881
    label "zmienia&#263;"
  ]
  node [
    id 882
    label "venture"
  ]
  node [
    id 883
    label "wpiernicza&#263;"
  ]
  node [
    id 884
    label "okre&#347;la&#263;"
  ]
  node [
    id 885
    label "oversight"
  ]
  node [
    id 886
    label "lekcewa&#380;y&#263;"
  ]
  node [
    id 887
    label "pozostawia&#263;"
  ]
  node [
    id 888
    label "traci&#263;"
  ]
  node [
    id 889
    label "obni&#380;a&#263;"
  ]
  node [
    id 890
    label "abort"
  ]
  node [
    id 891
    label "omija&#263;"
  ]
  node [
    id 892
    label "przestawa&#263;"
  ]
  node [
    id 893
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 894
    label "forowa&#263;_si&#281;"
  ]
  node [
    id 895
    label "potania&#263;"
  ]
  node [
    id 896
    label "goban"
  ]
  node [
    id 897
    label "gra_planszowa"
  ]
  node [
    id 898
    label "sport_umys&#322;owy"
  ]
  node [
    id 899
    label "chi&#324;ski"
  ]
  node [
    id 900
    label "raise"
  ]
  node [
    id 901
    label "pole"
  ]
  node [
    id 902
    label "uprawienie"
  ]
  node [
    id 903
    label "u&#322;o&#380;enie"
  ]
  node [
    id 904
    label "p&#322;osa"
  ]
  node [
    id 905
    label "ziemia"
  ]
  node [
    id 906
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 907
    label "gospodarstwo"
  ]
  node [
    id 908
    label "uprawi&#263;"
  ]
  node [
    id 909
    label "room"
  ]
  node [
    id 910
    label "dw&#243;r"
  ]
  node [
    id 911
    label "okazja"
  ]
  node [
    id 912
    label "irygowanie"
  ]
  node [
    id 913
    label "compass"
  ]
  node [
    id 914
    label "square"
  ]
  node [
    id 915
    label "zmienna"
  ]
  node [
    id 916
    label "irygowa&#263;"
  ]
  node [
    id 917
    label "socjologia"
  ]
  node [
    id 918
    label "boisko"
  ]
  node [
    id 919
    label "dziedzina"
  ]
  node [
    id 920
    label "baza_danych"
  ]
  node [
    id 921
    label "region"
  ]
  node [
    id 922
    label "sk&#322;ad"
  ]
  node [
    id 923
    label "powierzchnia"
  ]
  node [
    id 924
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 925
    label "plane"
  ]
  node [
    id 926
    label "radlina"
  ]
  node [
    id 927
    label "&#347;rodowisko"
  ]
  node [
    id 928
    label "plan"
  ]
  node [
    id 929
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 930
    label "gestaltyzm"
  ]
  node [
    id 931
    label "background"
  ]
  node [
    id 932
    label "melodia"
  ]
  node [
    id 933
    label "causal_agent"
  ]
  node [
    id 934
    label "dalszoplanowy"
  ]
  node [
    id 935
    label "warunki"
  ]
  node [
    id 936
    label "pod&#322;o&#380;e"
  ]
  node [
    id 937
    label "obiekt"
  ]
  node [
    id 938
    label "informacja"
  ]
  node [
    id 939
    label "obraz"
  ]
  node [
    id 940
    label "layer"
  ]
  node [
    id 941
    label "lingwistyka_kognitywna"
  ]
  node [
    id 942
    label "intencja"
  ]
  node [
    id 943
    label "rysunek"
  ]
  node [
    id 944
    label "miejsce_pracy"
  ]
  node [
    id 945
    label "device"
  ]
  node [
    id 946
    label "pomys&#322;"
  ]
  node [
    id 947
    label "reprezentacja"
  ]
  node [
    id 948
    label "agreement"
  ]
  node [
    id 949
    label "dekoracja"
  ]
  node [
    id 950
    label "perspektywa"
  ]
  node [
    id 951
    label "wymiar"
  ]
  node [
    id 952
    label "&#347;ciana"
  ]
  node [
    id 953
    label "surface"
  ]
  node [
    id 954
    label "zakres"
  ]
  node [
    id 955
    label "kwadrant"
  ]
  node [
    id 956
    label "degree"
  ]
  node [
    id 957
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 958
    label "ukszta&#322;towanie"
  ]
  node [
    id 959
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 960
    label "p&#322;aszczak"
  ]
  node [
    id 961
    label "wygl&#261;d"
  ]
  node [
    id 962
    label "r&#243;&#380;nokolorowy"
  ]
  node [
    id 963
    label "barwny"
  ]
  node [
    id 964
    label "przybranie"
  ]
  node [
    id 965
    label "color"
  ]
  node [
    id 966
    label "tone"
  ]
  node [
    id 967
    label "litosfera"
  ]
  node [
    id 968
    label "dotleni&#263;"
  ]
  node [
    id 969
    label "pr&#243;chnica"
  ]
  node [
    id 970
    label "glej"
  ]
  node [
    id 971
    label "martwica"
  ]
  node [
    id 972
    label "glinowa&#263;"
  ]
  node [
    id 973
    label "podglebie"
  ]
  node [
    id 974
    label "ryzosfera"
  ]
  node [
    id 975
    label "kompleks_sorpcyjny"
  ]
  node [
    id 976
    label "przyczyna"
  ]
  node [
    id 977
    label "geosystem"
  ]
  node [
    id 978
    label "glinowanie"
  ]
  node [
    id 979
    label "publikacja"
  ]
  node [
    id 980
    label "wiedza"
  ]
  node [
    id 981
    label "doj&#347;cie"
  ]
  node [
    id 982
    label "obiega&#263;"
  ]
  node [
    id 983
    label "powzi&#281;cie"
  ]
  node [
    id 984
    label "dane"
  ]
  node [
    id 985
    label "obiegni&#281;cie"
  ]
  node [
    id 986
    label "sygna&#322;"
  ]
  node [
    id 987
    label "obieganie"
  ]
  node [
    id 988
    label "powzi&#261;&#263;"
  ]
  node [
    id 989
    label "obiec"
  ]
  node [
    id 990
    label "doj&#347;&#263;"
  ]
  node [
    id 991
    label "co&#347;"
  ]
  node [
    id 992
    label "budynek"
  ]
  node [
    id 993
    label "thing"
  ]
  node [
    id 994
    label "program"
  ]
  node [
    id 995
    label "rzecz"
  ]
  node [
    id 996
    label "zanucenie"
  ]
  node [
    id 997
    label "nuta"
  ]
  node [
    id 998
    label "zakosztowa&#263;"
  ]
  node [
    id 999
    label "zajawka"
  ]
  node [
    id 1000
    label "zanuci&#263;"
  ]
  node [
    id 1001
    label "emocja"
  ]
  node [
    id 1002
    label "oskoma"
  ]
  node [
    id 1003
    label "melika"
  ]
  node [
    id 1004
    label "nucenie"
  ]
  node [
    id 1005
    label "nuci&#263;"
  ]
  node [
    id 1006
    label "istota"
  ]
  node [
    id 1007
    label "brzmienie"
  ]
  node [
    id 1008
    label "taste"
  ]
  node [
    id 1009
    label "muzyka"
  ]
  node [
    id 1010
    label "inclination"
  ]
  node [
    id 1011
    label "Bund"
  ]
  node [
    id 1012
    label "PPR"
  ]
  node [
    id 1013
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1014
    label "wybranek"
  ]
  node [
    id 1015
    label "Jakobici"
  ]
  node [
    id 1016
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1017
    label "SLD"
  ]
  node [
    id 1018
    label "Razem"
  ]
  node [
    id 1019
    label "PiS"
  ]
  node [
    id 1020
    label "package"
  ]
  node [
    id 1021
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1022
    label "Kuomintang"
  ]
  node [
    id 1023
    label "ZSL"
  ]
  node [
    id 1024
    label "AWS"
  ]
  node [
    id 1025
    label "gra"
  ]
  node [
    id 1026
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1027
    label "game"
  ]
  node [
    id 1028
    label "grupa"
  ]
  node [
    id 1029
    label "blok"
  ]
  node [
    id 1030
    label "materia&#322;"
  ]
  node [
    id 1031
    label "PO"
  ]
  node [
    id 1032
    label "si&#322;a"
  ]
  node [
    id 1033
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1034
    label "niedoczas"
  ]
  node [
    id 1035
    label "Federali&#347;ci"
  ]
  node [
    id 1036
    label "PSL"
  ]
  node [
    id 1037
    label "Wigowie"
  ]
  node [
    id 1038
    label "ZChN"
  ]
  node [
    id 1039
    label "aktyw"
  ]
  node [
    id 1040
    label "wybranka"
  ]
  node [
    id 1041
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1042
    label "unit"
  ]
  node [
    id 1043
    label "sk&#322;adnik"
  ]
  node [
    id 1044
    label "wydarzenie"
  ]
  node [
    id 1045
    label "class"
  ]
  node [
    id 1046
    label "obiekt_naturalny"
  ]
  node [
    id 1047
    label "otoczenie"
  ]
  node [
    id 1048
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1049
    label "environment"
  ]
  node [
    id 1050
    label "huczek"
  ]
  node [
    id 1051
    label "ekosystem"
  ]
  node [
    id 1052
    label "wszechstworzenie"
  ]
  node [
    id 1053
    label "woda"
  ]
  node [
    id 1054
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 1055
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1056
    label "teren"
  ]
  node [
    id 1057
    label "mikrokosmos"
  ]
  node [
    id 1058
    label "stw&#243;r"
  ]
  node [
    id 1059
    label "Ziemia"
  ]
  node [
    id 1060
    label "fauna"
  ]
  node [
    id 1061
    label "biota"
  ]
  node [
    id 1062
    label "pocz&#261;tki"
  ]
  node [
    id 1063
    label "pochodzenie"
  ]
  node [
    id 1064
    label "kontekst"
  ]
  node [
    id 1065
    label "representation"
  ]
  node [
    id 1066
    label "effigy"
  ]
  node [
    id 1067
    label "podobrazie"
  ]
  node [
    id 1068
    label "scena"
  ]
  node [
    id 1069
    label "human_body"
  ]
  node [
    id 1070
    label "projekcja"
  ]
  node [
    id 1071
    label "oprawia&#263;"
  ]
  node [
    id 1072
    label "postprodukcja"
  ]
  node [
    id 1073
    label "inning"
  ]
  node [
    id 1074
    label "pulment"
  ]
  node [
    id 1075
    label "pogl&#261;d"
  ]
  node [
    id 1076
    label "plama_barwna"
  ]
  node [
    id 1077
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 1078
    label "oprawianie"
  ]
  node [
    id 1079
    label "sztafa&#380;"
  ]
  node [
    id 1080
    label "parkiet"
  ]
  node [
    id 1081
    label "opinion"
  ]
  node [
    id 1082
    label "uj&#281;cie"
  ]
  node [
    id 1083
    label "zaj&#347;cie"
  ]
  node [
    id 1084
    label "persona"
  ]
  node [
    id 1085
    label "filmoteka"
  ]
  node [
    id 1086
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 1087
    label "ziarno"
  ]
  node [
    id 1088
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1089
    label "wypunktowa&#263;"
  ]
  node [
    id 1090
    label "ostro&#347;&#263;"
  ]
  node [
    id 1091
    label "malarz"
  ]
  node [
    id 1092
    label "napisy"
  ]
  node [
    id 1093
    label "przeplot"
  ]
  node [
    id 1094
    label "punktowa&#263;"
  ]
  node [
    id 1095
    label "anamorfoza"
  ]
  node [
    id 1096
    label "przedstawienie"
  ]
  node [
    id 1097
    label "ty&#322;&#243;wka"
  ]
  node [
    id 1098
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 1099
    label "widok"
  ]
  node [
    id 1100
    label "czo&#322;&#243;wka"
  ]
  node [
    id 1101
    label "rola"
  ]
  node [
    id 1102
    label "psychologia"
  ]
  node [
    id 1103
    label "drugoplanowo"
  ]
  node [
    id 1104
    label "nieznaczny"
  ]
  node [
    id 1105
    label "poboczny"
  ]
  node [
    id 1106
    label "dalszy"
  ]
  node [
    id 1107
    label "chmurnienie"
  ]
  node [
    id 1108
    label "p&#322;owy"
  ]
  node [
    id 1109
    label "srebrny"
  ]
  node [
    id 1110
    label "pochmurno"
  ]
  node [
    id 1111
    label "szaro"
  ]
  node [
    id 1112
    label "spochmurnienie"
  ]
  node [
    id 1113
    label "zwyk&#322;y"
  ]
  node [
    id 1114
    label "zbrzydni&#281;cie"
  ]
  node [
    id 1115
    label "skandaliczny"
  ]
  node [
    id 1116
    label "nieprzyjemny"
  ]
  node [
    id 1117
    label "oszpecanie"
  ]
  node [
    id 1118
    label "brzydni&#281;cie"
  ]
  node [
    id 1119
    label "nie&#322;adny"
  ]
  node [
    id 1120
    label "brzydko"
  ]
  node [
    id 1121
    label "nieprzyzwoity"
  ]
  node [
    id 1122
    label "oszpecenie"
  ]
  node [
    id 1123
    label "przeci&#281;tny"
  ]
  node [
    id 1124
    label "zwyczajnie"
  ]
  node [
    id 1125
    label "zwykle"
  ]
  node [
    id 1126
    label "cz&#281;sty"
  ]
  node [
    id 1127
    label "okre&#347;lony"
  ]
  node [
    id 1128
    label "oswojony"
  ]
  node [
    id 1129
    label "na&#322;o&#380;ny"
  ]
  node [
    id 1130
    label "nieciekawie"
  ]
  node [
    id 1131
    label "zoboj&#281;tnienie"
  ]
  node [
    id 1132
    label "nieszkodliwy"
  ]
  node [
    id 1133
    label "&#347;ni&#281;ty"
  ]
  node [
    id 1134
    label "oboj&#281;tnie"
  ]
  node [
    id 1135
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 1136
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 1137
    label "neutralizowanie"
  ]
  node [
    id 1138
    label "bierny"
  ]
  node [
    id 1139
    label "zneutralizowanie"
  ]
  node [
    id 1140
    label "zasnuwanie_si&#281;"
  ]
  node [
    id 1141
    label "powa&#380;nienie"
  ]
  node [
    id 1142
    label "pochmurny"
  ]
  node [
    id 1143
    label "zasnucie_si&#281;"
  ]
  node [
    id 1144
    label "blandly"
  ]
  node [
    id 1145
    label "nieefektownie"
  ]
  node [
    id 1146
    label "nijaki"
  ]
  node [
    id 1147
    label "rozwidnienie_si&#281;"
  ]
  node [
    id 1148
    label "zmierzchni&#281;cie_si&#281;"
  ]
  node [
    id 1149
    label "rozwidnianie_si&#281;"
  ]
  node [
    id 1150
    label "zmierzchanie_si&#281;"
  ]
  node [
    id 1151
    label "srebrzenie"
  ]
  node [
    id 1152
    label "metaliczny"
  ]
  node [
    id 1153
    label "srebrzy&#347;cie"
  ]
  node [
    id 1154
    label "posrebrzenie"
  ]
  node [
    id 1155
    label "srebrzenie_si&#281;"
  ]
  node [
    id 1156
    label "utytu&#322;owany"
  ]
  node [
    id 1157
    label "srebrno"
  ]
  node [
    id 1158
    label "p&#322;owo"
  ]
  node [
    id 1159
    label "&#380;&#243;&#322;tawy"
  ]
  node [
    id 1160
    label "niedobrze"
  ]
  node [
    id 1161
    label "wyblak&#322;y"
  ]
  node [
    id 1162
    label "bezbarwny"
  ]
  node [
    id 1163
    label "przewi&#261;zka"
  ]
  node [
    id 1164
    label "zone"
  ]
  node [
    id 1165
    label "dodatek"
  ]
  node [
    id 1166
    label "naszywka"
  ]
  node [
    id 1167
    label "prevention"
  ]
  node [
    id 1168
    label "oznaka"
  ]
  node [
    id 1169
    label "dyktando"
  ]
  node [
    id 1170
    label "us&#322;uga"
  ]
  node [
    id 1171
    label "spekulacja"
  ]
  node [
    id 1172
    label "handel"
  ]
  node [
    id 1173
    label "zwi&#261;zek"
  ]
  node [
    id 1174
    label "implikowa&#263;"
  ]
  node [
    id 1175
    label "signal"
  ]
  node [
    id 1176
    label "fakt"
  ]
  node [
    id 1177
    label "symbol"
  ]
  node [
    id 1178
    label "naszycie"
  ]
  node [
    id 1179
    label "mundur"
  ]
  node [
    id 1180
    label "logo"
  ]
  node [
    id 1181
    label "band"
  ]
  node [
    id 1182
    label "szamerunek"
  ]
  node [
    id 1183
    label "dochodzenie"
  ]
  node [
    id 1184
    label "przedmiot"
  ]
  node [
    id 1185
    label "doch&#243;d"
  ]
  node [
    id 1186
    label "dziennik"
  ]
  node [
    id 1187
    label "galanteria"
  ]
  node [
    id 1188
    label "aneks"
  ]
  node [
    id 1189
    label "dyktat"
  ]
  node [
    id 1190
    label "&#263;wiczenie"
  ]
  node [
    id 1191
    label "praca_pisemna"
  ]
  node [
    id 1192
    label "sprawdzian"
  ]
  node [
    id 1193
    label "command"
  ]
  node [
    id 1194
    label "wi&#261;zanie"
  ]
  node [
    id 1195
    label "bratnia_dusza"
  ]
  node [
    id 1196
    label "powi&#261;zanie"
  ]
  node [
    id 1197
    label "zwi&#261;zanie"
  ]
  node [
    id 1198
    label "konstytucja"
  ]
  node [
    id 1199
    label "marriage"
  ]
  node [
    id 1200
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1201
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1202
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1203
    label "zwi&#261;za&#263;"
  ]
  node [
    id 1204
    label "marketing_afiliacyjny"
  ]
  node [
    id 1205
    label "substancja_chemiczna"
  ]
  node [
    id 1206
    label "koligacja"
  ]
  node [
    id 1207
    label "bearing"
  ]
  node [
    id 1208
    label "lokant"
  ]
  node [
    id 1209
    label "azeotrop"
  ]
  node [
    id 1210
    label "produkt_gotowy"
  ]
  node [
    id 1211
    label "service"
  ]
  node [
    id 1212
    label "asortyment"
  ]
  node [
    id 1213
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 1214
    label "&#347;wiadczenie"
  ]
  node [
    id 1215
    label "przest&#281;pstwo"
  ]
  node [
    id 1216
    label "manipulacja"
  ]
  node [
    id 1217
    label "domys&#322;"
  ]
  node [
    id 1218
    label "transakcja"
  ]
  node [
    id 1219
    label "dywagacja"
  ]
  node [
    id 1220
    label "adventure"
  ]
  node [
    id 1221
    label "business"
  ]
  node [
    id 1222
    label "dzia&#322;alno&#347;&#263;_gospodarcza"
  ]
  node [
    id 1223
    label "komercja"
  ]
  node [
    id 1224
    label "popyt"
  ]
  node [
    id 1225
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1226
    label "przepaska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 220
  ]
  edge [
    source 12
    target 221
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 223
  ]
  edge [
    source 12
    target 224
  ]
  edge [
    source 12
    target 225
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 12
    target 228
  ]
  edge [
    source 12
    target 229
  ]
  edge [
    source 12
    target 230
  ]
  edge [
    source 12
    target 231
  ]
  edge [
    source 12
    target 232
  ]
  edge [
    source 12
    target 233
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 235
  ]
  edge [
    source 12
    target 236
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 12
    target 238
  ]
  edge [
    source 12
    target 239
  ]
  edge [
    source 12
    target 240
  ]
  edge [
    source 12
    target 241
  ]
  edge [
    source 12
    target 242
  ]
  edge [
    source 12
    target 243
  ]
  edge [
    source 12
    target 244
  ]
  edge [
    source 12
    target 245
  ]
  edge [
    source 12
    target 246
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 248
  ]
  edge [
    source 12
    target 249
  ]
  edge [
    source 12
    target 250
  ]
  edge [
    source 12
    target 251
  ]
  edge [
    source 12
    target 252
  ]
  edge [
    source 12
    target 253
  ]
  edge [
    source 12
    target 254
  ]
  edge [
    source 12
    target 255
  ]
  edge [
    source 12
    target 256
  ]
  edge [
    source 12
    target 257
  ]
  edge [
    source 12
    target 258
  ]
  edge [
    source 12
    target 259
  ]
  edge [
    source 12
    target 260
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 262
  ]
  edge [
    source 12
    target 263
  ]
  edge [
    source 12
    target 264
  ]
  edge [
    source 12
    target 265
  ]
  edge [
    source 12
    target 266
  ]
  edge [
    source 12
    target 267
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 270
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 274
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 276
  ]
  edge [
    source 13
    target 277
  ]
  edge [
    source 13
    target 278
  ]
  edge [
    source 13
    target 279
  ]
  edge [
    source 13
    target 280
  ]
  edge [
    source 13
    target 281
  ]
  edge [
    source 13
    target 282
  ]
  edge [
    source 13
    target 283
  ]
  edge [
    source 13
    target 284
  ]
  edge [
    source 13
    target 285
  ]
  edge [
    source 13
    target 286
  ]
  edge [
    source 13
    target 287
  ]
  edge [
    source 13
    target 232
  ]
  edge [
    source 13
    target 288
  ]
  edge [
    source 13
    target 289
  ]
  edge [
    source 13
    target 290
  ]
  edge [
    source 13
    target 291
  ]
  edge [
    source 13
    target 292
  ]
  edge [
    source 13
    target 235
  ]
  edge [
    source 13
    target 293
  ]
  edge [
    source 13
    target 294
  ]
  edge [
    source 13
    target 295
  ]
  edge [
    source 13
    target 296
  ]
  edge [
    source 13
    target 297
  ]
  edge [
    source 13
    target 298
  ]
  edge [
    source 13
    target 299
  ]
  edge [
    source 13
    target 300
  ]
  edge [
    source 13
    target 301
  ]
  edge [
    source 13
    target 226
  ]
  edge [
    source 13
    target 302
  ]
  edge [
    source 13
    target 303
  ]
  edge [
    source 13
    target 304
  ]
  edge [
    source 13
    target 305
  ]
  edge [
    source 13
    target 233
  ]
  edge [
    source 13
    target 306
  ]
  edge [
    source 13
    target 307
  ]
  edge [
    source 13
    target 308
  ]
  edge [
    source 13
    target 234
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 310
  ]
  edge [
    source 13
    target 311
  ]
  edge [
    source 13
    target 312
  ]
  edge [
    source 13
    target 313
  ]
  edge [
    source 13
    target 314
  ]
  edge [
    source 13
    target 315
  ]
  edge [
    source 13
    target 316
  ]
  edge [
    source 13
    target 317
  ]
  edge [
    source 13
    target 318
  ]
  edge [
    source 13
    target 319
  ]
  edge [
    source 13
    target 320
  ]
  edge [
    source 13
    target 321
  ]
  edge [
    source 13
    target 322
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 325
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 327
  ]
  edge [
    source 13
    target 328
  ]
  edge [
    source 13
    target 329
  ]
  edge [
    source 13
    target 330
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 334
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 336
  ]
  edge [
    source 13
    target 337
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 339
  ]
  edge [
    source 13
    target 340
  ]
  edge [
    source 13
    target 341
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 13
    target 343
  ]
  edge [
    source 13
    target 344
  ]
  edge [
    source 13
    target 345
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 352
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 355
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 403
  ]
  edge [
    source 14
    target 404
  ]
  edge [
    source 14
    target 405
  ]
  edge [
    source 14
    target 406
  ]
  edge [
    source 14
    target 407
  ]
  edge [
    source 14
    target 408
  ]
  edge [
    source 14
    target 409
  ]
  edge [
    source 14
    target 410
  ]
  edge [
    source 14
    target 272
  ]
  edge [
    source 14
    target 411
  ]
  edge [
    source 14
    target 412
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 413
  ]
  edge [
    source 14
    target 414
  ]
  edge [
    source 14
    target 415
  ]
  edge [
    source 14
    target 416
  ]
  edge [
    source 14
    target 417
  ]
  edge [
    source 14
    target 418
  ]
  edge [
    source 14
    target 419
  ]
  edge [
    source 14
    target 420
  ]
  edge [
    source 14
    target 421
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 422
  ]
  edge [
    source 14
    target 423
  ]
  edge [
    source 14
    target 424
  ]
  edge [
    source 14
    target 425
  ]
  edge [
    source 14
    target 426
  ]
  edge [
    source 14
    target 427
  ]
  edge [
    source 14
    target 428
  ]
  edge [
    source 14
    target 429
  ]
  edge [
    source 14
    target 430
  ]
  edge [
    source 14
    target 431
  ]
  edge [
    source 14
    target 432
  ]
  edge [
    source 14
    target 433
  ]
  edge [
    source 14
    target 434
  ]
  edge [
    source 14
    target 435
  ]
  edge [
    source 14
    target 436
  ]
  edge [
    source 14
    target 437
  ]
  edge [
    source 14
    target 438
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 392
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 456
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 458
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 467
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 469
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 471
  ]
  edge [
    source 14
    target 472
  ]
  edge [
    source 14
    target 473
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 221
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 502
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 505
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 14
    target 507
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 509
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 511
  ]
  edge [
    source 14
    target 512
  ]
  edge [
    source 14
    target 513
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 522
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 14
    target 529
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 531
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 561
  ]
  edge [
    source 15
    target 562
  ]
  edge [
    source 15
    target 563
  ]
  edge [
    source 15
    target 564
  ]
  edge [
    source 15
    target 565
  ]
  edge [
    source 15
    target 566
  ]
  edge [
    source 15
    target 567
  ]
  edge [
    source 15
    target 568
  ]
  edge [
    source 15
    target 569
  ]
  edge [
    source 15
    target 570
  ]
  edge [
    source 15
    target 571
  ]
  edge [
    source 15
    target 572
  ]
  edge [
    source 15
    target 573
  ]
  edge [
    source 15
    target 574
  ]
  edge [
    source 15
    target 575
  ]
  edge [
    source 15
    target 576
  ]
  edge [
    source 15
    target 312
  ]
  edge [
    source 15
    target 577
  ]
  edge [
    source 15
    target 578
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 579
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 586
  ]
  edge [
    source 16
    target 587
  ]
  edge [
    source 16
    target 588
  ]
  edge [
    source 16
    target 589
  ]
  edge [
    source 16
    target 590
  ]
  edge [
    source 16
    target 591
  ]
  edge [
    source 16
    target 592
  ]
  edge [
    source 16
    target 593
  ]
  edge [
    source 16
    target 594
  ]
  edge [
    source 16
    target 595
  ]
  edge [
    source 16
    target 596
  ]
  edge [
    source 16
    target 597
  ]
  edge [
    source 16
    target 598
  ]
  edge [
    source 16
    target 599
  ]
  edge [
    source 16
    target 600
  ]
  edge [
    source 16
    target 601
  ]
  edge [
    source 16
    target 602
  ]
  edge [
    source 16
    target 603
  ]
  edge [
    source 16
    target 604
  ]
  edge [
    source 16
    target 605
  ]
  edge [
    source 16
    target 606
  ]
  edge [
    source 16
    target 607
  ]
  edge [
    source 16
    target 608
  ]
  edge [
    source 16
    target 609
  ]
  edge [
    source 16
    target 610
  ]
  edge [
    source 16
    target 611
  ]
  edge [
    source 16
    target 612
  ]
  edge [
    source 16
    target 613
  ]
  edge [
    source 16
    target 30
  ]
  edge [
    source 16
    target 614
  ]
  edge [
    source 16
    target 56
  ]
  edge [
    source 16
    target 615
  ]
  edge [
    source 16
    target 616
  ]
  edge [
    source 16
    target 617
  ]
  edge [
    source 16
    target 618
  ]
  edge [
    source 16
    target 619
  ]
  edge [
    source 16
    target 620
  ]
  edge [
    source 16
    target 621
  ]
  edge [
    source 16
    target 622
  ]
  edge [
    source 16
    target 43
  ]
  edge [
    source 16
    target 623
  ]
  edge [
    source 16
    target 624
  ]
  edge [
    source 16
    target 625
  ]
  edge [
    source 16
    target 626
  ]
  edge [
    source 16
    target 627
  ]
  edge [
    source 16
    target 628
  ]
  edge [
    source 16
    target 629
  ]
  edge [
    source 16
    target 630
  ]
  edge [
    source 16
    target 631
  ]
  edge [
    source 16
    target 632
  ]
  edge [
    source 16
    target 633
  ]
  edge [
    source 16
    target 634
  ]
  edge [
    source 16
    target 635
  ]
  edge [
    source 16
    target 636
  ]
  edge [
    source 16
    target 637
  ]
  edge [
    source 16
    target 638
  ]
  edge [
    source 16
    target 639
  ]
  edge [
    source 16
    target 529
  ]
  edge [
    source 16
    target 34
  ]
  edge [
    source 16
    target 640
  ]
  edge [
    source 16
    target 641
  ]
  edge [
    source 16
    target 642
  ]
  edge [
    source 16
    target 643
  ]
  edge [
    source 16
    target 644
  ]
  edge [
    source 16
    target 645
  ]
  edge [
    source 16
    target 646
  ]
  edge [
    source 16
    target 647
  ]
  edge [
    source 16
    target 648
  ]
  edge [
    source 16
    target 649
  ]
  edge [
    source 16
    target 650
  ]
  edge [
    source 16
    target 651
  ]
  edge [
    source 16
    target 652
  ]
  edge [
    source 16
    target 653
  ]
  edge [
    source 16
    target 654
  ]
  edge [
    source 16
    target 655
  ]
  edge [
    source 16
    target 656
  ]
  edge [
    source 16
    target 657
  ]
  edge [
    source 16
    target 658
  ]
  edge [
    source 16
    target 659
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 661
  ]
  edge [
    source 16
    target 662
  ]
  edge [
    source 16
    target 663
  ]
  edge [
    source 16
    target 664
  ]
  edge [
    source 16
    target 665
  ]
  edge [
    source 16
    target 666
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 668
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 670
  ]
  edge [
    source 16
    target 671
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 673
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 677
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 679
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 58
  ]
  edge [
    source 16
    target 682
  ]
  edge [
    source 16
    target 63
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 684
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 694
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 16
    target 252
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 698
  ]
  edge [
    source 16
    target 699
  ]
  edge [
    source 16
    target 700
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 702
  ]
  edge [
    source 16
    target 703
  ]
  edge [
    source 16
    target 704
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 771
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 69
  ]
  edge [
    source 16
    target 44
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 16
    target 777
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 30
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 57
  ]
  edge [
    source 17
    target 58
  ]
  edge [
    source 17
    target 59
  ]
  edge [
    source 17
    target 60
  ]
  edge [
    source 17
    target 61
  ]
  edge [
    source 17
    target 62
  ]
  edge [
    source 17
    target 63
  ]
  edge [
    source 17
    target 64
  ]
  edge [
    source 17
    target 65
  ]
  edge [
    source 17
    target 66
  ]
  edge [
    source 17
    target 67
  ]
  edge [
    source 17
    target 56
  ]
  edge [
    source 17
    target 68
  ]
  edge [
    source 17
    target 31
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 34
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 44
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 229
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 118
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 364
  ]
  edge [
    source 18
    target 85
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 100
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 901
  ]
  edge [
    source 19
    target 902
  ]
  edge [
    source 19
    target 903
  ]
  edge [
    source 19
    target 904
  ]
  edge [
    source 19
    target 905
  ]
  edge [
    source 19
    target 411
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 906
  ]
  edge [
    source 19
    target 907
  ]
  edge [
    source 19
    target 908
  ]
  edge [
    source 19
    target 909
  ]
  edge [
    source 19
    target 910
  ]
  edge [
    source 19
    target 911
  ]
  edge [
    source 19
    target 478
  ]
  edge [
    source 19
    target 912
  ]
  edge [
    source 19
    target 913
  ]
  edge [
    source 19
    target 914
  ]
  edge [
    source 19
    target 915
  ]
  edge [
    source 19
    target 916
  ]
  edge [
    source 19
    target 470
  ]
  edge [
    source 19
    target 917
  ]
  edge [
    source 19
    target 918
  ]
  edge [
    source 19
    target 919
  ]
  edge [
    source 19
    target 920
  ]
  edge [
    source 19
    target 921
  ]
  edge [
    source 19
    target 407
  ]
  edge [
    source 19
    target 536
  ]
  edge [
    source 19
    target 922
  ]
  edge [
    source 19
    target 923
  ]
  edge [
    source 19
    target 924
  ]
  edge [
    source 19
    target 925
  ]
  edge [
    source 19
    target 926
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 486
  ]
  edge [
    source 21
    target 927
  ]
  edge [
    source 21
    target 928
  ]
  edge [
    source 21
    target 929
  ]
  edge [
    source 21
    target 930
  ]
  edge [
    source 21
    target 931
  ]
  edge [
    source 21
    target 932
  ]
  edge [
    source 21
    target 933
  ]
  edge [
    source 21
    target 934
  ]
  edge [
    source 21
    target 935
  ]
  edge [
    source 21
    target 936
  ]
  edge [
    source 21
    target 937
  ]
  edge [
    source 21
    target 938
  ]
  edge [
    source 21
    target 939
  ]
  edge [
    source 21
    target 940
  ]
  edge [
    source 21
    target 941
  ]
  edge [
    source 21
    target 729
  ]
  edge [
    source 21
    target 46
  ]
  edge [
    source 21
    target 863
  ]
  edge [
    source 21
    target 942
  ]
  edge [
    source 21
    target 458
  ]
  edge [
    source 21
    target 943
  ]
  edge [
    source 21
    target 944
  ]
  edge [
    source 21
    target 407
  ]
  edge [
    source 21
    target 392
  ]
  edge [
    source 21
    target 945
  ]
  edge [
    source 21
    target 946
  ]
  edge [
    source 21
    target 947
  ]
  edge [
    source 21
    target 948
  ]
  edge [
    source 21
    target 949
  ]
  edge [
    source 21
    target 950
  ]
  edge [
    source 21
    target 408
  ]
  edge [
    source 21
    target 475
  ]
  edge [
    source 21
    target 951
  ]
  edge [
    source 21
    target 952
  ]
  edge [
    source 21
    target 953
  ]
  edge [
    source 21
    target 954
  ]
  edge [
    source 21
    target 955
  ]
  edge [
    source 21
    target 956
  ]
  edge [
    source 21
    target 957
  ]
  edge [
    source 21
    target 923
  ]
  edge [
    source 21
    target 958
  ]
  edge [
    source 21
    target 272
  ]
  edge [
    source 21
    target 959
  ]
  edge [
    source 21
    target 960
  ]
  edge [
    source 21
    target 961
  ]
  edge [
    source 21
    target 962
  ]
  edge [
    source 21
    target 963
  ]
  edge [
    source 21
    target 964
  ]
  edge [
    source 21
    target 965
  ]
  edge [
    source 21
    target 411
  ]
  edge [
    source 21
    target 966
  ]
  edge [
    source 21
    target 967
  ]
  edge [
    source 21
    target 968
  ]
  edge [
    source 21
    target 969
  ]
  edge [
    source 21
    target 970
  ]
  edge [
    source 21
    target 971
  ]
  edge [
    source 21
    target 972
  ]
  edge [
    source 21
    target 973
  ]
  edge [
    source 21
    target 974
  ]
  edge [
    source 21
    target 975
  ]
  edge [
    source 21
    target 976
  ]
  edge [
    source 21
    target 977
  ]
  edge [
    source 21
    target 978
  ]
  edge [
    source 21
    target 979
  ]
  edge [
    source 21
    target 980
  ]
  edge [
    source 21
    target 981
  ]
  edge [
    source 21
    target 982
  ]
  edge [
    source 21
    target 983
  ]
  edge [
    source 21
    target 984
  ]
  edge [
    source 21
    target 985
  ]
  edge [
    source 21
    target 986
  ]
  edge [
    source 21
    target 987
  ]
  edge [
    source 21
    target 988
  ]
  edge [
    source 21
    target 989
  ]
  edge [
    source 21
    target 990
  ]
  edge [
    source 21
    target 991
  ]
  edge [
    source 21
    target 992
  ]
  edge [
    source 21
    target 993
  ]
  edge [
    source 21
    target 397
  ]
  edge [
    source 21
    target 994
  ]
  edge [
    source 21
    target 995
  ]
  edge [
    source 21
    target 483
  ]
  edge [
    source 21
    target 996
  ]
  edge [
    source 21
    target 997
  ]
  edge [
    source 21
    target 998
  ]
  edge [
    source 21
    target 999
  ]
  edge [
    source 21
    target 1000
  ]
  edge [
    source 21
    target 1001
  ]
  edge [
    source 21
    target 1002
  ]
  edge [
    source 21
    target 1003
  ]
  edge [
    source 21
    target 1004
  ]
  edge [
    source 21
    target 1005
  ]
  edge [
    source 21
    target 1006
  ]
  edge [
    source 21
    target 1007
  ]
  edge [
    source 21
    target 131
  ]
  edge [
    source 21
    target 1008
  ]
  edge [
    source 21
    target 1009
  ]
  edge [
    source 21
    target 1010
  ]
  edge [
    source 21
    target 1011
  ]
  edge [
    source 21
    target 1012
  ]
  edge [
    source 21
    target 1013
  ]
  edge [
    source 21
    target 1014
  ]
  edge [
    source 21
    target 1015
  ]
  edge [
    source 21
    target 1016
  ]
  edge [
    source 21
    target 1017
  ]
  edge [
    source 21
    target 1018
  ]
  edge [
    source 21
    target 1019
  ]
  edge [
    source 21
    target 1020
  ]
  edge [
    source 21
    target 1021
  ]
  edge [
    source 21
    target 1022
  ]
  edge [
    source 21
    target 1023
  ]
  edge [
    source 21
    target 728
  ]
  edge [
    source 21
    target 1024
  ]
  edge [
    source 21
    target 1025
  ]
  edge [
    source 21
    target 1026
  ]
  edge [
    source 21
    target 1027
  ]
  edge [
    source 21
    target 1028
  ]
  edge [
    source 21
    target 1029
  ]
  edge [
    source 21
    target 1030
  ]
  edge [
    source 21
    target 1031
  ]
  edge [
    source 21
    target 1032
  ]
  edge [
    source 21
    target 1033
  ]
  edge [
    source 21
    target 1034
  ]
  edge [
    source 21
    target 1035
  ]
  edge [
    source 21
    target 1036
  ]
  edge [
    source 21
    target 1037
  ]
  edge [
    source 21
    target 1038
  ]
  edge [
    source 21
    target 551
  ]
  edge [
    source 21
    target 1039
  ]
  edge [
    source 21
    target 1040
  ]
  edge [
    source 21
    target 1041
  ]
  edge [
    source 21
    target 1042
  ]
  edge [
    source 21
    target 412
  ]
  edge [
    source 21
    target 1043
  ]
  edge [
    source 21
    target 1044
  ]
  edge [
    source 21
    target 1045
  ]
  edge [
    source 21
    target 489
  ]
  edge [
    source 21
    target 1046
  ]
  edge [
    source 21
    target 1047
  ]
  edge [
    source 21
    target 1048
  ]
  edge [
    source 21
    target 1049
  ]
  edge [
    source 21
    target 1050
  ]
  edge [
    source 21
    target 1051
  ]
  edge [
    source 21
    target 1052
  ]
  edge [
    source 21
    target 1053
  ]
  edge [
    source 21
    target 1054
  ]
  edge [
    source 21
    target 1055
  ]
  edge [
    source 21
    target 1056
  ]
  edge [
    source 21
    target 1057
  ]
  edge [
    source 21
    target 1058
  ]
  edge [
    source 21
    target 1059
  ]
  edge [
    source 21
    target 1060
  ]
  edge [
    source 21
    target 1061
  ]
  edge [
    source 21
    target 1062
  ]
  edge [
    source 21
    target 1063
  ]
  edge [
    source 21
    target 1064
  ]
  edge [
    source 21
    target 1065
  ]
  edge [
    source 21
    target 1066
  ]
  edge [
    source 21
    target 1067
  ]
  edge [
    source 21
    target 1068
  ]
  edge [
    source 21
    target 1069
  ]
  edge [
    source 21
    target 1070
  ]
  edge [
    source 21
    target 1071
  ]
  edge [
    source 21
    target 1072
  ]
  edge [
    source 21
    target 1073
  ]
  edge [
    source 21
    target 1074
  ]
  edge [
    source 21
    target 1075
  ]
  edge [
    source 21
    target 460
  ]
  edge [
    source 21
    target 1076
  ]
  edge [
    source 21
    target 1077
  ]
  edge [
    source 21
    target 1078
  ]
  edge [
    source 21
    target 1079
  ]
  edge [
    source 21
    target 1080
  ]
  edge [
    source 21
    target 1081
  ]
  edge [
    source 21
    target 1082
  ]
  edge [
    source 21
    target 1083
  ]
  edge [
    source 21
    target 1084
  ]
  edge [
    source 21
    target 1085
  ]
  edge [
    source 21
    target 1086
  ]
  edge [
    source 21
    target 1087
  ]
  edge [
    source 21
    target 348
  ]
  edge [
    source 21
    target 1088
  ]
  edge [
    source 21
    target 1089
  ]
  edge [
    source 21
    target 1090
  ]
  edge [
    source 21
    target 1091
  ]
  edge [
    source 21
    target 1092
  ]
  edge [
    source 21
    target 1093
  ]
  edge [
    source 21
    target 1094
  ]
  edge [
    source 21
    target 1095
  ]
  edge [
    source 21
    target 1096
  ]
  edge [
    source 21
    target 1097
  ]
  edge [
    source 21
    target 1098
  ]
  edge [
    source 21
    target 1099
  ]
  edge [
    source 21
    target 1100
  ]
  edge [
    source 21
    target 1101
  ]
  edge [
    source 21
    target 1102
  ]
  edge [
    source 21
    target 826
  ]
  edge [
    source 21
    target 1103
  ]
  edge [
    source 21
    target 1104
  ]
  edge [
    source 21
    target 1105
  ]
  edge [
    source 21
    target 1106
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1107
  ]
  edge [
    source 23
    target 1108
  ]
  edge [
    source 23
    target 708
  ]
  edge [
    source 23
    target 1109
  ]
  edge [
    source 23
    target 632
  ]
  edge [
    source 23
    target 714
  ]
  edge [
    source 23
    target 607
  ]
  edge [
    source 23
    target 684
  ]
  edge [
    source 23
    target 1110
  ]
  edge [
    source 23
    target 587
  ]
  edge [
    source 23
    target 712
  ]
  edge [
    source 23
    target 713
  ]
  edge [
    source 23
    target 1111
  ]
  edge [
    source 23
    target 610
  ]
  edge [
    source 23
    target 1112
  ]
  edge [
    source 23
    target 1113
  ]
  edge [
    source 23
    target 715
  ]
  edge [
    source 23
    target 716
  ]
  edge [
    source 23
    target 1114
  ]
  edge [
    source 23
    target 1115
  ]
  edge [
    source 23
    target 1116
  ]
  edge [
    source 23
    target 1117
  ]
  edge [
    source 23
    target 1118
  ]
  edge [
    source 23
    target 1119
  ]
  edge [
    source 23
    target 1120
  ]
  edge [
    source 23
    target 1121
  ]
  edge [
    source 23
    target 1122
  ]
  edge [
    source 23
    target 1123
  ]
  edge [
    source 23
    target 1124
  ]
  edge [
    source 23
    target 1125
  ]
  edge [
    source 23
    target 1126
  ]
  edge [
    source 23
    target 1127
  ]
  edge [
    source 23
    target 705
  ]
  edge [
    source 23
    target 706
  ]
  edge [
    source 23
    target 707
  ]
  edge [
    source 23
    target 709
  ]
  edge [
    source 23
    target 710
  ]
  edge [
    source 23
    target 711
  ]
  edge [
    source 23
    target 30
  ]
  edge [
    source 23
    target 717
  ]
  edge [
    source 23
    target 718
  ]
  edge [
    source 23
    target 719
  ]
  edge [
    source 23
    target 720
  ]
  edge [
    source 23
    target 721
  ]
  edge [
    source 23
    target 722
  ]
  edge [
    source 23
    target 723
  ]
  edge [
    source 23
    target 724
  ]
  edge [
    source 23
    target 725
  ]
  edge [
    source 23
    target 1128
  ]
  edge [
    source 23
    target 1129
  ]
  edge [
    source 23
    target 1130
  ]
  edge [
    source 23
    target 799
  ]
  edge [
    source 23
    target 1131
  ]
  edge [
    source 23
    target 1132
  ]
  edge [
    source 23
    target 1133
  ]
  edge [
    source 23
    target 1134
  ]
  edge [
    source 23
    target 1135
  ]
  edge [
    source 23
    target 1136
  ]
  edge [
    source 23
    target 1137
  ]
  edge [
    source 23
    target 1138
  ]
  edge [
    source 23
    target 1139
  ]
  edge [
    source 23
    target 688
  ]
  edge [
    source 23
    target 1140
  ]
  edge [
    source 23
    target 1141
  ]
  edge [
    source 23
    target 1142
  ]
  edge [
    source 23
    target 1143
  ]
  edge [
    source 23
    target 694
  ]
  edge [
    source 23
    target 614
  ]
  edge [
    source 23
    target 1144
  ]
  edge [
    source 23
    target 1145
  ]
  edge [
    source 23
    target 1146
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 1147
  ]
  edge [
    source 23
    target 1148
  ]
  edge [
    source 23
    target 847
  ]
  edge [
    source 23
    target 1149
  ]
  edge [
    source 23
    target 69
  ]
  edge [
    source 23
    target 1150
  ]
  edge [
    source 23
    target 44
  ]
  edge [
    source 23
    target 862
  ]
  edge [
    source 23
    target 1151
  ]
  edge [
    source 23
    target 1152
  ]
  edge [
    source 23
    target 1153
  ]
  edge [
    source 23
    target 1154
  ]
  edge [
    source 23
    target 1155
  ]
  edge [
    source 23
    target 1156
  ]
  edge [
    source 23
    target 1157
  ]
  edge [
    source 23
    target 59
  ]
  edge [
    source 23
    target 1158
  ]
  edge [
    source 23
    target 1159
  ]
  edge [
    source 23
    target 1160
  ]
  edge [
    source 23
    target 1161
  ]
  edge [
    source 23
    target 1162
  ]
  edge [
    source 24
    target 1163
  ]
  edge [
    source 24
    target 1164
  ]
  edge [
    source 24
    target 1165
  ]
  edge [
    source 24
    target 1166
  ]
  edge [
    source 24
    target 1167
  ]
  edge [
    source 24
    target 1168
  ]
  edge [
    source 24
    target 1169
  ]
  edge [
    source 24
    target 1170
  ]
  edge [
    source 24
    target 937
  ]
  edge [
    source 24
    target 1171
  ]
  edge [
    source 24
    target 1172
  ]
  edge [
    source 24
    target 1173
  ]
  edge [
    source 24
    target 991
  ]
  edge [
    source 24
    target 992
  ]
  edge [
    source 24
    target 993
  ]
  edge [
    source 24
    target 397
  ]
  edge [
    source 24
    target 994
  ]
  edge [
    source 24
    target 995
  ]
  edge [
    source 24
    target 483
  ]
  edge [
    source 24
    target 1174
  ]
  edge [
    source 24
    target 1175
  ]
  edge [
    source 24
    target 1176
  ]
  edge [
    source 24
    target 1177
  ]
  edge [
    source 24
    target 258
  ]
  edge [
    source 24
    target 1178
  ]
  edge [
    source 24
    target 244
  ]
  edge [
    source 24
    target 1179
  ]
  edge [
    source 24
    target 1180
  ]
  edge [
    source 24
    target 1181
  ]
  edge [
    source 24
    target 1182
  ]
  edge [
    source 24
    target 268
  ]
  edge [
    source 24
    target 247
  ]
  edge [
    source 24
    target 1183
  ]
  edge [
    source 24
    target 1184
  ]
  edge [
    source 24
    target 1185
  ]
  edge [
    source 24
    target 1186
  ]
  edge [
    source 24
    target 432
  ]
  edge [
    source 24
    target 1187
  ]
  edge [
    source 24
    target 981
  ]
  edge [
    source 24
    target 1188
  ]
  edge [
    source 24
    target 990
  ]
  edge [
    source 24
    target 1189
  ]
  edge [
    source 24
    target 1190
  ]
  edge [
    source 24
    target 1191
  ]
  edge [
    source 24
    target 1192
  ]
  edge [
    source 24
    target 1193
  ]
  edge [
    source 24
    target 487
  ]
  edge [
    source 24
    target 1194
  ]
  edge [
    source 24
    target 491
  ]
  edge [
    source 24
    target 1195
  ]
  edge [
    source 24
    target 1196
  ]
  edge [
    source 24
    target 1197
  ]
  edge [
    source 24
    target 1198
  ]
  edge [
    source 24
    target 728
  ]
  edge [
    source 24
    target 1199
  ]
  edge [
    source 24
    target 1200
  ]
  edge [
    source 24
    target 1201
  ]
  edge [
    source 24
    target 1202
  ]
  edge [
    source 24
    target 1203
  ]
  edge [
    source 24
    target 514
  ]
  edge [
    source 24
    target 516
  ]
  edge [
    source 24
    target 1204
  ]
  edge [
    source 24
    target 1205
  ]
  edge [
    source 24
    target 1206
  ]
  edge [
    source 24
    target 1207
  ]
  edge [
    source 24
    target 1208
  ]
  edge [
    source 24
    target 1209
  ]
  edge [
    source 24
    target 1210
  ]
  edge [
    source 24
    target 1211
  ]
  edge [
    source 24
    target 1212
  ]
  edge [
    source 24
    target 143
  ]
  edge [
    source 24
    target 1213
  ]
  edge [
    source 24
    target 1214
  ]
  edge [
    source 24
    target 1215
  ]
  edge [
    source 24
    target 1216
  ]
  edge [
    source 24
    target 1217
  ]
  edge [
    source 24
    target 1218
  ]
  edge [
    source 24
    target 1219
  ]
  edge [
    source 24
    target 1220
  ]
  edge [
    source 24
    target 1221
  ]
  edge [
    source 24
    target 1222
  ]
  edge [
    source 24
    target 1223
  ]
  edge [
    source 24
    target 1224
  ]
  edge [
    source 24
    target 1225
  ]
  edge [
    source 24
    target 1226
  ]
]
