graph [
  node [
    id 0
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 1
    label "tylko"
    origin "text"
  ]
  node [
    id 2
    label "z&#322;uda"
    origin "text"
  ]
  node [
    id 3
    label "z&#322;udzenie"
  ]
  node [
    id 4
    label "doznanie"
  ]
  node [
    id 5
    label "marzenie"
  ]
  node [
    id 6
    label "wydawa&#263;_si&#281;"
  ]
  node [
    id 7
    label "nabranie"
  ]
  node [
    id 8
    label "omamienie_si&#281;"
  ]
  node [
    id 9
    label "delusion"
  ]
  node [
    id 10
    label "zba&#322;amucenie"
  ]
  node [
    id 11
    label "wyda&#263;_si&#281;"
  ]
  node [
    id 12
    label "fondness"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
]
