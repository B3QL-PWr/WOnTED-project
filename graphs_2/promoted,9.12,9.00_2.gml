graph [
  node [
    id 0
    label "protest"
    origin "text"
  ]
  node [
    id 1
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 2
    label "kamizelka"
    origin "text"
  ]
  node [
    id 3
    label "czerwona_kartka"
  ]
  node [
    id 4
    label "protestacja"
  ]
  node [
    id 5
    label "reakcja"
  ]
  node [
    id 6
    label "react"
  ]
  node [
    id 7
    label "zachowanie"
  ]
  node [
    id 8
    label "reaction"
  ]
  node [
    id 9
    label "organizm"
  ]
  node [
    id 10
    label "rozmowa"
  ]
  node [
    id 11
    label "response"
  ]
  node [
    id 12
    label "rezultat"
  ]
  node [
    id 13
    label "respondent"
  ]
  node [
    id 14
    label "sprzeciw"
  ]
  node [
    id 15
    label "typ_mongoloidalny"
  ]
  node [
    id 16
    label "kolorowy"
  ]
  node [
    id 17
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 18
    label "ciep&#322;y"
  ]
  node [
    id 19
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 20
    label "jasny"
  ]
  node [
    id 21
    label "zabarwienie_si&#281;"
  ]
  node [
    id 22
    label "ciekawy"
  ]
  node [
    id 23
    label "r&#243;&#380;nofarbny"
  ]
  node [
    id 24
    label "cz&#322;owiek"
  ]
  node [
    id 25
    label "r&#243;&#380;nokolorowo"
  ]
  node [
    id 26
    label "weso&#322;y"
  ]
  node [
    id 27
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 28
    label "barwienie"
  ]
  node [
    id 29
    label "kolorowo"
  ]
  node [
    id 30
    label "barwnie"
  ]
  node [
    id 31
    label "kolorowanie"
  ]
  node [
    id 32
    label "barwisty"
  ]
  node [
    id 33
    label "przyjemny"
  ]
  node [
    id 34
    label "barwienie_si&#281;"
  ]
  node [
    id 35
    label "pi&#281;kny"
  ]
  node [
    id 36
    label "ubarwienie"
  ]
  node [
    id 37
    label "mi&#322;y"
  ]
  node [
    id 38
    label "ocieplanie_si&#281;"
  ]
  node [
    id 39
    label "ocieplanie"
  ]
  node [
    id 40
    label "grzanie"
  ]
  node [
    id 41
    label "ocieplenie_si&#281;"
  ]
  node [
    id 42
    label "zagrzanie"
  ]
  node [
    id 43
    label "ocieplenie"
  ]
  node [
    id 44
    label "korzystny"
  ]
  node [
    id 45
    label "ciep&#322;o"
  ]
  node [
    id 46
    label "dobry"
  ]
  node [
    id 47
    label "o&#347;wietlenie"
  ]
  node [
    id 48
    label "szczery"
  ]
  node [
    id 49
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 50
    label "jasno"
  ]
  node [
    id 51
    label "o&#347;wietlanie"
  ]
  node [
    id 52
    label "przytomny"
  ]
  node [
    id 53
    label "zrozumia&#322;y"
  ]
  node [
    id 54
    label "niezm&#261;cony"
  ]
  node [
    id 55
    label "bia&#322;y"
  ]
  node [
    id 56
    label "klarowny"
  ]
  node [
    id 57
    label "jednoznaczny"
  ]
  node [
    id 58
    label "pogodny"
  ]
  node [
    id 59
    label "odcinanie_si&#281;"
  ]
  node [
    id 60
    label "g&#243;ra"
  ]
  node [
    id 61
    label "westa"
  ]
  node [
    id 62
    label "przedmiot"
  ]
  node [
    id 63
    label "przelezienie"
  ]
  node [
    id 64
    label "&#347;piew"
  ]
  node [
    id 65
    label "Synaj"
  ]
  node [
    id 66
    label "Kreml"
  ]
  node [
    id 67
    label "d&#378;wi&#281;k"
  ]
  node [
    id 68
    label "kierunek"
  ]
  node [
    id 69
    label "wysoki"
  ]
  node [
    id 70
    label "element"
  ]
  node [
    id 71
    label "wzniesienie"
  ]
  node [
    id 72
    label "grupa"
  ]
  node [
    id 73
    label "pi&#281;tro"
  ]
  node [
    id 74
    label "Ropa"
  ]
  node [
    id 75
    label "kupa"
  ]
  node [
    id 76
    label "przele&#378;&#263;"
  ]
  node [
    id 77
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 78
    label "karczek"
  ]
  node [
    id 79
    label "rami&#261;czko"
  ]
  node [
    id 80
    label "Jaworze"
  ]
  node [
    id 81
    label "cz&#281;&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
]
