graph [
  node [
    id 0
    label "flaga"
    origin "text"
  ]
  node [
    id 1
    label "odessa"
    origin "text"
  ]
  node [
    id 2
    label "oznaka"
  ]
  node [
    id 3
    label "flag"
  ]
  node [
    id 4
    label "transparent"
  ]
  node [
    id 5
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 6
    label "implikowa&#263;"
  ]
  node [
    id 7
    label "signal"
  ]
  node [
    id 8
    label "fakt"
  ]
  node [
    id 9
    label "symbol"
  ]
  node [
    id 10
    label "przedmiot"
  ]
  node [
    id 11
    label "tablica"
  ]
  node [
    id 12
    label "rada"
  ]
  node [
    id 13
    label "miasto"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 12
    target 13
  ]
]
