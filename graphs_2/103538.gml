graph [
  node [
    id 0
    label "kuchnia"
    origin "text"
  ]
  node [
    id 1
    label "wraca&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ola"
    origin "text"
  ]
  node [
    id 3
    label "zlewozmywak"
  ]
  node [
    id 4
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 5
    label "zaplecze"
  ]
  node [
    id 6
    label "gotowa&#263;"
  ]
  node [
    id 7
    label "jedzenie"
  ]
  node [
    id 8
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 9
    label "tajniki"
  ]
  node [
    id 10
    label "kultura"
  ]
  node [
    id 11
    label "zaj&#281;cie"
  ]
  node [
    id 12
    label "instytucja"
  ]
  node [
    id 13
    label "pomieszczenie"
  ]
  node [
    id 14
    label "przejadanie"
  ]
  node [
    id 15
    label "jadanie"
  ]
  node [
    id 16
    label "podanie"
  ]
  node [
    id 17
    label "posilanie"
  ]
  node [
    id 18
    label "przejedzenie"
  ]
  node [
    id 19
    label "szama"
  ]
  node [
    id 20
    label "odpasienie_si&#281;"
  ]
  node [
    id 21
    label "papusianie"
  ]
  node [
    id 22
    label "ufetowanie_si&#281;"
  ]
  node [
    id 23
    label "wyjadanie"
  ]
  node [
    id 24
    label "wpieprzanie"
  ]
  node [
    id 25
    label "wmuszanie"
  ]
  node [
    id 26
    label "objadanie"
  ]
  node [
    id 27
    label "odpasanie_si&#281;"
  ]
  node [
    id 28
    label "mlaskanie"
  ]
  node [
    id 29
    label "czynno&#347;&#263;"
  ]
  node [
    id 30
    label "posilenie"
  ]
  node [
    id 31
    label "polowanie"
  ]
  node [
    id 32
    label "&#380;arcie"
  ]
  node [
    id 33
    label "przejadanie_si&#281;"
  ]
  node [
    id 34
    label "podawanie"
  ]
  node [
    id 35
    label "koryto"
  ]
  node [
    id 36
    label "podawa&#263;"
  ]
  node [
    id 37
    label "jad&#322;o"
  ]
  node [
    id 38
    label "przejedzenie_si&#281;"
  ]
  node [
    id 39
    label "eating"
  ]
  node [
    id 40
    label "wiwenda"
  ]
  node [
    id 41
    label "rzecz"
  ]
  node [
    id 42
    label "wyjedzenie"
  ]
  node [
    id 43
    label "poda&#263;"
  ]
  node [
    id 44
    label "robienie"
  ]
  node [
    id 45
    label "smakowanie"
  ]
  node [
    id 46
    label "zatruwanie_si&#281;"
  ]
  node [
    id 47
    label "zakamarek"
  ]
  node [
    id 48
    label "amfilada"
  ]
  node [
    id 49
    label "sklepienie"
  ]
  node [
    id 50
    label "apartment"
  ]
  node [
    id 51
    label "udost&#281;pnienie"
  ]
  node [
    id 52
    label "front"
  ]
  node [
    id 53
    label "umieszczenie"
  ]
  node [
    id 54
    label "miejsce"
  ]
  node [
    id 55
    label "sufit"
  ]
  node [
    id 56
    label "pod&#322;oga"
  ]
  node [
    id 57
    label "wyposa&#380;enie"
  ]
  node [
    id 58
    label "infrastruktura"
  ]
  node [
    id 59
    label "sklep"
  ]
  node [
    id 60
    label "wiedza"
  ]
  node [
    id 61
    label "spos&#243;b"
  ]
  node [
    id 62
    label "spowodowanie"
  ]
  node [
    id 63
    label "zmiana"
  ]
  node [
    id 64
    label "u&#380;ycie"
  ]
  node [
    id 65
    label "wzi&#281;cie"
  ]
  node [
    id 66
    label "obj&#281;cie"
  ]
  node [
    id 67
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 68
    label "zadanie"
  ]
  node [
    id 69
    label "pozajmowanie"
  ]
  node [
    id 70
    label "ulokowanie_si&#281;"
  ]
  node [
    id 71
    label "zapanowanie"
  ]
  node [
    id 72
    label "czynnik_produkcji"
  ]
  node [
    id 73
    label "career"
  ]
  node [
    id 74
    label "klasyfikacja"
  ]
  node [
    id 75
    label "zdarzenie_si&#281;"
  ]
  node [
    id 76
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 77
    label "care"
  ]
  node [
    id 78
    label "usytuowanie_si&#281;"
  ]
  node [
    id 79
    label "anektowanie"
  ]
  node [
    id 80
    label "zabranie"
  ]
  node [
    id 81
    label "tynkarski"
  ]
  node [
    id 82
    label "benedykty&#324;ski"
  ]
  node [
    id 83
    label "wzbudzenie"
  ]
  node [
    id 84
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 85
    label "activity"
  ]
  node [
    id 86
    label "zaj&#281;cie_si&#281;"
  ]
  node [
    id 87
    label "wype&#322;nienie"
  ]
  node [
    id 88
    label "dostarczenie"
  ]
  node [
    id 89
    label "poj&#281;cie"
  ]
  node [
    id 90
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 91
    label "afiliowa&#263;"
  ]
  node [
    id 92
    label "establishment"
  ]
  node [
    id 93
    label "zamyka&#263;"
  ]
  node [
    id 94
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 95
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 96
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 97
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 98
    label "standard"
  ]
  node [
    id 99
    label "Fundusze_Unijne"
  ]
  node [
    id 100
    label "biuro"
  ]
  node [
    id 101
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 102
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 103
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 104
    label "zamykanie"
  ]
  node [
    id 105
    label "organizacja"
  ]
  node [
    id 106
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 107
    label "osoba_prawna"
  ]
  node [
    id 108
    label "urz&#261;d"
  ]
  node [
    id 109
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 110
    label "czynnik"
  ]
  node [
    id 111
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 112
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 113
    label "&#347;wiadectwo"
  ]
  node [
    id 114
    label "przyczyna"
  ]
  node [
    id 115
    label "kamena"
  ]
  node [
    id 116
    label "subject"
  ]
  node [
    id 117
    label "pocz&#261;tek"
  ]
  node [
    id 118
    label "rezultat"
  ]
  node [
    id 119
    label "poci&#261;ganie"
  ]
  node [
    id 120
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 121
    label "bra&#263;_si&#281;"
  ]
  node [
    id 122
    label "ciek_wodny"
  ]
  node [
    id 123
    label "geneza"
  ]
  node [
    id 124
    label "matuszka"
  ]
  node [
    id 125
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 126
    label "Wsch&#243;d"
  ]
  node [
    id 127
    label "jako&#347;&#263;"
  ]
  node [
    id 128
    label "sztuka"
  ]
  node [
    id 129
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 130
    label "praca_rolnicza"
  ]
  node [
    id 131
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 132
    label "makrokosmos"
  ]
  node [
    id 133
    label "przej&#281;cie"
  ]
  node [
    id 134
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 135
    label "przej&#261;&#263;"
  ]
  node [
    id 136
    label "przedmiot"
  ]
  node [
    id 137
    label "populace"
  ]
  node [
    id 138
    label "przejmowa&#263;"
  ]
  node [
    id 139
    label "hodowla"
  ]
  node [
    id 140
    label "religia"
  ]
  node [
    id 141
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 142
    label "propriety"
  ]
  node [
    id 143
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 144
    label "zwyczaj"
  ]
  node [
    id 145
    label "cecha"
  ]
  node [
    id 146
    label "zjawisko"
  ]
  node [
    id 147
    label "brzoskwiniarnia"
  ]
  node [
    id 148
    label "asymilowanie_si&#281;"
  ]
  node [
    id 149
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 150
    label "konwencja"
  ]
  node [
    id 151
    label "przejmowanie"
  ]
  node [
    id 152
    label "tradycja"
  ]
  node [
    id 153
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 154
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 155
    label "element_wyposa&#380;enia"
  ]
  node [
    id 156
    label "robi&#263;"
  ]
  node [
    id 157
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 158
    label "kucharz"
  ]
  node [
    id 159
    label "potrawka"
  ]
  node [
    id 160
    label "sposobi&#263;"
  ]
  node [
    id 161
    label "jajko_w_koszulce"
  ]
  node [
    id 162
    label "jajko_na_twardo"
  ]
  node [
    id 163
    label "wrz&#261;tek"
  ]
  node [
    id 164
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 165
    label "przynosi&#263;"
  ]
  node [
    id 166
    label "sztukami&#281;s"
  ]
  node [
    id 167
    label "jajko_na_mi&#281;kko"
  ]
  node [
    id 168
    label "train"
  ]
  node [
    id 169
    label "zupa"
  ]
  node [
    id 170
    label "nawi&#261;zywa&#263;"
  ]
  node [
    id 171
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 172
    label "zaczyna&#263;"
  ]
  node [
    id 173
    label "tax_return"
  ]
  node [
    id 174
    label "zostawa&#263;"
  ]
  node [
    id 175
    label "powodowa&#263;"
  ]
  node [
    id 176
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 177
    label "przychodzi&#263;"
  ]
  node [
    id 178
    label "przybywa&#263;"
  ]
  node [
    id 179
    label "return"
  ]
  node [
    id 180
    label "godzi&#263;_si&#281;"
  ]
  node [
    id 181
    label "recur"
  ]
  node [
    id 182
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 183
    label "bankrupt"
  ]
  node [
    id 184
    label "open"
  ]
  node [
    id 185
    label "post&#281;powa&#263;"
  ]
  node [
    id 186
    label "odejmowa&#263;"
  ]
  node [
    id 187
    label "set_about"
  ]
  node [
    id 188
    label "begin"
  ]
  node [
    id 189
    label "mie&#263;_miejsce"
  ]
  node [
    id 190
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 191
    label "blend"
  ]
  node [
    id 192
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 193
    label "pozostawa&#263;"
  ]
  node [
    id 194
    label "przebywa&#263;"
  ]
  node [
    id 195
    label "change"
  ]
  node [
    id 196
    label "by&#263;"
  ]
  node [
    id 197
    label "stop"
  ]
  node [
    id 198
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 199
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 200
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 201
    label "dochodzi&#263;"
  ]
  node [
    id 202
    label "czerpa&#263;"
  ]
  node [
    id 203
    label "bind"
  ]
  node [
    id 204
    label "motywowa&#263;"
  ]
  node [
    id 205
    label "act"
  ]
  node [
    id 206
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 207
    label "dociera&#263;"
  ]
  node [
    id 208
    label "get"
  ]
  node [
    id 209
    label "zyskiwa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
]
