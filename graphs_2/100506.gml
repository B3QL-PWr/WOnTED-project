graph [
  node [
    id 0
    label "gramatyk"
    origin "text"
  ]
  node [
    id 1
    label "kombinatoryczny"
    origin "text"
  ]
  node [
    id 2
    label "j&#281;zykoznawca"
  ]
  node [
    id 3
    label "naukowiec"
  ]
  node [
    id 4
    label "filolog"
  ]
  node [
    id 5
    label "kombinatorycznie"
  ]
  node [
    id 6
    label "niejednakowy"
  ]
  node [
    id 7
    label "zmiennie"
  ]
  node [
    id 8
    label "niejednakowo"
  ]
  node [
    id 9
    label "niepodobny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
]
