graph [
  node [
    id 0
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 1
    label "wierzy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "b&#243;g"
    origin "text"
  ]
  node [
    id 3
    label "dzienia"
    origin "text"
  ]
  node [
    id 4
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 5
    label "mat"
    origin "text"
  ]
  node [
    id 6
    label "tema"
    origin "text"
  ]
  node [
    id 7
    label "dostateczny"
    origin "text"
  ]
  node [
    id 8
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 9
    label "pobo&#380;no&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "abraham"
    origin "text"
  ]
  node [
    id 11
    label "rodzina"
    origin "text"
  ]
  node [
    id 12
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 13
    label "bezbo&#380;ny"
    origin "text"
  ]
  node [
    id 14
    label "odmawia&#263;"
    origin "text"
  ]
  node [
    id 15
    label "wszechmocny"
    origin "text"
  ]
  node [
    id 16
    label "nale&#380;ny"
    origin "text"
  ]
  node [
    id 17
    label "ho&#322;d"
    origin "text"
  ]
  node [
    id 18
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 19
    label "obej&#347;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "si&#281;"
    origin "text"
  ]
  node [
    id 21
    label "bez"
    origin "text"
  ]
  node [
    id 22
    label "cze&#347;&#263;"
    origin "text"
  ]
  node [
    id 23
    label "by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "bogaty"
    origin "text"
  ]
  node [
    id 25
    label "otoczy&#263;"
    origin "text"
  ]
  node [
    id 26
    label "chwa&#322;a"
    origin "text"
  ]
  node [
    id 27
    label "wierna"
    origin "text"
  ]
  node [
    id 28
    label "wierza&#263;"
  ]
  node [
    id 29
    label "trust"
  ]
  node [
    id 30
    label "powierzy&#263;"
  ]
  node [
    id 31
    label "wyznawa&#263;"
  ]
  node [
    id 32
    label "czu&#263;"
  ]
  node [
    id 33
    label "faith"
  ]
  node [
    id 34
    label "nadzieja"
  ]
  node [
    id 35
    label "chowa&#263;"
  ]
  node [
    id 36
    label "powierza&#263;"
  ]
  node [
    id 37
    label "uznawa&#263;"
  ]
  node [
    id 38
    label "szansa"
  ]
  node [
    id 39
    label "spoczywa&#263;"
  ]
  node [
    id 40
    label "&#347;wiate&#322;ko_w_tunelu"
  ]
  node [
    id 41
    label "oczekiwanie"
  ]
  node [
    id 42
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 43
    label "confide"
  ]
  node [
    id 44
    label "charge"
  ]
  node [
    id 45
    label "ufa&#263;"
  ]
  node [
    id 46
    label "odda&#263;"
  ]
  node [
    id 47
    label "entrust"
  ]
  node [
    id 48
    label "wyzna&#263;"
  ]
  node [
    id 49
    label "zleci&#263;"
  ]
  node [
    id 50
    label "consign"
  ]
  node [
    id 51
    label "oddawa&#263;"
  ]
  node [
    id 52
    label "zleca&#263;"
  ]
  node [
    id 53
    label "command"
  ]
  node [
    id 54
    label "grant"
  ]
  node [
    id 55
    label "monopol"
  ]
  node [
    id 56
    label "os&#261;dza&#263;"
  ]
  node [
    id 57
    label "consider"
  ]
  node [
    id 58
    label "notice"
  ]
  node [
    id 59
    label "stwierdza&#263;"
  ]
  node [
    id 60
    label "przyznawa&#263;"
  ]
  node [
    id 61
    label "postrzega&#263;"
  ]
  node [
    id 62
    label "przewidywa&#263;"
  ]
  node [
    id 63
    label "smell"
  ]
  node [
    id 64
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 65
    label "uczuwa&#263;"
  ]
  node [
    id 66
    label "spirit"
  ]
  node [
    id 67
    label "doznawa&#263;"
  ]
  node [
    id 68
    label "anticipate"
  ]
  node [
    id 69
    label "report"
  ]
  node [
    id 70
    label "hide"
  ]
  node [
    id 71
    label "znosi&#263;"
  ]
  node [
    id 72
    label "train"
  ]
  node [
    id 73
    label "przetrzymywa&#263;"
  ]
  node [
    id 74
    label "hodowa&#263;"
  ]
  node [
    id 75
    label "meliniarz"
  ]
  node [
    id 76
    label "umieszcza&#263;"
  ]
  node [
    id 77
    label "ukrywa&#263;"
  ]
  node [
    id 78
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 79
    label "continue"
  ]
  node [
    id 80
    label "wk&#322;ada&#263;"
  ]
  node [
    id 81
    label "acknowledge"
  ]
  node [
    id 82
    label "wyra&#380;a&#263;"
  ]
  node [
    id 83
    label "demaskowa&#263;"
  ]
  node [
    id 84
    label "Ereb"
  ]
  node [
    id 85
    label "Dionizos"
  ]
  node [
    id 86
    label "s&#261;d_ostateczny"
  ]
  node [
    id 87
    label "Waruna"
  ]
  node [
    id 88
    label "ofiarowywanie"
  ]
  node [
    id 89
    label "ba&#322;wan"
  ]
  node [
    id 90
    label "Hesperos"
  ]
  node [
    id 91
    label "Posejdon"
  ]
  node [
    id 92
    label "Sylen"
  ]
  node [
    id 93
    label "Janus"
  ]
  node [
    id 94
    label "istota_nadprzyrodzona"
  ]
  node [
    id 95
    label "niebiosa"
  ]
  node [
    id 96
    label "Boreasz"
  ]
  node [
    id 97
    label "ofiarowywa&#263;"
  ]
  node [
    id 98
    label "uwielbienie"
  ]
  node [
    id 99
    label "Bachus"
  ]
  node [
    id 100
    label "ofiarowanie"
  ]
  node [
    id 101
    label "Neptun"
  ]
  node [
    id 102
    label "tr&#243;jca"
  ]
  node [
    id 103
    label "Kupidyn"
  ]
  node [
    id 104
    label "igrzyska_greckie"
  ]
  node [
    id 105
    label "politeizm"
  ]
  node [
    id 106
    label "ofiarowa&#263;"
  ]
  node [
    id 107
    label "gigant"
  ]
  node [
    id 108
    label "idol"
  ]
  node [
    id 109
    label "osoba"
  ]
  node [
    id 110
    label "kombinacja_alpejska"
  ]
  node [
    id 111
    label "przedmiot"
  ]
  node [
    id 112
    label "figura"
  ]
  node [
    id 113
    label "podpora"
  ]
  node [
    id 114
    label "firma"
  ]
  node [
    id 115
    label "slalom"
  ]
  node [
    id 116
    label "zwierz&#281;"
  ]
  node [
    id 117
    label "element"
  ]
  node [
    id 118
    label "ucieczka"
  ]
  node [
    id 119
    label "zdobienie"
  ]
  node [
    id 120
    label "bestia"
  ]
  node [
    id 121
    label "istota_fantastyczna"
  ]
  node [
    id 122
    label "wielki"
  ]
  node [
    id 123
    label "olbrzym"
  ]
  node [
    id 124
    label "za&#347;wiaty"
  ]
  node [
    id 125
    label "bezchmurno&#347;&#263;"
  ]
  node [
    id 126
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 127
    label "znak_zodiaku"
  ]
  node [
    id 128
    label "Tr&#243;jca_&#346;wi&#281;ta"
  ]
  node [
    id 129
    label "opaczno&#347;&#263;"
  ]
  node [
    id 130
    label "si&#322;a"
  ]
  node [
    id 131
    label "absolut"
  ]
  node [
    id 132
    label "zodiak"
  ]
  node [
    id 133
    label "Kr&#243;lestwo_Niebieskie"
  ]
  node [
    id 134
    label "przestrze&#324;"
  ]
  node [
    id 135
    label "czczenie"
  ]
  node [
    id 136
    label "oko_opatrzno&#347;ci"
  ]
  node [
    id 137
    label "Chocho&#322;"
  ]
  node [
    id 138
    label "Herkules_Poirot"
  ]
  node [
    id 139
    label "Edyp"
  ]
  node [
    id 140
    label "ludzko&#347;&#263;"
  ]
  node [
    id 141
    label "parali&#380;owa&#263;"
  ]
  node [
    id 142
    label "Harry_Potter"
  ]
  node [
    id 143
    label "Casanova"
  ]
  node [
    id 144
    label "Zgredek"
  ]
  node [
    id 145
    label "Gargantua"
  ]
  node [
    id 146
    label "Winnetou"
  ]
  node [
    id 147
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 148
    label "posta&#263;"
  ]
  node [
    id 149
    label "Dulcynea"
  ]
  node [
    id 150
    label "kategoria_gramatyczna"
  ]
  node [
    id 151
    label "g&#322;owa"
  ]
  node [
    id 152
    label "portrecista"
  ]
  node [
    id 153
    label "person"
  ]
  node [
    id 154
    label "Plastu&#347;"
  ]
  node [
    id 155
    label "Quasimodo"
  ]
  node [
    id 156
    label "Sherlock_Holmes"
  ]
  node [
    id 157
    label "Faust"
  ]
  node [
    id 158
    label "Wallenrod"
  ]
  node [
    id 159
    label "Dwukwiat"
  ]
  node [
    id 160
    label "Don_Juan"
  ]
  node [
    id 161
    label "profanum"
  ]
  node [
    id 162
    label "koniugacja"
  ]
  node [
    id 163
    label "Don_Kiszot"
  ]
  node [
    id 164
    label "mikrokosmos"
  ]
  node [
    id 165
    label "duch"
  ]
  node [
    id 166
    label "antropochoria"
  ]
  node [
    id 167
    label "oddzia&#322;ywanie"
  ]
  node [
    id 168
    label "Hamlet"
  ]
  node [
    id 169
    label "Werter"
  ]
  node [
    id 170
    label "istota"
  ]
  node [
    id 171
    label "Szwejk"
  ]
  node [
    id 172
    label "homo_sapiens"
  ]
  node [
    id 173
    label "w&#281;gielek"
  ]
  node [
    id 174
    label "kula_&#347;niegowa"
  ]
  node [
    id 175
    label "fala_morska"
  ]
  node [
    id 176
    label "snowman"
  ]
  node [
    id 177
    label "wave"
  ]
  node [
    id 178
    label "marchewka"
  ]
  node [
    id 179
    label "g&#322;upek"
  ]
  node [
    id 180
    label "patyk"
  ]
  node [
    id 181
    label "Eastwood"
  ]
  node [
    id 182
    label "wz&#243;r"
  ]
  node [
    id 183
    label "gwiazda"
  ]
  node [
    id 184
    label "zbi&#243;r"
  ]
  node [
    id 185
    label "tr&#243;jka"
  ]
  node [
    id 186
    label "ciemno&#347;&#263;"
  ]
  node [
    id 187
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 188
    label "strza&#322;a_Amora"
  ]
  node [
    id 189
    label "orfik"
  ]
  node [
    id 190
    label "wino"
  ]
  node [
    id 191
    label "satyr"
  ]
  node [
    id 192
    label "orfizm"
  ]
  node [
    id 193
    label "p&#243;&#322;noc"
  ]
  node [
    id 194
    label "Prokrust"
  ]
  node [
    id 195
    label "woda"
  ]
  node [
    id 196
    label "hinduizm"
  ]
  node [
    id 197
    label "niebo"
  ]
  node [
    id 198
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 199
    label "tr&#243;jz&#261;b"
  ]
  node [
    id 200
    label "morze"
  ]
  node [
    id 201
    label "Logan"
  ]
  node [
    id 202
    label "winoro&#347;l"
  ]
  node [
    id 203
    label "po&#347;wi&#281;ci&#263;"
  ]
  node [
    id 204
    label "impart"
  ]
  node [
    id 205
    label "deklarowa&#263;"
  ]
  node [
    id 206
    label "zdeklarowa&#263;"
  ]
  node [
    id 207
    label "volunteer"
  ]
  node [
    id 208
    label "give"
  ]
  node [
    id 209
    label "zaproponowa&#263;"
  ]
  node [
    id 210
    label "podarowa&#263;"
  ]
  node [
    id 211
    label "afford"
  ]
  node [
    id 212
    label "B&#243;g"
  ]
  node [
    id 213
    label "oferowa&#263;"
  ]
  node [
    id 214
    label "wierzenie"
  ]
  node [
    id 215
    label "sacrifice"
  ]
  node [
    id 216
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 217
    label "podarowanie"
  ]
  node [
    id 218
    label "zaproponowanie"
  ]
  node [
    id 219
    label "oferowanie"
  ]
  node [
    id 220
    label "forfeit"
  ]
  node [
    id 221
    label "msza"
  ]
  node [
    id 222
    label "crack"
  ]
  node [
    id 223
    label "deklarowanie"
  ]
  node [
    id 224
    label "zdeklarowanie"
  ]
  node [
    id 225
    label "tender"
  ]
  node [
    id 226
    label "bo&#380;ek"
  ]
  node [
    id 227
    label "darowywa&#263;"
  ]
  node [
    id 228
    label "zapewnia&#263;"
  ]
  node [
    id 229
    label "po&#347;wi&#281;ca&#263;"
  ]
  node [
    id 230
    label "powa&#380;anie"
  ]
  node [
    id 231
    label "zachwyt"
  ]
  node [
    id 232
    label "admiracja"
  ]
  node [
    id 233
    label "po&#347;wi&#281;canie"
  ]
  node [
    id 234
    label "darowywanie"
  ]
  node [
    id 235
    label "zapewnianie"
  ]
  node [
    id 236
    label "zesp&#243;&#322;"
  ]
  node [
    id 237
    label "podejrzany"
  ]
  node [
    id 238
    label "s&#261;downictwo"
  ]
  node [
    id 239
    label "system"
  ]
  node [
    id 240
    label "biuro"
  ]
  node [
    id 241
    label "wytw&#243;r"
  ]
  node [
    id 242
    label "court"
  ]
  node [
    id 243
    label "forum"
  ]
  node [
    id 244
    label "bronienie"
  ]
  node [
    id 245
    label "urz&#261;d"
  ]
  node [
    id 246
    label "wydarzenie"
  ]
  node [
    id 247
    label "oskar&#380;yciel"
  ]
  node [
    id 248
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 249
    label "skazany"
  ]
  node [
    id 250
    label "post&#281;powanie"
  ]
  node [
    id 251
    label "broni&#263;"
  ]
  node [
    id 252
    label "my&#347;l"
  ]
  node [
    id 253
    label "pods&#261;dny"
  ]
  node [
    id 254
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 255
    label "obrona"
  ]
  node [
    id 256
    label "wypowied&#378;"
  ]
  node [
    id 257
    label "instytucja"
  ]
  node [
    id 258
    label "antylogizm"
  ]
  node [
    id 259
    label "konektyw"
  ]
  node [
    id 260
    label "&#347;wiadek"
  ]
  node [
    id 261
    label "procesowicz"
  ]
  node [
    id 262
    label "strona"
  ]
  node [
    id 263
    label "p&#322;&#243;d"
  ]
  node [
    id 264
    label "work"
  ]
  node [
    id 265
    label "rezultat"
  ]
  node [
    id 266
    label "Mazowsze"
  ]
  node [
    id 267
    label "odm&#322;adzanie"
  ]
  node [
    id 268
    label "&#346;wietliki"
  ]
  node [
    id 269
    label "whole"
  ]
  node [
    id 270
    label "skupienie"
  ]
  node [
    id 271
    label "The_Beatles"
  ]
  node [
    id 272
    label "odm&#322;adza&#263;"
  ]
  node [
    id 273
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 274
    label "zabudowania"
  ]
  node [
    id 275
    label "group"
  ]
  node [
    id 276
    label "zespolik"
  ]
  node [
    id 277
    label "schorzenie"
  ]
  node [
    id 278
    label "ro&#347;lina"
  ]
  node [
    id 279
    label "grupa"
  ]
  node [
    id 280
    label "Depeche_Mode"
  ]
  node [
    id 281
    label "batch"
  ]
  node [
    id 282
    label "odm&#322;odzenie"
  ]
  node [
    id 283
    label "stanowisko"
  ]
  node [
    id 284
    label "position"
  ]
  node [
    id 285
    label "siedziba"
  ]
  node [
    id 286
    label "organ"
  ]
  node [
    id 287
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 288
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 289
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 290
    label "mianowaniec"
  ]
  node [
    id 291
    label "dzia&#322;"
  ]
  node [
    id 292
    label "okienko"
  ]
  node [
    id 293
    label "w&#322;adza"
  ]
  node [
    id 294
    label "przebiec"
  ]
  node [
    id 295
    label "charakter"
  ]
  node [
    id 296
    label "czynno&#347;&#263;"
  ]
  node [
    id 297
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 298
    label "motyw"
  ]
  node [
    id 299
    label "przebiegni&#281;cie"
  ]
  node [
    id 300
    label "fabu&#322;a"
  ]
  node [
    id 301
    label "osoba_prawna"
  ]
  node [
    id 302
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 303
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 304
    label "poj&#281;cie"
  ]
  node [
    id 305
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 306
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 307
    label "organizacja"
  ]
  node [
    id 308
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 309
    label "Fundusze_Unijne"
  ]
  node [
    id 310
    label "zamyka&#263;"
  ]
  node [
    id 311
    label "establishment"
  ]
  node [
    id 312
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 313
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 314
    label "afiliowa&#263;"
  ]
  node [
    id 315
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 316
    label "standard"
  ]
  node [
    id 317
    label "zamykanie"
  ]
  node [
    id 318
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 319
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 320
    label "szko&#322;a"
  ]
  node [
    id 321
    label "thinking"
  ]
  node [
    id 322
    label "umys&#322;"
  ]
  node [
    id 323
    label "political_orientation"
  ]
  node [
    id 324
    label "pomys&#322;"
  ]
  node [
    id 325
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 326
    label "idea"
  ]
  node [
    id 327
    label "fantomatyka"
  ]
  node [
    id 328
    label "pos&#322;uchanie"
  ]
  node [
    id 329
    label "sparafrazowanie"
  ]
  node [
    id 330
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 331
    label "strawestowa&#263;"
  ]
  node [
    id 332
    label "sparafrazowa&#263;"
  ]
  node [
    id 333
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 334
    label "trawestowa&#263;"
  ]
  node [
    id 335
    label "sformu&#322;owanie"
  ]
  node [
    id 336
    label "parafrazowanie"
  ]
  node [
    id 337
    label "ozdobnik"
  ]
  node [
    id 338
    label "delimitacja"
  ]
  node [
    id 339
    label "parafrazowa&#263;"
  ]
  node [
    id 340
    label "stylizacja"
  ]
  node [
    id 341
    label "komunikat"
  ]
  node [
    id 342
    label "trawestowanie"
  ]
  node [
    id 343
    label "strawestowanie"
  ]
  node [
    id 344
    label "kognicja"
  ]
  node [
    id 345
    label "campaign"
  ]
  node [
    id 346
    label "rozprawa"
  ]
  node [
    id 347
    label "zachowanie"
  ]
  node [
    id 348
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 349
    label "fashion"
  ]
  node [
    id 350
    label "robienie"
  ]
  node [
    id 351
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 352
    label "zmierzanie"
  ]
  node [
    id 353
    label "przes&#322;anka"
  ]
  node [
    id 354
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 355
    label "kazanie"
  ]
  node [
    id 356
    label "funktor"
  ]
  node [
    id 357
    label "ob&#380;a&#322;owany"
  ]
  node [
    id 358
    label "skar&#380;yciel"
  ]
  node [
    id 359
    label "cz&#322;owiek"
  ]
  node [
    id 360
    label "dysponowanie"
  ]
  node [
    id 361
    label "dysponowa&#263;"
  ]
  node [
    id 362
    label "podejrzanie"
  ]
  node [
    id 363
    label "podmiot"
  ]
  node [
    id 364
    label "pos&#261;dzanie"
  ]
  node [
    id 365
    label "nieprzejrzysty"
  ]
  node [
    id 366
    label "niepewny"
  ]
  node [
    id 367
    label "z&#322;y"
  ]
  node [
    id 368
    label "egzamin"
  ]
  node [
    id 369
    label "walka"
  ]
  node [
    id 370
    label "liga"
  ]
  node [
    id 371
    label "gracz"
  ]
  node [
    id 372
    label "protection"
  ]
  node [
    id 373
    label "poparcie"
  ]
  node [
    id 374
    label "mecz"
  ]
  node [
    id 375
    label "reakcja"
  ]
  node [
    id 376
    label "defense"
  ]
  node [
    id 377
    label "auspices"
  ]
  node [
    id 378
    label "gra"
  ]
  node [
    id 379
    label "ochrona"
  ]
  node [
    id 380
    label "sp&#243;r"
  ]
  node [
    id 381
    label "wojsko"
  ]
  node [
    id 382
    label "manewr"
  ]
  node [
    id 383
    label "defensive_structure"
  ]
  node [
    id 384
    label "guard_duty"
  ]
  node [
    id 385
    label "uczestnik"
  ]
  node [
    id 386
    label "dru&#380;ba"
  ]
  node [
    id 387
    label "obserwator"
  ]
  node [
    id 388
    label "osoba_fizyczna"
  ]
  node [
    id 389
    label "niedost&#281;pny"
  ]
  node [
    id 390
    label "obstawanie"
  ]
  node [
    id 391
    label "adwokatowanie"
  ]
  node [
    id 392
    label "zdawanie"
  ]
  node [
    id 393
    label "walczenie"
  ]
  node [
    id 394
    label "zabezpieczenie"
  ]
  node [
    id 395
    label "t&#322;umaczenie"
  ]
  node [
    id 396
    label "parry"
  ]
  node [
    id 397
    label "or&#281;dowanie"
  ]
  node [
    id 398
    label "granie"
  ]
  node [
    id 399
    label "kartka"
  ]
  node [
    id 400
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 401
    label "logowanie"
  ]
  node [
    id 402
    label "plik"
  ]
  node [
    id 403
    label "adres_internetowy"
  ]
  node [
    id 404
    label "linia"
  ]
  node [
    id 405
    label "serwis_internetowy"
  ]
  node [
    id 406
    label "bok"
  ]
  node [
    id 407
    label "skr&#281;canie"
  ]
  node [
    id 408
    label "skr&#281;ca&#263;"
  ]
  node [
    id 409
    label "orientowanie"
  ]
  node [
    id 410
    label "skr&#281;ci&#263;"
  ]
  node [
    id 411
    label "uj&#281;cie"
  ]
  node [
    id 412
    label "zorientowanie"
  ]
  node [
    id 413
    label "ty&#322;"
  ]
  node [
    id 414
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 415
    label "fragment"
  ]
  node [
    id 416
    label "layout"
  ]
  node [
    id 417
    label "obiekt"
  ]
  node [
    id 418
    label "zorientowa&#263;"
  ]
  node [
    id 419
    label "pagina"
  ]
  node [
    id 420
    label "g&#243;ra"
  ]
  node [
    id 421
    label "orientowa&#263;"
  ]
  node [
    id 422
    label "voice"
  ]
  node [
    id 423
    label "orientacja"
  ]
  node [
    id 424
    label "prz&#243;d"
  ]
  node [
    id 425
    label "internet"
  ]
  node [
    id 426
    label "powierzchnia"
  ]
  node [
    id 427
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 428
    label "forma"
  ]
  node [
    id 429
    label "skr&#281;cenie"
  ]
  node [
    id 430
    label "fend"
  ]
  node [
    id 431
    label "reprezentowa&#263;"
  ]
  node [
    id 432
    label "robi&#263;"
  ]
  node [
    id 433
    label "zdawa&#263;"
  ]
  node [
    id 434
    label "czuwa&#263;"
  ]
  node [
    id 435
    label "preach"
  ]
  node [
    id 436
    label "chroni&#263;"
  ]
  node [
    id 437
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 438
    label "walczy&#263;"
  ]
  node [
    id 439
    label "resist"
  ]
  node [
    id 440
    label "adwokatowa&#263;"
  ]
  node [
    id 441
    label "rebuff"
  ]
  node [
    id 442
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 443
    label "udowadnia&#263;"
  ]
  node [
    id 444
    label "gra&#263;"
  ]
  node [
    id 445
    label "sprawowa&#263;"
  ]
  node [
    id 446
    label "refuse"
  ]
  node [
    id 447
    label "biurko"
  ]
  node [
    id 448
    label "boks"
  ]
  node [
    id 449
    label "palestra"
  ]
  node [
    id 450
    label "Biuro_Lustracyjne"
  ]
  node [
    id 451
    label "agency"
  ]
  node [
    id 452
    label "board"
  ]
  node [
    id 453
    label "pomieszczenie"
  ]
  node [
    id 454
    label "j&#261;dro"
  ]
  node [
    id 455
    label "systemik"
  ]
  node [
    id 456
    label "rozprz&#261;c"
  ]
  node [
    id 457
    label "oprogramowanie"
  ]
  node [
    id 458
    label "systemat"
  ]
  node [
    id 459
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 460
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 461
    label "model"
  ]
  node [
    id 462
    label "struktura"
  ]
  node [
    id 463
    label "usenet"
  ]
  node [
    id 464
    label "porz&#261;dek"
  ]
  node [
    id 465
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 466
    label "przyn&#281;ta"
  ]
  node [
    id 467
    label "net"
  ]
  node [
    id 468
    label "w&#281;dkarstwo"
  ]
  node [
    id 469
    label "eratem"
  ]
  node [
    id 470
    label "oddzia&#322;"
  ]
  node [
    id 471
    label "doktryna"
  ]
  node [
    id 472
    label "pulpit"
  ]
  node [
    id 473
    label "konstelacja"
  ]
  node [
    id 474
    label "jednostka_geologiczna"
  ]
  node [
    id 475
    label "o&#347;"
  ]
  node [
    id 476
    label "podsystem"
  ]
  node [
    id 477
    label "metoda"
  ]
  node [
    id 478
    label "ryba"
  ]
  node [
    id 479
    label "Leopard"
  ]
  node [
    id 480
    label "spos&#243;b"
  ]
  node [
    id 481
    label "Android"
  ]
  node [
    id 482
    label "cybernetyk"
  ]
  node [
    id 483
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 484
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 485
    label "method"
  ]
  node [
    id 486
    label "sk&#322;ad"
  ]
  node [
    id 487
    label "podstawa"
  ]
  node [
    id 488
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 489
    label "relacja_logiczna"
  ]
  node [
    id 490
    label "judiciary"
  ]
  node [
    id 491
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 492
    label "grupa_dyskusyjna"
  ]
  node [
    id 493
    label "plac"
  ]
  node [
    id 494
    label "bazylika"
  ]
  node [
    id 495
    label "miejsce"
  ]
  node [
    id 496
    label "portal"
  ]
  node [
    id 497
    label "konferencja"
  ]
  node [
    id 498
    label "agora"
  ]
  node [
    id 499
    label "szach"
  ]
  node [
    id 500
    label "podoficer_marynarki"
  ]
  node [
    id 501
    label "szachy"
  ]
  node [
    id 502
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 503
    label "ruch"
  ]
  node [
    id 504
    label "mechanika"
  ]
  node [
    id 505
    label "utrzymywanie"
  ]
  node [
    id 506
    label "move"
  ]
  node [
    id 507
    label "poruszenie"
  ]
  node [
    id 508
    label "movement"
  ]
  node [
    id 509
    label "myk"
  ]
  node [
    id 510
    label "utrzyma&#263;"
  ]
  node [
    id 511
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 512
    label "zjawisko"
  ]
  node [
    id 513
    label "utrzymanie"
  ]
  node [
    id 514
    label "travel"
  ]
  node [
    id 515
    label "kanciasty"
  ]
  node [
    id 516
    label "commercial_enterprise"
  ]
  node [
    id 517
    label "strumie&#324;"
  ]
  node [
    id 518
    label "proces"
  ]
  node [
    id 519
    label "aktywno&#347;&#263;"
  ]
  node [
    id 520
    label "kr&#243;tki"
  ]
  node [
    id 521
    label "taktyka"
  ]
  node [
    id 522
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 523
    label "apraksja"
  ]
  node [
    id 524
    label "natural_process"
  ]
  node [
    id 525
    label "utrzymywa&#263;"
  ]
  node [
    id 526
    label "d&#322;ugi"
  ]
  node [
    id 527
    label "dyssypacja_energii"
  ]
  node [
    id 528
    label "tumult"
  ]
  node [
    id 529
    label "stopek"
  ]
  node [
    id 530
    label "zmiana"
  ]
  node [
    id 531
    label "lokomocja"
  ]
  node [
    id 532
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 533
    label "komunikacja"
  ]
  node [
    id 534
    label "drift"
  ]
  node [
    id 535
    label "Afganistan"
  ]
  node [
    id 536
    label "arrest"
  ]
  node [
    id 537
    label "pozycja"
  ]
  node [
    id 538
    label "monarcha"
  ]
  node [
    id 539
    label "Persja"
  ]
  node [
    id 540
    label "szachownica"
  ]
  node [
    id 541
    label "linia_przemiany"
  ]
  node [
    id 542
    label "p&#243;&#322;ruch"
  ]
  node [
    id 543
    label "przes&#322;ona"
  ]
  node [
    id 544
    label "niedoczas"
  ]
  node [
    id 545
    label "zegar_szachowy"
  ]
  node [
    id 546
    label "sport_umys&#322;owy"
  ]
  node [
    id 547
    label "roszada"
  ]
  node [
    id 548
    label "promocja"
  ]
  node [
    id 549
    label "tempo"
  ]
  node [
    id 550
    label "bicie_w_przelocie"
  ]
  node [
    id 551
    label "gra_planszowa"
  ]
  node [
    id 552
    label "dual"
  ]
  node [
    id 553
    label "Cesarstwo_Bizanty&#324;skie"
  ]
  node [
    id 554
    label "jednostka_administracyjna"
  ]
  node [
    id 555
    label "dostatecznie"
  ]
  node [
    id 556
    label "wystarczaj&#261;cy"
  ]
  node [
    id 557
    label "zado&#347;&#263;"
  ]
  node [
    id 558
    label "wystarczaj&#261;co"
  ]
  node [
    id 559
    label "sufficiently"
  ]
  node [
    id 560
    label "niez&#322;y"
  ]
  node [
    id 561
    label "odpowiedni"
  ]
  node [
    id 562
    label "toto-lotek"
  ]
  node [
    id 563
    label "trafienie"
  ]
  node [
    id 564
    label "bilard"
  ]
  node [
    id 565
    label "kie&#322;"
  ]
  node [
    id 566
    label "hotel"
  ]
  node [
    id 567
    label "stopie&#324;"
  ]
  node [
    id 568
    label "cyfra"
  ]
  node [
    id 569
    label "pok&#243;j"
  ]
  node [
    id 570
    label "three"
  ]
  node [
    id 571
    label "blotka"
  ]
  node [
    id 572
    label "zaprz&#281;g"
  ]
  node [
    id 573
    label "krok_taneczny"
  ]
  node [
    id 574
    label "fakt"
  ]
  node [
    id 575
    label "czyn"
  ]
  node [
    id 576
    label "ilustracja"
  ]
  node [
    id 577
    label "przedstawiciel"
  ]
  node [
    id 578
    label "materia&#322;"
  ]
  node [
    id 579
    label "szata_graficzna"
  ]
  node [
    id 580
    label "photograph"
  ]
  node [
    id 581
    label "obrazek"
  ]
  node [
    id 582
    label "bia&#322;e_plamy"
  ]
  node [
    id 583
    label "asymilowanie"
  ]
  node [
    id 584
    label "wapniak"
  ]
  node [
    id 585
    label "asymilowa&#263;"
  ]
  node [
    id 586
    label "os&#322;abia&#263;"
  ]
  node [
    id 587
    label "hominid"
  ]
  node [
    id 588
    label "podw&#322;adny"
  ]
  node [
    id 589
    label "os&#322;abianie"
  ]
  node [
    id 590
    label "dwun&#243;g"
  ]
  node [
    id 591
    label "nasada"
  ]
  node [
    id 592
    label "senior"
  ]
  node [
    id 593
    label "Adam"
  ]
  node [
    id 594
    label "polifag"
  ]
  node [
    id 595
    label "funkcja"
  ]
  node [
    id 596
    label "act"
  ]
  node [
    id 597
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 598
    label "cz&#322;onek"
  ]
  node [
    id 599
    label "substytuowa&#263;"
  ]
  node [
    id 600
    label "substytuowanie"
  ]
  node [
    id 601
    label "zast&#281;pca"
  ]
  node [
    id 602
    label "holiness"
  ]
  node [
    id 603
    label "duchowo&#347;&#263;"
  ]
  node [
    id 604
    label "cecha"
  ]
  node [
    id 605
    label "charakterystyka"
  ]
  node [
    id 606
    label "m&#322;ot"
  ]
  node [
    id 607
    label "znak"
  ]
  node [
    id 608
    label "drzewo"
  ]
  node [
    id 609
    label "pr&#243;ba"
  ]
  node [
    id 610
    label "attribute"
  ]
  node [
    id 611
    label "marka"
  ]
  node [
    id 612
    label "idealizm"
  ]
  node [
    id 613
    label "pogl&#261;d"
  ]
  node [
    id 614
    label "seksualno&#347;&#263;"
  ]
  node [
    id 615
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 616
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 617
    label "deformowa&#263;"
  ]
  node [
    id 618
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 619
    label "osobowo&#347;&#263;"
  ]
  node [
    id 620
    label "ego"
  ]
  node [
    id 621
    label "nastawienie"
  ]
  node [
    id 622
    label "sfera_afektywna"
  ]
  node [
    id 623
    label "deformowanie"
  ]
  node [
    id 624
    label "kompleks"
  ]
  node [
    id 625
    label "spiritualism"
  ]
  node [
    id 626
    label "sumienie"
  ]
  node [
    id 627
    label "powinowaci"
  ]
  node [
    id 628
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 629
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 630
    label "rodze&#324;stwo"
  ]
  node [
    id 631
    label "jednostka_systematyczna"
  ]
  node [
    id 632
    label "krewni"
  ]
  node [
    id 633
    label "Ossoli&#324;scy"
  ]
  node [
    id 634
    label "potomstwo"
  ]
  node [
    id 635
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 636
    label "theater"
  ]
  node [
    id 637
    label "Soplicowie"
  ]
  node [
    id 638
    label "kin"
  ]
  node [
    id 639
    label "family"
  ]
  node [
    id 640
    label "rodzice"
  ]
  node [
    id 641
    label "ordynacja"
  ]
  node [
    id 642
    label "dom_rodzinny"
  ]
  node [
    id 643
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 644
    label "Ostrogscy"
  ]
  node [
    id 645
    label "bliscy"
  ]
  node [
    id 646
    label "przyjaciel_domu"
  ]
  node [
    id 647
    label "dom"
  ]
  node [
    id 648
    label "rz&#261;d"
  ]
  node [
    id 649
    label "Firlejowie"
  ]
  node [
    id 650
    label "Kossakowie"
  ]
  node [
    id 651
    label "Czartoryscy"
  ]
  node [
    id 652
    label "Sapiehowie"
  ]
  node [
    id 653
    label "gromada"
  ]
  node [
    id 654
    label "egzemplarz"
  ]
  node [
    id 655
    label "Entuzjastki"
  ]
  node [
    id 656
    label "kompozycja"
  ]
  node [
    id 657
    label "Terranie"
  ]
  node [
    id 658
    label "category"
  ]
  node [
    id 659
    label "pakiet_klimatyczny"
  ]
  node [
    id 660
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 661
    label "cz&#261;steczka"
  ]
  node [
    id 662
    label "stage_set"
  ]
  node [
    id 663
    label "type"
  ]
  node [
    id 664
    label "specgrupa"
  ]
  node [
    id 665
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 666
    label "Eurogrupa"
  ]
  node [
    id 667
    label "formacja_geologiczna"
  ]
  node [
    id 668
    label "harcerze_starsi"
  ]
  node [
    id 669
    label "series"
  ]
  node [
    id 670
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 671
    label "uprawianie"
  ]
  node [
    id 672
    label "praca_rolnicza"
  ]
  node [
    id 673
    label "collection"
  ]
  node [
    id 674
    label "dane"
  ]
  node [
    id 675
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 676
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 677
    label "sum"
  ]
  node [
    id 678
    label "gathering"
  ]
  node [
    id 679
    label "album"
  ]
  node [
    id 680
    label "grono"
  ]
  node [
    id 681
    label "kuzynostwo"
  ]
  node [
    id 682
    label "starzy"
  ]
  node [
    id 683
    label "pokolenie"
  ]
  node [
    id 684
    label "wapniaki"
  ]
  node [
    id 685
    label "stan_cywilny"
  ]
  node [
    id 686
    label "para"
  ]
  node [
    id 687
    label "matrymonialny"
  ]
  node [
    id 688
    label "lewirat"
  ]
  node [
    id 689
    label "sakrament"
  ]
  node [
    id 690
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 691
    label "zwi&#261;zek"
  ]
  node [
    id 692
    label "partia"
  ]
  node [
    id 693
    label "czeladka"
  ]
  node [
    id 694
    label "dzietno&#347;&#263;"
  ]
  node [
    id 695
    label "bawienie_si&#281;"
  ]
  node [
    id 696
    label "pomiot"
  ]
  node [
    id 697
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 698
    label "substancja_mieszkaniowa"
  ]
  node [
    id 699
    label "budynek"
  ]
  node [
    id 700
    label "stead"
  ]
  node [
    id 701
    label "garderoba"
  ]
  node [
    id 702
    label "wiecha"
  ]
  node [
    id 703
    label "fratria"
  ]
  node [
    id 704
    label "maj&#261;tek_ziemski"
  ]
  node [
    id 705
    label "obrz&#281;d"
  ]
  node [
    id 706
    label "przybli&#380;enie"
  ]
  node [
    id 707
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 708
    label "kategoria"
  ]
  node [
    id 709
    label "szpaler"
  ]
  node [
    id 710
    label "lon&#380;a"
  ]
  node [
    id 711
    label "uporz&#261;dkowanie"
  ]
  node [
    id 712
    label "egzekutywa"
  ]
  node [
    id 713
    label "premier"
  ]
  node [
    id 714
    label "Londyn"
  ]
  node [
    id 715
    label "gabinet_cieni"
  ]
  node [
    id 716
    label "number"
  ]
  node [
    id 717
    label "Konsulat"
  ]
  node [
    id 718
    label "tract"
  ]
  node [
    id 719
    label "klasa"
  ]
  node [
    id 720
    label "folk_music"
  ]
  node [
    id 721
    label "niezgodny"
  ]
  node [
    id 722
    label "bezlitosny"
  ]
  node [
    id 723
    label "niepobo&#380;ny"
  ]
  node [
    id 724
    label "bezbo&#380;nie"
  ]
  node [
    id 725
    label "naganny"
  ]
  node [
    id 726
    label "r&#243;&#380;ny"
  ]
  node [
    id 727
    label "niespokojny"
  ]
  node [
    id 728
    label "odmienny"
  ]
  node [
    id 729
    label "k&#322;&#243;tny"
  ]
  node [
    id 730
    label "niezgodnie"
  ]
  node [
    id 731
    label "napi&#281;ty"
  ]
  node [
    id 732
    label "pieski"
  ]
  node [
    id 733
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 734
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 735
    label "niekorzystny"
  ]
  node [
    id 736
    label "z&#322;oszczenie"
  ]
  node [
    id 737
    label "sierdzisty"
  ]
  node [
    id 738
    label "niegrzeczny"
  ]
  node [
    id 739
    label "zez&#322;oszczenie"
  ]
  node [
    id 740
    label "zdenerwowany"
  ]
  node [
    id 741
    label "negatywny"
  ]
  node [
    id 742
    label "rozgniewanie"
  ]
  node [
    id 743
    label "gniewanie"
  ]
  node [
    id 744
    label "niemoralny"
  ]
  node [
    id 745
    label "&#378;le"
  ]
  node [
    id 746
    label "niepomy&#347;lny"
  ]
  node [
    id 747
    label "syf"
  ]
  node [
    id 748
    label "straszny"
  ]
  node [
    id 749
    label "przykry"
  ]
  node [
    id 750
    label "bezwzgl&#281;dny"
  ]
  node [
    id 751
    label "trudny"
  ]
  node [
    id 752
    label "bezlito&#347;ny"
  ]
  node [
    id 753
    label "bezlito&#347;nie"
  ]
  node [
    id 754
    label "prawdziwy"
  ]
  node [
    id 755
    label "niepob&#322;a&#380;liwy"
  ]
  node [
    id 756
    label "nie&#322;askawy"
  ]
  node [
    id 757
    label "gro&#378;ny"
  ]
  node [
    id 758
    label "okrutnie"
  ]
  node [
    id 759
    label "nagannie"
  ]
  node [
    id 760
    label "contest"
  ]
  node [
    id 761
    label "wypowiada&#263;"
  ]
  node [
    id 762
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 763
    label "thank"
  ]
  node [
    id 764
    label "odpowiada&#263;"
  ]
  node [
    id 765
    label "odrzuca&#263;"
  ]
  node [
    id 766
    label "frame"
  ]
  node [
    id 767
    label "repudiate"
  ]
  node [
    id 768
    label "usuwa&#263;"
  ]
  node [
    id 769
    label "odpiera&#263;"
  ]
  node [
    id 770
    label "zmienia&#263;"
  ]
  node [
    id 771
    label "reagowa&#263;"
  ]
  node [
    id 772
    label "oddala&#263;"
  ]
  node [
    id 773
    label "react"
  ]
  node [
    id 774
    label "dawa&#263;"
  ]
  node [
    id 775
    label "ponosi&#263;"
  ]
  node [
    id 776
    label "pytanie"
  ]
  node [
    id 777
    label "equate"
  ]
  node [
    id 778
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 779
    label "answer"
  ]
  node [
    id 780
    label "powodowa&#263;"
  ]
  node [
    id 781
    label "tone"
  ]
  node [
    id 782
    label "contend"
  ]
  node [
    id 783
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 784
    label "express"
  ]
  node [
    id 785
    label "werbalizowa&#263;"
  ]
  node [
    id 786
    label "typify"
  ]
  node [
    id 787
    label "say"
  ]
  node [
    id 788
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 789
    label "wydobywa&#263;"
  ]
  node [
    id 790
    label "strike"
  ]
  node [
    id 791
    label "s&#261;dzi&#263;"
  ]
  node [
    id 792
    label "znajdowa&#263;"
  ]
  node [
    id 793
    label "hold"
  ]
  node [
    id 794
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 795
    label "pot&#281;&#380;ny"
  ]
  node [
    id 796
    label "olbrzymi"
  ]
  node [
    id 797
    label "wszechmocnie"
  ]
  node [
    id 798
    label "wszechw&#322;adnie"
  ]
  node [
    id 799
    label "mocny"
  ]
  node [
    id 800
    label "pot&#281;&#380;nie"
  ]
  node [
    id 801
    label "wielow&#322;adny"
  ]
  node [
    id 802
    label "ogromny"
  ]
  node [
    id 803
    label "ros&#322;y"
  ]
  node [
    id 804
    label "konkretny"
  ]
  node [
    id 805
    label "ogromnie"
  ]
  node [
    id 806
    label "okaza&#322;y"
  ]
  node [
    id 807
    label "jebitny"
  ]
  node [
    id 808
    label "olbrzymio"
  ]
  node [
    id 809
    label "wszechw&#322;adny"
  ]
  node [
    id 810
    label "zupe&#322;nie"
  ]
  node [
    id 811
    label "powinny"
  ]
  node [
    id 812
    label "nale&#380;nie"
  ]
  node [
    id 813
    label "nale&#380;yty"
  ]
  node [
    id 814
    label "godny"
  ]
  node [
    id 815
    label "przynale&#380;ny"
  ]
  node [
    id 816
    label "godnie"
  ]
  node [
    id 817
    label "godziwy"
  ]
  node [
    id 818
    label "powa&#380;ny"
  ]
  node [
    id 819
    label "zdystansowany"
  ]
  node [
    id 820
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 821
    label "okre&#347;lony"
  ]
  node [
    id 822
    label "identyfikowalny"
  ]
  node [
    id 823
    label "zadowalaj&#261;cy"
  ]
  node [
    id 824
    label "nale&#380;ycie"
  ]
  node [
    id 825
    label "przystojny"
  ]
  node [
    id 826
    label "respect"
  ]
  node [
    id 827
    label "deference"
  ]
  node [
    id 828
    label "honorowa&#263;"
  ]
  node [
    id 829
    label "uhonorowanie"
  ]
  node [
    id 830
    label "zaimponowanie"
  ]
  node [
    id 831
    label "honorowanie"
  ]
  node [
    id 832
    label "uszanowa&#263;"
  ]
  node [
    id 833
    label "chowanie"
  ]
  node [
    id 834
    label "respektowanie"
  ]
  node [
    id 835
    label "uszanowanie"
  ]
  node [
    id 836
    label "szacuneczek"
  ]
  node [
    id 837
    label "rewerencja"
  ]
  node [
    id 838
    label "uhonorowa&#263;"
  ]
  node [
    id 839
    label "czucie"
  ]
  node [
    id 840
    label "szanowa&#263;"
  ]
  node [
    id 841
    label "fame"
  ]
  node [
    id 842
    label "postawa"
  ]
  node [
    id 843
    label "imponowanie"
  ]
  node [
    id 844
    label "kult"
  ]
  node [
    id 845
    label "mod&#322;y"
  ]
  node [
    id 846
    label "modlitwa"
  ]
  node [
    id 847
    label "ceremony"
  ]
  node [
    id 848
    label "post&#261;pi&#263;"
  ]
  node [
    id 849
    label "zainteresowa&#263;"
  ]
  node [
    id 850
    label "wymin&#261;&#263;"
  ]
  node [
    id 851
    label "skirt"
  ]
  node [
    id 852
    label "odwiedzi&#263;"
  ]
  node [
    id 853
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 854
    label "poprowadzi&#263;"
  ]
  node [
    id 855
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 856
    label "przej&#347;&#263;_si&#281;"
  ]
  node [
    id 857
    label "wykorzysta&#263;"
  ]
  node [
    id 858
    label "omin&#261;&#263;"
  ]
  node [
    id 859
    label "interest"
  ]
  node [
    id 860
    label "rozciekawi&#263;"
  ]
  node [
    id 861
    label "wzbudzi&#263;"
  ]
  node [
    id 862
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 863
    label "advance"
  ]
  node [
    id 864
    label "zrobi&#263;"
  ]
  node [
    id 865
    label "see"
  ]
  node [
    id 866
    label "u&#380;y&#263;"
  ]
  node [
    id 867
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 868
    label "seize"
  ]
  node [
    id 869
    label "skorzysta&#263;"
  ]
  node [
    id 870
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 871
    label "doprowadzi&#263;"
  ]
  node [
    id 872
    label "zbudowa&#263;"
  ]
  node [
    id 873
    label "krzywa"
  ]
  node [
    id 874
    label "control"
  ]
  node [
    id 875
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 876
    label "leave"
  ]
  node [
    id 877
    label "nakre&#347;li&#263;"
  ]
  node [
    id 878
    label "moderate"
  ]
  node [
    id 879
    label "guidebook"
  ]
  node [
    id 880
    label "visualize"
  ]
  node [
    id 881
    label "zawita&#263;"
  ]
  node [
    id 882
    label "poradzi&#263;_sobie"
  ]
  node [
    id 883
    label "sidestep"
  ]
  node [
    id 884
    label "unikn&#261;&#263;"
  ]
  node [
    id 885
    label "min&#261;&#263;"
  ]
  node [
    id 886
    label "overwhelm"
  ]
  node [
    id 887
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 888
    label "spowodowa&#263;"
  ]
  node [
    id 889
    label "admit"
  ]
  node [
    id 890
    label "pomin&#261;&#263;"
  ]
  node [
    id 891
    label "zlekcewa&#380;y&#263;"
  ]
  node [
    id 892
    label "przej&#347;&#263;"
  ]
  node [
    id 893
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 894
    label "opu&#347;ci&#263;"
  ]
  node [
    id 895
    label "straci&#263;"
  ]
  node [
    id 896
    label "shed"
  ]
  node [
    id 897
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 898
    label "krzew"
  ]
  node [
    id 899
    label "delfinidyna"
  ]
  node [
    id 900
    label "pi&#380;maczkowate"
  ]
  node [
    id 901
    label "ki&#347;&#263;"
  ]
  node [
    id 902
    label "hy&#263;ka"
  ]
  node [
    id 903
    label "pestkowiec"
  ]
  node [
    id 904
    label "kwiat"
  ]
  node [
    id 905
    label "owoc"
  ]
  node [
    id 906
    label "oliwkowate"
  ]
  node [
    id 907
    label "lilac"
  ]
  node [
    id 908
    label "kostka"
  ]
  node [
    id 909
    label "kita"
  ]
  node [
    id 910
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 911
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 912
    label "d&#322;o&#324;"
  ]
  node [
    id 913
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 914
    label "powerball"
  ]
  node [
    id 915
    label "&#380;ubr"
  ]
  node [
    id 916
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 917
    label "p&#281;k"
  ]
  node [
    id 918
    label "r&#281;ka"
  ]
  node [
    id 919
    label "ogon"
  ]
  node [
    id 920
    label "zako&#324;czenie"
  ]
  node [
    id 921
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 922
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 923
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 924
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 925
    label "flakon"
  ]
  node [
    id 926
    label "przykoronek"
  ]
  node [
    id 927
    label "kielich"
  ]
  node [
    id 928
    label "dno_kwiatowe"
  ]
  node [
    id 929
    label "organ_ro&#347;linny"
  ]
  node [
    id 930
    label "warga"
  ]
  node [
    id 931
    label "korona"
  ]
  node [
    id 932
    label "rurka"
  ]
  node [
    id 933
    label "ozdoba"
  ]
  node [
    id 934
    label "&#322;yko"
  ]
  node [
    id 935
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 936
    label "karczowa&#263;"
  ]
  node [
    id 937
    label "wykarczowanie"
  ]
  node [
    id 938
    label "skupina"
  ]
  node [
    id 939
    label "wykarczowa&#263;"
  ]
  node [
    id 940
    label "karczowanie"
  ]
  node [
    id 941
    label "fanerofit"
  ]
  node [
    id 942
    label "zbiorowisko"
  ]
  node [
    id 943
    label "ro&#347;liny"
  ]
  node [
    id 944
    label "p&#281;d"
  ]
  node [
    id 945
    label "wegetowanie"
  ]
  node [
    id 946
    label "zadziorek"
  ]
  node [
    id 947
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 948
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 949
    label "do&#322;owa&#263;"
  ]
  node [
    id 950
    label "wegetacja"
  ]
  node [
    id 951
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 952
    label "strzyc"
  ]
  node [
    id 953
    label "w&#322;&#243;kno"
  ]
  node [
    id 954
    label "g&#322;uszenie"
  ]
  node [
    id 955
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 956
    label "fitotron"
  ]
  node [
    id 957
    label "bulwka"
  ]
  node [
    id 958
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 959
    label "odn&#243;&#380;ka"
  ]
  node [
    id 960
    label "epiderma"
  ]
  node [
    id 961
    label "gumoza"
  ]
  node [
    id 962
    label "strzy&#380;enie"
  ]
  node [
    id 963
    label "wypotnik"
  ]
  node [
    id 964
    label "flawonoid"
  ]
  node [
    id 965
    label "wyro&#347;le"
  ]
  node [
    id 966
    label "do&#322;owanie"
  ]
  node [
    id 967
    label "g&#322;uszy&#263;"
  ]
  node [
    id 968
    label "pora&#380;a&#263;"
  ]
  node [
    id 969
    label "fitocenoza"
  ]
  node [
    id 970
    label "hodowla"
  ]
  node [
    id 971
    label "fotoautotrof"
  ]
  node [
    id 972
    label "nieuleczalnie_chory"
  ]
  node [
    id 973
    label "wegetowa&#263;"
  ]
  node [
    id 974
    label "pochewka"
  ]
  node [
    id 975
    label "sok"
  ]
  node [
    id 976
    label "system_korzeniowy"
  ]
  node [
    id 977
    label "zawi&#261;zek"
  ]
  node [
    id 978
    label "pestka"
  ]
  node [
    id 979
    label "mi&#261;&#380;sz"
  ]
  node [
    id 980
    label "frukt"
  ]
  node [
    id 981
    label "drylowanie"
  ]
  node [
    id 982
    label "produkt"
  ]
  node [
    id 983
    label "owocnia"
  ]
  node [
    id 984
    label "fruktoza"
  ]
  node [
    id 985
    label "gniazdo_nasienne"
  ]
  node [
    id 986
    label "glukoza"
  ]
  node [
    id 987
    label "antocyjanidyn"
  ]
  node [
    id 988
    label "szczeciowce"
  ]
  node [
    id 989
    label "jasnotowce"
  ]
  node [
    id 990
    label "Oleaceae"
  ]
  node [
    id 991
    label "wielkopolski"
  ]
  node [
    id 992
    label "bez_czarny"
  ]
  node [
    id 993
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 994
    label "dobro"
  ]
  node [
    id 995
    label "ekstraspekcja"
  ]
  node [
    id 996
    label "feeling"
  ]
  node [
    id 997
    label "wiedza"
  ]
  node [
    id 998
    label "zemdle&#263;"
  ]
  node [
    id 999
    label "psychika"
  ]
  node [
    id 1000
    label "stan"
  ]
  node [
    id 1001
    label "Freud"
  ]
  node [
    id 1002
    label "psychoanaliza"
  ]
  node [
    id 1003
    label "conscience"
  ]
  node [
    id 1004
    label "warto&#347;&#263;"
  ]
  node [
    id 1005
    label "u&#380;yteczno&#347;&#263;_ca&#322;kowita"
  ]
  node [
    id 1006
    label "dobro&#263;"
  ]
  node [
    id 1007
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1008
    label "krzywa_Engla"
  ]
  node [
    id 1009
    label "cel"
  ]
  node [
    id 1010
    label "dobra"
  ]
  node [
    id 1011
    label "go&#322;&#261;bek"
  ]
  node [
    id 1012
    label "despond"
  ]
  node [
    id 1013
    label "litera"
  ]
  node [
    id 1014
    label "kalokagatia"
  ]
  node [
    id 1015
    label "rzecz"
  ]
  node [
    id 1016
    label "g&#322;agolica"
  ]
  node [
    id 1017
    label "attitude"
  ]
  node [
    id 1018
    label "uznawanie"
  ]
  node [
    id 1019
    label "p&#322;acenie"
  ]
  node [
    id 1020
    label "honor"
  ]
  node [
    id 1021
    label "okazywanie"
  ]
  node [
    id 1022
    label "czci&#263;"
  ]
  node [
    id 1023
    label "wyp&#322;aca&#263;"
  ]
  node [
    id 1024
    label "fit"
  ]
  node [
    id 1025
    label "treasure"
  ]
  node [
    id 1026
    label "respektowa&#263;"
  ]
  node [
    id 1027
    label "wyrazi&#263;"
  ]
  node [
    id 1028
    label "spare_part"
  ]
  node [
    id 1029
    label "nagrodzi&#263;"
  ]
  node [
    id 1030
    label "uczci&#263;"
  ]
  node [
    id 1031
    label "wyp&#322;aci&#263;"
  ]
  node [
    id 1032
    label "wzbudzanie"
  ]
  node [
    id 1033
    label "szanowanie"
  ]
  node [
    id 1034
    label "wzbudzenie"
  ]
  node [
    id 1035
    label "nagrodzenie"
  ]
  node [
    id 1036
    label "zap&#322;acenie"
  ]
  node [
    id 1037
    label "wyra&#380;enie"
  ]
  node [
    id 1038
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1039
    label "mie&#263;_miejsce"
  ]
  node [
    id 1040
    label "equal"
  ]
  node [
    id 1041
    label "trwa&#263;"
  ]
  node [
    id 1042
    label "chodzi&#263;"
  ]
  node [
    id 1043
    label "si&#281;ga&#263;"
  ]
  node [
    id 1044
    label "obecno&#347;&#263;"
  ]
  node [
    id 1045
    label "stand"
  ]
  node [
    id 1046
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1047
    label "uczestniczy&#263;"
  ]
  node [
    id 1048
    label "participate"
  ]
  node [
    id 1049
    label "istnie&#263;"
  ]
  node [
    id 1050
    label "pozostawa&#263;"
  ]
  node [
    id 1051
    label "zostawa&#263;"
  ]
  node [
    id 1052
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1053
    label "adhere"
  ]
  node [
    id 1054
    label "compass"
  ]
  node [
    id 1055
    label "korzysta&#263;"
  ]
  node [
    id 1056
    label "appreciation"
  ]
  node [
    id 1057
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1058
    label "dociera&#263;"
  ]
  node [
    id 1059
    label "get"
  ]
  node [
    id 1060
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1061
    label "mierzy&#263;"
  ]
  node [
    id 1062
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1063
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1064
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1065
    label "exsert"
  ]
  node [
    id 1066
    label "being"
  ]
  node [
    id 1067
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1068
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1069
    label "run"
  ]
  node [
    id 1070
    label "bangla&#263;"
  ]
  node [
    id 1071
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1072
    label "przebiega&#263;"
  ]
  node [
    id 1073
    label "proceed"
  ]
  node [
    id 1074
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1075
    label "carry"
  ]
  node [
    id 1076
    label "bywa&#263;"
  ]
  node [
    id 1077
    label "dziama&#263;"
  ]
  node [
    id 1078
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1079
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1080
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1081
    label "str&#243;j"
  ]
  node [
    id 1082
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1083
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1084
    label "krok"
  ]
  node [
    id 1085
    label "tryb"
  ]
  node [
    id 1086
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1087
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1088
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1089
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1090
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1091
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1092
    label "Ohio"
  ]
  node [
    id 1093
    label "wci&#281;cie"
  ]
  node [
    id 1094
    label "Nowy_York"
  ]
  node [
    id 1095
    label "warstwa"
  ]
  node [
    id 1096
    label "samopoczucie"
  ]
  node [
    id 1097
    label "Illinois"
  ]
  node [
    id 1098
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1099
    label "state"
  ]
  node [
    id 1100
    label "Jukatan"
  ]
  node [
    id 1101
    label "Kalifornia"
  ]
  node [
    id 1102
    label "Wirginia"
  ]
  node [
    id 1103
    label "wektor"
  ]
  node [
    id 1104
    label "Goa"
  ]
  node [
    id 1105
    label "Teksas"
  ]
  node [
    id 1106
    label "Waszyngton"
  ]
  node [
    id 1107
    label "Massachusetts"
  ]
  node [
    id 1108
    label "Alaska"
  ]
  node [
    id 1109
    label "Arakan"
  ]
  node [
    id 1110
    label "Hawaje"
  ]
  node [
    id 1111
    label "Maryland"
  ]
  node [
    id 1112
    label "punkt"
  ]
  node [
    id 1113
    label "Michigan"
  ]
  node [
    id 1114
    label "Arizona"
  ]
  node [
    id 1115
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1116
    label "Georgia"
  ]
  node [
    id 1117
    label "poziom"
  ]
  node [
    id 1118
    label "Pensylwania"
  ]
  node [
    id 1119
    label "shape"
  ]
  node [
    id 1120
    label "Luizjana"
  ]
  node [
    id 1121
    label "Nowy_Meksyk"
  ]
  node [
    id 1122
    label "Alabama"
  ]
  node [
    id 1123
    label "ilo&#347;&#263;"
  ]
  node [
    id 1124
    label "Kansas"
  ]
  node [
    id 1125
    label "Oregon"
  ]
  node [
    id 1126
    label "Oklahoma"
  ]
  node [
    id 1127
    label "Floryda"
  ]
  node [
    id 1128
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1129
    label "obfituj&#261;cy"
  ]
  node [
    id 1130
    label "nabab"
  ]
  node [
    id 1131
    label "r&#243;&#380;norodny"
  ]
  node [
    id 1132
    label "spania&#322;y"
  ]
  node [
    id 1133
    label "obficie"
  ]
  node [
    id 1134
    label "sytuowany"
  ]
  node [
    id 1135
    label "och&#281;do&#380;ny"
  ]
  node [
    id 1136
    label "forsiasty"
  ]
  node [
    id 1137
    label "zapa&#347;ny"
  ]
  node [
    id 1138
    label "bogato"
  ]
  node [
    id 1139
    label "&#347;wietny"
  ]
  node [
    id 1140
    label "wspania&#322;y"
  ]
  node [
    id 1141
    label "och&#281;do&#380;nie"
  ]
  node [
    id 1142
    label "porz&#261;dny"
  ]
  node [
    id 1143
    label "ch&#281;dogo"
  ]
  node [
    id 1144
    label "smaczny"
  ]
  node [
    id 1145
    label "rezerwowy"
  ]
  node [
    id 1146
    label "zapa&#347;nie"
  ]
  node [
    id 1147
    label "zapasowy"
  ]
  node [
    id 1148
    label "urz&#281;dnik"
  ]
  node [
    id 1149
    label "bogacz"
  ]
  node [
    id 1150
    label "zarz&#261;dca"
  ]
  node [
    id 1151
    label "dostojnik"
  ]
  node [
    id 1152
    label "obfito"
  ]
  node [
    id 1153
    label "obfity"
  ]
  node [
    id 1154
    label "pe&#322;no"
  ]
  node [
    id 1155
    label "r&#243;&#380;norodnie"
  ]
  node [
    id 1156
    label "intensywnie"
  ]
  node [
    id 1157
    label "poka&#378;ny"
  ]
  node [
    id 1158
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 1159
    label "pe&#322;ny"
  ]
  node [
    id 1160
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1161
    label "obdarowa&#263;"
  ]
  node [
    id 1162
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1163
    label "span"
  ]
  node [
    id 1164
    label "involve"
  ]
  node [
    id 1165
    label "roztoczy&#263;"
  ]
  node [
    id 1166
    label "zacz&#261;&#263;"
  ]
  node [
    id 1167
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 1168
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 1169
    label "odj&#261;&#263;"
  ]
  node [
    id 1170
    label "cause"
  ]
  node [
    id 1171
    label "introduce"
  ]
  node [
    id 1172
    label "begin"
  ]
  node [
    id 1173
    label "do"
  ]
  node [
    id 1174
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 1175
    label "circulate"
  ]
  node [
    id 1176
    label "obj&#261;&#263;"
  ]
  node [
    id 1177
    label "przedstawi&#263;"
  ]
  node [
    id 1178
    label "powi&#281;kszy&#263;"
  ]
  node [
    id 1179
    label "rozwin&#261;&#263;"
  ]
  node [
    id 1180
    label "rozprzestrzeni&#263;"
  ]
  node [
    id 1181
    label "udarowa&#263;"
  ]
  node [
    id 1182
    label "bestow"
  ]
  node [
    id 1183
    label "da&#263;"
  ]
  node [
    id 1184
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1185
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1186
    label "zorganizowa&#263;"
  ]
  node [
    id 1187
    label "appoint"
  ]
  node [
    id 1188
    label "wystylizowa&#263;"
  ]
  node [
    id 1189
    label "przerobi&#263;"
  ]
  node [
    id 1190
    label "nabra&#263;"
  ]
  node [
    id 1191
    label "make"
  ]
  node [
    id 1192
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1193
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1194
    label "wydali&#263;"
  ]
  node [
    id 1195
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 1196
    label "honours"
  ]
  node [
    id 1197
    label "pride"
  ]
  node [
    id 1198
    label "hyr"
  ]
  node [
    id 1199
    label "renoma"
  ]
  node [
    id 1200
    label "object"
  ]
  node [
    id 1201
    label "temat"
  ]
  node [
    id 1202
    label "wpadni&#281;cie"
  ]
  node [
    id 1203
    label "mienie"
  ]
  node [
    id 1204
    label "przyroda"
  ]
  node [
    id 1205
    label "kultura"
  ]
  node [
    id 1206
    label "wpa&#347;&#263;"
  ]
  node [
    id 1207
    label "wpadanie"
  ]
  node [
    id 1208
    label "wpada&#263;"
  ]
  node [
    id 1209
    label "prize"
  ]
  node [
    id 1210
    label "trophy"
  ]
  node [
    id 1211
    label "oznaczenie"
  ]
  node [
    id 1212
    label "potraktowanie"
  ]
  node [
    id 1213
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1214
    label "zrobienie"
  ]
  node [
    id 1215
    label "opinia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 285
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 293
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 723
  ]
  edge [
    source 13
    target 724
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 725
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 727
  ]
  edge [
    source 13
    target 728
  ]
  edge [
    source 13
    target 729
  ]
  edge [
    source 13
    target 730
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 13
    target 733
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 735
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 737
  ]
  edge [
    source 13
    target 738
  ]
  edge [
    source 13
    target 739
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 13
    target 742
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 753
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 756
  ]
  edge [
    source 13
    target 757
  ]
  edge [
    source 13
    target 758
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 760
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 762
  ]
  edge [
    source 14
    target 763
  ]
  edge [
    source 14
    target 764
  ]
  edge [
    source 14
    target 765
  ]
  edge [
    source 14
    target 766
  ]
  edge [
    source 14
    target 56
  ]
  edge [
    source 14
    target 767
  ]
  edge [
    source 14
    target 768
  ]
  edge [
    source 14
    target 51
  ]
  edge [
    source 14
    target 769
  ]
  edge [
    source 14
    target 770
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 771
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 773
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 69
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 14
    target 777
  ]
  edge [
    source 14
    target 778
  ]
  edge [
    source 14
    target 779
  ]
  edge [
    source 14
    target 780
  ]
  edge [
    source 14
    target 781
  ]
  edge [
    source 14
    target 782
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 783
  ]
  edge [
    source 14
    target 784
  ]
  edge [
    source 14
    target 785
  ]
  edge [
    source 14
    target 786
  ]
  edge [
    source 14
    target 787
  ]
  edge [
    source 14
    target 788
  ]
  edge [
    source 14
    target 789
  ]
  edge [
    source 14
    target 790
  ]
  edge [
    source 14
    target 432
  ]
  edge [
    source 14
    target 791
  ]
  edge [
    source 14
    target 792
  ]
  edge [
    source 14
    target 793
  ]
  edge [
    source 14
    target 794
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 561
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 705
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 296
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 848
  ]
  edge [
    source 19
    target 849
  ]
  edge [
    source 19
    target 850
  ]
  edge [
    source 19
    target 851
  ]
  edge [
    source 19
    target 852
  ]
  edge [
    source 19
    target 853
  ]
  edge [
    source 19
    target 854
  ]
  edge [
    source 19
    target 855
  ]
  edge [
    source 19
    target 856
  ]
  edge [
    source 19
    target 857
  ]
  edge [
    source 19
    target 858
  ]
  edge [
    source 19
    target 859
  ]
  edge [
    source 19
    target 860
  ]
  edge [
    source 19
    target 861
  ]
  edge [
    source 19
    target 862
  ]
  edge [
    source 19
    target 863
  ]
  edge [
    source 19
    target 864
  ]
  edge [
    source 19
    target 596
  ]
  edge [
    source 19
    target 865
  ]
  edge [
    source 19
    target 866
  ]
  edge [
    source 19
    target 867
  ]
  edge [
    source 19
    target 868
  ]
  edge [
    source 19
    target 869
  ]
  edge [
    source 19
    target 870
  ]
  edge [
    source 19
    target 871
  ]
  edge [
    source 19
    target 872
  ]
  edge [
    source 19
    target 873
  ]
  edge [
    source 19
    target 874
  ]
  edge [
    source 19
    target 875
  ]
  edge [
    source 19
    target 876
  ]
  edge [
    source 19
    target 877
  ]
  edge [
    source 19
    target 878
  ]
  edge [
    source 19
    target 879
  ]
  edge [
    source 19
    target 880
  ]
  edge [
    source 19
    target 881
  ]
  edge [
    source 19
    target 882
  ]
  edge [
    source 19
    target 883
  ]
  edge [
    source 19
    target 884
  ]
  edge [
    source 19
    target 885
  ]
  edge [
    source 19
    target 886
  ]
  edge [
    source 19
    target 887
  ]
  edge [
    source 19
    target 888
  ]
  edge [
    source 19
    target 889
  ]
  edge [
    source 19
    target 890
  ]
  edge [
    source 19
    target 891
  ]
  edge [
    source 19
    target 892
  ]
  edge [
    source 19
    target 893
  ]
  edge [
    source 19
    target 894
  ]
  edge [
    source 19
    target 895
  ]
  edge [
    source 19
    target 896
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 897
  ]
  edge [
    source 21
    target 898
  ]
  edge [
    source 21
    target 899
  ]
  edge [
    source 21
    target 900
  ]
  edge [
    source 21
    target 901
  ]
  edge [
    source 21
    target 902
  ]
  edge [
    source 21
    target 903
  ]
  edge [
    source 21
    target 904
  ]
  edge [
    source 21
    target 278
  ]
  edge [
    source 21
    target 905
  ]
  edge [
    source 21
    target 906
  ]
  edge [
    source 21
    target 907
  ]
  edge [
    source 21
    target 908
  ]
  edge [
    source 21
    target 909
  ]
  edge [
    source 21
    target 910
  ]
  edge [
    source 21
    target 911
  ]
  edge [
    source 21
    target 912
  ]
  edge [
    source 21
    target 913
  ]
  edge [
    source 21
    target 914
  ]
  edge [
    source 21
    target 915
  ]
  edge [
    source 21
    target 916
  ]
  edge [
    source 21
    target 917
  ]
  edge [
    source 21
    target 918
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 921
  ]
  edge [
    source 21
    target 922
  ]
  edge [
    source 21
    target 923
  ]
  edge [
    source 21
    target 924
  ]
  edge [
    source 21
    target 925
  ]
  edge [
    source 21
    target 926
  ]
  edge [
    source 21
    target 927
  ]
  edge [
    source 21
    target 928
  ]
  edge [
    source 21
    target 929
  ]
  edge [
    source 21
    target 930
  ]
  edge [
    source 21
    target 931
  ]
  edge [
    source 21
    target 932
  ]
  edge [
    source 21
    target 933
  ]
  edge [
    source 21
    target 934
  ]
  edge [
    source 21
    target 935
  ]
  edge [
    source 21
    target 936
  ]
  edge [
    source 21
    target 937
  ]
  edge [
    source 21
    target 938
  ]
  edge [
    source 21
    target 939
  ]
  edge [
    source 21
    target 940
  ]
  edge [
    source 21
    target 941
  ]
  edge [
    source 21
    target 942
  ]
  edge [
    source 21
    target 943
  ]
  edge [
    source 21
    target 944
  ]
  edge [
    source 21
    target 945
  ]
  edge [
    source 21
    target 946
  ]
  edge [
    source 21
    target 947
  ]
  edge [
    source 21
    target 948
  ]
  edge [
    source 21
    target 949
  ]
  edge [
    source 21
    target 950
  ]
  edge [
    source 21
    target 951
  ]
  edge [
    source 21
    target 952
  ]
  edge [
    source 21
    target 953
  ]
  edge [
    source 21
    target 954
  ]
  edge [
    source 21
    target 955
  ]
  edge [
    source 21
    target 956
  ]
  edge [
    source 21
    target 957
  ]
  edge [
    source 21
    target 958
  ]
  edge [
    source 21
    target 959
  ]
  edge [
    source 21
    target 960
  ]
  edge [
    source 21
    target 961
  ]
  edge [
    source 21
    target 962
  ]
  edge [
    source 21
    target 963
  ]
  edge [
    source 21
    target 964
  ]
  edge [
    source 21
    target 965
  ]
  edge [
    source 21
    target 966
  ]
  edge [
    source 21
    target 967
  ]
  edge [
    source 21
    target 968
  ]
  edge [
    source 21
    target 969
  ]
  edge [
    source 21
    target 970
  ]
  edge [
    source 21
    target 971
  ]
  edge [
    source 21
    target 972
  ]
  edge [
    source 21
    target 973
  ]
  edge [
    source 21
    target 974
  ]
  edge [
    source 21
    target 975
  ]
  edge [
    source 21
    target 976
  ]
  edge [
    source 21
    target 977
  ]
  edge [
    source 21
    target 978
  ]
  edge [
    source 21
    target 979
  ]
  edge [
    source 21
    target 980
  ]
  edge [
    source 21
    target 981
  ]
  edge [
    source 21
    target 982
  ]
  edge [
    source 21
    target 983
  ]
  edge [
    source 21
    target 984
  ]
  edge [
    source 21
    target 417
  ]
  edge [
    source 21
    target 985
  ]
  edge [
    source 21
    target 265
  ]
  edge [
    source 21
    target 986
  ]
  edge [
    source 21
    target 987
  ]
  edge [
    source 21
    target 988
  ]
  edge [
    source 21
    target 989
  ]
  edge [
    source 21
    target 990
  ]
  edge [
    source 21
    target 991
  ]
  edge [
    source 21
    target 992
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 828
  ]
  edge [
    source 22
    target 829
  ]
  edge [
    source 22
    target 830
  ]
  edge [
    source 22
    target 831
  ]
  edge [
    source 22
    target 832
  ]
  edge [
    source 22
    target 993
  ]
  edge [
    source 22
    target 835
  ]
  edge [
    source 22
    target 836
  ]
  edge [
    source 22
    target 837
  ]
  edge [
    source 22
    target 838
  ]
  edge [
    source 22
    target 994
  ]
  edge [
    source 22
    target 840
  ]
  edge [
    source 22
    target 826
  ]
  edge [
    source 22
    target 842
  ]
  edge [
    source 22
    target 843
  ]
  edge [
    source 22
    target 995
  ]
  edge [
    source 22
    target 996
  ]
  edge [
    source 22
    target 997
  ]
  edge [
    source 22
    target 998
  ]
  edge [
    source 22
    target 999
  ]
  edge [
    source 22
    target 1000
  ]
  edge [
    source 22
    target 1001
  ]
  edge [
    source 22
    target 1002
  ]
  edge [
    source 22
    target 1003
  ]
  edge [
    source 22
    target 1004
  ]
  edge [
    source 22
    target 1005
  ]
  edge [
    source 22
    target 1006
  ]
  edge [
    source 22
    target 1007
  ]
  edge [
    source 22
    target 1008
  ]
  edge [
    source 22
    target 1009
  ]
  edge [
    source 22
    target 1010
  ]
  edge [
    source 22
    target 1011
  ]
  edge [
    source 22
    target 1012
  ]
  edge [
    source 22
    target 1013
  ]
  edge [
    source 22
    target 1014
  ]
  edge [
    source 22
    target 1015
  ]
  edge [
    source 22
    target 1016
  ]
  edge [
    source 22
    target 621
  ]
  edge [
    source 22
    target 537
  ]
  edge [
    source 22
    target 1017
  ]
  edge [
    source 22
    target 230
  ]
  edge [
    source 22
    target 256
  ]
  edge [
    source 22
    target 1018
  ]
  edge [
    source 22
    target 1019
  ]
  edge [
    source 22
    target 1020
  ]
  edge [
    source 22
    target 1021
  ]
  edge [
    source 22
    target 1022
  ]
  edge [
    source 22
    target 81
  ]
  edge [
    source 22
    target 1023
  ]
  edge [
    source 22
    target 58
  ]
  edge [
    source 22
    target 1024
  ]
  edge [
    source 22
    target 37
  ]
  edge [
    source 22
    target 1025
  ]
  edge [
    source 22
    target 32
  ]
  edge [
    source 22
    target 1026
  ]
  edge [
    source 22
    target 82
  ]
  edge [
    source 22
    target 35
  ]
  edge [
    source 22
    target 1027
  ]
  edge [
    source 22
    target 1028
  ]
  edge [
    source 22
    target 1029
  ]
  edge [
    source 22
    target 1030
  ]
  edge [
    source 22
    target 1031
  ]
  edge [
    source 22
    target 1032
  ]
  edge [
    source 22
    target 1033
  ]
  edge [
    source 22
    target 1034
  ]
  edge [
    source 22
    target 1035
  ]
  edge [
    source 22
    target 1036
  ]
  edge [
    source 22
    target 1037
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1038
  ]
  edge [
    source 23
    target 1039
  ]
  edge [
    source 23
    target 1040
  ]
  edge [
    source 23
    target 1041
  ]
  edge [
    source 23
    target 1042
  ]
  edge [
    source 23
    target 1043
  ]
  edge [
    source 23
    target 1000
  ]
  edge [
    source 23
    target 1044
  ]
  edge [
    source 23
    target 1045
  ]
  edge [
    source 23
    target 1046
  ]
  edge [
    source 23
    target 1047
  ]
  edge [
    source 23
    target 1048
  ]
  edge [
    source 23
    target 432
  ]
  edge [
    source 23
    target 1049
  ]
  edge [
    source 23
    target 1050
  ]
  edge [
    source 23
    target 1051
  ]
  edge [
    source 23
    target 1052
  ]
  edge [
    source 23
    target 1053
  ]
  edge [
    source 23
    target 1054
  ]
  edge [
    source 23
    target 1055
  ]
  edge [
    source 23
    target 1056
  ]
  edge [
    source 23
    target 1057
  ]
  edge [
    source 23
    target 1058
  ]
  edge [
    source 23
    target 1059
  ]
  edge [
    source 23
    target 1060
  ]
  edge [
    source 23
    target 1061
  ]
  edge [
    source 23
    target 1062
  ]
  edge [
    source 23
    target 1063
  ]
  edge [
    source 23
    target 1064
  ]
  edge [
    source 23
    target 1065
  ]
  edge [
    source 23
    target 1066
  ]
  edge [
    source 23
    target 442
  ]
  edge [
    source 23
    target 604
  ]
  edge [
    source 23
    target 400
  ]
  edge [
    source 23
    target 1067
  ]
  edge [
    source 23
    target 1068
  ]
  edge [
    source 23
    target 1069
  ]
  edge [
    source 23
    target 1070
  ]
  edge [
    source 23
    target 1071
  ]
  edge [
    source 23
    target 1072
  ]
  edge [
    source 23
    target 80
  ]
  edge [
    source 23
    target 1073
  ]
  edge [
    source 23
    target 1074
  ]
  edge [
    source 23
    target 1075
  ]
  edge [
    source 23
    target 1076
  ]
  edge [
    source 23
    target 1077
  ]
  edge [
    source 23
    target 1078
  ]
  edge [
    source 23
    target 1079
  ]
  edge [
    source 23
    target 686
  ]
  edge [
    source 23
    target 1080
  ]
  edge [
    source 23
    target 1081
  ]
  edge [
    source 23
    target 1082
  ]
  edge [
    source 23
    target 1083
  ]
  edge [
    source 23
    target 1084
  ]
  edge [
    source 23
    target 1085
  ]
  edge [
    source 23
    target 1086
  ]
  edge [
    source 23
    target 1087
  ]
  edge [
    source 23
    target 1088
  ]
  edge [
    source 23
    target 1089
  ]
  edge [
    source 23
    target 1090
  ]
  edge [
    source 23
    target 79
  ]
  edge [
    source 23
    target 1091
  ]
  edge [
    source 23
    target 1092
  ]
  edge [
    source 23
    target 1093
  ]
  edge [
    source 23
    target 1094
  ]
  edge [
    source 23
    target 1095
  ]
  edge [
    source 23
    target 1096
  ]
  edge [
    source 23
    target 1097
  ]
  edge [
    source 23
    target 1098
  ]
  edge [
    source 23
    target 1099
  ]
  edge [
    source 23
    target 1100
  ]
  edge [
    source 23
    target 1101
  ]
  edge [
    source 23
    target 1102
  ]
  edge [
    source 23
    target 1103
  ]
  edge [
    source 23
    target 1104
  ]
  edge [
    source 23
    target 1105
  ]
  edge [
    source 23
    target 1106
  ]
  edge [
    source 23
    target 495
  ]
  edge [
    source 23
    target 1107
  ]
  edge [
    source 23
    target 1108
  ]
  edge [
    source 23
    target 1109
  ]
  edge [
    source 23
    target 1110
  ]
  edge [
    source 23
    target 1111
  ]
  edge [
    source 23
    target 1112
  ]
  edge [
    source 23
    target 1113
  ]
  edge [
    source 23
    target 1114
  ]
  edge [
    source 23
    target 1115
  ]
  edge [
    source 23
    target 1116
  ]
  edge [
    source 23
    target 1117
  ]
  edge [
    source 23
    target 1118
  ]
  edge [
    source 23
    target 1119
  ]
  edge [
    source 23
    target 1120
  ]
  edge [
    source 23
    target 1121
  ]
  edge [
    source 23
    target 1122
  ]
  edge [
    source 23
    target 1123
  ]
  edge [
    source 23
    target 1124
  ]
  edge [
    source 23
    target 1125
  ]
  edge [
    source 23
    target 1126
  ]
  edge [
    source 23
    target 1127
  ]
  edge [
    source 23
    target 554
  ]
  edge [
    source 23
    target 1128
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1129
  ]
  edge [
    source 24
    target 1130
  ]
  edge [
    source 24
    target 359
  ]
  edge [
    source 24
    target 1131
  ]
  edge [
    source 24
    target 1132
  ]
  edge [
    source 24
    target 1133
  ]
  edge [
    source 24
    target 1134
  ]
  edge [
    source 24
    target 1135
  ]
  edge [
    source 24
    target 1136
  ]
  edge [
    source 24
    target 1137
  ]
  edge [
    source 24
    target 1138
  ]
  edge [
    source 24
    target 1139
  ]
  edge [
    source 24
    target 1140
  ]
  edge [
    source 24
    target 1141
  ]
  edge [
    source 24
    target 1142
  ]
  edge [
    source 24
    target 1143
  ]
  edge [
    source 24
    target 1144
  ]
  edge [
    source 24
    target 1145
  ]
  edge [
    source 24
    target 1146
  ]
  edge [
    source 24
    target 1147
  ]
  edge [
    source 24
    target 1148
  ]
  edge [
    source 24
    target 1149
  ]
  edge [
    source 24
    target 1150
  ]
  edge [
    source 24
    target 1151
  ]
  edge [
    source 24
    target 1152
  ]
  edge [
    source 24
    target 1153
  ]
  edge [
    source 24
    target 1154
  ]
  edge [
    source 24
    target 1155
  ]
  edge [
    source 24
    target 1156
  ]
  edge [
    source 24
    target 1157
  ]
  edge [
    source 24
    target 726
  ]
  edge [
    source 24
    target 1158
  ]
  edge [
    source 24
    target 140
  ]
  edge [
    source 24
    target 583
  ]
  edge [
    source 24
    target 584
  ]
  edge [
    source 24
    target 585
  ]
  edge [
    source 24
    target 586
  ]
  edge [
    source 24
    target 148
  ]
  edge [
    source 24
    target 587
  ]
  edge [
    source 24
    target 588
  ]
  edge [
    source 24
    target 589
  ]
  edge [
    source 24
    target 151
  ]
  edge [
    source 24
    target 112
  ]
  edge [
    source 24
    target 152
  ]
  edge [
    source 24
    target 590
  ]
  edge [
    source 24
    target 161
  ]
  edge [
    source 24
    target 164
  ]
  edge [
    source 24
    target 591
  ]
  edge [
    source 24
    target 165
  ]
  edge [
    source 24
    target 166
  ]
  edge [
    source 24
    target 109
  ]
  edge [
    source 24
    target 182
  ]
  edge [
    source 24
    target 592
  ]
  edge [
    source 24
    target 167
  ]
  edge [
    source 24
    target 593
  ]
  edge [
    source 24
    target 172
  ]
  edge [
    source 24
    target 594
  ]
  edge [
    source 24
    target 1159
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1160
  ]
  edge [
    source 25
    target 1161
  ]
  edge [
    source 25
    target 1162
  ]
  edge [
    source 25
    target 1163
  ]
  edge [
    source 25
    target 864
  ]
  edge [
    source 25
    target 889
  ]
  edge [
    source 25
    target 888
  ]
  edge [
    source 25
    target 1164
  ]
  edge [
    source 25
    target 1165
  ]
  edge [
    source 25
    target 1166
  ]
  edge [
    source 25
    target 848
  ]
  edge [
    source 25
    target 1167
  ]
  edge [
    source 25
    target 1168
  ]
  edge [
    source 25
    target 1169
  ]
  edge [
    source 25
    target 1170
  ]
  edge [
    source 25
    target 1171
  ]
  edge [
    source 25
    target 1172
  ]
  edge [
    source 25
    target 1173
  ]
  edge [
    source 25
    target 1174
  ]
  edge [
    source 25
    target 1175
  ]
  edge [
    source 25
    target 1176
  ]
  edge [
    source 25
    target 1177
  ]
  edge [
    source 25
    target 1178
  ]
  edge [
    source 25
    target 1179
  ]
  edge [
    source 25
    target 1180
  ]
  edge [
    source 25
    target 1181
  ]
  edge [
    source 25
    target 1182
  ]
  edge [
    source 25
    target 1183
  ]
  edge [
    source 25
    target 870
  ]
  edge [
    source 25
    target 1184
  ]
  edge [
    source 25
    target 1185
  ]
  edge [
    source 25
    target 1186
  ]
  edge [
    source 25
    target 1187
  ]
  edge [
    source 25
    target 1188
  ]
  edge [
    source 25
    target 1189
  ]
  edge [
    source 25
    target 1190
  ]
  edge [
    source 25
    target 1191
  ]
  edge [
    source 25
    target 1192
  ]
  edge [
    source 25
    target 1193
  ]
  edge [
    source 25
    target 1194
  ]
  edge [
    source 25
    target 596
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1195
  ]
  edge [
    source 26
    target 1196
  ]
  edge [
    source 26
    target 1197
  ]
  edge [
    source 26
    target 1198
  ]
  edge [
    source 26
    target 1199
  ]
  edge [
    source 26
    target 1015
  ]
  edge [
    source 26
    target 1200
  ]
  edge [
    source 26
    target 111
  ]
  edge [
    source 26
    target 1201
  ]
  edge [
    source 26
    target 1202
  ]
  edge [
    source 26
    target 1203
  ]
  edge [
    source 26
    target 1204
  ]
  edge [
    source 26
    target 170
  ]
  edge [
    source 26
    target 417
  ]
  edge [
    source 26
    target 1205
  ]
  edge [
    source 26
    target 1206
  ]
  edge [
    source 26
    target 1207
  ]
  edge [
    source 26
    target 1208
  ]
  edge [
    source 26
    target 1209
  ]
  edge [
    source 26
    target 1210
  ]
  edge [
    source 26
    target 1211
  ]
  edge [
    source 26
    target 1212
  ]
  edge [
    source 26
    target 1035
  ]
  edge [
    source 26
    target 1213
  ]
  edge [
    source 26
    target 1214
  ]
  edge [
    source 26
    target 1215
  ]
]
