graph [
  node [
    id 0
    label "nadej&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ten"
    origin "text"
  ]
  node [
    id 2
    label "dzienia"
    origin "text"
  ]
  node [
    id 3
    label "rozdajo"
    origin "text"
  ]
  node [
    id 4
    label "xboxa"
    origin "text"
  ]
  node [
    id 5
    label "sekunda"
    origin "text"
  ]
  node [
    id 6
    label "line_up"
  ]
  node [
    id 7
    label "sta&#263;_si&#281;"
  ]
  node [
    id 8
    label "catch"
  ]
  node [
    id 9
    label "przyby&#263;"
  ]
  node [
    id 10
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 11
    label "czas"
  ]
  node [
    id 12
    label "become"
  ]
  node [
    id 13
    label "get"
  ]
  node [
    id 14
    label "dotrze&#263;"
  ]
  node [
    id 15
    label "zyska&#263;"
  ]
  node [
    id 16
    label "poprzedzanie"
  ]
  node [
    id 17
    label "czasoprzestrze&#324;"
  ]
  node [
    id 18
    label "laba"
  ]
  node [
    id 19
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 20
    label "chronometria"
  ]
  node [
    id 21
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 22
    label "rachuba_czasu"
  ]
  node [
    id 23
    label "przep&#322;ywanie"
  ]
  node [
    id 24
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 25
    label "czasokres"
  ]
  node [
    id 26
    label "odczyt"
  ]
  node [
    id 27
    label "chwila"
  ]
  node [
    id 28
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 29
    label "dzieje"
  ]
  node [
    id 30
    label "kategoria_gramatyczna"
  ]
  node [
    id 31
    label "poprzedzenie"
  ]
  node [
    id 32
    label "trawienie"
  ]
  node [
    id 33
    label "pochodzi&#263;"
  ]
  node [
    id 34
    label "period"
  ]
  node [
    id 35
    label "okres_czasu"
  ]
  node [
    id 36
    label "poprzedza&#263;"
  ]
  node [
    id 37
    label "schy&#322;ek"
  ]
  node [
    id 38
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 39
    label "odwlekanie_si&#281;"
  ]
  node [
    id 40
    label "zegar"
  ]
  node [
    id 41
    label "czwarty_wymiar"
  ]
  node [
    id 42
    label "pochodzenie"
  ]
  node [
    id 43
    label "koniugacja"
  ]
  node [
    id 44
    label "Zeitgeist"
  ]
  node [
    id 45
    label "trawi&#263;"
  ]
  node [
    id 46
    label "pogoda"
  ]
  node [
    id 47
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 48
    label "poprzedzi&#263;"
  ]
  node [
    id 49
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 50
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 51
    label "time_period"
  ]
  node [
    id 52
    label "okre&#347;lony"
  ]
  node [
    id 53
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 54
    label "wiadomy"
  ]
  node [
    id 55
    label "time"
  ]
  node [
    id 56
    label "mikrosekunda"
  ]
  node [
    id 57
    label "milisekunda"
  ]
  node [
    id 58
    label "jednostka"
  ]
  node [
    id 59
    label "tercja"
  ]
  node [
    id 60
    label "nanosekunda"
  ]
  node [
    id 61
    label "uk&#322;ad_SI"
  ]
  node [
    id 62
    label "jednostka_czasu"
  ]
  node [
    id 63
    label "minuta"
  ]
  node [
    id 64
    label "przyswoi&#263;"
  ]
  node [
    id 65
    label "ludzko&#347;&#263;"
  ]
  node [
    id 66
    label "one"
  ]
  node [
    id 67
    label "poj&#281;cie"
  ]
  node [
    id 68
    label "ewoluowanie"
  ]
  node [
    id 69
    label "supremum"
  ]
  node [
    id 70
    label "skala"
  ]
  node [
    id 71
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 72
    label "przyswajanie"
  ]
  node [
    id 73
    label "wyewoluowanie"
  ]
  node [
    id 74
    label "reakcja"
  ]
  node [
    id 75
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 76
    label "przeliczy&#263;"
  ]
  node [
    id 77
    label "wyewoluowa&#263;"
  ]
  node [
    id 78
    label "ewoluowa&#263;"
  ]
  node [
    id 79
    label "matematyka"
  ]
  node [
    id 80
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 81
    label "rzut"
  ]
  node [
    id 82
    label "liczba_naturalna"
  ]
  node [
    id 83
    label "czynnik_biotyczny"
  ]
  node [
    id 84
    label "g&#322;owa"
  ]
  node [
    id 85
    label "figura"
  ]
  node [
    id 86
    label "individual"
  ]
  node [
    id 87
    label "portrecista"
  ]
  node [
    id 88
    label "obiekt"
  ]
  node [
    id 89
    label "przyswaja&#263;"
  ]
  node [
    id 90
    label "przyswojenie"
  ]
  node [
    id 91
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 92
    label "profanum"
  ]
  node [
    id 93
    label "mikrokosmos"
  ]
  node [
    id 94
    label "starzenie_si&#281;"
  ]
  node [
    id 95
    label "duch"
  ]
  node [
    id 96
    label "przeliczanie"
  ]
  node [
    id 97
    label "osoba"
  ]
  node [
    id 98
    label "oddzia&#322;ywanie"
  ]
  node [
    id 99
    label "antropochoria"
  ]
  node [
    id 100
    label "funkcja"
  ]
  node [
    id 101
    label "homo_sapiens"
  ]
  node [
    id 102
    label "przelicza&#263;"
  ]
  node [
    id 103
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 104
    label "infimum"
  ]
  node [
    id 105
    label "przeliczenie"
  ]
  node [
    id 106
    label "stopie&#324;_pisma"
  ]
  node [
    id 107
    label "pole"
  ]
  node [
    id 108
    label "godzina_kanoniczna"
  ]
  node [
    id 109
    label "hokej"
  ]
  node [
    id 110
    label "hokej_na_lodzie"
  ]
  node [
    id 111
    label "zamek"
  ]
  node [
    id 112
    label "interwa&#322;"
  ]
  node [
    id 113
    label "mecz"
  ]
  node [
    id 114
    label "zapis"
  ]
  node [
    id 115
    label "stopie&#324;"
  ]
  node [
    id 116
    label "godzina"
  ]
  node [
    id 117
    label "design"
  ]
  node [
    id 118
    label "kwadrans"
  ]
  node [
    id 119
    label "Xboxa"
  ]
  node [
    id 120
    label "on"
  ]
  node [
    id 121
    label "planet"
  ]
  node [
    id 122
    label "plus"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 121
    target 122
  ]
]
