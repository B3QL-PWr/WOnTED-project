graph [
  node [
    id 0
    label "god&#322;o"
    origin "text"
  ]
  node [
    id 1
    label "hongkong"
    origin "text"
  ]
  node [
    id 2
    label "kogut_galijski"
  ]
  node [
    id 3
    label "symbol"
  ]
  node [
    id 4
    label "znak_pisarski"
  ]
  node [
    id 5
    label "znak"
  ]
  node [
    id 6
    label "notacja"
  ]
  node [
    id 7
    label "wcielenie"
  ]
  node [
    id 8
    label "brzoza_kr&#243;lewska"
  ]
  node [
    id 9
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 10
    label "character"
  ]
  node [
    id 11
    label "symbolizowanie"
  ]
  node [
    id 12
    label "wielki"
  ]
  node [
    id 13
    label "brytania"
  ]
  node [
    id 14
    label "chi&#324;ski"
  ]
  node [
    id 15
    label "republika"
  ]
  node [
    id 16
    label "ludowy"
  ]
  node [
    id 17
    label "Hongkong"
  ]
  node [
    id 18
    label "specjalny"
  ]
  node [
    id 19
    label "region"
  ]
  node [
    id 20
    label "administracyjny"
  ]
  node [
    id 21
    label "Hong"
  ]
  node [
    id 22
    label "Kongo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
]
