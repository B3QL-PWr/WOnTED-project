graph [
  node [
    id 0
    label "marek"
    origin "text"
  ]
  node [
    id 1
    label "zd&#261;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 2
    label "spier"
    origin "text"
  ]
  node [
    id 3
    label "li&#263;"
    origin "text"
  ]
  node [
    id 4
    label "hygrofit"
  ]
  node [
    id 5
    label "bylina"
  ]
  node [
    id 6
    label "selerowate"
  ]
  node [
    id 7
    label "ludowy"
  ]
  node [
    id 8
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 9
    label "utw&#243;r_epicki"
  ]
  node [
    id 10
    label "pie&#347;&#324;"
  ]
  node [
    id 11
    label "higrofil"
  ]
  node [
    id 12
    label "ro&#347;lina"
  ]
  node [
    id 13
    label "selerowce"
  ]
  node [
    id 14
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 15
    label "spieszy&#263;_si&#281;"
  ]
  node [
    id 16
    label "po&#347;pie&#263;"
  ]
  node [
    id 17
    label "sko&#324;czy&#263;"
  ]
  node [
    id 18
    label "utrzyma&#263;"
  ]
  node [
    id 19
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 20
    label "zrobi&#263;"
  ]
  node [
    id 21
    label "end"
  ]
  node [
    id 22
    label "zako&#324;czy&#263;"
  ]
  node [
    id 23
    label "communicate"
  ]
  node [
    id 24
    label "przesta&#263;"
  ]
  node [
    id 25
    label "obroni&#263;"
  ]
  node [
    id 26
    label "potrzyma&#263;"
  ]
  node [
    id 27
    label "byt"
  ]
  node [
    id 28
    label "op&#322;aci&#263;"
  ]
  node [
    id 29
    label "manewr"
  ]
  node [
    id 30
    label "zdo&#322;a&#263;"
  ]
  node [
    id 31
    label "podtrzyma&#263;"
  ]
  node [
    id 32
    label "feed"
  ]
  node [
    id 33
    label "przetrzyma&#263;"
  ]
  node [
    id 34
    label "foster"
  ]
  node [
    id 35
    label "preserve"
  ]
  node [
    id 36
    label "zapewni&#263;"
  ]
  node [
    id 37
    label "zachowa&#263;"
  ]
  node [
    id 38
    label "unie&#347;&#263;"
  ]
  node [
    id 39
    label "nad&#261;&#380;y&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
]
