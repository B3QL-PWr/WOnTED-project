graph [
  node [
    id 0
    label "podstawa"
    origin "text"
  ]
  node [
    id 1
    label "art"
    origin "text"
  ]
  node [
    id 2
    label "pkt"
    origin "text"
  ]
  node [
    id 3
    label "ustawa"
    origin "text"
  ]
  node [
    id 4
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "kwiecie&#324;"
    origin "text"
  ]
  node [
    id 6
    label "rocznik"
    origin "text"
  ]
  node [
    id 7
    label "narodowy"
    origin "text"
  ]
  node [
    id 8
    label "plan"
    origin "text"
  ]
  node [
    id 9
    label "rozw&#243;j"
    origin "text"
  ]
  node [
    id 10
    label "dziennik"
    origin "text"
  ]
  node [
    id 11
    label "poz"
    origin "text"
  ]
  node [
    id 12
    label "zarz&#261;dza&#263;"
    origin "text"
  ]
  node [
    id 13
    label "si&#281;"
    origin "text"
  ]
  node [
    id 14
    label "nast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "rozporz&#261;dzenie"
    origin "text"
  ]
  node [
    id 16
    label "minister"
    origin "text"
  ]
  node [
    id 17
    label "rolnictwo"
    origin "text"
  ]
  node [
    id 18
    label "wsi"
    origin "text"
  ]
  node [
    id 19
    label "listopad"
    origin "text"
  ]
  node [
    id 20
    label "sprawa"
    origin "text"
  ]
  node [
    id 21
    label "tryb"
    origin "text"
  ]
  node [
    id 22
    label "sk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 23
    label "wz&#243;r"
    origin "text"
  ]
  node [
    id 24
    label "wniosek"
    origin "text"
  ]
  node [
    id 25
    label "dofinansowa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "realizacja"
    origin "text"
  ]
  node [
    id 27
    label "projekt"
    origin "text"
  ]
  node [
    id 28
    label "rama"
    origin "text"
  ]
  node [
    id 29
    label "sektorowy"
    origin "text"
  ]
  node [
    id 30
    label "program"
    origin "text"
  ]
  node [
    id 31
    label "operacyjny"
    origin "text"
  ]
  node [
    id 32
    label "restrukturyzacja"
    origin "text"
  ]
  node [
    id 33
    label "modernizacja"
    origin "text"
  ]
  node [
    id 34
    label "sektor"
    origin "text"
  ]
  node [
    id 35
    label "&#380;ywno&#347;ciowy"
    origin "text"
  ]
  node [
    id 36
    label "obszar"
    origin "text"
  ]
  node [
    id 37
    label "wiejski"
    origin "text"
  ]
  node [
    id 38
    label "zakres"
    origin "text"
  ]
  node [
    id 39
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 40
    label "wsparcie"
    origin "text"
  ]
  node [
    id 41
    label "doradztwo"
    origin "text"
  ]
  node [
    id 42
    label "rolniczy"
    origin "text"
  ]
  node [
    id 43
    label "wprowadza&#263;"
    origin "text"
  ]
  node [
    id 44
    label "nast&#281;puj&#261;cy"
    origin "text"
  ]
  node [
    id 45
    label "zmiana"
    origin "text"
  ]
  node [
    id 46
    label "pot&#281;ga"
  ]
  node [
    id 47
    label "documentation"
  ]
  node [
    id 48
    label "przedmiot"
  ]
  node [
    id 49
    label "column"
  ]
  node [
    id 50
    label "zasadzenie"
  ]
  node [
    id 51
    label "za&#322;o&#380;enie"
  ]
  node [
    id 52
    label "punkt_odniesienia"
  ]
  node [
    id 53
    label "zasadzi&#263;"
  ]
  node [
    id 54
    label "bok"
  ]
  node [
    id 55
    label "d&#243;&#322;"
  ]
  node [
    id 56
    label "dzieci&#281;ctwo"
  ]
  node [
    id 57
    label "background"
  ]
  node [
    id 58
    label "podstawowy"
  ]
  node [
    id 59
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 60
    label "strategia"
  ]
  node [
    id 61
    label "pomys&#322;"
  ]
  node [
    id 62
    label "&#347;ciana"
  ]
  node [
    id 63
    label "podwini&#281;cie"
  ]
  node [
    id 64
    label "zap&#322;acenie"
  ]
  node [
    id 65
    label "przyodzianie"
  ]
  node [
    id 66
    label "budowla"
  ]
  node [
    id 67
    label "pokrycie"
  ]
  node [
    id 68
    label "rozebranie"
  ]
  node [
    id 69
    label "zak&#322;adka"
  ]
  node [
    id 70
    label "struktura"
  ]
  node [
    id 71
    label "poubieranie"
  ]
  node [
    id 72
    label "infliction"
  ]
  node [
    id 73
    label "spowodowanie"
  ]
  node [
    id 74
    label "pozak&#322;adanie"
  ]
  node [
    id 75
    label "przebranie"
  ]
  node [
    id 76
    label "przywdzianie"
  ]
  node [
    id 77
    label "obleczenie_si&#281;"
  ]
  node [
    id 78
    label "utworzenie"
  ]
  node [
    id 79
    label "str&#243;j"
  ]
  node [
    id 80
    label "twierdzenie"
  ]
  node [
    id 81
    label "obleczenie"
  ]
  node [
    id 82
    label "umieszczenie"
  ]
  node [
    id 83
    label "czynno&#347;&#263;"
  ]
  node [
    id 84
    label "przygotowywanie"
  ]
  node [
    id 85
    label "przymierzenie"
  ]
  node [
    id 86
    label "wyko&#324;czenie"
  ]
  node [
    id 87
    label "point"
  ]
  node [
    id 88
    label "przygotowanie"
  ]
  node [
    id 89
    label "proposition"
  ]
  node [
    id 90
    label "przewidzenie"
  ]
  node [
    id 91
    label "zrobienie"
  ]
  node [
    id 92
    label "tu&#322;&#243;w"
  ]
  node [
    id 93
    label "kierunek"
  ]
  node [
    id 94
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 95
    label "wielok&#261;t"
  ]
  node [
    id 96
    label "odcinek"
  ]
  node [
    id 97
    label "strzelba"
  ]
  node [
    id 98
    label "lufa"
  ]
  node [
    id 99
    label "strona"
  ]
  node [
    id 100
    label "profil"
  ]
  node [
    id 101
    label "zbocze"
  ]
  node [
    id 102
    label "kszta&#322;t"
  ]
  node [
    id 103
    label "p&#322;aszczyzna"
  ]
  node [
    id 104
    label "przegroda"
  ]
  node [
    id 105
    label "bariera"
  ]
  node [
    id 106
    label "kres"
  ]
  node [
    id 107
    label "facebook"
  ]
  node [
    id 108
    label "wielo&#347;cian"
  ]
  node [
    id 109
    label "obstruction"
  ]
  node [
    id 110
    label "pow&#322;oka"
  ]
  node [
    id 111
    label "wyrobisko"
  ]
  node [
    id 112
    label "miejsce"
  ]
  node [
    id 113
    label "trudno&#347;&#263;"
  ]
  node [
    id 114
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 115
    label "zboczenie"
  ]
  node [
    id 116
    label "om&#243;wienie"
  ]
  node [
    id 117
    label "sponiewieranie"
  ]
  node [
    id 118
    label "discipline"
  ]
  node [
    id 119
    label "rzecz"
  ]
  node [
    id 120
    label "omawia&#263;"
  ]
  node [
    id 121
    label "kr&#261;&#380;enie"
  ]
  node [
    id 122
    label "tre&#347;&#263;"
  ]
  node [
    id 123
    label "robienie"
  ]
  node [
    id 124
    label "sponiewiera&#263;"
  ]
  node [
    id 125
    label "element"
  ]
  node [
    id 126
    label "entity"
  ]
  node [
    id 127
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 128
    label "tematyka"
  ]
  node [
    id 129
    label "w&#261;tek"
  ]
  node [
    id 130
    label "charakter"
  ]
  node [
    id 131
    label "zbaczanie"
  ]
  node [
    id 132
    label "program_nauczania"
  ]
  node [
    id 133
    label "om&#243;wi&#263;"
  ]
  node [
    id 134
    label "omawianie"
  ]
  node [
    id 135
    label "thing"
  ]
  node [
    id 136
    label "kultura"
  ]
  node [
    id 137
    label "istota"
  ]
  node [
    id 138
    label "zbacza&#263;"
  ]
  node [
    id 139
    label "zboczy&#263;"
  ]
  node [
    id 140
    label "wykopywa&#263;"
  ]
  node [
    id 141
    label "wykopanie"
  ]
  node [
    id 142
    label "&#347;piew"
  ]
  node [
    id 143
    label "wykopywanie"
  ]
  node [
    id 144
    label "hole"
  ]
  node [
    id 145
    label "low"
  ]
  node [
    id 146
    label "niski"
  ]
  node [
    id 147
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 148
    label "depressive_disorder"
  ]
  node [
    id 149
    label "d&#378;wi&#281;k"
  ]
  node [
    id 150
    label "wykopa&#263;"
  ]
  node [
    id 151
    label "za&#322;amanie"
  ]
  node [
    id 152
    label "niezaawansowany"
  ]
  node [
    id 153
    label "najwa&#380;niejszy"
  ]
  node [
    id 154
    label "pocz&#261;tkowy"
  ]
  node [
    id 155
    label "podstawowo"
  ]
  node [
    id 156
    label "wetkni&#281;cie"
  ]
  node [
    id 157
    label "przetkanie"
  ]
  node [
    id 158
    label "anchor"
  ]
  node [
    id 159
    label "przymocowanie"
  ]
  node [
    id 160
    label "zaczerpni&#281;cie"
  ]
  node [
    id 161
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 162
    label "interposition"
  ]
  node [
    id 163
    label "odm&#322;odzenie"
  ]
  node [
    id 164
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 165
    label "establish"
  ]
  node [
    id 166
    label "plant"
  ]
  node [
    id 167
    label "osnowa&#263;"
  ]
  node [
    id 168
    label "przymocowa&#263;"
  ]
  node [
    id 169
    label "umie&#347;ci&#263;"
  ]
  node [
    id 170
    label "wetkn&#261;&#263;"
  ]
  node [
    id 171
    label "dzieci&#324;stwo"
  ]
  node [
    id 172
    label "pocz&#261;tki"
  ]
  node [
    id 173
    label "pochodzenie"
  ]
  node [
    id 174
    label "kontekst"
  ]
  node [
    id 175
    label "idea"
  ]
  node [
    id 176
    label "wytw&#243;r"
  ]
  node [
    id 177
    label "ukradzenie"
  ]
  node [
    id 178
    label "ukra&#347;&#263;"
  ]
  node [
    id 179
    label "system"
  ]
  node [
    id 180
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 181
    label "operacja"
  ]
  node [
    id 182
    label "metoda"
  ]
  node [
    id 183
    label "gra"
  ]
  node [
    id 184
    label "wzorzec_projektowy"
  ]
  node [
    id 185
    label "dziedzina"
  ]
  node [
    id 186
    label "doktryna"
  ]
  node [
    id 187
    label "wrinkle"
  ]
  node [
    id 188
    label "dokument"
  ]
  node [
    id 189
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 190
    label "wojsko"
  ]
  node [
    id 191
    label "wyk&#322;adnik_pot&#281;gi"
  ]
  node [
    id 192
    label "organizacja"
  ]
  node [
    id 193
    label "violence"
  ]
  node [
    id 194
    label "podstawa_pot&#281;gi"
  ]
  node [
    id 195
    label "zdolno&#347;&#263;"
  ]
  node [
    id 196
    label "potencja"
  ]
  node [
    id 197
    label "iloczyn"
  ]
  node [
    id 198
    label "Karta_Nauczyciela"
  ]
  node [
    id 199
    label "przej&#347;cie"
  ]
  node [
    id 200
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 201
    label "akt"
  ]
  node [
    id 202
    label "przej&#347;&#263;"
  ]
  node [
    id 203
    label "charter"
  ]
  node [
    id 204
    label "marc&#243;wka"
  ]
  node [
    id 205
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 206
    label "podnieci&#263;"
  ]
  node [
    id 207
    label "scena"
  ]
  node [
    id 208
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 209
    label "numer"
  ]
  node [
    id 210
    label "po&#380;ycie"
  ]
  node [
    id 211
    label "poj&#281;cie"
  ]
  node [
    id 212
    label "podniecenie"
  ]
  node [
    id 213
    label "nago&#347;&#263;"
  ]
  node [
    id 214
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 215
    label "fascyku&#322;"
  ]
  node [
    id 216
    label "seks"
  ]
  node [
    id 217
    label "podniecanie"
  ]
  node [
    id 218
    label "imisja"
  ]
  node [
    id 219
    label "zwyczaj"
  ]
  node [
    id 220
    label "rozmna&#380;anie"
  ]
  node [
    id 221
    label "ruch_frykcyjny"
  ]
  node [
    id 222
    label "ontologia"
  ]
  node [
    id 223
    label "wydarzenie"
  ]
  node [
    id 224
    label "na_pieska"
  ]
  node [
    id 225
    label "pozycja_misjonarska"
  ]
  node [
    id 226
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 227
    label "fragment"
  ]
  node [
    id 228
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 229
    label "z&#322;&#261;czenie"
  ]
  node [
    id 230
    label "gra_wst&#281;pna"
  ]
  node [
    id 231
    label "erotyka"
  ]
  node [
    id 232
    label "urzeczywistnienie"
  ]
  node [
    id 233
    label "baraszki"
  ]
  node [
    id 234
    label "certificate"
  ]
  node [
    id 235
    label "po&#380;&#261;danie"
  ]
  node [
    id 236
    label "wzw&#243;d"
  ]
  node [
    id 237
    label "funkcja"
  ]
  node [
    id 238
    label "act"
  ]
  node [
    id 239
    label "arystotelizm"
  ]
  node [
    id 240
    label "podnieca&#263;"
  ]
  node [
    id 241
    label "zabory"
  ]
  node [
    id 242
    label "ci&#281;&#380;arna"
  ]
  node [
    id 243
    label "rozwi&#261;zanie"
  ]
  node [
    id 244
    label "mini&#281;cie"
  ]
  node [
    id 245
    label "wymienienie"
  ]
  node [
    id 246
    label "zaliczenie"
  ]
  node [
    id 247
    label "traversal"
  ]
  node [
    id 248
    label "zdarzenie_si&#281;"
  ]
  node [
    id 249
    label "przewy&#380;szenie"
  ]
  node [
    id 250
    label "experience"
  ]
  node [
    id 251
    label "przepuszczenie"
  ]
  node [
    id 252
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 253
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 254
    label "strain"
  ]
  node [
    id 255
    label "faza"
  ]
  node [
    id 256
    label "przerobienie"
  ]
  node [
    id 257
    label "wydeptywanie"
  ]
  node [
    id 258
    label "crack"
  ]
  node [
    id 259
    label "wydeptanie"
  ]
  node [
    id 260
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 261
    label "wstawka"
  ]
  node [
    id 262
    label "prze&#380;ycie"
  ]
  node [
    id 263
    label "uznanie"
  ]
  node [
    id 264
    label "doznanie"
  ]
  node [
    id 265
    label "dostanie_si&#281;"
  ]
  node [
    id 266
    label "trwanie"
  ]
  node [
    id 267
    label "przebycie"
  ]
  node [
    id 268
    label "wytyczenie"
  ]
  node [
    id 269
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 270
    label "przepojenie"
  ]
  node [
    id 271
    label "nas&#261;czenie"
  ]
  node [
    id 272
    label "nale&#380;enie"
  ]
  node [
    id 273
    label "mienie"
  ]
  node [
    id 274
    label "odmienienie"
  ]
  node [
    id 275
    label "przedostanie_si&#281;"
  ]
  node [
    id 276
    label "przemokni&#281;cie"
  ]
  node [
    id 277
    label "nasycenie_si&#281;"
  ]
  node [
    id 278
    label "zacz&#281;cie"
  ]
  node [
    id 279
    label "stanie_si&#281;"
  ]
  node [
    id 280
    label "offense"
  ]
  node [
    id 281
    label "przestanie"
  ]
  node [
    id 282
    label "podlec"
  ]
  node [
    id 283
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 284
    label "min&#261;&#263;"
  ]
  node [
    id 285
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 286
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 287
    label "zaliczy&#263;"
  ]
  node [
    id 288
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 289
    label "zmieni&#263;"
  ]
  node [
    id 290
    label "przeby&#263;"
  ]
  node [
    id 291
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 292
    label "die"
  ]
  node [
    id 293
    label "dozna&#263;"
  ]
  node [
    id 294
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 295
    label "zacz&#261;&#263;"
  ]
  node [
    id 296
    label "happen"
  ]
  node [
    id 297
    label "pass"
  ]
  node [
    id 298
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 299
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 300
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 301
    label "beat"
  ]
  node [
    id 302
    label "absorb"
  ]
  node [
    id 303
    label "przerobi&#263;"
  ]
  node [
    id 304
    label "pique"
  ]
  node [
    id 305
    label "przesta&#263;"
  ]
  node [
    id 306
    label "odnaj&#281;cie"
  ]
  node [
    id 307
    label "naj&#281;cie"
  ]
  node [
    id 308
    label "ranek"
  ]
  node [
    id 309
    label "doba"
  ]
  node [
    id 310
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 311
    label "noc"
  ]
  node [
    id 312
    label "podwiecz&#243;r"
  ]
  node [
    id 313
    label "po&#322;udnie"
  ]
  node [
    id 314
    label "godzina"
  ]
  node [
    id 315
    label "przedpo&#322;udnie"
  ]
  node [
    id 316
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 317
    label "long_time"
  ]
  node [
    id 318
    label "wiecz&#243;r"
  ]
  node [
    id 319
    label "t&#322;usty_czwartek"
  ]
  node [
    id 320
    label "popo&#322;udnie"
  ]
  node [
    id 321
    label "walentynki"
  ]
  node [
    id 322
    label "czynienie_si&#281;"
  ]
  node [
    id 323
    label "s&#322;o&#324;ce"
  ]
  node [
    id 324
    label "rano"
  ]
  node [
    id 325
    label "tydzie&#324;"
  ]
  node [
    id 326
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 327
    label "wzej&#347;cie"
  ]
  node [
    id 328
    label "czas"
  ]
  node [
    id 329
    label "wsta&#263;"
  ]
  node [
    id 330
    label "day"
  ]
  node [
    id 331
    label "termin"
  ]
  node [
    id 332
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 333
    label "wstanie"
  ]
  node [
    id 334
    label "przedwiecz&#243;r"
  ]
  node [
    id 335
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 336
    label "Sylwester"
  ]
  node [
    id 337
    label "poprzedzanie"
  ]
  node [
    id 338
    label "czasoprzestrze&#324;"
  ]
  node [
    id 339
    label "laba"
  ]
  node [
    id 340
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 341
    label "chronometria"
  ]
  node [
    id 342
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 343
    label "rachuba_czasu"
  ]
  node [
    id 344
    label "przep&#322;ywanie"
  ]
  node [
    id 345
    label "czasokres"
  ]
  node [
    id 346
    label "odczyt"
  ]
  node [
    id 347
    label "chwila"
  ]
  node [
    id 348
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 349
    label "dzieje"
  ]
  node [
    id 350
    label "kategoria_gramatyczna"
  ]
  node [
    id 351
    label "poprzedzenie"
  ]
  node [
    id 352
    label "trawienie"
  ]
  node [
    id 353
    label "pochodzi&#263;"
  ]
  node [
    id 354
    label "period"
  ]
  node [
    id 355
    label "okres_czasu"
  ]
  node [
    id 356
    label "poprzedza&#263;"
  ]
  node [
    id 357
    label "schy&#322;ek"
  ]
  node [
    id 358
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 359
    label "odwlekanie_si&#281;"
  ]
  node [
    id 360
    label "zegar"
  ]
  node [
    id 361
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 362
    label "czwarty_wymiar"
  ]
  node [
    id 363
    label "koniugacja"
  ]
  node [
    id 364
    label "Zeitgeist"
  ]
  node [
    id 365
    label "trawi&#263;"
  ]
  node [
    id 366
    label "pogoda"
  ]
  node [
    id 367
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 368
    label "poprzedzi&#263;"
  ]
  node [
    id 369
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 370
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 371
    label "time_period"
  ]
  node [
    id 372
    label "nazewnictwo"
  ]
  node [
    id 373
    label "term"
  ]
  node [
    id 374
    label "przypadni&#281;cie"
  ]
  node [
    id 375
    label "ekspiracja"
  ]
  node [
    id 376
    label "przypa&#347;&#263;"
  ]
  node [
    id 377
    label "chronogram"
  ]
  node [
    id 378
    label "praktyka"
  ]
  node [
    id 379
    label "nazwa"
  ]
  node [
    id 380
    label "odwieczerz"
  ]
  node [
    id 381
    label "pora"
  ]
  node [
    id 382
    label "przyj&#281;cie"
  ]
  node [
    id 383
    label "spotkanie"
  ]
  node [
    id 384
    label "night"
  ]
  node [
    id 385
    label "zach&#243;d"
  ]
  node [
    id 386
    label "vesper"
  ]
  node [
    id 387
    label "aurora"
  ]
  node [
    id 388
    label "wsch&#243;d"
  ]
  node [
    id 389
    label "zjawisko"
  ]
  node [
    id 390
    label "&#347;rodek"
  ]
  node [
    id 391
    label "Ziemia"
  ]
  node [
    id 392
    label "dwunasta"
  ]
  node [
    id 393
    label "strona_&#347;wiata"
  ]
  node [
    id 394
    label "dopo&#322;udnie"
  ]
  node [
    id 395
    label "blady_&#347;wit"
  ]
  node [
    id 396
    label "podkurek"
  ]
  node [
    id 397
    label "time"
  ]
  node [
    id 398
    label "p&#243;&#322;godzina"
  ]
  node [
    id 399
    label "jednostka_czasu"
  ]
  node [
    id 400
    label "minuta"
  ]
  node [
    id 401
    label "kwadrans"
  ]
  node [
    id 402
    label "p&#243;&#322;noc"
  ]
  node [
    id 403
    label "nokturn"
  ]
  node [
    id 404
    label "jednostka_geologiczna"
  ]
  node [
    id 405
    label "weekend"
  ]
  node [
    id 406
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 407
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 408
    label "miesi&#261;c"
  ]
  node [
    id 409
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 410
    label "mount"
  ]
  node [
    id 411
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 412
    label "wzej&#347;&#263;"
  ]
  node [
    id 413
    label "ascend"
  ]
  node [
    id 414
    label "kuca&#263;"
  ]
  node [
    id 415
    label "wyzdrowie&#263;"
  ]
  node [
    id 416
    label "opu&#347;ci&#263;"
  ]
  node [
    id 417
    label "rise"
  ]
  node [
    id 418
    label "arise"
  ]
  node [
    id 419
    label "stan&#261;&#263;"
  ]
  node [
    id 420
    label "wyzdrowienie"
  ]
  node [
    id 421
    label "le&#380;enie"
  ]
  node [
    id 422
    label "kl&#281;czenie"
  ]
  node [
    id 423
    label "opuszczenie"
  ]
  node [
    id 424
    label "uniesienie_si&#281;"
  ]
  node [
    id 425
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 426
    label "siedzenie"
  ]
  node [
    id 427
    label "beginning"
  ]
  node [
    id 428
    label "S&#322;o&#324;ce"
  ]
  node [
    id 429
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 430
    label "&#347;wiat&#322;o"
  ]
  node [
    id 431
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 432
    label "kochanie"
  ]
  node [
    id 433
    label "sunlight"
  ]
  node [
    id 434
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 435
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 436
    label "grudzie&#324;"
  ]
  node [
    id 437
    label "luty"
  ]
  node [
    id 438
    label "miech"
  ]
  node [
    id 439
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 440
    label "rok"
  ]
  node [
    id 441
    label "kalendy"
  ]
  node [
    id 442
    label "formacja"
  ]
  node [
    id 443
    label "yearbook"
  ]
  node [
    id 444
    label "czasopismo"
  ]
  node [
    id 445
    label "kronika"
  ]
  node [
    id 446
    label "Bund"
  ]
  node [
    id 447
    label "Mazowsze"
  ]
  node [
    id 448
    label "PPR"
  ]
  node [
    id 449
    label "Jakobici"
  ]
  node [
    id 450
    label "zesp&#243;&#322;"
  ]
  node [
    id 451
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 452
    label "leksem"
  ]
  node [
    id 453
    label "SLD"
  ]
  node [
    id 454
    label "zespolik"
  ]
  node [
    id 455
    label "Razem"
  ]
  node [
    id 456
    label "PiS"
  ]
  node [
    id 457
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 458
    label "partia"
  ]
  node [
    id 459
    label "Kuomintang"
  ]
  node [
    id 460
    label "ZSL"
  ]
  node [
    id 461
    label "szko&#322;a"
  ]
  node [
    id 462
    label "jednostka"
  ]
  node [
    id 463
    label "proces"
  ]
  node [
    id 464
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 465
    label "rugby"
  ]
  node [
    id 466
    label "AWS"
  ]
  node [
    id 467
    label "posta&#263;"
  ]
  node [
    id 468
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 469
    label "blok"
  ]
  node [
    id 470
    label "PO"
  ]
  node [
    id 471
    label "si&#322;a"
  ]
  node [
    id 472
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 473
    label "Federali&#347;ci"
  ]
  node [
    id 474
    label "PSL"
  ]
  node [
    id 475
    label "Wigowie"
  ]
  node [
    id 476
    label "ZChN"
  ]
  node [
    id 477
    label "egzekutywa"
  ]
  node [
    id 478
    label "The_Beatles"
  ]
  node [
    id 479
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 480
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 481
    label "unit"
  ]
  node [
    id 482
    label "Depeche_Mode"
  ]
  node [
    id 483
    label "forma"
  ]
  node [
    id 484
    label "zapis"
  ]
  node [
    id 485
    label "chronograf"
  ]
  node [
    id 486
    label "latopis"
  ]
  node [
    id 487
    label "ksi&#281;ga"
  ]
  node [
    id 488
    label "egzemplarz"
  ]
  node [
    id 489
    label "psychotest"
  ]
  node [
    id 490
    label "pismo"
  ]
  node [
    id 491
    label "communication"
  ]
  node [
    id 492
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 493
    label "wk&#322;ad"
  ]
  node [
    id 494
    label "zajawka"
  ]
  node [
    id 495
    label "ok&#322;adka"
  ]
  node [
    id 496
    label "Zwrotnica"
  ]
  node [
    id 497
    label "dzia&#322;"
  ]
  node [
    id 498
    label "prasa"
  ]
  node [
    id 499
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 500
    label "nacjonalistyczny"
  ]
  node [
    id 501
    label "narodowo"
  ]
  node [
    id 502
    label "wa&#380;ny"
  ]
  node [
    id 503
    label "wynios&#322;y"
  ]
  node [
    id 504
    label "dono&#347;ny"
  ]
  node [
    id 505
    label "silny"
  ]
  node [
    id 506
    label "wa&#380;nie"
  ]
  node [
    id 507
    label "istotnie"
  ]
  node [
    id 508
    label "znaczny"
  ]
  node [
    id 509
    label "eksponowany"
  ]
  node [
    id 510
    label "dobry"
  ]
  node [
    id 511
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 512
    label "nale&#380;ny"
  ]
  node [
    id 513
    label "nale&#380;yty"
  ]
  node [
    id 514
    label "typowy"
  ]
  node [
    id 515
    label "uprawniony"
  ]
  node [
    id 516
    label "zasadniczy"
  ]
  node [
    id 517
    label "stosownie"
  ]
  node [
    id 518
    label "taki"
  ]
  node [
    id 519
    label "charakterystyczny"
  ]
  node [
    id 520
    label "prawdziwy"
  ]
  node [
    id 521
    label "ten"
  ]
  node [
    id 522
    label "polityczny"
  ]
  node [
    id 523
    label "nacjonalistycznie"
  ]
  node [
    id 524
    label "narodowo&#347;ciowy"
  ]
  node [
    id 525
    label "model"
  ]
  node [
    id 526
    label "intencja"
  ]
  node [
    id 527
    label "punkt"
  ]
  node [
    id 528
    label "rysunek"
  ]
  node [
    id 529
    label "miejsce_pracy"
  ]
  node [
    id 530
    label "przestrze&#324;"
  ]
  node [
    id 531
    label "device"
  ]
  node [
    id 532
    label "obraz"
  ]
  node [
    id 533
    label "reprezentacja"
  ]
  node [
    id 534
    label "agreement"
  ]
  node [
    id 535
    label "dekoracja"
  ]
  node [
    id 536
    label "perspektywa"
  ]
  node [
    id 537
    label "dru&#380;yna"
  ]
  node [
    id 538
    label "emblemat"
  ]
  node [
    id 539
    label "deputation"
  ]
  node [
    id 540
    label "kreska"
  ]
  node [
    id 541
    label "picture"
  ]
  node [
    id 542
    label "teka"
  ]
  node [
    id 543
    label "photograph"
  ]
  node [
    id 544
    label "ilustracja"
  ]
  node [
    id 545
    label "grafika"
  ]
  node [
    id 546
    label "plastyka"
  ]
  node [
    id 547
    label "shape"
  ]
  node [
    id 548
    label "spos&#243;b"
  ]
  node [
    id 549
    label "cz&#322;owiek"
  ]
  node [
    id 550
    label "prezenter"
  ]
  node [
    id 551
    label "typ"
  ]
  node [
    id 552
    label "mildew"
  ]
  node [
    id 553
    label "zi&#243;&#322;ko"
  ]
  node [
    id 554
    label "motif"
  ]
  node [
    id 555
    label "pozowanie"
  ]
  node [
    id 556
    label "ideal"
  ]
  node [
    id 557
    label "matryca"
  ]
  node [
    id 558
    label "adaptation"
  ]
  node [
    id 559
    label "ruch"
  ]
  node [
    id 560
    label "pozowa&#263;"
  ]
  node [
    id 561
    label "imitacja"
  ]
  node [
    id 562
    label "orygina&#322;"
  ]
  node [
    id 563
    label "facet"
  ]
  node [
    id 564
    label "miniatura"
  ]
  node [
    id 565
    label "rozdzielanie"
  ]
  node [
    id 566
    label "bezbrze&#380;e"
  ]
  node [
    id 567
    label "zbi&#243;r"
  ]
  node [
    id 568
    label "niezmierzony"
  ]
  node [
    id 569
    label "przedzielenie"
  ]
  node [
    id 570
    label "nielito&#347;ciwy"
  ]
  node [
    id 571
    label "rozdziela&#263;"
  ]
  node [
    id 572
    label "oktant"
  ]
  node [
    id 573
    label "przedzieli&#263;"
  ]
  node [
    id 574
    label "przestw&#243;r"
  ]
  node [
    id 575
    label "representation"
  ]
  node [
    id 576
    label "effigy"
  ]
  node [
    id 577
    label "podobrazie"
  ]
  node [
    id 578
    label "human_body"
  ]
  node [
    id 579
    label "projekcja"
  ]
  node [
    id 580
    label "oprawia&#263;"
  ]
  node [
    id 581
    label "postprodukcja"
  ]
  node [
    id 582
    label "t&#322;o"
  ]
  node [
    id 583
    label "inning"
  ]
  node [
    id 584
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 585
    label "pulment"
  ]
  node [
    id 586
    label "pogl&#261;d"
  ]
  node [
    id 587
    label "plama_barwna"
  ]
  node [
    id 588
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 589
    label "oprawianie"
  ]
  node [
    id 590
    label "sztafa&#380;"
  ]
  node [
    id 591
    label "parkiet"
  ]
  node [
    id 592
    label "opinion"
  ]
  node [
    id 593
    label "uj&#281;cie"
  ]
  node [
    id 594
    label "zaj&#347;cie"
  ]
  node [
    id 595
    label "persona"
  ]
  node [
    id 596
    label "filmoteka"
  ]
  node [
    id 597
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 598
    label "ziarno"
  ]
  node [
    id 599
    label "wypunktowa&#263;"
  ]
  node [
    id 600
    label "ostro&#347;&#263;"
  ]
  node [
    id 601
    label "malarz"
  ]
  node [
    id 602
    label "napisy"
  ]
  node [
    id 603
    label "przeplot"
  ]
  node [
    id 604
    label "punktowa&#263;"
  ]
  node [
    id 605
    label "anamorfoza"
  ]
  node [
    id 606
    label "przedstawienie"
  ]
  node [
    id 607
    label "ty&#322;&#243;wka"
  ]
  node [
    id 608
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 609
    label "widok"
  ]
  node [
    id 610
    label "czo&#322;&#243;wka"
  ]
  node [
    id 611
    label "rola"
  ]
  node [
    id 612
    label "thinking"
  ]
  node [
    id 613
    label "p&#322;&#243;d"
  ]
  node [
    id 614
    label "work"
  ]
  node [
    id 615
    label "rezultat"
  ]
  node [
    id 616
    label "patrzenie"
  ]
  node [
    id 617
    label "figura_geometryczna"
  ]
  node [
    id 618
    label "dystans"
  ]
  node [
    id 619
    label "patrze&#263;"
  ]
  node [
    id 620
    label "decentracja"
  ]
  node [
    id 621
    label "anticipation"
  ]
  node [
    id 622
    label "krajobraz"
  ]
  node [
    id 623
    label "expectation"
  ]
  node [
    id 624
    label "scene"
  ]
  node [
    id 625
    label "pojmowanie"
  ]
  node [
    id 626
    label "widzie&#263;"
  ]
  node [
    id 627
    label "prognoza"
  ]
  node [
    id 628
    label "przestrzenno&#347;&#263;"
  ]
  node [
    id 629
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 630
    label "ferm"
  ]
  node [
    id 631
    label "upi&#281;kszanie"
  ]
  node [
    id 632
    label "adornment"
  ]
  node [
    id 633
    label "pi&#281;kniejszy"
  ]
  node [
    id 634
    label "sznurownia"
  ]
  node [
    id 635
    label "plan_zdj&#281;ciowy"
  ]
  node [
    id 636
    label "scenografia"
  ]
  node [
    id 637
    label "wystr&#243;j"
  ]
  node [
    id 638
    label "ozdoba"
  ]
  node [
    id 639
    label "po&#322;o&#380;enie"
  ]
  node [
    id 640
    label "ust&#281;p"
  ]
  node [
    id 641
    label "obiekt_matematyczny"
  ]
  node [
    id 642
    label "problemat"
  ]
  node [
    id 643
    label "plamka"
  ]
  node [
    id 644
    label "stopie&#324;_pisma"
  ]
  node [
    id 645
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 646
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 647
    label "mark"
  ]
  node [
    id 648
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 649
    label "prosta"
  ]
  node [
    id 650
    label "problematyka"
  ]
  node [
    id 651
    label "obiekt"
  ]
  node [
    id 652
    label "zapunktowa&#263;"
  ]
  node [
    id 653
    label "podpunkt"
  ]
  node [
    id 654
    label "pozycja"
  ]
  node [
    id 655
    label "procedura"
  ]
  node [
    id 656
    label "&#380;ycie"
  ]
  node [
    id 657
    label "proces_biologiczny"
  ]
  node [
    id 658
    label "z&#322;ote_czasy"
  ]
  node [
    id 659
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 660
    label "process"
  ]
  node [
    id 661
    label "cycle"
  ]
  node [
    id 662
    label "kognicja"
  ]
  node [
    id 663
    label "przebieg"
  ]
  node [
    id 664
    label "rozprawa"
  ]
  node [
    id 665
    label "legislacyjnie"
  ]
  node [
    id 666
    label "przes&#322;anka"
  ]
  node [
    id 667
    label "nast&#281;pstwo"
  ]
  node [
    id 668
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 669
    label "raj_utracony"
  ]
  node [
    id 670
    label "umieranie"
  ]
  node [
    id 671
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 672
    label "prze&#380;ywanie"
  ]
  node [
    id 673
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 674
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 675
    label "po&#322;&#243;g"
  ]
  node [
    id 676
    label "umarcie"
  ]
  node [
    id 677
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 678
    label "subsistence"
  ]
  node [
    id 679
    label "power"
  ]
  node [
    id 680
    label "okres_noworodkowy"
  ]
  node [
    id 681
    label "wiek_matuzalemowy"
  ]
  node [
    id 682
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 683
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 684
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 685
    label "do&#380;ywanie"
  ]
  node [
    id 686
    label "byt"
  ]
  node [
    id 687
    label "andropauza"
  ]
  node [
    id 688
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 689
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 690
    label "menopauza"
  ]
  node [
    id 691
    label "&#347;mier&#263;"
  ]
  node [
    id 692
    label "koleje_losu"
  ]
  node [
    id 693
    label "bycie"
  ]
  node [
    id 694
    label "zegar_biologiczny"
  ]
  node [
    id 695
    label "szwung"
  ]
  node [
    id 696
    label "przebywanie"
  ]
  node [
    id 697
    label "warunki"
  ]
  node [
    id 698
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 699
    label "niemowl&#281;ctwo"
  ]
  node [
    id 700
    label "&#380;ywy"
  ]
  node [
    id 701
    label "life"
  ]
  node [
    id 702
    label "staro&#347;&#263;"
  ]
  node [
    id 703
    label "energy"
  ]
  node [
    id 704
    label "s&#261;d"
  ]
  node [
    id 705
    label "facylitator"
  ]
  node [
    id 706
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 707
    label "metodyka"
  ]
  node [
    id 708
    label "brak"
  ]
  node [
    id 709
    label "program_informacyjny"
  ]
  node [
    id 710
    label "journal"
  ]
  node [
    id 711
    label "diariusz"
  ]
  node [
    id 712
    label "spis"
  ]
  node [
    id 713
    label "sheet"
  ]
  node [
    id 714
    label "pami&#281;tnik"
  ]
  node [
    id 715
    label "gazeta"
  ]
  node [
    id 716
    label "tytu&#322;"
  ]
  node [
    id 717
    label "redakcja"
  ]
  node [
    id 718
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 719
    label "rozdzia&#322;"
  ]
  node [
    id 720
    label "Ewangelia"
  ]
  node [
    id 721
    label "book"
  ]
  node [
    id 722
    label "tome"
  ]
  node [
    id 723
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 724
    label "pami&#261;tka"
  ]
  node [
    id 725
    label "notes"
  ]
  node [
    id 726
    label "zapiski"
  ]
  node [
    id 727
    label "raptularz"
  ]
  node [
    id 728
    label "album"
  ]
  node [
    id 729
    label "utw&#243;r_epicki"
  ]
  node [
    id 730
    label "catalog"
  ]
  node [
    id 731
    label "tekst"
  ]
  node [
    id 732
    label "sumariusz"
  ]
  node [
    id 733
    label "stock"
  ]
  node [
    id 734
    label "figurowa&#263;"
  ]
  node [
    id 735
    label "wyliczanka"
  ]
  node [
    id 736
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 737
    label "poleca&#263;"
  ]
  node [
    id 738
    label "control"
  ]
  node [
    id 739
    label "decydowa&#263;"
  ]
  node [
    id 740
    label "sprawowa&#263;"
  ]
  node [
    id 741
    label "ordynowa&#263;"
  ]
  node [
    id 742
    label "doradza&#263;"
  ]
  node [
    id 743
    label "wydawa&#263;"
  ]
  node [
    id 744
    label "m&#243;wi&#263;"
  ]
  node [
    id 745
    label "charge"
  ]
  node [
    id 746
    label "placard"
  ]
  node [
    id 747
    label "powierza&#263;"
  ]
  node [
    id 748
    label "zadawa&#263;"
  ]
  node [
    id 749
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 750
    label "decide"
  ]
  node [
    id 751
    label "klasyfikator"
  ]
  node [
    id 752
    label "mean"
  ]
  node [
    id 753
    label "prosecute"
  ]
  node [
    id 754
    label "by&#263;"
  ]
  node [
    id 755
    label "klawisz"
  ]
  node [
    id 756
    label "naciska&#263;"
  ]
  node [
    id 757
    label "mie&#263;_miejsce"
  ]
  node [
    id 758
    label "atakowa&#263;"
  ]
  node [
    id 759
    label "alternate"
  ]
  node [
    id 760
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 761
    label "chance"
  ]
  node [
    id 762
    label "force"
  ]
  node [
    id 763
    label "rush"
  ]
  node [
    id 764
    label "crowd"
  ]
  node [
    id 765
    label "napierdziela&#263;"
  ]
  node [
    id 766
    label "przekonywa&#263;"
  ]
  node [
    id 767
    label "wk&#322;ada&#263;"
  ]
  node [
    id 768
    label "strike"
  ]
  node [
    id 769
    label "robi&#263;"
  ]
  node [
    id 770
    label "schorzenie"
  ]
  node [
    id 771
    label "dzia&#322;a&#263;"
  ]
  node [
    id 772
    label "ofensywny"
  ]
  node [
    id 773
    label "przewaga"
  ]
  node [
    id 774
    label "sport"
  ]
  node [
    id 775
    label "epidemia"
  ]
  node [
    id 776
    label "attack"
  ]
  node [
    id 777
    label "rozgrywa&#263;"
  ]
  node [
    id 778
    label "krytykowa&#263;"
  ]
  node [
    id 779
    label "walczy&#263;"
  ]
  node [
    id 780
    label "aim"
  ]
  node [
    id 781
    label "trouble_oneself"
  ]
  node [
    id 782
    label "napada&#263;"
  ]
  node [
    id 783
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 784
    label "usi&#322;owa&#263;"
  ]
  node [
    id 785
    label "arrangement"
  ]
  node [
    id 786
    label "zarz&#261;dzenie"
  ]
  node [
    id 787
    label "polecenie"
  ]
  node [
    id 788
    label "commission"
  ]
  node [
    id 789
    label "ordonans"
  ]
  node [
    id 790
    label "rule"
  ]
  node [
    id 791
    label "danie"
  ]
  node [
    id 792
    label "stipulation"
  ]
  node [
    id 793
    label "ukaz"
  ]
  node [
    id 794
    label "pognanie"
  ]
  node [
    id 795
    label "rekomendacja"
  ]
  node [
    id 796
    label "wypowied&#378;"
  ]
  node [
    id 797
    label "pobiegni&#281;cie"
  ]
  node [
    id 798
    label "education"
  ]
  node [
    id 799
    label "doradzenie"
  ]
  node [
    id 800
    label "statement"
  ]
  node [
    id 801
    label "recommendation"
  ]
  node [
    id 802
    label "zadanie"
  ]
  node [
    id 803
    label "zaordynowanie"
  ]
  node [
    id 804
    label "powierzenie"
  ]
  node [
    id 805
    label "przesadzenie"
  ]
  node [
    id 806
    label "consign"
  ]
  node [
    id 807
    label "dekret"
  ]
  node [
    id 808
    label "dostojnik"
  ]
  node [
    id 809
    label "Goebbels"
  ]
  node [
    id 810
    label "Sto&#322;ypin"
  ]
  node [
    id 811
    label "rz&#261;d"
  ]
  node [
    id 812
    label "przybli&#380;enie"
  ]
  node [
    id 813
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 814
    label "kategoria"
  ]
  node [
    id 815
    label "szpaler"
  ]
  node [
    id 816
    label "lon&#380;a"
  ]
  node [
    id 817
    label "uporz&#261;dkowanie"
  ]
  node [
    id 818
    label "instytucja"
  ]
  node [
    id 819
    label "jednostka_systematyczna"
  ]
  node [
    id 820
    label "premier"
  ]
  node [
    id 821
    label "Londyn"
  ]
  node [
    id 822
    label "gabinet_cieni"
  ]
  node [
    id 823
    label "gromada"
  ]
  node [
    id 824
    label "number"
  ]
  node [
    id 825
    label "Konsulat"
  ]
  node [
    id 826
    label "tract"
  ]
  node [
    id 827
    label "klasa"
  ]
  node [
    id 828
    label "w&#322;adza"
  ]
  node [
    id 829
    label "urz&#281;dnik"
  ]
  node [
    id 830
    label "notabl"
  ]
  node [
    id 831
    label "oficja&#322;"
  ]
  node [
    id 832
    label "nasiennictwo"
  ]
  node [
    id 833
    label "agrotechnika"
  ]
  node [
    id 834
    label "agroekologia"
  ]
  node [
    id 835
    label "agrobiznes"
  ]
  node [
    id 836
    label "intensyfikacja"
  ]
  node [
    id 837
    label "uprawianie"
  ]
  node [
    id 838
    label "gleboznawstwo"
  ]
  node [
    id 839
    label "gospodarka"
  ]
  node [
    id 840
    label "ogrodnictwo"
  ]
  node [
    id 841
    label "agronomia"
  ]
  node [
    id 842
    label "agrochemia"
  ]
  node [
    id 843
    label "farmerstwo"
  ]
  node [
    id 844
    label "zootechnika"
  ]
  node [
    id 845
    label "zgarniacz"
  ]
  node [
    id 846
    label "nauka"
  ]
  node [
    id 847
    label "hodowla"
  ]
  node [
    id 848
    label "sadownictwo"
  ]
  node [
    id 849
    label "&#322;&#261;karstwo"
  ]
  node [
    id 850
    label "&#322;owiectwo"
  ]
  node [
    id 851
    label "wytropienie"
  ]
  node [
    id 852
    label "podkurza&#263;"
  ]
  node [
    id 853
    label "wytropi&#263;"
  ]
  node [
    id 854
    label "polowanie"
  ]
  node [
    id 855
    label "tropienie"
  ]
  node [
    id 856
    label "tropi&#263;"
  ]
  node [
    id 857
    label "blood_sport"
  ]
  node [
    id 858
    label "defilowa&#263;"
  ]
  node [
    id 859
    label "gospodarka_le&#347;na"
  ]
  node [
    id 860
    label "potrzymanie"
  ]
  node [
    id 861
    label "praca_rolnicza"
  ]
  node [
    id 862
    label "pod&#243;j"
  ]
  node [
    id 863
    label "filiacja"
  ]
  node [
    id 864
    label "licencjonowanie"
  ]
  node [
    id 865
    label "opasa&#263;"
  ]
  node [
    id 866
    label "ch&#243;w"
  ]
  node [
    id 867
    label "licencja"
  ]
  node [
    id 868
    label "sokolarnia"
  ]
  node [
    id 869
    label "potrzyma&#263;"
  ]
  node [
    id 870
    label "rozp&#322;&#243;d"
  ]
  node [
    id 871
    label "grupa_organizm&#243;w"
  ]
  node [
    id 872
    label "wypas"
  ]
  node [
    id 873
    label "wychowalnia"
  ]
  node [
    id 874
    label "pstr&#261;garnia"
  ]
  node [
    id 875
    label "krzy&#380;owanie"
  ]
  node [
    id 876
    label "licencjonowa&#263;"
  ]
  node [
    id 877
    label "odch&#243;w"
  ]
  node [
    id 878
    label "tucz"
  ]
  node [
    id 879
    label "ud&#243;j"
  ]
  node [
    id 880
    label "klatka"
  ]
  node [
    id 881
    label "opasienie"
  ]
  node [
    id 882
    label "wych&#243;w"
  ]
  node [
    id 883
    label "obrz&#261;dek"
  ]
  node [
    id 884
    label "opasanie"
  ]
  node [
    id 885
    label "polish"
  ]
  node [
    id 886
    label "akwarium"
  ]
  node [
    id 887
    label "biotechnika"
  ]
  node [
    id 888
    label "pielenie"
  ]
  node [
    id 889
    label "culture"
  ]
  node [
    id 890
    label "sianie"
  ]
  node [
    id 891
    label "stanowisko"
  ]
  node [
    id 892
    label "sadzenie"
  ]
  node [
    id 893
    label "oprysk"
  ]
  node [
    id 894
    label "szczepienie"
  ]
  node [
    id 895
    label "orka"
  ]
  node [
    id 896
    label "siew"
  ]
  node [
    id 897
    label "exercise"
  ]
  node [
    id 898
    label "koszenie"
  ]
  node [
    id 899
    label "obrabianie"
  ]
  node [
    id 900
    label "zajmowanie_si&#281;"
  ]
  node [
    id 901
    label "use"
  ]
  node [
    id 902
    label "hodowanie"
  ]
  node [
    id 903
    label "intensywny"
  ]
  node [
    id 904
    label "&#347;ci&#243;&#322;kowanie"
  ]
  node [
    id 905
    label "szk&#243;&#322;karstwo"
  ]
  node [
    id 906
    label "hydro&#380;el"
  ]
  node [
    id 907
    label "ekstensywny"
  ]
  node [
    id 908
    label "produkcja"
  ]
  node [
    id 909
    label "zoohigiena"
  ]
  node [
    id 910
    label "agrologia"
  ]
  node [
    id 911
    label "agrofizyka"
  ]
  node [
    id 912
    label "pomologia"
  ]
  node [
    id 913
    label "ekologia"
  ]
  node [
    id 914
    label "poligonizacja"
  ]
  node [
    id 915
    label "inwentarz"
  ]
  node [
    id 916
    label "rynek"
  ]
  node [
    id 917
    label "mieszkalnictwo"
  ]
  node [
    id 918
    label "agregat_ekonomiczny"
  ]
  node [
    id 919
    label "produkowanie"
  ]
  node [
    id 920
    label "farmaceutyka"
  ]
  node [
    id 921
    label "transport"
  ]
  node [
    id 922
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 923
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 924
    label "obronno&#347;&#263;"
  ]
  node [
    id 925
    label "sektor_prywatny"
  ]
  node [
    id 926
    label "sch&#322;adza&#263;"
  ]
  node [
    id 927
    label "czerwona_strefa"
  ]
  node [
    id 928
    label "pole"
  ]
  node [
    id 929
    label "sektor_publiczny"
  ]
  node [
    id 930
    label "bankowo&#347;&#263;"
  ]
  node [
    id 931
    label "gospodarowanie"
  ]
  node [
    id 932
    label "obora"
  ]
  node [
    id 933
    label "gospodarka_wodna"
  ]
  node [
    id 934
    label "gospodarowa&#263;"
  ]
  node [
    id 935
    label "fabryka"
  ]
  node [
    id 936
    label "wytw&#243;rnia"
  ]
  node [
    id 937
    label "stodo&#322;a"
  ]
  node [
    id 938
    label "przemys&#322;"
  ]
  node [
    id 939
    label "spichlerz"
  ]
  node [
    id 940
    label "sch&#322;adzanie"
  ]
  node [
    id 941
    label "administracja"
  ]
  node [
    id 942
    label "sch&#322;odzenie"
  ]
  node [
    id 943
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 944
    label "zasada"
  ]
  node [
    id 945
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 946
    label "regulacja_cen"
  ]
  node [
    id 947
    label "szkolnictwo"
  ]
  node [
    id 948
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 949
    label "przemys&#322;_spo&#380;ywczy"
  ]
  node [
    id 950
    label "agroturystyka"
  ]
  node [
    id 951
    label "biznes"
  ]
  node [
    id 952
    label "o&#380;ywienie"
  ]
  node [
    id 953
    label "zwi&#281;kszenie"
  ]
  node [
    id 954
    label "budownictwo"
  ]
  node [
    id 955
    label "scraper"
  ]
  node [
    id 956
    label "g&#243;rnictwo"
  ]
  node [
    id 957
    label "urz&#261;dzenie"
  ]
  node [
    id 958
    label "wiedza"
  ]
  node [
    id 959
    label "miasteczko_rowerowe"
  ]
  node [
    id 960
    label "porada"
  ]
  node [
    id 961
    label "fotowoltaika"
  ]
  node [
    id 962
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 963
    label "przem&#243;wienie"
  ]
  node [
    id 964
    label "nauki_o_poznaniu"
  ]
  node [
    id 965
    label "nomotetyczny"
  ]
  node [
    id 966
    label "systematyka"
  ]
  node [
    id 967
    label "typologia"
  ]
  node [
    id 968
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 969
    label "kultura_duchowa"
  ]
  node [
    id 970
    label "&#322;awa_szkolna"
  ]
  node [
    id 971
    label "nauki_penalne"
  ]
  node [
    id 972
    label "imagineskopia"
  ]
  node [
    id 973
    label "teoria_naukowa"
  ]
  node [
    id 974
    label "inwentyka"
  ]
  node [
    id 975
    label "metodologia"
  ]
  node [
    id 976
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 977
    label "nauki_o_Ziemi"
  ]
  node [
    id 978
    label "object"
  ]
  node [
    id 979
    label "temat"
  ]
  node [
    id 980
    label "szczeg&#243;&#322;"
  ]
  node [
    id 981
    label "przebiec"
  ]
  node [
    id 982
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 983
    label "motyw"
  ]
  node [
    id 984
    label "przebiegni&#281;cie"
  ]
  node [
    id 985
    label "fabu&#322;a"
  ]
  node [
    id 986
    label "ideologia"
  ]
  node [
    id 987
    label "intelekt"
  ]
  node [
    id 988
    label "Kant"
  ]
  node [
    id 989
    label "cel"
  ]
  node [
    id 990
    label "ideacja"
  ]
  node [
    id 991
    label "wpadni&#281;cie"
  ]
  node [
    id 992
    label "przyroda"
  ]
  node [
    id 993
    label "wpa&#347;&#263;"
  ]
  node [
    id 994
    label "wpadanie"
  ]
  node [
    id 995
    label "wpada&#263;"
  ]
  node [
    id 996
    label "rozumowanie"
  ]
  node [
    id 997
    label "opracowanie"
  ]
  node [
    id 998
    label "obrady"
  ]
  node [
    id 999
    label "cytat"
  ]
  node [
    id 1000
    label "obja&#347;nienie"
  ]
  node [
    id 1001
    label "s&#261;dzenie"
  ]
  node [
    id 1002
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 1003
    label "niuansowa&#263;"
  ]
  node [
    id 1004
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 1005
    label "sk&#322;adnik"
  ]
  node [
    id 1006
    label "zniuansowa&#263;"
  ]
  node [
    id 1007
    label "fakt"
  ]
  node [
    id 1008
    label "przyczyna"
  ]
  node [
    id 1009
    label "wnioskowanie"
  ]
  node [
    id 1010
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 1011
    label "wyraz_pochodny"
  ]
  node [
    id 1012
    label "cecha"
  ]
  node [
    id 1013
    label "fraza"
  ]
  node [
    id 1014
    label "forum"
  ]
  node [
    id 1015
    label "topik"
  ]
  node [
    id 1016
    label "melodia"
  ]
  node [
    id 1017
    label "otoczka"
  ]
  node [
    id 1018
    label "ko&#322;o"
  ]
  node [
    id 1019
    label "modalno&#347;&#263;"
  ]
  node [
    id 1020
    label "z&#261;b"
  ]
  node [
    id 1021
    label "skala"
  ]
  node [
    id 1022
    label "funkcjonowa&#263;"
  ]
  node [
    id 1023
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 1024
    label "gang"
  ]
  node [
    id 1025
    label "&#322;ama&#263;"
  ]
  node [
    id 1026
    label "zabawa"
  ]
  node [
    id 1027
    label "&#322;amanie"
  ]
  node [
    id 1028
    label "obr&#281;cz"
  ]
  node [
    id 1029
    label "piasta"
  ]
  node [
    id 1030
    label "lap"
  ]
  node [
    id 1031
    label "sphere"
  ]
  node [
    id 1032
    label "grupa"
  ]
  node [
    id 1033
    label "o&#347;"
  ]
  node [
    id 1034
    label "kolokwium"
  ]
  node [
    id 1035
    label "pi"
  ]
  node [
    id 1036
    label "zwolnica"
  ]
  node [
    id 1037
    label "p&#243;&#322;kole"
  ]
  node [
    id 1038
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 1039
    label "sejmik"
  ]
  node [
    id 1040
    label "pojazd"
  ]
  node [
    id 1041
    label "figura_ograniczona"
  ]
  node [
    id 1042
    label "whip"
  ]
  node [
    id 1043
    label "okr&#261;g"
  ]
  node [
    id 1044
    label "odcinek_ko&#322;a"
  ]
  node [
    id 1045
    label "stowarzyszenie"
  ]
  node [
    id 1046
    label "podwozie"
  ]
  node [
    id 1047
    label "masztab"
  ]
  node [
    id 1048
    label "podzia&#322;ka"
  ]
  node [
    id 1049
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 1050
    label "wielko&#347;&#263;"
  ]
  node [
    id 1051
    label "zero"
  ]
  node [
    id 1052
    label "interwa&#322;"
  ]
  node [
    id 1053
    label "przymiar"
  ]
  node [
    id 1054
    label "sfera"
  ]
  node [
    id 1055
    label "dominanta"
  ]
  node [
    id 1056
    label "tetrachord"
  ]
  node [
    id 1057
    label "scale"
  ]
  node [
    id 1058
    label "przedzia&#322;"
  ]
  node [
    id 1059
    label "podzakres"
  ]
  node [
    id 1060
    label "proporcja"
  ]
  node [
    id 1061
    label "part"
  ]
  node [
    id 1062
    label "rejestr"
  ]
  node [
    id 1063
    label "subdominanta"
  ]
  node [
    id 1064
    label "kamfenol"
  ]
  node [
    id 1065
    label "artykulator"
  ]
  node [
    id 1066
    label "borowa&#263;"
  ]
  node [
    id 1067
    label "leczenie_kana&#322;owe"
  ]
  node [
    id 1068
    label "uz&#281;bienie"
  ]
  node [
    id 1069
    label "miazga_z&#281;ba"
  ]
  node [
    id 1070
    label "polakowa&#263;"
  ]
  node [
    id 1071
    label "ubytek"
  ]
  node [
    id 1072
    label "tooth"
  ]
  node [
    id 1073
    label "cement"
  ]
  node [
    id 1074
    label "element_anatomiczny"
  ]
  node [
    id 1075
    label "plombowa&#263;"
  ]
  node [
    id 1076
    label "z&#281;batka"
  ]
  node [
    id 1077
    label "emaliowanie"
  ]
  node [
    id 1078
    label "wierzcho&#322;ek_korzenia"
  ]
  node [
    id 1079
    label "ostrze"
  ]
  node [
    id 1080
    label "polakowanie"
  ]
  node [
    id 1081
    label "borowanie"
  ]
  node [
    id 1082
    label "zaplombowa&#263;"
  ]
  node [
    id 1083
    label "zaplombowanie"
  ]
  node [
    id 1084
    label "emaliowa&#263;"
  ]
  node [
    id 1085
    label "szkliwo"
  ]
  node [
    id 1086
    label "&#380;uchwa"
  ]
  node [
    id 1087
    label "z&#281;bina"
  ]
  node [
    id 1088
    label "szczoteczka"
  ]
  node [
    id 1089
    label "&#322;uk_z&#281;bowy"
  ]
  node [
    id 1090
    label "mostek"
  ]
  node [
    id 1091
    label "plombowanie"
  ]
  node [
    id 1092
    label "korona"
  ]
  node [
    id 1093
    label "charakterystyka"
  ]
  node [
    id 1094
    label "m&#322;ot"
  ]
  node [
    id 1095
    label "znak"
  ]
  node [
    id 1096
    label "drzewo"
  ]
  node [
    id 1097
    label "pr&#243;ba"
  ]
  node [
    id 1098
    label "attribute"
  ]
  node [
    id 1099
    label "marka"
  ]
  node [
    id 1100
    label "narz&#281;dzie"
  ]
  node [
    id 1101
    label "nature"
  ]
  node [
    id 1102
    label "jako&#347;&#263;"
  ]
  node [
    id 1103
    label "modulant"
  ]
  node [
    id 1104
    label "intonacja"
  ]
  node [
    id 1105
    label "partyku&#322;a"
  ]
  node [
    id 1106
    label "fleksja"
  ]
  node [
    id 1107
    label "liczba"
  ]
  node [
    id 1108
    label "coupling"
  ]
  node [
    id 1109
    label "osoba"
  ]
  node [
    id 1110
    label "czasownik"
  ]
  node [
    id 1111
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 1112
    label "orz&#281;sek"
  ]
  node [
    id 1113
    label "dziama&#263;"
  ]
  node [
    id 1114
    label "bangla&#263;"
  ]
  node [
    id 1115
    label "przekazywa&#263;"
  ]
  node [
    id 1116
    label "zbiera&#263;"
  ]
  node [
    id 1117
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 1118
    label "przywraca&#263;"
  ]
  node [
    id 1119
    label "dawa&#263;"
  ]
  node [
    id 1120
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 1121
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 1122
    label "convey"
  ]
  node [
    id 1123
    label "publicize"
  ]
  node [
    id 1124
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 1125
    label "render"
  ]
  node [
    id 1126
    label "uk&#322;ada&#263;"
  ]
  node [
    id 1127
    label "opracowywa&#263;"
  ]
  node [
    id 1128
    label "set"
  ]
  node [
    id 1129
    label "oddawa&#263;"
  ]
  node [
    id 1130
    label "train"
  ]
  node [
    id 1131
    label "zmienia&#263;"
  ]
  node [
    id 1132
    label "dzieli&#263;"
  ]
  node [
    id 1133
    label "scala&#263;"
  ]
  node [
    id 1134
    label "zestaw"
  ]
  node [
    id 1135
    label "divide"
  ]
  node [
    id 1136
    label "posiada&#263;"
  ]
  node [
    id 1137
    label "deal"
  ]
  node [
    id 1138
    label "cover"
  ]
  node [
    id 1139
    label "liczy&#263;"
  ]
  node [
    id 1140
    label "assign"
  ]
  node [
    id 1141
    label "korzysta&#263;"
  ]
  node [
    id 1142
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 1143
    label "digest"
  ]
  node [
    id 1144
    label "powodowa&#263;"
  ]
  node [
    id 1145
    label "share"
  ]
  node [
    id 1146
    label "iloraz"
  ]
  node [
    id 1147
    label "sk&#322;&#243;ca&#263;"
  ]
  node [
    id 1148
    label "rozdawa&#263;"
  ]
  node [
    id 1149
    label "dostarcza&#263;"
  ]
  node [
    id 1150
    label "sacrifice"
  ]
  node [
    id 1151
    label "odst&#281;powa&#263;"
  ]
  node [
    id 1152
    label "sprzedawa&#263;"
  ]
  node [
    id 1153
    label "give"
  ]
  node [
    id 1154
    label "reflect"
  ]
  node [
    id 1155
    label "surrender"
  ]
  node [
    id 1156
    label "deliver"
  ]
  node [
    id 1157
    label "odpowiada&#263;"
  ]
  node [
    id 1158
    label "umieszcza&#263;"
  ]
  node [
    id 1159
    label "blurt_out"
  ]
  node [
    id 1160
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 1161
    label "przedstawia&#263;"
  ]
  node [
    id 1162
    label "impart"
  ]
  node [
    id 1163
    label "przejmowa&#263;"
  ]
  node [
    id 1164
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 1165
    label "gromadzi&#263;"
  ]
  node [
    id 1166
    label "bra&#263;"
  ]
  node [
    id 1167
    label "pozyskiwa&#263;"
  ]
  node [
    id 1168
    label "poci&#261;ga&#263;"
  ]
  node [
    id 1169
    label "wzbiera&#263;"
  ]
  node [
    id 1170
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 1171
    label "meet"
  ]
  node [
    id 1172
    label "dostawa&#263;"
  ]
  node [
    id 1173
    label "consolidate"
  ]
  node [
    id 1174
    label "congregate"
  ]
  node [
    id 1175
    label "postpone"
  ]
  node [
    id 1176
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 1177
    label "znosi&#263;"
  ]
  node [
    id 1178
    label "chroni&#263;"
  ]
  node [
    id 1179
    label "darowywa&#263;"
  ]
  node [
    id 1180
    label "preserve"
  ]
  node [
    id 1181
    label "zachowywa&#263;"
  ]
  node [
    id 1182
    label "dispose"
  ]
  node [
    id 1183
    label "uczy&#263;"
  ]
  node [
    id 1184
    label "kszta&#322;ci&#263;"
  ]
  node [
    id 1185
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1186
    label "porz&#261;dkowa&#263;"
  ]
  node [
    id 1187
    label "przygotowywa&#263;"
  ]
  node [
    id 1188
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 1189
    label "tworzy&#263;"
  ]
  node [
    id 1190
    label "treser"
  ]
  node [
    id 1191
    label "raise"
  ]
  node [
    id 1192
    label "pozostawia&#263;"
  ]
  node [
    id 1193
    label "zaczyna&#263;"
  ]
  node [
    id 1194
    label "psu&#263;"
  ]
  node [
    id 1195
    label "wzbudza&#263;"
  ]
  node [
    id 1196
    label "go"
  ]
  node [
    id 1197
    label "inspirowa&#263;"
  ]
  node [
    id 1198
    label "wpaja&#263;"
  ]
  node [
    id 1199
    label "seat"
  ]
  node [
    id 1200
    label "wygrywa&#263;"
  ]
  node [
    id 1201
    label "go&#347;ci&#263;"
  ]
  node [
    id 1202
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 1203
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 1204
    label "pour"
  ]
  node [
    id 1205
    label "elaborate"
  ]
  node [
    id 1206
    label "pokrywa&#263;"
  ]
  node [
    id 1207
    label "traci&#263;"
  ]
  node [
    id 1208
    label "change"
  ]
  node [
    id 1209
    label "reengineering"
  ]
  node [
    id 1210
    label "zast&#281;powa&#263;"
  ]
  node [
    id 1211
    label "sprawia&#263;"
  ]
  node [
    id 1212
    label "zyskiwa&#263;"
  ]
  node [
    id 1213
    label "przechodzi&#263;"
  ]
  node [
    id 1214
    label "consort"
  ]
  node [
    id 1215
    label "jednoczy&#263;"
  ]
  node [
    id 1216
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1217
    label "podawa&#263;"
  ]
  node [
    id 1218
    label "wp&#322;aca&#263;"
  ]
  node [
    id 1219
    label "sygna&#322;"
  ]
  node [
    id 1220
    label "doprowadza&#263;"
  ]
  node [
    id 1221
    label "&#322;adowa&#263;"
  ]
  node [
    id 1222
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1223
    label "przeznacza&#263;"
  ]
  node [
    id 1224
    label "traktowa&#263;"
  ]
  node [
    id 1225
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1226
    label "obiecywa&#263;"
  ]
  node [
    id 1227
    label "tender"
  ]
  node [
    id 1228
    label "rap"
  ]
  node [
    id 1229
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 1230
    label "t&#322;uc"
  ]
  node [
    id 1231
    label "wpiernicza&#263;"
  ]
  node [
    id 1232
    label "exsert"
  ]
  node [
    id 1233
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 1234
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 1235
    label "p&#322;aci&#263;"
  ]
  node [
    id 1236
    label "hold_out"
  ]
  node [
    id 1237
    label "nalewa&#263;"
  ]
  node [
    id 1238
    label "zezwala&#263;"
  ]
  node [
    id 1239
    label "hold"
  ]
  node [
    id 1240
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 1241
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 1242
    label "relate"
  ]
  node [
    id 1243
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 1244
    label "dopieprza&#263;"
  ]
  node [
    id 1245
    label "press"
  ]
  node [
    id 1246
    label "urge"
  ]
  node [
    id 1247
    label "zbli&#380;a&#263;"
  ]
  node [
    id 1248
    label "przykrochmala&#263;"
  ]
  node [
    id 1249
    label "uderza&#263;"
  ]
  node [
    id 1250
    label "gem"
  ]
  node [
    id 1251
    label "kompozycja"
  ]
  node [
    id 1252
    label "runda"
  ]
  node [
    id 1253
    label "muzyka"
  ]
  node [
    id 1254
    label "stage_set"
  ]
  node [
    id 1255
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 1256
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1257
    label "figure"
  ]
  node [
    id 1258
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 1259
    label "dekal"
  ]
  node [
    id 1260
    label "program_u&#380;ytkowy"
  ]
  node [
    id 1261
    label "dokumentacja"
  ]
  node [
    id 1262
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 1263
    label "entrance"
  ]
  node [
    id 1264
    label "wpis"
  ]
  node [
    id 1265
    label "normalizacja"
  ]
  node [
    id 1266
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1267
    label "asymilowanie"
  ]
  node [
    id 1268
    label "wapniak"
  ]
  node [
    id 1269
    label "asymilowa&#263;"
  ]
  node [
    id 1270
    label "os&#322;abia&#263;"
  ]
  node [
    id 1271
    label "hominid"
  ]
  node [
    id 1272
    label "podw&#322;adny"
  ]
  node [
    id 1273
    label "os&#322;abianie"
  ]
  node [
    id 1274
    label "g&#322;owa"
  ]
  node [
    id 1275
    label "figura"
  ]
  node [
    id 1276
    label "portrecista"
  ]
  node [
    id 1277
    label "dwun&#243;g"
  ]
  node [
    id 1278
    label "profanum"
  ]
  node [
    id 1279
    label "mikrokosmos"
  ]
  node [
    id 1280
    label "nasada"
  ]
  node [
    id 1281
    label "duch"
  ]
  node [
    id 1282
    label "antropochoria"
  ]
  node [
    id 1283
    label "senior"
  ]
  node [
    id 1284
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1285
    label "Adam"
  ]
  node [
    id 1286
    label "homo_sapiens"
  ]
  node [
    id 1287
    label "polifag"
  ]
  node [
    id 1288
    label "kr&#243;lestwo"
  ]
  node [
    id 1289
    label "autorament"
  ]
  node [
    id 1290
    label "variety"
  ]
  node [
    id 1291
    label "antycypacja"
  ]
  node [
    id 1292
    label "przypuszczenie"
  ]
  node [
    id 1293
    label "cynk"
  ]
  node [
    id 1294
    label "obstawia&#263;"
  ]
  node [
    id 1295
    label "sztuka"
  ]
  node [
    id 1296
    label "design"
  ]
  node [
    id 1297
    label "sytuacja"
  ]
  node [
    id 1298
    label "mechanika"
  ]
  node [
    id 1299
    label "utrzymywanie"
  ]
  node [
    id 1300
    label "move"
  ]
  node [
    id 1301
    label "poruszenie"
  ]
  node [
    id 1302
    label "movement"
  ]
  node [
    id 1303
    label "myk"
  ]
  node [
    id 1304
    label "utrzyma&#263;"
  ]
  node [
    id 1305
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1306
    label "utrzymanie"
  ]
  node [
    id 1307
    label "travel"
  ]
  node [
    id 1308
    label "kanciasty"
  ]
  node [
    id 1309
    label "commercial_enterprise"
  ]
  node [
    id 1310
    label "strumie&#324;"
  ]
  node [
    id 1311
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1312
    label "kr&#243;tki"
  ]
  node [
    id 1313
    label "taktyka"
  ]
  node [
    id 1314
    label "apraksja"
  ]
  node [
    id 1315
    label "natural_process"
  ]
  node [
    id 1316
    label "utrzymywa&#263;"
  ]
  node [
    id 1317
    label "d&#322;ugi"
  ]
  node [
    id 1318
    label "dyssypacja_energii"
  ]
  node [
    id 1319
    label "tumult"
  ]
  node [
    id 1320
    label "stopek"
  ]
  node [
    id 1321
    label "manewr"
  ]
  node [
    id 1322
    label "lokomocja"
  ]
  node [
    id 1323
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1324
    label "komunikacja"
  ]
  node [
    id 1325
    label "drift"
  ]
  node [
    id 1326
    label "kalka"
  ]
  node [
    id 1327
    label "ceramika"
  ]
  node [
    id 1328
    label "kalkomania"
  ]
  node [
    id 1329
    label "prayer"
  ]
  node [
    id 1330
    label "propozycja"
  ]
  node [
    id 1331
    label "my&#347;l"
  ]
  node [
    id 1332
    label "motion"
  ]
  node [
    id 1333
    label "umys&#322;"
  ]
  node [
    id 1334
    label "political_orientation"
  ]
  node [
    id 1335
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 1336
    label "fantomatyka"
  ]
  node [
    id 1337
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 1338
    label "alternatywa_Fredholma"
  ]
  node [
    id 1339
    label "oznajmianie"
  ]
  node [
    id 1340
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 1341
    label "teoria"
  ]
  node [
    id 1342
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 1343
    label "paradoks_Leontiefa"
  ]
  node [
    id 1344
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 1345
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 1346
    label "teza"
  ]
  node [
    id 1347
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 1348
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 1349
    label "twierdzenie_Pettisa"
  ]
  node [
    id 1350
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 1351
    label "twierdzenie_Maya"
  ]
  node [
    id 1352
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 1353
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 1354
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 1355
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 1356
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 1357
    label "zapewnianie"
  ]
  node [
    id 1358
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 1359
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 1360
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 1361
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 1362
    label "twierdzenie_Stokesa"
  ]
  node [
    id 1363
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 1364
    label "twierdzenie_Cevy"
  ]
  node [
    id 1365
    label "twierdzenie_Pascala"
  ]
  node [
    id 1366
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 1367
    label "komunikowanie"
  ]
  node [
    id 1368
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 1369
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 1370
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 1371
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 1372
    label "handwriting"
  ]
  node [
    id 1373
    label "przekaz"
  ]
  node [
    id 1374
    label "dzie&#322;o"
  ]
  node [
    id 1375
    label "paleograf"
  ]
  node [
    id 1376
    label "interpunkcja"
  ]
  node [
    id 1377
    label "grafia"
  ]
  node [
    id 1378
    label "script"
  ]
  node [
    id 1379
    label "list"
  ]
  node [
    id 1380
    label "adres"
  ]
  node [
    id 1381
    label "ortografia"
  ]
  node [
    id 1382
    label "letter"
  ]
  node [
    id 1383
    label "paleografia"
  ]
  node [
    id 1384
    label "j&#281;zyk"
  ]
  node [
    id 1385
    label "proposal"
  ]
  node [
    id 1386
    label "proszenie"
  ]
  node [
    id 1387
    label "dochodzenie"
  ]
  node [
    id 1388
    label "proces_my&#347;lowy"
  ]
  node [
    id 1389
    label "lead"
  ]
  node [
    id 1390
    label "konkluzja"
  ]
  node [
    id 1391
    label "sk&#322;adanie"
  ]
  node [
    id 1392
    label "dop&#322;aci&#263;"
  ]
  node [
    id 1393
    label "zap&#322;aci&#263;"
  ]
  node [
    id 1394
    label "fabrication"
  ]
  node [
    id 1395
    label "scheduling"
  ]
  node [
    id 1396
    label "kreacja"
  ]
  node [
    id 1397
    label "monta&#380;"
  ]
  node [
    id 1398
    label "performance"
  ]
  node [
    id 1399
    label "plisa"
  ]
  node [
    id 1400
    label "ustawienie"
  ]
  node [
    id 1401
    label "function"
  ]
  node [
    id 1402
    label "tren"
  ]
  node [
    id 1403
    label "zreinterpretowa&#263;"
  ]
  node [
    id 1404
    label "production"
  ]
  node [
    id 1405
    label "reinterpretowa&#263;"
  ]
  node [
    id 1406
    label "ustawi&#263;"
  ]
  node [
    id 1407
    label "zreinterpretowanie"
  ]
  node [
    id 1408
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 1409
    label "gra&#263;"
  ]
  node [
    id 1410
    label "aktorstwo"
  ]
  node [
    id 1411
    label "kostium"
  ]
  node [
    id 1412
    label "toaleta"
  ]
  node [
    id 1413
    label "zagra&#263;"
  ]
  node [
    id 1414
    label "reinterpretowanie"
  ]
  node [
    id 1415
    label "zagranie"
  ]
  node [
    id 1416
    label "granie"
  ]
  node [
    id 1417
    label "liczenie"
  ]
  node [
    id 1418
    label "czyn"
  ]
  node [
    id 1419
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 1420
    label "supremum"
  ]
  node [
    id 1421
    label "laparotomia"
  ]
  node [
    id 1422
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1423
    label "matematyka"
  ]
  node [
    id 1424
    label "rzut"
  ]
  node [
    id 1425
    label "torakotomia"
  ]
  node [
    id 1426
    label "chirurg"
  ]
  node [
    id 1427
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 1428
    label "zabieg"
  ]
  node [
    id 1429
    label "szew"
  ]
  node [
    id 1430
    label "mathematical_process"
  ]
  node [
    id 1431
    label "infimum"
  ]
  node [
    id 1432
    label "obrazowanie"
  ]
  node [
    id 1433
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1434
    label "dorobek"
  ]
  node [
    id 1435
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 1436
    label "retrospektywa"
  ]
  node [
    id 1437
    label "works"
  ]
  node [
    id 1438
    label "creation"
  ]
  node [
    id 1439
    label "tetralogia"
  ]
  node [
    id 1440
    label "komunikat"
  ]
  node [
    id 1441
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 1442
    label "praca"
  ]
  node [
    id 1443
    label "spos&#243;b_dzia&#322;ania"
  ]
  node [
    id 1444
    label "konstrukcja"
  ]
  node [
    id 1445
    label "audycja"
  ]
  node [
    id 1446
    label "film"
  ]
  node [
    id 1447
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 1448
    label "zachowanie"
  ]
  node [
    id 1449
    label "ekscerpcja"
  ]
  node [
    id 1450
    label "materia&#322;"
  ]
  node [
    id 1451
    label "operat"
  ]
  node [
    id 1452
    label "kosztorys"
  ]
  node [
    id 1453
    label "&#347;wiadectwo"
  ]
  node [
    id 1454
    label "parafa"
  ]
  node [
    id 1455
    label "plik"
  ]
  node [
    id 1456
    label "raport&#243;wka"
  ]
  node [
    id 1457
    label "utw&#243;r"
  ]
  node [
    id 1458
    label "record"
  ]
  node [
    id 1459
    label "registratura"
  ]
  node [
    id 1460
    label "artyku&#322;"
  ]
  node [
    id 1461
    label "writing"
  ]
  node [
    id 1462
    label "sygnatariusz"
  ]
  node [
    id 1463
    label "dodatek"
  ]
  node [
    id 1464
    label "oprawa"
  ]
  node [
    id 1465
    label "stela&#380;"
  ]
  node [
    id 1466
    label "paczka"
  ]
  node [
    id 1467
    label "obramowanie"
  ]
  node [
    id 1468
    label "postawa"
  ]
  node [
    id 1469
    label "element_konstrukcyjny"
  ]
  node [
    id 1470
    label "szablon"
  ]
  node [
    id 1471
    label "doj&#347;cie"
  ]
  node [
    id 1472
    label "doch&#243;d"
  ]
  node [
    id 1473
    label "galanteria"
  ]
  node [
    id 1474
    label "doj&#347;&#263;"
  ]
  node [
    id 1475
    label "aneks"
  ]
  node [
    id 1476
    label "prevention"
  ]
  node [
    id 1477
    label "otoczenie"
  ]
  node [
    id 1478
    label "framing"
  ]
  node [
    id 1479
    label "boarding"
  ]
  node [
    id 1480
    label "binda"
  ]
  node [
    id 1481
    label "filet"
  ]
  node [
    id 1482
    label "Rzym_Zachodni"
  ]
  node [
    id 1483
    label "whole"
  ]
  node [
    id 1484
    label "ilo&#347;&#263;"
  ]
  node [
    id 1485
    label "Rzym_Wschodni"
  ]
  node [
    id 1486
    label "granica"
  ]
  node [
    id 1487
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 1488
    label "desygnat"
  ]
  node [
    id 1489
    label "circle"
  ]
  node [
    id 1490
    label "towarzystwo"
  ]
  node [
    id 1491
    label "granda"
  ]
  node [
    id 1492
    label "pakunek"
  ]
  node [
    id 1493
    label "poczta"
  ]
  node [
    id 1494
    label "pakiet"
  ]
  node [
    id 1495
    label "baletnica"
  ]
  node [
    id 1496
    label "przesy&#322;ka"
  ]
  node [
    id 1497
    label "opakowanie"
  ]
  node [
    id 1498
    label "usenet"
  ]
  node [
    id 1499
    label "rozprz&#261;c"
  ]
  node [
    id 1500
    label "cybernetyk"
  ]
  node [
    id 1501
    label "podsystem"
  ]
  node [
    id 1502
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1503
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1504
    label "sk&#322;ad"
  ]
  node [
    id 1505
    label "systemat"
  ]
  node [
    id 1506
    label "konstelacja"
  ]
  node [
    id 1507
    label "stan"
  ]
  node [
    id 1508
    label "nastawienie"
  ]
  node [
    id 1509
    label "attitude"
  ]
  node [
    id 1510
    label "jig"
  ]
  node [
    id 1511
    label "drabina_analgetyczna"
  ]
  node [
    id 1512
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1513
    label "C"
  ]
  node [
    id 1514
    label "D"
  ]
  node [
    id 1515
    label "exemplar"
  ]
  node [
    id 1516
    label "odholowa&#263;"
  ]
  node [
    id 1517
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 1518
    label "tabor"
  ]
  node [
    id 1519
    label "przyholowywanie"
  ]
  node [
    id 1520
    label "przyholowa&#263;"
  ]
  node [
    id 1521
    label "przyholowanie"
  ]
  node [
    id 1522
    label "fukni&#281;cie"
  ]
  node [
    id 1523
    label "l&#261;d"
  ]
  node [
    id 1524
    label "zielona_karta"
  ]
  node [
    id 1525
    label "fukanie"
  ]
  node [
    id 1526
    label "przyholowywa&#263;"
  ]
  node [
    id 1527
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1528
    label "woda"
  ]
  node [
    id 1529
    label "przeszklenie"
  ]
  node [
    id 1530
    label "test_zderzeniowy"
  ]
  node [
    id 1531
    label "powietrze"
  ]
  node [
    id 1532
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1533
    label "odzywka"
  ]
  node [
    id 1534
    label "nadwozie"
  ]
  node [
    id 1535
    label "odholowanie"
  ]
  node [
    id 1536
    label "prowadzenie_si&#281;"
  ]
  node [
    id 1537
    label "odholowywa&#263;"
  ]
  node [
    id 1538
    label "pod&#322;oga"
  ]
  node [
    id 1539
    label "odholowywanie"
  ]
  node [
    id 1540
    label "hamulec"
  ]
  node [
    id 1541
    label "instalowa&#263;"
  ]
  node [
    id 1542
    label "oprogramowanie"
  ]
  node [
    id 1543
    label "odinstalowywa&#263;"
  ]
  node [
    id 1544
    label "zaprezentowanie"
  ]
  node [
    id 1545
    label "podprogram"
  ]
  node [
    id 1546
    label "ogranicznik_referencyjny"
  ]
  node [
    id 1547
    label "course_of_study"
  ]
  node [
    id 1548
    label "booklet"
  ]
  node [
    id 1549
    label "odinstalowanie"
  ]
  node [
    id 1550
    label "broszura"
  ]
  node [
    id 1551
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 1552
    label "kana&#322;"
  ]
  node [
    id 1553
    label "teleferie"
  ]
  node [
    id 1554
    label "zainstalowanie"
  ]
  node [
    id 1555
    label "struktura_organizacyjna"
  ]
  node [
    id 1556
    label "pirat"
  ]
  node [
    id 1557
    label "zaprezentowa&#263;"
  ]
  node [
    id 1558
    label "prezentowanie"
  ]
  node [
    id 1559
    label "prezentowa&#263;"
  ]
  node [
    id 1560
    label "interfejs"
  ]
  node [
    id 1561
    label "okno"
  ]
  node [
    id 1562
    label "folder"
  ]
  node [
    id 1563
    label "zainstalowa&#263;"
  ]
  node [
    id 1564
    label "ram&#243;wka"
  ]
  node [
    id 1565
    label "emitowa&#263;"
  ]
  node [
    id 1566
    label "emitowanie"
  ]
  node [
    id 1567
    label "odinstalowywanie"
  ]
  node [
    id 1568
    label "instrukcja"
  ]
  node [
    id 1569
    label "informatyka"
  ]
  node [
    id 1570
    label "deklaracja"
  ]
  node [
    id 1571
    label "sekcja_krytyczna"
  ]
  node [
    id 1572
    label "menu"
  ]
  node [
    id 1573
    label "furkacja"
  ]
  node [
    id 1574
    label "instalowanie"
  ]
  node [
    id 1575
    label "oferta"
  ]
  node [
    id 1576
    label "odinstalowa&#263;"
  ]
  node [
    id 1577
    label "druk_ulotny"
  ]
  node [
    id 1578
    label "wydawnictwo"
  ]
  node [
    id 1579
    label "rozmiar"
  ]
  node [
    id 1580
    label "zasi&#261;g"
  ]
  node [
    id 1581
    label "izochronizm"
  ]
  node [
    id 1582
    label "bridge"
  ]
  node [
    id 1583
    label "distribution"
  ]
  node [
    id 1584
    label "offer"
  ]
  node [
    id 1585
    label "o&#347;wiadczenie"
  ]
  node [
    id 1586
    label "obietnica"
  ]
  node [
    id 1587
    label "formularz"
  ]
  node [
    id 1588
    label "announcement"
  ]
  node [
    id 1589
    label "o&#347;wiadczyny"
  ]
  node [
    id 1590
    label "szaniec"
  ]
  node [
    id 1591
    label "topologia_magistrali"
  ]
  node [
    id 1592
    label "grodzisko"
  ]
  node [
    id 1593
    label "tarapaty"
  ]
  node [
    id 1594
    label "piaskownik"
  ]
  node [
    id 1595
    label "struktura_anatomiczna"
  ]
  node [
    id 1596
    label "bystrza"
  ]
  node [
    id 1597
    label "pit"
  ]
  node [
    id 1598
    label "odk&#322;ad"
  ]
  node [
    id 1599
    label "chody"
  ]
  node [
    id 1600
    label "klarownia"
  ]
  node [
    id 1601
    label "kanalizacja"
  ]
  node [
    id 1602
    label "przew&#243;d"
  ]
  node [
    id 1603
    label "budowa"
  ]
  node [
    id 1604
    label "ciek"
  ]
  node [
    id 1605
    label "teatr"
  ]
  node [
    id 1606
    label "gara&#380;"
  ]
  node [
    id 1607
    label "zrzutowy"
  ]
  node [
    id 1608
    label "warsztat"
  ]
  node [
    id 1609
    label "syfon"
  ]
  node [
    id 1610
    label "odwa&#322;"
  ]
  node [
    id 1611
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 1612
    label "HP"
  ]
  node [
    id 1613
    label "dost&#281;p"
  ]
  node [
    id 1614
    label "infa"
  ]
  node [
    id 1615
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 1616
    label "kryptologia"
  ]
  node [
    id 1617
    label "baza_danych"
  ]
  node [
    id 1618
    label "przetwarzanie_informacji"
  ]
  node [
    id 1619
    label "sztuczna_inteligencja"
  ]
  node [
    id 1620
    label "gramatyka_formalna"
  ]
  node [
    id 1621
    label "zamek"
  ]
  node [
    id 1622
    label "dziedzina_informatyki"
  ]
  node [
    id 1623
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 1624
    label "artefakt"
  ]
  node [
    id 1625
    label "usuni&#281;cie"
  ]
  node [
    id 1626
    label "parapet"
  ]
  node [
    id 1627
    label "szyba"
  ]
  node [
    id 1628
    label "okiennica"
  ]
  node [
    id 1629
    label "prze&#347;wit"
  ]
  node [
    id 1630
    label "pulpit"
  ]
  node [
    id 1631
    label "transenna"
  ]
  node [
    id 1632
    label "kwatera_okienna"
  ]
  node [
    id 1633
    label "inspekt"
  ]
  node [
    id 1634
    label "nora"
  ]
  node [
    id 1635
    label "skrzyd&#322;o"
  ]
  node [
    id 1636
    label "nadokiennik"
  ]
  node [
    id 1637
    label "futryna"
  ]
  node [
    id 1638
    label "lufcik"
  ]
  node [
    id 1639
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 1640
    label "casement"
  ]
  node [
    id 1641
    label "menad&#380;er_okien"
  ]
  node [
    id 1642
    label "otw&#243;r"
  ]
  node [
    id 1643
    label "dostosowywa&#263;"
  ]
  node [
    id 1644
    label "supply"
  ]
  node [
    id 1645
    label "accommodate"
  ]
  node [
    id 1646
    label "komputer"
  ]
  node [
    id 1647
    label "fit"
  ]
  node [
    id 1648
    label "usuwa&#263;"
  ]
  node [
    id 1649
    label "usuwanie"
  ]
  node [
    id 1650
    label "dostosowa&#263;"
  ]
  node [
    id 1651
    label "zrobi&#263;"
  ]
  node [
    id 1652
    label "install"
  ]
  node [
    id 1653
    label "umieszczanie"
  ]
  node [
    id 1654
    label "installation"
  ]
  node [
    id 1655
    label "collection"
  ]
  node [
    id 1656
    label "wmontowanie"
  ]
  node [
    id 1657
    label "wmontowywanie"
  ]
  node [
    id 1658
    label "fitting"
  ]
  node [
    id 1659
    label "dostosowywanie"
  ]
  node [
    id 1660
    label "usun&#261;&#263;"
  ]
  node [
    id 1661
    label "dostosowanie"
  ]
  node [
    id 1662
    label "layout"
  ]
  node [
    id 1663
    label "przest&#281;pca"
  ]
  node [
    id 1664
    label "kopiowa&#263;"
  ]
  node [
    id 1665
    label "podr&#243;bka"
  ]
  node [
    id 1666
    label "kieruj&#261;cy"
  ]
  node [
    id 1667
    label "&#380;agl&#243;wka"
  ]
  node [
    id 1668
    label "rum"
  ]
  node [
    id 1669
    label "rozb&#243;jnik"
  ]
  node [
    id 1670
    label "postrzeleniec"
  ]
  node [
    id 1671
    label "testify"
  ]
  node [
    id 1672
    label "przedstawi&#263;"
  ]
  node [
    id 1673
    label "pokaza&#263;"
  ]
  node [
    id 1674
    label "zapozna&#263;"
  ]
  node [
    id 1675
    label "represent"
  ]
  node [
    id 1676
    label "typify"
  ]
  node [
    id 1677
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 1678
    label "uprzedzi&#263;"
  ]
  node [
    id 1679
    label "attest"
  ]
  node [
    id 1680
    label "wyra&#380;anie"
  ]
  node [
    id 1681
    label "uprzedzanie"
  ]
  node [
    id 1682
    label "zapoznawanie"
  ]
  node [
    id 1683
    label "present"
  ]
  node [
    id 1684
    label "przedstawianie"
  ]
  node [
    id 1685
    label "display"
  ]
  node [
    id 1686
    label "demonstrowanie"
  ]
  node [
    id 1687
    label "presentation"
  ]
  node [
    id 1688
    label "zapoznanie"
  ]
  node [
    id 1689
    label "zapoznanie_si&#281;"
  ]
  node [
    id 1690
    label "exhibit"
  ]
  node [
    id 1691
    label "pokazanie"
  ]
  node [
    id 1692
    label "wyst&#261;pienie"
  ]
  node [
    id 1693
    label "uprzedzenie"
  ]
  node [
    id 1694
    label "zapoznawa&#263;"
  ]
  node [
    id 1695
    label "uprzedza&#263;"
  ]
  node [
    id 1696
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1697
    label "nadawa&#263;"
  ]
  node [
    id 1698
    label "energia"
  ]
  node [
    id 1699
    label "nada&#263;"
  ]
  node [
    id 1700
    label "tembr"
  ]
  node [
    id 1701
    label "air"
  ]
  node [
    id 1702
    label "wydoby&#263;"
  ]
  node [
    id 1703
    label "emit"
  ]
  node [
    id 1704
    label "wys&#322;a&#263;"
  ]
  node [
    id 1705
    label "wydzieli&#263;"
  ]
  node [
    id 1706
    label "wydziela&#263;"
  ]
  node [
    id 1707
    label "wprowadzi&#263;"
  ]
  node [
    id 1708
    label "wydobywa&#263;"
  ]
  node [
    id 1709
    label "wys&#322;anie"
  ]
  node [
    id 1710
    label "wysy&#322;anie"
  ]
  node [
    id 1711
    label "wydzielenie"
  ]
  node [
    id 1712
    label "wprowadzenie"
  ]
  node [
    id 1713
    label "wydobycie"
  ]
  node [
    id 1714
    label "wydzielanie"
  ]
  node [
    id 1715
    label "wydobywanie"
  ]
  node [
    id 1716
    label "nadawanie"
  ]
  node [
    id 1717
    label "emission"
  ]
  node [
    id 1718
    label "wprowadzanie"
  ]
  node [
    id 1719
    label "nadanie"
  ]
  node [
    id 1720
    label "issue"
  ]
  node [
    id 1721
    label "ulotka"
  ]
  node [
    id 1722
    label "wskaz&#243;wka"
  ]
  node [
    id 1723
    label "instruktarz"
  ]
  node [
    id 1724
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 1725
    label "restauracja"
  ]
  node [
    id 1726
    label "cennik"
  ]
  node [
    id 1727
    label "chart"
  ]
  node [
    id 1728
    label "karta"
  ]
  node [
    id 1729
    label "bajt"
  ]
  node [
    id 1730
    label "bloking"
  ]
  node [
    id 1731
    label "j&#261;kanie"
  ]
  node [
    id 1732
    label "przeszkoda"
  ]
  node [
    id 1733
    label "blokada"
  ]
  node [
    id 1734
    label "bry&#322;a"
  ]
  node [
    id 1735
    label "kontynent"
  ]
  node [
    id 1736
    label "nastawnia"
  ]
  node [
    id 1737
    label "blockage"
  ]
  node [
    id 1738
    label "block"
  ]
  node [
    id 1739
    label "budynek"
  ]
  node [
    id 1740
    label "start"
  ]
  node [
    id 1741
    label "skorupa_ziemska"
  ]
  node [
    id 1742
    label "zeszyt"
  ]
  node [
    id 1743
    label "blokowisko"
  ]
  node [
    id 1744
    label "barak"
  ]
  node [
    id 1745
    label "stok_kontynentalny"
  ]
  node [
    id 1746
    label "square"
  ]
  node [
    id 1747
    label "siatk&#243;wka"
  ]
  node [
    id 1748
    label "kr&#261;g"
  ]
  node [
    id 1749
    label "obrona"
  ]
  node [
    id 1750
    label "bie&#380;nia"
  ]
  node [
    id 1751
    label "referat"
  ]
  node [
    id 1752
    label "dom_wielorodzinny"
  ]
  node [
    id 1753
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 1754
    label "routine"
  ]
  node [
    id 1755
    label "proceduralnie"
  ]
  node [
    id 1756
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1757
    label "jednostka_organizacyjna"
  ]
  node [
    id 1758
    label "urz&#261;d"
  ]
  node [
    id 1759
    label "insourcing"
  ]
  node [
    id 1760
    label "stopie&#324;"
  ]
  node [
    id 1761
    label "competence"
  ]
  node [
    id 1762
    label "bezdro&#380;e"
  ]
  node [
    id 1763
    label "poddzia&#322;"
  ]
  node [
    id 1764
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 1765
    label "okienko"
  ]
  node [
    id 1766
    label "mo&#380;liwy"
  ]
  node [
    id 1767
    label "medyczny"
  ]
  node [
    id 1768
    label "operacyjnie"
  ]
  node [
    id 1769
    label "leczniczy"
  ]
  node [
    id 1770
    label "lekarsko"
  ]
  node [
    id 1771
    label "medycznie"
  ]
  node [
    id 1772
    label "paramedyczny"
  ]
  node [
    id 1773
    label "profilowy"
  ]
  node [
    id 1774
    label "bia&#322;y"
  ]
  node [
    id 1775
    label "praktyczny"
  ]
  node [
    id 1776
    label "specjalistyczny"
  ]
  node [
    id 1777
    label "zgodny"
  ]
  node [
    id 1778
    label "specjalny"
  ]
  node [
    id 1779
    label "urealnianie"
  ]
  node [
    id 1780
    label "mo&#380;ebny"
  ]
  node [
    id 1781
    label "umo&#380;liwianie"
  ]
  node [
    id 1782
    label "zno&#347;ny"
  ]
  node [
    id 1783
    label "umo&#380;liwienie"
  ]
  node [
    id 1784
    label "mo&#380;liwie"
  ]
  node [
    id 1785
    label "urealnienie"
  ]
  node [
    id 1786
    label "dost&#281;pny"
  ]
  node [
    id 1787
    label "reorganizacja"
  ]
  node [
    id 1788
    label "restructure"
  ]
  node [
    id 1789
    label "conversion"
  ]
  node [
    id 1790
    label "modernization"
  ]
  node [
    id 1791
    label "ulepszenie"
  ]
  node [
    id 1792
    label "modyfikacja"
  ]
  node [
    id 1793
    label "lepszy"
  ]
  node [
    id 1794
    label "poprawa"
  ]
  node [
    id 1795
    label "zmienienie"
  ]
  node [
    id 1796
    label "klaster_dyskowy"
  ]
  node [
    id 1797
    label "widownia"
  ]
  node [
    id 1798
    label "teren"
  ]
  node [
    id 1799
    label "balkon"
  ]
  node [
    id 1800
    label "odbiorca"
  ]
  node [
    id 1801
    label "proscenium"
  ]
  node [
    id 1802
    label "audience"
  ]
  node [
    id 1803
    label "publiczka"
  ]
  node [
    id 1804
    label "widzownia"
  ]
  node [
    id 1805
    label "lo&#380;a"
  ]
  node [
    id 1806
    label "Kosowo"
  ]
  node [
    id 1807
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1808
    label "Zab&#322;ocie"
  ]
  node [
    id 1809
    label "Pow&#261;zki"
  ]
  node [
    id 1810
    label "Piotrowo"
  ]
  node [
    id 1811
    label "Olszanica"
  ]
  node [
    id 1812
    label "holarktyka"
  ]
  node [
    id 1813
    label "Ruda_Pabianicka"
  ]
  node [
    id 1814
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1815
    label "Ludwin&#243;w"
  ]
  node [
    id 1816
    label "Arktyka"
  ]
  node [
    id 1817
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1818
    label "Zabu&#380;e"
  ]
  node [
    id 1819
    label "antroposfera"
  ]
  node [
    id 1820
    label "terytorium"
  ]
  node [
    id 1821
    label "Neogea"
  ]
  node [
    id 1822
    label "Syberia_Zachodnia"
  ]
  node [
    id 1823
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1824
    label "pas_planetoid"
  ]
  node [
    id 1825
    label "Syberia_Wschodnia"
  ]
  node [
    id 1826
    label "Antarktyka"
  ]
  node [
    id 1827
    label "Rakowice"
  ]
  node [
    id 1828
    label "akrecja"
  ]
  node [
    id 1829
    label "wymiar"
  ]
  node [
    id 1830
    label "&#321;&#281;g"
  ]
  node [
    id 1831
    label "Kresy_Zachodnie"
  ]
  node [
    id 1832
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1833
    label "Notogea"
  ]
  node [
    id 1834
    label "series"
  ]
  node [
    id 1835
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1836
    label "dane"
  ]
  node [
    id 1837
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1838
    label "pakiet_klimatyczny"
  ]
  node [
    id 1839
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1840
    label "sum"
  ]
  node [
    id 1841
    label "gathering"
  ]
  node [
    id 1842
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1843
    label "Kanada"
  ]
  node [
    id 1844
    label "jednostka_administracyjna"
  ]
  node [
    id 1845
    label "Jukon"
  ]
  node [
    id 1846
    label "warunek_lokalowy"
  ]
  node [
    id 1847
    label "plac"
  ]
  node [
    id 1848
    label "location"
  ]
  node [
    id 1849
    label "uwaga"
  ]
  node [
    id 1850
    label "status"
  ]
  node [
    id 1851
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1852
    label "cia&#322;o"
  ]
  node [
    id 1853
    label "parametr"
  ]
  node [
    id 1854
    label "poziom"
  ]
  node [
    id 1855
    label "znaczenie"
  ]
  node [
    id 1856
    label "dymensja"
  ]
  node [
    id 1857
    label "integer"
  ]
  node [
    id 1858
    label "zlewanie_si&#281;"
  ]
  node [
    id 1859
    label "uk&#322;ad"
  ]
  node [
    id 1860
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1861
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1862
    label "pe&#322;ny"
  ]
  node [
    id 1863
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1864
    label "sunset"
  ]
  node [
    id 1865
    label "szar&#243;wka"
  ]
  node [
    id 1866
    label "usi&#322;owanie"
  ]
  node [
    id 1867
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1868
    label "trud"
  ]
  node [
    id 1869
    label "brzask"
  ]
  node [
    id 1870
    label "pocz&#261;tek"
  ]
  node [
    id 1871
    label "szabas"
  ]
  node [
    id 1872
    label "Boreasz"
  ]
  node [
    id 1873
    label "&#347;wiat"
  ]
  node [
    id 1874
    label "p&#243;&#322;nocek"
  ]
  node [
    id 1875
    label "Podg&#243;rze"
  ]
  node [
    id 1876
    label "palearktyka"
  ]
  node [
    id 1877
    label "nearktyka"
  ]
  node [
    id 1878
    label "Serbia"
  ]
  node [
    id 1879
    label "euro"
  ]
  node [
    id 1880
    label "Bie&#380;an&#243;w-Prokocim"
  ]
  node [
    id 1881
    label "Warszawa"
  ]
  node [
    id 1882
    label "Kaw&#281;czyn"
  ]
  node [
    id 1883
    label "Kresy"
  ]
  node [
    id 1884
    label "biosfera"
  ]
  node [
    id 1885
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 1886
    label "D&#281;bniki"
  ]
  node [
    id 1887
    label "lodowiec_kontynentalny"
  ]
  node [
    id 1888
    label "Antarktyda"
  ]
  node [
    id 1889
    label "Pr&#261;dnik_Czerwony"
  ]
  node [
    id 1890
    label "Czy&#380;yny"
  ]
  node [
    id 1891
    label "Zwierzyniec"
  ]
  node [
    id 1892
    label "Rataje"
  ]
  node [
    id 1893
    label "G&#322;uszyna"
  ]
  node [
    id 1894
    label "rozrost"
  ]
  node [
    id 1895
    label "wzrost"
  ]
  node [
    id 1896
    label "dysk_akrecyjny"
  ]
  node [
    id 1897
    label "accretion"
  ]
  node [
    id 1898
    label "wsiowo"
  ]
  node [
    id 1899
    label "nieatrakcyjny"
  ]
  node [
    id 1900
    label "obciachowy"
  ]
  node [
    id 1901
    label "wie&#347;ny"
  ]
  node [
    id 1902
    label "wsiowy"
  ]
  node [
    id 1903
    label "wiejsko"
  ]
  node [
    id 1904
    label "po_wiejsku"
  ]
  node [
    id 1905
    label "beznadziejny"
  ]
  node [
    id 1906
    label "nieatrakcyjnie"
  ]
  node [
    id 1907
    label "zwyczajny"
  ]
  node [
    id 1908
    label "typowo"
  ]
  node [
    id 1909
    label "cz&#281;sty"
  ]
  node [
    id 1910
    label "zwyk&#322;y"
  ]
  node [
    id 1911
    label "prowincjusz"
  ]
  node [
    id 1912
    label "wie&#347;"
  ]
  node [
    id 1913
    label "rurally"
  ]
  node [
    id 1914
    label "rzadko&#347;&#263;"
  ]
  node [
    id 1915
    label "zaleta"
  ]
  node [
    id 1916
    label "measure"
  ]
  node [
    id 1917
    label "opinia"
  ]
  node [
    id 1918
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 1919
    label "property"
  ]
  node [
    id 1920
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1921
    label "Ural"
  ]
  node [
    id 1922
    label "miara"
  ]
  node [
    id 1923
    label "end"
  ]
  node [
    id 1924
    label "pu&#322;ap"
  ]
  node [
    id 1925
    label "koniec"
  ]
  node [
    id 1926
    label "granice"
  ]
  node [
    id 1927
    label "frontier"
  ]
  node [
    id 1928
    label "strefa"
  ]
  node [
    id 1929
    label "kula"
  ]
  node [
    id 1930
    label "class"
  ]
  node [
    id 1931
    label "sector"
  ]
  node [
    id 1932
    label "p&#243;&#322;kula"
  ]
  node [
    id 1933
    label "huczek"
  ]
  node [
    id 1934
    label "p&#243;&#322;sfera"
  ]
  node [
    id 1935
    label "powierzchnia"
  ]
  node [
    id 1936
    label "kolur"
  ]
  node [
    id 1937
    label "odpowiednik"
  ]
  node [
    id 1938
    label "designatum"
  ]
  node [
    id 1939
    label "nazwa_rzetelna"
  ]
  node [
    id 1940
    label "nazwa_pozorna"
  ]
  node [
    id 1941
    label "denotacja"
  ]
  node [
    id 1942
    label "absolutorium"
  ]
  node [
    id 1943
    label "dzia&#322;anie"
  ]
  node [
    id 1944
    label "activity"
  ]
  node [
    id 1945
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 1946
    label "oparcie"
  ]
  node [
    id 1947
    label "darowizna"
  ]
  node [
    id 1948
    label "zapomoga"
  ]
  node [
    id 1949
    label "comfort"
  ]
  node [
    id 1950
    label "pocieszenie"
  ]
  node [
    id 1951
    label "telefon_zaufania"
  ]
  node [
    id 1952
    label "dar"
  ]
  node [
    id 1953
    label "support"
  ]
  node [
    id 1954
    label "u&#322;atwienie"
  ]
  node [
    id 1955
    label "pomoc"
  ]
  node [
    id 1956
    label "income"
  ]
  node [
    id 1957
    label "stopa_procentowa"
  ]
  node [
    id 1958
    label "krzywa_Engla"
  ]
  node [
    id 1959
    label "korzy&#347;&#263;"
  ]
  node [
    id 1960
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 1961
    label "wp&#322;yw"
  ]
  node [
    id 1962
    label "dyspozycja"
  ]
  node [
    id 1963
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1964
    label "da&#324;"
  ]
  node [
    id 1965
    label "faculty"
  ]
  node [
    id 1966
    label "stygmat"
  ]
  node [
    id 1967
    label "dobro"
  ]
  node [
    id 1968
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 1969
    label "liga"
  ]
  node [
    id 1970
    label "pomocnik"
  ]
  node [
    id 1971
    label "zgodzi&#263;"
  ]
  node [
    id 1972
    label "ukojenie"
  ]
  node [
    id 1973
    label "pomo&#380;enie"
  ]
  node [
    id 1974
    label "facilitation"
  ]
  node [
    id 1975
    label "abstrakcja"
  ]
  node [
    id 1976
    label "chemikalia"
  ]
  node [
    id 1977
    label "substancja"
  ]
  node [
    id 1978
    label "back"
  ]
  node [
    id 1979
    label "podpora"
  ]
  node [
    id 1980
    label "przeniesienie_praw"
  ]
  node [
    id 1981
    label "transakcja"
  ]
  node [
    id 1982
    label "wykonawca"
  ]
  node [
    id 1983
    label "interpretator"
  ]
  node [
    id 1984
    label "consultancy"
  ]
  node [
    id 1985
    label "us&#322;ugi"
  ]
  node [
    id 1986
    label "dzia&#322;alno&#347;&#263;_gospodarcza"
  ]
  node [
    id 1987
    label "service"
  ]
  node [
    id 1988
    label "rolniczo"
  ]
  node [
    id 1989
    label "przyrodniczo"
  ]
  node [
    id 1990
    label "intencjonalny"
  ]
  node [
    id 1991
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 1992
    label "niedorozw&#243;j"
  ]
  node [
    id 1993
    label "szczeg&#243;lny"
  ]
  node [
    id 1994
    label "specjalnie"
  ]
  node [
    id 1995
    label "nieetatowy"
  ]
  node [
    id 1996
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 1997
    label "nienormalny"
  ]
  node [
    id 1998
    label "umy&#347;lnie"
  ]
  node [
    id 1999
    label "odpowiedni"
  ]
  node [
    id 2000
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 2001
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 2002
    label "wprawia&#263;"
  ]
  node [
    id 2003
    label "wpisywa&#263;"
  ]
  node [
    id 2004
    label "wchodzi&#263;"
  ]
  node [
    id 2005
    label "take"
  ]
  node [
    id 2006
    label "inflict"
  ]
  node [
    id 2007
    label "schodzi&#263;"
  ]
  node [
    id 2008
    label "induct"
  ]
  node [
    id 2009
    label "begin"
  ]
  node [
    id 2010
    label "zawiera&#263;"
  ]
  node [
    id 2011
    label "poznawa&#263;"
  ]
  node [
    id 2012
    label "obznajamia&#263;"
  ]
  node [
    id 2013
    label "go_steady"
  ]
  node [
    id 2014
    label "informowa&#263;"
  ]
  node [
    id 2015
    label "odejmowa&#263;"
  ]
  node [
    id 2016
    label "bankrupt"
  ]
  node [
    id 2017
    label "open"
  ]
  node [
    id 2018
    label "set_about"
  ]
  node [
    id 2019
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 2020
    label "post&#281;powa&#263;"
  ]
  node [
    id 2021
    label "pull"
  ]
  node [
    id 2022
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 2023
    label "write"
  ]
  node [
    id 2024
    label "pisa&#263;"
  ]
  node [
    id 2025
    label "read"
  ]
  node [
    id 2026
    label "rig"
  ]
  node [
    id 2027
    label "message"
  ]
  node [
    id 2028
    label "wykonywa&#263;"
  ]
  node [
    id 2029
    label "prowadzi&#263;"
  ]
  node [
    id 2030
    label "moderate"
  ]
  node [
    id 2031
    label "doskonali&#263;"
  ]
  node [
    id 2032
    label "bind"
  ]
  node [
    id 2033
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 2034
    label "plasowa&#263;"
  ]
  node [
    id 2035
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 2036
    label "pomieszcza&#263;"
  ]
  node [
    id 2037
    label "venture"
  ]
  node [
    id 2038
    label "okre&#347;la&#263;"
  ]
  node [
    id 2039
    label "organizowa&#263;"
  ]
  node [
    id 2040
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 2041
    label "czyni&#263;"
  ]
  node [
    id 2042
    label "stylizowa&#263;"
  ]
  node [
    id 2043
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 2044
    label "falowa&#263;"
  ]
  node [
    id 2045
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 2046
    label "peddle"
  ]
  node [
    id 2047
    label "wydala&#263;"
  ]
  node [
    id 2048
    label "tentegowa&#263;"
  ]
  node [
    id 2049
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 2050
    label "urz&#261;dza&#263;"
  ]
  node [
    id 2051
    label "oszukiwa&#263;"
  ]
  node [
    id 2052
    label "ukazywa&#263;"
  ]
  node [
    id 2053
    label "przerabia&#263;"
  ]
  node [
    id 2054
    label "interrupt"
  ]
  node [
    id 2055
    label "narusza&#263;"
  ]
  node [
    id 2056
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 2057
    label "zaziera&#263;"
  ]
  node [
    id 2058
    label "spotyka&#263;"
  ]
  node [
    id 2059
    label "przenika&#263;"
  ]
  node [
    id 2060
    label "osi&#261;ga&#263;"
  ]
  node [
    id 2061
    label "&#322;oi&#263;"
  ]
  node [
    id 2062
    label "intervene"
  ]
  node [
    id 2063
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 2064
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 2065
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 2066
    label "dochodzi&#263;"
  ]
  node [
    id 2067
    label "przekracza&#263;"
  ]
  node [
    id 2068
    label "wnika&#263;"
  ]
  node [
    id 2069
    label "invade"
  ]
  node [
    id 2070
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 2071
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 2072
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 2073
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 2074
    label "motywowa&#263;"
  ]
  node [
    id 2075
    label "opuszcza&#263;"
  ]
  node [
    id 2076
    label "digress"
  ]
  node [
    id 2077
    label "obni&#380;a&#263;"
  ]
  node [
    id 2078
    label "przenosi&#263;_si&#281;"
  ]
  node [
    id 2079
    label "wschodzi&#263;"
  ]
  node [
    id 2080
    label "ubywa&#263;"
  ]
  node [
    id 2081
    label "mija&#263;"
  ]
  node [
    id 2082
    label "odpada&#263;"
  ]
  node [
    id 2083
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 2084
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 2085
    label "podrze&#263;"
  ]
  node [
    id 2086
    label "umiera&#263;"
  ]
  node [
    id 2087
    label "i&#347;&#263;"
  ]
  node [
    id 2088
    label "&#347;piewa&#263;"
  ]
  node [
    id 2089
    label "obni&#380;a&#263;_si&#281;"
  ]
  node [
    id 2090
    label "gin&#261;&#263;"
  ]
  node [
    id 2091
    label "przestawa&#263;"
  ]
  node [
    id 2092
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 2093
    label "authorize"
  ]
  node [
    id 2094
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 2095
    label "odpuszcza&#263;"
  ]
  node [
    id 2096
    label "zu&#380;y&#263;"
  ]
  node [
    id 2097
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 2098
    label "refuse"
  ]
  node [
    id 2099
    label "stoisko"
  ]
  node [
    id 2100
    label "rynek_podstawowy"
  ]
  node [
    id 2101
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 2102
    label "konsument"
  ]
  node [
    id 2103
    label "pojawienie_si&#281;"
  ]
  node [
    id 2104
    label "obiekt_handlowy"
  ]
  node [
    id 2105
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 2106
    label "wytw&#243;rca"
  ]
  node [
    id 2107
    label "rynek_wt&#243;rny"
  ]
  node [
    id 2108
    label "kram"
  ]
  node [
    id 2109
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 2110
    label "segment_rynku"
  ]
  node [
    id 2111
    label "targowica"
  ]
  node [
    id 2112
    label "okre&#347;lony"
  ]
  node [
    id 2113
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 2114
    label "wiadomy"
  ]
  node [
    id 2115
    label "rewizja"
  ]
  node [
    id 2116
    label "passage"
  ]
  node [
    id 2117
    label "oznaka"
  ]
  node [
    id 2118
    label "ferment"
  ]
  node [
    id 2119
    label "komplet"
  ]
  node [
    id 2120
    label "anatomopatolog"
  ]
  node [
    id 2121
    label "zmianka"
  ]
  node [
    id 2122
    label "amendment"
  ]
  node [
    id 2123
    label "odmienianie"
  ]
  node [
    id 2124
    label "tura"
  ]
  node [
    id 2125
    label "boski"
  ]
  node [
    id 2126
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 2127
    label "przywidzenie"
  ]
  node [
    id 2128
    label "presence"
  ]
  node [
    id 2129
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 2130
    label "lekcja"
  ]
  node [
    id 2131
    label "ensemble"
  ]
  node [
    id 2132
    label "implikowa&#263;"
  ]
  node [
    id 2133
    label "signal"
  ]
  node [
    id 2134
    label "symbol"
  ]
  node [
    id 2135
    label "bia&#322;ko"
  ]
  node [
    id 2136
    label "immobilizowa&#263;"
  ]
  node [
    id 2137
    label "immobilizacja"
  ]
  node [
    id 2138
    label "apoenzym"
  ]
  node [
    id 2139
    label "zymaza"
  ]
  node [
    id 2140
    label "enzyme"
  ]
  node [
    id 2141
    label "immobilizowanie"
  ]
  node [
    id 2142
    label "biokatalizator"
  ]
  node [
    id 2143
    label "dow&#243;d"
  ]
  node [
    id 2144
    label "krytyka"
  ]
  node [
    id 2145
    label "rekurs"
  ]
  node [
    id 2146
    label "checkup"
  ]
  node [
    id 2147
    label "kontrola"
  ]
  node [
    id 2148
    label "odwo&#322;anie"
  ]
  node [
    id 2149
    label "correction"
  ]
  node [
    id 2150
    label "przegl&#261;d"
  ]
  node [
    id 2151
    label "kipisz"
  ]
  node [
    id 2152
    label "korekta"
  ]
  node [
    id 2153
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 2154
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 2155
    label "najem"
  ]
  node [
    id 2156
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 2157
    label "zak&#322;ad"
  ]
  node [
    id 2158
    label "stosunek_pracy"
  ]
  node [
    id 2159
    label "benedykty&#324;ski"
  ]
  node [
    id 2160
    label "poda&#380;_pracy"
  ]
  node [
    id 2161
    label "pracowanie"
  ]
  node [
    id 2162
    label "tyrka"
  ]
  node [
    id 2163
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 2164
    label "zaw&#243;d"
  ]
  node [
    id 2165
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 2166
    label "tynkarski"
  ]
  node [
    id 2167
    label "pracowa&#263;"
  ]
  node [
    id 2168
    label "czynnik_produkcji"
  ]
  node [
    id 2169
    label "zobowi&#261;zanie"
  ]
  node [
    id 2170
    label "kierownictwo"
  ]
  node [
    id 2171
    label "siedziba"
  ]
  node [
    id 2172
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 2173
    label "patolog"
  ]
  node [
    id 2174
    label "anatom"
  ]
  node [
    id 2175
    label "sparafrazowanie"
  ]
  node [
    id 2176
    label "zmienianie"
  ]
  node [
    id 2177
    label "parafrazowanie"
  ]
  node [
    id 2178
    label "zamiana"
  ]
  node [
    id 2179
    label "wymienianie"
  ]
  node [
    id 2180
    label "Transfiguration"
  ]
  node [
    id 2181
    label "przeobra&#380;anie_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 43
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 24
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 43
  ]
  edge [
    source 13
    target 44
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 757
  ]
  edge [
    source 14
    target 758
  ]
  edge [
    source 14
    target 759
  ]
  edge [
    source 14
    target 760
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 762
  ]
  edge [
    source 14
    target 763
  ]
  edge [
    source 14
    target 764
  ]
  edge [
    source 14
    target 765
  ]
  edge [
    source 14
    target 766
  ]
  edge [
    source 14
    target 749
  ]
  edge [
    source 14
    target 767
  ]
  edge [
    source 14
    target 768
  ]
  edge [
    source 14
    target 769
  ]
  edge [
    source 14
    target 770
  ]
  edge [
    source 14
    target 771
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 773
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 14
    target 777
  ]
  edge [
    source 14
    target 778
  ]
  edge [
    source 14
    target 779
  ]
  edge [
    source 14
    target 780
  ]
  edge [
    source 14
    target 781
  ]
  edge [
    source 14
    target 782
  ]
  edge [
    source 14
    target 783
  ]
  edge [
    source 14
    target 744
  ]
  edge [
    source 14
    target 784
  ]
  edge [
    source 14
    target 43
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 201
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 15
    target 206
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 209
  ]
  edge [
    source 15
    target 210
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 215
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 219
  ]
  edge [
    source 15
    target 220
  ]
  edge [
    source 15
    target 221
  ]
  edge [
    source 15
    target 222
  ]
  edge [
    source 15
    target 223
  ]
  edge [
    source 15
    target 224
  ]
  edge [
    source 15
    target 225
  ]
  edge [
    source 15
    target 226
  ]
  edge [
    source 15
    target 227
  ]
  edge [
    source 15
    target 228
  ]
  edge [
    source 15
    target 229
  ]
  edge [
    source 15
    target 83
  ]
  edge [
    source 15
    target 230
  ]
  edge [
    source 15
    target 231
  ]
  edge [
    source 15
    target 232
  ]
  edge [
    source 15
    target 233
  ]
  edge [
    source 15
    target 234
  ]
  edge [
    source 15
    target 235
  ]
  edge [
    source 15
    target 236
  ]
  edge [
    source 15
    target 237
  ]
  edge [
    source 15
    target 238
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 239
  ]
  edge [
    source 15
    target 240
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 477
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 83
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 17
    target 866
  ]
  edge [
    source 17
    target 867
  ]
  edge [
    source 17
    target 868
  ]
  edge [
    source 17
    target 869
  ]
  edge [
    source 17
    target 870
  ]
  edge [
    source 17
    target 871
  ]
  edge [
    source 17
    target 872
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 567
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 894
  ]
  edge [
    source 17
    target 895
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 17
    target 897
  ]
  edge [
    source 17
    target 898
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 900
  ]
  edge [
    source 17
    target 901
  ]
  edge [
    source 17
    target 902
  ]
  edge [
    source 17
    target 903
  ]
  edge [
    source 17
    target 904
  ]
  edge [
    source 17
    target 905
  ]
  edge [
    source 17
    target 906
  ]
  edge [
    source 17
    target 907
  ]
  edge [
    source 17
    target 908
  ]
  edge [
    source 17
    target 909
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 911
  ]
  edge [
    source 17
    target 912
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 914
  ]
  edge [
    source 17
    target 915
  ]
  edge [
    source 17
    target 916
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 918
  ]
  edge [
    source 17
    target 529
  ]
  edge [
    source 17
    target 919
  ]
  edge [
    source 17
    target 920
  ]
  edge [
    source 17
    target 921
  ]
  edge [
    source 17
    target 922
  ]
  edge [
    source 17
    target 923
  ]
  edge [
    source 17
    target 924
  ]
  edge [
    source 17
    target 925
  ]
  edge [
    source 17
    target 926
  ]
  edge [
    source 17
    target 927
  ]
  edge [
    source 17
    target 70
  ]
  edge [
    source 17
    target 928
  ]
  edge [
    source 17
    target 929
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 17
    target 463
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 968
  ]
  edge [
    source 17
    target 969
  ]
  edge [
    source 17
    target 970
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 972
  ]
  edge [
    source 17
    target 973
  ]
  edge [
    source 17
    target 974
  ]
  edge [
    source 17
    target 975
  ]
  edge [
    source 17
    target 976
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 19
    target 408
  ]
  edge [
    source 19
    target 325
  ]
  edge [
    source 19
    target 438
  ]
  edge [
    source 19
    target 439
  ]
  edge [
    source 19
    target 328
  ]
  edge [
    source 19
    target 440
  ]
  edge [
    source 19
    target 441
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 662
  ]
  edge [
    source 20
    target 978
  ]
  edge [
    source 20
    target 664
  ]
  edge [
    source 20
    target 979
  ]
  edge [
    source 20
    target 223
  ]
  edge [
    source 20
    target 980
  ]
  edge [
    source 20
    target 89
  ]
  edge [
    source 20
    target 666
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 981
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 83
  ]
  edge [
    source 20
    target 982
  ]
  edge [
    source 20
    target 983
  ]
  edge [
    source 20
    target 984
  ]
  edge [
    source 20
    target 985
  ]
  edge [
    source 20
    target 986
  ]
  edge [
    source 20
    target 686
  ]
  edge [
    source 20
    target 987
  ]
  edge [
    source 20
    target 988
  ]
  edge [
    source 20
    target 613
  ]
  edge [
    source 20
    target 989
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 61
  ]
  edge [
    source 20
    target 990
  ]
  edge [
    source 20
    target 48
  ]
  edge [
    source 20
    target 991
  ]
  edge [
    source 20
    target 273
  ]
  edge [
    source 20
    target 992
  ]
  edge [
    source 20
    target 651
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 993
  ]
  edge [
    source 20
    target 994
  ]
  edge [
    source 20
    target 995
  ]
  edge [
    source 20
    target 704
  ]
  edge [
    source 20
    target 996
  ]
  edge [
    source 20
    target 997
  ]
  edge [
    source 20
    target 463
  ]
  edge [
    source 20
    target 998
  ]
  edge [
    source 20
    target 999
  ]
  edge [
    source 20
    target 731
  ]
  edge [
    source 20
    target 1000
  ]
  edge [
    source 20
    target 1001
  ]
  edge [
    source 20
    target 1002
  ]
  edge [
    source 20
    target 1003
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 20
    target 1004
  ]
  edge [
    source 20
    target 1005
  ]
  edge [
    source 20
    target 1006
  ]
  edge [
    source 20
    target 1007
  ]
  edge [
    source 20
    target 597
  ]
  edge [
    source 20
    target 1008
  ]
  edge [
    source 20
    target 1009
  ]
  edge [
    source 20
    target 1010
  ]
  edge [
    source 20
    target 1011
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 1012
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 1013
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 1014
  ]
  edge [
    source 20
    target 1015
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 483
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 1016
  ]
  edge [
    source 20
    target 1017
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 30
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1018
  ]
  edge [
    source 21
    target 548
  ]
  edge [
    source 21
    target 1019
  ]
  edge [
    source 21
    target 1020
  ]
  edge [
    source 21
    target 1012
  ]
  edge [
    source 21
    target 350
  ]
  edge [
    source 21
    target 1021
  ]
  edge [
    source 21
    target 1022
  ]
  edge [
    source 21
    target 1023
  ]
  edge [
    source 21
    target 363
  ]
  edge [
    source 21
    target 48
  ]
  edge [
    source 21
    target 1024
  ]
  edge [
    source 21
    target 1025
  ]
  edge [
    source 21
    target 1026
  ]
  edge [
    source 21
    target 1027
  ]
  edge [
    source 21
    target 1028
  ]
  edge [
    source 21
    target 1029
  ]
  edge [
    source 21
    target 1030
  ]
  edge [
    source 21
    target 617
  ]
  edge [
    source 21
    target 1031
  ]
  edge [
    source 21
    target 1032
  ]
  edge [
    source 21
    target 1033
  ]
  edge [
    source 21
    target 1034
  ]
  edge [
    source 21
    target 1035
  ]
  edge [
    source 21
    target 1036
  ]
  edge [
    source 21
    target 1037
  ]
  edge [
    source 21
    target 1038
  ]
  edge [
    source 21
    target 1039
  ]
  edge [
    source 21
    target 1040
  ]
  edge [
    source 21
    target 1041
  ]
  edge [
    source 21
    target 1042
  ]
  edge [
    source 21
    target 1043
  ]
  edge [
    source 21
    target 114
  ]
  edge [
    source 21
    target 1044
  ]
  edge [
    source 21
    target 1045
  ]
  edge [
    source 21
    target 1046
  ]
  edge [
    source 21
    target 1047
  ]
  edge [
    source 21
    target 540
  ]
  edge [
    source 21
    target 1048
  ]
  edge [
    source 21
    target 1049
  ]
  edge [
    source 21
    target 1050
  ]
  edge [
    source 21
    target 1051
  ]
  edge [
    source 21
    target 1052
  ]
  edge [
    source 21
    target 1053
  ]
  edge [
    source 21
    target 70
  ]
  edge [
    source 21
    target 1054
  ]
  edge [
    source 21
    target 462
  ]
  edge [
    source 21
    target 567
  ]
  edge [
    source 21
    target 1055
  ]
  edge [
    source 21
    target 1056
  ]
  edge [
    source 21
    target 1057
  ]
  edge [
    source 21
    target 1058
  ]
  edge [
    source 21
    target 1059
  ]
  edge [
    source 21
    target 1060
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 1061
  ]
  edge [
    source 21
    target 1062
  ]
  edge [
    source 21
    target 1063
  ]
  edge [
    source 21
    target 1064
  ]
  edge [
    source 21
    target 1065
  ]
  edge [
    source 21
    target 1066
  ]
  edge [
    source 21
    target 1067
  ]
  edge [
    source 21
    target 1068
  ]
  edge [
    source 21
    target 1069
  ]
  edge [
    source 21
    target 1070
  ]
  edge [
    source 21
    target 1071
  ]
  edge [
    source 21
    target 1072
  ]
  edge [
    source 21
    target 1073
  ]
  edge [
    source 21
    target 1074
  ]
  edge [
    source 21
    target 1075
  ]
  edge [
    source 21
    target 1076
  ]
  edge [
    source 21
    target 1077
  ]
  edge [
    source 21
    target 1078
  ]
  edge [
    source 21
    target 1079
  ]
  edge [
    source 21
    target 1080
  ]
  edge [
    source 21
    target 1081
  ]
  edge [
    source 21
    target 1082
  ]
  edge [
    source 21
    target 1083
  ]
  edge [
    source 21
    target 1084
  ]
  edge [
    source 21
    target 1085
  ]
  edge [
    source 21
    target 1086
  ]
  edge [
    source 21
    target 1087
  ]
  edge [
    source 21
    target 1088
  ]
  edge [
    source 21
    target 1089
  ]
  edge [
    source 21
    target 1090
  ]
  edge [
    source 21
    target 1091
  ]
  edge [
    source 21
    target 1092
  ]
  edge [
    source 21
    target 1093
  ]
  edge [
    source 21
    target 1094
  ]
  edge [
    source 21
    target 1095
  ]
  edge [
    source 21
    target 1096
  ]
  edge [
    source 21
    target 1097
  ]
  edge [
    source 21
    target 1098
  ]
  edge [
    source 21
    target 1099
  ]
  edge [
    source 21
    target 525
  ]
  edge [
    source 21
    target 1100
  ]
  edge [
    source 21
    target 1101
  ]
  edge [
    source 21
    target 1102
  ]
  edge [
    source 21
    target 1103
  ]
  edge [
    source 21
    target 389
  ]
  edge [
    source 21
    target 1104
  ]
  edge [
    source 21
    target 1105
  ]
  edge [
    source 21
    target 1106
  ]
  edge [
    source 21
    target 1107
  ]
  edge [
    source 21
    target 1108
  ]
  edge [
    source 21
    target 1109
  ]
  edge [
    source 21
    target 328
  ]
  edge [
    source 21
    target 1110
  ]
  edge [
    source 21
    target 1111
  ]
  edge [
    source 21
    target 1112
  ]
  edge [
    source 21
    target 1113
  ]
  edge [
    source 21
    target 757
  ]
  edge [
    source 21
    target 1114
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1115
  ]
  edge [
    source 22
    target 1116
  ]
  edge [
    source 22
    target 1117
  ]
  edge [
    source 22
    target 1118
  ]
  edge [
    source 22
    target 1119
  ]
  edge [
    source 22
    target 1120
  ]
  edge [
    source 22
    target 1121
  ]
  edge [
    source 22
    target 1122
  ]
  edge [
    source 22
    target 1123
  ]
  edge [
    source 22
    target 1124
  ]
  edge [
    source 22
    target 1125
  ]
  edge [
    source 22
    target 1126
  ]
  edge [
    source 22
    target 1127
  ]
  edge [
    source 22
    target 1128
  ]
  edge [
    source 22
    target 1129
  ]
  edge [
    source 22
    target 1130
  ]
  edge [
    source 22
    target 1131
  ]
  edge [
    source 22
    target 1132
  ]
  edge [
    source 22
    target 1133
  ]
  edge [
    source 22
    target 1134
  ]
  edge [
    source 22
    target 1135
  ]
  edge [
    source 22
    target 1136
  ]
  edge [
    source 22
    target 1137
  ]
  edge [
    source 22
    target 769
  ]
  edge [
    source 22
    target 1138
  ]
  edge [
    source 22
    target 1139
  ]
  edge [
    source 22
    target 1140
  ]
  edge [
    source 22
    target 1141
  ]
  edge [
    source 22
    target 1142
  ]
  edge [
    source 22
    target 1143
  ]
  edge [
    source 22
    target 1144
  ]
  edge [
    source 22
    target 1145
  ]
  edge [
    source 22
    target 1146
  ]
  edge [
    source 22
    target 1147
  ]
  edge [
    source 22
    target 1148
  ]
  edge [
    source 22
    target 740
  ]
  edge [
    source 22
    target 1149
  ]
  edge [
    source 22
    target 1150
  ]
  edge [
    source 22
    target 1151
  ]
  edge [
    source 22
    target 1152
  ]
  edge [
    source 22
    target 1153
  ]
  edge [
    source 22
    target 1154
  ]
  edge [
    source 22
    target 1155
  ]
  edge [
    source 22
    target 1156
  ]
  edge [
    source 22
    target 1157
  ]
  edge [
    source 22
    target 1158
  ]
  edge [
    source 22
    target 1159
  ]
  edge [
    source 22
    target 1160
  ]
  edge [
    source 22
    target 1161
  ]
  edge [
    source 22
    target 1162
  ]
  edge [
    source 22
    target 1163
  ]
  edge [
    source 22
    target 1164
  ]
  edge [
    source 22
    target 1165
  ]
  edge [
    source 22
    target 757
  ]
  edge [
    source 22
    target 1166
  ]
  edge [
    source 22
    target 1167
  ]
  edge [
    source 22
    target 1168
  ]
  edge [
    source 22
    target 1169
  ]
  edge [
    source 22
    target 1170
  ]
  edge [
    source 22
    target 1171
  ]
  edge [
    source 22
    target 1172
  ]
  edge [
    source 22
    target 1173
  ]
  edge [
    source 22
    target 1174
  ]
  edge [
    source 22
    target 1175
  ]
  edge [
    source 22
    target 1176
  ]
  edge [
    source 22
    target 1177
  ]
  edge [
    source 22
    target 1178
  ]
  edge [
    source 22
    target 1179
  ]
  edge [
    source 22
    target 1180
  ]
  edge [
    source 22
    target 1181
  ]
  edge [
    source 22
    target 934
  ]
  edge [
    source 22
    target 1182
  ]
  edge [
    source 22
    target 1183
  ]
  edge [
    source 22
    target 1184
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 1186
  ]
  edge [
    source 22
    target 1187
  ]
  edge [
    source 22
    target 1188
  ]
  edge [
    source 22
    target 1189
  ]
  edge [
    source 22
    target 1190
  ]
  edge [
    source 22
    target 1191
  ]
  edge [
    source 22
    target 1192
  ]
  edge [
    source 22
    target 1193
  ]
  edge [
    source 22
    target 1194
  ]
  edge [
    source 22
    target 1195
  ]
  edge [
    source 22
    target 767
  ]
  edge [
    source 22
    target 1196
  ]
  edge [
    source 22
    target 1197
  ]
  edge [
    source 22
    target 1198
  ]
  edge [
    source 22
    target 1095
  ]
  edge [
    source 22
    target 1199
  ]
  edge [
    source 22
    target 1200
  ]
  edge [
    source 22
    target 749
  ]
  edge [
    source 22
    target 1201
  ]
  edge [
    source 22
    target 1202
  ]
  edge [
    source 22
    target 1203
  ]
  edge [
    source 22
    target 1204
  ]
  edge [
    source 22
    target 1205
  ]
  edge [
    source 22
    target 1206
  ]
  edge [
    source 22
    target 1207
  ]
  edge [
    source 22
    target 759
  ]
  edge [
    source 22
    target 1208
  ]
  edge [
    source 22
    target 1209
  ]
  edge [
    source 22
    target 1210
  ]
  edge [
    source 22
    target 1211
  ]
  edge [
    source 22
    target 1212
  ]
  edge [
    source 22
    target 1213
  ]
  edge [
    source 22
    target 1214
  ]
  edge [
    source 22
    target 1215
  ]
  edge [
    source 22
    target 614
  ]
  edge [
    source 22
    target 1216
  ]
  edge [
    source 22
    target 1217
  ]
  edge [
    source 22
    target 1218
  ]
  edge [
    source 22
    target 1219
  ]
  edge [
    source 22
    target 1220
  ]
  edge [
    source 22
    target 1221
  ]
  edge [
    source 22
    target 1222
  ]
  edge [
    source 22
    target 1223
  ]
  edge [
    source 22
    target 1224
  ]
  edge [
    source 22
    target 1225
  ]
  edge [
    source 22
    target 1226
  ]
  edge [
    source 22
    target 1227
  ]
  edge [
    source 22
    target 1228
  ]
  edge [
    source 22
    target 1229
  ]
  edge [
    source 22
    target 1230
  ]
  edge [
    source 22
    target 747
  ]
  edge [
    source 22
    target 1231
  ]
  edge [
    source 22
    target 1232
  ]
  edge [
    source 22
    target 1233
  ]
  edge [
    source 22
    target 783
  ]
  edge [
    source 22
    target 1234
  ]
  edge [
    source 22
    target 1235
  ]
  edge [
    source 22
    target 1236
  ]
  edge [
    source 22
    target 1237
  ]
  edge [
    source 22
    target 1238
  ]
  edge [
    source 22
    target 1239
  ]
  edge [
    source 22
    target 1240
  ]
  edge [
    source 22
    target 1241
  ]
  edge [
    source 22
    target 1242
  ]
  edge [
    source 22
    target 1243
  ]
  edge [
    source 22
    target 1244
  ]
  edge [
    source 22
    target 1245
  ]
  edge [
    source 22
    target 1246
  ]
  edge [
    source 22
    target 1247
  ]
  edge [
    source 22
    target 1248
  ]
  edge [
    source 22
    target 1249
  ]
  edge [
    source 22
    target 1250
  ]
  edge [
    source 22
    target 1251
  ]
  edge [
    source 22
    target 1252
  ]
  edge [
    source 22
    target 1253
  ]
  edge [
    source 22
    target 70
  ]
  edge [
    source 22
    target 567
  ]
  edge [
    source 22
    target 1254
  ]
  edge [
    source 22
    target 1255
  ]
  edge [
    source 22
    target 1256
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 484
  ]
  edge [
    source 23
    target 1257
  ]
  edge [
    source 23
    target 551
  ]
  edge [
    source 23
    target 548
  ]
  edge [
    source 23
    target 549
  ]
  edge [
    source 23
    target 552
  ]
  edge [
    source 23
    target 1258
  ]
  edge [
    source 23
    target 556
  ]
  edge [
    source 23
    target 790
  ]
  edge [
    source 23
    target 559
  ]
  edge [
    source 23
    target 1259
  ]
  edge [
    source 23
    target 983
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 526
  ]
  edge [
    source 23
    target 531
  ]
  edge [
    source 23
    target 1260
  ]
  edge [
    source 23
    target 61
  ]
  edge [
    source 23
    target 1261
  ]
  edge [
    source 23
    target 1262
  ]
  edge [
    source 23
    target 534
  ]
  edge [
    source 23
    target 188
  ]
  edge [
    source 23
    target 176
  ]
  edge [
    source 23
    target 1263
  ]
  edge [
    source 23
    target 83
  ]
  edge [
    source 23
    target 1264
  ]
  edge [
    source 23
    target 1265
  ]
  edge [
    source 23
    target 525
  ]
  edge [
    source 23
    target 1100
  ]
  edge [
    source 23
    target 567
  ]
  edge [
    source 23
    target 1101
  ]
  edge [
    source 23
    target 1266
  ]
  edge [
    source 23
    target 1267
  ]
  edge [
    source 23
    target 1268
  ]
  edge [
    source 23
    target 1269
  ]
  edge [
    source 23
    target 1270
  ]
  edge [
    source 23
    target 467
  ]
  edge [
    source 23
    target 1271
  ]
  edge [
    source 23
    target 1272
  ]
  edge [
    source 23
    target 1273
  ]
  edge [
    source 23
    target 1274
  ]
  edge [
    source 23
    target 1275
  ]
  edge [
    source 23
    target 1276
  ]
  edge [
    source 23
    target 1277
  ]
  edge [
    source 23
    target 1278
  ]
  edge [
    source 23
    target 1279
  ]
  edge [
    source 23
    target 1280
  ]
  edge [
    source 23
    target 1281
  ]
  edge [
    source 23
    target 1282
  ]
  edge [
    source 23
    target 1109
  ]
  edge [
    source 23
    target 1283
  ]
  edge [
    source 23
    target 1284
  ]
  edge [
    source 23
    target 1285
  ]
  edge [
    source 23
    target 1286
  ]
  edge [
    source 23
    target 1287
  ]
  edge [
    source 23
    target 819
  ]
  edge [
    source 23
    target 1288
  ]
  edge [
    source 23
    target 1289
  ]
  edge [
    source 23
    target 1290
  ]
  edge [
    source 23
    target 1291
  ]
  edge [
    source 23
    target 1292
  ]
  edge [
    source 23
    target 1293
  ]
  edge [
    source 23
    target 1294
  ]
  edge [
    source 23
    target 823
  ]
  edge [
    source 23
    target 1295
  ]
  edge [
    source 23
    target 615
  ]
  edge [
    source 23
    target 563
  ]
  edge [
    source 23
    target 1296
  ]
  edge [
    source 23
    target 1013
  ]
  edge [
    source 23
    target 979
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 1016
  ]
  edge [
    source 23
    target 1012
  ]
  edge [
    source 23
    target 1008
  ]
  edge [
    source 23
    target 1297
  ]
  edge [
    source 23
    target 638
  ]
  edge [
    source 23
    target 1298
  ]
  edge [
    source 23
    target 1299
  ]
  edge [
    source 23
    target 1300
  ]
  edge [
    source 23
    target 1301
  ]
  edge [
    source 23
    target 1302
  ]
  edge [
    source 23
    target 1303
  ]
  edge [
    source 23
    target 1304
  ]
  edge [
    source 23
    target 1305
  ]
  edge [
    source 23
    target 389
  ]
  edge [
    source 23
    target 1306
  ]
  edge [
    source 23
    target 1307
  ]
  edge [
    source 23
    target 1308
  ]
  edge [
    source 23
    target 1309
  ]
  edge [
    source 23
    target 1310
  ]
  edge [
    source 23
    target 463
  ]
  edge [
    source 23
    target 1311
  ]
  edge [
    source 23
    target 1312
  ]
  edge [
    source 23
    target 1313
  ]
  edge [
    source 23
    target 677
  ]
  edge [
    source 23
    target 1314
  ]
  edge [
    source 23
    target 1315
  ]
  edge [
    source 23
    target 1316
  ]
  edge [
    source 23
    target 1317
  ]
  edge [
    source 23
    target 1318
  ]
  edge [
    source 23
    target 1319
  ]
  edge [
    source 23
    target 1320
  ]
  edge [
    source 23
    target 45
  ]
  edge [
    source 23
    target 1321
  ]
  edge [
    source 23
    target 1322
  ]
  edge [
    source 23
    target 1323
  ]
  edge [
    source 23
    target 1324
  ]
  edge [
    source 23
    target 1325
  ]
  edge [
    source 23
    target 1326
  ]
  edge [
    source 23
    target 1327
  ]
  edge [
    source 23
    target 1328
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 490
  ]
  edge [
    source 24
    target 1329
  ]
  edge [
    source 24
    target 80
  ]
  edge [
    source 24
    target 1330
  ]
  edge [
    source 24
    target 1331
  ]
  edge [
    source 24
    target 1332
  ]
  edge [
    source 24
    target 1009
  ]
  edge [
    source 24
    target 704
  ]
  edge [
    source 24
    target 461
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 24
    target 613
  ]
  edge [
    source 24
    target 612
  ]
  edge [
    source 24
    target 1333
  ]
  edge [
    source 24
    target 1334
  ]
  edge [
    source 24
    target 137
  ]
  edge [
    source 24
    target 61
  ]
  edge [
    source 24
    target 1335
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 179
  ]
  edge [
    source 24
    target 1336
  ]
  edge [
    source 24
    target 1337
  ]
  edge [
    source 24
    target 1338
  ]
  edge [
    source 24
    target 1339
  ]
  edge [
    source 24
    target 1340
  ]
  edge [
    source 24
    target 1341
  ]
  edge [
    source 24
    target 1342
  ]
  edge [
    source 24
    target 1343
  ]
  edge [
    source 24
    target 1344
  ]
  edge [
    source 24
    target 1345
  ]
  edge [
    source 24
    target 1346
  ]
  edge [
    source 24
    target 1347
  ]
  edge [
    source 24
    target 1348
  ]
  edge [
    source 24
    target 1349
  ]
  edge [
    source 24
    target 1350
  ]
  edge [
    source 24
    target 1351
  ]
  edge [
    source 24
    target 1352
  ]
  edge [
    source 24
    target 1353
  ]
  edge [
    source 24
    target 1354
  ]
  edge [
    source 24
    target 1355
  ]
  edge [
    source 24
    target 1356
  ]
  edge [
    source 24
    target 1357
  ]
  edge [
    source 24
    target 1358
  ]
  edge [
    source 24
    target 1359
  ]
  edge [
    source 24
    target 1360
  ]
  edge [
    source 24
    target 1361
  ]
  edge [
    source 24
    target 1362
  ]
  edge [
    source 24
    target 1363
  ]
  edge [
    source 24
    target 1364
  ]
  edge [
    source 24
    target 1365
  ]
  edge [
    source 24
    target 89
  ]
  edge [
    source 24
    target 1366
  ]
  edge [
    source 24
    target 1367
  ]
  edge [
    source 24
    target 944
  ]
  edge [
    source 24
    target 1368
  ]
  edge [
    source 24
    target 1369
  ]
  edge [
    source 24
    target 1370
  ]
  edge [
    source 24
    target 1371
  ]
  edge [
    source 24
    target 489
  ]
  edge [
    source 24
    target 493
  ]
  edge [
    source 24
    target 1372
  ]
  edge [
    source 24
    target 1373
  ]
  edge [
    source 24
    target 1374
  ]
  edge [
    source 24
    target 1375
  ]
  edge [
    source 24
    target 1376
  ]
  edge [
    source 24
    target 1012
  ]
  edge [
    source 24
    target 497
  ]
  edge [
    source 24
    target 1377
  ]
  edge [
    source 24
    target 488
  ]
  edge [
    source 24
    target 491
  ]
  edge [
    source 24
    target 1378
  ]
  edge [
    source 24
    target 494
  ]
  edge [
    source 24
    target 492
  ]
  edge [
    source 24
    target 1379
  ]
  edge [
    source 24
    target 1380
  ]
  edge [
    source 24
    target 496
  ]
  edge [
    source 24
    target 444
  ]
  edge [
    source 24
    target 495
  ]
  edge [
    source 24
    target 1381
  ]
  edge [
    source 24
    target 1382
  ]
  edge [
    source 24
    target 1324
  ]
  edge [
    source 24
    target 1383
  ]
  edge [
    source 24
    target 1384
  ]
  edge [
    source 24
    target 188
  ]
  edge [
    source 24
    target 498
  ]
  edge [
    source 24
    target 1385
  ]
  edge [
    source 24
    target 1386
  ]
  edge [
    source 24
    target 1387
  ]
  edge [
    source 24
    target 1388
  ]
  edge [
    source 24
    target 1389
  ]
  edge [
    source 24
    target 1390
  ]
  edge [
    source 24
    target 1391
  ]
  edge [
    source 24
    target 666
  ]
  edge [
    source 24
    target 44
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1392
  ]
  edge [
    source 25
    target 1393
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1394
  ]
  edge [
    source 26
    target 1395
  ]
  edge [
    source 26
    target 181
  ]
  edge [
    source 26
    target 463
  ]
  edge [
    source 26
    target 1374
  ]
  edge [
    source 26
    target 706
  ]
  edge [
    source 26
    target 1396
  ]
  edge [
    source 26
    target 1397
  ]
  edge [
    source 26
    target 581
  ]
  edge [
    source 26
    target 1398
  ]
  edge [
    source 26
    target 48
  ]
  edge [
    source 26
    target 1399
  ]
  edge [
    source 26
    target 1400
  ]
  edge [
    source 26
    target 1401
  ]
  edge [
    source 26
    target 1402
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 26
    target 467
  ]
  edge [
    source 26
    target 1403
  ]
  edge [
    source 26
    target 125
  ]
  edge [
    source 26
    target 677
  ]
  edge [
    source 26
    target 1404
  ]
  edge [
    source 26
    target 1405
  ]
  edge [
    source 26
    target 79
  ]
  edge [
    source 26
    target 1406
  ]
  edge [
    source 26
    target 1407
  ]
  edge [
    source 26
    target 1408
  ]
  edge [
    source 26
    target 1409
  ]
  edge [
    source 26
    target 1410
  ]
  edge [
    source 26
    target 1411
  ]
  edge [
    source 26
    target 1412
  ]
  edge [
    source 26
    target 1413
  ]
  edge [
    source 26
    target 1414
  ]
  edge [
    source 26
    target 1415
  ]
  edge [
    source 26
    target 1416
  ]
  edge [
    source 26
    target 1388
  ]
  edge [
    source 26
    target 1417
  ]
  edge [
    source 26
    target 1418
  ]
  edge [
    source 26
    target 1419
  ]
  edge [
    source 26
    target 1420
  ]
  edge [
    source 26
    target 1421
  ]
  edge [
    source 26
    target 1422
  ]
  edge [
    source 26
    target 462
  ]
  edge [
    source 26
    target 1423
  ]
  edge [
    source 26
    target 1424
  ]
  edge [
    source 26
    target 1139
  ]
  edge [
    source 26
    target 60
  ]
  edge [
    source 26
    target 1425
  ]
  edge [
    source 26
    target 1426
  ]
  edge [
    source 26
    target 1427
  ]
  edge [
    source 26
    target 1428
  ]
  edge [
    source 26
    target 1429
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 1430
  ]
  edge [
    source 26
    target 1431
  ]
  edge [
    source 26
    target 1432
  ]
  edge [
    source 26
    target 1433
  ]
  edge [
    source 26
    target 1434
  ]
  edge [
    source 26
    target 483
  ]
  edge [
    source 26
    target 122
  ]
  edge [
    source 26
    target 1435
  ]
  edge [
    source 26
    target 1436
  ]
  edge [
    source 26
    target 1437
  ]
  edge [
    source 26
    target 1438
  ]
  edge [
    source 26
    target 731
  ]
  edge [
    source 26
    target 1439
  ]
  edge [
    source 26
    target 1440
  ]
  edge [
    source 26
    target 1441
  ]
  edge [
    source 26
    target 1442
  ]
  edge [
    source 26
    target 662
  ]
  edge [
    source 26
    target 663
  ]
  edge [
    source 26
    target 664
  ]
  edge [
    source 26
    target 223
  ]
  edge [
    source 26
    target 665
  ]
  edge [
    source 26
    target 666
  ]
  edge [
    source 26
    target 389
  ]
  edge [
    source 26
    target 667
  ]
  edge [
    source 26
    target 668
  ]
  edge [
    source 26
    target 1443
  ]
  edge [
    source 26
    target 606
  ]
  edge [
    source 26
    target 1444
  ]
  edge [
    source 26
    target 83
  ]
  edge [
    source 26
    target 1445
  ]
  edge [
    source 26
    target 255
  ]
  edge [
    source 26
    target 1446
  ]
  edge [
    source 26
    target 1447
  ]
  edge [
    source 26
    target 1448
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 526
  ]
  edge [
    source 27
    target 531
  ]
  edge [
    source 27
    target 1260
  ]
  edge [
    source 27
    target 61
  ]
  edge [
    source 27
    target 1261
  ]
  edge [
    source 27
    target 1262
  ]
  edge [
    source 27
    target 534
  ]
  edge [
    source 27
    target 188
  ]
  edge [
    source 27
    target 176
  ]
  edge [
    source 27
    target 612
  ]
  edge [
    source 27
    target 525
  ]
  edge [
    source 27
    target 527
  ]
  edge [
    source 27
    target 528
  ]
  edge [
    source 27
    target 529
  ]
  edge [
    source 27
    target 530
  ]
  edge [
    source 27
    target 532
  ]
  edge [
    source 27
    target 533
  ]
  edge [
    source 27
    target 535
  ]
  edge [
    source 27
    target 536
  ]
  edge [
    source 27
    target 1449
  ]
  edge [
    source 27
    target 1450
  ]
  edge [
    source 27
    target 1451
  ]
  edge [
    source 27
    target 1452
  ]
  edge [
    source 27
    target 484
  ]
  edge [
    source 27
    target 1453
  ]
  edge [
    source 27
    target 205
  ]
  edge [
    source 27
    target 1454
  ]
  edge [
    source 27
    target 1455
  ]
  edge [
    source 27
    target 1456
  ]
  edge [
    source 27
    target 1457
  ]
  edge [
    source 27
    target 1458
  ]
  edge [
    source 27
    target 1459
  ]
  edge [
    source 27
    target 215
  ]
  edge [
    source 27
    target 1460
  ]
  edge [
    source 27
    target 1461
  ]
  edge [
    source 27
    target 1462
  ]
  edge [
    source 27
    target 175
  ]
  edge [
    source 27
    target 172
  ]
  edge [
    source 27
    target 177
  ]
  edge [
    source 27
    target 178
  ]
  edge [
    source 27
    target 179
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1463
  ]
  edge [
    source 28
    target 70
  ]
  edge [
    source 28
    target 1464
  ]
  edge [
    source 28
    target 1465
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 28
    target 51
  ]
  edge [
    source 28
    target 578
  ]
  edge [
    source 28
    target 1040
  ]
  edge [
    source 28
    target 1466
  ]
  edge [
    source 28
    target 1467
  ]
  edge [
    source 28
    target 114
  ]
  edge [
    source 28
    target 1468
  ]
  edge [
    source 28
    target 1469
  ]
  edge [
    source 28
    target 1470
  ]
  edge [
    source 28
    target 1387
  ]
  edge [
    source 28
    target 48
  ]
  edge [
    source 28
    target 1471
  ]
  edge [
    source 28
    target 1472
  ]
  edge [
    source 28
    target 125
  ]
  edge [
    source 28
    target 119
  ]
  edge [
    source 28
    target 1473
  ]
  edge [
    source 28
    target 1474
  ]
  edge [
    source 28
    target 1475
  ]
  edge [
    source 28
    target 1476
  ]
  edge [
    source 28
    target 1477
  ]
  edge [
    source 28
    target 1478
  ]
  edge [
    source 28
    target 1479
  ]
  edge [
    source 28
    target 1480
  ]
  edge [
    source 28
    target 697
  ]
  edge [
    source 28
    target 1481
  ]
  edge [
    source 28
    target 1482
  ]
  edge [
    source 28
    target 1483
  ]
  edge [
    source 28
    target 1484
  ]
  edge [
    source 28
    target 1485
  ]
  edge [
    source 28
    target 957
  ]
  edge [
    source 28
    target 1054
  ]
  edge [
    source 28
    target 1486
  ]
  edge [
    source 28
    target 567
  ]
  edge [
    source 28
    target 1050
  ]
  edge [
    source 28
    target 1487
  ]
  edge [
    source 28
    target 1059
  ]
  edge [
    source 28
    target 185
  ]
  edge [
    source 28
    target 1488
  ]
  edge [
    source 28
    target 1489
  ]
  edge [
    source 28
    target 1444
  ]
  edge [
    source 28
    target 1490
  ]
  edge [
    source 28
    target 79
  ]
  edge [
    source 28
    target 1491
  ]
  edge [
    source 28
    target 1492
  ]
  edge [
    source 28
    target 1493
  ]
  edge [
    source 28
    target 1494
  ]
  edge [
    source 28
    target 1495
  ]
  edge [
    source 28
    target 826
  ]
  edge [
    source 28
    target 1496
  ]
  edge [
    source 28
    target 1497
  ]
  edge [
    source 28
    target 63
  ]
  edge [
    source 28
    target 64
  ]
  edge [
    source 28
    target 65
  ]
  edge [
    source 28
    target 66
  ]
  edge [
    source 28
    target 67
  ]
  edge [
    source 28
    target 68
  ]
  edge [
    source 28
    target 69
  ]
  edge [
    source 28
    target 71
  ]
  edge [
    source 28
    target 72
  ]
  edge [
    source 28
    target 73
  ]
  edge [
    source 28
    target 74
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 75
  ]
  edge [
    source 28
    target 76
  ]
  edge [
    source 28
    target 77
  ]
  edge [
    source 28
    target 78
  ]
  edge [
    source 28
    target 80
  ]
  edge [
    source 28
    target 81
  ]
  edge [
    source 28
    target 82
  ]
  edge [
    source 28
    target 83
  ]
  edge [
    source 28
    target 84
  ]
  edge [
    source 28
    target 85
  ]
  edge [
    source 28
    target 86
  ]
  edge [
    source 28
    target 87
  ]
  edge [
    source 28
    target 88
  ]
  edge [
    source 28
    target 89
  ]
  edge [
    source 28
    target 90
  ]
  edge [
    source 28
    target 91
  ]
  edge [
    source 28
    target 1298
  ]
  edge [
    source 28
    target 1033
  ]
  edge [
    source 28
    target 1498
  ]
  edge [
    source 28
    target 1499
  ]
  edge [
    source 28
    target 1448
  ]
  edge [
    source 28
    target 1500
  ]
  edge [
    source 28
    target 1501
  ]
  edge [
    source 28
    target 179
  ]
  edge [
    source 28
    target 1502
  ]
  edge [
    source 28
    target 1503
  ]
  edge [
    source 28
    target 1504
  ]
  edge [
    source 28
    target 1505
  ]
  edge [
    source 28
    target 1012
  ]
  edge [
    source 28
    target 584
  ]
  edge [
    source 28
    target 1506
  ]
  edge [
    source 28
    target 1507
  ]
  edge [
    source 28
    target 1508
  ]
  edge [
    source 28
    target 654
  ]
  edge [
    source 28
    target 1509
  ]
  edge [
    source 28
    target 525
  ]
  edge [
    source 28
    target 552
  ]
  edge [
    source 28
    target 1510
  ]
  edge [
    source 28
    target 1511
  ]
  edge [
    source 28
    target 1512
  ]
  edge [
    source 28
    target 1513
  ]
  edge [
    source 28
    target 1514
  ]
  edge [
    source 28
    target 1515
  ]
  edge [
    source 28
    target 1516
  ]
  edge [
    source 28
    target 1517
  ]
  edge [
    source 28
    target 1518
  ]
  edge [
    source 28
    target 1519
  ]
  edge [
    source 28
    target 1520
  ]
  edge [
    source 28
    target 1521
  ]
  edge [
    source 28
    target 1522
  ]
  edge [
    source 28
    target 1523
  ]
  edge [
    source 28
    target 1524
  ]
  edge [
    source 28
    target 1525
  ]
  edge [
    source 28
    target 1526
  ]
  edge [
    source 28
    target 1527
  ]
  edge [
    source 28
    target 1528
  ]
  edge [
    source 28
    target 1529
  ]
  edge [
    source 28
    target 1530
  ]
  edge [
    source 28
    target 1531
  ]
  edge [
    source 28
    target 1532
  ]
  edge [
    source 28
    target 1533
  ]
  edge [
    source 28
    target 1534
  ]
  edge [
    source 28
    target 1535
  ]
  edge [
    source 28
    target 1536
  ]
  edge [
    source 28
    target 1537
  ]
  edge [
    source 28
    target 1538
  ]
  edge [
    source 28
    target 1539
  ]
  edge [
    source 28
    target 1540
  ]
  edge [
    source 28
    target 1046
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1541
  ]
  edge [
    source 30
    target 1542
  ]
  edge [
    source 30
    target 1543
  ]
  edge [
    source 30
    target 712
  ]
  edge [
    source 30
    target 1544
  ]
  edge [
    source 30
    target 1545
  ]
  edge [
    source 30
    target 1546
  ]
  edge [
    source 30
    target 1547
  ]
  edge [
    source 30
    target 1548
  ]
  edge [
    source 30
    target 497
  ]
  edge [
    source 30
    target 1549
  ]
  edge [
    source 30
    target 1550
  ]
  edge [
    source 30
    target 176
  ]
  edge [
    source 30
    target 1551
  ]
  edge [
    source 30
    target 1552
  ]
  edge [
    source 30
    target 1553
  ]
  edge [
    source 30
    target 1554
  ]
  edge [
    source 30
    target 1555
  ]
  edge [
    source 30
    target 1556
  ]
  edge [
    source 30
    target 1557
  ]
  edge [
    source 30
    target 1558
  ]
  edge [
    source 30
    target 1559
  ]
  edge [
    source 30
    target 1560
  ]
  edge [
    source 30
    target 180
  ]
  edge [
    source 30
    target 1561
  ]
  edge [
    source 30
    target 469
  ]
  edge [
    source 30
    target 527
  ]
  edge [
    source 30
    target 1562
  ]
  edge [
    source 30
    target 1563
  ]
  edge [
    source 30
    target 51
  ]
  edge [
    source 30
    target 1487
  ]
  edge [
    source 30
    target 1512
  ]
  edge [
    source 30
    target 1564
  ]
  edge [
    source 30
    target 1565
  ]
  edge [
    source 30
    target 1566
  ]
  edge [
    source 30
    target 1567
  ]
  edge [
    source 30
    target 1568
  ]
  edge [
    source 30
    target 1569
  ]
  edge [
    source 30
    target 1570
  ]
  edge [
    source 30
    target 1571
  ]
  edge [
    source 30
    target 1572
  ]
  edge [
    source 30
    target 1573
  ]
  edge [
    source 30
    target 1574
  ]
  edge [
    source 30
    target 1575
  ]
  edge [
    source 30
    target 1576
  ]
  edge [
    source 30
    target 1577
  ]
  edge [
    source 30
    target 1578
  ]
  edge [
    source 30
    target 46
  ]
  edge [
    source 30
    target 47
  ]
  edge [
    source 30
    target 48
  ]
  edge [
    source 30
    target 49
  ]
  edge [
    source 30
    target 50
  ]
  edge [
    source 30
    target 52
  ]
  edge [
    source 30
    target 53
  ]
  edge [
    source 30
    target 54
  ]
  edge [
    source 30
    target 55
  ]
  edge [
    source 30
    target 56
  ]
  edge [
    source 30
    target 57
  ]
  edge [
    source 30
    target 58
  ]
  edge [
    source 30
    target 59
  ]
  edge [
    source 30
    target 60
  ]
  edge [
    source 30
    target 61
  ]
  edge [
    source 30
    target 62
  ]
  edge [
    source 30
    target 1579
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 1580
  ]
  edge [
    source 30
    target 1581
  ]
  edge [
    source 30
    target 1582
  ]
  edge [
    source 30
    target 1583
  ]
  edge [
    source 30
    target 613
  ]
  edge [
    source 30
    target 614
  ]
  edge [
    source 30
    target 615
  ]
  edge [
    source 30
    target 1018
  ]
  edge [
    source 30
    target 548
  ]
  edge [
    source 30
    target 1019
  ]
  edge [
    source 30
    target 1020
  ]
  edge [
    source 30
    target 1012
  ]
  edge [
    source 30
    target 350
  ]
  edge [
    source 30
    target 1021
  ]
  edge [
    source 30
    target 1022
  ]
  edge [
    source 30
    target 1023
  ]
  edge [
    source 30
    target 363
  ]
  edge [
    source 30
    target 1584
  ]
  edge [
    source 30
    target 1330
  ]
  edge [
    source 30
    target 1585
  ]
  edge [
    source 30
    target 1586
  ]
  edge [
    source 30
    target 1587
  ]
  edge [
    source 30
    target 800
  ]
  edge [
    source 30
    target 1588
  ]
  edge [
    source 30
    target 201
  ]
  edge [
    source 30
    target 1143
  ]
  edge [
    source 30
    target 1444
  ]
  edge [
    source 30
    target 188
  ]
  edge [
    source 30
    target 1589
  ]
  edge [
    source 30
    target 1590
  ]
  edge [
    source 30
    target 1591
  ]
  edge [
    source 30
    target 147
  ]
  edge [
    source 30
    target 1592
  ]
  edge [
    source 30
    target 1256
  ]
  edge [
    source 30
    target 1593
  ]
  edge [
    source 30
    target 1594
  ]
  edge [
    source 30
    target 1595
  ]
  edge [
    source 30
    target 112
  ]
  edge [
    source 30
    target 1596
  ]
  edge [
    source 30
    target 1597
  ]
  edge [
    source 30
    target 1598
  ]
  edge [
    source 30
    target 1599
  ]
  edge [
    source 30
    target 1600
  ]
  edge [
    source 30
    target 1601
  ]
  edge [
    source 30
    target 1602
  ]
  edge [
    source 30
    target 1603
  ]
  edge [
    source 30
    target 1604
  ]
  edge [
    source 30
    target 1605
  ]
  edge [
    source 30
    target 1606
  ]
  edge [
    source 30
    target 1607
  ]
  edge [
    source 30
    target 1608
  ]
  edge [
    source 30
    target 1609
  ]
  edge [
    source 30
    target 1610
  ]
  edge [
    source 30
    target 957
  ]
  edge [
    source 30
    target 567
  ]
  edge [
    source 30
    target 730
  ]
  edge [
    source 30
    target 654
  ]
  edge [
    source 30
    target 731
  ]
  edge [
    source 30
    target 732
  ]
  edge [
    source 30
    target 721
  ]
  edge [
    source 30
    target 733
  ]
  edge [
    source 30
    target 734
  ]
  edge [
    source 30
    target 83
  ]
  edge [
    source 30
    target 735
  ]
  edge [
    source 30
    target 1611
  ]
  edge [
    source 30
    target 1612
  ]
  edge [
    source 30
    target 1613
  ]
  edge [
    source 30
    target 1614
  ]
  edge [
    source 30
    target 93
  ]
  edge [
    source 30
    target 1615
  ]
  edge [
    source 30
    target 1616
  ]
  edge [
    source 30
    target 1617
  ]
  edge [
    source 30
    target 1618
  ]
  edge [
    source 30
    target 1619
  ]
  edge [
    source 30
    target 1620
  ]
  edge [
    source 30
    target 1621
  ]
  edge [
    source 30
    target 1622
  ]
  edge [
    source 30
    target 1623
  ]
  edge [
    source 30
    target 1624
  ]
  edge [
    source 30
    target 1625
  ]
  edge [
    source 30
    target 1626
  ]
  edge [
    source 30
    target 1627
  ]
  edge [
    source 30
    target 1628
  ]
  edge [
    source 30
    target 1629
  ]
  edge [
    source 30
    target 1630
  ]
  edge [
    source 30
    target 1631
  ]
  edge [
    source 30
    target 1632
  ]
  edge [
    source 30
    target 1633
  ]
  edge [
    source 30
    target 1634
  ]
  edge [
    source 30
    target 1635
  ]
  edge [
    source 30
    target 1636
  ]
  edge [
    source 30
    target 1637
  ]
  edge [
    source 30
    target 1638
  ]
  edge [
    source 30
    target 1639
  ]
  edge [
    source 30
    target 1640
  ]
  edge [
    source 30
    target 1641
  ]
  edge [
    source 30
    target 1642
  ]
  edge [
    source 30
    target 1643
  ]
  edge [
    source 30
    target 1644
  ]
  edge [
    source 30
    target 769
  ]
  edge [
    source 30
    target 1645
  ]
  edge [
    source 30
    target 1646
  ]
  edge [
    source 30
    target 1158
  ]
  edge [
    source 30
    target 1647
  ]
  edge [
    source 30
    target 195
  ]
  edge [
    source 30
    target 1648
  ]
  edge [
    source 30
    target 1649
  ]
  edge [
    source 30
    target 1650
  ]
  edge [
    source 30
    target 1651
  ]
  edge [
    source 30
    target 1652
  ]
  edge [
    source 30
    target 169
  ]
  edge [
    source 30
    target 1653
  ]
  edge [
    source 30
    target 1654
  ]
  edge [
    source 30
    target 1655
  ]
  edge [
    source 30
    target 123
  ]
  edge [
    source 30
    target 1656
  ]
  edge [
    source 30
    target 1657
  ]
  edge [
    source 30
    target 1658
  ]
  edge [
    source 30
    target 1659
  ]
  edge [
    source 30
    target 1660
  ]
  edge [
    source 30
    target 1661
  ]
  edge [
    source 30
    target 74
  ]
  edge [
    source 30
    target 89
  ]
  edge [
    source 30
    target 1662
  ]
  edge [
    source 30
    target 82
  ]
  edge [
    source 30
    target 91
  ]
  edge [
    source 30
    target 1663
  ]
  edge [
    source 30
    target 1664
  ]
  edge [
    source 30
    target 1665
  ]
  edge [
    source 30
    target 1666
  ]
  edge [
    source 30
    target 1667
  ]
  edge [
    source 30
    target 1668
  ]
  edge [
    source 30
    target 1669
  ]
  edge [
    source 30
    target 1670
  ]
  edge [
    source 30
    target 1671
  ]
  edge [
    source 30
    target 1672
  ]
  edge [
    source 30
    target 1673
  ]
  edge [
    source 30
    target 1674
  ]
  edge [
    source 30
    target 1675
  ]
  edge [
    source 30
    target 1676
  ]
  edge [
    source 30
    target 1677
  ]
  edge [
    source 30
    target 1678
  ]
  edge [
    source 30
    target 1679
  ]
  edge [
    source 30
    target 1680
  ]
  edge [
    source 30
    target 1681
  ]
  edge [
    source 30
    target 575
  ]
  edge [
    source 30
    target 1682
  ]
  edge [
    source 30
    target 1683
  ]
  edge [
    source 30
    target 1684
  ]
  edge [
    source 30
    target 1685
  ]
  edge [
    source 30
    target 1686
  ]
  edge [
    source 30
    target 1687
  ]
  edge [
    source 30
    target 1416
  ]
  edge [
    source 30
    target 1688
  ]
  edge [
    source 30
    target 1689
  ]
  edge [
    source 30
    target 1690
  ]
  edge [
    source 30
    target 1691
  ]
  edge [
    source 30
    target 1692
  ]
  edge [
    source 30
    target 1693
  ]
  edge [
    source 30
    target 606
  ]
  edge [
    source 30
    target 1409
  ]
  edge [
    source 30
    target 1694
  ]
  edge [
    source 30
    target 1695
  ]
  edge [
    source 30
    target 1696
  ]
  edge [
    source 30
    target 1161
  ]
  edge [
    source 30
    target 916
  ]
  edge [
    source 30
    target 1697
  ]
  edge [
    source 30
    target 1216
  ]
  edge [
    source 30
    target 1698
  ]
  edge [
    source 30
    target 1699
  ]
  edge [
    source 30
    target 1700
  ]
  edge [
    source 30
    target 1701
  ]
  edge [
    source 30
    target 1702
  ]
  edge [
    source 30
    target 1703
  ]
  edge [
    source 30
    target 1704
  ]
  edge [
    source 30
    target 1705
  ]
  edge [
    source 30
    target 1706
  ]
  edge [
    source 30
    target 1707
  ]
  edge [
    source 30
    target 1708
  ]
  edge [
    source 30
    target 43
  ]
  edge [
    source 30
    target 1709
  ]
  edge [
    source 30
    target 1710
  ]
  edge [
    source 30
    target 1711
  ]
  edge [
    source 30
    target 1712
  ]
  edge [
    source 30
    target 1713
  ]
  edge [
    source 30
    target 1714
  ]
  edge [
    source 30
    target 1715
  ]
  edge [
    source 30
    target 1716
  ]
  edge [
    source 30
    target 1717
  ]
  edge [
    source 30
    target 1718
  ]
  edge [
    source 30
    target 1719
  ]
  edge [
    source 30
    target 1720
  ]
  edge [
    source 30
    target 1721
  ]
  edge [
    source 30
    target 1722
  ]
  edge [
    source 30
    target 1723
  ]
  edge [
    source 30
    target 791
  ]
  edge [
    source 30
    target 1724
  ]
  edge [
    source 30
    target 1725
  ]
  edge [
    source 30
    target 1726
  ]
  edge [
    source 30
    target 1727
  ]
  edge [
    source 30
    target 1728
  ]
  edge [
    source 30
    target 1134
  ]
  edge [
    source 30
    target 1729
  ]
  edge [
    source 30
    target 1730
  ]
  edge [
    source 30
    target 1731
  ]
  edge [
    source 30
    target 1732
  ]
  edge [
    source 30
    target 450
  ]
  edge [
    source 30
    target 1733
  ]
  edge [
    source 30
    target 1734
  ]
  edge [
    source 30
    target 1735
  ]
  edge [
    source 30
    target 1736
  ]
  edge [
    source 30
    target 1737
  ]
  edge [
    source 30
    target 1738
  ]
  edge [
    source 30
    target 192
  ]
  edge [
    source 30
    target 1739
  ]
  edge [
    source 30
    target 1740
  ]
  edge [
    source 30
    target 1741
  ]
  edge [
    source 30
    target 1742
  ]
  edge [
    source 30
    target 1032
  ]
  edge [
    source 30
    target 1743
  ]
  edge [
    source 30
    target 1460
  ]
  edge [
    source 30
    target 1744
  ]
  edge [
    source 30
    target 1745
  ]
  edge [
    source 30
    target 1483
  ]
  edge [
    source 30
    target 1746
  ]
  edge [
    source 30
    target 1747
  ]
  edge [
    source 30
    target 1748
  ]
  edge [
    source 30
    target 1749
  ]
  edge [
    source 30
    target 495
  ]
  edge [
    source 30
    target 1750
  ]
  edge [
    source 30
    target 1751
  ]
  edge [
    source 30
    target 1752
  ]
  edge [
    source 30
    target 1753
  ]
  edge [
    source 30
    target 1754
  ]
  edge [
    source 30
    target 1755
  ]
  edge [
    source 30
    target 1756
  ]
  edge [
    source 30
    target 1757
  ]
  edge [
    source 30
    target 1758
  ]
  edge [
    source 30
    target 1054
  ]
  edge [
    source 30
    target 529
  ]
  edge [
    source 30
    target 1759
  ]
  edge [
    source 30
    target 1760
  ]
  edge [
    source 30
    target 1761
  ]
  edge [
    source 30
    target 114
  ]
  edge [
    source 30
    target 1762
  ]
  edge [
    source 30
    target 1763
  ]
  edge [
    source 30
    target 63
  ]
  edge [
    source 30
    target 64
  ]
  edge [
    source 30
    target 65
  ]
  edge [
    source 30
    target 66
  ]
  edge [
    source 30
    target 67
  ]
  edge [
    source 30
    target 68
  ]
  edge [
    source 30
    target 69
  ]
  edge [
    source 30
    target 70
  ]
  edge [
    source 30
    target 71
  ]
  edge [
    source 30
    target 72
  ]
  edge [
    source 30
    target 73
  ]
  edge [
    source 30
    target 75
  ]
  edge [
    source 30
    target 76
  ]
  edge [
    source 30
    target 77
  ]
  edge [
    source 30
    target 78
  ]
  edge [
    source 30
    target 79
  ]
  edge [
    source 30
    target 80
  ]
  edge [
    source 30
    target 81
  ]
  edge [
    source 30
    target 84
  ]
  edge [
    source 30
    target 85
  ]
  edge [
    source 30
    target 86
  ]
  edge [
    source 30
    target 87
  ]
  edge [
    source 30
    target 88
  ]
  edge [
    source 30
    target 90
  ]
  edge [
    source 30
    target 639
  ]
  edge [
    source 30
    target 640
  ]
  edge [
    source 30
    target 641
  ]
  edge [
    source 30
    target 642
  ]
  edge [
    source 30
    target 643
  ]
  edge [
    source 30
    target 644
  ]
  edge [
    source 30
    target 462
  ]
  edge [
    source 30
    target 645
  ]
  edge [
    source 30
    target 646
  ]
  edge [
    source 30
    target 647
  ]
  edge [
    source 30
    target 347
  ]
  edge [
    source 30
    target 648
  ]
  edge [
    source 30
    target 649
  ]
  edge [
    source 30
    target 650
  ]
  edge [
    source 30
    target 651
  ]
  edge [
    source 30
    target 652
  ]
  edge [
    source 30
    target 653
  ]
  edge [
    source 30
    target 190
  ]
  edge [
    source 30
    target 106
  ]
  edge [
    source 30
    target 530
  ]
  edge [
    source 30
    target 1209
  ]
  edge [
    source 30
    target 1395
  ]
  edge [
    source 30
    target 1764
  ]
  edge [
    source 30
    target 1765
  ]
  edge [
    source 30
    target 39
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1766
  ]
  edge [
    source 31
    target 1767
  ]
  edge [
    source 31
    target 1768
  ]
  edge [
    source 31
    target 1769
  ]
  edge [
    source 31
    target 1770
  ]
  edge [
    source 31
    target 1771
  ]
  edge [
    source 31
    target 1772
  ]
  edge [
    source 31
    target 1773
  ]
  edge [
    source 31
    target 1774
  ]
  edge [
    source 31
    target 1775
  ]
  edge [
    source 31
    target 1776
  ]
  edge [
    source 31
    target 1777
  ]
  edge [
    source 31
    target 1778
  ]
  edge [
    source 31
    target 1779
  ]
  edge [
    source 31
    target 1780
  ]
  edge [
    source 31
    target 1781
  ]
  edge [
    source 31
    target 1782
  ]
  edge [
    source 31
    target 1783
  ]
  edge [
    source 31
    target 1784
  ]
  edge [
    source 31
    target 1785
  ]
  edge [
    source 31
    target 1786
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1787
  ]
  edge [
    source 32
    target 1788
  ]
  edge [
    source 32
    target 1789
  ]
  edge [
    source 32
    target 45
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1790
  ]
  edge [
    source 33
    target 1791
  ]
  edge [
    source 33
    target 655
  ]
  edge [
    source 33
    target 463
  ]
  edge [
    source 33
    target 656
  ]
  edge [
    source 33
    target 657
  ]
  edge [
    source 33
    target 658
  ]
  edge [
    source 33
    target 659
  ]
  edge [
    source 33
    target 660
  ]
  edge [
    source 33
    target 661
  ]
  edge [
    source 33
    target 1792
  ]
  edge [
    source 33
    target 1793
  ]
  edge [
    source 33
    target 1794
  ]
  edge [
    source 33
    target 1795
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 114
  ]
  edge [
    source 34
    target 185
  ]
  edge [
    source 34
    target 1796
  ]
  edge [
    source 34
    target 1797
  ]
  edge [
    source 34
    target 1798
  ]
  edge [
    source 34
    target 1799
  ]
  edge [
    source 34
    target 1800
  ]
  edge [
    source 34
    target 464
  ]
  edge [
    source 34
    target 1801
  ]
  edge [
    source 34
    target 1802
  ]
  edge [
    source 34
    target 1803
  ]
  edge [
    source 34
    target 1032
  ]
  edge [
    source 34
    target 1804
  ]
  edge [
    source 34
    target 1805
  ]
  edge [
    source 34
    target 1482
  ]
  edge [
    source 34
    target 1483
  ]
  edge [
    source 34
    target 1484
  ]
  edge [
    source 34
    target 125
  ]
  edge [
    source 34
    target 1485
  ]
  edge [
    source 34
    target 957
  ]
  edge [
    source 34
    target 1756
  ]
  edge [
    source 34
    target 1054
  ]
  edge [
    source 34
    target 567
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 34
    target 237
  ]
  edge [
    source 34
    target 1762
  ]
  edge [
    source 34
    target 1763
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 402
  ]
  edge [
    source 36
    target 1806
  ]
  edge [
    source 36
    target 1807
  ]
  edge [
    source 36
    target 1808
  ]
  edge [
    source 36
    target 385
  ]
  edge [
    source 36
    target 313
  ]
  edge [
    source 36
    target 1809
  ]
  edge [
    source 36
    target 584
  ]
  edge [
    source 36
    target 1810
  ]
  edge [
    source 36
    target 1811
  ]
  edge [
    source 36
    target 567
  ]
  edge [
    source 36
    target 1812
  ]
  edge [
    source 36
    target 1813
  ]
  edge [
    source 36
    target 1814
  ]
  edge [
    source 36
    target 1815
  ]
  edge [
    source 36
    target 1816
  ]
  edge [
    source 36
    target 1817
  ]
  edge [
    source 36
    target 1818
  ]
  edge [
    source 36
    target 112
  ]
  edge [
    source 36
    target 1819
  ]
  edge [
    source 36
    target 1820
  ]
  edge [
    source 36
    target 1821
  ]
  edge [
    source 36
    target 1822
  ]
  edge [
    source 36
    target 1823
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 36
    target 1824
  ]
  edge [
    source 36
    target 1825
  ]
  edge [
    source 36
    target 1826
  ]
  edge [
    source 36
    target 1827
  ]
  edge [
    source 36
    target 1828
  ]
  edge [
    source 36
    target 1829
  ]
  edge [
    source 36
    target 1830
  ]
  edge [
    source 36
    target 1831
  ]
  edge [
    source 36
    target 1832
  ]
  edge [
    source 36
    target 530
  ]
  edge [
    source 36
    target 388
  ]
  edge [
    source 36
    target 1833
  ]
  edge [
    source 36
    target 565
  ]
  edge [
    source 36
    target 566
  ]
  edge [
    source 36
    target 527
  ]
  edge [
    source 36
    target 338
  ]
  edge [
    source 36
    target 568
  ]
  edge [
    source 36
    target 569
  ]
  edge [
    source 36
    target 570
  ]
  edge [
    source 36
    target 571
  ]
  edge [
    source 36
    target 572
  ]
  edge [
    source 36
    target 573
  ]
  edge [
    source 36
    target 574
  ]
  edge [
    source 36
    target 488
  ]
  edge [
    source 36
    target 1834
  ]
  edge [
    source 36
    target 1835
  ]
  edge [
    source 36
    target 837
  ]
  edge [
    source 36
    target 861
  ]
  edge [
    source 36
    target 1655
  ]
  edge [
    source 36
    target 1836
  ]
  edge [
    source 36
    target 1837
  ]
  edge [
    source 36
    target 1838
  ]
  edge [
    source 36
    target 211
  ]
  edge [
    source 36
    target 1839
  ]
  edge [
    source 36
    target 1840
  ]
  edge [
    source 36
    target 1841
  ]
  edge [
    source 36
    target 728
  ]
  edge [
    source 36
    target 1842
  ]
  edge [
    source 36
    target 1843
  ]
  edge [
    source 36
    target 1844
  ]
  edge [
    source 36
    target 1845
  ]
  edge [
    source 36
    target 1846
  ]
  edge [
    source 36
    target 1847
  ]
  edge [
    source 36
    target 1848
  ]
  edge [
    source 36
    target 1849
  ]
  edge [
    source 36
    target 1850
  ]
  edge [
    source 36
    target 1851
  ]
  edge [
    source 36
    target 347
  ]
  edge [
    source 36
    target 1852
  ]
  edge [
    source 36
    target 1012
  ]
  edge [
    source 36
    target 114
  ]
  edge [
    source 36
    target 1442
  ]
  edge [
    source 36
    target 811
  ]
  edge [
    source 36
    target 1853
  ]
  edge [
    source 36
    target 1107
  ]
  edge [
    source 36
    target 1484
  ]
  edge [
    source 36
    target 1854
  ]
  edge [
    source 36
    target 1855
  ]
  edge [
    source 36
    target 1050
  ]
  edge [
    source 36
    target 1856
  ]
  edge [
    source 36
    target 99
  ]
  edge [
    source 36
    target 1857
  ]
  edge [
    source 36
    target 1858
  ]
  edge [
    source 36
    target 1859
  ]
  edge [
    source 36
    target 1860
  ]
  edge [
    source 36
    target 1861
  ]
  edge [
    source 36
    target 1862
  ]
  edge [
    source 36
    target 1863
  ]
  edge [
    source 36
    target 318
  ]
  edge [
    source 36
    target 1864
  ]
  edge [
    source 36
    target 1865
  ]
  edge [
    source 36
    target 1866
  ]
  edge [
    source 36
    target 393
  ]
  edge [
    source 36
    target 1867
  ]
  edge [
    source 36
    target 389
  ]
  edge [
    source 36
    target 381
  ]
  edge [
    source 36
    target 1868
  ]
  edge [
    source 36
    target 323
  ]
  edge [
    source 36
    target 390
  ]
  edge [
    source 36
    target 391
  ]
  edge [
    source 36
    target 392
  ]
  edge [
    source 36
    target 314
  ]
  edge [
    source 36
    target 1869
  ]
  edge [
    source 36
    target 1870
  ]
  edge [
    source 36
    target 1871
  ]
  edge [
    source 36
    target 324
  ]
  edge [
    source 36
    target 1872
  ]
  edge [
    source 36
    target 311
  ]
  edge [
    source 36
    target 1873
  ]
  edge [
    source 36
    target 1874
  ]
  edge [
    source 36
    target 1875
  ]
  edge [
    source 36
    target 1876
  ]
  edge [
    source 36
    target 1877
  ]
  edge [
    source 36
    target 1878
  ]
  edge [
    source 36
    target 1879
  ]
  edge [
    source 36
    target 1880
  ]
  edge [
    source 36
    target 1881
  ]
  edge [
    source 36
    target 1882
  ]
  edge [
    source 36
    target 1883
  ]
  edge [
    source 36
    target 1884
  ]
  edge [
    source 36
    target 1885
  ]
  edge [
    source 36
    target 1886
  ]
  edge [
    source 36
    target 1887
  ]
  edge [
    source 36
    target 1888
  ]
  edge [
    source 36
    target 1889
  ]
  edge [
    source 36
    target 1890
  ]
  edge [
    source 36
    target 1891
  ]
  edge [
    source 36
    target 1892
  ]
  edge [
    source 36
    target 1893
  ]
  edge [
    source 36
    target 1894
  ]
  edge [
    source 36
    target 1895
  ]
  edge [
    source 36
    target 1896
  ]
  edge [
    source 36
    target 657
  ]
  edge [
    source 36
    target 1897
  ]
  edge [
    source 36
    target 1486
  ]
  edge [
    source 36
    target 1054
  ]
  edge [
    source 36
    target 1487
  ]
  edge [
    source 36
    target 1059
  ]
  edge [
    source 36
    target 185
  ]
  edge [
    source 36
    target 1488
  ]
  edge [
    source 36
    target 1489
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1898
  ]
  edge [
    source 37
    target 1899
  ]
  edge [
    source 37
    target 1900
  ]
  edge [
    source 37
    target 1901
  ]
  edge [
    source 37
    target 1902
  ]
  edge [
    source 37
    target 514
  ]
  edge [
    source 37
    target 1903
  ]
  edge [
    source 37
    target 1904
  ]
  edge [
    source 37
    target 1905
  ]
  edge [
    source 37
    target 1906
  ]
  edge [
    source 37
    target 499
  ]
  edge [
    source 37
    target 1907
  ]
  edge [
    source 37
    target 1908
  ]
  edge [
    source 37
    target 1909
  ]
  edge [
    source 37
    target 1910
  ]
  edge [
    source 37
    target 1911
  ]
  edge [
    source 37
    target 1912
  ]
  edge [
    source 37
    target 1913
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1054
  ]
  edge [
    source 38
    target 1486
  ]
  edge [
    source 38
    target 567
  ]
  edge [
    source 38
    target 1050
  ]
  edge [
    source 38
    target 1487
  ]
  edge [
    source 38
    target 1059
  ]
  edge [
    source 38
    target 185
  ]
  edge [
    source 38
    target 1488
  ]
  edge [
    source 38
    target 1489
  ]
  edge [
    source 38
    target 1579
  ]
  edge [
    source 38
    target 1580
  ]
  edge [
    source 38
    target 1581
  ]
  edge [
    source 38
    target 1582
  ]
  edge [
    source 38
    target 1583
  ]
  edge [
    source 38
    target 1846
  ]
  edge [
    source 38
    target 1107
  ]
  edge [
    source 38
    target 1012
  ]
  edge [
    source 38
    target 1914
  ]
  edge [
    source 38
    target 1915
  ]
  edge [
    source 38
    target 1484
  ]
  edge [
    source 38
    target 1916
  ]
  edge [
    source 38
    target 1855
  ]
  edge [
    source 38
    target 1917
  ]
  edge [
    source 38
    target 1856
  ]
  edge [
    source 38
    target 211
  ]
  edge [
    source 38
    target 1918
  ]
  edge [
    source 38
    target 195
  ]
  edge [
    source 38
    target 196
  ]
  edge [
    source 38
    target 1919
  ]
  edge [
    source 38
    target 488
  ]
  edge [
    source 38
    target 1834
  ]
  edge [
    source 38
    target 1835
  ]
  edge [
    source 38
    target 837
  ]
  edge [
    source 38
    target 861
  ]
  edge [
    source 38
    target 1655
  ]
  edge [
    source 38
    target 1836
  ]
  edge [
    source 38
    target 1837
  ]
  edge [
    source 38
    target 1838
  ]
  edge [
    source 38
    target 1839
  ]
  edge [
    source 38
    target 1840
  ]
  edge [
    source 38
    target 1841
  ]
  edge [
    source 38
    target 584
  ]
  edge [
    source 38
    target 728
  ]
  edge [
    source 38
    target 199
  ]
  edge [
    source 38
    target 106
  ]
  edge [
    source 38
    target 1920
  ]
  edge [
    source 38
    target 1921
  ]
  edge [
    source 38
    target 1922
  ]
  edge [
    source 38
    target 1923
  ]
  edge [
    source 38
    target 1924
  ]
  edge [
    source 38
    target 1925
  ]
  edge [
    source 38
    target 1926
  ]
  edge [
    source 38
    target 1927
  ]
  edge [
    source 38
    target 1829
  ]
  edge [
    source 38
    target 1928
  ]
  edge [
    source 38
    target 1756
  ]
  edge [
    source 38
    target 1929
  ]
  edge [
    source 38
    target 1930
  ]
  edge [
    source 38
    target 1931
  ]
  edge [
    source 38
    target 530
  ]
  edge [
    source 38
    target 1932
  ]
  edge [
    source 38
    target 1933
  ]
  edge [
    source 38
    target 1934
  ]
  edge [
    source 38
    target 1935
  ]
  edge [
    source 38
    target 1936
  ]
  edge [
    source 38
    target 1032
  ]
  edge [
    source 38
    target 237
  ]
  edge [
    source 38
    target 1762
  ]
  edge [
    source 38
    target 1763
  ]
  edge [
    source 38
    target 114
  ]
  edge [
    source 38
    target 1937
  ]
  edge [
    source 38
    target 1938
  ]
  edge [
    source 38
    target 1939
  ]
  edge [
    source 38
    target 1940
  ]
  edge [
    source 38
    target 1941
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1419
  ]
  edge [
    source 39
    target 677
  ]
  edge [
    source 39
    target 60
  ]
  edge [
    source 39
    target 706
  ]
  edge [
    source 39
    target 180
  ]
  edge [
    source 39
    target 181
  ]
  edge [
    source 39
    target 182
  ]
  edge [
    source 39
    target 183
  ]
  edge [
    source 39
    target 172
  ]
  edge [
    source 39
    target 184
  ]
  edge [
    source 39
    target 185
  ]
  edge [
    source 39
    target 186
  ]
  edge [
    source 39
    target 187
  ]
  edge [
    source 39
    target 188
  ]
  edge [
    source 39
    target 189
  ]
  edge [
    source 39
    target 1942
  ]
  edge [
    source 39
    target 1756
  ]
  edge [
    source 39
    target 1943
  ]
  edge [
    source 39
    target 1944
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1945
  ]
  edge [
    source 40
    target 390
  ]
  edge [
    source 40
    target 1946
  ]
  edge [
    source 40
    target 1947
  ]
  edge [
    source 40
    target 1948
  ]
  edge [
    source 40
    target 1949
  ]
  edge [
    source 40
    target 1472
  ]
  edge [
    source 40
    target 1950
  ]
  edge [
    source 40
    target 1951
  ]
  edge [
    source 40
    target 1952
  ]
  edge [
    source 40
    target 1953
  ]
  edge [
    source 40
    target 1954
  ]
  edge [
    source 40
    target 1955
  ]
  edge [
    source 40
    target 1956
  ]
  edge [
    source 40
    target 1957
  ]
  edge [
    source 40
    target 1958
  ]
  edge [
    source 40
    target 1959
  ]
  edge [
    source 40
    target 1960
  ]
  edge [
    source 40
    target 1961
  ]
  edge [
    source 40
    target 1962
  ]
  edge [
    source 40
    target 1963
  ]
  edge [
    source 40
    target 1964
  ]
  edge [
    source 40
    target 1965
  ]
  edge [
    source 40
    target 1966
  ]
  edge [
    source 40
    target 1967
  ]
  edge [
    source 40
    target 119
  ]
  edge [
    source 40
    target 1968
  ]
  edge [
    source 40
    target 48
  ]
  edge [
    source 40
    target 1969
  ]
  edge [
    source 40
    target 1970
  ]
  edge [
    source 40
    target 1971
  ]
  edge [
    source 40
    target 1032
  ]
  edge [
    source 40
    target 1919
  ]
  edge [
    source 40
    target 1972
  ]
  edge [
    source 40
    target 1973
  ]
  edge [
    source 40
    target 1974
  ]
  edge [
    source 40
    target 1791
  ]
  edge [
    source 40
    target 91
  ]
  edge [
    source 40
    target 527
  ]
  edge [
    source 40
    target 548
  ]
  edge [
    source 40
    target 112
  ]
  edge [
    source 40
    target 1975
  ]
  edge [
    source 40
    target 328
  ]
  edge [
    source 40
    target 1976
  ]
  edge [
    source 40
    target 1977
  ]
  edge [
    source 40
    target 1400
  ]
  edge [
    source 40
    target 1978
  ]
  edge [
    source 40
    target 1979
  ]
  edge [
    source 40
    target 158
  ]
  edge [
    source 40
    target 160
  ]
  edge [
    source 40
    target 161
  ]
  edge [
    source 40
    target 114
  ]
  edge [
    source 40
    target 1980
  ]
  edge [
    source 40
    target 1981
  ]
  edge [
    source 40
    target 1982
  ]
  edge [
    source 40
    target 1983
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1955
  ]
  edge [
    source 41
    target 1984
  ]
  edge [
    source 41
    target 1985
  ]
  edge [
    source 41
    target 390
  ]
  edge [
    source 41
    target 1947
  ]
  edge [
    source 41
    target 48
  ]
  edge [
    source 41
    target 1969
  ]
  edge [
    source 41
    target 1472
  ]
  edge [
    source 41
    target 1951
  ]
  edge [
    source 41
    target 1970
  ]
  edge [
    source 41
    target 1971
  ]
  edge [
    source 41
    target 1032
  ]
  edge [
    source 41
    target 1919
  ]
  edge [
    source 41
    target 1986
  ]
  edge [
    source 41
    target 839
  ]
  edge [
    source 41
    target 1987
  ]
  edge [
    source 42
    target 1988
  ]
  edge [
    source 42
    target 1778
  ]
  edge [
    source 42
    target 1989
  ]
  edge [
    source 42
    target 1990
  ]
  edge [
    source 42
    target 1991
  ]
  edge [
    source 42
    target 1992
  ]
  edge [
    source 42
    target 1993
  ]
  edge [
    source 42
    target 1994
  ]
  edge [
    source 42
    target 1995
  ]
  edge [
    source 42
    target 1996
  ]
  edge [
    source 42
    target 1997
  ]
  edge [
    source 42
    target 1998
  ]
  edge [
    source 42
    target 1999
  ]
  edge [
    source 42
    target 2000
  ]
  edge [
    source 43
    target 916
  ]
  edge [
    source 43
    target 2001
  ]
  edge [
    source 43
    target 769
  ]
  edge [
    source 43
    target 2002
  ]
  edge [
    source 43
    target 1193
  ]
  edge [
    source 43
    target 2003
  ]
  edge [
    source 43
    target 1222
  ]
  edge [
    source 43
    target 2004
  ]
  edge [
    source 43
    target 2005
  ]
  edge [
    source 43
    target 1694
  ]
  edge [
    source 43
    target 1144
  ]
  edge [
    source 43
    target 2006
  ]
  edge [
    source 43
    target 1158
  ]
  edge [
    source 43
    target 2007
  ]
  edge [
    source 43
    target 2008
  ]
  edge [
    source 43
    target 2009
  ]
  edge [
    source 43
    target 1220
  ]
  edge [
    source 43
    target 2010
  ]
  edge [
    source 43
    target 2011
  ]
  edge [
    source 43
    target 2012
  ]
  edge [
    source 43
    target 2013
  ]
  edge [
    source 43
    target 2014
  ]
  edge [
    source 43
    target 2015
  ]
  edge [
    source 43
    target 757
  ]
  edge [
    source 43
    target 2016
  ]
  edge [
    source 43
    target 2017
  ]
  edge [
    source 43
    target 2018
  ]
  edge [
    source 43
    target 2019
  ]
  edge [
    source 43
    target 2020
  ]
  edge [
    source 43
    target 2021
  ]
  edge [
    source 43
    target 2022
  ]
  edge [
    source 43
    target 2023
  ]
  edge [
    source 43
    target 2024
  ]
  edge [
    source 43
    target 2025
  ]
  edge [
    source 43
    target 2026
  ]
  edge [
    source 43
    target 2027
  ]
  edge [
    source 43
    target 2028
  ]
  edge [
    source 43
    target 2029
  ]
  edge [
    source 43
    target 1156
  ]
  edge [
    source 43
    target 1195
  ]
  edge [
    source 43
    target 2030
  ]
  edge [
    source 43
    target 1130
  ]
  edge [
    source 43
    target 2031
  ]
  edge [
    source 43
    target 2032
  ]
  edge [
    source 43
    target 118
  ]
  edge [
    source 43
    target 532
  ]
  edge [
    source 43
    target 2033
  ]
  edge [
    source 43
    target 2034
  ]
  edge [
    source 43
    target 169
  ]
  edge [
    source 43
    target 2035
  ]
  edge [
    source 43
    target 2036
  ]
  edge [
    source 43
    target 1645
  ]
  edge [
    source 43
    target 1131
  ]
  edge [
    source 43
    target 2037
  ]
  edge [
    source 43
    target 1231
  ]
  edge [
    source 43
    target 2038
  ]
  edge [
    source 43
    target 2039
  ]
  edge [
    source 43
    target 2040
  ]
  edge [
    source 43
    target 2041
  ]
  edge [
    source 43
    target 1153
  ]
  edge [
    source 43
    target 2042
  ]
  edge [
    source 43
    target 2043
  ]
  edge [
    source 43
    target 2044
  ]
  edge [
    source 43
    target 2045
  ]
  edge [
    source 43
    target 2046
  ]
  edge [
    source 43
    target 1442
  ]
  edge [
    source 43
    target 2047
  ]
  edge [
    source 43
    target 736
  ]
  edge [
    source 43
    target 2048
  ]
  edge [
    source 43
    target 2049
  ]
  edge [
    source 43
    target 2050
  ]
  edge [
    source 43
    target 2051
  ]
  edge [
    source 43
    target 614
  ]
  edge [
    source 43
    target 2052
  ]
  edge [
    source 43
    target 2053
  ]
  edge [
    source 43
    target 238
  ]
  edge [
    source 43
    target 2054
  ]
  edge [
    source 43
    target 2055
  ]
  edge [
    source 43
    target 2056
  ]
  edge [
    source 43
    target 2057
  ]
  edge [
    source 43
    target 1300
  ]
  edge [
    source 43
    target 2058
  ]
  edge [
    source 43
    target 2059
  ]
  edge [
    source 43
    target 2060
  ]
  edge [
    source 43
    target 410
  ]
  edge [
    source 43
    target 1166
  ]
  edge [
    source 43
    target 1196
  ]
  edge [
    source 43
    target 2061
  ]
  edge [
    source 43
    target 2062
  ]
  edge [
    source 43
    target 1057
  ]
  edge [
    source 43
    target 2063
  ]
  edge [
    source 43
    target 2064
  ]
  edge [
    source 43
    target 2065
  ]
  edge [
    source 43
    target 2066
  ]
  edge [
    source 43
    target 2067
  ]
  edge [
    source 43
    target 2068
  ]
  edge [
    source 43
    target 758
  ]
  edge [
    source 43
    target 2069
  ]
  edge [
    source 43
    target 2070
  ]
  edge [
    source 43
    target 783
  ]
  edge [
    source 43
    target 2071
  ]
  edge [
    source 43
    target 2072
  ]
  edge [
    source 43
    target 2073
  ]
  edge [
    source 43
    target 2074
  ]
  edge [
    source 43
    target 749
  ]
  edge [
    source 43
    target 2075
  ]
  edge [
    source 43
    target 979
  ]
  edge [
    source 43
    target 2076
  ]
  edge [
    source 43
    target 2077
  ]
  edge [
    source 43
    target 2078
  ]
  edge [
    source 43
    target 2079
  ]
  edge [
    source 43
    target 2080
  ]
  edge [
    source 43
    target 2081
  ]
  edge [
    source 43
    target 2082
  ]
  edge [
    source 43
    target 2083
  ]
  edge [
    source 43
    target 2084
  ]
  edge [
    source 43
    target 202
  ]
  edge [
    source 43
    target 2085
  ]
  edge [
    source 43
    target 2086
  ]
  edge [
    source 43
    target 2087
  ]
  edge [
    source 43
    target 2088
  ]
  edge [
    source 43
    target 2089
  ]
  edge [
    source 43
    target 79
  ]
  edge [
    source 43
    target 2090
  ]
  edge [
    source 43
    target 2091
  ]
  edge [
    source 43
    target 2092
  ]
  edge [
    source 43
    target 2093
  ]
  edge [
    source 43
    target 1128
  ]
  edge [
    source 43
    target 2094
  ]
  edge [
    source 43
    target 2095
  ]
  edge [
    source 43
    target 2096
  ]
  edge [
    source 43
    target 2097
  ]
  edge [
    source 43
    target 138
  ]
  edge [
    source 43
    target 2098
  ]
  edge [
    source 43
    target 2099
  ]
  edge [
    source 43
    target 2100
  ]
  edge [
    source 43
    target 2101
  ]
  edge [
    source 43
    target 2102
  ]
  edge [
    source 43
    target 2103
  ]
  edge [
    source 43
    target 2104
  ]
  edge [
    source 43
    target 2105
  ]
  edge [
    source 43
    target 2106
  ]
  edge [
    source 43
    target 2107
  ]
  edge [
    source 43
    target 1718
  ]
  edge [
    source 43
    target 2108
  ]
  edge [
    source 43
    target 1847
  ]
  edge [
    source 43
    target 2109
  ]
  edge [
    source 43
    target 1823
  ]
  edge [
    source 43
    target 1565
  ]
  edge [
    source 43
    target 1707
  ]
  edge [
    source 43
    target 1566
  ]
  edge [
    source 43
    target 839
  ]
  edge [
    source 43
    target 951
  ]
  edge [
    source 43
    target 2110
  ]
  edge [
    source 43
    target 1712
  ]
  edge [
    source 43
    target 2111
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 2112
  ]
  edge [
    source 44
    target 2113
  ]
  edge [
    source 44
    target 2114
  ]
  edge [
    source 44
    target 521
  ]
  edge [
    source 45
    target 2115
  ]
  edge [
    source 45
    target 2116
  ]
  edge [
    source 45
    target 2117
  ]
  edge [
    source 45
    target 1208
  ]
  edge [
    source 45
    target 2118
  ]
  edge [
    source 45
    target 2119
  ]
  edge [
    source 45
    target 2120
  ]
  edge [
    source 45
    target 2121
  ]
  edge [
    source 45
    target 328
  ]
  edge [
    source 45
    target 389
  ]
  edge [
    source 45
    target 2122
  ]
  edge [
    source 45
    target 1442
  ]
  edge [
    source 45
    target 2123
  ]
  edge [
    source 45
    target 2124
  ]
  edge [
    source 45
    target 463
  ]
  edge [
    source 45
    target 2125
  ]
  edge [
    source 45
    target 622
  ]
  edge [
    source 45
    target 1963
  ]
  edge [
    source 45
    target 2126
  ]
  edge [
    source 45
    target 2127
  ]
  edge [
    source 45
    target 2128
  ]
  edge [
    source 45
    target 130
  ]
  edge [
    source 45
    target 2129
  ]
  edge [
    source 45
    target 2130
  ]
  edge [
    source 45
    target 2131
  ]
  edge [
    source 45
    target 1032
  ]
  edge [
    source 45
    target 827
  ]
  edge [
    source 45
    target 1134
  ]
  edge [
    source 45
    target 337
  ]
  edge [
    source 45
    target 338
  ]
  edge [
    source 45
    target 339
  ]
  edge [
    source 45
    target 340
  ]
  edge [
    source 45
    target 341
  ]
  edge [
    source 45
    target 342
  ]
  edge [
    source 45
    target 343
  ]
  edge [
    source 45
    target 344
  ]
  edge [
    source 45
    target 252
  ]
  edge [
    source 45
    target 345
  ]
  edge [
    source 45
    target 346
  ]
  edge [
    source 45
    target 347
  ]
  edge [
    source 45
    target 348
  ]
  edge [
    source 45
    target 349
  ]
  edge [
    source 45
    target 350
  ]
  edge [
    source 45
    target 351
  ]
  edge [
    source 45
    target 352
  ]
  edge [
    source 45
    target 353
  ]
  edge [
    source 45
    target 354
  ]
  edge [
    source 45
    target 355
  ]
  edge [
    source 45
    target 356
  ]
  edge [
    source 45
    target 357
  ]
  edge [
    source 45
    target 358
  ]
  edge [
    source 45
    target 359
  ]
  edge [
    source 45
    target 360
  ]
  edge [
    source 45
    target 361
  ]
  edge [
    source 45
    target 362
  ]
  edge [
    source 45
    target 173
  ]
  edge [
    source 45
    target 363
  ]
  edge [
    source 45
    target 364
  ]
  edge [
    source 45
    target 365
  ]
  edge [
    source 45
    target 366
  ]
  edge [
    source 45
    target 367
  ]
  edge [
    source 45
    target 368
  ]
  edge [
    source 45
    target 369
  ]
  edge [
    source 45
    target 370
  ]
  edge [
    source 45
    target 371
  ]
  edge [
    source 45
    target 668
  ]
  edge [
    source 45
    target 2132
  ]
  edge [
    source 45
    target 2133
  ]
  edge [
    source 45
    target 1007
  ]
  edge [
    source 45
    target 2134
  ]
  edge [
    source 45
    target 2135
  ]
  edge [
    source 45
    target 2136
  ]
  edge [
    source 45
    target 1301
  ]
  edge [
    source 45
    target 2137
  ]
  edge [
    source 45
    target 2138
  ]
  edge [
    source 45
    target 2139
  ]
  edge [
    source 45
    target 2140
  ]
  edge [
    source 45
    target 2141
  ]
  edge [
    source 45
    target 2142
  ]
  edge [
    source 45
    target 1388
  ]
  edge [
    source 45
    target 2143
  ]
  edge [
    source 45
    target 2144
  ]
  edge [
    source 45
    target 2145
  ]
  edge [
    source 45
    target 2146
  ]
  edge [
    source 45
    target 2147
  ]
  edge [
    source 45
    target 2148
  ]
  edge [
    source 45
    target 2149
  ]
  edge [
    source 45
    target 2150
  ]
  edge [
    source 45
    target 2151
  ]
  edge [
    source 45
    target 2152
  ]
  edge [
    source 45
    target 2153
  ]
  edge [
    source 45
    target 2154
  ]
  edge [
    source 45
    target 2155
  ]
  edge [
    source 45
    target 2156
  ]
  edge [
    source 45
    target 2157
  ]
  edge [
    source 45
    target 2158
  ]
  edge [
    source 45
    target 2159
  ]
  edge [
    source 45
    target 2160
  ]
  edge [
    source 45
    target 2161
  ]
  edge [
    source 45
    target 2162
  ]
  edge [
    source 45
    target 2163
  ]
  edge [
    source 45
    target 176
  ]
  edge [
    source 45
    target 112
  ]
  edge [
    source 45
    target 2164
  ]
  edge [
    source 45
    target 2165
  ]
  edge [
    source 45
    target 2166
  ]
  edge [
    source 45
    target 2167
  ]
  edge [
    source 45
    target 83
  ]
  edge [
    source 45
    target 2168
  ]
  edge [
    source 45
    target 2169
  ]
  edge [
    source 45
    target 2170
  ]
  edge [
    source 45
    target 2171
  ]
  edge [
    source 45
    target 2172
  ]
  edge [
    source 45
    target 2173
  ]
  edge [
    source 45
    target 2174
  ]
  edge [
    source 45
    target 2175
  ]
  edge [
    source 45
    target 2176
  ]
  edge [
    source 45
    target 2177
  ]
  edge [
    source 45
    target 2178
  ]
  edge [
    source 45
    target 2179
  ]
  edge [
    source 45
    target 2180
  ]
  edge [
    source 45
    target 2181
  ]
]
