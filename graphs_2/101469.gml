graph [
  node [
    id 0
    label "paolo"
    origin "text"
  ]
  node [
    id 1
    label "romeo"
    origin "text"
  ]
  node [
    id 2
    label "Paolo"
  ]
  node [
    id 3
    label "Romeo"
  ]
  node [
    id 4
    label "uniwersytet"
  ]
  node [
    id 5
    label "gregoria&#324;ski"
  ]
  node [
    id 6
    label "latera&#324;ski"
  ]
  node [
    id 7
    label "Angela"
  ]
  node [
    id 8
    label "Calabretta"
  ]
  node [
    id 9
    label "papieski"
  ]
  node [
    id 10
    label "akademia"
  ]
  node [
    id 11
    label "ko&#347;cielny"
  ]
  node [
    id 12
    label "stolica"
  ]
  node [
    id 13
    label "&#347;wi&#281;ty"
  ]
  node [
    id 14
    label "sekretariat"
  ]
  node [
    id 15
    label "stan"
  ]
  node [
    id 16
    label "Jan"
  ]
  node [
    id 17
    label "pawe&#322;"
  ]
  node [
    id 18
    label "ii"
  ]
  node [
    id 19
    label "san"
  ]
  node [
    id 20
    label "marina"
  ]
  node [
    id 21
    label "benedykta"
  ]
  node [
    id 22
    label "XVI"
  ]
  node [
    id 23
    label "Salvatore"
  ]
  node [
    id 24
    label "de"
  ]
  node [
    id 25
    label "Giorgi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
]
