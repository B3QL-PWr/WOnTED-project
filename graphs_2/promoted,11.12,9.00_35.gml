graph [
  node [
    id 0
    label "film"
    origin "text"
  ]
  node [
    id 1
    label "dokumentalny"
    origin "text"
  ]
  node [
    id 2
    label "traktowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "historia"
    origin "text"
  ]
  node [
    id 4
    label "gie&#322;da"
    origin "text"
  ]
  node [
    id 5
    label "komputerowy"
    origin "text"
  ]
  node [
    id 6
    label "polska"
    origin "text"
  ]
  node [
    id 7
    label "moment"
    origin "text"
  ]
  node [
    id 8
    label "powstanie"
    origin "text"
  ]
  node [
    id 9
    label "po&#322;owa"
    origin "text"
  ]
  node [
    id 10
    label "lato"
    origin "text"
  ]
  node [
    id 11
    label "ten"
    origin "text"
  ]
  node [
    id 12
    label "przez"
    origin "text"
  ]
  node [
    id 13
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 14
    label "istnienie"
    origin "text"
  ]
  node [
    id 15
    label "si&#281;"
    origin "text"
  ]
  node [
    id 16
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 17
    label "wej&#347;cie"
    origin "text"
  ]
  node [
    id 18
    label "ustawa"
    origin "text"
  ]
  node [
    id 19
    label "prawie"
    origin "text"
  ]
  node [
    id 20
    label "autorski"
    origin "text"
  ]
  node [
    id 21
    label "rok"
    origin "text"
  ]
  node [
    id 22
    label "wreszcie"
    origin "text"
  ]
  node [
    id 23
    label "upadek"
    origin "text"
  ]
  node [
    id 24
    label "animatronika"
  ]
  node [
    id 25
    label "odczulenie"
  ]
  node [
    id 26
    label "odczula&#263;"
  ]
  node [
    id 27
    label "blik"
  ]
  node [
    id 28
    label "odczuli&#263;"
  ]
  node [
    id 29
    label "scena"
  ]
  node [
    id 30
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 31
    label "muza"
  ]
  node [
    id 32
    label "postprodukcja"
  ]
  node [
    id 33
    label "block"
  ]
  node [
    id 34
    label "trawiarnia"
  ]
  node [
    id 35
    label "sklejarka"
  ]
  node [
    id 36
    label "sztuka"
  ]
  node [
    id 37
    label "uj&#281;cie"
  ]
  node [
    id 38
    label "filmoteka"
  ]
  node [
    id 39
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 40
    label "klatka"
  ]
  node [
    id 41
    label "rozbieg&#243;wka"
  ]
  node [
    id 42
    label "napisy"
  ]
  node [
    id 43
    label "ta&#347;ma"
  ]
  node [
    id 44
    label "odczulanie"
  ]
  node [
    id 45
    label "anamorfoza"
  ]
  node [
    id 46
    label "dorobek"
  ]
  node [
    id 47
    label "ty&#322;&#243;wka"
  ]
  node [
    id 48
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 49
    label "b&#322;ona"
  ]
  node [
    id 50
    label "emulsja_fotograficzna"
  ]
  node [
    id 51
    label "photograph"
  ]
  node [
    id 52
    label "czo&#322;&#243;wka"
  ]
  node [
    id 53
    label "rola"
  ]
  node [
    id 54
    label "&#347;cie&#380;ka"
  ]
  node [
    id 55
    label "wodorost"
  ]
  node [
    id 56
    label "webbing"
  ]
  node [
    id 57
    label "p&#243;&#322;produkt"
  ]
  node [
    id 58
    label "nagranie"
  ]
  node [
    id 59
    label "przewija&#263;_si&#281;"
  ]
  node [
    id 60
    label "kula"
  ]
  node [
    id 61
    label "pas"
  ]
  node [
    id 62
    label "watkowce"
  ]
  node [
    id 63
    label "zielenica"
  ]
  node [
    id 64
    label "ta&#347;moteka"
  ]
  node [
    id 65
    label "no&#347;nik_danych"
  ]
  node [
    id 66
    label "transporter"
  ]
  node [
    id 67
    label "hutnictwo"
  ]
  node [
    id 68
    label "klaps"
  ]
  node [
    id 69
    label "pasek"
  ]
  node [
    id 70
    label "artyku&#322;"
  ]
  node [
    id 71
    label "przewijanie_si&#281;"
  ]
  node [
    id 72
    label "blacha"
  ]
  node [
    id 73
    label "tkanka"
  ]
  node [
    id 74
    label "m&#243;zgoczaszka"
  ]
  node [
    id 75
    label "wytw&#243;r"
  ]
  node [
    id 76
    label "konto"
  ]
  node [
    id 77
    label "mienie"
  ]
  node [
    id 78
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 79
    label "wypracowa&#263;"
  ]
  node [
    id 80
    label "pr&#243;bowanie"
  ]
  node [
    id 81
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 82
    label "przedmiot"
  ]
  node [
    id 83
    label "cz&#322;owiek"
  ]
  node [
    id 84
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 85
    label "realizacja"
  ]
  node [
    id 86
    label "didaskalia"
  ]
  node [
    id 87
    label "czyn"
  ]
  node [
    id 88
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 89
    label "environment"
  ]
  node [
    id 90
    label "head"
  ]
  node [
    id 91
    label "scenariusz"
  ]
  node [
    id 92
    label "egzemplarz"
  ]
  node [
    id 93
    label "jednostka"
  ]
  node [
    id 94
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 95
    label "utw&#243;r"
  ]
  node [
    id 96
    label "kultura_duchowa"
  ]
  node [
    id 97
    label "fortel"
  ]
  node [
    id 98
    label "theatrical_performance"
  ]
  node [
    id 99
    label "ambala&#380;"
  ]
  node [
    id 100
    label "sprawno&#347;&#263;"
  ]
  node [
    id 101
    label "kobieta"
  ]
  node [
    id 102
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 103
    label "Faust"
  ]
  node [
    id 104
    label "scenografia"
  ]
  node [
    id 105
    label "ods&#322;ona"
  ]
  node [
    id 106
    label "turn"
  ]
  node [
    id 107
    label "pokaz"
  ]
  node [
    id 108
    label "ilo&#347;&#263;"
  ]
  node [
    id 109
    label "przedstawienie"
  ]
  node [
    id 110
    label "przedstawi&#263;"
  ]
  node [
    id 111
    label "Apollo"
  ]
  node [
    id 112
    label "kultura"
  ]
  node [
    id 113
    label "przedstawianie"
  ]
  node [
    id 114
    label "przedstawia&#263;"
  ]
  node [
    id 115
    label "towar"
  ]
  node [
    id 116
    label "inspiratorka"
  ]
  node [
    id 117
    label "banan"
  ]
  node [
    id 118
    label "talent"
  ]
  node [
    id 119
    label "Melpomena"
  ]
  node [
    id 120
    label "natchnienie"
  ]
  node [
    id 121
    label "bogini"
  ]
  node [
    id 122
    label "ro&#347;lina"
  ]
  node [
    id 123
    label "muzyka"
  ]
  node [
    id 124
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 125
    label "palma"
  ]
  node [
    id 126
    label "pocz&#261;tek"
  ]
  node [
    id 127
    label "ta&#347;ma_filmowa"
  ]
  node [
    id 128
    label "kle&#263;"
  ]
  node [
    id 129
    label "hodowla"
  ]
  node [
    id 130
    label "human_body"
  ]
  node [
    id 131
    label "miejsce"
  ]
  node [
    id 132
    label "pr&#281;t"
  ]
  node [
    id 133
    label "kopalnia"
  ]
  node [
    id 134
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 135
    label "pomieszczenie"
  ]
  node [
    id 136
    label "konstrukcja"
  ]
  node [
    id 137
    label "ogranicza&#263;"
  ]
  node [
    id 138
    label "sytuacja"
  ]
  node [
    id 139
    label "akwarium"
  ]
  node [
    id 140
    label "d&#378;wig"
  ]
  node [
    id 141
    label "technika"
  ]
  node [
    id 142
    label "kinematografia"
  ]
  node [
    id 143
    label "podwy&#380;szenie"
  ]
  node [
    id 144
    label "kurtyna"
  ]
  node [
    id 145
    label "akt"
  ]
  node [
    id 146
    label "widzownia"
  ]
  node [
    id 147
    label "sznurownia"
  ]
  node [
    id 148
    label "dramaturgy"
  ]
  node [
    id 149
    label "sphere"
  ]
  node [
    id 150
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 151
    label "budka_suflera"
  ]
  node [
    id 152
    label "epizod"
  ]
  node [
    id 153
    label "wydarzenie"
  ]
  node [
    id 154
    label "fragment"
  ]
  node [
    id 155
    label "k&#322;&#243;tnia"
  ]
  node [
    id 156
    label "kiesze&#324;"
  ]
  node [
    id 157
    label "stadium"
  ]
  node [
    id 158
    label "podest"
  ]
  node [
    id 159
    label "horyzont"
  ]
  node [
    id 160
    label "teren"
  ]
  node [
    id 161
    label "instytucja"
  ]
  node [
    id 162
    label "proscenium"
  ]
  node [
    id 163
    label "nadscenie"
  ]
  node [
    id 164
    label "antyteatr"
  ]
  node [
    id 165
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 166
    label "pochwytanie"
  ]
  node [
    id 167
    label "wording"
  ]
  node [
    id 168
    label "wzbudzenie"
  ]
  node [
    id 169
    label "withdrawal"
  ]
  node [
    id 170
    label "capture"
  ]
  node [
    id 171
    label "podniesienie"
  ]
  node [
    id 172
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 173
    label "zapisanie"
  ]
  node [
    id 174
    label "prezentacja"
  ]
  node [
    id 175
    label "rzucenie"
  ]
  node [
    id 176
    label "zamkni&#281;cie"
  ]
  node [
    id 177
    label "zabranie"
  ]
  node [
    id 178
    label "poinformowanie"
  ]
  node [
    id 179
    label "zaaresztowanie"
  ]
  node [
    id 180
    label "strona"
  ]
  node [
    id 181
    label "wzi&#281;cie"
  ]
  node [
    id 182
    label "materia&#322;"
  ]
  node [
    id 183
    label "rz&#261;d"
  ]
  node [
    id 184
    label "alpinizm"
  ]
  node [
    id 185
    label "wst&#281;p"
  ]
  node [
    id 186
    label "bieg"
  ]
  node [
    id 187
    label "elita"
  ]
  node [
    id 188
    label "rajd"
  ]
  node [
    id 189
    label "poligrafia"
  ]
  node [
    id 190
    label "pododdzia&#322;"
  ]
  node [
    id 191
    label "latarka_czo&#322;owa"
  ]
  node [
    id 192
    label "grupa"
  ]
  node [
    id 193
    label "&#347;ciana"
  ]
  node [
    id 194
    label "zderzenie"
  ]
  node [
    id 195
    label "front"
  ]
  node [
    id 196
    label "fina&#322;"
  ]
  node [
    id 197
    label "uprawienie"
  ]
  node [
    id 198
    label "kszta&#322;t"
  ]
  node [
    id 199
    label "dialog"
  ]
  node [
    id 200
    label "p&#322;osa"
  ]
  node [
    id 201
    label "wykonywanie"
  ]
  node [
    id 202
    label "plik"
  ]
  node [
    id 203
    label "ziemia"
  ]
  node [
    id 204
    label "wykonywa&#263;"
  ]
  node [
    id 205
    label "ustawienie"
  ]
  node [
    id 206
    label "pole"
  ]
  node [
    id 207
    label "gospodarstwo"
  ]
  node [
    id 208
    label "uprawi&#263;"
  ]
  node [
    id 209
    label "function"
  ]
  node [
    id 210
    label "posta&#263;"
  ]
  node [
    id 211
    label "zreinterpretowa&#263;"
  ]
  node [
    id 212
    label "zastosowanie"
  ]
  node [
    id 213
    label "reinterpretowa&#263;"
  ]
  node [
    id 214
    label "wrench"
  ]
  node [
    id 215
    label "irygowanie"
  ]
  node [
    id 216
    label "ustawi&#263;"
  ]
  node [
    id 217
    label "irygowa&#263;"
  ]
  node [
    id 218
    label "zreinterpretowanie"
  ]
  node [
    id 219
    label "cel"
  ]
  node [
    id 220
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 221
    label "gra&#263;"
  ]
  node [
    id 222
    label "aktorstwo"
  ]
  node [
    id 223
    label "kostium"
  ]
  node [
    id 224
    label "zagon"
  ]
  node [
    id 225
    label "znaczenie"
  ]
  node [
    id 226
    label "zagra&#263;"
  ]
  node [
    id 227
    label "reinterpretowanie"
  ]
  node [
    id 228
    label "sk&#322;ad"
  ]
  node [
    id 229
    label "tekst"
  ]
  node [
    id 230
    label "zagranie"
  ]
  node [
    id 231
    label "radlina"
  ]
  node [
    id 232
    label "granie"
  ]
  node [
    id 233
    label "farba"
  ]
  node [
    id 234
    label "odblask"
  ]
  node [
    id 235
    label "plama"
  ]
  node [
    id 236
    label "urz&#261;dzenie"
  ]
  node [
    id 237
    label "przyrz&#261;d_mechaniczny"
  ]
  node [
    id 238
    label "alergia"
  ]
  node [
    id 239
    label "usuni&#281;cie"
  ]
  node [
    id 240
    label "zmniejszenie"
  ]
  node [
    id 241
    label "wyleczenie"
  ]
  node [
    id 242
    label "desensitization"
  ]
  node [
    id 243
    label "zmniejszanie"
  ]
  node [
    id 244
    label "usuwanie"
  ]
  node [
    id 245
    label "terapia"
  ]
  node [
    id 246
    label "usun&#261;&#263;"
  ]
  node [
    id 247
    label "wyleczy&#263;"
  ]
  node [
    id 248
    label "zmniejszy&#263;"
  ]
  node [
    id 249
    label "leczy&#263;"
  ]
  node [
    id 250
    label "usuwa&#263;"
  ]
  node [
    id 251
    label "zmniejsza&#263;"
  ]
  node [
    id 252
    label "przek&#322;ad"
  ]
  node [
    id 253
    label "dialogista"
  ]
  node [
    id 254
    label "proces_biologiczny"
  ]
  node [
    id 255
    label "zamiana"
  ]
  node [
    id 256
    label "deformacja"
  ]
  node [
    id 257
    label "faza"
  ]
  node [
    id 258
    label "archiwum"
  ]
  node [
    id 259
    label "dokumentalnie"
  ]
  node [
    id 260
    label "robi&#263;"
  ]
  node [
    id 261
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 262
    label "poddawa&#263;"
  ]
  node [
    id 263
    label "dotyczy&#263;"
  ]
  node [
    id 264
    label "use"
  ]
  node [
    id 265
    label "treat"
  ]
  node [
    id 266
    label "oferowa&#263;"
  ]
  node [
    id 267
    label "introduce"
  ]
  node [
    id 268
    label "deliver"
  ]
  node [
    id 269
    label "opowiada&#263;"
  ]
  node [
    id 270
    label "krzywdzi&#263;"
  ]
  node [
    id 271
    label "organizowa&#263;"
  ]
  node [
    id 272
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 273
    label "czyni&#263;"
  ]
  node [
    id 274
    label "give"
  ]
  node [
    id 275
    label "stylizowa&#263;"
  ]
  node [
    id 276
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 277
    label "falowa&#263;"
  ]
  node [
    id 278
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 279
    label "peddle"
  ]
  node [
    id 280
    label "praca"
  ]
  node [
    id 281
    label "wydala&#263;"
  ]
  node [
    id 282
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 283
    label "tentegowa&#263;"
  ]
  node [
    id 284
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 285
    label "urz&#261;dza&#263;"
  ]
  node [
    id 286
    label "oszukiwa&#263;"
  ]
  node [
    id 287
    label "work"
  ]
  node [
    id 288
    label "ukazywa&#263;"
  ]
  node [
    id 289
    label "przerabia&#263;"
  ]
  node [
    id 290
    label "act"
  ]
  node [
    id 291
    label "post&#281;powa&#263;"
  ]
  node [
    id 292
    label "podpowiada&#263;"
  ]
  node [
    id 293
    label "render"
  ]
  node [
    id 294
    label "decydowa&#263;"
  ]
  node [
    id 295
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 296
    label "rezygnowa&#263;"
  ]
  node [
    id 297
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 298
    label "bargain"
  ]
  node [
    id 299
    label "tycze&#263;"
  ]
  node [
    id 300
    label "historiografia"
  ]
  node [
    id 301
    label "nauka_humanistyczna"
  ]
  node [
    id 302
    label "nautologia"
  ]
  node [
    id 303
    label "epigrafika"
  ]
  node [
    id 304
    label "muzealnictwo"
  ]
  node [
    id 305
    label "report"
  ]
  node [
    id 306
    label "hista"
  ]
  node [
    id 307
    label "przebiec"
  ]
  node [
    id 308
    label "zabytkoznawstwo"
  ]
  node [
    id 309
    label "historia_gospodarcza"
  ]
  node [
    id 310
    label "motyw"
  ]
  node [
    id 311
    label "kierunek"
  ]
  node [
    id 312
    label "varsavianistyka"
  ]
  node [
    id 313
    label "filigranistyka"
  ]
  node [
    id 314
    label "neografia"
  ]
  node [
    id 315
    label "prezentyzm"
  ]
  node [
    id 316
    label "genealogia"
  ]
  node [
    id 317
    label "ikonografia"
  ]
  node [
    id 318
    label "bizantynistyka"
  ]
  node [
    id 319
    label "epoka"
  ]
  node [
    id 320
    label "historia_sztuki"
  ]
  node [
    id 321
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 322
    label "ruralistyka"
  ]
  node [
    id 323
    label "annalistyka"
  ]
  node [
    id 324
    label "charakter"
  ]
  node [
    id 325
    label "papirologia"
  ]
  node [
    id 326
    label "heraldyka"
  ]
  node [
    id 327
    label "archiwistyka"
  ]
  node [
    id 328
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 329
    label "dyplomatyka"
  ]
  node [
    id 330
    label "czynno&#347;&#263;"
  ]
  node [
    id 331
    label "numizmatyka"
  ]
  node [
    id 332
    label "chronologia"
  ]
  node [
    id 333
    label "wypowied&#378;"
  ]
  node [
    id 334
    label "historyka"
  ]
  node [
    id 335
    label "prozopografia"
  ]
  node [
    id 336
    label "sfragistyka"
  ]
  node [
    id 337
    label "weksylologia"
  ]
  node [
    id 338
    label "paleografia"
  ]
  node [
    id 339
    label "mediewistyka"
  ]
  node [
    id 340
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 341
    label "przebiegni&#281;cie"
  ]
  node [
    id 342
    label "fabu&#322;a"
  ]
  node [
    id 343
    label "koleje_losu"
  ]
  node [
    id 344
    label "&#380;ycie"
  ]
  node [
    id 345
    label "czas"
  ]
  node [
    id 346
    label "zboczenie"
  ]
  node [
    id 347
    label "om&#243;wienie"
  ]
  node [
    id 348
    label "sponiewieranie"
  ]
  node [
    id 349
    label "discipline"
  ]
  node [
    id 350
    label "rzecz"
  ]
  node [
    id 351
    label "omawia&#263;"
  ]
  node [
    id 352
    label "kr&#261;&#380;enie"
  ]
  node [
    id 353
    label "tre&#347;&#263;"
  ]
  node [
    id 354
    label "robienie"
  ]
  node [
    id 355
    label "sponiewiera&#263;"
  ]
  node [
    id 356
    label "element"
  ]
  node [
    id 357
    label "entity"
  ]
  node [
    id 358
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 359
    label "tematyka"
  ]
  node [
    id 360
    label "w&#261;tek"
  ]
  node [
    id 361
    label "zbaczanie"
  ]
  node [
    id 362
    label "program_nauczania"
  ]
  node [
    id 363
    label "om&#243;wi&#263;"
  ]
  node [
    id 364
    label "omawianie"
  ]
  node [
    id 365
    label "thing"
  ]
  node [
    id 366
    label "istota"
  ]
  node [
    id 367
    label "zbacza&#263;"
  ]
  node [
    id 368
    label "zboczy&#263;"
  ]
  node [
    id 369
    label "pos&#322;uchanie"
  ]
  node [
    id 370
    label "s&#261;d"
  ]
  node [
    id 371
    label "sparafrazowanie"
  ]
  node [
    id 372
    label "strawestowa&#263;"
  ]
  node [
    id 373
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 374
    label "trawestowa&#263;"
  ]
  node [
    id 375
    label "sparafrazowa&#263;"
  ]
  node [
    id 376
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 377
    label "sformu&#322;owanie"
  ]
  node [
    id 378
    label "parafrazowanie"
  ]
  node [
    id 379
    label "ozdobnik"
  ]
  node [
    id 380
    label "delimitacja"
  ]
  node [
    id 381
    label "parafrazowa&#263;"
  ]
  node [
    id 382
    label "stylizacja"
  ]
  node [
    id 383
    label "komunikat"
  ]
  node [
    id 384
    label "trawestowanie"
  ]
  node [
    id 385
    label "strawestowanie"
  ]
  node [
    id 386
    label "rezultat"
  ]
  node [
    id 387
    label "przebieg"
  ]
  node [
    id 388
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 389
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 390
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 391
    label "praktyka"
  ]
  node [
    id 392
    label "system"
  ]
  node [
    id 393
    label "przeorientowywanie"
  ]
  node [
    id 394
    label "studia"
  ]
  node [
    id 395
    label "linia"
  ]
  node [
    id 396
    label "bok"
  ]
  node [
    id 397
    label "skr&#281;canie"
  ]
  node [
    id 398
    label "skr&#281;ca&#263;"
  ]
  node [
    id 399
    label "przeorientowywa&#263;"
  ]
  node [
    id 400
    label "orientowanie"
  ]
  node [
    id 401
    label "skr&#281;ci&#263;"
  ]
  node [
    id 402
    label "przeorientowanie"
  ]
  node [
    id 403
    label "zorientowanie"
  ]
  node [
    id 404
    label "przeorientowa&#263;"
  ]
  node [
    id 405
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 406
    label "metoda"
  ]
  node [
    id 407
    label "ty&#322;"
  ]
  node [
    id 408
    label "zorientowa&#263;"
  ]
  node [
    id 409
    label "g&#243;ra"
  ]
  node [
    id 410
    label "orientowa&#263;"
  ]
  node [
    id 411
    label "spos&#243;b"
  ]
  node [
    id 412
    label "ideologia"
  ]
  node [
    id 413
    label "orientacja"
  ]
  node [
    id 414
    label "prz&#243;d"
  ]
  node [
    id 415
    label "bearing"
  ]
  node [
    id 416
    label "skr&#281;cenie"
  ]
  node [
    id 417
    label "aalen"
  ]
  node [
    id 418
    label "jura_wczesna"
  ]
  node [
    id 419
    label "holocen"
  ]
  node [
    id 420
    label "pliocen"
  ]
  node [
    id 421
    label "plejstocen"
  ]
  node [
    id 422
    label "paleocen"
  ]
  node [
    id 423
    label "dzieje"
  ]
  node [
    id 424
    label "bajos"
  ]
  node [
    id 425
    label "kelowej"
  ]
  node [
    id 426
    label "eocen"
  ]
  node [
    id 427
    label "jednostka_geologiczna"
  ]
  node [
    id 428
    label "okres"
  ]
  node [
    id 429
    label "schy&#322;ek"
  ]
  node [
    id 430
    label "miocen"
  ]
  node [
    id 431
    label "&#347;rodkowy_trias"
  ]
  node [
    id 432
    label "term"
  ]
  node [
    id 433
    label "Zeitgeist"
  ]
  node [
    id 434
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 435
    label "wczesny_trias"
  ]
  node [
    id 436
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 437
    label "jura_&#347;rodkowa"
  ]
  node [
    id 438
    label "oligocen"
  ]
  node [
    id 439
    label "w&#281;ze&#322;"
  ]
  node [
    id 440
    label "perypetia"
  ]
  node [
    id 441
    label "opowiadanie"
  ]
  node [
    id 442
    label "datacja"
  ]
  node [
    id 443
    label "dendrochronologia"
  ]
  node [
    id 444
    label "kolejno&#347;&#263;"
  ]
  node [
    id 445
    label "plastyka"
  ]
  node [
    id 446
    label "&#347;redniowiecze"
  ]
  node [
    id 447
    label "descendencja"
  ]
  node [
    id 448
    label "drzewo_genealogiczne"
  ]
  node [
    id 449
    label "procedencja"
  ]
  node [
    id 450
    label "pochodzenie"
  ]
  node [
    id 451
    label "medal"
  ]
  node [
    id 452
    label "kolekcjonerstwo"
  ]
  node [
    id 453
    label "numismatics"
  ]
  node [
    id 454
    label "archeologia"
  ]
  node [
    id 455
    label "archiwoznawstwo"
  ]
  node [
    id 456
    label "Byzantine_Empire"
  ]
  node [
    id 457
    label "pismo"
  ]
  node [
    id 458
    label "brachygrafia"
  ]
  node [
    id 459
    label "architektura"
  ]
  node [
    id 460
    label "nauka"
  ]
  node [
    id 461
    label "oksza"
  ]
  node [
    id 462
    label "s&#322;up"
  ]
  node [
    id 463
    label "barwa_heraldyczna"
  ]
  node [
    id 464
    label "herb"
  ]
  node [
    id 465
    label "or&#281;&#380;"
  ]
  node [
    id 466
    label "museum"
  ]
  node [
    id 467
    label "bibliologia"
  ]
  node [
    id 468
    label "historiography"
  ]
  node [
    id 469
    label "pi&#347;miennictwo"
  ]
  node [
    id 470
    label "metodologia"
  ]
  node [
    id 471
    label "fraza"
  ]
  node [
    id 472
    label "temat"
  ]
  node [
    id 473
    label "melodia"
  ]
  node [
    id 474
    label "cecha"
  ]
  node [
    id 475
    label "przyczyna"
  ]
  node [
    id 476
    label "ozdoba"
  ]
  node [
    id 477
    label "umowa"
  ]
  node [
    id 478
    label "cover"
  ]
  node [
    id 479
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 480
    label "zbi&#243;r"
  ]
  node [
    id 481
    label "osobowo&#347;&#263;"
  ]
  node [
    id 482
    label "psychika"
  ]
  node [
    id 483
    label "kompleksja"
  ]
  node [
    id 484
    label "fizjonomia"
  ]
  node [
    id 485
    label "zjawisko"
  ]
  node [
    id 486
    label "activity"
  ]
  node [
    id 487
    label "bezproblemowy"
  ]
  node [
    id 488
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 489
    label "przeby&#263;"
  ]
  node [
    id 490
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 491
    label "run"
  ]
  node [
    id 492
    label "proceed"
  ]
  node [
    id 493
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 494
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 495
    label "przemierzy&#263;"
  ]
  node [
    id 496
    label "fly"
  ]
  node [
    id 497
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 498
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 499
    label "przesun&#261;&#263;"
  ]
  node [
    id 500
    label "przemkni&#281;cie"
  ]
  node [
    id 501
    label "zabrzmienie"
  ]
  node [
    id 502
    label "przebycie"
  ]
  node [
    id 503
    label "zdarzenie_si&#281;"
  ]
  node [
    id 504
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 505
    label "odreagowywa&#263;"
  ]
  node [
    id 506
    label "targ"
  ]
  node [
    id 507
    label "arbitra&#380;"
  ]
  node [
    id 508
    label "odreagowanie"
  ]
  node [
    id 509
    label "dzie&#324;_trzech_wied&#378;m"
  ]
  node [
    id 510
    label "odreagowywanie"
  ]
  node [
    id 511
    label "odreagowa&#263;"
  ]
  node [
    id 512
    label "sesja"
  ]
  node [
    id 513
    label "byk"
  ]
  node [
    id 514
    label "nied&#378;wied&#378;"
  ]
  node [
    id 515
    label "osoba_prawna"
  ]
  node [
    id 516
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 517
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 518
    label "poj&#281;cie"
  ]
  node [
    id 519
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 520
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 521
    label "biuro"
  ]
  node [
    id 522
    label "organizacja"
  ]
  node [
    id 523
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 524
    label "Fundusze_Unijne"
  ]
  node [
    id 525
    label "zamyka&#263;"
  ]
  node [
    id 526
    label "establishment"
  ]
  node [
    id 527
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 528
    label "urz&#261;d"
  ]
  node [
    id 529
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 530
    label "afiliowa&#263;"
  ]
  node [
    id 531
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 532
    label "standard"
  ]
  node [
    id 533
    label "zamykanie"
  ]
  node [
    id 534
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 535
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 536
    label "plac"
  ]
  node [
    id 537
    label "market"
  ]
  node [
    id 538
    label "stoisko"
  ]
  node [
    id 539
    label "sprzeda&#380;"
  ]
  node [
    id 540
    label "obiekt_handlowy"
  ]
  node [
    id 541
    label "targowica"
  ]
  node [
    id 542
    label "kram"
  ]
  node [
    id 543
    label "pozbycie_si&#281;"
  ]
  node [
    id 544
    label "zniwelowanie"
  ]
  node [
    id 545
    label "catharsis"
  ]
  node [
    id 546
    label "uspokojenie_si&#281;"
  ]
  node [
    id 547
    label "tracenie"
  ]
  node [
    id 548
    label "niwelowanie"
  ]
  node [
    id 549
    label "pozbywanie_si&#281;"
  ]
  node [
    id 550
    label "pozbywa&#263;_si&#281;"
  ]
  node [
    id 551
    label "niwelowa&#263;"
  ]
  node [
    id 552
    label "zarabia&#263;"
  ]
  node [
    id 553
    label "g&#243;ra_mi&#281;sa"
  ]
  node [
    id 554
    label "bydl&#281;"
  ]
  node [
    id 555
    label "strategia_byka"
  ]
  node [
    id 556
    label "si&#322;acz"
  ]
  node [
    id 557
    label "gie&#322;da_papier&#243;w_warto&#347;ciowych"
  ]
  node [
    id 558
    label "brat"
  ]
  node [
    id 559
    label "cios"
  ]
  node [
    id 560
    label "b&#322;&#261;d"
  ]
  node [
    id 561
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 562
    label "symbol"
  ]
  node [
    id 563
    label "bull"
  ]
  node [
    id 564
    label "inwestor"
  ]
  node [
    id 565
    label "samiec"
  ]
  node [
    id 566
    label "optymista"
  ]
  node [
    id 567
    label "olbrzym"
  ]
  node [
    id 568
    label "zniwelowa&#263;"
  ]
  node [
    id 569
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 570
    label "zarobi&#263;"
  ]
  node [
    id 571
    label "pojednawstwo"
  ]
  node [
    id 572
    label "kurs_walutowy"
  ]
  node [
    id 573
    label "transakcja"
  ]
  node [
    id 574
    label "gawrowanie"
  ]
  node [
    id 575
    label "mi&#347;"
  ]
  node [
    id 576
    label "gawrowa&#263;"
  ]
  node [
    id 577
    label "gawra"
  ]
  node [
    id 578
    label "wszystko&#380;erca"
  ]
  node [
    id 579
    label "ch&#322;op_jak_d&#261;b"
  ]
  node [
    id 580
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 581
    label "strategia_nied&#378;wiedzia"
  ]
  node [
    id 582
    label "nied&#378;wiedziowate"
  ]
  node [
    id 583
    label "marucha"
  ]
  node [
    id 584
    label "egzamin"
  ]
  node [
    id 585
    label "dzie&#324;_pracy"
  ]
  node [
    id 586
    label "dogrywka"
  ]
  node [
    id 587
    label "spotkanie"
  ]
  node [
    id 588
    label "seria"
  ]
  node [
    id 589
    label "rok_akademicki"
  ]
  node [
    id 590
    label "konsylium"
  ]
  node [
    id 591
    label "psychoterapia"
  ]
  node [
    id 592
    label "obiekt"
  ]
  node [
    id 593
    label "sesyjka"
  ]
  node [
    id 594
    label "conference"
  ]
  node [
    id 595
    label "dyskusja"
  ]
  node [
    id 596
    label "komputerowo"
  ]
  node [
    id 597
    label "time"
  ]
  node [
    id 598
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 599
    label "okres_czasu"
  ]
  node [
    id 600
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 601
    label "chron"
  ]
  node [
    id 602
    label "minute"
  ]
  node [
    id 603
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 604
    label "wiek"
  ]
  node [
    id 605
    label "stworzenie"
  ]
  node [
    id 606
    label "walka"
  ]
  node [
    id 607
    label "koliszczyzna"
  ]
  node [
    id 608
    label "zbuntowanie_si&#281;"
  ]
  node [
    id 609
    label "Ko&#347;ciuszko"
  ]
  node [
    id 610
    label "powstanie_tambowskie"
  ]
  node [
    id 611
    label "odbudowanie_si&#281;"
  ]
  node [
    id 612
    label "powstanie_listopadowe"
  ]
  node [
    id 613
    label "origin"
  ]
  node [
    id 614
    label "&#380;akieria"
  ]
  node [
    id 615
    label "zaistnienie"
  ]
  node [
    id 616
    label "potworzenie_si&#281;"
  ]
  node [
    id 617
    label "geneza"
  ]
  node [
    id 618
    label "orgy"
  ]
  node [
    id 619
    label "beginning"
  ]
  node [
    id 620
    label "utworzenie"
  ]
  node [
    id 621
    label "le&#380;enie"
  ]
  node [
    id 622
    label "kl&#281;czenie"
  ]
  node [
    id 623
    label "chmielnicczyzna"
  ]
  node [
    id 624
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 625
    label "pierwocina"
  ]
  node [
    id 626
    label "powstanie_warszawskie"
  ]
  node [
    id 627
    label "uniesienie_si&#281;"
  ]
  node [
    id 628
    label "siedzenie"
  ]
  node [
    id 629
    label "obrona"
  ]
  node [
    id 630
    label "zaatakowanie"
  ]
  node [
    id 631
    label "konfrontacyjny"
  ]
  node [
    id 632
    label "contest"
  ]
  node [
    id 633
    label "action"
  ]
  node [
    id 634
    label "sambo"
  ]
  node [
    id 635
    label "rywalizacja"
  ]
  node [
    id 636
    label "trudno&#347;&#263;"
  ]
  node [
    id 637
    label "sp&#243;r"
  ]
  node [
    id 638
    label "wrestle"
  ]
  node [
    id 639
    label "military_action"
  ]
  node [
    id 640
    label "wojna_stuletnia"
  ]
  node [
    id 641
    label "po&#322;o&#380;enie"
  ]
  node [
    id 642
    label "bycie"
  ]
  node [
    id 643
    label "pobyczenie_si&#281;"
  ]
  node [
    id 644
    label "tarzanie_si&#281;"
  ]
  node [
    id 645
    label "trwanie"
  ]
  node [
    id 646
    label "zwierz&#281;"
  ]
  node [
    id 647
    label "wstanie"
  ]
  node [
    id 648
    label "przele&#380;enie"
  ]
  node [
    id 649
    label "odpowiedni"
  ]
  node [
    id 650
    label "zlegni&#281;cie"
  ]
  node [
    id 651
    label "fit"
  ]
  node [
    id 652
    label "spoczywanie"
  ]
  node [
    id 653
    label "pole&#380;enie"
  ]
  node [
    id 654
    label "odsiedzenie"
  ]
  node [
    id 655
    label "wysiadywanie"
  ]
  node [
    id 656
    label "odsiadywanie"
  ]
  node [
    id 657
    label "otoczenie_si&#281;"
  ]
  node [
    id 658
    label "posiedzenie"
  ]
  node [
    id 659
    label "wysiedzenie"
  ]
  node [
    id 660
    label "posadzenie"
  ]
  node [
    id 661
    label "wychodzenie"
  ]
  node [
    id 662
    label "zajmowanie_si&#281;"
  ]
  node [
    id 663
    label "tkwienie"
  ]
  node [
    id 664
    label "sadzanie"
  ]
  node [
    id 665
    label "trybuna"
  ]
  node [
    id 666
    label "ocieranie_si&#281;"
  ]
  node [
    id 667
    label "room"
  ]
  node [
    id 668
    label "jadalnia"
  ]
  node [
    id 669
    label "residency"
  ]
  node [
    id 670
    label "pupa"
  ]
  node [
    id 671
    label "samolot"
  ]
  node [
    id 672
    label "touch"
  ]
  node [
    id 673
    label "otarcie_si&#281;"
  ]
  node [
    id 674
    label "position"
  ]
  node [
    id 675
    label "otaczanie_si&#281;"
  ]
  node [
    id 676
    label "wyj&#347;cie"
  ]
  node [
    id 677
    label "przedzia&#322;"
  ]
  node [
    id 678
    label "umieszczenie"
  ]
  node [
    id 679
    label "mieszkanie"
  ]
  node [
    id 680
    label "przebywanie"
  ]
  node [
    id 681
    label "ujmowanie"
  ]
  node [
    id 682
    label "autobus"
  ]
  node [
    id 683
    label "czynnik"
  ]
  node [
    id 684
    label "proces"
  ]
  node [
    id 685
    label "zesp&#243;&#322;"
  ]
  node [
    id 686
    label "rodny"
  ]
  node [
    id 687
    label "monogeneza"
  ]
  node [
    id 688
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 689
    label "foundation"
  ]
  node [
    id 690
    label "potworzenie"
  ]
  node [
    id 691
    label "erecting"
  ]
  node [
    id 692
    label "ukszta&#322;towanie"
  ]
  node [
    id 693
    label "stanie_si&#281;"
  ]
  node [
    id 694
    label "zorganizowanie"
  ]
  node [
    id 695
    label "zrobienie"
  ]
  node [
    id 696
    label "apparition"
  ]
  node [
    id 697
    label "nap&#281;dzenie"
  ]
  node [
    id 698
    label "obiekt_naturalny"
  ]
  node [
    id 699
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 700
    label "ekosystem"
  ]
  node [
    id 701
    label "organizm"
  ]
  node [
    id 702
    label "biota"
  ]
  node [
    id 703
    label "pope&#322;nienie"
  ]
  node [
    id 704
    label "wizerunek"
  ]
  node [
    id 705
    label "wszechstworzenie"
  ]
  node [
    id 706
    label "woda"
  ]
  node [
    id 707
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 708
    label "mikrokosmos"
  ]
  node [
    id 709
    label "stw&#243;r"
  ]
  node [
    id 710
    label "Ziemia"
  ]
  node [
    id 711
    label "cia&#322;o"
  ]
  node [
    id 712
    label "fauna"
  ]
  node [
    id 713
    label "debiut"
  ]
  node [
    id 714
    label "medium"
  ]
  node [
    id 715
    label "poprzedzanie"
  ]
  node [
    id 716
    label "czasoprzestrze&#324;"
  ]
  node [
    id 717
    label "laba"
  ]
  node [
    id 718
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 719
    label "chronometria"
  ]
  node [
    id 720
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 721
    label "rachuba_czasu"
  ]
  node [
    id 722
    label "przep&#322;ywanie"
  ]
  node [
    id 723
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 724
    label "czasokres"
  ]
  node [
    id 725
    label "odczyt"
  ]
  node [
    id 726
    label "chwila"
  ]
  node [
    id 727
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 728
    label "kategoria_gramatyczna"
  ]
  node [
    id 729
    label "poprzedzenie"
  ]
  node [
    id 730
    label "trawienie"
  ]
  node [
    id 731
    label "pochodzi&#263;"
  ]
  node [
    id 732
    label "period"
  ]
  node [
    id 733
    label "poprzedza&#263;"
  ]
  node [
    id 734
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 735
    label "odwlekanie_si&#281;"
  ]
  node [
    id 736
    label "zegar"
  ]
  node [
    id 737
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 738
    label "czwarty_wymiar"
  ]
  node [
    id 739
    label "koniugacja"
  ]
  node [
    id 740
    label "trawi&#263;"
  ]
  node [
    id 741
    label "pogoda"
  ]
  node [
    id 742
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 743
    label "poprzedzi&#263;"
  ]
  node [
    id 744
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 745
    label "time_period"
  ]
  node [
    id 746
    label "Rzym_Zachodni"
  ]
  node [
    id 747
    label "whole"
  ]
  node [
    id 748
    label "Rzym_Wschodni"
  ]
  node [
    id 749
    label "&#347;rodek"
  ]
  node [
    id 750
    label "jasnowidz"
  ]
  node [
    id 751
    label "hipnoza"
  ]
  node [
    id 752
    label "spirytysta"
  ]
  node [
    id 753
    label "otoczenie"
  ]
  node [
    id 754
    label "publikator"
  ]
  node [
    id 755
    label "przekazior"
  ]
  node [
    id 756
    label "warunki"
  ]
  node [
    id 757
    label "pora_roku"
  ]
  node [
    id 758
    label "okre&#347;lony"
  ]
  node [
    id 759
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 760
    label "wiadomy"
  ]
  node [
    id 761
    label "jednostka_monetarna"
  ]
  node [
    id 762
    label "wspania&#322;y"
  ]
  node [
    id 763
    label "metaliczny"
  ]
  node [
    id 764
    label "Polska"
  ]
  node [
    id 765
    label "szlachetny"
  ]
  node [
    id 766
    label "kochany"
  ]
  node [
    id 767
    label "doskona&#322;y"
  ]
  node [
    id 768
    label "grosz"
  ]
  node [
    id 769
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 770
    label "poz&#322;ocenie"
  ]
  node [
    id 771
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 772
    label "utytu&#322;owany"
  ]
  node [
    id 773
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 774
    label "z&#322;ocenie"
  ]
  node [
    id 775
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 776
    label "prominentny"
  ]
  node [
    id 777
    label "znany"
  ]
  node [
    id 778
    label "wybitny"
  ]
  node [
    id 779
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 780
    label "naj"
  ]
  node [
    id 781
    label "&#347;wietny"
  ]
  node [
    id 782
    label "pe&#322;ny"
  ]
  node [
    id 783
    label "doskonale"
  ]
  node [
    id 784
    label "szlachetnie"
  ]
  node [
    id 785
    label "uczciwy"
  ]
  node [
    id 786
    label "zacny"
  ]
  node [
    id 787
    label "harmonijny"
  ]
  node [
    id 788
    label "gatunkowy"
  ]
  node [
    id 789
    label "pi&#281;kny"
  ]
  node [
    id 790
    label "dobry"
  ]
  node [
    id 791
    label "typowy"
  ]
  node [
    id 792
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 793
    label "metaloplastyczny"
  ]
  node [
    id 794
    label "metalicznie"
  ]
  node [
    id 795
    label "kochanek"
  ]
  node [
    id 796
    label "wybranek"
  ]
  node [
    id 797
    label "umi&#322;owany"
  ]
  node [
    id 798
    label "drogi"
  ]
  node [
    id 799
    label "kochanie"
  ]
  node [
    id 800
    label "wspaniale"
  ]
  node [
    id 801
    label "pomy&#347;lny"
  ]
  node [
    id 802
    label "pozytywny"
  ]
  node [
    id 803
    label "&#347;wietnie"
  ]
  node [
    id 804
    label "spania&#322;y"
  ]
  node [
    id 805
    label "och&#281;do&#380;ny"
  ]
  node [
    id 806
    label "warto&#347;ciowy"
  ]
  node [
    id 807
    label "zajebisty"
  ]
  node [
    id 808
    label "bogato"
  ]
  node [
    id 809
    label "typ_mongoloidalny"
  ]
  node [
    id 810
    label "kolorowy"
  ]
  node [
    id 811
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 812
    label "ciep&#322;y"
  ]
  node [
    id 813
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 814
    label "jasny"
  ]
  node [
    id 815
    label "kwota"
  ]
  node [
    id 816
    label "groszak"
  ]
  node [
    id 817
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 818
    label "szyling_austryjacki"
  ]
  node [
    id 819
    label "moneta"
  ]
  node [
    id 820
    label "Mazowsze"
  ]
  node [
    id 821
    label "Pa&#322;uki"
  ]
  node [
    id 822
    label "Pomorze_Zachodnie"
  ]
  node [
    id 823
    label "Powi&#347;le"
  ]
  node [
    id 824
    label "Wolin"
  ]
  node [
    id 825
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 826
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 827
    label "So&#322;a"
  ]
  node [
    id 828
    label "Unia_Europejska"
  ]
  node [
    id 829
    label "Krajna"
  ]
  node [
    id 830
    label "Opolskie"
  ]
  node [
    id 831
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 832
    label "Suwalszczyzna"
  ]
  node [
    id 833
    label "barwy_polskie"
  ]
  node [
    id 834
    label "Nadbu&#380;e"
  ]
  node [
    id 835
    label "Podlasie"
  ]
  node [
    id 836
    label "Izera"
  ]
  node [
    id 837
    label "Ma&#322;opolska"
  ]
  node [
    id 838
    label "Warmia"
  ]
  node [
    id 839
    label "Mazury"
  ]
  node [
    id 840
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 841
    label "NATO"
  ]
  node [
    id 842
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 843
    label "Kaczawa"
  ]
  node [
    id 844
    label "Lubelszczyzna"
  ]
  node [
    id 845
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 846
    label "Kielecczyzna"
  ]
  node [
    id 847
    label "Lubuskie"
  ]
  node [
    id 848
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 849
    label "&#321;&#243;dzkie"
  ]
  node [
    id 850
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 851
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 852
    label "Kujawy"
  ]
  node [
    id 853
    label "Podkarpacie"
  ]
  node [
    id 854
    label "Wielkopolska"
  ]
  node [
    id 855
    label "Wis&#322;a"
  ]
  node [
    id 856
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 857
    label "Bory_Tucholskie"
  ]
  node [
    id 858
    label "z&#322;ocisty"
  ]
  node [
    id 859
    label "powleczenie"
  ]
  node [
    id 860
    label "zabarwienie"
  ]
  node [
    id 861
    label "platerowanie"
  ]
  node [
    id 862
    label "barwienie"
  ]
  node [
    id 863
    label "gilt"
  ]
  node [
    id 864
    label "plating"
  ]
  node [
    id 865
    label "zdobienie"
  ]
  node [
    id 866
    label "club"
  ]
  node [
    id 867
    label "urzeczywistnianie"
  ]
  node [
    id 868
    label "utrzymywanie"
  ]
  node [
    id 869
    label "byt"
  ]
  node [
    id 870
    label "produkowanie"
  ]
  node [
    id 871
    label "znikni&#281;cie"
  ]
  node [
    id 872
    label "being"
  ]
  node [
    id 873
    label "utrzyma&#263;"
  ]
  node [
    id 874
    label "egzystencja"
  ]
  node [
    id 875
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 876
    label "utrzymanie"
  ]
  node [
    id 877
    label "wyprodukowanie"
  ]
  node [
    id 878
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 879
    label "utrzymywa&#263;"
  ]
  node [
    id 880
    label "integer"
  ]
  node [
    id 881
    label "liczba"
  ]
  node [
    id 882
    label "zlewanie_si&#281;"
  ]
  node [
    id 883
    label "uk&#322;ad"
  ]
  node [
    id 884
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 885
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 886
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 887
    label "obroni&#263;"
  ]
  node [
    id 888
    label "potrzyma&#263;"
  ]
  node [
    id 889
    label "op&#322;aci&#263;"
  ]
  node [
    id 890
    label "manewr"
  ]
  node [
    id 891
    label "zdo&#322;a&#263;"
  ]
  node [
    id 892
    label "podtrzyma&#263;"
  ]
  node [
    id 893
    label "feed"
  ]
  node [
    id 894
    label "zrobi&#263;"
  ]
  node [
    id 895
    label "przetrzyma&#263;"
  ]
  node [
    id 896
    label "foster"
  ]
  node [
    id 897
    label "preserve"
  ]
  node [
    id 898
    label "zapewni&#263;"
  ]
  node [
    id 899
    label "zachowa&#263;"
  ]
  node [
    id 900
    label "unie&#347;&#263;"
  ]
  node [
    id 901
    label "obronienie"
  ]
  node [
    id 902
    label "zap&#322;acenie"
  ]
  node [
    id 903
    label "zachowanie"
  ]
  node [
    id 904
    label "potrzymanie"
  ]
  node [
    id 905
    label "przetrzymanie"
  ]
  node [
    id 906
    label "preservation"
  ]
  node [
    id 907
    label "zdo&#322;anie"
  ]
  node [
    id 908
    label "subsystencja"
  ]
  node [
    id 909
    label "uniesienie"
  ]
  node [
    id 910
    label "wy&#380;ywienie"
  ]
  node [
    id 911
    label "zapewnienie"
  ]
  node [
    id 912
    label "podtrzymanie"
  ]
  node [
    id 913
    label "wychowanie"
  ]
  node [
    id 914
    label "bronienie"
  ]
  node [
    id 915
    label "trzymanie"
  ]
  node [
    id 916
    label "podtrzymywanie"
  ]
  node [
    id 917
    label "wychowywanie"
  ]
  node [
    id 918
    label "panowanie"
  ]
  node [
    id 919
    label "zachowywanie"
  ]
  node [
    id 920
    label "twierdzenie"
  ]
  node [
    id 921
    label "chowanie"
  ]
  node [
    id 922
    label "retention"
  ]
  node [
    id 923
    label "op&#322;acanie"
  ]
  node [
    id 924
    label "s&#261;dzenie"
  ]
  node [
    id 925
    label "zapewnianie"
  ]
  node [
    id 926
    label "obejrzenie"
  ]
  node [
    id 927
    label "widzenie"
  ]
  node [
    id 928
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 929
    label "przeszkodzenie"
  ]
  node [
    id 930
    label "przeszkadzanie"
  ]
  node [
    id 931
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 932
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 933
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 934
    label "wiek_matuzalemowy"
  ]
  node [
    id 935
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 936
    label "argue"
  ]
  node [
    id 937
    label "podtrzymywa&#263;"
  ]
  node [
    id 938
    label "s&#261;dzi&#263;"
  ]
  node [
    id 939
    label "twierdzi&#263;"
  ]
  node [
    id 940
    label "zapewnia&#263;"
  ]
  node [
    id 941
    label "corroborate"
  ]
  node [
    id 942
    label "trzyma&#263;"
  ]
  node [
    id 943
    label "panowa&#263;"
  ]
  node [
    id 944
    label "defy"
  ]
  node [
    id 945
    label "cope"
  ]
  node [
    id 946
    label "broni&#263;"
  ]
  node [
    id 947
    label "sprawowa&#263;"
  ]
  node [
    id 948
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 949
    label "zachowywa&#263;"
  ]
  node [
    id 950
    label "ontologicznie"
  ]
  node [
    id 951
    label "potencja"
  ]
  node [
    id 952
    label "fabrication"
  ]
  node [
    id 953
    label "koszt_ca&#322;kowity"
  ]
  node [
    id 954
    label "pojawianie_si&#281;"
  ]
  node [
    id 955
    label "tworzenie"
  ]
  node [
    id 956
    label "gospodarka"
  ]
  node [
    id 957
    label "ubycie"
  ]
  node [
    id 958
    label "disappearance"
  ]
  node [
    id 959
    label "poznikanie"
  ]
  node [
    id 960
    label "evanescence"
  ]
  node [
    id 961
    label "die"
  ]
  node [
    id 962
    label "przepadni&#281;cie"
  ]
  node [
    id 963
    label "ukradzenie"
  ]
  node [
    id 964
    label "niewidoczny"
  ]
  node [
    id 965
    label "zgini&#281;cie"
  ]
  node [
    id 966
    label "devising"
  ]
  node [
    id 967
    label "pojawienie_si&#281;"
  ]
  node [
    id 968
    label "shuffle"
  ]
  node [
    id 969
    label "spe&#322;nianie"
  ]
  node [
    id 970
    label "fulfillment"
  ]
  node [
    id 971
    label "powodowanie"
  ]
  node [
    id 972
    label "porobienie"
  ]
  node [
    id 973
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 974
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 975
    label "creation"
  ]
  node [
    id 976
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 977
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 978
    label "tentegowanie"
  ]
  node [
    id 979
    label "wystarczy&#263;"
  ]
  node [
    id 980
    label "trwa&#263;"
  ]
  node [
    id 981
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 982
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 983
    label "przebywa&#263;"
  ]
  node [
    id 984
    label "by&#263;"
  ]
  node [
    id 985
    label "pozostawa&#263;"
  ]
  node [
    id 986
    label "kosztowa&#263;"
  ]
  node [
    id 987
    label "undertaking"
  ]
  node [
    id 988
    label "digest"
  ]
  node [
    id 989
    label "wystawa&#263;"
  ]
  node [
    id 990
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 991
    label "wystarcza&#263;"
  ]
  node [
    id 992
    label "base"
  ]
  node [
    id 993
    label "mieszka&#263;"
  ]
  node [
    id 994
    label "stand"
  ]
  node [
    id 995
    label "czeka&#263;"
  ]
  node [
    id 996
    label "dobywa&#263;_si&#281;"
  ]
  node [
    id 997
    label "istnie&#263;"
  ]
  node [
    id 998
    label "zostawa&#263;"
  ]
  node [
    id 999
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1000
    label "adhere"
  ]
  node [
    id 1001
    label "bind"
  ]
  node [
    id 1002
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 1003
    label "zjednywa&#263;"
  ]
  node [
    id 1004
    label "tkwi&#263;"
  ]
  node [
    id 1005
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1006
    label "pause"
  ]
  node [
    id 1007
    label "przestawa&#263;"
  ]
  node [
    id 1008
    label "hesitate"
  ]
  node [
    id 1009
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1010
    label "mie&#263;_miejsce"
  ]
  node [
    id 1011
    label "equal"
  ]
  node [
    id 1012
    label "chodzi&#263;"
  ]
  node [
    id 1013
    label "si&#281;ga&#263;"
  ]
  node [
    id 1014
    label "stan"
  ]
  node [
    id 1015
    label "obecno&#347;&#263;"
  ]
  node [
    id 1016
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1017
    label "uczestniczy&#263;"
  ]
  node [
    id 1018
    label "try"
  ]
  node [
    id 1019
    label "savor"
  ]
  node [
    id 1020
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 1021
    label "cena"
  ]
  node [
    id 1022
    label "doznawa&#263;"
  ]
  node [
    id 1023
    label "essay"
  ]
  node [
    id 1024
    label "suffice"
  ]
  node [
    id 1025
    label "spowodowa&#263;"
  ]
  node [
    id 1026
    label "stan&#261;&#263;"
  ]
  node [
    id 1027
    label "zaspokoi&#263;"
  ]
  node [
    id 1028
    label "dosta&#263;"
  ]
  node [
    id 1029
    label "zaspokaja&#263;"
  ]
  node [
    id 1030
    label "dostawa&#263;"
  ]
  node [
    id 1031
    label "stawa&#263;"
  ]
  node [
    id 1032
    label "pauzowa&#263;"
  ]
  node [
    id 1033
    label "oczekiwa&#263;"
  ]
  node [
    id 1034
    label "sp&#281;dza&#263;"
  ]
  node [
    id 1035
    label "look"
  ]
  node [
    id 1036
    label "hold"
  ]
  node [
    id 1037
    label "anticipate"
  ]
  node [
    id 1038
    label "blend"
  ]
  node [
    id 1039
    label "stop"
  ]
  node [
    id 1040
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 1041
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 1042
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 1043
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 1044
    label "support"
  ]
  node [
    id 1045
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 1046
    label "prosecute"
  ]
  node [
    id 1047
    label "zajmowa&#263;"
  ]
  node [
    id 1048
    label "fall"
  ]
  node [
    id 1049
    label "wnikni&#281;cie"
  ]
  node [
    id 1050
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 1051
    label "poznanie"
  ]
  node [
    id 1052
    label "przenikni&#281;cie"
  ]
  node [
    id 1053
    label "wpuszczenie"
  ]
  node [
    id 1054
    label "trespass"
  ]
  node [
    id 1055
    label "dost&#281;p"
  ]
  node [
    id 1056
    label "doj&#347;cie"
  ]
  node [
    id 1057
    label "przekroczenie"
  ]
  node [
    id 1058
    label "otw&#243;r"
  ]
  node [
    id 1059
    label "vent"
  ]
  node [
    id 1060
    label "stimulation"
  ]
  node [
    id 1061
    label "dostanie_si&#281;"
  ]
  node [
    id 1062
    label "approach"
  ]
  node [
    id 1063
    label "release"
  ]
  node [
    id 1064
    label "wnij&#347;cie"
  ]
  node [
    id 1065
    label "bramka"
  ]
  node [
    id 1066
    label "wzniesienie_si&#281;"
  ]
  node [
    id 1067
    label "podw&#243;rze"
  ]
  node [
    id 1068
    label "dom"
  ]
  node [
    id 1069
    label "wch&#243;d"
  ]
  node [
    id 1070
    label "nast&#261;pienie"
  ]
  node [
    id 1071
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1072
    label "zacz&#281;cie"
  ]
  node [
    id 1073
    label "cz&#322;onek"
  ]
  node [
    id 1074
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 1075
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1076
    label "discourtesy"
  ]
  node [
    id 1077
    label "odj&#281;cie"
  ]
  node [
    id 1078
    label "post&#261;pienie"
  ]
  node [
    id 1079
    label "opening"
  ]
  node [
    id 1080
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 1081
    label "wyra&#380;enie"
  ]
  node [
    id 1082
    label "porobienie_si&#281;"
  ]
  node [
    id 1083
    label "naci&#347;ni&#281;cie"
  ]
  node [
    id 1084
    label "chodzenie"
  ]
  node [
    id 1085
    label "event"
  ]
  node [
    id 1086
    label "advent"
  ]
  node [
    id 1087
    label "uzyskanie"
  ]
  node [
    id 1088
    label "dochrapanie_si&#281;"
  ]
  node [
    id 1089
    label "skill"
  ]
  node [
    id 1090
    label "accomplishment"
  ]
  node [
    id 1091
    label "sukces"
  ]
  node [
    id 1092
    label "zaawansowanie"
  ]
  node [
    id 1093
    label "dotarcie"
  ]
  node [
    id 1094
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 1095
    label "doznanie"
  ]
  node [
    id 1096
    label "gathering"
  ]
  node [
    id 1097
    label "zawarcie"
  ]
  node [
    id 1098
    label "znajomy"
  ]
  node [
    id 1099
    label "powitanie"
  ]
  node [
    id 1100
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 1101
    label "spowodowanie"
  ]
  node [
    id 1102
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 1103
    label "znalezienie"
  ]
  node [
    id 1104
    label "match"
  ]
  node [
    id 1105
    label "employment"
  ]
  node [
    id 1106
    label "po&#380;egnanie"
  ]
  node [
    id 1107
    label "gather"
  ]
  node [
    id 1108
    label "spotykanie"
  ]
  node [
    id 1109
    label "spotkanie_si&#281;"
  ]
  node [
    id 1110
    label "dochodzenie"
  ]
  node [
    id 1111
    label "znajomo&#347;ci"
  ]
  node [
    id 1112
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 1113
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 1114
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1115
    label "powi&#261;zanie"
  ]
  node [
    id 1116
    label "entrance"
  ]
  node [
    id 1117
    label "affiliation"
  ]
  node [
    id 1118
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1119
    label "dor&#281;czenie"
  ]
  node [
    id 1120
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 1121
    label "bodziec"
  ]
  node [
    id 1122
    label "informacja"
  ]
  node [
    id 1123
    label "przesy&#322;ka"
  ]
  node [
    id 1124
    label "gotowy"
  ]
  node [
    id 1125
    label "avenue"
  ]
  node [
    id 1126
    label "postrzeganie"
  ]
  node [
    id 1127
    label "dodatek"
  ]
  node [
    id 1128
    label "dojrza&#322;y"
  ]
  node [
    id 1129
    label "dojechanie"
  ]
  node [
    id 1130
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 1131
    label "ingress"
  ]
  node [
    id 1132
    label "strzelenie"
  ]
  node [
    id 1133
    label "orzekni&#281;cie"
  ]
  node [
    id 1134
    label "orgazm"
  ]
  node [
    id 1135
    label "dolecenie"
  ]
  node [
    id 1136
    label "rozpowszechnienie"
  ]
  node [
    id 1137
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 1138
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 1139
    label "dop&#322;ata"
  ]
  node [
    id 1140
    label "informatyka"
  ]
  node [
    id 1141
    label "operacja"
  ]
  node [
    id 1142
    label "has&#322;o"
  ]
  node [
    id 1143
    label "pierworodztwo"
  ]
  node [
    id 1144
    label "upgrade"
  ]
  node [
    id 1145
    label "nast&#281;pstwo"
  ]
  node [
    id 1146
    label "skrytykowanie"
  ]
  node [
    id 1147
    label "oddzia&#322;anie"
  ]
  node [
    id 1148
    label "upolowanie"
  ]
  node [
    id 1149
    label "wdarcie_si&#281;"
  ]
  node [
    id 1150
    label "wyskoczenie_z_g&#281;b&#261;"
  ]
  node [
    id 1151
    label "sport"
  ]
  node [
    id 1152
    label "progress"
  ]
  node [
    id 1153
    label "spr&#243;bowanie"
  ]
  node [
    id 1154
    label "powiedzenie"
  ]
  node [
    id 1155
    label "rozegranie"
  ]
  node [
    id 1156
    label "mini&#281;cie"
  ]
  node [
    id 1157
    label "ograniczenie"
  ]
  node [
    id 1158
    label "przepuszczenie"
  ]
  node [
    id 1159
    label "transgression"
  ]
  node [
    id 1160
    label "transgresja"
  ]
  node [
    id 1161
    label "emergence"
  ]
  node [
    id 1162
    label "offense"
  ]
  node [
    id 1163
    label "kom&#243;rka"
  ]
  node [
    id 1164
    label "furnishing"
  ]
  node [
    id 1165
    label "zabezpieczenie"
  ]
  node [
    id 1166
    label "wyrz&#261;dzenie"
  ]
  node [
    id 1167
    label "zagospodarowanie"
  ]
  node [
    id 1168
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 1169
    label "ig&#322;a"
  ]
  node [
    id 1170
    label "narz&#281;dzie"
  ]
  node [
    id 1171
    label "wirnik"
  ]
  node [
    id 1172
    label "aparatura"
  ]
  node [
    id 1173
    label "system_energetyczny"
  ]
  node [
    id 1174
    label "impulsator"
  ]
  node [
    id 1175
    label "mechanizm"
  ]
  node [
    id 1176
    label "sprz&#281;t"
  ]
  node [
    id 1177
    label "blokowanie"
  ]
  node [
    id 1178
    label "set"
  ]
  node [
    id 1179
    label "zablokowanie"
  ]
  node [
    id 1180
    label "przygotowanie"
  ]
  node [
    id 1181
    label "komora"
  ]
  node [
    id 1182
    label "j&#281;zyk"
  ]
  node [
    id 1183
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 1184
    label "wsi&#261;kni&#281;cie"
  ]
  node [
    id 1185
    label "infiltration"
  ]
  node [
    id 1186
    label "penetration"
  ]
  node [
    id 1187
    label "przestrze&#324;"
  ]
  node [
    id 1188
    label "wyd&#322;uba&#263;"
  ]
  node [
    id 1189
    label "wybicie"
  ]
  node [
    id 1190
    label "wyd&#322;ubanie"
  ]
  node [
    id 1191
    label "przerwa"
  ]
  node [
    id 1192
    label "powybijanie"
  ]
  node [
    id 1193
    label "wybijanie"
  ]
  node [
    id 1194
    label "wiercenie"
  ]
  node [
    id 1195
    label "acquaintance"
  ]
  node [
    id 1196
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 1197
    label "nauczenie_si&#281;"
  ]
  node [
    id 1198
    label "poczucie"
  ]
  node [
    id 1199
    label "knowing"
  ]
  node [
    id 1200
    label "zapoznanie_si&#281;"
  ]
  node [
    id 1201
    label "wy&#347;wiadczenie"
  ]
  node [
    id 1202
    label "inclusion"
  ]
  node [
    id 1203
    label "zrozumienie"
  ]
  node [
    id 1204
    label "designation"
  ]
  node [
    id 1205
    label "umo&#380;liwienie"
  ]
  node [
    id 1206
    label "sensing"
  ]
  node [
    id 1207
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 1208
    label "zapoznanie"
  ]
  node [
    id 1209
    label "forma"
  ]
  node [
    id 1210
    label "zgromadzenie_si&#281;"
  ]
  node [
    id 1211
    label "insekt"
  ]
  node [
    id 1212
    label "puszczenie"
  ]
  node [
    id 1213
    label "entree"
  ]
  node [
    id 1214
    label "wprowadzenie"
  ]
  node [
    id 1215
    label "dmuchni&#281;cie"
  ]
  node [
    id 1216
    label "niesienie"
  ]
  node [
    id 1217
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 1218
    label "nakazanie"
  ]
  node [
    id 1219
    label "ruszenie"
  ]
  node [
    id 1220
    label "pokonanie"
  ]
  node [
    id 1221
    label "take"
  ]
  node [
    id 1222
    label "wywiezienie"
  ]
  node [
    id 1223
    label "zaopatrzenie_si&#281;"
  ]
  node [
    id 1224
    label "wymienienie_si&#281;"
  ]
  node [
    id 1225
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 1226
    label "uciekni&#281;cie"
  ]
  node [
    id 1227
    label "pobranie"
  ]
  node [
    id 1228
    label "poczytanie"
  ]
  node [
    id 1229
    label "si&#281;gni&#281;cie"
  ]
  node [
    id 1230
    label "pozabieranie"
  ]
  node [
    id 1231
    label "u&#380;ycie"
  ]
  node [
    id 1232
    label "powodzenie"
  ]
  node [
    id 1233
    label "pickings"
  ]
  node [
    id 1234
    label "przyj&#281;cie"
  ]
  node [
    id 1235
    label "zniesienie"
  ]
  node [
    id 1236
    label "kupienie"
  ]
  node [
    id 1237
    label "bite"
  ]
  node [
    id 1238
    label "dostanie"
  ]
  node [
    id 1239
    label "wyruchanie"
  ]
  node [
    id 1240
    label "odziedziczenie"
  ]
  node [
    id 1241
    label "otrzymanie"
  ]
  node [
    id 1242
    label "branie"
  ]
  node [
    id 1243
    label "wygranie"
  ]
  node [
    id 1244
    label "wzi&#261;&#263;"
  ]
  node [
    id 1245
    label "obj&#281;cie"
  ]
  node [
    id 1246
    label "w&#322;o&#380;enie"
  ]
  node [
    id 1247
    label "udanie_si&#281;"
  ]
  node [
    id 1248
    label "wych&#281;do&#380;enie"
  ]
  node [
    id 1249
    label "podmiot"
  ]
  node [
    id 1250
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 1251
    label "organ"
  ]
  node [
    id 1252
    label "ptaszek"
  ]
  node [
    id 1253
    label "element_anatomiczny"
  ]
  node [
    id 1254
    label "przyrodzenie"
  ]
  node [
    id 1255
    label "fiut"
  ]
  node [
    id 1256
    label "shaft"
  ]
  node [
    id 1257
    label "wchodzenie"
  ]
  node [
    id 1258
    label "przedstawiciel"
  ]
  node [
    id 1259
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1260
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 1261
    label "nas&#261;czenie"
  ]
  node [
    id 1262
    label "strain"
  ]
  node [
    id 1263
    label "przedostanie_si&#281;"
  ]
  node [
    id 1264
    label "control"
  ]
  node [
    id 1265
    label "nasycenie_si&#281;"
  ]
  node [
    id 1266
    label "permeation"
  ]
  node [
    id 1267
    label "nasilenie_si&#281;"
  ]
  node [
    id 1268
    label "przemokni&#281;cie"
  ]
  node [
    id 1269
    label "przepojenie"
  ]
  node [
    id 1270
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 1271
    label "obstawianie"
  ]
  node [
    id 1272
    label "trafienie"
  ]
  node [
    id 1273
    label "obstawienie"
  ]
  node [
    id 1274
    label "przeszkoda"
  ]
  node [
    id 1275
    label "zawiasy"
  ]
  node [
    id 1276
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 1277
    label "s&#322;upek"
  ]
  node [
    id 1278
    label "boisko"
  ]
  node [
    id 1279
    label "siatka"
  ]
  node [
    id 1280
    label "obstawia&#263;"
  ]
  node [
    id 1281
    label "ogrodzenie"
  ]
  node [
    id 1282
    label "zamek"
  ]
  node [
    id 1283
    label "goal"
  ]
  node [
    id 1284
    label "poprzeczka"
  ]
  node [
    id 1285
    label "p&#322;ot"
  ]
  node [
    id 1286
    label "obstawi&#263;"
  ]
  node [
    id 1287
    label "brama"
  ]
  node [
    id 1288
    label "przechowalnia"
  ]
  node [
    id 1289
    label "podjazd"
  ]
  node [
    id 1290
    label "ogr&#243;d"
  ]
  node [
    id 1291
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1292
    label "rodzina"
  ]
  node [
    id 1293
    label "substancja_mieszkaniowa"
  ]
  node [
    id 1294
    label "siedziba"
  ]
  node [
    id 1295
    label "dom_rodzinny"
  ]
  node [
    id 1296
    label "budynek"
  ]
  node [
    id 1297
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1298
    label "stead"
  ]
  node [
    id 1299
    label "garderoba"
  ]
  node [
    id 1300
    label "wiecha"
  ]
  node [
    id 1301
    label "fratria"
  ]
  node [
    id 1302
    label "Karta_Nauczyciela"
  ]
  node [
    id 1303
    label "przej&#347;cie"
  ]
  node [
    id 1304
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 1305
    label "przej&#347;&#263;"
  ]
  node [
    id 1306
    label "charter"
  ]
  node [
    id 1307
    label "marc&#243;wka"
  ]
  node [
    id 1308
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1309
    label "podnieci&#263;"
  ]
  node [
    id 1310
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 1311
    label "numer"
  ]
  node [
    id 1312
    label "po&#380;ycie"
  ]
  node [
    id 1313
    label "podniecenie"
  ]
  node [
    id 1314
    label "nago&#347;&#263;"
  ]
  node [
    id 1315
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 1316
    label "fascyku&#322;"
  ]
  node [
    id 1317
    label "seks"
  ]
  node [
    id 1318
    label "podniecanie"
  ]
  node [
    id 1319
    label "imisja"
  ]
  node [
    id 1320
    label "zwyczaj"
  ]
  node [
    id 1321
    label "rozmna&#380;anie"
  ]
  node [
    id 1322
    label "ruch_frykcyjny"
  ]
  node [
    id 1323
    label "ontologia"
  ]
  node [
    id 1324
    label "na_pieska"
  ]
  node [
    id 1325
    label "pozycja_misjonarska"
  ]
  node [
    id 1326
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 1327
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1328
    label "gra_wst&#281;pna"
  ]
  node [
    id 1329
    label "erotyka"
  ]
  node [
    id 1330
    label "urzeczywistnienie"
  ]
  node [
    id 1331
    label "baraszki"
  ]
  node [
    id 1332
    label "certificate"
  ]
  node [
    id 1333
    label "po&#380;&#261;danie"
  ]
  node [
    id 1334
    label "wzw&#243;d"
  ]
  node [
    id 1335
    label "funkcja"
  ]
  node [
    id 1336
    label "dokument"
  ]
  node [
    id 1337
    label "arystotelizm"
  ]
  node [
    id 1338
    label "podnieca&#263;"
  ]
  node [
    id 1339
    label "zabory"
  ]
  node [
    id 1340
    label "ci&#281;&#380;arna"
  ]
  node [
    id 1341
    label "rozwi&#261;zanie"
  ]
  node [
    id 1342
    label "podlec"
  ]
  node [
    id 1343
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 1344
    label "min&#261;&#263;"
  ]
  node [
    id 1345
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 1346
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 1347
    label "zaliczy&#263;"
  ]
  node [
    id 1348
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1349
    label "zmieni&#263;"
  ]
  node [
    id 1350
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 1351
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1352
    label "dozna&#263;"
  ]
  node [
    id 1353
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1354
    label "zacz&#261;&#263;"
  ]
  node [
    id 1355
    label "happen"
  ]
  node [
    id 1356
    label "pass"
  ]
  node [
    id 1357
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1358
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 1359
    label "beat"
  ]
  node [
    id 1360
    label "absorb"
  ]
  node [
    id 1361
    label "przerobi&#263;"
  ]
  node [
    id 1362
    label "pique"
  ]
  node [
    id 1363
    label "przesta&#263;"
  ]
  node [
    id 1364
    label "wymienienie"
  ]
  node [
    id 1365
    label "zaliczenie"
  ]
  node [
    id 1366
    label "traversal"
  ]
  node [
    id 1367
    label "przewy&#380;szenie"
  ]
  node [
    id 1368
    label "experience"
  ]
  node [
    id 1369
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 1370
    label "przerobienie"
  ]
  node [
    id 1371
    label "wydeptywanie"
  ]
  node [
    id 1372
    label "crack"
  ]
  node [
    id 1373
    label "wydeptanie"
  ]
  node [
    id 1374
    label "wstawka"
  ]
  node [
    id 1375
    label "prze&#380;ycie"
  ]
  node [
    id 1376
    label "uznanie"
  ]
  node [
    id 1377
    label "wytyczenie"
  ]
  node [
    id 1378
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1379
    label "nale&#380;enie"
  ]
  node [
    id 1380
    label "odmienienie"
  ]
  node [
    id 1381
    label "przestanie"
  ]
  node [
    id 1382
    label "odnaj&#281;cie"
  ]
  node [
    id 1383
    label "naj&#281;cie"
  ]
  node [
    id 1384
    label "w&#322;asny"
  ]
  node [
    id 1385
    label "oryginalny"
  ]
  node [
    id 1386
    label "autorsko"
  ]
  node [
    id 1387
    label "niespotykany"
  ]
  node [
    id 1388
    label "o&#380;ywczy"
  ]
  node [
    id 1389
    label "ekscentryczny"
  ]
  node [
    id 1390
    label "nowy"
  ]
  node [
    id 1391
    label "oryginalnie"
  ]
  node [
    id 1392
    label "inny"
  ]
  node [
    id 1393
    label "pierwotny"
  ]
  node [
    id 1394
    label "prawdziwy"
  ]
  node [
    id 1395
    label "samodzielny"
  ]
  node [
    id 1396
    label "zwi&#261;zany"
  ]
  node [
    id 1397
    label "czyj&#347;"
  ]
  node [
    id 1398
    label "swoisty"
  ]
  node [
    id 1399
    label "osobny"
  ]
  node [
    id 1400
    label "prawnie"
  ]
  node [
    id 1401
    label "indywidualnie"
  ]
  node [
    id 1402
    label "p&#243;&#322;rocze"
  ]
  node [
    id 1403
    label "martwy_sezon"
  ]
  node [
    id 1404
    label "kalendarz"
  ]
  node [
    id 1405
    label "cykl_astronomiczny"
  ]
  node [
    id 1406
    label "lata"
  ]
  node [
    id 1407
    label "stulecie"
  ]
  node [
    id 1408
    label "kurs"
  ]
  node [
    id 1409
    label "jubileusz"
  ]
  node [
    id 1410
    label "kwarta&#322;"
  ]
  node [
    id 1411
    label "miesi&#261;c"
  ]
  node [
    id 1412
    label "summer"
  ]
  node [
    id 1413
    label "odm&#322;adzanie"
  ]
  node [
    id 1414
    label "liga"
  ]
  node [
    id 1415
    label "jednostka_systematyczna"
  ]
  node [
    id 1416
    label "asymilowanie"
  ]
  node [
    id 1417
    label "gromada"
  ]
  node [
    id 1418
    label "asymilowa&#263;"
  ]
  node [
    id 1419
    label "Entuzjastki"
  ]
  node [
    id 1420
    label "kompozycja"
  ]
  node [
    id 1421
    label "Terranie"
  ]
  node [
    id 1422
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1423
    label "category"
  ]
  node [
    id 1424
    label "pakiet_klimatyczny"
  ]
  node [
    id 1425
    label "oddzia&#322;"
  ]
  node [
    id 1426
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1427
    label "cz&#261;steczka"
  ]
  node [
    id 1428
    label "stage_set"
  ]
  node [
    id 1429
    label "type"
  ]
  node [
    id 1430
    label "specgrupa"
  ]
  node [
    id 1431
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1432
    label "&#346;wietliki"
  ]
  node [
    id 1433
    label "odm&#322;odzenie"
  ]
  node [
    id 1434
    label "Eurogrupa"
  ]
  node [
    id 1435
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1436
    label "formacja_geologiczna"
  ]
  node [
    id 1437
    label "harcerze_starsi"
  ]
  node [
    id 1438
    label "rok_szkolny"
  ]
  node [
    id 1439
    label "semester"
  ]
  node [
    id 1440
    label "anniwersarz"
  ]
  node [
    id 1441
    label "rocznica"
  ]
  node [
    id 1442
    label "obszar"
  ]
  node [
    id 1443
    label "tydzie&#324;"
  ]
  node [
    id 1444
    label "miech"
  ]
  node [
    id 1445
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 1446
    label "kalendy"
  ]
  node [
    id 1447
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 1448
    label "long_time"
  ]
  node [
    id 1449
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 1450
    label "almanac"
  ]
  node [
    id 1451
    label "rozk&#322;ad"
  ]
  node [
    id 1452
    label "wydawnictwo"
  ]
  node [
    id 1453
    label "Juliusz_Cezar"
  ]
  node [
    id 1454
    label "zwy&#380;kowanie"
  ]
  node [
    id 1455
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 1456
    label "zaj&#281;cia"
  ]
  node [
    id 1457
    label "trasa"
  ]
  node [
    id 1458
    label "przejazd"
  ]
  node [
    id 1459
    label "klasa"
  ]
  node [
    id 1460
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 1461
    label "manner"
  ]
  node [
    id 1462
    label "course"
  ]
  node [
    id 1463
    label "passage"
  ]
  node [
    id 1464
    label "zni&#380;kowanie"
  ]
  node [
    id 1465
    label "stawka"
  ]
  node [
    id 1466
    label "way"
  ]
  node [
    id 1467
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 1468
    label "deprecjacja"
  ]
  node [
    id 1469
    label "cedu&#322;a"
  ]
  node [
    id 1470
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 1471
    label "drive"
  ]
  node [
    id 1472
    label "Lira"
  ]
  node [
    id 1473
    label "w&#380;dy"
  ]
  node [
    id 1474
    label "gleba"
  ]
  node [
    id 1475
    label "kondycja"
  ]
  node [
    id 1476
    label "kres"
  ]
  node [
    id 1477
    label "ruch"
  ]
  node [
    id 1478
    label "pogorszenie"
  ]
  node [
    id 1479
    label "inclination"
  ]
  node [
    id 1480
    label "zmiana"
  ]
  node [
    id 1481
    label "aggravation"
  ]
  node [
    id 1482
    label "worsening"
  ]
  node [
    id 1483
    label "zmienienie"
  ]
  node [
    id 1484
    label "gorszy"
  ]
  node [
    id 1485
    label "ostatnie_podrygi"
  ]
  node [
    id 1486
    label "punkt"
  ]
  node [
    id 1487
    label "dzia&#322;anie"
  ]
  node [
    id 1488
    label "koniec"
  ]
  node [
    id 1489
    label "mechanika"
  ]
  node [
    id 1490
    label "move"
  ]
  node [
    id 1491
    label "poruszenie"
  ]
  node [
    id 1492
    label "movement"
  ]
  node [
    id 1493
    label "myk"
  ]
  node [
    id 1494
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1495
    label "travel"
  ]
  node [
    id 1496
    label "kanciasty"
  ]
  node [
    id 1497
    label "commercial_enterprise"
  ]
  node [
    id 1498
    label "model"
  ]
  node [
    id 1499
    label "strumie&#324;"
  ]
  node [
    id 1500
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1501
    label "kr&#243;tki"
  ]
  node [
    id 1502
    label "taktyka"
  ]
  node [
    id 1503
    label "apraksja"
  ]
  node [
    id 1504
    label "natural_process"
  ]
  node [
    id 1505
    label "d&#322;ugi"
  ]
  node [
    id 1506
    label "dyssypacja_energii"
  ]
  node [
    id 1507
    label "tumult"
  ]
  node [
    id 1508
    label "stopek"
  ]
  node [
    id 1509
    label "lokomocja"
  ]
  node [
    id 1510
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1511
    label "komunikacja"
  ]
  node [
    id 1512
    label "drift"
  ]
  node [
    id 1513
    label "dyspozycja"
  ]
  node [
    id 1514
    label "situation"
  ]
  node [
    id 1515
    label "rank"
  ]
  node [
    id 1516
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1517
    label "litosfera"
  ]
  node [
    id 1518
    label "dotleni&#263;"
  ]
  node [
    id 1519
    label "pr&#243;chnica"
  ]
  node [
    id 1520
    label "glej"
  ]
  node [
    id 1521
    label "martwica"
  ]
  node [
    id 1522
    label "glinowa&#263;"
  ]
  node [
    id 1523
    label "podglebie"
  ]
  node [
    id 1524
    label "ryzosfera"
  ]
  node [
    id 1525
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1526
    label "geosystem"
  ]
  node [
    id 1527
    label "glinowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 596
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 762
  ]
  edge [
    source 13
    target 763
  ]
  edge [
    source 13
    target 764
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 867
  ]
  edge [
    source 14
    target 868
  ]
  edge [
    source 14
    target 642
  ]
  edge [
    source 14
    target 869
  ]
  edge [
    source 14
    target 870
  ]
  edge [
    source 14
    target 871
  ]
  edge [
    source 14
    target 872
  ]
  edge [
    source 14
    target 354
  ]
  edge [
    source 14
    target 873
  ]
  edge [
    source 14
    target 874
  ]
  edge [
    source 14
    target 875
  ]
  edge [
    source 14
    target 876
  ]
  edge [
    source 14
    target 877
  ]
  edge [
    source 14
    target 878
  ]
  edge [
    source 14
    target 357
  ]
  edge [
    source 14
    target 879
  ]
  edge [
    source 14
    target 880
  ]
  edge [
    source 14
    target 881
  ]
  edge [
    source 14
    target 882
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 883
  ]
  edge [
    source 14
    target 884
  ]
  edge [
    source 14
    target 885
  ]
  edge [
    source 14
    target 782
  ]
  edge [
    source 14
    target 886
  ]
  edge [
    source 14
    target 887
  ]
  edge [
    source 14
    target 888
  ]
  edge [
    source 14
    target 889
  ]
  edge [
    source 14
    target 890
  ]
  edge [
    source 14
    target 891
  ]
  edge [
    source 14
    target 892
  ]
  edge [
    source 14
    target 893
  ]
  edge [
    source 14
    target 894
  ]
  edge [
    source 14
    target 895
  ]
  edge [
    source 14
    target 896
  ]
  edge [
    source 14
    target 897
  ]
  edge [
    source 14
    target 898
  ]
  edge [
    source 14
    target 899
  ]
  edge [
    source 14
    target 900
  ]
  edge [
    source 14
    target 901
  ]
  edge [
    source 14
    target 902
  ]
  edge [
    source 14
    target 903
  ]
  edge [
    source 14
    target 904
  ]
  edge [
    source 14
    target 905
  ]
  edge [
    source 14
    target 906
  ]
  edge [
    source 14
    target 415
  ]
  edge [
    source 14
    target 907
  ]
  edge [
    source 14
    target 908
  ]
  edge [
    source 14
    target 909
  ]
  edge [
    source 14
    target 910
  ]
  edge [
    source 14
    target 911
  ]
  edge [
    source 14
    target 912
  ]
  edge [
    source 14
    target 913
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 914
  ]
  edge [
    source 14
    target 915
  ]
  edge [
    source 14
    target 916
  ]
  edge [
    source 14
    target 917
  ]
  edge [
    source 14
    target 918
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 920
  ]
  edge [
    source 14
    target 921
  ]
  edge [
    source 14
    target 922
  ]
  edge [
    source 14
    target 923
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 14
    target 925
  ]
  edge [
    source 14
    target 926
  ]
  edge [
    source 14
    target 927
  ]
  edge [
    source 14
    target 928
  ]
  edge [
    source 14
    target 929
  ]
  edge [
    source 14
    target 930
  ]
  edge [
    source 14
    target 931
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 932
  ]
  edge [
    source 14
    target 933
  ]
  edge [
    source 14
    target 934
  ]
  edge [
    source 14
    target 935
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 937
  ]
  edge [
    source 14
    target 938
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 940
  ]
  edge [
    source 14
    target 941
  ]
  edge [
    source 14
    target 942
  ]
  edge [
    source 14
    target 943
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 945
  ]
  edge [
    source 14
    target 946
  ]
  edge [
    source 14
    target 947
  ]
  edge [
    source 14
    target 948
  ]
  edge [
    source 14
    target 949
  ]
  edge [
    source 14
    target 950
  ]
  edge [
    source 14
    target 951
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 952
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 14
    target 954
  ]
  edge [
    source 14
    target 955
  ]
  edge [
    source 14
    target 956
  ]
  edge [
    source 14
    target 957
  ]
  edge [
    source 14
    target 958
  ]
  edge [
    source 14
    target 239
  ]
  edge [
    source 14
    target 959
  ]
  edge [
    source 14
    target 960
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 961
  ]
  edge [
    source 14
    target 962
  ]
  edge [
    source 14
    target 963
  ]
  edge [
    source 14
    target 964
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 965
  ]
  edge [
    source 14
    target 605
  ]
  edge [
    source 14
    target 966
  ]
  edge [
    source 14
    target 967
  ]
  edge [
    source 14
    target 968
  ]
  edge [
    source 14
    target 969
  ]
  edge [
    source 14
    target 970
  ]
  edge [
    source 14
    target 971
  ]
  edge [
    source 14
    target 972
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 973
  ]
  edge [
    source 14
    target 974
  ]
  edge [
    source 14
    target 975
  ]
  edge [
    source 14
    target 976
  ]
  edge [
    source 14
    target 290
  ]
  edge [
    source 14
    target 977
  ]
  edge [
    source 14
    target 330
  ]
  edge [
    source 14
    target 978
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 979
  ]
  edge [
    source 16
    target 980
  ]
  edge [
    source 16
    target 981
  ]
  edge [
    source 16
    target 982
  ]
  edge [
    source 16
    target 983
  ]
  edge [
    source 16
    target 984
  ]
  edge [
    source 16
    target 985
  ]
  edge [
    source 16
    target 986
  ]
  edge [
    source 16
    target 987
  ]
  edge [
    source 16
    target 988
  ]
  edge [
    source 16
    target 989
  ]
  edge [
    source 16
    target 990
  ]
  edge [
    source 16
    target 991
  ]
  edge [
    source 16
    target 992
  ]
  edge [
    source 16
    target 993
  ]
  edge [
    source 16
    target 994
  ]
  edge [
    source 16
    target 947
  ]
  edge [
    source 16
    target 995
  ]
  edge [
    source 16
    target 996
  ]
  edge [
    source 16
    target 997
  ]
  edge [
    source 16
    target 998
  ]
  edge [
    source 16
    target 999
  ]
  edge [
    source 16
    target 1000
  ]
  edge [
    source 16
    target 209
  ]
  edge [
    source 16
    target 1001
  ]
  edge [
    source 16
    target 943
  ]
  edge [
    source 16
    target 1002
  ]
  edge [
    source 16
    target 1003
  ]
  edge [
    source 16
    target 1004
  ]
  edge [
    source 16
    target 1005
  ]
  edge [
    source 16
    target 1006
  ]
  edge [
    source 16
    target 1007
  ]
  edge [
    source 16
    target 1008
  ]
  edge [
    source 16
    target 1009
  ]
  edge [
    source 16
    target 1010
  ]
  edge [
    source 16
    target 1011
  ]
  edge [
    source 16
    target 1012
  ]
  edge [
    source 16
    target 1013
  ]
  edge [
    source 16
    target 1014
  ]
  edge [
    source 16
    target 1015
  ]
  edge [
    source 16
    target 1016
  ]
  edge [
    source 16
    target 1017
  ]
  edge [
    source 16
    target 1018
  ]
  edge [
    source 16
    target 1019
  ]
  edge [
    source 16
    target 1020
  ]
  edge [
    source 16
    target 1021
  ]
  edge [
    source 16
    target 1022
  ]
  edge [
    source 16
    target 1023
  ]
  edge [
    source 16
    target 1024
  ]
  edge [
    source 16
    target 1025
  ]
  edge [
    source 16
    target 1026
  ]
  edge [
    source 16
    target 1027
  ]
  edge [
    source 16
    target 1028
  ]
  edge [
    source 16
    target 1029
  ]
  edge [
    source 16
    target 1030
  ]
  edge [
    source 16
    target 1031
  ]
  edge [
    source 16
    target 1032
  ]
  edge [
    source 16
    target 1033
  ]
  edge [
    source 16
    target 294
  ]
  edge [
    source 16
    target 1034
  ]
  edge [
    source 16
    target 1035
  ]
  edge [
    source 16
    target 1036
  ]
  edge [
    source 16
    target 1037
  ]
  edge [
    source 16
    target 1038
  ]
  edge [
    source 16
    target 1039
  ]
  edge [
    source 16
    target 1040
  ]
  edge [
    source 16
    target 1041
  ]
  edge [
    source 16
    target 1042
  ]
  edge [
    source 16
    target 1043
  ]
  edge [
    source 16
    target 1044
  ]
  edge [
    source 16
    target 1045
  ]
  edge [
    source 16
    target 1046
  ]
  edge [
    source 16
    target 1047
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 1048
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 587
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 503
  ]
  edge [
    source 17
    target 1052
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 630
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 1055
  ]
  edge [
    source 17
    target 1056
  ]
  edge [
    source 17
    target 1057
  ]
  edge [
    source 17
    target 1058
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 1059
  ]
  edge [
    source 17
    target 1060
  ]
  edge [
    source 17
    target 1061
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 1062
  ]
  edge [
    source 17
    target 1063
  ]
  edge [
    source 17
    target 1064
  ]
  edge [
    source 17
    target 1065
  ]
  edge [
    source 17
    target 1066
  ]
  edge [
    source 17
    target 1067
  ]
  edge [
    source 17
    target 78
  ]
  edge [
    source 17
    target 1068
  ]
  edge [
    source 17
    target 1069
  ]
  edge [
    source 17
    target 1070
  ]
  edge [
    source 17
    target 1071
  ]
  edge [
    source 17
    target 1072
  ]
  edge [
    source 17
    target 1073
  ]
  edge [
    source 17
    target 693
  ]
  edge [
    source 17
    target 1074
  ]
  edge [
    source 17
    target 1075
  ]
  edge [
    source 17
    target 236
  ]
  edge [
    source 17
    target 1076
  ]
  edge [
    source 17
    target 1077
  ]
  edge [
    source 17
    target 1078
  ]
  edge [
    source 17
    target 1079
  ]
  edge [
    source 17
    target 330
  ]
  edge [
    source 17
    target 1080
  ]
  edge [
    source 17
    target 1081
  ]
  edge [
    source 17
    target 695
  ]
  edge [
    source 17
    target 1082
  ]
  edge [
    source 17
    target 1083
  ]
  edge [
    source 17
    target 1084
  ]
  edge [
    source 17
    target 1085
  ]
  edge [
    source 17
    target 1086
  ]
  edge [
    source 17
    target 1087
  ]
  edge [
    source 17
    target 1088
  ]
  edge [
    source 17
    target 1089
  ]
  edge [
    source 17
    target 1090
  ]
  edge [
    source 17
    target 1091
  ]
  edge [
    source 17
    target 1092
  ]
  edge [
    source 17
    target 1093
  ]
  edge [
    source 17
    target 290
  ]
  edge [
    source 17
    target 1094
  ]
  edge [
    source 17
    target 1095
  ]
  edge [
    source 17
    target 1096
  ]
  edge [
    source 17
    target 1097
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 1098
  ]
  edge [
    source 17
    target 1099
  ]
  edge [
    source 17
    target 1100
  ]
  edge [
    source 17
    target 1101
  ]
  edge [
    source 17
    target 1102
  ]
  edge [
    source 17
    target 1103
  ]
  edge [
    source 17
    target 1104
  ]
  edge [
    source 17
    target 1105
  ]
  edge [
    source 17
    target 1106
  ]
  edge [
    source 17
    target 1107
  ]
  edge [
    source 17
    target 1108
  ]
  edge [
    source 17
    target 1109
  ]
  edge [
    source 17
    target 1110
  ]
  edge [
    source 17
    target 1111
  ]
  edge [
    source 17
    target 1112
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 1113
  ]
  edge [
    source 17
    target 1114
  ]
  edge [
    source 17
    target 1115
  ]
  edge [
    source 17
    target 1116
  ]
  edge [
    source 17
    target 1117
  ]
  edge [
    source 17
    target 1118
  ]
  edge [
    source 17
    target 1119
  ]
  edge [
    source 17
    target 1120
  ]
  edge [
    source 17
    target 1121
  ]
  edge [
    source 17
    target 1122
  ]
  edge [
    source 17
    target 1123
  ]
  edge [
    source 17
    target 1124
  ]
  edge [
    source 17
    target 1125
  ]
  edge [
    source 17
    target 1126
  ]
  edge [
    source 17
    target 1127
  ]
  edge [
    source 17
    target 1128
  ]
  edge [
    source 17
    target 1129
  ]
  edge [
    source 17
    target 1130
  ]
  edge [
    source 17
    target 1131
  ]
  edge [
    source 17
    target 1132
  ]
  edge [
    source 17
    target 1133
  ]
  edge [
    source 17
    target 1134
  ]
  edge [
    source 17
    target 1135
  ]
  edge [
    source 17
    target 1136
  ]
  edge [
    source 17
    target 1137
  ]
  edge [
    source 17
    target 1138
  ]
  edge [
    source 17
    target 1139
  ]
  edge [
    source 17
    target 1140
  ]
  edge [
    source 17
    target 1141
  ]
  edge [
    source 17
    target 76
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 1142
  ]
  edge [
    source 17
    target 1143
  ]
  edge [
    source 17
    target 257
  ]
  edge [
    source 17
    target 1144
  ]
  edge [
    source 17
    target 1145
  ]
  edge [
    source 17
    target 603
  ]
  edge [
    source 17
    target 1146
  ]
  edge [
    source 17
    target 597
  ]
  edge [
    source 17
    target 606
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 1147
  ]
  edge [
    source 17
    target 502
  ]
  edge [
    source 17
    target 1148
  ]
  edge [
    source 17
    target 1149
  ]
  edge [
    source 17
    target 1150
  ]
  edge [
    source 17
    target 1151
  ]
  edge [
    source 17
    target 1152
  ]
  edge [
    source 17
    target 1153
  ]
  edge [
    source 17
    target 1154
  ]
  edge [
    source 17
    target 1155
  ]
  edge [
    source 17
    target 1156
  ]
  edge [
    source 17
    target 1157
  ]
  edge [
    source 17
    target 1158
  ]
  edge [
    source 17
    target 1159
  ]
  edge [
    source 17
    target 1160
  ]
  edge [
    source 17
    target 1161
  ]
  edge [
    source 17
    target 1162
  ]
  edge [
    source 17
    target 82
  ]
  edge [
    source 17
    target 1163
  ]
  edge [
    source 17
    target 1164
  ]
  edge [
    source 17
    target 1165
  ]
  edge [
    source 17
    target 1166
  ]
  edge [
    source 17
    target 1167
  ]
  edge [
    source 17
    target 1168
  ]
  edge [
    source 17
    target 1169
  ]
  edge [
    source 17
    target 1170
  ]
  edge [
    source 17
    target 1171
  ]
  edge [
    source 17
    target 1172
  ]
  edge [
    source 17
    target 1173
  ]
  edge [
    source 17
    target 1174
  ]
  edge [
    source 17
    target 1175
  ]
  edge [
    source 17
    target 1176
  ]
  edge [
    source 17
    target 1177
  ]
  edge [
    source 17
    target 1178
  ]
  edge [
    source 17
    target 1179
  ]
  edge [
    source 17
    target 1180
  ]
  edge [
    source 17
    target 1181
  ]
  edge [
    source 17
    target 1182
  ]
  edge [
    source 17
    target 1183
  ]
  edge [
    source 17
    target 1184
  ]
  edge [
    source 17
    target 1185
  ]
  edge [
    source 17
    target 1186
  ]
  edge [
    source 17
    target 1187
  ]
  edge [
    source 17
    target 1188
  ]
  edge [
    source 17
    target 1189
  ]
  edge [
    source 17
    target 1190
  ]
  edge [
    source 17
    target 1191
  ]
  edge [
    source 17
    target 1192
  ]
  edge [
    source 17
    target 1193
  ]
  edge [
    source 17
    target 1194
  ]
  edge [
    source 17
    target 1195
  ]
  edge [
    source 17
    target 1196
  ]
  edge [
    source 17
    target 1197
  ]
  edge [
    source 17
    target 1198
  ]
  edge [
    source 17
    target 1199
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 1200
  ]
  edge [
    source 17
    target 1201
  ]
  edge [
    source 17
    target 1202
  ]
  edge [
    source 17
    target 1203
  ]
  edge [
    source 17
    target 1204
  ]
  edge [
    source 17
    target 1205
  ]
  edge [
    source 17
    target 1206
  ]
  edge [
    source 17
    target 1207
  ]
  edge [
    source 17
    target 1208
  ]
  edge [
    source 17
    target 1209
  ]
  edge [
    source 17
    target 1210
  ]
  edge [
    source 17
    target 1211
  ]
  edge [
    source 17
    target 1212
  ]
  edge [
    source 17
    target 1213
  ]
  edge [
    source 17
    target 1214
  ]
  edge [
    source 17
    target 1215
  ]
  edge [
    source 17
    target 1216
  ]
  edge [
    source 17
    target 1217
  ]
  edge [
    source 17
    target 1218
  ]
  edge [
    source 17
    target 1219
  ]
  edge [
    source 17
    target 1220
  ]
  edge [
    source 17
    target 1221
  ]
  edge [
    source 17
    target 1222
  ]
  edge [
    source 17
    target 1223
  ]
  edge [
    source 17
    target 1224
  ]
  edge [
    source 17
    target 1225
  ]
  edge [
    source 17
    target 1226
  ]
  edge [
    source 17
    target 1227
  ]
  edge [
    source 17
    target 1228
  ]
  edge [
    source 17
    target 1229
  ]
  edge [
    source 17
    target 1230
  ]
  edge [
    source 17
    target 1231
  ]
  edge [
    source 17
    target 1232
  ]
  edge [
    source 17
    target 1233
  ]
  edge [
    source 17
    target 1234
  ]
  edge [
    source 17
    target 1235
  ]
  edge [
    source 17
    target 1236
  ]
  edge [
    source 17
    target 1237
  ]
  edge [
    source 17
    target 1238
  ]
  edge [
    source 17
    target 1239
  ]
  edge [
    source 17
    target 1240
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 1241
  ]
  edge [
    source 17
    target 1242
  ]
  edge [
    source 17
    target 1243
  ]
  edge [
    source 17
    target 1244
  ]
  edge [
    source 17
    target 1245
  ]
  edge [
    source 17
    target 1246
  ]
  edge [
    source 17
    target 1247
  ]
  edge [
    source 17
    target 1248
  ]
  edge [
    source 17
    target 1249
  ]
  edge [
    source 17
    target 1250
  ]
  edge [
    source 17
    target 83
  ]
  edge [
    source 17
    target 1251
  ]
  edge [
    source 17
    target 1252
  ]
  edge [
    source 17
    target 522
  ]
  edge [
    source 17
    target 1253
  ]
  edge [
    source 17
    target 711
  ]
  edge [
    source 17
    target 1254
  ]
  edge [
    source 17
    target 1255
  ]
  edge [
    source 17
    target 1256
  ]
  edge [
    source 17
    target 1257
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 1258
  ]
  edge [
    source 17
    target 1259
  ]
  edge [
    source 17
    target 1260
  ]
  edge [
    source 17
    target 1261
  ]
  edge [
    source 17
    target 1262
  ]
  edge [
    source 17
    target 1263
  ]
  edge [
    source 17
    target 1264
  ]
  edge [
    source 17
    target 1265
  ]
  edge [
    source 17
    target 1266
  ]
  edge [
    source 17
    target 1267
  ]
  edge [
    source 17
    target 1268
  ]
  edge [
    source 17
    target 1269
  ]
  edge [
    source 17
    target 1270
  ]
  edge [
    source 17
    target 1271
  ]
  edge [
    source 17
    target 1272
  ]
  edge [
    source 17
    target 1273
  ]
  edge [
    source 17
    target 1274
  ]
  edge [
    source 17
    target 1275
  ]
  edge [
    source 17
    target 1276
  ]
  edge [
    source 17
    target 1277
  ]
  edge [
    source 17
    target 1278
  ]
  edge [
    source 17
    target 1279
  ]
  edge [
    source 17
    target 1280
  ]
  edge [
    source 17
    target 1281
  ]
  edge [
    source 17
    target 1282
  ]
  edge [
    source 17
    target 1283
  ]
  edge [
    source 17
    target 1284
  ]
  edge [
    source 17
    target 1285
  ]
  edge [
    source 17
    target 1286
  ]
  edge [
    source 17
    target 1287
  ]
  edge [
    source 17
    target 536
  ]
  edge [
    source 17
    target 1288
  ]
  edge [
    source 17
    target 1289
  ]
  edge [
    source 17
    target 1290
  ]
  edge [
    source 17
    target 1291
  ]
  edge [
    source 17
    target 1292
  ]
  edge [
    source 17
    target 1293
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 1294
  ]
  edge [
    source 17
    target 1295
  ]
  edge [
    source 17
    target 1296
  ]
  edge [
    source 17
    target 1297
  ]
  edge [
    source 17
    target 518
  ]
  edge [
    source 17
    target 1298
  ]
  edge [
    source 17
    target 1299
  ]
  edge [
    source 17
    target 1300
  ]
  edge [
    source 17
    target 1301
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1302
  ]
  edge [
    source 18
    target 1303
  ]
  edge [
    source 18
    target 1304
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 1305
  ]
  edge [
    source 18
    target 1306
  ]
  edge [
    source 18
    target 1307
  ]
  edge [
    source 18
    target 1308
  ]
  edge [
    source 18
    target 1309
  ]
  edge [
    source 18
    target 29
  ]
  edge [
    source 18
    target 1310
  ]
  edge [
    source 18
    target 1311
  ]
  edge [
    source 18
    target 1312
  ]
  edge [
    source 18
    target 518
  ]
  edge [
    source 18
    target 1313
  ]
  edge [
    source 18
    target 1314
  ]
  edge [
    source 18
    target 1315
  ]
  edge [
    source 18
    target 1316
  ]
  edge [
    source 18
    target 1317
  ]
  edge [
    source 18
    target 1318
  ]
  edge [
    source 18
    target 1319
  ]
  edge [
    source 18
    target 1320
  ]
  edge [
    source 18
    target 1321
  ]
  edge [
    source 18
    target 1322
  ]
  edge [
    source 18
    target 1323
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 1324
  ]
  edge [
    source 18
    target 1325
  ]
  edge [
    source 18
    target 102
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 1326
  ]
  edge [
    source 18
    target 1327
  ]
  edge [
    source 18
    target 330
  ]
  edge [
    source 18
    target 1328
  ]
  edge [
    source 18
    target 1329
  ]
  edge [
    source 18
    target 1330
  ]
  edge [
    source 18
    target 1331
  ]
  edge [
    source 18
    target 1332
  ]
  edge [
    source 18
    target 1333
  ]
  edge [
    source 18
    target 1334
  ]
  edge [
    source 18
    target 1335
  ]
  edge [
    source 18
    target 290
  ]
  edge [
    source 18
    target 1336
  ]
  edge [
    source 18
    target 1337
  ]
  edge [
    source 18
    target 1338
  ]
  edge [
    source 18
    target 1339
  ]
  edge [
    source 18
    target 1340
  ]
  edge [
    source 18
    target 1341
  ]
  edge [
    source 18
    target 1342
  ]
  edge [
    source 18
    target 1343
  ]
  edge [
    source 18
    target 1344
  ]
  edge [
    source 18
    target 1345
  ]
  edge [
    source 18
    target 1346
  ]
  edge [
    source 18
    target 1347
  ]
  edge [
    source 18
    target 1348
  ]
  edge [
    source 18
    target 1349
  ]
  edge [
    source 18
    target 1350
  ]
  edge [
    source 18
    target 489
  ]
  edge [
    source 18
    target 1351
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 1352
  ]
  edge [
    source 18
    target 1353
  ]
  edge [
    source 18
    target 1354
  ]
  edge [
    source 18
    target 1355
  ]
  edge [
    source 18
    target 1356
  ]
  edge [
    source 18
    target 1357
  ]
  edge [
    source 18
    target 488
  ]
  edge [
    source 18
    target 1358
  ]
  edge [
    source 18
    target 1359
  ]
  edge [
    source 18
    target 77
  ]
  edge [
    source 18
    target 1360
  ]
  edge [
    source 18
    target 1361
  ]
  edge [
    source 18
    target 1362
  ]
  edge [
    source 18
    target 1363
  ]
  edge [
    source 18
    target 1156
  ]
  edge [
    source 18
    target 1364
  ]
  edge [
    source 18
    target 1365
  ]
  edge [
    source 18
    target 1366
  ]
  edge [
    source 18
    target 503
  ]
  edge [
    source 18
    target 1367
  ]
  edge [
    source 18
    target 1368
  ]
  edge [
    source 18
    target 1158
  ]
  edge [
    source 18
    target 723
  ]
  edge [
    source 18
    target 1369
  ]
  edge [
    source 18
    target 1262
  ]
  edge [
    source 18
    target 257
  ]
  edge [
    source 18
    target 1370
  ]
  edge [
    source 18
    target 1371
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 1372
  ]
  edge [
    source 18
    target 1373
  ]
  edge [
    source 18
    target 1374
  ]
  edge [
    source 18
    target 1375
  ]
  edge [
    source 18
    target 1376
  ]
  edge [
    source 18
    target 1095
  ]
  edge [
    source 18
    target 1061
  ]
  edge [
    source 18
    target 645
  ]
  edge [
    source 18
    target 502
  ]
  edge [
    source 18
    target 1377
  ]
  edge [
    source 18
    target 1378
  ]
  edge [
    source 18
    target 1269
  ]
  edge [
    source 18
    target 1261
  ]
  edge [
    source 18
    target 1379
  ]
  edge [
    source 18
    target 1380
  ]
  edge [
    source 18
    target 1263
  ]
  edge [
    source 18
    target 1268
  ]
  edge [
    source 18
    target 1265
  ]
  edge [
    source 18
    target 1072
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 1162
  ]
  edge [
    source 18
    target 1381
  ]
  edge [
    source 18
    target 1382
  ]
  edge [
    source 18
    target 1383
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1384
  ]
  edge [
    source 20
    target 1385
  ]
  edge [
    source 20
    target 1386
  ]
  edge [
    source 20
    target 1387
  ]
  edge [
    source 20
    target 1388
  ]
  edge [
    source 20
    target 1389
  ]
  edge [
    source 20
    target 1390
  ]
  edge [
    source 20
    target 1391
  ]
  edge [
    source 20
    target 1392
  ]
  edge [
    source 20
    target 1393
  ]
  edge [
    source 20
    target 1394
  ]
  edge [
    source 20
    target 806
  ]
  edge [
    source 20
    target 1395
  ]
  edge [
    source 20
    target 1396
  ]
  edge [
    source 20
    target 1397
  ]
  edge [
    source 20
    target 1398
  ]
  edge [
    source 20
    target 1399
  ]
  edge [
    source 20
    target 1400
  ]
  edge [
    source 20
    target 1401
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1402
  ]
  edge [
    source 21
    target 1403
  ]
  edge [
    source 21
    target 1404
  ]
  edge [
    source 21
    target 1405
  ]
  edge [
    source 21
    target 1406
  ]
  edge [
    source 21
    target 757
  ]
  edge [
    source 21
    target 1407
  ]
  edge [
    source 21
    target 1408
  ]
  edge [
    source 21
    target 345
  ]
  edge [
    source 21
    target 1409
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 1410
  ]
  edge [
    source 21
    target 1411
  ]
  edge [
    source 21
    target 1412
  ]
  edge [
    source 21
    target 1413
  ]
  edge [
    source 21
    target 1414
  ]
  edge [
    source 21
    target 1415
  ]
  edge [
    source 21
    target 1416
  ]
  edge [
    source 21
    target 1417
  ]
  edge [
    source 21
    target 878
  ]
  edge [
    source 21
    target 1418
  ]
  edge [
    source 21
    target 92
  ]
  edge [
    source 21
    target 1419
  ]
  edge [
    source 21
    target 480
  ]
  edge [
    source 21
    target 1420
  ]
  edge [
    source 21
    target 1421
  ]
  edge [
    source 21
    target 1422
  ]
  edge [
    source 21
    target 1423
  ]
  edge [
    source 21
    target 1424
  ]
  edge [
    source 21
    target 1425
  ]
  edge [
    source 21
    target 1426
  ]
  edge [
    source 21
    target 1427
  ]
  edge [
    source 21
    target 1428
  ]
  edge [
    source 21
    target 1429
  ]
  edge [
    source 21
    target 1430
  ]
  edge [
    source 21
    target 1431
  ]
  edge [
    source 21
    target 1432
  ]
  edge [
    source 21
    target 1433
  ]
  edge [
    source 21
    target 1434
  ]
  edge [
    source 21
    target 1435
  ]
  edge [
    source 21
    target 1436
  ]
  edge [
    source 21
    target 1437
  ]
  edge [
    source 21
    target 715
  ]
  edge [
    source 21
    target 716
  ]
  edge [
    source 21
    target 717
  ]
  edge [
    source 21
    target 718
  ]
  edge [
    source 21
    target 719
  ]
  edge [
    source 21
    target 720
  ]
  edge [
    source 21
    target 721
  ]
  edge [
    source 21
    target 722
  ]
  edge [
    source 21
    target 723
  ]
  edge [
    source 21
    target 724
  ]
  edge [
    source 21
    target 725
  ]
  edge [
    source 21
    target 726
  ]
  edge [
    source 21
    target 727
  ]
  edge [
    source 21
    target 423
  ]
  edge [
    source 21
    target 728
  ]
  edge [
    source 21
    target 729
  ]
  edge [
    source 21
    target 730
  ]
  edge [
    source 21
    target 731
  ]
  edge [
    source 21
    target 732
  ]
  edge [
    source 21
    target 599
  ]
  edge [
    source 21
    target 733
  ]
  edge [
    source 21
    target 429
  ]
  edge [
    source 21
    target 734
  ]
  edge [
    source 21
    target 735
  ]
  edge [
    source 21
    target 736
  ]
  edge [
    source 21
    target 737
  ]
  edge [
    source 21
    target 738
  ]
  edge [
    source 21
    target 450
  ]
  edge [
    source 21
    target 739
  ]
  edge [
    source 21
    target 433
  ]
  edge [
    source 21
    target 740
  ]
  edge [
    source 21
    target 741
  ]
  edge [
    source 21
    target 742
  ]
  edge [
    source 21
    target 743
  ]
  edge [
    source 21
    target 744
  ]
  edge [
    source 21
    target 436
  ]
  edge [
    source 21
    target 745
  ]
  edge [
    source 21
    target 432
  ]
  edge [
    source 21
    target 589
  ]
  edge [
    source 21
    target 1438
  ]
  edge [
    source 21
    target 1439
  ]
  edge [
    source 21
    target 1440
  ]
  edge [
    source 21
    target 1441
  ]
  edge [
    source 21
    target 1442
  ]
  edge [
    source 21
    target 1443
  ]
  edge [
    source 21
    target 1444
  ]
  edge [
    source 21
    target 1445
  ]
  edge [
    source 21
    target 1446
  ]
  edge [
    source 21
    target 1447
  ]
  edge [
    source 21
    target 1448
  ]
  edge [
    source 21
    target 1449
  ]
  edge [
    source 21
    target 1450
  ]
  edge [
    source 21
    target 1451
  ]
  edge [
    source 21
    target 1452
  ]
  edge [
    source 21
    target 1453
  ]
  edge [
    source 21
    target 388
  ]
  edge [
    source 21
    target 1454
  ]
  edge [
    source 21
    target 1455
  ]
  edge [
    source 21
    target 389
  ]
  edge [
    source 21
    target 1456
  ]
  edge [
    source 21
    target 390
  ]
  edge [
    source 21
    target 1457
  ]
  edge [
    source 21
    target 393
  ]
  edge [
    source 21
    target 1458
  ]
  edge [
    source 21
    target 311
  ]
  edge [
    source 21
    target 399
  ]
  edge [
    source 21
    target 460
  ]
  edge [
    source 21
    target 402
  ]
  edge [
    source 21
    target 1459
  ]
  edge [
    source 21
    target 1460
  ]
  edge [
    source 21
    target 404
  ]
  edge [
    source 21
    target 1461
  ]
  edge [
    source 21
    target 1462
  ]
  edge [
    source 21
    target 1463
  ]
  edge [
    source 21
    target 1464
  ]
  edge [
    source 21
    target 405
  ]
  edge [
    source 21
    target 588
  ]
  edge [
    source 21
    target 1465
  ]
  edge [
    source 21
    target 1466
  ]
  edge [
    source 21
    target 1467
  ]
  edge [
    source 21
    target 411
  ]
  edge [
    source 21
    target 1468
  ]
  edge [
    source 21
    target 1469
  ]
  edge [
    source 21
    target 1470
  ]
  edge [
    source 21
    target 1471
  ]
  edge [
    source 21
    target 415
  ]
  edge [
    source 21
    target 1472
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1473
  ]
  edge [
    source 23
    target 1474
  ]
  edge [
    source 23
    target 1475
  ]
  edge [
    source 23
    target 1476
  ]
  edge [
    source 23
    target 1477
  ]
  edge [
    source 23
    target 1478
  ]
  edge [
    source 23
    target 1479
  ]
  edge [
    source 23
    target 1480
  ]
  edge [
    source 23
    target 1481
  ]
  edge [
    source 23
    target 1482
  ]
  edge [
    source 23
    target 1483
  ]
  edge [
    source 23
    target 1484
  ]
  edge [
    source 23
    target 1485
  ]
  edge [
    source 23
    target 1486
  ]
  edge [
    source 23
    target 1487
  ]
  edge [
    source 23
    target 726
  ]
  edge [
    source 23
    target 1488
  ]
  edge [
    source 23
    target 1489
  ]
  edge [
    source 23
    target 868
  ]
  edge [
    source 23
    target 1490
  ]
  edge [
    source 23
    target 1491
  ]
  edge [
    source 23
    target 1492
  ]
  edge [
    source 23
    target 1493
  ]
  edge [
    source 23
    target 873
  ]
  edge [
    source 23
    target 1494
  ]
  edge [
    source 23
    target 485
  ]
  edge [
    source 23
    target 876
  ]
  edge [
    source 23
    target 1495
  ]
  edge [
    source 23
    target 1496
  ]
  edge [
    source 23
    target 1497
  ]
  edge [
    source 23
    target 1498
  ]
  edge [
    source 23
    target 1499
  ]
  edge [
    source 23
    target 684
  ]
  edge [
    source 23
    target 1500
  ]
  edge [
    source 23
    target 1501
  ]
  edge [
    source 23
    target 1502
  ]
  edge [
    source 23
    target 150
  ]
  edge [
    source 23
    target 1503
  ]
  edge [
    source 23
    target 1504
  ]
  edge [
    source 23
    target 879
  ]
  edge [
    source 23
    target 1505
  ]
  edge [
    source 23
    target 153
  ]
  edge [
    source 23
    target 1506
  ]
  edge [
    source 23
    target 1507
  ]
  edge [
    source 23
    target 1508
  ]
  edge [
    source 23
    target 330
  ]
  edge [
    source 23
    target 890
  ]
  edge [
    source 23
    target 1509
  ]
  edge [
    source 23
    target 1510
  ]
  edge [
    source 23
    target 1511
  ]
  edge [
    source 23
    target 1512
  ]
  edge [
    source 23
    target 982
  ]
  edge [
    source 23
    target 1513
  ]
  edge [
    source 23
    target 1014
  ]
  edge [
    source 23
    target 1514
  ]
  edge [
    source 23
    target 1515
  ]
  edge [
    source 23
    target 1516
  ]
  edge [
    source 23
    target 138
  ]
  edge [
    source 23
    target 1517
  ]
  edge [
    source 23
    target 1518
  ]
  edge [
    source 23
    target 1519
  ]
  edge [
    source 23
    target 1520
  ]
  edge [
    source 23
    target 1521
  ]
  edge [
    source 23
    target 1522
  ]
  edge [
    source 23
    target 1523
  ]
  edge [
    source 23
    target 1524
  ]
  edge [
    source 23
    target 1525
  ]
  edge [
    source 23
    target 1526
  ]
  edge [
    source 23
    target 1527
  ]
]
