graph [
  node [
    id 0
    label "zasadniczy"
    origin "text"
  ]
  node [
    id 1
    label "pytanie"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zada&#263;"
    origin "text"
  ]
  node [
    id 5
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 6
    label "podstawa"
    origin "text"
  ]
  node [
    id 7
    label "autor"
    origin "text"
  ]
  node [
    id 8
    label "sprawozdanie"
    origin "text"
  ]
  node [
    id 9
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 10
    label "niedopuszczalny"
    origin "text"
  ]
  node [
    id 11
    label "interpretowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "stanowisko"
    origin "text"
  ]
  node [
    id 13
    label "minister"
    origin "text"
  ]
  node [
    id 14
    label "gilowska"
    origin "text"
  ]
  node [
    id 15
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "poda&#263;"
    origin "text"
  ]
  node [
    id 17
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 18
    label "dow&#243;d"
    origin "text"
  ]
  node [
    id 19
    label "taka"
    origin "text"
  ]
  node [
    id 20
    label "interpretacja"
    origin "text"
  ]
  node [
    id 21
    label "uprawdopodobni&#263;"
    origin "text"
  ]
  node [
    id 22
    label "miara"
    origin "text"
  ]
  node [
    id 23
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 24
    label "uzna&#263;"
    origin "text"
  ]
  node [
    id 25
    label "wiarygodny"
    origin "text"
  ]
  node [
    id 26
    label "g&#322;&#243;wny"
  ]
  node [
    id 27
    label "og&#243;lny"
  ]
  node [
    id 28
    label "zasadniczo"
  ]
  node [
    id 29
    label "surowy"
  ]
  node [
    id 30
    label "najwa&#380;niejszy"
  ]
  node [
    id 31
    label "g&#322;&#243;wnie"
  ]
  node [
    id 32
    label "og&#243;lnie"
  ]
  node [
    id 33
    label "zbiorowy"
  ]
  node [
    id 34
    label "og&#243;&#322;owy"
  ]
  node [
    id 35
    label "nadrz&#281;dny"
  ]
  node [
    id 36
    label "ca&#322;y"
  ]
  node [
    id 37
    label "kompletny"
  ]
  node [
    id 38
    label "&#322;&#261;czny"
  ]
  node [
    id 39
    label "powszechnie"
  ]
  node [
    id 40
    label "gro&#378;nie"
  ]
  node [
    id 41
    label "twardy"
  ]
  node [
    id 42
    label "trudny"
  ]
  node [
    id 43
    label "srogi"
  ]
  node [
    id 44
    label "powa&#380;ny"
  ]
  node [
    id 45
    label "dokuczliwy"
  ]
  node [
    id 46
    label "surowo"
  ]
  node [
    id 47
    label "oszcz&#281;dny"
  ]
  node [
    id 48
    label "&#347;wie&#380;y"
  ]
  node [
    id 49
    label "pryncypalnie"
  ]
  node [
    id 50
    label "sprawa"
  ]
  node [
    id 51
    label "wypytanie"
  ]
  node [
    id 52
    label "egzaminowanie"
  ]
  node [
    id 53
    label "zwracanie_si&#281;"
  ]
  node [
    id 54
    label "wywo&#322;ywanie"
  ]
  node [
    id 55
    label "rozpytywanie"
  ]
  node [
    id 56
    label "wypowiedzenie"
  ]
  node [
    id 57
    label "wypowied&#378;"
  ]
  node [
    id 58
    label "problemat"
  ]
  node [
    id 59
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 60
    label "problematyka"
  ]
  node [
    id 61
    label "sprawdzian"
  ]
  node [
    id 62
    label "zadanie"
  ]
  node [
    id 63
    label "odpowiada&#263;"
  ]
  node [
    id 64
    label "przes&#322;uchiwanie"
  ]
  node [
    id 65
    label "question"
  ]
  node [
    id 66
    label "sprawdzanie"
  ]
  node [
    id 67
    label "odpowiadanie"
  ]
  node [
    id 68
    label "survey"
  ]
  node [
    id 69
    label "pos&#322;uchanie"
  ]
  node [
    id 70
    label "s&#261;d"
  ]
  node [
    id 71
    label "sparafrazowanie"
  ]
  node [
    id 72
    label "strawestowa&#263;"
  ]
  node [
    id 73
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 74
    label "trawestowa&#263;"
  ]
  node [
    id 75
    label "sparafrazowa&#263;"
  ]
  node [
    id 76
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 77
    label "sformu&#322;owanie"
  ]
  node [
    id 78
    label "parafrazowanie"
  ]
  node [
    id 79
    label "ozdobnik"
  ]
  node [
    id 80
    label "delimitacja"
  ]
  node [
    id 81
    label "parafrazowa&#263;"
  ]
  node [
    id 82
    label "stylizacja"
  ]
  node [
    id 83
    label "komunikat"
  ]
  node [
    id 84
    label "trawestowanie"
  ]
  node [
    id 85
    label "strawestowanie"
  ]
  node [
    id 86
    label "rezultat"
  ]
  node [
    id 87
    label "konwersja"
  ]
  node [
    id 88
    label "notice"
  ]
  node [
    id 89
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 90
    label "przepowiedzenie"
  ]
  node [
    id 91
    label "rozwi&#261;zanie"
  ]
  node [
    id 92
    label "generowa&#263;"
  ]
  node [
    id 93
    label "wydanie"
  ]
  node [
    id 94
    label "message"
  ]
  node [
    id 95
    label "generowanie"
  ]
  node [
    id 96
    label "wydobycie"
  ]
  node [
    id 97
    label "zwerbalizowanie"
  ]
  node [
    id 98
    label "szyk"
  ]
  node [
    id 99
    label "notification"
  ]
  node [
    id 100
    label "powiedzenie"
  ]
  node [
    id 101
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 102
    label "denunciation"
  ]
  node [
    id 103
    label "wyra&#380;enie"
  ]
  node [
    id 104
    label "zaj&#281;cie"
  ]
  node [
    id 105
    label "yield"
  ]
  node [
    id 106
    label "zbi&#243;r"
  ]
  node [
    id 107
    label "zaszkodzenie"
  ]
  node [
    id 108
    label "za&#322;o&#380;enie"
  ]
  node [
    id 109
    label "duty"
  ]
  node [
    id 110
    label "powierzanie"
  ]
  node [
    id 111
    label "work"
  ]
  node [
    id 112
    label "problem"
  ]
  node [
    id 113
    label "przepisanie"
  ]
  node [
    id 114
    label "nakarmienie"
  ]
  node [
    id 115
    label "przepisa&#263;"
  ]
  node [
    id 116
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 117
    label "czynno&#347;&#263;"
  ]
  node [
    id 118
    label "zobowi&#261;zanie"
  ]
  node [
    id 119
    label "kognicja"
  ]
  node [
    id 120
    label "object"
  ]
  node [
    id 121
    label "rozprawa"
  ]
  node [
    id 122
    label "temat"
  ]
  node [
    id 123
    label "wydarzenie"
  ]
  node [
    id 124
    label "szczeg&#243;&#322;"
  ]
  node [
    id 125
    label "proposition"
  ]
  node [
    id 126
    label "przes&#322;anka"
  ]
  node [
    id 127
    label "rzecz"
  ]
  node [
    id 128
    label "idea"
  ]
  node [
    id 129
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 130
    label "ustalenie"
  ]
  node [
    id 131
    label "redagowanie"
  ]
  node [
    id 132
    label "ustalanie"
  ]
  node [
    id 133
    label "dociekanie"
  ]
  node [
    id 134
    label "robienie"
  ]
  node [
    id 135
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 136
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 137
    label "investigation"
  ]
  node [
    id 138
    label "macanie"
  ]
  node [
    id 139
    label "usi&#322;owanie"
  ]
  node [
    id 140
    label "penetrowanie"
  ]
  node [
    id 141
    label "przymierzanie"
  ]
  node [
    id 142
    label "przymierzenie"
  ]
  node [
    id 143
    label "examination"
  ]
  node [
    id 144
    label "wypytywanie"
  ]
  node [
    id 145
    label "zbadanie"
  ]
  node [
    id 146
    label "react"
  ]
  node [
    id 147
    label "dawa&#263;"
  ]
  node [
    id 148
    label "by&#263;"
  ]
  node [
    id 149
    label "ponosi&#263;"
  ]
  node [
    id 150
    label "report"
  ]
  node [
    id 151
    label "equate"
  ]
  node [
    id 152
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 153
    label "answer"
  ]
  node [
    id 154
    label "powodowa&#263;"
  ]
  node [
    id 155
    label "tone"
  ]
  node [
    id 156
    label "contend"
  ]
  node [
    id 157
    label "reagowa&#263;"
  ]
  node [
    id 158
    label "impart"
  ]
  node [
    id 159
    label "reagowanie"
  ]
  node [
    id 160
    label "dawanie"
  ]
  node [
    id 161
    label "powodowanie"
  ]
  node [
    id 162
    label "bycie"
  ]
  node [
    id 163
    label "pokutowanie"
  ]
  node [
    id 164
    label "odpowiedzialny"
  ]
  node [
    id 165
    label "winny"
  ]
  node [
    id 166
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 167
    label "picie_piwa"
  ]
  node [
    id 168
    label "odpowiedni"
  ]
  node [
    id 169
    label "parry"
  ]
  node [
    id 170
    label "fit"
  ]
  node [
    id 171
    label "dzianie_si&#281;"
  ]
  node [
    id 172
    label "rendition"
  ]
  node [
    id 173
    label "ponoszenie"
  ]
  node [
    id 174
    label "rozmawianie"
  ]
  node [
    id 175
    label "faza"
  ]
  node [
    id 176
    label "podchodzi&#263;"
  ]
  node [
    id 177
    label "&#263;wiczenie"
  ]
  node [
    id 178
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 179
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 180
    label "praca_pisemna"
  ]
  node [
    id 181
    label "kontrola"
  ]
  node [
    id 182
    label "dydaktyka"
  ]
  node [
    id 183
    label "pr&#243;ba"
  ]
  node [
    id 184
    label "przepytywanie"
  ]
  node [
    id 185
    label "oznajmianie"
  ]
  node [
    id 186
    label "wzywanie"
  ]
  node [
    id 187
    label "development"
  ]
  node [
    id 188
    label "exploitation"
  ]
  node [
    id 189
    label "zdawanie"
  ]
  node [
    id 190
    label "w&#322;&#261;czanie"
  ]
  node [
    id 191
    label "s&#322;uchanie"
  ]
  node [
    id 192
    label "para"
  ]
  node [
    id 193
    label "necessity"
  ]
  node [
    id 194
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 195
    label "trza"
  ]
  node [
    id 196
    label "uczestniczy&#263;"
  ]
  node [
    id 197
    label "participate"
  ]
  node [
    id 198
    label "robi&#263;"
  ]
  node [
    id 199
    label "trzeba"
  ]
  node [
    id 200
    label "pair"
  ]
  node [
    id 201
    label "zesp&#243;&#322;"
  ]
  node [
    id 202
    label "odparowywanie"
  ]
  node [
    id 203
    label "gaz_cieplarniany"
  ]
  node [
    id 204
    label "chodzi&#263;"
  ]
  node [
    id 205
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 206
    label "poker"
  ]
  node [
    id 207
    label "moneta"
  ]
  node [
    id 208
    label "parowanie"
  ]
  node [
    id 209
    label "damp"
  ]
  node [
    id 210
    label "sztuka"
  ]
  node [
    id 211
    label "odparowanie"
  ]
  node [
    id 212
    label "grupa"
  ]
  node [
    id 213
    label "odparowa&#263;"
  ]
  node [
    id 214
    label "dodatek"
  ]
  node [
    id 215
    label "jednostka_monetarna"
  ]
  node [
    id 216
    label "smoke"
  ]
  node [
    id 217
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 218
    label "odparowywa&#263;"
  ]
  node [
    id 219
    label "uk&#322;ad"
  ]
  node [
    id 220
    label "Albania"
  ]
  node [
    id 221
    label "gaz"
  ]
  node [
    id 222
    label "wyparowanie"
  ]
  node [
    id 223
    label "d&#378;wign&#261;&#263;"
  ]
  node [
    id 224
    label "zaszkodzi&#263;"
  ]
  node [
    id 225
    label "put"
  ]
  node [
    id 226
    label "deal"
  ]
  node [
    id 227
    label "set"
  ]
  node [
    id 228
    label "zaj&#261;&#263;"
  ]
  node [
    id 229
    label "distribute"
  ]
  node [
    id 230
    label "nakarmi&#263;"
  ]
  node [
    id 231
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 232
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 233
    label "obowi&#261;za&#263;"
  ]
  node [
    id 234
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 235
    label "perpetrate"
  ]
  node [
    id 236
    label "hurt"
  ]
  node [
    id 237
    label "spowodowa&#263;"
  ]
  node [
    id 238
    label "injury"
  ]
  node [
    id 239
    label "zrobi&#263;"
  ]
  node [
    id 240
    label "wyr&#281;czy&#263;"
  ]
  node [
    id 241
    label "da&#263;"
  ]
  node [
    id 242
    label "feed"
  ]
  node [
    id 243
    label "utrzyma&#263;"
  ]
  node [
    id 244
    label "zapoda&#263;"
  ]
  node [
    id 245
    label "wm&#243;wi&#263;"
  ]
  node [
    id 246
    label "invest"
  ]
  node [
    id 247
    label "plant"
  ]
  node [
    id 248
    label "load"
  ]
  node [
    id 249
    label "ubra&#263;"
  ]
  node [
    id 250
    label "oblec_si&#281;"
  ]
  node [
    id 251
    label "oblec"
  ]
  node [
    id 252
    label "str&#243;j"
  ]
  node [
    id 253
    label "pokry&#263;"
  ]
  node [
    id 254
    label "podwin&#261;&#263;"
  ]
  node [
    id 255
    label "przewidzie&#263;"
  ]
  node [
    id 256
    label "przyodzia&#263;"
  ]
  node [
    id 257
    label "jell"
  ]
  node [
    id 258
    label "umie&#347;ci&#263;"
  ]
  node [
    id 259
    label "insert"
  ]
  node [
    id 260
    label "utworzy&#263;"
  ]
  node [
    id 261
    label "zap&#322;aci&#263;"
  ]
  node [
    id 262
    label "create"
  ]
  node [
    id 263
    label "install"
  ]
  node [
    id 264
    label "map"
  ]
  node [
    id 265
    label "zapanowa&#263;"
  ]
  node [
    id 266
    label "rozciekawi&#263;"
  ]
  node [
    id 267
    label "skorzysta&#263;"
  ]
  node [
    id 268
    label "komornik"
  ]
  node [
    id 269
    label "ulokowa&#263;_si&#281;"
  ]
  node [
    id 270
    label "klasyfikacja"
  ]
  node [
    id 271
    label "wype&#322;ni&#263;"
  ]
  node [
    id 272
    label "topographic_point"
  ]
  node [
    id 273
    label "obj&#261;&#263;"
  ]
  node [
    id 274
    label "seize"
  ]
  node [
    id 275
    label "interest"
  ]
  node [
    id 276
    label "anektowa&#263;"
  ]
  node [
    id 277
    label "employment"
  ]
  node [
    id 278
    label "prosecute"
  ]
  node [
    id 279
    label "dostarczy&#263;"
  ]
  node [
    id 280
    label "usytuowa&#263;_si&#281;"
  ]
  node [
    id 281
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 282
    label "bankrupt"
  ]
  node [
    id 283
    label "sorb"
  ]
  node [
    id 284
    label "zabra&#263;"
  ]
  node [
    id 285
    label "wzi&#261;&#263;"
  ]
  node [
    id 286
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 287
    label "do"
  ]
  node [
    id 288
    label "wzbudzi&#263;"
  ]
  node [
    id 289
    label "ulepszy&#263;"
  ]
  node [
    id 290
    label "znie&#347;&#263;"
  ]
  node [
    id 291
    label "heft"
  ]
  node [
    id 292
    label "podnie&#347;&#263;"
  ]
  node [
    id 293
    label "arise"
  ]
  node [
    id 294
    label "odbudowa&#263;"
  ]
  node [
    id 295
    label "ud&#378;wign&#261;&#263;"
  ]
  node [
    id 296
    label "raise"
  ]
  node [
    id 297
    label "cover"
  ]
  node [
    id 298
    label "gem"
  ]
  node [
    id 299
    label "kompozycja"
  ]
  node [
    id 300
    label "runda"
  ]
  node [
    id 301
    label "muzyka"
  ]
  node [
    id 302
    label "zestaw"
  ]
  node [
    id 303
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 304
    label "bargain"
  ]
  node [
    id 305
    label "tycze&#263;"
  ]
  node [
    id 306
    label "pot&#281;ga"
  ]
  node [
    id 307
    label "documentation"
  ]
  node [
    id 308
    label "przedmiot"
  ]
  node [
    id 309
    label "column"
  ]
  node [
    id 310
    label "zasadzenie"
  ]
  node [
    id 311
    label "punkt_odniesienia"
  ]
  node [
    id 312
    label "zasadzi&#263;"
  ]
  node [
    id 313
    label "bok"
  ]
  node [
    id 314
    label "d&#243;&#322;"
  ]
  node [
    id 315
    label "dzieci&#281;ctwo"
  ]
  node [
    id 316
    label "background"
  ]
  node [
    id 317
    label "podstawowy"
  ]
  node [
    id 318
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 319
    label "strategia"
  ]
  node [
    id 320
    label "pomys&#322;"
  ]
  node [
    id 321
    label "&#347;ciana"
  ]
  node [
    id 322
    label "podwini&#281;cie"
  ]
  node [
    id 323
    label "zap&#322;acenie"
  ]
  node [
    id 324
    label "przyodzianie"
  ]
  node [
    id 325
    label "budowla"
  ]
  node [
    id 326
    label "pokrycie"
  ]
  node [
    id 327
    label "rozebranie"
  ]
  node [
    id 328
    label "zak&#322;adka"
  ]
  node [
    id 329
    label "struktura"
  ]
  node [
    id 330
    label "poubieranie"
  ]
  node [
    id 331
    label "infliction"
  ]
  node [
    id 332
    label "spowodowanie"
  ]
  node [
    id 333
    label "pozak&#322;adanie"
  ]
  node [
    id 334
    label "program"
  ]
  node [
    id 335
    label "przebranie"
  ]
  node [
    id 336
    label "przywdzianie"
  ]
  node [
    id 337
    label "obleczenie_si&#281;"
  ]
  node [
    id 338
    label "utworzenie"
  ]
  node [
    id 339
    label "twierdzenie"
  ]
  node [
    id 340
    label "obleczenie"
  ]
  node [
    id 341
    label "umieszczenie"
  ]
  node [
    id 342
    label "przygotowywanie"
  ]
  node [
    id 343
    label "wyko&#324;czenie"
  ]
  node [
    id 344
    label "point"
  ]
  node [
    id 345
    label "przygotowanie"
  ]
  node [
    id 346
    label "przewidzenie"
  ]
  node [
    id 347
    label "zrobienie"
  ]
  node [
    id 348
    label "tu&#322;&#243;w"
  ]
  node [
    id 349
    label "kierunek"
  ]
  node [
    id 350
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 351
    label "wielok&#261;t"
  ]
  node [
    id 352
    label "odcinek"
  ]
  node [
    id 353
    label "strzelba"
  ]
  node [
    id 354
    label "lufa"
  ]
  node [
    id 355
    label "strona"
  ]
  node [
    id 356
    label "profil"
  ]
  node [
    id 357
    label "zbocze"
  ]
  node [
    id 358
    label "kszta&#322;t"
  ]
  node [
    id 359
    label "przegroda"
  ]
  node [
    id 360
    label "p&#322;aszczyzna"
  ]
  node [
    id 361
    label "bariera"
  ]
  node [
    id 362
    label "kres"
  ]
  node [
    id 363
    label "facebook"
  ]
  node [
    id 364
    label "wielo&#347;cian"
  ]
  node [
    id 365
    label "obstruction"
  ]
  node [
    id 366
    label "pow&#322;oka"
  ]
  node [
    id 367
    label "wyrobisko"
  ]
  node [
    id 368
    label "miejsce"
  ]
  node [
    id 369
    label "trudno&#347;&#263;"
  ]
  node [
    id 370
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 371
    label "zboczenie"
  ]
  node [
    id 372
    label "om&#243;wienie"
  ]
  node [
    id 373
    label "sponiewieranie"
  ]
  node [
    id 374
    label "discipline"
  ]
  node [
    id 375
    label "omawia&#263;"
  ]
  node [
    id 376
    label "kr&#261;&#380;enie"
  ]
  node [
    id 377
    label "sponiewiera&#263;"
  ]
  node [
    id 378
    label "element"
  ]
  node [
    id 379
    label "entity"
  ]
  node [
    id 380
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 381
    label "tematyka"
  ]
  node [
    id 382
    label "w&#261;tek"
  ]
  node [
    id 383
    label "charakter"
  ]
  node [
    id 384
    label "zbaczanie"
  ]
  node [
    id 385
    label "program_nauczania"
  ]
  node [
    id 386
    label "om&#243;wi&#263;"
  ]
  node [
    id 387
    label "omawianie"
  ]
  node [
    id 388
    label "thing"
  ]
  node [
    id 389
    label "kultura"
  ]
  node [
    id 390
    label "istota"
  ]
  node [
    id 391
    label "zbacza&#263;"
  ]
  node [
    id 392
    label "zboczy&#263;"
  ]
  node [
    id 393
    label "wykopywa&#263;"
  ]
  node [
    id 394
    label "wykopanie"
  ]
  node [
    id 395
    label "&#347;piew"
  ]
  node [
    id 396
    label "wykopywanie"
  ]
  node [
    id 397
    label "hole"
  ]
  node [
    id 398
    label "low"
  ]
  node [
    id 399
    label "niski"
  ]
  node [
    id 400
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 401
    label "depressive_disorder"
  ]
  node [
    id 402
    label "d&#378;wi&#281;k"
  ]
  node [
    id 403
    label "wykopa&#263;"
  ]
  node [
    id 404
    label "za&#322;amanie"
  ]
  node [
    id 405
    label "niezaawansowany"
  ]
  node [
    id 406
    label "pocz&#261;tkowy"
  ]
  node [
    id 407
    label "podstawowo"
  ]
  node [
    id 408
    label "wetkni&#281;cie"
  ]
  node [
    id 409
    label "przetkanie"
  ]
  node [
    id 410
    label "anchor"
  ]
  node [
    id 411
    label "przymocowanie"
  ]
  node [
    id 412
    label "zaczerpni&#281;cie"
  ]
  node [
    id 413
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 414
    label "interposition"
  ]
  node [
    id 415
    label "odm&#322;odzenie"
  ]
  node [
    id 416
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 417
    label "establish"
  ]
  node [
    id 418
    label "osnowa&#263;"
  ]
  node [
    id 419
    label "przymocowa&#263;"
  ]
  node [
    id 420
    label "wetkn&#261;&#263;"
  ]
  node [
    id 421
    label "dzieci&#324;stwo"
  ]
  node [
    id 422
    label "pocz&#261;tki"
  ]
  node [
    id 423
    label "pochodzenie"
  ]
  node [
    id 424
    label "kontekst"
  ]
  node [
    id 425
    label "wytw&#243;r"
  ]
  node [
    id 426
    label "ukra&#347;&#263;"
  ]
  node [
    id 427
    label "ukradzenie"
  ]
  node [
    id 428
    label "system"
  ]
  node [
    id 429
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 430
    label "plan"
  ]
  node [
    id 431
    label "operacja"
  ]
  node [
    id 432
    label "metoda"
  ]
  node [
    id 433
    label "gra"
  ]
  node [
    id 434
    label "wzorzec_projektowy"
  ]
  node [
    id 435
    label "dziedzina"
  ]
  node [
    id 436
    label "doktryna"
  ]
  node [
    id 437
    label "wrinkle"
  ]
  node [
    id 438
    label "dokument"
  ]
  node [
    id 439
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 440
    label "wojsko"
  ]
  node [
    id 441
    label "wyk&#322;adnik_pot&#281;gi"
  ]
  node [
    id 442
    label "organizacja"
  ]
  node [
    id 443
    label "violence"
  ]
  node [
    id 444
    label "podstawa_pot&#281;gi"
  ]
  node [
    id 445
    label "zdolno&#347;&#263;"
  ]
  node [
    id 446
    label "potencja"
  ]
  node [
    id 447
    label "iloczyn"
  ]
  node [
    id 448
    label "kszta&#322;ciciel"
  ]
  node [
    id 449
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 450
    label "tworzyciel"
  ]
  node [
    id 451
    label "wykonawca"
  ]
  node [
    id 452
    label "pomys&#322;odawca"
  ]
  node [
    id 453
    label "&#347;w"
  ]
  node [
    id 454
    label "inicjator"
  ]
  node [
    id 455
    label "podmiot_gospodarczy"
  ]
  node [
    id 456
    label "artysta"
  ]
  node [
    id 457
    label "cz&#322;owiek"
  ]
  node [
    id 458
    label "muzyk"
  ]
  node [
    id 459
    label "nauczyciel"
  ]
  node [
    id 460
    label "korespondent"
  ]
  node [
    id 461
    label "sprawko"
  ]
  node [
    id 462
    label "relacja"
  ]
  node [
    id 463
    label "reporter"
  ]
  node [
    id 464
    label "model"
  ]
  node [
    id 465
    label "narz&#281;dzie"
  ]
  node [
    id 466
    label "tryb"
  ]
  node [
    id 467
    label "nature"
  ]
  node [
    id 468
    label "egzemplarz"
  ]
  node [
    id 469
    label "series"
  ]
  node [
    id 470
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 471
    label "uprawianie"
  ]
  node [
    id 472
    label "praca_rolnicza"
  ]
  node [
    id 473
    label "collection"
  ]
  node [
    id 474
    label "dane"
  ]
  node [
    id 475
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 476
    label "pakiet_klimatyczny"
  ]
  node [
    id 477
    label "poj&#281;cie"
  ]
  node [
    id 478
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 479
    label "sum"
  ]
  node [
    id 480
    label "gathering"
  ]
  node [
    id 481
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 482
    label "album"
  ]
  node [
    id 483
    label "&#347;rodek"
  ]
  node [
    id 484
    label "niezb&#281;dnik"
  ]
  node [
    id 485
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 486
    label "tylec"
  ]
  node [
    id 487
    label "urz&#261;dzenie"
  ]
  node [
    id 488
    label "ko&#322;o"
  ]
  node [
    id 489
    label "modalno&#347;&#263;"
  ]
  node [
    id 490
    label "z&#261;b"
  ]
  node [
    id 491
    label "cecha"
  ]
  node [
    id 492
    label "kategoria_gramatyczna"
  ]
  node [
    id 493
    label "skala"
  ]
  node [
    id 494
    label "funkcjonowa&#263;"
  ]
  node [
    id 495
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 496
    label "koniugacja"
  ]
  node [
    id 497
    label "prezenter"
  ]
  node [
    id 498
    label "typ"
  ]
  node [
    id 499
    label "mildew"
  ]
  node [
    id 500
    label "zi&#243;&#322;ko"
  ]
  node [
    id 501
    label "motif"
  ]
  node [
    id 502
    label "pozowanie"
  ]
  node [
    id 503
    label "ideal"
  ]
  node [
    id 504
    label "wz&#243;r"
  ]
  node [
    id 505
    label "matryca"
  ]
  node [
    id 506
    label "adaptation"
  ]
  node [
    id 507
    label "ruch"
  ]
  node [
    id 508
    label "pozowa&#263;"
  ]
  node [
    id 509
    label "imitacja"
  ]
  node [
    id 510
    label "orygina&#322;"
  ]
  node [
    id 511
    label "facet"
  ]
  node [
    id 512
    label "miniatura"
  ]
  node [
    id 513
    label "niezgodny"
  ]
  node [
    id 514
    label "niedopuszczalnie"
  ]
  node [
    id 515
    label "r&#243;&#380;ny"
  ]
  node [
    id 516
    label "niespokojny"
  ]
  node [
    id 517
    label "odmienny"
  ]
  node [
    id 518
    label "k&#322;&#243;tny"
  ]
  node [
    id 519
    label "niezgodnie"
  ]
  node [
    id 520
    label "napi&#281;ty"
  ]
  node [
    id 521
    label "niedozwolony"
  ]
  node [
    id 522
    label "impermissibly"
  ]
  node [
    id 523
    label "gloss"
  ]
  node [
    id 524
    label "rozumie&#263;"
  ]
  node [
    id 525
    label "give"
  ]
  node [
    id 526
    label "wykonywa&#263;"
  ]
  node [
    id 527
    label "analizowa&#263;"
  ]
  node [
    id 528
    label "odbiera&#263;"
  ]
  node [
    id 529
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 530
    label "wytwarza&#263;"
  ]
  node [
    id 531
    label "praca"
  ]
  node [
    id 532
    label "rola"
  ]
  node [
    id 533
    label "zabiera&#263;"
  ]
  node [
    id 534
    label "zlecenie"
  ]
  node [
    id 535
    label "odzyskiwa&#263;"
  ]
  node [
    id 536
    label "radio"
  ]
  node [
    id 537
    label "przyjmowa&#263;"
  ]
  node [
    id 538
    label "bra&#263;"
  ]
  node [
    id 539
    label "antena"
  ]
  node [
    id 540
    label "fall"
  ]
  node [
    id 541
    label "liszy&#263;"
  ]
  node [
    id 542
    label "pozbawia&#263;"
  ]
  node [
    id 543
    label "telewizor"
  ]
  node [
    id 544
    label "konfiskowa&#263;"
  ]
  node [
    id 545
    label "deprive"
  ]
  node [
    id 546
    label "accept"
  ]
  node [
    id 547
    label "doznawa&#263;"
  ]
  node [
    id 548
    label "wiedzie&#263;"
  ]
  node [
    id 549
    label "kuma&#263;"
  ]
  node [
    id 550
    label "czu&#263;"
  ]
  node [
    id 551
    label "dziama&#263;"
  ]
  node [
    id 552
    label "match"
  ]
  node [
    id 553
    label "empatia"
  ]
  node [
    id 554
    label "j&#281;zyk"
  ]
  node [
    id 555
    label "see"
  ]
  node [
    id 556
    label "zna&#263;"
  ]
  node [
    id 557
    label "consider"
  ]
  node [
    id 558
    label "badany"
  ]
  node [
    id 559
    label "poddawa&#263;"
  ]
  node [
    id 560
    label "bada&#263;"
  ]
  node [
    id 561
    label "rozpatrywa&#263;"
  ]
  node [
    id 562
    label "po&#322;o&#380;enie"
  ]
  node [
    id 563
    label "punkt"
  ]
  node [
    id 564
    label "pogl&#261;d"
  ]
  node [
    id 565
    label "awansowa&#263;"
  ]
  node [
    id 566
    label "stawia&#263;"
  ]
  node [
    id 567
    label "wakowa&#263;"
  ]
  node [
    id 568
    label "postawi&#263;"
  ]
  node [
    id 569
    label "awansowanie"
  ]
  node [
    id 570
    label "ust&#281;p"
  ]
  node [
    id 571
    label "obiekt_matematyczny"
  ]
  node [
    id 572
    label "plamka"
  ]
  node [
    id 573
    label "stopie&#324;_pisma"
  ]
  node [
    id 574
    label "jednostka"
  ]
  node [
    id 575
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 576
    label "mark"
  ]
  node [
    id 577
    label "chwila"
  ]
  node [
    id 578
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 579
    label "prosta"
  ]
  node [
    id 580
    label "obiekt"
  ]
  node [
    id 581
    label "zapunktowa&#263;"
  ]
  node [
    id 582
    label "podpunkt"
  ]
  node [
    id 583
    label "przestrze&#324;"
  ]
  node [
    id 584
    label "pozycja"
  ]
  node [
    id 585
    label "przenocowanie"
  ]
  node [
    id 586
    label "pora&#380;ka"
  ]
  node [
    id 587
    label "nak&#322;adzenie"
  ]
  node [
    id 588
    label "pouk&#322;adanie"
  ]
  node [
    id 589
    label "zepsucie"
  ]
  node [
    id 590
    label "ustawienie"
  ]
  node [
    id 591
    label "trim"
  ]
  node [
    id 592
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 593
    label "ugoszczenie"
  ]
  node [
    id 594
    label "le&#380;enie"
  ]
  node [
    id 595
    label "adres"
  ]
  node [
    id 596
    label "zbudowanie"
  ]
  node [
    id 597
    label "reading"
  ]
  node [
    id 598
    label "sytuacja"
  ]
  node [
    id 599
    label "zabicie"
  ]
  node [
    id 600
    label "wygranie"
  ]
  node [
    id 601
    label "presentation"
  ]
  node [
    id 602
    label "le&#380;e&#263;"
  ]
  node [
    id 603
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 604
    label "najem"
  ]
  node [
    id 605
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 606
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 607
    label "zak&#322;ad"
  ]
  node [
    id 608
    label "stosunek_pracy"
  ]
  node [
    id 609
    label "benedykty&#324;ski"
  ]
  node [
    id 610
    label "poda&#380;_pracy"
  ]
  node [
    id 611
    label "pracowanie"
  ]
  node [
    id 612
    label "tyrka"
  ]
  node [
    id 613
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 614
    label "zaw&#243;d"
  ]
  node [
    id 615
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 616
    label "tynkarski"
  ]
  node [
    id 617
    label "pracowa&#263;"
  ]
  node [
    id 618
    label "zmiana"
  ]
  node [
    id 619
    label "czynnik_produkcji"
  ]
  node [
    id 620
    label "kierownictwo"
  ]
  node [
    id 621
    label "siedziba"
  ]
  node [
    id 622
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 623
    label "warunek_lokalowy"
  ]
  node [
    id 624
    label "plac"
  ]
  node [
    id 625
    label "location"
  ]
  node [
    id 626
    label "uwaga"
  ]
  node [
    id 627
    label "status"
  ]
  node [
    id 628
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 629
    label "cia&#322;o"
  ]
  node [
    id 630
    label "rz&#261;d"
  ]
  node [
    id 631
    label "teologicznie"
  ]
  node [
    id 632
    label "belief"
  ]
  node [
    id 633
    label "zderzenie_si&#281;"
  ]
  node [
    id 634
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 635
    label "teoria_Arrheniusa"
  ]
  node [
    id 636
    label "zrejterowanie"
  ]
  node [
    id 637
    label "zmobilizowa&#263;"
  ]
  node [
    id 638
    label "dezerter"
  ]
  node [
    id 639
    label "oddzia&#322;_karny"
  ]
  node [
    id 640
    label "rezerwa"
  ]
  node [
    id 641
    label "tabor"
  ]
  node [
    id 642
    label "wermacht"
  ]
  node [
    id 643
    label "cofni&#281;cie"
  ]
  node [
    id 644
    label "fala"
  ]
  node [
    id 645
    label "szko&#322;a"
  ]
  node [
    id 646
    label "korpus"
  ]
  node [
    id 647
    label "soldateska"
  ]
  node [
    id 648
    label "ods&#322;ugiwanie"
  ]
  node [
    id 649
    label "werbowanie_si&#281;"
  ]
  node [
    id 650
    label "zdemobilizowanie"
  ]
  node [
    id 651
    label "oddzia&#322;"
  ]
  node [
    id 652
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 653
    label "s&#322;u&#380;ba"
  ]
  node [
    id 654
    label "or&#281;&#380;"
  ]
  node [
    id 655
    label "Legia_Cudzoziemska"
  ]
  node [
    id 656
    label "Armia_Czerwona"
  ]
  node [
    id 657
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 658
    label "rejterowanie"
  ]
  node [
    id 659
    label "Czerwona_Gwardia"
  ]
  node [
    id 660
    label "si&#322;a"
  ]
  node [
    id 661
    label "zrejterowa&#263;"
  ]
  node [
    id 662
    label "sztabslekarz"
  ]
  node [
    id 663
    label "zmobilizowanie"
  ]
  node [
    id 664
    label "wojo"
  ]
  node [
    id 665
    label "pospolite_ruszenie"
  ]
  node [
    id 666
    label "Eurokorpus"
  ]
  node [
    id 667
    label "mobilizowanie"
  ]
  node [
    id 668
    label "rejterowa&#263;"
  ]
  node [
    id 669
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 670
    label "mobilizowa&#263;"
  ]
  node [
    id 671
    label "Armia_Krajowa"
  ]
  node [
    id 672
    label "obrona"
  ]
  node [
    id 673
    label "dryl"
  ]
  node [
    id 674
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 675
    label "petarda"
  ]
  node [
    id 676
    label "zdemobilizowa&#263;"
  ]
  node [
    id 677
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 678
    label "oddawanie"
  ]
  node [
    id 679
    label "zlecanie"
  ]
  node [
    id 680
    label "ufanie"
  ]
  node [
    id 681
    label "wyznawanie"
  ]
  node [
    id 682
    label "przej&#347;cie"
  ]
  node [
    id 683
    label "przechodzenie"
  ]
  node [
    id 684
    label "przeniesienie"
  ]
  node [
    id 685
    label "promowanie"
  ]
  node [
    id 686
    label "habilitowanie_si&#281;"
  ]
  node [
    id 687
    label "obj&#281;cie"
  ]
  node [
    id 688
    label "obejmowanie"
  ]
  node [
    id 689
    label "kariera"
  ]
  node [
    id 690
    label "przenoszenie"
  ]
  node [
    id 691
    label "pozyskiwanie"
  ]
  node [
    id 692
    label "pozyskanie"
  ]
  node [
    id 693
    label "pi&#281;cie_si&#281;"
  ]
  node [
    id 694
    label "zafundowa&#263;"
  ]
  node [
    id 695
    label "wyda&#263;"
  ]
  node [
    id 696
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 697
    label "uruchomi&#263;"
  ]
  node [
    id 698
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 699
    label "pozostawi&#263;"
  ]
  node [
    id 700
    label "obra&#263;"
  ]
  node [
    id 701
    label "peddle"
  ]
  node [
    id 702
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 703
    label "obstawi&#263;"
  ]
  node [
    id 704
    label "zmieni&#263;"
  ]
  node [
    id 705
    label "post"
  ]
  node [
    id 706
    label "wyznaczy&#263;"
  ]
  node [
    id 707
    label "oceni&#263;"
  ]
  node [
    id 708
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 709
    label "uczyni&#263;"
  ]
  node [
    id 710
    label "znak"
  ]
  node [
    id 711
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 712
    label "wytworzy&#263;"
  ]
  node [
    id 713
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 714
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 715
    label "wskaza&#263;"
  ]
  node [
    id 716
    label "przyzna&#263;"
  ]
  node [
    id 717
    label "wydoby&#263;"
  ]
  node [
    id 718
    label "przedstawi&#263;"
  ]
  node [
    id 719
    label "stawi&#263;"
  ]
  node [
    id 720
    label "pozostawia&#263;"
  ]
  node [
    id 721
    label "czyni&#263;"
  ]
  node [
    id 722
    label "wydawa&#263;"
  ]
  node [
    id 723
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 724
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 725
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 726
    label "przewidywa&#263;"
  ]
  node [
    id 727
    label "przyznawa&#263;"
  ]
  node [
    id 728
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 729
    label "go"
  ]
  node [
    id 730
    label "obstawia&#263;"
  ]
  node [
    id 731
    label "umieszcza&#263;"
  ]
  node [
    id 732
    label "ocenia&#263;"
  ]
  node [
    id 733
    label "zastawia&#263;"
  ]
  node [
    id 734
    label "wskazywa&#263;"
  ]
  node [
    id 735
    label "introduce"
  ]
  node [
    id 736
    label "uruchamia&#263;"
  ]
  node [
    id 737
    label "fundowa&#263;"
  ]
  node [
    id 738
    label "zmienia&#263;"
  ]
  node [
    id 739
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 740
    label "deliver"
  ]
  node [
    id 741
    label "wyznacza&#263;"
  ]
  node [
    id 742
    label "przedstawia&#263;"
  ]
  node [
    id 743
    label "wydobywa&#263;"
  ]
  node [
    id 744
    label "wolny"
  ]
  node [
    id 745
    label "pozyska&#263;"
  ]
  node [
    id 746
    label "obejmowa&#263;"
  ]
  node [
    id 747
    label "pozyskiwa&#263;"
  ]
  node [
    id 748
    label "dawa&#263;_awans"
  ]
  node [
    id 749
    label "przej&#347;&#263;"
  ]
  node [
    id 750
    label "pi&#261;&#263;_si&#281;"
  ]
  node [
    id 751
    label "da&#263;_awans"
  ]
  node [
    id 752
    label "przechodzi&#263;"
  ]
  node [
    id 753
    label "pielenie"
  ]
  node [
    id 754
    label "culture"
  ]
  node [
    id 755
    label "sianie"
  ]
  node [
    id 756
    label "sadzenie"
  ]
  node [
    id 757
    label "oprysk"
  ]
  node [
    id 758
    label "szczepienie"
  ]
  node [
    id 759
    label "orka"
  ]
  node [
    id 760
    label "rolnictwo"
  ]
  node [
    id 761
    label "siew"
  ]
  node [
    id 762
    label "exercise"
  ]
  node [
    id 763
    label "koszenie"
  ]
  node [
    id 764
    label "obrabianie"
  ]
  node [
    id 765
    label "zajmowanie_si&#281;"
  ]
  node [
    id 766
    label "use"
  ]
  node [
    id 767
    label "biotechnika"
  ]
  node [
    id 768
    label "hodowanie"
  ]
  node [
    id 769
    label "dostojnik"
  ]
  node [
    id 770
    label "Goebbels"
  ]
  node [
    id 771
    label "Sto&#322;ypin"
  ]
  node [
    id 772
    label "przybli&#380;enie"
  ]
  node [
    id 773
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 774
    label "kategoria"
  ]
  node [
    id 775
    label "szpaler"
  ]
  node [
    id 776
    label "lon&#380;a"
  ]
  node [
    id 777
    label "uporz&#261;dkowanie"
  ]
  node [
    id 778
    label "instytucja"
  ]
  node [
    id 779
    label "jednostka_systematyczna"
  ]
  node [
    id 780
    label "egzekutywa"
  ]
  node [
    id 781
    label "premier"
  ]
  node [
    id 782
    label "Londyn"
  ]
  node [
    id 783
    label "gabinet_cieni"
  ]
  node [
    id 784
    label "gromada"
  ]
  node [
    id 785
    label "number"
  ]
  node [
    id 786
    label "Konsulat"
  ]
  node [
    id 787
    label "tract"
  ]
  node [
    id 788
    label "klasa"
  ]
  node [
    id 789
    label "w&#322;adza"
  ]
  node [
    id 790
    label "urz&#281;dnik"
  ]
  node [
    id 791
    label "notabl"
  ]
  node [
    id 792
    label "oficja&#322;"
  ]
  node [
    id 793
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 794
    label "informacja"
  ]
  node [
    id 795
    label "zawarto&#347;&#263;"
  ]
  node [
    id 796
    label "ilo&#347;&#263;"
  ]
  node [
    id 797
    label "wn&#281;trze"
  ]
  node [
    id 798
    label "publikacja"
  ]
  node [
    id 799
    label "wiedza"
  ]
  node [
    id 800
    label "doj&#347;cie"
  ]
  node [
    id 801
    label "obiega&#263;"
  ]
  node [
    id 802
    label "powzi&#281;cie"
  ]
  node [
    id 803
    label "obiegni&#281;cie"
  ]
  node [
    id 804
    label "sygna&#322;"
  ]
  node [
    id 805
    label "obieganie"
  ]
  node [
    id 806
    label "powzi&#261;&#263;"
  ]
  node [
    id 807
    label "obiec"
  ]
  node [
    id 808
    label "doj&#347;&#263;"
  ]
  node [
    id 809
    label "mentalno&#347;&#263;"
  ]
  node [
    id 810
    label "superego"
  ]
  node [
    id 811
    label "psychika"
  ]
  node [
    id 812
    label "znaczenie"
  ]
  node [
    id 813
    label "wyraz_pochodny"
  ]
  node [
    id 814
    label "fraza"
  ]
  node [
    id 815
    label "forum"
  ]
  node [
    id 816
    label "topik"
  ]
  node [
    id 817
    label "forma"
  ]
  node [
    id 818
    label "melodia"
  ]
  node [
    id 819
    label "otoczka"
  ]
  node [
    id 820
    label "tenis"
  ]
  node [
    id 821
    label "supply"
  ]
  node [
    id 822
    label "ustawi&#263;"
  ]
  node [
    id 823
    label "siatk&#243;wka"
  ]
  node [
    id 824
    label "zagra&#263;"
  ]
  node [
    id 825
    label "jedzenie"
  ]
  node [
    id 826
    label "poinformowa&#263;"
  ]
  node [
    id 827
    label "nafaszerowa&#263;"
  ]
  node [
    id 828
    label "zaserwowa&#263;"
  ]
  node [
    id 829
    label "pi&#322;ka"
  ]
  node [
    id 830
    label "poprawi&#263;"
  ]
  node [
    id 831
    label "nada&#263;"
  ]
  node [
    id 832
    label "marshal"
  ]
  node [
    id 833
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 834
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 835
    label "zabezpieczy&#263;"
  ]
  node [
    id 836
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 837
    label "zinterpretowa&#263;"
  ]
  node [
    id 838
    label "sk&#322;oni&#263;"
  ]
  node [
    id 839
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 840
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 841
    label "zdecydowa&#263;"
  ]
  node [
    id 842
    label "accommodate"
  ]
  node [
    id 843
    label "ustali&#263;"
  ]
  node [
    id 844
    label "situate"
  ]
  node [
    id 845
    label "inform"
  ]
  node [
    id 846
    label "zakomunikowa&#263;"
  ]
  node [
    id 847
    label "powierzy&#263;"
  ]
  node [
    id 848
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 849
    label "obieca&#263;"
  ]
  node [
    id 850
    label "pozwoli&#263;"
  ]
  node [
    id 851
    label "odst&#261;pi&#263;"
  ]
  node [
    id 852
    label "przywali&#263;"
  ]
  node [
    id 853
    label "wyrzec_si&#281;"
  ]
  node [
    id 854
    label "sztachn&#261;&#263;"
  ]
  node [
    id 855
    label "rap"
  ]
  node [
    id 856
    label "convey"
  ]
  node [
    id 857
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 858
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 859
    label "testify"
  ]
  node [
    id 860
    label "udost&#281;pni&#263;"
  ]
  node [
    id 861
    label "przeznaczy&#263;"
  ]
  node [
    id 862
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 863
    label "picture"
  ]
  node [
    id 864
    label "dress"
  ]
  node [
    id 865
    label "przekaza&#263;"
  ]
  node [
    id 866
    label "doda&#263;"
  ]
  node [
    id 867
    label "play"
  ]
  node [
    id 868
    label "zabrzmie&#263;"
  ]
  node [
    id 869
    label "leave"
  ]
  node [
    id 870
    label "instrument_muzyczny"
  ]
  node [
    id 871
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 872
    label "flare"
  ]
  node [
    id 873
    label "rozegra&#263;"
  ]
  node [
    id 874
    label "zaszczeka&#263;"
  ]
  node [
    id 875
    label "sound"
  ]
  node [
    id 876
    label "represent"
  ]
  node [
    id 877
    label "wykorzysta&#263;"
  ]
  node [
    id 878
    label "zatokowa&#263;"
  ]
  node [
    id 879
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 880
    label "uda&#263;_si&#281;"
  ]
  node [
    id 881
    label "zacz&#261;&#263;"
  ]
  node [
    id 882
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 883
    label "wykona&#263;"
  ]
  node [
    id 884
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 885
    label "typify"
  ]
  node [
    id 886
    label "blok"
  ]
  node [
    id 887
    label "lobowanie"
  ]
  node [
    id 888
    label "&#347;cina&#263;"
  ]
  node [
    id 889
    label "retinopatia"
  ]
  node [
    id 890
    label "podanie"
  ]
  node [
    id 891
    label "plamka_&#380;&#243;&#322;ta"
  ]
  node [
    id 892
    label "cia&#322;o_szkliste"
  ]
  node [
    id 893
    label "&#347;cinanie"
  ]
  node [
    id 894
    label "zeaksantyna"
  ]
  node [
    id 895
    label "podawa&#263;"
  ]
  node [
    id 896
    label "przelobowa&#263;"
  ]
  node [
    id 897
    label "lobowa&#263;"
  ]
  node [
    id 898
    label "dno_oka"
  ]
  node [
    id 899
    label "przelobowanie"
  ]
  node [
    id 900
    label "barwnik_&#347;wiat&#322;oczu&#322;y"
  ]
  node [
    id 901
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 902
    label "&#347;ci&#281;cie"
  ]
  node [
    id 903
    label "podawanie"
  ]
  node [
    id 904
    label "pelota"
  ]
  node [
    id 905
    label "sport_rakietowy"
  ]
  node [
    id 906
    label "wolej"
  ]
  node [
    id 907
    label "supervisor"
  ]
  node [
    id 908
    label "ubrani&#243;wka"
  ]
  node [
    id 909
    label "singlista"
  ]
  node [
    id 910
    label "bekhend"
  ]
  node [
    id 911
    label "forhend"
  ]
  node [
    id 912
    label "p&#243;&#322;wolej"
  ]
  node [
    id 913
    label "pr&#261;&#380;kowany"
  ]
  node [
    id 914
    label "singlowy"
  ]
  node [
    id 915
    label "tkanina_we&#322;niana"
  ]
  node [
    id 916
    label "deblowy"
  ]
  node [
    id 917
    label "tkanina"
  ]
  node [
    id 918
    label "mikst"
  ]
  node [
    id 919
    label "slajs"
  ]
  node [
    id 920
    label "deblista"
  ]
  node [
    id 921
    label "miksista"
  ]
  node [
    id 922
    label "Wimbledon"
  ]
  node [
    id 923
    label "zatruwanie_si&#281;"
  ]
  node [
    id 924
    label "przejadanie_si&#281;"
  ]
  node [
    id 925
    label "szama"
  ]
  node [
    id 926
    label "koryto"
  ]
  node [
    id 927
    label "odpasanie_si&#281;"
  ]
  node [
    id 928
    label "eating"
  ]
  node [
    id 929
    label "jadanie"
  ]
  node [
    id 930
    label "posilenie"
  ]
  node [
    id 931
    label "wpieprzanie"
  ]
  node [
    id 932
    label "wmuszanie"
  ]
  node [
    id 933
    label "wiwenda"
  ]
  node [
    id 934
    label "polowanie"
  ]
  node [
    id 935
    label "ufetowanie_si&#281;"
  ]
  node [
    id 936
    label "wyjadanie"
  ]
  node [
    id 937
    label "smakowanie"
  ]
  node [
    id 938
    label "przejedzenie"
  ]
  node [
    id 939
    label "jad&#322;o"
  ]
  node [
    id 940
    label "mlaskanie"
  ]
  node [
    id 941
    label "papusianie"
  ]
  node [
    id 942
    label "posilanie"
  ]
  node [
    id 943
    label "przejedzenie_si&#281;"
  ]
  node [
    id 944
    label "&#380;arcie"
  ]
  node [
    id 945
    label "odpasienie_si&#281;"
  ]
  node [
    id 946
    label "wyjedzenie"
  ]
  node [
    id 947
    label "przejadanie"
  ]
  node [
    id 948
    label "objadanie"
  ]
  node [
    id 949
    label "przesadzi&#263;"
  ]
  node [
    id 950
    label "nadzia&#263;"
  ]
  node [
    id 951
    label "stuff"
  ]
  node [
    id 952
    label "nijaki"
  ]
  node [
    id 953
    label "nijak"
  ]
  node [
    id 954
    label "niezabawny"
  ]
  node [
    id 955
    label "zwyczajny"
  ]
  node [
    id 956
    label "oboj&#281;tny"
  ]
  node [
    id 957
    label "poszarzenie"
  ]
  node [
    id 958
    label "neutralny"
  ]
  node [
    id 959
    label "szarzenie"
  ]
  node [
    id 960
    label "bezbarwnie"
  ]
  node [
    id 961
    label "nieciekawy"
  ]
  node [
    id 962
    label "rewizja"
  ]
  node [
    id 963
    label "certificate"
  ]
  node [
    id 964
    label "argument"
  ]
  node [
    id 965
    label "act"
  ]
  node [
    id 966
    label "forsing"
  ]
  node [
    id 967
    label "uzasadnienie"
  ]
  node [
    id 968
    label "zapis"
  ]
  node [
    id 969
    label "&#347;wiadectwo"
  ]
  node [
    id 970
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 971
    label "parafa"
  ]
  node [
    id 972
    label "plik"
  ]
  node [
    id 973
    label "raport&#243;wka"
  ]
  node [
    id 974
    label "utw&#243;r"
  ]
  node [
    id 975
    label "record"
  ]
  node [
    id 976
    label "registratura"
  ]
  node [
    id 977
    label "dokumentacja"
  ]
  node [
    id 978
    label "fascyku&#322;"
  ]
  node [
    id 979
    label "artyku&#322;"
  ]
  node [
    id 980
    label "writing"
  ]
  node [
    id 981
    label "sygnatariusz"
  ]
  node [
    id 982
    label "wpadni&#281;cie"
  ]
  node [
    id 983
    label "mienie"
  ]
  node [
    id 984
    label "przyroda"
  ]
  node [
    id 985
    label "wpa&#347;&#263;"
  ]
  node [
    id 986
    label "wpadanie"
  ]
  node [
    id 987
    label "wpada&#263;"
  ]
  node [
    id 988
    label "wyja&#347;nienie"
  ]
  node [
    id 989
    label "apologetyk"
  ]
  node [
    id 990
    label "justyfikacja"
  ]
  node [
    id 991
    label "gossip"
  ]
  node [
    id 992
    label "abstrakcja"
  ]
  node [
    id 993
    label "czas"
  ]
  node [
    id 994
    label "chemikalia"
  ]
  node [
    id 995
    label "substancja"
  ]
  node [
    id 996
    label "parametr"
  ]
  node [
    id 997
    label "operand"
  ]
  node [
    id 998
    label "zmienna"
  ]
  node [
    id 999
    label "argumentacja"
  ]
  node [
    id 1000
    label "matematyka"
  ]
  node [
    id 1001
    label "proces_my&#347;lowy"
  ]
  node [
    id 1002
    label "krytyka"
  ]
  node [
    id 1003
    label "rekurs"
  ]
  node [
    id 1004
    label "checkup"
  ]
  node [
    id 1005
    label "odwo&#322;anie"
  ]
  node [
    id 1006
    label "correction"
  ]
  node [
    id 1007
    label "przegl&#261;d"
  ]
  node [
    id 1008
    label "kipisz"
  ]
  node [
    id 1009
    label "amendment"
  ]
  node [
    id 1010
    label "korekta"
  ]
  node [
    id 1011
    label "Bangladesz"
  ]
  node [
    id 1012
    label "Bengal"
  ]
  node [
    id 1013
    label "explanation"
  ]
  node [
    id 1014
    label "hermeneutyka"
  ]
  node [
    id 1015
    label "wypracowanie"
  ]
  node [
    id 1016
    label "realizacja"
  ]
  node [
    id 1017
    label "interpretation"
  ]
  node [
    id 1018
    label "obja&#347;nienie"
  ]
  node [
    id 1019
    label "fabrication"
  ]
  node [
    id 1020
    label "scheduling"
  ]
  node [
    id 1021
    label "proces"
  ]
  node [
    id 1022
    label "dzie&#322;o"
  ]
  node [
    id 1023
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1024
    label "kreacja"
  ]
  node [
    id 1025
    label "monta&#380;"
  ]
  node [
    id 1026
    label "postprodukcja"
  ]
  node [
    id 1027
    label "performance"
  ]
  node [
    id 1028
    label "draft"
  ]
  node [
    id 1029
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1030
    label "papier_kancelaryjny"
  ]
  node [
    id 1031
    label "p&#322;&#243;d"
  ]
  node [
    id 1032
    label "remark"
  ]
  node [
    id 1033
    label "zrozumia&#322;y"
  ]
  node [
    id 1034
    label "przedstawienie"
  ]
  node [
    id 1035
    label "poinformowanie"
  ]
  node [
    id 1036
    label "&#347;rodowisko"
  ]
  node [
    id 1037
    label "odniesienie"
  ]
  node [
    id 1038
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1039
    label "otoczenie"
  ]
  node [
    id 1040
    label "causal_agent"
  ]
  node [
    id 1041
    label "context"
  ]
  node [
    id 1042
    label "warunki"
  ]
  node [
    id 1043
    label "fragment"
  ]
  node [
    id 1044
    label "hermeneutics"
  ]
  node [
    id 1045
    label "legalize"
  ]
  node [
    id 1046
    label "proportion"
  ]
  node [
    id 1047
    label "wielko&#347;&#263;"
  ]
  node [
    id 1048
    label "continence"
  ]
  node [
    id 1049
    label "supremum"
  ]
  node [
    id 1050
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1051
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1052
    label "przeliczy&#263;"
  ]
  node [
    id 1053
    label "rzut"
  ]
  node [
    id 1054
    label "odwiedziny"
  ]
  node [
    id 1055
    label "liczba"
  ]
  node [
    id 1056
    label "granica"
  ]
  node [
    id 1057
    label "zakres"
  ]
  node [
    id 1058
    label "przeliczanie"
  ]
  node [
    id 1059
    label "dymensja"
  ]
  node [
    id 1060
    label "funkcja"
  ]
  node [
    id 1061
    label "przelicza&#263;"
  ]
  node [
    id 1062
    label "infimum"
  ]
  node [
    id 1063
    label "przeliczenie"
  ]
  node [
    id 1064
    label "czyn"
  ]
  node [
    id 1065
    label "addytywno&#347;&#263;"
  ]
  node [
    id 1066
    label "function"
  ]
  node [
    id 1067
    label "zastosowanie"
  ]
  node [
    id 1068
    label "funkcjonowanie"
  ]
  node [
    id 1069
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 1070
    label "cel"
  ]
  node [
    id 1071
    label "przeciwdziedzina"
  ]
  node [
    id 1072
    label "sfera"
  ]
  node [
    id 1073
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 1074
    label "podzakres"
  ]
  node [
    id 1075
    label "desygnat"
  ]
  node [
    id 1076
    label "circle"
  ]
  node [
    id 1077
    label "skumanie"
  ]
  node [
    id 1078
    label "orientacja"
  ]
  node [
    id 1079
    label "zorientowanie"
  ]
  node [
    id 1080
    label "teoria"
  ]
  node [
    id 1081
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1082
    label "clasp"
  ]
  node [
    id 1083
    label "przem&#243;wienie"
  ]
  node [
    id 1084
    label "rozmiar"
  ]
  node [
    id 1085
    label "rzadko&#347;&#263;"
  ]
  node [
    id 1086
    label "zaleta"
  ]
  node [
    id 1087
    label "measure"
  ]
  node [
    id 1088
    label "opinia"
  ]
  node [
    id 1089
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 1090
    label "property"
  ]
  node [
    id 1091
    label "go&#347;&#263;"
  ]
  node [
    id 1092
    label "pobyt"
  ]
  node [
    id 1093
    label "odwiedzalno&#347;&#263;"
  ]
  node [
    id 1094
    label "charakterystyka"
  ]
  node [
    id 1095
    label "m&#322;ot"
  ]
  node [
    id 1096
    label "drzewo"
  ]
  node [
    id 1097
    label "attribute"
  ]
  node [
    id 1098
    label "marka"
  ]
  node [
    id 1099
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1100
    label "Ural"
  ]
  node [
    id 1101
    label "end"
  ]
  node [
    id 1102
    label "pu&#322;ap"
  ]
  node [
    id 1103
    label "koniec"
  ]
  node [
    id 1104
    label "granice"
  ]
  node [
    id 1105
    label "frontier"
  ]
  node [
    id 1106
    label "wymienienie"
  ]
  node [
    id 1107
    label "przerachowanie"
  ]
  node [
    id 1108
    label "skontrolowanie"
  ]
  node [
    id 1109
    label "count"
  ]
  node [
    id 1110
    label "przyswoi&#263;"
  ]
  node [
    id 1111
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1112
    label "one"
  ]
  node [
    id 1113
    label "ewoluowanie"
  ]
  node [
    id 1114
    label "przyswajanie"
  ]
  node [
    id 1115
    label "wyewoluowanie"
  ]
  node [
    id 1116
    label "reakcja"
  ]
  node [
    id 1117
    label "wyewoluowa&#263;"
  ]
  node [
    id 1118
    label "ewoluowa&#263;"
  ]
  node [
    id 1119
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1120
    label "liczba_naturalna"
  ]
  node [
    id 1121
    label "czynnik_biotyczny"
  ]
  node [
    id 1122
    label "g&#322;owa"
  ]
  node [
    id 1123
    label "figura"
  ]
  node [
    id 1124
    label "individual"
  ]
  node [
    id 1125
    label "portrecista"
  ]
  node [
    id 1126
    label "przyswaja&#263;"
  ]
  node [
    id 1127
    label "przyswojenie"
  ]
  node [
    id 1128
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 1129
    label "profanum"
  ]
  node [
    id 1130
    label "mikrokosmos"
  ]
  node [
    id 1131
    label "starzenie_si&#281;"
  ]
  node [
    id 1132
    label "duch"
  ]
  node [
    id 1133
    label "osoba"
  ]
  node [
    id 1134
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1135
    label "antropochoria"
  ]
  node [
    id 1136
    label "homo_sapiens"
  ]
  node [
    id 1137
    label "sprawdza&#263;"
  ]
  node [
    id 1138
    label "przerachowywa&#263;"
  ]
  node [
    id 1139
    label "ograniczenie"
  ]
  node [
    id 1140
    label "armia"
  ]
  node [
    id 1141
    label "nawr&#243;t_choroby"
  ]
  node [
    id 1142
    label "potomstwo"
  ]
  node [
    id 1143
    label "odwzorowanie"
  ]
  node [
    id 1144
    label "rysunek"
  ]
  node [
    id 1145
    label "scene"
  ]
  node [
    id 1146
    label "throw"
  ]
  node [
    id 1147
    label "float"
  ]
  node [
    id 1148
    label "projection"
  ]
  node [
    id 1149
    label "injection"
  ]
  node [
    id 1150
    label "blow"
  ]
  node [
    id 1151
    label "k&#322;ad"
  ]
  node [
    id 1152
    label "mold"
  ]
  node [
    id 1153
    label "zast&#281;powanie"
  ]
  node [
    id 1154
    label "przerachowywanie"
  ]
  node [
    id 1155
    label "rachunek_operatorowy"
  ]
  node [
    id 1156
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 1157
    label "kryptologia"
  ]
  node [
    id 1158
    label "logicyzm"
  ]
  node [
    id 1159
    label "logika"
  ]
  node [
    id 1160
    label "matematyka_czysta"
  ]
  node [
    id 1161
    label "modelowanie_matematyczne"
  ]
  node [
    id 1162
    label "matma"
  ]
  node [
    id 1163
    label "teoria_katastrof"
  ]
  node [
    id 1164
    label "fizyka_matematyczna"
  ]
  node [
    id 1165
    label "teoria_graf&#243;w"
  ]
  node [
    id 1166
    label "rachunki"
  ]
  node [
    id 1167
    label "topologia_algebraiczna"
  ]
  node [
    id 1168
    label "matematyka_stosowana"
  ]
  node [
    id 1169
    label "sprawdzi&#263;"
  ]
  node [
    id 1170
    label "change"
  ]
  node [
    id 1171
    label "przerachowa&#263;"
  ]
  node [
    id 1172
    label "pierwiastek"
  ]
  node [
    id 1173
    label "kwadrat_magiczny"
  ]
  node [
    id 1174
    label "part"
  ]
  node [
    id 1175
    label "masztab"
  ]
  node [
    id 1176
    label "kreska"
  ]
  node [
    id 1177
    label "podzia&#322;ka"
  ]
  node [
    id 1178
    label "zero"
  ]
  node [
    id 1179
    label "interwa&#322;"
  ]
  node [
    id 1180
    label "przymiar"
  ]
  node [
    id 1181
    label "dominanta"
  ]
  node [
    id 1182
    label "tetrachord"
  ]
  node [
    id 1183
    label "scale"
  ]
  node [
    id 1184
    label "przedzia&#322;"
  ]
  node [
    id 1185
    label "proporcja"
  ]
  node [
    id 1186
    label "rejestr"
  ]
  node [
    id 1187
    label "subdominanta"
  ]
  node [
    id 1188
    label "free"
  ]
  node [
    id 1189
    label "stwierdzi&#263;"
  ]
  node [
    id 1190
    label "assent"
  ]
  node [
    id 1191
    label "rede"
  ]
  node [
    id 1192
    label "visualize"
  ]
  node [
    id 1193
    label "okre&#347;li&#263;"
  ]
  node [
    id 1194
    label "wystawi&#263;"
  ]
  node [
    id 1195
    label "evaluate"
  ]
  node [
    id 1196
    label "znale&#378;&#263;"
  ]
  node [
    id 1197
    label "pomy&#347;le&#263;"
  ]
  node [
    id 1198
    label "powiedzie&#263;"
  ]
  node [
    id 1199
    label "oznajmi&#263;"
  ]
  node [
    id 1200
    label "declare"
  ]
  node [
    id 1201
    label "pewny"
  ]
  node [
    id 1202
    label "wiarygodnie"
  ]
  node [
    id 1203
    label "uwiarygodnienie"
  ]
  node [
    id 1204
    label "uwiarygodnianie"
  ]
  node [
    id 1205
    label "mo&#380;liwy"
  ]
  node [
    id 1206
    label "bezpieczny"
  ]
  node [
    id 1207
    label "spokojny"
  ]
  node [
    id 1208
    label "pewnie"
  ]
  node [
    id 1209
    label "upewnianie_si&#281;"
  ]
  node [
    id 1210
    label "wierzenie"
  ]
  node [
    id 1211
    label "upewnienie_si&#281;"
  ]
  node [
    id 1212
    label "credibly"
  ]
  node [
    id 1213
    label "believably"
  ]
  node [
    id 1214
    label "certification"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 368
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 50
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 326
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 370
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 325
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 258
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 390
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 563
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 474
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 383
  ]
  edge [
    source 15
    target 491
  ]
  edge [
    source 15
    target 50
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 371
  ]
  edge [
    source 15
    target 372
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 375
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 379
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 381
  ]
  edge [
    source 15
    target 382
  ]
  edge [
    source 15
    target 384
  ]
  edge [
    source 15
    target 817
  ]
  edge [
    source 15
    target 386
  ]
  edge [
    source 15
    target 387
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 391
  ]
  edge [
    source 15
    target 392
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 241
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 525
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 237
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 258
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 227
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 532
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 242
  ]
  edge [
    source 16
    target 239
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 16
    target 858
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 16
    target 860
  ]
  edge [
    source 16
    target 861
  ]
  edge [
    source 16
    target 862
  ]
  edge [
    source 16
    target 863
  ]
  edge [
    source 16
    target 864
  ]
  edge [
    source 16
    target 279
  ]
  edge [
    source 16
    target 281
  ]
  edge [
    source 16
    target 865
  ]
  edge [
    source 16
    target 866
  ]
  edge [
    source 16
    target 261
  ]
  edge [
    source 16
    target 867
  ]
  edge [
    source 16
    target 868
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 871
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 16
    target 873
  ]
  edge [
    source 16
    target 874
  ]
  edge [
    source 16
    target 875
  ]
  edge [
    source 16
    target 876
  ]
  edge [
    source 16
    target 877
  ]
  edge [
    source 16
    target 878
  ]
  edge [
    source 16
    target 879
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 881
  ]
  edge [
    source 16
    target 882
  ]
  edge [
    source 16
    target 883
  ]
  edge [
    source 16
    target 884
  ]
  edge [
    source 16
    target 885
  ]
  edge [
    source 16
    target 886
  ]
  edge [
    source 16
    target 887
  ]
  edge [
    source 16
    target 888
  ]
  edge [
    source 16
    target 889
  ]
  edge [
    source 16
    target 890
  ]
  edge [
    source 16
    target 891
  ]
  edge [
    source 16
    target 892
  ]
  edge [
    source 16
    target 893
  ]
  edge [
    source 16
    target 894
  ]
  edge [
    source 16
    target 895
  ]
  edge [
    source 16
    target 896
  ]
  edge [
    source 16
    target 897
  ]
  edge [
    source 16
    target 898
  ]
  edge [
    source 16
    target 899
  ]
  edge [
    source 16
    target 900
  ]
  edge [
    source 16
    target 901
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 903
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 16
    target 930
  ]
  edge [
    source 16
    target 931
  ]
  edge [
    source 16
    target 932
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 933
  ]
  edge [
    source 16
    target 934
  ]
  edge [
    source 16
    target 935
  ]
  edge [
    source 16
    target 936
  ]
  edge [
    source 16
    target 937
  ]
  edge [
    source 16
    target 938
  ]
  edge [
    source 16
    target 939
  ]
  edge [
    source 16
    target 940
  ]
  edge [
    source 16
    target 941
  ]
  edge [
    source 16
    target 942
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 943
  ]
  edge [
    source 16
    target 944
  ]
  edge [
    source 16
    target 945
  ]
  edge [
    source 16
    target 946
  ]
  edge [
    source 16
    target 947
  ]
  edge [
    source 16
    target 948
  ]
  edge [
    source 16
    target 949
  ]
  edge [
    source 16
    target 950
  ]
  edge [
    source 16
    target 951
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 438
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 425
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 120
  ]
  edge [
    source 18
    target 308
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 390
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 389
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 794
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 563
  ]
  edge [
    source 18
    target 368
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 70
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 432
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 18
    target 1002
  ]
  edge [
    source 18
    target 1003
  ]
  edge [
    source 18
    target 1004
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 1005
  ]
  edge [
    source 18
    target 1006
  ]
  edge [
    source 18
    target 1007
  ]
  edge [
    source 18
    target 1008
  ]
  edge [
    source 18
    target 1009
  ]
  edge [
    source 18
    target 618
  ]
  edge [
    source 18
    target 1010
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1013
  ]
  edge [
    source 20
    target 1014
  ]
  edge [
    source 20
    target 1015
  ]
  edge [
    source 20
    target 424
  ]
  edge [
    source 20
    target 57
  ]
  edge [
    source 20
    target 425
  ]
  edge [
    source 20
    target 1016
  ]
  edge [
    source 20
    target 1017
  ]
  edge [
    source 20
    target 1018
  ]
  edge [
    source 20
    target 1019
  ]
  edge [
    source 20
    target 1020
  ]
  edge [
    source 20
    target 431
  ]
  edge [
    source 20
    target 1021
  ]
  edge [
    source 20
    target 1022
  ]
  edge [
    source 20
    target 1023
  ]
  edge [
    source 20
    target 1024
  ]
  edge [
    source 20
    target 1025
  ]
  edge [
    source 20
    target 1026
  ]
  edge [
    source 20
    target 1027
  ]
  edge [
    source 20
    target 464
  ]
  edge [
    source 20
    target 465
  ]
  edge [
    source 20
    target 106
  ]
  edge [
    source 20
    target 466
  ]
  edge [
    source 20
    target 467
  ]
  edge [
    source 20
    target 69
  ]
  edge [
    source 20
    target 70
  ]
  edge [
    source 20
    target 71
  ]
  edge [
    source 20
    target 73
  ]
  edge [
    source 20
    target 72
  ]
  edge [
    source 20
    target 75
  ]
  edge [
    source 20
    target 76
  ]
  edge [
    source 20
    target 74
  ]
  edge [
    source 20
    target 77
  ]
  edge [
    source 20
    target 78
  ]
  edge [
    source 20
    target 79
  ]
  edge [
    source 20
    target 80
  ]
  edge [
    source 20
    target 81
  ]
  edge [
    source 20
    target 82
  ]
  edge [
    source 20
    target 83
  ]
  edge [
    source 20
    target 84
  ]
  edge [
    source 20
    target 85
  ]
  edge [
    source 20
    target 86
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 1028
  ]
  edge [
    source 20
    target 1029
  ]
  edge [
    source 20
    target 1030
  ]
  edge [
    source 20
    target 308
  ]
  edge [
    source 20
    target 1031
  ]
  edge [
    source 20
    target 111
  ]
  edge [
    source 20
    target 1032
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 1033
  ]
  edge [
    source 20
    target 1034
  ]
  edge [
    source 20
    target 794
  ]
  edge [
    source 20
    target 1035
  ]
  edge [
    source 20
    target 1036
  ]
  edge [
    source 20
    target 1037
  ]
  edge [
    source 20
    target 1038
  ]
  edge [
    source 20
    target 1039
  ]
  edge [
    source 20
    target 316
  ]
  edge [
    source 20
    target 1040
  ]
  edge [
    source 20
    target 1041
  ]
  edge [
    source 20
    target 1042
  ]
  edge [
    source 20
    target 1043
  ]
  edge [
    source 20
    target 645
  ]
  edge [
    source 20
    target 1044
  ]
  edge [
    source 21
    target 237
  ]
  edge [
    source 21
    target 1045
  ]
  edge [
    source 21
    target 281
  ]
  edge [
    source 21
    target 965
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1046
  ]
  edge [
    source 22
    target 178
  ]
  edge [
    source 22
    target 1047
  ]
  edge [
    source 22
    target 477
  ]
  edge [
    source 22
    target 1048
  ]
  edge [
    source 22
    target 1049
  ]
  edge [
    source 22
    target 491
  ]
  edge [
    source 22
    target 493
  ]
  edge [
    source 22
    target 1050
  ]
  edge [
    source 22
    target 1051
  ]
  edge [
    source 22
    target 574
  ]
  edge [
    source 22
    target 1052
  ]
  edge [
    source 22
    target 1000
  ]
  edge [
    source 22
    target 1053
  ]
  edge [
    source 22
    target 1054
  ]
  edge [
    source 22
    target 1055
  ]
  edge [
    source 22
    target 1056
  ]
  edge [
    source 22
    target 1057
  ]
  edge [
    source 22
    target 623
  ]
  edge [
    source 22
    target 796
  ]
  edge [
    source 22
    target 1058
  ]
  edge [
    source 22
    target 1059
  ]
  edge [
    source 22
    target 1060
  ]
  edge [
    source 22
    target 1061
  ]
  edge [
    source 22
    target 1062
  ]
  edge [
    source 22
    target 1063
  ]
  edge [
    source 22
    target 1064
  ]
  edge [
    source 22
    target 1065
  ]
  edge [
    source 22
    target 1066
  ]
  edge [
    source 22
    target 1067
  ]
  edge [
    source 22
    target 1068
  ]
  edge [
    source 22
    target 531
  ]
  edge [
    source 22
    target 1069
  ]
  edge [
    source 22
    target 110
  ]
  edge [
    source 22
    target 1070
  ]
  edge [
    source 22
    target 435
  ]
  edge [
    source 22
    target 1071
  ]
  edge [
    source 22
    target 565
  ]
  edge [
    source 22
    target 566
  ]
  edge [
    source 22
    target 567
  ]
  edge [
    source 22
    target 812
  ]
  edge [
    source 22
    target 568
  ]
  edge [
    source 22
    target 569
  ]
  edge [
    source 22
    target 1072
  ]
  edge [
    source 22
    target 106
  ]
  edge [
    source 22
    target 1073
  ]
  edge [
    source 22
    target 1074
  ]
  edge [
    source 22
    target 1075
  ]
  edge [
    source 22
    target 1076
  ]
  edge [
    source 22
    target 69
  ]
  edge [
    source 22
    target 1077
  ]
  edge [
    source 22
    target 1078
  ]
  edge [
    source 22
    target 425
  ]
  edge [
    source 22
    target 1079
  ]
  edge [
    source 22
    target 1080
  ]
  edge [
    source 22
    target 1081
  ]
  edge [
    source 22
    target 1082
  ]
  edge [
    source 22
    target 817
  ]
  edge [
    source 22
    target 1083
  ]
  edge [
    source 22
    target 1084
  ]
  edge [
    source 22
    target 1085
  ]
  edge [
    source 22
    target 1086
  ]
  edge [
    source 22
    target 1087
  ]
  edge [
    source 22
    target 1088
  ]
  edge [
    source 22
    target 1089
  ]
  edge [
    source 22
    target 445
  ]
  edge [
    source 22
    target 446
  ]
  edge [
    source 22
    target 1090
  ]
  edge [
    source 22
    target 1091
  ]
  edge [
    source 22
    target 1092
  ]
  edge [
    source 22
    target 1093
  ]
  edge [
    source 22
    target 1094
  ]
  edge [
    source 22
    target 1095
  ]
  edge [
    source 22
    target 710
  ]
  edge [
    source 22
    target 1096
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 1097
  ]
  edge [
    source 22
    target 1098
  ]
  edge [
    source 22
    target 682
  ]
  edge [
    source 22
    target 362
  ]
  edge [
    source 22
    target 1099
  ]
  edge [
    source 22
    target 1100
  ]
  edge [
    source 22
    target 1101
  ]
  edge [
    source 22
    target 1102
  ]
  edge [
    source 22
    target 1103
  ]
  edge [
    source 22
    target 1104
  ]
  edge [
    source 22
    target 1105
  ]
  edge [
    source 22
    target 1106
  ]
  edge [
    source 22
    target 1107
  ]
  edge [
    source 22
    target 1108
  ]
  edge [
    source 22
    target 1109
  ]
  edge [
    source 22
    target 1110
  ]
  edge [
    source 22
    target 1111
  ]
  edge [
    source 22
    target 1112
  ]
  edge [
    source 22
    target 1113
  ]
  edge [
    source 22
    target 481
  ]
  edge [
    source 22
    target 1114
  ]
  edge [
    source 22
    target 1115
  ]
  edge [
    source 22
    target 1116
  ]
  edge [
    source 22
    target 1117
  ]
  edge [
    source 22
    target 1118
  ]
  edge [
    source 22
    target 1119
  ]
  edge [
    source 22
    target 1120
  ]
  edge [
    source 22
    target 1121
  ]
  edge [
    source 22
    target 1122
  ]
  edge [
    source 22
    target 1123
  ]
  edge [
    source 22
    target 1124
  ]
  edge [
    source 22
    target 1125
  ]
  edge [
    source 22
    target 580
  ]
  edge [
    source 22
    target 1126
  ]
  edge [
    source 22
    target 1127
  ]
  edge [
    source 22
    target 1128
  ]
  edge [
    source 22
    target 1129
  ]
  edge [
    source 22
    target 1130
  ]
  edge [
    source 22
    target 1131
  ]
  edge [
    source 22
    target 1132
  ]
  edge [
    source 22
    target 1133
  ]
  edge [
    source 22
    target 1134
  ]
  edge [
    source 22
    target 1135
  ]
  edge [
    source 22
    target 1136
  ]
  edge [
    source 22
    target 370
  ]
  edge [
    source 22
    target 1137
  ]
  edge [
    source 22
    target 738
  ]
  edge [
    source 22
    target 1138
  ]
  edge [
    source 22
    target 1139
  ]
  edge [
    source 22
    target 1140
  ]
  edge [
    source 22
    target 1141
  ]
  edge [
    source 22
    target 1142
  ]
  edge [
    source 22
    target 1143
  ]
  edge [
    source 22
    target 1144
  ]
  edge [
    source 22
    target 1145
  ]
  edge [
    source 22
    target 1146
  ]
  edge [
    source 22
    target 1147
  ]
  edge [
    source 22
    target 563
  ]
  edge [
    source 22
    target 1148
  ]
  edge [
    source 22
    target 1149
  ]
  edge [
    source 22
    target 1150
  ]
  edge [
    source 22
    target 320
  ]
  edge [
    source 22
    target 507
  ]
  edge [
    source 22
    target 1151
  ]
  edge [
    source 22
    target 1152
  ]
  edge [
    source 22
    target 66
  ]
  edge [
    source 22
    target 1153
  ]
  edge [
    source 22
    target 1154
  ]
  edge [
    source 22
    target 1155
  ]
  edge [
    source 22
    target 308
  ]
  edge [
    source 22
    target 1156
  ]
  edge [
    source 22
    target 1157
  ]
  edge [
    source 22
    target 1158
  ]
  edge [
    source 22
    target 1159
  ]
  edge [
    source 22
    target 1160
  ]
  edge [
    source 22
    target 966
  ]
  edge [
    source 22
    target 1161
  ]
  edge [
    source 22
    target 1162
  ]
  edge [
    source 22
    target 1163
  ]
  edge [
    source 22
    target 349
  ]
  edge [
    source 22
    target 1164
  ]
  edge [
    source 22
    target 1165
  ]
  edge [
    source 22
    target 1166
  ]
  edge [
    source 22
    target 1167
  ]
  edge [
    source 22
    target 1168
  ]
  edge [
    source 22
    target 1169
  ]
  edge [
    source 22
    target 1170
  ]
  edge [
    source 22
    target 1171
  ]
  edge [
    source 22
    target 704
  ]
  edge [
    source 22
    target 774
  ]
  edge [
    source 22
    target 1172
  ]
  edge [
    source 22
    target 103
  ]
  edge [
    source 22
    target 785
  ]
  edge [
    source 22
    target 492
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 1173
  ]
  edge [
    source 22
    target 496
  ]
  edge [
    source 22
    target 1174
  ]
  edge [
    source 22
    target 1175
  ]
  edge [
    source 22
    target 1176
  ]
  edge [
    source 22
    target 1177
  ]
  edge [
    source 22
    target 1178
  ]
  edge [
    source 22
    target 1179
  ]
  edge [
    source 22
    target 1180
  ]
  edge [
    source 22
    target 329
  ]
  edge [
    source 22
    target 1181
  ]
  edge [
    source 22
    target 1182
  ]
  edge [
    source 22
    target 1183
  ]
  edge [
    source 22
    target 1184
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 1186
  ]
  edge [
    source 22
    target 1187
  ]
  edge [
    source 22
    target 996
  ]
  edge [
    source 22
    target 474
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1188
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 707
  ]
  edge [
    source 24
    target 716
  ]
  edge [
    source 24
    target 1189
  ]
  edge [
    source 24
    target 1190
  ]
  edge [
    source 24
    target 1191
  ]
  edge [
    source 24
    target 555
  ]
  edge [
    source 24
    target 1192
  ]
  edge [
    source 24
    target 1193
  ]
  edge [
    source 24
    target 239
  ]
  edge [
    source 24
    target 1194
  ]
  edge [
    source 24
    target 1195
  ]
  edge [
    source 24
    target 1196
  ]
  edge [
    source 24
    target 1197
  ]
  edge [
    source 24
    target 859
  ]
  edge [
    source 24
    target 1198
  ]
  edge [
    source 24
    target 1199
  ]
  edge [
    source 24
    target 1200
  ]
  edge [
    source 24
    target 831
  ]
  edge [
    source 24
    target 241
  ]
  edge [
    source 24
    target 525
  ]
  edge [
    source 24
    target 850
  ]
  edge [
    source 25
    target 1201
  ]
  edge [
    source 25
    target 1202
  ]
  edge [
    source 25
    target 1203
  ]
  edge [
    source 25
    target 1204
  ]
  edge [
    source 25
    target 1205
  ]
  edge [
    source 25
    target 1206
  ]
  edge [
    source 25
    target 1207
  ]
  edge [
    source 25
    target 1208
  ]
  edge [
    source 25
    target 1209
  ]
  edge [
    source 25
    target 680
  ]
  edge [
    source 25
    target 1210
  ]
  edge [
    source 25
    target 1211
  ]
  edge [
    source 25
    target 161
  ]
  edge [
    source 25
    target 1212
  ]
  edge [
    source 25
    target 1213
  ]
  edge [
    source 25
    target 117
  ]
  edge [
    source 25
    target 332
  ]
  edge [
    source 25
    target 1214
  ]
]
