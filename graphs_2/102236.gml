graph [
  node [
    id 0
    label "op&#322;aca&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "sk&#322;adka"
    origin "text"
  ]
  node [
    id 3
    label "ubezpieczenie"
    origin "text"
  ]
  node [
    id 4
    label "emerytalny"
    origin "text"
  ]
  node [
    id 5
    label "rentowy"
    origin "text"
  ]
  node [
    id 6
    label "osoba"
    origin "text"
  ]
  node [
    id 7
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 8
    label "mowa"
    origin "text"
  ]
  node [
    id 9
    label "usta"
    origin "text"
  ]
  node [
    id 10
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 11
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 12
    label "z&#322;o&#380;enie"
    origin "text"
  ]
  node [
    id 13
    label "wniosek"
    origin "text"
  ]
  node [
    id 14
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 15
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 16
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 17
    label "lata"
    origin "text"
  ]
  node [
    id 18
    label "okres"
    origin "text"
  ]
  node [
    id 19
    label "sk&#322;adkowy"
    origin "text"
  ]
  node [
    id 20
    label "niesk&#322;adkowy"
    origin "text"
  ]
  node [
    id 21
    label "wynosi&#263;"
    origin "text"
  ]
  node [
    id 22
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 23
    label "legitymowa&#263;"
    origin "text"
  ]
  node [
    id 24
    label "tym"
    origin "text"
  ]
  node [
    id 25
    label "wymiar"
    origin "text"
  ]
  node [
    id 26
    label "przypadek"
    origin "text"
  ]
  node [
    id 27
    label "kobieta"
    origin "text"
  ]
  node [
    id 28
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 29
    label "give"
  ]
  node [
    id 30
    label "p&#322;aci&#263;"
  ]
  node [
    id 31
    label "finance"
  ]
  node [
    id 32
    label "osi&#261;ga&#263;"
  ]
  node [
    id 33
    label "wydawa&#263;"
  ]
  node [
    id 34
    label "pay"
  ]
  node [
    id 35
    label "buli&#263;"
  ]
  node [
    id 36
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 37
    label "uzyskiwa&#263;"
  ]
  node [
    id 38
    label "dociera&#263;"
  ]
  node [
    id 39
    label "mark"
  ]
  node [
    id 40
    label "get"
  ]
  node [
    id 41
    label "&#243;semka"
  ]
  node [
    id 42
    label "czw&#243;rka"
  ]
  node [
    id 43
    label "sk&#322;ada&#263;_si&#281;"
  ]
  node [
    id 44
    label "z&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 45
    label "zbi&#243;rka"
  ]
  node [
    id 46
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 47
    label "arkusz"
  ]
  node [
    id 48
    label "fee"
  ]
  node [
    id 49
    label "kwota"
  ]
  node [
    id 50
    label "uregulowa&#263;"
  ]
  node [
    id 51
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 52
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 53
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 54
    label "p&#322;at"
  ]
  node [
    id 55
    label "p&#243;&#322;arkusz"
  ]
  node [
    id 56
    label "kwestarz"
  ]
  node [
    id 57
    label "kwestowanie"
  ]
  node [
    id 58
    label "apel"
  ]
  node [
    id 59
    label "recoil"
  ]
  node [
    id 60
    label "collection"
  ]
  node [
    id 61
    label "spotkanie"
  ]
  node [
    id 62
    label "koszyk&#243;wka"
  ]
  node [
    id 63
    label "chwyt"
  ]
  node [
    id 64
    label "czynno&#347;&#263;"
  ]
  node [
    id 65
    label "toto-lotek"
  ]
  node [
    id 66
    label "trafienie"
  ]
  node [
    id 67
    label "zbi&#243;r"
  ]
  node [
    id 68
    label "arkusz_drukarski"
  ]
  node [
    id 69
    label "&#322;&#243;dka"
  ]
  node [
    id 70
    label "four"
  ]
  node [
    id 71
    label "&#263;wiartka"
  ]
  node [
    id 72
    label "hotel"
  ]
  node [
    id 73
    label "cyfra"
  ]
  node [
    id 74
    label "pok&#243;j"
  ]
  node [
    id 75
    label "stopie&#324;"
  ]
  node [
    id 76
    label "obiekt"
  ]
  node [
    id 77
    label "minialbum"
  ]
  node [
    id 78
    label "osada"
  ]
  node [
    id 79
    label "p&#322;yta_winylowa"
  ]
  node [
    id 80
    label "blotka"
  ]
  node [
    id 81
    label "zaprz&#281;g"
  ]
  node [
    id 82
    label "przedtrzonowiec"
  ]
  node [
    id 83
    label "przyrz&#261;d_asekuracyjny"
  ]
  node [
    id 84
    label "&#322;&#243;d&#378;_regatowa"
  ]
  node [
    id 85
    label "nuta"
  ]
  node [
    id 86
    label "kszta&#322;t"
  ]
  node [
    id 87
    label "trzonowiec"
  ]
  node [
    id 88
    label "przegub"
  ]
  node [
    id 89
    label "w&#281;ze&#322;"
  ]
  node [
    id 90
    label "&#243;sma_cz&#281;&#347;&#263;"
  ]
  node [
    id 91
    label "eight"
  ]
  node [
    id 92
    label "eighth"
  ]
  node [
    id 93
    label "figura"
  ]
  node [
    id 94
    label "cepy"
  ]
  node [
    id 95
    label "bilard"
  ]
  node [
    id 96
    label "kapica"
  ]
  node [
    id 97
    label "format_arkusza"
  ]
  node [
    id 98
    label "wisdom_tooth"
  ]
  node [
    id 99
    label "&#322;&#243;d&#378;_wios&#322;owa"
  ]
  node [
    id 100
    label "op&#322;ata"
  ]
  node [
    id 101
    label "ubezpieczalnia"
  ]
  node [
    id 102
    label "insurance"
  ]
  node [
    id 103
    label "cover"
  ]
  node [
    id 104
    label "suma_ubezpieczenia"
  ]
  node [
    id 105
    label "screen"
  ]
  node [
    id 106
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 107
    label "franszyza"
  ]
  node [
    id 108
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 109
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 110
    label "oddzia&#322;"
  ]
  node [
    id 111
    label "zapewnienie"
  ]
  node [
    id 112
    label "przyznanie"
  ]
  node [
    id 113
    label "ochrona"
  ]
  node [
    id 114
    label "umowa"
  ]
  node [
    id 115
    label "uchronienie"
  ]
  node [
    id 116
    label "automatyczny"
  ]
  node [
    id 117
    label "obietnica"
  ]
  node [
    id 118
    label "za&#347;wiadczenie"
  ]
  node [
    id 119
    label "spowodowanie"
  ]
  node [
    id 120
    label "zapowied&#378;"
  ]
  node [
    id 121
    label "statement"
  ]
  node [
    id 122
    label "proposition"
  ]
  node [
    id 123
    label "poinformowanie"
  ]
  node [
    id 124
    label "security"
  ]
  node [
    id 125
    label "zrobienie"
  ]
  node [
    id 126
    label "zesp&#243;&#322;"
  ]
  node [
    id 127
    label "dzia&#322;"
  ]
  node [
    id 128
    label "system"
  ]
  node [
    id 129
    label "lias"
  ]
  node [
    id 130
    label "jednostka"
  ]
  node [
    id 131
    label "pi&#281;tro"
  ]
  node [
    id 132
    label "klasa"
  ]
  node [
    id 133
    label "jednostka_geologiczna"
  ]
  node [
    id 134
    label "filia"
  ]
  node [
    id 135
    label "malm"
  ]
  node [
    id 136
    label "whole"
  ]
  node [
    id 137
    label "dogger"
  ]
  node [
    id 138
    label "poziom"
  ]
  node [
    id 139
    label "promocja"
  ]
  node [
    id 140
    label "kurs"
  ]
  node [
    id 141
    label "bank"
  ]
  node [
    id 142
    label "formacja"
  ]
  node [
    id 143
    label "ajencja"
  ]
  node [
    id 144
    label "wojsko"
  ]
  node [
    id 145
    label "siedziba"
  ]
  node [
    id 146
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 147
    label "agencja"
  ]
  node [
    id 148
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 149
    label "szpital"
  ]
  node [
    id 150
    label "zawarcie"
  ]
  node [
    id 151
    label "zawrze&#263;"
  ]
  node [
    id 152
    label "czyn"
  ]
  node [
    id 153
    label "warunek"
  ]
  node [
    id 154
    label "gestia_transportowa"
  ]
  node [
    id 155
    label "contract"
  ]
  node [
    id 156
    label "porozumienie"
  ]
  node [
    id 157
    label "klauzula"
  ]
  node [
    id 158
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 159
    label "obstawianie"
  ]
  node [
    id 160
    label "obstawienie"
  ]
  node [
    id 161
    label "tarcza"
  ]
  node [
    id 162
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 163
    label "transportacja"
  ]
  node [
    id 164
    label "obstawia&#263;"
  ]
  node [
    id 165
    label "borowiec"
  ]
  node [
    id 166
    label "chemical_bond"
  ]
  node [
    id 167
    label "danie"
  ]
  node [
    id 168
    label "confession"
  ]
  node [
    id 169
    label "stwierdzenie"
  ]
  node [
    id 170
    label "recognition"
  ]
  node [
    id 171
    label "oznajmienie"
  ]
  node [
    id 172
    label "preservation"
  ]
  node [
    id 173
    label "test_zderzeniowy"
  ]
  node [
    id 174
    label "porz&#261;dek"
  ]
  node [
    id 175
    label "katapultowa&#263;"
  ]
  node [
    id 176
    label "BHP"
  ]
  node [
    id 177
    label "ubezpieczy&#263;"
  ]
  node [
    id 178
    label "katapultowanie"
  ]
  node [
    id 179
    label "stan"
  ]
  node [
    id 180
    label "ubezpiecza&#263;"
  ]
  node [
    id 181
    label "safety"
  ]
  node [
    id 182
    label "cecha"
  ]
  node [
    id 183
    label "ubezpieczanie"
  ]
  node [
    id 184
    label "ubezpieczy&#263;_si&#281;"
  ]
  node [
    id 185
    label "instytucja"
  ]
  node [
    id 186
    label "ubezpieczenie_si&#281;"
  ]
  node [
    id 187
    label "zapis"
  ]
  node [
    id 188
    label "screenshot"
  ]
  node [
    id 189
    label "kawa&#322;ek"
  ]
  node [
    id 190
    label "aran&#380;acja"
  ]
  node [
    id 191
    label "Chocho&#322;"
  ]
  node [
    id 192
    label "Herkules_Poirot"
  ]
  node [
    id 193
    label "Edyp"
  ]
  node [
    id 194
    label "ludzko&#347;&#263;"
  ]
  node [
    id 195
    label "parali&#380;owa&#263;"
  ]
  node [
    id 196
    label "Harry_Potter"
  ]
  node [
    id 197
    label "Casanova"
  ]
  node [
    id 198
    label "Gargantua"
  ]
  node [
    id 199
    label "Zgredek"
  ]
  node [
    id 200
    label "Winnetou"
  ]
  node [
    id 201
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 202
    label "posta&#263;"
  ]
  node [
    id 203
    label "Dulcynea"
  ]
  node [
    id 204
    label "kategoria_gramatyczna"
  ]
  node [
    id 205
    label "g&#322;owa"
  ]
  node [
    id 206
    label "portrecista"
  ]
  node [
    id 207
    label "person"
  ]
  node [
    id 208
    label "Sherlock_Holmes"
  ]
  node [
    id 209
    label "Quasimodo"
  ]
  node [
    id 210
    label "Plastu&#347;"
  ]
  node [
    id 211
    label "Faust"
  ]
  node [
    id 212
    label "Wallenrod"
  ]
  node [
    id 213
    label "Dwukwiat"
  ]
  node [
    id 214
    label "koniugacja"
  ]
  node [
    id 215
    label "profanum"
  ]
  node [
    id 216
    label "Don_Juan"
  ]
  node [
    id 217
    label "Don_Kiszot"
  ]
  node [
    id 218
    label "mikrokosmos"
  ]
  node [
    id 219
    label "duch"
  ]
  node [
    id 220
    label "antropochoria"
  ]
  node [
    id 221
    label "oddzia&#322;ywanie"
  ]
  node [
    id 222
    label "Hamlet"
  ]
  node [
    id 223
    label "Werter"
  ]
  node [
    id 224
    label "istota"
  ]
  node [
    id 225
    label "Szwejk"
  ]
  node [
    id 226
    label "homo_sapiens"
  ]
  node [
    id 227
    label "mentalno&#347;&#263;"
  ]
  node [
    id 228
    label "superego"
  ]
  node [
    id 229
    label "psychika"
  ]
  node [
    id 230
    label "znaczenie"
  ]
  node [
    id 231
    label "wn&#281;trze"
  ]
  node [
    id 232
    label "charakter"
  ]
  node [
    id 233
    label "charakterystyka"
  ]
  node [
    id 234
    label "cz&#322;owiek"
  ]
  node [
    id 235
    label "zaistnie&#263;"
  ]
  node [
    id 236
    label "Osjan"
  ]
  node [
    id 237
    label "kto&#347;"
  ]
  node [
    id 238
    label "wygl&#261;d"
  ]
  node [
    id 239
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 240
    label "osobowo&#347;&#263;"
  ]
  node [
    id 241
    label "wytw&#243;r"
  ]
  node [
    id 242
    label "trim"
  ]
  node [
    id 243
    label "poby&#263;"
  ]
  node [
    id 244
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 245
    label "Aspazja"
  ]
  node [
    id 246
    label "punkt_widzenia"
  ]
  node [
    id 247
    label "kompleksja"
  ]
  node [
    id 248
    label "wytrzyma&#263;"
  ]
  node [
    id 249
    label "budowa"
  ]
  node [
    id 250
    label "pozosta&#263;"
  ]
  node [
    id 251
    label "point"
  ]
  node [
    id 252
    label "przedstawienie"
  ]
  node [
    id 253
    label "go&#347;&#263;"
  ]
  node [
    id 254
    label "hamper"
  ]
  node [
    id 255
    label "spasm"
  ]
  node [
    id 256
    label "mrozi&#263;"
  ]
  node [
    id 257
    label "pora&#380;a&#263;"
  ]
  node [
    id 258
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 259
    label "fleksja"
  ]
  node [
    id 260
    label "liczba"
  ]
  node [
    id 261
    label "coupling"
  ]
  node [
    id 262
    label "tryb"
  ]
  node [
    id 263
    label "czas"
  ]
  node [
    id 264
    label "czasownik"
  ]
  node [
    id 265
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 266
    label "orz&#281;sek"
  ]
  node [
    id 267
    label "fotograf"
  ]
  node [
    id 268
    label "malarz"
  ]
  node [
    id 269
    label "artysta"
  ]
  node [
    id 270
    label "powodowanie"
  ]
  node [
    id 271
    label "hipnotyzowanie"
  ]
  node [
    id 272
    label "&#347;lad"
  ]
  node [
    id 273
    label "docieranie"
  ]
  node [
    id 274
    label "natural_process"
  ]
  node [
    id 275
    label "reakcja_chemiczna"
  ]
  node [
    id 276
    label "wdzieranie_si&#281;"
  ]
  node [
    id 277
    label "zjawisko"
  ]
  node [
    id 278
    label "act"
  ]
  node [
    id 279
    label "rezultat"
  ]
  node [
    id 280
    label "lobbysta"
  ]
  node [
    id 281
    label "pryncypa&#322;"
  ]
  node [
    id 282
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 283
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 284
    label "wiedza"
  ]
  node [
    id 285
    label "kierowa&#263;"
  ]
  node [
    id 286
    label "alkohol"
  ]
  node [
    id 287
    label "zdolno&#347;&#263;"
  ]
  node [
    id 288
    label "&#380;ycie"
  ]
  node [
    id 289
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 290
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 291
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 292
    label "sztuka"
  ]
  node [
    id 293
    label "dekiel"
  ]
  node [
    id 294
    label "ro&#347;lina"
  ]
  node [
    id 295
    label "&#347;ci&#281;cie"
  ]
  node [
    id 296
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 297
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 298
    label "&#347;ci&#281;gno"
  ]
  node [
    id 299
    label "noosfera"
  ]
  node [
    id 300
    label "byd&#322;o"
  ]
  node [
    id 301
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 302
    label "makrocefalia"
  ]
  node [
    id 303
    label "ucho"
  ]
  node [
    id 304
    label "g&#243;ra"
  ]
  node [
    id 305
    label "m&#243;zg"
  ]
  node [
    id 306
    label "kierownictwo"
  ]
  node [
    id 307
    label "fryzura"
  ]
  node [
    id 308
    label "umys&#322;"
  ]
  node [
    id 309
    label "cia&#322;o"
  ]
  node [
    id 310
    label "cz&#322;onek"
  ]
  node [
    id 311
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 312
    label "czaszka"
  ]
  node [
    id 313
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 314
    label "allochoria"
  ]
  node [
    id 315
    label "p&#322;aszczyzna"
  ]
  node [
    id 316
    label "przedmiot"
  ]
  node [
    id 317
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 318
    label "bierka_szachowa"
  ]
  node [
    id 319
    label "obiekt_matematyczny"
  ]
  node [
    id 320
    label "gestaltyzm"
  ]
  node [
    id 321
    label "styl"
  ]
  node [
    id 322
    label "obraz"
  ]
  node [
    id 323
    label "rzecz"
  ]
  node [
    id 324
    label "d&#378;wi&#281;k"
  ]
  node [
    id 325
    label "character"
  ]
  node [
    id 326
    label "rze&#378;ba"
  ]
  node [
    id 327
    label "stylistyka"
  ]
  node [
    id 328
    label "figure"
  ]
  node [
    id 329
    label "miejsce"
  ]
  node [
    id 330
    label "antycypacja"
  ]
  node [
    id 331
    label "ornamentyka"
  ]
  node [
    id 332
    label "informacja"
  ]
  node [
    id 333
    label "facet"
  ]
  node [
    id 334
    label "popis"
  ]
  node [
    id 335
    label "wiersz"
  ]
  node [
    id 336
    label "symetria"
  ]
  node [
    id 337
    label "lingwistyka_kognitywna"
  ]
  node [
    id 338
    label "karta"
  ]
  node [
    id 339
    label "shape"
  ]
  node [
    id 340
    label "podzbi&#243;r"
  ]
  node [
    id 341
    label "perspektywa"
  ]
  node [
    id 342
    label "dziedzina"
  ]
  node [
    id 343
    label "Szekspir"
  ]
  node [
    id 344
    label "Mickiewicz"
  ]
  node [
    id 345
    label "cierpienie"
  ]
  node [
    id 346
    label "piek&#322;o"
  ]
  node [
    id 347
    label "human_body"
  ]
  node [
    id 348
    label "ofiarowywanie"
  ]
  node [
    id 349
    label "sfera_afektywna"
  ]
  node [
    id 350
    label "nekromancja"
  ]
  node [
    id 351
    label "Po&#347;wist"
  ]
  node [
    id 352
    label "podekscytowanie"
  ]
  node [
    id 353
    label "deformowanie"
  ]
  node [
    id 354
    label "sumienie"
  ]
  node [
    id 355
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 356
    label "deformowa&#263;"
  ]
  node [
    id 357
    label "zjawa"
  ]
  node [
    id 358
    label "zmar&#322;y"
  ]
  node [
    id 359
    label "istota_nadprzyrodzona"
  ]
  node [
    id 360
    label "power"
  ]
  node [
    id 361
    label "entity"
  ]
  node [
    id 362
    label "ofiarowywa&#263;"
  ]
  node [
    id 363
    label "oddech"
  ]
  node [
    id 364
    label "seksualno&#347;&#263;"
  ]
  node [
    id 365
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 366
    label "byt"
  ]
  node [
    id 367
    label "si&#322;a"
  ]
  node [
    id 368
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 369
    label "ego"
  ]
  node [
    id 370
    label "ofiarowanie"
  ]
  node [
    id 371
    label "fizjonomia"
  ]
  node [
    id 372
    label "kompleks"
  ]
  node [
    id 373
    label "zapalno&#347;&#263;"
  ]
  node [
    id 374
    label "T&#281;sknica"
  ]
  node [
    id 375
    label "ofiarowa&#263;"
  ]
  node [
    id 376
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 377
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 378
    label "passion"
  ]
  node [
    id 379
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 380
    label "odbicie"
  ]
  node [
    id 381
    label "atom"
  ]
  node [
    id 382
    label "przyroda"
  ]
  node [
    id 383
    label "Ziemia"
  ]
  node [
    id 384
    label "kosmos"
  ]
  node [
    id 385
    label "miniatura"
  ]
  node [
    id 386
    label "gramatyka"
  ]
  node [
    id 387
    label "kod"
  ]
  node [
    id 388
    label "przet&#322;umaczenie"
  ]
  node [
    id 389
    label "konsonantyzm"
  ]
  node [
    id 390
    label "wokalizm"
  ]
  node [
    id 391
    label "fonetyka"
  ]
  node [
    id 392
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 393
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 394
    label "po_koroniarsku"
  ]
  node [
    id 395
    label "t&#322;umaczenie"
  ]
  node [
    id 396
    label "m&#243;wienie"
  ]
  node [
    id 397
    label "pismo"
  ]
  node [
    id 398
    label "rozumie&#263;"
  ]
  node [
    id 399
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 400
    label "rozumienie"
  ]
  node [
    id 401
    label "wypowied&#378;"
  ]
  node [
    id 402
    label "address"
  ]
  node [
    id 403
    label "m&#243;wi&#263;"
  ]
  node [
    id 404
    label "komunikacja"
  ]
  node [
    id 405
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 406
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 407
    label "s&#322;ownictwo"
  ]
  node [
    id 408
    label "tongue"
  ]
  node [
    id 409
    label "pos&#322;uchanie"
  ]
  node [
    id 410
    label "s&#261;d"
  ]
  node [
    id 411
    label "sparafrazowanie"
  ]
  node [
    id 412
    label "strawestowa&#263;"
  ]
  node [
    id 413
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 414
    label "trawestowa&#263;"
  ]
  node [
    id 415
    label "sparafrazowa&#263;"
  ]
  node [
    id 416
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 417
    label "sformu&#322;owanie"
  ]
  node [
    id 418
    label "parafrazowanie"
  ]
  node [
    id 419
    label "ozdobnik"
  ]
  node [
    id 420
    label "delimitacja"
  ]
  node [
    id 421
    label "parafrazowa&#263;"
  ]
  node [
    id 422
    label "stylizacja"
  ]
  node [
    id 423
    label "komunikat"
  ]
  node [
    id 424
    label "trawestowanie"
  ]
  node [
    id 425
    label "strawestowanie"
  ]
  node [
    id 426
    label "posiada&#263;"
  ]
  node [
    id 427
    label "potencja&#322;"
  ]
  node [
    id 428
    label "zapomnienie"
  ]
  node [
    id 429
    label "zapomina&#263;"
  ]
  node [
    id 430
    label "zapominanie"
  ]
  node [
    id 431
    label "ability"
  ]
  node [
    id 432
    label "obliczeniowo"
  ]
  node [
    id 433
    label "zapomnie&#263;"
  ]
  node [
    id 434
    label "transportation_system"
  ]
  node [
    id 435
    label "explicite"
  ]
  node [
    id 436
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 437
    label "wydeptywanie"
  ]
  node [
    id 438
    label "wydeptanie"
  ]
  node [
    id 439
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 440
    label "implicite"
  ]
  node [
    id 441
    label "ekspedytor"
  ]
  node [
    id 442
    label "activity"
  ]
  node [
    id 443
    label "bezproblemowy"
  ]
  node [
    id 444
    label "wydarzenie"
  ]
  node [
    id 445
    label "struktura"
  ]
  node [
    id 446
    label "language"
  ]
  node [
    id 447
    label "code"
  ]
  node [
    id 448
    label "szyfrowanie"
  ]
  node [
    id 449
    label "ci&#261;g"
  ]
  node [
    id 450
    label "szablon"
  ]
  node [
    id 451
    label "j&#281;zyk"
  ]
  node [
    id 452
    label "public_speaking"
  ]
  node [
    id 453
    label "powiadanie"
  ]
  node [
    id 454
    label "przepowiadanie"
  ]
  node [
    id 455
    label "wyznawanie"
  ]
  node [
    id 456
    label "wypowiadanie"
  ]
  node [
    id 457
    label "wydobywanie"
  ]
  node [
    id 458
    label "gaworzenie"
  ]
  node [
    id 459
    label "stosowanie"
  ]
  node [
    id 460
    label "wyra&#380;anie"
  ]
  node [
    id 461
    label "formu&#322;owanie"
  ]
  node [
    id 462
    label "dowalenie"
  ]
  node [
    id 463
    label "przerywanie"
  ]
  node [
    id 464
    label "wydawanie"
  ]
  node [
    id 465
    label "dogadywanie_si&#281;"
  ]
  node [
    id 466
    label "dodawanie"
  ]
  node [
    id 467
    label "prawienie"
  ]
  node [
    id 468
    label "opowiadanie"
  ]
  node [
    id 469
    label "ozywanie_si&#281;"
  ]
  node [
    id 470
    label "zapeszanie"
  ]
  node [
    id 471
    label "zwracanie_si&#281;"
  ]
  node [
    id 472
    label "dysfonia"
  ]
  node [
    id 473
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 474
    label "speaking"
  ]
  node [
    id 475
    label "zauwa&#380;enie"
  ]
  node [
    id 476
    label "mawianie"
  ]
  node [
    id 477
    label "opowiedzenie"
  ]
  node [
    id 478
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 479
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 480
    label "informowanie"
  ]
  node [
    id 481
    label "dogadanie_si&#281;"
  ]
  node [
    id 482
    label "wygadanie"
  ]
  node [
    id 483
    label "psychotest"
  ]
  node [
    id 484
    label "wk&#322;ad"
  ]
  node [
    id 485
    label "handwriting"
  ]
  node [
    id 486
    label "przekaz"
  ]
  node [
    id 487
    label "dzie&#322;o"
  ]
  node [
    id 488
    label "paleograf"
  ]
  node [
    id 489
    label "interpunkcja"
  ]
  node [
    id 490
    label "grafia"
  ]
  node [
    id 491
    label "egzemplarz"
  ]
  node [
    id 492
    label "communication"
  ]
  node [
    id 493
    label "script"
  ]
  node [
    id 494
    label "zajawka"
  ]
  node [
    id 495
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 496
    label "list"
  ]
  node [
    id 497
    label "adres"
  ]
  node [
    id 498
    label "Zwrotnica"
  ]
  node [
    id 499
    label "czasopismo"
  ]
  node [
    id 500
    label "ok&#322;adka"
  ]
  node [
    id 501
    label "ortografia"
  ]
  node [
    id 502
    label "letter"
  ]
  node [
    id 503
    label "paleografia"
  ]
  node [
    id 504
    label "dokument"
  ]
  node [
    id 505
    label "prasa"
  ]
  node [
    id 506
    label "terminology"
  ]
  node [
    id 507
    label "termin"
  ]
  node [
    id 508
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 509
    label "sk&#322;adnia"
  ]
  node [
    id 510
    label "morfologia"
  ]
  node [
    id 511
    label "asymilowa&#263;"
  ]
  node [
    id 512
    label "g&#322;osownia"
  ]
  node [
    id 513
    label "zasymilowa&#263;"
  ]
  node [
    id 514
    label "phonetics"
  ]
  node [
    id 515
    label "asymilowanie"
  ]
  node [
    id 516
    label "palatogram"
  ]
  node [
    id 517
    label "transkrypcja"
  ]
  node [
    id 518
    label "zasymilowanie"
  ]
  node [
    id 519
    label "explanation"
  ]
  node [
    id 520
    label "bronienie"
  ]
  node [
    id 521
    label "remark"
  ]
  node [
    id 522
    label "przek&#322;adanie"
  ]
  node [
    id 523
    label "zrozumia&#322;y"
  ]
  node [
    id 524
    label "robienie"
  ]
  node [
    id 525
    label "przekonywanie"
  ]
  node [
    id 526
    label "uzasadnianie"
  ]
  node [
    id 527
    label "rozwianie"
  ]
  node [
    id 528
    label "rozwiewanie"
  ]
  node [
    id 529
    label "tekst"
  ]
  node [
    id 530
    label "gossip"
  ]
  node [
    id 531
    label "przedstawianie"
  ]
  node [
    id 532
    label "rendition"
  ]
  node [
    id 533
    label "kr&#281;ty"
  ]
  node [
    id 534
    label "zinterpretowa&#263;"
  ]
  node [
    id 535
    label "put"
  ]
  node [
    id 536
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 537
    label "zrobi&#263;"
  ]
  node [
    id 538
    label "przekona&#263;"
  ]
  node [
    id 539
    label "frame"
  ]
  node [
    id 540
    label "poja&#347;nia&#263;"
  ]
  node [
    id 541
    label "robi&#263;"
  ]
  node [
    id 542
    label "u&#322;atwia&#263;"
  ]
  node [
    id 543
    label "elaborate"
  ]
  node [
    id 544
    label "suplikowa&#263;"
  ]
  node [
    id 545
    label "przek&#322;ada&#263;"
  ]
  node [
    id 546
    label "przekonywa&#263;"
  ]
  node [
    id 547
    label "interpretowa&#263;"
  ]
  node [
    id 548
    label "broni&#263;"
  ]
  node [
    id 549
    label "explain"
  ]
  node [
    id 550
    label "przedstawia&#263;"
  ]
  node [
    id 551
    label "sprawowa&#263;"
  ]
  node [
    id 552
    label "uzasadnia&#263;"
  ]
  node [
    id 553
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 554
    label "gaworzy&#263;"
  ]
  node [
    id 555
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 556
    label "rozmawia&#263;"
  ]
  node [
    id 557
    label "wyra&#380;a&#263;"
  ]
  node [
    id 558
    label "umie&#263;"
  ]
  node [
    id 559
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 560
    label "dziama&#263;"
  ]
  node [
    id 561
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 562
    label "formu&#322;owa&#263;"
  ]
  node [
    id 563
    label "express"
  ]
  node [
    id 564
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 565
    label "talk"
  ]
  node [
    id 566
    label "u&#380;ywa&#263;"
  ]
  node [
    id 567
    label "prawi&#263;"
  ]
  node [
    id 568
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 569
    label "powiada&#263;"
  ]
  node [
    id 570
    label "tell"
  ]
  node [
    id 571
    label "chew_the_fat"
  ]
  node [
    id 572
    label "say"
  ]
  node [
    id 573
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 574
    label "informowa&#263;"
  ]
  node [
    id 575
    label "wydobywa&#263;"
  ]
  node [
    id 576
    label "okre&#347;la&#263;"
  ]
  node [
    id 577
    label "hermeneutyka"
  ]
  node [
    id 578
    label "bycie"
  ]
  node [
    id 579
    label "kontekst"
  ]
  node [
    id 580
    label "apprehension"
  ]
  node [
    id 581
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 582
    label "interpretation"
  ]
  node [
    id 583
    label "obja&#347;nienie"
  ]
  node [
    id 584
    label "czucie"
  ]
  node [
    id 585
    label "realization"
  ]
  node [
    id 586
    label "kumanie"
  ]
  node [
    id 587
    label "wnioskowanie"
  ]
  node [
    id 588
    label "wiedzie&#263;"
  ]
  node [
    id 589
    label "kuma&#263;"
  ]
  node [
    id 590
    label "czu&#263;"
  ]
  node [
    id 591
    label "match"
  ]
  node [
    id 592
    label "empatia"
  ]
  node [
    id 593
    label "odbiera&#263;"
  ]
  node [
    id 594
    label "see"
  ]
  node [
    id 595
    label "zna&#263;"
  ]
  node [
    id 596
    label "dzi&#243;b"
  ]
  node [
    id 597
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 598
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 599
    label "zacinanie"
  ]
  node [
    id 600
    label "ssa&#263;"
  ]
  node [
    id 601
    label "organ"
  ]
  node [
    id 602
    label "zacina&#263;"
  ]
  node [
    id 603
    label "zaci&#261;&#263;"
  ]
  node [
    id 604
    label "ssanie"
  ]
  node [
    id 605
    label "jama_ustna"
  ]
  node [
    id 606
    label "jadaczka"
  ]
  node [
    id 607
    label "zaci&#281;cie"
  ]
  node [
    id 608
    label "warga_dolna"
  ]
  node [
    id 609
    label "twarz"
  ]
  node [
    id 610
    label "warga_g&#243;rna"
  ]
  node [
    id 611
    label "ryjek"
  ]
  node [
    id 612
    label "tkanka"
  ]
  node [
    id 613
    label "jednostka_organizacyjna"
  ]
  node [
    id 614
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 615
    label "tw&#243;r"
  ]
  node [
    id 616
    label "organogeneza"
  ]
  node [
    id 617
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 618
    label "struktura_anatomiczna"
  ]
  node [
    id 619
    label "uk&#322;ad"
  ]
  node [
    id 620
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 621
    label "dekortykacja"
  ]
  node [
    id 622
    label "Izba_Konsyliarska"
  ]
  node [
    id 623
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 624
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 625
    label "stomia"
  ]
  node [
    id 626
    label "okolica"
  ]
  node [
    id 627
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 628
    label "Komitet_Region&#243;w"
  ]
  node [
    id 629
    label "ptak"
  ]
  node [
    id 630
    label "grzebie&#324;"
  ]
  node [
    id 631
    label "bow"
  ]
  node [
    id 632
    label "statek"
  ]
  node [
    id 633
    label "ustnik"
  ]
  node [
    id 634
    label "sto&#380;ek_dziobowy"
  ]
  node [
    id 635
    label "samolot"
  ]
  node [
    id 636
    label "zako&#324;czenie"
  ]
  node [
    id 637
    label "ostry"
  ]
  node [
    id 638
    label "blizna"
  ]
  node [
    id 639
    label "dziob&#243;wka"
  ]
  node [
    id 640
    label "cera"
  ]
  node [
    id 641
    label "wielko&#347;&#263;"
  ]
  node [
    id 642
    label "rys"
  ]
  node [
    id 643
    label "przedstawiciel"
  ]
  node [
    id 644
    label "profil"
  ]
  node [
    id 645
    label "p&#322;e&#263;"
  ]
  node [
    id 646
    label "zas&#322;ona"
  ]
  node [
    id 647
    label "p&#243;&#322;profil"
  ]
  node [
    id 648
    label "policzek"
  ]
  node [
    id 649
    label "brew"
  ]
  node [
    id 650
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 651
    label "uj&#281;cie"
  ]
  node [
    id 652
    label "micha"
  ]
  node [
    id 653
    label "reputacja"
  ]
  node [
    id 654
    label "wyraz_twarzy"
  ]
  node [
    id 655
    label "powieka"
  ]
  node [
    id 656
    label "czo&#322;o"
  ]
  node [
    id 657
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 658
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 659
    label "twarzyczka"
  ]
  node [
    id 660
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 661
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 662
    label "prz&#243;d"
  ]
  node [
    id 663
    label "oko"
  ]
  node [
    id 664
    label "nos"
  ]
  node [
    id 665
    label "podbr&#243;dek"
  ]
  node [
    id 666
    label "liczko"
  ]
  node [
    id 667
    label "pysk"
  ]
  node [
    id 668
    label "maskowato&#347;&#263;"
  ]
  node [
    id 669
    label "poderwa&#263;"
  ]
  node [
    id 670
    label "ko&#324;"
  ]
  node [
    id 671
    label "&#347;cisn&#261;&#263;"
  ]
  node [
    id 672
    label "ostruga&#263;"
  ]
  node [
    id 673
    label "nadci&#261;&#263;"
  ]
  node [
    id 674
    label "bat"
  ]
  node [
    id 675
    label "uderzy&#263;"
  ]
  node [
    id 676
    label "zrani&#263;"
  ]
  node [
    id 677
    label "w&#281;dka"
  ]
  node [
    id 678
    label "lejce"
  ]
  node [
    id 679
    label "wprawi&#263;"
  ]
  node [
    id 680
    label "cut"
  ]
  node [
    id 681
    label "przerwa&#263;"
  ]
  node [
    id 682
    label "ch&#322;osn&#261;&#263;"
  ]
  node [
    id 683
    label "zepsu&#263;"
  ]
  node [
    id 684
    label "naci&#261;&#263;"
  ]
  node [
    id 685
    label "odebra&#263;"
  ]
  node [
    id 686
    label "zablokowa&#263;"
  ]
  node [
    id 687
    label "write_out"
  ]
  node [
    id 688
    label "padanie"
  ]
  node [
    id 689
    label "mina"
  ]
  node [
    id 690
    label "w&#281;dkowanie"
  ]
  node [
    id 691
    label "kaleczenie"
  ]
  node [
    id 692
    label "nacinanie"
  ]
  node [
    id 693
    label "podrywanie"
  ]
  node [
    id 694
    label "powo&#380;enie"
  ]
  node [
    id 695
    label "zaciskanie"
  ]
  node [
    id 696
    label "struganie"
  ]
  node [
    id 697
    label "ch&#322;ostanie"
  ]
  node [
    id 698
    label "&#347;cina&#263;"
  ]
  node [
    id 699
    label "zaciera&#263;"
  ]
  node [
    id 700
    label "przerywa&#263;"
  ]
  node [
    id 701
    label "psu&#263;"
  ]
  node [
    id 702
    label "zaciska&#263;"
  ]
  node [
    id 703
    label "zakrawa&#263;"
  ]
  node [
    id 704
    label "wprawia&#263;"
  ]
  node [
    id 705
    label "podrywa&#263;"
  ]
  node [
    id 706
    label "hack"
  ]
  node [
    id 707
    label "reduce"
  ]
  node [
    id 708
    label "przestawa&#263;"
  ]
  node [
    id 709
    label "blokowa&#263;"
  ]
  node [
    id 710
    label "nacina&#263;"
  ]
  node [
    id 711
    label "pocina&#263;"
  ]
  node [
    id 712
    label "uderza&#263;"
  ]
  node [
    id 713
    label "ch&#322;osta&#263;"
  ]
  node [
    id 714
    label "kaleczy&#263;"
  ]
  node [
    id 715
    label "struga&#263;"
  ]
  node [
    id 716
    label "ch&#322;o&#347;ni&#281;cie"
  ]
  node [
    id 717
    label "&#347;ci&#347;ni&#281;cie"
  ]
  node [
    id 718
    label "naci&#281;cie"
  ]
  node [
    id 719
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 720
    label "zaci&#281;ty"
  ]
  node [
    id 721
    label "poderwanie"
  ]
  node [
    id 722
    label "talent"
  ]
  node [
    id 723
    label "go"
  ]
  node [
    id 724
    label "capability"
  ]
  node [
    id 725
    label "stanowczo"
  ]
  node [
    id 726
    label "ostruganie"
  ]
  node [
    id 727
    label "formacja_skalna"
  ]
  node [
    id 728
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 729
    label "potkni&#281;cie"
  ]
  node [
    id 730
    label "zranienie"
  ]
  node [
    id 731
    label "turn"
  ]
  node [
    id 732
    label "dash"
  ]
  node [
    id 733
    label "uwa&#380;nie"
  ]
  node [
    id 734
    label "nieust&#281;pliwie"
  ]
  node [
    id 735
    label "&#347;lina"
  ]
  node [
    id 736
    label "pi&#263;"
  ]
  node [
    id 737
    label "sponge"
  ]
  node [
    id 738
    label "mleko"
  ]
  node [
    id 739
    label "rozpuszcza&#263;"
  ]
  node [
    id 740
    label "wci&#261;ga&#263;"
  ]
  node [
    id 741
    label "rusza&#263;"
  ]
  node [
    id 742
    label "sucking"
  ]
  node [
    id 743
    label "smoczek"
  ]
  node [
    id 744
    label "narz&#261;d_g&#281;bowy"
  ]
  node [
    id 745
    label "picie"
  ]
  node [
    id 746
    label "ruszanie"
  ]
  node [
    id 747
    label "consumption"
  ]
  node [
    id 748
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 749
    label "rozpuszczanie"
  ]
  node [
    id 750
    label "aspiration"
  ]
  node [
    id 751
    label "wci&#261;ganie"
  ]
  node [
    id 752
    label "odci&#261;ganie"
  ]
  node [
    id 753
    label "wessanie"
  ]
  node [
    id 754
    label "ga&#378;nik"
  ]
  node [
    id 755
    label "mechanizm"
  ]
  node [
    id 756
    label "wysysanie"
  ]
  node [
    id 757
    label "wyssanie"
  ]
  node [
    id 758
    label "ranek"
  ]
  node [
    id 759
    label "doba"
  ]
  node [
    id 760
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 761
    label "noc"
  ]
  node [
    id 762
    label "podwiecz&#243;r"
  ]
  node [
    id 763
    label "po&#322;udnie"
  ]
  node [
    id 764
    label "godzina"
  ]
  node [
    id 765
    label "przedpo&#322;udnie"
  ]
  node [
    id 766
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 767
    label "long_time"
  ]
  node [
    id 768
    label "wiecz&#243;r"
  ]
  node [
    id 769
    label "t&#322;usty_czwartek"
  ]
  node [
    id 770
    label "popo&#322;udnie"
  ]
  node [
    id 771
    label "walentynki"
  ]
  node [
    id 772
    label "czynienie_si&#281;"
  ]
  node [
    id 773
    label "s&#322;o&#324;ce"
  ]
  node [
    id 774
    label "rano"
  ]
  node [
    id 775
    label "tydzie&#324;"
  ]
  node [
    id 776
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 777
    label "wzej&#347;cie"
  ]
  node [
    id 778
    label "wsta&#263;"
  ]
  node [
    id 779
    label "day"
  ]
  node [
    id 780
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 781
    label "wstanie"
  ]
  node [
    id 782
    label "przedwiecz&#243;r"
  ]
  node [
    id 783
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 784
    label "Sylwester"
  ]
  node [
    id 785
    label "poprzedzanie"
  ]
  node [
    id 786
    label "czasoprzestrze&#324;"
  ]
  node [
    id 787
    label "laba"
  ]
  node [
    id 788
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 789
    label "chronometria"
  ]
  node [
    id 790
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 791
    label "rachuba_czasu"
  ]
  node [
    id 792
    label "przep&#322;ywanie"
  ]
  node [
    id 793
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 794
    label "czasokres"
  ]
  node [
    id 795
    label "odczyt"
  ]
  node [
    id 796
    label "chwila"
  ]
  node [
    id 797
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 798
    label "dzieje"
  ]
  node [
    id 799
    label "poprzedzenie"
  ]
  node [
    id 800
    label "trawienie"
  ]
  node [
    id 801
    label "pochodzi&#263;"
  ]
  node [
    id 802
    label "period"
  ]
  node [
    id 803
    label "okres_czasu"
  ]
  node [
    id 804
    label "poprzedza&#263;"
  ]
  node [
    id 805
    label "schy&#322;ek"
  ]
  node [
    id 806
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 807
    label "odwlekanie_si&#281;"
  ]
  node [
    id 808
    label "zegar"
  ]
  node [
    id 809
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 810
    label "czwarty_wymiar"
  ]
  node [
    id 811
    label "pochodzenie"
  ]
  node [
    id 812
    label "Zeitgeist"
  ]
  node [
    id 813
    label "trawi&#263;"
  ]
  node [
    id 814
    label "pogoda"
  ]
  node [
    id 815
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 816
    label "poprzedzi&#263;"
  ]
  node [
    id 817
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 818
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 819
    label "time_period"
  ]
  node [
    id 820
    label "nazewnictwo"
  ]
  node [
    id 821
    label "term"
  ]
  node [
    id 822
    label "przypadni&#281;cie"
  ]
  node [
    id 823
    label "ekspiracja"
  ]
  node [
    id 824
    label "przypa&#347;&#263;"
  ]
  node [
    id 825
    label "chronogram"
  ]
  node [
    id 826
    label "praktyka"
  ]
  node [
    id 827
    label "nazwa"
  ]
  node [
    id 828
    label "odwieczerz"
  ]
  node [
    id 829
    label "pora"
  ]
  node [
    id 830
    label "przyj&#281;cie"
  ]
  node [
    id 831
    label "night"
  ]
  node [
    id 832
    label "zach&#243;d"
  ]
  node [
    id 833
    label "vesper"
  ]
  node [
    id 834
    label "aurora"
  ]
  node [
    id 835
    label "wsch&#243;d"
  ]
  node [
    id 836
    label "&#347;rodek"
  ]
  node [
    id 837
    label "obszar"
  ]
  node [
    id 838
    label "dwunasta"
  ]
  node [
    id 839
    label "strona_&#347;wiata"
  ]
  node [
    id 840
    label "dopo&#322;udnie"
  ]
  node [
    id 841
    label "blady_&#347;wit"
  ]
  node [
    id 842
    label "podkurek"
  ]
  node [
    id 843
    label "time"
  ]
  node [
    id 844
    label "p&#243;&#322;godzina"
  ]
  node [
    id 845
    label "jednostka_czasu"
  ]
  node [
    id 846
    label "minuta"
  ]
  node [
    id 847
    label "kwadrans"
  ]
  node [
    id 848
    label "p&#243;&#322;noc"
  ]
  node [
    id 849
    label "nokturn"
  ]
  node [
    id 850
    label "weekend"
  ]
  node [
    id 851
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 852
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 853
    label "miesi&#261;c"
  ]
  node [
    id 854
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 855
    label "mount"
  ]
  node [
    id 856
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 857
    label "wzej&#347;&#263;"
  ]
  node [
    id 858
    label "ascend"
  ]
  node [
    id 859
    label "kuca&#263;"
  ]
  node [
    id 860
    label "wyzdrowie&#263;"
  ]
  node [
    id 861
    label "opu&#347;ci&#263;"
  ]
  node [
    id 862
    label "rise"
  ]
  node [
    id 863
    label "arise"
  ]
  node [
    id 864
    label "stan&#261;&#263;"
  ]
  node [
    id 865
    label "przesta&#263;"
  ]
  node [
    id 866
    label "wyzdrowienie"
  ]
  node [
    id 867
    label "le&#380;enie"
  ]
  node [
    id 868
    label "kl&#281;czenie"
  ]
  node [
    id 869
    label "opuszczenie"
  ]
  node [
    id 870
    label "uniesienie_si&#281;"
  ]
  node [
    id 871
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 872
    label "siedzenie"
  ]
  node [
    id 873
    label "beginning"
  ]
  node [
    id 874
    label "przestanie"
  ]
  node [
    id 875
    label "S&#322;o&#324;ce"
  ]
  node [
    id 876
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 877
    label "&#347;wiat&#322;o"
  ]
  node [
    id 878
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 879
    label "kochanie"
  ]
  node [
    id 880
    label "sunlight"
  ]
  node [
    id 881
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 882
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 883
    label "grudzie&#324;"
  ]
  node [
    id 884
    label "luty"
  ]
  node [
    id 885
    label "blend"
  ]
  node [
    id 886
    label "set"
  ]
  node [
    id 887
    label "zgi&#281;cie"
  ]
  node [
    id 888
    label "fold"
  ]
  node [
    id 889
    label "opracowanie"
  ]
  node [
    id 890
    label "posk&#322;adanie"
  ]
  node [
    id 891
    label "przekazanie"
  ]
  node [
    id 892
    label "stage_set"
  ]
  node [
    id 893
    label "powiedzenie"
  ]
  node [
    id 894
    label "leksem"
  ]
  node [
    id 895
    label "przy&#322;o&#380;enie"
  ]
  node [
    id 896
    label "derywat_z&#322;o&#380;ony"
  ]
  node [
    id 897
    label "lodging"
  ]
  node [
    id 898
    label "zgromadzenie"
  ]
  node [
    id 899
    label "removal"
  ]
  node [
    id 900
    label "zestawienie"
  ]
  node [
    id 901
    label "obiecanie"
  ]
  node [
    id 902
    label "zap&#322;acenie"
  ]
  node [
    id 903
    label "cios"
  ]
  node [
    id 904
    label "udost&#281;pnienie"
  ]
  node [
    id 905
    label "wymienienie_si&#281;"
  ]
  node [
    id 906
    label "eating"
  ]
  node [
    id 907
    label "coup"
  ]
  node [
    id 908
    label "hand"
  ]
  node [
    id 909
    label "uprawianie_seksu"
  ]
  node [
    id 910
    label "allow"
  ]
  node [
    id 911
    label "dostarczenie"
  ]
  node [
    id 912
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 913
    label "uderzenie"
  ]
  node [
    id 914
    label "zadanie"
  ]
  node [
    id 915
    label "powierzenie"
  ]
  node [
    id 916
    label "przeznaczenie"
  ]
  node [
    id 917
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 918
    label "odst&#261;pienie"
  ]
  node [
    id 919
    label "dodanie"
  ]
  node [
    id 920
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 921
    label "wyposa&#380;enie"
  ]
  node [
    id 922
    label "dostanie"
  ]
  node [
    id 923
    label "potrawa"
  ]
  node [
    id 924
    label "pass"
  ]
  node [
    id 925
    label "menu"
  ]
  node [
    id 926
    label "uderzanie"
  ]
  node [
    id 927
    label "wyst&#261;pienie"
  ]
  node [
    id 928
    label "jedzenie"
  ]
  node [
    id 929
    label "wyposa&#380;anie"
  ]
  node [
    id 930
    label "pobicie"
  ]
  node [
    id 931
    label "posi&#322;ek"
  ]
  node [
    id 932
    label "urz&#261;dzenie"
  ]
  node [
    id 933
    label "sumariusz"
  ]
  node [
    id 934
    label "ustawienie"
  ]
  node [
    id 935
    label "z&#322;amanie"
  ]
  node [
    id 936
    label "kompozycja"
  ]
  node [
    id 937
    label "strata"
  ]
  node [
    id 938
    label "composition"
  ]
  node [
    id 939
    label "book"
  ]
  node [
    id 940
    label "stock"
  ]
  node [
    id 941
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 942
    label "catalog"
  ]
  node [
    id 943
    label "sprawozdanie_finansowe"
  ]
  node [
    id 944
    label "figurowa&#263;"
  ]
  node [
    id 945
    label "z&#322;&#261;czenie"
  ]
  node [
    id 946
    label "count"
  ]
  node [
    id 947
    label "wyra&#380;enie"
  ]
  node [
    id 948
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 949
    label "wyliczanka"
  ]
  node [
    id 950
    label "analiza"
  ]
  node [
    id 951
    label "deficyt"
  ]
  node [
    id 952
    label "obrot&#243;wka"
  ]
  node [
    id 953
    label "pozycja"
  ]
  node [
    id 954
    label "comparison"
  ]
  node [
    id 955
    label "zanalizowanie"
  ]
  node [
    id 956
    label "powyginanie"
  ]
  node [
    id 957
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 958
    label "pochylenie"
  ]
  node [
    id 959
    label "zdeformowanie"
  ]
  node [
    id 960
    label "wygi&#281;cie_si&#281;"
  ]
  node [
    id 961
    label "camber"
  ]
  node [
    id 962
    label "zginanie"
  ]
  node [
    id 963
    label "bending"
  ]
  node [
    id 964
    label "przygotowanie"
  ]
  node [
    id 965
    label "rozprawa"
  ]
  node [
    id 966
    label "paper"
  ]
  node [
    id 967
    label "concourse"
  ]
  node [
    id 968
    label "gathering"
  ]
  node [
    id 969
    label "skupienie"
  ]
  node [
    id 970
    label "wsp&#243;lnota"
  ]
  node [
    id 971
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 972
    label "grupa"
  ]
  node [
    id 973
    label "gromadzenie"
  ]
  node [
    id 974
    label "templum"
  ]
  node [
    id 975
    label "konwentykiel"
  ]
  node [
    id 976
    label "klasztor"
  ]
  node [
    id 977
    label "caucus"
  ]
  node [
    id 978
    label "pozyskanie"
  ]
  node [
    id 979
    label "kongregacja"
  ]
  node [
    id 980
    label "wordnet"
  ]
  node [
    id 981
    label "wypowiedzenie"
  ]
  node [
    id 982
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 983
    label "morfem"
  ]
  node [
    id 984
    label "wykrzyknik"
  ]
  node [
    id 985
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 986
    label "pole_semantyczne"
  ]
  node [
    id 987
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 988
    label "pisanie_si&#281;"
  ]
  node [
    id 989
    label "nag&#322;os"
  ]
  node [
    id 990
    label "wyg&#322;os"
  ]
  node [
    id 991
    label "jednostka_leksykalna"
  ]
  node [
    id 992
    label "rozwleczenie"
  ]
  node [
    id 993
    label "wyznanie"
  ]
  node [
    id 994
    label "przepowiedzenie"
  ]
  node [
    id 995
    label "podanie"
  ]
  node [
    id 996
    label "wydanie"
  ]
  node [
    id 997
    label "zapeszenie"
  ]
  node [
    id 998
    label "wydobycie"
  ]
  node [
    id 999
    label "proverb"
  ]
  node [
    id 1000
    label "ozwanie_si&#281;"
  ]
  node [
    id 1001
    label "nazwanie"
  ]
  node [
    id 1002
    label "notification"
  ]
  node [
    id 1003
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 1004
    label "doprowadzenie"
  ]
  node [
    id 1005
    label "zbli&#380;enie"
  ]
  node [
    id 1006
    label "dopieprzenie"
  ]
  node [
    id 1007
    label "apposition"
  ]
  node [
    id 1008
    label "przypalantowanie"
  ]
  node [
    id 1009
    label "juxtaposition"
  ]
  node [
    id 1010
    label "gem"
  ]
  node [
    id 1011
    label "runda"
  ]
  node [
    id 1012
    label "muzyka"
  ]
  node [
    id 1013
    label "zestaw"
  ]
  node [
    id 1014
    label "wp&#322;acenie"
  ]
  node [
    id 1015
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1016
    label "dor&#281;czenie"
  ]
  node [
    id 1017
    label "wys&#322;anie"
  ]
  node [
    id 1018
    label "delivery"
  ]
  node [
    id 1019
    label "transfer"
  ]
  node [
    id 1020
    label "sygna&#322;"
  ]
  node [
    id 1021
    label "prayer"
  ]
  node [
    id 1022
    label "twierdzenie"
  ]
  node [
    id 1023
    label "propozycja"
  ]
  node [
    id 1024
    label "my&#347;l"
  ]
  node [
    id 1025
    label "motion"
  ]
  node [
    id 1026
    label "szko&#322;a"
  ]
  node [
    id 1027
    label "p&#322;&#243;d"
  ]
  node [
    id 1028
    label "thinking"
  ]
  node [
    id 1029
    label "political_orientation"
  ]
  node [
    id 1030
    label "pomys&#322;"
  ]
  node [
    id 1031
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 1032
    label "idea"
  ]
  node [
    id 1033
    label "fantomatyka"
  ]
  node [
    id 1034
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 1035
    label "alternatywa_Fredholma"
  ]
  node [
    id 1036
    label "oznajmianie"
  ]
  node [
    id 1037
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 1038
    label "teoria"
  ]
  node [
    id 1039
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 1040
    label "paradoks_Leontiefa"
  ]
  node [
    id 1041
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 1042
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 1043
    label "teza"
  ]
  node [
    id 1044
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 1045
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 1046
    label "twierdzenie_Pettisa"
  ]
  node [
    id 1047
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 1048
    label "twierdzenie_Maya"
  ]
  node [
    id 1049
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 1050
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 1051
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 1052
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 1053
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 1054
    label "zapewnianie"
  ]
  node [
    id 1055
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 1056
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 1057
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 1058
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 1059
    label "twierdzenie_Stokesa"
  ]
  node [
    id 1060
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 1061
    label "twierdzenie_Cevy"
  ]
  node [
    id 1062
    label "twierdzenie_Pascala"
  ]
  node [
    id 1063
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 1064
    label "komunikowanie"
  ]
  node [
    id 1065
    label "zasada"
  ]
  node [
    id 1066
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 1067
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 1068
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 1069
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 1070
    label "proposal"
  ]
  node [
    id 1071
    label "proszenie"
  ]
  node [
    id 1072
    label "dochodzenie"
  ]
  node [
    id 1073
    label "proces_my&#347;lowy"
  ]
  node [
    id 1074
    label "lead"
  ]
  node [
    id 1075
    label "konkluzja"
  ]
  node [
    id 1076
    label "sk&#322;adanie"
  ]
  node [
    id 1077
    label "przes&#322;anka"
  ]
  node [
    id 1078
    label "czyj&#347;"
  ]
  node [
    id 1079
    label "m&#261;&#380;"
  ]
  node [
    id 1080
    label "prywatny"
  ]
  node [
    id 1081
    label "ma&#322;&#380;onek"
  ]
  node [
    id 1082
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1083
    label "ch&#322;op"
  ]
  node [
    id 1084
    label "pan_m&#322;ody"
  ]
  node [
    id 1085
    label "&#347;lubny"
  ]
  node [
    id 1086
    label "pan_domu"
  ]
  node [
    id 1087
    label "pan_i_w&#322;adca"
  ]
  node [
    id 1088
    label "stary"
  ]
  node [
    id 1089
    label "du&#380;y"
  ]
  node [
    id 1090
    label "mocno"
  ]
  node [
    id 1091
    label "wiela"
  ]
  node [
    id 1092
    label "bardzo"
  ]
  node [
    id 1093
    label "cz&#281;sto"
  ]
  node [
    id 1094
    label "wiele"
  ]
  node [
    id 1095
    label "doros&#322;y"
  ]
  node [
    id 1096
    label "znaczny"
  ]
  node [
    id 1097
    label "niema&#322;o"
  ]
  node [
    id 1098
    label "rozwini&#281;ty"
  ]
  node [
    id 1099
    label "dorodny"
  ]
  node [
    id 1100
    label "wa&#380;ny"
  ]
  node [
    id 1101
    label "prawdziwy"
  ]
  node [
    id 1102
    label "intensywny"
  ]
  node [
    id 1103
    label "mocny"
  ]
  node [
    id 1104
    label "silny"
  ]
  node [
    id 1105
    label "przekonuj&#261;co"
  ]
  node [
    id 1106
    label "powerfully"
  ]
  node [
    id 1107
    label "widocznie"
  ]
  node [
    id 1108
    label "szczerze"
  ]
  node [
    id 1109
    label "konkretnie"
  ]
  node [
    id 1110
    label "niepodwa&#380;alnie"
  ]
  node [
    id 1111
    label "stabilnie"
  ]
  node [
    id 1112
    label "silnie"
  ]
  node [
    id 1113
    label "zdecydowanie"
  ]
  node [
    id 1114
    label "strongly"
  ]
  node [
    id 1115
    label "w_chuj"
  ]
  node [
    id 1116
    label "cz&#281;sty"
  ]
  node [
    id 1117
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 1118
    label "faza"
  ]
  node [
    id 1119
    label "nizina"
  ]
  node [
    id 1120
    label "depression"
  ]
  node [
    id 1121
    label "Wybrze&#380;e_Koromandelskie"
  ]
  node [
    id 1122
    label "l&#261;d"
  ]
  node [
    id 1123
    label "Pampa"
  ]
  node [
    id 1124
    label "Nizina_Nadwi&#347;la&#324;ska"
  ]
  node [
    id 1125
    label "proces"
  ]
  node [
    id 1126
    label "boski"
  ]
  node [
    id 1127
    label "krajobraz"
  ]
  node [
    id 1128
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1129
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1130
    label "przywidzenie"
  ]
  node [
    id 1131
    label "presence"
  ]
  node [
    id 1132
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1133
    label "cykl_astronomiczny"
  ]
  node [
    id 1134
    label "coil"
  ]
  node [
    id 1135
    label "fotoelement"
  ]
  node [
    id 1136
    label "komutowanie"
  ]
  node [
    id 1137
    label "stan_skupienia"
  ]
  node [
    id 1138
    label "nastr&#243;j"
  ]
  node [
    id 1139
    label "przerywacz"
  ]
  node [
    id 1140
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 1141
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 1142
    label "kraw&#281;d&#378;"
  ]
  node [
    id 1143
    label "obsesja"
  ]
  node [
    id 1144
    label "dw&#243;jnik"
  ]
  node [
    id 1145
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 1146
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 1147
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1148
    label "przew&#243;d"
  ]
  node [
    id 1149
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 1150
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 1151
    label "obw&#243;d"
  ]
  node [
    id 1152
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1153
    label "degree"
  ]
  node [
    id 1154
    label "komutowa&#263;"
  ]
  node [
    id 1155
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1156
    label "jako&#347;&#263;"
  ]
  node [
    id 1157
    label "kierunek"
  ]
  node [
    id 1158
    label "wyk&#322;adnik"
  ]
  node [
    id 1159
    label "szczebel"
  ]
  node [
    id 1160
    label "budynek"
  ]
  node [
    id 1161
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1162
    label "ranga"
  ]
  node [
    id 1163
    label "summer"
  ]
  node [
    id 1164
    label "okres_amazo&#324;ski"
  ]
  node [
    id 1165
    label "stater"
  ]
  node [
    id 1166
    label "flow"
  ]
  node [
    id 1167
    label "choroba_przyrodzona"
  ]
  node [
    id 1168
    label "postglacja&#322;"
  ]
  node [
    id 1169
    label "sylur"
  ]
  node [
    id 1170
    label "kreda"
  ]
  node [
    id 1171
    label "ordowik"
  ]
  node [
    id 1172
    label "okres_hesperyjski"
  ]
  node [
    id 1173
    label "paleogen"
  ]
  node [
    id 1174
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 1175
    label "okres_halsztacki"
  ]
  node [
    id 1176
    label "riak"
  ]
  node [
    id 1177
    label "czwartorz&#281;d"
  ]
  node [
    id 1178
    label "podokres"
  ]
  node [
    id 1179
    label "trzeciorz&#281;d"
  ]
  node [
    id 1180
    label "kalim"
  ]
  node [
    id 1181
    label "fala"
  ]
  node [
    id 1182
    label "perm"
  ]
  node [
    id 1183
    label "retoryka"
  ]
  node [
    id 1184
    label "prekambr"
  ]
  node [
    id 1185
    label "neogen"
  ]
  node [
    id 1186
    label "pulsacja"
  ]
  node [
    id 1187
    label "proces_fizjologiczny"
  ]
  node [
    id 1188
    label "kambr"
  ]
  node [
    id 1189
    label "kriogen"
  ]
  node [
    id 1190
    label "ton"
  ]
  node [
    id 1191
    label "orosir"
  ]
  node [
    id 1192
    label "poprzednik"
  ]
  node [
    id 1193
    label "spell"
  ]
  node [
    id 1194
    label "interstadia&#322;"
  ]
  node [
    id 1195
    label "ektas"
  ]
  node [
    id 1196
    label "sider"
  ]
  node [
    id 1197
    label "epoka"
  ]
  node [
    id 1198
    label "rok_akademicki"
  ]
  node [
    id 1199
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 1200
    label "cykl"
  ]
  node [
    id 1201
    label "ciota"
  ]
  node [
    id 1202
    label "pierwszorz&#281;d"
  ]
  node [
    id 1203
    label "okres_noachijski"
  ]
  node [
    id 1204
    label "ediakar"
  ]
  node [
    id 1205
    label "zdanie"
  ]
  node [
    id 1206
    label "nast&#281;pnik"
  ]
  node [
    id 1207
    label "condition"
  ]
  node [
    id 1208
    label "jura"
  ]
  node [
    id 1209
    label "glacja&#322;"
  ]
  node [
    id 1210
    label "sten"
  ]
  node [
    id 1211
    label "era"
  ]
  node [
    id 1212
    label "trias"
  ]
  node [
    id 1213
    label "p&#243;&#322;okres"
  ]
  node [
    id 1214
    label "rok_szkolny"
  ]
  node [
    id 1215
    label "dewon"
  ]
  node [
    id 1216
    label "karbon"
  ]
  node [
    id 1217
    label "izochronizm"
  ]
  node [
    id 1218
    label "preglacja&#322;"
  ]
  node [
    id 1219
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 1220
    label "drugorz&#281;d"
  ]
  node [
    id 1221
    label "semester"
  ]
  node [
    id 1222
    label "zniewie&#347;cialec"
  ]
  node [
    id 1223
    label "oferma"
  ]
  node [
    id 1224
    label "miesi&#261;czka"
  ]
  node [
    id 1225
    label "gej"
  ]
  node [
    id 1226
    label "pedalstwo"
  ]
  node [
    id 1227
    label "mazgaj"
  ]
  node [
    id 1228
    label "fraza"
  ]
  node [
    id 1229
    label "stanowisko"
  ]
  node [
    id 1230
    label "prison_term"
  ]
  node [
    id 1231
    label "zaliczenie"
  ]
  node [
    id 1232
    label "antylogizm"
  ]
  node [
    id 1233
    label "zmuszenie"
  ]
  node [
    id 1234
    label "konektyw"
  ]
  node [
    id 1235
    label "attitude"
  ]
  node [
    id 1236
    label "adjudication"
  ]
  node [
    id 1237
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 1238
    label "kres"
  ]
  node [
    id 1239
    label "aalen"
  ]
  node [
    id 1240
    label "jura_wczesna"
  ]
  node [
    id 1241
    label "holocen"
  ]
  node [
    id 1242
    label "pliocen"
  ]
  node [
    id 1243
    label "plejstocen"
  ]
  node [
    id 1244
    label "paleocen"
  ]
  node [
    id 1245
    label "bajos"
  ]
  node [
    id 1246
    label "kelowej"
  ]
  node [
    id 1247
    label "eocen"
  ]
  node [
    id 1248
    label "miocen"
  ]
  node [
    id 1249
    label "&#347;rodkowy_trias"
  ]
  node [
    id 1250
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 1251
    label "wczesny_trias"
  ]
  node [
    id 1252
    label "jura_&#347;rodkowa"
  ]
  node [
    id 1253
    label "oligocen"
  ]
  node [
    id 1254
    label "implikacja"
  ]
  node [
    id 1255
    label "argument"
  ]
  node [
    id 1256
    label "pasemko"
  ]
  node [
    id 1257
    label "znak_diakrytyczny"
  ]
  node [
    id 1258
    label "zafalowanie"
  ]
  node [
    id 1259
    label "kot"
  ]
  node [
    id 1260
    label "przemoc"
  ]
  node [
    id 1261
    label "reakcja"
  ]
  node [
    id 1262
    label "strumie&#324;"
  ]
  node [
    id 1263
    label "karb"
  ]
  node [
    id 1264
    label "mn&#243;stwo"
  ]
  node [
    id 1265
    label "fit"
  ]
  node [
    id 1266
    label "grzywa_fali"
  ]
  node [
    id 1267
    label "woda"
  ]
  node [
    id 1268
    label "efekt_Dopplera"
  ]
  node [
    id 1269
    label "obcinka"
  ]
  node [
    id 1270
    label "t&#322;um"
  ]
  node [
    id 1271
    label "stream"
  ]
  node [
    id 1272
    label "zafalowa&#263;"
  ]
  node [
    id 1273
    label "rozbicie_si&#281;"
  ]
  node [
    id 1274
    label "clutter"
  ]
  node [
    id 1275
    label "rozbijanie_si&#281;"
  ]
  node [
    id 1276
    label "czo&#322;o_fali"
  ]
  node [
    id 1277
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 1278
    label "serce"
  ]
  node [
    id 1279
    label "ripple"
  ]
  node [
    id 1280
    label "pracowanie"
  ]
  node [
    id 1281
    label "zabicie"
  ]
  node [
    id 1282
    label "przebieg"
  ]
  node [
    id 1283
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 1284
    label "owulacja"
  ]
  node [
    id 1285
    label "sekwencja"
  ]
  node [
    id 1286
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 1287
    label "edycja"
  ]
  node [
    id 1288
    label "cycle"
  ]
  node [
    id 1289
    label "nauka_humanistyczna"
  ]
  node [
    id 1290
    label "erystyka"
  ]
  node [
    id 1291
    label "chironomia"
  ]
  node [
    id 1292
    label "elokwencja"
  ]
  node [
    id 1293
    label "elokucja"
  ]
  node [
    id 1294
    label "tropika"
  ]
  node [
    id 1295
    label "formacja_geologiczna"
  ]
  node [
    id 1296
    label "era_paleozoiczna"
  ]
  node [
    id 1297
    label "ludlow"
  ]
  node [
    id 1298
    label "moneta"
  ]
  node [
    id 1299
    label "paleoproterozoik"
  ]
  node [
    id 1300
    label "zlodowacenie"
  ]
  node [
    id 1301
    label "asteroksylon"
  ]
  node [
    id 1302
    label "pluwia&#322;"
  ]
  node [
    id 1303
    label "mezoproterozoik"
  ]
  node [
    id 1304
    label "era_kenozoiczna"
  ]
  node [
    id 1305
    label "p&#243;&#378;ny_trias"
  ]
  node [
    id 1306
    label "ret"
  ]
  node [
    id 1307
    label "era_mezozoiczna"
  ]
  node [
    id 1308
    label "konodont"
  ]
  node [
    id 1309
    label "kajper"
  ]
  node [
    id 1310
    label "neoproterozoik"
  ]
  node [
    id 1311
    label "chalk"
  ]
  node [
    id 1312
    label "narz&#281;dzie"
  ]
  node [
    id 1313
    label "turon"
  ]
  node [
    id 1314
    label "santon"
  ]
  node [
    id 1315
    label "cenoman"
  ]
  node [
    id 1316
    label "pobia&#322;ka"
  ]
  node [
    id 1317
    label "apt"
  ]
  node [
    id 1318
    label "ska&#322;a_organogeniczna"
  ]
  node [
    id 1319
    label "alb"
  ]
  node [
    id 1320
    label "pastel"
  ]
  node [
    id 1321
    label "neokom"
  ]
  node [
    id 1322
    label "pteranodon"
  ]
  node [
    id 1323
    label "wieloton"
  ]
  node [
    id 1324
    label "tu&#324;czyk"
  ]
  node [
    id 1325
    label "zabarwienie"
  ]
  node [
    id 1326
    label "interwa&#322;"
  ]
  node [
    id 1327
    label "modalizm"
  ]
  node [
    id 1328
    label "ubarwienie"
  ]
  node [
    id 1329
    label "note"
  ]
  node [
    id 1330
    label "formality"
  ]
  node [
    id 1331
    label "glinka"
  ]
  node [
    id 1332
    label "sound"
  ]
  node [
    id 1333
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1334
    label "zwyczaj"
  ]
  node [
    id 1335
    label "solmizacja"
  ]
  node [
    id 1336
    label "seria"
  ]
  node [
    id 1337
    label "tone"
  ]
  node [
    id 1338
    label "kolorystyka"
  ]
  node [
    id 1339
    label "r&#243;&#380;nica"
  ]
  node [
    id 1340
    label "akcent"
  ]
  node [
    id 1341
    label "repetycja"
  ]
  node [
    id 1342
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1343
    label "heksachord"
  ]
  node [
    id 1344
    label "rejestr"
  ]
  node [
    id 1345
    label "pistolet_maszynowy"
  ]
  node [
    id 1346
    label "jednostka_si&#322;y"
  ]
  node [
    id 1347
    label "pozauk&#322;adowa_jednostka_miary"
  ]
  node [
    id 1348
    label "wapie&#324;_pi&#324;czowski"
  ]
  node [
    id 1349
    label "pensylwan"
  ]
  node [
    id 1350
    label "tworzywo"
  ]
  node [
    id 1351
    label "mezozaur"
  ]
  node [
    id 1352
    label "pikaia"
  ]
  node [
    id 1353
    label "era_eozoiczna"
  ]
  node [
    id 1354
    label "rand"
  ]
  node [
    id 1355
    label "era_archaiczna"
  ]
  node [
    id 1356
    label "huron"
  ]
  node [
    id 1357
    label "Permian"
  ]
  node [
    id 1358
    label "blokada"
  ]
  node [
    id 1359
    label "cechsztyn"
  ]
  node [
    id 1360
    label "plezjozaur"
  ]
  node [
    id 1361
    label "euoplocefal"
  ]
  node [
    id 1362
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1363
    label "eon"
  ]
  node [
    id 1364
    label "podnosi&#263;"
  ]
  node [
    id 1365
    label "liczy&#263;"
  ]
  node [
    id 1366
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 1367
    label "zanosi&#263;"
  ]
  node [
    id 1368
    label "rozpowszechnia&#263;"
  ]
  node [
    id 1369
    label "ujawnia&#263;"
  ]
  node [
    id 1370
    label "otrzymywa&#263;"
  ]
  node [
    id 1371
    label "kra&#347;&#263;"
  ]
  node [
    id 1372
    label "nasi&#261;ka&#263;"
  ]
  node [
    id 1373
    label "raise"
  ]
  node [
    id 1374
    label "odsuwa&#263;"
  ]
  node [
    id 1375
    label "nagradza&#263;"
  ]
  node [
    id 1376
    label "forytowa&#263;"
  ]
  node [
    id 1377
    label "traktowa&#263;"
  ]
  node [
    id 1378
    label "sign"
  ]
  node [
    id 1379
    label "podpierdala&#263;"
  ]
  node [
    id 1380
    label "mie&#263;_lepkie_r&#281;ce"
  ]
  node [
    id 1381
    label "r&#261;ba&#263;"
  ]
  node [
    id 1382
    label "podsuwa&#263;"
  ]
  node [
    id 1383
    label "overcharge"
  ]
  node [
    id 1384
    label "zar&#261;bywa&#263;"
  ]
  node [
    id 1385
    label "przyw&#322;aszcza&#263;"
  ]
  node [
    id 1386
    label "return"
  ]
  node [
    id 1387
    label "dostawa&#263;"
  ]
  node [
    id 1388
    label "take"
  ]
  node [
    id 1389
    label "wytwarza&#263;"
  ]
  node [
    id 1390
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 1391
    label "przejmowa&#263;"
  ]
  node [
    id 1392
    label "saturate"
  ]
  node [
    id 1393
    label "report"
  ]
  node [
    id 1394
    label "dyskalkulia"
  ]
  node [
    id 1395
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1396
    label "wynagrodzenie"
  ]
  node [
    id 1397
    label "wymienia&#263;"
  ]
  node [
    id 1398
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1399
    label "wycenia&#263;"
  ]
  node [
    id 1400
    label "bra&#263;"
  ]
  node [
    id 1401
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 1402
    label "mierzy&#263;"
  ]
  node [
    id 1403
    label "rachowa&#263;"
  ]
  node [
    id 1404
    label "odlicza&#263;"
  ]
  node [
    id 1405
    label "dodawa&#263;"
  ]
  node [
    id 1406
    label "wyznacza&#263;"
  ]
  node [
    id 1407
    label "admit"
  ]
  node [
    id 1408
    label "policza&#263;"
  ]
  node [
    id 1409
    label "dostarcza&#263;"
  ]
  node [
    id 1410
    label "kry&#263;"
  ]
  node [
    id 1411
    label "przenosi&#263;"
  ]
  node [
    id 1412
    label "usi&#322;owa&#263;"
  ]
  node [
    id 1413
    label "remove"
  ]
  node [
    id 1414
    label "seclude"
  ]
  node [
    id 1415
    label "przemieszcza&#263;"
  ]
  node [
    id 1416
    label "przesuwa&#263;"
  ]
  node [
    id 1417
    label "oddala&#263;"
  ]
  node [
    id 1418
    label "dissolve"
  ]
  node [
    id 1419
    label "odw&#322;&#243;czy&#263;"
  ]
  node [
    id 1420
    label "retard"
  ]
  node [
    id 1421
    label "blurt_out"
  ]
  node [
    id 1422
    label "odci&#261;ga&#263;"
  ]
  node [
    id 1423
    label "odk&#322;ada&#263;"
  ]
  node [
    id 1424
    label "demaskator"
  ]
  node [
    id 1425
    label "dostrzega&#263;"
  ]
  node [
    id 1426
    label "objawia&#263;"
  ]
  node [
    id 1427
    label "unwrap"
  ]
  node [
    id 1428
    label "indicate"
  ]
  node [
    id 1429
    label "generalize"
  ]
  node [
    id 1430
    label "sprawia&#263;"
  ]
  node [
    id 1431
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 1432
    label "zaczyna&#263;"
  ]
  node [
    id 1433
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 1434
    label "escalate"
  ]
  node [
    id 1435
    label "pia&#263;"
  ]
  node [
    id 1436
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 1437
    label "przybli&#380;a&#263;"
  ]
  node [
    id 1438
    label "ulepsza&#263;"
  ]
  node [
    id 1439
    label "tire"
  ]
  node [
    id 1440
    label "pomaga&#263;"
  ]
  node [
    id 1441
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 1442
    label "chwali&#263;"
  ]
  node [
    id 1443
    label "os&#322;awia&#263;"
  ]
  node [
    id 1444
    label "odbudowywa&#263;"
  ]
  node [
    id 1445
    label "drive"
  ]
  node [
    id 1446
    label "zmienia&#263;"
  ]
  node [
    id 1447
    label "enhance"
  ]
  node [
    id 1448
    label "za&#322;apywa&#263;"
  ]
  node [
    id 1449
    label "lift"
  ]
  node [
    id 1450
    label "wynie&#347;&#263;"
  ]
  node [
    id 1451
    label "pieni&#261;dze"
  ]
  node [
    id 1452
    label "ilo&#347;&#263;"
  ]
  node [
    id 1453
    label "limit"
  ]
  node [
    id 1454
    label "nieznaczny"
  ]
  node [
    id 1455
    label "pomiernie"
  ]
  node [
    id 1456
    label "kr&#243;tko"
  ]
  node [
    id 1457
    label "mikroskopijnie"
  ]
  node [
    id 1458
    label "nieliczny"
  ]
  node [
    id 1459
    label "mo&#380;liwie"
  ]
  node [
    id 1460
    label "nieistotnie"
  ]
  node [
    id 1461
    label "ma&#322;y"
  ]
  node [
    id 1462
    label "niepowa&#380;nie"
  ]
  node [
    id 1463
    label "niewa&#380;ny"
  ]
  node [
    id 1464
    label "mo&#380;liwy"
  ]
  node [
    id 1465
    label "zno&#347;nie"
  ]
  node [
    id 1466
    label "kr&#243;tki"
  ]
  node [
    id 1467
    label "nieznacznie"
  ]
  node [
    id 1468
    label "drobnostkowy"
  ]
  node [
    id 1469
    label "malusie&#324;ko"
  ]
  node [
    id 1470
    label "mikroskopijny"
  ]
  node [
    id 1471
    label "szybki"
  ]
  node [
    id 1472
    label "przeci&#281;tny"
  ]
  node [
    id 1473
    label "wstydliwy"
  ]
  node [
    id 1474
    label "s&#322;aby"
  ]
  node [
    id 1475
    label "ch&#322;opiec"
  ]
  node [
    id 1476
    label "m&#322;ody"
  ]
  node [
    id 1477
    label "marny"
  ]
  node [
    id 1478
    label "n&#281;dznie"
  ]
  node [
    id 1479
    label "nielicznie"
  ]
  node [
    id 1480
    label "licho"
  ]
  node [
    id 1481
    label "proporcjonalnie"
  ]
  node [
    id 1482
    label "pomierny"
  ]
  node [
    id 1483
    label "miernie"
  ]
  node [
    id 1484
    label "sprawdza&#263;"
  ]
  node [
    id 1485
    label "spadkobierca"
  ]
  node [
    id 1486
    label "powodowa&#263;"
  ]
  node [
    id 1487
    label "uznawa&#263;"
  ]
  node [
    id 1488
    label "authorize"
  ]
  node [
    id 1489
    label "os&#261;dza&#263;"
  ]
  node [
    id 1490
    label "consider"
  ]
  node [
    id 1491
    label "notice"
  ]
  node [
    id 1492
    label "stwierdza&#263;"
  ]
  node [
    id 1493
    label "przyznawa&#263;"
  ]
  node [
    id 1494
    label "examine"
  ]
  node [
    id 1495
    label "szpiegowa&#263;"
  ]
  node [
    id 1496
    label "mie&#263;_miejsce"
  ]
  node [
    id 1497
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1498
    label "motywowa&#263;"
  ]
  node [
    id 1499
    label "organizowa&#263;"
  ]
  node [
    id 1500
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 1501
    label "czyni&#263;"
  ]
  node [
    id 1502
    label "stylizowa&#263;"
  ]
  node [
    id 1503
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 1504
    label "falowa&#263;"
  ]
  node [
    id 1505
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 1506
    label "peddle"
  ]
  node [
    id 1507
    label "praca"
  ]
  node [
    id 1508
    label "wydala&#263;"
  ]
  node [
    id 1509
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1510
    label "tentegowa&#263;"
  ]
  node [
    id 1511
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 1512
    label "urz&#261;dza&#263;"
  ]
  node [
    id 1513
    label "oszukiwa&#263;"
  ]
  node [
    id 1514
    label "work"
  ]
  node [
    id 1515
    label "ukazywa&#263;"
  ]
  node [
    id 1516
    label "przerabia&#263;"
  ]
  node [
    id 1517
    label "post&#281;powa&#263;"
  ]
  node [
    id 1518
    label "podmiot"
  ]
  node [
    id 1519
    label "legitymowanie"
  ]
  node [
    id 1520
    label "parametr"
  ]
  node [
    id 1521
    label "warunek_lokalowy"
  ]
  node [
    id 1522
    label "dane"
  ]
  node [
    id 1523
    label "dymensja"
  ]
  node [
    id 1524
    label "strona"
  ]
  node [
    id 1525
    label "m&#322;ot"
  ]
  node [
    id 1526
    label "znak"
  ]
  node [
    id 1527
    label "drzewo"
  ]
  node [
    id 1528
    label "pr&#243;ba"
  ]
  node [
    id 1529
    label "attribute"
  ]
  node [
    id 1530
    label "marka"
  ]
  node [
    id 1531
    label "rozmiar"
  ]
  node [
    id 1532
    label "rzadko&#347;&#263;"
  ]
  node [
    id 1533
    label "zaleta"
  ]
  node [
    id 1534
    label "measure"
  ]
  node [
    id 1535
    label "opinia"
  ]
  node [
    id 1536
    label "poj&#281;cie"
  ]
  node [
    id 1537
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 1538
    label "potencja"
  ]
  node [
    id 1539
    label "property"
  ]
  node [
    id 1540
    label "odk&#322;adanie"
  ]
  node [
    id 1541
    label "liczenie"
  ]
  node [
    id 1542
    label "stawianie"
  ]
  node [
    id 1543
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 1544
    label "assay"
  ]
  node [
    id 1545
    label "wskazywanie"
  ]
  node [
    id 1546
    label "wyraz"
  ]
  node [
    id 1547
    label "gravity"
  ]
  node [
    id 1548
    label "weight"
  ]
  node [
    id 1549
    label "command"
  ]
  node [
    id 1550
    label "odgrywanie_roli"
  ]
  node [
    id 1551
    label "okre&#347;lanie"
  ]
  node [
    id 1552
    label "kartka"
  ]
  node [
    id 1553
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1554
    label "logowanie"
  ]
  node [
    id 1555
    label "plik"
  ]
  node [
    id 1556
    label "adres_internetowy"
  ]
  node [
    id 1557
    label "linia"
  ]
  node [
    id 1558
    label "serwis_internetowy"
  ]
  node [
    id 1559
    label "bok"
  ]
  node [
    id 1560
    label "skr&#281;canie"
  ]
  node [
    id 1561
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1562
    label "orientowanie"
  ]
  node [
    id 1563
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1564
    label "zorientowanie"
  ]
  node [
    id 1565
    label "ty&#322;"
  ]
  node [
    id 1566
    label "fragment"
  ]
  node [
    id 1567
    label "layout"
  ]
  node [
    id 1568
    label "zorientowa&#263;"
  ]
  node [
    id 1569
    label "pagina"
  ]
  node [
    id 1570
    label "orientowa&#263;"
  ]
  node [
    id 1571
    label "voice"
  ]
  node [
    id 1572
    label "orientacja"
  ]
  node [
    id 1573
    label "internet"
  ]
  node [
    id 1574
    label "powierzchnia"
  ]
  node [
    id 1575
    label "forma"
  ]
  node [
    id 1576
    label "skr&#281;cenie"
  ]
  node [
    id 1577
    label "edytowa&#263;"
  ]
  node [
    id 1578
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 1579
    label "spakowanie"
  ]
  node [
    id 1580
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1581
    label "pakowa&#263;"
  ]
  node [
    id 1582
    label "rekord"
  ]
  node [
    id 1583
    label "korelator"
  ]
  node [
    id 1584
    label "wyci&#261;ganie"
  ]
  node [
    id 1585
    label "pakowanie"
  ]
  node [
    id 1586
    label "sekwencjonowa&#263;"
  ]
  node [
    id 1587
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 1588
    label "jednostka_informacji"
  ]
  node [
    id 1589
    label "evidence"
  ]
  node [
    id 1590
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 1591
    label "rozpakowywanie"
  ]
  node [
    id 1592
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1593
    label "rozpakowanie"
  ]
  node [
    id 1594
    label "rozpakowywa&#263;"
  ]
  node [
    id 1595
    label "konwersja"
  ]
  node [
    id 1596
    label "nap&#322;ywanie"
  ]
  node [
    id 1597
    label "rozpakowa&#263;"
  ]
  node [
    id 1598
    label "spakowa&#263;"
  ]
  node [
    id 1599
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 1600
    label "edytowanie"
  ]
  node [
    id 1601
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 1602
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 1603
    label "sekwencjonowanie"
  ]
  node [
    id 1604
    label "kategoria"
  ]
  node [
    id 1605
    label "pierwiastek"
  ]
  node [
    id 1606
    label "number"
  ]
  node [
    id 1607
    label "kwadrat_magiczny"
  ]
  node [
    id 1608
    label "part"
  ]
  node [
    id 1609
    label "zmienna"
  ]
  node [
    id 1610
    label "pacjent"
  ]
  node [
    id 1611
    label "happening"
  ]
  node [
    id 1612
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 1613
    label "schorzenie"
  ]
  node [
    id 1614
    label "przyk&#322;ad"
  ]
  node [
    id 1615
    label "fakt"
  ]
  node [
    id 1616
    label "ilustracja"
  ]
  node [
    id 1617
    label "przebiec"
  ]
  node [
    id 1618
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1619
    label "motyw"
  ]
  node [
    id 1620
    label "przebiegni&#281;cie"
  ]
  node [
    id 1621
    label "fabu&#322;a"
  ]
  node [
    id 1622
    label "rzuci&#263;"
  ]
  node [
    id 1623
    label "destiny"
  ]
  node [
    id 1624
    label "ustalenie"
  ]
  node [
    id 1625
    label "przymus"
  ]
  node [
    id 1626
    label "przydzielenie"
  ]
  node [
    id 1627
    label "p&#243;j&#347;cie"
  ]
  node [
    id 1628
    label "oblat"
  ]
  node [
    id 1629
    label "obowi&#261;zek"
  ]
  node [
    id 1630
    label "rzucenie"
  ]
  node [
    id 1631
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 1632
    label "wybranie"
  ]
  node [
    id 1633
    label "ognisko"
  ]
  node [
    id 1634
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 1635
    label "powalenie"
  ]
  node [
    id 1636
    label "odezwanie_si&#281;"
  ]
  node [
    id 1637
    label "atakowanie"
  ]
  node [
    id 1638
    label "grupa_ryzyka"
  ]
  node [
    id 1639
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 1640
    label "nabawienie_si&#281;"
  ]
  node [
    id 1641
    label "inkubacja"
  ]
  node [
    id 1642
    label "kryzys"
  ]
  node [
    id 1643
    label "powali&#263;"
  ]
  node [
    id 1644
    label "remisja"
  ]
  node [
    id 1645
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 1646
    label "zajmowa&#263;"
  ]
  node [
    id 1647
    label "zaburzenie"
  ]
  node [
    id 1648
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 1649
    label "badanie_histopatologiczne"
  ]
  node [
    id 1650
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 1651
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 1652
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1653
    label "odzywanie_si&#281;"
  ]
  node [
    id 1654
    label "diagnoza"
  ]
  node [
    id 1655
    label "atakowa&#263;"
  ]
  node [
    id 1656
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1657
    label "nabawianie_si&#281;"
  ]
  node [
    id 1658
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 1659
    label "zajmowanie"
  ]
  node [
    id 1660
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 1661
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 1662
    label "klient"
  ]
  node [
    id 1663
    label "piel&#281;gniarz"
  ]
  node [
    id 1664
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 1665
    label "od&#322;&#261;czanie"
  ]
  node [
    id 1666
    label "od&#322;&#261;czenie"
  ]
  node [
    id 1667
    label "chory"
  ]
  node [
    id 1668
    label "szpitalnik"
  ]
  node [
    id 1669
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 1670
    label "&#380;ona"
  ]
  node [
    id 1671
    label "samica"
  ]
  node [
    id 1672
    label "uleganie"
  ]
  node [
    id 1673
    label "ulec"
  ]
  node [
    id 1674
    label "m&#281;&#380;yna"
  ]
  node [
    id 1675
    label "partnerka"
  ]
  node [
    id 1676
    label "ulegni&#281;cie"
  ]
  node [
    id 1677
    label "pa&#324;stwo"
  ]
  node [
    id 1678
    label "&#322;ono"
  ]
  node [
    id 1679
    label "menopauza"
  ]
  node [
    id 1680
    label "przekwitanie"
  ]
  node [
    id 1681
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 1682
    label "babka"
  ]
  node [
    id 1683
    label "ulega&#263;"
  ]
  node [
    id 1684
    label "wydoro&#347;lenie"
  ]
  node [
    id 1685
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1686
    label "doro&#347;lenie"
  ]
  node [
    id 1687
    label "&#378;ra&#322;y"
  ]
  node [
    id 1688
    label "doro&#347;le"
  ]
  node [
    id 1689
    label "senior"
  ]
  node [
    id 1690
    label "dojrzale"
  ]
  node [
    id 1691
    label "wapniak"
  ]
  node [
    id 1692
    label "dojrza&#322;y"
  ]
  node [
    id 1693
    label "m&#261;dry"
  ]
  node [
    id 1694
    label "doletni"
  ]
  node [
    id 1695
    label "os&#322;abia&#263;"
  ]
  node [
    id 1696
    label "hominid"
  ]
  node [
    id 1697
    label "podw&#322;adny"
  ]
  node [
    id 1698
    label "os&#322;abianie"
  ]
  node [
    id 1699
    label "dwun&#243;g"
  ]
  node [
    id 1700
    label "nasada"
  ]
  node [
    id 1701
    label "wz&#243;r"
  ]
  node [
    id 1702
    label "Adam"
  ]
  node [
    id 1703
    label "polifag"
  ]
  node [
    id 1704
    label "&#347;lubna"
  ]
  node [
    id 1705
    label "kobita"
  ]
  node [
    id 1706
    label "panna_m&#322;oda"
  ]
  node [
    id 1707
    label "aktorka"
  ]
  node [
    id 1708
    label "partner"
  ]
  node [
    id 1709
    label "poddanie_si&#281;"
  ]
  node [
    id 1710
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 1711
    label "poddanie"
  ]
  node [
    id 1712
    label "nagi&#281;cie_si&#281;"
  ]
  node [
    id 1713
    label "pozwolenie"
  ]
  node [
    id 1714
    label "subjugation"
  ]
  node [
    id 1715
    label "stanie_si&#281;"
  ]
  node [
    id 1716
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 1717
    label "przywo&#322;a&#263;"
  ]
  node [
    id 1718
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 1719
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 1720
    label "poddawa&#263;"
  ]
  node [
    id 1721
    label "postpone"
  ]
  node [
    id 1722
    label "render"
  ]
  node [
    id 1723
    label "zezwala&#263;"
  ]
  node [
    id 1724
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 1725
    label "subject"
  ]
  node [
    id 1726
    label "kwitnienie"
  ]
  node [
    id 1727
    label "przemijanie"
  ]
  node [
    id 1728
    label "przestawanie"
  ]
  node [
    id 1729
    label "starzenie_si&#281;"
  ]
  node [
    id 1730
    label "menopause"
  ]
  node [
    id 1731
    label "obumieranie"
  ]
  node [
    id 1732
    label "dojrzewanie"
  ]
  node [
    id 1733
    label "zezwalanie"
  ]
  node [
    id 1734
    label "zaliczanie"
  ]
  node [
    id 1735
    label "podporz&#261;dkowywanie_si&#281;"
  ]
  node [
    id 1736
    label "poddawanie"
  ]
  node [
    id 1737
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 1738
    label "burst"
  ]
  node [
    id 1739
    label "przywo&#322;anie"
  ]
  node [
    id 1740
    label "naginanie_si&#281;"
  ]
  node [
    id 1741
    label "poddawanie_si&#281;"
  ]
  node [
    id 1742
    label "stawanie_si&#281;"
  ]
  node [
    id 1743
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 1744
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1745
    label "fall"
  ]
  node [
    id 1746
    label "pozwoli&#263;"
  ]
  node [
    id 1747
    label "podda&#263;"
  ]
  node [
    id 1748
    label "put_in"
  ]
  node [
    id 1749
    label "podda&#263;_si&#281;"
  ]
  node [
    id 1750
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1751
    label "Katar"
  ]
  node [
    id 1752
    label "Libia"
  ]
  node [
    id 1753
    label "Gwatemala"
  ]
  node [
    id 1754
    label "Ekwador"
  ]
  node [
    id 1755
    label "Afganistan"
  ]
  node [
    id 1756
    label "Tad&#380;ykistan"
  ]
  node [
    id 1757
    label "Bhutan"
  ]
  node [
    id 1758
    label "Argentyna"
  ]
  node [
    id 1759
    label "D&#380;ibuti"
  ]
  node [
    id 1760
    label "Wenezuela"
  ]
  node [
    id 1761
    label "Gabon"
  ]
  node [
    id 1762
    label "Ukraina"
  ]
  node [
    id 1763
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1764
    label "Rwanda"
  ]
  node [
    id 1765
    label "Liechtenstein"
  ]
  node [
    id 1766
    label "organizacja"
  ]
  node [
    id 1767
    label "Sri_Lanka"
  ]
  node [
    id 1768
    label "Madagaskar"
  ]
  node [
    id 1769
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1770
    label "Kongo"
  ]
  node [
    id 1771
    label "Tonga"
  ]
  node [
    id 1772
    label "Bangladesz"
  ]
  node [
    id 1773
    label "Kanada"
  ]
  node [
    id 1774
    label "Wehrlen"
  ]
  node [
    id 1775
    label "Algieria"
  ]
  node [
    id 1776
    label "Uganda"
  ]
  node [
    id 1777
    label "Surinam"
  ]
  node [
    id 1778
    label "Sahara_Zachodnia"
  ]
  node [
    id 1779
    label "Chile"
  ]
  node [
    id 1780
    label "W&#281;gry"
  ]
  node [
    id 1781
    label "Birma"
  ]
  node [
    id 1782
    label "Kazachstan"
  ]
  node [
    id 1783
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1784
    label "Armenia"
  ]
  node [
    id 1785
    label "Tuwalu"
  ]
  node [
    id 1786
    label "Timor_Wschodni"
  ]
  node [
    id 1787
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1788
    label "Izrael"
  ]
  node [
    id 1789
    label "Estonia"
  ]
  node [
    id 1790
    label "Komory"
  ]
  node [
    id 1791
    label "Kamerun"
  ]
  node [
    id 1792
    label "Haiti"
  ]
  node [
    id 1793
    label "Belize"
  ]
  node [
    id 1794
    label "Sierra_Leone"
  ]
  node [
    id 1795
    label "Luksemburg"
  ]
  node [
    id 1796
    label "USA"
  ]
  node [
    id 1797
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1798
    label "Barbados"
  ]
  node [
    id 1799
    label "San_Marino"
  ]
  node [
    id 1800
    label "Bu&#322;garia"
  ]
  node [
    id 1801
    label "Indonezja"
  ]
  node [
    id 1802
    label "Wietnam"
  ]
  node [
    id 1803
    label "Malawi"
  ]
  node [
    id 1804
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1805
    label "Francja"
  ]
  node [
    id 1806
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1807
    label "partia"
  ]
  node [
    id 1808
    label "Zambia"
  ]
  node [
    id 1809
    label "Angola"
  ]
  node [
    id 1810
    label "Grenada"
  ]
  node [
    id 1811
    label "Nepal"
  ]
  node [
    id 1812
    label "Panama"
  ]
  node [
    id 1813
    label "Rumunia"
  ]
  node [
    id 1814
    label "Czarnog&#243;ra"
  ]
  node [
    id 1815
    label "Malediwy"
  ]
  node [
    id 1816
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1817
    label "S&#322;owacja"
  ]
  node [
    id 1818
    label "para"
  ]
  node [
    id 1819
    label "Egipt"
  ]
  node [
    id 1820
    label "zwrot"
  ]
  node [
    id 1821
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1822
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1823
    label "Mozambik"
  ]
  node [
    id 1824
    label "Kolumbia"
  ]
  node [
    id 1825
    label "Laos"
  ]
  node [
    id 1826
    label "Burundi"
  ]
  node [
    id 1827
    label "Suazi"
  ]
  node [
    id 1828
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1829
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1830
    label "Czechy"
  ]
  node [
    id 1831
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1832
    label "Wyspy_Marshalla"
  ]
  node [
    id 1833
    label "Dominika"
  ]
  node [
    id 1834
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1835
    label "Syria"
  ]
  node [
    id 1836
    label "Palau"
  ]
  node [
    id 1837
    label "Gwinea_Bissau"
  ]
  node [
    id 1838
    label "Liberia"
  ]
  node [
    id 1839
    label "Jamajka"
  ]
  node [
    id 1840
    label "Zimbabwe"
  ]
  node [
    id 1841
    label "Polska"
  ]
  node [
    id 1842
    label "Dominikana"
  ]
  node [
    id 1843
    label "Senegal"
  ]
  node [
    id 1844
    label "Togo"
  ]
  node [
    id 1845
    label "Gujana"
  ]
  node [
    id 1846
    label "Gruzja"
  ]
  node [
    id 1847
    label "Albania"
  ]
  node [
    id 1848
    label "Zair"
  ]
  node [
    id 1849
    label "Meksyk"
  ]
  node [
    id 1850
    label "Macedonia"
  ]
  node [
    id 1851
    label "Chorwacja"
  ]
  node [
    id 1852
    label "Kambod&#380;a"
  ]
  node [
    id 1853
    label "Monako"
  ]
  node [
    id 1854
    label "Mauritius"
  ]
  node [
    id 1855
    label "Gwinea"
  ]
  node [
    id 1856
    label "Mali"
  ]
  node [
    id 1857
    label "Nigeria"
  ]
  node [
    id 1858
    label "Kostaryka"
  ]
  node [
    id 1859
    label "Hanower"
  ]
  node [
    id 1860
    label "Paragwaj"
  ]
  node [
    id 1861
    label "W&#322;ochy"
  ]
  node [
    id 1862
    label "Seszele"
  ]
  node [
    id 1863
    label "Wyspy_Salomona"
  ]
  node [
    id 1864
    label "Hiszpania"
  ]
  node [
    id 1865
    label "Boliwia"
  ]
  node [
    id 1866
    label "Kirgistan"
  ]
  node [
    id 1867
    label "Irlandia"
  ]
  node [
    id 1868
    label "Czad"
  ]
  node [
    id 1869
    label "Irak"
  ]
  node [
    id 1870
    label "Lesoto"
  ]
  node [
    id 1871
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1872
    label "Malta"
  ]
  node [
    id 1873
    label "Andora"
  ]
  node [
    id 1874
    label "Chiny"
  ]
  node [
    id 1875
    label "Filipiny"
  ]
  node [
    id 1876
    label "Antarktis"
  ]
  node [
    id 1877
    label "Niemcy"
  ]
  node [
    id 1878
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1879
    label "Pakistan"
  ]
  node [
    id 1880
    label "terytorium"
  ]
  node [
    id 1881
    label "Nikaragua"
  ]
  node [
    id 1882
    label "Brazylia"
  ]
  node [
    id 1883
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1884
    label "Maroko"
  ]
  node [
    id 1885
    label "Portugalia"
  ]
  node [
    id 1886
    label "Niger"
  ]
  node [
    id 1887
    label "Kenia"
  ]
  node [
    id 1888
    label "Botswana"
  ]
  node [
    id 1889
    label "Fid&#380;i"
  ]
  node [
    id 1890
    label "Tunezja"
  ]
  node [
    id 1891
    label "Australia"
  ]
  node [
    id 1892
    label "Tajlandia"
  ]
  node [
    id 1893
    label "Burkina_Faso"
  ]
  node [
    id 1894
    label "interior"
  ]
  node [
    id 1895
    label "Tanzania"
  ]
  node [
    id 1896
    label "Benin"
  ]
  node [
    id 1897
    label "Indie"
  ]
  node [
    id 1898
    label "&#321;otwa"
  ]
  node [
    id 1899
    label "Kiribati"
  ]
  node [
    id 1900
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1901
    label "Rodezja"
  ]
  node [
    id 1902
    label "Cypr"
  ]
  node [
    id 1903
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1904
    label "Peru"
  ]
  node [
    id 1905
    label "Austria"
  ]
  node [
    id 1906
    label "Urugwaj"
  ]
  node [
    id 1907
    label "Jordania"
  ]
  node [
    id 1908
    label "Grecja"
  ]
  node [
    id 1909
    label "Azerbejd&#380;an"
  ]
  node [
    id 1910
    label "Turcja"
  ]
  node [
    id 1911
    label "Samoa"
  ]
  node [
    id 1912
    label "Sudan"
  ]
  node [
    id 1913
    label "Oman"
  ]
  node [
    id 1914
    label "ziemia"
  ]
  node [
    id 1915
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1916
    label "Uzbekistan"
  ]
  node [
    id 1917
    label "Portoryko"
  ]
  node [
    id 1918
    label "Honduras"
  ]
  node [
    id 1919
    label "Mongolia"
  ]
  node [
    id 1920
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1921
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1922
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1923
    label "Serbia"
  ]
  node [
    id 1924
    label "Tajwan"
  ]
  node [
    id 1925
    label "Wielka_Brytania"
  ]
  node [
    id 1926
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1927
    label "Liban"
  ]
  node [
    id 1928
    label "Japonia"
  ]
  node [
    id 1929
    label "Ghana"
  ]
  node [
    id 1930
    label "Belgia"
  ]
  node [
    id 1931
    label "Bahrajn"
  ]
  node [
    id 1932
    label "Mikronezja"
  ]
  node [
    id 1933
    label "Etiopia"
  ]
  node [
    id 1934
    label "Kuwejt"
  ]
  node [
    id 1935
    label "Bahamy"
  ]
  node [
    id 1936
    label "Rosja"
  ]
  node [
    id 1937
    label "Mo&#322;dawia"
  ]
  node [
    id 1938
    label "Litwa"
  ]
  node [
    id 1939
    label "S&#322;owenia"
  ]
  node [
    id 1940
    label "Szwajcaria"
  ]
  node [
    id 1941
    label "Erytrea"
  ]
  node [
    id 1942
    label "Arabia_Saudyjska"
  ]
  node [
    id 1943
    label "Kuba"
  ]
  node [
    id 1944
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1945
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1946
    label "Malezja"
  ]
  node [
    id 1947
    label "Korea"
  ]
  node [
    id 1948
    label "Jemen"
  ]
  node [
    id 1949
    label "Nowa_Zelandia"
  ]
  node [
    id 1950
    label "Namibia"
  ]
  node [
    id 1951
    label "Nauru"
  ]
  node [
    id 1952
    label "holoarktyka"
  ]
  node [
    id 1953
    label "Brunei"
  ]
  node [
    id 1954
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1955
    label "Khitai"
  ]
  node [
    id 1956
    label "Mauretania"
  ]
  node [
    id 1957
    label "Iran"
  ]
  node [
    id 1958
    label "Gambia"
  ]
  node [
    id 1959
    label "Somalia"
  ]
  node [
    id 1960
    label "Holandia"
  ]
  node [
    id 1961
    label "Turkmenistan"
  ]
  node [
    id 1962
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1963
    label "Salwador"
  ]
  node [
    id 1964
    label "klatka_piersiowa"
  ]
  node [
    id 1965
    label "penis"
  ]
  node [
    id 1966
    label "wzg&#243;rek_&#322;onowy"
  ]
  node [
    id 1967
    label "brzuch"
  ]
  node [
    id 1968
    label "ow&#322;osienie_&#322;onowe"
  ]
  node [
    id 1969
    label "podbrzusze"
  ]
  node [
    id 1970
    label "l&#281;d&#378;wie"
  ]
  node [
    id 1971
    label "macica"
  ]
  node [
    id 1972
    label "pochwa"
  ]
  node [
    id 1973
    label "samka"
  ]
  node [
    id 1974
    label "p&#322;e&#263;_pi&#281;kna"
  ]
  node [
    id 1975
    label "drogi_rodne"
  ]
  node [
    id 1976
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 1977
    label "zwierz&#281;"
  ]
  node [
    id 1978
    label "female"
  ]
  node [
    id 1979
    label "przodkini"
  ]
  node [
    id 1980
    label "baba"
  ]
  node [
    id 1981
    label "babulinka"
  ]
  node [
    id 1982
    label "ciasto"
  ]
  node [
    id 1983
    label "ro&#347;lina_zielna"
  ]
  node [
    id 1984
    label "babkowate"
  ]
  node [
    id 1985
    label "po&#322;o&#380;na"
  ]
  node [
    id 1986
    label "dziadkowie"
  ]
  node [
    id 1987
    label "ryba"
  ]
  node [
    id 1988
    label "ko&#378;larz_babka"
  ]
  node [
    id 1989
    label "plantain"
  ]
  node [
    id 1990
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 1991
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 1992
    label "ojciec"
  ]
  node [
    id 1993
    label "jegomo&#347;&#263;"
  ]
  node [
    id 1994
    label "andropauza"
  ]
  node [
    id 1995
    label "bratek"
  ]
  node [
    id 1996
    label "ch&#322;opina"
  ]
  node [
    id 1997
    label "samiec"
  ]
  node [
    id 1998
    label "twardziel"
  ]
  node [
    id 1999
    label "androlog"
  ]
  node [
    id 2000
    label "pami&#281;&#263;"
  ]
  node [
    id 2001
    label "powierzchnia_zr&#243;wnania"
  ]
  node [
    id 2002
    label "zapalenie"
  ]
  node [
    id 2003
    label "drewno_wt&#243;rne"
  ]
  node [
    id 2004
    label "heartwood"
  ]
  node [
    id 2005
    label "monolit"
  ]
  node [
    id 2006
    label "klaster_dyskowy"
  ]
  node [
    id 2007
    label "komputer"
  ]
  node [
    id 2008
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 2009
    label "hard_disc"
  ]
  node [
    id 2010
    label "&#347;mia&#322;ek"
  ]
  node [
    id 2011
    label "mo&#347;&#263;"
  ]
  node [
    id 2012
    label "kszta&#322;ciciel"
  ]
  node [
    id 2013
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 2014
    label "kuwada"
  ]
  node [
    id 2015
    label "tworzyciel"
  ]
  node [
    id 2016
    label "rodzice"
  ]
  node [
    id 2017
    label "&#347;w"
  ]
  node [
    id 2018
    label "pomys&#322;odawca"
  ]
  node [
    id 2019
    label "rodzic"
  ]
  node [
    id 2020
    label "wykonawca"
  ]
  node [
    id 2021
    label "ojczym"
  ]
  node [
    id 2022
    label "przodek"
  ]
  node [
    id 2023
    label "papa"
  ]
  node [
    id 2024
    label "zakonnik"
  ]
  node [
    id 2025
    label "kochanek"
  ]
  node [
    id 2026
    label "fio&#322;ek"
  ]
  node [
    id 2027
    label "brat"
  ]
  node [
    id 2028
    label "specjalista"
  ]
  node [
    id 2029
    label "ustawa"
  ]
  node [
    id 2030
    label "zeszyt"
  ]
  node [
    id 2031
    label "20"
  ]
  node [
    id 2032
    label "1990"
  ]
  node [
    id 2033
    label "rok"
  ]
  node [
    id 2034
    label "spo&#322;eczny"
  ]
  node [
    id 2035
    label "rolnik"
  ]
  node [
    id 2036
    label "dziennik"
  ]
  node [
    id 2037
    label "u"
  ]
  node [
    id 2038
    label "26"
  ]
  node [
    id 2039
    label "lipiec"
  ]
  node [
    id 2040
    label "1991"
  ]
  node [
    id 2041
    label "podatek"
  ]
  node [
    id 2042
    label "dochodowy"
  ]
  node [
    id 2043
    label "od"
  ]
  node [
    id 2044
    label "fizyczny"
  ]
  node [
    id 2045
    label "1"
  ]
  node [
    id 2046
    label "1994"
  ]
  node [
    id 2047
    label "zasi&#322;ek"
  ]
  node [
    id 2048
    label "rodzinny"
  ]
  node [
    id 2049
    label "piel&#281;gnacyjny"
  ]
  node [
    id 2050
    label "i"
  ]
  node [
    id 2051
    label "wychowawczy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 2029
  ]
  edge [
    source 6
    target 2030
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 2038
  ]
  edge [
    source 6
    target 2039
  ]
  edge [
    source 6
    target 2040
  ]
  edge [
    source 6
    target 2033
  ]
  edge [
    source 6
    target 1992
  ]
  edge [
    source 6
    target 2041
  ]
  edge [
    source 6
    target 2042
  ]
  edge [
    source 6
    target 2043
  ]
  edge [
    source 6
    target 2044
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 757
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 772
  ]
  edge [
    source 11
    target 773
  ]
  edge [
    source 11
    target 774
  ]
  edge [
    source 11
    target 775
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 777
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 778
  ]
  edge [
    source 11
    target 779
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 781
  ]
  edge [
    source 11
    target 782
  ]
  edge [
    source 11
    target 783
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 204
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 214
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 824
  ]
  edge [
    source 11
    target 825
  ]
  edge [
    source 11
    target 826
  ]
  edge [
    source 11
    target 827
  ]
  edge [
    source 11
    target 828
  ]
  edge [
    source 11
    target 829
  ]
  edge [
    source 11
    target 830
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 831
  ]
  edge [
    source 11
    target 832
  ]
  edge [
    source 11
    target 833
  ]
  edge [
    source 11
    target 834
  ]
  edge [
    source 11
    target 835
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 836
  ]
  edge [
    source 11
    target 837
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 838
  ]
  edge [
    source 11
    target 839
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 840
  ]
  edge [
    source 11
    target 841
  ]
  edge [
    source 11
    target 842
  ]
  edge [
    source 11
    target 843
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 845
  ]
  edge [
    source 11
    target 846
  ]
  edge [
    source 11
    target 847
  ]
  edge [
    source 11
    target 848
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 850
  ]
  edge [
    source 11
    target 851
  ]
  edge [
    source 11
    target 852
  ]
  edge [
    source 11
    target 853
  ]
  edge [
    source 11
    target 854
  ]
  edge [
    source 11
    target 855
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 858
  ]
  edge [
    source 11
    target 859
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 861
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 879
  ]
  edge [
    source 11
    target 880
  ]
  edge [
    source 11
    target 881
  ]
  edge [
    source 11
    target 882
  ]
  edge [
    source 11
    target 883
  ]
  edge [
    source 11
    target 884
  ]
  edge [
    source 11
    target 2029
  ]
  edge [
    source 11
    target 2030
  ]
  edge [
    source 11
    target 2031
  ]
  edge [
    source 11
    target 2032
  ]
  edge [
    source 11
    target 2033
  ]
  edge [
    source 11
    target 1992
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 2034
  ]
  edge [
    source 11
    target 2035
  ]
  edge [
    source 11
    target 2038
  ]
  edge [
    source 11
    target 2039
  ]
  edge [
    source 11
    target 2040
  ]
  edge [
    source 11
    target 2041
  ]
  edge [
    source 11
    target 2042
  ]
  edge [
    source 11
    target 2043
  ]
  edge [
    source 11
    target 2044
  ]
  edge [
    source 11
    target 2045
  ]
  edge [
    source 11
    target 2046
  ]
  edge [
    source 11
    target 2047
  ]
  edge [
    source 11
    target 2048
  ]
  edge [
    source 11
    target 2049
  ]
  edge [
    source 11
    target 2050
  ]
  edge [
    source 11
    target 2051
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 12
    target 891
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 893
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 34
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 29
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 12
    target 914
  ]
  edge [
    source 12
    target 915
  ]
  edge [
    source 12
    target 916
  ]
  edge [
    source 12
    target 917
  ]
  edge [
    source 12
    target 918
  ]
  edge [
    source 12
    target 919
  ]
  edge [
    source 12
    target 920
  ]
  edge [
    source 12
    target 921
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 922
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 923
  ]
  edge [
    source 12
    target 924
  ]
  edge [
    source 12
    target 925
  ]
  edge [
    source 12
    target 926
  ]
  edge [
    source 12
    target 927
  ]
  edge [
    source 12
    target 928
  ]
  edge [
    source 12
    target 929
  ]
  edge [
    source 12
    target 930
  ]
  edge [
    source 12
    target 931
  ]
  edge [
    source 12
    target 932
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 933
  ]
  edge [
    source 12
    target 934
  ]
  edge [
    source 12
    target 935
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 936
  ]
  edge [
    source 12
    target 937
  ]
  edge [
    source 12
    target 938
  ]
  edge [
    source 12
    target 939
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 940
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 942
  ]
  edge [
    source 12
    target 943
  ]
  edge [
    source 12
    target 944
  ]
  edge [
    source 12
    target 945
  ]
  edge [
    source 12
    target 946
  ]
  edge [
    source 12
    target 947
  ]
  edge [
    source 12
    target 948
  ]
  edge [
    source 12
    target 949
  ]
  edge [
    source 12
    target 950
  ]
  edge [
    source 12
    target 951
  ]
  edge [
    source 12
    target 952
  ]
  edge [
    source 12
    target 252
  ]
  edge [
    source 12
    target 953
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 954
  ]
  edge [
    source 12
    target 955
  ]
  edge [
    source 12
    target 956
  ]
  edge [
    source 12
    target 957
  ]
  edge [
    source 12
    target 958
  ]
  edge [
    source 12
    target 959
  ]
  edge [
    source 12
    target 960
  ]
  edge [
    source 12
    target 961
  ]
  edge [
    source 12
    target 962
  ]
  edge [
    source 12
    target 963
  ]
  edge [
    source 12
    target 964
  ]
  edge [
    source 12
    target 965
  ]
  edge [
    source 12
    target 966
  ]
  edge [
    source 12
    target 967
  ]
  edge [
    source 12
    target 968
  ]
  edge [
    source 12
    target 969
  ]
  edge [
    source 12
    target 970
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 971
  ]
  edge [
    source 12
    target 972
  ]
  edge [
    source 12
    target 973
  ]
  edge [
    source 12
    target 974
  ]
  edge [
    source 12
    target 975
  ]
  edge [
    source 12
    target 976
  ]
  edge [
    source 12
    target 977
  ]
  edge [
    source 12
    target 978
  ]
  edge [
    source 12
    target 979
  ]
  edge [
    source 12
    target 980
  ]
  edge [
    source 12
    target 981
  ]
  edge [
    source 12
    target 982
  ]
  edge [
    source 12
    target 983
  ]
  edge [
    source 12
    target 407
  ]
  edge [
    source 12
    target 984
  ]
  edge [
    source 12
    target 985
  ]
  edge [
    source 12
    target 986
  ]
  edge [
    source 12
    target 987
  ]
  edge [
    source 12
    target 988
  ]
  edge [
    source 12
    target 989
  ]
  edge [
    source 12
    target 990
  ]
  edge [
    source 12
    target 991
  ]
  edge [
    source 12
    target 992
  ]
  edge [
    source 12
    target 993
  ]
  edge [
    source 12
    target 994
  ]
  edge [
    source 12
    target 995
  ]
  edge [
    source 12
    target 996
  ]
  edge [
    source 12
    target 997
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 998
  ]
  edge [
    source 12
    target 999
  ]
  edge [
    source 12
    target 1000
  ]
  edge [
    source 12
    target 1001
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 1002
  ]
  edge [
    source 12
    target 1003
  ]
  edge [
    source 12
    target 1004
  ]
  edge [
    source 12
    target 1005
  ]
  edge [
    source 12
    target 1006
  ]
  edge [
    source 12
    target 1007
  ]
  edge [
    source 12
    target 1008
  ]
  edge [
    source 12
    target 1009
  ]
  edge [
    source 12
    target 1010
  ]
  edge [
    source 12
    target 1011
  ]
  edge [
    source 12
    target 1012
  ]
  edge [
    source 12
    target 1013
  ]
  edge [
    source 12
    target 1014
  ]
  edge [
    source 12
    target 1015
  ]
  edge [
    source 12
    target 1016
  ]
  edge [
    source 12
    target 1017
  ]
  edge [
    source 12
    target 1018
  ]
  edge [
    source 12
    target 1019
  ]
  edge [
    source 12
    target 1020
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 1021
  ]
  edge [
    source 13
    target 1022
  ]
  edge [
    source 13
    target 1023
  ]
  edge [
    source 13
    target 1024
  ]
  edge [
    source 13
    target 1025
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 1026
  ]
  edge [
    source 13
    target 241
  ]
  edge [
    source 13
    target 1027
  ]
  edge [
    source 13
    target 1028
  ]
  edge [
    source 13
    target 308
  ]
  edge [
    source 13
    target 1029
  ]
  edge [
    source 13
    target 224
  ]
  edge [
    source 13
    target 1030
  ]
  edge [
    source 13
    target 1031
  ]
  edge [
    source 13
    target 1032
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 1033
  ]
  edge [
    source 13
    target 1034
  ]
  edge [
    source 13
    target 1035
  ]
  edge [
    source 13
    target 1036
  ]
  edge [
    source 13
    target 1037
  ]
  edge [
    source 13
    target 1038
  ]
  edge [
    source 13
    target 1039
  ]
  edge [
    source 13
    target 1040
  ]
  edge [
    source 13
    target 1041
  ]
  edge [
    source 13
    target 1042
  ]
  edge [
    source 13
    target 1043
  ]
  edge [
    source 13
    target 1044
  ]
  edge [
    source 13
    target 1045
  ]
  edge [
    source 13
    target 1046
  ]
  edge [
    source 13
    target 1047
  ]
  edge [
    source 13
    target 1048
  ]
  edge [
    source 13
    target 1049
  ]
  edge [
    source 13
    target 1050
  ]
  edge [
    source 13
    target 1051
  ]
  edge [
    source 13
    target 1052
  ]
  edge [
    source 13
    target 1053
  ]
  edge [
    source 13
    target 1054
  ]
  edge [
    source 13
    target 1055
  ]
  edge [
    source 13
    target 1056
  ]
  edge [
    source 13
    target 1057
  ]
  edge [
    source 13
    target 1058
  ]
  edge [
    source 13
    target 1059
  ]
  edge [
    source 13
    target 1060
  ]
  edge [
    source 13
    target 1061
  ]
  edge [
    source 13
    target 1062
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 1063
  ]
  edge [
    source 13
    target 1064
  ]
  edge [
    source 13
    target 1065
  ]
  edge [
    source 13
    target 1066
  ]
  edge [
    source 13
    target 1067
  ]
  edge [
    source 13
    target 1068
  ]
  edge [
    source 13
    target 1069
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 1070
  ]
  edge [
    source 13
    target 1071
  ]
  edge [
    source 13
    target 1072
  ]
  edge [
    source 13
    target 1073
  ]
  edge [
    source 13
    target 1074
  ]
  edge [
    source 13
    target 1075
  ]
  edge [
    source 13
    target 1076
  ]
  edge [
    source 13
    target 1077
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 1078
  ]
  edge [
    source 14
    target 1079
  ]
  edge [
    source 14
    target 1080
  ]
  edge [
    source 14
    target 1081
  ]
  edge [
    source 14
    target 1082
  ]
  edge [
    source 14
    target 1083
  ]
  edge [
    source 14
    target 234
  ]
  edge [
    source 14
    target 1084
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 1085
  ]
  edge [
    source 14
    target 1086
  ]
  edge [
    source 14
    target 1087
  ]
  edge [
    source 14
    target 1088
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1089
  ]
  edge [
    source 15
    target 1090
  ]
  edge [
    source 15
    target 1091
  ]
  edge [
    source 15
    target 1092
  ]
  edge [
    source 15
    target 1093
  ]
  edge [
    source 15
    target 1094
  ]
  edge [
    source 15
    target 1095
  ]
  edge [
    source 15
    target 1096
  ]
  edge [
    source 15
    target 1097
  ]
  edge [
    source 15
    target 1098
  ]
  edge [
    source 15
    target 1099
  ]
  edge [
    source 15
    target 1100
  ]
  edge [
    source 15
    target 1101
  ]
  edge [
    source 15
    target 1102
  ]
  edge [
    source 15
    target 1103
  ]
  edge [
    source 15
    target 1104
  ]
  edge [
    source 15
    target 1105
  ]
  edge [
    source 15
    target 1106
  ]
  edge [
    source 15
    target 1107
  ]
  edge [
    source 15
    target 1108
  ]
  edge [
    source 15
    target 1109
  ]
  edge [
    source 15
    target 1110
  ]
  edge [
    source 15
    target 1111
  ]
  edge [
    source 15
    target 1112
  ]
  edge [
    source 15
    target 1113
  ]
  edge [
    source 15
    target 1114
  ]
  edge [
    source 15
    target 1115
  ]
  edge [
    source 15
    target 1116
  ]
  edge [
    source 15
    target 1117
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1118
  ]
  edge [
    source 16
    target 1119
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 277
  ]
  edge [
    source 16
    target 1120
  ]
  edge [
    source 16
    target 1121
  ]
  edge [
    source 16
    target 1122
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 1123
  ]
  edge [
    source 16
    target 1124
  ]
  edge [
    source 16
    target 1125
  ]
  edge [
    source 16
    target 1126
  ]
  edge [
    source 16
    target 1127
  ]
  edge [
    source 16
    target 1128
  ]
  edge [
    source 16
    target 1129
  ]
  edge [
    source 16
    target 1130
  ]
  edge [
    source 16
    target 1131
  ]
  edge [
    source 16
    target 232
  ]
  edge [
    source 16
    target 1132
  ]
  edge [
    source 16
    target 1133
  ]
  edge [
    source 16
    target 1134
  ]
  edge [
    source 16
    target 1135
  ]
  edge [
    source 16
    target 1136
  ]
  edge [
    source 16
    target 1137
  ]
  edge [
    source 16
    target 1138
  ]
  edge [
    source 16
    target 1139
  ]
  edge [
    source 16
    target 1140
  ]
  edge [
    source 16
    target 1141
  ]
  edge [
    source 16
    target 1142
  ]
  edge [
    source 16
    target 1143
  ]
  edge [
    source 16
    target 1144
  ]
  edge [
    source 16
    target 1145
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 1146
  ]
  edge [
    source 16
    target 1147
  ]
  edge [
    source 16
    target 1148
  ]
  edge [
    source 16
    target 1149
  ]
  edge [
    source 16
    target 263
  ]
  edge [
    source 16
    target 1150
  ]
  edge [
    source 16
    target 1151
  ]
  edge [
    source 16
    target 1152
  ]
  edge [
    source 16
    target 1153
  ]
  edge [
    source 16
    target 1154
  ]
  edge [
    source 16
    target 1155
  ]
  edge [
    source 16
    target 1156
  ]
  edge [
    source 16
    target 315
  ]
  edge [
    source 16
    target 246
  ]
  edge [
    source 16
    target 1157
  ]
  edge [
    source 16
    target 1158
  ]
  edge [
    source 16
    target 1159
  ]
  edge [
    source 16
    target 1160
  ]
  edge [
    source 16
    target 1161
  ]
  edge [
    source 16
    target 1162
  ]
  edge [
    source 16
    target 627
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 25
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 17
    target 27
  ]
  edge [
    source 17
    target 1163
  ]
  edge [
    source 17
    target 263
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 214
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 18
    target 25
  ]
  edge [
    source 18
    target 1164
  ]
  edge [
    source 18
    target 1165
  ]
  edge [
    source 18
    target 1166
  ]
  edge [
    source 18
    target 1167
  ]
  edge [
    source 18
    target 1168
  ]
  edge [
    source 18
    target 1169
  ]
  edge [
    source 18
    target 1170
  ]
  edge [
    source 18
    target 1171
  ]
  edge [
    source 18
    target 1172
  ]
  edge [
    source 18
    target 1173
  ]
  edge [
    source 18
    target 1174
  ]
  edge [
    source 18
    target 1175
  ]
  edge [
    source 18
    target 1176
  ]
  edge [
    source 18
    target 1177
  ]
  edge [
    source 18
    target 1178
  ]
  edge [
    source 18
    target 1179
  ]
  edge [
    source 18
    target 1180
  ]
  edge [
    source 18
    target 1181
  ]
  edge [
    source 18
    target 1182
  ]
  edge [
    source 18
    target 1183
  ]
  edge [
    source 18
    target 1184
  ]
  edge [
    source 18
    target 1118
  ]
  edge [
    source 18
    target 1185
  ]
  edge [
    source 18
    target 1186
  ]
  edge [
    source 18
    target 1187
  ]
  edge [
    source 18
    target 1188
  ]
  edge [
    source 18
    target 798
  ]
  edge [
    source 18
    target 1189
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 819
  ]
  edge [
    source 18
    target 802
  ]
  edge [
    source 18
    target 1190
  ]
  edge [
    source 18
    target 1191
  ]
  edge [
    source 18
    target 803
  ]
  edge [
    source 18
    target 1192
  ]
  edge [
    source 18
    target 1193
  ]
  edge [
    source 18
    target 1194
  ]
  edge [
    source 18
    target 1195
  ]
  edge [
    source 18
    target 1196
  ]
  edge [
    source 18
    target 1197
  ]
  edge [
    source 18
    target 1198
  ]
  edge [
    source 18
    target 1199
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 1200
  ]
  edge [
    source 18
    target 1201
  ]
  edge [
    source 18
    target 1202
  ]
  edge [
    source 18
    target 1203
  ]
  edge [
    source 18
    target 263
  ]
  edge [
    source 18
    target 1204
  ]
  edge [
    source 18
    target 1205
  ]
  edge [
    source 18
    target 1206
  ]
  edge [
    source 18
    target 1207
  ]
  edge [
    source 18
    target 1208
  ]
  edge [
    source 18
    target 1209
  ]
  edge [
    source 18
    target 1210
  ]
  edge [
    source 18
    target 812
  ]
  edge [
    source 18
    target 1211
  ]
  edge [
    source 18
    target 1212
  ]
  edge [
    source 18
    target 1213
  ]
  edge [
    source 18
    target 1214
  ]
  edge [
    source 18
    target 1215
  ]
  edge [
    source 18
    target 1216
  ]
  edge [
    source 18
    target 1217
  ]
  edge [
    source 18
    target 1218
  ]
  edge [
    source 18
    target 818
  ]
  edge [
    source 18
    target 1219
  ]
  edge [
    source 18
    target 1220
  ]
  edge [
    source 18
    target 1221
  ]
  edge [
    source 18
    target 1222
  ]
  edge [
    source 18
    target 1223
  ]
  edge [
    source 18
    target 1224
  ]
  edge [
    source 18
    target 1225
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 1226
  ]
  edge [
    source 18
    target 1227
  ]
  edge [
    source 18
    target 785
  ]
  edge [
    source 18
    target 786
  ]
  edge [
    source 18
    target 787
  ]
  edge [
    source 18
    target 788
  ]
  edge [
    source 18
    target 789
  ]
  edge [
    source 18
    target 790
  ]
  edge [
    source 18
    target 791
  ]
  edge [
    source 18
    target 792
  ]
  edge [
    source 18
    target 793
  ]
  edge [
    source 18
    target 794
  ]
  edge [
    source 18
    target 795
  ]
  edge [
    source 18
    target 796
  ]
  edge [
    source 18
    target 797
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 799
  ]
  edge [
    source 18
    target 800
  ]
  edge [
    source 18
    target 801
  ]
  edge [
    source 18
    target 804
  ]
  edge [
    source 18
    target 806
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 808
  ]
  edge [
    source 18
    target 809
  ]
  edge [
    source 18
    target 810
  ]
  edge [
    source 18
    target 811
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 813
  ]
  edge [
    source 18
    target 814
  ]
  edge [
    source 18
    target 815
  ]
  edge [
    source 18
    target 816
  ]
  edge [
    source 18
    target 817
  ]
  edge [
    source 18
    target 1026
  ]
  edge [
    source 18
    target 1228
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 1229
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 1230
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 252
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 1231
  ]
  edge [
    source 18
    target 1232
  ]
  edge [
    source 18
    target 1233
  ]
  edge [
    source 18
    target 1234
  ]
  edge [
    source 18
    target 1235
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 1236
  ]
  edge [
    source 18
    target 1237
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 1238
  ]
  edge [
    source 18
    target 1239
  ]
  edge [
    source 18
    target 1240
  ]
  edge [
    source 18
    target 1241
  ]
  edge [
    source 18
    target 1242
  ]
  edge [
    source 18
    target 1243
  ]
  edge [
    source 18
    target 1244
  ]
  edge [
    source 18
    target 1245
  ]
  edge [
    source 18
    target 1246
  ]
  edge [
    source 18
    target 1247
  ]
  edge [
    source 18
    target 1248
  ]
  edge [
    source 18
    target 1249
  ]
  edge [
    source 18
    target 821
  ]
  edge [
    source 18
    target 1250
  ]
  edge [
    source 18
    target 1251
  ]
  edge [
    source 18
    target 1252
  ]
  edge [
    source 18
    target 1253
  ]
  edge [
    source 18
    target 1254
  ]
  edge [
    source 18
    target 323
  ]
  edge [
    source 18
    target 234
  ]
  edge [
    source 18
    target 1255
  ]
  edge [
    source 18
    target 86
  ]
  edge [
    source 18
    target 1256
  ]
  edge [
    source 18
    target 1257
  ]
  edge [
    source 18
    target 277
  ]
  edge [
    source 18
    target 1258
  ]
  edge [
    source 18
    target 1259
  ]
  edge [
    source 18
    target 1260
  ]
  edge [
    source 18
    target 1261
  ]
  edge [
    source 18
    target 1262
  ]
  edge [
    source 18
    target 1263
  ]
  edge [
    source 18
    target 1264
  ]
  edge [
    source 18
    target 1265
  ]
  edge [
    source 18
    target 1266
  ]
  edge [
    source 18
    target 1267
  ]
  edge [
    source 18
    target 1268
  ]
  edge [
    source 18
    target 1269
  ]
  edge [
    source 18
    target 1270
  ]
  edge [
    source 18
    target 1271
  ]
  edge [
    source 18
    target 1272
  ]
  edge [
    source 18
    target 1273
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 1274
  ]
  edge [
    source 18
    target 1275
  ]
  edge [
    source 18
    target 1276
  ]
  edge [
    source 18
    target 1133
  ]
  edge [
    source 18
    target 1134
  ]
  edge [
    source 18
    target 1135
  ]
  edge [
    source 18
    target 1136
  ]
  edge [
    source 18
    target 1137
  ]
  edge [
    source 18
    target 1138
  ]
  edge [
    source 18
    target 1139
  ]
  edge [
    source 18
    target 1140
  ]
  edge [
    source 18
    target 1141
  ]
  edge [
    source 18
    target 1142
  ]
  edge [
    source 18
    target 1143
  ]
  edge [
    source 18
    target 1144
  ]
  edge [
    source 18
    target 1145
  ]
  edge [
    source 18
    target 1146
  ]
  edge [
    source 18
    target 1147
  ]
  edge [
    source 18
    target 1148
  ]
  edge [
    source 18
    target 1149
  ]
  edge [
    source 18
    target 1150
  ]
  edge [
    source 18
    target 1151
  ]
  edge [
    source 18
    target 1152
  ]
  edge [
    source 18
    target 1153
  ]
  edge [
    source 18
    target 1154
  ]
  edge [
    source 18
    target 1277
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 1278
  ]
  edge [
    source 18
    target 1279
  ]
  edge [
    source 18
    target 1280
  ]
  edge [
    source 18
    target 1281
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 1282
  ]
  edge [
    source 18
    target 1283
  ]
  edge [
    source 18
    target 1284
  ]
  edge [
    source 18
    target 1285
  ]
  edge [
    source 18
    target 1286
  ]
  edge [
    source 18
    target 1287
  ]
  edge [
    source 18
    target 1288
  ]
  edge [
    source 18
    target 232
  ]
  edge [
    source 18
    target 1289
  ]
  edge [
    source 18
    target 1290
  ]
  edge [
    source 18
    target 1291
  ]
  edge [
    source 18
    target 1292
  ]
  edge [
    source 18
    target 292
  ]
  edge [
    source 18
    target 1293
  ]
  edge [
    source 18
    target 1294
  ]
  edge [
    source 18
    target 1295
  ]
  edge [
    source 18
    target 1296
  ]
  edge [
    source 18
    target 1297
  ]
  edge [
    source 18
    target 1298
  ]
  edge [
    source 18
    target 1299
  ]
  edge [
    source 18
    target 1300
  ]
  edge [
    source 18
    target 1301
  ]
  edge [
    source 18
    target 1302
  ]
  edge [
    source 18
    target 1303
  ]
  edge [
    source 18
    target 1304
  ]
  edge [
    source 18
    target 1305
  ]
  edge [
    source 18
    target 1306
  ]
  edge [
    source 18
    target 1307
  ]
  edge [
    source 18
    target 1308
  ]
  edge [
    source 18
    target 1309
  ]
  edge [
    source 18
    target 1310
  ]
  edge [
    source 18
    target 1311
  ]
  edge [
    source 18
    target 1312
  ]
  edge [
    source 18
    target 1313
  ]
  edge [
    source 18
    target 1314
  ]
  edge [
    source 18
    target 1315
  ]
  edge [
    source 18
    target 1316
  ]
  edge [
    source 18
    target 1317
  ]
  edge [
    source 18
    target 1318
  ]
  edge [
    source 18
    target 1319
  ]
  edge [
    source 18
    target 1320
  ]
  edge [
    source 18
    target 1321
  ]
  edge [
    source 18
    target 1322
  ]
  edge [
    source 18
    target 1323
  ]
  edge [
    source 18
    target 1324
  ]
  edge [
    source 18
    target 324
  ]
  edge [
    source 18
    target 1325
  ]
  edge [
    source 18
    target 1326
  ]
  edge [
    source 18
    target 1327
  ]
  edge [
    source 18
    target 1328
  ]
  edge [
    source 18
    target 1329
  ]
  edge [
    source 18
    target 1330
  ]
  edge [
    source 18
    target 1331
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 1332
  ]
  edge [
    source 18
    target 1333
  ]
  edge [
    source 18
    target 1334
  ]
  edge [
    source 18
    target 1335
  ]
  edge [
    source 18
    target 1336
  ]
  edge [
    source 18
    target 1337
  ]
  edge [
    source 18
    target 1338
  ]
  edge [
    source 18
    target 1339
  ]
  edge [
    source 18
    target 1340
  ]
  edge [
    source 18
    target 1341
  ]
  edge [
    source 18
    target 1342
  ]
  edge [
    source 18
    target 1343
  ]
  edge [
    source 18
    target 1344
  ]
  edge [
    source 18
    target 1345
  ]
  edge [
    source 18
    target 1346
  ]
  edge [
    source 18
    target 1347
  ]
  edge [
    source 18
    target 1348
  ]
  edge [
    source 18
    target 1349
  ]
  edge [
    source 18
    target 1350
  ]
  edge [
    source 18
    target 1351
  ]
  edge [
    source 18
    target 1352
  ]
  edge [
    source 18
    target 1353
  ]
  edge [
    source 18
    target 1354
  ]
  edge [
    source 18
    target 1355
  ]
  edge [
    source 18
    target 1356
  ]
  edge [
    source 18
    target 1357
  ]
  edge [
    source 18
    target 1358
  ]
  edge [
    source 18
    target 1359
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 1360
  ]
  edge [
    source 18
    target 1361
  ]
  edge [
    source 18
    target 1362
  ]
  edge [
    source 18
    target 1363
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1364
  ]
  edge [
    source 21
    target 49
  ]
  edge [
    source 21
    target 1365
  ]
  edge [
    source 21
    target 1366
  ]
  edge [
    source 21
    target 1367
  ]
  edge [
    source 21
    target 1368
  ]
  edge [
    source 21
    target 1369
  ]
  edge [
    source 21
    target 1370
  ]
  edge [
    source 21
    target 1371
  ]
  edge [
    source 21
    target 1372
  ]
  edge [
    source 21
    target 1373
  ]
  edge [
    source 21
    target 1374
  ]
  edge [
    source 21
    target 1375
  ]
  edge [
    source 21
    target 1376
  ]
  edge [
    source 21
    target 1377
  ]
  edge [
    source 21
    target 1378
  ]
  edge [
    source 21
    target 541
  ]
  edge [
    source 21
    target 1379
  ]
  edge [
    source 21
    target 1380
  ]
  edge [
    source 21
    target 1381
  ]
  edge [
    source 21
    target 1382
  ]
  edge [
    source 21
    target 1383
  ]
  edge [
    source 21
    target 1384
  ]
  edge [
    source 21
    target 1385
  ]
  edge [
    source 21
    target 1386
  ]
  edge [
    source 21
    target 1387
  ]
  edge [
    source 21
    target 1388
  ]
  edge [
    source 21
    target 1389
  ]
  edge [
    source 21
    target 1390
  ]
  edge [
    source 21
    target 1391
  ]
  edge [
    source 21
    target 1392
  ]
  edge [
    source 21
    target 1393
  ]
  edge [
    source 21
    target 1394
  ]
  edge [
    source 21
    target 1395
  ]
  edge [
    source 21
    target 1396
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 1397
  ]
  edge [
    source 21
    target 426
  ]
  edge [
    source 21
    target 1398
  ]
  edge [
    source 21
    target 1399
  ]
  edge [
    source 21
    target 1400
  ]
  edge [
    source 21
    target 1401
  ]
  edge [
    source 21
    target 1402
  ]
  edge [
    source 21
    target 1403
  ]
  edge [
    source 21
    target 946
  ]
  edge [
    source 21
    target 570
  ]
  edge [
    source 21
    target 1404
  ]
  edge [
    source 21
    target 1405
  ]
  edge [
    source 21
    target 1406
  ]
  edge [
    source 21
    target 1407
  ]
  edge [
    source 21
    target 1408
  ]
  edge [
    source 21
    target 576
  ]
  edge [
    source 21
    target 1409
  ]
  edge [
    source 21
    target 1410
  ]
  edge [
    source 21
    target 40
  ]
  edge [
    source 21
    target 1411
  ]
  edge [
    source 21
    target 1412
  ]
  edge [
    source 21
    target 1413
  ]
  edge [
    source 21
    target 1414
  ]
  edge [
    source 21
    target 708
  ]
  edge [
    source 21
    target 1415
  ]
  edge [
    source 21
    target 1416
  ]
  edge [
    source 21
    target 1417
  ]
  edge [
    source 21
    target 1418
  ]
  edge [
    source 21
    target 1419
  ]
  edge [
    source 21
    target 1420
  ]
  edge [
    source 21
    target 1421
  ]
  edge [
    source 21
    target 1422
  ]
  edge [
    source 21
    target 1423
  ]
  edge [
    source 21
    target 1424
  ]
  edge [
    source 21
    target 1425
  ]
  edge [
    source 21
    target 1426
  ]
  edge [
    source 21
    target 1427
  ]
  edge [
    source 21
    target 574
  ]
  edge [
    source 21
    target 1428
  ]
  edge [
    source 21
    target 1429
  ]
  edge [
    source 21
    target 1430
  ]
  edge [
    source 21
    target 1431
  ]
  edge [
    source 21
    target 1432
  ]
  edge [
    source 21
    target 1433
  ]
  edge [
    source 21
    target 1434
  ]
  edge [
    source 21
    target 1435
  ]
  edge [
    source 21
    target 1436
  ]
  edge [
    source 21
    target 1437
  ]
  edge [
    source 21
    target 1438
  ]
  edge [
    source 21
    target 1439
  ]
  edge [
    source 21
    target 1440
  ]
  edge [
    source 21
    target 563
  ]
  edge [
    source 21
    target 1441
  ]
  edge [
    source 21
    target 1442
  ]
  edge [
    source 21
    target 862
  ]
  edge [
    source 21
    target 1443
  ]
  edge [
    source 21
    target 1444
  ]
  edge [
    source 21
    target 1445
  ]
  edge [
    source 21
    target 1446
  ]
  edge [
    source 21
    target 1447
  ]
  edge [
    source 21
    target 1448
  ]
  edge [
    source 21
    target 1449
  ]
  edge [
    source 21
    target 1450
  ]
  edge [
    source 21
    target 1451
  ]
  edge [
    source 21
    target 1452
  ]
  edge [
    source 21
    target 1453
  ]
  edge [
    source 22
    target 1454
  ]
  edge [
    source 22
    target 1455
  ]
  edge [
    source 22
    target 1456
  ]
  edge [
    source 22
    target 1457
  ]
  edge [
    source 22
    target 1458
  ]
  edge [
    source 22
    target 1459
  ]
  edge [
    source 22
    target 1460
  ]
  edge [
    source 22
    target 1461
  ]
  edge [
    source 22
    target 1462
  ]
  edge [
    source 22
    target 1463
  ]
  edge [
    source 22
    target 1464
  ]
  edge [
    source 22
    target 1465
  ]
  edge [
    source 22
    target 1466
  ]
  edge [
    source 22
    target 1467
  ]
  edge [
    source 22
    target 1468
  ]
  edge [
    source 22
    target 1469
  ]
  edge [
    source 22
    target 1470
  ]
  edge [
    source 22
    target 1092
  ]
  edge [
    source 22
    target 1471
  ]
  edge [
    source 22
    target 1472
  ]
  edge [
    source 22
    target 1473
  ]
  edge [
    source 22
    target 1474
  ]
  edge [
    source 22
    target 1475
  ]
  edge [
    source 22
    target 1476
  ]
  edge [
    source 22
    target 1477
  ]
  edge [
    source 22
    target 1478
  ]
  edge [
    source 22
    target 1479
  ]
  edge [
    source 22
    target 1480
  ]
  edge [
    source 22
    target 1481
  ]
  edge [
    source 22
    target 1482
  ]
  edge [
    source 22
    target 1483
  ]
  edge [
    source 23
    target 1484
  ]
  edge [
    source 23
    target 541
  ]
  edge [
    source 23
    target 1485
  ]
  edge [
    source 23
    target 1486
  ]
  edge [
    source 23
    target 1487
  ]
  edge [
    source 23
    target 1488
  ]
  edge [
    source 23
    target 552
  ]
  edge [
    source 23
    target 392
  ]
  edge [
    source 23
    target 549
  ]
  edge [
    source 23
    target 1489
  ]
  edge [
    source 23
    target 1490
  ]
  edge [
    source 23
    target 1491
  ]
  edge [
    source 23
    target 1492
  ]
  edge [
    source 23
    target 1493
  ]
  edge [
    source 23
    target 1494
  ]
  edge [
    source 23
    target 1495
  ]
  edge [
    source 23
    target 1496
  ]
  edge [
    source 23
    target 1497
  ]
  edge [
    source 23
    target 1498
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 23
    target 568
  ]
  edge [
    source 23
    target 1499
  ]
  edge [
    source 23
    target 1500
  ]
  edge [
    source 23
    target 1501
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 23
    target 1502
  ]
  edge [
    source 23
    target 1503
  ]
  edge [
    source 23
    target 1504
  ]
  edge [
    source 23
    target 1505
  ]
  edge [
    source 23
    target 1506
  ]
  edge [
    source 23
    target 1507
  ]
  edge [
    source 23
    target 1508
  ]
  edge [
    source 23
    target 1509
  ]
  edge [
    source 23
    target 1510
  ]
  edge [
    source 23
    target 1511
  ]
  edge [
    source 23
    target 1512
  ]
  edge [
    source 23
    target 1513
  ]
  edge [
    source 23
    target 1514
  ]
  edge [
    source 23
    target 1515
  ]
  edge [
    source 23
    target 1516
  ]
  edge [
    source 23
    target 1517
  ]
  edge [
    source 23
    target 1518
  ]
  edge [
    source 23
    target 1519
  ]
  edge [
    source 25
    target 1520
  ]
  edge [
    source 25
    target 1521
  ]
  edge [
    source 25
    target 260
  ]
  edge [
    source 25
    target 1522
  ]
  edge [
    source 25
    target 1452
  ]
  edge [
    source 25
    target 138
  ]
  edge [
    source 25
    target 230
  ]
  edge [
    source 25
    target 641
  ]
  edge [
    source 25
    target 1523
  ]
  edge [
    source 25
    target 182
  ]
  edge [
    source 25
    target 1524
  ]
  edge [
    source 25
    target 233
  ]
  edge [
    source 25
    target 1525
  ]
  edge [
    source 25
    target 1526
  ]
  edge [
    source 25
    target 1527
  ]
  edge [
    source 25
    target 1528
  ]
  edge [
    source 25
    target 1529
  ]
  edge [
    source 25
    target 1530
  ]
  edge [
    source 25
    target 1531
  ]
  edge [
    source 25
    target 1532
  ]
  edge [
    source 25
    target 1533
  ]
  edge [
    source 25
    target 1534
  ]
  edge [
    source 25
    target 1535
  ]
  edge [
    source 25
    target 1536
  ]
  edge [
    source 25
    target 1537
  ]
  edge [
    source 25
    target 287
  ]
  edge [
    source 25
    target 1538
  ]
  edge [
    source 25
    target 1539
  ]
  edge [
    source 25
    target 1155
  ]
  edge [
    source 25
    target 1156
  ]
  edge [
    source 25
    target 315
  ]
  edge [
    source 25
    target 246
  ]
  edge [
    source 25
    target 1157
  ]
  edge [
    source 25
    target 1158
  ]
  edge [
    source 25
    target 1118
  ]
  edge [
    source 25
    target 1159
  ]
  edge [
    source 25
    target 1160
  ]
  edge [
    source 25
    target 1161
  ]
  edge [
    source 25
    target 1162
  ]
  edge [
    source 25
    target 627
  ]
  edge [
    source 25
    target 1540
  ]
  edge [
    source 25
    target 1207
  ]
  edge [
    source 25
    target 1541
  ]
  edge [
    source 25
    target 1542
  ]
  edge [
    source 25
    target 578
  ]
  edge [
    source 25
    target 1543
  ]
  edge [
    source 25
    target 1544
  ]
  edge [
    source 25
    target 1545
  ]
  edge [
    source 25
    target 1546
  ]
  edge [
    source 25
    target 1547
  ]
  edge [
    source 25
    target 1548
  ]
  edge [
    source 25
    target 1549
  ]
  edge [
    source 25
    target 1550
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 332
  ]
  edge [
    source 25
    target 1551
  ]
  edge [
    source 25
    target 237
  ]
  edge [
    source 25
    target 947
  ]
  edge [
    source 25
    target 1552
  ]
  edge [
    source 25
    target 1553
  ]
  edge [
    source 25
    target 1554
  ]
  edge [
    source 25
    target 1555
  ]
  edge [
    source 25
    target 410
  ]
  edge [
    source 25
    target 1556
  ]
  edge [
    source 25
    target 1557
  ]
  edge [
    source 25
    target 1558
  ]
  edge [
    source 25
    target 202
  ]
  edge [
    source 25
    target 1559
  ]
  edge [
    source 25
    target 1560
  ]
  edge [
    source 25
    target 1561
  ]
  edge [
    source 25
    target 1562
  ]
  edge [
    source 25
    target 1563
  ]
  edge [
    source 25
    target 651
  ]
  edge [
    source 25
    target 1564
  ]
  edge [
    source 25
    target 1565
  ]
  edge [
    source 25
    target 987
  ]
  edge [
    source 25
    target 1566
  ]
  edge [
    source 25
    target 1567
  ]
  edge [
    source 25
    target 76
  ]
  edge [
    source 25
    target 1568
  ]
  edge [
    source 25
    target 1569
  ]
  edge [
    source 25
    target 1518
  ]
  edge [
    source 25
    target 304
  ]
  edge [
    source 25
    target 1570
  ]
  edge [
    source 25
    target 1571
  ]
  edge [
    source 25
    target 1572
  ]
  edge [
    source 25
    target 662
  ]
  edge [
    source 25
    target 1573
  ]
  edge [
    source 25
    target 1574
  ]
  edge [
    source 25
    target 1575
  ]
  edge [
    source 25
    target 1576
  ]
  edge [
    source 25
    target 1577
  ]
  edge [
    source 25
    target 1578
  ]
  edge [
    source 25
    target 1579
  ]
  edge [
    source 25
    target 1580
  ]
  edge [
    source 25
    target 1581
  ]
  edge [
    source 25
    target 1582
  ]
  edge [
    source 25
    target 1583
  ]
  edge [
    source 25
    target 1584
  ]
  edge [
    source 25
    target 1585
  ]
  edge [
    source 25
    target 1586
  ]
  edge [
    source 25
    target 1587
  ]
  edge [
    source 25
    target 1588
  ]
  edge [
    source 25
    target 67
  ]
  edge [
    source 25
    target 1589
  ]
  edge [
    source 25
    target 1590
  ]
  edge [
    source 25
    target 1591
  ]
  edge [
    source 25
    target 1592
  ]
  edge [
    source 25
    target 1593
  ]
  edge [
    source 25
    target 1594
  ]
  edge [
    source 25
    target 1595
  ]
  edge [
    source 25
    target 1596
  ]
  edge [
    source 25
    target 1597
  ]
  edge [
    source 25
    target 1598
  ]
  edge [
    source 25
    target 1599
  ]
  edge [
    source 25
    target 1600
  ]
  edge [
    source 25
    target 1601
  ]
  edge [
    source 25
    target 1602
  ]
  edge [
    source 25
    target 1603
  ]
  edge [
    source 25
    target 1604
  ]
  edge [
    source 25
    target 1605
  ]
  edge [
    source 25
    target 1606
  ]
  edge [
    source 25
    target 204
  ]
  edge [
    source 25
    target 972
  ]
  edge [
    source 25
    target 1607
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 1286
  ]
  edge [
    source 25
    target 1608
  ]
  edge [
    source 25
    target 1609
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 444
  ]
  edge [
    source 26
    target 1610
  ]
  edge [
    source 26
    target 1611
  ]
  edge [
    source 26
    target 1612
  ]
  edge [
    source 26
    target 1613
  ]
  edge [
    source 26
    target 1614
  ]
  edge [
    source 26
    target 204
  ]
  edge [
    source 26
    target 916
  ]
  edge [
    source 26
    target 1615
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 152
  ]
  edge [
    source 26
    target 1616
  ]
  edge [
    source 26
    target 643
  ]
  edge [
    source 26
    target 1617
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 64
  ]
  edge [
    source 26
    target 1618
  ]
  edge [
    source 26
    target 1619
  ]
  edge [
    source 26
    target 1620
  ]
  edge [
    source 26
    target 1621
  ]
  edge [
    source 26
    target 1622
  ]
  edge [
    source 26
    target 1623
  ]
  edge [
    source 26
    target 367
  ]
  edge [
    source 26
    target 1624
  ]
  edge [
    source 26
    target 1625
  ]
  edge [
    source 26
    target 1626
  ]
  edge [
    source 26
    target 1627
  ]
  edge [
    source 26
    target 1628
  ]
  edge [
    source 26
    target 1629
  ]
  edge [
    source 26
    target 1630
  ]
  edge [
    source 26
    target 1631
  ]
  edge [
    source 26
    target 1632
  ]
  edge [
    source 26
    target 125
  ]
  edge [
    source 26
    target 1633
  ]
  edge [
    source 26
    target 1634
  ]
  edge [
    source 26
    target 1635
  ]
  edge [
    source 26
    target 1636
  ]
  edge [
    source 26
    target 1637
  ]
  edge [
    source 26
    target 1638
  ]
  edge [
    source 26
    target 1639
  ]
  edge [
    source 26
    target 1640
  ]
  edge [
    source 26
    target 1641
  ]
  edge [
    source 26
    target 1642
  ]
  edge [
    source 26
    target 1643
  ]
  edge [
    source 26
    target 1644
  ]
  edge [
    source 26
    target 1645
  ]
  edge [
    source 26
    target 1646
  ]
  edge [
    source 26
    target 1647
  ]
  edge [
    source 26
    target 1648
  ]
  edge [
    source 26
    target 1649
  ]
  edge [
    source 26
    target 1650
  ]
  edge [
    source 26
    target 1651
  ]
  edge [
    source 26
    target 1652
  ]
  edge [
    source 26
    target 1653
  ]
  edge [
    source 26
    target 1654
  ]
  edge [
    source 26
    target 1655
  ]
  edge [
    source 26
    target 1656
  ]
  edge [
    source 26
    target 1657
  ]
  edge [
    source 26
    target 1658
  ]
  edge [
    source 26
    target 1659
  ]
  edge [
    source 26
    target 1660
  ]
  edge [
    source 26
    target 1661
  ]
  edge [
    source 26
    target 1662
  ]
  edge [
    source 26
    target 1663
  ]
  edge [
    source 26
    target 1664
  ]
  edge [
    source 26
    target 1665
  ]
  edge [
    source 26
    target 1666
  ]
  edge [
    source 26
    target 1667
  ]
  edge [
    source 26
    target 1668
  ]
  edge [
    source 26
    target 1669
  ]
  edge [
    source 26
    target 252
  ]
  edge [
    source 27
    target 1095
  ]
  edge [
    source 27
    target 1670
  ]
  edge [
    source 27
    target 234
  ]
  edge [
    source 27
    target 1671
  ]
  edge [
    source 27
    target 1672
  ]
  edge [
    source 27
    target 1673
  ]
  edge [
    source 27
    target 1674
  ]
  edge [
    source 27
    target 1675
  ]
  edge [
    source 27
    target 1676
  ]
  edge [
    source 27
    target 1677
  ]
  edge [
    source 27
    target 1678
  ]
  edge [
    source 27
    target 1679
  ]
  edge [
    source 27
    target 1680
  ]
  edge [
    source 27
    target 1681
  ]
  edge [
    source 27
    target 1682
  ]
  edge [
    source 27
    target 1683
  ]
  edge [
    source 27
    target 1684
  ]
  edge [
    source 27
    target 1089
  ]
  edge [
    source 27
    target 1685
  ]
  edge [
    source 27
    target 1686
  ]
  edge [
    source 27
    target 1687
  ]
  edge [
    source 27
    target 1688
  ]
  edge [
    source 27
    target 1689
  ]
  edge [
    source 27
    target 1690
  ]
  edge [
    source 27
    target 1691
  ]
  edge [
    source 27
    target 1692
  ]
  edge [
    source 27
    target 1693
  ]
  edge [
    source 27
    target 1694
  ]
  edge [
    source 27
    target 194
  ]
  edge [
    source 27
    target 515
  ]
  edge [
    source 27
    target 511
  ]
  edge [
    source 27
    target 1695
  ]
  edge [
    source 27
    target 202
  ]
  edge [
    source 27
    target 1696
  ]
  edge [
    source 27
    target 1697
  ]
  edge [
    source 27
    target 1698
  ]
  edge [
    source 27
    target 205
  ]
  edge [
    source 27
    target 93
  ]
  edge [
    source 27
    target 206
  ]
  edge [
    source 27
    target 1699
  ]
  edge [
    source 27
    target 215
  ]
  edge [
    source 27
    target 218
  ]
  edge [
    source 27
    target 1700
  ]
  edge [
    source 27
    target 219
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 27
    target 1701
  ]
  edge [
    source 27
    target 221
  ]
  edge [
    source 27
    target 1702
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 27
    target 1703
  ]
  edge [
    source 27
    target 1081
  ]
  edge [
    source 27
    target 1082
  ]
  edge [
    source 27
    target 1704
  ]
  edge [
    source 27
    target 1705
  ]
  edge [
    source 27
    target 1706
  ]
  edge [
    source 27
    target 1707
  ]
  edge [
    source 27
    target 1708
  ]
  edge [
    source 27
    target 1709
  ]
  edge [
    source 27
    target 1710
  ]
  edge [
    source 27
    target 917
  ]
  edge [
    source 27
    target 1386
  ]
  edge [
    source 27
    target 1711
  ]
  edge [
    source 27
    target 1712
  ]
  edge [
    source 27
    target 1713
  ]
  edge [
    source 27
    target 1714
  ]
  edge [
    source 27
    target 1715
  ]
  edge [
    source 27
    target 288
  ]
  edge [
    source 27
    target 1716
  ]
  edge [
    source 27
    target 1717
  ]
  edge [
    source 27
    target 1718
  ]
  edge [
    source 27
    target 1719
  ]
  edge [
    source 27
    target 1720
  ]
  edge [
    source 27
    target 1721
  ]
  edge [
    source 27
    target 1722
  ]
  edge [
    source 27
    target 1723
  ]
  edge [
    source 27
    target 1724
  ]
  edge [
    source 27
    target 1725
  ]
  edge [
    source 27
    target 1726
  ]
  edge [
    source 27
    target 1727
  ]
  edge [
    source 27
    target 1728
  ]
  edge [
    source 27
    target 1729
  ]
  edge [
    source 27
    target 1730
  ]
  edge [
    source 27
    target 1731
  ]
  edge [
    source 27
    target 1732
  ]
  edge [
    source 27
    target 1733
  ]
  edge [
    source 27
    target 1734
  ]
  edge [
    source 27
    target 1735
  ]
  edge [
    source 27
    target 1736
  ]
  edge [
    source 27
    target 1737
  ]
  edge [
    source 27
    target 1738
  ]
  edge [
    source 27
    target 1739
  ]
  edge [
    source 27
    target 1740
  ]
  edge [
    source 27
    target 1741
  ]
  edge [
    source 27
    target 1742
  ]
  edge [
    source 27
    target 1743
  ]
  edge [
    source 27
    target 1744
  ]
  edge [
    source 27
    target 1745
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 1746
  ]
  edge [
    source 27
    target 1747
  ]
  edge [
    source 27
    target 1748
  ]
  edge [
    source 27
    target 1749
  ]
  edge [
    source 27
    target 1750
  ]
  edge [
    source 27
    target 1751
  ]
  edge [
    source 27
    target 1752
  ]
  edge [
    source 27
    target 1753
  ]
  edge [
    source 27
    target 1754
  ]
  edge [
    source 27
    target 1755
  ]
  edge [
    source 27
    target 1756
  ]
  edge [
    source 27
    target 1757
  ]
  edge [
    source 27
    target 1758
  ]
  edge [
    source 27
    target 1759
  ]
  edge [
    source 27
    target 1760
  ]
  edge [
    source 27
    target 1761
  ]
  edge [
    source 27
    target 1762
  ]
  edge [
    source 27
    target 1763
  ]
  edge [
    source 27
    target 1764
  ]
  edge [
    source 27
    target 1765
  ]
  edge [
    source 27
    target 1766
  ]
  edge [
    source 27
    target 1767
  ]
  edge [
    source 27
    target 1768
  ]
  edge [
    source 27
    target 1769
  ]
  edge [
    source 27
    target 1770
  ]
  edge [
    source 27
    target 1771
  ]
  edge [
    source 27
    target 1772
  ]
  edge [
    source 27
    target 1773
  ]
  edge [
    source 27
    target 1774
  ]
  edge [
    source 27
    target 1775
  ]
  edge [
    source 27
    target 1776
  ]
  edge [
    source 27
    target 1777
  ]
  edge [
    source 27
    target 1778
  ]
  edge [
    source 27
    target 1779
  ]
  edge [
    source 27
    target 1780
  ]
  edge [
    source 27
    target 1781
  ]
  edge [
    source 27
    target 1782
  ]
  edge [
    source 27
    target 1783
  ]
  edge [
    source 27
    target 1784
  ]
  edge [
    source 27
    target 1785
  ]
  edge [
    source 27
    target 1786
  ]
  edge [
    source 27
    target 1787
  ]
  edge [
    source 27
    target 1788
  ]
  edge [
    source 27
    target 1789
  ]
  edge [
    source 27
    target 1790
  ]
  edge [
    source 27
    target 1791
  ]
  edge [
    source 27
    target 1792
  ]
  edge [
    source 27
    target 1793
  ]
  edge [
    source 27
    target 1794
  ]
  edge [
    source 27
    target 1795
  ]
  edge [
    source 27
    target 1796
  ]
  edge [
    source 27
    target 1797
  ]
  edge [
    source 27
    target 1798
  ]
  edge [
    source 27
    target 1799
  ]
  edge [
    source 27
    target 1800
  ]
  edge [
    source 27
    target 1801
  ]
  edge [
    source 27
    target 1802
  ]
  edge [
    source 27
    target 1803
  ]
  edge [
    source 27
    target 1804
  ]
  edge [
    source 27
    target 1805
  ]
  edge [
    source 27
    target 1806
  ]
  edge [
    source 27
    target 1807
  ]
  edge [
    source 27
    target 1808
  ]
  edge [
    source 27
    target 1809
  ]
  edge [
    source 27
    target 1810
  ]
  edge [
    source 27
    target 1811
  ]
  edge [
    source 27
    target 1812
  ]
  edge [
    source 27
    target 1813
  ]
  edge [
    source 27
    target 1814
  ]
  edge [
    source 27
    target 1815
  ]
  edge [
    source 27
    target 1816
  ]
  edge [
    source 27
    target 1817
  ]
  edge [
    source 27
    target 1818
  ]
  edge [
    source 27
    target 1819
  ]
  edge [
    source 27
    target 1820
  ]
  edge [
    source 27
    target 1821
  ]
  edge [
    source 27
    target 1822
  ]
  edge [
    source 27
    target 1823
  ]
  edge [
    source 27
    target 1824
  ]
  edge [
    source 27
    target 1825
  ]
  edge [
    source 27
    target 1826
  ]
  edge [
    source 27
    target 1827
  ]
  edge [
    source 27
    target 1828
  ]
  edge [
    source 27
    target 1829
  ]
  edge [
    source 27
    target 1830
  ]
  edge [
    source 27
    target 1831
  ]
  edge [
    source 27
    target 1832
  ]
  edge [
    source 27
    target 1833
  ]
  edge [
    source 27
    target 1834
  ]
  edge [
    source 27
    target 1835
  ]
  edge [
    source 27
    target 1836
  ]
  edge [
    source 27
    target 1837
  ]
  edge [
    source 27
    target 1838
  ]
  edge [
    source 27
    target 1839
  ]
  edge [
    source 27
    target 1840
  ]
  edge [
    source 27
    target 1841
  ]
  edge [
    source 27
    target 1842
  ]
  edge [
    source 27
    target 1843
  ]
  edge [
    source 27
    target 1844
  ]
  edge [
    source 27
    target 1845
  ]
  edge [
    source 27
    target 1846
  ]
  edge [
    source 27
    target 1847
  ]
  edge [
    source 27
    target 1848
  ]
  edge [
    source 27
    target 1849
  ]
  edge [
    source 27
    target 1850
  ]
  edge [
    source 27
    target 1851
  ]
  edge [
    source 27
    target 1852
  ]
  edge [
    source 27
    target 1853
  ]
  edge [
    source 27
    target 1854
  ]
  edge [
    source 27
    target 1855
  ]
  edge [
    source 27
    target 1856
  ]
  edge [
    source 27
    target 1857
  ]
  edge [
    source 27
    target 1858
  ]
  edge [
    source 27
    target 1859
  ]
  edge [
    source 27
    target 1860
  ]
  edge [
    source 27
    target 1861
  ]
  edge [
    source 27
    target 1862
  ]
  edge [
    source 27
    target 1863
  ]
  edge [
    source 27
    target 1864
  ]
  edge [
    source 27
    target 1865
  ]
  edge [
    source 27
    target 1866
  ]
  edge [
    source 27
    target 1867
  ]
  edge [
    source 27
    target 1868
  ]
  edge [
    source 27
    target 1869
  ]
  edge [
    source 27
    target 1870
  ]
  edge [
    source 27
    target 1871
  ]
  edge [
    source 27
    target 1872
  ]
  edge [
    source 27
    target 1873
  ]
  edge [
    source 27
    target 1874
  ]
  edge [
    source 27
    target 1875
  ]
  edge [
    source 27
    target 1876
  ]
  edge [
    source 27
    target 1877
  ]
  edge [
    source 27
    target 1878
  ]
  edge [
    source 27
    target 1879
  ]
  edge [
    source 27
    target 1880
  ]
  edge [
    source 27
    target 1881
  ]
  edge [
    source 27
    target 1882
  ]
  edge [
    source 27
    target 1883
  ]
  edge [
    source 27
    target 1884
  ]
  edge [
    source 27
    target 1885
  ]
  edge [
    source 27
    target 1886
  ]
  edge [
    source 27
    target 1887
  ]
  edge [
    source 27
    target 1888
  ]
  edge [
    source 27
    target 1889
  ]
  edge [
    source 27
    target 1890
  ]
  edge [
    source 27
    target 1891
  ]
  edge [
    source 27
    target 1892
  ]
  edge [
    source 27
    target 1893
  ]
  edge [
    source 27
    target 1894
  ]
  edge [
    source 27
    target 1895
  ]
  edge [
    source 27
    target 1896
  ]
  edge [
    source 27
    target 1897
  ]
  edge [
    source 27
    target 1898
  ]
  edge [
    source 27
    target 1899
  ]
  edge [
    source 27
    target 1900
  ]
  edge [
    source 27
    target 1901
  ]
  edge [
    source 27
    target 1902
  ]
  edge [
    source 27
    target 1903
  ]
  edge [
    source 27
    target 1904
  ]
  edge [
    source 27
    target 1905
  ]
  edge [
    source 27
    target 1906
  ]
  edge [
    source 27
    target 1907
  ]
  edge [
    source 27
    target 1908
  ]
  edge [
    source 27
    target 1909
  ]
  edge [
    source 27
    target 1910
  ]
  edge [
    source 27
    target 1911
  ]
  edge [
    source 27
    target 1912
  ]
  edge [
    source 27
    target 1913
  ]
  edge [
    source 27
    target 1914
  ]
  edge [
    source 27
    target 1915
  ]
  edge [
    source 27
    target 1916
  ]
  edge [
    source 27
    target 1917
  ]
  edge [
    source 27
    target 1918
  ]
  edge [
    source 27
    target 1919
  ]
  edge [
    source 27
    target 1920
  ]
  edge [
    source 27
    target 1921
  ]
  edge [
    source 27
    target 1922
  ]
  edge [
    source 27
    target 1923
  ]
  edge [
    source 27
    target 1924
  ]
  edge [
    source 27
    target 1925
  ]
  edge [
    source 27
    target 1926
  ]
  edge [
    source 27
    target 1927
  ]
  edge [
    source 27
    target 1928
  ]
  edge [
    source 27
    target 1929
  ]
  edge [
    source 27
    target 1930
  ]
  edge [
    source 27
    target 1931
  ]
  edge [
    source 27
    target 1932
  ]
  edge [
    source 27
    target 1933
  ]
  edge [
    source 27
    target 1934
  ]
  edge [
    source 27
    target 972
  ]
  edge [
    source 27
    target 1935
  ]
  edge [
    source 27
    target 1936
  ]
  edge [
    source 27
    target 1937
  ]
  edge [
    source 27
    target 1938
  ]
  edge [
    source 27
    target 1939
  ]
  edge [
    source 27
    target 1940
  ]
  edge [
    source 27
    target 1941
  ]
  edge [
    source 27
    target 1942
  ]
  edge [
    source 27
    target 1943
  ]
  edge [
    source 27
    target 1944
  ]
  edge [
    source 27
    target 1945
  ]
  edge [
    source 27
    target 1946
  ]
  edge [
    source 27
    target 1947
  ]
  edge [
    source 27
    target 1948
  ]
  edge [
    source 27
    target 1949
  ]
  edge [
    source 27
    target 1950
  ]
  edge [
    source 27
    target 1951
  ]
  edge [
    source 27
    target 1952
  ]
  edge [
    source 27
    target 1953
  ]
  edge [
    source 27
    target 1954
  ]
  edge [
    source 27
    target 1955
  ]
  edge [
    source 27
    target 1956
  ]
  edge [
    source 27
    target 1957
  ]
  edge [
    source 27
    target 1958
  ]
  edge [
    source 27
    target 1959
  ]
  edge [
    source 27
    target 1960
  ]
  edge [
    source 27
    target 1961
  ]
  edge [
    source 27
    target 1962
  ]
  edge [
    source 27
    target 1963
  ]
  edge [
    source 27
    target 1964
  ]
  edge [
    source 27
    target 1965
  ]
  edge [
    source 27
    target 1966
  ]
  edge [
    source 27
    target 1967
  ]
  edge [
    source 27
    target 1968
  ]
  edge [
    source 27
    target 1969
  ]
  edge [
    source 27
    target 382
  ]
  edge [
    source 27
    target 1970
  ]
  edge [
    source 27
    target 231
  ]
  edge [
    source 27
    target 309
  ]
  edge [
    source 27
    target 342
  ]
  edge [
    source 27
    target 1574
  ]
  edge [
    source 27
    target 1971
  ]
  edge [
    source 27
    target 1972
  ]
  edge [
    source 27
    target 1973
  ]
  edge [
    source 27
    target 1974
  ]
  edge [
    source 27
    target 1975
  ]
  edge [
    source 27
    target 1976
  ]
  edge [
    source 27
    target 1977
  ]
  edge [
    source 27
    target 1978
  ]
  edge [
    source 27
    target 1979
  ]
  edge [
    source 27
    target 1980
  ]
  edge [
    source 27
    target 1981
  ]
  edge [
    source 27
    target 1982
  ]
  edge [
    source 27
    target 1983
  ]
  edge [
    source 27
    target 1984
  ]
  edge [
    source 27
    target 1985
  ]
  edge [
    source 27
    target 1986
  ]
  edge [
    source 27
    target 1987
  ]
  edge [
    source 27
    target 1988
  ]
  edge [
    source 27
    target 1298
  ]
  edge [
    source 27
    target 1989
  ]
  edge [
    source 27
    target 1990
  ]
  edge [
    source 28
    target 1095
  ]
  edge [
    source 28
    target 1991
  ]
  edge [
    source 28
    target 234
  ]
  edge [
    source 28
    target 1992
  ]
  edge [
    source 28
    target 1993
  ]
  edge [
    source 28
    target 1994
  ]
  edge [
    source 28
    target 1677
  ]
  edge [
    source 28
    target 1995
  ]
  edge [
    source 28
    target 1996
  ]
  edge [
    source 28
    target 1997
  ]
  edge [
    source 28
    target 1998
  ]
  edge [
    source 28
    target 1999
  ]
  edge [
    source 28
    target 1079
  ]
  edge [
    source 28
    target 1751
  ]
  edge [
    source 28
    target 1750
  ]
  edge [
    source 28
    target 1752
  ]
  edge [
    source 28
    target 1753
  ]
  edge [
    source 28
    target 1755
  ]
  edge [
    source 28
    target 1754
  ]
  edge [
    source 28
    target 1756
  ]
  edge [
    source 28
    target 1757
  ]
  edge [
    source 28
    target 1758
  ]
  edge [
    source 28
    target 1759
  ]
  edge [
    source 28
    target 1760
  ]
  edge [
    source 28
    target 1762
  ]
  edge [
    source 28
    target 1761
  ]
  edge [
    source 28
    target 1763
  ]
  edge [
    source 28
    target 1764
  ]
  edge [
    source 28
    target 1765
  ]
  edge [
    source 28
    target 1766
  ]
  edge [
    source 28
    target 1767
  ]
  edge [
    source 28
    target 1768
  ]
  edge [
    source 28
    target 1769
  ]
  edge [
    source 28
    target 1771
  ]
  edge [
    source 28
    target 1770
  ]
  edge [
    source 28
    target 1772
  ]
  edge [
    source 28
    target 1773
  ]
  edge [
    source 28
    target 1774
  ]
  edge [
    source 28
    target 1775
  ]
  edge [
    source 28
    target 1777
  ]
  edge [
    source 28
    target 1779
  ]
  edge [
    source 28
    target 1778
  ]
  edge [
    source 28
    target 1776
  ]
  edge [
    source 28
    target 1780
  ]
  edge [
    source 28
    target 1781
  ]
  edge [
    source 28
    target 1782
  ]
  edge [
    source 28
    target 1783
  ]
  edge [
    source 28
    target 1784
  ]
  edge [
    source 28
    target 1785
  ]
  edge [
    source 28
    target 1786
  ]
  edge [
    source 28
    target 1787
  ]
  edge [
    source 28
    target 1788
  ]
  edge [
    source 28
    target 1789
  ]
  edge [
    source 28
    target 1790
  ]
  edge [
    source 28
    target 1791
  ]
  edge [
    source 28
    target 1792
  ]
  edge [
    source 28
    target 1793
  ]
  edge [
    source 28
    target 1794
  ]
  edge [
    source 28
    target 1795
  ]
  edge [
    source 28
    target 1796
  ]
  edge [
    source 28
    target 1797
  ]
  edge [
    source 28
    target 1798
  ]
  edge [
    source 28
    target 1799
  ]
  edge [
    source 28
    target 1800
  ]
  edge [
    source 28
    target 1802
  ]
  edge [
    source 28
    target 1801
  ]
  edge [
    source 28
    target 1803
  ]
  edge [
    source 28
    target 1804
  ]
  edge [
    source 28
    target 1805
  ]
  edge [
    source 28
    target 1806
  ]
  edge [
    source 28
    target 1807
  ]
  edge [
    source 28
    target 1808
  ]
  edge [
    source 28
    target 1809
  ]
  edge [
    source 28
    target 1810
  ]
  edge [
    source 28
    target 1811
  ]
  edge [
    source 28
    target 1812
  ]
  edge [
    source 28
    target 1813
  ]
  edge [
    source 28
    target 1814
  ]
  edge [
    source 28
    target 1815
  ]
  edge [
    source 28
    target 1816
  ]
  edge [
    source 28
    target 1817
  ]
  edge [
    source 28
    target 1818
  ]
  edge [
    source 28
    target 1819
  ]
  edge [
    source 28
    target 1820
  ]
  edge [
    source 28
    target 1821
  ]
  edge [
    source 28
    target 1822
  ]
  edge [
    source 28
    target 1824
  ]
  edge [
    source 28
    target 1823
  ]
  edge [
    source 28
    target 1825
  ]
  edge [
    source 28
    target 1826
  ]
  edge [
    source 28
    target 1827
  ]
  edge [
    source 28
    target 1828
  ]
  edge [
    source 28
    target 1829
  ]
  edge [
    source 28
    target 1830
  ]
  edge [
    source 28
    target 1831
  ]
  edge [
    source 28
    target 1832
  ]
  edge [
    source 28
    target 1834
  ]
  edge [
    source 28
    target 1833
  ]
  edge [
    source 28
    target 1836
  ]
  edge [
    source 28
    target 1835
  ]
  edge [
    source 28
    target 1837
  ]
  edge [
    source 28
    target 1838
  ]
  edge [
    source 28
    target 1840
  ]
  edge [
    source 28
    target 1841
  ]
  edge [
    source 28
    target 1839
  ]
  edge [
    source 28
    target 1842
  ]
  edge [
    source 28
    target 1843
  ]
  edge [
    source 28
    target 1846
  ]
  edge [
    source 28
    target 1844
  ]
  edge [
    source 28
    target 1851
  ]
  edge [
    source 28
    target 1849
  ]
  edge [
    source 28
    target 1850
  ]
  edge [
    source 28
    target 1845
  ]
  edge [
    source 28
    target 1848
  ]
  edge [
    source 28
    target 1847
  ]
  edge [
    source 28
    target 1852
  ]
  edge [
    source 28
    target 1854
  ]
  edge [
    source 28
    target 1853
  ]
  edge [
    source 28
    target 1855
  ]
  edge [
    source 28
    target 1856
  ]
  edge [
    source 28
    target 1857
  ]
  edge [
    source 28
    target 1858
  ]
  edge [
    source 28
    target 1859
  ]
  edge [
    source 28
    target 1860
  ]
  edge [
    source 28
    target 1861
  ]
  edge [
    source 28
    target 1863
  ]
  edge [
    source 28
    target 1862
  ]
  edge [
    source 28
    target 1864
  ]
  edge [
    source 28
    target 1865
  ]
  edge [
    source 28
    target 1866
  ]
  edge [
    source 28
    target 1867
  ]
  edge [
    source 28
    target 1868
  ]
  edge [
    source 28
    target 1869
  ]
  edge [
    source 28
    target 1870
  ]
  edge [
    source 28
    target 1871
  ]
  edge [
    source 28
    target 1872
  ]
  edge [
    source 28
    target 1873
  ]
  edge [
    source 28
    target 1874
  ]
  edge [
    source 28
    target 1875
  ]
  edge [
    source 28
    target 1876
  ]
  edge [
    source 28
    target 1877
  ]
  edge [
    source 28
    target 1878
  ]
  edge [
    source 28
    target 1882
  ]
  edge [
    source 28
    target 1880
  ]
  edge [
    source 28
    target 1881
  ]
  edge [
    source 28
    target 1879
  ]
  edge [
    source 28
    target 1883
  ]
  edge [
    source 28
    target 1887
  ]
  edge [
    source 28
    target 1886
  ]
  edge [
    source 28
    target 1890
  ]
  edge [
    source 28
    target 1885
  ]
  edge [
    source 28
    target 1889
  ]
  edge [
    source 28
    target 1884
  ]
  edge [
    source 28
    target 1888
  ]
  edge [
    source 28
    target 1892
  ]
  edge [
    source 28
    target 1891
  ]
  edge [
    source 28
    target 1893
  ]
  edge [
    source 28
    target 1894
  ]
  edge [
    source 28
    target 1896
  ]
  edge [
    source 28
    target 1895
  ]
  edge [
    source 28
    target 1897
  ]
  edge [
    source 28
    target 1898
  ]
  edge [
    source 28
    target 1899
  ]
  edge [
    source 28
    target 1900
  ]
  edge [
    source 28
    target 1901
  ]
  edge [
    source 28
    target 1902
  ]
  edge [
    source 28
    target 1903
  ]
  edge [
    source 28
    target 1904
  ]
  edge [
    source 28
    target 1905
  ]
  edge [
    source 28
    target 1906
  ]
  edge [
    source 28
    target 1907
  ]
  edge [
    source 28
    target 1908
  ]
  edge [
    source 28
    target 1909
  ]
  edge [
    source 28
    target 1910
  ]
  edge [
    source 28
    target 1911
  ]
  edge [
    source 28
    target 1912
  ]
  edge [
    source 28
    target 1913
  ]
  edge [
    source 28
    target 1914
  ]
  edge [
    source 28
    target 1915
  ]
  edge [
    source 28
    target 1916
  ]
  edge [
    source 28
    target 1921
  ]
  edge [
    source 28
    target 1920
  ]
  edge [
    source 28
    target 1918
  ]
  edge [
    source 28
    target 1919
  ]
  edge [
    source 28
    target 1917
  ]
  edge [
    source 28
    target 1922
  ]
  edge [
    source 28
    target 1923
  ]
  edge [
    source 28
    target 1924
  ]
  edge [
    source 28
    target 1925
  ]
  edge [
    source 28
    target 1926
  ]
  edge [
    source 28
    target 1927
  ]
  edge [
    source 28
    target 1928
  ]
  edge [
    source 28
    target 1929
  ]
  edge [
    source 28
    target 1931
  ]
  edge [
    source 28
    target 1930
  ]
  edge [
    source 28
    target 1933
  ]
  edge [
    source 28
    target 1932
  ]
  edge [
    source 28
    target 1934
  ]
  edge [
    source 28
    target 972
  ]
  edge [
    source 28
    target 1935
  ]
  edge [
    source 28
    target 1936
  ]
  edge [
    source 28
    target 1937
  ]
  edge [
    source 28
    target 1938
  ]
  edge [
    source 28
    target 1939
  ]
  edge [
    source 28
    target 1940
  ]
  edge [
    source 28
    target 1941
  ]
  edge [
    source 28
    target 1943
  ]
  edge [
    source 28
    target 1942
  ]
  edge [
    source 28
    target 1944
  ]
  edge [
    source 28
    target 1945
  ]
  edge [
    source 28
    target 1946
  ]
  edge [
    source 28
    target 1947
  ]
  edge [
    source 28
    target 1948
  ]
  edge [
    source 28
    target 1949
  ]
  edge [
    source 28
    target 1950
  ]
  edge [
    source 28
    target 1951
  ]
  edge [
    source 28
    target 1952
  ]
  edge [
    source 28
    target 1953
  ]
  edge [
    source 28
    target 1954
  ]
  edge [
    source 28
    target 1955
  ]
  edge [
    source 28
    target 1956
  ]
  edge [
    source 28
    target 1957
  ]
  edge [
    source 28
    target 1958
  ]
  edge [
    source 28
    target 1959
  ]
  edge [
    source 28
    target 1960
  ]
  edge [
    source 28
    target 1961
  ]
  edge [
    source 28
    target 1962
  ]
  edge [
    source 28
    target 1963
  ]
  edge [
    source 28
    target 194
  ]
  edge [
    source 28
    target 515
  ]
  edge [
    source 28
    target 1691
  ]
  edge [
    source 28
    target 511
  ]
  edge [
    source 28
    target 1695
  ]
  edge [
    source 28
    target 202
  ]
  edge [
    source 28
    target 1696
  ]
  edge [
    source 28
    target 1697
  ]
  edge [
    source 28
    target 1698
  ]
  edge [
    source 28
    target 205
  ]
  edge [
    source 28
    target 93
  ]
  edge [
    source 28
    target 206
  ]
  edge [
    source 28
    target 1699
  ]
  edge [
    source 28
    target 215
  ]
  edge [
    source 28
    target 218
  ]
  edge [
    source 28
    target 1700
  ]
  edge [
    source 28
    target 219
  ]
  edge [
    source 28
    target 220
  ]
  edge [
    source 28
    target 1701
  ]
  edge [
    source 28
    target 1689
  ]
  edge [
    source 28
    target 221
  ]
  edge [
    source 28
    target 1702
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 28
    target 1703
  ]
  edge [
    source 28
    target 1684
  ]
  edge [
    source 28
    target 1089
  ]
  edge [
    source 28
    target 1685
  ]
  edge [
    source 28
    target 1686
  ]
  edge [
    source 28
    target 1687
  ]
  edge [
    source 28
    target 1688
  ]
  edge [
    source 28
    target 1690
  ]
  edge [
    source 28
    target 1692
  ]
  edge [
    source 28
    target 1693
  ]
  edge [
    source 28
    target 1694
  ]
  edge [
    source 28
    target 2000
  ]
  edge [
    source 28
    target 2001
  ]
  edge [
    source 28
    target 2002
  ]
  edge [
    source 28
    target 2003
  ]
  edge [
    source 28
    target 2004
  ]
  edge [
    source 28
    target 2005
  ]
  edge [
    source 28
    target 727
  ]
  edge [
    source 28
    target 2006
  ]
  edge [
    source 28
    target 2007
  ]
  edge [
    source 28
    target 2008
  ]
  edge [
    source 28
    target 2009
  ]
  edge [
    source 28
    target 2010
  ]
  edge [
    source 28
    target 2011
  ]
  edge [
    source 28
    target 2012
  ]
  edge [
    source 28
    target 2013
  ]
  edge [
    source 28
    target 2014
  ]
  edge [
    source 28
    target 2015
  ]
  edge [
    source 28
    target 2016
  ]
  edge [
    source 28
    target 2017
  ]
  edge [
    source 28
    target 2018
  ]
  edge [
    source 28
    target 2019
  ]
  edge [
    source 28
    target 2020
  ]
  edge [
    source 28
    target 2021
  ]
  edge [
    source 28
    target 2022
  ]
  edge [
    source 28
    target 2023
  ]
  edge [
    source 28
    target 2024
  ]
  edge [
    source 28
    target 1088
  ]
  edge [
    source 28
    target 1977
  ]
  edge [
    source 28
    target 1976
  ]
  edge [
    source 28
    target 2025
  ]
  edge [
    source 28
    target 2026
  ]
  edge [
    source 28
    target 333
  ]
  edge [
    source 28
    target 2027
  ]
  edge [
    source 28
    target 1081
  ]
  edge [
    source 28
    target 1082
  ]
  edge [
    source 28
    target 1083
  ]
  edge [
    source 28
    target 1084
  ]
  edge [
    source 28
    target 1085
  ]
  edge [
    source 28
    target 1086
  ]
  edge [
    source 28
    target 1087
  ]
  edge [
    source 28
    target 2028
  ]
  edge [
    source 28
    target 288
  ]
  edge [
    source 28
    target 1716
  ]
  edge [
    source 177
    target 2029
  ]
  edge [
    source 177
    target 2030
  ]
  edge [
    source 177
    target 2031
  ]
  edge [
    source 177
    target 883
  ]
  edge [
    source 177
    target 2032
  ]
  edge [
    source 177
    target 2033
  ]
  edge [
    source 177
    target 1992
  ]
  edge [
    source 177
    target 2034
  ]
  edge [
    source 177
    target 2035
  ]
  edge [
    source 883
    target 2029
  ]
  edge [
    source 883
    target 2030
  ]
  edge [
    source 883
    target 2031
  ]
  edge [
    source 883
    target 2032
  ]
  edge [
    source 883
    target 2033
  ]
  edge [
    source 883
    target 1992
  ]
  edge [
    source 883
    target 2034
  ]
  edge [
    source 883
    target 2035
  ]
  edge [
    source 883
    target 2045
  ]
  edge [
    source 883
    target 2046
  ]
  edge [
    source 883
    target 2047
  ]
  edge [
    source 883
    target 2048
  ]
  edge [
    source 883
    target 2049
  ]
  edge [
    source 883
    target 2050
  ]
  edge [
    source 883
    target 2051
  ]
  edge [
    source 1992
    target 2029
  ]
  edge [
    source 1992
    target 2030
  ]
  edge [
    source 1992
    target 2031
  ]
  edge [
    source 1992
    target 2032
  ]
  edge [
    source 1992
    target 2033
  ]
  edge [
    source 1992
    target 2034
  ]
  edge [
    source 1992
    target 2035
  ]
  edge [
    source 1992
    target 2038
  ]
  edge [
    source 1992
    target 2039
  ]
  edge [
    source 1992
    target 2040
  ]
  edge [
    source 1992
    target 2041
  ]
  edge [
    source 1992
    target 2042
  ]
  edge [
    source 1992
    target 2043
  ]
  edge [
    source 1992
    target 2044
  ]
  edge [
    source 1992
    target 2045
  ]
  edge [
    source 1992
    target 2046
  ]
  edge [
    source 1992
    target 2047
  ]
  edge [
    source 1992
    target 2048
  ]
  edge [
    source 1992
    target 2049
  ]
  edge [
    source 1992
    target 2050
  ]
  edge [
    source 1992
    target 2051
  ]
  edge [
    source 2029
    target 2030
  ]
  edge [
    source 2029
    target 2031
  ]
  edge [
    source 2029
    target 2032
  ]
  edge [
    source 2029
    target 2033
  ]
  edge [
    source 2029
    target 2034
  ]
  edge [
    source 2029
    target 2035
  ]
  edge [
    source 2029
    target 2038
  ]
  edge [
    source 2029
    target 2039
  ]
  edge [
    source 2029
    target 2040
  ]
  edge [
    source 2029
    target 2041
  ]
  edge [
    source 2029
    target 2042
  ]
  edge [
    source 2029
    target 2043
  ]
  edge [
    source 2029
    target 2044
  ]
  edge [
    source 2029
    target 2045
  ]
  edge [
    source 2029
    target 2046
  ]
  edge [
    source 2029
    target 2047
  ]
  edge [
    source 2029
    target 2048
  ]
  edge [
    source 2029
    target 2049
  ]
  edge [
    source 2029
    target 2050
  ]
  edge [
    source 2029
    target 2051
  ]
  edge [
    source 2030
    target 2031
  ]
  edge [
    source 2030
    target 2032
  ]
  edge [
    source 2030
    target 2033
  ]
  edge [
    source 2030
    target 2034
  ]
  edge [
    source 2030
    target 2035
  ]
  edge [
    source 2030
    target 2038
  ]
  edge [
    source 2030
    target 2039
  ]
  edge [
    source 2030
    target 2040
  ]
  edge [
    source 2030
    target 2041
  ]
  edge [
    source 2030
    target 2042
  ]
  edge [
    source 2030
    target 2043
  ]
  edge [
    source 2030
    target 2044
  ]
  edge [
    source 2030
    target 2045
  ]
  edge [
    source 2030
    target 2046
  ]
  edge [
    source 2030
    target 2047
  ]
  edge [
    source 2030
    target 2048
  ]
  edge [
    source 2030
    target 2049
  ]
  edge [
    source 2030
    target 2050
  ]
  edge [
    source 2030
    target 2051
  ]
  edge [
    source 2031
    target 2032
  ]
  edge [
    source 2031
    target 2033
  ]
  edge [
    source 2031
    target 2034
  ]
  edge [
    source 2031
    target 2035
  ]
  edge [
    source 2032
    target 2033
  ]
  edge [
    source 2032
    target 2034
  ]
  edge [
    source 2032
    target 2035
  ]
  edge [
    source 2033
    target 2034
  ]
  edge [
    source 2033
    target 2035
  ]
  edge [
    source 2033
    target 2038
  ]
  edge [
    source 2033
    target 2039
  ]
  edge [
    source 2033
    target 2040
  ]
  edge [
    source 2033
    target 2041
  ]
  edge [
    source 2033
    target 2042
  ]
  edge [
    source 2033
    target 2043
  ]
  edge [
    source 2033
    target 2044
  ]
  edge [
    source 2033
    target 2045
  ]
  edge [
    source 2033
    target 2046
  ]
  edge [
    source 2033
    target 2047
  ]
  edge [
    source 2033
    target 2048
  ]
  edge [
    source 2033
    target 2049
  ]
  edge [
    source 2033
    target 2050
  ]
  edge [
    source 2033
    target 2051
  ]
  edge [
    source 2034
    target 2035
  ]
  edge [
    source 2036
    target 2037
  ]
  edge [
    source 2038
    target 2039
  ]
  edge [
    source 2038
    target 2040
  ]
  edge [
    source 2038
    target 2041
  ]
  edge [
    source 2038
    target 2042
  ]
  edge [
    source 2038
    target 2043
  ]
  edge [
    source 2038
    target 2044
  ]
  edge [
    source 2039
    target 2040
  ]
  edge [
    source 2039
    target 2041
  ]
  edge [
    source 2039
    target 2042
  ]
  edge [
    source 2039
    target 2043
  ]
  edge [
    source 2039
    target 2044
  ]
  edge [
    source 2040
    target 2041
  ]
  edge [
    source 2040
    target 2042
  ]
  edge [
    source 2040
    target 2043
  ]
  edge [
    source 2040
    target 2044
  ]
  edge [
    source 2041
    target 2042
  ]
  edge [
    source 2041
    target 2043
  ]
  edge [
    source 2041
    target 2044
  ]
  edge [
    source 2042
    target 2043
  ]
  edge [
    source 2042
    target 2044
  ]
  edge [
    source 2043
    target 2044
  ]
  edge [
    source 2045
    target 2046
  ]
  edge [
    source 2045
    target 2047
  ]
  edge [
    source 2045
    target 2048
  ]
  edge [
    source 2045
    target 2049
  ]
  edge [
    source 2045
    target 2050
  ]
  edge [
    source 2045
    target 2051
  ]
  edge [
    source 2046
    target 2047
  ]
  edge [
    source 2046
    target 2048
  ]
  edge [
    source 2046
    target 2049
  ]
  edge [
    source 2046
    target 2050
  ]
  edge [
    source 2046
    target 2051
  ]
  edge [
    source 2047
    target 2048
  ]
  edge [
    source 2047
    target 2049
  ]
  edge [
    source 2047
    target 2050
  ]
  edge [
    source 2047
    target 2051
  ]
  edge [
    source 2048
    target 2049
  ]
  edge [
    source 2048
    target 2050
  ]
  edge [
    source 2048
    target 2051
  ]
  edge [
    source 2049
    target 2050
  ]
  edge [
    source 2049
    target 2051
  ]
  edge [
    source 2050
    target 2051
  ]
]
