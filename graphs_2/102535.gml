graph [
  node [
    id 0
    label "serwis"
    origin "text"
  ]
  node [
    id 1
    label "wikibooks"
    origin "text"
  ]
  node [
    id 2
    label "dost&#281;pny"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "podr&#281;cznik"
    origin "text"
  ]
  node [
    id 5
    label "pomaga&#263;"
    origin "text"
  ]
  node [
    id 6
    label "zapozna&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "standardowy"
    origin "text"
  ]
  node [
    id 9
    label "wersja"
    origin "text"
  ]
  node [
    id 10
    label "oprogramowanie"
    origin "text"
  ]
  node [
    id 11
    label "ponadto"
    origin "text"
  ]
  node [
    id 12
    label "rama"
    origin "text"
  ]
  node [
    id 13
    label "wika"
    origin "text"
  ]
  node [
    id 14
    label "projekt"
    origin "text"
  ]
  node [
    id 15
    label "wolne"
    origin "text"
  ]
  node [
    id 16
    label "strona"
    origin "text"
  ]
  node [
    id 17
    label "porada"
    origin "text"
  ]
  node [
    id 18
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "zasada"
    origin "text"
  ]
  node [
    id 20
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 21
    label "redagowa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "specyficzny"
    origin "text"
  ]
  node [
    id 23
    label "dla"
    origin "text"
  ]
  node [
    id 24
    label "lista"
    origin "text"
  ]
  node [
    id 25
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "punkt"
  ]
  node [
    id 27
    label "YouTube"
  ]
  node [
    id 28
    label "wytw&#243;r"
  ]
  node [
    id 29
    label "zak&#322;ad"
  ]
  node [
    id 30
    label "uderzenie"
  ]
  node [
    id 31
    label "service"
  ]
  node [
    id 32
    label "us&#322;uga"
  ]
  node [
    id 33
    label "porcja"
  ]
  node [
    id 34
    label "zastawa"
  ]
  node [
    id 35
    label "mecz"
  ]
  node [
    id 36
    label "doniesienie"
  ]
  node [
    id 37
    label "instrumentalizacja"
  ]
  node [
    id 38
    label "trafienie"
  ]
  node [
    id 39
    label "walka"
  ]
  node [
    id 40
    label "cios"
  ]
  node [
    id 41
    label "zdarzenie_si&#281;"
  ]
  node [
    id 42
    label "wdarcie_si&#281;"
  ]
  node [
    id 43
    label "pogorszenie"
  ]
  node [
    id 44
    label "d&#378;wi&#281;k"
  ]
  node [
    id 45
    label "poczucie"
  ]
  node [
    id 46
    label "coup"
  ]
  node [
    id 47
    label "reakcja"
  ]
  node [
    id 48
    label "contact"
  ]
  node [
    id 49
    label "stukni&#281;cie"
  ]
  node [
    id 50
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 51
    label "bat"
  ]
  node [
    id 52
    label "spowodowanie"
  ]
  node [
    id 53
    label "rush"
  ]
  node [
    id 54
    label "odbicie"
  ]
  node [
    id 55
    label "dawka"
  ]
  node [
    id 56
    label "zadanie"
  ]
  node [
    id 57
    label "&#347;ci&#281;cie"
  ]
  node [
    id 58
    label "st&#322;uczenie"
  ]
  node [
    id 59
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 60
    label "time"
  ]
  node [
    id 61
    label "odbicie_si&#281;"
  ]
  node [
    id 62
    label "dotkni&#281;cie"
  ]
  node [
    id 63
    label "charge"
  ]
  node [
    id 64
    label "dostanie"
  ]
  node [
    id 65
    label "skrytykowanie"
  ]
  node [
    id 66
    label "zagrywka"
  ]
  node [
    id 67
    label "manewr"
  ]
  node [
    id 68
    label "nast&#261;pienie"
  ]
  node [
    id 69
    label "uderzanie"
  ]
  node [
    id 70
    label "pogoda"
  ]
  node [
    id 71
    label "stroke"
  ]
  node [
    id 72
    label "pobicie"
  ]
  node [
    id 73
    label "ruch"
  ]
  node [
    id 74
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 75
    label "flap"
  ]
  node [
    id 76
    label "dotyk"
  ]
  node [
    id 77
    label "zrobienie"
  ]
  node [
    id 78
    label "produkt_gotowy"
  ]
  node [
    id 79
    label "asortyment"
  ]
  node [
    id 80
    label "czynno&#347;&#263;"
  ]
  node [
    id 81
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 82
    label "&#347;wiadczenie"
  ]
  node [
    id 83
    label "element_wyposa&#380;enia"
  ]
  node [
    id 84
    label "sto&#322;owizna"
  ]
  node [
    id 85
    label "przedmiot"
  ]
  node [
    id 86
    label "p&#322;&#243;d"
  ]
  node [
    id 87
    label "work"
  ]
  node [
    id 88
    label "rezultat"
  ]
  node [
    id 89
    label "zas&#243;b"
  ]
  node [
    id 90
    label "ilo&#347;&#263;"
  ]
  node [
    id 91
    label "&#380;o&#322;d"
  ]
  node [
    id 92
    label "zak&#322;adka"
  ]
  node [
    id 93
    label "jednostka_organizacyjna"
  ]
  node [
    id 94
    label "miejsce_pracy"
  ]
  node [
    id 95
    label "instytucja"
  ]
  node [
    id 96
    label "wyko&#324;czenie"
  ]
  node [
    id 97
    label "firma"
  ]
  node [
    id 98
    label "czyn"
  ]
  node [
    id 99
    label "company"
  ]
  node [
    id 100
    label "instytut"
  ]
  node [
    id 101
    label "umowa"
  ]
  node [
    id 102
    label "po&#322;o&#380;enie"
  ]
  node [
    id 103
    label "sprawa"
  ]
  node [
    id 104
    label "ust&#281;p"
  ]
  node [
    id 105
    label "plan"
  ]
  node [
    id 106
    label "obiekt_matematyczny"
  ]
  node [
    id 107
    label "problemat"
  ]
  node [
    id 108
    label "plamka"
  ]
  node [
    id 109
    label "stopie&#324;_pisma"
  ]
  node [
    id 110
    label "jednostka"
  ]
  node [
    id 111
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 112
    label "miejsce"
  ]
  node [
    id 113
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 114
    label "mark"
  ]
  node [
    id 115
    label "chwila"
  ]
  node [
    id 116
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 117
    label "prosta"
  ]
  node [
    id 118
    label "problematyka"
  ]
  node [
    id 119
    label "obiekt"
  ]
  node [
    id 120
    label "zapunktowa&#263;"
  ]
  node [
    id 121
    label "podpunkt"
  ]
  node [
    id 122
    label "wojsko"
  ]
  node [
    id 123
    label "kres"
  ]
  node [
    id 124
    label "przestrze&#324;"
  ]
  node [
    id 125
    label "point"
  ]
  node [
    id 126
    label "pozycja"
  ]
  node [
    id 127
    label "obrona"
  ]
  node [
    id 128
    label "gra"
  ]
  node [
    id 129
    label "game"
  ]
  node [
    id 130
    label "serw"
  ]
  node [
    id 131
    label "dwumecz"
  ]
  node [
    id 132
    label "kartka"
  ]
  node [
    id 133
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 134
    label "logowanie"
  ]
  node [
    id 135
    label "plik"
  ]
  node [
    id 136
    label "s&#261;d"
  ]
  node [
    id 137
    label "adres_internetowy"
  ]
  node [
    id 138
    label "linia"
  ]
  node [
    id 139
    label "serwis_internetowy"
  ]
  node [
    id 140
    label "posta&#263;"
  ]
  node [
    id 141
    label "bok"
  ]
  node [
    id 142
    label "skr&#281;canie"
  ]
  node [
    id 143
    label "skr&#281;ca&#263;"
  ]
  node [
    id 144
    label "orientowanie"
  ]
  node [
    id 145
    label "skr&#281;ci&#263;"
  ]
  node [
    id 146
    label "uj&#281;cie"
  ]
  node [
    id 147
    label "zorientowanie"
  ]
  node [
    id 148
    label "ty&#322;"
  ]
  node [
    id 149
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 150
    label "fragment"
  ]
  node [
    id 151
    label "layout"
  ]
  node [
    id 152
    label "zorientowa&#263;"
  ]
  node [
    id 153
    label "pagina"
  ]
  node [
    id 154
    label "podmiot"
  ]
  node [
    id 155
    label "g&#243;ra"
  ]
  node [
    id 156
    label "orientowa&#263;"
  ]
  node [
    id 157
    label "voice"
  ]
  node [
    id 158
    label "orientacja"
  ]
  node [
    id 159
    label "prz&#243;d"
  ]
  node [
    id 160
    label "internet"
  ]
  node [
    id 161
    label "powierzchnia"
  ]
  node [
    id 162
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 163
    label "forma"
  ]
  node [
    id 164
    label "skr&#281;cenie"
  ]
  node [
    id 165
    label "do&#322;&#261;czenie"
  ]
  node [
    id 166
    label "message"
  ]
  node [
    id 167
    label "naznoszenie"
  ]
  node [
    id 168
    label "zawiadomienie"
  ]
  node [
    id 169
    label "zniesienie"
  ]
  node [
    id 170
    label "zaniesienie"
  ]
  node [
    id 171
    label "announcement"
  ]
  node [
    id 172
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 173
    label "fetch"
  ]
  node [
    id 174
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 175
    label "poinformowanie"
  ]
  node [
    id 176
    label "mo&#380;liwy"
  ]
  node [
    id 177
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 178
    label "odblokowanie_si&#281;"
  ]
  node [
    id 179
    label "zrozumia&#322;y"
  ]
  node [
    id 180
    label "dost&#281;pnie"
  ]
  node [
    id 181
    label "&#322;atwy"
  ]
  node [
    id 182
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 183
    label "przyst&#281;pnie"
  ]
  node [
    id 184
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 185
    label "&#322;atwo"
  ]
  node [
    id 186
    label "letki"
  ]
  node [
    id 187
    label "prosty"
  ]
  node [
    id 188
    label "&#322;acny"
  ]
  node [
    id 189
    label "snadny"
  ]
  node [
    id 190
    label "przyjemny"
  ]
  node [
    id 191
    label "urealnianie"
  ]
  node [
    id 192
    label "mo&#380;ebny"
  ]
  node [
    id 193
    label "umo&#380;liwianie"
  ]
  node [
    id 194
    label "zno&#347;ny"
  ]
  node [
    id 195
    label "umo&#380;liwienie"
  ]
  node [
    id 196
    label "mo&#380;liwie"
  ]
  node [
    id 197
    label "urealnienie"
  ]
  node [
    id 198
    label "pojmowalny"
  ]
  node [
    id 199
    label "uzasadniony"
  ]
  node [
    id 200
    label "wyja&#347;nienie"
  ]
  node [
    id 201
    label "wyja&#347;nianie_si&#281;"
  ]
  node [
    id 202
    label "rozja&#347;nienie"
  ]
  node [
    id 203
    label "przeja&#347;nienie_si&#281;"
  ]
  node [
    id 204
    label "zrozumiale"
  ]
  node [
    id 205
    label "t&#322;umaczenie"
  ]
  node [
    id 206
    label "przeja&#347;nianie_si&#281;"
  ]
  node [
    id 207
    label "sensowny"
  ]
  node [
    id 208
    label "rozja&#347;nianie"
  ]
  node [
    id 209
    label "wyja&#347;nienie_si&#281;"
  ]
  node [
    id 210
    label "przyst&#281;pny"
  ]
  node [
    id 211
    label "wygodnie"
  ]
  node [
    id 212
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 213
    label "mie&#263;_miejsce"
  ]
  node [
    id 214
    label "equal"
  ]
  node [
    id 215
    label "trwa&#263;"
  ]
  node [
    id 216
    label "chodzi&#263;"
  ]
  node [
    id 217
    label "si&#281;ga&#263;"
  ]
  node [
    id 218
    label "stan"
  ]
  node [
    id 219
    label "obecno&#347;&#263;"
  ]
  node [
    id 220
    label "stand"
  ]
  node [
    id 221
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 222
    label "uczestniczy&#263;"
  ]
  node [
    id 223
    label "participate"
  ]
  node [
    id 224
    label "robi&#263;"
  ]
  node [
    id 225
    label "istnie&#263;"
  ]
  node [
    id 226
    label "pozostawa&#263;"
  ]
  node [
    id 227
    label "zostawa&#263;"
  ]
  node [
    id 228
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 229
    label "adhere"
  ]
  node [
    id 230
    label "compass"
  ]
  node [
    id 231
    label "korzysta&#263;"
  ]
  node [
    id 232
    label "appreciation"
  ]
  node [
    id 233
    label "osi&#261;ga&#263;"
  ]
  node [
    id 234
    label "dociera&#263;"
  ]
  node [
    id 235
    label "get"
  ]
  node [
    id 236
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 237
    label "mierzy&#263;"
  ]
  node [
    id 238
    label "u&#380;ywa&#263;"
  ]
  node [
    id 239
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 240
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 241
    label "exsert"
  ]
  node [
    id 242
    label "being"
  ]
  node [
    id 243
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 244
    label "cecha"
  ]
  node [
    id 245
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 246
    label "p&#322;ywa&#263;"
  ]
  node [
    id 247
    label "run"
  ]
  node [
    id 248
    label "bangla&#263;"
  ]
  node [
    id 249
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 250
    label "przebiega&#263;"
  ]
  node [
    id 251
    label "wk&#322;ada&#263;"
  ]
  node [
    id 252
    label "proceed"
  ]
  node [
    id 253
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 254
    label "carry"
  ]
  node [
    id 255
    label "bywa&#263;"
  ]
  node [
    id 256
    label "dziama&#263;"
  ]
  node [
    id 257
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 258
    label "stara&#263;_si&#281;"
  ]
  node [
    id 259
    label "para"
  ]
  node [
    id 260
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 261
    label "str&#243;j"
  ]
  node [
    id 262
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 263
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 264
    label "krok"
  ]
  node [
    id 265
    label "tryb"
  ]
  node [
    id 266
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 267
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 268
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 269
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 270
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 271
    label "continue"
  ]
  node [
    id 272
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 273
    label "Ohio"
  ]
  node [
    id 274
    label "wci&#281;cie"
  ]
  node [
    id 275
    label "Nowy_York"
  ]
  node [
    id 276
    label "warstwa"
  ]
  node [
    id 277
    label "samopoczucie"
  ]
  node [
    id 278
    label "Illinois"
  ]
  node [
    id 279
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 280
    label "state"
  ]
  node [
    id 281
    label "Jukatan"
  ]
  node [
    id 282
    label "Kalifornia"
  ]
  node [
    id 283
    label "Wirginia"
  ]
  node [
    id 284
    label "wektor"
  ]
  node [
    id 285
    label "Goa"
  ]
  node [
    id 286
    label "Teksas"
  ]
  node [
    id 287
    label "Waszyngton"
  ]
  node [
    id 288
    label "Massachusetts"
  ]
  node [
    id 289
    label "Alaska"
  ]
  node [
    id 290
    label "Arakan"
  ]
  node [
    id 291
    label "Hawaje"
  ]
  node [
    id 292
    label "Maryland"
  ]
  node [
    id 293
    label "Michigan"
  ]
  node [
    id 294
    label "Arizona"
  ]
  node [
    id 295
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 296
    label "Georgia"
  ]
  node [
    id 297
    label "poziom"
  ]
  node [
    id 298
    label "Pensylwania"
  ]
  node [
    id 299
    label "shape"
  ]
  node [
    id 300
    label "Luizjana"
  ]
  node [
    id 301
    label "Nowy_Meksyk"
  ]
  node [
    id 302
    label "Alabama"
  ]
  node [
    id 303
    label "Kansas"
  ]
  node [
    id 304
    label "Oregon"
  ]
  node [
    id 305
    label "Oklahoma"
  ]
  node [
    id 306
    label "Floryda"
  ]
  node [
    id 307
    label "jednostka_administracyjna"
  ]
  node [
    id 308
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 309
    label "wyprawka"
  ]
  node [
    id 310
    label "pomoc_naukowa"
  ]
  node [
    id 311
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 312
    label "egzemplarz"
  ]
  node [
    id 313
    label "rozdzia&#322;"
  ]
  node [
    id 314
    label "wk&#322;ad"
  ]
  node [
    id 315
    label "tytu&#322;"
  ]
  node [
    id 316
    label "nomina&#322;"
  ]
  node [
    id 317
    label "ok&#322;adka"
  ]
  node [
    id 318
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 319
    label "wydawnictwo"
  ]
  node [
    id 320
    label "ekslibris"
  ]
  node [
    id 321
    label "tekst"
  ]
  node [
    id 322
    label "przek&#322;adacz"
  ]
  node [
    id 323
    label "bibliofilstwo"
  ]
  node [
    id 324
    label "falc"
  ]
  node [
    id 325
    label "zw&#243;j"
  ]
  node [
    id 326
    label "zapomoga"
  ]
  node [
    id 327
    label "komplet"
  ]
  node [
    id 328
    label "ucze&#324;"
  ]
  node [
    id 329
    label "layette"
  ]
  node [
    id 330
    label "wyprawa"
  ]
  node [
    id 331
    label "niemowl&#281;"
  ]
  node [
    id 332
    label "wiano"
  ]
  node [
    id 333
    label "aid"
  ]
  node [
    id 334
    label "u&#322;atwia&#263;"
  ]
  node [
    id 335
    label "concur"
  ]
  node [
    id 336
    label "sprzyja&#263;"
  ]
  node [
    id 337
    label "skutkowa&#263;"
  ]
  node [
    id 338
    label "digest"
  ]
  node [
    id 339
    label "Warszawa"
  ]
  node [
    id 340
    label "powodowa&#263;"
  ]
  node [
    id 341
    label "back"
  ]
  node [
    id 342
    label "&#322;atwi&#263;"
  ]
  node [
    id 343
    label "ease"
  ]
  node [
    id 344
    label "czu&#263;"
  ]
  node [
    id 345
    label "stanowi&#263;"
  ]
  node [
    id 346
    label "chowa&#263;"
  ]
  node [
    id 347
    label "sprawdza&#263;_si&#281;"
  ]
  node [
    id 348
    label "poci&#261;ga&#263;"
  ]
  node [
    id 349
    label "przynosi&#263;"
  ]
  node [
    id 350
    label "i&#347;&#263;_w_parze"
  ]
  node [
    id 351
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 352
    label "organizowa&#263;"
  ]
  node [
    id 353
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 354
    label "czyni&#263;"
  ]
  node [
    id 355
    label "give"
  ]
  node [
    id 356
    label "stylizowa&#263;"
  ]
  node [
    id 357
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 358
    label "falowa&#263;"
  ]
  node [
    id 359
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 360
    label "peddle"
  ]
  node [
    id 361
    label "praca"
  ]
  node [
    id 362
    label "wydala&#263;"
  ]
  node [
    id 363
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 364
    label "tentegowa&#263;"
  ]
  node [
    id 365
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 366
    label "urz&#261;dza&#263;"
  ]
  node [
    id 367
    label "oszukiwa&#263;"
  ]
  node [
    id 368
    label "ukazywa&#263;"
  ]
  node [
    id 369
    label "przerabia&#263;"
  ]
  node [
    id 370
    label "act"
  ]
  node [
    id 371
    label "post&#281;powa&#263;"
  ]
  node [
    id 372
    label "cover"
  ]
  node [
    id 373
    label "warszawa"
  ]
  node [
    id 374
    label "Powi&#347;le"
  ]
  node [
    id 375
    label "Wawa"
  ]
  node [
    id 376
    label "syreni_gr&#243;d"
  ]
  node [
    id 377
    label "Wawer"
  ]
  node [
    id 378
    label "W&#322;ochy"
  ]
  node [
    id 379
    label "Ursyn&#243;w"
  ]
  node [
    id 380
    label "Bielany"
  ]
  node [
    id 381
    label "Weso&#322;a"
  ]
  node [
    id 382
    label "Bia&#322;o&#322;&#281;ka"
  ]
  node [
    id 383
    label "Targ&#243;wek"
  ]
  node [
    id 384
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 385
    label "Muran&#243;w"
  ]
  node [
    id 386
    label "Warsiawa"
  ]
  node [
    id 387
    label "Ursus"
  ]
  node [
    id 388
    label "Ochota"
  ]
  node [
    id 389
    label "Marymont"
  ]
  node [
    id 390
    label "Ujazd&#243;w"
  ]
  node [
    id 391
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 392
    label "Solec"
  ]
  node [
    id 393
    label "Bemowo"
  ]
  node [
    id 394
    label "Mokot&#243;w"
  ]
  node [
    id 395
    label "Wilan&#243;w"
  ]
  node [
    id 396
    label "warszawka"
  ]
  node [
    id 397
    label "varsaviana"
  ]
  node [
    id 398
    label "Wola"
  ]
  node [
    id 399
    label "Rembert&#243;w"
  ]
  node [
    id 400
    label "Praga"
  ]
  node [
    id 401
    label "&#379;oliborz"
  ]
  node [
    id 402
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 403
    label "motywowa&#263;"
  ]
  node [
    id 404
    label "insert"
  ]
  node [
    id 405
    label "obznajomi&#263;"
  ]
  node [
    id 406
    label "zawrze&#263;"
  ]
  node [
    id 407
    label "pozna&#263;"
  ]
  node [
    id 408
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 409
    label "poinformowa&#263;"
  ]
  node [
    id 410
    label "teach"
  ]
  node [
    id 411
    label "inform"
  ]
  node [
    id 412
    label "zakomunikowa&#263;"
  ]
  node [
    id 413
    label "spowodowa&#263;"
  ]
  node [
    id 414
    label "permit"
  ]
  node [
    id 415
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 416
    label "zrozumie&#263;"
  ]
  node [
    id 417
    label "feel"
  ]
  node [
    id 418
    label "topographic_point"
  ]
  node [
    id 419
    label "visualize"
  ]
  node [
    id 420
    label "przyswoi&#263;"
  ]
  node [
    id 421
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 422
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 423
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 424
    label "experience"
  ]
  node [
    id 425
    label "straci&#263;_g&#322;ow&#281;"
  ]
  node [
    id 426
    label "sta&#263;_si&#281;"
  ]
  node [
    id 427
    label "raptowny"
  ]
  node [
    id 428
    label "incorporate"
  ]
  node [
    id 429
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 430
    label "boil"
  ]
  node [
    id 431
    label "uk&#322;ad"
  ]
  node [
    id 432
    label "ugotowa&#263;_si&#281;"
  ]
  node [
    id 433
    label "zamkn&#261;&#263;"
  ]
  node [
    id 434
    label "spot&#281;gowa&#263;_si&#281;"
  ]
  node [
    id 435
    label "ustali&#263;"
  ]
  node [
    id 436
    label "admit"
  ]
  node [
    id 437
    label "wezbra&#263;"
  ]
  node [
    id 438
    label "embrace"
  ]
  node [
    id 439
    label "schematycznie"
  ]
  node [
    id 440
    label "zwyczajny"
  ]
  node [
    id 441
    label "typowy"
  ]
  node [
    id 442
    label "standardowo"
  ]
  node [
    id 443
    label "standartowy"
  ]
  node [
    id 444
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 445
    label "typowo"
  ]
  node [
    id 446
    label "cz&#281;sty"
  ]
  node [
    id 447
    label "zwyk&#322;y"
  ]
  node [
    id 448
    label "przeci&#281;tny"
  ]
  node [
    id 449
    label "oswojony"
  ]
  node [
    id 450
    label "zwyczajnie"
  ]
  node [
    id 451
    label "zwykle"
  ]
  node [
    id 452
    label "na&#322;o&#380;ny"
  ]
  node [
    id 453
    label "okre&#347;lony"
  ]
  node [
    id 454
    label "schematyczny"
  ]
  node [
    id 455
    label "metodycznie"
  ]
  node [
    id 456
    label "typ"
  ]
  node [
    id 457
    label "charakterystyka"
  ]
  node [
    id 458
    label "cz&#322;owiek"
  ]
  node [
    id 459
    label "zaistnie&#263;"
  ]
  node [
    id 460
    label "Osjan"
  ]
  node [
    id 461
    label "kto&#347;"
  ]
  node [
    id 462
    label "wygl&#261;d"
  ]
  node [
    id 463
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 464
    label "osobowo&#347;&#263;"
  ]
  node [
    id 465
    label "trim"
  ]
  node [
    id 466
    label "poby&#263;"
  ]
  node [
    id 467
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 468
    label "Aspazja"
  ]
  node [
    id 469
    label "punkt_widzenia"
  ]
  node [
    id 470
    label "kompleksja"
  ]
  node [
    id 471
    label "wytrzyma&#263;"
  ]
  node [
    id 472
    label "budowa"
  ]
  node [
    id 473
    label "formacja"
  ]
  node [
    id 474
    label "pozosta&#263;"
  ]
  node [
    id 475
    label "przedstawienie"
  ]
  node [
    id 476
    label "go&#347;&#263;"
  ]
  node [
    id 477
    label "facet"
  ]
  node [
    id 478
    label "jednostka_systematyczna"
  ]
  node [
    id 479
    label "kr&#243;lestwo"
  ]
  node [
    id 480
    label "autorament"
  ]
  node [
    id 481
    label "variety"
  ]
  node [
    id 482
    label "antycypacja"
  ]
  node [
    id 483
    label "przypuszczenie"
  ]
  node [
    id 484
    label "cynk"
  ]
  node [
    id 485
    label "obstawia&#263;"
  ]
  node [
    id 486
    label "gromada"
  ]
  node [
    id 487
    label "sztuka"
  ]
  node [
    id 488
    label "design"
  ]
  node [
    id 489
    label "reengineering"
  ]
  node [
    id 490
    label "program"
  ]
  node [
    id 491
    label "zbi&#243;r"
  ]
  node [
    id 492
    label "series"
  ]
  node [
    id 493
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 494
    label "uprawianie"
  ]
  node [
    id 495
    label "praca_rolnicza"
  ]
  node [
    id 496
    label "collection"
  ]
  node [
    id 497
    label "dane"
  ]
  node [
    id 498
    label "pakiet_klimatyczny"
  ]
  node [
    id 499
    label "poj&#281;cie"
  ]
  node [
    id 500
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 501
    label "sum"
  ]
  node [
    id 502
    label "gathering"
  ]
  node [
    id 503
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 504
    label "album"
  ]
  node [
    id 505
    label "instalowa&#263;"
  ]
  node [
    id 506
    label "odinstalowywa&#263;"
  ]
  node [
    id 507
    label "spis"
  ]
  node [
    id 508
    label "zaprezentowanie"
  ]
  node [
    id 509
    label "podprogram"
  ]
  node [
    id 510
    label "ogranicznik_referencyjny"
  ]
  node [
    id 511
    label "course_of_study"
  ]
  node [
    id 512
    label "booklet"
  ]
  node [
    id 513
    label "dzia&#322;"
  ]
  node [
    id 514
    label "odinstalowanie"
  ]
  node [
    id 515
    label "broszura"
  ]
  node [
    id 516
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 517
    label "kana&#322;"
  ]
  node [
    id 518
    label "teleferie"
  ]
  node [
    id 519
    label "zainstalowanie"
  ]
  node [
    id 520
    label "struktura_organizacyjna"
  ]
  node [
    id 521
    label "pirat"
  ]
  node [
    id 522
    label "zaprezentowa&#263;"
  ]
  node [
    id 523
    label "prezentowanie"
  ]
  node [
    id 524
    label "prezentowa&#263;"
  ]
  node [
    id 525
    label "interfejs"
  ]
  node [
    id 526
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 527
    label "okno"
  ]
  node [
    id 528
    label "blok"
  ]
  node [
    id 529
    label "folder"
  ]
  node [
    id 530
    label "zainstalowa&#263;"
  ]
  node [
    id 531
    label "za&#322;o&#380;enie"
  ]
  node [
    id 532
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 533
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 534
    label "ram&#243;wka"
  ]
  node [
    id 535
    label "emitowa&#263;"
  ]
  node [
    id 536
    label "emitowanie"
  ]
  node [
    id 537
    label "odinstalowywanie"
  ]
  node [
    id 538
    label "instrukcja"
  ]
  node [
    id 539
    label "informatyka"
  ]
  node [
    id 540
    label "deklaracja"
  ]
  node [
    id 541
    label "sekcja_krytyczna"
  ]
  node [
    id 542
    label "menu"
  ]
  node [
    id 543
    label "furkacja"
  ]
  node [
    id 544
    label "podstawa"
  ]
  node [
    id 545
    label "instalowanie"
  ]
  node [
    id 546
    label "oferta"
  ]
  node [
    id 547
    label "odinstalowa&#263;"
  ]
  node [
    id 548
    label "przer&#243;bka"
  ]
  node [
    id 549
    label "odmienienie"
  ]
  node [
    id 550
    label "strategia"
  ]
  node [
    id 551
    label "zmienia&#263;"
  ]
  node [
    id 552
    label "dodatek"
  ]
  node [
    id 553
    label "struktura"
  ]
  node [
    id 554
    label "oprawa"
  ]
  node [
    id 555
    label "stela&#380;"
  ]
  node [
    id 556
    label "zakres"
  ]
  node [
    id 557
    label "human_body"
  ]
  node [
    id 558
    label "pojazd"
  ]
  node [
    id 559
    label "paczka"
  ]
  node [
    id 560
    label "obramowanie"
  ]
  node [
    id 561
    label "postawa"
  ]
  node [
    id 562
    label "element_konstrukcyjny"
  ]
  node [
    id 563
    label "szablon"
  ]
  node [
    id 564
    label "dochodzenie"
  ]
  node [
    id 565
    label "doch&#243;d"
  ]
  node [
    id 566
    label "dziennik"
  ]
  node [
    id 567
    label "element"
  ]
  node [
    id 568
    label "rzecz"
  ]
  node [
    id 569
    label "galanteria"
  ]
  node [
    id 570
    label "doj&#347;cie"
  ]
  node [
    id 571
    label "aneks"
  ]
  node [
    id 572
    label "doj&#347;&#263;"
  ]
  node [
    id 573
    label "prevention"
  ]
  node [
    id 574
    label "otoczenie"
  ]
  node [
    id 575
    label "framing"
  ]
  node [
    id 576
    label "boarding"
  ]
  node [
    id 577
    label "binda"
  ]
  node [
    id 578
    label "warunki"
  ]
  node [
    id 579
    label "filet"
  ]
  node [
    id 580
    label "Rzym_Zachodni"
  ]
  node [
    id 581
    label "whole"
  ]
  node [
    id 582
    label "Rzym_Wschodni"
  ]
  node [
    id 583
    label "urz&#261;dzenie"
  ]
  node [
    id 584
    label "sfera"
  ]
  node [
    id 585
    label "granica"
  ]
  node [
    id 586
    label "wielko&#347;&#263;"
  ]
  node [
    id 587
    label "podzakres"
  ]
  node [
    id 588
    label "dziedzina"
  ]
  node [
    id 589
    label "desygnat"
  ]
  node [
    id 590
    label "circle"
  ]
  node [
    id 591
    label "konstrukcja"
  ]
  node [
    id 592
    label "towarzystwo"
  ]
  node [
    id 593
    label "granda"
  ]
  node [
    id 594
    label "pakunek"
  ]
  node [
    id 595
    label "poczta"
  ]
  node [
    id 596
    label "pakiet"
  ]
  node [
    id 597
    label "baletnica"
  ]
  node [
    id 598
    label "tract"
  ]
  node [
    id 599
    label "przesy&#322;ka"
  ]
  node [
    id 600
    label "opakowanie"
  ]
  node [
    id 601
    label "podwini&#281;cie"
  ]
  node [
    id 602
    label "zap&#322;acenie"
  ]
  node [
    id 603
    label "przyodzianie"
  ]
  node [
    id 604
    label "budowla"
  ]
  node [
    id 605
    label "pokrycie"
  ]
  node [
    id 606
    label "rozebranie"
  ]
  node [
    id 607
    label "poubieranie"
  ]
  node [
    id 608
    label "infliction"
  ]
  node [
    id 609
    label "pozak&#322;adanie"
  ]
  node [
    id 610
    label "przebranie"
  ]
  node [
    id 611
    label "przywdzianie"
  ]
  node [
    id 612
    label "obleczenie_si&#281;"
  ]
  node [
    id 613
    label "utworzenie"
  ]
  node [
    id 614
    label "twierdzenie"
  ]
  node [
    id 615
    label "obleczenie"
  ]
  node [
    id 616
    label "umieszczenie"
  ]
  node [
    id 617
    label "przygotowywanie"
  ]
  node [
    id 618
    label "przymierzenie"
  ]
  node [
    id 619
    label "przygotowanie"
  ]
  node [
    id 620
    label "proposition"
  ]
  node [
    id 621
    label "przewidzenie"
  ]
  node [
    id 622
    label "mechanika"
  ]
  node [
    id 623
    label "o&#347;"
  ]
  node [
    id 624
    label "usenet"
  ]
  node [
    id 625
    label "rozprz&#261;c"
  ]
  node [
    id 626
    label "zachowanie"
  ]
  node [
    id 627
    label "cybernetyk"
  ]
  node [
    id 628
    label "podsystem"
  ]
  node [
    id 629
    label "system"
  ]
  node [
    id 630
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 631
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 632
    label "sk&#322;ad"
  ]
  node [
    id 633
    label "systemat"
  ]
  node [
    id 634
    label "konstelacja"
  ]
  node [
    id 635
    label "nastawienie"
  ]
  node [
    id 636
    label "attitude"
  ]
  node [
    id 637
    label "model"
  ]
  node [
    id 638
    label "mildew"
  ]
  node [
    id 639
    label "jig"
  ]
  node [
    id 640
    label "drabina_analgetyczna"
  ]
  node [
    id 641
    label "wz&#243;r"
  ]
  node [
    id 642
    label "C"
  ]
  node [
    id 643
    label "D"
  ]
  node [
    id 644
    label "exemplar"
  ]
  node [
    id 645
    label "odholowa&#263;"
  ]
  node [
    id 646
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 647
    label "tabor"
  ]
  node [
    id 648
    label "przyholowywanie"
  ]
  node [
    id 649
    label "przyholowa&#263;"
  ]
  node [
    id 650
    label "przyholowanie"
  ]
  node [
    id 651
    label "fukni&#281;cie"
  ]
  node [
    id 652
    label "l&#261;d"
  ]
  node [
    id 653
    label "zielona_karta"
  ]
  node [
    id 654
    label "fukanie"
  ]
  node [
    id 655
    label "przyholowywa&#263;"
  ]
  node [
    id 656
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 657
    label "woda"
  ]
  node [
    id 658
    label "przeszklenie"
  ]
  node [
    id 659
    label "test_zderzeniowy"
  ]
  node [
    id 660
    label "powietrze"
  ]
  node [
    id 661
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 662
    label "odzywka"
  ]
  node [
    id 663
    label "nadwozie"
  ]
  node [
    id 664
    label "odholowanie"
  ]
  node [
    id 665
    label "prowadzenie_si&#281;"
  ]
  node [
    id 666
    label "odholowywa&#263;"
  ]
  node [
    id 667
    label "pod&#322;oga"
  ]
  node [
    id 668
    label "odholowywanie"
  ]
  node [
    id 669
    label "hamulec"
  ]
  node [
    id 670
    label "podwozie"
  ]
  node [
    id 671
    label "intencja"
  ]
  node [
    id 672
    label "device"
  ]
  node [
    id 673
    label "program_u&#380;ytkowy"
  ]
  node [
    id 674
    label "pomys&#322;"
  ]
  node [
    id 675
    label "dokumentacja"
  ]
  node [
    id 676
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 677
    label "agreement"
  ]
  node [
    id 678
    label "dokument"
  ]
  node [
    id 679
    label "thinking"
  ]
  node [
    id 680
    label "zapis"
  ]
  node [
    id 681
    label "&#347;wiadectwo"
  ]
  node [
    id 682
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 683
    label "parafa"
  ]
  node [
    id 684
    label "raport&#243;wka"
  ]
  node [
    id 685
    label "utw&#243;r"
  ]
  node [
    id 686
    label "record"
  ]
  node [
    id 687
    label "fascyku&#322;"
  ]
  node [
    id 688
    label "registratura"
  ]
  node [
    id 689
    label "artyku&#322;"
  ]
  node [
    id 690
    label "writing"
  ]
  node [
    id 691
    label "sygnatariusz"
  ]
  node [
    id 692
    label "rysunek"
  ]
  node [
    id 693
    label "obraz"
  ]
  node [
    id 694
    label "reprezentacja"
  ]
  node [
    id 695
    label "dekoracja"
  ]
  node [
    id 696
    label "perspektywa"
  ]
  node [
    id 697
    label "ekscerpcja"
  ]
  node [
    id 698
    label "materia&#322;"
  ]
  node [
    id 699
    label "operat"
  ]
  node [
    id 700
    label "kosztorys"
  ]
  node [
    id 701
    label "pocz&#261;tki"
  ]
  node [
    id 702
    label "ukra&#347;&#263;"
  ]
  node [
    id 703
    label "ukradzenie"
  ]
  node [
    id 704
    label "idea"
  ]
  node [
    id 705
    label "czas_wolny"
  ]
  node [
    id 706
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 707
    label "byt"
  ]
  node [
    id 708
    label "organizacja"
  ]
  node [
    id 709
    label "prawo"
  ]
  node [
    id 710
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 711
    label "nauka_prawa"
  ]
  node [
    id 712
    label "kszta&#322;t"
  ]
  node [
    id 713
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 714
    label "armia"
  ]
  node [
    id 715
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 716
    label "poprowadzi&#263;"
  ]
  node [
    id 717
    label "cord"
  ]
  node [
    id 718
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 719
    label "trasa"
  ]
  node [
    id 720
    label "po&#322;&#261;czenie"
  ]
  node [
    id 721
    label "materia&#322;_zecerski"
  ]
  node [
    id 722
    label "przeorientowywanie"
  ]
  node [
    id 723
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 724
    label "curve"
  ]
  node [
    id 725
    label "figura_geometryczna"
  ]
  node [
    id 726
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 727
    label "jard"
  ]
  node [
    id 728
    label "szczep"
  ]
  node [
    id 729
    label "phreaker"
  ]
  node [
    id 730
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 731
    label "grupa_organizm&#243;w"
  ]
  node [
    id 732
    label "prowadzi&#263;"
  ]
  node [
    id 733
    label "przeorientowywa&#263;"
  ]
  node [
    id 734
    label "access"
  ]
  node [
    id 735
    label "przeorientowanie"
  ]
  node [
    id 736
    label "przeorientowa&#263;"
  ]
  node [
    id 737
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 738
    label "billing"
  ]
  node [
    id 739
    label "szpaler"
  ]
  node [
    id 740
    label "sztrych"
  ]
  node [
    id 741
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 742
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 743
    label "drzewo_genealogiczne"
  ]
  node [
    id 744
    label "transporter"
  ]
  node [
    id 745
    label "line"
  ]
  node [
    id 746
    label "przew&#243;d"
  ]
  node [
    id 747
    label "granice"
  ]
  node [
    id 748
    label "kontakt"
  ]
  node [
    id 749
    label "rz&#261;d"
  ]
  node [
    id 750
    label "przewo&#378;nik"
  ]
  node [
    id 751
    label "przystanek"
  ]
  node [
    id 752
    label "linijka"
  ]
  node [
    id 753
    label "uporz&#261;dkowanie"
  ]
  node [
    id 754
    label "coalescence"
  ]
  node [
    id 755
    label "Ural"
  ]
  node [
    id 756
    label "bearing"
  ]
  node [
    id 757
    label "prowadzenie"
  ]
  node [
    id 758
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 759
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 760
    label "koniec"
  ]
  node [
    id 761
    label "podkatalog"
  ]
  node [
    id 762
    label "nadpisa&#263;"
  ]
  node [
    id 763
    label "nadpisanie"
  ]
  node [
    id 764
    label "bundle"
  ]
  node [
    id 765
    label "nadpisywanie"
  ]
  node [
    id 766
    label "nadpisywa&#263;"
  ]
  node [
    id 767
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 768
    label "rozmiar"
  ]
  node [
    id 769
    label "obszar"
  ]
  node [
    id 770
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 771
    label "zwierciad&#322;o"
  ]
  node [
    id 772
    label "capacity"
  ]
  node [
    id 773
    label "plane"
  ]
  node [
    id 774
    label "temat"
  ]
  node [
    id 775
    label "poznanie"
  ]
  node [
    id 776
    label "leksem"
  ]
  node [
    id 777
    label "dzie&#322;o"
  ]
  node [
    id 778
    label "blaszka"
  ]
  node [
    id 779
    label "kantyzm"
  ]
  node [
    id 780
    label "zdolno&#347;&#263;"
  ]
  node [
    id 781
    label "do&#322;ek"
  ]
  node [
    id 782
    label "zawarto&#347;&#263;"
  ]
  node [
    id 783
    label "gwiazda"
  ]
  node [
    id 784
    label "formality"
  ]
  node [
    id 785
    label "mode"
  ]
  node [
    id 786
    label "morfem"
  ]
  node [
    id 787
    label "rdze&#324;"
  ]
  node [
    id 788
    label "kielich"
  ]
  node [
    id 789
    label "ornamentyka"
  ]
  node [
    id 790
    label "pasmo"
  ]
  node [
    id 791
    label "zwyczaj"
  ]
  node [
    id 792
    label "g&#322;owa"
  ]
  node [
    id 793
    label "naczynie"
  ]
  node [
    id 794
    label "p&#322;at"
  ]
  node [
    id 795
    label "maszyna_drukarska"
  ]
  node [
    id 796
    label "style"
  ]
  node [
    id 797
    label "linearno&#347;&#263;"
  ]
  node [
    id 798
    label "wyra&#380;enie"
  ]
  node [
    id 799
    label "spirala"
  ]
  node [
    id 800
    label "dyspozycja"
  ]
  node [
    id 801
    label "odmiana"
  ]
  node [
    id 802
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 803
    label "October"
  ]
  node [
    id 804
    label "creation"
  ]
  node [
    id 805
    label "p&#281;tla"
  ]
  node [
    id 806
    label "arystotelizm"
  ]
  node [
    id 807
    label "miniatura"
  ]
  node [
    id 808
    label "zesp&#243;&#322;"
  ]
  node [
    id 809
    label "podejrzany"
  ]
  node [
    id 810
    label "s&#261;downictwo"
  ]
  node [
    id 811
    label "biuro"
  ]
  node [
    id 812
    label "court"
  ]
  node [
    id 813
    label "forum"
  ]
  node [
    id 814
    label "bronienie"
  ]
  node [
    id 815
    label "urz&#261;d"
  ]
  node [
    id 816
    label "wydarzenie"
  ]
  node [
    id 817
    label "oskar&#380;yciel"
  ]
  node [
    id 818
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 819
    label "skazany"
  ]
  node [
    id 820
    label "post&#281;powanie"
  ]
  node [
    id 821
    label "broni&#263;"
  ]
  node [
    id 822
    label "my&#347;l"
  ]
  node [
    id 823
    label "pods&#261;dny"
  ]
  node [
    id 824
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 825
    label "wypowied&#378;"
  ]
  node [
    id 826
    label "antylogizm"
  ]
  node [
    id 827
    label "konektyw"
  ]
  node [
    id 828
    label "&#347;wiadek"
  ]
  node [
    id 829
    label "procesowicz"
  ]
  node [
    id 830
    label "pochwytanie"
  ]
  node [
    id 831
    label "wording"
  ]
  node [
    id 832
    label "wzbudzenie"
  ]
  node [
    id 833
    label "withdrawal"
  ]
  node [
    id 834
    label "capture"
  ]
  node [
    id 835
    label "podniesienie"
  ]
  node [
    id 836
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 837
    label "film"
  ]
  node [
    id 838
    label "scena"
  ]
  node [
    id 839
    label "zapisanie"
  ]
  node [
    id 840
    label "prezentacja"
  ]
  node [
    id 841
    label "rzucenie"
  ]
  node [
    id 842
    label "zamkni&#281;cie"
  ]
  node [
    id 843
    label "zabranie"
  ]
  node [
    id 844
    label "zaaresztowanie"
  ]
  node [
    id 845
    label "wzi&#281;cie"
  ]
  node [
    id 846
    label "kierunek"
  ]
  node [
    id 847
    label "wyznaczenie"
  ]
  node [
    id 848
    label "przyczynienie_si&#281;"
  ]
  node [
    id 849
    label "zwr&#243;cenie"
  ]
  node [
    id 850
    label "zrozumienie"
  ]
  node [
    id 851
    label "tu&#322;&#243;w"
  ]
  node [
    id 852
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 853
    label "wielok&#261;t"
  ]
  node [
    id 854
    label "odcinek"
  ]
  node [
    id 855
    label "strzelba"
  ]
  node [
    id 856
    label "lufa"
  ]
  node [
    id 857
    label "&#347;ciana"
  ]
  node [
    id 858
    label "set"
  ]
  node [
    id 859
    label "orient"
  ]
  node [
    id 860
    label "eastern_hemisphere"
  ]
  node [
    id 861
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 862
    label "aim"
  ]
  node [
    id 863
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 864
    label "wyznaczy&#263;"
  ]
  node [
    id 865
    label "wrench"
  ]
  node [
    id 866
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 867
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 868
    label "sple&#347;&#263;"
  ]
  node [
    id 869
    label "os&#322;abi&#263;"
  ]
  node [
    id 870
    label "nawin&#261;&#263;"
  ]
  node [
    id 871
    label "scali&#263;"
  ]
  node [
    id 872
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 873
    label "twist"
  ]
  node [
    id 874
    label "splay"
  ]
  node [
    id 875
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 876
    label "uszkodzi&#263;"
  ]
  node [
    id 877
    label "break"
  ]
  node [
    id 878
    label "flex"
  ]
  node [
    id 879
    label "os&#322;abia&#263;"
  ]
  node [
    id 880
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 881
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 882
    label "splata&#263;"
  ]
  node [
    id 883
    label "throw"
  ]
  node [
    id 884
    label "screw"
  ]
  node [
    id 885
    label "scala&#263;"
  ]
  node [
    id 886
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 887
    label "przelezienie"
  ]
  node [
    id 888
    label "&#347;piew"
  ]
  node [
    id 889
    label "Synaj"
  ]
  node [
    id 890
    label "Kreml"
  ]
  node [
    id 891
    label "wysoki"
  ]
  node [
    id 892
    label "wzniesienie"
  ]
  node [
    id 893
    label "grupa"
  ]
  node [
    id 894
    label "pi&#281;tro"
  ]
  node [
    id 895
    label "Ropa"
  ]
  node [
    id 896
    label "kupa"
  ]
  node [
    id 897
    label "przele&#378;&#263;"
  ]
  node [
    id 898
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 899
    label "karczek"
  ]
  node [
    id 900
    label "rami&#261;czko"
  ]
  node [
    id 901
    label "Jaworze"
  ]
  node [
    id 902
    label "odchylanie_si&#281;"
  ]
  node [
    id 903
    label "kszta&#322;towanie"
  ]
  node [
    id 904
    label "os&#322;abianie"
  ]
  node [
    id 905
    label "uprz&#281;dzenie"
  ]
  node [
    id 906
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 907
    label "scalanie"
  ]
  node [
    id 908
    label "prawoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 909
    label "snucie"
  ]
  node [
    id 910
    label "lewoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 911
    label "tortuosity"
  ]
  node [
    id 912
    label "odbijanie"
  ]
  node [
    id 913
    label "contortion"
  ]
  node [
    id 914
    label "splatanie"
  ]
  node [
    id 915
    label "turn"
  ]
  node [
    id 916
    label "nawini&#281;cie"
  ]
  node [
    id 917
    label "os&#322;abienie"
  ]
  node [
    id 918
    label "uszkodzenie"
  ]
  node [
    id 919
    label "poskr&#281;canie"
  ]
  node [
    id 920
    label "uraz"
  ]
  node [
    id 921
    label "odchylenie_si&#281;"
  ]
  node [
    id 922
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 923
    label "z&#322;&#261;czenie"
  ]
  node [
    id 924
    label "splecenie"
  ]
  node [
    id 925
    label "turning"
  ]
  node [
    id 926
    label "kierowa&#263;"
  ]
  node [
    id 927
    label "marshal"
  ]
  node [
    id 928
    label "wyznacza&#263;"
  ]
  node [
    id 929
    label "pomaganie"
  ]
  node [
    id 930
    label "orientation"
  ]
  node [
    id 931
    label "przyczynianie_si&#281;"
  ]
  node [
    id 932
    label "zwracanie"
  ]
  node [
    id 933
    label "rozeznawanie"
  ]
  node [
    id 934
    label "oznaczanie"
  ]
  node [
    id 935
    label "cia&#322;o"
  ]
  node [
    id 936
    label "seksualno&#347;&#263;"
  ]
  node [
    id 937
    label "wiedza"
  ]
  node [
    id 938
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 939
    label "zorientowanie_si&#281;"
  ]
  node [
    id 940
    label "pogubienie_si&#281;"
  ]
  node [
    id 941
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 942
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 943
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 944
    label "gubienie_si&#281;"
  ]
  node [
    id 945
    label "zaty&#322;"
  ]
  node [
    id 946
    label "pupa"
  ]
  node [
    id 947
    label "figura"
  ]
  node [
    id 948
    label "graficzny_interfejs_u&#380;ytkownika"
  ]
  node [
    id 949
    label "uk&#322;ad_graficzny"
  ]
  node [
    id 950
    label "uwierzytelnienie"
  ]
  node [
    id 951
    label "liczba"
  ]
  node [
    id 952
    label "circumference"
  ]
  node [
    id 953
    label "cyrkumferencja"
  ]
  node [
    id 954
    label "provider"
  ]
  node [
    id 955
    label "hipertekst"
  ]
  node [
    id 956
    label "cyberprzestrze&#324;"
  ]
  node [
    id 957
    label "mem"
  ]
  node [
    id 958
    label "grooming"
  ]
  node [
    id 959
    label "gra_sieciowa"
  ]
  node [
    id 960
    label "media"
  ]
  node [
    id 961
    label "biznes_elektroniczny"
  ]
  node [
    id 962
    label "sie&#263;_komputerowa"
  ]
  node [
    id 963
    label "punkt_dost&#281;pu"
  ]
  node [
    id 964
    label "us&#322;uga_internetowa"
  ]
  node [
    id 965
    label "netbook"
  ]
  node [
    id 966
    label "e-hazard"
  ]
  node [
    id 967
    label "podcast"
  ]
  node [
    id 968
    label "co&#347;"
  ]
  node [
    id 969
    label "budynek"
  ]
  node [
    id 970
    label "thing"
  ]
  node [
    id 971
    label "faul"
  ]
  node [
    id 972
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 973
    label "s&#281;dzia"
  ]
  node [
    id 974
    label "bon"
  ]
  node [
    id 975
    label "ticket"
  ]
  node [
    id 976
    label "arkusz"
  ]
  node [
    id 977
    label "kartonik"
  ]
  node [
    id 978
    label "kara"
  ]
  node [
    id 979
    label "pagination"
  ]
  node [
    id 980
    label "numer"
  ]
  node [
    id 981
    label "wskaz&#243;wka"
  ]
  node [
    id 982
    label "fakt"
  ]
  node [
    id 983
    label "tarcza"
  ]
  node [
    id 984
    label "zegar"
  ]
  node [
    id 985
    label "solucja"
  ]
  node [
    id 986
    label "informacja"
  ]
  node [
    id 987
    label "implikowa&#263;"
  ]
  node [
    id 988
    label "zapoznawa&#263;"
  ]
  node [
    id 989
    label "represent"
  ]
  node [
    id 990
    label "zawiera&#263;"
  ]
  node [
    id 991
    label "poznawa&#263;"
  ]
  node [
    id 992
    label "obznajamia&#263;"
  ]
  node [
    id 993
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 994
    label "go_steady"
  ]
  node [
    id 995
    label "informowa&#263;"
  ]
  node [
    id 996
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 997
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 998
    label "regu&#322;a_Allena"
  ]
  node [
    id 999
    label "base"
  ]
  node [
    id 1000
    label "obserwacja"
  ]
  node [
    id 1001
    label "zasada_d'Alemberta"
  ]
  node [
    id 1002
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1003
    label "normalizacja"
  ]
  node [
    id 1004
    label "moralno&#347;&#263;"
  ]
  node [
    id 1005
    label "criterion"
  ]
  node [
    id 1006
    label "opis"
  ]
  node [
    id 1007
    label "regu&#322;a_Glogera"
  ]
  node [
    id 1008
    label "prawo_Mendla"
  ]
  node [
    id 1009
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 1010
    label "standard"
  ]
  node [
    id 1011
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 1012
    label "qualification"
  ]
  node [
    id 1013
    label "dominion"
  ]
  node [
    id 1014
    label "occupation"
  ]
  node [
    id 1015
    label "substancja"
  ]
  node [
    id 1016
    label "prawid&#322;o"
  ]
  node [
    id 1017
    label "dobro&#263;"
  ]
  node [
    id 1018
    label "aretologia"
  ]
  node [
    id 1019
    label "morality"
  ]
  node [
    id 1020
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 1021
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1022
    label "honesty"
  ]
  node [
    id 1023
    label "ordinariness"
  ]
  node [
    id 1024
    label "zorganizowa&#263;"
  ]
  node [
    id 1025
    label "taniec_towarzyski"
  ]
  node [
    id 1026
    label "organizowanie"
  ]
  node [
    id 1027
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 1028
    label "zorganizowanie"
  ]
  node [
    id 1029
    label "exposition"
  ]
  node [
    id 1030
    label "obja&#347;nienie"
  ]
  node [
    id 1031
    label "zawarcie"
  ]
  node [
    id 1032
    label "warunek"
  ]
  node [
    id 1033
    label "gestia_transportowa"
  ]
  node [
    id 1034
    label "contract"
  ]
  node [
    id 1035
    label "porozumienie"
  ]
  node [
    id 1036
    label "klauzula"
  ]
  node [
    id 1037
    label "przenikanie"
  ]
  node [
    id 1038
    label "materia"
  ]
  node [
    id 1039
    label "cz&#261;steczka"
  ]
  node [
    id 1040
    label "temperatura_krytyczna"
  ]
  node [
    id 1041
    label "przenika&#263;"
  ]
  node [
    id 1042
    label "smolisty"
  ]
  node [
    id 1043
    label "narz&#281;dzie"
  ]
  node [
    id 1044
    label "nature"
  ]
  node [
    id 1045
    label "pot&#281;ga"
  ]
  node [
    id 1046
    label "documentation"
  ]
  node [
    id 1047
    label "column"
  ]
  node [
    id 1048
    label "zasadzi&#263;"
  ]
  node [
    id 1049
    label "punkt_odniesienia"
  ]
  node [
    id 1050
    label "zasadzenie"
  ]
  node [
    id 1051
    label "d&#243;&#322;"
  ]
  node [
    id 1052
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1053
    label "background"
  ]
  node [
    id 1054
    label "podstawowy"
  ]
  node [
    id 1055
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1056
    label "shoetree"
  ]
  node [
    id 1057
    label "badanie"
  ]
  node [
    id 1058
    label "proces_my&#347;lowy"
  ]
  node [
    id 1059
    label "remark"
  ]
  node [
    id 1060
    label "metoda"
  ]
  node [
    id 1061
    label "stwierdzenie"
  ]
  node [
    id 1062
    label "observation"
  ]
  node [
    id 1063
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 1064
    label "alternatywa_Fredholma"
  ]
  node [
    id 1065
    label "oznajmianie"
  ]
  node [
    id 1066
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 1067
    label "teoria"
  ]
  node [
    id 1068
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 1069
    label "paradoks_Leontiefa"
  ]
  node [
    id 1070
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 1071
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 1072
    label "teza"
  ]
  node [
    id 1073
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 1074
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 1075
    label "twierdzenie_Pettisa"
  ]
  node [
    id 1076
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 1077
    label "twierdzenie_Maya"
  ]
  node [
    id 1078
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 1079
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 1080
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 1081
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 1082
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 1083
    label "zapewnianie"
  ]
  node [
    id 1084
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 1085
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 1086
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 1087
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 1088
    label "twierdzenie_Stokesa"
  ]
  node [
    id 1089
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 1090
    label "twierdzenie_Cevy"
  ]
  node [
    id 1091
    label "twierdzenie_Pascala"
  ]
  node [
    id 1092
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 1093
    label "komunikowanie"
  ]
  node [
    id 1094
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 1095
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 1096
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 1097
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 1098
    label "relacja"
  ]
  node [
    id 1099
    label "calibration"
  ]
  node [
    id 1100
    label "operacja"
  ]
  node [
    id 1101
    label "proces"
  ]
  node [
    id 1102
    label "porz&#261;dek"
  ]
  node [
    id 1103
    label "dominance"
  ]
  node [
    id 1104
    label "zabieg"
  ]
  node [
    id 1105
    label "standardization"
  ]
  node [
    id 1106
    label "zmiana"
  ]
  node [
    id 1107
    label "umocowa&#263;"
  ]
  node [
    id 1108
    label "procesualistyka"
  ]
  node [
    id 1109
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1110
    label "kryminalistyka"
  ]
  node [
    id 1111
    label "szko&#322;a"
  ]
  node [
    id 1112
    label "normatywizm"
  ]
  node [
    id 1113
    label "jurisprudence"
  ]
  node [
    id 1114
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 1115
    label "kultura_duchowa"
  ]
  node [
    id 1116
    label "przepis"
  ]
  node [
    id 1117
    label "prawo_karne_procesowe"
  ]
  node [
    id 1118
    label "kazuistyka"
  ]
  node [
    id 1119
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 1120
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 1121
    label "kryminologia"
  ]
  node [
    id 1122
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 1123
    label "prawo_karne"
  ]
  node [
    id 1124
    label "legislacyjnie"
  ]
  node [
    id 1125
    label "cywilistyka"
  ]
  node [
    id 1126
    label "judykatura"
  ]
  node [
    id 1127
    label "kanonistyka"
  ]
  node [
    id 1128
    label "law"
  ]
  node [
    id 1129
    label "wykonawczy"
  ]
  node [
    id 1130
    label "&#347;rodek"
  ]
  node [
    id 1131
    label "niezb&#281;dnik"
  ]
  node [
    id 1132
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 1133
    label "tylec"
  ]
  node [
    id 1134
    label "ko&#322;o"
  ]
  node [
    id 1135
    label "modalno&#347;&#263;"
  ]
  node [
    id 1136
    label "z&#261;b"
  ]
  node [
    id 1137
    label "kategoria_gramatyczna"
  ]
  node [
    id 1138
    label "skala"
  ]
  node [
    id 1139
    label "funkcjonowa&#263;"
  ]
  node [
    id 1140
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 1141
    label "koniugacja"
  ]
  node [
    id 1142
    label "prezenter"
  ]
  node [
    id 1143
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1144
    label "motif"
  ]
  node [
    id 1145
    label "pozowanie"
  ]
  node [
    id 1146
    label "ideal"
  ]
  node [
    id 1147
    label "matryca"
  ]
  node [
    id 1148
    label "adaptation"
  ]
  node [
    id 1149
    label "pozowa&#263;"
  ]
  node [
    id 1150
    label "imitacja"
  ]
  node [
    id 1151
    label "orygina&#322;"
  ]
  node [
    id 1152
    label "edit"
  ]
  node [
    id 1153
    label "give_voice"
  ]
  node [
    id 1154
    label "poprawia&#263;"
  ]
  node [
    id 1155
    label "opracowywa&#263;"
  ]
  node [
    id 1156
    label "przygotowywa&#263;"
  ]
  node [
    id 1157
    label "sterowa&#263;"
  ]
  node [
    id 1158
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1159
    label "manipulate"
  ]
  node [
    id 1160
    label "zwierzchnik"
  ]
  node [
    id 1161
    label "ustawia&#263;"
  ]
  node [
    id 1162
    label "przeznacza&#263;"
  ]
  node [
    id 1163
    label "control"
  ]
  node [
    id 1164
    label "match"
  ]
  node [
    id 1165
    label "administrowa&#263;"
  ]
  node [
    id 1166
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 1167
    label "order"
  ]
  node [
    id 1168
    label "indicate"
  ]
  node [
    id 1169
    label "upomina&#263;"
  ]
  node [
    id 1170
    label "ulepsza&#263;"
  ]
  node [
    id 1171
    label "check"
  ]
  node [
    id 1172
    label "rusza&#263;"
  ]
  node [
    id 1173
    label "right"
  ]
  node [
    id 1174
    label "wprowadza&#263;"
  ]
  node [
    id 1175
    label "charakterystycznie"
  ]
  node [
    id 1176
    label "wyj&#261;tkowy"
  ]
  node [
    id 1177
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 1178
    label "specyficznie"
  ]
  node [
    id 1179
    label "specjalny"
  ]
  node [
    id 1180
    label "intencjonalny"
  ]
  node [
    id 1181
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 1182
    label "niedorozw&#243;j"
  ]
  node [
    id 1183
    label "szczeg&#243;lny"
  ]
  node [
    id 1184
    label "specjalnie"
  ]
  node [
    id 1185
    label "nieetatowy"
  ]
  node [
    id 1186
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 1187
    label "nienormalny"
  ]
  node [
    id 1188
    label "umy&#347;lnie"
  ]
  node [
    id 1189
    label "odpowiedni"
  ]
  node [
    id 1190
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 1191
    label "wyj&#261;tkowo"
  ]
  node [
    id 1192
    label "inny"
  ]
  node [
    id 1193
    label "charakterystyczny"
  ]
  node [
    id 1194
    label "podobnie"
  ]
  node [
    id 1195
    label "szczeg&#243;lnie"
  ]
  node [
    id 1196
    label "catalog"
  ]
  node [
    id 1197
    label "sumariusz"
  ]
  node [
    id 1198
    label "book"
  ]
  node [
    id 1199
    label "stock"
  ]
  node [
    id 1200
    label "figurowa&#263;"
  ]
  node [
    id 1201
    label "wyliczanka"
  ]
  node [
    id 1202
    label "j&#281;zykowo"
  ]
  node [
    id 1203
    label "redakcja"
  ]
  node [
    id 1204
    label "pomini&#281;cie"
  ]
  node [
    id 1205
    label "preparacja"
  ]
  node [
    id 1206
    label "odmianka"
  ]
  node [
    id 1207
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1208
    label "koniektura"
  ]
  node [
    id 1209
    label "pisa&#263;"
  ]
  node [
    id 1210
    label "obelga"
  ]
  node [
    id 1211
    label "debit"
  ]
  node [
    id 1212
    label "druk"
  ]
  node [
    id 1213
    label "szata_graficzna"
  ]
  node [
    id 1214
    label "wydawa&#263;"
  ]
  node [
    id 1215
    label "szermierka"
  ]
  node [
    id 1216
    label "wyda&#263;"
  ]
  node [
    id 1217
    label "ustawienie"
  ]
  node [
    id 1218
    label "publikacja"
  ]
  node [
    id 1219
    label "status"
  ]
  node [
    id 1220
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 1221
    label "adres"
  ]
  node [
    id 1222
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1223
    label "rozmieszczenie"
  ]
  node [
    id 1224
    label "sytuacja"
  ]
  node [
    id 1225
    label "redaktor"
  ]
  node [
    id 1226
    label "awansowa&#263;"
  ]
  node [
    id 1227
    label "znaczenie"
  ]
  node [
    id 1228
    label "awans"
  ]
  node [
    id 1229
    label "awansowanie"
  ]
  node [
    id 1230
    label "poster"
  ]
  node [
    id 1231
    label "le&#380;e&#263;"
  ]
  node [
    id 1232
    label "entliczek"
  ]
  node [
    id 1233
    label "zabawa"
  ]
  node [
    id 1234
    label "wiersz"
  ]
  node [
    id 1235
    label "pentliczek"
  ]
  node [
    id 1236
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1237
    label "odzyskiwa&#263;"
  ]
  node [
    id 1238
    label "znachodzi&#263;"
  ]
  node [
    id 1239
    label "pozyskiwa&#263;"
  ]
  node [
    id 1240
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 1241
    label "detect"
  ]
  node [
    id 1242
    label "unwrap"
  ]
  node [
    id 1243
    label "wykrywa&#263;"
  ]
  node [
    id 1244
    label "os&#261;dza&#263;"
  ]
  node [
    id 1245
    label "doznawa&#263;"
  ]
  node [
    id 1246
    label "wymy&#347;la&#263;"
  ]
  node [
    id 1247
    label "mistreat"
  ]
  node [
    id 1248
    label "obra&#380;a&#263;"
  ]
  node [
    id 1249
    label "odkrywa&#263;"
  ]
  node [
    id 1250
    label "debunk"
  ]
  node [
    id 1251
    label "dostrzega&#263;"
  ]
  node [
    id 1252
    label "okre&#347;la&#263;"
  ]
  node [
    id 1253
    label "uzyskiwa&#263;"
  ]
  node [
    id 1254
    label "wytwarza&#263;"
  ]
  node [
    id 1255
    label "tease"
  ]
  node [
    id 1256
    label "take"
  ]
  node [
    id 1257
    label "hurt"
  ]
  node [
    id 1258
    label "recur"
  ]
  node [
    id 1259
    label "przychodzi&#263;"
  ]
  node [
    id 1260
    label "sum_up"
  ]
  node [
    id 1261
    label "strike"
  ]
  node [
    id 1262
    label "s&#261;dzi&#263;"
  ]
  node [
    id 1263
    label "hold"
  ]
  node [
    id 1264
    label "s&#281;dziowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 26
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 52
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 244
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 637
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 629
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 458
  ]
  edge [
    source 16
    target 464
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 457
  ]
  edge [
    source 16
    target 459
  ]
  edge [
    source 16
    target 460
  ]
  edge [
    source 16
    target 244
  ]
  edge [
    source 16
    target 461
  ]
  edge [
    source 16
    target 462
  ]
  edge [
    source 16
    target 463
  ]
  edge [
    source 16
    target 28
  ]
  edge [
    source 16
    target 465
  ]
  edge [
    source 16
    target 466
  ]
  edge [
    source 16
    target 467
  ]
  edge [
    source 16
    target 468
  ]
  edge [
    source 16
    target 469
  ]
  edge [
    source 16
    target 470
  ]
  edge [
    source 16
    target 471
  ]
  edge [
    source 16
    target 472
  ]
  edge [
    source 16
    target 473
  ]
  edge [
    source 16
    target 474
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 475
  ]
  edge [
    source 16
    target 476
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 598
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 491
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 321
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 529
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 559
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 503
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 90
  ]
  edge [
    source 16
    target 567
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 499
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 771
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 478
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 16
    target 777
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 779
  ]
  edge [
    source 16
    target 780
  ]
  edge [
    source 16
    target 781
  ]
  edge [
    source 16
    target 782
  ]
  edge [
    source 16
    target 783
  ]
  edge [
    source 16
    target 784
  ]
  edge [
    source 16
    target 553
  ]
  edge [
    source 16
    target 785
  ]
  edge [
    source 16
    target 786
  ]
  edge [
    source 16
    target 787
  ]
  edge [
    source 16
    target 788
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 641
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 563
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 629
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 16
    target 858
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 16
    target 860
  ]
  edge [
    source 16
    target 861
  ]
  edge [
    source 16
    target 862
  ]
  edge [
    source 16
    target 863
  ]
  edge [
    source 16
    target 864
  ]
  edge [
    source 16
    target 865
  ]
  edge [
    source 16
    target 866
  ]
  edge [
    source 16
    target 867
  ]
  edge [
    source 16
    target 868
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 871
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 16
    target 873
  ]
  edge [
    source 16
    target 874
  ]
  edge [
    source 16
    target 875
  ]
  edge [
    source 16
    target 876
  ]
  edge [
    source 16
    target 877
  ]
  edge [
    source 16
    target 878
  ]
  edge [
    source 16
    target 253
  ]
  edge [
    source 16
    target 879
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 881
  ]
  edge [
    source 16
    target 882
  ]
  edge [
    source 16
    target 883
  ]
  edge [
    source 16
    target 884
  ]
  edge [
    source 16
    target 239
  ]
  edge [
    source 16
    target 885
  ]
  edge [
    source 16
    target 886
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 887
  ]
  edge [
    source 16
    target 888
  ]
  edge [
    source 16
    target 889
  ]
  edge [
    source 16
    target 890
  ]
  edge [
    source 16
    target 44
  ]
  edge [
    source 16
    target 891
  ]
  edge [
    source 16
    target 892
  ]
  edge [
    source 16
    target 893
  ]
  edge [
    source 16
    target 894
  ]
  edge [
    source 16
    target 895
  ]
  edge [
    source 16
    target 896
  ]
  edge [
    source 16
    target 897
  ]
  edge [
    source 16
    target 898
  ]
  edge [
    source 16
    target 899
  ]
  edge [
    source 16
    target 900
  ]
  edge [
    source 16
    target 901
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 903
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 54
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 411
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 402
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 16
    target 930
  ]
  edge [
    source 16
    target 931
  ]
  edge [
    source 16
    target 932
  ]
  edge [
    source 16
    target 933
  ]
  edge [
    source 16
    target 934
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 935
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 936
  ]
  edge [
    source 16
    target 937
  ]
  edge [
    source 16
    target 938
  ]
  edge [
    source 16
    target 939
  ]
  edge [
    source 16
    target 940
  ]
  edge [
    source 16
    target 941
  ]
  edge [
    source 16
    target 942
  ]
  edge [
    source 16
    target 943
  ]
  edge [
    source 16
    target 944
  ]
  edge [
    source 16
    target 945
  ]
  edge [
    source 16
    target 946
  ]
  edge [
    source 16
    target 947
  ]
  edge [
    source 16
    target 948
  ]
  edge [
    source 16
    target 949
  ]
  edge [
    source 16
    target 950
  ]
  edge [
    source 16
    target 951
  ]
  edge [
    source 16
    target 952
  ]
  edge [
    source 16
    target 953
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 954
  ]
  edge [
    source 16
    target 955
  ]
  edge [
    source 16
    target 956
  ]
  edge [
    source 16
    target 957
  ]
  edge [
    source 16
    target 958
  ]
  edge [
    source 16
    target 959
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 961
  ]
  edge [
    source 16
    target 962
  ]
  edge [
    source 16
    target 963
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 966
  ]
  edge [
    source 16
    target 967
  ]
  edge [
    source 16
    target 968
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 16
    target 490
  ]
  edge [
    source 16
    target 568
  ]
  edge [
    source 16
    target 971
  ]
  edge [
    source 16
    target 314
  ]
  edge [
    source 16
    target 972
  ]
  edge [
    source 16
    target 973
  ]
  edge [
    source 16
    target 974
  ]
  edge [
    source 16
    target 975
  ]
  edge [
    source 16
    target 976
  ]
  edge [
    source 16
    target 977
  ]
  edge [
    source 16
    target 978
  ]
  edge [
    source 16
    target 979
  ]
  edge [
    source 16
    target 311
  ]
  edge [
    source 16
    target 980
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 101
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 614
  ]
  edge [
    source 19
    target 709
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 544
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 808
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 19
    target 244
  ]
  edge [
    source 19
    target 637
  ]
  edge [
    source 19
    target 352
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 95
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 19
    target 1026
  ]
  edge [
    source 19
    target 1027
  ]
  edge [
    source 19
    target 1028
  ]
  edge [
    source 19
    target 825
  ]
  edge [
    source 19
    target 1029
  ]
  edge [
    source 19
    target 80
  ]
  edge [
    source 19
    target 1030
  ]
  edge [
    source 19
    target 1031
  ]
  edge [
    source 19
    target 406
  ]
  edge [
    source 19
    target 98
  ]
  edge [
    source 19
    target 1032
  ]
  edge [
    source 19
    target 1033
  ]
  edge [
    source 19
    target 1034
  ]
  edge [
    source 19
    target 1035
  ]
  edge [
    source 19
    target 1036
  ]
  edge [
    source 19
    target 1037
  ]
  edge [
    source 19
    target 707
  ]
  edge [
    source 19
    target 1038
  ]
  edge [
    source 19
    target 1039
  ]
  edge [
    source 19
    target 1040
  ]
  edge [
    source 19
    target 1041
  ]
  edge [
    source 19
    target 1042
  ]
  edge [
    source 19
    target 1043
  ]
  edge [
    source 19
    target 491
  ]
  edge [
    source 19
    target 265
  ]
  edge [
    source 19
    target 1044
  ]
  edge [
    source 19
    target 1045
  ]
  edge [
    source 19
    target 1046
  ]
  edge [
    source 19
    target 85
  ]
  edge [
    source 19
    target 1047
  ]
  edge [
    source 19
    target 1048
  ]
  edge [
    source 19
    target 531
  ]
  edge [
    source 19
    target 1049
  ]
  edge [
    source 19
    target 1050
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 1051
  ]
  edge [
    source 19
    target 1052
  ]
  edge [
    source 19
    target 1053
  ]
  edge [
    source 19
    target 1054
  ]
  edge [
    source 19
    target 1055
  ]
  edge [
    source 19
    target 550
  ]
  edge [
    source 19
    target 674
  ]
  edge [
    source 19
    target 857
  ]
  edge [
    source 19
    target 1056
  ]
  edge [
    source 19
    target 1057
  ]
  edge [
    source 19
    target 1058
  ]
  edge [
    source 19
    target 1059
  ]
  edge [
    source 19
    target 1060
  ]
  edge [
    source 19
    target 1061
  ]
  edge [
    source 19
    target 1062
  ]
  edge [
    source 19
    target 1063
  ]
  edge [
    source 19
    target 1064
  ]
  edge [
    source 19
    target 1065
  ]
  edge [
    source 19
    target 1066
  ]
  edge [
    source 19
    target 1067
  ]
  edge [
    source 19
    target 1068
  ]
  edge [
    source 19
    target 1069
  ]
  edge [
    source 19
    target 136
  ]
  edge [
    source 19
    target 1070
  ]
  edge [
    source 19
    target 1071
  ]
  edge [
    source 19
    target 1072
  ]
  edge [
    source 19
    target 1073
  ]
  edge [
    source 19
    target 1074
  ]
  edge [
    source 19
    target 1075
  ]
  edge [
    source 19
    target 1076
  ]
  edge [
    source 19
    target 1077
  ]
  edge [
    source 19
    target 1078
  ]
  edge [
    source 19
    target 1079
  ]
  edge [
    source 19
    target 1080
  ]
  edge [
    source 19
    target 1081
  ]
  edge [
    source 19
    target 1082
  ]
  edge [
    source 19
    target 1083
  ]
  edge [
    source 19
    target 1084
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 19
    target 1089
  ]
  edge [
    source 19
    target 1090
  ]
  edge [
    source 19
    target 1091
  ]
  edge [
    source 19
    target 620
  ]
  edge [
    source 19
    target 1092
  ]
  edge [
    source 19
    target 1093
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 1095
  ]
  edge [
    source 19
    target 1096
  ]
  edge [
    source 19
    target 1097
  ]
  edge [
    source 19
    target 1098
  ]
  edge [
    source 19
    target 1099
  ]
  edge [
    source 19
    target 1100
  ]
  edge [
    source 19
    target 1101
  ]
  edge [
    source 19
    target 1102
  ]
  edge [
    source 19
    target 1103
  ]
  edge [
    source 19
    target 1104
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 1106
  ]
  edge [
    source 19
    target 706
  ]
  edge [
    source 19
    target 1107
  ]
  edge [
    source 19
    target 1108
  ]
  edge [
    source 19
    target 1109
  ]
  edge [
    source 19
    target 1110
  ]
  edge [
    source 19
    target 553
  ]
  edge [
    source 19
    target 1111
  ]
  edge [
    source 19
    target 846
  ]
  edge [
    source 19
    target 1112
  ]
  edge [
    source 19
    target 1113
  ]
  edge [
    source 19
    target 1114
  ]
  edge [
    source 19
    target 1115
  ]
  edge [
    source 19
    target 1116
  ]
  edge [
    source 19
    target 1117
  ]
  edge [
    source 19
    target 1118
  ]
  edge [
    source 19
    target 1119
  ]
  edge [
    source 19
    target 1120
  ]
  edge [
    source 19
    target 1121
  ]
  edge [
    source 19
    target 1122
  ]
  edge [
    source 19
    target 1123
  ]
  edge [
    source 19
    target 1124
  ]
  edge [
    source 19
    target 1125
  ]
  edge [
    source 19
    target 1126
  ]
  edge [
    source 19
    target 1127
  ]
  edge [
    source 19
    target 711
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 1128
  ]
  edge [
    source 19
    target 1129
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 637
  ]
  edge [
    source 20
    target 1043
  ]
  edge [
    source 20
    target 491
  ]
  edge [
    source 20
    target 265
  ]
  edge [
    source 20
    target 1044
  ]
  edge [
    source 20
    target 312
  ]
  edge [
    source 20
    target 492
  ]
  edge [
    source 20
    target 493
  ]
  edge [
    source 20
    target 494
  ]
  edge [
    source 20
    target 495
  ]
  edge [
    source 20
    target 496
  ]
  edge [
    source 20
    target 497
  ]
  edge [
    source 20
    target 311
  ]
  edge [
    source 20
    target 498
  ]
  edge [
    source 20
    target 499
  ]
  edge [
    source 20
    target 500
  ]
  edge [
    source 20
    target 501
  ]
  edge [
    source 20
    target 502
  ]
  edge [
    source 20
    target 503
  ]
  edge [
    source 20
    target 504
  ]
  edge [
    source 20
    target 1130
  ]
  edge [
    source 20
    target 1131
  ]
  edge [
    source 20
    target 85
  ]
  edge [
    source 20
    target 458
  ]
  edge [
    source 20
    target 1132
  ]
  edge [
    source 20
    target 1133
  ]
  edge [
    source 20
    target 583
  ]
  edge [
    source 20
    target 1134
  ]
  edge [
    source 20
    target 1135
  ]
  edge [
    source 20
    target 1136
  ]
  edge [
    source 20
    target 244
  ]
  edge [
    source 20
    target 1137
  ]
  edge [
    source 20
    target 1138
  ]
  edge [
    source 20
    target 1139
  ]
  edge [
    source 20
    target 1140
  ]
  edge [
    source 20
    target 1141
  ]
  edge [
    source 20
    target 1142
  ]
  edge [
    source 20
    target 456
  ]
  edge [
    source 20
    target 638
  ]
  edge [
    source 20
    target 1143
  ]
  edge [
    source 20
    target 1144
  ]
  edge [
    source 20
    target 1145
  ]
  edge [
    source 20
    target 1146
  ]
  edge [
    source 20
    target 641
  ]
  edge [
    source 20
    target 1147
  ]
  edge [
    source 20
    target 1148
  ]
  edge [
    source 20
    target 73
  ]
  edge [
    source 20
    target 1149
  ]
  edge [
    source 20
    target 1150
  ]
  edge [
    source 20
    target 1151
  ]
  edge [
    source 20
    target 477
  ]
  edge [
    source 20
    target 807
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1152
  ]
  edge [
    source 21
    target 1153
  ]
  edge [
    source 21
    target 926
  ]
  edge [
    source 21
    target 1154
  ]
  edge [
    source 21
    target 1155
  ]
  edge [
    source 21
    target 87
  ]
  edge [
    source 21
    target 1156
  ]
  edge [
    source 21
    target 1157
  ]
  edge [
    source 21
    target 1158
  ]
  edge [
    source 21
    target 1159
  ]
  edge [
    source 21
    target 1160
  ]
  edge [
    source 21
    target 351
  ]
  edge [
    source 21
    target 661
  ]
  edge [
    source 21
    target 1161
  ]
  edge [
    source 21
    target 355
  ]
  edge [
    source 21
    target 1162
  ]
  edge [
    source 21
    target 1163
  ]
  edge [
    source 21
    target 1164
  ]
  edge [
    source 21
    target 403
  ]
  edge [
    source 21
    target 1165
  ]
  edge [
    source 21
    target 1166
  ]
  edge [
    source 21
    target 656
  ]
  edge [
    source 21
    target 1167
  ]
  edge [
    source 21
    target 1168
  ]
  edge [
    source 21
    target 1169
  ]
  edge [
    source 21
    target 1170
  ]
  edge [
    source 21
    target 1171
  ]
  edge [
    source 21
    target 1172
  ]
  edge [
    source 21
    target 1173
  ]
  edge [
    source 21
    target 1174
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1175
  ]
  edge [
    source 22
    target 1176
  ]
  edge [
    source 22
    target 1177
  ]
  edge [
    source 22
    target 1178
  ]
  edge [
    source 22
    target 1179
  ]
  edge [
    source 22
    target 1180
  ]
  edge [
    source 22
    target 1181
  ]
  edge [
    source 22
    target 1182
  ]
  edge [
    source 22
    target 1183
  ]
  edge [
    source 22
    target 1184
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 1186
  ]
  edge [
    source 22
    target 1187
  ]
  edge [
    source 22
    target 1188
  ]
  edge [
    source 22
    target 1189
  ]
  edge [
    source 22
    target 1190
  ]
  edge [
    source 22
    target 1191
  ]
  edge [
    source 22
    target 1192
  ]
  edge [
    source 22
    target 1193
  ]
  edge [
    source 22
    target 1194
  ]
  edge [
    source 22
    target 445
  ]
  edge [
    source 22
    target 1195
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 491
  ]
  edge [
    source 24
    target 1196
  ]
  edge [
    source 24
    target 126
  ]
  edge [
    source 24
    target 321
  ]
  edge [
    source 24
    target 1197
  ]
  edge [
    source 24
    target 1198
  ]
  edge [
    source 24
    target 1199
  ]
  edge [
    source 24
    target 1200
  ]
  edge [
    source 24
    target 1201
  ]
  edge [
    source 24
    target 697
  ]
  edge [
    source 24
    target 1202
  ]
  edge [
    source 24
    target 825
  ]
  edge [
    source 24
    target 1203
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 1204
  ]
  edge [
    source 24
    target 777
  ]
  edge [
    source 24
    target 1205
  ]
  edge [
    source 24
    target 1206
  ]
  edge [
    source 24
    target 1207
  ]
  edge [
    source 24
    target 1208
  ]
  edge [
    source 24
    target 1209
  ]
  edge [
    source 24
    target 1210
  ]
  edge [
    source 24
    target 312
  ]
  edge [
    source 24
    target 492
  ]
  edge [
    source 24
    target 493
  ]
  edge [
    source 24
    target 494
  ]
  edge [
    source 24
    target 495
  ]
  edge [
    source 24
    target 496
  ]
  edge [
    source 24
    target 497
  ]
  edge [
    source 24
    target 311
  ]
  edge [
    source 24
    target 498
  ]
  edge [
    source 24
    target 499
  ]
  edge [
    source 24
    target 500
  ]
  edge [
    source 24
    target 501
  ]
  edge [
    source 24
    target 502
  ]
  edge [
    source 24
    target 503
  ]
  edge [
    source 24
    target 504
  ]
  edge [
    source 24
    target 102
  ]
  edge [
    source 24
    target 1211
  ]
  edge [
    source 24
    target 1212
  ]
  edge [
    source 24
    target 133
  ]
  edge [
    source 24
    target 1213
  ]
  edge [
    source 24
    target 1214
  ]
  edge [
    source 24
    target 1215
  ]
  edge [
    source 24
    target 507
  ]
  edge [
    source 24
    target 1216
  ]
  edge [
    source 24
    target 1217
  ]
  edge [
    source 24
    target 1218
  ]
  edge [
    source 24
    target 1219
  ]
  edge [
    source 24
    target 112
  ]
  edge [
    source 24
    target 1220
  ]
  edge [
    source 24
    target 1221
  ]
  edge [
    source 24
    target 1222
  ]
  edge [
    source 24
    target 1223
  ]
  edge [
    source 24
    target 1224
  ]
  edge [
    source 24
    target 749
  ]
  edge [
    source 24
    target 1225
  ]
  edge [
    source 24
    target 1226
  ]
  edge [
    source 24
    target 122
  ]
  edge [
    source 24
    target 756
  ]
  edge [
    source 24
    target 1227
  ]
  edge [
    source 24
    target 1228
  ]
  edge [
    source 24
    target 1229
  ]
  edge [
    source 24
    target 1230
  ]
  edge [
    source 24
    target 1231
  ]
  edge [
    source 24
    target 1232
  ]
  edge [
    source 24
    target 1233
  ]
  edge [
    source 24
    target 1234
  ]
  edge [
    source 24
    target 1235
  ]
  edge [
    source 24
    target 1236
  ]
  edge [
    source 25
    target 1237
  ]
  edge [
    source 25
    target 1238
  ]
  edge [
    source 25
    target 1239
  ]
  edge [
    source 25
    target 1240
  ]
  edge [
    source 25
    target 1241
  ]
  edge [
    source 25
    target 340
  ]
  edge [
    source 25
    target 1242
  ]
  edge [
    source 25
    target 1243
  ]
  edge [
    source 25
    target 1244
  ]
  edge [
    source 25
    target 1245
  ]
  edge [
    source 25
    target 1246
  ]
  edge [
    source 25
    target 1247
  ]
  edge [
    source 25
    target 1248
  ]
  edge [
    source 25
    target 1249
  ]
  edge [
    source 25
    target 1250
  ]
  edge [
    source 25
    target 1251
  ]
  edge [
    source 25
    target 1252
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 402
  ]
  edge [
    source 25
    target 403
  ]
  edge [
    source 25
    target 370
  ]
  edge [
    source 25
    target 351
  ]
  edge [
    source 25
    target 1253
  ]
  edge [
    source 25
    target 1254
  ]
  edge [
    source 25
    target 1255
  ]
  edge [
    source 25
    target 1256
  ]
  edge [
    source 25
    target 1257
  ]
  edge [
    source 25
    target 1258
  ]
  edge [
    source 25
    target 1259
  ]
  edge [
    source 25
    target 1260
  ]
  edge [
    source 25
    target 1261
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 1262
  ]
  edge [
    source 25
    target 1263
  ]
  edge [
    source 25
    target 1264
  ]
]
