graph [
  node [
    id 0
    label "przemys&#322;"
    origin "text"
  ]
  node [
    id 1
    label "zbrojeniowy"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 4
    label "wszystko"
    origin "text"
  ]
  node [
    id 5
    label "stworzy&#263;"
    origin "text"
  ]
  node [
    id 6
    label "dobry"
    origin "text"
  ]
  node [
    id 7
    label "odpowiednik"
    origin "text"
  ]
  node [
    id 8
    label "rosyjski"
    origin "text"
  ]
  node [
    id 9
    label "przeciwlotniczy"
    origin "text"
  ]
  node [
    id 10
    label "system"
    origin "text"
  ]
  node [
    id 11
    label "rakietowo"
    origin "text"
  ]
  node [
    id 12
    label "artyleryjski"
    origin "text"
  ]
  node [
    id 13
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 14
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 15
    label "uprzemys&#322;owienie"
  ]
  node [
    id 16
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 17
    label "przechowalnictwo"
  ]
  node [
    id 18
    label "uprzemys&#322;awianie"
  ]
  node [
    id 19
    label "gospodarka"
  ]
  node [
    id 20
    label "przemys&#322;_spo&#380;ywczy"
  ]
  node [
    id 21
    label "wiedza"
  ]
  node [
    id 22
    label "dobra_konsumpcyjne"
  ]
  node [
    id 23
    label "szko&#322;a"
  ]
  node [
    id 24
    label "produkt"
  ]
  node [
    id 25
    label "inwentarz"
  ]
  node [
    id 26
    label "rynek"
  ]
  node [
    id 27
    label "mieszkalnictwo"
  ]
  node [
    id 28
    label "agregat_ekonomiczny"
  ]
  node [
    id 29
    label "miejsce_pracy"
  ]
  node [
    id 30
    label "produkowanie"
  ]
  node [
    id 31
    label "farmaceutyka"
  ]
  node [
    id 32
    label "rolnictwo"
  ]
  node [
    id 33
    label "transport"
  ]
  node [
    id 34
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 35
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 36
    label "obronno&#347;&#263;"
  ]
  node [
    id 37
    label "sektor_prywatny"
  ]
  node [
    id 38
    label "sch&#322;adza&#263;"
  ]
  node [
    id 39
    label "czerwona_strefa"
  ]
  node [
    id 40
    label "struktura"
  ]
  node [
    id 41
    label "pole"
  ]
  node [
    id 42
    label "sektor_publiczny"
  ]
  node [
    id 43
    label "bankowo&#347;&#263;"
  ]
  node [
    id 44
    label "gospodarowanie"
  ]
  node [
    id 45
    label "obora"
  ]
  node [
    id 46
    label "gospodarka_wodna"
  ]
  node [
    id 47
    label "gospodarka_le&#347;na"
  ]
  node [
    id 48
    label "gospodarowa&#263;"
  ]
  node [
    id 49
    label "fabryka"
  ]
  node [
    id 50
    label "wytw&#243;rnia"
  ]
  node [
    id 51
    label "stodo&#322;a"
  ]
  node [
    id 52
    label "spichlerz"
  ]
  node [
    id 53
    label "sch&#322;adzanie"
  ]
  node [
    id 54
    label "administracja"
  ]
  node [
    id 55
    label "sch&#322;odzenie"
  ]
  node [
    id 56
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 57
    label "zasada"
  ]
  node [
    id 58
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 59
    label "regulacja_cen"
  ]
  node [
    id 60
    label "szkolnictwo"
  ]
  node [
    id 61
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 62
    label "rozwini&#281;cie"
  ]
  node [
    id 63
    label "proces_ekonomiczny"
  ]
  node [
    id 64
    label "spowodowanie"
  ]
  node [
    id 65
    label "modernizacja"
  ]
  node [
    id 66
    label "industrialization"
  ]
  node [
    id 67
    label "czynno&#347;&#263;"
  ]
  node [
    id 68
    label "powodowanie"
  ]
  node [
    id 69
    label "czyj&#347;"
  ]
  node [
    id 70
    label "m&#261;&#380;"
  ]
  node [
    id 71
    label "prywatny"
  ]
  node [
    id 72
    label "ma&#322;&#380;onek"
  ]
  node [
    id 73
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 74
    label "ch&#322;op"
  ]
  node [
    id 75
    label "cz&#322;owiek"
  ]
  node [
    id 76
    label "pan_m&#322;ody"
  ]
  node [
    id 77
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 78
    label "&#347;lubny"
  ]
  node [
    id 79
    label "pan_domu"
  ]
  node [
    id 80
    label "pan_i_w&#322;adca"
  ]
  node [
    id 81
    label "stary"
  ]
  node [
    id 82
    label "lock"
  ]
  node [
    id 83
    label "absolut"
  ]
  node [
    id 84
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 85
    label "integer"
  ]
  node [
    id 86
    label "liczba"
  ]
  node [
    id 87
    label "zlewanie_si&#281;"
  ]
  node [
    id 88
    label "ilo&#347;&#263;"
  ]
  node [
    id 89
    label "uk&#322;ad"
  ]
  node [
    id 90
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 91
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 92
    label "pe&#322;ny"
  ]
  node [
    id 93
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 94
    label "olejek_eteryczny"
  ]
  node [
    id 95
    label "byt"
  ]
  node [
    id 96
    label "create"
  ]
  node [
    id 97
    label "specjalista_od_public_relations"
  ]
  node [
    id 98
    label "zrobi&#263;"
  ]
  node [
    id 99
    label "wizerunek"
  ]
  node [
    id 100
    label "przygotowa&#263;"
  ]
  node [
    id 101
    label "set"
  ]
  node [
    id 102
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 103
    label "wykona&#263;"
  ]
  node [
    id 104
    label "cook"
  ]
  node [
    id 105
    label "wyszkoli&#263;"
  ]
  node [
    id 106
    label "train"
  ]
  node [
    id 107
    label "arrange"
  ]
  node [
    id 108
    label "spowodowa&#263;"
  ]
  node [
    id 109
    label "wytworzy&#263;"
  ]
  node [
    id 110
    label "dress"
  ]
  node [
    id 111
    label "ukierunkowa&#263;"
  ]
  node [
    id 112
    label "post&#261;pi&#263;"
  ]
  node [
    id 113
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 114
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 115
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 116
    label "zorganizowa&#263;"
  ]
  node [
    id 117
    label "appoint"
  ]
  node [
    id 118
    label "wystylizowa&#263;"
  ]
  node [
    id 119
    label "cause"
  ]
  node [
    id 120
    label "przerobi&#263;"
  ]
  node [
    id 121
    label "nabra&#263;"
  ]
  node [
    id 122
    label "make"
  ]
  node [
    id 123
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 124
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 125
    label "wydali&#263;"
  ]
  node [
    id 126
    label "wykreowanie"
  ]
  node [
    id 127
    label "wygl&#261;d"
  ]
  node [
    id 128
    label "wytw&#243;r"
  ]
  node [
    id 129
    label "posta&#263;"
  ]
  node [
    id 130
    label "kreacja"
  ]
  node [
    id 131
    label "appearance"
  ]
  node [
    id 132
    label "kreowanie"
  ]
  node [
    id 133
    label "wykreowa&#263;"
  ]
  node [
    id 134
    label "kreowa&#263;"
  ]
  node [
    id 135
    label "dobroczynny"
  ]
  node [
    id 136
    label "czw&#243;rka"
  ]
  node [
    id 137
    label "spokojny"
  ]
  node [
    id 138
    label "skuteczny"
  ]
  node [
    id 139
    label "&#347;mieszny"
  ]
  node [
    id 140
    label "mi&#322;y"
  ]
  node [
    id 141
    label "grzeczny"
  ]
  node [
    id 142
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 143
    label "powitanie"
  ]
  node [
    id 144
    label "dobrze"
  ]
  node [
    id 145
    label "ca&#322;y"
  ]
  node [
    id 146
    label "zwrot"
  ]
  node [
    id 147
    label "pomy&#347;lny"
  ]
  node [
    id 148
    label "moralny"
  ]
  node [
    id 149
    label "drogi"
  ]
  node [
    id 150
    label "pozytywny"
  ]
  node [
    id 151
    label "odpowiedni"
  ]
  node [
    id 152
    label "korzystny"
  ]
  node [
    id 153
    label "pos&#322;uszny"
  ]
  node [
    id 154
    label "moralnie"
  ]
  node [
    id 155
    label "warto&#347;ciowy"
  ]
  node [
    id 156
    label "etycznie"
  ]
  node [
    id 157
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 158
    label "nale&#380;ny"
  ]
  node [
    id 159
    label "nale&#380;yty"
  ]
  node [
    id 160
    label "typowy"
  ]
  node [
    id 161
    label "uprawniony"
  ]
  node [
    id 162
    label "zasadniczy"
  ]
  node [
    id 163
    label "stosownie"
  ]
  node [
    id 164
    label "taki"
  ]
  node [
    id 165
    label "charakterystyczny"
  ]
  node [
    id 166
    label "prawdziwy"
  ]
  node [
    id 167
    label "ten"
  ]
  node [
    id 168
    label "pozytywnie"
  ]
  node [
    id 169
    label "fajny"
  ]
  node [
    id 170
    label "dodatnio"
  ]
  node [
    id 171
    label "przyjemny"
  ]
  node [
    id 172
    label "po&#380;&#261;dany"
  ]
  node [
    id 173
    label "niepowa&#380;ny"
  ]
  node [
    id 174
    label "o&#347;mieszanie"
  ]
  node [
    id 175
    label "&#347;miesznie"
  ]
  node [
    id 176
    label "bawny"
  ]
  node [
    id 177
    label "o&#347;mieszenie"
  ]
  node [
    id 178
    label "dziwny"
  ]
  node [
    id 179
    label "nieadekwatny"
  ]
  node [
    id 180
    label "wolny"
  ]
  node [
    id 181
    label "uspokajanie_si&#281;"
  ]
  node [
    id 182
    label "bezproblemowy"
  ]
  node [
    id 183
    label "spokojnie"
  ]
  node [
    id 184
    label "uspokojenie_si&#281;"
  ]
  node [
    id 185
    label "cicho"
  ]
  node [
    id 186
    label "uspokojenie"
  ]
  node [
    id 187
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 188
    label "nietrudny"
  ]
  node [
    id 189
    label "uspokajanie"
  ]
  node [
    id 190
    label "zale&#380;ny"
  ]
  node [
    id 191
    label "uleg&#322;y"
  ]
  node [
    id 192
    label "pos&#322;usznie"
  ]
  node [
    id 193
    label "grzecznie"
  ]
  node [
    id 194
    label "stosowny"
  ]
  node [
    id 195
    label "niewinny"
  ]
  node [
    id 196
    label "konserwatywny"
  ]
  node [
    id 197
    label "nijaki"
  ]
  node [
    id 198
    label "korzystnie"
  ]
  node [
    id 199
    label "drogo"
  ]
  node [
    id 200
    label "bliski"
  ]
  node [
    id 201
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 202
    label "przyjaciel"
  ]
  node [
    id 203
    label "jedyny"
  ]
  node [
    id 204
    label "du&#380;y"
  ]
  node [
    id 205
    label "zdr&#243;w"
  ]
  node [
    id 206
    label "calu&#347;ko"
  ]
  node [
    id 207
    label "kompletny"
  ]
  node [
    id 208
    label "&#380;ywy"
  ]
  node [
    id 209
    label "podobny"
  ]
  node [
    id 210
    label "ca&#322;o"
  ]
  node [
    id 211
    label "poskutkowanie"
  ]
  node [
    id 212
    label "sprawny"
  ]
  node [
    id 213
    label "skutecznie"
  ]
  node [
    id 214
    label "skutkowanie"
  ]
  node [
    id 215
    label "pomy&#347;lnie"
  ]
  node [
    id 216
    label "toto-lotek"
  ]
  node [
    id 217
    label "trafienie"
  ]
  node [
    id 218
    label "zbi&#243;r"
  ]
  node [
    id 219
    label "arkusz_drukarski"
  ]
  node [
    id 220
    label "&#322;&#243;dka"
  ]
  node [
    id 221
    label "four"
  ]
  node [
    id 222
    label "&#263;wiartka"
  ]
  node [
    id 223
    label "hotel"
  ]
  node [
    id 224
    label "cyfra"
  ]
  node [
    id 225
    label "pok&#243;j"
  ]
  node [
    id 226
    label "stopie&#324;"
  ]
  node [
    id 227
    label "obiekt"
  ]
  node [
    id 228
    label "minialbum"
  ]
  node [
    id 229
    label "osada"
  ]
  node [
    id 230
    label "p&#322;yta_winylowa"
  ]
  node [
    id 231
    label "blotka"
  ]
  node [
    id 232
    label "zaprz&#281;g"
  ]
  node [
    id 233
    label "przedtrzonowiec"
  ]
  node [
    id 234
    label "punkt"
  ]
  node [
    id 235
    label "turn"
  ]
  node [
    id 236
    label "turning"
  ]
  node [
    id 237
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 238
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 239
    label "skr&#281;t"
  ]
  node [
    id 240
    label "obr&#243;t"
  ]
  node [
    id 241
    label "fraza_czasownikowa"
  ]
  node [
    id 242
    label "jednostka_leksykalna"
  ]
  node [
    id 243
    label "zmiana"
  ]
  node [
    id 244
    label "wyra&#380;enie"
  ]
  node [
    id 245
    label "welcome"
  ]
  node [
    id 246
    label "spotkanie"
  ]
  node [
    id 247
    label "pozdrowienie"
  ]
  node [
    id 248
    label "zwyczaj"
  ]
  node [
    id 249
    label "greeting"
  ]
  node [
    id 250
    label "zdarzony"
  ]
  node [
    id 251
    label "odpowiednio"
  ]
  node [
    id 252
    label "odpowiadanie"
  ]
  node [
    id 253
    label "specjalny"
  ]
  node [
    id 254
    label "kochanek"
  ]
  node [
    id 255
    label "sk&#322;onny"
  ]
  node [
    id 256
    label "wybranek"
  ]
  node [
    id 257
    label "umi&#322;owany"
  ]
  node [
    id 258
    label "przyjemnie"
  ]
  node [
    id 259
    label "mi&#322;o"
  ]
  node [
    id 260
    label "kochanie"
  ]
  node [
    id 261
    label "dyplomata"
  ]
  node [
    id 262
    label "dobroczynnie"
  ]
  node [
    id 263
    label "lepiej"
  ]
  node [
    id 264
    label "wiele"
  ]
  node [
    id 265
    label "spo&#322;eczny"
  ]
  node [
    id 266
    label "odmiana"
  ]
  node [
    id 267
    label "mutant"
  ]
  node [
    id 268
    label "rewizja"
  ]
  node [
    id 269
    label "gramatyka"
  ]
  node [
    id 270
    label "typ"
  ]
  node [
    id 271
    label "paradygmat"
  ]
  node [
    id 272
    label "jednostka_systematyczna"
  ]
  node [
    id 273
    label "change"
  ]
  node [
    id 274
    label "podgatunek"
  ]
  node [
    id 275
    label "ferment"
  ]
  node [
    id 276
    label "rasa"
  ]
  node [
    id 277
    label "zjawisko"
  ]
  node [
    id 278
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 279
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 280
    label "kacapski"
  ]
  node [
    id 281
    label "po_rosyjsku"
  ]
  node [
    id 282
    label "wielkoruski"
  ]
  node [
    id 283
    label "Russian"
  ]
  node [
    id 284
    label "j&#281;zyk"
  ]
  node [
    id 285
    label "rusek"
  ]
  node [
    id 286
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 287
    label "artykulator"
  ]
  node [
    id 288
    label "kod"
  ]
  node [
    id 289
    label "kawa&#322;ek"
  ]
  node [
    id 290
    label "przedmiot"
  ]
  node [
    id 291
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 292
    label "stylik"
  ]
  node [
    id 293
    label "przet&#322;umaczenie"
  ]
  node [
    id 294
    label "formalizowanie"
  ]
  node [
    id 295
    label "ssanie"
  ]
  node [
    id 296
    label "ssa&#263;"
  ]
  node [
    id 297
    label "language"
  ]
  node [
    id 298
    label "liza&#263;"
  ]
  node [
    id 299
    label "napisa&#263;"
  ]
  node [
    id 300
    label "konsonantyzm"
  ]
  node [
    id 301
    label "wokalizm"
  ]
  node [
    id 302
    label "pisa&#263;"
  ]
  node [
    id 303
    label "fonetyka"
  ]
  node [
    id 304
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 305
    label "jeniec"
  ]
  node [
    id 306
    label "but"
  ]
  node [
    id 307
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 308
    label "po_koroniarsku"
  ]
  node [
    id 309
    label "kultura_duchowa"
  ]
  node [
    id 310
    label "t&#322;umaczenie"
  ]
  node [
    id 311
    label "m&#243;wienie"
  ]
  node [
    id 312
    label "pype&#263;"
  ]
  node [
    id 313
    label "lizanie"
  ]
  node [
    id 314
    label "pismo"
  ]
  node [
    id 315
    label "formalizowa&#263;"
  ]
  node [
    id 316
    label "rozumie&#263;"
  ]
  node [
    id 317
    label "organ"
  ]
  node [
    id 318
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 319
    label "rozumienie"
  ]
  node [
    id 320
    label "spos&#243;b"
  ]
  node [
    id 321
    label "makroglosja"
  ]
  node [
    id 322
    label "m&#243;wi&#263;"
  ]
  node [
    id 323
    label "jama_ustna"
  ]
  node [
    id 324
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 325
    label "formacja_geologiczna"
  ]
  node [
    id 326
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 327
    label "natural_language"
  ]
  node [
    id 328
    label "s&#322;ownictwo"
  ]
  node [
    id 329
    label "urz&#261;dzenie"
  ]
  node [
    id 330
    label "wschodnioeuropejski"
  ]
  node [
    id 331
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 332
    label "poga&#324;ski"
  ]
  node [
    id 333
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 334
    label "topielec"
  ]
  node [
    id 335
    label "europejski"
  ]
  node [
    id 336
    label "j&#281;zyk_rosyjski"
  ]
  node [
    id 337
    label "po_kacapsku"
  ]
  node [
    id 338
    label "ruski"
  ]
  node [
    id 339
    label "imperialny"
  ]
  node [
    id 340
    label "po_wielkorusku"
  ]
  node [
    id 341
    label "bojowy"
  ]
  node [
    id 342
    label "bojowo"
  ]
  node [
    id 343
    label "pewny"
  ]
  node [
    id 344
    label "&#347;mia&#322;y"
  ]
  node [
    id 345
    label "zadziorny"
  ]
  node [
    id 346
    label "bojowniczy"
  ]
  node [
    id 347
    label "waleczny"
  ]
  node [
    id 348
    label "j&#261;dro"
  ]
  node [
    id 349
    label "systemik"
  ]
  node [
    id 350
    label "rozprz&#261;c"
  ]
  node [
    id 351
    label "oprogramowanie"
  ]
  node [
    id 352
    label "poj&#281;cie"
  ]
  node [
    id 353
    label "systemat"
  ]
  node [
    id 354
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 355
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 356
    label "model"
  ]
  node [
    id 357
    label "usenet"
  ]
  node [
    id 358
    label "s&#261;d"
  ]
  node [
    id 359
    label "porz&#261;dek"
  ]
  node [
    id 360
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 361
    label "przyn&#281;ta"
  ]
  node [
    id 362
    label "p&#322;&#243;d"
  ]
  node [
    id 363
    label "net"
  ]
  node [
    id 364
    label "w&#281;dkarstwo"
  ]
  node [
    id 365
    label "eratem"
  ]
  node [
    id 366
    label "oddzia&#322;"
  ]
  node [
    id 367
    label "doktryna"
  ]
  node [
    id 368
    label "pulpit"
  ]
  node [
    id 369
    label "konstelacja"
  ]
  node [
    id 370
    label "jednostka_geologiczna"
  ]
  node [
    id 371
    label "o&#347;"
  ]
  node [
    id 372
    label "podsystem"
  ]
  node [
    id 373
    label "metoda"
  ]
  node [
    id 374
    label "ryba"
  ]
  node [
    id 375
    label "Leopard"
  ]
  node [
    id 376
    label "Android"
  ]
  node [
    id 377
    label "zachowanie"
  ]
  node [
    id 378
    label "cybernetyk"
  ]
  node [
    id 379
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 380
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 381
    label "method"
  ]
  node [
    id 382
    label "sk&#322;ad"
  ]
  node [
    id 383
    label "podstawa"
  ]
  node [
    id 384
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 385
    label "pot&#281;ga"
  ]
  node [
    id 386
    label "documentation"
  ]
  node [
    id 387
    label "column"
  ]
  node [
    id 388
    label "zasadzenie"
  ]
  node [
    id 389
    label "za&#322;o&#380;enie"
  ]
  node [
    id 390
    label "punkt_odniesienia"
  ]
  node [
    id 391
    label "zasadzi&#263;"
  ]
  node [
    id 392
    label "bok"
  ]
  node [
    id 393
    label "d&#243;&#322;"
  ]
  node [
    id 394
    label "dzieci&#281;ctwo"
  ]
  node [
    id 395
    label "background"
  ]
  node [
    id 396
    label "podstawowy"
  ]
  node [
    id 397
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 398
    label "strategia"
  ]
  node [
    id 399
    label "pomys&#322;"
  ]
  node [
    id 400
    label "&#347;ciana"
  ]
  node [
    id 401
    label "narz&#281;dzie"
  ]
  node [
    id 402
    label "tryb"
  ]
  node [
    id 403
    label "nature"
  ]
  node [
    id 404
    label "relacja"
  ]
  node [
    id 405
    label "stan"
  ]
  node [
    id 406
    label "cecha"
  ]
  node [
    id 407
    label "styl_architektoniczny"
  ]
  node [
    id 408
    label "normalizacja"
  ]
  node [
    id 409
    label "egzemplarz"
  ]
  node [
    id 410
    label "series"
  ]
  node [
    id 411
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 412
    label "uprawianie"
  ]
  node [
    id 413
    label "praca_rolnicza"
  ]
  node [
    id 414
    label "collection"
  ]
  node [
    id 415
    label "dane"
  ]
  node [
    id 416
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 417
    label "pakiet_klimatyczny"
  ]
  node [
    id 418
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 419
    label "sum"
  ]
  node [
    id 420
    label "gathering"
  ]
  node [
    id 421
    label "album"
  ]
  node [
    id 422
    label "pos&#322;uchanie"
  ]
  node [
    id 423
    label "skumanie"
  ]
  node [
    id 424
    label "orientacja"
  ]
  node [
    id 425
    label "teoria"
  ]
  node [
    id 426
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 427
    label "clasp"
  ]
  node [
    id 428
    label "przem&#243;wienie"
  ]
  node [
    id 429
    label "forma"
  ]
  node [
    id 430
    label "zorientowanie"
  ]
  node [
    id 431
    label "mechanika"
  ]
  node [
    id 432
    label "konstrukcja"
  ]
  node [
    id 433
    label "system_komputerowy"
  ]
  node [
    id 434
    label "sprz&#281;t"
  ]
  node [
    id 435
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 436
    label "moczownik"
  ]
  node [
    id 437
    label "embryo"
  ]
  node [
    id 438
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 439
    label "zarodek"
  ]
  node [
    id 440
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 441
    label "latawiec"
  ]
  node [
    id 442
    label "reengineering"
  ]
  node [
    id 443
    label "program"
  ]
  node [
    id 444
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 445
    label "grupa_dyskusyjna"
  ]
  node [
    id 446
    label "doctrine"
  ]
  node [
    id 447
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 448
    label "kr&#281;gowiec"
  ]
  node [
    id 449
    label "doniczkowiec"
  ]
  node [
    id 450
    label "mi&#281;so"
  ]
  node [
    id 451
    label "patroszy&#263;"
  ]
  node [
    id 452
    label "rakowato&#347;&#263;"
  ]
  node [
    id 453
    label "ryby"
  ]
  node [
    id 454
    label "fish"
  ]
  node [
    id 455
    label "linia_boczna"
  ]
  node [
    id 456
    label "tar&#322;o"
  ]
  node [
    id 457
    label "wyrostek_filtracyjny"
  ]
  node [
    id 458
    label "m&#281;tnooki"
  ]
  node [
    id 459
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 460
    label "pokrywa_skrzelowa"
  ]
  node [
    id 461
    label "ikra"
  ]
  node [
    id 462
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 463
    label "szczelina_skrzelowa"
  ]
  node [
    id 464
    label "sport"
  ]
  node [
    id 465
    label "urozmaicenie"
  ]
  node [
    id 466
    label "pu&#322;apka"
  ]
  node [
    id 467
    label "pon&#281;ta"
  ]
  node [
    id 468
    label "wabik"
  ]
  node [
    id 469
    label "blat"
  ]
  node [
    id 470
    label "interfejs"
  ]
  node [
    id 471
    label "okno"
  ]
  node [
    id 472
    label "obszar"
  ]
  node [
    id 473
    label "ikona"
  ]
  node [
    id 474
    label "system_operacyjny"
  ]
  node [
    id 475
    label "mebel"
  ]
  node [
    id 476
    label "zdolno&#347;&#263;"
  ]
  node [
    id 477
    label "oswobodzi&#263;"
  ]
  node [
    id 478
    label "os&#322;abi&#263;"
  ]
  node [
    id 479
    label "disengage"
  ]
  node [
    id 480
    label "zdezorganizowa&#263;"
  ]
  node [
    id 481
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 482
    label "reakcja"
  ]
  node [
    id 483
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 484
    label "tajemnica"
  ]
  node [
    id 485
    label "wydarzenie"
  ]
  node [
    id 486
    label "pochowanie"
  ]
  node [
    id 487
    label "zdyscyplinowanie"
  ]
  node [
    id 488
    label "post&#261;pienie"
  ]
  node [
    id 489
    label "post"
  ]
  node [
    id 490
    label "bearing"
  ]
  node [
    id 491
    label "zwierz&#281;"
  ]
  node [
    id 492
    label "behawior"
  ]
  node [
    id 493
    label "observation"
  ]
  node [
    id 494
    label "dieta"
  ]
  node [
    id 495
    label "podtrzymanie"
  ]
  node [
    id 496
    label "etolog"
  ]
  node [
    id 497
    label "przechowanie"
  ]
  node [
    id 498
    label "zrobienie"
  ]
  node [
    id 499
    label "relaxation"
  ]
  node [
    id 500
    label "os&#322;abienie"
  ]
  node [
    id 501
    label "oswobodzenie"
  ]
  node [
    id 502
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 503
    label "zdezorganizowanie"
  ]
  node [
    id 504
    label "naukowiec"
  ]
  node [
    id 505
    label "provider"
  ]
  node [
    id 506
    label "b&#322;&#261;d"
  ]
  node [
    id 507
    label "hipertekst"
  ]
  node [
    id 508
    label "cyberprzestrze&#324;"
  ]
  node [
    id 509
    label "mem"
  ]
  node [
    id 510
    label "grooming"
  ]
  node [
    id 511
    label "gra_sieciowa"
  ]
  node [
    id 512
    label "media"
  ]
  node [
    id 513
    label "biznes_elektroniczny"
  ]
  node [
    id 514
    label "sie&#263;_komputerowa"
  ]
  node [
    id 515
    label "punkt_dost&#281;pu"
  ]
  node [
    id 516
    label "us&#322;uga_internetowa"
  ]
  node [
    id 517
    label "netbook"
  ]
  node [
    id 518
    label "e-hazard"
  ]
  node [
    id 519
    label "podcast"
  ]
  node [
    id 520
    label "strona"
  ]
  node [
    id 521
    label "prezenter"
  ]
  node [
    id 522
    label "mildew"
  ]
  node [
    id 523
    label "zi&#243;&#322;ko"
  ]
  node [
    id 524
    label "motif"
  ]
  node [
    id 525
    label "pozowanie"
  ]
  node [
    id 526
    label "ideal"
  ]
  node [
    id 527
    label "wz&#243;r"
  ]
  node [
    id 528
    label "matryca"
  ]
  node [
    id 529
    label "adaptation"
  ]
  node [
    id 530
    label "ruch"
  ]
  node [
    id 531
    label "pozowa&#263;"
  ]
  node [
    id 532
    label "imitacja"
  ]
  node [
    id 533
    label "orygina&#322;"
  ]
  node [
    id 534
    label "facet"
  ]
  node [
    id 535
    label "miniatura"
  ]
  node [
    id 536
    label "zesp&#243;&#322;"
  ]
  node [
    id 537
    label "podejrzany"
  ]
  node [
    id 538
    label "s&#261;downictwo"
  ]
  node [
    id 539
    label "biuro"
  ]
  node [
    id 540
    label "court"
  ]
  node [
    id 541
    label "forum"
  ]
  node [
    id 542
    label "bronienie"
  ]
  node [
    id 543
    label "urz&#261;d"
  ]
  node [
    id 544
    label "oskar&#380;yciel"
  ]
  node [
    id 545
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 546
    label "skazany"
  ]
  node [
    id 547
    label "post&#281;powanie"
  ]
  node [
    id 548
    label "broni&#263;"
  ]
  node [
    id 549
    label "my&#347;l"
  ]
  node [
    id 550
    label "pods&#261;dny"
  ]
  node [
    id 551
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 552
    label "obrona"
  ]
  node [
    id 553
    label "wypowied&#378;"
  ]
  node [
    id 554
    label "instytucja"
  ]
  node [
    id 555
    label "antylogizm"
  ]
  node [
    id 556
    label "konektyw"
  ]
  node [
    id 557
    label "&#347;wiadek"
  ]
  node [
    id 558
    label "procesowicz"
  ]
  node [
    id 559
    label "lias"
  ]
  node [
    id 560
    label "dzia&#322;"
  ]
  node [
    id 561
    label "jednostka"
  ]
  node [
    id 562
    label "pi&#281;tro"
  ]
  node [
    id 563
    label "klasa"
  ]
  node [
    id 564
    label "filia"
  ]
  node [
    id 565
    label "malm"
  ]
  node [
    id 566
    label "whole"
  ]
  node [
    id 567
    label "dogger"
  ]
  node [
    id 568
    label "poziom"
  ]
  node [
    id 569
    label "promocja"
  ]
  node [
    id 570
    label "kurs"
  ]
  node [
    id 571
    label "bank"
  ]
  node [
    id 572
    label "formacja"
  ]
  node [
    id 573
    label "ajencja"
  ]
  node [
    id 574
    label "wojsko"
  ]
  node [
    id 575
    label "siedziba"
  ]
  node [
    id 576
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 577
    label "agencja"
  ]
  node [
    id 578
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 579
    label "szpital"
  ]
  node [
    id 580
    label "algebra_liniowa"
  ]
  node [
    id 581
    label "macierz_j&#261;drowa"
  ]
  node [
    id 582
    label "atom"
  ]
  node [
    id 583
    label "nukleon"
  ]
  node [
    id 584
    label "kariokineza"
  ]
  node [
    id 585
    label "core"
  ]
  node [
    id 586
    label "chemia_j&#261;drowa"
  ]
  node [
    id 587
    label "anorchizm"
  ]
  node [
    id 588
    label "os&#322;onka_pochwowa_j&#261;dra"
  ]
  node [
    id 589
    label "nasieniak"
  ]
  node [
    id 590
    label "wn&#281;trostwo"
  ]
  node [
    id 591
    label "ziarno"
  ]
  node [
    id 592
    label "kom&#243;rka_Sertolego"
  ]
  node [
    id 593
    label "j&#261;derko"
  ]
  node [
    id 594
    label "kom&#243;rka_Leydiga"
  ]
  node [
    id 595
    label "jajo"
  ]
  node [
    id 596
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 597
    label "chromosom"
  ]
  node [
    id 598
    label "organellum"
  ]
  node [
    id 599
    label "moszna"
  ]
  node [
    id 600
    label "przeciwobraz"
  ]
  node [
    id 601
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 602
    label "&#347;rodek"
  ]
  node [
    id 603
    label "protoplazma"
  ]
  node [
    id 604
    label "znaczenie"
  ]
  node [
    id 605
    label "gruczo&#322;_rozrodczy"
  ]
  node [
    id 606
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 607
    label "nukleosynteza"
  ]
  node [
    id 608
    label "subsystem"
  ]
  node [
    id 609
    label "ko&#322;o"
  ]
  node [
    id 610
    label "granica"
  ]
  node [
    id 611
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 612
    label "suport"
  ]
  node [
    id 613
    label "prosta"
  ]
  node [
    id 614
    label "o&#347;rodek"
  ]
  node [
    id 615
    label "eonotem"
  ]
  node [
    id 616
    label "constellation"
  ]
  node [
    id 617
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 618
    label "Ptak_Rajski"
  ]
  node [
    id 619
    label "W&#281;&#380;ownik"
  ]
  node [
    id 620
    label "Panna"
  ]
  node [
    id 621
    label "W&#261;&#380;"
  ]
  node [
    id 622
    label "blokada"
  ]
  node [
    id 623
    label "hurtownia"
  ]
  node [
    id 624
    label "pomieszczenie"
  ]
  node [
    id 625
    label "pas"
  ]
  node [
    id 626
    label "miejsce"
  ]
  node [
    id 627
    label "basic"
  ]
  node [
    id 628
    label "sk&#322;adnik"
  ]
  node [
    id 629
    label "sklep"
  ]
  node [
    id 630
    label "obr&#243;bka"
  ]
  node [
    id 631
    label "constitution"
  ]
  node [
    id 632
    label "&#347;wiat&#322;o"
  ]
  node [
    id 633
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 634
    label "syf"
  ]
  node [
    id 635
    label "rank_and_file"
  ]
  node [
    id 636
    label "tabulacja"
  ]
  node [
    id 637
    label "tekst"
  ]
  node [
    id 638
    label "Pancyr"
  ]
  node [
    id 639
    label "S1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 638
    target 639
  ]
]
