graph [
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "doj&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "szklany"
    origin "text"
  ]
  node [
    id 3
    label "sufit"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 6
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 7
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 8
    label "przebi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "sta&#263;_si&#281;"
  ]
  node [
    id 10
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 11
    label "supervene"
  ]
  node [
    id 12
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 13
    label "zaj&#347;&#263;"
  ]
  node [
    id 14
    label "catch"
  ]
  node [
    id 15
    label "get"
  ]
  node [
    id 16
    label "bodziec"
  ]
  node [
    id 17
    label "informacja"
  ]
  node [
    id 18
    label "przesy&#322;ka"
  ]
  node [
    id 19
    label "dodatek"
  ]
  node [
    id 20
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 21
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 22
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 23
    label "heed"
  ]
  node [
    id 24
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 25
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 26
    label "spowodowa&#263;"
  ]
  node [
    id 27
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 28
    label "dozna&#263;"
  ]
  node [
    id 29
    label "dokoptowa&#263;"
  ]
  node [
    id 30
    label "postrzega&#263;"
  ]
  node [
    id 31
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 32
    label "orgazm"
  ]
  node [
    id 33
    label "dolecie&#263;"
  ]
  node [
    id 34
    label "drive"
  ]
  node [
    id 35
    label "dotrze&#263;"
  ]
  node [
    id 36
    label "uzyska&#263;"
  ]
  node [
    id 37
    label "dop&#322;ata"
  ]
  node [
    id 38
    label "become"
  ]
  node [
    id 39
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 40
    label "spotka&#263;"
  ]
  node [
    id 41
    label "range"
  ]
  node [
    id 42
    label "fall_upon"
  ]
  node [
    id 43
    label "profit"
  ]
  node [
    id 44
    label "score"
  ]
  node [
    id 45
    label "make"
  ]
  node [
    id 46
    label "utrze&#263;"
  ]
  node [
    id 47
    label "znale&#378;&#263;"
  ]
  node [
    id 48
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 49
    label "silnik"
  ]
  node [
    id 50
    label "dopasowa&#263;"
  ]
  node [
    id 51
    label "advance"
  ]
  node [
    id 52
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 53
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 54
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 55
    label "dorobi&#263;"
  ]
  node [
    id 56
    label "act"
  ]
  node [
    id 57
    label "realize"
  ]
  node [
    id 58
    label "promocja"
  ]
  node [
    id 59
    label "zrobi&#263;"
  ]
  node [
    id 60
    label "wytworzy&#263;"
  ]
  node [
    id 61
    label "give_birth"
  ]
  node [
    id 62
    label "feel"
  ]
  node [
    id 63
    label "punkt"
  ]
  node [
    id 64
    label "publikacja"
  ]
  node [
    id 65
    label "wiedza"
  ]
  node [
    id 66
    label "obiega&#263;"
  ]
  node [
    id 67
    label "powzi&#281;cie"
  ]
  node [
    id 68
    label "dane"
  ]
  node [
    id 69
    label "obiegni&#281;cie"
  ]
  node [
    id 70
    label "sygna&#322;"
  ]
  node [
    id 71
    label "obieganie"
  ]
  node [
    id 72
    label "powzi&#261;&#263;"
  ]
  node [
    id 73
    label "obiec"
  ]
  node [
    id 74
    label "doj&#347;cie"
  ]
  node [
    id 75
    label "pobudka"
  ]
  node [
    id 76
    label "ankus"
  ]
  node [
    id 77
    label "przedmiot"
  ]
  node [
    id 78
    label "czynnik"
  ]
  node [
    id 79
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 80
    label "przewodzi&#263;"
  ]
  node [
    id 81
    label "zach&#281;ta"
  ]
  node [
    id 82
    label "drift"
  ]
  node [
    id 83
    label "przewodzenie"
  ]
  node [
    id 84
    label "o&#347;cie&#324;"
  ]
  node [
    id 85
    label "perceive"
  ]
  node [
    id 86
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 87
    label "punkt_widzenia"
  ]
  node [
    id 88
    label "obacza&#263;"
  ]
  node [
    id 89
    label "widzie&#263;"
  ]
  node [
    id 90
    label "dochodzi&#263;"
  ]
  node [
    id 91
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 92
    label "notice"
  ]
  node [
    id 93
    label "os&#261;dza&#263;"
  ]
  node [
    id 94
    label "dochodzenie"
  ]
  node [
    id 95
    label "doch&#243;d"
  ]
  node [
    id 96
    label "dziennik"
  ]
  node [
    id 97
    label "element"
  ]
  node [
    id 98
    label "rzecz"
  ]
  node [
    id 99
    label "galanteria"
  ]
  node [
    id 100
    label "aneks"
  ]
  node [
    id 101
    label "akt_p&#322;ciowy"
  ]
  node [
    id 102
    label "posy&#322;ka"
  ]
  node [
    id 103
    label "nadawca"
  ]
  node [
    id 104
    label "adres"
  ]
  node [
    id 105
    label "jell"
  ]
  node [
    id 106
    label "przys&#322;oni&#263;"
  ]
  node [
    id 107
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 108
    label "zaistnie&#263;"
  ]
  node [
    id 109
    label "surprise"
  ]
  node [
    id 110
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 111
    label "ukry&#263;_si&#281;"
  ]
  node [
    id 112
    label "podej&#347;&#263;"
  ]
  node [
    id 113
    label "wpa&#347;&#263;"
  ]
  node [
    id 114
    label "pokry&#263;_si&#281;"
  ]
  node [
    id 115
    label "wsp&#243;&#322;wyst&#261;pi&#263;"
  ]
  node [
    id 116
    label "przesta&#263;"
  ]
  node [
    id 117
    label "dokooptowa&#263;"
  ]
  node [
    id 118
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 119
    label "przyp&#322;yn&#261;&#263;"
  ]
  node [
    id 120
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 121
    label "swimming"
  ]
  node [
    id 122
    label "flow"
  ]
  node [
    id 123
    label "pusty"
  ]
  node [
    id 124
    label "przezroczysty"
  ]
  node [
    id 125
    label "chorobliwy"
  ]
  node [
    id 126
    label "szkli&#347;cie"
  ]
  node [
    id 127
    label "oboj&#281;tny"
  ]
  node [
    id 128
    label "d&#378;wi&#281;czny"
  ]
  node [
    id 129
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 130
    label "prze&#378;roczy"
  ]
  node [
    id 131
    label "przezroczy&#347;cie"
  ]
  node [
    id 132
    label "blady"
  ]
  node [
    id 133
    label "klarowanie"
  ]
  node [
    id 134
    label "klarowanie_si&#281;"
  ]
  node [
    id 135
    label "sklarowanie"
  ]
  node [
    id 136
    label "czysty"
  ]
  node [
    id 137
    label "przezroczysto"
  ]
  node [
    id 138
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 139
    label "skrawy"
  ]
  node [
    id 140
    label "b&#322;yszcz&#261;co"
  ]
  node [
    id 141
    label "przepe&#322;niony"
  ]
  node [
    id 142
    label "wysychanie"
  ]
  node [
    id 143
    label "ja&#322;owy"
  ]
  node [
    id 144
    label "zoboj&#281;tnia&#322;y"
  ]
  node [
    id 145
    label "wyschni&#281;cie"
  ]
  node [
    id 146
    label "opr&#243;&#380;nianie_si&#281;"
  ]
  node [
    id 147
    label "bezmy&#347;lny"
  ]
  node [
    id 148
    label "do_czysta"
  ]
  node [
    id 149
    label "pusto"
  ]
  node [
    id 150
    label "opr&#243;&#380;nienie_si&#281;"
  ]
  node [
    id 151
    label "g&#322;upi"
  ]
  node [
    id 152
    label "nijaki"
  ]
  node [
    id 153
    label "zoboj&#281;tnienie"
  ]
  node [
    id 154
    label "nieszkodliwy"
  ]
  node [
    id 155
    label "&#347;ni&#281;ty"
  ]
  node [
    id 156
    label "oboj&#281;tnie"
  ]
  node [
    id 157
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 158
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 159
    label "niewa&#380;ny"
  ]
  node [
    id 160
    label "neutralizowanie"
  ]
  node [
    id 161
    label "bierny"
  ]
  node [
    id 162
    label "zneutralizowanie"
  ]
  node [
    id 163
    label "neutralny"
  ]
  node [
    id 164
    label "straszny"
  ]
  node [
    id 165
    label "nadzwyczajny"
  ]
  node [
    id 166
    label "niepokoj&#261;cy"
  ]
  node [
    id 167
    label "chorobliwie"
  ]
  node [
    id 168
    label "nienormalny"
  ]
  node [
    id 169
    label "nieprawid&#322;owy"
  ]
  node [
    id 170
    label "d&#378;wi&#281;cznie"
  ]
  node [
    id 171
    label "efektowny"
  ]
  node [
    id 172
    label "pi&#281;kny"
  ]
  node [
    id 173
    label "klarowny"
  ]
  node [
    id 174
    label "nieprzytomnie"
  ]
  node [
    id 175
    label "nieruchomo"
  ]
  node [
    id 176
    label "szklisty"
  ]
  node [
    id 177
    label "transparently"
  ]
  node [
    id 178
    label "m&#281;tnie"
  ]
  node [
    id 179
    label "metalicznie"
  ]
  node [
    id 180
    label "kaseton"
  ]
  node [
    id 181
    label "p&#322;aszczyzna"
  ]
  node [
    id 182
    label "pomieszczenie"
  ]
  node [
    id 183
    label "wymiar"
  ]
  node [
    id 184
    label "&#347;ciana"
  ]
  node [
    id 185
    label "surface"
  ]
  node [
    id 186
    label "zakres"
  ]
  node [
    id 187
    label "kwadrant"
  ]
  node [
    id 188
    label "degree"
  ]
  node [
    id 189
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 190
    label "powierzchnia"
  ]
  node [
    id 191
    label "ukszta&#322;towanie"
  ]
  node [
    id 192
    label "cia&#322;o"
  ]
  node [
    id 193
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 194
    label "p&#322;aszczak"
  ]
  node [
    id 195
    label "sklepienie"
  ]
  node [
    id 196
    label "strop"
  ]
  node [
    id 197
    label "zdobienie"
  ]
  node [
    id 198
    label "amfilada"
  ]
  node [
    id 199
    label "front"
  ]
  node [
    id 200
    label "apartment"
  ]
  node [
    id 201
    label "pod&#322;oga"
  ]
  node [
    id 202
    label "udost&#281;pnienie"
  ]
  node [
    id 203
    label "miejsce"
  ]
  node [
    id 204
    label "umieszczenie"
  ]
  node [
    id 205
    label "zakamarek"
  ]
  node [
    id 206
    label "nijak"
  ]
  node [
    id 207
    label "niezabawny"
  ]
  node [
    id 208
    label "zwyczajny"
  ]
  node [
    id 209
    label "poszarzenie"
  ]
  node [
    id 210
    label "szarzenie"
  ]
  node [
    id 211
    label "bezbarwnie"
  ]
  node [
    id 212
    label "nieciekawy"
  ]
  node [
    id 213
    label "model"
  ]
  node [
    id 214
    label "narz&#281;dzie"
  ]
  node [
    id 215
    label "zbi&#243;r"
  ]
  node [
    id 216
    label "tryb"
  ]
  node [
    id 217
    label "nature"
  ]
  node [
    id 218
    label "egzemplarz"
  ]
  node [
    id 219
    label "series"
  ]
  node [
    id 220
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 221
    label "uprawianie"
  ]
  node [
    id 222
    label "praca_rolnicza"
  ]
  node [
    id 223
    label "collection"
  ]
  node [
    id 224
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 225
    label "pakiet_klimatyczny"
  ]
  node [
    id 226
    label "poj&#281;cie"
  ]
  node [
    id 227
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 228
    label "sum"
  ]
  node [
    id 229
    label "gathering"
  ]
  node [
    id 230
    label "album"
  ]
  node [
    id 231
    label "&#347;rodek"
  ]
  node [
    id 232
    label "niezb&#281;dnik"
  ]
  node [
    id 233
    label "cz&#322;owiek"
  ]
  node [
    id 234
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 235
    label "tylec"
  ]
  node [
    id 236
    label "urz&#261;dzenie"
  ]
  node [
    id 237
    label "ko&#322;o"
  ]
  node [
    id 238
    label "modalno&#347;&#263;"
  ]
  node [
    id 239
    label "z&#261;b"
  ]
  node [
    id 240
    label "cecha"
  ]
  node [
    id 241
    label "kategoria_gramatyczna"
  ]
  node [
    id 242
    label "skala"
  ]
  node [
    id 243
    label "funkcjonowa&#263;"
  ]
  node [
    id 244
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 245
    label "koniugacja"
  ]
  node [
    id 246
    label "prezenter"
  ]
  node [
    id 247
    label "typ"
  ]
  node [
    id 248
    label "mildew"
  ]
  node [
    id 249
    label "zi&#243;&#322;ko"
  ]
  node [
    id 250
    label "motif"
  ]
  node [
    id 251
    label "pozowanie"
  ]
  node [
    id 252
    label "ideal"
  ]
  node [
    id 253
    label "wz&#243;r"
  ]
  node [
    id 254
    label "matryca"
  ]
  node [
    id 255
    label "adaptation"
  ]
  node [
    id 256
    label "ruch"
  ]
  node [
    id 257
    label "pozowa&#263;"
  ]
  node [
    id 258
    label "imitacja"
  ]
  node [
    id 259
    label "orygina&#322;"
  ]
  node [
    id 260
    label "facet"
  ]
  node [
    id 261
    label "miniatura"
  ]
  node [
    id 262
    label "by&#263;"
  ]
  node [
    id 263
    label "gotowy"
  ]
  node [
    id 264
    label "might"
  ]
  node [
    id 265
    label "uprawi&#263;"
  ]
  node [
    id 266
    label "public_treasury"
  ]
  node [
    id 267
    label "pole"
  ]
  node [
    id 268
    label "obrobi&#263;"
  ]
  node [
    id 269
    label "nietrze&#378;wy"
  ]
  node [
    id 270
    label "czekanie"
  ]
  node [
    id 271
    label "martwy"
  ]
  node [
    id 272
    label "bliski"
  ]
  node [
    id 273
    label "gotowo"
  ]
  node [
    id 274
    label "przygotowywanie"
  ]
  node [
    id 275
    label "przygotowanie"
  ]
  node [
    id 276
    label "dyspozycyjny"
  ]
  node [
    id 277
    label "zalany"
  ]
  node [
    id 278
    label "nieuchronny"
  ]
  node [
    id 279
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 280
    label "mie&#263;_miejsce"
  ]
  node [
    id 281
    label "equal"
  ]
  node [
    id 282
    label "trwa&#263;"
  ]
  node [
    id 283
    label "chodzi&#263;"
  ]
  node [
    id 284
    label "si&#281;ga&#263;"
  ]
  node [
    id 285
    label "stan"
  ]
  node [
    id 286
    label "obecno&#347;&#263;"
  ]
  node [
    id 287
    label "stand"
  ]
  node [
    id 288
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 289
    label "uczestniczy&#263;"
  ]
  node [
    id 290
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 291
    label "zdeklasowa&#263;"
  ]
  node [
    id 292
    label "przeszy&#263;"
  ]
  node [
    id 293
    label "tear"
  ]
  node [
    id 294
    label "wybi&#263;"
  ]
  node [
    id 295
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 296
    label "przekrzycze&#263;"
  ]
  node [
    id 297
    label "przerzuci&#263;"
  ]
  node [
    id 298
    label "rozgromi&#263;"
  ]
  node [
    id 299
    label "zmieni&#263;"
  ]
  node [
    id 300
    label "dopowiedzie&#263;"
  ]
  node [
    id 301
    label "doj&#261;&#263;"
  ]
  node [
    id 302
    label "zbi&#263;"
  ]
  node [
    id 303
    label "zarysowa&#263;_si&#281;"
  ]
  node [
    id 304
    label "stick"
  ]
  node [
    id 305
    label "zaproponowa&#263;"
  ]
  node [
    id 306
    label "pokona&#263;"
  ]
  node [
    id 307
    label "zm&#261;ci&#263;"
  ]
  node [
    id 308
    label "przenikn&#261;&#263;"
  ]
  node [
    id 309
    label "beat"
  ]
  node [
    id 310
    label "przedziurawi&#263;"
  ]
  node [
    id 311
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 312
    label "tug"
  ]
  node [
    id 313
    label "przewierci&#263;"
  ]
  node [
    id 314
    label "strickle"
  ]
  node [
    id 315
    label "broach"
  ]
  node [
    id 316
    label "sprawi&#263;"
  ]
  node [
    id 317
    label "popsu&#263;"
  ]
  node [
    id 318
    label "pomiesza&#263;"
  ]
  node [
    id 319
    label "wzburzy&#263;"
  ]
  node [
    id 320
    label "clutter"
  ]
  node [
    id 321
    label "naruszy&#263;"
  ]
  node [
    id 322
    label "zak&#322;ama&#263;"
  ]
  node [
    id 323
    label "interrupt"
  ]
  node [
    id 324
    label "namiesza&#263;"
  ]
  node [
    id 325
    label "zszy&#263;"
  ]
  node [
    id 326
    label "przej&#347;&#263;"
  ]
  node [
    id 327
    label "przerobi&#263;"
  ]
  node [
    id 328
    label "spike"
  ]
  node [
    id 329
    label "przelecie&#263;"
  ]
  node [
    id 330
    label "fascinate"
  ]
  node [
    id 331
    label "blast"
  ]
  node [
    id 332
    label "pull"
  ]
  node [
    id 333
    label "strike"
  ]
  node [
    id 334
    label "sypn&#261;&#263;_si&#281;"
  ]
  node [
    id 335
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 336
    label "usun&#261;&#263;"
  ]
  node [
    id 337
    label "obi&#263;"
  ]
  node [
    id 338
    label "nast&#261;pi&#263;"
  ]
  node [
    id 339
    label "przegoni&#263;"
  ]
  node [
    id 340
    label "pozbija&#263;"
  ]
  node [
    id 341
    label "thrash"
  ]
  node [
    id 342
    label "wyperswadowa&#263;"
  ]
  node [
    id 343
    label "uszkodzi&#263;"
  ]
  node [
    id 344
    label "crush"
  ]
  node [
    id 345
    label "wywy&#380;szy&#263;"
  ]
  node [
    id 346
    label "zabi&#263;"
  ]
  node [
    id 347
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 348
    label "po&#322;ama&#263;"
  ]
  node [
    id 349
    label "wystuka&#263;"
  ]
  node [
    id 350
    label "wskaza&#263;"
  ]
  node [
    id 351
    label "pozabija&#263;"
  ]
  node [
    id 352
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 353
    label "zagra&#263;"
  ]
  node [
    id 354
    label "transgress"
  ]
  node [
    id 355
    label "precipitate"
  ]
  node [
    id 356
    label "wyt&#322;oczy&#263;"
  ]
  node [
    id 357
    label "nabi&#263;"
  ]
  node [
    id 358
    label "&#347;cisn&#261;&#263;"
  ]
  node [
    id 359
    label "wy&#322;oi&#263;"
  ]
  node [
    id 360
    label "wyrobi&#263;"
  ]
  node [
    id 361
    label "obni&#380;y&#263;"
  ]
  node [
    id 362
    label "ubi&#263;"
  ]
  node [
    id 363
    label "&#347;cie&#347;ni&#263;"
  ]
  node [
    id 364
    label "str&#261;ci&#263;"
  ]
  node [
    id 365
    label "zebra&#263;"
  ]
  node [
    id 366
    label "break"
  ]
  node [
    id 367
    label "obali&#263;"
  ]
  node [
    id 368
    label "zgromadzi&#263;"
  ]
  node [
    id 369
    label "pobi&#263;"
  ]
  node [
    id 370
    label "sku&#263;"
  ]
  node [
    id 371
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 372
    label "rozbi&#263;"
  ]
  node [
    id 373
    label "change"
  ]
  node [
    id 374
    label "zast&#261;pi&#263;"
  ]
  node [
    id 375
    label "come_up"
  ]
  node [
    id 376
    label "straci&#263;"
  ]
  node [
    id 377
    label "zyska&#263;"
  ]
  node [
    id 378
    label "zach&#281;ci&#263;"
  ]
  node [
    id 379
    label "volunteer"
  ]
  node [
    id 380
    label "poinformowa&#263;"
  ]
  node [
    id 381
    label "kandydatura"
  ]
  node [
    id 382
    label "announce"
  ]
  node [
    id 383
    label "indicate"
  ]
  node [
    id 384
    label "podra&#380;ni&#263;"
  ]
  node [
    id 385
    label "wywierci&#263;"
  ]
  node [
    id 386
    label "bang"
  ]
  node [
    id 387
    label "ogarn&#261;&#263;"
  ]
  node [
    id 388
    label "zdegradowa&#263;"
  ]
  node [
    id 389
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 390
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 391
    label "give"
  ]
  node [
    id 392
    label "przemyci&#263;"
  ]
  node [
    id 393
    label "throw"
  ]
  node [
    id 394
    label "przejrze&#263;"
  ]
  node [
    id 395
    label "bewilder"
  ]
  node [
    id 396
    label "zarzuci&#263;"
  ]
  node [
    id 397
    label "przeszuka&#263;"
  ]
  node [
    id 398
    label "przesun&#261;&#263;"
  ]
  node [
    id 399
    label "poradzi&#263;_sobie"
  ]
  node [
    id 400
    label "zapobiec"
  ]
  node [
    id 401
    label "z&#322;oi&#263;"
  ]
  node [
    id 402
    label "zaatakowa&#263;"
  ]
  node [
    id 403
    label "overwhelm"
  ]
  node [
    id 404
    label "wygra&#263;"
  ]
  node [
    id 405
    label "manipulate"
  ]
  node [
    id 406
    label "z&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 407
    label "erupt"
  ]
  node [
    id 408
    label "absorb"
  ]
  node [
    id 409
    label "wej&#347;&#263;"
  ]
  node [
    id 410
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 411
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 412
    label "infiltrate"
  ]
  node [
    id 413
    label "zag&#322;uszy&#263;"
  ]
  node [
    id 414
    label "set"
  ]
  node [
    id 415
    label "zawt&#243;rowa&#263;"
  ]
  node [
    id 416
    label "doda&#263;"
  ]
  node [
    id 417
    label "ujawni&#263;"
  ]
  node [
    id 418
    label "powiedzie&#263;"
  ]
  node [
    id 419
    label "rytm"
  ]
  node [
    id 420
    label "Justyn"
  ]
  node [
    id 421
    label "Krowicka"
  ]
  node [
    id 422
    label "Gemini"
  ]
  node [
    id 423
    label "w"
  ]
  node [
    id 424
    label "Ostrawie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 420
    target 421
  ]
  edge [
    source 422
    target 423
  ]
  edge [
    source 422
    target 424
  ]
  edge [
    source 423
    target 424
  ]
]
