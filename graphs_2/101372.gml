graph [
  node [
    id 0
    label "retencja"
    origin "text"
  ]
  node [
    id 1
    label "wodny"
    origin "text"
  ]
  node [
    id 2
    label "retentiveness"
  ]
  node [
    id 3
    label "wierzyciel"
  ]
  node [
    id 4
    label "retention"
  ]
  node [
    id 5
    label "zapas"
  ]
  node [
    id 6
    label "prawo"
  ]
  node [
    id 7
    label "substytut"
  ]
  node [
    id 8
    label "nadwy&#380;ka"
  ]
  node [
    id 9
    label "zas&#243;b"
  ]
  node [
    id 10
    label "stock"
  ]
  node [
    id 11
    label "zapasy"
  ]
  node [
    id 12
    label "resource"
  ]
  node [
    id 13
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 14
    label "umocowa&#263;"
  ]
  node [
    id 15
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 16
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 17
    label "procesualistyka"
  ]
  node [
    id 18
    label "regu&#322;a_Allena"
  ]
  node [
    id 19
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 20
    label "kryminalistyka"
  ]
  node [
    id 21
    label "struktura"
  ]
  node [
    id 22
    label "szko&#322;a"
  ]
  node [
    id 23
    label "kierunek"
  ]
  node [
    id 24
    label "zasada_d'Alemberta"
  ]
  node [
    id 25
    label "obserwacja"
  ]
  node [
    id 26
    label "normatywizm"
  ]
  node [
    id 27
    label "jurisprudence"
  ]
  node [
    id 28
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 29
    label "kultura_duchowa"
  ]
  node [
    id 30
    label "przepis"
  ]
  node [
    id 31
    label "prawo_karne_procesowe"
  ]
  node [
    id 32
    label "criterion"
  ]
  node [
    id 33
    label "kazuistyka"
  ]
  node [
    id 34
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 35
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 36
    label "kryminologia"
  ]
  node [
    id 37
    label "opis"
  ]
  node [
    id 38
    label "regu&#322;a_Glogera"
  ]
  node [
    id 39
    label "prawo_Mendla"
  ]
  node [
    id 40
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 41
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 42
    label "prawo_karne"
  ]
  node [
    id 43
    label "legislacyjnie"
  ]
  node [
    id 44
    label "twierdzenie"
  ]
  node [
    id 45
    label "cywilistyka"
  ]
  node [
    id 46
    label "judykatura"
  ]
  node [
    id 47
    label "kanonistyka"
  ]
  node [
    id 48
    label "standard"
  ]
  node [
    id 49
    label "nauka_prawa"
  ]
  node [
    id 50
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 51
    label "podmiot"
  ]
  node [
    id 52
    label "law"
  ]
  node [
    id 53
    label "qualification"
  ]
  node [
    id 54
    label "dominion"
  ]
  node [
    id 55
    label "wykonawczy"
  ]
  node [
    id 56
    label "zasada"
  ]
  node [
    id 57
    label "normalizacja"
  ]
  node [
    id 58
    label "specjalny"
  ]
  node [
    id 59
    label "intencjonalny"
  ]
  node [
    id 60
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 61
    label "niedorozw&#243;j"
  ]
  node [
    id 62
    label "szczeg&#243;lny"
  ]
  node [
    id 63
    label "specjalnie"
  ]
  node [
    id 64
    label "nieetatowy"
  ]
  node [
    id 65
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 66
    label "nienormalny"
  ]
  node [
    id 67
    label "umy&#347;lnie"
  ]
  node [
    id 68
    label "odpowiedni"
  ]
  node [
    id 69
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
]
