graph [
  node [
    id 0
    label "kolejny"
    origin "text"
  ]
  node [
    id 1
    label "szeryf"
    origin "text"
  ]
  node [
    id 2
    label "nast&#281;pnie"
  ]
  node [
    id 3
    label "inny"
  ]
  node [
    id 4
    label "nastopny"
  ]
  node [
    id 5
    label "kolejno"
  ]
  node [
    id 6
    label "kt&#243;ry&#347;"
  ]
  node [
    id 7
    label "osobno"
  ]
  node [
    id 8
    label "r&#243;&#380;ny"
  ]
  node [
    id 9
    label "inszy"
  ]
  node [
    id 10
    label "inaczej"
  ]
  node [
    id 11
    label "urz&#281;dnik"
  ]
  node [
    id 12
    label "tytu&#322;"
  ]
  node [
    id 13
    label "osobisto&#347;&#263;"
  ]
  node [
    id 14
    label "czcionka"
  ]
  node [
    id 15
    label "str&#243;&#380;"
  ]
  node [
    id 16
    label "odznaczenie"
  ]
  node [
    id 17
    label "&#347;redniowieczny"
  ]
  node [
    id 18
    label "pracownik"
  ]
  node [
    id 19
    label "korpus_urz&#281;dniczy"
  ]
  node [
    id 20
    label "pragmatyka"
  ]
  node [
    id 21
    label "kto&#347;"
  ]
  node [
    id 22
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 23
    label "odznaczanie"
  ]
  node [
    id 24
    label "nagroda"
  ]
  node [
    id 25
    label "nagrodzenie"
  ]
  node [
    id 26
    label "note"
  ]
  node [
    id 27
    label "order"
  ]
  node [
    id 28
    label "award"
  ]
  node [
    id 29
    label "debit"
  ]
  node [
    id 30
    label "redaktor"
  ]
  node [
    id 31
    label "druk"
  ]
  node [
    id 32
    label "publikacja"
  ]
  node [
    id 33
    label "nadtytu&#322;"
  ]
  node [
    id 34
    label "szata_graficzna"
  ]
  node [
    id 35
    label "tytulatura"
  ]
  node [
    id 36
    label "wydawa&#263;"
  ]
  node [
    id 37
    label "elevation"
  ]
  node [
    id 38
    label "wyda&#263;"
  ]
  node [
    id 39
    label "mianowaniec"
  ]
  node [
    id 40
    label "poster"
  ]
  node [
    id 41
    label "nazwa"
  ]
  node [
    id 42
    label "podtytu&#322;"
  ]
  node [
    id 43
    label "stra&#380;nik"
  ]
  node [
    id 44
    label "opiekun"
  ]
  node [
    id 45
    label "obro&#324;ca"
  ]
  node [
    id 46
    label "anio&#322;"
  ]
  node [
    id 47
    label "trabant"
  ]
  node [
    id 48
    label "dawny"
  ]
  node [
    id 49
    label "histrion"
  ]
  node [
    id 50
    label "saltarello"
  ]
  node [
    id 51
    label "ban"
  ]
  node [
    id 52
    label "kantylena"
  ]
  node [
    id 53
    label "anachroniczny"
  ]
  node [
    id 54
    label "zacofany"
  ]
  node [
    id 55
    label "margrabia"
  ]
  node [
    id 56
    label "artes_liberales"
  ]
  node [
    id 57
    label "&#380;ak"
  ]
  node [
    id 58
    label "stary"
  ]
  node [
    id 59
    label "piszczek"
  ]
  node [
    id 60
    label "oczko"
  ]
  node [
    id 61
    label "pismo"
  ]
  node [
    id 62
    label "glif"
  ]
  node [
    id 63
    label "prostopad&#322;o&#347;cian"
  ]
  node [
    id 64
    label "kr&#243;bka"
  ]
  node [
    id 65
    label "odlewacz"
  ]
  node [
    id 66
    label "zdobnik"
  ]
  node [
    id 67
    label "prasa_drukarska"
  ]
  node [
    id 68
    label "character"
  ]
  node [
    id 69
    label "kaszta"
  ]
  node [
    id 70
    label "no&#347;nik"
  ]
  node [
    id 71
    label "zjazd"
  ]
  node [
    id 72
    label "A1"
  ]
  node [
    id 73
    label "Wieszowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 73
  ]
  edge [
    source 72
    target 73
  ]
]
