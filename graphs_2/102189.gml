graph [
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "s&#281;dzia"
    origin "text"
  ]
  node [
    id 2
    label "opami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "wasze&#263;"
    origin "text"
  ]
  node [
    id 5
    label "coca"
    origin "text"
  ]
  node [
    id 6
    label "&#380;e&#263;"
    origin "text"
  ]
  node [
    id 7
    label "obsypa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "z&#322;oto"
    origin "text"
  ]
  node [
    id 9
    label "jak"
    origin "text"
  ]
  node [
    id 10
    label "wilia"
    origin "text"
  ]
  node [
    id 11
    label "szczupak"
    origin "text"
  ]
  node [
    id 12
    label "szafran"
    origin "text"
  ]
  node [
    id 13
    label "czy&#380;"
    origin "text"
  ]
  node [
    id 14
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 15
    label "aby"
    origin "text"
  ]
  node [
    id 16
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 17
    label "hetman"
    origin "text"
  ]
  node [
    id 18
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 19
    label "chrze&#347;cija&#324;stwo"
    origin "text"
  ]
  node [
    id 20
    label "taca"
    origin "text"
  ]
  node [
    id 21
    label "tam"
    origin "text"
  ]
  node [
    id 22
    label "gdzie"
    origin "text"
  ]
  node [
    id 23
    label "mak&#243;wka"
    origin "text"
  ]
  node [
    id 24
    label "strzela&#263;"
    origin "text"
  ]
  node [
    id 25
    label "i&#347;&#263;"
    origin "text"
  ]
  node [
    id 26
    label "kto"
    origin "text"
  ]
  node [
    id 27
    label "potrza"
    origin "text"
  ]
  node [
    id 28
    label "to&#263;"
    origin "text"
  ]
  node [
    id 29
    label "tak"
    origin "text"
  ]
  node [
    id 30
    label "b&#322;yszcz&#261;cy"
    origin "text"
  ]
  node [
    id 31
    label "zobaczy&#263;"
    origin "text"
  ]
  node [
    id 32
    label "przebra&#263;"
    origin "text"
  ]
  node [
    id 33
    label "bracia"
    origin "text"
  ]
  node [
    id 34
    label "uczy&#263;"
    origin "text"
  ]
  node [
    id 35
    label "cudze"
    origin "text"
  ]
  node [
    id 36
    label "ku&#263;"
    origin "text"
  ]
  node [
    id 37
    label "koga"
    origin "text"
  ]
  node [
    id 38
    label "najpierwej"
    origin "text"
  ]
  node [
    id 39
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 40
    label "maja"
    origin "text"
  ]
  node [
    id 41
    label "murza"
  ]
  node [
    id 42
    label "belfer"
  ]
  node [
    id 43
    label "szkolnik"
  ]
  node [
    id 44
    label "pupil"
  ]
  node [
    id 45
    label "ojciec"
  ]
  node [
    id 46
    label "kszta&#322;ciciel"
  ]
  node [
    id 47
    label "Midas"
  ]
  node [
    id 48
    label "przyw&#243;dca"
  ]
  node [
    id 49
    label "opiekun"
  ]
  node [
    id 50
    label "Mieszko_I"
  ]
  node [
    id 51
    label "doros&#322;y"
  ]
  node [
    id 52
    label "pracodawca"
  ]
  node [
    id 53
    label "profesor"
  ]
  node [
    id 54
    label "m&#261;&#380;"
  ]
  node [
    id 55
    label "rz&#261;dzenie"
  ]
  node [
    id 56
    label "bogaty"
  ]
  node [
    id 57
    label "cz&#322;owiek"
  ]
  node [
    id 58
    label "pa&#324;stwo"
  ]
  node [
    id 59
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 60
    label "w&#322;odarz"
  ]
  node [
    id 61
    label "nabab"
  ]
  node [
    id 62
    label "samiec"
  ]
  node [
    id 63
    label "preceptor"
  ]
  node [
    id 64
    label "pedagog"
  ]
  node [
    id 65
    label "efendi"
  ]
  node [
    id 66
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 67
    label "popularyzator"
  ]
  node [
    id 68
    label "gra_w_karty"
  ]
  node [
    id 69
    label "zwrot"
  ]
  node [
    id 70
    label "jegomo&#347;&#263;"
  ]
  node [
    id 71
    label "androlog"
  ]
  node [
    id 72
    label "bratek"
  ]
  node [
    id 73
    label "andropauza"
  ]
  node [
    id 74
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 75
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 76
    label "ch&#322;opina"
  ]
  node [
    id 77
    label "w&#322;adza"
  ]
  node [
    id 78
    label "Sabataj_Cwi"
  ]
  node [
    id 79
    label "lider"
  ]
  node [
    id 80
    label "Mao"
  ]
  node [
    id 81
    label "Anders"
  ]
  node [
    id 82
    label "Fidel_Castro"
  ]
  node [
    id 83
    label "Miko&#322;ajczyk"
  ]
  node [
    id 84
    label "Tito"
  ]
  node [
    id 85
    label "Ko&#347;ciuszko"
  ]
  node [
    id 86
    label "p&#322;atnik"
  ]
  node [
    id 87
    label "zwierzchnik"
  ]
  node [
    id 88
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 89
    label "nadzorca"
  ]
  node [
    id 90
    label "funkcjonariusz"
  ]
  node [
    id 91
    label "podmiot"
  ]
  node [
    id 92
    label "wykupywanie"
  ]
  node [
    id 93
    label "wykupienie"
  ]
  node [
    id 94
    label "bycie_w_posiadaniu"
  ]
  node [
    id 95
    label "rozszerzyciel"
  ]
  node [
    id 96
    label "asymilowa&#263;"
  ]
  node [
    id 97
    label "nasada"
  ]
  node [
    id 98
    label "profanum"
  ]
  node [
    id 99
    label "wz&#243;r"
  ]
  node [
    id 100
    label "senior"
  ]
  node [
    id 101
    label "asymilowanie"
  ]
  node [
    id 102
    label "os&#322;abia&#263;"
  ]
  node [
    id 103
    label "homo_sapiens"
  ]
  node [
    id 104
    label "osoba"
  ]
  node [
    id 105
    label "ludzko&#347;&#263;"
  ]
  node [
    id 106
    label "Adam"
  ]
  node [
    id 107
    label "hominid"
  ]
  node [
    id 108
    label "posta&#263;"
  ]
  node [
    id 109
    label "portrecista"
  ]
  node [
    id 110
    label "polifag"
  ]
  node [
    id 111
    label "podw&#322;adny"
  ]
  node [
    id 112
    label "dwun&#243;g"
  ]
  node [
    id 113
    label "wapniak"
  ]
  node [
    id 114
    label "duch"
  ]
  node [
    id 115
    label "os&#322;abianie"
  ]
  node [
    id 116
    label "antropochoria"
  ]
  node [
    id 117
    label "figura"
  ]
  node [
    id 118
    label "g&#322;owa"
  ]
  node [
    id 119
    label "mikrokosmos"
  ]
  node [
    id 120
    label "oddzia&#322;ywanie"
  ]
  node [
    id 121
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 122
    label "du&#380;y"
  ]
  node [
    id 123
    label "dojrza&#322;y"
  ]
  node [
    id 124
    label "dojrzale"
  ]
  node [
    id 125
    label "wydoro&#347;lenie"
  ]
  node [
    id 126
    label "doro&#347;lenie"
  ]
  node [
    id 127
    label "m&#261;dry"
  ]
  node [
    id 128
    label "&#378;ra&#322;y"
  ]
  node [
    id 129
    label "doletni"
  ]
  node [
    id 130
    label "doro&#347;le"
  ]
  node [
    id 131
    label "punkt"
  ]
  node [
    id 132
    label "zmiana"
  ]
  node [
    id 133
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 134
    label "turn"
  ]
  node [
    id 135
    label "wyra&#380;enie"
  ]
  node [
    id 136
    label "fraza_czasownikowa"
  ]
  node [
    id 137
    label "turning"
  ]
  node [
    id 138
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 139
    label "skr&#281;t"
  ]
  node [
    id 140
    label "jednostka_leksykalna"
  ]
  node [
    id 141
    label "obr&#243;t"
  ]
  node [
    id 142
    label "starosta"
  ]
  node [
    id 143
    label "w&#322;adca"
  ]
  node [
    id 144
    label "zarz&#261;dca"
  ]
  node [
    id 145
    label "nauczyciel_akademicki"
  ]
  node [
    id 146
    label "tytu&#322;"
  ]
  node [
    id 147
    label "stopie&#324;_naukowy"
  ]
  node [
    id 148
    label "konsulent"
  ]
  node [
    id 149
    label "profesura"
  ]
  node [
    id 150
    label "nauczyciel"
  ]
  node [
    id 151
    label "wirtuoz"
  ]
  node [
    id 152
    label "autor"
  ]
  node [
    id 153
    label "szko&#322;a"
  ]
  node [
    id 154
    label "tarcza"
  ]
  node [
    id 155
    label "klasa"
  ]
  node [
    id 156
    label "elew"
  ]
  node [
    id 157
    label "wyprawka"
  ]
  node [
    id 158
    label "mundurek"
  ]
  node [
    id 159
    label "absolwent"
  ]
  node [
    id 160
    label "ochotnik"
  ]
  node [
    id 161
    label "nauczyciel_muzyki"
  ]
  node [
    id 162
    label "pomocnik"
  ]
  node [
    id 163
    label "zakonnik"
  ]
  node [
    id 164
    label "student"
  ]
  node [
    id 165
    label "ekspert"
  ]
  node [
    id 166
    label "bogacz"
  ]
  node [
    id 167
    label "dostojnik"
  ]
  node [
    id 168
    label "urz&#281;dnik"
  ]
  node [
    id 169
    label "&#347;w"
  ]
  node [
    id 170
    label "rodzic"
  ]
  node [
    id 171
    label "pomys&#322;odawca"
  ]
  node [
    id 172
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 173
    label "rodzice"
  ]
  node [
    id 174
    label "wykonawca"
  ]
  node [
    id 175
    label "stary"
  ]
  node [
    id 176
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 177
    label "kuwada"
  ]
  node [
    id 178
    label "ojczym"
  ]
  node [
    id 179
    label "papa"
  ]
  node [
    id 180
    label "przodek"
  ]
  node [
    id 181
    label "tworzyciel"
  ]
  node [
    id 182
    label "facet"
  ]
  node [
    id 183
    label "kochanek"
  ]
  node [
    id 184
    label "fio&#322;ek"
  ]
  node [
    id 185
    label "brat"
  ]
  node [
    id 186
    label "zwierz&#281;"
  ]
  node [
    id 187
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 188
    label "pan_i_w&#322;adca"
  ]
  node [
    id 189
    label "pan_m&#322;ody"
  ]
  node [
    id 190
    label "ch&#322;op"
  ]
  node [
    id 191
    label "&#347;lubny"
  ]
  node [
    id 192
    label "m&#243;j"
  ]
  node [
    id 193
    label "pan_domu"
  ]
  node [
    id 194
    label "ma&#322;&#380;onek"
  ]
  node [
    id 195
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 196
    label "mo&#347;&#263;"
  ]
  node [
    id 197
    label "Frygia"
  ]
  node [
    id 198
    label "dominowanie"
  ]
  node [
    id 199
    label "reign"
  ]
  node [
    id 200
    label "sprawowanie"
  ]
  node [
    id 201
    label "dominion"
  ]
  node [
    id 202
    label "rule"
  ]
  node [
    id 203
    label "zwierz&#281;_domowe"
  ]
  node [
    id 204
    label "John_Dewey"
  ]
  node [
    id 205
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 206
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 207
    label "J&#281;drzejewicz"
  ]
  node [
    id 208
    label "specjalista"
  ]
  node [
    id 209
    label "&#380;ycie"
  ]
  node [
    id 210
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 211
    label "Turek"
  ]
  node [
    id 212
    label "effendi"
  ]
  node [
    id 213
    label "och&#281;do&#380;ny"
  ]
  node [
    id 214
    label "zapa&#347;ny"
  ]
  node [
    id 215
    label "sytuowany"
  ]
  node [
    id 216
    label "obfituj&#261;cy"
  ]
  node [
    id 217
    label "forsiasty"
  ]
  node [
    id 218
    label "spania&#322;y"
  ]
  node [
    id 219
    label "obficie"
  ]
  node [
    id 220
    label "r&#243;&#380;norodny"
  ]
  node [
    id 221
    label "bogato"
  ]
  node [
    id 222
    label "Japonia"
  ]
  node [
    id 223
    label "Zair"
  ]
  node [
    id 224
    label "Belize"
  ]
  node [
    id 225
    label "San_Marino"
  ]
  node [
    id 226
    label "Tanzania"
  ]
  node [
    id 227
    label "Antigua_i_Barbuda"
  ]
  node [
    id 228
    label "granica_pa&#324;stwa"
  ]
  node [
    id 229
    label "Senegal"
  ]
  node [
    id 230
    label "Indie"
  ]
  node [
    id 231
    label "Seszele"
  ]
  node [
    id 232
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 233
    label "Zimbabwe"
  ]
  node [
    id 234
    label "Filipiny"
  ]
  node [
    id 235
    label "Mauretania"
  ]
  node [
    id 236
    label "Malezja"
  ]
  node [
    id 237
    label "Rumunia"
  ]
  node [
    id 238
    label "Surinam"
  ]
  node [
    id 239
    label "Ukraina"
  ]
  node [
    id 240
    label "Syria"
  ]
  node [
    id 241
    label "Wyspy_Marshalla"
  ]
  node [
    id 242
    label "Burkina_Faso"
  ]
  node [
    id 243
    label "Grecja"
  ]
  node [
    id 244
    label "Polska"
  ]
  node [
    id 245
    label "Wenezuela"
  ]
  node [
    id 246
    label "Nepal"
  ]
  node [
    id 247
    label "Suazi"
  ]
  node [
    id 248
    label "S&#322;owacja"
  ]
  node [
    id 249
    label "Algieria"
  ]
  node [
    id 250
    label "Chiny"
  ]
  node [
    id 251
    label "Grenada"
  ]
  node [
    id 252
    label "Barbados"
  ]
  node [
    id 253
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 254
    label "Pakistan"
  ]
  node [
    id 255
    label "Niemcy"
  ]
  node [
    id 256
    label "Bahrajn"
  ]
  node [
    id 257
    label "Komory"
  ]
  node [
    id 258
    label "Australia"
  ]
  node [
    id 259
    label "Rodezja"
  ]
  node [
    id 260
    label "Malawi"
  ]
  node [
    id 261
    label "Gwinea"
  ]
  node [
    id 262
    label "Wehrlen"
  ]
  node [
    id 263
    label "Meksyk"
  ]
  node [
    id 264
    label "Liechtenstein"
  ]
  node [
    id 265
    label "Czarnog&#243;ra"
  ]
  node [
    id 266
    label "Wielka_Brytania"
  ]
  node [
    id 267
    label "Kuwejt"
  ]
  node [
    id 268
    label "Angola"
  ]
  node [
    id 269
    label "Monako"
  ]
  node [
    id 270
    label "Jemen"
  ]
  node [
    id 271
    label "Etiopia"
  ]
  node [
    id 272
    label "Madagaskar"
  ]
  node [
    id 273
    label "terytorium"
  ]
  node [
    id 274
    label "Kolumbia"
  ]
  node [
    id 275
    label "Portoryko"
  ]
  node [
    id 276
    label "Mauritius"
  ]
  node [
    id 277
    label "Kostaryka"
  ]
  node [
    id 278
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 279
    label "Tajlandia"
  ]
  node [
    id 280
    label "Argentyna"
  ]
  node [
    id 281
    label "Zambia"
  ]
  node [
    id 282
    label "Sri_Lanka"
  ]
  node [
    id 283
    label "Gwatemala"
  ]
  node [
    id 284
    label "Kirgistan"
  ]
  node [
    id 285
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 286
    label "Hiszpania"
  ]
  node [
    id 287
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 288
    label "Salwador"
  ]
  node [
    id 289
    label "Korea"
  ]
  node [
    id 290
    label "Macedonia"
  ]
  node [
    id 291
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 292
    label "Brunei"
  ]
  node [
    id 293
    label "Mozambik"
  ]
  node [
    id 294
    label "Turcja"
  ]
  node [
    id 295
    label "Kambod&#380;a"
  ]
  node [
    id 296
    label "Benin"
  ]
  node [
    id 297
    label "Bhutan"
  ]
  node [
    id 298
    label "Tunezja"
  ]
  node [
    id 299
    label "Austria"
  ]
  node [
    id 300
    label "Izrael"
  ]
  node [
    id 301
    label "Sierra_Leone"
  ]
  node [
    id 302
    label "Jamajka"
  ]
  node [
    id 303
    label "Rosja"
  ]
  node [
    id 304
    label "Rwanda"
  ]
  node [
    id 305
    label "holoarktyka"
  ]
  node [
    id 306
    label "Nigeria"
  ]
  node [
    id 307
    label "USA"
  ]
  node [
    id 308
    label "Oman"
  ]
  node [
    id 309
    label "Luksemburg"
  ]
  node [
    id 310
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 311
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 312
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 313
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 314
    label "Dominikana"
  ]
  node [
    id 315
    label "Irlandia"
  ]
  node [
    id 316
    label "Liban"
  ]
  node [
    id 317
    label "Hanower"
  ]
  node [
    id 318
    label "Estonia"
  ]
  node [
    id 319
    label "Samoa"
  ]
  node [
    id 320
    label "Nowa_Zelandia"
  ]
  node [
    id 321
    label "Gabon"
  ]
  node [
    id 322
    label "Iran"
  ]
  node [
    id 323
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 324
    label "S&#322;owenia"
  ]
  node [
    id 325
    label "Egipt"
  ]
  node [
    id 326
    label "Kiribati"
  ]
  node [
    id 327
    label "Togo"
  ]
  node [
    id 328
    label "Mongolia"
  ]
  node [
    id 329
    label "Sudan"
  ]
  node [
    id 330
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 331
    label "Bahamy"
  ]
  node [
    id 332
    label "Bangladesz"
  ]
  node [
    id 333
    label "partia"
  ]
  node [
    id 334
    label "Serbia"
  ]
  node [
    id 335
    label "Czechy"
  ]
  node [
    id 336
    label "Holandia"
  ]
  node [
    id 337
    label "Birma"
  ]
  node [
    id 338
    label "Albania"
  ]
  node [
    id 339
    label "Mikronezja"
  ]
  node [
    id 340
    label "Gambia"
  ]
  node [
    id 341
    label "Kazachstan"
  ]
  node [
    id 342
    label "interior"
  ]
  node [
    id 343
    label "Uzbekistan"
  ]
  node [
    id 344
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 345
    label "Malta"
  ]
  node [
    id 346
    label "Lesoto"
  ]
  node [
    id 347
    label "para"
  ]
  node [
    id 348
    label "Antarktis"
  ]
  node [
    id 349
    label "Andora"
  ]
  node [
    id 350
    label "Nauru"
  ]
  node [
    id 351
    label "Kuba"
  ]
  node [
    id 352
    label "Wietnam"
  ]
  node [
    id 353
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 354
    label "ziemia"
  ]
  node [
    id 355
    label "Chorwacja"
  ]
  node [
    id 356
    label "Kamerun"
  ]
  node [
    id 357
    label "Urugwaj"
  ]
  node [
    id 358
    label "Niger"
  ]
  node [
    id 359
    label "Turkmenistan"
  ]
  node [
    id 360
    label "Szwajcaria"
  ]
  node [
    id 361
    label "organizacja"
  ]
  node [
    id 362
    label "grupa"
  ]
  node [
    id 363
    label "Litwa"
  ]
  node [
    id 364
    label "Palau"
  ]
  node [
    id 365
    label "Gruzja"
  ]
  node [
    id 366
    label "Kongo"
  ]
  node [
    id 367
    label "Tajwan"
  ]
  node [
    id 368
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 369
    label "Honduras"
  ]
  node [
    id 370
    label "Boliwia"
  ]
  node [
    id 371
    label "Uganda"
  ]
  node [
    id 372
    label "Namibia"
  ]
  node [
    id 373
    label "Erytrea"
  ]
  node [
    id 374
    label "Azerbejd&#380;an"
  ]
  node [
    id 375
    label "Panama"
  ]
  node [
    id 376
    label "Gujana"
  ]
  node [
    id 377
    label "Somalia"
  ]
  node [
    id 378
    label "Burundi"
  ]
  node [
    id 379
    label "Tuwalu"
  ]
  node [
    id 380
    label "Libia"
  ]
  node [
    id 381
    label "Katar"
  ]
  node [
    id 382
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 383
    label "Trynidad_i_Tobago"
  ]
  node [
    id 384
    label "Sahara_Zachodnia"
  ]
  node [
    id 385
    label "Gwinea_Bissau"
  ]
  node [
    id 386
    label "Bu&#322;garia"
  ]
  node [
    id 387
    label "Tonga"
  ]
  node [
    id 388
    label "Nikaragua"
  ]
  node [
    id 389
    label "Fid&#380;i"
  ]
  node [
    id 390
    label "Timor_Wschodni"
  ]
  node [
    id 391
    label "Laos"
  ]
  node [
    id 392
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 393
    label "Ghana"
  ]
  node [
    id 394
    label "Brazylia"
  ]
  node [
    id 395
    label "Belgia"
  ]
  node [
    id 396
    label "Irak"
  ]
  node [
    id 397
    label "Peru"
  ]
  node [
    id 398
    label "Arabia_Saudyjska"
  ]
  node [
    id 399
    label "Indonezja"
  ]
  node [
    id 400
    label "Malediwy"
  ]
  node [
    id 401
    label "Afganistan"
  ]
  node [
    id 402
    label "Jordania"
  ]
  node [
    id 403
    label "Kenia"
  ]
  node [
    id 404
    label "Czad"
  ]
  node [
    id 405
    label "Liberia"
  ]
  node [
    id 406
    label "Mali"
  ]
  node [
    id 407
    label "Armenia"
  ]
  node [
    id 408
    label "W&#281;gry"
  ]
  node [
    id 409
    label "Chile"
  ]
  node [
    id 410
    label "Kanada"
  ]
  node [
    id 411
    label "Cypr"
  ]
  node [
    id 412
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 413
    label "Ekwador"
  ]
  node [
    id 414
    label "Mo&#322;dawia"
  ]
  node [
    id 415
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 416
    label "W&#322;ochy"
  ]
  node [
    id 417
    label "Wyspy_Salomona"
  ]
  node [
    id 418
    label "&#321;otwa"
  ]
  node [
    id 419
    label "D&#380;ibuti"
  ]
  node [
    id 420
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 421
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 422
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 423
    label "Portugalia"
  ]
  node [
    id 424
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 425
    label "Maroko"
  ]
  node [
    id 426
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 427
    label "Francja"
  ]
  node [
    id 428
    label "Botswana"
  ]
  node [
    id 429
    label "Dominika"
  ]
  node [
    id 430
    label "Paragwaj"
  ]
  node [
    id 431
    label "Tad&#380;ykistan"
  ]
  node [
    id 432
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 433
    label "Haiti"
  ]
  node [
    id 434
    label "Khitai"
  ]
  node [
    id 435
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 436
    label "sport"
  ]
  node [
    id 437
    label "opiniodawca"
  ]
  node [
    id 438
    label "orzekanie"
  ]
  node [
    id 439
    label "orzeka&#263;"
  ]
  node [
    id 440
    label "os&#261;dziciel"
  ]
  node [
    id 441
    label "s&#261;d"
  ]
  node [
    id 442
    label "prawnik"
  ]
  node [
    id 443
    label "pracownik"
  ]
  node [
    id 444
    label "kartka"
  ]
  node [
    id 445
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 446
    label "delegowa&#263;"
  ]
  node [
    id 447
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 448
    label "salariat"
  ]
  node [
    id 449
    label "pracu&#347;"
  ]
  node [
    id 450
    label "r&#281;ka"
  ]
  node [
    id 451
    label "delegowanie"
  ]
  node [
    id 452
    label "jurysta"
  ]
  node [
    id 453
    label "prawnicy"
  ]
  node [
    id 454
    label "aplikant"
  ]
  node [
    id 455
    label "Machiavelli"
  ]
  node [
    id 456
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 457
    label "forum"
  ]
  node [
    id 458
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 459
    label "s&#261;downictwo"
  ]
  node [
    id 460
    label "wydarzenie"
  ]
  node [
    id 461
    label "podejrzany"
  ]
  node [
    id 462
    label "&#347;wiadek"
  ]
  node [
    id 463
    label "instytucja"
  ]
  node [
    id 464
    label "biuro"
  ]
  node [
    id 465
    label "post&#281;powanie"
  ]
  node [
    id 466
    label "court"
  ]
  node [
    id 467
    label "my&#347;l"
  ]
  node [
    id 468
    label "obrona"
  ]
  node [
    id 469
    label "system"
  ]
  node [
    id 470
    label "broni&#263;"
  ]
  node [
    id 471
    label "antylogizm"
  ]
  node [
    id 472
    label "strona"
  ]
  node [
    id 473
    label "oskar&#380;yciel"
  ]
  node [
    id 474
    label "urz&#261;d"
  ]
  node [
    id 475
    label "skazany"
  ]
  node [
    id 476
    label "konektyw"
  ]
  node [
    id 477
    label "wypowied&#378;"
  ]
  node [
    id 478
    label "bronienie"
  ]
  node [
    id 479
    label "wytw&#243;r"
  ]
  node [
    id 480
    label "pods&#261;dny"
  ]
  node [
    id 481
    label "zesp&#243;&#322;"
  ]
  node [
    id 482
    label "procesowicz"
  ]
  node [
    id 483
    label "testify"
  ]
  node [
    id 484
    label "decydowa&#263;"
  ]
  node [
    id 485
    label "stwierdza&#263;"
  ]
  node [
    id 486
    label "connote"
  ]
  node [
    id 487
    label "decydowanie"
  ]
  node [
    id 488
    label "s&#261;dzenie"
  ]
  node [
    id 489
    label "stwierdzanie"
  ]
  node [
    id 490
    label "ticket"
  ]
  node [
    id 491
    label "kara"
  ]
  node [
    id 492
    label "bon"
  ]
  node [
    id 493
    label "kartonik"
  ]
  node [
    id 494
    label "faul"
  ]
  node [
    id 495
    label "arkusz"
  ]
  node [
    id 496
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 497
    label "wk&#322;ad"
  ]
  node [
    id 498
    label "atakowanie"
  ]
  node [
    id 499
    label "zaatakowanie"
  ]
  node [
    id 500
    label "kultura_fizyczna"
  ]
  node [
    id 501
    label "zgrupowanie"
  ]
  node [
    id 502
    label "atakowa&#263;"
  ]
  node [
    id 503
    label "usportowienie"
  ]
  node [
    id 504
    label "sokolstwo"
  ]
  node [
    id 505
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 506
    label "zaatakowa&#263;"
  ]
  node [
    id 507
    label "usportowi&#263;"
  ]
  node [
    id 508
    label "uspokoi&#263;"
  ]
  node [
    id 509
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 510
    label "zapanowa&#263;"
  ]
  node [
    id 511
    label "accommodate"
  ]
  node [
    id 512
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 513
    label "pokry&#263;"
  ]
  node [
    id 514
    label "shower"
  ]
  node [
    id 515
    label "obrzuci&#263;"
  ]
  node [
    id 516
    label "spray"
  ]
  node [
    id 517
    label "obdarowa&#263;"
  ]
  node [
    id 518
    label "powiedzie&#263;"
  ]
  node [
    id 519
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 520
    label "okre&#347;li&#263;"
  ]
  node [
    id 521
    label "express"
  ]
  node [
    id 522
    label "wydoby&#263;"
  ]
  node [
    id 523
    label "wyrazi&#263;"
  ]
  node [
    id 524
    label "poda&#263;"
  ]
  node [
    id 525
    label "unwrap"
  ]
  node [
    id 526
    label "rzekn&#261;&#263;"
  ]
  node [
    id 527
    label "discover"
  ]
  node [
    id 528
    label "convey"
  ]
  node [
    id 529
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 530
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 531
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 532
    label "zamaskowa&#263;"
  ]
  node [
    id 533
    label "zaj&#261;&#263;"
  ]
  node [
    id 534
    label "brood"
  ]
  node [
    id 535
    label "cover"
  ]
  node [
    id 536
    label "defray"
  ]
  node [
    id 537
    label "ustawi&#263;_si&#281;"
  ]
  node [
    id 538
    label "przykry&#263;"
  ]
  node [
    id 539
    label "sheathing"
  ]
  node [
    id 540
    label "zaspokoi&#263;"
  ]
  node [
    id 541
    label "zap&#322;odni&#263;"
  ]
  node [
    id 542
    label "zap&#322;aci&#263;"
  ]
  node [
    id 543
    label "bestow"
  ]
  node [
    id 544
    label "udarowa&#263;"
  ]
  node [
    id 545
    label "da&#263;"
  ]
  node [
    id 546
    label "zacz&#261;&#263;"
  ]
  node [
    id 547
    label "napa&#347;&#263;"
  ]
  node [
    id 548
    label "rzuci&#263;"
  ]
  node [
    id 549
    label "obr&#281;bi&#263;"
  ]
  node [
    id 550
    label "hide"
  ]
  node [
    id 551
    label "hem"
  ]
  node [
    id 552
    label "ciecz"
  ]
  node [
    id 553
    label "zawarto&#347;&#263;"
  ]
  node [
    id 554
    label "pojemnik"
  ]
  node [
    id 555
    label "aerosol"
  ]
  node [
    id 556
    label "metalicznie"
  ]
  node [
    id 557
    label "pieni&#261;dze"
  ]
  node [
    id 558
    label "z&#322;ocisty"
  ]
  node [
    id 559
    label "medal"
  ]
  node [
    id 560
    label "p&#322;uczkarnia"
  ]
  node [
    id 561
    label "metal_szlachetny"
  ]
  node [
    id 562
    label "miedziowiec"
  ]
  node [
    id 563
    label "&#380;&#243;&#322;&#263;"
  ]
  node [
    id 564
    label "amber"
  ]
  node [
    id 565
    label "bi&#380;uteria"
  ]
  node [
    id 566
    label "p&#322;ucznia"
  ]
  node [
    id 567
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 568
    label "g&#243;rnik"
  ]
  node [
    id 569
    label "pierwiastek"
  ]
  node [
    id 570
    label "metal"
  ]
  node [
    id 571
    label "b&#322;yszcz&#261;co"
  ]
  node [
    id 572
    label "metaliczny"
  ]
  node [
    id 573
    label "jasno"
  ]
  node [
    id 574
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 575
    label "ciep&#322;o"
  ]
  node [
    id 576
    label "kosztowno&#347;ci"
  ]
  node [
    id 577
    label "g&#243;rka"
  ]
  node [
    id 578
    label "ozdoba"
  ]
  node [
    id 579
    label "sutasz"
  ]
  node [
    id 580
    label "kapanie"
  ]
  node [
    id 581
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 582
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 583
    label "kapn&#261;&#263;"
  ]
  node [
    id 584
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 585
    label "portfel"
  ]
  node [
    id 586
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 587
    label "forsa"
  ]
  node [
    id 588
    label "kapa&#263;"
  ]
  node [
    id 589
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 590
    label "kwota"
  ]
  node [
    id 591
    label "kapita&#322;"
  ]
  node [
    id 592
    label "kapni&#281;cie"
  ]
  node [
    id 593
    label "wyda&#263;"
  ]
  node [
    id 594
    label "hajs"
  ]
  node [
    id 595
    label "dydki"
  ]
  node [
    id 596
    label "rewers"
  ]
  node [
    id 597
    label "legenda"
  ]
  node [
    id 598
    label "odznaka"
  ]
  node [
    id 599
    label "numizmat"
  ]
  node [
    id 600
    label "numizmatyka"
  ]
  node [
    id 601
    label "decoration"
  ]
  node [
    id 602
    label "awers"
  ]
  node [
    id 603
    label "bilirubina"
  ]
  node [
    id 604
    label "kolor"
  ]
  node [
    id 605
    label "wydzielina"
  ]
  node [
    id 606
    label "mucyna"
  ]
  node [
    id 607
    label "barwa_podstawowa"
  ]
  node [
    id 608
    label "gniew"
  ]
  node [
    id 609
    label "fury"
  ]
  node [
    id 610
    label "miska"
  ]
  node [
    id 611
    label "wyr&#243;b"
  ]
  node [
    id 612
    label "zbiornik"
  ]
  node [
    id 613
    label "piasek"
  ]
  node [
    id 614
    label "urz&#261;dzenie"
  ]
  node [
    id 615
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 616
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 617
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 618
    label "z&#322;ocenie"
  ]
  node [
    id 619
    label "poz&#322;ocenie"
  ]
  node [
    id 620
    label "&#380;ywica"
  ]
  node [
    id 621
    label "mineraloid"
  ]
  node [
    id 622
    label "tworzywo"
  ]
  node [
    id 623
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 624
    label "zobo"
  ]
  node [
    id 625
    label "byd&#322;o"
  ]
  node [
    id 626
    label "dzo"
  ]
  node [
    id 627
    label "yakalo"
  ]
  node [
    id 628
    label "zbi&#243;r"
  ]
  node [
    id 629
    label "kr&#281;torogie"
  ]
  node [
    id 630
    label "livestock"
  ]
  node [
    id 631
    label "posp&#243;lstwo"
  ]
  node [
    id 632
    label "kraal"
  ]
  node [
    id 633
    label "czochrad&#322;o"
  ]
  node [
    id 634
    label "prze&#380;uwacz"
  ]
  node [
    id 635
    label "zebu"
  ]
  node [
    id 636
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 637
    label "bizon"
  ]
  node [
    id 638
    label "byd&#322;o_domowe"
  ]
  node [
    id 639
    label "doba"
  ]
  node [
    id 640
    label "wigilia"
  ]
  node [
    id 641
    label "long_time"
  ]
  node [
    id 642
    label "tydzie&#324;"
  ]
  node [
    id 643
    label "noc"
  ]
  node [
    id 644
    label "czas"
  ]
  node [
    id 645
    label "godzina"
  ]
  node [
    id 646
    label "dzie&#324;"
  ]
  node [
    id 647
    label "jednostka_geologiczna"
  ]
  node [
    id 648
    label "spotkanie"
  ]
  node [
    id 649
    label "kolacja"
  ]
  node [
    id 650
    label "impreza"
  ]
  node [
    id 651
    label "postnik"
  ]
  node [
    id 652
    label "vigil"
  ]
  node [
    id 653
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 654
    label "pasterka"
  ]
  node [
    id 655
    label "ryba"
  ]
  node [
    id 656
    label "pike"
  ]
  node [
    id 657
    label "chudeusz"
  ]
  node [
    id 658
    label "drapie&#380;nik"
  ]
  node [
    id 659
    label "szczupakowate"
  ]
  node [
    id 660
    label "sk&#243;ra"
  ]
  node [
    id 661
    label "skok"
  ]
  node [
    id 662
    label "coating"
  ]
  node [
    id 663
    label "okrywa"
  ]
  node [
    id 664
    label "nask&#243;rek"
  ]
  node [
    id 665
    label "surowiec"
  ]
  node [
    id 666
    label "gruczo&#322;_&#322;ojowy"
  ]
  node [
    id 667
    label "wyprze&#263;"
  ]
  node [
    id 668
    label "gestapowiec"
  ]
  node [
    id 669
    label "tkanka_podsk&#243;rna"
  ]
  node [
    id 670
    label "lico"
  ]
  node [
    id 671
    label "organ"
  ]
  node [
    id 672
    label "pow&#322;oka"
  ]
  node [
    id 673
    label "czerniak_z&#322;o&#347;liwy"
  ]
  node [
    id 674
    label "rockers"
  ]
  node [
    id 675
    label "&#322;uskowato&#347;&#263;"
  ]
  node [
    id 676
    label "gruczo&#322;_kuprowy"
  ]
  node [
    id 677
    label "shell"
  ]
  node [
    id 678
    label "p&#322;aszcz"
  ]
  node [
    id 679
    label "wi&#243;rkownik"
  ]
  node [
    id 680
    label "hardrockowiec"
  ]
  node [
    id 681
    label "dupa"
  ]
  node [
    id 682
    label "funkcja"
  ]
  node [
    id 683
    label "mizdra"
  ]
  node [
    id 684
    label "sk&#243;ra_w&#322;a&#347;ciwa"
  ]
  node [
    id 685
    label "gruczo&#322;_potowy"
  ]
  node [
    id 686
    label "wyprawa"
  ]
  node [
    id 687
    label "zdrowie"
  ]
  node [
    id 688
    label "harleyowiec"
  ]
  node [
    id 689
    label "krupon"
  ]
  node [
    id 690
    label "kurtka"
  ]
  node [
    id 691
    label "cia&#322;o"
  ]
  node [
    id 692
    label "&#322;upa"
  ]
  node [
    id 693
    label "mi&#281;so&#380;erca"
  ]
  node [
    id 694
    label "systemik"
  ]
  node [
    id 695
    label "tar&#322;o"
  ]
  node [
    id 696
    label "rakowato&#347;&#263;"
  ]
  node [
    id 697
    label "szczelina_skrzelowa"
  ]
  node [
    id 698
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 699
    label "doniczkowiec"
  ]
  node [
    id 700
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 701
    label "mi&#281;so"
  ]
  node [
    id 702
    label "fish"
  ]
  node [
    id 703
    label "patroszy&#263;"
  ]
  node [
    id 704
    label "linia_boczna"
  ]
  node [
    id 705
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 706
    label "pokrywa_skrzelowa"
  ]
  node [
    id 707
    label "kr&#281;gowiec"
  ]
  node [
    id 708
    label "w&#281;dkarstwo"
  ]
  node [
    id 709
    label "ryby"
  ]
  node [
    id 710
    label "m&#281;tnooki"
  ]
  node [
    id 711
    label "ikra"
  ]
  node [
    id 712
    label "wyrostek_filtracyjny"
  ]
  node [
    id 713
    label "derail"
  ]
  node [
    id 714
    label "naskok"
  ]
  node [
    id 715
    label "gimnastyka_przyrz&#261;dowa"
  ]
  node [
    id 716
    label "&#322;apa"
  ]
  node [
    id 717
    label "struktura_anatomiczna"
  ]
  node [
    id 718
    label "zaj&#261;c"
  ]
  node [
    id 719
    label "l&#261;dowanie"
  ]
  node [
    id 720
    label "napad"
  ]
  node [
    id 721
    label "wybicie"
  ]
  node [
    id 722
    label "ptak"
  ]
  node [
    id 723
    label "ruch"
  ]
  node [
    id 724
    label "stroke"
  ]
  node [
    id 725
    label "noga"
  ]
  node [
    id 726
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 727
    label "ko&#347;&#263;_skokowa"
  ]
  node [
    id 728
    label "caper"
  ]
  node [
    id 729
    label "konkurencja"
  ]
  node [
    id 730
    label "szczupakokszta&#322;tne"
  ]
  node [
    id 731
    label "szczur"
  ]
  node [
    id 732
    label "chudzielec"
  ]
  node [
    id 733
    label "geofit"
  ]
  node [
    id 734
    label "przyprawa"
  ]
  node [
    id 735
    label "kosa&#263;cowate"
  ]
  node [
    id 736
    label "bylina"
  ]
  node [
    id 737
    label "kwiat"
  ]
  node [
    id 738
    label "znami&#281;"
  ]
  node [
    id 739
    label "bulwa_p&#281;dowa"
  ]
  node [
    id 740
    label "stamp"
  ]
  node [
    id 741
    label "cecha"
  ]
  node [
    id 742
    label "py&#322;ek"
  ]
  node [
    id 743
    label "wygl&#261;d"
  ]
  node [
    id 744
    label "s&#322;upek"
  ]
  node [
    id 745
    label "okrytonasienne"
  ]
  node [
    id 746
    label "jedzenie"
  ]
  node [
    id 747
    label "produkt"
  ]
  node [
    id 748
    label "dodatek"
  ]
  node [
    id 749
    label "bakalie"
  ]
  node [
    id 750
    label "ro&#347;lina_przyprawowa"
  ]
  node [
    id 751
    label "kabaret"
  ]
  node [
    id 752
    label "rurka"
  ]
  node [
    id 753
    label "ro&#347;lina"
  ]
  node [
    id 754
    label "kielich"
  ]
  node [
    id 755
    label "flakon"
  ]
  node [
    id 756
    label "organ_ro&#347;linny"
  ]
  node [
    id 757
    label "dno_kwiatowe"
  ]
  node [
    id 758
    label "warga"
  ]
  node [
    id 759
    label "przykoronek"
  ]
  node [
    id 760
    label "korona"
  ]
  node [
    id 761
    label "ogon"
  ]
  node [
    id 762
    label "kryptofit"
  ]
  node [
    id 763
    label "utw&#243;r_epicki"
  ]
  node [
    id 764
    label "ludowy"
  ]
  node [
    id 765
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 766
    label "pie&#347;&#324;"
  ]
  node [
    id 767
    label "szparagowce"
  ]
  node [
    id 768
    label "&#322;uskacze"
  ]
  node [
    id 769
    label "ptak_&#347;piewaj&#261;cy"
  ]
  node [
    id 770
    label "&#322;uszczaki"
  ]
  node [
    id 771
    label "kcie&#263;"
  ]
  node [
    id 772
    label "czu&#263;"
  ]
  node [
    id 773
    label "desire"
  ]
  node [
    id 774
    label "uczuwa&#263;"
  ]
  node [
    id 775
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 776
    label "smell"
  ]
  node [
    id 777
    label "doznawa&#263;"
  ]
  node [
    id 778
    label "przewidywa&#263;"
  ]
  node [
    id 779
    label "anticipate"
  ]
  node [
    id 780
    label "postrzega&#263;"
  ]
  node [
    id 781
    label "by&#263;"
  ]
  node [
    id 782
    label "spirit"
  ]
  node [
    id 783
    label "troch&#281;"
  ]
  node [
    id 784
    label "need"
  ]
  node [
    id 785
    label "support"
  ]
  node [
    id 786
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 787
    label "interpretator"
  ]
  node [
    id 788
    label "w&#243;dz"
  ]
  node [
    id 789
    label "strzelec"
  ]
  node [
    id 790
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 791
    label "Napoleon"
  ]
  node [
    id 792
    label "dow&#243;dca"
  ]
  node [
    id 793
    label "Bismarck"
  ]
  node [
    id 794
    label "Juliusz_Cezar"
  ]
  node [
    id 795
    label "sportowiec"
  ]
  node [
    id 796
    label "&#380;o&#322;nierz"
  ]
  node [
    id 797
    label "Renata_Mauer"
  ]
  node [
    id 798
    label "futbolista"
  ]
  node [
    id 799
    label "kompletny"
  ]
  node [
    id 800
    label "zdr&#243;w"
  ]
  node [
    id 801
    label "ca&#322;o"
  ]
  node [
    id 802
    label "calu&#347;ko"
  ]
  node [
    id 803
    label "podobny"
  ]
  node [
    id 804
    label "&#380;ywy"
  ]
  node [
    id 805
    label "pe&#322;ny"
  ]
  node [
    id 806
    label "jedyny"
  ]
  node [
    id 807
    label "w_pizdu"
  ]
  node [
    id 808
    label "zupe&#322;ny"
  ]
  node [
    id 809
    label "kompletnie"
  ]
  node [
    id 810
    label "taki"
  ]
  node [
    id 811
    label "drugi"
  ]
  node [
    id 812
    label "upodobnienie_si&#281;"
  ]
  node [
    id 813
    label "przypominanie"
  ]
  node [
    id 814
    label "podobnie"
  ]
  node [
    id 815
    label "charakterystyczny"
  ]
  node [
    id 816
    label "upodabnianie_si&#281;"
  ]
  node [
    id 817
    label "zasymilowanie"
  ]
  node [
    id 818
    label "upodobnienie"
  ]
  node [
    id 819
    label "optymalnie"
  ]
  node [
    id 820
    label "najlepszy"
  ]
  node [
    id 821
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 822
    label "ukochany"
  ]
  node [
    id 823
    label "znaczny"
  ]
  node [
    id 824
    label "du&#380;o"
  ]
  node [
    id 825
    label "wa&#380;ny"
  ]
  node [
    id 826
    label "niema&#322;o"
  ]
  node [
    id 827
    label "wiele"
  ]
  node [
    id 828
    label "prawdziwy"
  ]
  node [
    id 829
    label "rozwini&#281;ty"
  ]
  node [
    id 830
    label "dorodny"
  ]
  node [
    id 831
    label "realistyczny"
  ]
  node [
    id 832
    label "silny"
  ]
  node [
    id 833
    label "o&#380;ywianie"
  ]
  node [
    id 834
    label "zgrabny"
  ]
  node [
    id 835
    label "g&#322;&#281;boki"
  ]
  node [
    id 836
    label "energiczny"
  ]
  node [
    id 837
    label "naturalny"
  ]
  node [
    id 838
    label "ciekawy"
  ]
  node [
    id 839
    label "&#380;ywo"
  ]
  node [
    id 840
    label "wyra&#378;ny"
  ]
  node [
    id 841
    label "&#380;ywotny"
  ]
  node [
    id 842
    label "czynny"
  ]
  node [
    id 843
    label "aktualny"
  ]
  node [
    id 844
    label "szybki"
  ]
  node [
    id 845
    label "zdrowy"
  ]
  node [
    id 846
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 847
    label "nieograniczony"
  ]
  node [
    id 848
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 849
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 850
    label "bezwzgl&#281;dny"
  ]
  node [
    id 851
    label "satysfakcja"
  ]
  node [
    id 852
    label "pe&#322;no"
  ]
  node [
    id 853
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 854
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 855
    label "r&#243;wny"
  ]
  node [
    id 856
    label "wype&#322;nienie"
  ]
  node [
    id 857
    label "otwarty"
  ]
  node [
    id 858
    label "odpowiednio"
  ]
  node [
    id 859
    label "nieuszkodzony"
  ]
  node [
    id 860
    label "trynitaryzm"
  ]
  node [
    id 861
    label "kataryzm"
  ]
  node [
    id 862
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 863
    label "Ojciec_Ko&#347;cio&#322;a"
  ]
  node [
    id 864
    label "husytyzm"
  ]
  node [
    id 865
    label "antytrynitaryzm"
  ]
  node [
    id 866
    label "b&#322;ogos&#322;awiony"
  ]
  node [
    id 867
    label "protestantyzm"
  ]
  node [
    id 868
    label "mariawityzm"
  ]
  node [
    id 869
    label "krze&#347;cija&#324;stwo"
  ]
  node [
    id 870
    label "religia"
  ]
  node [
    id 871
    label "ochrzci&#263;_si&#281;"
  ]
  node [
    id 872
    label "prawos&#322;awie"
  ]
  node [
    id 873
    label "katolicyzm"
  ]
  node [
    id 874
    label "ekskomunika"
  ]
  node [
    id 875
    label "afrochrze&#347;cija&#324;stwo"
  ]
  node [
    id 876
    label "sabatarianizm"
  ]
  node [
    id 877
    label "chrzci&#263;_si&#281;"
  ]
  node [
    id 878
    label "mormonizm"
  ]
  node [
    id 879
    label "nawracanie_si&#281;"
  ]
  node [
    id 880
    label "mistyka"
  ]
  node [
    id 881
    label "duchowny"
  ]
  node [
    id 882
    label "kultura_duchowa"
  ]
  node [
    id 883
    label "przedmiot"
  ]
  node [
    id 884
    label "rela"
  ]
  node [
    id 885
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 886
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 887
    label "kultura"
  ]
  node [
    id 888
    label "kosmologia"
  ]
  node [
    id 889
    label "wyznanie"
  ]
  node [
    id 890
    label "kosmogonia"
  ]
  node [
    id 891
    label "kult"
  ]
  node [
    id 892
    label "nawraca&#263;"
  ]
  node [
    id 893
    label "mitologia"
  ]
  node [
    id 894
    label "ideologia"
  ]
  node [
    id 895
    label "pastor"
  ]
  node [
    id 896
    label "superintendent"
  ]
  node [
    id 897
    label "anglikanizm"
  ]
  node [
    id 898
    label "adwentyzm"
  ]
  node [
    id 899
    label "kalwinizm"
  ]
  node [
    id 900
    label "staroluteranizm"
  ]
  node [
    id 901
    label "nonkonformizm"
  ]
  node [
    id 902
    label "Ko&#347;ci&#243;&#322;_reformowany"
  ]
  node [
    id 903
    label "Ko&#347;ci&#243;&#322;_episkopalny"
  ]
  node [
    id 904
    label "kongregacjonalizm"
  ]
  node [
    id 905
    label "konsubstancjacja"
  ]
  node [
    id 906
    label "arianizm"
  ]
  node [
    id 907
    label "reformacja"
  ]
  node [
    id 908
    label "predykant"
  ]
  node [
    id 909
    label "Ko&#347;ci&#243;&#322;_zielono&#347;wi&#261;tkowy"
  ]
  node [
    id 910
    label "unitarianizm"
  ]
  node [
    id 911
    label "kwakierstwo"
  ]
  node [
    id 912
    label "baptyzm"
  ]
  node [
    id 913
    label "anabaptyzm"
  ]
  node [
    id 914
    label "metodyzm"
  ]
  node [
    id 915
    label "Ko&#347;ci&#243;&#322;_Adwentyst&#243;w_Dnia_Si&#243;dmego"
  ]
  node [
    id 916
    label "luteranizm"
  ]
  node [
    id 917
    label "Tr&#243;jca_&#346;wi&#281;ta"
  ]
  node [
    id 918
    label "doktryna"
  ]
  node [
    id 919
    label "prereformacja"
  ]
  node [
    id 920
    label "utrakwizm"
  ]
  node [
    id 921
    label "staroprawos&#322;awie"
  ]
  node [
    id 922
    label "stauropigia"
  ]
  node [
    id 923
    label "msza"
  ]
  node [
    id 924
    label "herezja"
  ]
  node [
    id 925
    label "objawienie"
  ]
  node [
    id 926
    label "oratorianie"
  ]
  node [
    id 927
    label "chrze&#347;cija&#324;stwo_zachodnie"
  ]
  node [
    id 928
    label "koncyliaryzm"
  ]
  node [
    id 929
    label "starokatolicyzm"
  ]
  node [
    id 930
    label "grekokatolicyzm"
  ]
  node [
    id 931
    label "katolicyzm_dynamiczny"
  ]
  node [
    id 932
    label "ruch_religijny"
  ]
  node [
    id 933
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 934
    label "przedmiot_kultu"
  ]
  node [
    id 935
    label "zbawienny"
  ]
  node [
    id 936
    label "raj"
  ]
  node [
    id 937
    label "zmar&#322;y"
  ]
  node [
    id 938
    label "Blessed"
  ]
  node [
    id 939
    label "hagiografia"
  ]
  node [
    id 940
    label "niebianin"
  ]
  node [
    id 941
    label "beatyfikowanie"
  ]
  node [
    id 942
    label "kl&#261;twa"
  ]
  node [
    id 943
    label "cenzura"
  ]
  node [
    id 944
    label "ko&#347;cielny"
  ]
  node [
    id 945
    label "zbi&#243;rka"
  ]
  node [
    id 946
    label "discipline"
  ]
  node [
    id 947
    label "zboczy&#263;"
  ]
  node [
    id 948
    label "w&#261;tek"
  ]
  node [
    id 949
    label "entity"
  ]
  node [
    id 950
    label "sponiewiera&#263;"
  ]
  node [
    id 951
    label "zboczenie"
  ]
  node [
    id 952
    label "zbaczanie"
  ]
  node [
    id 953
    label "charakter"
  ]
  node [
    id 954
    label "thing"
  ]
  node [
    id 955
    label "om&#243;wi&#263;"
  ]
  node [
    id 956
    label "tre&#347;&#263;"
  ]
  node [
    id 957
    label "element"
  ]
  node [
    id 958
    label "kr&#261;&#380;enie"
  ]
  node [
    id 959
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 960
    label "istota"
  ]
  node [
    id 961
    label "zbacza&#263;"
  ]
  node [
    id 962
    label "om&#243;wienie"
  ]
  node [
    id 963
    label "rzecz"
  ]
  node [
    id 964
    label "tematyka"
  ]
  node [
    id 965
    label "omawianie"
  ]
  node [
    id 966
    label "omawia&#263;"
  ]
  node [
    id 967
    label "robienie"
  ]
  node [
    id 968
    label "program_nauczania"
  ]
  node [
    id 969
    label "sponiewieranie"
  ]
  node [
    id 970
    label "koszyk&#243;wka"
  ]
  node [
    id 971
    label "kwestowanie"
  ]
  node [
    id 972
    label "recoil"
  ]
  node [
    id 973
    label "kwestarz"
  ]
  node [
    id 974
    label "czynno&#347;&#263;"
  ]
  node [
    id 975
    label "apel"
  ]
  node [
    id 976
    label "collection"
  ]
  node [
    id 977
    label "chwyt"
  ]
  node [
    id 978
    label "s&#322;u&#380;ba_ko&#347;cielna"
  ]
  node [
    id 979
    label "ko&#347;cielnie"
  ]
  node [
    id 980
    label "pomoc"
  ]
  node [
    id 981
    label "sidesman"
  ]
  node [
    id 982
    label "parafianin"
  ]
  node [
    id 983
    label "tu"
  ]
  node [
    id 984
    label "mak"
  ]
  node [
    id 985
    label "dynia"
  ]
  node [
    id 986
    label "puszka"
  ]
  node [
    id 987
    label "czaszka"
  ]
  node [
    id 988
    label "&#322;eb"
  ]
  node [
    id 989
    label "gourd"
  ]
  node [
    id 990
    label "dyniowate"
  ]
  node [
    id 991
    label "warzywo"
  ]
  node [
    id 992
    label "nibyjagoda"
  ]
  node [
    id 993
    label "owoc"
  ]
  node [
    id 994
    label "zdolno&#347;&#263;"
  ]
  node [
    id 995
    label "umys&#322;"
  ]
  node [
    id 996
    label "wiedza"
  ]
  node [
    id 997
    label "morda"
  ]
  node [
    id 998
    label "alkohol"
  ]
  node [
    id 999
    label "noosfera"
  ]
  node [
    id 1000
    label "rozszczep_czaszki"
  ]
  node [
    id 1001
    label "ko&#347;&#263;_czo&#322;owa"
  ]
  node [
    id 1002
    label "diafanoskopia"
  ]
  node [
    id 1003
    label "&#380;uchwa"
  ]
  node [
    id 1004
    label "ma&#322;og&#322;owie"
  ]
  node [
    id 1005
    label "siode&#322;ko_tureckie"
  ]
  node [
    id 1006
    label "&#322;uk_jarzmowy"
  ]
  node [
    id 1007
    label "zatoka"
  ]
  node [
    id 1008
    label "szew_kostny"
  ]
  node [
    id 1009
    label "ko&#347;&#263;_podniebienna"
  ]
  node [
    id 1010
    label "lemiesz"
  ]
  node [
    id 1011
    label "ko&#347;&#263;_klinowa"
  ]
  node [
    id 1012
    label "oczod&#243;&#322;"
  ]
  node [
    id 1013
    label "ciemi&#281;"
  ]
  node [
    id 1014
    label "m&#243;zgoczaszka"
  ]
  node [
    id 1015
    label "po&#347;redniog&#322;owo&#347;&#263;"
  ]
  node [
    id 1016
    label "wa&#322;_nadoczodo&#322;owy"
  ]
  node [
    id 1017
    label "szkielet"
  ]
  node [
    id 1018
    label "szew_strza&#322;kowy"
  ]
  node [
    id 1019
    label "potylica"
  ]
  node [
    id 1020
    label "puszka_m&#243;zgowa"
  ]
  node [
    id 1021
    label "szczelina_oczodo&#322;owa"
  ]
  node [
    id 1022
    label "trzewioczaszka"
  ]
  node [
    id 1023
    label "bro&#324;_palna"
  ]
  node [
    id 1024
    label "torebka"
  ]
  node [
    id 1025
    label "mszak"
  ]
  node [
    id 1026
    label "reedukator"
  ]
  node [
    id 1027
    label "ciupa"
  ]
  node [
    id 1028
    label "miejsce_odosobnienia"
  ]
  node [
    id 1029
    label "opakowanie"
  ]
  node [
    id 1030
    label "&#321;ubianka"
  ]
  node [
    id 1031
    label "naczynie"
  ]
  node [
    id 1032
    label "os&#322;ona"
  ]
  node [
    id 1033
    label "skarbonka"
  ]
  node [
    id 1034
    label "hostia"
  ]
  node [
    id 1035
    label "box"
  ]
  node [
    id 1036
    label "zarodnia"
  ]
  node [
    id 1037
    label "Butyrki"
  ]
  node [
    id 1038
    label "ro&#347;lina_zielna"
  ]
  node [
    id 1039
    label "kwiat_polny"
  ]
  node [
    id 1040
    label "ro&#347;lina_oleista"
  ]
  node [
    id 1041
    label "nasionko"
  ]
  node [
    id 1042
    label "makowate"
  ]
  node [
    id 1043
    label "robi&#263;"
  ]
  node [
    id 1044
    label "uderza&#263;"
  ]
  node [
    id 1045
    label "snap"
  ]
  node [
    id 1046
    label "odpala&#263;"
  ]
  node [
    id 1047
    label "kierowa&#263;"
  ]
  node [
    id 1048
    label "fight"
  ]
  node [
    id 1049
    label "napierdziela&#263;"
  ]
  node [
    id 1050
    label "train"
  ]
  node [
    id 1051
    label "plu&#263;"
  ]
  node [
    id 1052
    label "walczy&#263;"
  ]
  node [
    id 1053
    label "match"
  ]
  node [
    id 1054
    label "przeznacza&#263;"
  ]
  node [
    id 1055
    label "administrowa&#263;"
  ]
  node [
    id 1056
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1057
    label "motywowa&#263;"
  ]
  node [
    id 1058
    label "order"
  ]
  node [
    id 1059
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1060
    label "sterowa&#263;"
  ]
  node [
    id 1061
    label "ustawia&#263;"
  ]
  node [
    id 1062
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1063
    label "control"
  ]
  node [
    id 1064
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 1065
    label "give"
  ]
  node [
    id 1066
    label "manipulate"
  ]
  node [
    id 1067
    label "indicate"
  ]
  node [
    id 1068
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1069
    label "uderza&#263;_do_panny"
  ]
  node [
    id 1070
    label "zadawa&#263;"
  ]
  node [
    id 1071
    label "break"
  ]
  node [
    id 1072
    label "konkurowa&#263;"
  ]
  node [
    id 1073
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1074
    label "mie&#263;_miejsce"
  ]
  node [
    id 1075
    label "hopka&#263;"
  ]
  node [
    id 1076
    label "blend"
  ]
  node [
    id 1077
    label "startowa&#263;"
  ]
  node [
    id 1078
    label "rani&#263;"
  ]
  node [
    id 1079
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 1080
    label "przypieprza&#263;"
  ]
  node [
    id 1081
    label "go"
  ]
  node [
    id 1082
    label "funkcjonowa&#263;"
  ]
  node [
    id 1083
    label "chop"
  ]
  node [
    id 1084
    label "ofensywny"
  ]
  node [
    id 1085
    label "sztacha&#263;"
  ]
  node [
    id 1086
    label "rwa&#263;"
  ]
  node [
    id 1087
    label "napada&#263;"
  ]
  node [
    id 1088
    label "powodowa&#263;"
  ]
  node [
    id 1089
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 1090
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1091
    label "woo"
  ]
  node [
    id 1092
    label "wstrzeliwa&#263;_si&#281;"
  ]
  node [
    id 1093
    label "nast&#281;powa&#263;"
  ]
  node [
    id 1094
    label "strike"
  ]
  node [
    id 1095
    label "take"
  ]
  node [
    id 1096
    label "krytykowa&#263;"
  ]
  node [
    id 1097
    label "dotyka&#263;"
  ]
  node [
    id 1098
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1099
    label "oszukiwa&#263;"
  ]
  node [
    id 1100
    label "tentegowa&#263;"
  ]
  node [
    id 1101
    label "urz&#261;dza&#263;"
  ]
  node [
    id 1102
    label "praca"
  ]
  node [
    id 1103
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 1104
    label "czyni&#263;"
  ]
  node [
    id 1105
    label "work"
  ]
  node [
    id 1106
    label "przerabia&#263;"
  ]
  node [
    id 1107
    label "act"
  ]
  node [
    id 1108
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1109
    label "post&#281;powa&#263;"
  ]
  node [
    id 1110
    label "peddle"
  ]
  node [
    id 1111
    label "organizowa&#263;"
  ]
  node [
    id 1112
    label "falowa&#263;"
  ]
  node [
    id 1113
    label "stylizowa&#263;"
  ]
  node [
    id 1114
    label "wydala&#263;"
  ]
  node [
    id 1115
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 1116
    label "ukazywa&#263;"
  ]
  node [
    id 1117
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 1118
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 1119
    label "opowiada&#263;_si&#281;"
  ]
  node [
    id 1120
    label "my&#347;lenie"
  ]
  node [
    id 1121
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 1122
    label "argue"
  ]
  node [
    id 1123
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1124
    label "contend"
  ]
  node [
    id 1125
    label "zmaga&#263;_si&#281;"
  ]
  node [
    id 1126
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 1127
    label "zawody"
  ]
  node [
    id 1128
    label "cope"
  ]
  node [
    id 1129
    label "wrestle"
  ]
  node [
    id 1130
    label "odpowiada&#263;"
  ]
  node [
    id 1131
    label "fuel"
  ]
  node [
    id 1132
    label "dawa&#263;"
  ]
  node [
    id 1133
    label "reject"
  ]
  node [
    id 1134
    label "za&#347;wieca&#263;"
  ]
  node [
    id 1135
    label "zapala&#263;"
  ]
  node [
    id 1136
    label "gardzi&#263;"
  ]
  node [
    id 1137
    label "spew"
  ]
  node [
    id 1138
    label "plwa&#263;"
  ]
  node [
    id 1139
    label "wydziela&#263;"
  ]
  node [
    id 1140
    label "naciska&#263;"
  ]
  node [
    id 1141
    label "gra&#263;"
  ]
  node [
    id 1142
    label "gada&#263;"
  ]
  node [
    id 1143
    label "bi&#263;"
  ]
  node [
    id 1144
    label "bole&#263;"
  ]
  node [
    id 1145
    label "popyla&#263;"
  ]
  node [
    id 1146
    label "psu&#263;_si&#281;"
  ]
  node [
    id 1147
    label "harowa&#263;"
  ]
  node [
    id 1148
    label "proceed"
  ]
  node [
    id 1149
    label "try"
  ]
  node [
    id 1150
    label "bie&#380;e&#263;"
  ]
  node [
    id 1151
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 1152
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 1153
    label "impart"
  ]
  node [
    id 1154
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1155
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1156
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1157
    label "continue"
  ]
  node [
    id 1158
    label "draw"
  ]
  node [
    id 1159
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 1160
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 1161
    label "trace"
  ]
  node [
    id 1162
    label "describe"
  ]
  node [
    id 1163
    label "bangla&#263;"
  ]
  node [
    id 1164
    label "lecie&#263;"
  ]
  node [
    id 1165
    label "tryb"
  ]
  node [
    id 1166
    label "wyrusza&#263;"
  ]
  node [
    id 1167
    label "boost"
  ]
  node [
    id 1168
    label "dziama&#263;"
  ]
  node [
    id 1169
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 1170
    label "dzia&#263;_si&#281;"
  ]
  node [
    id 1171
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1172
    label "stan"
  ]
  node [
    id 1173
    label "stand"
  ]
  node [
    id 1174
    label "trwa&#263;"
  ]
  node [
    id 1175
    label "equal"
  ]
  node [
    id 1176
    label "chodzi&#263;"
  ]
  node [
    id 1177
    label "uczestniczy&#263;"
  ]
  node [
    id 1178
    label "obecno&#347;&#263;"
  ]
  node [
    id 1179
    label "si&#281;ga&#263;"
  ]
  node [
    id 1180
    label "rusza&#263;"
  ]
  node [
    id 1181
    label "usi&#322;owa&#263;"
  ]
  node [
    id 1182
    label "trouble_oneself"
  ]
  node [
    id 1183
    label "przewaga"
  ]
  node [
    id 1184
    label "epidemia"
  ]
  node [
    id 1185
    label "schorzenie"
  ]
  node [
    id 1186
    label "aim"
  ]
  node [
    id 1187
    label "rozgrywa&#263;"
  ]
  node [
    id 1188
    label "m&#243;wi&#263;"
  ]
  node [
    id 1189
    label "attack"
  ]
  node [
    id 1190
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1191
    label "po&#380;&#261;da&#263;"
  ]
  node [
    id 1192
    label "fly"
  ]
  node [
    id 1193
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 1194
    label "odchodzi&#263;"
  ]
  node [
    id 1195
    label "spada&#263;"
  ]
  node [
    id 1196
    label "lata&#263;"
  ]
  node [
    id 1197
    label "rush"
  ]
  node [
    id 1198
    label "podr&#243;&#380;owa&#263;"
  ]
  node [
    id 1199
    label "biega&#263;"
  ]
  node [
    id 1200
    label "omdlewa&#263;"
  ]
  node [
    id 1201
    label "mija&#263;"
  ]
  node [
    id 1202
    label "spieszy&#263;_si&#281;"
  ]
  node [
    id 1203
    label "przybiera&#263;"
  ]
  node [
    id 1204
    label "use"
  ]
  node [
    id 1205
    label "chronometria"
  ]
  node [
    id 1206
    label "odczyt"
  ]
  node [
    id 1207
    label "laba"
  ]
  node [
    id 1208
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1209
    label "time_period"
  ]
  node [
    id 1210
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1211
    label "Zeitgeist"
  ]
  node [
    id 1212
    label "pochodzenie"
  ]
  node [
    id 1213
    label "przep&#322;ywanie"
  ]
  node [
    id 1214
    label "schy&#322;ek"
  ]
  node [
    id 1215
    label "czwarty_wymiar"
  ]
  node [
    id 1216
    label "kategoria_gramatyczna"
  ]
  node [
    id 1217
    label "poprzedzi&#263;"
  ]
  node [
    id 1218
    label "pogoda"
  ]
  node [
    id 1219
    label "czasokres"
  ]
  node [
    id 1220
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1221
    label "poprzedzenie"
  ]
  node [
    id 1222
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1223
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1224
    label "dzieje"
  ]
  node [
    id 1225
    label "zegar"
  ]
  node [
    id 1226
    label "koniugacja"
  ]
  node [
    id 1227
    label "trawi&#263;"
  ]
  node [
    id 1228
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1229
    label "poprzedza&#263;"
  ]
  node [
    id 1230
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1231
    label "trawienie"
  ]
  node [
    id 1232
    label "chwila"
  ]
  node [
    id 1233
    label "rachuba_czasu"
  ]
  node [
    id 1234
    label "poprzedzanie"
  ]
  node [
    id 1235
    label "okres_czasu"
  ]
  node [
    id 1236
    label "period"
  ]
  node [
    id 1237
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1238
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1239
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1240
    label "pochodzi&#263;"
  ]
  node [
    id 1241
    label "spos&#243;b"
  ]
  node [
    id 1242
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 1243
    label "modalno&#347;&#263;"
  ]
  node [
    id 1244
    label "z&#261;b"
  ]
  node [
    id 1245
    label "skala"
  ]
  node [
    id 1246
    label "ko&#322;o"
  ]
  node [
    id 1247
    label "biec"
  ]
  node [
    id 1248
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 1249
    label "szczeka&#263;"
  ]
  node [
    id 1250
    label "rozmawia&#263;"
  ]
  node [
    id 1251
    label "rozumie&#263;"
  ]
  node [
    id 1252
    label "skrawy"
  ]
  node [
    id 1253
    label "przepe&#322;niony"
  ]
  node [
    id 1254
    label "skrawo"
  ]
  node [
    id 1255
    label "dostrzec"
  ]
  node [
    id 1256
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 1257
    label "spot"
  ]
  node [
    id 1258
    label "go_steady"
  ]
  node [
    id 1259
    label "obejrze&#263;"
  ]
  node [
    id 1260
    label "spotka&#263;"
  ]
  node [
    id 1261
    label "znale&#378;&#263;"
  ]
  node [
    id 1262
    label "postrzec"
  ]
  node [
    id 1263
    label "pojrze&#263;"
  ]
  node [
    id 1264
    label "see"
  ]
  node [
    id 1265
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 1266
    label "przekona&#263;_si&#281;"
  ]
  node [
    id 1267
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 1268
    label "popatrze&#263;"
  ]
  node [
    id 1269
    label "cognizance"
  ]
  node [
    id 1270
    label "u&#347;wiadomi&#263;_sobie"
  ]
  node [
    id 1271
    label "spoziera&#263;"
  ]
  node [
    id 1272
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 1273
    label "peek"
  ]
  node [
    id 1274
    label "zinterpretowa&#263;"
  ]
  node [
    id 1275
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 1276
    label "think"
  ]
  node [
    id 1277
    label "oceni&#263;"
  ]
  node [
    id 1278
    label "illustrate"
  ]
  node [
    id 1279
    label "zanalizowa&#263;"
  ]
  node [
    id 1280
    label "zagra&#263;"
  ]
  node [
    id 1281
    label "read"
  ]
  node [
    id 1282
    label "visualize"
  ]
  node [
    id 1283
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 1284
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 1285
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1286
    label "zrobi&#263;"
  ]
  node [
    id 1287
    label "respect"
  ]
  node [
    id 1288
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1289
    label "notice"
  ]
  node [
    id 1290
    label "spowodowa&#263;"
  ]
  node [
    id 1291
    label "pozna&#263;"
  ]
  node [
    id 1292
    label "insert"
  ]
  node [
    id 1293
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 1294
    label "befall"
  ]
  node [
    id 1295
    label "zorientowa&#263;_si&#281;"
  ]
  node [
    id 1296
    label "wymy&#347;li&#263;"
  ]
  node [
    id 1297
    label "znaj&#347;&#263;"
  ]
  node [
    id 1298
    label "odzyska&#263;"
  ]
  node [
    id 1299
    label "pozyska&#263;"
  ]
  node [
    id 1300
    label "dozna&#263;"
  ]
  node [
    id 1301
    label "wykry&#263;"
  ]
  node [
    id 1302
    label "devise"
  ]
  node [
    id 1303
    label "invent"
  ]
  node [
    id 1304
    label "radio"
  ]
  node [
    id 1305
    label "nagranie_d&#378;wi&#281;kowe"
  ]
  node [
    id 1306
    label "lampa"
  ]
  node [
    id 1307
    label "fotografia"
  ]
  node [
    id 1308
    label "transakcja"
  ]
  node [
    id 1309
    label "ekspozycja"
  ]
  node [
    id 1310
    label "film"
  ]
  node [
    id 1311
    label "reklama"
  ]
  node [
    id 1312
    label "booklet"
  ]
  node [
    id 1313
    label "pomiar"
  ]
  node [
    id 1314
    label "spojrze&#263;"
  ]
  node [
    id 1315
    label "spogl&#261;da&#263;"
  ]
  node [
    id 1316
    label "przemieni&#263;"
  ]
  node [
    id 1317
    label "zmieni&#263;"
  ]
  node [
    id 1318
    label "pick"
  ]
  node [
    id 1319
    label "dress"
  ]
  node [
    id 1320
    label "upodobni&#263;"
  ]
  node [
    id 1321
    label "przerzuci&#263;"
  ]
  node [
    id 1322
    label "&#347;ci&#261;gn&#261;&#263;"
  ]
  node [
    id 1323
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1324
    label "skopiowa&#263;"
  ]
  node [
    id 1325
    label "przewi&#261;za&#263;"
  ]
  node [
    id 1326
    label "znie&#347;&#263;"
  ]
  node [
    id 1327
    label "zmniejszy&#263;"
  ]
  node [
    id 1328
    label "zmusi&#263;"
  ]
  node [
    id 1329
    label "ukra&#347;&#263;"
  ]
  node [
    id 1330
    label "zgromadzi&#263;"
  ]
  node [
    id 1331
    label "zdj&#261;&#263;"
  ]
  node [
    id 1332
    label "pull"
  ]
  node [
    id 1333
    label "przepisa&#263;"
  ]
  node [
    id 1334
    label "perpetrate"
  ]
  node [
    id 1335
    label "odprowadzi&#263;"
  ]
  node [
    id 1336
    label "zgromadzi&#263;_si&#281;"
  ]
  node [
    id 1337
    label "przyby&#263;"
  ]
  node [
    id 1338
    label "accept"
  ]
  node [
    id 1339
    label "come_up"
  ]
  node [
    id 1340
    label "sprawi&#263;"
  ]
  node [
    id 1341
    label "zyska&#263;"
  ]
  node [
    id 1342
    label "change"
  ]
  node [
    id 1343
    label "straci&#263;"
  ]
  node [
    id 1344
    label "zast&#261;pi&#263;"
  ]
  node [
    id 1345
    label "przej&#347;&#263;"
  ]
  node [
    id 1346
    label "assimilate"
  ]
  node [
    id 1347
    label "dopasowa&#263;"
  ]
  node [
    id 1348
    label "przemyci&#263;"
  ]
  node [
    id 1349
    label "zarzuci&#263;"
  ]
  node [
    id 1350
    label "przeszuka&#263;"
  ]
  node [
    id 1351
    label "throw"
  ]
  node [
    id 1352
    label "przesun&#261;&#263;"
  ]
  node [
    id 1353
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 1354
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 1355
    label "bewilder"
  ]
  node [
    id 1356
    label "przejrze&#263;"
  ]
  node [
    id 1357
    label "teach"
  ]
  node [
    id 1358
    label "pracowa&#263;"
  ]
  node [
    id 1359
    label "szkoli&#263;"
  ]
  node [
    id 1360
    label "rozwija&#263;"
  ]
  node [
    id 1361
    label "zapoznawa&#263;"
  ]
  node [
    id 1362
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 1363
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1364
    label "endeavor"
  ]
  node [
    id 1365
    label "maszyna"
  ]
  node [
    id 1366
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 1367
    label "do"
  ]
  node [
    id 1368
    label "podejmowa&#263;"
  ]
  node [
    id 1369
    label "puszcza&#263;"
  ]
  node [
    id 1370
    label "rozstawia&#263;"
  ]
  node [
    id 1371
    label "dopowiada&#263;"
  ]
  node [
    id 1372
    label "inflate"
  ]
  node [
    id 1373
    label "rozpakowywa&#263;"
  ]
  node [
    id 1374
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 1375
    label "stawia&#263;"
  ]
  node [
    id 1376
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 1377
    label "reakcja_chemiczna"
  ]
  node [
    id 1378
    label "determine"
  ]
  node [
    id 1379
    label "prowadzi&#263;"
  ]
  node [
    id 1380
    label "o&#347;wieca&#263;"
  ]
  node [
    id 1381
    label "pomaga&#263;"
  ]
  node [
    id 1382
    label "doskonali&#263;"
  ]
  node [
    id 1383
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1384
    label "informowa&#263;"
  ]
  node [
    id 1385
    label "obznajamia&#263;"
  ]
  node [
    id 1386
    label "zawiera&#263;"
  ]
  node [
    id 1387
    label "poznawa&#263;"
  ]
  node [
    id 1388
    label "kuwa&#263;"
  ]
  node [
    id 1389
    label "kruszy&#263;"
  ]
  node [
    id 1390
    label "wbija&#263;"
  ]
  node [
    id 1391
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 1392
    label "forge"
  ]
  node [
    id 1393
    label "chase"
  ]
  node [
    id 1394
    label "obrabia&#263;"
  ]
  node [
    id 1395
    label "kowal"
  ]
  node [
    id 1396
    label "kuli&#263;"
  ]
  node [
    id 1397
    label "przybija&#263;"
  ]
  node [
    id 1398
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1399
    label "dodawa&#263;"
  ]
  node [
    id 1400
    label "cofa&#263;"
  ]
  node [
    id 1401
    label "przyswaja&#263;"
  ]
  node [
    id 1402
    label "wrzuca&#263;"
  ]
  node [
    id 1403
    label "przychodzi&#263;"
  ]
  node [
    id 1404
    label "zdobywa&#263;"
  ]
  node [
    id 1405
    label "zag&#322;&#281;bia&#263;"
  ]
  node [
    id 1406
    label "nasadza&#263;"
  ]
  node [
    id 1407
    label "beat"
  ]
  node [
    id 1408
    label "tug"
  ]
  node [
    id 1409
    label "mie&#263;_gdzie&#347;"
  ]
  node [
    id 1410
    label "wprowadza&#263;"
  ]
  node [
    id 1411
    label "umieszcza&#263;"
  ]
  node [
    id 1412
    label "wprawia&#263;"
  ]
  node [
    id 1413
    label "wt&#322;acza&#263;"
  ]
  node [
    id 1414
    label "transgress"
  ]
  node [
    id 1415
    label "rozdrabnia&#263;"
  ]
  node [
    id 1416
    label "plotkowa&#263;"
  ]
  node [
    id 1417
    label "&#322;oi&#263;"
  ]
  node [
    id 1418
    label "overcharge"
  ]
  node [
    id 1419
    label "okrada&#263;"
  ]
  node [
    id 1420
    label "obrabia&#263;_dup&#281;"
  ]
  node [
    id 1421
    label "wyka&#324;cza&#263;"
  ]
  node [
    id 1422
    label "rabowa&#263;"
  ]
  node [
    id 1423
    label "slur"
  ]
  node [
    id 1424
    label "poddawa&#263;"
  ]
  node [
    id 1425
    label "kucie"
  ]
  node [
    id 1426
    label "rzemie&#347;lnik"
  ]
  node [
    id 1427
    label "kowacz"
  ]
  node [
    id 1428
    label "pod&#322;u&#380;nik"
  ]
  node [
    id 1429
    label "wytrawialnia"
  ]
  node [
    id 1430
    label "odlewalnia"
  ]
  node [
    id 1431
    label "przebijarka"
  ]
  node [
    id 1432
    label "przeci&#261;gn&#261;&#263;"
  ]
  node [
    id 1433
    label "pogo"
  ]
  node [
    id 1434
    label "topialnia"
  ]
  node [
    id 1435
    label "metallic_element"
  ]
  node [
    id 1436
    label "naszywka"
  ]
  node [
    id 1437
    label "kuc"
  ]
  node [
    id 1438
    label "fan"
  ]
  node [
    id 1439
    label "orygina&#322;"
  ]
  node [
    id 1440
    label "pieszczocha"
  ]
  node [
    id 1441
    label "przeci&#261;gni&#281;cie"
  ]
  node [
    id 1442
    label "pogowa&#263;"
  ]
  node [
    id 1443
    label "wytrawia&#263;"
  ]
  node [
    id 1444
    label "rock"
  ]
  node [
    id 1445
    label "przedstawiciel"
  ]
  node [
    id 1446
    label "pi&#243;ra"
  ]
  node [
    id 1447
    label "hulk"
  ]
  node [
    id 1448
    label "statek_handlowy"
  ]
  node [
    id 1449
    label "nef"
  ]
  node [
    id 1450
    label "kasztel"
  ]
  node [
    id 1451
    label "&#380;aglowiec"
  ]
  node [
    id 1452
    label "ko&#322;kownica"
  ]
  node [
    id 1453
    label "statek"
  ]
  node [
    id 1454
    label "sejzing"
  ]
  node [
    id 1455
    label "piel&#281;gnicowate"
  ]
  node [
    id 1456
    label "takielunek"
  ]
  node [
    id 1457
    label "sztagownik"
  ]
  node [
    id 1458
    label "scalar"
  ]
  node [
    id 1459
    label "zamek"
  ]
  node [
    id 1460
    label "fluita"
  ]
  node [
    id 1461
    label "karaka"
  ]
  node [
    id 1462
    label "nadbud&#243;wka"
  ]
  node [
    id 1463
    label "poszycie_klepkowe"
  ]
  node [
    id 1464
    label "port"
  ]
  node [
    id 1465
    label "frachtowiec"
  ]
  node [
    id 1466
    label "poszycie_zak&#322;adkowe"
  ]
  node [
    id 1467
    label "jednomasztowiec"
  ]
  node [
    id 1468
    label "ster_wios&#322;owy"
  ]
  node [
    id 1469
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 1470
    label "welcome"
  ]
  node [
    id 1471
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 1472
    label "greet"
  ]
  node [
    id 1473
    label "pozdrawia&#263;"
  ]
  node [
    id 1474
    label "obchodzi&#263;"
  ]
  node [
    id 1475
    label "bless"
  ]
  node [
    id 1476
    label "energia"
  ]
  node [
    id 1477
    label "wedyzm"
  ]
  node [
    id 1478
    label "buddyzm"
  ]
  node [
    id 1479
    label "egzergia"
  ]
  node [
    id 1480
    label "emitowanie"
  ]
  node [
    id 1481
    label "power"
  ]
  node [
    id 1482
    label "zjawisko"
  ]
  node [
    id 1483
    label "emitowa&#263;"
  ]
  node [
    id 1484
    label "szwung"
  ]
  node [
    id 1485
    label "energy"
  ]
  node [
    id 1486
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1487
    label "kwant_energii"
  ]
  node [
    id 1488
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1489
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 1490
    label "hinduizm"
  ]
  node [
    id 1491
    label "Buddhism"
  ]
  node [
    id 1492
    label "wad&#378;rajana"
  ]
  node [
    id 1493
    label "arahant"
  ]
  node [
    id 1494
    label "tantryzm"
  ]
  node [
    id 1495
    label "therawada"
  ]
  node [
    id 1496
    label "mahajana"
  ]
  node [
    id 1497
    label "kalpa"
  ]
  node [
    id 1498
    label "li"
  ]
  node [
    id 1499
    label "bardo"
  ]
  node [
    id 1500
    label "dana"
  ]
  node [
    id 1501
    label "ahinsa"
  ]
  node [
    id 1502
    label "lampka_ma&#347;lana"
  ]
  node [
    id 1503
    label "asura"
  ]
  node [
    id 1504
    label "hinajana"
  ]
  node [
    id 1505
    label "bonzo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 0
    target 401
  ]
  edge [
    source 0
    target 402
  ]
  edge [
    source 0
    target 403
  ]
  edge [
    source 0
    target 404
  ]
  edge [
    source 0
    target 405
  ]
  edge [
    source 0
    target 406
  ]
  edge [
    source 0
    target 407
  ]
  edge [
    source 0
    target 408
  ]
  edge [
    source 0
    target 409
  ]
  edge [
    source 0
    target 410
  ]
  edge [
    source 0
    target 411
  ]
  edge [
    source 0
    target 412
  ]
  edge [
    source 0
    target 413
  ]
  edge [
    source 0
    target 414
  ]
  edge [
    source 0
    target 415
  ]
  edge [
    source 0
    target 416
  ]
  edge [
    source 0
    target 417
  ]
  edge [
    source 0
    target 418
  ]
  edge [
    source 0
    target 419
  ]
  edge [
    source 0
    target 420
  ]
  edge [
    source 0
    target 421
  ]
  edge [
    source 0
    target 422
  ]
  edge [
    source 0
    target 423
  ]
  edge [
    source 0
    target 424
  ]
  edge [
    source 0
    target 425
  ]
  edge [
    source 0
    target 426
  ]
  edge [
    source 0
    target 427
  ]
  edge [
    source 0
    target 428
  ]
  edge [
    source 0
    target 429
  ]
  edge [
    source 0
    target 430
  ]
  edge [
    source 0
    target 431
  ]
  edge [
    source 0
    target 432
  ]
  edge [
    source 0
    target 433
  ]
  edge [
    source 0
    target 434
  ]
  edge [
    source 0
    target 435
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 436
  ]
  edge [
    source 1
    target 437
  ]
  edge [
    source 1
    target 438
  ]
  edge [
    source 1
    target 439
  ]
  edge [
    source 1
    target 440
  ]
  edge [
    source 1
    target 441
  ]
  edge [
    source 1
    target 442
  ]
  edge [
    source 1
    target 443
  ]
  edge [
    source 1
    target 444
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 445
  ]
  edge [
    source 1
    target 446
  ]
  edge [
    source 1
    target 447
  ]
  edge [
    source 1
    target 448
  ]
  edge [
    source 1
    target 449
  ]
  edge [
    source 1
    target 450
  ]
  edge [
    source 1
    target 451
  ]
  edge [
    source 1
    target 452
  ]
  edge [
    source 1
    target 453
  ]
  edge [
    source 1
    target 454
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 455
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 456
  ]
  edge [
    source 1
    target 457
  ]
  edge [
    source 1
    target 458
  ]
  edge [
    source 1
    target 459
  ]
  edge [
    source 1
    target 460
  ]
  edge [
    source 1
    target 461
  ]
  edge [
    source 1
    target 462
  ]
  edge [
    source 1
    target 463
  ]
  edge [
    source 1
    target 464
  ]
  edge [
    source 1
    target 465
  ]
  edge [
    source 1
    target 466
  ]
  edge [
    source 1
    target 467
  ]
  edge [
    source 1
    target 468
  ]
  edge [
    source 1
    target 469
  ]
  edge [
    source 1
    target 470
  ]
  edge [
    source 1
    target 471
  ]
  edge [
    source 1
    target 472
  ]
  edge [
    source 1
    target 473
  ]
  edge [
    source 1
    target 474
  ]
  edge [
    source 1
    target 475
  ]
  edge [
    source 1
    target 476
  ]
  edge [
    source 1
    target 477
  ]
  edge [
    source 1
    target 478
  ]
  edge [
    source 1
    target 479
  ]
  edge [
    source 1
    target 480
  ]
  edge [
    source 1
    target 481
  ]
  edge [
    source 1
    target 482
  ]
  edge [
    source 1
    target 483
  ]
  edge [
    source 1
    target 484
  ]
  edge [
    source 1
    target 485
  ]
  edge [
    source 1
    target 486
  ]
  edge [
    source 1
    target 487
  ]
  edge [
    source 1
    target 488
  ]
  edge [
    source 1
    target 489
  ]
  edge [
    source 1
    target 490
  ]
  edge [
    source 1
    target 491
  ]
  edge [
    source 1
    target 492
  ]
  edge [
    source 1
    target 493
  ]
  edge [
    source 1
    target 494
  ]
  edge [
    source 1
    target 495
  ]
  edge [
    source 1
    target 496
  ]
  edge [
    source 1
    target 497
  ]
  edge [
    source 1
    target 498
  ]
  edge [
    source 1
    target 499
  ]
  edge [
    source 1
    target 500
  ]
  edge [
    source 1
    target 501
  ]
  edge [
    source 1
    target 502
  ]
  edge [
    source 1
    target 503
  ]
  edge [
    source 1
    target 504
  ]
  edge [
    source 1
    target 505
  ]
  edge [
    source 1
    target 506
  ]
  edge [
    source 1
    target 507
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 209
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 771
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 773
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 14
    target 777
  ]
  edge [
    source 14
    target 778
  ]
  edge [
    source 14
    target 779
  ]
  edge [
    source 14
    target 780
  ]
  edge [
    source 14
    target 781
  ]
  edge [
    source 14
    target 782
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 784
  ]
  edge [
    source 16
    target 785
  ]
  edge [
    source 16
    target 550
  ]
  edge [
    source 16
    target 786
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 787
  ]
  edge [
    source 16
    target 535
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 16
    target 777
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 779
  ]
  edge [
    source 16
    target 780
  ]
  edge [
    source 16
    target 781
  ]
  edge [
    source 16
    target 782
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 48
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 57
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 799
  ]
  edge [
    source 18
    target 800
  ]
  edge [
    source 18
    target 801
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 802
  ]
  edge [
    source 18
    target 803
  ]
  edge [
    source 18
    target 804
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 806
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 808
  ]
  edge [
    source 18
    target 809
  ]
  edge [
    source 18
    target 810
  ]
  edge [
    source 18
    target 811
  ]
  edge [
    source 18
    target 812
  ]
  edge [
    source 18
    target 101
  ]
  edge [
    source 18
    target 813
  ]
  edge [
    source 18
    target 814
  ]
  edge [
    source 18
    target 815
  ]
  edge [
    source 18
    target 816
  ]
  edge [
    source 18
    target 817
  ]
  edge [
    source 18
    target 818
  ]
  edge [
    source 18
    target 819
  ]
  edge [
    source 18
    target 820
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 821
  ]
  edge [
    source 18
    target 822
  ]
  edge [
    source 18
    target 823
  ]
  edge [
    source 18
    target 824
  ]
  edge [
    source 18
    target 825
  ]
  edge [
    source 18
    target 826
  ]
  edge [
    source 18
    target 827
  ]
  edge [
    source 18
    target 828
  ]
  edge [
    source 18
    target 829
  ]
  edge [
    source 18
    target 51
  ]
  edge [
    source 18
    target 830
  ]
  edge [
    source 18
    target 831
  ]
  edge [
    source 18
    target 57
  ]
  edge [
    source 18
    target 832
  ]
  edge [
    source 18
    target 833
  ]
  edge [
    source 18
    target 834
  ]
  edge [
    source 18
    target 209
  ]
  edge [
    source 18
    target 835
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 860
  ]
  edge [
    source 19
    target 861
  ]
  edge [
    source 19
    target 862
  ]
  edge [
    source 19
    target 863
  ]
  edge [
    source 19
    target 864
  ]
  edge [
    source 19
    target 865
  ]
  edge [
    source 19
    target 866
  ]
  edge [
    source 19
    target 867
  ]
  edge [
    source 19
    target 868
  ]
  edge [
    source 19
    target 869
  ]
  edge [
    source 19
    target 870
  ]
  edge [
    source 19
    target 871
  ]
  edge [
    source 19
    target 872
  ]
  edge [
    source 19
    target 873
  ]
  edge [
    source 19
    target 874
  ]
  edge [
    source 19
    target 875
  ]
  edge [
    source 19
    target 876
  ]
  edge [
    source 19
    target 877
  ]
  edge [
    source 19
    target 878
  ]
  edge [
    source 19
    target 879
  ]
  edge [
    source 19
    target 880
  ]
  edge [
    source 19
    target 881
  ]
  edge [
    source 19
    target 882
  ]
  edge [
    source 19
    target 883
  ]
  edge [
    source 19
    target 884
  ]
  edge [
    source 19
    target 885
  ]
  edge [
    source 19
    target 886
  ]
  edge [
    source 19
    target 887
  ]
  edge [
    source 19
    target 888
  ]
  edge [
    source 19
    target 889
  ]
  edge [
    source 19
    target 890
  ]
  edge [
    source 19
    target 891
  ]
  edge [
    source 19
    target 892
  ]
  edge [
    source 19
    target 893
  ]
  edge [
    source 19
    target 894
  ]
  edge [
    source 19
    target 895
  ]
  edge [
    source 19
    target 896
  ]
  edge [
    source 19
    target 897
  ]
  edge [
    source 19
    target 898
  ]
  edge [
    source 19
    target 899
  ]
  edge [
    source 19
    target 900
  ]
  edge [
    source 19
    target 901
  ]
  edge [
    source 19
    target 902
  ]
  edge [
    source 19
    target 903
  ]
  edge [
    source 19
    target 904
  ]
  edge [
    source 19
    target 905
  ]
  edge [
    source 19
    target 906
  ]
  edge [
    source 19
    target 907
  ]
  edge [
    source 19
    target 908
  ]
  edge [
    source 19
    target 909
  ]
  edge [
    source 19
    target 910
  ]
  edge [
    source 19
    target 911
  ]
  edge [
    source 19
    target 912
  ]
  edge [
    source 19
    target 913
  ]
  edge [
    source 19
    target 914
  ]
  edge [
    source 19
    target 915
  ]
  edge [
    source 19
    target 916
  ]
  edge [
    source 19
    target 917
  ]
  edge [
    source 19
    target 918
  ]
  edge [
    source 19
    target 919
  ]
  edge [
    source 19
    target 920
  ]
  edge [
    source 19
    target 921
  ]
  edge [
    source 19
    target 922
  ]
  edge [
    source 19
    target 923
  ]
  edge [
    source 19
    target 924
  ]
  edge [
    source 19
    target 925
  ]
  edge [
    source 19
    target 926
  ]
  edge [
    source 19
    target 927
  ]
  edge [
    source 19
    target 928
  ]
  edge [
    source 19
    target 929
  ]
  edge [
    source 19
    target 930
  ]
  edge [
    source 19
    target 931
  ]
  edge [
    source 19
    target 932
  ]
  edge [
    source 19
    target 933
  ]
  edge [
    source 19
    target 934
  ]
  edge [
    source 19
    target 935
  ]
  edge [
    source 19
    target 936
  ]
  edge [
    source 19
    target 937
  ]
  edge [
    source 19
    target 938
  ]
  edge [
    source 19
    target 939
  ]
  edge [
    source 19
    target 99
  ]
  edge [
    source 19
    target 940
  ]
  edge [
    source 19
    target 941
  ]
  edge [
    source 19
    target 942
  ]
  edge [
    source 19
    target 943
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 944
  ]
  edge [
    source 20
    target 945
  ]
  edge [
    source 20
    target 883
  ]
  edge [
    source 20
    target 946
  ]
  edge [
    source 20
    target 947
  ]
  edge [
    source 20
    target 948
  ]
  edge [
    source 20
    target 887
  ]
  edge [
    source 20
    target 949
  ]
  edge [
    source 20
    target 950
  ]
  edge [
    source 20
    target 951
  ]
  edge [
    source 20
    target 952
  ]
  edge [
    source 20
    target 953
  ]
  edge [
    source 20
    target 954
  ]
  edge [
    source 20
    target 955
  ]
  edge [
    source 20
    target 956
  ]
  edge [
    source 20
    target 957
  ]
  edge [
    source 20
    target 958
  ]
  edge [
    source 20
    target 959
  ]
  edge [
    source 20
    target 960
  ]
  edge [
    source 20
    target 961
  ]
  edge [
    source 20
    target 962
  ]
  edge [
    source 20
    target 963
  ]
  edge [
    source 20
    target 964
  ]
  edge [
    source 20
    target 965
  ]
  edge [
    source 20
    target 966
  ]
  edge [
    source 20
    target 967
  ]
  edge [
    source 20
    target 968
  ]
  edge [
    source 20
    target 969
  ]
  edge [
    source 20
    target 648
  ]
  edge [
    source 20
    target 970
  ]
  edge [
    source 20
    target 971
  ]
  edge [
    source 20
    target 972
  ]
  edge [
    source 20
    target 973
  ]
  edge [
    source 20
    target 974
  ]
  edge [
    source 20
    target 975
  ]
  edge [
    source 20
    target 976
  ]
  edge [
    source 20
    target 977
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 978
  ]
  edge [
    source 20
    target 979
  ]
  edge [
    source 20
    target 980
  ]
  edge [
    source 20
    target 981
  ]
  edge [
    source 20
    target 982
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 983
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 984
  ]
  edge [
    source 23
    target 985
  ]
  edge [
    source 23
    target 986
  ]
  edge [
    source 23
    target 987
  ]
  edge [
    source 23
    target 988
  ]
  edge [
    source 23
    target 989
  ]
  edge [
    source 23
    target 990
  ]
  edge [
    source 23
    target 753
  ]
  edge [
    source 23
    target 991
  ]
  edge [
    source 23
    target 992
  ]
  edge [
    source 23
    target 993
  ]
  edge [
    source 23
    target 994
  ]
  edge [
    source 23
    target 118
  ]
  edge [
    source 23
    target 186
  ]
  edge [
    source 23
    target 741
  ]
  edge [
    source 23
    target 995
  ]
  edge [
    source 23
    target 996
  ]
  edge [
    source 23
    target 997
  ]
  edge [
    source 23
    target 998
  ]
  edge [
    source 23
    target 999
  ]
  edge [
    source 23
    target 1000
  ]
  edge [
    source 23
    target 1001
  ]
  edge [
    source 23
    target 1002
  ]
  edge [
    source 23
    target 1003
  ]
  edge [
    source 23
    target 1004
  ]
  edge [
    source 23
    target 1005
  ]
  edge [
    source 23
    target 1006
  ]
  edge [
    source 23
    target 1007
  ]
  edge [
    source 23
    target 1008
  ]
  edge [
    source 23
    target 1009
  ]
  edge [
    source 23
    target 1010
  ]
  edge [
    source 23
    target 1011
  ]
  edge [
    source 23
    target 1012
  ]
  edge [
    source 23
    target 1013
  ]
  edge [
    source 23
    target 1014
  ]
  edge [
    source 23
    target 1015
  ]
  edge [
    source 23
    target 1016
  ]
  edge [
    source 23
    target 1017
  ]
  edge [
    source 23
    target 1018
  ]
  edge [
    source 23
    target 1019
  ]
  edge [
    source 23
    target 1020
  ]
  edge [
    source 23
    target 1021
  ]
  edge [
    source 23
    target 1022
  ]
  edge [
    source 23
    target 1023
  ]
  edge [
    source 23
    target 553
  ]
  edge [
    source 23
    target 1024
  ]
  edge [
    source 23
    target 1025
  ]
  edge [
    source 23
    target 1026
  ]
  edge [
    source 23
    target 1027
  ]
  edge [
    source 23
    target 1028
  ]
  edge [
    source 23
    target 1029
  ]
  edge [
    source 23
    target 1030
  ]
  edge [
    source 23
    target 1031
  ]
  edge [
    source 23
    target 1032
  ]
  edge [
    source 23
    target 554
  ]
  edge [
    source 23
    target 1033
  ]
  edge [
    source 23
    target 1034
  ]
  edge [
    source 23
    target 1035
  ]
  edge [
    source 23
    target 1036
  ]
  edge [
    source 23
    target 1037
  ]
  edge [
    source 23
    target 1038
  ]
  edge [
    source 23
    target 1039
  ]
  edge [
    source 23
    target 1040
  ]
  edge [
    source 23
    target 749
  ]
  edge [
    source 23
    target 1041
  ]
  edge [
    source 23
    target 1042
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1043
  ]
  edge [
    source 24
    target 1044
  ]
  edge [
    source 24
    target 1045
  ]
  edge [
    source 24
    target 1046
  ]
  edge [
    source 24
    target 1047
  ]
  edge [
    source 24
    target 1048
  ]
  edge [
    source 24
    target 1049
  ]
  edge [
    source 24
    target 1050
  ]
  edge [
    source 24
    target 1051
  ]
  edge [
    source 24
    target 1052
  ]
  edge [
    source 24
    target 1053
  ]
  edge [
    source 24
    target 1054
  ]
  edge [
    source 24
    target 1055
  ]
  edge [
    source 24
    target 1056
  ]
  edge [
    source 24
    target 1057
  ]
  edge [
    source 24
    target 1058
  ]
  edge [
    source 24
    target 1059
  ]
  edge [
    source 24
    target 1060
  ]
  edge [
    source 24
    target 1061
  ]
  edge [
    source 24
    target 87
  ]
  edge [
    source 24
    target 1062
  ]
  edge [
    source 24
    target 1063
  ]
  edge [
    source 24
    target 1064
  ]
  edge [
    source 24
    target 1065
  ]
  edge [
    source 24
    target 1066
  ]
  edge [
    source 24
    target 1067
  ]
  edge [
    source 24
    target 1068
  ]
  edge [
    source 24
    target 1069
  ]
  edge [
    source 24
    target 1070
  ]
  edge [
    source 24
    target 1071
  ]
  edge [
    source 24
    target 1072
  ]
  edge [
    source 24
    target 1073
  ]
  edge [
    source 24
    target 1074
  ]
  edge [
    source 24
    target 1075
  ]
  edge [
    source 24
    target 1076
  ]
  edge [
    source 24
    target 1077
  ]
  edge [
    source 24
    target 1078
  ]
  edge [
    source 24
    target 1079
  ]
  edge [
    source 24
    target 1080
  ]
  edge [
    source 24
    target 1081
  ]
  edge [
    source 24
    target 1082
  ]
  edge [
    source 24
    target 1083
  ]
  edge [
    source 24
    target 1084
  ]
  edge [
    source 24
    target 1085
  ]
  edge [
    source 24
    target 1086
  ]
  edge [
    source 24
    target 1087
  ]
  edge [
    source 24
    target 1088
  ]
  edge [
    source 24
    target 1089
  ]
  edge [
    source 24
    target 1090
  ]
  edge [
    source 24
    target 1091
  ]
  edge [
    source 24
    target 1092
  ]
  edge [
    source 24
    target 1093
  ]
  edge [
    source 24
    target 1094
  ]
  edge [
    source 24
    target 1095
  ]
  edge [
    source 24
    target 1096
  ]
  edge [
    source 24
    target 1097
  ]
  edge [
    source 24
    target 1098
  ]
  edge [
    source 24
    target 1099
  ]
  edge [
    source 24
    target 1100
  ]
  edge [
    source 24
    target 1101
  ]
  edge [
    source 24
    target 1102
  ]
  edge [
    source 24
    target 1103
  ]
  edge [
    source 24
    target 1104
  ]
  edge [
    source 24
    target 1105
  ]
  edge [
    source 24
    target 1106
  ]
  edge [
    source 24
    target 1107
  ]
  edge [
    source 24
    target 1108
  ]
  edge [
    source 24
    target 1109
  ]
  edge [
    source 24
    target 1110
  ]
  edge [
    source 24
    target 1111
  ]
  edge [
    source 24
    target 1112
  ]
  edge [
    source 24
    target 1113
  ]
  edge [
    source 24
    target 1114
  ]
  edge [
    source 24
    target 1115
  ]
  edge [
    source 24
    target 1116
  ]
  edge [
    source 24
    target 1117
  ]
  edge [
    source 24
    target 1118
  ]
  edge [
    source 24
    target 1119
  ]
  edge [
    source 24
    target 1120
  ]
  edge [
    source 24
    target 1121
  ]
  edge [
    source 24
    target 1122
  ]
  edge [
    source 24
    target 1123
  ]
  edge [
    source 24
    target 1124
  ]
  edge [
    source 24
    target 1125
  ]
  edge [
    source 24
    target 1126
  ]
  edge [
    source 24
    target 1127
  ]
  edge [
    source 24
    target 1128
  ]
  edge [
    source 24
    target 1129
  ]
  edge [
    source 24
    target 1130
  ]
  edge [
    source 24
    target 1131
  ]
  edge [
    source 24
    target 1132
  ]
  edge [
    source 24
    target 1133
  ]
  edge [
    source 24
    target 1134
  ]
  edge [
    source 24
    target 1135
  ]
  edge [
    source 24
    target 1136
  ]
  edge [
    source 24
    target 1137
  ]
  edge [
    source 24
    target 1138
  ]
  edge [
    source 24
    target 1139
  ]
  edge [
    source 24
    target 1140
  ]
  edge [
    source 24
    target 1141
  ]
  edge [
    source 24
    target 1142
  ]
  edge [
    source 24
    target 1143
  ]
  edge [
    source 24
    target 1144
  ]
  edge [
    source 24
    target 1145
  ]
  edge [
    source 24
    target 1146
  ]
  edge [
    source 24
    target 1147
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1148
  ]
  edge [
    source 25
    target 1149
  ]
  edge [
    source 25
    target 1150
  ]
  edge [
    source 25
    target 1151
  ]
  edge [
    source 25
    target 1152
  ]
  edge [
    source 25
    target 781
  ]
  edge [
    source 25
    target 1153
  ]
  edge [
    source 25
    target 1073
  ]
  edge [
    source 25
    target 1074
  ]
  edge [
    source 25
    target 1154
  ]
  edge [
    source 25
    target 1076
  ]
  edge [
    source 25
    target 1155
  ]
  edge [
    source 25
    target 1156
  ]
  edge [
    source 25
    target 1157
  ]
  edge [
    source 25
    target 1158
  ]
  edge [
    source 25
    target 1159
  ]
  edge [
    source 25
    target 502
  ]
  edge [
    source 25
    target 1160
  ]
  edge [
    source 25
    target 1161
  ]
  edge [
    source 25
    target 1109
  ]
  edge [
    source 25
    target 1162
  ]
  edge [
    source 25
    target 1163
  ]
  edge [
    source 25
    target 1164
  ]
  edge [
    source 25
    target 1165
  ]
  edge [
    source 25
    target 1166
  ]
  edge [
    source 25
    target 1167
  ]
  edge [
    source 25
    target 1168
  ]
  edge [
    source 25
    target 644
  ]
  edge [
    source 25
    target 1169
  ]
  edge [
    source 25
    target 1170
  ]
  edge [
    source 25
    target 1171
  ]
  edge [
    source 25
    target 1172
  ]
  edge [
    source 25
    target 1173
  ]
  edge [
    source 25
    target 1174
  ]
  edge [
    source 25
    target 1175
  ]
  edge [
    source 25
    target 786
  ]
  edge [
    source 25
    target 1176
  ]
  edge [
    source 25
    target 1177
  ]
  edge [
    source 25
    target 1178
  ]
  edge [
    source 25
    target 1179
  ]
  edge [
    source 25
    target 1180
  ]
  edge [
    source 25
    target 1056
  ]
  edge [
    source 25
    target 1181
  ]
  edge [
    source 25
    target 1123
  ]
  edge [
    source 25
    target 1182
  ]
  edge [
    source 25
    target 1183
  ]
  edge [
    source 25
    target 436
  ]
  edge [
    source 25
    target 1184
  ]
  edge [
    source 25
    target 1185
  ]
  edge [
    source 25
    target 1084
  ]
  edge [
    source 25
    target 1186
  ]
  edge [
    source 25
    target 1187
  ]
  edge [
    source 25
    target 1087
  ]
  edge [
    source 25
    target 1188
  ]
  edge [
    source 25
    target 1052
  ]
  edge [
    source 25
    target 1043
  ]
  edge [
    source 25
    target 1093
  ]
  edge [
    source 25
    target 1094
  ]
  edge [
    source 25
    target 1096
  ]
  edge [
    source 25
    target 1189
  ]
  edge [
    source 25
    target 1190
  ]
  edge [
    source 25
    target 1191
  ]
  edge [
    source 25
    target 1192
  ]
  edge [
    source 25
    target 1193
  ]
  edge [
    source 25
    target 1194
  ]
  edge [
    source 25
    target 1060
  ]
  edge [
    source 25
    target 1195
  ]
  edge [
    source 25
    target 1196
  ]
  edge [
    source 25
    target 1197
  ]
  edge [
    source 25
    target 1198
  ]
  edge [
    source 25
    target 1199
  ]
  edge [
    source 25
    target 1200
  ]
  edge [
    source 25
    target 1201
  ]
  edge [
    source 25
    target 1202
  ]
  edge [
    source 25
    target 1203
  ]
  edge [
    source 25
    target 1107
  ]
  edge [
    source 25
    target 1081
  ]
  edge [
    source 25
    target 1204
  ]
  edge [
    source 25
    target 535
  ]
  edge [
    source 25
    target 1205
  ]
  edge [
    source 25
    target 1206
  ]
  edge [
    source 25
    target 1207
  ]
  edge [
    source 25
    target 1208
  ]
  edge [
    source 25
    target 1209
  ]
  edge [
    source 25
    target 1210
  ]
  edge [
    source 25
    target 1211
  ]
  edge [
    source 25
    target 1212
  ]
  edge [
    source 25
    target 1213
  ]
  edge [
    source 25
    target 1214
  ]
  edge [
    source 25
    target 1215
  ]
  edge [
    source 25
    target 1216
  ]
  edge [
    source 25
    target 1217
  ]
  edge [
    source 25
    target 1218
  ]
  edge [
    source 25
    target 1219
  ]
  edge [
    source 25
    target 1220
  ]
  edge [
    source 25
    target 1221
  ]
  edge [
    source 25
    target 1222
  ]
  edge [
    source 25
    target 1223
  ]
  edge [
    source 25
    target 1224
  ]
  edge [
    source 25
    target 1225
  ]
  edge [
    source 25
    target 1226
  ]
  edge [
    source 25
    target 1227
  ]
  edge [
    source 25
    target 1228
  ]
  edge [
    source 25
    target 1229
  ]
  edge [
    source 25
    target 1230
  ]
  edge [
    source 25
    target 1231
  ]
  edge [
    source 25
    target 1232
  ]
  edge [
    source 25
    target 1233
  ]
  edge [
    source 25
    target 1234
  ]
  edge [
    source 25
    target 1235
  ]
  edge [
    source 25
    target 1236
  ]
  edge [
    source 25
    target 1237
  ]
  edge [
    source 25
    target 1238
  ]
  edge [
    source 25
    target 1239
  ]
  edge [
    source 25
    target 1240
  ]
  edge [
    source 25
    target 1241
  ]
  edge [
    source 25
    target 1242
  ]
  edge [
    source 25
    target 1243
  ]
  edge [
    source 25
    target 741
  ]
  edge [
    source 25
    target 1244
  ]
  edge [
    source 25
    target 1082
  ]
  edge [
    source 25
    target 1245
  ]
  edge [
    source 25
    target 1246
  ]
  edge [
    source 25
    target 1247
  ]
  edge [
    source 25
    target 1248
  ]
  edge [
    source 25
    target 1249
  ]
  edge [
    source 25
    target 1250
  ]
  edge [
    source 25
    target 1251
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 571
  ]
  edge [
    source 30
    target 1252
  ]
  edge [
    source 30
    target 1253
  ]
  edge [
    source 30
    target 805
  ]
  edge [
    source 30
    target 1254
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1255
  ]
  edge [
    source 31
    target 1256
  ]
  edge [
    source 31
    target 1257
  ]
  edge [
    source 31
    target 1258
  ]
  edge [
    source 31
    target 1259
  ]
  edge [
    source 31
    target 1260
  ]
  edge [
    source 31
    target 1261
  ]
  edge [
    source 31
    target 1262
  ]
  edge [
    source 31
    target 1263
  ]
  edge [
    source 31
    target 1264
  ]
  edge [
    source 31
    target 1265
  ]
  edge [
    source 31
    target 1266
  ]
  edge [
    source 31
    target 1267
  ]
  edge [
    source 31
    target 1268
  ]
  edge [
    source 31
    target 1269
  ]
  edge [
    source 31
    target 1270
  ]
  edge [
    source 31
    target 1271
  ]
  edge [
    source 31
    target 1272
  ]
  edge [
    source 31
    target 1273
  ]
  edge [
    source 31
    target 1274
  ]
  edge [
    source 31
    target 1275
  ]
  edge [
    source 31
    target 1276
  ]
  edge [
    source 31
    target 1277
  ]
  edge [
    source 31
    target 1278
  ]
  edge [
    source 31
    target 1279
  ]
  edge [
    source 31
    target 1280
  ]
  edge [
    source 31
    target 1281
  ]
  edge [
    source 31
    target 1282
  ]
  edge [
    source 31
    target 1283
  ]
  edge [
    source 31
    target 1284
  ]
  edge [
    source 31
    target 1285
  ]
  edge [
    source 31
    target 1286
  ]
  edge [
    source 31
    target 1287
  ]
  edge [
    source 31
    target 1288
  ]
  edge [
    source 31
    target 1289
  ]
  edge [
    source 31
    target 1290
  ]
  edge [
    source 31
    target 1291
  ]
  edge [
    source 31
    target 1292
  ]
  edge [
    source 31
    target 1293
  ]
  edge [
    source 31
    target 1294
  ]
  edge [
    source 31
    target 527
  ]
  edge [
    source 31
    target 1295
  ]
  edge [
    source 31
    target 1296
  ]
  edge [
    source 31
    target 1297
  ]
  edge [
    source 31
    target 1298
  ]
  edge [
    source 31
    target 1299
  ]
  edge [
    source 31
    target 1300
  ]
  edge [
    source 31
    target 1301
  ]
  edge [
    source 31
    target 1302
  ]
  edge [
    source 31
    target 1303
  ]
  edge [
    source 31
    target 1304
  ]
  edge [
    source 31
    target 1305
  ]
  edge [
    source 31
    target 1306
  ]
  edge [
    source 31
    target 1307
  ]
  edge [
    source 31
    target 1308
  ]
  edge [
    source 31
    target 1309
  ]
  edge [
    source 31
    target 1310
  ]
  edge [
    source 31
    target 1311
  ]
  edge [
    source 31
    target 1312
  ]
  edge [
    source 31
    target 1313
  ]
  edge [
    source 31
    target 1314
  ]
  edge [
    source 31
    target 1315
  ]
  edge [
    source 32
    target 1316
  ]
  edge [
    source 32
    target 1317
  ]
  edge [
    source 32
    target 1318
  ]
  edge [
    source 32
    target 1319
  ]
  edge [
    source 32
    target 1320
  ]
  edge [
    source 32
    target 1321
  ]
  edge [
    source 32
    target 1322
  ]
  edge [
    source 32
    target 1323
  ]
  edge [
    source 32
    target 1095
  ]
  edge [
    source 32
    target 1324
  ]
  edge [
    source 32
    target 1325
  ]
  edge [
    source 32
    target 1326
  ]
  edge [
    source 32
    target 1327
  ]
  edge [
    source 32
    target 1328
  ]
  edge [
    source 32
    target 1329
  ]
  edge [
    source 32
    target 1330
  ]
  edge [
    source 32
    target 1331
  ]
  edge [
    source 32
    target 1290
  ]
  edge [
    source 32
    target 1332
  ]
  edge [
    source 32
    target 1333
  ]
  edge [
    source 32
    target 1158
  ]
  edge [
    source 32
    target 1334
  ]
  edge [
    source 32
    target 1335
  ]
  edge [
    source 32
    target 1336
  ]
  edge [
    source 32
    target 1337
  ]
  edge [
    source 32
    target 1338
  ]
  edge [
    source 32
    target 1286
  ]
  edge [
    source 32
    target 1339
  ]
  edge [
    source 32
    target 1340
  ]
  edge [
    source 32
    target 1341
  ]
  edge [
    source 32
    target 1342
  ]
  edge [
    source 32
    target 1343
  ]
  edge [
    source 32
    target 1344
  ]
  edge [
    source 32
    target 1345
  ]
  edge [
    source 32
    target 1346
  ]
  edge [
    source 32
    target 1347
  ]
  edge [
    source 32
    target 1348
  ]
  edge [
    source 32
    target 1349
  ]
  edge [
    source 32
    target 1350
  ]
  edge [
    source 32
    target 1351
  ]
  edge [
    source 32
    target 1352
  ]
  edge [
    source 32
    target 1353
  ]
  edge [
    source 32
    target 1065
  ]
  edge [
    source 32
    target 1354
  ]
  edge [
    source 32
    target 1355
  ]
  edge [
    source 32
    target 1356
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1056
  ]
  edge [
    source 34
    target 1357
  ]
  edge [
    source 34
    target 1358
  ]
  edge [
    source 34
    target 1359
  ]
  edge [
    source 34
    target 1360
  ]
  edge [
    source 34
    target 1050
  ]
  edge [
    source 34
    target 1361
  ]
  edge [
    source 34
    target 1362
  ]
  edge [
    source 34
    target 1105
  ]
  edge [
    source 34
    target 1363
  ]
  edge [
    source 34
    target 1123
  ]
  edge [
    source 34
    target 1102
  ]
  edge [
    source 34
    target 1108
  ]
  edge [
    source 34
    target 1165
  ]
  edge [
    source 34
    target 1364
  ]
  edge [
    source 34
    target 1365
  ]
  edge [
    source 34
    target 1366
  ]
  edge [
    source 34
    target 1082
  ]
  edge [
    source 34
    target 1367
  ]
  edge [
    source 34
    target 1168
  ]
  edge [
    source 34
    target 1163
  ]
  edge [
    source 34
    target 1074
  ]
  edge [
    source 34
    target 1368
  ]
  edge [
    source 34
    target 1369
  ]
  edge [
    source 34
    target 1370
  ]
  edge [
    source 34
    target 1371
  ]
  edge [
    source 34
    target 1372
  ]
  edge [
    source 34
    target 966
  ]
  edge [
    source 34
    target 1373
  ]
  edge [
    source 34
    target 1374
  ]
  edge [
    source 34
    target 1375
  ]
  edge [
    source 34
    target 1376
  ]
  edge [
    source 34
    target 1043
  ]
  edge [
    source 34
    target 1088
  ]
  edge [
    source 34
    target 1377
  ]
  edge [
    source 34
    target 1378
  ]
  edge [
    source 34
    target 1379
  ]
  edge [
    source 34
    target 1380
  ]
  edge [
    source 34
    target 1381
  ]
  edge [
    source 34
    target 1382
  ]
  edge [
    source 34
    target 1383
  ]
  edge [
    source 34
    target 1384
  ]
  edge [
    source 34
    target 1385
  ]
  edge [
    source 34
    target 1258
  ]
  edge [
    source 34
    target 1386
  ]
  edge [
    source 34
    target 1387
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1094
  ]
  edge [
    source 36
    target 1388
  ]
  edge [
    source 36
    target 570
  ]
  edge [
    source 36
    target 1389
  ]
  edge [
    source 36
    target 1390
  ]
  edge [
    source 36
    target 1391
  ]
  edge [
    source 36
    target 1392
  ]
  edge [
    source 36
    target 1393
  ]
  edge [
    source 36
    target 1394
  ]
  edge [
    source 36
    target 1395
  ]
  edge [
    source 36
    target 1396
  ]
  edge [
    source 36
    target 1397
  ]
  edge [
    source 36
    target 1398
  ]
  edge [
    source 36
    target 1399
  ]
  edge [
    source 36
    target 1400
  ]
  edge [
    source 36
    target 1401
  ]
  edge [
    source 36
    target 1402
  ]
  edge [
    source 36
    target 1403
  ]
  edge [
    source 36
    target 1132
  ]
  edge [
    source 36
    target 1047
  ]
  edge [
    source 36
    target 1404
  ]
  edge [
    source 36
    target 1405
  ]
  edge [
    source 36
    target 1406
  ]
  edge [
    source 36
    target 1407
  ]
  edge [
    source 36
    target 1408
  ]
  edge [
    source 36
    target 1409
  ]
  edge [
    source 36
    target 1410
  ]
  edge [
    source 36
    target 1411
  ]
  edge [
    source 36
    target 1412
  ]
  edge [
    source 36
    target 1413
  ]
  edge [
    source 36
    target 1414
  ]
  edge [
    source 36
    target 1415
  ]
  edge [
    source 36
    target 1416
  ]
  edge [
    source 36
    target 1417
  ]
  edge [
    source 36
    target 1418
  ]
  edge [
    source 36
    target 1419
  ]
  edge [
    source 36
    target 1096
  ]
  edge [
    source 36
    target 1420
  ]
  edge [
    source 36
    target 1421
  ]
  edge [
    source 36
    target 1422
  ]
  edge [
    source 36
    target 1108
  ]
  edge [
    source 36
    target 1423
  ]
  edge [
    source 36
    target 1424
  ]
  edge [
    source 36
    target 1105
  ]
  edge [
    source 36
    target 1425
  ]
  edge [
    source 36
    target 1426
  ]
  edge [
    source 36
    target 1427
  ]
  edge [
    source 36
    target 1428
  ]
  edge [
    source 36
    target 569
  ]
  edge [
    source 36
    target 1429
  ]
  edge [
    source 36
    target 1430
  ]
  edge [
    source 36
    target 1431
  ]
  edge [
    source 36
    target 1432
  ]
  edge [
    source 36
    target 1433
  ]
  edge [
    source 36
    target 1434
  ]
  edge [
    source 36
    target 660
  ]
  edge [
    source 36
    target 1435
  ]
  edge [
    source 36
    target 1436
  ]
  edge [
    source 36
    target 57
  ]
  edge [
    source 36
    target 1437
  ]
  edge [
    source 36
    target 1438
  ]
  edge [
    source 36
    target 1439
  ]
  edge [
    source 36
    target 1440
  ]
  edge [
    source 36
    target 1441
  ]
  edge [
    source 36
    target 1442
  ]
  edge [
    source 36
    target 1443
  ]
  edge [
    source 36
    target 1444
  ]
  edge [
    source 36
    target 1445
  ]
  edge [
    source 36
    target 1446
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1447
  ]
  edge [
    source 37
    target 1448
  ]
  edge [
    source 37
    target 1449
  ]
  edge [
    source 37
    target 1450
  ]
  edge [
    source 37
    target 1451
  ]
  edge [
    source 37
    target 655
  ]
  edge [
    source 37
    target 1452
  ]
  edge [
    source 37
    target 1453
  ]
  edge [
    source 37
    target 1454
  ]
  edge [
    source 37
    target 1455
  ]
  edge [
    source 37
    target 1456
  ]
  edge [
    source 37
    target 1457
  ]
  edge [
    source 37
    target 1458
  ]
  edge [
    source 37
    target 1459
  ]
  edge [
    source 37
    target 1460
  ]
  edge [
    source 37
    target 1461
  ]
  edge [
    source 37
    target 1462
  ]
  edge [
    source 37
    target 1463
  ]
  edge [
    source 37
    target 1464
  ]
  edge [
    source 37
    target 1465
  ]
  edge [
    source 37
    target 1466
  ]
  edge [
    source 37
    target 1467
  ]
  edge [
    source 37
    target 1468
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1469
  ]
  edge [
    source 39
    target 1470
  ]
  edge [
    source 39
    target 1471
  ]
  edge [
    source 39
    target 1472
  ]
  edge [
    source 39
    target 1473
  ]
  edge [
    source 39
    target 1043
  ]
  edge [
    source 39
    target 1474
  ]
  edge [
    source 39
    target 1475
  ]
  edge [
    source 40
    target 1476
  ]
  edge [
    source 40
    target 1477
  ]
  edge [
    source 40
    target 1478
  ]
  edge [
    source 40
    target 1479
  ]
  edge [
    source 40
    target 1480
  ]
  edge [
    source 40
    target 741
  ]
  edge [
    source 40
    target 1481
  ]
  edge [
    source 40
    target 1482
  ]
  edge [
    source 40
    target 1483
  ]
  edge [
    source 40
    target 1484
  ]
  edge [
    source 40
    target 1485
  ]
  edge [
    source 40
    target 1486
  ]
  edge [
    source 40
    target 1487
  ]
  edge [
    source 40
    target 1488
  ]
  edge [
    source 40
    target 1489
  ]
  edge [
    source 40
    target 1490
  ]
  edge [
    source 40
    target 1491
  ]
  edge [
    source 40
    target 1492
  ]
  edge [
    source 40
    target 1493
  ]
  edge [
    source 40
    target 1494
  ]
  edge [
    source 40
    target 1495
  ]
  edge [
    source 40
    target 1496
  ]
  edge [
    source 40
    target 1497
  ]
  edge [
    source 40
    target 1498
  ]
  edge [
    source 40
    target 1499
  ]
  edge [
    source 40
    target 1500
  ]
  edge [
    source 40
    target 1501
  ]
  edge [
    source 40
    target 870
  ]
  edge [
    source 40
    target 1502
  ]
  edge [
    source 40
    target 1503
  ]
  edge [
    source 40
    target 1504
  ]
  edge [
    source 40
    target 1505
  ]
]
