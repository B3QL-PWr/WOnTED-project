graph [
  node [
    id 0
    label "narazi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wykop"
    origin "text"
  ]
  node [
    id 2
    label "dobre"
    origin "text"
  ]
  node [
    id 3
    label "imi&#281;"
    origin "text"
  ]
  node [
    id 4
    label "sklep"
    origin "text"
  ]
  node [
    id 5
    label "sprzeda&#263;"
    origin "text"
  ]
  node [
    id 6
    label "samsunga"
    origin "text"
  ]
  node [
    id 7
    label "galaxy"
    origin "text"
  ]
  node [
    id 8
    label "krzywo"
    origin "text"
  ]
  node [
    id 9
    label "wklei&#263;"
    origin "text"
  ]
  node [
    id 10
    label "szk&#322;o"
    origin "text"
  ]
  node [
    id 11
    label "aparat"
    origin "text"
  ]
  node [
    id 12
    label "budowa"
  ]
  node [
    id 13
    label "zrzutowy"
  ]
  node [
    id 14
    label "odk&#322;ad"
  ]
  node [
    id 15
    label "chody"
  ]
  node [
    id 16
    label "szaniec"
  ]
  node [
    id 17
    label "wyrobisko"
  ]
  node [
    id 18
    label "kopniak"
  ]
  node [
    id 19
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 20
    label "odwa&#322;"
  ]
  node [
    id 21
    label "grodzisko"
  ]
  node [
    id 22
    label "cios"
  ]
  node [
    id 23
    label "kick"
  ]
  node [
    id 24
    label "kopni&#281;cie"
  ]
  node [
    id 25
    label "&#347;rodkowiec"
  ]
  node [
    id 26
    label "podsadzka"
  ]
  node [
    id 27
    label "obudowa"
  ]
  node [
    id 28
    label "sp&#261;g"
  ]
  node [
    id 29
    label "strop"
  ]
  node [
    id 30
    label "rabowarka"
  ]
  node [
    id 31
    label "opinka"
  ]
  node [
    id 32
    label "stojak_cierny"
  ]
  node [
    id 33
    label "kopalnia"
  ]
  node [
    id 34
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 35
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 36
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 37
    label "immersion"
  ]
  node [
    id 38
    label "umieszczenie"
  ]
  node [
    id 39
    label "las"
  ]
  node [
    id 40
    label "nora"
  ]
  node [
    id 41
    label "pies_my&#347;liwski"
  ]
  node [
    id 42
    label "miejsce"
  ]
  node [
    id 43
    label "trasa"
  ]
  node [
    id 44
    label "doj&#347;cie"
  ]
  node [
    id 45
    label "zesp&#243;&#322;"
  ]
  node [
    id 46
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 47
    label "horodyszcze"
  ]
  node [
    id 48
    label "Wyszogr&#243;d"
  ]
  node [
    id 49
    label "usypisko"
  ]
  node [
    id 50
    label "r&#243;w"
  ]
  node [
    id 51
    label "wa&#322;"
  ]
  node [
    id 52
    label "redoubt"
  ]
  node [
    id 53
    label "fortyfikacja"
  ]
  node [
    id 54
    label "mechanika"
  ]
  node [
    id 55
    label "struktura"
  ]
  node [
    id 56
    label "figura"
  ]
  node [
    id 57
    label "miejsce_pracy"
  ]
  node [
    id 58
    label "cecha"
  ]
  node [
    id 59
    label "organ"
  ]
  node [
    id 60
    label "kreacja"
  ]
  node [
    id 61
    label "zwierz&#281;"
  ]
  node [
    id 62
    label "posesja"
  ]
  node [
    id 63
    label "konstrukcja"
  ]
  node [
    id 64
    label "wjazd"
  ]
  node [
    id 65
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 66
    label "praca"
  ]
  node [
    id 67
    label "constitution"
  ]
  node [
    id 68
    label "gleba"
  ]
  node [
    id 69
    label "p&#281;d"
  ]
  node [
    id 70
    label "zbi&#243;r"
  ]
  node [
    id 71
    label "ablegier"
  ]
  node [
    id 72
    label "rozmna&#380;anie_bezp&#322;ciowe"
  ]
  node [
    id 73
    label "layer"
  ]
  node [
    id 74
    label "r&#243;j"
  ]
  node [
    id 75
    label "mrowisko"
  ]
  node [
    id 76
    label "reputacja"
  ]
  node [
    id 77
    label "deklinacja"
  ]
  node [
    id 78
    label "term"
  ]
  node [
    id 79
    label "personalia"
  ]
  node [
    id 80
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 81
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 82
    label "leksem"
  ]
  node [
    id 83
    label "wezwanie"
  ]
  node [
    id 84
    label "wielko&#347;&#263;"
  ]
  node [
    id 85
    label "nazwa_w&#322;asna"
  ]
  node [
    id 86
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 87
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 88
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 89
    label "patron"
  ]
  node [
    id 90
    label "imiennictwo"
  ]
  node [
    id 91
    label "wordnet"
  ]
  node [
    id 92
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 93
    label "wypowiedzenie"
  ]
  node [
    id 94
    label "morfem"
  ]
  node [
    id 95
    label "s&#322;ownictwo"
  ]
  node [
    id 96
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 97
    label "wykrzyknik"
  ]
  node [
    id 98
    label "pole_semantyczne"
  ]
  node [
    id 99
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 100
    label "pisanie_si&#281;"
  ]
  node [
    id 101
    label "nag&#322;os"
  ]
  node [
    id 102
    label "wyg&#322;os"
  ]
  node [
    id 103
    label "jednostka_leksykalna"
  ]
  node [
    id 104
    label "znaczenie"
  ]
  node [
    id 105
    label "opinia"
  ]
  node [
    id 106
    label "perversion"
  ]
  node [
    id 107
    label "k&#261;t"
  ]
  node [
    id 108
    label "poj&#281;cie"
  ]
  node [
    id 109
    label "fleksja"
  ]
  node [
    id 110
    label "&#322;uska"
  ]
  node [
    id 111
    label "opiekun"
  ]
  node [
    id 112
    label "patrycjusz"
  ]
  node [
    id 113
    label "prawnik"
  ]
  node [
    id 114
    label "nab&#243;j"
  ]
  node [
    id 115
    label "&#347;wi&#281;ty"
  ]
  node [
    id 116
    label "zmar&#322;y"
  ]
  node [
    id 117
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 118
    label "&#347;w"
  ]
  node [
    id 119
    label "nazwa"
  ]
  node [
    id 120
    label "szablon"
  ]
  node [
    id 121
    label "warunek_lokalowy"
  ]
  node [
    id 122
    label "rozmiar"
  ]
  node [
    id 123
    label "liczba"
  ]
  node [
    id 124
    label "rzadko&#347;&#263;"
  ]
  node [
    id 125
    label "zaleta"
  ]
  node [
    id 126
    label "ilo&#347;&#263;"
  ]
  node [
    id 127
    label "measure"
  ]
  node [
    id 128
    label "dymensja"
  ]
  node [
    id 129
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 130
    label "zdolno&#347;&#263;"
  ]
  node [
    id 131
    label "potencja"
  ]
  node [
    id 132
    label "property"
  ]
  node [
    id 133
    label "nakaz"
  ]
  node [
    id 134
    label "pismo_urz&#281;dowe"
  ]
  node [
    id 135
    label "zwo&#322;anie_si&#281;"
  ]
  node [
    id 136
    label "wst&#281;p"
  ]
  node [
    id 137
    label "pro&#347;ba"
  ]
  node [
    id 138
    label "nakazanie"
  ]
  node [
    id 139
    label "admonition"
  ]
  node [
    id 140
    label "summons"
  ]
  node [
    id 141
    label "poproszenie"
  ]
  node [
    id 142
    label "bid"
  ]
  node [
    id 143
    label "apostrofa"
  ]
  node [
    id 144
    label "zach&#281;cenie"
  ]
  node [
    id 145
    label "poinformowanie"
  ]
  node [
    id 146
    label "fitonimia"
  ]
  node [
    id 147
    label "ideonimia"
  ]
  node [
    id 148
    label "leksykologia"
  ]
  node [
    id 149
    label "etnonimia"
  ]
  node [
    id 150
    label "zas&#243;b"
  ]
  node [
    id 151
    label "antroponimia"
  ]
  node [
    id 152
    label "toponimia"
  ]
  node [
    id 153
    label "chrematonimia"
  ]
  node [
    id 154
    label "NN"
  ]
  node [
    id 155
    label "nazwisko"
  ]
  node [
    id 156
    label "adres"
  ]
  node [
    id 157
    label "dane"
  ]
  node [
    id 158
    label "to&#380;samo&#347;&#263;_osobista"
  ]
  node [
    id 159
    label "pesel"
  ]
  node [
    id 160
    label "p&#243;&#322;ka"
  ]
  node [
    id 161
    label "firma"
  ]
  node [
    id 162
    label "stoisko"
  ]
  node [
    id 163
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 164
    label "sk&#322;ad"
  ]
  node [
    id 165
    label "obiekt_handlowy"
  ]
  node [
    id 166
    label "zaplecze"
  ]
  node [
    id 167
    label "witryna"
  ]
  node [
    id 168
    label "Apeks"
  ]
  node [
    id 169
    label "zasoby"
  ]
  node [
    id 170
    label "cz&#322;owiek"
  ]
  node [
    id 171
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 172
    label "zaufanie"
  ]
  node [
    id 173
    label "Hortex"
  ]
  node [
    id 174
    label "reengineering"
  ]
  node [
    id 175
    label "podmiot_gospodarczy"
  ]
  node [
    id 176
    label "paczkarnia"
  ]
  node [
    id 177
    label "Orlen"
  ]
  node [
    id 178
    label "interes"
  ]
  node [
    id 179
    label "Google"
  ]
  node [
    id 180
    label "Canon"
  ]
  node [
    id 181
    label "Pewex"
  ]
  node [
    id 182
    label "MAN_SE"
  ]
  node [
    id 183
    label "Spo&#322;em"
  ]
  node [
    id 184
    label "klasa"
  ]
  node [
    id 185
    label "networking"
  ]
  node [
    id 186
    label "MAC"
  ]
  node [
    id 187
    label "zasoby_ludzkie"
  ]
  node [
    id 188
    label "Baltona"
  ]
  node [
    id 189
    label "Orbis"
  ]
  node [
    id 190
    label "biurowiec"
  ]
  node [
    id 191
    label "HP"
  ]
  node [
    id 192
    label "siedziba"
  ]
  node [
    id 193
    label "szyba"
  ]
  node [
    id 194
    label "okno"
  ]
  node [
    id 195
    label "YouTube"
  ]
  node [
    id 196
    label "wytw&#243;r"
  ]
  node [
    id 197
    label "gablota"
  ]
  node [
    id 198
    label "strona"
  ]
  node [
    id 199
    label "stela&#380;"
  ]
  node [
    id 200
    label "szafa"
  ]
  node [
    id 201
    label "mebel"
  ]
  node [
    id 202
    label "meblo&#347;cianka"
  ]
  node [
    id 203
    label "infrastruktura"
  ]
  node [
    id 204
    label "wyposa&#380;enie"
  ]
  node [
    id 205
    label "pomieszczenie"
  ]
  node [
    id 206
    label "blokada"
  ]
  node [
    id 207
    label "hurtownia"
  ]
  node [
    id 208
    label "pole"
  ]
  node [
    id 209
    label "pas"
  ]
  node [
    id 210
    label "basic"
  ]
  node [
    id 211
    label "sk&#322;adnik"
  ]
  node [
    id 212
    label "obr&#243;bka"
  ]
  node [
    id 213
    label "fabryka"
  ]
  node [
    id 214
    label "&#347;wiat&#322;o"
  ]
  node [
    id 215
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 216
    label "syf"
  ]
  node [
    id 217
    label "rank_and_file"
  ]
  node [
    id 218
    label "set"
  ]
  node [
    id 219
    label "tabulacja"
  ]
  node [
    id 220
    label "tekst"
  ]
  node [
    id 221
    label "sell"
  ]
  node [
    id 222
    label "zach&#281;ci&#263;"
  ]
  node [
    id 223
    label "op&#281;dzi&#263;"
  ]
  node [
    id 224
    label "odda&#263;"
  ]
  node [
    id 225
    label "give_birth"
  ]
  node [
    id 226
    label "zdradzi&#263;"
  ]
  node [
    id 227
    label "zhandlowa&#263;"
  ]
  node [
    id 228
    label "przekaza&#263;"
  ]
  node [
    id 229
    label "umie&#347;ci&#263;"
  ]
  node [
    id 230
    label "sacrifice"
  ]
  node [
    id 231
    label "da&#263;"
  ]
  node [
    id 232
    label "transfer"
  ]
  node [
    id 233
    label "give"
  ]
  node [
    id 234
    label "picture"
  ]
  node [
    id 235
    label "przedstawi&#263;"
  ]
  node [
    id 236
    label "zrobi&#263;"
  ]
  node [
    id 237
    label "reflect"
  ]
  node [
    id 238
    label "odst&#261;pi&#263;"
  ]
  node [
    id 239
    label "deliver"
  ]
  node [
    id 240
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 241
    label "restore"
  ]
  node [
    id 242
    label "odpowiedzie&#263;"
  ]
  node [
    id 243
    label "convey"
  ]
  node [
    id 244
    label "dostarczy&#263;"
  ]
  node [
    id 245
    label "z_powrotem"
  ]
  node [
    id 246
    label "rogi"
  ]
  node [
    id 247
    label "objawi&#263;"
  ]
  node [
    id 248
    label "poinformowa&#263;"
  ]
  node [
    id 249
    label "naruszy&#263;"
  ]
  node [
    id 250
    label "nabra&#263;"
  ]
  node [
    id 251
    label "denounce"
  ]
  node [
    id 252
    label "invite"
  ]
  node [
    id 253
    label "pozyska&#263;"
  ]
  node [
    id 254
    label "wymieni&#263;"
  ]
  node [
    id 255
    label "skorzysta&#263;"
  ]
  node [
    id 256
    label "poradzi&#263;_sobie"
  ]
  node [
    id 257
    label "spo&#380;y&#263;"
  ]
  node [
    id 258
    label "nie&#380;yczliwie"
  ]
  node [
    id 259
    label "znacz&#261;co"
  ]
  node [
    id 260
    label "krzywy"
  ]
  node [
    id 261
    label "&#378;le"
  ]
  node [
    id 262
    label "niemile"
  ]
  node [
    id 263
    label "niemi&#322;y"
  ]
  node [
    id 264
    label "znacz&#261;cy"
  ]
  node [
    id 265
    label "niech&#281;tny"
  ]
  node [
    id 266
    label "z&#322;y"
  ]
  node [
    id 267
    label "znacznie"
  ]
  node [
    id 268
    label "unpleasantly"
  ]
  node [
    id 269
    label "nieprzyjemny"
  ]
  node [
    id 270
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 271
    label "przeciwnie"
  ]
  node [
    id 272
    label "negatywnie"
  ]
  node [
    id 273
    label "nieprzyjazny"
  ]
  node [
    id 274
    label "niepomy&#347;lnie"
  ]
  node [
    id 275
    label "piesko"
  ]
  node [
    id 276
    label "niezgodnie"
  ]
  node [
    id 277
    label "gorzej"
  ]
  node [
    id 278
    label "niekorzystnie"
  ]
  node [
    id 279
    label "przyklei&#263;"
  ]
  node [
    id 280
    label "paste"
  ]
  node [
    id 281
    label "wgra&#263;"
  ]
  node [
    id 282
    label "wpisa&#263;"
  ]
  node [
    id 283
    label "przymocowa&#263;"
  ]
  node [
    id 284
    label "cleave"
  ]
  node [
    id 285
    label "naczynie"
  ]
  node [
    id 286
    label "kawa&#322;ek"
  ]
  node [
    id 287
    label "alkohol"
  ]
  node [
    id 288
    label "zeszklenie"
  ]
  node [
    id 289
    label "zeszklenie_si&#281;"
  ]
  node [
    id 290
    label "szklarstwo"
  ]
  node [
    id 291
    label "zastawa"
  ]
  node [
    id 292
    label "substancja"
  ]
  node [
    id 293
    label "lufka"
  ]
  node [
    id 294
    label "krajalno&#347;&#263;"
  ]
  node [
    id 295
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 296
    label "vessel"
  ]
  node [
    id 297
    label "sprz&#281;t"
  ]
  node [
    id 298
    label "statki"
  ]
  node [
    id 299
    label "rewaskularyzacja"
  ]
  node [
    id 300
    label "ceramika"
  ]
  node [
    id 301
    label "drewno"
  ]
  node [
    id 302
    label "przew&#243;d"
  ]
  node [
    id 303
    label "unaczyni&#263;"
  ]
  node [
    id 304
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 305
    label "receptacle"
  ]
  node [
    id 306
    label "shot"
  ]
  node [
    id 307
    label "ustnik"
  ]
  node [
    id 308
    label "dulawka"
  ]
  node [
    id 309
    label "w&#243;dka"
  ]
  node [
    id 310
    label "kieliszek"
  ]
  node [
    id 311
    label "przenikanie"
  ]
  node [
    id 312
    label "byt"
  ]
  node [
    id 313
    label "materia"
  ]
  node [
    id 314
    label "cz&#261;steczka"
  ]
  node [
    id 315
    label "temperatura_krytyczna"
  ]
  node [
    id 316
    label "przenika&#263;"
  ]
  node [
    id 317
    label "smolisty"
  ]
  node [
    id 318
    label "element_wyposa&#380;enia"
  ]
  node [
    id 319
    label "sto&#322;owizna"
  ]
  node [
    id 320
    label "kawa&#322;"
  ]
  node [
    id 321
    label "plot"
  ]
  node [
    id 322
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 323
    label "utw&#243;r"
  ]
  node [
    id 324
    label "piece"
  ]
  node [
    id 325
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 326
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 327
    label "podp&#322;ywanie"
  ]
  node [
    id 328
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 329
    label "przemiana_fazowa"
  ]
  node [
    id 330
    label "spowodowanie"
  ]
  node [
    id 331
    label "usma&#380;enie"
  ]
  node [
    id 332
    label "czynno&#347;&#263;"
  ]
  node [
    id 333
    label "rzemios&#322;o"
  ]
  node [
    id 334
    label "materia&#322;"
  ]
  node [
    id 335
    label "blacha"
  ]
  node [
    id 336
    label "u&#380;ywka"
  ]
  node [
    id 337
    label "najebka"
  ]
  node [
    id 338
    label "upajanie"
  ]
  node [
    id 339
    label "wypicie"
  ]
  node [
    id 340
    label "rozgrzewacz"
  ]
  node [
    id 341
    label "nap&#243;j"
  ]
  node [
    id 342
    label "alko"
  ]
  node [
    id 343
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 344
    label "picie"
  ]
  node [
    id 345
    label "upojenie"
  ]
  node [
    id 346
    label "g&#322;owa"
  ]
  node [
    id 347
    label "upija&#263;"
  ]
  node [
    id 348
    label "likwor"
  ]
  node [
    id 349
    label "poniewierca"
  ]
  node [
    id 350
    label "grupa_hydroksylowa"
  ]
  node [
    id 351
    label "spirytualia"
  ]
  node [
    id 352
    label "le&#380;akownia"
  ]
  node [
    id 353
    label "upi&#263;"
  ]
  node [
    id 354
    label "piwniczka"
  ]
  node [
    id 355
    label "gorzelnia_rolnicza"
  ]
  node [
    id 356
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 357
    label "zadzwoni&#263;"
  ]
  node [
    id 358
    label "zi&#243;&#322;ko"
  ]
  node [
    id 359
    label "mat&#243;wka"
  ]
  node [
    id 360
    label "celownik"
  ]
  node [
    id 361
    label "przyrz&#261;d"
  ]
  node [
    id 362
    label "obiektyw"
  ]
  node [
    id 363
    label "odleg&#322;o&#347;&#263;_hiperfokalna"
  ]
  node [
    id 364
    label "lampa_b&#322;yskowa"
  ]
  node [
    id 365
    label "mikrotelefon"
  ]
  node [
    id 366
    label "orygina&#322;"
  ]
  node [
    id 367
    label "facet"
  ]
  node [
    id 368
    label "w&#322;adza"
  ]
  node [
    id 369
    label "miech"
  ]
  node [
    id 370
    label "dzwoni&#263;"
  ]
  node [
    id 371
    label "ciemnia_optyczna"
  ]
  node [
    id 372
    label "spust"
  ]
  node [
    id 373
    label "wyzwalacz"
  ]
  node [
    id 374
    label "dekielek"
  ]
  node [
    id 375
    label "wy&#347;wietlacz"
  ]
  node [
    id 376
    label "device"
  ]
  node [
    id 377
    label "dzwonienie"
  ]
  node [
    id 378
    label "migawka"
  ]
  node [
    id 379
    label "aparatownia"
  ]
  node [
    id 380
    label "urz&#261;dzenie"
  ]
  node [
    id 381
    label "Mazowsze"
  ]
  node [
    id 382
    label "odm&#322;adzanie"
  ]
  node [
    id 383
    label "&#346;wietliki"
  ]
  node [
    id 384
    label "whole"
  ]
  node [
    id 385
    label "skupienie"
  ]
  node [
    id 386
    label "The_Beatles"
  ]
  node [
    id 387
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 388
    label "odm&#322;adza&#263;"
  ]
  node [
    id 389
    label "zabudowania"
  ]
  node [
    id 390
    label "group"
  ]
  node [
    id 391
    label "zespolik"
  ]
  node [
    id 392
    label "schorzenie"
  ]
  node [
    id 393
    label "ro&#347;lina"
  ]
  node [
    id 394
    label "grupa"
  ]
  node [
    id 395
    label "Depeche_Mode"
  ]
  node [
    id 396
    label "batch"
  ]
  node [
    id 397
    label "odm&#322;odzenie"
  ]
  node [
    id 398
    label "utensylia"
  ]
  node [
    id 399
    label "narz&#281;dzie"
  ]
  node [
    id 400
    label "przedmiot"
  ]
  node [
    id 401
    label "kom&#243;rka"
  ]
  node [
    id 402
    label "furnishing"
  ]
  node [
    id 403
    label "zabezpieczenie"
  ]
  node [
    id 404
    label "zrobienie"
  ]
  node [
    id 405
    label "wyrz&#261;dzenie"
  ]
  node [
    id 406
    label "zagospodarowanie"
  ]
  node [
    id 407
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 408
    label "ig&#322;a"
  ]
  node [
    id 409
    label "wirnik"
  ]
  node [
    id 410
    label "aparatura"
  ]
  node [
    id 411
    label "system_energetyczny"
  ]
  node [
    id 412
    label "impulsator"
  ]
  node [
    id 413
    label "mechanizm"
  ]
  node [
    id 414
    label "blokowanie"
  ]
  node [
    id 415
    label "zablokowanie"
  ]
  node [
    id 416
    label "przygotowanie"
  ]
  node [
    id 417
    label "komora"
  ]
  node [
    id 418
    label "j&#281;zyk"
  ]
  node [
    id 419
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 420
    label "model"
  ]
  node [
    id 421
    label "bratek"
  ]
  node [
    id 422
    label "prawo"
  ]
  node [
    id 423
    label "rz&#261;dzenie"
  ]
  node [
    id 424
    label "panowanie"
  ]
  node [
    id 425
    label "Kreml"
  ]
  node [
    id 426
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 427
    label "wydolno&#347;&#263;"
  ]
  node [
    id 428
    label "rz&#261;d"
  ]
  node [
    id 429
    label "aparat_fotograficzny"
  ]
  node [
    id 430
    label "spadochron"
  ]
  node [
    id 431
    label "snapshot"
  ]
  node [
    id 432
    label "&#322;&#243;dzki"
  ]
  node [
    id 433
    label "film"
  ]
  node [
    id 434
    label "bilet_komunikacji_miejskiej"
  ]
  node [
    id 435
    label "uk&#322;ad_optyczny"
  ]
  node [
    id 436
    label "beczkowa&#263;"
  ]
  node [
    id 437
    label "soczewka"
  ]
  node [
    id 438
    label "przys&#322;ona"
  ]
  node [
    id 439
    label "decentracja"
  ]
  node [
    id 440
    label "filtr_fotograficzny"
  ]
  node [
    id 441
    label "przeziernik"
  ]
  node [
    id 442
    label "geodezja"
  ]
  node [
    id 443
    label "przypadek"
  ]
  node [
    id 444
    label "wy&#347;cig"
  ]
  node [
    id 445
    label "wizjer"
  ]
  node [
    id 446
    label "meta"
  ]
  node [
    id 447
    label "bro&#324;_palna"
  ]
  node [
    id 448
    label "szczerbina"
  ]
  node [
    id 449
    label "czapka"
  ]
  node [
    id 450
    label "ko&#322;pak"
  ]
  node [
    id 451
    label "piasta"
  ]
  node [
    id 452
    label "obiektyw_fotograficzny"
  ]
  node [
    id 453
    label "os&#322;ona"
  ]
  node [
    id 454
    label "k&#243;&#322;ko"
  ]
  node [
    id 455
    label "os&#322;onka"
  ]
  node [
    id 456
    label "szybka"
  ]
  node [
    id 457
    label "d&#378;wignia"
  ]
  node [
    id 458
    label "wylot"
  ]
  node [
    id 459
    label "przy&#347;piesznik"
  ]
  node [
    id 460
    label "bag"
  ]
  node [
    id 461
    label "sakwa"
  ]
  node [
    id 462
    label "torba"
  ]
  node [
    id 463
    label "w&#243;r"
  ]
  node [
    id 464
    label "miesi&#261;c"
  ]
  node [
    id 465
    label "ekran"
  ]
  node [
    id 466
    label "telefon"
  ]
  node [
    id 467
    label "handset"
  ]
  node [
    id 468
    label "call"
  ]
  node [
    id 469
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 470
    label "dzwonek"
  ]
  node [
    id 471
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 472
    label "sound"
  ]
  node [
    id 473
    label "bi&#263;"
  ]
  node [
    id 474
    label "brzmie&#263;"
  ]
  node [
    id 475
    label "drynda&#263;"
  ]
  node [
    id 476
    label "brz&#281;cze&#263;"
  ]
  node [
    id 477
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 478
    label "jingle"
  ]
  node [
    id 479
    label "zabrz&#281;cze&#263;"
  ]
  node [
    id 480
    label "zabi&#263;"
  ]
  node [
    id 481
    label "zadrynda&#263;"
  ]
  node [
    id 482
    label "zabrzmie&#263;"
  ]
  node [
    id 483
    label "nacisn&#261;&#263;"
  ]
  node [
    id 484
    label "dodzwanianie_si&#281;"
  ]
  node [
    id 485
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 486
    label "wydzwanianie"
  ]
  node [
    id 487
    label "naciskanie"
  ]
  node [
    id 488
    label "brzmienie"
  ]
  node [
    id 489
    label "wybijanie"
  ]
  node [
    id 490
    label "dryndanie"
  ]
  node [
    id 491
    label "dodzwonienie_si&#281;"
  ]
  node [
    id 492
    label "wydzwonienie"
  ]
  node [
    id 493
    label "nicpo&#324;"
  ]
  node [
    id 494
    label "agent"
  ]
  node [
    id 495
    label "Samsunga"
  ]
  node [
    id 496
    label "Galaxy"
  ]
  node [
    id 497
    label "S8"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 297
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 495
    target 496
  ]
  edge [
    source 495
    target 497
  ]
  edge [
    source 496
    target 497
  ]
]
