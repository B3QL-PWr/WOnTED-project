graph [
  node [
    id 0
    label "po&#380;ar"
    origin "text"
  ]
  node [
    id 1
    label "pi&#281;trowy"
    origin "text"
  ]
  node [
    id 2
    label "blok"
    origin "text"
  ]
  node [
    id 3
    label "strunino"
    origin "text"
  ]
  node [
    id 4
    label "region"
    origin "text"
  ]
  node [
    id 5
    label "vladimirski"
    origin "text"
  ]
  node [
    id 6
    label "rosja"
    origin "text"
  ]
  node [
    id 7
    label "pogorzelec"
  ]
  node [
    id 8
    label "zalew"
  ]
  node [
    id 9
    label "podpalenie"
  ]
  node [
    id 10
    label "wydarzenie"
  ]
  node [
    id 11
    label "kl&#281;ska"
  ]
  node [
    id 12
    label "stra&#380;ak"
  ]
  node [
    id 13
    label "przyp&#322;yw"
  ]
  node [
    id 14
    label "p&#322;omie&#324;"
  ]
  node [
    id 15
    label "nawa&#322;no&#347;&#263;"
  ]
  node [
    id 16
    label "zapr&#243;szenie"
  ]
  node [
    id 17
    label "wojna"
  ]
  node [
    id 18
    label "miazmaty"
  ]
  node [
    id 19
    label "kryzys"
  ]
  node [
    id 20
    label "zap&#322;on"
  ]
  node [
    id 21
    label "fire"
  ]
  node [
    id 22
    label "ogie&#324;"
  ]
  node [
    id 23
    label "July"
  ]
  node [
    id 24
    label "k&#322;opot"
  ]
  node [
    id 25
    label "cykl_koniunkturalny"
  ]
  node [
    id 26
    label "zwrot"
  ]
  node [
    id 27
    label "Marzec_'68"
  ]
  node [
    id 28
    label "schorzenie"
  ]
  node [
    id 29
    label "pogorszenie"
  ]
  node [
    id 30
    label "sytuacja"
  ]
  node [
    id 31
    label "head"
  ]
  node [
    id 32
    label "energia"
  ]
  node [
    id 33
    label "rumieniec"
  ]
  node [
    id 34
    label "nowotw&#243;r_niez&#322;o&#347;liwy"
  ]
  node [
    id 35
    label "sk&#243;ra"
  ]
  node [
    id 36
    label "hell"
  ]
  node [
    id 37
    label "rozpalenie"
  ]
  node [
    id 38
    label "co&#347;"
  ]
  node [
    id 39
    label "iskra"
  ]
  node [
    id 40
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 41
    label "light"
  ]
  node [
    id 42
    label "rozpalanie"
  ]
  node [
    id 43
    label "deszcz"
  ]
  node [
    id 44
    label "akcesorium"
  ]
  node [
    id 45
    label "zapalenie"
  ]
  node [
    id 46
    label "pali&#263;_si&#281;"
  ]
  node [
    id 47
    label "&#347;wiat&#322;o"
  ]
  node [
    id 48
    label "zarzewie"
  ]
  node [
    id 49
    label "ciep&#322;o"
  ]
  node [
    id 50
    label "znami&#281;"
  ]
  node [
    id 51
    label "war"
  ]
  node [
    id 52
    label "kolor"
  ]
  node [
    id 53
    label "palenie_si&#281;"
  ]
  node [
    id 54
    label "&#380;ywio&#322;"
  ]
  node [
    id 55
    label "incandescence"
  ]
  node [
    id 56
    label "atak"
  ]
  node [
    id 57
    label "palenie"
  ]
  node [
    id 58
    label "ardor"
  ]
  node [
    id 59
    label "woda"
  ]
  node [
    id 60
    label "attack"
  ]
  node [
    id 61
    label "golf"
  ]
  node [
    id 62
    label "Zatoka_Botnicka"
  ]
  node [
    id 63
    label "zjawisko"
  ]
  node [
    id 64
    label "zbiornik_wodny"
  ]
  node [
    id 65
    label "przebiec"
  ]
  node [
    id 66
    label "charakter"
  ]
  node [
    id 67
    label "czynno&#347;&#263;"
  ]
  node [
    id 68
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 69
    label "motyw"
  ]
  node [
    id 70
    label "przebiegni&#281;cie"
  ]
  node [
    id 71
    label "fabu&#322;a"
  ]
  node [
    id 72
    label "flow"
  ]
  node [
    id 73
    label "p&#322;yw"
  ]
  node [
    id 74
    label "wzrost"
  ]
  node [
    id 75
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 76
    label "ruch"
  ]
  node [
    id 77
    label "fit"
  ]
  node [
    id 78
    label "reakcja"
  ]
  node [
    id 79
    label "po&#322;o&#380;enie"
  ]
  node [
    id 80
    label "wysiadka"
  ]
  node [
    id 81
    label "visitation"
  ]
  node [
    id 82
    label "reverse"
  ]
  node [
    id 83
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 84
    label "gorzka_pigu&#322;ka"
  ]
  node [
    id 85
    label "calamity"
  ]
  node [
    id 86
    label "przegra"
  ]
  node [
    id 87
    label "k&#322;adzenie"
  ]
  node [
    id 88
    label "niepowodzenie"
  ]
  node [
    id 89
    label "niefortunno&#347;&#263;"
  ]
  node [
    id 90
    label "rezultat"
  ]
  node [
    id 91
    label "passa"
  ]
  node [
    id 92
    label "gwa&#322;towno&#347;&#263;"
  ]
  node [
    id 93
    label "nawa&#322;nica"
  ]
  node [
    id 94
    label "burza"
  ]
  node [
    id 95
    label "walka"
  ]
  node [
    id 96
    label "angaria"
  ]
  node [
    id 97
    label "zimna_wojna"
  ]
  node [
    id 98
    label "zbrodnia_przeciw_pokojowi"
  ]
  node [
    id 99
    label "konflikt"
  ]
  node [
    id 100
    label "sp&#243;r"
  ]
  node [
    id 101
    label "wojna_stuletnia"
  ]
  node [
    id 102
    label "wr&#243;g"
  ]
  node [
    id 103
    label "zbrodnia_przeciw_ludzko&#347;ci"
  ]
  node [
    id 104
    label "gra_w_karty"
  ]
  node [
    id 105
    label "zbrodnia_wojenna"
  ]
  node [
    id 106
    label "kszta&#322;t"
  ]
  node [
    id 107
    label "wyraz"
  ]
  node [
    id 108
    label "emocja"
  ]
  node [
    id 109
    label "ostentation"
  ]
  node [
    id 110
    label "dostanie_si&#281;"
  ]
  node [
    id 111
    label "spowodowanie"
  ]
  node [
    id 112
    label "zdarzenie_si&#281;"
  ]
  node [
    id 113
    label "pokrycie"
  ]
  node [
    id 114
    label "burning"
  ]
  node [
    id 115
    label "zniszczenie"
  ]
  node [
    id 116
    label "spalenie"
  ]
  node [
    id 117
    label "przypalenie"
  ]
  node [
    id 118
    label "poszkodowany"
  ]
  node [
    id 119
    label "gasi&#263;"
  ]
  node [
    id 120
    label "po&#380;arnik"
  ]
  node [
    id 121
    label "sztafeta_po&#380;arnicza"
  ]
  node [
    id 122
    label "mundurowy"
  ]
  node [
    id 123
    label "stra&#380;_po&#380;arna"
  ]
  node [
    id 124
    label "rota"
  ]
  node [
    id 125
    label "sikawkowy"
  ]
  node [
    id 126
    label "kopu&#322;ka"
  ]
  node [
    id 127
    label "silnik_spalinowy"
  ]
  node [
    id 128
    label "palec"
  ]
  node [
    id 129
    label "opary"
  ]
  node [
    id 130
    label "wp&#322;yw"
  ]
  node [
    id 131
    label "pi&#281;trowo"
  ]
  node [
    id 132
    label "du&#380;y"
  ]
  node [
    id 133
    label "rozbudowany"
  ]
  node [
    id 134
    label "zagmatwany"
  ]
  node [
    id 135
    label "g&#243;rny"
  ]
  node [
    id 136
    label "skomplikowany"
  ]
  node [
    id 137
    label "niezrozumia&#322;y"
  ]
  node [
    id 138
    label "zawile"
  ]
  node [
    id 139
    label "maksymalizowanie"
  ]
  node [
    id 140
    label "zmaksymalizowanie"
  ]
  node [
    id 141
    label "maxymalny"
  ]
  node [
    id 142
    label "szlachetny"
  ]
  node [
    id 143
    label "graniczny"
  ]
  node [
    id 144
    label "wysoki"
  ]
  node [
    id 145
    label "wy&#380;ni"
  ]
  node [
    id 146
    label "wznio&#347;le"
  ]
  node [
    id 147
    label "powa&#380;ny"
  ]
  node [
    id 148
    label "g&#243;rnie"
  ]
  node [
    id 149
    label "maksymalnie"
  ]
  node [
    id 150
    label "oderwany"
  ]
  node [
    id 151
    label "doros&#322;y"
  ]
  node [
    id 152
    label "znaczny"
  ]
  node [
    id 153
    label "niema&#322;o"
  ]
  node [
    id 154
    label "wiele"
  ]
  node [
    id 155
    label "rozwini&#281;ty"
  ]
  node [
    id 156
    label "dorodny"
  ]
  node [
    id 157
    label "wa&#380;ny"
  ]
  node [
    id 158
    label "prawdziwy"
  ]
  node [
    id 159
    label "du&#380;o"
  ]
  node [
    id 160
    label "bajt"
  ]
  node [
    id 161
    label "bloking"
  ]
  node [
    id 162
    label "j&#261;kanie"
  ]
  node [
    id 163
    label "przeszkoda"
  ]
  node [
    id 164
    label "zesp&#243;&#322;"
  ]
  node [
    id 165
    label "blokada"
  ]
  node [
    id 166
    label "bry&#322;a"
  ]
  node [
    id 167
    label "dzia&#322;"
  ]
  node [
    id 168
    label "kontynent"
  ]
  node [
    id 169
    label "nastawnia"
  ]
  node [
    id 170
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 171
    label "blockage"
  ]
  node [
    id 172
    label "zbi&#243;r"
  ]
  node [
    id 173
    label "block"
  ]
  node [
    id 174
    label "organizacja"
  ]
  node [
    id 175
    label "budynek"
  ]
  node [
    id 176
    label "start"
  ]
  node [
    id 177
    label "skorupa_ziemska"
  ]
  node [
    id 178
    label "program"
  ]
  node [
    id 179
    label "zeszyt"
  ]
  node [
    id 180
    label "grupa"
  ]
  node [
    id 181
    label "blokowisko"
  ]
  node [
    id 182
    label "artyku&#322;"
  ]
  node [
    id 183
    label "barak"
  ]
  node [
    id 184
    label "stok_kontynentalny"
  ]
  node [
    id 185
    label "whole"
  ]
  node [
    id 186
    label "square"
  ]
  node [
    id 187
    label "siatk&#243;wka"
  ]
  node [
    id 188
    label "kr&#261;g"
  ]
  node [
    id 189
    label "ram&#243;wka"
  ]
  node [
    id 190
    label "zamek"
  ]
  node [
    id 191
    label "obrona"
  ]
  node [
    id 192
    label "ok&#322;adka"
  ]
  node [
    id 193
    label "bie&#380;nia"
  ]
  node [
    id 194
    label "referat"
  ]
  node [
    id 195
    label "dom_wielorodzinny"
  ]
  node [
    id 196
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 197
    label "figura_geometryczna"
  ]
  node [
    id 198
    label "bezkszta&#322;tno&#347;&#263;"
  ]
  node [
    id 199
    label "kawa&#322;ek"
  ]
  node [
    id 200
    label "solid"
  ]
  node [
    id 201
    label "Mazowsze"
  ]
  node [
    id 202
    label "odm&#322;adzanie"
  ]
  node [
    id 203
    label "&#346;wietliki"
  ]
  node [
    id 204
    label "skupienie"
  ]
  node [
    id 205
    label "The_Beatles"
  ]
  node [
    id 206
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 207
    label "odm&#322;adza&#263;"
  ]
  node [
    id 208
    label "zabudowania"
  ]
  node [
    id 209
    label "group"
  ]
  node [
    id 210
    label "zespolik"
  ]
  node [
    id 211
    label "ro&#347;lina"
  ]
  node [
    id 212
    label "Depeche_Mode"
  ]
  node [
    id 213
    label "batch"
  ]
  node [
    id 214
    label "odm&#322;odzenie"
  ]
  node [
    id 215
    label "podmiot"
  ]
  node [
    id 216
    label "jednostka_organizacyjna"
  ]
  node [
    id 217
    label "struktura"
  ]
  node [
    id 218
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 219
    label "TOPR"
  ]
  node [
    id 220
    label "endecki"
  ]
  node [
    id 221
    label "przedstawicielstwo"
  ]
  node [
    id 222
    label "od&#322;am"
  ]
  node [
    id 223
    label "Cepelia"
  ]
  node [
    id 224
    label "ZBoWiD"
  ]
  node [
    id 225
    label "organization"
  ]
  node [
    id 226
    label "centrala"
  ]
  node [
    id 227
    label "GOPR"
  ]
  node [
    id 228
    label "ZOMO"
  ]
  node [
    id 229
    label "ZMP"
  ]
  node [
    id 230
    label "komitet_koordynacyjny"
  ]
  node [
    id 231
    label "przybud&#243;wka"
  ]
  node [
    id 232
    label "boj&#243;wka"
  ]
  node [
    id 233
    label "liga"
  ]
  node [
    id 234
    label "jednostka_systematyczna"
  ]
  node [
    id 235
    label "asymilowanie"
  ]
  node [
    id 236
    label "gromada"
  ]
  node [
    id 237
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 238
    label "asymilowa&#263;"
  ]
  node [
    id 239
    label "egzemplarz"
  ]
  node [
    id 240
    label "Entuzjastki"
  ]
  node [
    id 241
    label "kompozycja"
  ]
  node [
    id 242
    label "Terranie"
  ]
  node [
    id 243
    label "category"
  ]
  node [
    id 244
    label "pakiet_klimatyczny"
  ]
  node [
    id 245
    label "oddzia&#322;"
  ]
  node [
    id 246
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 247
    label "cz&#261;steczka"
  ]
  node [
    id 248
    label "stage_set"
  ]
  node [
    id 249
    label "type"
  ]
  node [
    id 250
    label "specgrupa"
  ]
  node [
    id 251
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 252
    label "Eurogrupa"
  ]
  node [
    id 253
    label "formacja_geologiczna"
  ]
  node [
    id 254
    label "harcerze_starsi"
  ]
  node [
    id 255
    label "kartka"
  ]
  node [
    id 256
    label "impression"
  ]
  node [
    id 257
    label "wk&#322;ad"
  ]
  node [
    id 258
    label "publikacja"
  ]
  node [
    id 259
    label "wydanie"
  ]
  node [
    id 260
    label "kajet"
  ]
  node [
    id 261
    label "wydawnictwo"
  ]
  node [
    id 262
    label "czasopismo"
  ]
  node [
    id 263
    label "egzamin"
  ]
  node [
    id 264
    label "gracz"
  ]
  node [
    id 265
    label "poj&#281;cie"
  ]
  node [
    id 266
    label "protection"
  ]
  node [
    id 267
    label "poparcie"
  ]
  node [
    id 268
    label "mecz"
  ]
  node [
    id 269
    label "defense"
  ]
  node [
    id 270
    label "s&#261;d"
  ]
  node [
    id 271
    label "auspices"
  ]
  node [
    id 272
    label "gra"
  ]
  node [
    id 273
    label "ochrona"
  ]
  node [
    id 274
    label "post&#281;powanie"
  ]
  node [
    id 275
    label "wojsko"
  ]
  node [
    id 276
    label "manewr"
  ]
  node [
    id 277
    label "defensive_structure"
  ]
  node [
    id 278
    label "guard_duty"
  ]
  node [
    id 279
    label "strona"
  ]
  node [
    id 280
    label "series"
  ]
  node [
    id 281
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 282
    label "uprawianie"
  ]
  node [
    id 283
    label "praca_rolnicza"
  ]
  node [
    id 284
    label "collection"
  ]
  node [
    id 285
    label "dane"
  ]
  node [
    id 286
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 287
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 288
    label "sum"
  ]
  node [
    id 289
    label "gathering"
  ]
  node [
    id 290
    label "album"
  ]
  node [
    id 291
    label "przedmiot"
  ]
  node [
    id 292
    label "circumference"
  ]
  node [
    id 293
    label "&#322;uk"
  ]
  node [
    id 294
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 295
    label "urz&#261;d"
  ]
  node [
    id 296
    label "sfera"
  ]
  node [
    id 297
    label "zakres"
  ]
  node [
    id 298
    label "miejsce_pracy"
  ]
  node [
    id 299
    label "insourcing"
  ]
  node [
    id 300
    label "wytw&#243;r"
  ]
  node [
    id 301
    label "column"
  ]
  node [
    id 302
    label "distribution"
  ]
  node [
    id 303
    label "stopie&#324;"
  ]
  node [
    id 304
    label "competence"
  ]
  node [
    id 305
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 306
    label "bezdro&#380;e"
  ]
  node [
    id 307
    label "poddzia&#322;"
  ]
  node [
    id 308
    label "Zamek_Ogrodzieniec"
  ]
  node [
    id 309
    label "drzwi"
  ]
  node [
    id 310
    label "budowla"
  ]
  node [
    id 311
    label "zamkni&#281;cie"
  ]
  node [
    id 312
    label "wjazd"
  ]
  node [
    id 313
    label "bro&#324;_palna"
  ]
  node [
    id 314
    label "brama"
  ]
  node [
    id 315
    label "zapi&#281;cie"
  ]
  node [
    id 316
    label "tercja"
  ]
  node [
    id 317
    label "ekspres"
  ]
  node [
    id 318
    label "mechanizm"
  ]
  node [
    id 319
    label "komora_zamkowa"
  ]
  node [
    id 320
    label "Windsor"
  ]
  node [
    id 321
    label "stra&#380;nica"
  ]
  node [
    id 322
    label "fortyfikacja"
  ]
  node [
    id 323
    label "rezydencja"
  ]
  node [
    id 324
    label "bramka"
  ]
  node [
    id 325
    label "iglica"
  ]
  node [
    id 326
    label "informatyka"
  ]
  node [
    id 327
    label "zagrywka"
  ]
  node [
    id 328
    label "hokej"
  ]
  node [
    id 329
    label "baszta"
  ]
  node [
    id 330
    label "fastener"
  ]
  node [
    id 331
    label "Wawel"
  ]
  node [
    id 332
    label "say_farewell"
  ]
  node [
    id 333
    label "dzielenie"
  ]
  node [
    id 334
    label "je&#378;dziectwo"
  ]
  node [
    id 335
    label "obstruction"
  ]
  node [
    id 336
    label "trudno&#347;&#263;"
  ]
  node [
    id 337
    label "bieg_prze&#322;ajowy"
  ]
  node [
    id 338
    label "podzielenie"
  ]
  node [
    id 339
    label "osiedle"
  ]
  node [
    id 340
    label "oprawa"
  ]
  node [
    id 341
    label "boarding"
  ]
  node [
    id 342
    label "oprawianie"
  ]
  node [
    id 343
    label "os&#322;ona"
  ]
  node [
    id 344
    label "oprawia&#263;"
  ]
  node [
    id 345
    label "megabajt"
  ]
  node [
    id 346
    label "partycja"
  ]
  node [
    id 347
    label "jednostka_informacji"
  ]
  node [
    id 348
    label "gazel"
  ]
  node [
    id 349
    label "p&#243;&#322;bajt"
  ]
  node [
    id 350
    label "terabajt"
  ]
  node [
    id 351
    label "wers"
  ]
  node [
    id 352
    label "gigabajt"
  ]
  node [
    id 353
    label "jednostka_alokacji"
  ]
  node [
    id 354
    label "bit"
  ]
  node [
    id 355
    label "kilobajt"
  ]
  node [
    id 356
    label "instalowa&#263;"
  ]
  node [
    id 357
    label "oprogramowanie"
  ]
  node [
    id 358
    label "odinstalowywa&#263;"
  ]
  node [
    id 359
    label "spis"
  ]
  node [
    id 360
    label "zaprezentowanie"
  ]
  node [
    id 361
    label "podprogram"
  ]
  node [
    id 362
    label "ogranicznik_referencyjny"
  ]
  node [
    id 363
    label "course_of_study"
  ]
  node [
    id 364
    label "booklet"
  ]
  node [
    id 365
    label "odinstalowanie"
  ]
  node [
    id 366
    label "broszura"
  ]
  node [
    id 367
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 368
    label "kana&#322;"
  ]
  node [
    id 369
    label "teleferie"
  ]
  node [
    id 370
    label "zainstalowanie"
  ]
  node [
    id 371
    label "struktura_organizacyjna"
  ]
  node [
    id 372
    label "pirat"
  ]
  node [
    id 373
    label "zaprezentowa&#263;"
  ]
  node [
    id 374
    label "prezentowanie"
  ]
  node [
    id 375
    label "prezentowa&#263;"
  ]
  node [
    id 376
    label "interfejs"
  ]
  node [
    id 377
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 378
    label "okno"
  ]
  node [
    id 379
    label "punkt"
  ]
  node [
    id 380
    label "folder"
  ]
  node [
    id 381
    label "zainstalowa&#263;"
  ]
  node [
    id 382
    label "za&#322;o&#380;enie"
  ]
  node [
    id 383
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 384
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 385
    label "tryb"
  ]
  node [
    id 386
    label "emitowa&#263;"
  ]
  node [
    id 387
    label "emitowanie"
  ]
  node [
    id 388
    label "odinstalowywanie"
  ]
  node [
    id 389
    label "instrukcja"
  ]
  node [
    id 390
    label "deklaracja"
  ]
  node [
    id 391
    label "sekcja_krytyczna"
  ]
  node [
    id 392
    label "menu"
  ]
  node [
    id 393
    label "furkacja"
  ]
  node [
    id 394
    label "podstawa"
  ]
  node [
    id 395
    label "instalowanie"
  ]
  node [
    id 396
    label "oferta"
  ]
  node [
    id 397
    label "odinstalowa&#263;"
  ]
  node [
    id 398
    label "relation"
  ]
  node [
    id 399
    label "podsekcja"
  ]
  node [
    id 400
    label "handout"
  ]
  node [
    id 401
    label "lecture"
  ]
  node [
    id 402
    label "ministerstwo"
  ]
  node [
    id 403
    label "wyk&#322;ad"
  ]
  node [
    id 404
    label "prawda"
  ]
  node [
    id 405
    label "znak_j&#281;zykowy"
  ]
  node [
    id 406
    label "nag&#322;&#243;wek"
  ]
  node [
    id 407
    label "szkic"
  ]
  node [
    id 408
    label "line"
  ]
  node [
    id 409
    label "fragment"
  ]
  node [
    id 410
    label "tekst"
  ]
  node [
    id 411
    label "wyr&#243;b"
  ]
  node [
    id 412
    label "rodzajnik"
  ]
  node [
    id 413
    label "dokument"
  ]
  node [
    id 414
    label "towar"
  ]
  node [
    id 415
    label "paragraf"
  ]
  node [
    id 416
    label "lobowanie"
  ]
  node [
    id 417
    label "&#347;cina&#263;"
  ]
  node [
    id 418
    label "cia&#322;o_szkliste"
  ]
  node [
    id 419
    label "podanie"
  ]
  node [
    id 420
    label "plamka_&#380;&#243;&#322;ta"
  ]
  node [
    id 421
    label "retinopatia"
  ]
  node [
    id 422
    label "&#347;cinanie"
  ]
  node [
    id 423
    label "zeaksantyna"
  ]
  node [
    id 424
    label "podawa&#263;"
  ]
  node [
    id 425
    label "pi&#322;ka"
  ]
  node [
    id 426
    label "przelobowa&#263;"
  ]
  node [
    id 427
    label "lobowa&#263;"
  ]
  node [
    id 428
    label "dno_oka"
  ]
  node [
    id 429
    label "przelobowanie"
  ]
  node [
    id 430
    label "poda&#263;"
  ]
  node [
    id 431
    label "barwnik_&#347;wiat&#322;oczu&#322;y"
  ]
  node [
    id 432
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 433
    label "&#347;ci&#281;cie"
  ]
  node [
    id 434
    label "podawanie"
  ]
  node [
    id 435
    label "m&#243;wienie"
  ]
  node [
    id 436
    label "przeci&#261;ganie"
  ]
  node [
    id 437
    label "rozwojowe_zaburzenie_mowy"
  ]
  node [
    id 438
    label "trena&#380;er"
  ]
  node [
    id 439
    label "blok_startowy"
  ]
  node [
    id 440
    label "obiekt"
  ]
  node [
    id 441
    label "urz&#261;dzenie"
  ]
  node [
    id 442
    label "tor"
  ]
  node [
    id 443
    label "balkon"
  ]
  node [
    id 444
    label "pod&#322;oga"
  ]
  node [
    id 445
    label "kondygnacja"
  ]
  node [
    id 446
    label "skrzyd&#322;o"
  ]
  node [
    id 447
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 448
    label "dach"
  ]
  node [
    id 449
    label "strop"
  ]
  node [
    id 450
    label "klatka_schodowa"
  ]
  node [
    id 451
    label "przedpro&#380;e"
  ]
  node [
    id 452
    label "Pentagon"
  ]
  node [
    id 453
    label "alkierz"
  ]
  node [
    id 454
    label "front"
  ]
  node [
    id 455
    label "scheduling"
  ]
  node [
    id 456
    label "plan"
  ]
  node [
    id 457
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 458
    label "okienko"
  ]
  node [
    id 459
    label "znieczulenie"
  ]
  node [
    id 460
    label "kolej"
  ]
  node [
    id 461
    label "utrudnienie"
  ]
  node [
    id 462
    label "arrest"
  ]
  node [
    id 463
    label "anestezja"
  ]
  node [
    id 464
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 465
    label "izolacja"
  ]
  node [
    id 466
    label "zwrotnica"
  ]
  node [
    id 467
    label "sankcja"
  ]
  node [
    id 468
    label "semafor"
  ]
  node [
    id 469
    label "deadlock"
  ]
  node [
    id 470
    label "lock"
  ]
  node [
    id 471
    label "niemo&#380;no&#347;&#263;"
  ]
  node [
    id 472
    label "Stary_&#346;wiat"
  ]
  node [
    id 473
    label "palearktyka"
  ]
  node [
    id 474
    label "Afryka"
  ]
  node [
    id 475
    label "Europa_Zachodnia"
  ]
  node [
    id 476
    label "l&#261;d"
  ]
  node [
    id 477
    label "epejroforeza"
  ]
  node [
    id 478
    label "Dunaj"
  ]
  node [
    id 479
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 480
    label "Azja"
  ]
  node [
    id 481
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 482
    label "Ameryka_Centralna"
  ]
  node [
    id 483
    label "Antarktyda"
  ]
  node [
    id 484
    label "Australia"
  ]
  node [
    id 485
    label "Europa_Wschodnia"
  ]
  node [
    id 486
    label "Germania"
  ]
  node [
    id 487
    label "Moza"
  ]
  node [
    id 488
    label "Ren"
  ]
  node [
    id 489
    label "Ameryka"
  ]
  node [
    id 490
    label "Europa"
  ]
  node [
    id 491
    label "Eurazja"
  ]
  node [
    id 492
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 493
    label "blok_kontynentalny"
  ]
  node [
    id 494
    label "lot"
  ]
  node [
    id 495
    label "rozpocz&#281;cie"
  ]
  node [
    id 496
    label "uczestnictwo"
  ]
  node [
    id 497
    label "okno_startowe"
  ]
  node [
    id 498
    label "pocz&#261;tek"
  ]
  node [
    id 499
    label "wy&#347;cig"
  ]
  node [
    id 500
    label "pomieszczenie"
  ]
  node [
    id 501
    label "Anglia"
  ]
  node [
    id 502
    label "Amazonia"
  ]
  node [
    id 503
    label "Bordeaux"
  ]
  node [
    id 504
    label "Naddniestrze"
  ]
  node [
    id 505
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 506
    label "Armagnac"
  ]
  node [
    id 507
    label "Zamojszczyzna"
  ]
  node [
    id 508
    label "Amhara"
  ]
  node [
    id 509
    label "okr&#281;g"
  ]
  node [
    id 510
    label "Ma&#322;opolska"
  ]
  node [
    id 511
    label "Turkiestan"
  ]
  node [
    id 512
    label "Burgundia"
  ]
  node [
    id 513
    label "Noworosja"
  ]
  node [
    id 514
    label "Mezoameryka"
  ]
  node [
    id 515
    label "Lubelszczyzna"
  ]
  node [
    id 516
    label "Krajina"
  ]
  node [
    id 517
    label "Ba&#322;kany"
  ]
  node [
    id 518
    label "Kurdystan"
  ]
  node [
    id 519
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 520
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 521
    label "Baszkiria"
  ]
  node [
    id 522
    label "Szkocja"
  ]
  node [
    id 523
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 524
    label "Tonkin"
  ]
  node [
    id 525
    label "Maghreb"
  ]
  node [
    id 526
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 527
    label "Nadrenia"
  ]
  node [
    id 528
    label "Wielkopolska"
  ]
  node [
    id 529
    label "Zabajkale"
  ]
  node [
    id 530
    label "Apulia"
  ]
  node [
    id 531
    label "Bojkowszczyzna"
  ]
  node [
    id 532
    label "podregion"
  ]
  node [
    id 533
    label "Liguria"
  ]
  node [
    id 534
    label "Pamir"
  ]
  node [
    id 535
    label "Indochiny"
  ]
  node [
    id 536
    label "Polinezja"
  ]
  node [
    id 537
    label "Kurpie"
  ]
  node [
    id 538
    label "Podlasie"
  ]
  node [
    id 539
    label "S&#261;decczyzna"
  ]
  node [
    id 540
    label "Umbria"
  ]
  node [
    id 541
    label "Flandria"
  ]
  node [
    id 542
    label "Karaiby"
  ]
  node [
    id 543
    label "Ukraina_Zachodnia"
  ]
  node [
    id 544
    label "Kielecczyzna"
  ]
  node [
    id 545
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 546
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 547
    label "Skandynawia"
  ]
  node [
    id 548
    label "Kujawy"
  ]
  node [
    id 549
    label "Tyrol"
  ]
  node [
    id 550
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 551
    label "Huculszczyzna"
  ]
  node [
    id 552
    label "Turyngia"
  ]
  node [
    id 553
    label "jednostka_administracyjna"
  ]
  node [
    id 554
    label "Podhale"
  ]
  node [
    id 555
    label "Toskania"
  ]
  node [
    id 556
    label "Bory_Tucholskie"
  ]
  node [
    id 557
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 558
    label "country"
  ]
  node [
    id 559
    label "Kalabria"
  ]
  node [
    id 560
    label "Hercegowina"
  ]
  node [
    id 561
    label "Lotaryngia"
  ]
  node [
    id 562
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 563
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 564
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 565
    label "Walia"
  ]
  node [
    id 566
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 567
    label "Opolskie"
  ]
  node [
    id 568
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 569
    label "Kampania"
  ]
  node [
    id 570
    label "Chiny_Zachodnie"
  ]
  node [
    id 571
    label "Sand&#380;ak"
  ]
  node [
    id 572
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 573
    label "Syjon"
  ]
  node [
    id 574
    label "Kabylia"
  ]
  node [
    id 575
    label "Lombardia"
  ]
  node [
    id 576
    label "Warmia"
  ]
  node [
    id 577
    label "Kaszmir"
  ]
  node [
    id 578
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 579
    label "&#321;&#243;dzkie"
  ]
  node [
    id 580
    label "Kaukaz"
  ]
  node [
    id 581
    label "subregion"
  ]
  node [
    id 582
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 583
    label "Biskupizna"
  ]
  node [
    id 584
    label "Afryka_Wschodnia"
  ]
  node [
    id 585
    label "Podkarpacie"
  ]
  node [
    id 586
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 587
    label "Chiny_Wschodnie"
  ]
  node [
    id 588
    label "obszar"
  ]
  node [
    id 589
    label "Afryka_Zachodnia"
  ]
  node [
    id 590
    label "&#379;mud&#378;"
  ]
  node [
    id 591
    label "Bo&#347;nia"
  ]
  node [
    id 592
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 593
    label "Oceania"
  ]
  node [
    id 594
    label "Pomorze_Zachodnie"
  ]
  node [
    id 595
    label "Powi&#347;le"
  ]
  node [
    id 596
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 597
    label "Opolszczyzna"
  ]
  node [
    id 598
    label "&#321;emkowszczyzna"
  ]
  node [
    id 599
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 600
    label "Podbeskidzie"
  ]
  node [
    id 601
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 602
    label "Kaszuby"
  ]
  node [
    id 603
    label "Ko&#322;yma"
  ]
  node [
    id 604
    label "Szlezwik"
  ]
  node [
    id 605
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 606
    label "Mikronezja"
  ]
  node [
    id 607
    label "Polesie"
  ]
  node [
    id 608
    label "Kerala"
  ]
  node [
    id 609
    label "Mazury"
  ]
  node [
    id 610
    label "Palestyna"
  ]
  node [
    id 611
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 612
    label "Lauda"
  ]
  node [
    id 613
    label "Azja_Wschodnia"
  ]
  node [
    id 614
    label "Galicja"
  ]
  node [
    id 615
    label "Zakarpacie"
  ]
  node [
    id 616
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 617
    label "Lubuskie"
  ]
  node [
    id 618
    label "Laponia"
  ]
  node [
    id 619
    label "Yorkshire"
  ]
  node [
    id 620
    label "Bawaria"
  ]
  node [
    id 621
    label "Zag&#243;rze"
  ]
  node [
    id 622
    label "Andaluzja"
  ]
  node [
    id 623
    label "Kraina"
  ]
  node [
    id 624
    label "&#379;ywiecczyzna"
  ]
  node [
    id 625
    label "Oksytania"
  ]
  node [
    id 626
    label "Kociewie"
  ]
  node [
    id 627
    label "Lasko"
  ]
  node [
    id 628
    label "p&#243;&#322;noc"
  ]
  node [
    id 629
    label "Kosowo"
  ]
  node [
    id 630
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 631
    label "Zab&#322;ocie"
  ]
  node [
    id 632
    label "zach&#243;d"
  ]
  node [
    id 633
    label "po&#322;udnie"
  ]
  node [
    id 634
    label "Pow&#261;zki"
  ]
  node [
    id 635
    label "Piotrowo"
  ]
  node [
    id 636
    label "Olszanica"
  ]
  node [
    id 637
    label "Ruda_Pabianicka"
  ]
  node [
    id 638
    label "holarktyka"
  ]
  node [
    id 639
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 640
    label "Ludwin&#243;w"
  ]
  node [
    id 641
    label "Arktyka"
  ]
  node [
    id 642
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 643
    label "Zabu&#380;e"
  ]
  node [
    id 644
    label "miejsce"
  ]
  node [
    id 645
    label "antroposfera"
  ]
  node [
    id 646
    label "Neogea"
  ]
  node [
    id 647
    label "terytorium"
  ]
  node [
    id 648
    label "Syberia_Zachodnia"
  ]
  node [
    id 649
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 650
    label "pas_planetoid"
  ]
  node [
    id 651
    label "Syberia_Wschodnia"
  ]
  node [
    id 652
    label "Antarktyka"
  ]
  node [
    id 653
    label "Rakowice"
  ]
  node [
    id 654
    label "akrecja"
  ]
  node [
    id 655
    label "wymiar"
  ]
  node [
    id 656
    label "&#321;&#281;g"
  ]
  node [
    id 657
    label "Kresy_Zachodnie"
  ]
  node [
    id 658
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 659
    label "przestrze&#324;"
  ]
  node [
    id 660
    label "wsch&#243;d"
  ]
  node [
    id 661
    label "Notogea"
  ]
  node [
    id 662
    label "Judea"
  ]
  node [
    id 663
    label "moszaw"
  ]
  node [
    id 664
    label "Kanaan"
  ]
  node [
    id 665
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 666
    label "Anglosas"
  ]
  node [
    id 667
    label "Jerozolima"
  ]
  node [
    id 668
    label "Etiopia"
  ]
  node [
    id 669
    label "Beskidy_Zachodnie"
  ]
  node [
    id 670
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 671
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 672
    label "Wiktoria"
  ]
  node [
    id 673
    label "Wielka_Brytania"
  ]
  node [
    id 674
    label "Guernsey"
  ]
  node [
    id 675
    label "Conrad"
  ]
  node [
    id 676
    label "funt_szterling"
  ]
  node [
    id 677
    label "Unia_Europejska"
  ]
  node [
    id 678
    label "Portland"
  ]
  node [
    id 679
    label "NATO"
  ]
  node [
    id 680
    label "El&#380;bieta_I"
  ]
  node [
    id 681
    label "Kornwalia"
  ]
  node [
    id 682
    label "Dolna_Frankonia"
  ]
  node [
    id 683
    label "Niemcy"
  ]
  node [
    id 684
    label "W&#322;ochy"
  ]
  node [
    id 685
    label "Ukraina"
  ]
  node [
    id 686
    label "Wyspy_Marshalla"
  ]
  node [
    id 687
    label "Nauru"
  ]
  node [
    id 688
    label "Mariany"
  ]
  node [
    id 689
    label "dolar"
  ]
  node [
    id 690
    label "Karpaty"
  ]
  node [
    id 691
    label "Beskid_Niski"
  ]
  node [
    id 692
    label "Polska"
  ]
  node [
    id 693
    label "Warszawa"
  ]
  node [
    id 694
    label "Mariensztat"
  ]
  node [
    id 695
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 696
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 697
    label "Paj&#281;czno"
  ]
  node [
    id 698
    label "Mogielnica"
  ]
  node [
    id 699
    label "Gop&#322;o"
  ]
  node [
    id 700
    label "Francja"
  ]
  node [
    id 701
    label "Poprad"
  ]
  node [
    id 702
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 703
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 704
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 705
    label "Bojanowo"
  ]
  node [
    id 706
    label "Obra"
  ]
  node [
    id 707
    label "Wilkowo_Polskie"
  ]
  node [
    id 708
    label "Dobra"
  ]
  node [
    id 709
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 710
    label "Samoa"
  ]
  node [
    id 711
    label "Tonga"
  ]
  node [
    id 712
    label "Tuwalu"
  ]
  node [
    id 713
    label "Hawaje"
  ]
  node [
    id 714
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 715
    label "Rosja"
  ]
  node [
    id 716
    label "Etruria"
  ]
  node [
    id 717
    label "Rumelia"
  ]
  node [
    id 718
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 719
    label "Nowa_Zelandia"
  ]
  node [
    id 720
    label "Ocean_Spokojny"
  ]
  node [
    id 721
    label "Palau"
  ]
  node [
    id 722
    label "Melanezja"
  ]
  node [
    id 723
    label "Nowy_&#346;wiat"
  ]
  node [
    id 724
    label "Tar&#322;&#243;w"
  ]
  node [
    id 725
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 726
    label "Czeczenia"
  ]
  node [
    id 727
    label "Inguszetia"
  ]
  node [
    id 728
    label "Abchazja"
  ]
  node [
    id 729
    label "Sarmata"
  ]
  node [
    id 730
    label "Dagestan"
  ]
  node [
    id 731
    label "Pakistan"
  ]
  node [
    id 732
    label "Indie"
  ]
  node [
    id 733
    label "Czarnog&#243;ra"
  ]
  node [
    id 734
    label "Serbia"
  ]
  node [
    id 735
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 736
    label "Tatry"
  ]
  node [
    id 737
    label "Podtatrze"
  ]
  node [
    id 738
    label "Imperium_Rosyjskie"
  ]
  node [
    id 739
    label "jezioro"
  ]
  node [
    id 740
    label "&#346;l&#261;sk"
  ]
  node [
    id 741
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 742
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 743
    label "Mo&#322;dawia"
  ]
  node [
    id 744
    label "Podole"
  ]
  node [
    id 745
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 746
    label "Hiszpania"
  ]
  node [
    id 747
    label "Austro-W&#281;gry"
  ]
  node [
    id 748
    label "Algieria"
  ]
  node [
    id 749
    label "funt_szkocki"
  ]
  node [
    id 750
    label "Kaledonia"
  ]
  node [
    id 751
    label "Libia"
  ]
  node [
    id 752
    label "Maroko"
  ]
  node [
    id 753
    label "Tunezja"
  ]
  node [
    id 754
    label "Mauretania"
  ]
  node [
    id 755
    label "Sahara_Zachodnia"
  ]
  node [
    id 756
    label "Biskupice"
  ]
  node [
    id 757
    label "Iwanowice"
  ]
  node [
    id 758
    label "Ziemia_Sandomierska"
  ]
  node [
    id 759
    label "Rogo&#378;nik"
  ]
  node [
    id 760
    label "Ropa"
  ]
  node [
    id 761
    label "Buriacja"
  ]
  node [
    id 762
    label "Rozewie"
  ]
  node [
    id 763
    label "Norwegia"
  ]
  node [
    id 764
    label "Szwecja"
  ]
  node [
    id 765
    label "Finlandia"
  ]
  node [
    id 766
    label "Antigua_i_Barbuda"
  ]
  node [
    id 767
    label "Kuba"
  ]
  node [
    id 768
    label "Jamajka"
  ]
  node [
    id 769
    label "Aruba"
  ]
  node [
    id 770
    label "Haiti"
  ]
  node [
    id 771
    label "Kajmany"
  ]
  node [
    id 772
    label "Portoryko"
  ]
  node [
    id 773
    label "Anguilla"
  ]
  node [
    id 774
    label "Bahamy"
  ]
  node [
    id 775
    label "Antyle"
  ]
  node [
    id 776
    label "Czechy"
  ]
  node [
    id 777
    label "Amazonka"
  ]
  node [
    id 778
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 779
    label "Wietnam"
  ]
  node [
    id 780
    label "Austria"
  ]
  node [
    id 781
    label "Alpy"
  ]
  node [
    id 782
    label "&#379;mujd&#378;"
  ]
  node [
    id 783
    label "Litwa"
  ]
  node [
    id 784
    label "j&#281;zyk_flamandzki"
  ]
  node [
    id 785
    label "Belgia"
  ]
  node [
    id 786
    label "okr&#281;g_przemys&#322;owy"
  ]
  node [
    id 787
    label "pa&#324;stwo"
  ]
  node [
    id 788
    label "muzyka_rozrywkowa"
  ]
  node [
    id 789
    label "bluegrass"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 617
  ]
  edge [
    source 4
    target 618
  ]
  edge [
    source 4
    target 619
  ]
  edge [
    source 4
    target 620
  ]
  edge [
    source 4
    target 621
  ]
  edge [
    source 4
    target 622
  ]
  edge [
    source 4
    target 623
  ]
  edge [
    source 4
    target 624
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 626
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 4
    target 655
  ]
  edge [
    source 4
    target 656
  ]
  edge [
    source 4
    target 657
  ]
  edge [
    source 4
    target 658
  ]
  edge [
    source 4
    target 659
  ]
  edge [
    source 4
    target 660
  ]
  edge [
    source 4
    target 661
  ]
  edge [
    source 4
    target 662
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 665
  ]
  edge [
    source 4
    target 666
  ]
  edge [
    source 4
    target 667
  ]
  edge [
    source 4
    target 668
  ]
  edge [
    source 4
    target 669
  ]
  edge [
    source 4
    target 670
  ]
  edge [
    source 4
    target 671
  ]
  edge [
    source 4
    target 672
  ]
  edge [
    source 4
    target 673
  ]
  edge [
    source 4
    target 674
  ]
  edge [
    source 4
    target 675
  ]
  edge [
    source 4
    target 676
  ]
  edge [
    source 4
    target 677
  ]
  edge [
    source 4
    target 678
  ]
  edge [
    source 4
    target 679
  ]
  edge [
    source 4
    target 680
  ]
  edge [
    source 4
    target 681
  ]
  edge [
    source 4
    target 682
  ]
  edge [
    source 4
    target 683
  ]
  edge [
    source 4
    target 684
  ]
  edge [
    source 4
    target 685
  ]
  edge [
    source 4
    target 686
  ]
  edge [
    source 4
    target 687
  ]
  edge [
    source 4
    target 688
  ]
  edge [
    source 4
    target 689
  ]
  edge [
    source 4
    target 690
  ]
  edge [
    source 4
    target 691
  ]
  edge [
    source 4
    target 692
  ]
  edge [
    source 4
    target 693
  ]
  edge [
    source 4
    target 694
  ]
  edge [
    source 4
    target 695
  ]
  edge [
    source 4
    target 696
  ]
  edge [
    source 4
    target 697
  ]
  edge [
    source 4
    target 698
  ]
  edge [
    source 4
    target 699
  ]
  edge [
    source 4
    target 700
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 701
  ]
  edge [
    source 4
    target 702
  ]
  edge [
    source 4
    target 703
  ]
  edge [
    source 4
    target 704
  ]
  edge [
    source 4
    target 705
  ]
  edge [
    source 4
    target 706
  ]
  edge [
    source 4
    target 707
  ]
  edge [
    source 4
    target 708
  ]
  edge [
    source 4
    target 709
  ]
  edge [
    source 4
    target 710
  ]
  edge [
    source 4
    target 711
  ]
  edge [
    source 4
    target 712
  ]
  edge [
    source 4
    target 713
  ]
  edge [
    source 4
    target 714
  ]
  edge [
    source 4
    target 715
  ]
  edge [
    source 4
    target 716
  ]
  edge [
    source 4
    target 717
  ]
  edge [
    source 4
    target 718
  ]
  edge [
    source 4
    target 719
  ]
  edge [
    source 4
    target 720
  ]
  edge [
    source 4
    target 721
  ]
  edge [
    source 4
    target 722
  ]
  edge [
    source 4
    target 723
  ]
  edge [
    source 4
    target 724
  ]
  edge [
    source 4
    target 725
  ]
  edge [
    source 4
    target 726
  ]
  edge [
    source 4
    target 727
  ]
  edge [
    source 4
    target 728
  ]
  edge [
    source 4
    target 729
  ]
  edge [
    source 4
    target 730
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 731
  ]
  edge [
    source 4
    target 732
  ]
  edge [
    source 4
    target 733
  ]
  edge [
    source 4
    target 734
  ]
  edge [
    source 4
    target 735
  ]
  edge [
    source 4
    target 736
  ]
  edge [
    source 4
    target 737
  ]
  edge [
    source 4
    target 738
  ]
  edge [
    source 4
    target 739
  ]
  edge [
    source 4
    target 740
  ]
  edge [
    source 4
    target 741
  ]
  edge [
    source 4
    target 742
  ]
  edge [
    source 4
    target 743
  ]
  edge [
    source 4
    target 744
  ]
  edge [
    source 4
    target 745
  ]
  edge [
    source 4
    target 746
  ]
  edge [
    source 4
    target 747
  ]
  edge [
    source 4
    target 748
  ]
  edge [
    source 4
    target 749
  ]
  edge [
    source 4
    target 750
  ]
  edge [
    source 4
    target 751
  ]
  edge [
    source 4
    target 752
  ]
  edge [
    source 4
    target 753
  ]
  edge [
    source 4
    target 754
  ]
  edge [
    source 4
    target 755
  ]
  edge [
    source 4
    target 756
  ]
  edge [
    source 4
    target 757
  ]
  edge [
    source 4
    target 758
  ]
  edge [
    source 4
    target 759
  ]
  edge [
    source 4
    target 760
  ]
  edge [
    source 4
    target 761
  ]
  edge [
    source 4
    target 762
  ]
  edge [
    source 4
    target 763
  ]
  edge [
    source 4
    target 764
  ]
  edge [
    source 4
    target 765
  ]
  edge [
    source 4
    target 766
  ]
  edge [
    source 4
    target 767
  ]
  edge [
    source 4
    target 768
  ]
  edge [
    source 4
    target 769
  ]
  edge [
    source 4
    target 770
  ]
  edge [
    source 4
    target 771
  ]
  edge [
    source 4
    target 772
  ]
  edge [
    source 4
    target 773
  ]
  edge [
    source 4
    target 774
  ]
  edge [
    source 4
    target 775
  ]
  edge [
    source 4
    target 776
  ]
  edge [
    source 4
    target 777
  ]
  edge [
    source 4
    target 778
  ]
  edge [
    source 4
    target 779
  ]
  edge [
    source 4
    target 780
  ]
  edge [
    source 4
    target 781
  ]
  edge [
    source 4
    target 782
  ]
  edge [
    source 4
    target 783
  ]
  edge [
    source 4
    target 784
  ]
  edge [
    source 4
    target 785
  ]
  edge [
    source 4
    target 786
  ]
  edge [
    source 4
    target 787
  ]
  edge [
    source 4
    target 788
  ]
  edge [
    source 4
    target 789
  ]
  edge [
    source 5
    target 6
  ]
]
