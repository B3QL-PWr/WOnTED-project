graph [
  node [
    id 0
    label "nadajnik"
    origin "text"
  ]
  node [
    id 1
    label "gps"
    origin "text"
  ]
  node [
    id 2
    label "pokazowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "poszczeg&#243;lny"
    origin "text"
  ]
  node [
    id 4
    label "stado"
    origin "text"
  ]
  node [
    id 5
    label "wchodzi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "siebie"
    origin "text"
  ]
  node [
    id 7
    label "droga"
    origin "text"
  ]
  node [
    id 8
    label "antena"
  ]
  node [
    id 9
    label "urz&#261;dzenie"
  ]
  node [
    id 10
    label "przedmiot"
  ]
  node [
    id 11
    label "kom&#243;rka"
  ]
  node [
    id 12
    label "furnishing"
  ]
  node [
    id 13
    label "zabezpieczenie"
  ]
  node [
    id 14
    label "zrobienie"
  ]
  node [
    id 15
    label "wyrz&#261;dzenie"
  ]
  node [
    id 16
    label "zagospodarowanie"
  ]
  node [
    id 17
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 18
    label "ig&#322;a"
  ]
  node [
    id 19
    label "narz&#281;dzie"
  ]
  node [
    id 20
    label "wirnik"
  ]
  node [
    id 21
    label "aparatura"
  ]
  node [
    id 22
    label "system_energetyczny"
  ]
  node [
    id 23
    label "impulsator"
  ]
  node [
    id 24
    label "mechanizm"
  ]
  node [
    id 25
    label "sprz&#281;t"
  ]
  node [
    id 26
    label "czynno&#347;&#263;"
  ]
  node [
    id 27
    label "blokowanie"
  ]
  node [
    id 28
    label "set"
  ]
  node [
    id 29
    label "zablokowanie"
  ]
  node [
    id 30
    label "przygotowanie"
  ]
  node [
    id 31
    label "komora"
  ]
  node [
    id 32
    label "j&#281;zyk"
  ]
  node [
    id 33
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 34
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 35
    label "szyk_antenowy"
  ]
  node [
    id 36
    label "odbiornik"
  ]
  node [
    id 37
    label "stawon&#243;g"
  ]
  node [
    id 38
    label "odbieranie"
  ]
  node [
    id 39
    label "odbiera&#263;"
  ]
  node [
    id 40
    label "czu&#322;ek"
  ]
  node [
    id 41
    label "poszczeg&#243;lnie"
  ]
  node [
    id 42
    label "pojedynczy"
  ]
  node [
    id 43
    label "jednodzielny"
  ]
  node [
    id 44
    label "rzadki"
  ]
  node [
    id 45
    label "pojedynczo"
  ]
  node [
    id 46
    label "hurma"
  ]
  node [
    id 47
    label "grupa"
  ]
  node [
    id 48
    label "zbi&#243;r"
  ]
  node [
    id 49
    label "school"
  ]
  node [
    id 50
    label "odm&#322;adzanie"
  ]
  node [
    id 51
    label "liga"
  ]
  node [
    id 52
    label "jednostka_systematyczna"
  ]
  node [
    id 53
    label "asymilowanie"
  ]
  node [
    id 54
    label "gromada"
  ]
  node [
    id 55
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 56
    label "asymilowa&#263;"
  ]
  node [
    id 57
    label "egzemplarz"
  ]
  node [
    id 58
    label "Entuzjastki"
  ]
  node [
    id 59
    label "kompozycja"
  ]
  node [
    id 60
    label "Terranie"
  ]
  node [
    id 61
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 62
    label "category"
  ]
  node [
    id 63
    label "pakiet_klimatyczny"
  ]
  node [
    id 64
    label "oddzia&#322;"
  ]
  node [
    id 65
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 66
    label "cz&#261;steczka"
  ]
  node [
    id 67
    label "stage_set"
  ]
  node [
    id 68
    label "type"
  ]
  node [
    id 69
    label "specgrupa"
  ]
  node [
    id 70
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 71
    label "&#346;wietliki"
  ]
  node [
    id 72
    label "odm&#322;odzenie"
  ]
  node [
    id 73
    label "Eurogrupa"
  ]
  node [
    id 74
    label "odm&#322;adza&#263;"
  ]
  node [
    id 75
    label "formacja_geologiczna"
  ]
  node [
    id 76
    label "harcerze_starsi"
  ]
  node [
    id 77
    label "series"
  ]
  node [
    id 78
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 79
    label "uprawianie"
  ]
  node [
    id 80
    label "praca_rolnicza"
  ]
  node [
    id 81
    label "collection"
  ]
  node [
    id 82
    label "dane"
  ]
  node [
    id 83
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 84
    label "poj&#281;cie"
  ]
  node [
    id 85
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 86
    label "sum"
  ]
  node [
    id 87
    label "gathering"
  ]
  node [
    id 88
    label "album"
  ]
  node [
    id 89
    label "egzotyk"
  ]
  node [
    id 90
    label "hurmowate"
  ]
  node [
    id 91
    label "drzewo"
  ]
  node [
    id 92
    label "ro&#347;lina"
  ]
  node [
    id 93
    label "owoc_egzotyczny"
  ]
  node [
    id 94
    label "jagoda"
  ]
  node [
    id 95
    label "ro&#347;lina_u&#380;ytkowa"
  ]
  node [
    id 96
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 97
    label "zaziera&#263;"
  ]
  node [
    id 98
    label "move"
  ]
  node [
    id 99
    label "zaczyna&#263;"
  ]
  node [
    id 100
    label "spotyka&#263;"
  ]
  node [
    id 101
    label "przenika&#263;"
  ]
  node [
    id 102
    label "osi&#261;ga&#263;"
  ]
  node [
    id 103
    label "nast&#281;powa&#263;"
  ]
  node [
    id 104
    label "mount"
  ]
  node [
    id 105
    label "bra&#263;"
  ]
  node [
    id 106
    label "go"
  ]
  node [
    id 107
    label "&#322;oi&#263;"
  ]
  node [
    id 108
    label "intervene"
  ]
  node [
    id 109
    label "scale"
  ]
  node [
    id 110
    label "poznawa&#263;"
  ]
  node [
    id 111
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 112
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 113
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 114
    label "dochodzi&#263;"
  ]
  node [
    id 115
    label "przekracza&#263;"
  ]
  node [
    id 116
    label "wnika&#263;"
  ]
  node [
    id 117
    label "atakowa&#263;"
  ]
  node [
    id 118
    label "invade"
  ]
  node [
    id 119
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 120
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 121
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 122
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 123
    label "strike"
  ]
  node [
    id 124
    label "robi&#263;"
  ]
  node [
    id 125
    label "schorzenie"
  ]
  node [
    id 126
    label "dzia&#322;a&#263;"
  ]
  node [
    id 127
    label "ofensywny"
  ]
  node [
    id 128
    label "przewaga"
  ]
  node [
    id 129
    label "sport"
  ]
  node [
    id 130
    label "epidemia"
  ]
  node [
    id 131
    label "attack"
  ]
  node [
    id 132
    label "rozgrywa&#263;"
  ]
  node [
    id 133
    label "krytykowa&#263;"
  ]
  node [
    id 134
    label "walczy&#263;"
  ]
  node [
    id 135
    label "aim"
  ]
  node [
    id 136
    label "trouble_oneself"
  ]
  node [
    id 137
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 138
    label "napada&#263;"
  ]
  node [
    id 139
    label "m&#243;wi&#263;"
  ]
  node [
    id 140
    label "usi&#322;owa&#263;"
  ]
  node [
    id 141
    label "ograniczenie"
  ]
  node [
    id 142
    label "przebywa&#263;"
  ]
  node [
    id 143
    label "conflict"
  ]
  node [
    id 144
    label "transgress"
  ]
  node [
    id 145
    label "appear"
  ]
  node [
    id 146
    label "mija&#263;"
  ]
  node [
    id 147
    label "zawiera&#263;"
  ]
  node [
    id 148
    label "cognizance"
  ]
  node [
    id 149
    label "szkoli&#263;_si&#281;"
  ]
  node [
    id 150
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 151
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 152
    label "go_steady"
  ]
  node [
    id 153
    label "detect"
  ]
  node [
    id 154
    label "make"
  ]
  node [
    id 155
    label "rozr&#243;&#380;nia&#263;"
  ]
  node [
    id 156
    label "hurt"
  ]
  node [
    id 157
    label "styka&#263;_si&#281;"
  ]
  node [
    id 158
    label "trespass"
  ]
  node [
    id 159
    label "transpire"
  ]
  node [
    id 160
    label "naciska&#263;"
  ]
  node [
    id 161
    label "mie&#263;_miejsce"
  ]
  node [
    id 162
    label "alternate"
  ]
  node [
    id 163
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 164
    label "chance"
  ]
  node [
    id 165
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 166
    label "uzyskiwa&#263;"
  ]
  node [
    id 167
    label "dociera&#263;"
  ]
  node [
    id 168
    label "mark"
  ]
  node [
    id 169
    label "get"
  ]
  node [
    id 170
    label "claim"
  ]
  node [
    id 171
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 172
    label "ripen"
  ]
  node [
    id 173
    label "supervene"
  ]
  node [
    id 174
    label "doczeka&#263;"
  ]
  node [
    id 175
    label "przesy&#322;ka"
  ]
  node [
    id 176
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 177
    label "doznawa&#263;"
  ]
  node [
    id 178
    label "reach"
  ]
  node [
    id 179
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 180
    label "zachodzi&#263;"
  ]
  node [
    id 181
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 182
    label "postrzega&#263;"
  ]
  node [
    id 183
    label "orgazm"
  ]
  node [
    id 184
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 185
    label "dokoptowywa&#263;"
  ]
  node [
    id 186
    label "dolatywa&#263;"
  ]
  node [
    id 187
    label "powodowa&#263;"
  ]
  node [
    id 188
    label "submit"
  ]
  node [
    id 189
    label "odejmowa&#263;"
  ]
  node [
    id 190
    label "bankrupt"
  ]
  node [
    id 191
    label "open"
  ]
  node [
    id 192
    label "set_about"
  ]
  node [
    id 193
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 194
    label "begin"
  ]
  node [
    id 195
    label "post&#281;powa&#263;"
  ]
  node [
    id 196
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 197
    label "fall"
  ]
  node [
    id 198
    label "znajdowa&#263;"
  ]
  node [
    id 199
    label "happen"
  ]
  node [
    id 200
    label "goban"
  ]
  node [
    id 201
    label "gra_planszowa"
  ]
  node [
    id 202
    label "sport_umys&#322;owy"
  ]
  node [
    id 203
    label "chi&#324;ski"
  ]
  node [
    id 204
    label "zagl&#261;da&#263;"
  ]
  node [
    id 205
    label "wpada&#263;"
  ]
  node [
    id 206
    label "pokonywa&#263;"
  ]
  node [
    id 207
    label "nat&#322;uszcza&#263;"
  ]
  node [
    id 208
    label "je&#378;dzi&#263;"
  ]
  node [
    id 209
    label "peddle"
  ]
  node [
    id 210
    label "obgadywa&#263;"
  ]
  node [
    id 211
    label "bi&#263;"
  ]
  node [
    id 212
    label "wspina&#263;_si&#281;"
  ]
  node [
    id 213
    label "naci&#261;ga&#263;"
  ]
  node [
    id 214
    label "gra&#263;"
  ]
  node [
    id 215
    label "tankowa&#263;"
  ]
  node [
    id 216
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 217
    label "bang"
  ]
  node [
    id 218
    label "domy&#347;la&#263;_si&#281;"
  ]
  node [
    id 219
    label "drench"
  ]
  node [
    id 220
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 221
    label "meet"
  ]
  node [
    id 222
    label "substancja"
  ]
  node [
    id 223
    label "saturate"
  ]
  node [
    id 224
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 225
    label "tworzy&#263;"
  ]
  node [
    id 226
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 227
    label "porywa&#263;"
  ]
  node [
    id 228
    label "korzysta&#263;"
  ]
  node [
    id 229
    label "take"
  ]
  node [
    id 230
    label "poczytywa&#263;"
  ]
  node [
    id 231
    label "levy"
  ]
  node [
    id 232
    label "wk&#322;ada&#263;"
  ]
  node [
    id 233
    label "raise"
  ]
  node [
    id 234
    label "by&#263;"
  ]
  node [
    id 235
    label "przyjmowa&#263;"
  ]
  node [
    id 236
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 237
    label "rucha&#263;"
  ]
  node [
    id 238
    label "prowadzi&#263;"
  ]
  node [
    id 239
    label "za&#380;ywa&#263;"
  ]
  node [
    id 240
    label "otrzymywa&#263;"
  ]
  node [
    id 241
    label "&#263;pa&#263;"
  ]
  node [
    id 242
    label "interpretowa&#263;"
  ]
  node [
    id 243
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 244
    label "dostawa&#263;"
  ]
  node [
    id 245
    label "rusza&#263;"
  ]
  node [
    id 246
    label "chwyta&#263;"
  ]
  node [
    id 247
    label "grza&#263;"
  ]
  node [
    id 248
    label "wch&#322;ania&#263;"
  ]
  node [
    id 249
    label "wygrywa&#263;"
  ]
  node [
    id 250
    label "u&#380;ywa&#263;"
  ]
  node [
    id 251
    label "ucieka&#263;"
  ]
  node [
    id 252
    label "arise"
  ]
  node [
    id 253
    label "uprawia&#263;_seks"
  ]
  node [
    id 254
    label "abstract"
  ]
  node [
    id 255
    label "towarzystwo"
  ]
  node [
    id 256
    label "branie"
  ]
  node [
    id 257
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 258
    label "zalicza&#263;"
  ]
  node [
    id 259
    label "wzi&#261;&#263;"
  ]
  node [
    id 260
    label "&#322;apa&#263;"
  ]
  node [
    id 261
    label "przewa&#380;a&#263;"
  ]
  node [
    id 262
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 263
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 264
    label "ekskursja"
  ]
  node [
    id 265
    label "bezsilnikowy"
  ]
  node [
    id 266
    label "budowla"
  ]
  node [
    id 267
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 268
    label "trasa"
  ]
  node [
    id 269
    label "podbieg"
  ]
  node [
    id 270
    label "turystyka"
  ]
  node [
    id 271
    label "nawierzchnia"
  ]
  node [
    id 272
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 273
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 274
    label "rajza"
  ]
  node [
    id 275
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 276
    label "korona_drogi"
  ]
  node [
    id 277
    label "passage"
  ]
  node [
    id 278
    label "wylot"
  ]
  node [
    id 279
    label "ekwipunek"
  ]
  node [
    id 280
    label "zbior&#243;wka"
  ]
  node [
    id 281
    label "marszrutyzacja"
  ]
  node [
    id 282
    label "wyb&#243;j"
  ]
  node [
    id 283
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 284
    label "drogowskaz"
  ]
  node [
    id 285
    label "spos&#243;b"
  ]
  node [
    id 286
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 287
    label "pobocze"
  ]
  node [
    id 288
    label "journey"
  ]
  node [
    id 289
    label "ruch"
  ]
  node [
    id 290
    label "przebieg"
  ]
  node [
    id 291
    label "infrastruktura"
  ]
  node [
    id 292
    label "w&#281;ze&#322;"
  ]
  node [
    id 293
    label "obudowanie"
  ]
  node [
    id 294
    label "obudowywa&#263;"
  ]
  node [
    id 295
    label "zbudowa&#263;"
  ]
  node [
    id 296
    label "obudowa&#263;"
  ]
  node [
    id 297
    label "kolumnada"
  ]
  node [
    id 298
    label "korpus"
  ]
  node [
    id 299
    label "Sukiennice"
  ]
  node [
    id 300
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 301
    label "fundament"
  ]
  node [
    id 302
    label "obudowywanie"
  ]
  node [
    id 303
    label "postanie"
  ]
  node [
    id 304
    label "zbudowanie"
  ]
  node [
    id 305
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 306
    label "stan_surowy"
  ]
  node [
    id 307
    label "konstrukcja"
  ]
  node [
    id 308
    label "rzecz"
  ]
  node [
    id 309
    label "model"
  ]
  node [
    id 310
    label "tryb"
  ]
  node [
    id 311
    label "nature"
  ]
  node [
    id 312
    label "ton"
  ]
  node [
    id 313
    label "rozmiar"
  ]
  node [
    id 314
    label "odcinek"
  ]
  node [
    id 315
    label "ambitus"
  ]
  node [
    id 316
    label "czas"
  ]
  node [
    id 317
    label "skala"
  ]
  node [
    id 318
    label "mechanika"
  ]
  node [
    id 319
    label "utrzymywanie"
  ]
  node [
    id 320
    label "poruszenie"
  ]
  node [
    id 321
    label "movement"
  ]
  node [
    id 322
    label "myk"
  ]
  node [
    id 323
    label "utrzyma&#263;"
  ]
  node [
    id 324
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 325
    label "zjawisko"
  ]
  node [
    id 326
    label "utrzymanie"
  ]
  node [
    id 327
    label "travel"
  ]
  node [
    id 328
    label "kanciasty"
  ]
  node [
    id 329
    label "commercial_enterprise"
  ]
  node [
    id 330
    label "strumie&#324;"
  ]
  node [
    id 331
    label "proces"
  ]
  node [
    id 332
    label "aktywno&#347;&#263;"
  ]
  node [
    id 333
    label "kr&#243;tki"
  ]
  node [
    id 334
    label "taktyka"
  ]
  node [
    id 335
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 336
    label "apraksja"
  ]
  node [
    id 337
    label "natural_process"
  ]
  node [
    id 338
    label "utrzymywa&#263;"
  ]
  node [
    id 339
    label "d&#322;ugi"
  ]
  node [
    id 340
    label "wydarzenie"
  ]
  node [
    id 341
    label "dyssypacja_energii"
  ]
  node [
    id 342
    label "tumult"
  ]
  node [
    id 343
    label "stopek"
  ]
  node [
    id 344
    label "zmiana"
  ]
  node [
    id 345
    label "manewr"
  ]
  node [
    id 346
    label "lokomocja"
  ]
  node [
    id 347
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 348
    label "komunikacja"
  ]
  node [
    id 349
    label "drift"
  ]
  node [
    id 350
    label "warstwa"
  ]
  node [
    id 351
    label "pokrycie"
  ]
  node [
    id 352
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 353
    label "fingerpost"
  ]
  node [
    id 354
    label "tablica"
  ]
  node [
    id 355
    label "r&#281;kaw"
  ]
  node [
    id 356
    label "kontusz"
  ]
  node [
    id 357
    label "koniec"
  ]
  node [
    id 358
    label "otw&#243;r"
  ]
  node [
    id 359
    label "przydro&#380;e"
  ]
  node [
    id 360
    label "autostrada"
  ]
  node [
    id 361
    label "operacja"
  ]
  node [
    id 362
    label "bieg"
  ]
  node [
    id 363
    label "podr&#243;&#380;"
  ]
  node [
    id 364
    label "digress"
  ]
  node [
    id 365
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 366
    label "pozostawa&#263;"
  ]
  node [
    id 367
    label "s&#261;dzi&#263;"
  ]
  node [
    id 368
    label "chodzi&#263;"
  ]
  node [
    id 369
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 370
    label "stray"
  ]
  node [
    id 371
    label "mieszanie_si&#281;"
  ]
  node [
    id 372
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 373
    label "chodzenie"
  ]
  node [
    id 374
    label "beznap&#281;dowy"
  ]
  node [
    id 375
    label "dormitorium"
  ]
  node [
    id 376
    label "sk&#322;adanka"
  ]
  node [
    id 377
    label "wyprawa"
  ]
  node [
    id 378
    label "polowanie"
  ]
  node [
    id 379
    label "spis"
  ]
  node [
    id 380
    label "pomieszczenie"
  ]
  node [
    id 381
    label "fotografia"
  ]
  node [
    id 382
    label "kocher"
  ]
  node [
    id 383
    label "wyposa&#380;enie"
  ]
  node [
    id 384
    label "nie&#347;miertelnik"
  ]
  node [
    id 385
    label "moderunek"
  ]
  node [
    id 386
    label "cz&#322;owiek"
  ]
  node [
    id 387
    label "ukochanie"
  ]
  node [
    id 388
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 389
    label "feblik"
  ]
  node [
    id 390
    label "podnieci&#263;"
  ]
  node [
    id 391
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 392
    label "numer"
  ]
  node [
    id 393
    label "po&#380;ycie"
  ]
  node [
    id 394
    label "tendency"
  ]
  node [
    id 395
    label "podniecenie"
  ]
  node [
    id 396
    label "afekt"
  ]
  node [
    id 397
    label "zakochanie"
  ]
  node [
    id 398
    label "zajawka"
  ]
  node [
    id 399
    label "seks"
  ]
  node [
    id 400
    label "podniecanie"
  ]
  node [
    id 401
    label "imisja"
  ]
  node [
    id 402
    label "love"
  ]
  node [
    id 403
    label "rozmna&#380;anie"
  ]
  node [
    id 404
    label "ruch_frykcyjny"
  ]
  node [
    id 405
    label "na_pieska"
  ]
  node [
    id 406
    label "serce"
  ]
  node [
    id 407
    label "pozycja_misjonarska"
  ]
  node [
    id 408
    label "wi&#281;&#378;"
  ]
  node [
    id 409
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 410
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 411
    label "z&#322;&#261;czenie"
  ]
  node [
    id 412
    label "gra_wst&#281;pna"
  ]
  node [
    id 413
    label "erotyka"
  ]
  node [
    id 414
    label "emocja"
  ]
  node [
    id 415
    label "baraszki"
  ]
  node [
    id 416
    label "drogi"
  ]
  node [
    id 417
    label "po&#380;&#261;danie"
  ]
  node [
    id 418
    label "wzw&#243;d"
  ]
  node [
    id 419
    label "podnieca&#263;"
  ]
  node [
    id 420
    label "psiapsi&#243;&#322;ka"
  ]
  node [
    id 421
    label "kochanka"
  ]
  node [
    id 422
    label "kultura_fizyczna"
  ]
  node [
    id 423
    label "turyzm"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
]
