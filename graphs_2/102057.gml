graph [
  node [
    id 0
    label "qmam"
    origin "text"
  ]
  node [
    id 1
    label "the"
    origin "text"
  ]
  node [
    id 2
    label "best"
    origin "text"
  ]
  node [
    id 3
    label "internauta"
    origin "text"
  ]
  node [
    id 4
    label "wybra&#263;"
    origin "text"
  ]
  node [
    id 5
    label "laureat"
    origin "text"
  ]
  node [
    id 6
    label "u&#380;ytkownik"
  ]
  node [
    id 7
    label "podmiot"
  ]
  node [
    id 8
    label "j&#281;zykowo"
  ]
  node [
    id 9
    label "powo&#322;a&#263;"
  ]
  node [
    id 10
    label "sie&#263;_rybacka"
  ]
  node [
    id 11
    label "zu&#380;y&#263;"
  ]
  node [
    id 12
    label "wyj&#261;&#263;"
  ]
  node [
    id 13
    label "ustali&#263;"
  ]
  node [
    id 14
    label "distill"
  ]
  node [
    id 15
    label "pick"
  ]
  node [
    id 16
    label "kotwica"
  ]
  node [
    id 17
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 18
    label "wydzieli&#263;"
  ]
  node [
    id 19
    label "wy&#322;&#261;czy&#263;"
  ]
  node [
    id 20
    label "remove"
  ]
  node [
    id 21
    label "spowodowa&#263;"
  ]
  node [
    id 22
    label "consume"
  ]
  node [
    id 23
    label "zrobi&#263;"
  ]
  node [
    id 24
    label "appoint"
  ]
  node [
    id 25
    label "poborowy"
  ]
  node [
    id 26
    label "wskaza&#263;"
  ]
  node [
    id 27
    label "put"
  ]
  node [
    id 28
    label "zdecydowa&#263;"
  ]
  node [
    id 29
    label "bind"
  ]
  node [
    id 30
    label "umocni&#263;"
  ]
  node [
    id 31
    label "unwrap"
  ]
  node [
    id 32
    label "narz&#281;dzie"
  ]
  node [
    id 33
    label "kotwiczy&#263;"
  ]
  node [
    id 34
    label "zakotwiczenie"
  ]
  node [
    id 35
    label "emocja"
  ]
  node [
    id 36
    label "wybieranie"
  ]
  node [
    id 37
    label "wybiera&#263;"
  ]
  node [
    id 38
    label "statek"
  ]
  node [
    id 39
    label "zegar"
  ]
  node [
    id 40
    label "zakotwiczy&#263;"
  ]
  node [
    id 41
    label "wybranie"
  ]
  node [
    id 42
    label "kotwiczenie"
  ]
  node [
    id 43
    label "zdobywca"
  ]
  node [
    id 44
    label "w&#243;dz"
  ]
  node [
    id 45
    label "zwyci&#281;zca"
  ]
  node [
    id 46
    label "odkrywca"
  ]
  node [
    id 47
    label "podr&#243;&#380;nik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
]
