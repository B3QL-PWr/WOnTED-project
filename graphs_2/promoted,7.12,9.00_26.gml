graph [
  node [
    id 0
    label "go&#324;"
    origin "text"
  ]
  node [
    id 1
    label "pan"
    origin "text"
  ]
  node [
    id 2
    label "kielce"
    origin "text"
  ]
  node [
    id 3
    label "krzycze&#263;"
    origin "text"
  ]
  node [
    id 4
    label "przez"
    origin "text"
  ]
  node [
    id 5
    label "stacja"
    origin "text"
  ]
  node [
    id 6
    label "poleca&#263;"
    origin "text"
  ]
  node [
    id 7
    label "dy&#380;urny"
    origin "text"
  ]
  node [
    id 8
    label "murza"
  ]
  node [
    id 9
    label "belfer"
  ]
  node [
    id 10
    label "szkolnik"
  ]
  node [
    id 11
    label "pupil"
  ]
  node [
    id 12
    label "ojciec"
  ]
  node [
    id 13
    label "kszta&#322;ciciel"
  ]
  node [
    id 14
    label "Midas"
  ]
  node [
    id 15
    label "przyw&#243;dca"
  ]
  node [
    id 16
    label "opiekun"
  ]
  node [
    id 17
    label "Mieszko_I"
  ]
  node [
    id 18
    label "doros&#322;y"
  ]
  node [
    id 19
    label "pracodawca"
  ]
  node [
    id 20
    label "profesor"
  ]
  node [
    id 21
    label "m&#261;&#380;"
  ]
  node [
    id 22
    label "rz&#261;dzenie"
  ]
  node [
    id 23
    label "bogaty"
  ]
  node [
    id 24
    label "cz&#322;owiek"
  ]
  node [
    id 25
    label "pa&#324;stwo"
  ]
  node [
    id 26
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 27
    label "w&#322;odarz"
  ]
  node [
    id 28
    label "nabab"
  ]
  node [
    id 29
    label "samiec"
  ]
  node [
    id 30
    label "preceptor"
  ]
  node [
    id 31
    label "pedagog"
  ]
  node [
    id 32
    label "efendi"
  ]
  node [
    id 33
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 34
    label "popularyzator"
  ]
  node [
    id 35
    label "gra_w_karty"
  ]
  node [
    id 36
    label "zwrot"
  ]
  node [
    id 37
    label "jegomo&#347;&#263;"
  ]
  node [
    id 38
    label "androlog"
  ]
  node [
    id 39
    label "bratek"
  ]
  node [
    id 40
    label "andropauza"
  ]
  node [
    id 41
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 42
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 43
    label "ch&#322;opina"
  ]
  node [
    id 44
    label "w&#322;adza"
  ]
  node [
    id 45
    label "Sabataj_Cwi"
  ]
  node [
    id 46
    label "lider"
  ]
  node [
    id 47
    label "Mao"
  ]
  node [
    id 48
    label "Anders"
  ]
  node [
    id 49
    label "Fidel_Castro"
  ]
  node [
    id 50
    label "Miko&#322;ajczyk"
  ]
  node [
    id 51
    label "Tito"
  ]
  node [
    id 52
    label "Ko&#347;ciuszko"
  ]
  node [
    id 53
    label "p&#322;atnik"
  ]
  node [
    id 54
    label "zwierzchnik"
  ]
  node [
    id 55
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 56
    label "nadzorca"
  ]
  node [
    id 57
    label "funkcjonariusz"
  ]
  node [
    id 58
    label "podmiot"
  ]
  node [
    id 59
    label "wykupywanie"
  ]
  node [
    id 60
    label "wykupienie"
  ]
  node [
    id 61
    label "bycie_w_posiadaniu"
  ]
  node [
    id 62
    label "rozszerzyciel"
  ]
  node [
    id 63
    label "asymilowa&#263;"
  ]
  node [
    id 64
    label "nasada"
  ]
  node [
    id 65
    label "profanum"
  ]
  node [
    id 66
    label "wz&#243;r"
  ]
  node [
    id 67
    label "senior"
  ]
  node [
    id 68
    label "asymilowanie"
  ]
  node [
    id 69
    label "os&#322;abia&#263;"
  ]
  node [
    id 70
    label "homo_sapiens"
  ]
  node [
    id 71
    label "osoba"
  ]
  node [
    id 72
    label "ludzko&#347;&#263;"
  ]
  node [
    id 73
    label "Adam"
  ]
  node [
    id 74
    label "hominid"
  ]
  node [
    id 75
    label "posta&#263;"
  ]
  node [
    id 76
    label "portrecista"
  ]
  node [
    id 77
    label "polifag"
  ]
  node [
    id 78
    label "podw&#322;adny"
  ]
  node [
    id 79
    label "dwun&#243;g"
  ]
  node [
    id 80
    label "wapniak"
  ]
  node [
    id 81
    label "duch"
  ]
  node [
    id 82
    label "os&#322;abianie"
  ]
  node [
    id 83
    label "antropochoria"
  ]
  node [
    id 84
    label "figura"
  ]
  node [
    id 85
    label "g&#322;owa"
  ]
  node [
    id 86
    label "mikrokosmos"
  ]
  node [
    id 87
    label "oddzia&#322;ywanie"
  ]
  node [
    id 88
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 89
    label "du&#380;y"
  ]
  node [
    id 90
    label "dojrza&#322;y"
  ]
  node [
    id 91
    label "dojrzale"
  ]
  node [
    id 92
    label "wydoro&#347;lenie"
  ]
  node [
    id 93
    label "doro&#347;lenie"
  ]
  node [
    id 94
    label "m&#261;dry"
  ]
  node [
    id 95
    label "&#378;ra&#322;y"
  ]
  node [
    id 96
    label "doletni"
  ]
  node [
    id 97
    label "doro&#347;le"
  ]
  node [
    id 98
    label "punkt"
  ]
  node [
    id 99
    label "zmiana"
  ]
  node [
    id 100
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 101
    label "turn"
  ]
  node [
    id 102
    label "wyra&#380;enie"
  ]
  node [
    id 103
    label "fraza_czasownikowa"
  ]
  node [
    id 104
    label "turning"
  ]
  node [
    id 105
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 106
    label "skr&#281;t"
  ]
  node [
    id 107
    label "jednostka_leksykalna"
  ]
  node [
    id 108
    label "obr&#243;t"
  ]
  node [
    id 109
    label "starosta"
  ]
  node [
    id 110
    label "w&#322;adca"
  ]
  node [
    id 111
    label "zarz&#261;dca"
  ]
  node [
    id 112
    label "nauczyciel_akademicki"
  ]
  node [
    id 113
    label "tytu&#322;"
  ]
  node [
    id 114
    label "stopie&#324;_naukowy"
  ]
  node [
    id 115
    label "konsulent"
  ]
  node [
    id 116
    label "profesura"
  ]
  node [
    id 117
    label "nauczyciel"
  ]
  node [
    id 118
    label "wirtuoz"
  ]
  node [
    id 119
    label "autor"
  ]
  node [
    id 120
    label "szko&#322;a"
  ]
  node [
    id 121
    label "tarcza"
  ]
  node [
    id 122
    label "klasa"
  ]
  node [
    id 123
    label "elew"
  ]
  node [
    id 124
    label "wyprawka"
  ]
  node [
    id 125
    label "mundurek"
  ]
  node [
    id 126
    label "absolwent"
  ]
  node [
    id 127
    label "ochotnik"
  ]
  node [
    id 128
    label "nauczyciel_muzyki"
  ]
  node [
    id 129
    label "pomocnik"
  ]
  node [
    id 130
    label "zakonnik"
  ]
  node [
    id 131
    label "student"
  ]
  node [
    id 132
    label "ekspert"
  ]
  node [
    id 133
    label "bogacz"
  ]
  node [
    id 134
    label "dostojnik"
  ]
  node [
    id 135
    label "urz&#281;dnik"
  ]
  node [
    id 136
    label "&#347;w"
  ]
  node [
    id 137
    label "rodzic"
  ]
  node [
    id 138
    label "pomys&#322;odawca"
  ]
  node [
    id 139
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 140
    label "rodzice"
  ]
  node [
    id 141
    label "wykonawca"
  ]
  node [
    id 142
    label "stary"
  ]
  node [
    id 143
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 144
    label "kuwada"
  ]
  node [
    id 145
    label "ojczym"
  ]
  node [
    id 146
    label "papa"
  ]
  node [
    id 147
    label "przodek"
  ]
  node [
    id 148
    label "tworzyciel"
  ]
  node [
    id 149
    label "facet"
  ]
  node [
    id 150
    label "kochanek"
  ]
  node [
    id 151
    label "fio&#322;ek"
  ]
  node [
    id 152
    label "brat"
  ]
  node [
    id 153
    label "zwierz&#281;"
  ]
  node [
    id 154
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 155
    label "pan_i_w&#322;adca"
  ]
  node [
    id 156
    label "pan_m&#322;ody"
  ]
  node [
    id 157
    label "ch&#322;op"
  ]
  node [
    id 158
    label "&#347;lubny"
  ]
  node [
    id 159
    label "m&#243;j"
  ]
  node [
    id 160
    label "pan_domu"
  ]
  node [
    id 161
    label "ma&#322;&#380;onek"
  ]
  node [
    id 162
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 163
    label "mo&#347;&#263;"
  ]
  node [
    id 164
    label "Frygia"
  ]
  node [
    id 165
    label "dominowanie"
  ]
  node [
    id 166
    label "reign"
  ]
  node [
    id 167
    label "sprawowanie"
  ]
  node [
    id 168
    label "dominion"
  ]
  node [
    id 169
    label "rule"
  ]
  node [
    id 170
    label "zwierz&#281;_domowe"
  ]
  node [
    id 171
    label "John_Dewey"
  ]
  node [
    id 172
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 173
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 174
    label "J&#281;drzejewicz"
  ]
  node [
    id 175
    label "specjalista"
  ]
  node [
    id 176
    label "&#380;ycie"
  ]
  node [
    id 177
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 178
    label "Turek"
  ]
  node [
    id 179
    label "effendi"
  ]
  node [
    id 180
    label "och&#281;do&#380;ny"
  ]
  node [
    id 181
    label "zapa&#347;ny"
  ]
  node [
    id 182
    label "sytuowany"
  ]
  node [
    id 183
    label "obfituj&#261;cy"
  ]
  node [
    id 184
    label "forsiasty"
  ]
  node [
    id 185
    label "spania&#322;y"
  ]
  node [
    id 186
    label "obficie"
  ]
  node [
    id 187
    label "r&#243;&#380;norodny"
  ]
  node [
    id 188
    label "bogato"
  ]
  node [
    id 189
    label "Japonia"
  ]
  node [
    id 190
    label "Zair"
  ]
  node [
    id 191
    label "Belize"
  ]
  node [
    id 192
    label "San_Marino"
  ]
  node [
    id 193
    label "Tanzania"
  ]
  node [
    id 194
    label "Antigua_i_Barbuda"
  ]
  node [
    id 195
    label "granica_pa&#324;stwa"
  ]
  node [
    id 196
    label "Senegal"
  ]
  node [
    id 197
    label "Indie"
  ]
  node [
    id 198
    label "Seszele"
  ]
  node [
    id 199
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 200
    label "Zimbabwe"
  ]
  node [
    id 201
    label "Filipiny"
  ]
  node [
    id 202
    label "Mauretania"
  ]
  node [
    id 203
    label "Malezja"
  ]
  node [
    id 204
    label "Rumunia"
  ]
  node [
    id 205
    label "Surinam"
  ]
  node [
    id 206
    label "Ukraina"
  ]
  node [
    id 207
    label "Syria"
  ]
  node [
    id 208
    label "Wyspy_Marshalla"
  ]
  node [
    id 209
    label "Burkina_Faso"
  ]
  node [
    id 210
    label "Grecja"
  ]
  node [
    id 211
    label "Polska"
  ]
  node [
    id 212
    label "Wenezuela"
  ]
  node [
    id 213
    label "Nepal"
  ]
  node [
    id 214
    label "Suazi"
  ]
  node [
    id 215
    label "S&#322;owacja"
  ]
  node [
    id 216
    label "Algieria"
  ]
  node [
    id 217
    label "Chiny"
  ]
  node [
    id 218
    label "Grenada"
  ]
  node [
    id 219
    label "Barbados"
  ]
  node [
    id 220
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 221
    label "Pakistan"
  ]
  node [
    id 222
    label "Niemcy"
  ]
  node [
    id 223
    label "Bahrajn"
  ]
  node [
    id 224
    label "Komory"
  ]
  node [
    id 225
    label "Australia"
  ]
  node [
    id 226
    label "Rodezja"
  ]
  node [
    id 227
    label "Malawi"
  ]
  node [
    id 228
    label "Gwinea"
  ]
  node [
    id 229
    label "Wehrlen"
  ]
  node [
    id 230
    label "Meksyk"
  ]
  node [
    id 231
    label "Liechtenstein"
  ]
  node [
    id 232
    label "Czarnog&#243;ra"
  ]
  node [
    id 233
    label "Wielka_Brytania"
  ]
  node [
    id 234
    label "Kuwejt"
  ]
  node [
    id 235
    label "Angola"
  ]
  node [
    id 236
    label "Monako"
  ]
  node [
    id 237
    label "Jemen"
  ]
  node [
    id 238
    label "Etiopia"
  ]
  node [
    id 239
    label "Madagaskar"
  ]
  node [
    id 240
    label "terytorium"
  ]
  node [
    id 241
    label "Kolumbia"
  ]
  node [
    id 242
    label "Portoryko"
  ]
  node [
    id 243
    label "Mauritius"
  ]
  node [
    id 244
    label "Kostaryka"
  ]
  node [
    id 245
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 246
    label "Tajlandia"
  ]
  node [
    id 247
    label "Argentyna"
  ]
  node [
    id 248
    label "Zambia"
  ]
  node [
    id 249
    label "Sri_Lanka"
  ]
  node [
    id 250
    label "Gwatemala"
  ]
  node [
    id 251
    label "Kirgistan"
  ]
  node [
    id 252
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 253
    label "Hiszpania"
  ]
  node [
    id 254
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 255
    label "Salwador"
  ]
  node [
    id 256
    label "Korea"
  ]
  node [
    id 257
    label "Macedonia"
  ]
  node [
    id 258
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 259
    label "Brunei"
  ]
  node [
    id 260
    label "Mozambik"
  ]
  node [
    id 261
    label "Turcja"
  ]
  node [
    id 262
    label "Kambod&#380;a"
  ]
  node [
    id 263
    label "Benin"
  ]
  node [
    id 264
    label "Bhutan"
  ]
  node [
    id 265
    label "Tunezja"
  ]
  node [
    id 266
    label "Austria"
  ]
  node [
    id 267
    label "Izrael"
  ]
  node [
    id 268
    label "Sierra_Leone"
  ]
  node [
    id 269
    label "Jamajka"
  ]
  node [
    id 270
    label "Rosja"
  ]
  node [
    id 271
    label "Rwanda"
  ]
  node [
    id 272
    label "holoarktyka"
  ]
  node [
    id 273
    label "Nigeria"
  ]
  node [
    id 274
    label "USA"
  ]
  node [
    id 275
    label "Oman"
  ]
  node [
    id 276
    label "Luksemburg"
  ]
  node [
    id 277
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 278
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 279
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 280
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 281
    label "Dominikana"
  ]
  node [
    id 282
    label "Irlandia"
  ]
  node [
    id 283
    label "Liban"
  ]
  node [
    id 284
    label "Hanower"
  ]
  node [
    id 285
    label "Estonia"
  ]
  node [
    id 286
    label "Samoa"
  ]
  node [
    id 287
    label "Nowa_Zelandia"
  ]
  node [
    id 288
    label "Gabon"
  ]
  node [
    id 289
    label "Iran"
  ]
  node [
    id 290
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 291
    label "S&#322;owenia"
  ]
  node [
    id 292
    label "Egipt"
  ]
  node [
    id 293
    label "Kiribati"
  ]
  node [
    id 294
    label "Togo"
  ]
  node [
    id 295
    label "Mongolia"
  ]
  node [
    id 296
    label "Sudan"
  ]
  node [
    id 297
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 298
    label "Bahamy"
  ]
  node [
    id 299
    label "Bangladesz"
  ]
  node [
    id 300
    label "partia"
  ]
  node [
    id 301
    label "Serbia"
  ]
  node [
    id 302
    label "Czechy"
  ]
  node [
    id 303
    label "Holandia"
  ]
  node [
    id 304
    label "Birma"
  ]
  node [
    id 305
    label "Albania"
  ]
  node [
    id 306
    label "Mikronezja"
  ]
  node [
    id 307
    label "Gambia"
  ]
  node [
    id 308
    label "Kazachstan"
  ]
  node [
    id 309
    label "interior"
  ]
  node [
    id 310
    label "Uzbekistan"
  ]
  node [
    id 311
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 312
    label "Malta"
  ]
  node [
    id 313
    label "Lesoto"
  ]
  node [
    id 314
    label "para"
  ]
  node [
    id 315
    label "Antarktis"
  ]
  node [
    id 316
    label "Andora"
  ]
  node [
    id 317
    label "Nauru"
  ]
  node [
    id 318
    label "Kuba"
  ]
  node [
    id 319
    label "Wietnam"
  ]
  node [
    id 320
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 321
    label "ziemia"
  ]
  node [
    id 322
    label "Chorwacja"
  ]
  node [
    id 323
    label "Kamerun"
  ]
  node [
    id 324
    label "Urugwaj"
  ]
  node [
    id 325
    label "Niger"
  ]
  node [
    id 326
    label "Turkmenistan"
  ]
  node [
    id 327
    label "Szwajcaria"
  ]
  node [
    id 328
    label "organizacja"
  ]
  node [
    id 329
    label "grupa"
  ]
  node [
    id 330
    label "Litwa"
  ]
  node [
    id 331
    label "Palau"
  ]
  node [
    id 332
    label "Gruzja"
  ]
  node [
    id 333
    label "Kongo"
  ]
  node [
    id 334
    label "Tajwan"
  ]
  node [
    id 335
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 336
    label "Honduras"
  ]
  node [
    id 337
    label "Boliwia"
  ]
  node [
    id 338
    label "Uganda"
  ]
  node [
    id 339
    label "Namibia"
  ]
  node [
    id 340
    label "Erytrea"
  ]
  node [
    id 341
    label "Azerbejd&#380;an"
  ]
  node [
    id 342
    label "Panama"
  ]
  node [
    id 343
    label "Gujana"
  ]
  node [
    id 344
    label "Somalia"
  ]
  node [
    id 345
    label "Burundi"
  ]
  node [
    id 346
    label "Tuwalu"
  ]
  node [
    id 347
    label "Libia"
  ]
  node [
    id 348
    label "Katar"
  ]
  node [
    id 349
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 350
    label "Trynidad_i_Tobago"
  ]
  node [
    id 351
    label "Sahara_Zachodnia"
  ]
  node [
    id 352
    label "Gwinea_Bissau"
  ]
  node [
    id 353
    label "Bu&#322;garia"
  ]
  node [
    id 354
    label "Tonga"
  ]
  node [
    id 355
    label "Nikaragua"
  ]
  node [
    id 356
    label "Fid&#380;i"
  ]
  node [
    id 357
    label "Timor_Wschodni"
  ]
  node [
    id 358
    label "Laos"
  ]
  node [
    id 359
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 360
    label "Ghana"
  ]
  node [
    id 361
    label "Brazylia"
  ]
  node [
    id 362
    label "Belgia"
  ]
  node [
    id 363
    label "Irak"
  ]
  node [
    id 364
    label "Peru"
  ]
  node [
    id 365
    label "Arabia_Saudyjska"
  ]
  node [
    id 366
    label "Indonezja"
  ]
  node [
    id 367
    label "Malediwy"
  ]
  node [
    id 368
    label "Afganistan"
  ]
  node [
    id 369
    label "Jordania"
  ]
  node [
    id 370
    label "Kenia"
  ]
  node [
    id 371
    label "Czad"
  ]
  node [
    id 372
    label "Liberia"
  ]
  node [
    id 373
    label "Mali"
  ]
  node [
    id 374
    label "Armenia"
  ]
  node [
    id 375
    label "W&#281;gry"
  ]
  node [
    id 376
    label "Chile"
  ]
  node [
    id 377
    label "Kanada"
  ]
  node [
    id 378
    label "Cypr"
  ]
  node [
    id 379
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 380
    label "Ekwador"
  ]
  node [
    id 381
    label "Mo&#322;dawia"
  ]
  node [
    id 382
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 383
    label "W&#322;ochy"
  ]
  node [
    id 384
    label "Wyspy_Salomona"
  ]
  node [
    id 385
    label "&#321;otwa"
  ]
  node [
    id 386
    label "D&#380;ibuti"
  ]
  node [
    id 387
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 388
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 389
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 390
    label "Portugalia"
  ]
  node [
    id 391
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 392
    label "Maroko"
  ]
  node [
    id 393
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 394
    label "Francja"
  ]
  node [
    id 395
    label "Botswana"
  ]
  node [
    id 396
    label "Dominika"
  ]
  node [
    id 397
    label "Paragwaj"
  ]
  node [
    id 398
    label "Tad&#380;ykistan"
  ]
  node [
    id 399
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 400
    label "Haiti"
  ]
  node [
    id 401
    label "Khitai"
  ]
  node [
    id 402
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 403
    label "gl&#281;dzi&#263;"
  ]
  node [
    id 404
    label "rant"
  ]
  node [
    id 405
    label "p&#322;aka&#263;"
  ]
  node [
    id 406
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 407
    label "wydobywa&#263;"
  ]
  node [
    id 408
    label "wyjmowa&#263;"
  ]
  node [
    id 409
    label "wydostawa&#263;"
  ]
  node [
    id 410
    label "wydawa&#263;"
  ]
  node [
    id 411
    label "g&#243;rnictwo"
  ]
  node [
    id 412
    label "dobywa&#263;"
  ]
  node [
    id 413
    label "uwydatnia&#263;"
  ]
  node [
    id 414
    label "eksploatowa&#263;"
  ]
  node [
    id 415
    label "excavate"
  ]
  node [
    id 416
    label "raise"
  ]
  node [
    id 417
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 418
    label "train"
  ]
  node [
    id 419
    label "ocala&#263;"
  ]
  node [
    id 420
    label "uzyskiwa&#263;"
  ]
  node [
    id 421
    label "spill_the_beans"
  ]
  node [
    id 422
    label "rykowisko"
  ]
  node [
    id 423
    label "&#322;ania"
  ]
  node [
    id 424
    label "mumble"
  ]
  node [
    id 425
    label "gada&#263;"
  ]
  node [
    id 426
    label "robi&#263;"
  ]
  node [
    id 427
    label "cudowa&#263;"
  ]
  node [
    id 428
    label "backfire"
  ]
  node [
    id 429
    label "szkoda"
  ]
  node [
    id 430
    label "wy&#263;"
  ]
  node [
    id 431
    label "sorrow"
  ]
  node [
    id 432
    label "pokrywa&#263;_si&#281;"
  ]
  node [
    id 433
    label "reagowa&#263;"
  ]
  node [
    id 434
    label "sting"
  ]
  node [
    id 435
    label "narzeka&#263;"
  ]
  node [
    id 436
    label "wydziela&#263;"
  ]
  node [
    id 437
    label "snivel"
  ]
  node [
    id 438
    label "kraw&#281;d&#378;"
  ]
  node [
    id 439
    label "siedziba"
  ]
  node [
    id 440
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 441
    label "miejsce"
  ]
  node [
    id 442
    label "instytucja"
  ]
  node [
    id 443
    label "urz&#261;dzenie"
  ]
  node [
    id 444
    label "droga_krzy&#380;owa"
  ]
  node [
    id 445
    label "komora"
  ]
  node [
    id 446
    label "wyrz&#261;dzenie"
  ]
  node [
    id 447
    label "kom&#243;rka"
  ]
  node [
    id 448
    label "impulsator"
  ]
  node [
    id 449
    label "przygotowanie"
  ]
  node [
    id 450
    label "furnishing"
  ]
  node [
    id 451
    label "zabezpieczenie"
  ]
  node [
    id 452
    label "sprz&#281;t"
  ]
  node [
    id 453
    label "aparatura"
  ]
  node [
    id 454
    label "ig&#322;a"
  ]
  node [
    id 455
    label "wirnik"
  ]
  node [
    id 456
    label "przedmiot"
  ]
  node [
    id 457
    label "zablokowanie"
  ]
  node [
    id 458
    label "blokowanie"
  ]
  node [
    id 459
    label "j&#281;zyk"
  ]
  node [
    id 460
    label "czynno&#347;&#263;"
  ]
  node [
    id 461
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 462
    label "system_energetyczny"
  ]
  node [
    id 463
    label "narz&#281;dzie"
  ]
  node [
    id 464
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 465
    label "set"
  ]
  node [
    id 466
    label "zrobienie"
  ]
  node [
    id 467
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 468
    label "zagospodarowanie"
  ]
  node [
    id 469
    label "mechanizm"
  ]
  node [
    id 470
    label "miejsce_pracy"
  ]
  node [
    id 471
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 472
    label "budynek"
  ]
  node [
    id 473
    label "&#321;ubianka"
  ]
  node [
    id 474
    label "Bia&#322;y_Dom"
  ]
  node [
    id 475
    label "dzia&#322;_personalny"
  ]
  node [
    id 476
    label "Kreml"
  ]
  node [
    id 477
    label "sadowisko"
  ]
  node [
    id 478
    label "obiekt_matematyczny"
  ]
  node [
    id 479
    label "stopie&#324;_pisma"
  ]
  node [
    id 480
    label "pozycja"
  ]
  node [
    id 481
    label "problemat"
  ]
  node [
    id 482
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 483
    label "obiekt"
  ]
  node [
    id 484
    label "point"
  ]
  node [
    id 485
    label "plamka"
  ]
  node [
    id 486
    label "przestrze&#324;"
  ]
  node [
    id 487
    label "mark"
  ]
  node [
    id 488
    label "ust&#281;p"
  ]
  node [
    id 489
    label "po&#322;o&#380;enie"
  ]
  node [
    id 490
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 491
    label "kres"
  ]
  node [
    id 492
    label "plan"
  ]
  node [
    id 493
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 494
    label "chwila"
  ]
  node [
    id 495
    label "podpunkt"
  ]
  node [
    id 496
    label "jednostka"
  ]
  node [
    id 497
    label "sprawa"
  ]
  node [
    id 498
    label "problematyka"
  ]
  node [
    id 499
    label "prosta"
  ]
  node [
    id 500
    label "wojsko"
  ]
  node [
    id 501
    label "zapunktowa&#263;"
  ]
  node [
    id 502
    label "rz&#261;d"
  ]
  node [
    id 503
    label "uwaga"
  ]
  node [
    id 504
    label "cecha"
  ]
  node [
    id 505
    label "praca"
  ]
  node [
    id 506
    label "plac"
  ]
  node [
    id 507
    label "location"
  ]
  node [
    id 508
    label "warunek_lokalowy"
  ]
  node [
    id 509
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 510
    label "cia&#322;o"
  ]
  node [
    id 511
    label "status"
  ]
  node [
    id 512
    label "poj&#281;cie"
  ]
  node [
    id 513
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 514
    label "afiliowa&#263;"
  ]
  node [
    id 515
    label "establishment"
  ]
  node [
    id 516
    label "zamyka&#263;"
  ]
  node [
    id 517
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 518
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 519
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 520
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 521
    label "standard"
  ]
  node [
    id 522
    label "Fundusze_Unijne"
  ]
  node [
    id 523
    label "biuro"
  ]
  node [
    id 524
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 525
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 526
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 527
    label "zamykanie"
  ]
  node [
    id 528
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 529
    label "osoba_prawna"
  ]
  node [
    id 530
    label "urz&#261;d"
  ]
  node [
    id 531
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 532
    label "placard"
  ]
  node [
    id 533
    label "doradza&#263;"
  ]
  node [
    id 534
    label "m&#243;wi&#263;"
  ]
  node [
    id 535
    label "zadawa&#263;"
  ]
  node [
    id 536
    label "control"
  ]
  node [
    id 537
    label "ordynowa&#263;"
  ]
  node [
    id 538
    label "powierza&#263;"
  ]
  node [
    id 539
    label "charge"
  ]
  node [
    id 540
    label "plon"
  ]
  node [
    id 541
    label "d&#378;wi&#281;k"
  ]
  node [
    id 542
    label "reszta"
  ]
  node [
    id 543
    label "panna_na_wydaniu"
  ]
  node [
    id 544
    label "denuncjowa&#263;"
  ]
  node [
    id 545
    label "impart"
  ]
  node [
    id 546
    label "zapach"
  ]
  node [
    id 547
    label "mie&#263;_miejsce"
  ]
  node [
    id 548
    label "dawa&#263;"
  ]
  node [
    id 549
    label "wytwarza&#263;"
  ]
  node [
    id 550
    label "tajemnica"
  ]
  node [
    id 551
    label "give"
  ]
  node [
    id 552
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 553
    label "wiano"
  ]
  node [
    id 554
    label "podawa&#263;"
  ]
  node [
    id 555
    label "ujawnia&#263;"
  ]
  node [
    id 556
    label "produkcja"
  ]
  node [
    id 557
    label "kojarzy&#263;"
  ]
  node [
    id 558
    label "surrender"
  ]
  node [
    id 559
    label "wydawnictwo"
  ]
  node [
    id 560
    label "wprowadza&#263;"
  ]
  node [
    id 561
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 562
    label "dysfonia"
  ]
  node [
    id 563
    label "prawi&#263;"
  ]
  node [
    id 564
    label "remark"
  ]
  node [
    id 565
    label "express"
  ]
  node [
    id 566
    label "chew_the_fat"
  ]
  node [
    id 567
    label "talk"
  ]
  node [
    id 568
    label "say"
  ]
  node [
    id 569
    label "wyra&#380;a&#263;"
  ]
  node [
    id 570
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 571
    label "tell"
  ]
  node [
    id 572
    label "informowa&#263;"
  ]
  node [
    id 573
    label "rozmawia&#263;"
  ]
  node [
    id 574
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 575
    label "powiada&#263;"
  ]
  node [
    id 576
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 577
    label "okre&#347;la&#263;"
  ]
  node [
    id 578
    label "u&#380;ywa&#263;"
  ]
  node [
    id 579
    label "gaworzy&#263;"
  ]
  node [
    id 580
    label "formu&#322;owa&#263;"
  ]
  node [
    id 581
    label "dziama&#263;"
  ]
  node [
    id 582
    label "umie&#263;"
  ]
  node [
    id 583
    label "szkodzi&#263;"
  ]
  node [
    id 584
    label "inflict"
  ]
  node [
    id 585
    label "karmi&#263;"
  ]
  node [
    id 586
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 587
    label "share"
  ]
  node [
    id 588
    label "pose"
  ]
  node [
    id 589
    label "d&#378;wiga&#263;"
  ]
  node [
    id 590
    label "zajmowa&#263;"
  ]
  node [
    id 591
    label "zak&#322;ada&#263;"
  ]
  node [
    id 592
    label "deal"
  ]
  node [
    id 593
    label "wyznawa&#263;"
  ]
  node [
    id 594
    label "grant"
  ]
  node [
    id 595
    label "command"
  ]
  node [
    id 596
    label "confide"
  ]
  node [
    id 597
    label "zleca&#263;"
  ]
  node [
    id 598
    label "ufa&#263;"
  ]
  node [
    id 599
    label "oddawa&#263;"
  ]
  node [
    id 600
    label "rede"
  ]
  node [
    id 601
    label "radzi&#263;"
  ]
  node [
    id 602
    label "order"
  ]
  node [
    id 603
    label "wyprawia&#263;"
  ]
  node [
    id 604
    label "kierowa&#263;"
  ]
  node [
    id 605
    label "save"
  ]
  node [
    id 606
    label "zaleca&#263;"
  ]
  node [
    id 607
    label "klawisz"
  ]
  node [
    id 608
    label "sta&#322;y"
  ]
  node [
    id 609
    label "jednakowy"
  ]
  node [
    id 610
    label "regularny"
  ]
  node [
    id 611
    label "stale"
  ]
  node [
    id 612
    label "zwyk&#322;y"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 1
    target 393
  ]
  edge [
    source 1
    target 394
  ]
  edge [
    source 1
    target 395
  ]
  edge [
    source 1
    target 396
  ]
  edge [
    source 1
    target 397
  ]
  edge [
    source 1
    target 398
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 400
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 1
    target 402
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
]
