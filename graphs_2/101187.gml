graph [
  node [
    id 0
    label "system"
    origin "text"
  ]
  node [
    id 1
    label "zefir"
    origin "text"
  ]
  node [
    id 2
    label "informatyczny"
    origin "text"
  ]
  node [
    id 3
    label "kompleksowo"
    origin "text"
  ]
  node [
    id 4
    label "obs&#322;ugiwa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "sprawa"
    origin "text"
  ]
  node [
    id 6
    label "zwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 7
    label "finanse"
    origin "text"
  ]
  node [
    id 8
    label "polski"
    origin "text"
  ]
  node [
    id 9
    label "s&#322;u&#380;ba"
    origin "text"
  ]
  node [
    id 10
    label "celny"
    origin "text"
  ]
  node [
    id 11
    label "j&#261;dro"
  ]
  node [
    id 12
    label "systemik"
  ]
  node [
    id 13
    label "rozprz&#261;c"
  ]
  node [
    id 14
    label "oprogramowanie"
  ]
  node [
    id 15
    label "poj&#281;cie"
  ]
  node [
    id 16
    label "systemat"
  ]
  node [
    id 17
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 18
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 19
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 20
    label "model"
  ]
  node [
    id 21
    label "struktura"
  ]
  node [
    id 22
    label "usenet"
  ]
  node [
    id 23
    label "s&#261;d"
  ]
  node [
    id 24
    label "zbi&#243;r"
  ]
  node [
    id 25
    label "porz&#261;dek"
  ]
  node [
    id 26
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 27
    label "przyn&#281;ta"
  ]
  node [
    id 28
    label "p&#322;&#243;d"
  ]
  node [
    id 29
    label "net"
  ]
  node [
    id 30
    label "w&#281;dkarstwo"
  ]
  node [
    id 31
    label "eratem"
  ]
  node [
    id 32
    label "oddzia&#322;"
  ]
  node [
    id 33
    label "doktryna"
  ]
  node [
    id 34
    label "pulpit"
  ]
  node [
    id 35
    label "konstelacja"
  ]
  node [
    id 36
    label "jednostka_geologiczna"
  ]
  node [
    id 37
    label "o&#347;"
  ]
  node [
    id 38
    label "podsystem"
  ]
  node [
    id 39
    label "metoda"
  ]
  node [
    id 40
    label "ryba"
  ]
  node [
    id 41
    label "Leopard"
  ]
  node [
    id 42
    label "spos&#243;b"
  ]
  node [
    id 43
    label "Android"
  ]
  node [
    id 44
    label "zachowanie"
  ]
  node [
    id 45
    label "cybernetyk"
  ]
  node [
    id 46
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 47
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 48
    label "method"
  ]
  node [
    id 49
    label "sk&#322;ad"
  ]
  node [
    id 50
    label "podstawa"
  ]
  node [
    id 51
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 52
    label "narz&#281;dzie"
  ]
  node [
    id 53
    label "tryb"
  ]
  node [
    id 54
    label "nature"
  ]
  node [
    id 55
    label "pot&#281;ga"
  ]
  node [
    id 56
    label "documentation"
  ]
  node [
    id 57
    label "przedmiot"
  ]
  node [
    id 58
    label "column"
  ]
  node [
    id 59
    label "zasadzi&#263;"
  ]
  node [
    id 60
    label "za&#322;o&#380;enie"
  ]
  node [
    id 61
    label "punkt_odniesienia"
  ]
  node [
    id 62
    label "zasadzenie"
  ]
  node [
    id 63
    label "bok"
  ]
  node [
    id 64
    label "d&#243;&#322;"
  ]
  node [
    id 65
    label "dzieci&#281;ctwo"
  ]
  node [
    id 66
    label "background"
  ]
  node [
    id 67
    label "podstawowy"
  ]
  node [
    id 68
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 69
    label "strategia"
  ]
  node [
    id 70
    label "pomys&#322;"
  ]
  node [
    id 71
    label "&#347;ciana"
  ]
  node [
    id 72
    label "relacja"
  ]
  node [
    id 73
    label "uk&#322;ad"
  ]
  node [
    id 74
    label "stan"
  ]
  node [
    id 75
    label "zasada"
  ]
  node [
    id 76
    label "cecha"
  ]
  node [
    id 77
    label "styl_architektoniczny"
  ]
  node [
    id 78
    label "normalizacja"
  ]
  node [
    id 79
    label "pos&#322;uchanie"
  ]
  node [
    id 80
    label "skumanie"
  ]
  node [
    id 81
    label "orientacja"
  ]
  node [
    id 82
    label "wytw&#243;r"
  ]
  node [
    id 83
    label "zorientowanie"
  ]
  node [
    id 84
    label "teoria"
  ]
  node [
    id 85
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 86
    label "clasp"
  ]
  node [
    id 87
    label "forma"
  ]
  node [
    id 88
    label "przem&#243;wienie"
  ]
  node [
    id 89
    label "egzemplarz"
  ]
  node [
    id 90
    label "series"
  ]
  node [
    id 91
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 92
    label "uprawianie"
  ]
  node [
    id 93
    label "praca_rolnicza"
  ]
  node [
    id 94
    label "collection"
  ]
  node [
    id 95
    label "dane"
  ]
  node [
    id 96
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 97
    label "pakiet_klimatyczny"
  ]
  node [
    id 98
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 99
    label "sum"
  ]
  node [
    id 100
    label "gathering"
  ]
  node [
    id 101
    label "album"
  ]
  node [
    id 102
    label "system_komputerowy"
  ]
  node [
    id 103
    label "sprz&#281;t"
  ]
  node [
    id 104
    label "mechanika"
  ]
  node [
    id 105
    label "konstrukcja"
  ]
  node [
    id 106
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 107
    label "moczownik"
  ]
  node [
    id 108
    label "embryo"
  ]
  node [
    id 109
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 110
    label "zarodek"
  ]
  node [
    id 111
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 112
    label "latawiec"
  ]
  node [
    id 113
    label "reengineering"
  ]
  node [
    id 114
    label "program"
  ]
  node [
    id 115
    label "integer"
  ]
  node [
    id 116
    label "liczba"
  ]
  node [
    id 117
    label "zlewanie_si&#281;"
  ]
  node [
    id 118
    label "ilo&#347;&#263;"
  ]
  node [
    id 119
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 120
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 121
    label "pe&#322;ny"
  ]
  node [
    id 122
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 123
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 124
    label "grupa_dyskusyjna"
  ]
  node [
    id 125
    label "doctrine"
  ]
  node [
    id 126
    label "urozmaicenie"
  ]
  node [
    id 127
    label "pu&#322;apka"
  ]
  node [
    id 128
    label "pon&#281;ta"
  ]
  node [
    id 129
    label "wabik"
  ]
  node [
    id 130
    label "sport"
  ]
  node [
    id 131
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 132
    label "kr&#281;gowiec"
  ]
  node [
    id 133
    label "cz&#322;owiek"
  ]
  node [
    id 134
    label "doniczkowiec"
  ]
  node [
    id 135
    label "mi&#281;so"
  ]
  node [
    id 136
    label "patroszy&#263;"
  ]
  node [
    id 137
    label "rakowato&#347;&#263;"
  ]
  node [
    id 138
    label "ryby"
  ]
  node [
    id 139
    label "fish"
  ]
  node [
    id 140
    label "linia_boczna"
  ]
  node [
    id 141
    label "tar&#322;o"
  ]
  node [
    id 142
    label "wyrostek_filtracyjny"
  ]
  node [
    id 143
    label "m&#281;tnooki"
  ]
  node [
    id 144
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 145
    label "pokrywa_skrzelowa"
  ]
  node [
    id 146
    label "ikra"
  ]
  node [
    id 147
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 148
    label "szczelina_skrzelowa"
  ]
  node [
    id 149
    label "blat"
  ]
  node [
    id 150
    label "interfejs"
  ]
  node [
    id 151
    label "okno"
  ]
  node [
    id 152
    label "obszar"
  ]
  node [
    id 153
    label "ikona"
  ]
  node [
    id 154
    label "system_operacyjny"
  ]
  node [
    id 155
    label "mebel"
  ]
  node [
    id 156
    label "zdolno&#347;&#263;"
  ]
  node [
    id 157
    label "relaxation"
  ]
  node [
    id 158
    label "os&#322;abienie"
  ]
  node [
    id 159
    label "oswobodzenie"
  ]
  node [
    id 160
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 161
    label "zdezorganizowanie"
  ]
  node [
    id 162
    label "reakcja"
  ]
  node [
    id 163
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 164
    label "tajemnica"
  ]
  node [
    id 165
    label "wydarzenie"
  ]
  node [
    id 166
    label "pochowanie"
  ]
  node [
    id 167
    label "zdyscyplinowanie"
  ]
  node [
    id 168
    label "post&#261;pienie"
  ]
  node [
    id 169
    label "post"
  ]
  node [
    id 170
    label "bearing"
  ]
  node [
    id 171
    label "zwierz&#281;"
  ]
  node [
    id 172
    label "behawior"
  ]
  node [
    id 173
    label "observation"
  ]
  node [
    id 174
    label "dieta"
  ]
  node [
    id 175
    label "podtrzymanie"
  ]
  node [
    id 176
    label "etolog"
  ]
  node [
    id 177
    label "przechowanie"
  ]
  node [
    id 178
    label "zrobienie"
  ]
  node [
    id 179
    label "oswobodzi&#263;"
  ]
  node [
    id 180
    label "os&#322;abi&#263;"
  ]
  node [
    id 181
    label "disengage"
  ]
  node [
    id 182
    label "zdezorganizowa&#263;"
  ]
  node [
    id 183
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 184
    label "naukowiec"
  ]
  node [
    id 185
    label "provider"
  ]
  node [
    id 186
    label "b&#322;&#261;d"
  ]
  node [
    id 187
    label "hipertekst"
  ]
  node [
    id 188
    label "cyberprzestrze&#324;"
  ]
  node [
    id 189
    label "mem"
  ]
  node [
    id 190
    label "gra_sieciowa"
  ]
  node [
    id 191
    label "grooming"
  ]
  node [
    id 192
    label "media"
  ]
  node [
    id 193
    label "biznes_elektroniczny"
  ]
  node [
    id 194
    label "sie&#263;_komputerowa"
  ]
  node [
    id 195
    label "punkt_dost&#281;pu"
  ]
  node [
    id 196
    label "us&#322;uga_internetowa"
  ]
  node [
    id 197
    label "netbook"
  ]
  node [
    id 198
    label "e-hazard"
  ]
  node [
    id 199
    label "podcast"
  ]
  node [
    id 200
    label "strona"
  ]
  node [
    id 201
    label "prezenter"
  ]
  node [
    id 202
    label "typ"
  ]
  node [
    id 203
    label "mildew"
  ]
  node [
    id 204
    label "zi&#243;&#322;ko"
  ]
  node [
    id 205
    label "motif"
  ]
  node [
    id 206
    label "pozowanie"
  ]
  node [
    id 207
    label "ideal"
  ]
  node [
    id 208
    label "wz&#243;r"
  ]
  node [
    id 209
    label "matryca"
  ]
  node [
    id 210
    label "adaptation"
  ]
  node [
    id 211
    label "ruch"
  ]
  node [
    id 212
    label "pozowa&#263;"
  ]
  node [
    id 213
    label "imitacja"
  ]
  node [
    id 214
    label "orygina&#322;"
  ]
  node [
    id 215
    label "facet"
  ]
  node [
    id 216
    label "miniatura"
  ]
  node [
    id 217
    label "zesp&#243;&#322;"
  ]
  node [
    id 218
    label "podejrzany"
  ]
  node [
    id 219
    label "s&#261;downictwo"
  ]
  node [
    id 220
    label "biuro"
  ]
  node [
    id 221
    label "court"
  ]
  node [
    id 222
    label "forum"
  ]
  node [
    id 223
    label "bronienie"
  ]
  node [
    id 224
    label "urz&#261;d"
  ]
  node [
    id 225
    label "oskar&#380;yciel"
  ]
  node [
    id 226
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 227
    label "skazany"
  ]
  node [
    id 228
    label "post&#281;powanie"
  ]
  node [
    id 229
    label "broni&#263;"
  ]
  node [
    id 230
    label "my&#347;l"
  ]
  node [
    id 231
    label "pods&#261;dny"
  ]
  node [
    id 232
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 233
    label "obrona"
  ]
  node [
    id 234
    label "wypowied&#378;"
  ]
  node [
    id 235
    label "instytucja"
  ]
  node [
    id 236
    label "antylogizm"
  ]
  node [
    id 237
    label "konektyw"
  ]
  node [
    id 238
    label "&#347;wiadek"
  ]
  node [
    id 239
    label "procesowicz"
  ]
  node [
    id 240
    label "dzia&#322;"
  ]
  node [
    id 241
    label "lias"
  ]
  node [
    id 242
    label "jednostka"
  ]
  node [
    id 243
    label "pi&#281;tro"
  ]
  node [
    id 244
    label "klasa"
  ]
  node [
    id 245
    label "filia"
  ]
  node [
    id 246
    label "malm"
  ]
  node [
    id 247
    label "whole"
  ]
  node [
    id 248
    label "dogger"
  ]
  node [
    id 249
    label "poziom"
  ]
  node [
    id 250
    label "promocja"
  ]
  node [
    id 251
    label "kurs"
  ]
  node [
    id 252
    label "bank"
  ]
  node [
    id 253
    label "formacja"
  ]
  node [
    id 254
    label "ajencja"
  ]
  node [
    id 255
    label "wojsko"
  ]
  node [
    id 256
    label "siedziba"
  ]
  node [
    id 257
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 258
    label "agencja"
  ]
  node [
    id 259
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 260
    label "szpital"
  ]
  node [
    id 261
    label "algebra_liniowa"
  ]
  node [
    id 262
    label "macierz_j&#261;drowa"
  ]
  node [
    id 263
    label "atom"
  ]
  node [
    id 264
    label "nukleon"
  ]
  node [
    id 265
    label "kariokineza"
  ]
  node [
    id 266
    label "core"
  ]
  node [
    id 267
    label "chemia_j&#261;drowa"
  ]
  node [
    id 268
    label "anorchizm"
  ]
  node [
    id 269
    label "os&#322;onka_pochwowa_j&#261;dra"
  ]
  node [
    id 270
    label "nasieniak"
  ]
  node [
    id 271
    label "wn&#281;trostwo"
  ]
  node [
    id 272
    label "ziarno"
  ]
  node [
    id 273
    label "kom&#243;rka_Sertolego"
  ]
  node [
    id 274
    label "j&#261;derko"
  ]
  node [
    id 275
    label "kom&#243;rka_Leydiga"
  ]
  node [
    id 276
    label "jajo"
  ]
  node [
    id 277
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 278
    label "chromosom"
  ]
  node [
    id 279
    label "organellum"
  ]
  node [
    id 280
    label "moszna"
  ]
  node [
    id 281
    label "przeciwobraz"
  ]
  node [
    id 282
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 283
    label "&#347;rodek"
  ]
  node [
    id 284
    label "protoplazma"
  ]
  node [
    id 285
    label "znaczenie"
  ]
  node [
    id 286
    label "gruczo&#322;_rozrodczy"
  ]
  node [
    id 287
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 288
    label "nukleosynteza"
  ]
  node [
    id 289
    label "subsystem"
  ]
  node [
    id 290
    label "ko&#322;o"
  ]
  node [
    id 291
    label "granica"
  ]
  node [
    id 292
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 293
    label "suport"
  ]
  node [
    id 294
    label "prosta"
  ]
  node [
    id 295
    label "o&#347;rodek"
  ]
  node [
    id 296
    label "eonotem"
  ]
  node [
    id 297
    label "constellation"
  ]
  node [
    id 298
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 299
    label "Ptak_Rajski"
  ]
  node [
    id 300
    label "W&#281;&#380;ownik"
  ]
  node [
    id 301
    label "Panna"
  ]
  node [
    id 302
    label "W&#261;&#380;"
  ]
  node [
    id 303
    label "blokada"
  ]
  node [
    id 304
    label "hurtownia"
  ]
  node [
    id 305
    label "pomieszczenie"
  ]
  node [
    id 306
    label "pole"
  ]
  node [
    id 307
    label "pas"
  ]
  node [
    id 308
    label "miejsce"
  ]
  node [
    id 309
    label "basic"
  ]
  node [
    id 310
    label "sk&#322;adnik"
  ]
  node [
    id 311
    label "sklep"
  ]
  node [
    id 312
    label "obr&#243;bka"
  ]
  node [
    id 313
    label "constitution"
  ]
  node [
    id 314
    label "fabryka"
  ]
  node [
    id 315
    label "&#347;wiat&#322;o"
  ]
  node [
    id 316
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 317
    label "syf"
  ]
  node [
    id 318
    label "rank_and_file"
  ]
  node [
    id 319
    label "set"
  ]
  node [
    id 320
    label "tabulacja"
  ]
  node [
    id 321
    label "tekst"
  ]
  node [
    id 322
    label "bawe&#322;na"
  ]
  node [
    id 323
    label "wiatr"
  ]
  node [
    id 324
    label "powianie"
  ]
  node [
    id 325
    label "powietrze"
  ]
  node [
    id 326
    label "wietrzno&#347;&#263;"
  ]
  node [
    id 327
    label "zjawisko"
  ]
  node [
    id 328
    label "porywisto&#347;&#263;"
  ]
  node [
    id 329
    label "powia&#263;"
  ]
  node [
    id 330
    label "skala_Beauforta"
  ]
  node [
    id 331
    label "tkanina"
  ]
  node [
    id 332
    label "w&#322;&#243;kno_naturalne"
  ]
  node [
    id 333
    label "targanie"
  ]
  node [
    id 334
    label "rafinoza"
  ]
  node [
    id 335
    label "ro&#347;lina"
  ]
  node [
    id 336
    label "targa&#263;"
  ]
  node [
    id 337
    label "ro&#347;lina_w&#322;&#243;knista"
  ]
  node [
    id 338
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 339
    label "wielostronnie"
  ]
  node [
    id 340
    label "comprehensively"
  ]
  node [
    id 341
    label "multilateralny"
  ]
  node [
    id 342
    label "wielostronny"
  ]
  node [
    id 343
    label "r&#243;&#380;norodnie"
  ]
  node [
    id 344
    label "variously"
  ]
  node [
    id 345
    label "ca&#322;o&#347;ciowo"
  ]
  node [
    id 346
    label "og&#243;lny"
  ]
  node [
    id 347
    label "treat"
  ]
  node [
    id 348
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 349
    label "zaspokaja&#263;"
  ]
  node [
    id 350
    label "suffice"
  ]
  node [
    id 351
    label "zaspakaja&#263;"
  ]
  node [
    id 352
    label "uprawia&#263;_seks"
  ]
  node [
    id 353
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 354
    label "serve"
  ]
  node [
    id 355
    label "supply"
  ]
  node [
    id 356
    label "testify"
  ]
  node [
    id 357
    label "op&#322;aca&#263;"
  ]
  node [
    id 358
    label "by&#263;"
  ]
  node [
    id 359
    label "wyraz"
  ]
  node [
    id 360
    label "sk&#322;ada&#263;"
  ]
  node [
    id 361
    label "pracowa&#263;"
  ]
  node [
    id 362
    label "us&#322;uga"
  ]
  node [
    id 363
    label "represent"
  ]
  node [
    id 364
    label "bespeak"
  ]
  node [
    id 365
    label "opowiada&#263;"
  ]
  node [
    id 366
    label "attest"
  ]
  node [
    id 367
    label "informowa&#263;"
  ]
  node [
    id 368
    label "czyni&#263;_dobro"
  ]
  node [
    id 369
    label "satisfy"
  ]
  node [
    id 370
    label "satysfakcjonowa&#263;"
  ]
  node [
    id 371
    label "poi&#263;_si&#281;"
  ]
  node [
    id 372
    label "cover"
  ]
  node [
    id 373
    label "kognicja"
  ]
  node [
    id 374
    label "object"
  ]
  node [
    id 375
    label "rozprawa"
  ]
  node [
    id 376
    label "temat"
  ]
  node [
    id 377
    label "szczeg&#243;&#322;"
  ]
  node [
    id 378
    label "proposition"
  ]
  node [
    id 379
    label "przes&#322;anka"
  ]
  node [
    id 380
    label "rzecz"
  ]
  node [
    id 381
    label "idea"
  ]
  node [
    id 382
    label "przebiec"
  ]
  node [
    id 383
    label "charakter"
  ]
  node [
    id 384
    label "czynno&#347;&#263;"
  ]
  node [
    id 385
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 386
    label "motyw"
  ]
  node [
    id 387
    label "przebiegni&#281;cie"
  ]
  node [
    id 388
    label "fabu&#322;a"
  ]
  node [
    id 389
    label "ideologia"
  ]
  node [
    id 390
    label "byt"
  ]
  node [
    id 391
    label "intelekt"
  ]
  node [
    id 392
    label "Kant"
  ]
  node [
    id 393
    label "cel"
  ]
  node [
    id 394
    label "istota"
  ]
  node [
    id 395
    label "ideacja"
  ]
  node [
    id 396
    label "wpadni&#281;cie"
  ]
  node [
    id 397
    label "mienie"
  ]
  node [
    id 398
    label "przyroda"
  ]
  node [
    id 399
    label "obiekt"
  ]
  node [
    id 400
    label "kultura"
  ]
  node [
    id 401
    label "wpa&#347;&#263;"
  ]
  node [
    id 402
    label "wpadanie"
  ]
  node [
    id 403
    label "wpada&#263;"
  ]
  node [
    id 404
    label "rozumowanie"
  ]
  node [
    id 405
    label "opracowanie"
  ]
  node [
    id 406
    label "proces"
  ]
  node [
    id 407
    label "obrady"
  ]
  node [
    id 408
    label "cytat"
  ]
  node [
    id 409
    label "obja&#347;nienie"
  ]
  node [
    id 410
    label "s&#261;dzenie"
  ]
  node [
    id 411
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 412
    label "niuansowa&#263;"
  ]
  node [
    id 413
    label "element"
  ]
  node [
    id 414
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 415
    label "zniuansowa&#263;"
  ]
  node [
    id 416
    label "fakt"
  ]
  node [
    id 417
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 418
    label "przyczyna"
  ]
  node [
    id 419
    label "wnioskowanie"
  ]
  node [
    id 420
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 421
    label "wyraz_pochodny"
  ]
  node [
    id 422
    label "zboczenie"
  ]
  node [
    id 423
    label "om&#243;wienie"
  ]
  node [
    id 424
    label "omawia&#263;"
  ]
  node [
    id 425
    label "fraza"
  ]
  node [
    id 426
    label "tre&#347;&#263;"
  ]
  node [
    id 427
    label "entity"
  ]
  node [
    id 428
    label "topik"
  ]
  node [
    id 429
    label "tematyka"
  ]
  node [
    id 430
    label "w&#261;tek"
  ]
  node [
    id 431
    label "zbaczanie"
  ]
  node [
    id 432
    label "om&#243;wi&#263;"
  ]
  node [
    id 433
    label "omawianie"
  ]
  node [
    id 434
    label "melodia"
  ]
  node [
    id 435
    label "otoczka"
  ]
  node [
    id 436
    label "zbacza&#263;"
  ]
  node [
    id 437
    label "zboczy&#263;"
  ]
  node [
    id 438
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 439
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 440
    label "w&#281;ze&#322;"
  ]
  node [
    id 441
    label "consort"
  ]
  node [
    id 442
    label "cement"
  ]
  node [
    id 443
    label "opakowa&#263;"
  ]
  node [
    id 444
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 445
    label "relate"
  ]
  node [
    id 446
    label "form"
  ]
  node [
    id 447
    label "tobo&#322;ek"
  ]
  node [
    id 448
    label "unify"
  ]
  node [
    id 449
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 450
    label "incorporate"
  ]
  node [
    id 451
    label "wi&#281;&#378;"
  ]
  node [
    id 452
    label "bind"
  ]
  node [
    id 453
    label "zawi&#261;za&#263;"
  ]
  node [
    id 454
    label "zaprawa"
  ]
  node [
    id 455
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 456
    label "powi&#261;za&#263;"
  ]
  node [
    id 457
    label "scali&#263;"
  ]
  node [
    id 458
    label "zatrzyma&#263;"
  ]
  node [
    id 459
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 460
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 461
    label "komornik"
  ]
  node [
    id 462
    label "suspend"
  ]
  node [
    id 463
    label "zaczepi&#263;"
  ]
  node [
    id 464
    label "bury"
  ]
  node [
    id 465
    label "bankrupt"
  ]
  node [
    id 466
    label "zabra&#263;"
  ]
  node [
    id 467
    label "continue"
  ]
  node [
    id 468
    label "give"
  ]
  node [
    id 469
    label "spowodowa&#263;"
  ]
  node [
    id 470
    label "zamkn&#261;&#263;"
  ]
  node [
    id 471
    label "przechowa&#263;"
  ]
  node [
    id 472
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 473
    label "zaaresztowa&#263;"
  ]
  node [
    id 474
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 475
    label "przerwa&#263;"
  ]
  node [
    id 476
    label "unieruchomi&#263;"
  ]
  node [
    id 477
    label "anticipate"
  ]
  node [
    id 478
    label "p&#281;tla"
  ]
  node [
    id 479
    label "zawi&#261;zek"
  ]
  node [
    id 480
    label "wytworzy&#263;"
  ]
  node [
    id 481
    label "zacz&#261;&#263;"
  ]
  node [
    id 482
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 483
    label "zjednoczy&#263;"
  ]
  node [
    id 484
    label "ally"
  ]
  node [
    id 485
    label "connect"
  ]
  node [
    id 486
    label "obowi&#261;za&#263;"
  ]
  node [
    id 487
    label "perpetrate"
  ]
  node [
    id 488
    label "articulation"
  ]
  node [
    id 489
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 490
    label "catch"
  ]
  node [
    id 491
    label "zrobi&#263;"
  ]
  node [
    id 492
    label "dokoptowa&#263;"
  ]
  node [
    id 493
    label "stworzy&#263;"
  ]
  node [
    id 494
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 495
    label "po&#322;&#261;czenie"
  ]
  node [
    id 496
    label "pack"
  ]
  node [
    id 497
    label "owin&#261;&#263;"
  ]
  node [
    id 498
    label "zwielokrotni&#263;_si&#281;"
  ]
  node [
    id 499
    label "sta&#263;_si&#281;"
  ]
  node [
    id 500
    label "clot"
  ]
  node [
    id 501
    label "przybra&#263;_na_sile"
  ]
  node [
    id 502
    label "narosn&#261;&#263;"
  ]
  node [
    id 503
    label "stwardnie&#263;"
  ]
  node [
    id 504
    label "solidify"
  ]
  node [
    id 505
    label "znieruchomie&#263;"
  ]
  node [
    id 506
    label "zg&#281;stnie&#263;"
  ]
  node [
    id 507
    label "porazi&#263;"
  ]
  node [
    id 508
    label "overwhelm"
  ]
  node [
    id 509
    label "zwi&#261;zanie"
  ]
  node [
    id 510
    label "zgrupowanie"
  ]
  node [
    id 511
    label "materia&#322;_budowlany"
  ]
  node [
    id 512
    label "mortar"
  ]
  node [
    id 513
    label "podk&#322;ad"
  ]
  node [
    id 514
    label "training"
  ]
  node [
    id 515
    label "mieszanina"
  ]
  node [
    id 516
    label "&#263;wiczenie"
  ]
  node [
    id 517
    label "s&#322;oik"
  ]
  node [
    id 518
    label "przyprawa"
  ]
  node [
    id 519
    label "kastra"
  ]
  node [
    id 520
    label "wi&#261;za&#263;"
  ]
  node [
    id 521
    label "przetw&#243;r"
  ]
  node [
    id 522
    label "obw&#243;d"
  ]
  node [
    id 523
    label "praktyka"
  ]
  node [
    id 524
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 525
    label "wi&#261;zanie"
  ]
  node [
    id 526
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 527
    label "bratnia_dusza"
  ]
  node [
    id 528
    label "trasa"
  ]
  node [
    id 529
    label "uczesanie"
  ]
  node [
    id 530
    label "orbita"
  ]
  node [
    id 531
    label "kryszta&#322;"
  ]
  node [
    id 532
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 533
    label "graf"
  ]
  node [
    id 534
    label "hitch"
  ]
  node [
    id 535
    label "akcja"
  ]
  node [
    id 536
    label "struktura_anatomiczna"
  ]
  node [
    id 537
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 538
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 539
    label "marriage"
  ]
  node [
    id 540
    label "punkt"
  ]
  node [
    id 541
    label "ekliptyka"
  ]
  node [
    id 542
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 543
    label "problem"
  ]
  node [
    id 544
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 545
    label "fala_stoj&#261;ca"
  ]
  node [
    id 546
    label "tying"
  ]
  node [
    id 547
    label "argument"
  ]
  node [
    id 548
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 549
    label "mila_morska"
  ]
  node [
    id 550
    label "skupienie"
  ]
  node [
    id 551
    label "zgrubienie"
  ]
  node [
    id 552
    label "pismo_klinowe"
  ]
  node [
    id 553
    label "przeci&#281;cie"
  ]
  node [
    id 554
    label "band"
  ]
  node [
    id 555
    label "zwi&#261;zek"
  ]
  node [
    id 556
    label "marketing_afiliacyjny"
  ]
  node [
    id 557
    label "tob&#243;&#322;"
  ]
  node [
    id 558
    label "alga"
  ]
  node [
    id 559
    label "tobo&#322;ki"
  ]
  node [
    id 560
    label "wiciowiec"
  ]
  node [
    id 561
    label "spoiwo"
  ]
  node [
    id 562
    label "wertebroplastyka"
  ]
  node [
    id 563
    label "wype&#322;nienie"
  ]
  node [
    id 564
    label "tworzywo"
  ]
  node [
    id 565
    label "z&#261;b"
  ]
  node [
    id 566
    label "tkanka_kostna"
  ]
  node [
    id 567
    label "absolutorium"
  ]
  node [
    id 568
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 569
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 570
    label "nap&#322;ywanie"
  ]
  node [
    id 571
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 572
    label "nauka_ekonomiczna"
  ]
  node [
    id 573
    label "podupada&#263;"
  ]
  node [
    id 574
    label "podupadanie"
  ]
  node [
    id 575
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 576
    label "kwestor"
  ]
  node [
    id 577
    label "uruchomienie"
  ]
  node [
    id 578
    label "supernadz&#243;r"
  ]
  node [
    id 579
    label "uruchamia&#263;"
  ]
  node [
    id 580
    label "uruchamianie"
  ]
  node [
    id 581
    label "czynnik_produkcji"
  ]
  node [
    id 582
    label "przej&#347;cie"
  ]
  node [
    id 583
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 584
    label "rodowo&#347;&#263;"
  ]
  node [
    id 585
    label "patent"
  ]
  node [
    id 586
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 587
    label "dobra"
  ]
  node [
    id 588
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 589
    label "przej&#347;&#263;"
  ]
  node [
    id 590
    label "possession"
  ]
  node [
    id 591
    label "kapita&#322;"
  ]
  node [
    id 592
    label "begin"
  ]
  node [
    id 593
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 594
    label "zaczyna&#263;"
  ]
  node [
    id 595
    label "urz&#281;dnik"
  ]
  node [
    id 596
    label "ksi&#281;gowy"
  ]
  node [
    id 597
    label "kwestura"
  ]
  node [
    id 598
    label "Katon"
  ]
  node [
    id 599
    label "polityk"
  ]
  node [
    id 600
    label "decline"
  ]
  node [
    id 601
    label "traci&#263;"
  ]
  node [
    id 602
    label "fall"
  ]
  node [
    id 603
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 604
    label "graduation"
  ]
  node [
    id 605
    label "uko&#324;czenie"
  ]
  node [
    id 606
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 607
    label "ocena"
  ]
  node [
    id 608
    label "powodowanie"
  ]
  node [
    id 609
    label "w&#322;&#261;czanie"
  ]
  node [
    id 610
    label "robienie"
  ]
  node [
    id 611
    label "zaczynanie"
  ]
  node [
    id 612
    label "funkcjonowanie"
  ]
  node [
    id 613
    label "upadanie"
  ]
  node [
    id 614
    label "s&#322;abni&#281;cie"
  ]
  node [
    id 615
    label "shoot"
  ]
  node [
    id 616
    label "pour"
  ]
  node [
    id 617
    label "zasila&#263;"
  ]
  node [
    id 618
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 619
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 620
    label "meet"
  ]
  node [
    id 621
    label "dociera&#263;"
  ]
  node [
    id 622
    label "zbiera&#263;_si&#281;"
  ]
  node [
    id 623
    label "wzbiera&#263;"
  ]
  node [
    id 624
    label "ogarnia&#263;"
  ]
  node [
    id 625
    label "wype&#322;nia&#263;"
  ]
  node [
    id 626
    label "gromadzenie_si&#281;"
  ]
  node [
    id 627
    label "zbieranie_si&#281;"
  ]
  node [
    id 628
    label "zasilanie"
  ]
  node [
    id 629
    label "docieranie"
  ]
  node [
    id 630
    label "t&#281;&#380;enie"
  ]
  node [
    id 631
    label "nawiewanie"
  ]
  node [
    id 632
    label "nadmuchanie"
  ]
  node [
    id 633
    label "ogarnianie"
  ]
  node [
    id 634
    label "zasilenie"
  ]
  node [
    id 635
    label "zgromadzenie_si&#281;"
  ]
  node [
    id 636
    label "opanowanie"
  ]
  node [
    id 637
    label "zebranie_si&#281;"
  ]
  node [
    id 638
    label "dotarcie"
  ]
  node [
    id 639
    label "nasilenie_si&#281;"
  ]
  node [
    id 640
    label "bulge"
  ]
  node [
    id 641
    label "bankowo&#347;&#263;"
  ]
  node [
    id 642
    label "nadz&#243;r"
  ]
  node [
    id 643
    label "spowodowanie"
  ]
  node [
    id 644
    label "zacz&#281;cie"
  ]
  node [
    id 645
    label "w&#322;&#261;czenie"
  ]
  node [
    id 646
    label "propulsion"
  ]
  node [
    id 647
    label "wype&#322;ni&#263;"
  ]
  node [
    id 648
    label "mount"
  ]
  node [
    id 649
    label "zasili&#263;"
  ]
  node [
    id 650
    label "wax"
  ]
  node [
    id 651
    label "dotrze&#263;"
  ]
  node [
    id 652
    label "zebra&#263;_si&#281;"
  ]
  node [
    id 653
    label "zgromadzi&#263;_si&#281;"
  ]
  node [
    id 654
    label "rise"
  ]
  node [
    id 655
    label "ogarn&#261;&#263;"
  ]
  node [
    id 656
    label "saddle_horse"
  ]
  node [
    id 657
    label "wezbra&#263;"
  ]
  node [
    id 658
    label "Polish"
  ]
  node [
    id 659
    label "goniony"
  ]
  node [
    id 660
    label "oberek"
  ]
  node [
    id 661
    label "ryba_po_grecku"
  ]
  node [
    id 662
    label "sztajer"
  ]
  node [
    id 663
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 664
    label "krakowiak"
  ]
  node [
    id 665
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 666
    label "pierogi_ruskie"
  ]
  node [
    id 667
    label "lacki"
  ]
  node [
    id 668
    label "polak"
  ]
  node [
    id 669
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 670
    label "chodzony"
  ]
  node [
    id 671
    label "po_polsku"
  ]
  node [
    id 672
    label "mazur"
  ]
  node [
    id 673
    label "polsko"
  ]
  node [
    id 674
    label "skoczny"
  ]
  node [
    id 675
    label "drabant"
  ]
  node [
    id 676
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 677
    label "j&#281;zyk"
  ]
  node [
    id 678
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 679
    label "artykulator"
  ]
  node [
    id 680
    label "kod"
  ]
  node [
    id 681
    label "kawa&#322;ek"
  ]
  node [
    id 682
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 683
    label "gramatyka"
  ]
  node [
    id 684
    label "stylik"
  ]
  node [
    id 685
    label "przet&#322;umaczenie"
  ]
  node [
    id 686
    label "formalizowanie"
  ]
  node [
    id 687
    label "ssanie"
  ]
  node [
    id 688
    label "ssa&#263;"
  ]
  node [
    id 689
    label "language"
  ]
  node [
    id 690
    label "liza&#263;"
  ]
  node [
    id 691
    label "napisa&#263;"
  ]
  node [
    id 692
    label "konsonantyzm"
  ]
  node [
    id 693
    label "wokalizm"
  ]
  node [
    id 694
    label "pisa&#263;"
  ]
  node [
    id 695
    label "fonetyka"
  ]
  node [
    id 696
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 697
    label "jeniec"
  ]
  node [
    id 698
    label "but"
  ]
  node [
    id 699
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 700
    label "po_koroniarsku"
  ]
  node [
    id 701
    label "kultura_duchowa"
  ]
  node [
    id 702
    label "t&#322;umaczenie"
  ]
  node [
    id 703
    label "m&#243;wienie"
  ]
  node [
    id 704
    label "pype&#263;"
  ]
  node [
    id 705
    label "lizanie"
  ]
  node [
    id 706
    label "pismo"
  ]
  node [
    id 707
    label "formalizowa&#263;"
  ]
  node [
    id 708
    label "rozumie&#263;"
  ]
  node [
    id 709
    label "organ"
  ]
  node [
    id 710
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 711
    label "rozumienie"
  ]
  node [
    id 712
    label "makroglosja"
  ]
  node [
    id 713
    label "m&#243;wi&#263;"
  ]
  node [
    id 714
    label "jama_ustna"
  ]
  node [
    id 715
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 716
    label "formacja_geologiczna"
  ]
  node [
    id 717
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 718
    label "natural_language"
  ]
  node [
    id 719
    label "s&#322;ownictwo"
  ]
  node [
    id 720
    label "urz&#261;dzenie"
  ]
  node [
    id 721
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 722
    label "wschodnioeuropejski"
  ]
  node [
    id 723
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 724
    label "poga&#324;ski"
  ]
  node [
    id 725
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 726
    label "topielec"
  ]
  node [
    id 727
    label "europejski"
  ]
  node [
    id 728
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 729
    label "langosz"
  ]
  node [
    id 730
    label "sponiewieranie"
  ]
  node [
    id 731
    label "discipline"
  ]
  node [
    id 732
    label "kr&#261;&#380;enie"
  ]
  node [
    id 733
    label "sponiewiera&#263;"
  ]
  node [
    id 734
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 735
    label "program_nauczania"
  ]
  node [
    id 736
    label "thing"
  ]
  node [
    id 737
    label "gwardzista"
  ]
  node [
    id 738
    label "taniec"
  ]
  node [
    id 739
    label "taniec_ludowy"
  ]
  node [
    id 740
    label "&#347;redniowieczny"
  ]
  node [
    id 741
    label "specjalny"
  ]
  node [
    id 742
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 743
    label "weso&#322;y"
  ]
  node [
    id 744
    label "sprawny"
  ]
  node [
    id 745
    label "rytmiczny"
  ]
  node [
    id 746
    label "skocznie"
  ]
  node [
    id 747
    label "energiczny"
  ]
  node [
    id 748
    label "lendler"
  ]
  node [
    id 749
    label "austriacki"
  ]
  node [
    id 750
    label "polka"
  ]
  node [
    id 751
    label "europejsko"
  ]
  node [
    id 752
    label "przytup"
  ]
  node [
    id 753
    label "ho&#322;ubiec"
  ]
  node [
    id 754
    label "wodzi&#263;"
  ]
  node [
    id 755
    label "ludowy"
  ]
  node [
    id 756
    label "pie&#347;&#324;"
  ]
  node [
    id 757
    label "mieszkaniec"
  ]
  node [
    id 758
    label "centu&#347;"
  ]
  node [
    id 759
    label "lalka"
  ]
  node [
    id 760
    label "Ma&#322;opolanin"
  ]
  node [
    id 761
    label "krakauer"
  ]
  node [
    id 762
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 763
    label "wys&#322;uga"
  ]
  node [
    id 764
    label "service"
  ]
  node [
    id 765
    label "czworak"
  ]
  node [
    id 766
    label "ZOMO"
  ]
  node [
    id 767
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 768
    label "praca"
  ]
  node [
    id 769
    label "moneta"
  ]
  node [
    id 770
    label "szesnastowieczny"
  ]
  node [
    id 771
    label "dom_wielorodzinny"
  ]
  node [
    id 772
    label "maj&#261;tek_ziemski"
  ]
  node [
    id 773
    label "Mazowsze"
  ]
  node [
    id 774
    label "odm&#322;adzanie"
  ]
  node [
    id 775
    label "&#346;wietliki"
  ]
  node [
    id 776
    label "The_Beatles"
  ]
  node [
    id 777
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 778
    label "odm&#322;adza&#263;"
  ]
  node [
    id 779
    label "zabudowania"
  ]
  node [
    id 780
    label "group"
  ]
  node [
    id 781
    label "zespolik"
  ]
  node [
    id 782
    label "schorzenie"
  ]
  node [
    id 783
    label "grupa"
  ]
  node [
    id 784
    label "Depeche_Mode"
  ]
  node [
    id 785
    label "batch"
  ]
  node [
    id 786
    label "odm&#322;odzenie"
  ]
  node [
    id 787
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 788
    label "najem"
  ]
  node [
    id 789
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 790
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 791
    label "zak&#322;ad"
  ]
  node [
    id 792
    label "stosunek_pracy"
  ]
  node [
    id 793
    label "benedykty&#324;ski"
  ]
  node [
    id 794
    label "poda&#380;_pracy"
  ]
  node [
    id 795
    label "pracowanie"
  ]
  node [
    id 796
    label "tyrka"
  ]
  node [
    id 797
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 798
    label "zaw&#243;d"
  ]
  node [
    id 799
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 800
    label "tynkarski"
  ]
  node [
    id 801
    label "zmiana"
  ]
  node [
    id 802
    label "zobowi&#261;zanie"
  ]
  node [
    id 803
    label "kierownictwo"
  ]
  node [
    id 804
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 805
    label "osoba_prawna"
  ]
  node [
    id 806
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 807
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 808
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 809
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 810
    label "organizacja"
  ]
  node [
    id 811
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 812
    label "Fundusze_Unijne"
  ]
  node [
    id 813
    label "zamyka&#263;"
  ]
  node [
    id 814
    label "establishment"
  ]
  node [
    id 815
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 816
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 817
    label "afiliowa&#263;"
  ]
  node [
    id 818
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 819
    label "standard"
  ]
  node [
    id 820
    label "zamykanie"
  ]
  node [
    id 821
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 822
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 823
    label "s&#322;uga"
  ]
  node [
    id 824
    label "podw&#322;adny"
  ]
  node [
    id 825
    label "s&#322;u&#380;ebnik"
  ]
  node [
    id 826
    label "ochmistrzyni"
  ]
  node [
    id 827
    label "s&#322;u&#380;ebnica"
  ]
  node [
    id 828
    label "milicja_obywatelska"
  ]
  node [
    id 829
    label "s&#322;uszny"
  ]
  node [
    id 830
    label "c&#322;owy"
  ]
  node [
    id 831
    label "trafnie"
  ]
  node [
    id 832
    label "udany"
  ]
  node [
    id 833
    label "celnie"
  ]
  node [
    id 834
    label "wybitny"
  ]
  node [
    id 835
    label "dobry"
  ]
  node [
    id 836
    label "dobroczynny"
  ]
  node [
    id 837
    label "czw&#243;rka"
  ]
  node [
    id 838
    label "spokojny"
  ]
  node [
    id 839
    label "skuteczny"
  ]
  node [
    id 840
    label "&#347;mieszny"
  ]
  node [
    id 841
    label "mi&#322;y"
  ]
  node [
    id 842
    label "grzeczny"
  ]
  node [
    id 843
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 844
    label "powitanie"
  ]
  node [
    id 845
    label "dobrze"
  ]
  node [
    id 846
    label "ca&#322;y"
  ]
  node [
    id 847
    label "zwrot"
  ]
  node [
    id 848
    label "pomy&#347;lny"
  ]
  node [
    id 849
    label "moralny"
  ]
  node [
    id 850
    label "drogi"
  ]
  node [
    id 851
    label "pozytywny"
  ]
  node [
    id 852
    label "odpowiedni"
  ]
  node [
    id 853
    label "korzystny"
  ]
  node [
    id 854
    label "pos&#322;uszny"
  ]
  node [
    id 855
    label "udanie"
  ]
  node [
    id 856
    label "przyjemny"
  ]
  node [
    id 857
    label "fajny"
  ]
  node [
    id 858
    label "s&#322;usznie"
  ]
  node [
    id 859
    label "zasadny"
  ]
  node [
    id 860
    label "nale&#380;yty"
  ]
  node [
    id 861
    label "prawdziwy"
  ]
  node [
    id 862
    label "solidny"
  ]
  node [
    id 863
    label "niespotykany"
  ]
  node [
    id 864
    label "wydatny"
  ]
  node [
    id 865
    label "wspania&#322;y"
  ]
  node [
    id 866
    label "ponadprzeci&#281;tny"
  ]
  node [
    id 867
    label "&#347;wietny"
  ]
  node [
    id 868
    label "imponuj&#261;cy"
  ]
  node [
    id 869
    label "wybitnie"
  ]
  node [
    id 870
    label "trafny"
  ]
  node [
    id 871
    label "zgrabny"
  ]
  node [
    id 872
    label "adekwatnie"
  ]
  node [
    id 873
    label "sprawnie"
  ]
  node [
    id 874
    label "skarb"
  ]
  node [
    id 875
    label "pa&#324;stwo"
  ]
  node [
    id 876
    label "narodowy"
  ]
  node [
    id 877
    label "polskie"
  ]
  node [
    id 878
    label "og&#243;lnopolski"
  ]
  node [
    id 879
    label "systema"
  ]
  node [
    id 880
    label "obs&#322;uga"
  ]
  node [
    id 881
    label "zabezpieczenie"
  ]
  node [
    id 882
    label "i"
  ]
  node [
    id 883
    label "pozwolenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 748
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 760
  ]
  edge [
    source 8
    target 761
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 9
    target 800
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 801
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 802
  ]
  edge [
    source 9
    target 803
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 805
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 9
    target 807
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 808
  ]
  edge [
    source 9
    target 809
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 810
  ]
  edge [
    source 9
    target 811
  ]
  edge [
    source 9
    target 812
  ]
  edge [
    source 9
    target 813
  ]
  edge [
    source 9
    target 814
  ]
  edge [
    source 9
    target 815
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 816
  ]
  edge [
    source 9
    target 817
  ]
  edge [
    source 9
    target 818
  ]
  edge [
    source 9
    target 819
  ]
  edge [
    source 9
    target 820
  ]
  edge [
    source 9
    target 821
  ]
  edge [
    source 9
    target 822
  ]
  edge [
    source 9
    target 823
  ]
  edge [
    source 9
    target 824
  ]
  edge [
    source 9
    target 825
  ]
  edge [
    source 9
    target 826
  ]
  edge [
    source 9
    target 827
  ]
  edge [
    source 9
    target 828
  ]
  edge [
    source 10
    target 829
  ]
  edge [
    source 10
    target 830
  ]
  edge [
    source 10
    target 831
  ]
  edge [
    source 10
    target 832
  ]
  edge [
    source 10
    target 833
  ]
  edge [
    source 10
    target 834
  ]
  edge [
    source 10
    target 835
  ]
  edge [
    source 10
    target 836
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 10
    target 839
  ]
  edge [
    source 10
    target 840
  ]
  edge [
    source 10
    target 841
  ]
  edge [
    source 10
    target 842
  ]
  edge [
    source 10
    target 843
  ]
  edge [
    source 10
    target 844
  ]
  edge [
    source 10
    target 845
  ]
  edge [
    source 10
    target 846
  ]
  edge [
    source 10
    target 847
  ]
  edge [
    source 10
    target 848
  ]
  edge [
    source 10
    target 849
  ]
  edge [
    source 10
    target 850
  ]
  edge [
    source 10
    target 851
  ]
  edge [
    source 10
    target 852
  ]
  edge [
    source 10
    target 853
  ]
  edge [
    source 10
    target 854
  ]
  edge [
    source 10
    target 855
  ]
  edge [
    source 10
    target 856
  ]
  edge [
    source 10
    target 857
  ]
  edge [
    source 10
    target 858
  ]
  edge [
    source 10
    target 859
  ]
  edge [
    source 10
    target 860
  ]
  edge [
    source 10
    target 861
  ]
  edge [
    source 10
    target 862
  ]
  edge [
    source 10
    target 863
  ]
  edge [
    source 10
    target 864
  ]
  edge [
    source 10
    target 865
  ]
  edge [
    source 10
    target 866
  ]
  edge [
    source 10
    target 867
  ]
  edge [
    source 10
    target 868
  ]
  edge [
    source 10
    target 869
  ]
  edge [
    source 10
    target 870
  ]
  edge [
    source 10
    target 871
  ]
  edge [
    source 10
    target 872
  ]
  edge [
    source 10
    target 873
  ]
  edge [
    source 252
    target 876
  ]
  edge [
    source 252
    target 877
  ]
  edge [
    source 874
    target 875
  ]
  edge [
    source 876
    target 877
  ]
  edge [
    source 878
    target 879
  ]
  edge [
    source 878
    target 880
  ]
  edge [
    source 878
    target 881
  ]
  edge [
    source 878
    target 882
  ]
  edge [
    source 878
    target 883
  ]
  edge [
    source 879
    target 880
  ]
  edge [
    source 879
    target 881
  ]
  edge [
    source 879
    target 882
  ]
  edge [
    source 879
    target 883
  ]
  edge [
    source 880
    target 881
  ]
  edge [
    source 880
    target 882
  ]
  edge [
    source 880
    target 883
  ]
  edge [
    source 881
    target 882
  ]
  edge [
    source 881
    target 883
  ]
  edge [
    source 882
    target 883
  ]
]
