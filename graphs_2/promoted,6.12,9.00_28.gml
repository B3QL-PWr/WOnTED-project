graph [
  node [
    id 0
    label "brawo"
    origin "text"
  ]
  node [
    id 1
    label "doktor"
    origin "text"
  ]
  node [
    id 2
    label "religa"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 4
    label "dumny"
    origin "text"
  ]
  node [
    id 5
    label "kolega"
    origin "text"
  ]
  node [
    id 6
    label "aprobata"
  ]
  node [
    id 7
    label "acclaim"
  ]
  node [
    id 8
    label "pochwa&#322;a"
  ]
  node [
    id 9
    label "akcept"
  ]
  node [
    id 10
    label "aprobacja"
  ]
  node [
    id 11
    label "pozwolenie"
  ]
  node [
    id 12
    label "accept"
  ]
  node [
    id 13
    label "ocena"
  ]
  node [
    id 14
    label "doktoryzowanie_si&#281;"
  ]
  node [
    id 15
    label "pracownik"
  ]
  node [
    id 16
    label "stopie&#324;_naukowy"
  ]
  node [
    id 17
    label "pracownik_naukowy"
  ]
  node [
    id 18
    label "doktorant"
  ]
  node [
    id 19
    label "salariat"
  ]
  node [
    id 20
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 21
    label "cz&#322;owiek"
  ]
  node [
    id 22
    label "delegowanie"
  ]
  node [
    id 23
    label "pracu&#347;"
  ]
  node [
    id 24
    label "r&#281;ka"
  ]
  node [
    id 25
    label "delegowa&#263;"
  ]
  node [
    id 26
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 27
    label "s&#322;uchacz"
  ]
  node [
    id 28
    label "kandydat"
  ]
  node [
    id 29
    label "dumnie"
  ]
  node [
    id 30
    label "zadowolony"
  ]
  node [
    id 31
    label "pewny"
  ]
  node [
    id 32
    label "dostojny"
  ]
  node [
    id 33
    label "godny"
  ]
  node [
    id 34
    label "zadowolenie_si&#281;"
  ]
  node [
    id 35
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 36
    label "pogodny"
  ]
  node [
    id 37
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 38
    label "godnie"
  ]
  node [
    id 39
    label "godziwy"
  ]
  node [
    id 40
    label "powa&#380;ny"
  ]
  node [
    id 41
    label "odpowiedni"
  ]
  node [
    id 42
    label "zdystansowany"
  ]
  node [
    id 43
    label "mo&#380;liwy"
  ]
  node [
    id 44
    label "bezpieczny"
  ]
  node [
    id 45
    label "spokojny"
  ]
  node [
    id 46
    label "pewnie"
  ]
  node [
    id 47
    label "upewnianie_si&#281;"
  ]
  node [
    id 48
    label "ufanie"
  ]
  node [
    id 49
    label "wierzenie"
  ]
  node [
    id 50
    label "upewnienie_si&#281;"
  ]
  node [
    id 51
    label "wiarygodny"
  ]
  node [
    id 52
    label "dostojnie"
  ]
  node [
    id 53
    label "elegancki"
  ]
  node [
    id 54
    label "majestatycznie"
  ]
  node [
    id 55
    label "baronial"
  ]
  node [
    id 56
    label "dumno"
  ]
  node [
    id 57
    label "znajomy"
  ]
  node [
    id 58
    label "towarzysz"
  ]
  node [
    id 59
    label "kumplowanie_si&#281;"
  ]
  node [
    id 60
    label "ziom"
  ]
  node [
    id 61
    label "partner"
  ]
  node [
    id 62
    label "zakolegowanie_si&#281;"
  ]
  node [
    id 63
    label "konfrater"
  ]
  node [
    id 64
    label "partyjny"
  ]
  node [
    id 65
    label "komunista"
  ]
  node [
    id 66
    label "przedsi&#281;biorca"
  ]
  node [
    id 67
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 68
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 69
    label "kolaborator"
  ]
  node [
    id 70
    label "prowadzi&#263;"
  ]
  node [
    id 71
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 72
    label "sp&#243;lnik"
  ]
  node [
    id 73
    label "aktor"
  ]
  node [
    id 74
    label "uczestniczenie"
  ]
  node [
    id 75
    label "znany"
  ]
  node [
    id 76
    label "zapoznanie"
  ]
  node [
    id 77
    label "sw&#243;j"
  ]
  node [
    id 78
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 79
    label "zapoznanie_si&#281;"
  ]
  node [
    id 80
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 81
    label "znajomek"
  ]
  node [
    id 82
    label "zapoznawanie"
  ]
  node [
    id 83
    label "znajomo"
  ]
  node [
    id 84
    label "pewien"
  ]
  node [
    id 85
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 86
    label "przyj&#281;ty"
  ]
  node [
    id 87
    label "za_pan_brat"
  ]
  node [
    id 88
    label "swojak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
]
