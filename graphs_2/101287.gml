graph [
  node [
    id 0
    label "bruno"
    origin "text"
  ]
  node [
    id 1
    label "mossa"
    origin "text"
  ]
  node [
    id 2
    label "rezende"
    origin "text"
  ]
  node [
    id 3
    label "Bruno"
  ]
  node [
    id 4
    label "Mossa"
  ]
  node [
    id 5
    label "Rezende"
  ]
  node [
    id 6
    label "Rio"
  ]
  node [
    id 7
    label "de"
  ]
  node [
    id 8
    label "Janeiro"
  ]
  node [
    id 9
    label "brazylijski"
  ]
  node [
    id 10
    label "superliga"
  ]
  node [
    id 11
    label "Cimed"
  ]
  node [
    id 12
    label "Florian&#243;polis"
  ]
  node [
    id 13
    label "Bernardo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
]
