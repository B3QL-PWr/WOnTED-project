graph [
  node [
    id 0
    label "bethesda"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "problem"
    origin "text"
  ]
  node [
    id 3
    label "fallout"
    origin "text"
  ]
  node [
    id 4
    label "czyj&#347;"
  ]
  node [
    id 5
    label "m&#261;&#380;"
  ]
  node [
    id 6
    label "prywatny"
  ]
  node [
    id 7
    label "ma&#322;&#380;onek"
  ]
  node [
    id 8
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 9
    label "ch&#322;op"
  ]
  node [
    id 10
    label "cz&#322;owiek"
  ]
  node [
    id 11
    label "pan_m&#322;ody"
  ]
  node [
    id 12
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 13
    label "&#347;lubny"
  ]
  node [
    id 14
    label "pan_domu"
  ]
  node [
    id 15
    label "pan_i_w&#322;adca"
  ]
  node [
    id 16
    label "stary"
  ]
  node [
    id 17
    label "sprawa"
  ]
  node [
    id 18
    label "subiekcja"
  ]
  node [
    id 19
    label "problemat"
  ]
  node [
    id 20
    label "jajko_Kolumba"
  ]
  node [
    id 21
    label "obstruction"
  ]
  node [
    id 22
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 23
    label "problematyka"
  ]
  node [
    id 24
    label "trudno&#347;&#263;"
  ]
  node [
    id 25
    label "pierepa&#322;ka"
  ]
  node [
    id 26
    label "ambaras"
  ]
  node [
    id 27
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 28
    label "napotka&#263;"
  ]
  node [
    id 29
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 30
    label "k&#322;opotliwy"
  ]
  node [
    id 31
    label "napotkanie"
  ]
  node [
    id 32
    label "poziom"
  ]
  node [
    id 33
    label "difficulty"
  ]
  node [
    id 34
    label "obstacle"
  ]
  node [
    id 35
    label "cecha"
  ]
  node [
    id 36
    label "sytuacja"
  ]
  node [
    id 37
    label "kognicja"
  ]
  node [
    id 38
    label "object"
  ]
  node [
    id 39
    label "rozprawa"
  ]
  node [
    id 40
    label "temat"
  ]
  node [
    id 41
    label "wydarzenie"
  ]
  node [
    id 42
    label "szczeg&#243;&#322;"
  ]
  node [
    id 43
    label "proposition"
  ]
  node [
    id 44
    label "przes&#322;anka"
  ]
  node [
    id 45
    label "rzecz"
  ]
  node [
    id 46
    label "idea"
  ]
  node [
    id 47
    label "k&#322;opot"
  ]
  node [
    id 48
    label "zbi&#243;r"
  ]
  node [
    id 49
    label "Fallout"
  ]
  node [
    id 50
    label "76"
  ]
  node [
    id 51
    label "Power"
  ]
  node [
    id 52
    label "Armor"
  ]
  node [
    id 53
    label "Edition"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 52
    target 53
  ]
]
