graph [
  node [
    id 0
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiadek"
    origin "text"
  ]
  node [
    id 2
    label "miko&#322;aj"
    origin "text"
  ]
  node [
    id 3
    label "biskup"
    origin "text"
  ]
  node [
    id 4
    label "grabowcu"
    origin "text"
  ]
  node [
    id 5
    label "nawa"
  ]
  node [
    id 6
    label "prezbiterium"
  ]
  node [
    id 7
    label "nerwica_eklezjogenna"
  ]
  node [
    id 8
    label "kropielnica"
  ]
  node [
    id 9
    label "ub&#322;agalnia"
  ]
  node [
    id 10
    label "zakrystia"
  ]
  node [
    id 11
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 12
    label "kult"
  ]
  node [
    id 13
    label "church"
  ]
  node [
    id 14
    label "organizacja_religijna"
  ]
  node [
    id 15
    label "wsp&#243;lnota"
  ]
  node [
    id 16
    label "kruchta"
  ]
  node [
    id 17
    label "dom"
  ]
  node [
    id 18
    label "Ska&#322;ka"
  ]
  node [
    id 19
    label "zwi&#261;zanie"
  ]
  node [
    id 20
    label "Walencja"
  ]
  node [
    id 21
    label "society"
  ]
  node [
    id 22
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 23
    label "partnership"
  ]
  node [
    id 24
    label "Ba&#322;kany"
  ]
  node [
    id 25
    label "zwi&#261;zek"
  ]
  node [
    id 26
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 27
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 28
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 29
    label "zwi&#261;za&#263;"
  ]
  node [
    id 30
    label "wi&#261;zanie"
  ]
  node [
    id 31
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 32
    label "marriage"
  ]
  node [
    id 33
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 34
    label "bratnia_dusza"
  ]
  node [
    id 35
    label "Skandynawia"
  ]
  node [
    id 36
    label "podobie&#324;stwo"
  ]
  node [
    id 37
    label "przybytek"
  ]
  node [
    id 38
    label "budynek"
  ]
  node [
    id 39
    label "siedlisko"
  ]
  node [
    id 40
    label "poj&#281;cie"
  ]
  node [
    id 41
    label "substancja_mieszkaniowa"
  ]
  node [
    id 42
    label "rodzina"
  ]
  node [
    id 43
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 44
    label "siedziba"
  ]
  node [
    id 45
    label "dom_rodzinny"
  ]
  node [
    id 46
    label "garderoba"
  ]
  node [
    id 47
    label "fratria"
  ]
  node [
    id 48
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 49
    label "stead"
  ]
  node [
    id 50
    label "instytucja"
  ]
  node [
    id 51
    label "grupa"
  ]
  node [
    id 52
    label "wiecha"
  ]
  node [
    id 53
    label "postawa"
  ]
  node [
    id 54
    label "translacja"
  ]
  node [
    id 55
    label "obrz&#281;d"
  ]
  node [
    id 56
    label "uwielbienie"
  ]
  node [
    id 57
    label "religia"
  ]
  node [
    id 58
    label "egzegeta"
  ]
  node [
    id 59
    label "worship"
  ]
  node [
    id 60
    label "przedsionek"
  ]
  node [
    id 61
    label "babiniec"
  ]
  node [
    id 62
    label "pomieszczenie"
  ]
  node [
    id 63
    label "zesp&#243;&#322;"
  ]
  node [
    id 64
    label "korpus"
  ]
  node [
    id 65
    label "tabernakulum"
  ]
  node [
    id 66
    label "o&#322;tarz"
  ]
  node [
    id 67
    label "&#347;rodowisko"
  ]
  node [
    id 68
    label "stalle"
  ]
  node [
    id 69
    label "duchowie&#324;stwo"
  ]
  node [
    id 70
    label "lampka_wieczysta"
  ]
  node [
    id 71
    label "naczynie_na_wod&#281;_&#347;wi&#281;con&#261;"
  ]
  node [
    id 72
    label "paramenty"
  ]
  node [
    id 73
    label "cz&#322;owiek"
  ]
  node [
    id 74
    label "uczestnik"
  ]
  node [
    id 75
    label "osoba_fizyczna"
  ]
  node [
    id 76
    label "dru&#380;ba"
  ]
  node [
    id 77
    label "s&#261;d"
  ]
  node [
    id 78
    label "obserwator"
  ]
  node [
    id 79
    label "asymilowa&#263;"
  ]
  node [
    id 80
    label "nasada"
  ]
  node [
    id 81
    label "profanum"
  ]
  node [
    id 82
    label "wz&#243;r"
  ]
  node [
    id 83
    label "senior"
  ]
  node [
    id 84
    label "asymilowanie"
  ]
  node [
    id 85
    label "os&#322;abia&#263;"
  ]
  node [
    id 86
    label "homo_sapiens"
  ]
  node [
    id 87
    label "osoba"
  ]
  node [
    id 88
    label "ludzko&#347;&#263;"
  ]
  node [
    id 89
    label "Adam"
  ]
  node [
    id 90
    label "hominid"
  ]
  node [
    id 91
    label "posta&#263;"
  ]
  node [
    id 92
    label "portrecista"
  ]
  node [
    id 93
    label "polifag"
  ]
  node [
    id 94
    label "podw&#322;adny"
  ]
  node [
    id 95
    label "dwun&#243;g"
  ]
  node [
    id 96
    label "wapniak"
  ]
  node [
    id 97
    label "duch"
  ]
  node [
    id 98
    label "os&#322;abianie"
  ]
  node [
    id 99
    label "antropochoria"
  ]
  node [
    id 100
    label "figura"
  ]
  node [
    id 101
    label "g&#322;owa"
  ]
  node [
    id 102
    label "mikrokosmos"
  ]
  node [
    id 103
    label "oddzia&#322;ywanie"
  ]
  node [
    id 104
    label "wys&#322;annik"
  ]
  node [
    id 105
    label "widownia"
  ]
  node [
    id 106
    label "ogl&#261;dacz"
  ]
  node [
    id 107
    label "za&#380;y&#322;o&#347;&#263;"
  ]
  node [
    id 108
    label "&#347;lub"
  ]
  node [
    id 109
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 110
    label "forum"
  ]
  node [
    id 111
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 112
    label "s&#261;downictwo"
  ]
  node [
    id 113
    label "wydarzenie"
  ]
  node [
    id 114
    label "podejrzany"
  ]
  node [
    id 115
    label "biuro"
  ]
  node [
    id 116
    label "post&#281;powanie"
  ]
  node [
    id 117
    label "court"
  ]
  node [
    id 118
    label "my&#347;l"
  ]
  node [
    id 119
    label "obrona"
  ]
  node [
    id 120
    label "system"
  ]
  node [
    id 121
    label "broni&#263;"
  ]
  node [
    id 122
    label "antylogizm"
  ]
  node [
    id 123
    label "strona"
  ]
  node [
    id 124
    label "oskar&#380;yciel"
  ]
  node [
    id 125
    label "urz&#261;d"
  ]
  node [
    id 126
    label "skazany"
  ]
  node [
    id 127
    label "konektyw"
  ]
  node [
    id 128
    label "wypowied&#378;"
  ]
  node [
    id 129
    label "bronienie"
  ]
  node [
    id 130
    label "wytw&#243;r"
  ]
  node [
    id 131
    label "pods&#261;dny"
  ]
  node [
    id 132
    label "procesowicz"
  ]
  node [
    id 133
    label "pontyfikat"
  ]
  node [
    id 134
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 135
    label "episkopat"
  ]
  node [
    id 136
    label "&#347;w"
  ]
  node [
    id 137
    label "prekonizacja"
  ]
  node [
    id 138
    label "pontyfikalia"
  ]
  node [
    id 139
    label "sakra"
  ]
  node [
    id 140
    label "Berkeley"
  ]
  node [
    id 141
    label "konsekrowanie"
  ]
  node [
    id 142
    label "ideologia"
  ]
  node [
    id 143
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 144
    label "rada"
  ]
  node [
    id 145
    label "nominacja"
  ]
  node [
    id 146
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 147
    label "monarcha"
  ]
  node [
    id 148
    label "konsekracja"
  ]
  node [
    id 149
    label "&#347;wi&#281;cenia"
  ]
  node [
    id 150
    label "wy&#347;wi&#281;canie"
  ]
  node [
    id 151
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 152
    label "po&#347;wi&#281;canie"
  ]
  node [
    id 153
    label "wy&#347;wi&#281;cenie"
  ]
  node [
    id 154
    label "czas"
  ]
  node [
    id 155
    label "papiestwo"
  ]
  node [
    id 156
    label "panowanie"
  ]
  node [
    id 157
    label "&#347;wi&#281;ty"
  ]
  node [
    id 158
    label "diecezja"
  ]
  node [
    id 159
    label "zamojsko"
  ]
  node [
    id 160
    label "lubaczowski"
  ]
  node [
    id 161
    label "dekanat"
  ]
  node [
    id 162
    label "grabowieckiego"
  ]
  node [
    id 163
    label "matka"
  ]
  node [
    id 164
    label "bo&#380;y"
  ]
  node [
    id 165
    label "Ziemowit"
  ]
  node [
    id 166
    label "IV"
  ]
  node [
    id 167
    label "Krzysztofa"
  ]
  node [
    id 168
    label "&#346;wi&#281;cicki"
  ]
  node [
    id 169
    label "Fryderyka"
  ]
  node [
    id 170
    label "Libenau"
  ]
  node [
    id 171
    label "Ignacy"
  ]
  node [
    id 172
    label "Kaliniak"
  ]
  node [
    id 173
    label "Adolfa"
  ]
  node [
    id 174
    label "Zdzierski"
  ]
  node [
    id 175
    label "Witolda"
  ]
  node [
    id 176
    label "Skulicz"
  ]
  node [
    id 177
    label "Jerzy"
  ]
  node [
    id 178
    label "leski"
  ]
  node [
    id 179
    label "MB"
  ]
  node [
    id 180
    label "szkaplerzny"
  ]
  node [
    id 181
    label "Jan"
  ]
  node [
    id 182
    label "pawe&#322;"
  ]
  node [
    id 183
    label "ii"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 158
    target 160
  ]
  edge [
    source 159
    target 160
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 169
    target 170
  ]
  edge [
    source 171
    target 172
  ]
  edge [
    source 173
    target 174
  ]
  edge [
    source 175
    target 176
  ]
  edge [
    source 177
    target 178
  ]
  edge [
    source 179
    target 180
  ]
  edge [
    source 181
    target 182
  ]
  edge [
    source 181
    target 183
  ]
  edge [
    source 182
    target 183
  ]
]
