graph [
  node [
    id 0
    label "dlatego"
    origin "text"
  ]
  node [
    id 1
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "polak"
    origin "text"
  ]
  node [
    id 3
    label "biedaka"
    origin "text"
  ]
  node [
    id 4
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 5
    label "mentalno&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "s&#322;aby"
    origin "text"
  ]
  node [
    id 7
    label "wykszta&#322;cenie"
    origin "text"
  ]
  node [
    id 8
    label "brak"
    origin "text"
  ]
  node [
    id 9
    label "podstawa"
    origin "text"
  ]
  node [
    id 10
    label "ekonomia"
    origin "text"
  ]
  node [
    id 11
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 12
    label "pilnowa&#263;"
  ]
  node [
    id 13
    label "robi&#263;"
  ]
  node [
    id 14
    label "my&#347;le&#263;"
  ]
  node [
    id 15
    label "continue"
  ]
  node [
    id 16
    label "consider"
  ]
  node [
    id 17
    label "deliver"
  ]
  node [
    id 18
    label "obserwowa&#263;"
  ]
  node [
    id 19
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 20
    label "uznawa&#263;"
  ]
  node [
    id 21
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 22
    label "organizowa&#263;"
  ]
  node [
    id 23
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 24
    label "czyni&#263;"
  ]
  node [
    id 25
    label "give"
  ]
  node [
    id 26
    label "stylizowa&#263;"
  ]
  node [
    id 27
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 28
    label "falowa&#263;"
  ]
  node [
    id 29
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 30
    label "peddle"
  ]
  node [
    id 31
    label "praca"
  ]
  node [
    id 32
    label "wydala&#263;"
  ]
  node [
    id 33
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 34
    label "tentegowa&#263;"
  ]
  node [
    id 35
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 36
    label "urz&#261;dza&#263;"
  ]
  node [
    id 37
    label "oszukiwa&#263;"
  ]
  node [
    id 38
    label "work"
  ]
  node [
    id 39
    label "ukazywa&#263;"
  ]
  node [
    id 40
    label "przerabia&#263;"
  ]
  node [
    id 41
    label "act"
  ]
  node [
    id 42
    label "post&#281;powa&#263;"
  ]
  node [
    id 43
    label "take_care"
  ]
  node [
    id 44
    label "troska&#263;_si&#281;"
  ]
  node [
    id 45
    label "rozpatrywa&#263;"
  ]
  node [
    id 46
    label "zamierza&#263;"
  ]
  node [
    id 47
    label "argue"
  ]
  node [
    id 48
    label "os&#261;dza&#263;"
  ]
  node [
    id 49
    label "notice"
  ]
  node [
    id 50
    label "stwierdza&#263;"
  ]
  node [
    id 51
    label "przyznawa&#263;"
  ]
  node [
    id 52
    label "zachowywa&#263;"
  ]
  node [
    id 53
    label "dostrzega&#263;"
  ]
  node [
    id 54
    label "patrze&#263;"
  ]
  node [
    id 55
    label "look"
  ]
  node [
    id 56
    label "cover"
  ]
  node [
    id 57
    label "polski"
  ]
  node [
    id 58
    label "przedmiot"
  ]
  node [
    id 59
    label "Polish"
  ]
  node [
    id 60
    label "goniony"
  ]
  node [
    id 61
    label "oberek"
  ]
  node [
    id 62
    label "ryba_po_grecku"
  ]
  node [
    id 63
    label "sztajer"
  ]
  node [
    id 64
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 65
    label "krakowiak"
  ]
  node [
    id 66
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 67
    label "pierogi_ruskie"
  ]
  node [
    id 68
    label "lacki"
  ]
  node [
    id 69
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 70
    label "chodzony"
  ]
  node [
    id 71
    label "po_polsku"
  ]
  node [
    id 72
    label "mazur"
  ]
  node [
    id 73
    label "polsko"
  ]
  node [
    id 74
    label "skoczny"
  ]
  node [
    id 75
    label "drabant"
  ]
  node [
    id 76
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 77
    label "j&#281;zyk"
  ]
  node [
    id 78
    label "uwaga"
  ]
  node [
    id 79
    label "punkt_widzenia"
  ]
  node [
    id 80
    label "przyczyna"
  ]
  node [
    id 81
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 82
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 83
    label "subject"
  ]
  node [
    id 84
    label "czynnik"
  ]
  node [
    id 85
    label "matuszka"
  ]
  node [
    id 86
    label "rezultat"
  ]
  node [
    id 87
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 88
    label "geneza"
  ]
  node [
    id 89
    label "poci&#261;ganie"
  ]
  node [
    id 90
    label "sk&#322;adnik"
  ]
  node [
    id 91
    label "warunki"
  ]
  node [
    id 92
    label "sytuacja"
  ]
  node [
    id 93
    label "wydarzenie"
  ]
  node [
    id 94
    label "wypowied&#378;"
  ]
  node [
    id 95
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 96
    label "stan"
  ]
  node [
    id 97
    label "nagana"
  ]
  node [
    id 98
    label "tekst"
  ]
  node [
    id 99
    label "upomnienie"
  ]
  node [
    id 100
    label "dzienniczek"
  ]
  node [
    id 101
    label "gossip"
  ]
  node [
    id 102
    label "osobowo&#347;&#263;"
  ]
  node [
    id 103
    label "kulturowo&#347;&#263;"
  ]
  node [
    id 104
    label "podmiot"
  ]
  node [
    id 105
    label "byt"
  ]
  node [
    id 106
    label "cz&#322;owiek"
  ]
  node [
    id 107
    label "superego"
  ]
  node [
    id 108
    label "wyj&#261;tkowy"
  ]
  node [
    id 109
    label "psychika"
  ]
  node [
    id 110
    label "charakter"
  ]
  node [
    id 111
    label "wn&#281;trze"
  ]
  node [
    id 112
    label "cecha"
  ]
  node [
    id 113
    label "self"
  ]
  node [
    id 114
    label "nietrwa&#322;y"
  ]
  node [
    id 115
    label "mizerny"
  ]
  node [
    id 116
    label "marnie"
  ]
  node [
    id 117
    label "delikatny"
  ]
  node [
    id 118
    label "po&#347;ledni"
  ]
  node [
    id 119
    label "niezdrowy"
  ]
  node [
    id 120
    label "z&#322;y"
  ]
  node [
    id 121
    label "nieumiej&#281;tny"
  ]
  node [
    id 122
    label "s&#322;abo"
  ]
  node [
    id 123
    label "nieznaczny"
  ]
  node [
    id 124
    label "lura"
  ]
  node [
    id 125
    label "nieudany"
  ]
  node [
    id 126
    label "s&#322;abowity"
  ]
  node [
    id 127
    label "zawodny"
  ]
  node [
    id 128
    label "&#322;agodny"
  ]
  node [
    id 129
    label "md&#322;y"
  ]
  node [
    id 130
    label "niedoskona&#322;y"
  ]
  node [
    id 131
    label "przemijaj&#261;cy"
  ]
  node [
    id 132
    label "niemocny"
  ]
  node [
    id 133
    label "niefajny"
  ]
  node [
    id 134
    label "kiepsko"
  ]
  node [
    id 135
    label "pieski"
  ]
  node [
    id 136
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 137
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 138
    label "niekorzystny"
  ]
  node [
    id 139
    label "z&#322;oszczenie"
  ]
  node [
    id 140
    label "sierdzisty"
  ]
  node [
    id 141
    label "niegrzeczny"
  ]
  node [
    id 142
    label "zez&#322;oszczenie"
  ]
  node [
    id 143
    label "zdenerwowany"
  ]
  node [
    id 144
    label "negatywny"
  ]
  node [
    id 145
    label "rozgniewanie"
  ]
  node [
    id 146
    label "gniewanie"
  ]
  node [
    id 147
    label "niemoralny"
  ]
  node [
    id 148
    label "&#378;le"
  ]
  node [
    id 149
    label "niepomy&#347;lny"
  ]
  node [
    id 150
    label "syf"
  ]
  node [
    id 151
    label "domek_z_kart"
  ]
  node [
    id 152
    label "kr&#243;tki"
  ]
  node [
    id 153
    label "zmienny"
  ]
  node [
    id 154
    label "nietrwale"
  ]
  node [
    id 155
    label "przemijaj&#261;co"
  ]
  node [
    id 156
    label "zawodnie"
  ]
  node [
    id 157
    label "niepewny"
  ]
  node [
    id 158
    label "niezdrowo"
  ]
  node [
    id 159
    label "dziwaczny"
  ]
  node [
    id 160
    label "chorobliwy"
  ]
  node [
    id 161
    label "szkodliwy"
  ]
  node [
    id 162
    label "chory"
  ]
  node [
    id 163
    label "chorowicie"
  ]
  node [
    id 164
    label "nieudanie"
  ]
  node [
    id 165
    label "nieciekawy"
  ]
  node [
    id 166
    label "niemi&#322;y"
  ]
  node [
    id 167
    label "nieprzyjemny"
  ]
  node [
    id 168
    label "niefajnie"
  ]
  node [
    id 169
    label "nieznacznie"
  ]
  node [
    id 170
    label "drobnostkowy"
  ]
  node [
    id 171
    label "niewa&#380;ny"
  ]
  node [
    id 172
    label "ma&#322;y"
  ]
  node [
    id 173
    label "nieumiej&#281;tnie"
  ]
  node [
    id 174
    label "niedoskonale"
  ]
  node [
    id 175
    label "ma&#322;o"
  ]
  node [
    id 176
    label "kiepski"
  ]
  node [
    id 177
    label "marny"
  ]
  node [
    id 178
    label "nadaremnie"
  ]
  node [
    id 179
    label "nieswojo"
  ]
  node [
    id 180
    label "feebly"
  ]
  node [
    id 181
    label "si&#322;a"
  ]
  node [
    id 182
    label "w&#261;t&#322;y"
  ]
  node [
    id 183
    label "po&#347;lednio"
  ]
  node [
    id 184
    label "przeci&#281;tny"
  ]
  node [
    id 185
    label "delikatnienie"
  ]
  node [
    id 186
    label "subtelny"
  ]
  node [
    id 187
    label "spokojny"
  ]
  node [
    id 188
    label "mi&#322;y"
  ]
  node [
    id 189
    label "prosty"
  ]
  node [
    id 190
    label "lekki"
  ]
  node [
    id 191
    label "zdelikatnienie"
  ]
  node [
    id 192
    label "&#322;agodnie"
  ]
  node [
    id 193
    label "nieszkodliwy"
  ]
  node [
    id 194
    label "delikatnie"
  ]
  node [
    id 195
    label "letki"
  ]
  node [
    id 196
    label "typowy"
  ]
  node [
    id 197
    label "zwyczajny"
  ]
  node [
    id 198
    label "harmonijny"
  ]
  node [
    id 199
    label "niesurowy"
  ]
  node [
    id 200
    label "przyjemny"
  ]
  node [
    id 201
    label "nieostry"
  ]
  node [
    id 202
    label "biedny"
  ]
  node [
    id 203
    label "blady"
  ]
  node [
    id 204
    label "sm&#281;tny"
  ]
  node [
    id 205
    label "mizernie"
  ]
  node [
    id 206
    label "n&#281;dznie"
  ]
  node [
    id 207
    label "szczyny"
  ]
  node [
    id 208
    label "nap&#243;j"
  ]
  node [
    id 209
    label "wydelikacanie"
  ]
  node [
    id 210
    label "k&#322;opotliwy"
  ]
  node [
    id 211
    label "dra&#380;liwy"
  ]
  node [
    id 212
    label "ostro&#380;ny"
  ]
  node [
    id 213
    label "wra&#380;liwy"
  ]
  node [
    id 214
    label "wydelikacenie"
  ]
  node [
    id 215
    label "taktowny"
  ]
  node [
    id 216
    label "choro"
  ]
  node [
    id 217
    label "przykry"
  ]
  node [
    id 218
    label "md&#322;o"
  ]
  node [
    id 219
    label "ckliwy"
  ]
  node [
    id 220
    label "nik&#322;y"
  ]
  node [
    id 221
    label "nijaki"
  ]
  node [
    id 222
    label "rozwini&#281;cie"
  ]
  node [
    id 223
    label "zapoznanie"
  ]
  node [
    id 224
    label "wys&#322;anie"
  ]
  node [
    id 225
    label "udoskonalenie"
  ]
  node [
    id 226
    label "pomo&#380;enie"
  ]
  node [
    id 227
    label "wiedza"
  ]
  node [
    id 228
    label "urszulanki"
  ]
  node [
    id 229
    label "training"
  ]
  node [
    id 230
    label "niepokalanki"
  ]
  node [
    id 231
    label "o&#347;wiecenie"
  ]
  node [
    id 232
    label "kwalifikacje"
  ]
  node [
    id 233
    label "sophistication"
  ]
  node [
    id 234
    label "skolaryzacja"
  ]
  node [
    id 235
    label "form"
  ]
  node [
    id 236
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 237
    label "eliminacje"
  ]
  node [
    id 238
    label "ustawienie"
  ]
  node [
    id 239
    label "exploitation"
  ]
  node [
    id 240
    label "powi&#281;kszenie"
  ]
  node [
    id 241
    label "oddzia&#322;anie"
  ]
  node [
    id 242
    label "dodanie"
  ]
  node [
    id 243
    label "enlargement"
  ]
  node [
    id 244
    label "przed&#322;o&#380;enie"
  ]
  node [
    id 245
    label "development"
  ]
  node [
    id 246
    label "rozpakowanie"
  ]
  node [
    id 247
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 248
    label "rozpostarcie"
  ]
  node [
    id 249
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 250
    label "rozstawienie"
  ]
  node [
    id 251
    label "przekazanie"
  ]
  node [
    id 252
    label "nakazanie"
  ]
  node [
    id 253
    label "wy&#322;o&#380;enie"
  ]
  node [
    id 254
    label "nabicie"
  ]
  node [
    id 255
    label "commitment"
  ]
  node [
    id 256
    label "p&#243;j&#347;cie"
  ]
  node [
    id 257
    label "bed"
  ]
  node [
    id 258
    label "stuffing"
  ]
  node [
    id 259
    label "wytworzenie"
  ]
  node [
    id 260
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 261
    label "representation"
  ]
  node [
    id 262
    label "zawarcie"
  ]
  node [
    id 263
    label "znajomy"
  ]
  node [
    id 264
    label "obznajomienie"
  ]
  node [
    id 265
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 266
    label "umo&#380;liwienie"
  ]
  node [
    id 267
    label "gathering"
  ]
  node [
    id 268
    label "poinformowanie"
  ]
  node [
    id 269
    label "knowing"
  ]
  node [
    id 270
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 271
    label "comfort"
  ]
  node [
    id 272
    label "poskutkowanie"
  ]
  node [
    id 273
    label "u&#322;atwienie"
  ]
  node [
    id 274
    label "zrobienie"
  ]
  node [
    id 275
    label "poprawa"
  ]
  node [
    id 276
    label "doskonalszy"
  ]
  node [
    id 277
    label "modyfikacja"
  ]
  node [
    id 278
    label "wyszlifowanie"
  ]
  node [
    id 279
    label "ulepszenie"
  ]
  node [
    id 280
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 281
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 282
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 283
    label "urszulanki_szare"
  ]
  node [
    id 284
    label "proporcja"
  ]
  node [
    id 285
    label "cognition"
  ]
  node [
    id 286
    label "intelekt"
  ]
  node [
    id 287
    label "pozwolenie"
  ]
  node [
    id 288
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 289
    label "zaawansowanie"
  ]
  node [
    id 290
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 291
    label "iluminacja"
  ]
  node [
    id 292
    label "epoka"
  ]
  node [
    id 293
    label "poznanie"
  ]
  node [
    id 294
    label "spowodowanie"
  ]
  node [
    id 295
    label "consciousness"
  ]
  node [
    id 296
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 297
    label "nowo&#380;ytno&#347;&#263;"
  ]
  node [
    id 298
    label "encyklopedyzm"
  ]
  node [
    id 299
    label "encyklopedy&#347;ci"
  ]
  node [
    id 300
    label "czynno&#347;&#263;"
  ]
  node [
    id 301
    label "u&#347;wiadomienie"
  ]
  node [
    id 302
    label "nauczenie"
  ]
  node [
    id 303
    label "nieistnienie"
  ]
  node [
    id 304
    label "odej&#347;cie"
  ]
  node [
    id 305
    label "defect"
  ]
  node [
    id 306
    label "gap"
  ]
  node [
    id 307
    label "odej&#347;&#263;"
  ]
  node [
    id 308
    label "wada"
  ]
  node [
    id 309
    label "odchodzi&#263;"
  ]
  node [
    id 310
    label "wyr&#243;b"
  ]
  node [
    id 311
    label "odchodzenie"
  ]
  node [
    id 312
    label "prywatywny"
  ]
  node [
    id 313
    label "niebyt"
  ]
  node [
    id 314
    label "nonexistence"
  ]
  node [
    id 315
    label "faintness"
  ]
  node [
    id 316
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 317
    label "schorzenie"
  ]
  node [
    id 318
    label "strona"
  ]
  node [
    id 319
    label "imperfection"
  ]
  node [
    id 320
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 321
    label "wytw&#243;r"
  ]
  node [
    id 322
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 323
    label "produkt"
  ]
  node [
    id 324
    label "creation"
  ]
  node [
    id 325
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 326
    label "p&#322;uczkarnia"
  ]
  node [
    id 327
    label "znakowarka"
  ]
  node [
    id 328
    label "produkcja"
  ]
  node [
    id 329
    label "szybki"
  ]
  node [
    id 330
    label "jednowyrazowy"
  ]
  node [
    id 331
    label "bliski"
  ]
  node [
    id 332
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 333
    label "kr&#243;tko"
  ]
  node [
    id 334
    label "drobny"
  ]
  node [
    id 335
    label "ruch"
  ]
  node [
    id 336
    label "odrzut"
  ]
  node [
    id 337
    label "drop"
  ]
  node [
    id 338
    label "proceed"
  ]
  node [
    id 339
    label "zrezygnowa&#263;"
  ]
  node [
    id 340
    label "ruszy&#263;"
  ]
  node [
    id 341
    label "min&#261;&#263;"
  ]
  node [
    id 342
    label "zrobi&#263;"
  ]
  node [
    id 343
    label "leave_office"
  ]
  node [
    id 344
    label "die"
  ]
  node [
    id 345
    label "retract"
  ]
  node [
    id 346
    label "opu&#347;ci&#263;"
  ]
  node [
    id 347
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 348
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 349
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 350
    label "przesta&#263;"
  ]
  node [
    id 351
    label "korkowanie"
  ]
  node [
    id 352
    label "death"
  ]
  node [
    id 353
    label "k&#322;adzenie_lachy"
  ]
  node [
    id 354
    label "przestawanie"
  ]
  node [
    id 355
    label "machanie_r&#281;k&#261;"
  ]
  node [
    id 356
    label "zb&#281;dny"
  ]
  node [
    id 357
    label "zdychanie"
  ]
  node [
    id 358
    label "spisywanie_"
  ]
  node [
    id 359
    label "usuwanie"
  ]
  node [
    id 360
    label "tracenie"
  ]
  node [
    id 361
    label "ko&#324;czenie"
  ]
  node [
    id 362
    label "zwalnianie_si&#281;"
  ]
  node [
    id 363
    label "&#380;ycie"
  ]
  node [
    id 364
    label "robienie"
  ]
  node [
    id 365
    label "opuszczanie"
  ]
  node [
    id 366
    label "wydalanie"
  ]
  node [
    id 367
    label "odrzucanie"
  ]
  node [
    id 368
    label "odstawianie"
  ]
  node [
    id 369
    label "martwy"
  ]
  node [
    id 370
    label "ust&#281;powanie"
  ]
  node [
    id 371
    label "egress"
  ]
  node [
    id 372
    label "zrzekanie_si&#281;"
  ]
  node [
    id 373
    label "dzianie_si&#281;"
  ]
  node [
    id 374
    label "oddzielanie_si&#281;"
  ]
  node [
    id 375
    label "bycie"
  ]
  node [
    id 376
    label "wyruszanie"
  ]
  node [
    id 377
    label "odumieranie"
  ]
  node [
    id 378
    label "odstawanie"
  ]
  node [
    id 379
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 380
    label "mijanie"
  ]
  node [
    id 381
    label "wracanie"
  ]
  node [
    id 382
    label "oddalanie_si&#281;"
  ]
  node [
    id 383
    label "kursowanie"
  ]
  node [
    id 384
    label "blend"
  ]
  node [
    id 385
    label "rozgrywa&#263;_si&#281;"
  ]
  node [
    id 386
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 387
    label "opuszcza&#263;"
  ]
  node [
    id 388
    label "impart"
  ]
  node [
    id 389
    label "wyrusza&#263;"
  ]
  node [
    id 390
    label "go"
  ]
  node [
    id 391
    label "seclude"
  ]
  node [
    id 392
    label "gasn&#261;&#263;"
  ]
  node [
    id 393
    label "przestawa&#263;"
  ]
  node [
    id 394
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 395
    label "odstawa&#263;"
  ]
  node [
    id 396
    label "rezygnowa&#263;"
  ]
  node [
    id 397
    label "i&#347;&#263;"
  ]
  node [
    id 398
    label "mija&#263;"
  ]
  node [
    id 399
    label "mini&#281;cie"
  ]
  node [
    id 400
    label "odumarcie"
  ]
  node [
    id 401
    label "dysponowanie_si&#281;"
  ]
  node [
    id 402
    label "ruszenie"
  ]
  node [
    id 403
    label "ust&#261;pienie"
  ]
  node [
    id 404
    label "mogi&#322;a"
  ]
  node [
    id 405
    label "pomarcie"
  ]
  node [
    id 406
    label "opuszczenie"
  ]
  node [
    id 407
    label "spisanie_"
  ]
  node [
    id 408
    label "oddalenie_si&#281;"
  ]
  node [
    id 409
    label "defenestracja"
  ]
  node [
    id 410
    label "danie_sobie_spokoju"
  ]
  node [
    id 411
    label "kres_&#380;ycia"
  ]
  node [
    id 412
    label "zwolnienie_si&#281;"
  ]
  node [
    id 413
    label "zdechni&#281;cie"
  ]
  node [
    id 414
    label "exit"
  ]
  node [
    id 415
    label "stracenie"
  ]
  node [
    id 416
    label "przestanie"
  ]
  node [
    id 417
    label "wr&#243;cenie"
  ]
  node [
    id 418
    label "szeol"
  ]
  node [
    id 419
    label "oddzielenie_si&#281;"
  ]
  node [
    id 420
    label "deviation"
  ]
  node [
    id 421
    label "wydalenie"
  ]
  node [
    id 422
    label "pogrzebanie"
  ]
  node [
    id 423
    label "&#380;a&#322;oba"
  ]
  node [
    id 424
    label "sko&#324;czenie"
  ]
  node [
    id 425
    label "withdrawal"
  ]
  node [
    id 426
    label "porozchodzenie_si&#281;"
  ]
  node [
    id 427
    label "zabicie"
  ]
  node [
    id 428
    label "agonia"
  ]
  node [
    id 429
    label "po&#322;o&#380;enie_lachy"
  ]
  node [
    id 430
    label "kres"
  ]
  node [
    id 431
    label "usuni&#281;cie"
  ]
  node [
    id 432
    label "relinquishment"
  ]
  node [
    id 433
    label "poniechanie"
  ]
  node [
    id 434
    label "zako&#324;czenie"
  ]
  node [
    id 435
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 436
    label "wypisanie_si&#281;"
  ]
  node [
    id 437
    label "ciekawski"
  ]
  node [
    id 438
    label "statysta"
  ]
  node [
    id 439
    label "pot&#281;ga"
  ]
  node [
    id 440
    label "documentation"
  ]
  node [
    id 441
    label "column"
  ]
  node [
    id 442
    label "zasadzenie"
  ]
  node [
    id 443
    label "za&#322;o&#380;enie"
  ]
  node [
    id 444
    label "punkt_odniesienia"
  ]
  node [
    id 445
    label "zasadzi&#263;"
  ]
  node [
    id 446
    label "bok"
  ]
  node [
    id 447
    label "d&#243;&#322;"
  ]
  node [
    id 448
    label "dzieci&#281;ctwo"
  ]
  node [
    id 449
    label "background"
  ]
  node [
    id 450
    label "podstawowy"
  ]
  node [
    id 451
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 452
    label "strategia"
  ]
  node [
    id 453
    label "pomys&#322;"
  ]
  node [
    id 454
    label "&#347;ciana"
  ]
  node [
    id 455
    label "podwini&#281;cie"
  ]
  node [
    id 456
    label "zap&#322;acenie"
  ]
  node [
    id 457
    label "przyodzianie"
  ]
  node [
    id 458
    label "budowla"
  ]
  node [
    id 459
    label "pokrycie"
  ]
  node [
    id 460
    label "rozebranie"
  ]
  node [
    id 461
    label "zak&#322;adka"
  ]
  node [
    id 462
    label "struktura"
  ]
  node [
    id 463
    label "poubieranie"
  ]
  node [
    id 464
    label "infliction"
  ]
  node [
    id 465
    label "pozak&#322;adanie"
  ]
  node [
    id 466
    label "program"
  ]
  node [
    id 467
    label "przebranie"
  ]
  node [
    id 468
    label "przywdzianie"
  ]
  node [
    id 469
    label "obleczenie_si&#281;"
  ]
  node [
    id 470
    label "utworzenie"
  ]
  node [
    id 471
    label "str&#243;j"
  ]
  node [
    id 472
    label "twierdzenie"
  ]
  node [
    id 473
    label "obleczenie"
  ]
  node [
    id 474
    label "umieszczenie"
  ]
  node [
    id 475
    label "przygotowywanie"
  ]
  node [
    id 476
    label "przymierzenie"
  ]
  node [
    id 477
    label "wyko&#324;czenie"
  ]
  node [
    id 478
    label "point"
  ]
  node [
    id 479
    label "przygotowanie"
  ]
  node [
    id 480
    label "proposition"
  ]
  node [
    id 481
    label "przewidzenie"
  ]
  node [
    id 482
    label "tu&#322;&#243;w"
  ]
  node [
    id 483
    label "kierunek"
  ]
  node [
    id 484
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 485
    label "wielok&#261;t"
  ]
  node [
    id 486
    label "odcinek"
  ]
  node [
    id 487
    label "strzelba"
  ]
  node [
    id 488
    label "lufa"
  ]
  node [
    id 489
    label "profil"
  ]
  node [
    id 490
    label "zbocze"
  ]
  node [
    id 491
    label "kszta&#322;t"
  ]
  node [
    id 492
    label "przegroda"
  ]
  node [
    id 493
    label "p&#322;aszczyzna"
  ]
  node [
    id 494
    label "bariera"
  ]
  node [
    id 495
    label "facebook"
  ]
  node [
    id 496
    label "wielo&#347;cian"
  ]
  node [
    id 497
    label "obstruction"
  ]
  node [
    id 498
    label "pow&#322;oka"
  ]
  node [
    id 499
    label "wyrobisko"
  ]
  node [
    id 500
    label "miejsce"
  ]
  node [
    id 501
    label "trudno&#347;&#263;"
  ]
  node [
    id 502
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 503
    label "zboczenie"
  ]
  node [
    id 504
    label "om&#243;wienie"
  ]
  node [
    id 505
    label "sponiewieranie"
  ]
  node [
    id 506
    label "discipline"
  ]
  node [
    id 507
    label "rzecz"
  ]
  node [
    id 508
    label "omawia&#263;"
  ]
  node [
    id 509
    label "kr&#261;&#380;enie"
  ]
  node [
    id 510
    label "tre&#347;&#263;"
  ]
  node [
    id 511
    label "sponiewiera&#263;"
  ]
  node [
    id 512
    label "element"
  ]
  node [
    id 513
    label "entity"
  ]
  node [
    id 514
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 515
    label "tematyka"
  ]
  node [
    id 516
    label "w&#261;tek"
  ]
  node [
    id 517
    label "zbaczanie"
  ]
  node [
    id 518
    label "program_nauczania"
  ]
  node [
    id 519
    label "om&#243;wi&#263;"
  ]
  node [
    id 520
    label "omawianie"
  ]
  node [
    id 521
    label "thing"
  ]
  node [
    id 522
    label "kultura"
  ]
  node [
    id 523
    label "istota"
  ]
  node [
    id 524
    label "zbacza&#263;"
  ]
  node [
    id 525
    label "zboczy&#263;"
  ]
  node [
    id 526
    label "wykopywa&#263;"
  ]
  node [
    id 527
    label "wykopanie"
  ]
  node [
    id 528
    label "&#347;piew"
  ]
  node [
    id 529
    label "wykopywanie"
  ]
  node [
    id 530
    label "hole"
  ]
  node [
    id 531
    label "low"
  ]
  node [
    id 532
    label "niski"
  ]
  node [
    id 533
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 534
    label "depressive_disorder"
  ]
  node [
    id 535
    label "d&#378;wi&#281;k"
  ]
  node [
    id 536
    label "wykopa&#263;"
  ]
  node [
    id 537
    label "za&#322;amanie"
  ]
  node [
    id 538
    label "niezaawansowany"
  ]
  node [
    id 539
    label "najwa&#380;niejszy"
  ]
  node [
    id 540
    label "pocz&#261;tkowy"
  ]
  node [
    id 541
    label "podstawowo"
  ]
  node [
    id 542
    label "wetkni&#281;cie"
  ]
  node [
    id 543
    label "przetkanie"
  ]
  node [
    id 544
    label "anchor"
  ]
  node [
    id 545
    label "przymocowanie"
  ]
  node [
    id 546
    label "zaczerpni&#281;cie"
  ]
  node [
    id 547
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 548
    label "interposition"
  ]
  node [
    id 549
    label "odm&#322;odzenie"
  ]
  node [
    id 550
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 551
    label "establish"
  ]
  node [
    id 552
    label "plant"
  ]
  node [
    id 553
    label "osnowa&#263;"
  ]
  node [
    id 554
    label "przymocowa&#263;"
  ]
  node [
    id 555
    label "umie&#347;ci&#263;"
  ]
  node [
    id 556
    label "wetkn&#261;&#263;"
  ]
  node [
    id 557
    label "dzieci&#324;stwo"
  ]
  node [
    id 558
    label "pocz&#261;tki"
  ]
  node [
    id 559
    label "pochodzenie"
  ]
  node [
    id 560
    label "kontekst"
  ]
  node [
    id 561
    label "idea"
  ]
  node [
    id 562
    label "ukradzenie"
  ]
  node [
    id 563
    label "ukra&#347;&#263;"
  ]
  node [
    id 564
    label "system"
  ]
  node [
    id 565
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 566
    label "plan"
  ]
  node [
    id 567
    label "operacja"
  ]
  node [
    id 568
    label "metoda"
  ]
  node [
    id 569
    label "gra"
  ]
  node [
    id 570
    label "wzorzec_projektowy"
  ]
  node [
    id 571
    label "dziedzina"
  ]
  node [
    id 572
    label "doktryna"
  ]
  node [
    id 573
    label "wrinkle"
  ]
  node [
    id 574
    label "dokument"
  ]
  node [
    id 575
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 576
    label "wojsko"
  ]
  node [
    id 577
    label "wyk&#322;adnik_pot&#281;gi"
  ]
  node [
    id 578
    label "organizacja"
  ]
  node [
    id 579
    label "violence"
  ]
  node [
    id 580
    label "podstawa_pot&#281;gi"
  ]
  node [
    id 581
    label "zdolno&#347;&#263;"
  ]
  node [
    id 582
    label "potencja"
  ]
  node [
    id 583
    label "iloczyn"
  ]
  node [
    id 584
    label "ekonomia_gospodarstw_domowych"
  ]
  node [
    id 585
    label "sprawno&#347;&#263;"
  ]
  node [
    id 586
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 587
    label "neuroekonimia"
  ]
  node [
    id 588
    label "makroekonomia"
  ]
  node [
    id 589
    label "nominalizm"
  ]
  node [
    id 590
    label "nauka_ekonomiczna"
  ]
  node [
    id 591
    label "bankowo&#347;&#263;"
  ]
  node [
    id 592
    label "katalaksja"
  ]
  node [
    id 593
    label "ekonomia_instytucjonalna"
  ]
  node [
    id 594
    label "dysponowa&#263;"
  ]
  node [
    id 595
    label "ekonometria"
  ]
  node [
    id 596
    label "book-building"
  ]
  node [
    id 597
    label "ekonomika"
  ]
  node [
    id 598
    label "farmakoekonomika"
  ]
  node [
    id 599
    label "wydzia&#322;"
  ]
  node [
    id 600
    label "mikroekonomia"
  ]
  node [
    id 601
    label "jako&#347;&#263;"
  ]
  node [
    id 602
    label "szybko&#347;&#263;"
  ]
  node [
    id 603
    label "kondycja_fizyczna"
  ]
  node [
    id 604
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 605
    label "zdrowie"
  ]
  node [
    id 606
    label "poj&#281;cie"
  ]
  node [
    id 607
    label "harcerski"
  ]
  node [
    id 608
    label "odznaka"
  ]
  node [
    id 609
    label "przebieg"
  ]
  node [
    id 610
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 611
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 612
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 613
    label "praktyka"
  ]
  node [
    id 614
    label "przeorientowywanie"
  ]
  node [
    id 615
    label "studia"
  ]
  node [
    id 616
    label "linia"
  ]
  node [
    id 617
    label "skr&#281;canie"
  ]
  node [
    id 618
    label "skr&#281;ca&#263;"
  ]
  node [
    id 619
    label "przeorientowywa&#263;"
  ]
  node [
    id 620
    label "orientowanie"
  ]
  node [
    id 621
    label "skr&#281;ci&#263;"
  ]
  node [
    id 622
    label "przeorientowanie"
  ]
  node [
    id 623
    label "zorientowanie"
  ]
  node [
    id 624
    label "przeorientowa&#263;"
  ]
  node [
    id 625
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 626
    label "ty&#322;"
  ]
  node [
    id 627
    label "zorientowa&#263;"
  ]
  node [
    id 628
    label "g&#243;ra"
  ]
  node [
    id 629
    label "orientowa&#263;"
  ]
  node [
    id 630
    label "spos&#243;b"
  ]
  node [
    id 631
    label "ideologia"
  ]
  node [
    id 632
    label "orientacja"
  ]
  node [
    id 633
    label "prz&#243;d"
  ]
  node [
    id 634
    label "bearing"
  ]
  node [
    id 635
    label "skr&#281;cenie"
  ]
  node [
    id 636
    label "jednostka_organizacyjna"
  ]
  node [
    id 637
    label "relation"
  ]
  node [
    id 638
    label "urz&#261;d"
  ]
  node [
    id 639
    label "whole"
  ]
  node [
    id 640
    label "miejsce_pracy"
  ]
  node [
    id 641
    label "podsekcja"
  ]
  node [
    id 642
    label "insourcing"
  ]
  node [
    id 643
    label "politechnika"
  ]
  node [
    id 644
    label "katedra"
  ]
  node [
    id 645
    label "ministerstwo"
  ]
  node [
    id 646
    label "uniwersytet"
  ]
  node [
    id 647
    label "dzia&#322;"
  ]
  node [
    id 648
    label "dispose"
  ]
  node [
    id 649
    label "namaszczenie_chorych"
  ]
  node [
    id 650
    label "control"
  ]
  node [
    id 651
    label "skazany"
  ]
  node [
    id 652
    label "rozporz&#261;dza&#263;"
  ]
  node [
    id 653
    label "przygotowywa&#263;"
  ]
  node [
    id 654
    label "command"
  ]
  node [
    id 655
    label "zasada"
  ]
  node [
    id 656
    label "pogl&#261;d"
  ]
  node [
    id 657
    label "nominalism"
  ]
  node [
    id 658
    label "finanse"
  ]
  node [
    id 659
    label "bankowo&#347;&#263;_elektroniczna"
  ]
  node [
    id 660
    label "bankowo&#347;&#263;_detaliczna"
  ]
  node [
    id 661
    label "bankowo&#347;&#263;_inwestycyjna"
  ]
  node [
    id 662
    label "supernadz&#243;r"
  ]
  node [
    id 663
    label "bankowo&#347;&#263;_sp&#243;&#322;dzielcza"
  ]
  node [
    id 664
    label "gospodarka"
  ]
  node [
    id 665
    label "regulacja_cen"
  ]
  node [
    id 666
    label "nauka"
  ]
  node [
    id 667
    label "farmacja"
  ]
  node [
    id 668
    label "analiza_ekonomiczna"
  ]
  node [
    id 669
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 670
    label "oferta_handlowa"
  ]
  node [
    id 671
    label "mechanizm_mno&#380;nikowy"
  ]
  node [
    id 672
    label "agregat_ekonomiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
]
