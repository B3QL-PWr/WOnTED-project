graph [
  node [
    id 0
    label "gazeta"
    origin "text"
  ]
  node [
    id 1
    label "wyborczy"
    origin "text"
  ]
  node [
    id 2
    label "tekst"
    origin "text"
  ]
  node [
    id 3
    label "polski"
    origin "text"
  ]
  node [
    id 4
    label "bootlegowym"
    origin "text"
  ]
  node [
    id 5
    label "wideo"
    origin "text"
  ]
  node [
    id 6
    label "koncert"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "ten"
    origin "text"
  ]
  node [
    id 9
    label "historia"
    origin "text"
  ]
  node [
    id 10
    label "wszystko"
    origin "text"
  ]
  node [
    id 11
    label "ekscytowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "nowa"
    origin "text"
  ]
  node [
    id 13
    label "kultura"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "wyra&#378;nie"
    origin "text"
  ]
  node [
    id 16
    label "wreszcie"
    origin "text"
  ]
  node [
    id 17
    label "si&#281;"
    origin "text"
  ]
  node [
    id 18
    label "otwiera&#263;"
    origin "text"
  ]
  node [
    id 19
    label "tytu&#322;"
  ]
  node [
    id 20
    label "redakcja"
  ]
  node [
    id 21
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 22
    label "czasopismo"
  ]
  node [
    id 23
    label "prasa"
  ]
  node [
    id 24
    label "egzemplarz"
  ]
  node [
    id 25
    label "psychotest"
  ]
  node [
    id 26
    label "pismo"
  ]
  node [
    id 27
    label "communication"
  ]
  node [
    id 28
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 29
    label "wk&#322;ad"
  ]
  node [
    id 30
    label "zajawka"
  ]
  node [
    id 31
    label "ok&#322;adka"
  ]
  node [
    id 32
    label "Zwrotnica"
  ]
  node [
    id 33
    label "dzia&#322;"
  ]
  node [
    id 34
    label "redaktor"
  ]
  node [
    id 35
    label "radio"
  ]
  node [
    id 36
    label "zesp&#243;&#322;"
  ]
  node [
    id 37
    label "siedziba"
  ]
  node [
    id 38
    label "composition"
  ]
  node [
    id 39
    label "wydawnictwo"
  ]
  node [
    id 40
    label "redaction"
  ]
  node [
    id 41
    label "telewizja"
  ]
  node [
    id 42
    label "obr&#243;bka"
  ]
  node [
    id 43
    label "debit"
  ]
  node [
    id 44
    label "druk"
  ]
  node [
    id 45
    label "publikacja"
  ]
  node [
    id 46
    label "nadtytu&#322;"
  ]
  node [
    id 47
    label "szata_graficzna"
  ]
  node [
    id 48
    label "tytulatura"
  ]
  node [
    id 49
    label "wydawa&#263;"
  ]
  node [
    id 50
    label "elevation"
  ]
  node [
    id 51
    label "wyda&#263;"
  ]
  node [
    id 52
    label "mianowaniec"
  ]
  node [
    id 53
    label "poster"
  ]
  node [
    id 54
    label "nazwa"
  ]
  node [
    id 55
    label "podtytu&#322;"
  ]
  node [
    id 56
    label "centerfold"
  ]
  node [
    id 57
    label "t&#322;oczysko"
  ]
  node [
    id 58
    label "depesza"
  ]
  node [
    id 59
    label "maszyna"
  ]
  node [
    id 60
    label "media"
  ]
  node [
    id 61
    label "napisa&#263;"
  ]
  node [
    id 62
    label "dziennikarz_prasowy"
  ]
  node [
    id 63
    label "pisa&#263;"
  ]
  node [
    id 64
    label "kiosk"
  ]
  node [
    id 65
    label "maszyna_rolnicza"
  ]
  node [
    id 66
    label "ekscerpcja"
  ]
  node [
    id 67
    label "j&#281;zykowo"
  ]
  node [
    id 68
    label "wypowied&#378;"
  ]
  node [
    id 69
    label "wytw&#243;r"
  ]
  node [
    id 70
    label "pomini&#281;cie"
  ]
  node [
    id 71
    label "dzie&#322;o"
  ]
  node [
    id 72
    label "preparacja"
  ]
  node [
    id 73
    label "odmianka"
  ]
  node [
    id 74
    label "opu&#347;ci&#263;"
  ]
  node [
    id 75
    label "koniektura"
  ]
  node [
    id 76
    label "obelga"
  ]
  node [
    id 77
    label "przedmiot"
  ]
  node [
    id 78
    label "p&#322;&#243;d"
  ]
  node [
    id 79
    label "work"
  ]
  node [
    id 80
    label "rezultat"
  ]
  node [
    id 81
    label "obrazowanie"
  ]
  node [
    id 82
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 83
    label "dorobek"
  ]
  node [
    id 84
    label "forma"
  ]
  node [
    id 85
    label "tre&#347;&#263;"
  ]
  node [
    id 86
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 87
    label "retrospektywa"
  ]
  node [
    id 88
    label "works"
  ]
  node [
    id 89
    label "creation"
  ]
  node [
    id 90
    label "tetralogia"
  ]
  node [
    id 91
    label "komunikat"
  ]
  node [
    id 92
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 93
    label "praca"
  ]
  node [
    id 94
    label "pos&#322;uchanie"
  ]
  node [
    id 95
    label "s&#261;d"
  ]
  node [
    id 96
    label "sparafrazowanie"
  ]
  node [
    id 97
    label "strawestowa&#263;"
  ]
  node [
    id 98
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 99
    label "trawestowa&#263;"
  ]
  node [
    id 100
    label "sparafrazowa&#263;"
  ]
  node [
    id 101
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 102
    label "sformu&#322;owanie"
  ]
  node [
    id 103
    label "parafrazowanie"
  ]
  node [
    id 104
    label "ozdobnik"
  ]
  node [
    id 105
    label "delimitacja"
  ]
  node [
    id 106
    label "parafrazowa&#263;"
  ]
  node [
    id 107
    label "stylizacja"
  ]
  node [
    id 108
    label "trawestowanie"
  ]
  node [
    id 109
    label "strawestowanie"
  ]
  node [
    id 110
    label "cholera"
  ]
  node [
    id 111
    label "ubliga"
  ]
  node [
    id 112
    label "niedorobek"
  ]
  node [
    id 113
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 114
    label "chuj"
  ]
  node [
    id 115
    label "bluzg"
  ]
  node [
    id 116
    label "wyzwisko"
  ]
  node [
    id 117
    label "indignation"
  ]
  node [
    id 118
    label "pies"
  ]
  node [
    id 119
    label "wrzuta"
  ]
  node [
    id 120
    label "chujowy"
  ]
  node [
    id 121
    label "krzywda"
  ]
  node [
    id 122
    label "szmata"
  ]
  node [
    id 123
    label "odmiana"
  ]
  node [
    id 124
    label "formu&#322;owa&#263;"
  ]
  node [
    id 125
    label "ozdabia&#263;"
  ]
  node [
    id 126
    label "stawia&#263;"
  ]
  node [
    id 127
    label "spell"
  ]
  node [
    id 128
    label "styl"
  ]
  node [
    id 129
    label "skryba"
  ]
  node [
    id 130
    label "read"
  ]
  node [
    id 131
    label "donosi&#263;"
  ]
  node [
    id 132
    label "code"
  ]
  node [
    id 133
    label "dysgrafia"
  ]
  node [
    id 134
    label "dysortografia"
  ]
  node [
    id 135
    label "tworzy&#263;"
  ]
  node [
    id 136
    label "preparation"
  ]
  node [
    id 137
    label "proces_technologiczny"
  ]
  node [
    id 138
    label "uj&#281;cie"
  ]
  node [
    id 139
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 140
    label "pozostawi&#263;"
  ]
  node [
    id 141
    label "obni&#380;y&#263;"
  ]
  node [
    id 142
    label "zostawi&#263;"
  ]
  node [
    id 143
    label "przesta&#263;"
  ]
  node [
    id 144
    label "potani&#263;"
  ]
  node [
    id 145
    label "drop"
  ]
  node [
    id 146
    label "evacuate"
  ]
  node [
    id 147
    label "humiliate"
  ]
  node [
    id 148
    label "leave"
  ]
  node [
    id 149
    label "straci&#263;"
  ]
  node [
    id 150
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 151
    label "authorize"
  ]
  node [
    id 152
    label "omin&#261;&#263;"
  ]
  node [
    id 153
    label "przypuszczenie"
  ]
  node [
    id 154
    label "conjecture"
  ]
  node [
    id 155
    label "wniosek"
  ]
  node [
    id 156
    label "wyb&#243;r"
  ]
  node [
    id 157
    label "dokumentacja"
  ]
  node [
    id 158
    label "u&#380;ytkownik"
  ]
  node [
    id 159
    label "komunikacyjnie"
  ]
  node [
    id 160
    label "ellipsis"
  ]
  node [
    id 161
    label "wykluczenie"
  ]
  node [
    id 162
    label "figura_my&#347;li"
  ]
  node [
    id 163
    label "zrobienie"
  ]
  node [
    id 164
    label "Polish"
  ]
  node [
    id 165
    label "goniony"
  ]
  node [
    id 166
    label "oberek"
  ]
  node [
    id 167
    label "ryba_po_grecku"
  ]
  node [
    id 168
    label "sztajer"
  ]
  node [
    id 169
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 170
    label "krakowiak"
  ]
  node [
    id 171
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 172
    label "pierogi_ruskie"
  ]
  node [
    id 173
    label "lacki"
  ]
  node [
    id 174
    label "polak"
  ]
  node [
    id 175
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 176
    label "chodzony"
  ]
  node [
    id 177
    label "po_polsku"
  ]
  node [
    id 178
    label "mazur"
  ]
  node [
    id 179
    label "polsko"
  ]
  node [
    id 180
    label "skoczny"
  ]
  node [
    id 181
    label "drabant"
  ]
  node [
    id 182
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 183
    label "j&#281;zyk"
  ]
  node [
    id 184
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 185
    label "artykulator"
  ]
  node [
    id 186
    label "kod"
  ]
  node [
    id 187
    label "kawa&#322;ek"
  ]
  node [
    id 188
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 189
    label "gramatyka"
  ]
  node [
    id 190
    label "stylik"
  ]
  node [
    id 191
    label "przet&#322;umaczenie"
  ]
  node [
    id 192
    label "formalizowanie"
  ]
  node [
    id 193
    label "ssanie"
  ]
  node [
    id 194
    label "ssa&#263;"
  ]
  node [
    id 195
    label "language"
  ]
  node [
    id 196
    label "liza&#263;"
  ]
  node [
    id 197
    label "konsonantyzm"
  ]
  node [
    id 198
    label "wokalizm"
  ]
  node [
    id 199
    label "fonetyka"
  ]
  node [
    id 200
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 201
    label "jeniec"
  ]
  node [
    id 202
    label "but"
  ]
  node [
    id 203
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 204
    label "po_koroniarsku"
  ]
  node [
    id 205
    label "kultura_duchowa"
  ]
  node [
    id 206
    label "t&#322;umaczenie"
  ]
  node [
    id 207
    label "m&#243;wienie"
  ]
  node [
    id 208
    label "pype&#263;"
  ]
  node [
    id 209
    label "lizanie"
  ]
  node [
    id 210
    label "formalizowa&#263;"
  ]
  node [
    id 211
    label "rozumie&#263;"
  ]
  node [
    id 212
    label "organ"
  ]
  node [
    id 213
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 214
    label "rozumienie"
  ]
  node [
    id 215
    label "spos&#243;b"
  ]
  node [
    id 216
    label "makroglosja"
  ]
  node [
    id 217
    label "m&#243;wi&#263;"
  ]
  node [
    id 218
    label "jama_ustna"
  ]
  node [
    id 219
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 220
    label "formacja_geologiczna"
  ]
  node [
    id 221
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 222
    label "natural_language"
  ]
  node [
    id 223
    label "s&#322;ownictwo"
  ]
  node [
    id 224
    label "urz&#261;dzenie"
  ]
  node [
    id 225
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 226
    label "wschodnioeuropejski"
  ]
  node [
    id 227
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 228
    label "poga&#324;ski"
  ]
  node [
    id 229
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 230
    label "topielec"
  ]
  node [
    id 231
    label "europejski"
  ]
  node [
    id 232
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 233
    label "langosz"
  ]
  node [
    id 234
    label "zboczenie"
  ]
  node [
    id 235
    label "om&#243;wienie"
  ]
  node [
    id 236
    label "sponiewieranie"
  ]
  node [
    id 237
    label "discipline"
  ]
  node [
    id 238
    label "rzecz"
  ]
  node [
    id 239
    label "omawia&#263;"
  ]
  node [
    id 240
    label "kr&#261;&#380;enie"
  ]
  node [
    id 241
    label "robienie"
  ]
  node [
    id 242
    label "sponiewiera&#263;"
  ]
  node [
    id 243
    label "element"
  ]
  node [
    id 244
    label "entity"
  ]
  node [
    id 245
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 246
    label "tematyka"
  ]
  node [
    id 247
    label "w&#261;tek"
  ]
  node [
    id 248
    label "charakter"
  ]
  node [
    id 249
    label "zbaczanie"
  ]
  node [
    id 250
    label "program_nauczania"
  ]
  node [
    id 251
    label "om&#243;wi&#263;"
  ]
  node [
    id 252
    label "omawianie"
  ]
  node [
    id 253
    label "thing"
  ]
  node [
    id 254
    label "istota"
  ]
  node [
    id 255
    label "zbacza&#263;"
  ]
  node [
    id 256
    label "zboczy&#263;"
  ]
  node [
    id 257
    label "gwardzista"
  ]
  node [
    id 258
    label "melodia"
  ]
  node [
    id 259
    label "taniec"
  ]
  node [
    id 260
    label "taniec_ludowy"
  ]
  node [
    id 261
    label "&#347;redniowieczny"
  ]
  node [
    id 262
    label "specjalny"
  ]
  node [
    id 263
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 264
    label "weso&#322;y"
  ]
  node [
    id 265
    label "sprawny"
  ]
  node [
    id 266
    label "rytmiczny"
  ]
  node [
    id 267
    label "skocznie"
  ]
  node [
    id 268
    label "energiczny"
  ]
  node [
    id 269
    label "lendler"
  ]
  node [
    id 270
    label "austriacki"
  ]
  node [
    id 271
    label "polka"
  ]
  node [
    id 272
    label "europejsko"
  ]
  node [
    id 273
    label "przytup"
  ]
  node [
    id 274
    label "ho&#322;ubiec"
  ]
  node [
    id 275
    label "wodzi&#263;"
  ]
  node [
    id 276
    label "ludowy"
  ]
  node [
    id 277
    label "pie&#347;&#324;"
  ]
  node [
    id 278
    label "mieszkaniec"
  ]
  node [
    id 279
    label "centu&#347;"
  ]
  node [
    id 280
    label "lalka"
  ]
  node [
    id 281
    label "Ma&#322;opolanin"
  ]
  node [
    id 282
    label "krakauer"
  ]
  node [
    id 283
    label "technika"
  ]
  node [
    id 284
    label "film"
  ]
  node [
    id 285
    label "g&#322;owica_elektromagnetyczna"
  ]
  node [
    id 286
    label "wideokaseta"
  ]
  node [
    id 287
    label "odtwarzacz"
  ]
  node [
    id 288
    label "telekomunikacja"
  ]
  node [
    id 289
    label "cywilizacja"
  ]
  node [
    id 290
    label "wiedza"
  ]
  node [
    id 291
    label "sprawno&#347;&#263;"
  ]
  node [
    id 292
    label "engineering"
  ]
  node [
    id 293
    label "fotowoltaika"
  ]
  node [
    id 294
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 295
    label "teletechnika"
  ]
  node [
    id 296
    label "mechanika_precyzyjna"
  ]
  node [
    id 297
    label "technologia"
  ]
  node [
    id 298
    label "animatronika"
  ]
  node [
    id 299
    label "odczulenie"
  ]
  node [
    id 300
    label "odczula&#263;"
  ]
  node [
    id 301
    label "blik"
  ]
  node [
    id 302
    label "odczuli&#263;"
  ]
  node [
    id 303
    label "scena"
  ]
  node [
    id 304
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 305
    label "muza"
  ]
  node [
    id 306
    label "postprodukcja"
  ]
  node [
    id 307
    label "block"
  ]
  node [
    id 308
    label "trawiarnia"
  ]
  node [
    id 309
    label "sklejarka"
  ]
  node [
    id 310
    label "sztuka"
  ]
  node [
    id 311
    label "filmoteka"
  ]
  node [
    id 312
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 313
    label "klatka"
  ]
  node [
    id 314
    label "rozbieg&#243;wka"
  ]
  node [
    id 315
    label "napisy"
  ]
  node [
    id 316
    label "ta&#347;ma"
  ]
  node [
    id 317
    label "odczulanie"
  ]
  node [
    id 318
    label "anamorfoza"
  ]
  node [
    id 319
    label "ty&#322;&#243;wka"
  ]
  node [
    id 320
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 321
    label "b&#322;ona"
  ]
  node [
    id 322
    label "emulsja_fotograficzna"
  ]
  node [
    id 323
    label "photograph"
  ]
  node [
    id 324
    label "czo&#322;&#243;wka"
  ]
  node [
    id 325
    label "rola"
  ]
  node [
    id 326
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 327
    label "sprz&#281;t_audio"
  ]
  node [
    id 328
    label "magnetowid"
  ]
  node [
    id 329
    label "wideoteka"
  ]
  node [
    id 330
    label "wypo&#380;yczalnia_wideo"
  ]
  node [
    id 331
    label "kaseta"
  ]
  node [
    id 332
    label "pokaz"
  ]
  node [
    id 333
    label "performance"
  ]
  node [
    id 334
    label "wyst&#281;p"
  ]
  node [
    id 335
    label "show"
  ]
  node [
    id 336
    label "bogactwo"
  ]
  node [
    id 337
    label "mn&#243;stwo"
  ]
  node [
    id 338
    label "utw&#243;r"
  ]
  node [
    id 339
    label "zjawisko"
  ]
  node [
    id 340
    label "szale&#324;stwo"
  ]
  node [
    id 341
    label "p&#322;acz"
  ]
  node [
    id 342
    label "proces"
  ]
  node [
    id 343
    label "boski"
  ]
  node [
    id 344
    label "krajobraz"
  ]
  node [
    id 345
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 346
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 347
    label "przywidzenie"
  ]
  node [
    id 348
    label "presence"
  ]
  node [
    id 349
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 350
    label "pokaz&#243;wka"
  ]
  node [
    id 351
    label "prezenter"
  ]
  node [
    id 352
    label "wydarzenie"
  ]
  node [
    id 353
    label "wyraz"
  ]
  node [
    id 354
    label "impreza"
  ]
  node [
    id 355
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 356
    label "tingel-tangel"
  ]
  node [
    id 357
    label "numer"
  ]
  node [
    id 358
    label "trema"
  ]
  node [
    id 359
    label "odtworzenie"
  ]
  node [
    id 360
    label "ilo&#347;&#263;"
  ]
  node [
    id 361
    label "enormousness"
  ]
  node [
    id 362
    label "g&#322;upstwo"
  ]
  node [
    id 363
    label "oszo&#322;omstwo"
  ]
  node [
    id 364
    label "ob&#322;&#281;d"
  ]
  node [
    id 365
    label "pomieszanie_zmys&#322;&#243;w"
  ]
  node [
    id 366
    label "temper"
  ]
  node [
    id 367
    label "zamieszanie"
  ]
  node [
    id 368
    label "folly"
  ]
  node [
    id 369
    label "zabawa"
  ]
  node [
    id 370
    label "poryw"
  ]
  node [
    id 371
    label "choroba_psychiczna"
  ]
  node [
    id 372
    label "gor&#261;cy_okres"
  ]
  node [
    id 373
    label "stupidity"
  ]
  node [
    id 374
    label "sytuacja"
  ]
  node [
    id 375
    label "wysyp"
  ]
  node [
    id 376
    label "fullness"
  ]
  node [
    id 377
    label "podostatek"
  ]
  node [
    id 378
    label "mienie"
  ]
  node [
    id 379
    label "fortune"
  ]
  node [
    id 380
    label "z&#322;ote_czasy"
  ]
  node [
    id 381
    label "cecha"
  ]
  node [
    id 382
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 383
    label "part"
  ]
  node [
    id 384
    label "element_anatomiczny"
  ]
  node [
    id 385
    label "czynno&#347;&#263;"
  ]
  node [
    id 386
    label "&#380;al"
  ]
  node [
    id 387
    label "reakcja"
  ]
  node [
    id 388
    label "przedstawienie"
  ]
  node [
    id 389
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 390
    label "zachowanie"
  ]
  node [
    id 391
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 392
    label "mie&#263;_miejsce"
  ]
  node [
    id 393
    label "equal"
  ]
  node [
    id 394
    label "trwa&#263;"
  ]
  node [
    id 395
    label "chodzi&#263;"
  ]
  node [
    id 396
    label "si&#281;ga&#263;"
  ]
  node [
    id 397
    label "stan"
  ]
  node [
    id 398
    label "obecno&#347;&#263;"
  ]
  node [
    id 399
    label "stand"
  ]
  node [
    id 400
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 401
    label "uczestniczy&#263;"
  ]
  node [
    id 402
    label "participate"
  ]
  node [
    id 403
    label "robi&#263;"
  ]
  node [
    id 404
    label "istnie&#263;"
  ]
  node [
    id 405
    label "pozostawa&#263;"
  ]
  node [
    id 406
    label "zostawa&#263;"
  ]
  node [
    id 407
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 408
    label "adhere"
  ]
  node [
    id 409
    label "compass"
  ]
  node [
    id 410
    label "korzysta&#263;"
  ]
  node [
    id 411
    label "appreciation"
  ]
  node [
    id 412
    label "osi&#261;ga&#263;"
  ]
  node [
    id 413
    label "dociera&#263;"
  ]
  node [
    id 414
    label "get"
  ]
  node [
    id 415
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 416
    label "mierzy&#263;"
  ]
  node [
    id 417
    label "u&#380;ywa&#263;"
  ]
  node [
    id 418
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 419
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 420
    label "exsert"
  ]
  node [
    id 421
    label "being"
  ]
  node [
    id 422
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 423
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 424
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 425
    label "p&#322;ywa&#263;"
  ]
  node [
    id 426
    label "run"
  ]
  node [
    id 427
    label "bangla&#263;"
  ]
  node [
    id 428
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 429
    label "przebiega&#263;"
  ]
  node [
    id 430
    label "wk&#322;ada&#263;"
  ]
  node [
    id 431
    label "proceed"
  ]
  node [
    id 432
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 433
    label "carry"
  ]
  node [
    id 434
    label "bywa&#263;"
  ]
  node [
    id 435
    label "dziama&#263;"
  ]
  node [
    id 436
    label "stara&#263;_si&#281;"
  ]
  node [
    id 437
    label "para"
  ]
  node [
    id 438
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 439
    label "str&#243;j"
  ]
  node [
    id 440
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 441
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 442
    label "krok"
  ]
  node [
    id 443
    label "tryb"
  ]
  node [
    id 444
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 445
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 446
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 447
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 448
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 449
    label "continue"
  ]
  node [
    id 450
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 451
    label "Ohio"
  ]
  node [
    id 452
    label "wci&#281;cie"
  ]
  node [
    id 453
    label "Nowy_York"
  ]
  node [
    id 454
    label "warstwa"
  ]
  node [
    id 455
    label "samopoczucie"
  ]
  node [
    id 456
    label "Illinois"
  ]
  node [
    id 457
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 458
    label "state"
  ]
  node [
    id 459
    label "Jukatan"
  ]
  node [
    id 460
    label "Kalifornia"
  ]
  node [
    id 461
    label "Wirginia"
  ]
  node [
    id 462
    label "wektor"
  ]
  node [
    id 463
    label "Teksas"
  ]
  node [
    id 464
    label "Goa"
  ]
  node [
    id 465
    label "Waszyngton"
  ]
  node [
    id 466
    label "miejsce"
  ]
  node [
    id 467
    label "Massachusetts"
  ]
  node [
    id 468
    label "Alaska"
  ]
  node [
    id 469
    label "Arakan"
  ]
  node [
    id 470
    label "Hawaje"
  ]
  node [
    id 471
    label "Maryland"
  ]
  node [
    id 472
    label "punkt"
  ]
  node [
    id 473
    label "Michigan"
  ]
  node [
    id 474
    label "Arizona"
  ]
  node [
    id 475
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 476
    label "Georgia"
  ]
  node [
    id 477
    label "poziom"
  ]
  node [
    id 478
    label "Pensylwania"
  ]
  node [
    id 479
    label "shape"
  ]
  node [
    id 480
    label "Luizjana"
  ]
  node [
    id 481
    label "Nowy_Meksyk"
  ]
  node [
    id 482
    label "Alabama"
  ]
  node [
    id 483
    label "Kansas"
  ]
  node [
    id 484
    label "Oregon"
  ]
  node [
    id 485
    label "Floryda"
  ]
  node [
    id 486
    label "Oklahoma"
  ]
  node [
    id 487
    label "jednostka_administracyjna"
  ]
  node [
    id 488
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 489
    label "okre&#347;lony"
  ]
  node [
    id 490
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 491
    label "wiadomy"
  ]
  node [
    id 492
    label "historiografia"
  ]
  node [
    id 493
    label "nauka_humanistyczna"
  ]
  node [
    id 494
    label "nautologia"
  ]
  node [
    id 495
    label "epigrafika"
  ]
  node [
    id 496
    label "muzealnictwo"
  ]
  node [
    id 497
    label "report"
  ]
  node [
    id 498
    label "hista"
  ]
  node [
    id 499
    label "przebiec"
  ]
  node [
    id 500
    label "zabytkoznawstwo"
  ]
  node [
    id 501
    label "historia_gospodarcza"
  ]
  node [
    id 502
    label "motyw"
  ]
  node [
    id 503
    label "kierunek"
  ]
  node [
    id 504
    label "varsavianistyka"
  ]
  node [
    id 505
    label "filigranistyka"
  ]
  node [
    id 506
    label "neografia"
  ]
  node [
    id 507
    label "prezentyzm"
  ]
  node [
    id 508
    label "bizantynistyka"
  ]
  node [
    id 509
    label "ikonografia"
  ]
  node [
    id 510
    label "genealogia"
  ]
  node [
    id 511
    label "epoka"
  ]
  node [
    id 512
    label "historia_sztuki"
  ]
  node [
    id 513
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 514
    label "ruralistyka"
  ]
  node [
    id 515
    label "annalistyka"
  ]
  node [
    id 516
    label "papirologia"
  ]
  node [
    id 517
    label "heraldyka"
  ]
  node [
    id 518
    label "archiwistyka"
  ]
  node [
    id 519
    label "dyplomatyka"
  ]
  node [
    id 520
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 521
    label "numizmatyka"
  ]
  node [
    id 522
    label "chronologia"
  ]
  node [
    id 523
    label "historyka"
  ]
  node [
    id 524
    label "prozopografia"
  ]
  node [
    id 525
    label "sfragistyka"
  ]
  node [
    id 526
    label "weksylologia"
  ]
  node [
    id 527
    label "paleografia"
  ]
  node [
    id 528
    label "mediewistyka"
  ]
  node [
    id 529
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 530
    label "przebiegni&#281;cie"
  ]
  node [
    id 531
    label "fabu&#322;a"
  ]
  node [
    id 532
    label "koleje_losu"
  ]
  node [
    id 533
    label "&#380;ycie"
  ]
  node [
    id 534
    label "czas"
  ]
  node [
    id 535
    label "przebieg"
  ]
  node [
    id 536
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 537
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 538
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 539
    label "praktyka"
  ]
  node [
    id 540
    label "system"
  ]
  node [
    id 541
    label "przeorientowywanie"
  ]
  node [
    id 542
    label "studia"
  ]
  node [
    id 543
    label "linia"
  ]
  node [
    id 544
    label "bok"
  ]
  node [
    id 545
    label "skr&#281;canie"
  ]
  node [
    id 546
    label "skr&#281;ca&#263;"
  ]
  node [
    id 547
    label "przeorientowywa&#263;"
  ]
  node [
    id 548
    label "orientowanie"
  ]
  node [
    id 549
    label "skr&#281;ci&#263;"
  ]
  node [
    id 550
    label "przeorientowanie"
  ]
  node [
    id 551
    label "zorientowanie"
  ]
  node [
    id 552
    label "przeorientowa&#263;"
  ]
  node [
    id 553
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 554
    label "metoda"
  ]
  node [
    id 555
    label "ty&#322;"
  ]
  node [
    id 556
    label "zorientowa&#263;"
  ]
  node [
    id 557
    label "g&#243;ra"
  ]
  node [
    id 558
    label "orientowa&#263;"
  ]
  node [
    id 559
    label "ideologia"
  ]
  node [
    id 560
    label "orientacja"
  ]
  node [
    id 561
    label "prz&#243;d"
  ]
  node [
    id 562
    label "bearing"
  ]
  node [
    id 563
    label "skr&#281;cenie"
  ]
  node [
    id 564
    label "aalen"
  ]
  node [
    id 565
    label "jura_wczesna"
  ]
  node [
    id 566
    label "holocen"
  ]
  node [
    id 567
    label "pliocen"
  ]
  node [
    id 568
    label "plejstocen"
  ]
  node [
    id 569
    label "paleocen"
  ]
  node [
    id 570
    label "dzieje"
  ]
  node [
    id 571
    label "bajos"
  ]
  node [
    id 572
    label "kelowej"
  ]
  node [
    id 573
    label "eocen"
  ]
  node [
    id 574
    label "jednostka_geologiczna"
  ]
  node [
    id 575
    label "okres"
  ]
  node [
    id 576
    label "schy&#322;ek"
  ]
  node [
    id 577
    label "miocen"
  ]
  node [
    id 578
    label "&#347;rodkowy_trias"
  ]
  node [
    id 579
    label "term"
  ]
  node [
    id 580
    label "Zeitgeist"
  ]
  node [
    id 581
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 582
    label "wczesny_trias"
  ]
  node [
    id 583
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 584
    label "jura_&#347;rodkowa"
  ]
  node [
    id 585
    label "oligocen"
  ]
  node [
    id 586
    label "w&#281;ze&#322;"
  ]
  node [
    id 587
    label "perypetia"
  ]
  node [
    id 588
    label "opowiadanie"
  ]
  node [
    id 589
    label "Byzantine_Empire"
  ]
  node [
    id 590
    label "metodologia"
  ]
  node [
    id 591
    label "datacja"
  ]
  node [
    id 592
    label "dendrochronologia"
  ]
  node [
    id 593
    label "kolejno&#347;&#263;"
  ]
  node [
    id 594
    label "bibliologia"
  ]
  node [
    id 595
    label "brachygrafia"
  ]
  node [
    id 596
    label "architektura"
  ]
  node [
    id 597
    label "nauka"
  ]
  node [
    id 598
    label "museum"
  ]
  node [
    id 599
    label "archiwoznawstwo"
  ]
  node [
    id 600
    label "historiography"
  ]
  node [
    id 601
    label "pi&#347;miennictwo"
  ]
  node [
    id 602
    label "archeologia"
  ]
  node [
    id 603
    label "plastyka"
  ]
  node [
    id 604
    label "oksza"
  ]
  node [
    id 605
    label "pas"
  ]
  node [
    id 606
    label "s&#322;up"
  ]
  node [
    id 607
    label "barwa_heraldyczna"
  ]
  node [
    id 608
    label "herb"
  ]
  node [
    id 609
    label "or&#281;&#380;"
  ]
  node [
    id 610
    label "&#347;redniowiecze"
  ]
  node [
    id 611
    label "descendencja"
  ]
  node [
    id 612
    label "drzewo_genealogiczne"
  ]
  node [
    id 613
    label "procedencja"
  ]
  node [
    id 614
    label "pochodzenie"
  ]
  node [
    id 615
    label "medal"
  ]
  node [
    id 616
    label "kolekcjonerstwo"
  ]
  node [
    id 617
    label "numismatics"
  ]
  node [
    id 618
    label "fraza"
  ]
  node [
    id 619
    label "temat"
  ]
  node [
    id 620
    label "przyczyna"
  ]
  node [
    id 621
    label "ozdoba"
  ]
  node [
    id 622
    label "umowa"
  ]
  node [
    id 623
    label "cover"
  ]
  node [
    id 624
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 625
    label "przeby&#263;"
  ]
  node [
    id 626
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 627
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 628
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 629
    label "przemierzy&#263;"
  ]
  node [
    id 630
    label "fly"
  ]
  node [
    id 631
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 632
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 633
    label "przesun&#261;&#263;"
  ]
  node [
    id 634
    label "activity"
  ]
  node [
    id 635
    label "bezproblemowy"
  ]
  node [
    id 636
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 637
    label "zbi&#243;r"
  ]
  node [
    id 638
    label "cz&#322;owiek"
  ]
  node [
    id 639
    label "osobowo&#347;&#263;"
  ]
  node [
    id 640
    label "psychika"
  ]
  node [
    id 641
    label "posta&#263;"
  ]
  node [
    id 642
    label "kompleksja"
  ]
  node [
    id 643
    label "fizjonomia"
  ]
  node [
    id 644
    label "przemkni&#281;cie"
  ]
  node [
    id 645
    label "zabrzmienie"
  ]
  node [
    id 646
    label "przebycie"
  ]
  node [
    id 647
    label "zdarzenie_si&#281;"
  ]
  node [
    id 648
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 649
    label "lock"
  ]
  node [
    id 650
    label "absolut"
  ]
  node [
    id 651
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 652
    label "integer"
  ]
  node [
    id 653
    label "liczba"
  ]
  node [
    id 654
    label "zlewanie_si&#281;"
  ]
  node [
    id 655
    label "uk&#322;ad"
  ]
  node [
    id 656
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 657
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 658
    label "pe&#322;ny"
  ]
  node [
    id 659
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 660
    label "olejek_eteryczny"
  ]
  node [
    id 661
    label "byt"
  ]
  node [
    id 662
    label "revolutionize"
  ]
  node [
    id 663
    label "porusza&#263;"
  ]
  node [
    id 664
    label "podnosi&#263;"
  ]
  node [
    id 665
    label "move"
  ]
  node [
    id 666
    label "go"
  ]
  node [
    id 667
    label "drive"
  ]
  node [
    id 668
    label "meet"
  ]
  node [
    id 669
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 670
    label "act"
  ]
  node [
    id 671
    label "powodowa&#263;"
  ]
  node [
    id 672
    label "wzbudza&#263;"
  ]
  node [
    id 673
    label "porobi&#263;"
  ]
  node [
    id 674
    label "gwiazda"
  ]
  node [
    id 675
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 676
    label "Arktur"
  ]
  node [
    id 677
    label "kszta&#322;t"
  ]
  node [
    id 678
    label "Gwiazda_Polarna"
  ]
  node [
    id 679
    label "agregatka"
  ]
  node [
    id 680
    label "gromada"
  ]
  node [
    id 681
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 682
    label "S&#322;o&#324;ce"
  ]
  node [
    id 683
    label "Nibiru"
  ]
  node [
    id 684
    label "konstelacja"
  ]
  node [
    id 685
    label "ornament"
  ]
  node [
    id 686
    label "delta_Scuti"
  ]
  node [
    id 687
    label "&#347;wiat&#322;o"
  ]
  node [
    id 688
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 689
    label "obiekt"
  ]
  node [
    id 690
    label "s&#322;awa"
  ]
  node [
    id 691
    label "promie&#324;"
  ]
  node [
    id 692
    label "star"
  ]
  node [
    id 693
    label "gwiazdosz"
  ]
  node [
    id 694
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 695
    label "asocjacja_gwiazd"
  ]
  node [
    id 696
    label "supergrupa"
  ]
  node [
    id 697
    label "asymilowanie_si&#281;"
  ]
  node [
    id 698
    label "Wsch&#243;d"
  ]
  node [
    id 699
    label "praca_rolnicza"
  ]
  node [
    id 700
    label "przejmowanie"
  ]
  node [
    id 701
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 702
    label "makrokosmos"
  ]
  node [
    id 703
    label "konwencja"
  ]
  node [
    id 704
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 705
    label "propriety"
  ]
  node [
    id 706
    label "przejmowa&#263;"
  ]
  node [
    id 707
    label "brzoskwiniarnia"
  ]
  node [
    id 708
    label "zwyczaj"
  ]
  node [
    id 709
    label "jako&#347;&#263;"
  ]
  node [
    id 710
    label "kuchnia"
  ]
  node [
    id 711
    label "tradycja"
  ]
  node [
    id 712
    label "populace"
  ]
  node [
    id 713
    label "hodowla"
  ]
  node [
    id 714
    label "religia"
  ]
  node [
    id 715
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 716
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 717
    label "przej&#281;cie"
  ]
  node [
    id 718
    label "przej&#261;&#263;"
  ]
  node [
    id 719
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 720
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 721
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 722
    label "warto&#347;&#263;"
  ]
  node [
    id 723
    label "quality"
  ]
  node [
    id 724
    label "co&#347;"
  ]
  node [
    id 725
    label "syf"
  ]
  node [
    id 726
    label "absolutorium"
  ]
  node [
    id 727
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 728
    label "dzia&#322;anie"
  ]
  node [
    id 729
    label "potrzymanie"
  ]
  node [
    id 730
    label "rolnictwo"
  ]
  node [
    id 731
    label "pod&#243;j"
  ]
  node [
    id 732
    label "filiacja"
  ]
  node [
    id 733
    label "licencjonowanie"
  ]
  node [
    id 734
    label "opasa&#263;"
  ]
  node [
    id 735
    label "ch&#243;w"
  ]
  node [
    id 736
    label "licencja"
  ]
  node [
    id 737
    label "sokolarnia"
  ]
  node [
    id 738
    label "potrzyma&#263;"
  ]
  node [
    id 739
    label "rozp&#322;&#243;d"
  ]
  node [
    id 740
    label "grupa_organizm&#243;w"
  ]
  node [
    id 741
    label "wypas"
  ]
  node [
    id 742
    label "wychowalnia"
  ]
  node [
    id 743
    label "pstr&#261;garnia"
  ]
  node [
    id 744
    label "krzy&#380;owanie"
  ]
  node [
    id 745
    label "licencjonowa&#263;"
  ]
  node [
    id 746
    label "odch&#243;w"
  ]
  node [
    id 747
    label "tucz"
  ]
  node [
    id 748
    label "ud&#243;j"
  ]
  node [
    id 749
    label "opasienie"
  ]
  node [
    id 750
    label "wych&#243;w"
  ]
  node [
    id 751
    label "obrz&#261;dek"
  ]
  node [
    id 752
    label "opasanie"
  ]
  node [
    id 753
    label "polish"
  ]
  node [
    id 754
    label "akwarium"
  ]
  node [
    id 755
    label "biotechnika"
  ]
  node [
    id 756
    label "charakterystyka"
  ]
  node [
    id 757
    label "m&#322;ot"
  ]
  node [
    id 758
    label "znak"
  ]
  node [
    id 759
    label "drzewo"
  ]
  node [
    id 760
    label "pr&#243;ba"
  ]
  node [
    id 761
    label "attribute"
  ]
  node [
    id 762
    label "marka"
  ]
  node [
    id 763
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 764
    label "line"
  ]
  node [
    id 765
    label "kanon"
  ]
  node [
    id 766
    label "zjazd"
  ]
  node [
    id 767
    label "biom"
  ]
  node [
    id 768
    label "szata_ro&#347;linna"
  ]
  node [
    id 769
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 770
    label "formacja_ro&#347;linna"
  ]
  node [
    id 771
    label "przyroda"
  ]
  node [
    id 772
    label "zielono&#347;&#263;"
  ]
  node [
    id 773
    label "pi&#281;tro"
  ]
  node [
    id 774
    label "plant"
  ]
  node [
    id 775
    label "ro&#347;lina"
  ]
  node [
    id 776
    label "geosystem"
  ]
  node [
    id 777
    label "pr&#243;bowanie"
  ]
  node [
    id 778
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 779
    label "realizacja"
  ]
  node [
    id 780
    label "didaskalia"
  ]
  node [
    id 781
    label "czyn"
  ]
  node [
    id 782
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 783
    label "environment"
  ]
  node [
    id 784
    label "head"
  ]
  node [
    id 785
    label "scenariusz"
  ]
  node [
    id 786
    label "jednostka"
  ]
  node [
    id 787
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 788
    label "fortel"
  ]
  node [
    id 789
    label "theatrical_performance"
  ]
  node [
    id 790
    label "ambala&#380;"
  ]
  node [
    id 791
    label "kobieta"
  ]
  node [
    id 792
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 793
    label "Faust"
  ]
  node [
    id 794
    label "scenografia"
  ]
  node [
    id 795
    label "ods&#322;ona"
  ]
  node [
    id 796
    label "turn"
  ]
  node [
    id 797
    label "przedstawi&#263;"
  ]
  node [
    id 798
    label "Apollo"
  ]
  node [
    id 799
    label "przedstawianie"
  ]
  node [
    id 800
    label "przedstawia&#263;"
  ]
  node [
    id 801
    label "towar"
  ]
  node [
    id 802
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 803
    label "ceremony"
  ]
  node [
    id 804
    label "kult"
  ]
  node [
    id 805
    label "wyznanie"
  ]
  node [
    id 806
    label "mitologia"
  ]
  node [
    id 807
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 808
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 809
    label "nawracanie_si&#281;"
  ]
  node [
    id 810
    label "duchowny"
  ]
  node [
    id 811
    label "rela"
  ]
  node [
    id 812
    label "kosmologia"
  ]
  node [
    id 813
    label "kosmogonia"
  ]
  node [
    id 814
    label "nawraca&#263;"
  ]
  node [
    id 815
    label "mistyka"
  ]
  node [
    id 816
    label "staro&#347;cina_weselna"
  ]
  node [
    id 817
    label "folklor"
  ]
  node [
    id 818
    label "objawienie"
  ]
  node [
    id 819
    label "tworzenie"
  ]
  node [
    id 820
    label "kreacja"
  ]
  node [
    id 821
    label "zaj&#281;cie"
  ]
  node [
    id 822
    label "instytucja"
  ]
  node [
    id 823
    label "tajniki"
  ]
  node [
    id 824
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 825
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 826
    label "jedzenie"
  ]
  node [
    id 827
    label "zaplecze"
  ]
  node [
    id 828
    label "pomieszczenie"
  ]
  node [
    id 829
    label "zlewozmywak"
  ]
  node [
    id 830
    label "gotowa&#263;"
  ]
  node [
    id 831
    label "ciemna_materia"
  ]
  node [
    id 832
    label "planeta"
  ]
  node [
    id 833
    label "mikrokosmos"
  ]
  node [
    id 834
    label "ekosfera"
  ]
  node [
    id 835
    label "przestrze&#324;"
  ]
  node [
    id 836
    label "czarna_dziura"
  ]
  node [
    id 837
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 838
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 839
    label "kosmos"
  ]
  node [
    id 840
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 841
    label "poprawno&#347;&#263;"
  ]
  node [
    id 842
    label "og&#322;ada"
  ]
  node [
    id 843
    label "service"
  ]
  node [
    id 844
    label "stosowno&#347;&#263;"
  ]
  node [
    id 845
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 846
    label "Ukraina"
  ]
  node [
    id 847
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 848
    label "blok_wschodni"
  ]
  node [
    id 849
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 850
    label "wsch&#243;d"
  ]
  node [
    id 851
    label "Europa_Wschodnia"
  ]
  node [
    id 852
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 853
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 854
    label "treat"
  ]
  node [
    id 855
    label "czerpa&#263;"
  ]
  node [
    id 856
    label "bra&#263;"
  ]
  node [
    id 857
    label "handle"
  ]
  node [
    id 858
    label "ogarnia&#263;"
  ]
  node [
    id 859
    label "bang"
  ]
  node [
    id 860
    label "wzi&#261;&#263;"
  ]
  node [
    id 861
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 862
    label "stimulate"
  ]
  node [
    id 863
    label "ogarn&#261;&#263;"
  ]
  node [
    id 864
    label "wzbudzi&#263;"
  ]
  node [
    id 865
    label "thrill"
  ]
  node [
    id 866
    label "czerpanie"
  ]
  node [
    id 867
    label "acquisition"
  ]
  node [
    id 868
    label "branie"
  ]
  node [
    id 869
    label "caparison"
  ]
  node [
    id 870
    label "movement"
  ]
  node [
    id 871
    label "wzbudzanie"
  ]
  node [
    id 872
    label "ogarnianie"
  ]
  node [
    id 873
    label "wra&#380;enie"
  ]
  node [
    id 874
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 875
    label "interception"
  ]
  node [
    id 876
    label "wzbudzenie"
  ]
  node [
    id 877
    label "emotion"
  ]
  node [
    id 878
    label "zaczerpni&#281;cie"
  ]
  node [
    id 879
    label "wzi&#281;cie"
  ]
  node [
    id 880
    label "object"
  ]
  node [
    id 881
    label "wpadni&#281;cie"
  ]
  node [
    id 882
    label "wpa&#347;&#263;"
  ]
  node [
    id 883
    label "wpadanie"
  ]
  node [
    id 884
    label "wpada&#263;"
  ]
  node [
    id 885
    label "uprawa"
  ]
  node [
    id 886
    label "nieneutralnie"
  ]
  node [
    id 887
    label "zauwa&#380;alnie"
  ]
  node [
    id 888
    label "wyra&#378;ny"
  ]
  node [
    id 889
    label "zdecydowanie"
  ]
  node [
    id 890
    label "distinctly"
  ]
  node [
    id 891
    label "stronniczo"
  ]
  node [
    id 892
    label "nieneutralny"
  ]
  node [
    id 893
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 894
    label "decyzja"
  ]
  node [
    id 895
    label "pewnie"
  ]
  node [
    id 896
    label "zdecydowany"
  ]
  node [
    id 897
    label "oddzia&#322;anie"
  ]
  node [
    id 898
    label "podj&#281;cie"
  ]
  node [
    id 899
    label "resoluteness"
  ]
  node [
    id 900
    label "judgment"
  ]
  node [
    id 901
    label "zauwa&#380;alny"
  ]
  node [
    id 902
    label "postrzegalnie"
  ]
  node [
    id 903
    label "nieoboj&#281;tny"
  ]
  node [
    id 904
    label "w&#380;dy"
  ]
  node [
    id 905
    label "przecina&#263;"
  ]
  node [
    id 906
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 907
    label "unboxing"
  ]
  node [
    id 908
    label "zaczyna&#263;"
  ]
  node [
    id 909
    label "train"
  ]
  node [
    id 910
    label "cia&#322;o"
  ]
  node [
    id 911
    label "uruchamia&#263;"
  ]
  node [
    id 912
    label "begin"
  ]
  node [
    id 913
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 914
    label "odejmowa&#263;"
  ]
  node [
    id 915
    label "bankrupt"
  ]
  node [
    id 916
    label "open"
  ]
  node [
    id 917
    label "set_about"
  ]
  node [
    id 918
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 919
    label "post&#281;powa&#263;"
  ]
  node [
    id 920
    label "kapita&#322;"
  ]
  node [
    id 921
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 922
    label "set"
  ]
  node [
    id 923
    label "os&#322;abia&#263;"
  ]
  node [
    id 924
    label "publicize"
  ]
  node [
    id 925
    label "psu&#263;"
  ]
  node [
    id 926
    label "rozmieszcza&#263;"
  ]
  node [
    id 927
    label "zmienia&#263;"
  ]
  node [
    id 928
    label "wygrywa&#263;"
  ]
  node [
    id 929
    label "dzieli&#263;"
  ]
  node [
    id 930
    label "oddala&#263;"
  ]
  node [
    id 931
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 932
    label "znaczy&#263;"
  ]
  node [
    id 933
    label "przerywa&#263;"
  ]
  node [
    id 934
    label "ucina&#263;"
  ]
  node [
    id 935
    label "przedziela&#263;"
  ]
  node [
    id 936
    label "blokowa&#263;"
  ]
  node [
    id 937
    label "przebija&#263;"
  ]
  node [
    id 938
    label "cut"
  ]
  node [
    id 939
    label "traversal"
  ]
  node [
    id 940
    label "narusza&#263;"
  ]
  node [
    id 941
    label "write_out"
  ]
  node [
    id 942
    label "przechodzi&#263;"
  ]
  node [
    id 943
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 944
    label "motywowa&#263;"
  ]
  node [
    id 945
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 946
    label "organizowa&#263;"
  ]
  node [
    id 947
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 948
    label "czyni&#263;"
  ]
  node [
    id 949
    label "give"
  ]
  node [
    id 950
    label "stylizowa&#263;"
  ]
  node [
    id 951
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 952
    label "falowa&#263;"
  ]
  node [
    id 953
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 954
    label "peddle"
  ]
  node [
    id 955
    label "wydala&#263;"
  ]
  node [
    id 956
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 957
    label "tentegowa&#263;"
  ]
  node [
    id 958
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 959
    label "urz&#261;dza&#263;"
  ]
  node [
    id 960
    label "oszukiwa&#263;"
  ]
  node [
    id 961
    label "ukazywa&#263;"
  ]
  node [
    id 962
    label "przerabia&#263;"
  ]
  node [
    id 963
    label "relacja"
  ]
  node [
    id 964
    label "ekshumowanie"
  ]
  node [
    id 965
    label "jednostka_organizacyjna"
  ]
  node [
    id 966
    label "p&#322;aszczyzna"
  ]
  node [
    id 967
    label "odwadnia&#263;"
  ]
  node [
    id 968
    label "zabalsamowanie"
  ]
  node [
    id 969
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 970
    label "odwodni&#263;"
  ]
  node [
    id 971
    label "sk&#243;ra"
  ]
  node [
    id 972
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 973
    label "staw"
  ]
  node [
    id 974
    label "ow&#322;osienie"
  ]
  node [
    id 975
    label "mi&#281;so"
  ]
  node [
    id 976
    label "zabalsamowa&#263;"
  ]
  node [
    id 977
    label "Izba_Konsyliarska"
  ]
  node [
    id 978
    label "unerwienie"
  ]
  node [
    id 979
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 980
    label "kremacja"
  ]
  node [
    id 981
    label "biorytm"
  ]
  node [
    id 982
    label "sekcja"
  ]
  node [
    id 983
    label "istota_&#380;ywa"
  ]
  node [
    id 984
    label "otworzy&#263;"
  ]
  node [
    id 985
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 986
    label "otworzenie"
  ]
  node [
    id 987
    label "materia"
  ]
  node [
    id 988
    label "pochowanie"
  ]
  node [
    id 989
    label "otwieranie"
  ]
  node [
    id 990
    label "szkielet"
  ]
  node [
    id 991
    label "tanatoplastyk"
  ]
  node [
    id 992
    label "odwadnianie"
  ]
  node [
    id 993
    label "Komitet_Region&#243;w"
  ]
  node [
    id 994
    label "odwodnienie"
  ]
  node [
    id 995
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 996
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 997
    label "pochowa&#263;"
  ]
  node [
    id 998
    label "tanatoplastyka"
  ]
  node [
    id 999
    label "balsamowa&#263;"
  ]
  node [
    id 1000
    label "nieumar&#322;y"
  ]
  node [
    id 1001
    label "temperatura"
  ]
  node [
    id 1002
    label "balsamowanie"
  ]
  node [
    id 1003
    label "ekshumowa&#263;"
  ]
  node [
    id 1004
    label "l&#281;d&#378;wie"
  ]
  node [
    id 1005
    label "cz&#322;onek"
  ]
  node [
    id 1006
    label "pogrzeb"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 339
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 238
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 294
  ]
  edge [
    source 13
    target 310
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 723
  ]
  edge [
    source 13
    target 724
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 725
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 727
  ]
  edge [
    source 13
    target 728
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 13
    target 343
  ]
  edge [
    source 13
    target 344
  ]
  edge [
    source 13
    target 345
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 248
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 729
  ]
  edge [
    source 13
    target 730
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 13
    target 733
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 735
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 737
  ]
  edge [
    source 13
    target 738
  ]
  edge [
    source 13
    target 739
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 13
    target 742
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 313
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 753
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 756
  ]
  edge [
    source 13
    target 757
  ]
  edge [
    source 13
    target 758
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 760
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 762
  ]
  edge [
    source 13
    target 763
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 764
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 325
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 303
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 291
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 234
  ]
  edge [
    source 13
    target 235
  ]
  edge [
    source 13
    target 236
  ]
  edge [
    source 13
    target 237
  ]
  edge [
    source 13
    target 239
  ]
  edge [
    source 13
    target 240
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 241
  ]
  edge [
    source 13
    target 242
  ]
  edge [
    source 13
    target 243
  ]
  edge [
    source 13
    target 244
  ]
  edge [
    source 13
    target 245
  ]
  edge [
    source 13
    target 246
  ]
  edge [
    source 13
    target 247
  ]
  edge [
    source 13
    target 249
  ]
  edge [
    source 13
    target 250
  ]
  edge [
    source 13
    target 251
  ]
  edge [
    source 13
    target 252
  ]
  edge [
    source 13
    target 253
  ]
  edge [
    source 13
    target 254
  ]
  edge [
    source 13
    target 255
  ]
  edge [
    source 13
    target 256
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 887
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 890
  ]
  edge [
    source 15
    target 891
  ]
  edge [
    source 15
    target 892
  ]
  edge [
    source 15
    target 893
  ]
  edge [
    source 15
    target 894
  ]
  edge [
    source 15
    target 895
  ]
  edge [
    source 15
    target 896
  ]
  edge [
    source 15
    target 897
  ]
  edge [
    source 15
    target 898
  ]
  edge [
    source 15
    target 381
  ]
  edge [
    source 15
    target 899
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 901
  ]
  edge [
    source 15
    target 902
  ]
  edge [
    source 15
    target 903
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 403
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 671
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 392
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 420
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 432
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 441
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 670
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 93
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 79
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 637
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 555
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 18
    target 1002
  ]
  edge [
    source 18
    target 1003
  ]
  edge [
    source 18
    target 1004
  ]
  edge [
    source 18
    target 561
  ]
  edge [
    source 18
    target 1005
  ]
  edge [
    source 18
    target 1006
  ]
]
