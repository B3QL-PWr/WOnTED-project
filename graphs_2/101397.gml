graph [
  node [
    id 0
    label "partia"
    origin "text"
  ]
  node [
    id 1
    label "towaroznawstwo"
    origin "text"
  ]
  node [
    id 2
    label "Bund"
  ]
  node [
    id 3
    label "PPR"
  ]
  node [
    id 4
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 5
    label "wybranek"
  ]
  node [
    id 6
    label "Jakobici"
  ]
  node [
    id 7
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 8
    label "SLD"
  ]
  node [
    id 9
    label "Razem"
  ]
  node [
    id 10
    label "PiS"
  ]
  node [
    id 11
    label "package"
  ]
  node [
    id 12
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 13
    label "Kuomintang"
  ]
  node [
    id 14
    label "ZSL"
  ]
  node [
    id 15
    label "organizacja"
  ]
  node [
    id 16
    label "AWS"
  ]
  node [
    id 17
    label "gra"
  ]
  node [
    id 18
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 19
    label "game"
  ]
  node [
    id 20
    label "grupa"
  ]
  node [
    id 21
    label "blok"
  ]
  node [
    id 22
    label "materia&#322;"
  ]
  node [
    id 23
    label "PO"
  ]
  node [
    id 24
    label "si&#322;a"
  ]
  node [
    id 25
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 26
    label "niedoczas"
  ]
  node [
    id 27
    label "Federali&#347;ci"
  ]
  node [
    id 28
    label "PSL"
  ]
  node [
    id 29
    label "Wigowie"
  ]
  node [
    id 30
    label "ZChN"
  ]
  node [
    id 31
    label "egzekutywa"
  ]
  node [
    id 32
    label "aktyw"
  ]
  node [
    id 33
    label "wybranka"
  ]
  node [
    id 34
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 35
    label "unit"
  ]
  node [
    id 36
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 37
    label "Rzym_Zachodni"
  ]
  node [
    id 38
    label "whole"
  ]
  node [
    id 39
    label "ilo&#347;&#263;"
  ]
  node [
    id 40
    label "element"
  ]
  node [
    id 41
    label "Rzym_Wschodni"
  ]
  node [
    id 42
    label "urz&#261;dzenie"
  ]
  node [
    id 43
    label "odm&#322;adzanie"
  ]
  node [
    id 44
    label "liga"
  ]
  node [
    id 45
    label "jednostka_systematyczna"
  ]
  node [
    id 46
    label "asymilowanie"
  ]
  node [
    id 47
    label "gromada"
  ]
  node [
    id 48
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 49
    label "asymilowa&#263;"
  ]
  node [
    id 50
    label "egzemplarz"
  ]
  node [
    id 51
    label "Entuzjastki"
  ]
  node [
    id 52
    label "zbi&#243;r"
  ]
  node [
    id 53
    label "kompozycja"
  ]
  node [
    id 54
    label "Terranie"
  ]
  node [
    id 55
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 56
    label "category"
  ]
  node [
    id 57
    label "pakiet_klimatyczny"
  ]
  node [
    id 58
    label "oddzia&#322;"
  ]
  node [
    id 59
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 60
    label "cz&#261;steczka"
  ]
  node [
    id 61
    label "stage_set"
  ]
  node [
    id 62
    label "type"
  ]
  node [
    id 63
    label "specgrupa"
  ]
  node [
    id 64
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 65
    label "&#346;wietliki"
  ]
  node [
    id 66
    label "odm&#322;odzenie"
  ]
  node [
    id 67
    label "Eurogrupa"
  ]
  node [
    id 68
    label "odm&#322;adza&#263;"
  ]
  node [
    id 69
    label "formacja_geologiczna"
  ]
  node [
    id 70
    label "harcerze_starsi"
  ]
  node [
    id 71
    label "podmiot"
  ]
  node [
    id 72
    label "jednostka_organizacyjna"
  ]
  node [
    id 73
    label "struktura"
  ]
  node [
    id 74
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 75
    label "TOPR"
  ]
  node [
    id 76
    label "endecki"
  ]
  node [
    id 77
    label "zesp&#243;&#322;"
  ]
  node [
    id 78
    label "przedstawicielstwo"
  ]
  node [
    id 79
    label "od&#322;am"
  ]
  node [
    id 80
    label "Cepelia"
  ]
  node [
    id 81
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 82
    label "ZBoWiD"
  ]
  node [
    id 83
    label "organization"
  ]
  node [
    id 84
    label "centrala"
  ]
  node [
    id 85
    label "GOPR"
  ]
  node [
    id 86
    label "ZOMO"
  ]
  node [
    id 87
    label "ZMP"
  ]
  node [
    id 88
    label "komitet_koordynacyjny"
  ]
  node [
    id 89
    label "przybud&#243;wka"
  ]
  node [
    id 90
    label "boj&#243;wka"
  ]
  node [
    id 91
    label "energia"
  ]
  node [
    id 92
    label "parametr"
  ]
  node [
    id 93
    label "rozwi&#261;zanie"
  ]
  node [
    id 94
    label "wojsko"
  ]
  node [
    id 95
    label "cecha"
  ]
  node [
    id 96
    label "wuchta"
  ]
  node [
    id 97
    label "zaleta"
  ]
  node [
    id 98
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 99
    label "moment_si&#322;y"
  ]
  node [
    id 100
    label "mn&#243;stwo"
  ]
  node [
    id 101
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 102
    label "zjawisko"
  ]
  node [
    id 103
    label "zdolno&#347;&#263;"
  ]
  node [
    id 104
    label "capacity"
  ]
  node [
    id 105
    label "magnitude"
  ]
  node [
    id 106
    label "potencja"
  ]
  node [
    id 107
    label "przemoc"
  ]
  node [
    id 108
    label "zmienno&#347;&#263;"
  ]
  node [
    id 109
    label "play"
  ]
  node [
    id 110
    label "rozgrywka"
  ]
  node [
    id 111
    label "apparent_motion"
  ]
  node [
    id 112
    label "wydarzenie"
  ]
  node [
    id 113
    label "contest"
  ]
  node [
    id 114
    label "akcja"
  ]
  node [
    id 115
    label "komplet"
  ]
  node [
    id 116
    label "zabawa"
  ]
  node [
    id 117
    label "zasada"
  ]
  node [
    id 118
    label "rywalizacja"
  ]
  node [
    id 119
    label "zbijany"
  ]
  node [
    id 120
    label "post&#281;powanie"
  ]
  node [
    id 121
    label "odg&#322;os"
  ]
  node [
    id 122
    label "Pok&#233;mon"
  ]
  node [
    id 123
    label "czynno&#347;&#263;"
  ]
  node [
    id 124
    label "synteza"
  ]
  node [
    id 125
    label "odtworzenie"
  ]
  node [
    id 126
    label "rekwizyt_do_gry"
  ]
  node [
    id 127
    label "cz&#322;owiek"
  ]
  node [
    id 128
    label "materia"
  ]
  node [
    id 129
    label "nawil&#380;arka"
  ]
  node [
    id 130
    label "bielarnia"
  ]
  node [
    id 131
    label "dyspozycja"
  ]
  node [
    id 132
    label "dane"
  ]
  node [
    id 133
    label "tworzywo"
  ]
  node [
    id 134
    label "substancja"
  ]
  node [
    id 135
    label "kandydat"
  ]
  node [
    id 136
    label "archiwum"
  ]
  node [
    id 137
    label "krajka"
  ]
  node [
    id 138
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 139
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 140
    label "krajalno&#347;&#263;"
  ]
  node [
    id 141
    label "kobieta"
  ]
  node [
    id 142
    label "M&#322;odzie&#380;_Wszechpolska"
  ]
  node [
    id 143
    label "kadra"
  ]
  node [
    id 144
    label "luzacki"
  ]
  node [
    id 145
    label "organ"
  ]
  node [
    id 146
    label "obrady"
  ]
  node [
    id 147
    label "executive"
  ]
  node [
    id 148
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 149
    label "federacja"
  ]
  node [
    id 150
    label "w&#322;adza"
  ]
  node [
    id 151
    label "op&#243;&#378;nienie"
  ]
  node [
    id 152
    label "szachy"
  ]
  node [
    id 153
    label "bajt"
  ]
  node [
    id 154
    label "bloking"
  ]
  node [
    id 155
    label "j&#261;kanie"
  ]
  node [
    id 156
    label "przeszkoda"
  ]
  node [
    id 157
    label "blokada"
  ]
  node [
    id 158
    label "bry&#322;a"
  ]
  node [
    id 159
    label "dzia&#322;"
  ]
  node [
    id 160
    label "kontynent"
  ]
  node [
    id 161
    label "nastawnia"
  ]
  node [
    id 162
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 163
    label "blockage"
  ]
  node [
    id 164
    label "block"
  ]
  node [
    id 165
    label "budynek"
  ]
  node [
    id 166
    label "start"
  ]
  node [
    id 167
    label "skorupa_ziemska"
  ]
  node [
    id 168
    label "program"
  ]
  node [
    id 169
    label "zeszyt"
  ]
  node [
    id 170
    label "blokowisko"
  ]
  node [
    id 171
    label "artyku&#322;"
  ]
  node [
    id 172
    label "barak"
  ]
  node [
    id 173
    label "stok_kontynentalny"
  ]
  node [
    id 174
    label "square"
  ]
  node [
    id 175
    label "siatk&#243;wka"
  ]
  node [
    id 176
    label "kr&#261;g"
  ]
  node [
    id 177
    label "ram&#243;wka"
  ]
  node [
    id 178
    label "zamek"
  ]
  node [
    id 179
    label "obrona"
  ]
  node [
    id 180
    label "ok&#322;adka"
  ]
  node [
    id 181
    label "bie&#380;nia"
  ]
  node [
    id 182
    label "referat"
  ]
  node [
    id 183
    label "dom_wielorodzinny"
  ]
  node [
    id 184
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 185
    label "edukacja_dla_bezpiecze&#324;stwa"
  ]
  node [
    id 186
    label "przedmiot"
  ]
  node [
    id 187
    label "stan_cywilny"
  ]
  node [
    id 188
    label "para"
  ]
  node [
    id 189
    label "matrymonialny"
  ]
  node [
    id 190
    label "lewirat"
  ]
  node [
    id 191
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 192
    label "sakrament"
  ]
  node [
    id 193
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 194
    label "zwi&#261;zek"
  ]
  node [
    id 195
    label "jedyna"
  ]
  node [
    id 196
    label "mi&#322;a"
  ]
  node [
    id 197
    label "mi&#322;y"
  ]
  node [
    id 198
    label "jedyny"
  ]
  node [
    id 199
    label "nauka_ekonomiczna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 1
    target 199
  ]
]
