graph [
  node [
    id 0
    label "drogi"
    origin "text"
  ]
  node [
    id 1
    label "uk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wafelek"
    origin "text"
  ]
  node [
    id 3
    label "musza"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "chowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "las"
    origin "text"
  ]
  node [
    id 7
    label "drogo"
  ]
  node [
    id 8
    label "mi&#322;y"
  ]
  node [
    id 9
    label "cz&#322;owiek"
  ]
  node [
    id 10
    label "bliski"
  ]
  node [
    id 11
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 12
    label "przyjaciel"
  ]
  node [
    id 13
    label "warto&#347;ciowy"
  ]
  node [
    id 14
    label "blisko"
  ]
  node [
    id 15
    label "znajomy"
  ]
  node [
    id 16
    label "zwi&#261;zany"
  ]
  node [
    id 17
    label "przesz&#322;y"
  ]
  node [
    id 18
    label "silny"
  ]
  node [
    id 19
    label "zbli&#380;enie"
  ]
  node [
    id 20
    label "kr&#243;tki"
  ]
  node [
    id 21
    label "oddalony"
  ]
  node [
    id 22
    label "dok&#322;adny"
  ]
  node [
    id 23
    label "nieodleg&#322;y"
  ]
  node [
    id 24
    label "przysz&#322;y"
  ]
  node [
    id 25
    label "gotowy"
  ]
  node [
    id 26
    label "ma&#322;y"
  ]
  node [
    id 27
    label "ludzko&#347;&#263;"
  ]
  node [
    id 28
    label "asymilowanie"
  ]
  node [
    id 29
    label "wapniak"
  ]
  node [
    id 30
    label "asymilowa&#263;"
  ]
  node [
    id 31
    label "os&#322;abia&#263;"
  ]
  node [
    id 32
    label "posta&#263;"
  ]
  node [
    id 33
    label "hominid"
  ]
  node [
    id 34
    label "podw&#322;adny"
  ]
  node [
    id 35
    label "os&#322;abianie"
  ]
  node [
    id 36
    label "g&#322;owa"
  ]
  node [
    id 37
    label "figura"
  ]
  node [
    id 38
    label "portrecista"
  ]
  node [
    id 39
    label "dwun&#243;g"
  ]
  node [
    id 40
    label "profanum"
  ]
  node [
    id 41
    label "mikrokosmos"
  ]
  node [
    id 42
    label "nasada"
  ]
  node [
    id 43
    label "duch"
  ]
  node [
    id 44
    label "antropochoria"
  ]
  node [
    id 45
    label "osoba"
  ]
  node [
    id 46
    label "wz&#243;r"
  ]
  node [
    id 47
    label "senior"
  ]
  node [
    id 48
    label "oddzia&#322;ywanie"
  ]
  node [
    id 49
    label "Adam"
  ]
  node [
    id 50
    label "homo_sapiens"
  ]
  node [
    id 51
    label "polifag"
  ]
  node [
    id 52
    label "kochanek"
  ]
  node [
    id 53
    label "kum"
  ]
  node [
    id 54
    label "amikus"
  ]
  node [
    id 55
    label "zaprzyja&#378;nianie_si&#281;"
  ]
  node [
    id 56
    label "pobratymiec"
  ]
  node [
    id 57
    label "zaprzyja&#378;nienie_si&#281;"
  ]
  node [
    id 58
    label "sympatyk"
  ]
  node [
    id 59
    label "bratnia_dusza"
  ]
  node [
    id 60
    label "przyja&#378;nienie_si&#281;"
  ]
  node [
    id 61
    label "droga"
  ]
  node [
    id 62
    label "ukochanie"
  ]
  node [
    id 63
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 64
    label "feblik"
  ]
  node [
    id 65
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 66
    label "podnieci&#263;"
  ]
  node [
    id 67
    label "numer"
  ]
  node [
    id 68
    label "po&#380;ycie"
  ]
  node [
    id 69
    label "tendency"
  ]
  node [
    id 70
    label "podniecenie"
  ]
  node [
    id 71
    label "afekt"
  ]
  node [
    id 72
    label "zakochanie"
  ]
  node [
    id 73
    label "zajawka"
  ]
  node [
    id 74
    label "seks"
  ]
  node [
    id 75
    label "podniecanie"
  ]
  node [
    id 76
    label "imisja"
  ]
  node [
    id 77
    label "love"
  ]
  node [
    id 78
    label "rozmna&#380;anie"
  ]
  node [
    id 79
    label "ruch_frykcyjny"
  ]
  node [
    id 80
    label "na_pieska"
  ]
  node [
    id 81
    label "serce"
  ]
  node [
    id 82
    label "pozycja_misjonarska"
  ]
  node [
    id 83
    label "wi&#281;&#378;"
  ]
  node [
    id 84
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 85
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 86
    label "z&#322;&#261;czenie"
  ]
  node [
    id 87
    label "czynno&#347;&#263;"
  ]
  node [
    id 88
    label "gra_wst&#281;pna"
  ]
  node [
    id 89
    label "erotyka"
  ]
  node [
    id 90
    label "emocja"
  ]
  node [
    id 91
    label "baraszki"
  ]
  node [
    id 92
    label "po&#380;&#261;danie"
  ]
  node [
    id 93
    label "wzw&#243;d"
  ]
  node [
    id 94
    label "podnieca&#263;"
  ]
  node [
    id 95
    label "sk&#322;onny"
  ]
  node [
    id 96
    label "wybranek"
  ]
  node [
    id 97
    label "umi&#322;owany"
  ]
  node [
    id 98
    label "przyjemnie"
  ]
  node [
    id 99
    label "mi&#322;o"
  ]
  node [
    id 100
    label "kochanie"
  ]
  node [
    id 101
    label "dyplomata"
  ]
  node [
    id 102
    label "dobry"
  ]
  node [
    id 103
    label "rewaluowanie"
  ]
  node [
    id 104
    label "warto&#347;ciowo"
  ]
  node [
    id 105
    label "u&#380;yteczny"
  ]
  node [
    id 106
    label "zrewaluowanie"
  ]
  node [
    id 107
    label "dro&#380;ej"
  ]
  node [
    id 108
    label "p&#322;atnie"
  ]
  node [
    id 109
    label "dispose"
  ]
  node [
    id 110
    label "uczy&#263;"
  ]
  node [
    id 111
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 112
    label "kszta&#322;ci&#263;"
  ]
  node [
    id 113
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 114
    label "porz&#261;dkowa&#263;"
  ]
  node [
    id 115
    label "przygotowywa&#263;"
  ]
  node [
    id 116
    label "zbiera&#263;"
  ]
  node [
    id 117
    label "digest"
  ]
  node [
    id 118
    label "umieszcza&#263;"
  ]
  node [
    id 119
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 120
    label "tworzy&#263;"
  ]
  node [
    id 121
    label "treser"
  ]
  node [
    id 122
    label "raise"
  ]
  node [
    id 123
    label "przejmowa&#263;"
  ]
  node [
    id 124
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 125
    label "robi&#263;"
  ]
  node [
    id 126
    label "gromadzi&#263;"
  ]
  node [
    id 127
    label "mie&#263;_miejsce"
  ]
  node [
    id 128
    label "bra&#263;"
  ]
  node [
    id 129
    label "pozyskiwa&#263;"
  ]
  node [
    id 130
    label "poci&#261;ga&#263;"
  ]
  node [
    id 131
    label "wzbiera&#263;"
  ]
  node [
    id 132
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 133
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 134
    label "meet"
  ]
  node [
    id 135
    label "dostawa&#263;"
  ]
  node [
    id 136
    label "powodowa&#263;"
  ]
  node [
    id 137
    label "consolidate"
  ]
  node [
    id 138
    label "congregate"
  ]
  node [
    id 139
    label "pozostawia&#263;"
  ]
  node [
    id 140
    label "zaczyna&#263;"
  ]
  node [
    id 141
    label "psu&#263;"
  ]
  node [
    id 142
    label "wzbudza&#263;"
  ]
  node [
    id 143
    label "wk&#322;ada&#263;"
  ]
  node [
    id 144
    label "go"
  ]
  node [
    id 145
    label "inspirowa&#263;"
  ]
  node [
    id 146
    label "wpaja&#263;"
  ]
  node [
    id 147
    label "znak"
  ]
  node [
    id 148
    label "seat"
  ]
  node [
    id 149
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 150
    label "wygrywa&#263;"
  ]
  node [
    id 151
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 152
    label "go&#347;ci&#263;"
  ]
  node [
    id 153
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 154
    label "set"
  ]
  node [
    id 155
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 156
    label "pour"
  ]
  node [
    id 157
    label "elaborate"
  ]
  node [
    id 158
    label "train"
  ]
  node [
    id 159
    label "pokrywa&#263;"
  ]
  node [
    id 160
    label "zmienia&#263;"
  ]
  node [
    id 161
    label "sposobi&#263;"
  ]
  node [
    id 162
    label "wytwarza&#263;"
  ]
  node [
    id 163
    label "usposabia&#263;"
  ]
  node [
    id 164
    label "arrange"
  ]
  node [
    id 165
    label "szkoli&#263;"
  ]
  node [
    id 166
    label "wykonywa&#263;"
  ]
  node [
    id 167
    label "pryczy&#263;"
  ]
  node [
    id 168
    label "organizowa&#263;"
  ]
  node [
    id 169
    label "ustawia&#263;"
  ]
  node [
    id 170
    label "dba&#263;"
  ]
  node [
    id 171
    label "order"
  ]
  node [
    id 172
    label "rozwija&#263;"
  ]
  node [
    id 173
    label "zapoznawa&#263;"
  ]
  node [
    id 174
    label "pracowa&#263;"
  ]
  node [
    id 175
    label "teach"
  ]
  node [
    id 176
    label "edukowa&#263;"
  ]
  node [
    id 177
    label "wysy&#322;a&#263;"
  ]
  node [
    id 178
    label "wychowywa&#263;"
  ]
  node [
    id 179
    label "doskonali&#263;"
  ]
  node [
    id 180
    label "pope&#322;nia&#263;"
  ]
  node [
    id 181
    label "get"
  ]
  node [
    id 182
    label "consist"
  ]
  node [
    id 183
    label "stanowi&#263;"
  ]
  node [
    id 184
    label "plasowa&#263;"
  ]
  node [
    id 185
    label "umie&#347;ci&#263;"
  ]
  node [
    id 186
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 187
    label "pomieszcza&#263;"
  ]
  node [
    id 188
    label "accommodate"
  ]
  node [
    id 189
    label "venture"
  ]
  node [
    id 190
    label "wpiernicza&#263;"
  ]
  node [
    id 191
    label "okre&#347;la&#263;"
  ]
  node [
    id 192
    label "dostosowywa&#263;"
  ]
  node [
    id 193
    label "nadawa&#263;"
  ]
  node [
    id 194
    label "shape"
  ]
  node [
    id 195
    label "trener"
  ]
  node [
    id 196
    label "zwierz&#281;"
  ]
  node [
    id 197
    label "baton"
  ]
  node [
    id 198
    label "wypiek"
  ]
  node [
    id 199
    label "ciastko"
  ]
  node [
    id 200
    label "wafel"
  ]
  node [
    id 201
    label "pa&#322;ka"
  ]
  node [
    id 202
    label "bu&#322;ka_paryska"
  ]
  node [
    id 203
    label "nadzienie"
  ]
  node [
    id 204
    label "barroom"
  ]
  node [
    id 205
    label "s&#322;odko&#347;&#263;"
  ]
  node [
    id 206
    label "wiek"
  ]
  node [
    id 207
    label "pi&#281;tro"
  ]
  node [
    id 208
    label "jura_&#347;rodkowa"
  ]
  node [
    id 209
    label "wyr&#243;b_cukierniczy"
  ]
  node [
    id 210
    label "&#322;ako&#263;"
  ]
  node [
    id 211
    label "baking"
  ]
  node [
    id 212
    label "upiek"
  ]
  node [
    id 213
    label "jedzenie"
  ]
  node [
    id 214
    label "produkt"
  ]
  node [
    id 215
    label "pieczenie"
  ]
  node [
    id 216
    label "produkcja"
  ]
  node [
    id 217
    label "p&#322;at"
  ]
  node [
    id 218
    label "tagestolog"
  ]
  node [
    id 219
    label "tagestologia"
  ]
  node [
    id 220
    label "lizus"
  ]
  node [
    id 221
    label "podstawka"
  ]
  node [
    id 222
    label "report"
  ]
  node [
    id 223
    label "hide"
  ]
  node [
    id 224
    label "znosi&#263;"
  ]
  node [
    id 225
    label "czu&#263;"
  ]
  node [
    id 226
    label "przetrzymywa&#263;"
  ]
  node [
    id 227
    label "hodowa&#263;"
  ]
  node [
    id 228
    label "meliniarz"
  ]
  node [
    id 229
    label "ukrywa&#263;"
  ]
  node [
    id 230
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 231
    label "continue"
  ]
  node [
    id 232
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 233
    label "pozostawa&#263;"
  ]
  node [
    id 234
    label "stay"
  ]
  node [
    id 235
    label "trzyma&#263;"
  ]
  node [
    id 236
    label "zachowywa&#263;"
  ]
  node [
    id 237
    label "usuwa&#263;"
  ]
  node [
    id 238
    label "porywa&#263;"
  ]
  node [
    id 239
    label "sk&#322;ada&#263;"
  ]
  node [
    id 240
    label "ranny"
  ]
  node [
    id 241
    label "behave"
  ]
  node [
    id 242
    label "carry"
  ]
  node [
    id 243
    label "represent"
  ]
  node [
    id 244
    label "podrze&#263;"
  ]
  node [
    id 245
    label "przenosi&#263;"
  ]
  node [
    id 246
    label "str&#243;j"
  ]
  node [
    id 247
    label "wytrzymywa&#263;"
  ]
  node [
    id 248
    label "seclude"
  ]
  node [
    id 249
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 250
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 251
    label "zu&#380;y&#263;"
  ]
  node [
    id 252
    label "niszczy&#263;"
  ]
  node [
    id 253
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 254
    label "tolerowa&#263;"
  ]
  node [
    id 255
    label "przekazywa&#263;"
  ]
  node [
    id 256
    label "ubiera&#263;"
  ]
  node [
    id 257
    label "odziewa&#263;"
  ]
  node [
    id 258
    label "obleka&#263;"
  ]
  node [
    id 259
    label "nosi&#263;"
  ]
  node [
    id 260
    label "introduce"
  ]
  node [
    id 261
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 262
    label "place"
  ]
  node [
    id 263
    label "zapuszcza&#263;"
  ]
  node [
    id 264
    label "postrzega&#263;"
  ]
  node [
    id 265
    label "przewidywa&#263;"
  ]
  node [
    id 266
    label "by&#263;"
  ]
  node [
    id 267
    label "smell"
  ]
  node [
    id 268
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 269
    label "uczuwa&#263;"
  ]
  node [
    id 270
    label "spirit"
  ]
  node [
    id 271
    label "doznawa&#263;"
  ]
  node [
    id 272
    label "anticipate"
  ]
  node [
    id 273
    label "zawiera&#263;"
  ]
  node [
    id 274
    label "suppress"
  ]
  node [
    id 275
    label "cache"
  ]
  node [
    id 276
    label "cover"
  ]
  node [
    id 277
    label "umowa"
  ]
  node [
    id 278
    label "przest&#281;pca"
  ]
  node [
    id 279
    label "bimbrownik"
  ]
  node [
    id 280
    label "p&#281;dzi&#263;"
  ]
  node [
    id 281
    label "dno_lasu"
  ]
  node [
    id 282
    label "podszyt"
  ]
  node [
    id 283
    label "nadle&#347;nictwo"
  ]
  node [
    id 284
    label "teren_le&#347;ny"
  ]
  node [
    id 285
    label "zalesienie"
  ]
  node [
    id 286
    label "karczowa&#263;"
  ]
  node [
    id 287
    label "mn&#243;stwo"
  ]
  node [
    id 288
    label "wykarczowa&#263;"
  ]
  node [
    id 289
    label "rewir"
  ]
  node [
    id 290
    label "karczowanie"
  ]
  node [
    id 291
    label "obr&#281;b"
  ]
  node [
    id 292
    label "chody"
  ]
  node [
    id 293
    label "wykarczowanie"
  ]
  node [
    id 294
    label "wiatro&#322;om"
  ]
  node [
    id 295
    label "teren"
  ]
  node [
    id 296
    label "podrost"
  ]
  node [
    id 297
    label "&#347;ci&#243;&#322;ka"
  ]
  node [
    id 298
    label "le&#347;nictwo"
  ]
  node [
    id 299
    label "driada"
  ]
  node [
    id 300
    label "formacja_ro&#347;linna"
  ]
  node [
    id 301
    label "runo"
  ]
  node [
    id 302
    label "ilo&#347;&#263;"
  ]
  node [
    id 303
    label "enormousness"
  ]
  node [
    id 304
    label "wymiar"
  ]
  node [
    id 305
    label "zakres"
  ]
  node [
    id 306
    label "kontekst"
  ]
  node [
    id 307
    label "miejsce_pracy"
  ]
  node [
    id 308
    label "nation"
  ]
  node [
    id 309
    label "krajobraz"
  ]
  node [
    id 310
    label "obszar"
  ]
  node [
    id 311
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 312
    label "przyroda"
  ]
  node [
    id 313
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 314
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 315
    label "w&#322;adza"
  ]
  node [
    id 316
    label "samosiejka"
  ]
  node [
    id 317
    label "dar&#324;"
  ]
  node [
    id 318
    label "warstwa"
  ]
  node [
    id 319
    label "sier&#347;&#263;"
  ]
  node [
    id 320
    label "s&#322;oma"
  ]
  node [
    id 321
    label "afforestation"
  ]
  node [
    id 322
    label "zadrzewienie"
  ]
  node [
    id 323
    label "biologia"
  ]
  node [
    id 324
    label "szk&#243;&#322;karstwo"
  ]
  node [
    id 325
    label "jednostka_administracyjna"
  ]
  node [
    id 326
    label "nauka_le&#347;na"
  ]
  node [
    id 327
    label "usuni&#281;cie"
  ]
  node [
    id 328
    label "krzew"
  ]
  node [
    id 329
    label "drzewo"
  ]
  node [
    id 330
    label "ablation"
  ]
  node [
    id 331
    label "usuwanie"
  ]
  node [
    id 332
    label "usun&#261;&#263;"
  ]
  node [
    id 333
    label "nimfa"
  ]
  node [
    id 334
    label "nora"
  ]
  node [
    id 335
    label "pies_my&#347;liwski"
  ]
  node [
    id 336
    label "miejsce"
  ]
  node [
    id 337
    label "trasa"
  ]
  node [
    id 338
    label "doj&#347;cie"
  ]
  node [
    id 339
    label "p&#243;&#322;noc"
  ]
  node [
    id 340
    label "Kosowo"
  ]
  node [
    id 341
    label "&#347;cieg"
  ]
  node [
    id 342
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 343
    label "Zab&#322;ocie"
  ]
  node [
    id 344
    label "sector"
  ]
  node [
    id 345
    label "zach&#243;d"
  ]
  node [
    id 346
    label "po&#322;udnie"
  ]
  node [
    id 347
    label "Pow&#261;zki"
  ]
  node [
    id 348
    label "circle"
  ]
  node [
    id 349
    label "Piotrowo"
  ]
  node [
    id 350
    label "Olszanica"
  ]
  node [
    id 351
    label "Ruda_Pabianicka"
  ]
  node [
    id 352
    label "holarktyka"
  ]
  node [
    id 353
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 354
    label "Ludwin&#243;w"
  ]
  node [
    id 355
    label "Arktyka"
  ]
  node [
    id 356
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 357
    label "Zabu&#380;e"
  ]
  node [
    id 358
    label "antroposfera"
  ]
  node [
    id 359
    label "Neogea"
  ]
  node [
    id 360
    label "Syberia_Zachodnia"
  ]
  node [
    id 361
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 362
    label "pas_planetoid"
  ]
  node [
    id 363
    label "Syberia_Wschodnia"
  ]
  node [
    id 364
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 365
    label "Antarktyka"
  ]
  node [
    id 366
    label "Rakowice"
  ]
  node [
    id 367
    label "akrecja"
  ]
  node [
    id 368
    label "&#321;&#281;g"
  ]
  node [
    id 369
    label "Kresy_Zachodnie"
  ]
  node [
    id 370
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 371
    label "przestrze&#324;"
  ]
  node [
    id 372
    label "wyko&#324;czenie"
  ]
  node [
    id 373
    label "wsch&#243;d"
  ]
  node [
    id 374
    label "Notogea"
  ]
  node [
    id 375
    label "rejon"
  ]
  node [
    id 376
    label "okr&#281;g"
  ]
  node [
    id 377
    label "komisariat"
  ]
  node [
    id 378
    label "szpital"
  ]
  node [
    id 379
    label "authorize"
  ]
  node [
    id 380
    label "urz&#261;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
]
