graph [
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 2
    label "bogactwo"
    origin "text"
  ]
  node [
    id 3
    label "pieniadze"
    origin "text"
  ]
  node [
    id 4
    label "memy"
    origin "text"
  ]
  node [
    id 5
    label "wysyp"
  ]
  node [
    id 6
    label "fullness"
  ]
  node [
    id 7
    label "podostatek"
  ]
  node [
    id 8
    label "mienie"
  ]
  node [
    id 9
    label "ilo&#347;&#263;"
  ]
  node [
    id 10
    label "fortune"
  ]
  node [
    id 11
    label "z&#322;ote_czasy"
  ]
  node [
    id 12
    label "cecha"
  ]
  node [
    id 13
    label "sytuacja"
  ]
  node [
    id 14
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 15
    label "charakterystyka"
  ]
  node [
    id 16
    label "m&#322;ot"
  ]
  node [
    id 17
    label "znak"
  ]
  node [
    id 18
    label "drzewo"
  ]
  node [
    id 19
    label "pr&#243;ba"
  ]
  node [
    id 20
    label "attribute"
  ]
  node [
    id 21
    label "marka"
  ]
  node [
    id 22
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 23
    label "warunki"
  ]
  node [
    id 24
    label "szczeg&#243;&#322;"
  ]
  node [
    id 25
    label "state"
  ]
  node [
    id 26
    label "motyw"
  ]
  node [
    id 27
    label "realia"
  ]
  node [
    id 28
    label "przej&#347;cie"
  ]
  node [
    id 29
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 30
    label "rodowo&#347;&#263;"
  ]
  node [
    id 31
    label "patent"
  ]
  node [
    id 32
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 33
    label "dobra"
  ]
  node [
    id 34
    label "stan"
  ]
  node [
    id 35
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 36
    label "przej&#347;&#263;"
  ]
  node [
    id 37
    label "possession"
  ]
  node [
    id 38
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 39
    label "rozmiar"
  ]
  node [
    id 40
    label "part"
  ]
  node [
    id 41
    label "rozpraszanie_si&#281;"
  ]
  node [
    id 42
    label "discrimination"
  ]
  node [
    id 43
    label "diverseness"
  ]
  node [
    id 44
    label "eklektyk"
  ]
  node [
    id 45
    label "rozproszenie_si&#281;"
  ]
  node [
    id 46
    label "differentiation"
  ]
  node [
    id 47
    label "multikulturalizm"
  ]
  node [
    id 48
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 49
    label "rozdzielenie"
  ]
  node [
    id 50
    label "nadanie"
  ]
  node [
    id 51
    label "podzielenie"
  ]
  node [
    id 52
    label "zrobienie"
  ]
  node [
    id 53
    label "urodzaj"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 3
    target 4
  ]
]
