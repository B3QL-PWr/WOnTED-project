graph [
  node [
    id 0
    label "zosta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 2
    label "rok"
    origin "text"
  ]
  node [
    id 3
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 4
    label "time"
    origin "text"
  ]
  node [
    id 5
    label "ludzko&#347;&#263;"
  ]
  node [
    id 6
    label "asymilowanie"
  ]
  node [
    id 7
    label "wapniak"
  ]
  node [
    id 8
    label "asymilowa&#263;"
  ]
  node [
    id 9
    label "os&#322;abia&#263;"
  ]
  node [
    id 10
    label "posta&#263;"
  ]
  node [
    id 11
    label "hominid"
  ]
  node [
    id 12
    label "podw&#322;adny"
  ]
  node [
    id 13
    label "os&#322;abianie"
  ]
  node [
    id 14
    label "g&#322;owa"
  ]
  node [
    id 15
    label "figura"
  ]
  node [
    id 16
    label "portrecista"
  ]
  node [
    id 17
    label "dwun&#243;g"
  ]
  node [
    id 18
    label "profanum"
  ]
  node [
    id 19
    label "mikrokosmos"
  ]
  node [
    id 20
    label "nasada"
  ]
  node [
    id 21
    label "duch"
  ]
  node [
    id 22
    label "antropochoria"
  ]
  node [
    id 23
    label "osoba"
  ]
  node [
    id 24
    label "wz&#243;r"
  ]
  node [
    id 25
    label "senior"
  ]
  node [
    id 26
    label "oddzia&#322;ywanie"
  ]
  node [
    id 27
    label "Adam"
  ]
  node [
    id 28
    label "homo_sapiens"
  ]
  node [
    id 29
    label "polifag"
  ]
  node [
    id 30
    label "konsument"
  ]
  node [
    id 31
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 32
    label "cz&#322;owiekowate"
  ]
  node [
    id 33
    label "istota_&#380;ywa"
  ]
  node [
    id 34
    label "pracownik"
  ]
  node [
    id 35
    label "Chocho&#322;"
  ]
  node [
    id 36
    label "Herkules_Poirot"
  ]
  node [
    id 37
    label "Edyp"
  ]
  node [
    id 38
    label "parali&#380;owa&#263;"
  ]
  node [
    id 39
    label "Harry_Potter"
  ]
  node [
    id 40
    label "Casanova"
  ]
  node [
    id 41
    label "Zgredek"
  ]
  node [
    id 42
    label "Gargantua"
  ]
  node [
    id 43
    label "Winnetou"
  ]
  node [
    id 44
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 45
    label "Dulcynea"
  ]
  node [
    id 46
    label "kategoria_gramatyczna"
  ]
  node [
    id 47
    label "person"
  ]
  node [
    id 48
    label "Plastu&#347;"
  ]
  node [
    id 49
    label "Quasimodo"
  ]
  node [
    id 50
    label "Sherlock_Holmes"
  ]
  node [
    id 51
    label "Faust"
  ]
  node [
    id 52
    label "Wallenrod"
  ]
  node [
    id 53
    label "Dwukwiat"
  ]
  node [
    id 54
    label "Don_Juan"
  ]
  node [
    id 55
    label "koniugacja"
  ]
  node [
    id 56
    label "Don_Kiszot"
  ]
  node [
    id 57
    label "Hamlet"
  ]
  node [
    id 58
    label "Werter"
  ]
  node [
    id 59
    label "istota"
  ]
  node [
    id 60
    label "Szwejk"
  ]
  node [
    id 61
    label "doros&#322;y"
  ]
  node [
    id 62
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 63
    label "jajko"
  ]
  node [
    id 64
    label "rodzic"
  ]
  node [
    id 65
    label "wapniaki"
  ]
  node [
    id 66
    label "zwierzchnik"
  ]
  node [
    id 67
    label "feuda&#322;"
  ]
  node [
    id 68
    label "starzec"
  ]
  node [
    id 69
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 70
    label "zawodnik"
  ]
  node [
    id 71
    label "komendancja"
  ]
  node [
    id 72
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 73
    label "asymilowanie_si&#281;"
  ]
  node [
    id 74
    label "absorption"
  ]
  node [
    id 75
    label "pobieranie"
  ]
  node [
    id 76
    label "czerpanie"
  ]
  node [
    id 77
    label "acquisition"
  ]
  node [
    id 78
    label "zmienianie"
  ]
  node [
    id 79
    label "organizm"
  ]
  node [
    id 80
    label "assimilation"
  ]
  node [
    id 81
    label "upodabnianie"
  ]
  node [
    id 82
    label "g&#322;oska"
  ]
  node [
    id 83
    label "kultura"
  ]
  node [
    id 84
    label "podobny"
  ]
  node [
    id 85
    label "grupa"
  ]
  node [
    id 86
    label "fonetyka"
  ]
  node [
    id 87
    label "suppress"
  ]
  node [
    id 88
    label "robi&#263;"
  ]
  node [
    id 89
    label "os&#322;abienie"
  ]
  node [
    id 90
    label "kondycja_fizyczna"
  ]
  node [
    id 91
    label "os&#322;abi&#263;"
  ]
  node [
    id 92
    label "zdrowie"
  ]
  node [
    id 93
    label "powodowa&#263;"
  ]
  node [
    id 94
    label "zmniejsza&#263;"
  ]
  node [
    id 95
    label "bate"
  ]
  node [
    id 96
    label "de-escalation"
  ]
  node [
    id 97
    label "powodowanie"
  ]
  node [
    id 98
    label "debilitation"
  ]
  node [
    id 99
    label "zmniejszanie"
  ]
  node [
    id 100
    label "s&#322;abszy"
  ]
  node [
    id 101
    label "pogarszanie"
  ]
  node [
    id 102
    label "assimilate"
  ]
  node [
    id 103
    label "dostosowywa&#263;"
  ]
  node [
    id 104
    label "dostosowa&#263;"
  ]
  node [
    id 105
    label "przejmowa&#263;"
  ]
  node [
    id 106
    label "upodobni&#263;"
  ]
  node [
    id 107
    label "przej&#261;&#263;"
  ]
  node [
    id 108
    label "upodabnia&#263;"
  ]
  node [
    id 109
    label "pobiera&#263;"
  ]
  node [
    id 110
    label "pobra&#263;"
  ]
  node [
    id 111
    label "zapis"
  ]
  node [
    id 112
    label "figure"
  ]
  node [
    id 113
    label "typ"
  ]
  node [
    id 114
    label "spos&#243;b"
  ]
  node [
    id 115
    label "mildew"
  ]
  node [
    id 116
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 117
    label "ideal"
  ]
  node [
    id 118
    label "rule"
  ]
  node [
    id 119
    label "ruch"
  ]
  node [
    id 120
    label "dekal"
  ]
  node [
    id 121
    label "motyw"
  ]
  node [
    id 122
    label "projekt"
  ]
  node [
    id 123
    label "charakterystyka"
  ]
  node [
    id 124
    label "zaistnie&#263;"
  ]
  node [
    id 125
    label "cecha"
  ]
  node [
    id 126
    label "Osjan"
  ]
  node [
    id 127
    label "kto&#347;"
  ]
  node [
    id 128
    label "wygl&#261;d"
  ]
  node [
    id 129
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 130
    label "osobowo&#347;&#263;"
  ]
  node [
    id 131
    label "wytw&#243;r"
  ]
  node [
    id 132
    label "trim"
  ]
  node [
    id 133
    label "poby&#263;"
  ]
  node [
    id 134
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 135
    label "Aspazja"
  ]
  node [
    id 136
    label "punkt_widzenia"
  ]
  node [
    id 137
    label "kompleksja"
  ]
  node [
    id 138
    label "wytrzyma&#263;"
  ]
  node [
    id 139
    label "budowa"
  ]
  node [
    id 140
    label "formacja"
  ]
  node [
    id 141
    label "pozosta&#263;"
  ]
  node [
    id 142
    label "point"
  ]
  node [
    id 143
    label "przedstawienie"
  ]
  node [
    id 144
    label "go&#347;&#263;"
  ]
  node [
    id 145
    label "fotograf"
  ]
  node [
    id 146
    label "malarz"
  ]
  node [
    id 147
    label "artysta"
  ]
  node [
    id 148
    label "hipnotyzowanie"
  ]
  node [
    id 149
    label "&#347;lad"
  ]
  node [
    id 150
    label "docieranie"
  ]
  node [
    id 151
    label "natural_process"
  ]
  node [
    id 152
    label "reakcja_chemiczna"
  ]
  node [
    id 153
    label "wdzieranie_si&#281;"
  ]
  node [
    id 154
    label "zjawisko"
  ]
  node [
    id 155
    label "act"
  ]
  node [
    id 156
    label "rezultat"
  ]
  node [
    id 157
    label "lobbysta"
  ]
  node [
    id 158
    label "pryncypa&#322;"
  ]
  node [
    id 159
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 160
    label "kszta&#322;t"
  ]
  node [
    id 161
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 162
    label "wiedza"
  ]
  node [
    id 163
    label "kierowa&#263;"
  ]
  node [
    id 164
    label "alkohol"
  ]
  node [
    id 165
    label "zdolno&#347;&#263;"
  ]
  node [
    id 166
    label "&#380;ycie"
  ]
  node [
    id 167
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 168
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 169
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 170
    label "sztuka"
  ]
  node [
    id 171
    label "dekiel"
  ]
  node [
    id 172
    label "ro&#347;lina"
  ]
  node [
    id 173
    label "&#347;ci&#281;cie"
  ]
  node [
    id 174
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 175
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 176
    label "&#347;ci&#281;gno"
  ]
  node [
    id 177
    label "noosfera"
  ]
  node [
    id 178
    label "byd&#322;o"
  ]
  node [
    id 179
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 180
    label "makrocefalia"
  ]
  node [
    id 181
    label "obiekt"
  ]
  node [
    id 182
    label "ucho"
  ]
  node [
    id 183
    label "g&#243;ra"
  ]
  node [
    id 184
    label "m&#243;zg"
  ]
  node [
    id 185
    label "kierownictwo"
  ]
  node [
    id 186
    label "fryzura"
  ]
  node [
    id 187
    label "umys&#322;"
  ]
  node [
    id 188
    label "cia&#322;o"
  ]
  node [
    id 189
    label "cz&#322;onek"
  ]
  node [
    id 190
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 191
    label "czaszka"
  ]
  node [
    id 192
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 193
    label "allochoria"
  ]
  node [
    id 194
    label "p&#322;aszczyzna"
  ]
  node [
    id 195
    label "przedmiot"
  ]
  node [
    id 196
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 197
    label "bierka_szachowa"
  ]
  node [
    id 198
    label "obiekt_matematyczny"
  ]
  node [
    id 199
    label "gestaltyzm"
  ]
  node [
    id 200
    label "styl"
  ]
  node [
    id 201
    label "obraz"
  ]
  node [
    id 202
    label "rzecz"
  ]
  node [
    id 203
    label "d&#378;wi&#281;k"
  ]
  node [
    id 204
    label "character"
  ]
  node [
    id 205
    label "rze&#378;ba"
  ]
  node [
    id 206
    label "stylistyka"
  ]
  node [
    id 207
    label "miejsce"
  ]
  node [
    id 208
    label "antycypacja"
  ]
  node [
    id 209
    label "ornamentyka"
  ]
  node [
    id 210
    label "informacja"
  ]
  node [
    id 211
    label "facet"
  ]
  node [
    id 212
    label "popis"
  ]
  node [
    id 213
    label "wiersz"
  ]
  node [
    id 214
    label "symetria"
  ]
  node [
    id 215
    label "lingwistyka_kognitywna"
  ]
  node [
    id 216
    label "karta"
  ]
  node [
    id 217
    label "shape"
  ]
  node [
    id 218
    label "podzbi&#243;r"
  ]
  node [
    id 219
    label "perspektywa"
  ]
  node [
    id 220
    label "dziedzina"
  ]
  node [
    id 221
    label "nak&#322;adka"
  ]
  node [
    id 222
    label "li&#347;&#263;"
  ]
  node [
    id 223
    label "jama_gard&#322;owa"
  ]
  node [
    id 224
    label "rezonator"
  ]
  node [
    id 225
    label "podstawa"
  ]
  node [
    id 226
    label "base"
  ]
  node [
    id 227
    label "piek&#322;o"
  ]
  node [
    id 228
    label "human_body"
  ]
  node [
    id 229
    label "ofiarowywanie"
  ]
  node [
    id 230
    label "sfera_afektywna"
  ]
  node [
    id 231
    label "nekromancja"
  ]
  node [
    id 232
    label "Po&#347;wist"
  ]
  node [
    id 233
    label "podekscytowanie"
  ]
  node [
    id 234
    label "deformowanie"
  ]
  node [
    id 235
    label "sumienie"
  ]
  node [
    id 236
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 237
    label "deformowa&#263;"
  ]
  node [
    id 238
    label "psychika"
  ]
  node [
    id 239
    label "zjawa"
  ]
  node [
    id 240
    label "zmar&#322;y"
  ]
  node [
    id 241
    label "istota_nadprzyrodzona"
  ]
  node [
    id 242
    label "power"
  ]
  node [
    id 243
    label "entity"
  ]
  node [
    id 244
    label "ofiarowywa&#263;"
  ]
  node [
    id 245
    label "oddech"
  ]
  node [
    id 246
    label "seksualno&#347;&#263;"
  ]
  node [
    id 247
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 248
    label "byt"
  ]
  node [
    id 249
    label "si&#322;a"
  ]
  node [
    id 250
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 251
    label "ego"
  ]
  node [
    id 252
    label "ofiarowanie"
  ]
  node [
    id 253
    label "charakter"
  ]
  node [
    id 254
    label "fizjonomia"
  ]
  node [
    id 255
    label "kompleks"
  ]
  node [
    id 256
    label "zapalno&#347;&#263;"
  ]
  node [
    id 257
    label "T&#281;sknica"
  ]
  node [
    id 258
    label "ofiarowa&#263;"
  ]
  node [
    id 259
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 260
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 261
    label "passion"
  ]
  node [
    id 262
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 263
    label "odbicie"
  ]
  node [
    id 264
    label "atom"
  ]
  node [
    id 265
    label "przyroda"
  ]
  node [
    id 266
    label "Ziemia"
  ]
  node [
    id 267
    label "kosmos"
  ]
  node [
    id 268
    label "miniatura"
  ]
  node [
    id 269
    label "p&#243;&#322;rocze"
  ]
  node [
    id 270
    label "martwy_sezon"
  ]
  node [
    id 271
    label "kalendarz"
  ]
  node [
    id 272
    label "cykl_astronomiczny"
  ]
  node [
    id 273
    label "lata"
  ]
  node [
    id 274
    label "pora_roku"
  ]
  node [
    id 275
    label "stulecie"
  ]
  node [
    id 276
    label "kurs"
  ]
  node [
    id 277
    label "czas"
  ]
  node [
    id 278
    label "jubileusz"
  ]
  node [
    id 279
    label "kwarta&#322;"
  ]
  node [
    id 280
    label "miesi&#261;c"
  ]
  node [
    id 281
    label "summer"
  ]
  node [
    id 282
    label "odm&#322;adzanie"
  ]
  node [
    id 283
    label "liga"
  ]
  node [
    id 284
    label "jednostka_systematyczna"
  ]
  node [
    id 285
    label "gromada"
  ]
  node [
    id 286
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 287
    label "egzemplarz"
  ]
  node [
    id 288
    label "Entuzjastki"
  ]
  node [
    id 289
    label "zbi&#243;r"
  ]
  node [
    id 290
    label "kompozycja"
  ]
  node [
    id 291
    label "Terranie"
  ]
  node [
    id 292
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 293
    label "category"
  ]
  node [
    id 294
    label "pakiet_klimatyczny"
  ]
  node [
    id 295
    label "oddzia&#322;"
  ]
  node [
    id 296
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 297
    label "cz&#261;steczka"
  ]
  node [
    id 298
    label "stage_set"
  ]
  node [
    id 299
    label "type"
  ]
  node [
    id 300
    label "specgrupa"
  ]
  node [
    id 301
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 302
    label "&#346;wietliki"
  ]
  node [
    id 303
    label "odm&#322;odzenie"
  ]
  node [
    id 304
    label "Eurogrupa"
  ]
  node [
    id 305
    label "odm&#322;adza&#263;"
  ]
  node [
    id 306
    label "formacja_geologiczna"
  ]
  node [
    id 307
    label "harcerze_starsi"
  ]
  node [
    id 308
    label "poprzedzanie"
  ]
  node [
    id 309
    label "czasoprzestrze&#324;"
  ]
  node [
    id 310
    label "laba"
  ]
  node [
    id 311
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 312
    label "chronometria"
  ]
  node [
    id 313
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 314
    label "rachuba_czasu"
  ]
  node [
    id 315
    label "przep&#322;ywanie"
  ]
  node [
    id 316
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 317
    label "czasokres"
  ]
  node [
    id 318
    label "odczyt"
  ]
  node [
    id 319
    label "chwila"
  ]
  node [
    id 320
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 321
    label "dzieje"
  ]
  node [
    id 322
    label "poprzedzenie"
  ]
  node [
    id 323
    label "trawienie"
  ]
  node [
    id 324
    label "pochodzi&#263;"
  ]
  node [
    id 325
    label "period"
  ]
  node [
    id 326
    label "okres_czasu"
  ]
  node [
    id 327
    label "poprzedza&#263;"
  ]
  node [
    id 328
    label "schy&#322;ek"
  ]
  node [
    id 329
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 330
    label "odwlekanie_si&#281;"
  ]
  node [
    id 331
    label "zegar"
  ]
  node [
    id 332
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 333
    label "czwarty_wymiar"
  ]
  node [
    id 334
    label "pochodzenie"
  ]
  node [
    id 335
    label "Zeitgeist"
  ]
  node [
    id 336
    label "trawi&#263;"
  ]
  node [
    id 337
    label "pogoda"
  ]
  node [
    id 338
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 339
    label "poprzedzi&#263;"
  ]
  node [
    id 340
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 341
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 342
    label "time_period"
  ]
  node [
    id 343
    label "tydzie&#324;"
  ]
  node [
    id 344
    label "miech"
  ]
  node [
    id 345
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 346
    label "kalendy"
  ]
  node [
    id 347
    label "term"
  ]
  node [
    id 348
    label "rok_akademicki"
  ]
  node [
    id 349
    label "rok_szkolny"
  ]
  node [
    id 350
    label "semester"
  ]
  node [
    id 351
    label "anniwersarz"
  ]
  node [
    id 352
    label "rocznica"
  ]
  node [
    id 353
    label "obszar"
  ]
  node [
    id 354
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 355
    label "long_time"
  ]
  node [
    id 356
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 357
    label "almanac"
  ]
  node [
    id 358
    label "rozk&#322;ad"
  ]
  node [
    id 359
    label "wydawnictwo"
  ]
  node [
    id 360
    label "Juliusz_Cezar"
  ]
  node [
    id 361
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 362
    label "zwy&#380;kowanie"
  ]
  node [
    id 363
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 364
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 365
    label "zaj&#281;cia"
  ]
  node [
    id 366
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 367
    label "trasa"
  ]
  node [
    id 368
    label "przeorientowywanie"
  ]
  node [
    id 369
    label "przejazd"
  ]
  node [
    id 370
    label "kierunek"
  ]
  node [
    id 371
    label "przeorientowywa&#263;"
  ]
  node [
    id 372
    label "nauka"
  ]
  node [
    id 373
    label "przeorientowanie"
  ]
  node [
    id 374
    label "klasa"
  ]
  node [
    id 375
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 376
    label "przeorientowa&#263;"
  ]
  node [
    id 377
    label "manner"
  ]
  node [
    id 378
    label "course"
  ]
  node [
    id 379
    label "passage"
  ]
  node [
    id 380
    label "zni&#380;kowanie"
  ]
  node [
    id 381
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 382
    label "seria"
  ]
  node [
    id 383
    label "stawka"
  ]
  node [
    id 384
    label "way"
  ]
  node [
    id 385
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 386
    label "deprecjacja"
  ]
  node [
    id 387
    label "cedu&#322;a"
  ]
  node [
    id 388
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 389
    label "drive"
  ]
  node [
    id 390
    label "bearing"
  ]
  node [
    id 391
    label "Lira"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 3
    target 4
  ]
]
