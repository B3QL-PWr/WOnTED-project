graph [
  node [
    id 0
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 2
    label "raz"
    origin "text"
  ]
  node [
    id 3
    label "sprawdzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 5
    label "element"
    origin "text"
  ]
  node [
    id 6
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 7
    label "wyko&#324;czenie"
    origin "text"
  ]
  node [
    id 8
    label "sko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 9
    label "co&#347;"
    origin "text"
  ]
  node [
    id 10
    label "echo"
  ]
  node [
    id 11
    label "pilnowa&#263;"
  ]
  node [
    id 12
    label "robi&#263;"
  ]
  node [
    id 13
    label "recall"
  ]
  node [
    id 14
    label "si&#281;ga&#263;"
  ]
  node [
    id 15
    label "take_care"
  ]
  node [
    id 16
    label "troska&#263;_si&#281;"
  ]
  node [
    id 17
    label "chowa&#263;"
  ]
  node [
    id 18
    label "zachowywa&#263;"
  ]
  node [
    id 19
    label "zna&#263;"
  ]
  node [
    id 20
    label "think"
  ]
  node [
    id 21
    label "report"
  ]
  node [
    id 22
    label "hide"
  ]
  node [
    id 23
    label "znosi&#263;"
  ]
  node [
    id 24
    label "czu&#263;"
  ]
  node [
    id 25
    label "train"
  ]
  node [
    id 26
    label "przetrzymywa&#263;"
  ]
  node [
    id 27
    label "hodowa&#263;"
  ]
  node [
    id 28
    label "meliniarz"
  ]
  node [
    id 29
    label "umieszcza&#263;"
  ]
  node [
    id 30
    label "ukrywa&#263;"
  ]
  node [
    id 31
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 32
    label "continue"
  ]
  node [
    id 33
    label "wk&#322;ada&#263;"
  ]
  node [
    id 34
    label "tajemnica"
  ]
  node [
    id 35
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 36
    label "zdyscyplinowanie"
  ]
  node [
    id 37
    label "podtrzymywa&#263;"
  ]
  node [
    id 38
    label "post"
  ]
  node [
    id 39
    label "control"
  ]
  node [
    id 40
    label "przechowywa&#263;"
  ]
  node [
    id 41
    label "behave"
  ]
  node [
    id 42
    label "dieta"
  ]
  node [
    id 43
    label "hold"
  ]
  node [
    id 44
    label "post&#281;powa&#263;"
  ]
  node [
    id 45
    label "compass"
  ]
  node [
    id 46
    label "korzysta&#263;"
  ]
  node [
    id 47
    label "appreciation"
  ]
  node [
    id 48
    label "osi&#261;ga&#263;"
  ]
  node [
    id 49
    label "dociera&#263;"
  ]
  node [
    id 50
    label "get"
  ]
  node [
    id 51
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 52
    label "mierzy&#263;"
  ]
  node [
    id 53
    label "u&#380;ywa&#263;"
  ]
  node [
    id 54
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 55
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 56
    label "exsert"
  ]
  node [
    id 57
    label "organizowa&#263;"
  ]
  node [
    id 58
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 59
    label "czyni&#263;"
  ]
  node [
    id 60
    label "give"
  ]
  node [
    id 61
    label "stylizowa&#263;"
  ]
  node [
    id 62
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 63
    label "falowa&#263;"
  ]
  node [
    id 64
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 65
    label "peddle"
  ]
  node [
    id 66
    label "praca"
  ]
  node [
    id 67
    label "wydala&#263;"
  ]
  node [
    id 68
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 69
    label "tentegowa&#263;"
  ]
  node [
    id 70
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 71
    label "urz&#261;dza&#263;"
  ]
  node [
    id 72
    label "oszukiwa&#263;"
  ]
  node [
    id 73
    label "work"
  ]
  node [
    id 74
    label "ukazywa&#263;"
  ]
  node [
    id 75
    label "przerabia&#263;"
  ]
  node [
    id 76
    label "act"
  ]
  node [
    id 77
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 78
    label "cognizance"
  ]
  node [
    id 79
    label "wiedzie&#263;"
  ]
  node [
    id 80
    label "resonance"
  ]
  node [
    id 81
    label "zjawisko"
  ]
  node [
    id 82
    label "time"
  ]
  node [
    id 83
    label "cios"
  ]
  node [
    id 84
    label "chwila"
  ]
  node [
    id 85
    label "uderzenie"
  ]
  node [
    id 86
    label "blok"
  ]
  node [
    id 87
    label "shot"
  ]
  node [
    id 88
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 89
    label "struktura_geologiczna"
  ]
  node [
    id 90
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 91
    label "pr&#243;ba"
  ]
  node [
    id 92
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 93
    label "coup"
  ]
  node [
    id 94
    label "siekacz"
  ]
  node [
    id 95
    label "instrumentalizacja"
  ]
  node [
    id 96
    label "trafienie"
  ]
  node [
    id 97
    label "walka"
  ]
  node [
    id 98
    label "zdarzenie_si&#281;"
  ]
  node [
    id 99
    label "wdarcie_si&#281;"
  ]
  node [
    id 100
    label "pogorszenie"
  ]
  node [
    id 101
    label "d&#378;wi&#281;k"
  ]
  node [
    id 102
    label "poczucie"
  ]
  node [
    id 103
    label "reakcja"
  ]
  node [
    id 104
    label "contact"
  ]
  node [
    id 105
    label "stukni&#281;cie"
  ]
  node [
    id 106
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 107
    label "bat"
  ]
  node [
    id 108
    label "spowodowanie"
  ]
  node [
    id 109
    label "rush"
  ]
  node [
    id 110
    label "odbicie"
  ]
  node [
    id 111
    label "dawka"
  ]
  node [
    id 112
    label "zadanie"
  ]
  node [
    id 113
    label "&#347;ci&#281;cie"
  ]
  node [
    id 114
    label "st&#322;uczenie"
  ]
  node [
    id 115
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 116
    label "odbicie_si&#281;"
  ]
  node [
    id 117
    label "dotkni&#281;cie"
  ]
  node [
    id 118
    label "charge"
  ]
  node [
    id 119
    label "dostanie"
  ]
  node [
    id 120
    label "skrytykowanie"
  ]
  node [
    id 121
    label "zagrywka"
  ]
  node [
    id 122
    label "manewr"
  ]
  node [
    id 123
    label "nast&#261;pienie"
  ]
  node [
    id 124
    label "uderzanie"
  ]
  node [
    id 125
    label "pogoda"
  ]
  node [
    id 126
    label "stroke"
  ]
  node [
    id 127
    label "pobicie"
  ]
  node [
    id 128
    label "ruch"
  ]
  node [
    id 129
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 130
    label "flap"
  ]
  node [
    id 131
    label "dotyk"
  ]
  node [
    id 132
    label "zrobienie"
  ]
  node [
    id 133
    label "czas"
  ]
  node [
    id 134
    label "examine"
  ]
  node [
    id 135
    label "zrobi&#263;"
  ]
  node [
    id 136
    label "post&#261;pi&#263;"
  ]
  node [
    id 137
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 138
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 139
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 140
    label "zorganizowa&#263;"
  ]
  node [
    id 141
    label "appoint"
  ]
  node [
    id 142
    label "wystylizowa&#263;"
  ]
  node [
    id 143
    label "cause"
  ]
  node [
    id 144
    label "przerobi&#263;"
  ]
  node [
    id 145
    label "nabra&#263;"
  ]
  node [
    id 146
    label "make"
  ]
  node [
    id 147
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 148
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 149
    label "wydali&#263;"
  ]
  node [
    id 150
    label "jaki&#347;"
  ]
  node [
    id 151
    label "przyzwoity"
  ]
  node [
    id 152
    label "ciekawy"
  ]
  node [
    id 153
    label "jako&#347;"
  ]
  node [
    id 154
    label "jako_tako"
  ]
  node [
    id 155
    label "niez&#322;y"
  ]
  node [
    id 156
    label "dziwny"
  ]
  node [
    id 157
    label "charakterystyczny"
  ]
  node [
    id 158
    label "r&#243;&#380;niczka"
  ]
  node [
    id 159
    label "&#347;rodowisko"
  ]
  node [
    id 160
    label "przedmiot"
  ]
  node [
    id 161
    label "materia"
  ]
  node [
    id 162
    label "szambo"
  ]
  node [
    id 163
    label "aspo&#322;eczny"
  ]
  node [
    id 164
    label "component"
  ]
  node [
    id 165
    label "szkodnik"
  ]
  node [
    id 166
    label "gangsterski"
  ]
  node [
    id 167
    label "poj&#281;cie"
  ]
  node [
    id 168
    label "underworld"
  ]
  node [
    id 169
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 170
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 171
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 172
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 173
    label "materia&#322;"
  ]
  node [
    id 174
    label "temat"
  ]
  node [
    id 175
    label "byt"
  ]
  node [
    id 176
    label "szczeg&#243;&#322;"
  ]
  node [
    id 177
    label "ropa"
  ]
  node [
    id 178
    label "informacja"
  ]
  node [
    id 179
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 180
    label "rzecz"
  ]
  node [
    id 181
    label "Rzym_Zachodni"
  ]
  node [
    id 182
    label "whole"
  ]
  node [
    id 183
    label "ilo&#347;&#263;"
  ]
  node [
    id 184
    label "Rzym_Wschodni"
  ]
  node [
    id 185
    label "urz&#261;dzenie"
  ]
  node [
    id 186
    label "pos&#322;uchanie"
  ]
  node [
    id 187
    label "skumanie"
  ]
  node [
    id 188
    label "orientacja"
  ]
  node [
    id 189
    label "wytw&#243;r"
  ]
  node [
    id 190
    label "zorientowanie"
  ]
  node [
    id 191
    label "teoria"
  ]
  node [
    id 192
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 193
    label "clasp"
  ]
  node [
    id 194
    label "forma"
  ]
  node [
    id 195
    label "przem&#243;wienie"
  ]
  node [
    id 196
    label "cz&#322;owiek"
  ]
  node [
    id 197
    label "fumigacja"
  ]
  node [
    id 198
    label "zwierz&#281;"
  ]
  node [
    id 199
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 200
    label "niszczyciel"
  ]
  node [
    id 201
    label "zwierz&#281;_domowe"
  ]
  node [
    id 202
    label "vermin"
  ]
  node [
    id 203
    label "class"
  ]
  node [
    id 204
    label "zesp&#243;&#322;"
  ]
  node [
    id 205
    label "obiekt_naturalny"
  ]
  node [
    id 206
    label "otoczenie"
  ]
  node [
    id 207
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 208
    label "environment"
  ]
  node [
    id 209
    label "huczek"
  ]
  node [
    id 210
    label "ekosystem"
  ]
  node [
    id 211
    label "wszechstworzenie"
  ]
  node [
    id 212
    label "grupa"
  ]
  node [
    id 213
    label "woda"
  ]
  node [
    id 214
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 215
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 216
    label "teren"
  ]
  node [
    id 217
    label "mikrokosmos"
  ]
  node [
    id 218
    label "stw&#243;r"
  ]
  node [
    id 219
    label "warunki"
  ]
  node [
    id 220
    label "Ziemia"
  ]
  node [
    id 221
    label "fauna"
  ]
  node [
    id 222
    label "biota"
  ]
  node [
    id 223
    label "integer"
  ]
  node [
    id 224
    label "liczba"
  ]
  node [
    id 225
    label "zlewanie_si&#281;"
  ]
  node [
    id 226
    label "uk&#322;ad"
  ]
  node [
    id 227
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 228
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 229
    label "pe&#322;ny"
  ]
  node [
    id 230
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 231
    label "zboczenie"
  ]
  node [
    id 232
    label "om&#243;wienie"
  ]
  node [
    id 233
    label "sponiewieranie"
  ]
  node [
    id 234
    label "discipline"
  ]
  node [
    id 235
    label "omawia&#263;"
  ]
  node [
    id 236
    label "kr&#261;&#380;enie"
  ]
  node [
    id 237
    label "tre&#347;&#263;"
  ]
  node [
    id 238
    label "robienie"
  ]
  node [
    id 239
    label "sponiewiera&#263;"
  ]
  node [
    id 240
    label "entity"
  ]
  node [
    id 241
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 242
    label "tematyka"
  ]
  node [
    id 243
    label "w&#261;tek"
  ]
  node [
    id 244
    label "charakter"
  ]
  node [
    id 245
    label "zbaczanie"
  ]
  node [
    id 246
    label "program_nauczania"
  ]
  node [
    id 247
    label "om&#243;wi&#263;"
  ]
  node [
    id 248
    label "omawianie"
  ]
  node [
    id 249
    label "thing"
  ]
  node [
    id 250
    label "kultura"
  ]
  node [
    id 251
    label "istota"
  ]
  node [
    id 252
    label "zbacza&#263;"
  ]
  node [
    id 253
    label "zboczy&#263;"
  ]
  node [
    id 254
    label "po_gangstersku"
  ]
  node [
    id 255
    label "przest&#281;pczy"
  ]
  node [
    id 256
    label "smr&#243;d"
  ]
  node [
    id 257
    label "gips"
  ]
  node [
    id 258
    label "koszmar"
  ]
  node [
    id 259
    label "pasztet"
  ]
  node [
    id 260
    label "kanalizacja"
  ]
  node [
    id 261
    label "mire"
  ]
  node [
    id 262
    label "budowla"
  ]
  node [
    id 263
    label "zbiornik"
  ]
  node [
    id 264
    label "kloaka"
  ]
  node [
    id 265
    label "niekorzystny"
  ]
  node [
    id 266
    label "aspo&#322;ecznie"
  ]
  node [
    id 267
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 268
    label "typowy"
  ]
  node [
    id 269
    label "niech&#281;tny"
  ]
  node [
    id 270
    label "adjustment"
  ]
  node [
    id 271
    label "panowanie"
  ]
  node [
    id 272
    label "przebywanie"
  ]
  node [
    id 273
    label "animation"
  ]
  node [
    id 274
    label "kwadrat"
  ]
  node [
    id 275
    label "stanie"
  ]
  node [
    id 276
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 277
    label "pomieszkanie"
  ]
  node [
    id 278
    label "lokal"
  ]
  node [
    id 279
    label "dom"
  ]
  node [
    id 280
    label "zajmowanie"
  ]
  node [
    id 281
    label "sprawowanie"
  ]
  node [
    id 282
    label "bycie"
  ]
  node [
    id 283
    label "kierowanie"
  ]
  node [
    id 284
    label "w&#322;adca"
  ]
  node [
    id 285
    label "dominowanie"
  ]
  node [
    id 286
    label "przewaga"
  ]
  node [
    id 287
    label "przewa&#380;anie"
  ]
  node [
    id 288
    label "znaczenie"
  ]
  node [
    id 289
    label "laterality"
  ]
  node [
    id 290
    label "dominance"
  ]
  node [
    id 291
    label "kontrolowanie"
  ]
  node [
    id 292
    label "temper"
  ]
  node [
    id 293
    label "rule"
  ]
  node [
    id 294
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 295
    label "prym"
  ]
  node [
    id 296
    label "w&#322;adza"
  ]
  node [
    id 297
    label "ocieranie_si&#281;"
  ]
  node [
    id 298
    label "otoczenie_si&#281;"
  ]
  node [
    id 299
    label "posiedzenie"
  ]
  node [
    id 300
    label "otarcie_si&#281;"
  ]
  node [
    id 301
    label "atakowanie"
  ]
  node [
    id 302
    label "otaczanie_si&#281;"
  ]
  node [
    id 303
    label "wyj&#347;cie"
  ]
  node [
    id 304
    label "zmierzanie"
  ]
  node [
    id 305
    label "residency"
  ]
  node [
    id 306
    label "sojourn"
  ]
  node [
    id 307
    label "wychodzenie"
  ]
  node [
    id 308
    label "tkwienie"
  ]
  node [
    id 309
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 310
    label "powodowanie"
  ]
  node [
    id 311
    label "lokowanie_si&#281;"
  ]
  node [
    id 312
    label "schorzenie"
  ]
  node [
    id 313
    label "zajmowanie_si&#281;"
  ]
  node [
    id 314
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 315
    label "stosowanie"
  ]
  node [
    id 316
    label "anektowanie"
  ]
  node [
    id 317
    label "zabieranie"
  ]
  node [
    id 318
    label "sytuowanie_si&#281;"
  ]
  node [
    id 319
    label "wype&#322;nianie"
  ]
  node [
    id 320
    label "obejmowanie"
  ]
  node [
    id 321
    label "klasyfikacja"
  ]
  node [
    id 322
    label "czynno&#347;&#263;"
  ]
  node [
    id 323
    label "dzianie_si&#281;"
  ]
  node [
    id 324
    label "branie"
  ]
  node [
    id 325
    label "rz&#261;dzenie"
  ]
  node [
    id 326
    label "occupation"
  ]
  node [
    id 327
    label "zadawanie"
  ]
  node [
    id 328
    label "zaj&#281;ty"
  ]
  node [
    id 329
    label "miejsce"
  ]
  node [
    id 330
    label "gastronomia"
  ]
  node [
    id 331
    label "zak&#322;ad"
  ]
  node [
    id 332
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 333
    label "rodzina"
  ]
  node [
    id 334
    label "substancja_mieszkaniowa"
  ]
  node [
    id 335
    label "instytucja"
  ]
  node [
    id 336
    label "siedziba"
  ]
  node [
    id 337
    label "dom_rodzinny"
  ]
  node [
    id 338
    label "budynek"
  ]
  node [
    id 339
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 340
    label "stead"
  ]
  node [
    id 341
    label "garderoba"
  ]
  node [
    id 342
    label "wiecha"
  ]
  node [
    id 343
    label "fratria"
  ]
  node [
    id 344
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 345
    label "trwanie"
  ]
  node [
    id 346
    label "ustanie"
  ]
  node [
    id 347
    label "wystanie"
  ]
  node [
    id 348
    label "postanie"
  ]
  node [
    id 349
    label "wystawanie"
  ]
  node [
    id 350
    label "kosztowanie"
  ]
  node [
    id 351
    label "przestanie"
  ]
  node [
    id 352
    label "pot&#281;ga"
  ]
  node [
    id 353
    label "wielok&#261;t_foremny"
  ]
  node [
    id 354
    label "stopie&#324;_pisma"
  ]
  node [
    id 355
    label "prostok&#261;t"
  ]
  node [
    id 356
    label "square"
  ]
  node [
    id 357
    label "romb"
  ]
  node [
    id 358
    label "justunek"
  ]
  node [
    id 359
    label "dzielnica"
  ]
  node [
    id 360
    label "poletko"
  ]
  node [
    id 361
    label "ekologia"
  ]
  node [
    id 362
    label "tango"
  ]
  node [
    id 363
    label "figura_taneczna"
  ]
  node [
    id 364
    label "zu&#380;ycie"
  ]
  node [
    id 365
    label "skonany"
  ]
  node [
    id 366
    label "zniszczenie"
  ]
  node [
    id 367
    label "os&#322;abienie"
  ]
  node [
    id 368
    label "str&#243;j"
  ]
  node [
    id 369
    label "wymordowanie"
  ]
  node [
    id 370
    label "murder"
  ]
  node [
    id 371
    label "pomordowanie"
  ]
  node [
    id 372
    label "znu&#380;enie"
  ]
  node [
    id 373
    label "ukszta&#322;towanie"
  ]
  node [
    id 374
    label "zm&#281;czenie"
  ]
  node [
    id 375
    label "zabicie"
  ]
  node [
    id 376
    label "rozwini&#281;cie"
  ]
  node [
    id 377
    label "kszta&#322;t"
  ]
  node [
    id 378
    label "training"
  ]
  node [
    id 379
    label "zakr&#281;cenie"
  ]
  node [
    id 380
    label "figuration"
  ]
  node [
    id 381
    label "shape"
  ]
  node [
    id 382
    label "narobienie"
  ]
  node [
    id 383
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 384
    label "creation"
  ]
  node [
    id 385
    label "porobienie"
  ]
  node [
    id 386
    label "wear"
  ]
  node [
    id 387
    label "destruction"
  ]
  node [
    id 388
    label "attrition"
  ]
  node [
    id 389
    label "zaszkodzenie"
  ]
  node [
    id 390
    label "podpalenie"
  ]
  node [
    id 391
    label "strata"
  ]
  node [
    id 392
    label "kondycja_fizyczna"
  ]
  node [
    id 393
    label "spl&#261;drowanie"
  ]
  node [
    id 394
    label "zdrowie"
  ]
  node [
    id 395
    label "poniszczenie"
  ]
  node [
    id 396
    label "ruin"
  ]
  node [
    id 397
    label "stanie_si&#281;"
  ]
  node [
    id 398
    label "rezultat"
  ]
  node [
    id 399
    label "poniszczenie_si&#281;"
  ]
  node [
    id 400
    label "&#347;mier&#263;"
  ]
  node [
    id 401
    label "zabrzmienie"
  ]
  node [
    id 402
    label "skrzywdzenie"
  ]
  node [
    id 403
    label "pozabijanie"
  ]
  node [
    id 404
    label "usuni&#281;cie"
  ]
  node [
    id 405
    label "killing"
  ]
  node [
    id 406
    label "czyn"
  ]
  node [
    id 407
    label "umarcie"
  ]
  node [
    id 408
    label "granie"
  ]
  node [
    id 409
    label "zamkni&#281;cie"
  ]
  node [
    id 410
    label "compaction"
  ]
  node [
    id 411
    label "doznanie"
  ]
  node [
    id 412
    label "os&#322;abianie"
  ]
  node [
    id 413
    label "fatigue_duty"
  ]
  node [
    id 414
    label "os&#322;abia&#263;"
  ]
  node [
    id 415
    label "zmniejszenie"
  ]
  node [
    id 416
    label "os&#322;abi&#263;"
  ]
  node [
    id 417
    label "infirmity"
  ]
  node [
    id 418
    label "s&#322;abszy"
  ]
  node [
    id 419
    label "wydanie"
  ]
  node [
    id 420
    label "exhaustion"
  ]
  node [
    id 421
    label "use"
  ]
  node [
    id 422
    label "wybicie"
  ]
  node [
    id 423
    label "kr&#281;gi_pod_oczami"
  ]
  node [
    id 424
    label "niewyspanie"
  ]
  node [
    id 425
    label "inanition"
  ]
  node [
    id 426
    label "overstrain"
  ]
  node [
    id 427
    label "siniec"
  ]
  node [
    id 428
    label "wywo&#322;anie"
  ]
  node [
    id 429
    label "znudzenie"
  ]
  node [
    id 430
    label "niesw&#243;j"
  ]
  node [
    id 431
    label "wp&#243;&#322;&#380;ywy"
  ]
  node [
    id 432
    label "wyko&#324;czanie"
  ]
  node [
    id 433
    label "gorset"
  ]
  node [
    id 434
    label "zrzucenie"
  ]
  node [
    id 435
    label "znoszenie"
  ]
  node [
    id 436
    label "kr&#243;j"
  ]
  node [
    id 437
    label "struktura"
  ]
  node [
    id 438
    label "ubranie_si&#281;"
  ]
  node [
    id 439
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 440
    label "pochodzi&#263;"
  ]
  node [
    id 441
    label "zrzuci&#263;"
  ]
  node [
    id 442
    label "pasmanteria"
  ]
  node [
    id 443
    label "pochodzenie"
  ]
  node [
    id 444
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 445
    label "odzie&#380;"
  ]
  node [
    id 446
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 447
    label "nosi&#263;"
  ]
  node [
    id 448
    label "zasada"
  ]
  node [
    id 449
    label "w&#322;o&#380;enie"
  ]
  node [
    id 450
    label "odziewek"
  ]
  node [
    id 451
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 452
    label "end"
  ]
  node [
    id 453
    label "zako&#324;czy&#263;"
  ]
  node [
    id 454
    label "communicate"
  ]
  node [
    id 455
    label "przesta&#263;"
  ]
  node [
    id 456
    label "dispose"
  ]
  node [
    id 457
    label "zrezygnowa&#263;"
  ]
  node [
    id 458
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 459
    label "wytworzy&#263;"
  ]
  node [
    id 460
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 461
    label "coating"
  ]
  node [
    id 462
    label "drop"
  ]
  node [
    id 463
    label "leave_office"
  ]
  node [
    id 464
    label "fail"
  ]
  node [
    id 465
    label "cosik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 465
  ]
]
