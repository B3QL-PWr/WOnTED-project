graph [
  node [
    id 0
    label "mark"
    origin "text"
  ]
  node [
    id 1
    label "ryan"
    origin "text"
  ]
  node [
    id 2
    label "Mark"
  ]
  node [
    id 3
    label "Ryan"
  ]
  node [
    id 4
    label "WHO"
  ]
  node [
    id 5
    label "Dares"
  ]
  node [
    id 6
    label "Wins"
  ]
  node [
    id 7
    label "Robin"
  ]
  node [
    id 8
    label "zeszyt"
  ]
  node [
    id 9
    label "Sherwood"
  ]
  node [
    id 10
    label "Loxley"
  ]
  node [
    id 11
    label "The"
  ]
  node [
    id 12
    label "secret"
  ]
  node [
    id 13
    label "Adventures"
  ]
  node [
    id 14
    label "of"
  ]
  node [
    id 15
    label "Jules"
  ]
  node [
    id 16
    label "Verne"
  ]
  node [
    id 17
    label "kr&#243;l"
  ]
  node [
    id 18
    label "Artur"
  ]
  node [
    id 19
    label "Bacchae"
  ]
  node [
    id 20
    label "westa"
  ]
  node [
    id 21
    label "end"
  ]
  node [
    id 22
    label "This"
  ]
  node [
    id 23
    label "Incredible"
  ]
  node [
    id 24
    label "Journey"
  ]
  node [
    id 25
    label "tom"
  ]
  node [
    id 26
    label "Jones"
  ]
  node [
    id 27
    label "Over"
  ]
  node [
    id 28
    label "the"
  ]
  node [
    id 29
    label "Hills"
  ]
  node [
    id 30
    label "Anda"
  ]
  node [
    id 31
    label "fara"
  ]
  node [
    id 32
    label "Away"
  ]
  node [
    id 33
    label "Garyego"
  ]
  node [
    id 34
    label "Moore"
  ]
  node [
    id 35
    label "upi&#243;r"
  ]
  node [
    id 36
    label "wyspa"
  ]
  node [
    id 37
    label "opera"
  ]
  node [
    id 38
    label "dzia&#322;o"
  ]
  node [
    id 39
    label "zag&#322;ada"
  ]
  node [
    id 40
    label "rycerz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 40
  ]
  edge [
    source 18
    target 40
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 31
  ]
  edge [
    source 27
    target 32
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
]
