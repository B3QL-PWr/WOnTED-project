graph [
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "czo&#322;&#243;wka"
    origin "text"
  ]
  node [
    id 2
    label "znana"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zaawansowana"
    origin "text"
  ]
  node [
    id 5
    label "animacja"
    origin "text"
  ]
  node [
    id 6
    label "przystawa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "lata"
    origin "text"
  ]
  node [
    id 8
    label "kogut"
    origin "text"
  ]
  node [
    id 9
    label "milicja"
    origin "text"
  ]
  node [
    id 10
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 11
    label "symbol"
    origin "text"
  ]
  node [
    id 12
    label "audycja"
    origin "text"
  ]
  node [
    id 13
    label "godzina"
  ]
  node [
    id 14
    label "time"
  ]
  node [
    id 15
    label "kwadrans"
  ]
  node [
    id 16
    label "p&#243;&#322;godzina"
  ]
  node [
    id 17
    label "doba"
  ]
  node [
    id 18
    label "czas"
  ]
  node [
    id 19
    label "jednostka_czasu"
  ]
  node [
    id 20
    label "minuta"
  ]
  node [
    id 21
    label "poligrafia"
  ]
  node [
    id 22
    label "latarka_czo&#322;owa"
  ]
  node [
    id 23
    label "&#347;ciana"
  ]
  node [
    id 24
    label "pododdzia&#322;"
  ]
  node [
    id 25
    label "rajd"
  ]
  node [
    id 26
    label "alpinizm"
  ]
  node [
    id 27
    label "film"
  ]
  node [
    id 28
    label "bieg"
  ]
  node [
    id 29
    label "front"
  ]
  node [
    id 30
    label "materia&#322;"
  ]
  node [
    id 31
    label "rz&#261;d"
  ]
  node [
    id 32
    label "grupa"
  ]
  node [
    id 33
    label "wst&#281;p"
  ]
  node [
    id 34
    label "elita"
  ]
  node [
    id 35
    label "zderzenie"
  ]
  node [
    id 36
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 37
    label "elite"
  ]
  node [
    id 38
    label "&#347;rodowisko"
  ]
  node [
    id 39
    label "asymilowa&#263;"
  ]
  node [
    id 40
    label "kompozycja"
  ]
  node [
    id 41
    label "pakiet_klimatyczny"
  ]
  node [
    id 42
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 43
    label "type"
  ]
  node [
    id 44
    label "cz&#261;steczka"
  ]
  node [
    id 45
    label "gromada"
  ]
  node [
    id 46
    label "specgrupa"
  ]
  node [
    id 47
    label "egzemplarz"
  ]
  node [
    id 48
    label "stage_set"
  ]
  node [
    id 49
    label "asymilowanie"
  ]
  node [
    id 50
    label "zbi&#243;r"
  ]
  node [
    id 51
    label "odm&#322;odzenie"
  ]
  node [
    id 52
    label "odm&#322;adza&#263;"
  ]
  node [
    id 53
    label "harcerze_starsi"
  ]
  node [
    id 54
    label "jednostka_systematyczna"
  ]
  node [
    id 55
    label "oddzia&#322;"
  ]
  node [
    id 56
    label "category"
  ]
  node [
    id 57
    label "liga"
  ]
  node [
    id 58
    label "&#346;wietliki"
  ]
  node [
    id 59
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 60
    label "formacja_geologiczna"
  ]
  node [
    id 61
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 62
    label "Eurogrupa"
  ]
  node [
    id 63
    label "Terranie"
  ]
  node [
    id 64
    label "odm&#322;adzanie"
  ]
  node [
    id 65
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 66
    label "Entuzjastki"
  ]
  node [
    id 67
    label "wyrobisko"
  ]
  node [
    id 68
    label "trudno&#347;&#263;"
  ]
  node [
    id 69
    label "kres"
  ]
  node [
    id 70
    label "bariera"
  ]
  node [
    id 71
    label "przegroda"
  ]
  node [
    id 72
    label "p&#322;aszczyzna"
  ]
  node [
    id 73
    label "profil"
  ]
  node [
    id 74
    label "facebook"
  ]
  node [
    id 75
    label "zbocze"
  ]
  node [
    id 76
    label "miejsce"
  ]
  node [
    id 77
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 78
    label "obstruction"
  ]
  node [
    id 79
    label "kszta&#322;t"
  ]
  node [
    id 80
    label "pow&#322;oka"
  ]
  node [
    id 81
    label "wielo&#347;cian"
  ]
  node [
    id 82
    label "pu&#322;k"
  ]
  node [
    id 83
    label "sfera"
  ]
  node [
    id 84
    label "stowarzyszenie"
  ]
  node [
    id 85
    label "przycz&#243;&#322;ek"
  ]
  node [
    id 86
    label "linia"
  ]
  node [
    id 87
    label "prz&#243;d"
  ]
  node [
    id 88
    label "szczyt"
  ]
  node [
    id 89
    label "pole_bitwy"
  ]
  node [
    id 90
    label "budynek"
  ]
  node [
    id 91
    label "zjawisko"
  ]
  node [
    id 92
    label "zjednoczenie"
  ]
  node [
    id 93
    label "przedpole"
  ]
  node [
    id 94
    label "zalega&#263;"
  ]
  node [
    id 95
    label "zaleganie"
  ]
  node [
    id 96
    label "rokada"
  ]
  node [
    id 97
    label "elewacja"
  ]
  node [
    id 98
    label "powietrze"
  ]
  node [
    id 99
    label "pomieszczenie"
  ]
  node [
    id 100
    label "szpaler"
  ]
  node [
    id 101
    label "number"
  ]
  node [
    id 102
    label "Londyn"
  ]
  node [
    id 103
    label "przybli&#380;enie"
  ]
  node [
    id 104
    label "premier"
  ]
  node [
    id 105
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 106
    label "tract"
  ]
  node [
    id 107
    label "uporz&#261;dkowanie"
  ]
  node [
    id 108
    label "egzekutywa"
  ]
  node [
    id 109
    label "klasa"
  ]
  node [
    id 110
    label "w&#322;adza"
  ]
  node [
    id 111
    label "Konsulat"
  ]
  node [
    id 112
    label "gabinet_cieni"
  ]
  node [
    id 113
    label "lon&#380;a"
  ]
  node [
    id 114
    label "kategoria"
  ]
  node [
    id 115
    label "instytucja"
  ]
  node [
    id 116
    label "tekst"
  ]
  node [
    id 117
    label "g&#322;oska"
  ]
  node [
    id 118
    label "podstawy"
  ]
  node [
    id 119
    label "utw&#243;r"
  ]
  node [
    id 120
    label "pocz&#261;tek"
  ]
  node [
    id 121
    label "evocation"
  ]
  node [
    id 122
    label "doj&#347;cie"
  ]
  node [
    id 123
    label "zapowied&#378;"
  ]
  node [
    id 124
    label "wymowa"
  ]
  node [
    id 125
    label "zanalizowanie"
  ]
  node [
    id 126
    label "comparison"
  ]
  node [
    id 127
    label "crash"
  ]
  node [
    id 128
    label "konfrontacja"
  ]
  node [
    id 129
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 130
    label "konflikt"
  ]
  node [
    id 131
    label "wypadek"
  ]
  node [
    id 132
    label "cz&#322;owiek"
  ]
  node [
    id 133
    label "tworzywo"
  ]
  node [
    id 134
    label "substancja"
  ]
  node [
    id 135
    label "materia"
  ]
  node [
    id 136
    label "bielarnia"
  ]
  node [
    id 137
    label "dyspozycja"
  ]
  node [
    id 138
    label "archiwum"
  ]
  node [
    id 139
    label "krajalno&#347;&#263;"
  ]
  node [
    id 140
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 141
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 142
    label "kandydat"
  ]
  node [
    id 143
    label "krajka"
  ]
  node [
    id 144
    label "nawil&#380;arka"
  ]
  node [
    id 145
    label "dane"
  ]
  node [
    id 146
    label "pozycja"
  ]
  node [
    id 147
    label "przedbieg"
  ]
  node [
    id 148
    label "d&#261;&#380;enie"
  ]
  node [
    id 149
    label "syfon"
  ]
  node [
    id 150
    label "bystrzyca"
  ]
  node [
    id 151
    label "parametr"
  ]
  node [
    id 152
    label "ciek_wodny"
  ]
  node [
    id 153
    label "pr&#261;d"
  ]
  node [
    id 154
    label "konkurencja"
  ]
  node [
    id 155
    label "cycle"
  ]
  node [
    id 156
    label "wy&#347;cig"
  ]
  node [
    id 157
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 158
    label "tryb"
  ]
  node [
    id 159
    label "proces"
  ]
  node [
    id 160
    label "ruch"
  ]
  node [
    id 161
    label "roll"
  ]
  node [
    id 162
    label "kierunek"
  ]
  node [
    id 163
    label "kurs"
  ]
  node [
    id 164
    label "procedura"
  ]
  node [
    id 165
    label "gra_MMORPG"
  ]
  node [
    id 166
    label "Rajd_Dakar"
  ]
  node [
    id 167
    label "rally"
  ]
  node [
    id 168
    label "foray"
  ]
  node [
    id 169
    label "Rajd_Arsena&#322;"
  ]
  node [
    id 170
    label "boss"
  ]
  node [
    id 171
    label "rozgrywka"
  ]
  node [
    id 172
    label "Rajd_Barb&#243;rka"
  ]
  node [
    id 173
    label "podr&#243;&#380;"
  ]
  node [
    id 174
    label "mountain_climbing"
  ]
  node [
    id 175
    label "wspinaczka"
  ]
  node [
    id 176
    label "przemys&#322;"
  ]
  node [
    id 177
    label "na&#347;wietlarka"
  ]
  node [
    id 178
    label "graphic_art"
  ]
  node [
    id 179
    label "zecerstwo"
  ]
  node [
    id 180
    label "poligrafika"
  ]
  node [
    id 181
    label "desktop_publishing"
  ]
  node [
    id 182
    label "introligatorstwo"
  ]
  node [
    id 183
    label "b&#322;ona"
  ]
  node [
    id 184
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 185
    label "trawiarnia"
  ]
  node [
    id 186
    label "sztuka"
  ]
  node [
    id 187
    label "emulsja_fotograficzna"
  ]
  node [
    id 188
    label "rozbieg&#243;wka"
  ]
  node [
    id 189
    label "sklejarka"
  ]
  node [
    id 190
    label "odczuli&#263;"
  ]
  node [
    id 191
    label "filmoteka"
  ]
  node [
    id 192
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 193
    label "dorobek"
  ]
  node [
    id 194
    label "animatronika"
  ]
  node [
    id 195
    label "napisy"
  ]
  node [
    id 196
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 197
    label "blik"
  ]
  node [
    id 198
    label "odczula&#263;"
  ]
  node [
    id 199
    label "odczulenie"
  ]
  node [
    id 200
    label "photograph"
  ]
  node [
    id 201
    label "scena"
  ]
  node [
    id 202
    label "klatka"
  ]
  node [
    id 203
    label "ta&#347;ma"
  ]
  node [
    id 204
    label "uj&#281;cie"
  ]
  node [
    id 205
    label "anamorfoza"
  ]
  node [
    id 206
    label "odczulanie"
  ]
  node [
    id 207
    label "postprodukcja"
  ]
  node [
    id 208
    label "muza"
  ]
  node [
    id 209
    label "block"
  ]
  node [
    id 210
    label "rola"
  ]
  node [
    id 211
    label "ty&#322;&#243;wka"
  ]
  node [
    id 212
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 213
    label "stan"
  ]
  node [
    id 214
    label "stand"
  ]
  node [
    id 215
    label "trwa&#263;"
  ]
  node [
    id 216
    label "equal"
  ]
  node [
    id 217
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 218
    label "chodzi&#263;"
  ]
  node [
    id 219
    label "uczestniczy&#263;"
  ]
  node [
    id 220
    label "obecno&#347;&#263;"
  ]
  node [
    id 221
    label "si&#281;ga&#263;"
  ]
  node [
    id 222
    label "mie&#263;_miejsce"
  ]
  node [
    id 223
    label "robi&#263;"
  ]
  node [
    id 224
    label "participate"
  ]
  node [
    id 225
    label "adhere"
  ]
  node [
    id 226
    label "pozostawa&#263;"
  ]
  node [
    id 227
    label "zostawa&#263;"
  ]
  node [
    id 228
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 229
    label "istnie&#263;"
  ]
  node [
    id 230
    label "compass"
  ]
  node [
    id 231
    label "exsert"
  ]
  node [
    id 232
    label "get"
  ]
  node [
    id 233
    label "u&#380;ywa&#263;"
  ]
  node [
    id 234
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 235
    label "osi&#261;ga&#263;"
  ]
  node [
    id 236
    label "korzysta&#263;"
  ]
  node [
    id 237
    label "appreciation"
  ]
  node [
    id 238
    label "dociera&#263;"
  ]
  node [
    id 239
    label "mierzy&#263;"
  ]
  node [
    id 240
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 241
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 242
    label "being"
  ]
  node [
    id 243
    label "cecha"
  ]
  node [
    id 244
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 245
    label "proceed"
  ]
  node [
    id 246
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 247
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 248
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 249
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 250
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 251
    label "str&#243;j"
  ]
  node [
    id 252
    label "para"
  ]
  node [
    id 253
    label "krok"
  ]
  node [
    id 254
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 255
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 256
    label "przebiega&#263;"
  ]
  node [
    id 257
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 258
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 259
    label "continue"
  ]
  node [
    id 260
    label "carry"
  ]
  node [
    id 261
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 262
    label "wk&#322;ada&#263;"
  ]
  node [
    id 263
    label "p&#322;ywa&#263;"
  ]
  node [
    id 264
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 265
    label "bangla&#263;"
  ]
  node [
    id 266
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 267
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 268
    label "bywa&#263;"
  ]
  node [
    id 269
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 270
    label "dziama&#263;"
  ]
  node [
    id 271
    label "run"
  ]
  node [
    id 272
    label "stara&#263;_si&#281;"
  ]
  node [
    id 273
    label "Arakan"
  ]
  node [
    id 274
    label "Teksas"
  ]
  node [
    id 275
    label "Georgia"
  ]
  node [
    id 276
    label "Maryland"
  ]
  node [
    id 277
    label "warstwa"
  ]
  node [
    id 278
    label "Luizjana"
  ]
  node [
    id 279
    label "Massachusetts"
  ]
  node [
    id 280
    label "Michigan"
  ]
  node [
    id 281
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 282
    label "samopoczucie"
  ]
  node [
    id 283
    label "Floryda"
  ]
  node [
    id 284
    label "Ohio"
  ]
  node [
    id 285
    label "Alaska"
  ]
  node [
    id 286
    label "Nowy_Meksyk"
  ]
  node [
    id 287
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 288
    label "wci&#281;cie"
  ]
  node [
    id 289
    label "Kansas"
  ]
  node [
    id 290
    label "Alabama"
  ]
  node [
    id 291
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 292
    label "Kalifornia"
  ]
  node [
    id 293
    label "Wirginia"
  ]
  node [
    id 294
    label "punkt"
  ]
  node [
    id 295
    label "Nowy_York"
  ]
  node [
    id 296
    label "Waszyngton"
  ]
  node [
    id 297
    label "Pensylwania"
  ]
  node [
    id 298
    label "wektor"
  ]
  node [
    id 299
    label "Hawaje"
  ]
  node [
    id 300
    label "state"
  ]
  node [
    id 301
    label "poziom"
  ]
  node [
    id 302
    label "jednostka_administracyjna"
  ]
  node [
    id 303
    label "Illinois"
  ]
  node [
    id 304
    label "Oklahoma"
  ]
  node [
    id 305
    label "Jukatan"
  ]
  node [
    id 306
    label "Arizona"
  ]
  node [
    id 307
    label "ilo&#347;&#263;"
  ]
  node [
    id 308
    label "Oregon"
  ]
  node [
    id 309
    label "shape"
  ]
  node [
    id 310
    label "Goa"
  ]
  node [
    id 311
    label "Pok&#233;mon"
  ]
  node [
    id 312
    label "boost"
  ]
  node [
    id 313
    label "pomoc"
  ]
  node [
    id 314
    label "morfing"
  ]
  node [
    id 315
    label "technika"
  ]
  node [
    id 316
    label "mechanika_precyzyjna"
  ]
  node [
    id 317
    label "technologia"
  ]
  node [
    id 318
    label "spos&#243;b"
  ]
  node [
    id 319
    label "fotowoltaika"
  ]
  node [
    id 320
    label "przedmiot"
  ]
  node [
    id 321
    label "cywilizacja"
  ]
  node [
    id 322
    label "telekomunikacja"
  ]
  node [
    id 323
    label "teletechnika"
  ]
  node [
    id 324
    label "wiedza"
  ]
  node [
    id 325
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 326
    label "engineering"
  ]
  node [
    id 327
    label "sprawno&#347;&#263;"
  ]
  node [
    id 328
    label "telefon_zaufania"
  ]
  node [
    id 329
    label "property"
  ]
  node [
    id 330
    label "&#347;rodek"
  ]
  node [
    id 331
    label "zgodzi&#263;"
  ]
  node [
    id 332
    label "darowizna"
  ]
  node [
    id 333
    label "pomocnik"
  ]
  node [
    id 334
    label "doch&#243;d"
  ]
  node [
    id 335
    label "przeobra&#380;anie"
  ]
  node [
    id 336
    label "tone"
  ]
  node [
    id 337
    label "uznawa&#263;"
  ]
  node [
    id 338
    label "submit"
  ]
  node [
    id 339
    label "authorize"
  ]
  node [
    id 340
    label "wchodzi&#263;"
  ]
  node [
    id 341
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 342
    label "przylega&#263;"
  ]
  node [
    id 343
    label "s&#261;siadowa&#263;"
  ]
  node [
    id 344
    label "przykleja&#263;_si&#281;"
  ]
  node [
    id 345
    label "bind"
  ]
  node [
    id 346
    label "fit"
  ]
  node [
    id 347
    label "stick"
  ]
  node [
    id 348
    label "dotyka&#263;"
  ]
  node [
    id 349
    label "consider"
  ]
  node [
    id 350
    label "przyznawa&#263;"
  ]
  node [
    id 351
    label "os&#261;dza&#263;"
  ]
  node [
    id 352
    label "stwierdza&#263;"
  ]
  node [
    id 353
    label "notice"
  ]
  node [
    id 354
    label "move"
  ]
  node [
    id 355
    label "przekracza&#263;"
  ]
  node [
    id 356
    label "&#322;oi&#263;"
  ]
  node [
    id 357
    label "wnika&#263;"
  ]
  node [
    id 358
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 359
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 360
    label "intervene"
  ]
  node [
    id 361
    label "spotyka&#263;"
  ]
  node [
    id 362
    label "mount"
  ]
  node [
    id 363
    label "invade"
  ]
  node [
    id 364
    label "scale"
  ]
  node [
    id 365
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 366
    label "go"
  ]
  node [
    id 367
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 368
    label "bra&#263;"
  ]
  node [
    id 369
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 370
    label "poznawa&#263;"
  ]
  node [
    id 371
    label "dochodzi&#263;"
  ]
  node [
    id 372
    label "atakowa&#263;"
  ]
  node [
    id 373
    label "zaczyna&#263;"
  ]
  node [
    id 374
    label "nast&#281;powa&#263;"
  ]
  node [
    id 375
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 376
    label "przenika&#263;"
  ]
  node [
    id 377
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 378
    label "zaziera&#263;"
  ]
  node [
    id 379
    label "summer"
  ]
  node [
    id 380
    label "chronometria"
  ]
  node [
    id 381
    label "odczyt"
  ]
  node [
    id 382
    label "laba"
  ]
  node [
    id 383
    label "czasoprzestrze&#324;"
  ]
  node [
    id 384
    label "time_period"
  ]
  node [
    id 385
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 386
    label "Zeitgeist"
  ]
  node [
    id 387
    label "pochodzenie"
  ]
  node [
    id 388
    label "przep&#322;ywanie"
  ]
  node [
    id 389
    label "schy&#322;ek"
  ]
  node [
    id 390
    label "czwarty_wymiar"
  ]
  node [
    id 391
    label "kategoria_gramatyczna"
  ]
  node [
    id 392
    label "poprzedzi&#263;"
  ]
  node [
    id 393
    label "pogoda"
  ]
  node [
    id 394
    label "czasokres"
  ]
  node [
    id 395
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 396
    label "poprzedzenie"
  ]
  node [
    id 397
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 398
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 399
    label "dzieje"
  ]
  node [
    id 400
    label "zegar"
  ]
  node [
    id 401
    label "koniugacja"
  ]
  node [
    id 402
    label "trawi&#263;"
  ]
  node [
    id 403
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 404
    label "poprzedza&#263;"
  ]
  node [
    id 405
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 406
    label "trawienie"
  ]
  node [
    id 407
    label "chwila"
  ]
  node [
    id 408
    label "rachuba_czasu"
  ]
  node [
    id 409
    label "poprzedzanie"
  ]
  node [
    id 410
    label "okres_czasu"
  ]
  node [
    id 411
    label "period"
  ]
  node [
    id 412
    label "odwlekanie_si&#281;"
  ]
  node [
    id 413
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 414
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 415
    label "pochodzi&#263;"
  ]
  node [
    id 416
    label "&#347;wintuch"
  ]
  node [
    id 417
    label "ryzykant"
  ]
  node [
    id 418
    label "pianie"
  ]
  node [
    id 419
    label "ko&#324;skie_zaloty"
  ]
  node [
    id 420
    label "lampa"
  ]
  node [
    id 421
    label "pia&#263;"
  ]
  node [
    id 422
    label "w&#322;osy"
  ]
  node [
    id 423
    label "sygna&#322;"
  ]
  node [
    id 424
    label "zapianie"
  ]
  node [
    id 425
    label "samiec"
  ]
  node [
    id 426
    label "zapia&#263;"
  ]
  node [
    id 427
    label "kura"
  ]
  node [
    id 428
    label "kurczak"
  ]
  node [
    id 429
    label "zadziora"
  ]
  node [
    id 430
    label "uwodziciel"
  ]
  node [
    id 431
    label "no&#347;no&#347;&#263;"
  ]
  node [
    id 432
    label "zagdaka&#263;"
  ]
  node [
    id 433
    label "dr&#243;b"
  ]
  node [
    id 434
    label "kur_bankiwa"
  ]
  node [
    id 435
    label "gdakni&#281;cie"
  ]
  node [
    id 436
    label "ptak"
  ]
  node [
    id 437
    label "gdakanie"
  ]
  node [
    id 438
    label "samica"
  ]
  node [
    id 439
    label "ptak_&#322;owny"
  ]
  node [
    id 440
    label "zwierz&#281;"
  ]
  node [
    id 441
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 442
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 443
    label "mi&#281;siwo"
  ]
  node [
    id 444
    label "m&#322;ode"
  ]
  node [
    id 445
    label "Casanova"
  ]
  node [
    id 446
    label "Don_Juan"
  ]
  node [
    id 447
    label "poku&#347;nik"
  ]
  node [
    id 448
    label "wyrywacz"
  ]
  node [
    id 449
    label "dewiant"
  ]
  node [
    id 450
    label "gorszyciel"
  ]
  node [
    id 451
    label "paskudziarz"
  ]
  node [
    id 452
    label "pulsation"
  ]
  node [
    id 453
    label "d&#378;wi&#281;k"
  ]
  node [
    id 454
    label "wizja"
  ]
  node [
    id 455
    label "point"
  ]
  node [
    id 456
    label "fala"
  ]
  node [
    id 457
    label "czynnik"
  ]
  node [
    id 458
    label "modulacja"
  ]
  node [
    id 459
    label "po&#322;&#261;czenie"
  ]
  node [
    id 460
    label "przewodzenie"
  ]
  node [
    id 461
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 462
    label "demodulacja"
  ]
  node [
    id 463
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 464
    label "medium_transmisyjne"
  ]
  node [
    id 465
    label "drift"
  ]
  node [
    id 466
    label "przekazywa&#263;"
  ]
  node [
    id 467
    label "przekazywanie"
  ]
  node [
    id 468
    label "aliasing"
  ]
  node [
    id 469
    label "znak"
  ]
  node [
    id 470
    label "przekazanie"
  ]
  node [
    id 471
    label "przewodzi&#263;"
  ]
  node [
    id 472
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 473
    label "doj&#347;&#263;"
  ]
  node [
    id 474
    label "przekaza&#263;"
  ]
  node [
    id 475
    label "iluminowa&#263;"
  ]
  node [
    id 476
    label "sztuczne_&#378;r&#243;d&#322;o_&#347;wiat&#322;a"
  ]
  node [
    id 477
    label "o&#347;wietla&#263;"
  ]
  node [
    id 478
    label "&#380;ar&#243;wka"
  ]
  node [
    id 479
    label "strzyc"
  ]
  node [
    id 480
    label "ostrzy&#380;enie"
  ]
  node [
    id 481
    label "brylantynowa&#263;"
  ]
  node [
    id 482
    label "posiwienie"
  ]
  node [
    id 483
    label "k&#281;dzierzawie&#263;"
  ]
  node [
    id 484
    label "goli&#263;"
  ]
  node [
    id 485
    label "mierzwienie_si&#281;"
  ]
  node [
    id 486
    label "przet&#322;uszczanie_si&#281;"
  ]
  node [
    id 487
    label "ogolenie"
  ]
  node [
    id 488
    label "fryzura"
  ]
  node [
    id 489
    label "mierzwienie"
  ]
  node [
    id 490
    label "golenie"
  ]
  node [
    id 491
    label "ostrzyc"
  ]
  node [
    id 492
    label "ogoli&#263;"
  ]
  node [
    id 493
    label "mierzwi&#263;_si&#281;"
  ]
  node [
    id 494
    label "zmierzwienie_si&#281;"
  ]
  node [
    id 495
    label "k&#281;dzierzawienie"
  ]
  node [
    id 496
    label "zmierzwi&#263;_si&#281;"
  ]
  node [
    id 497
    label "mierzwi&#263;"
  ]
  node [
    id 498
    label "posiwie&#263;"
  ]
  node [
    id 499
    label "przet&#322;uszcza&#263;_si&#281;"
  ]
  node [
    id 500
    label "strzy&#380;enie"
  ]
  node [
    id 501
    label "ow&#322;osienie"
  ]
  node [
    id 502
    label "brylantynowanie"
  ]
  node [
    id 503
    label "krepina"
  ]
  node [
    id 504
    label "rozrabiaka"
  ]
  node [
    id 505
    label "drzazga"
  ]
  node [
    id 506
    label "bur"
  ]
  node [
    id 507
    label "niespokojny_duch"
  ]
  node [
    id 508
    label "wydanie"
  ]
  node [
    id 509
    label "ba&#380;ant"
  ]
  node [
    id 510
    label "gaworzy&#263;"
  ]
  node [
    id 511
    label "gloat"
  ]
  node [
    id 512
    label "wys&#322;awia&#263;"
  ]
  node [
    id 513
    label "brag"
  ]
  node [
    id 514
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 515
    label "wydawanie"
  ]
  node [
    id 516
    label "pienie"
  ]
  node [
    id 517
    label "wys&#322;awianie"
  ]
  node [
    id 518
    label "policja"
  ]
  node [
    id 519
    label "formacja_paramilitarna"
  ]
  node [
    id 520
    label "armia"
  ]
  node [
    id 521
    label "psiarnia"
  ]
  node [
    id 522
    label "s&#322;u&#380;ba"
  ]
  node [
    id 523
    label "komisariat"
  ]
  node [
    id 524
    label "posterunek"
  ]
  node [
    id 525
    label "organ"
  ]
  node [
    id 526
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 527
    label "rezerwa"
  ]
  node [
    id 528
    label "werbowanie_si&#281;"
  ]
  node [
    id 529
    label "Eurokorpus"
  ]
  node [
    id 530
    label "Armia_Czerwona"
  ]
  node [
    id 531
    label "potencja"
  ]
  node [
    id 532
    label "soldateska"
  ]
  node [
    id 533
    label "pospolite_ruszenie"
  ]
  node [
    id 534
    label "kawaleria_powietrzna"
  ]
  node [
    id 535
    label "mobilizowa&#263;"
  ]
  node [
    id 536
    label "mobilizowanie"
  ]
  node [
    id 537
    label "military"
  ]
  node [
    id 538
    label "tabor"
  ]
  node [
    id 539
    label "zrejterowanie"
  ]
  node [
    id 540
    label "korpus"
  ]
  node [
    id 541
    label "zdemobilizowanie"
  ]
  node [
    id 542
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 543
    label "zmobilizowanie"
  ]
  node [
    id 544
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 545
    label "oddzia&#322;_karny"
  ]
  node [
    id 546
    label "or&#281;&#380;"
  ]
  node [
    id 547
    label "artyleria"
  ]
  node [
    id 548
    label "si&#322;a"
  ]
  node [
    id 549
    label "obrona"
  ]
  node [
    id 550
    label "bateria"
  ]
  node [
    id 551
    label "dywizjon_kawaleryjski"
  ]
  node [
    id 552
    label "Armia_Krajowa"
  ]
  node [
    id 553
    label "rzut"
  ]
  node [
    id 554
    label "brygada"
  ]
  node [
    id 555
    label "wermacht"
  ]
  node [
    id 556
    label "szlak_bojowy"
  ]
  node [
    id 557
    label "Czerwona_Gwardia"
  ]
  node [
    id 558
    label "wojska_pancerne"
  ]
  node [
    id 559
    label "zmobilizowa&#263;"
  ]
  node [
    id 560
    label "struktura"
  ]
  node [
    id 561
    label "legia"
  ]
  node [
    id 562
    label "zwi&#261;zek_operacyjny"
  ]
  node [
    id 563
    label "Legia_Cudzoziemska"
  ]
  node [
    id 564
    label "cofni&#281;cie"
  ]
  node [
    id 565
    label "dywizjon_artylerii"
  ]
  node [
    id 566
    label "zrejterowa&#263;"
  ]
  node [
    id 567
    label "t&#322;um"
  ]
  node [
    id 568
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 569
    label "zdemobilizowa&#263;"
  ]
  node [
    id 570
    label "wojsko"
  ]
  node [
    id 571
    label "rejterowa&#263;"
  ]
  node [
    id 572
    label "piechota"
  ]
  node [
    id 573
    label "rejterowanie"
  ]
  node [
    id 574
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 575
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 576
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 577
    label "teraz"
  ]
  node [
    id 578
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 579
    label "jednocze&#347;nie"
  ]
  node [
    id 580
    label "long_time"
  ]
  node [
    id 581
    label "tydzie&#324;"
  ]
  node [
    id 582
    label "noc"
  ]
  node [
    id 583
    label "dzie&#324;"
  ]
  node [
    id 584
    label "jednostka_geologiczna"
  ]
  node [
    id 585
    label "znak_pisarski"
  ]
  node [
    id 586
    label "notacja"
  ]
  node [
    id 587
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 588
    label "character"
  ]
  node [
    id 589
    label "brzoza_kr&#243;lewska"
  ]
  node [
    id 590
    label "wcielenie"
  ]
  node [
    id 591
    label "symbolizowanie"
  ]
  node [
    id 592
    label "model"
  ]
  node [
    id 593
    label "pasywa"
  ]
  node [
    id 594
    label "osoba"
  ]
  node [
    id 595
    label "wydarzenie"
  ]
  node [
    id 596
    label "involvement"
  ]
  node [
    id 597
    label "imposture"
  ]
  node [
    id 598
    label "podmiot_gospodarczy"
  ]
  node [
    id 599
    label "aktywa"
  ]
  node [
    id 600
    label "reinkarnacja"
  ]
  node [
    id 601
    label "zrobienie"
  ]
  node [
    id 602
    label "fuzja"
  ]
  node [
    id 603
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 604
    label "posta&#263;"
  ]
  node [
    id 605
    label "postawi&#263;"
  ]
  node [
    id 606
    label "mark"
  ]
  node [
    id 607
    label "kodzik"
  ]
  node [
    id 608
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 609
    label "oznakowanie"
  ]
  node [
    id 610
    label "implikowa&#263;"
  ]
  node [
    id 611
    label "attribute"
  ]
  node [
    id 612
    label "wytw&#243;r"
  ]
  node [
    id 613
    label "fakt"
  ]
  node [
    id 614
    label "dow&#243;d"
  ]
  node [
    id 615
    label "herb"
  ]
  node [
    id 616
    label "stawia&#263;"
  ]
  node [
    id 617
    label "znaczenie"
  ]
  node [
    id 618
    label "j&#281;zyk"
  ]
  node [
    id 619
    label "notation"
  ]
  node [
    id 620
    label "zapis"
  ]
  node [
    id 621
    label "program"
  ]
  node [
    id 622
    label "za&#322;o&#380;enie"
  ]
  node [
    id 623
    label "dzia&#322;"
  ]
  node [
    id 624
    label "odinstalowa&#263;"
  ]
  node [
    id 625
    label "spis"
  ]
  node [
    id 626
    label "broszura"
  ]
  node [
    id 627
    label "informatyka"
  ]
  node [
    id 628
    label "odinstalowywa&#263;"
  ]
  node [
    id 629
    label "furkacja"
  ]
  node [
    id 630
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 631
    label "ogranicznik_referencyjny"
  ]
  node [
    id 632
    label "oprogramowanie"
  ]
  node [
    id 633
    label "blok"
  ]
  node [
    id 634
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 635
    label "prezentowa&#263;"
  ]
  node [
    id 636
    label "emitowa&#263;"
  ]
  node [
    id 637
    label "kana&#322;"
  ]
  node [
    id 638
    label "sekcja_krytyczna"
  ]
  node [
    id 639
    label "pirat"
  ]
  node [
    id 640
    label "folder"
  ]
  node [
    id 641
    label "zaprezentowa&#263;"
  ]
  node [
    id 642
    label "course_of_study"
  ]
  node [
    id 643
    label "zainstalowa&#263;"
  ]
  node [
    id 644
    label "emitowanie"
  ]
  node [
    id 645
    label "teleferie"
  ]
  node [
    id 646
    label "podstawa"
  ]
  node [
    id 647
    label "deklaracja"
  ]
  node [
    id 648
    label "instrukcja"
  ]
  node [
    id 649
    label "zainstalowanie"
  ]
  node [
    id 650
    label "zaprezentowanie"
  ]
  node [
    id 651
    label "instalowa&#263;"
  ]
  node [
    id 652
    label "oferta"
  ]
  node [
    id 653
    label "odinstalowanie"
  ]
  node [
    id 654
    label "odinstalowywanie"
  ]
  node [
    id 655
    label "okno"
  ]
  node [
    id 656
    label "ram&#243;wka"
  ]
  node [
    id 657
    label "menu"
  ]
  node [
    id 658
    label "podprogram"
  ]
  node [
    id 659
    label "instalowanie"
  ]
  node [
    id 660
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 661
    label "booklet"
  ]
  node [
    id 662
    label "struktura_organizacyjna"
  ]
  node [
    id 663
    label "interfejs"
  ]
  node [
    id 664
    label "prezentowanie"
  ]
  node [
    id 665
    label "magazyn"
  ]
  node [
    id 666
    label "kryminalny"
  ]
  node [
    id 667
    label "997"
  ]
  node [
    id 668
    label "Jan"
  ]
  node [
    id 669
    label "P&#322;&#243;cienniczak"
  ]
  node [
    id 670
    label "bernard"
  ]
  node [
    id 671
    label "Margueritte"
  ]
  node [
    id 672
    label "zalesie"
  ]
  node [
    id 673
    label "g&#243;rny"
  ]
  node [
    id 674
    label "da&#263;"
  ]
  node [
    id 675
    label "&#380;y&#263;"
  ]
  node [
    id 676
    label "ojciec"
  ]
  node [
    id 677
    label "ludzie"
  ]
  node [
    id 678
    label "zaginiony"
  ]
  node [
    id 679
    label "komendant"
  ]
  node [
    id 680
    label "g&#322;&#243;wny"
  ]
  node [
    id 681
    label "Zenon"
  ]
  node [
    id 682
    label "Smolarek"
  ]
  node [
    id 683
    label "Micha&#322;"
  ]
  node [
    id 684
    label "Fajbusiewicz"
  ]
  node [
    id 685
    label "TVP"
  ]
  node [
    id 686
    label "info"
  ]
  node [
    id 687
    label "2"
  ]
  node [
    id 688
    label "mi"
  ]
  node [
    id 689
    label "8T"
  ]
  node [
    id 690
    label "103"
  ]
  node [
    id 691
    label "lotniczy"
  ]
  node [
    id 692
    label "ministerstwo"
  ]
  node [
    id 693
    label "sprawi&#263;"
  ]
  node [
    id 694
    label "wewn&#281;trzny"
  ]
  node [
    id 695
    label "nadwi&#347;la&#324;ski"
  ]
  node [
    id 696
    label "jednostka"
  ]
  node [
    id 697
    label "wojskowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 82
    target 690
  ]
  edge [
    source 82
    target 691
  ]
  edge [
    source 469
    target 674
  ]
  edge [
    source 469
    target 675
  ]
  edge [
    source 469
    target 676
  ]
  edge [
    source 469
    target 677
  ]
  edge [
    source 469
    target 678
  ]
  edge [
    source 665
    target 666
  ]
  edge [
    source 665
    target 667
  ]
  edge [
    source 666
    target 667
  ]
  edge [
    source 668
    target 669
  ]
  edge [
    source 670
    target 671
  ]
  edge [
    source 672
    target 673
  ]
  edge [
    source 674
    target 675
  ]
  edge [
    source 674
    target 676
  ]
  edge [
    source 674
    target 677
  ]
  edge [
    source 674
    target 678
  ]
  edge [
    source 675
    target 676
  ]
  edge [
    source 675
    target 677
  ]
  edge [
    source 675
    target 678
  ]
  edge [
    source 676
    target 677
  ]
  edge [
    source 676
    target 678
  ]
  edge [
    source 677
    target 678
  ]
  edge [
    source 679
    target 680
  ]
  edge [
    source 681
    target 682
  ]
  edge [
    source 683
    target 684
  ]
  edge [
    source 685
    target 686
  ]
  edge [
    source 685
    target 687
  ]
  edge [
    source 688
    target 689
  ]
  edge [
    source 690
    target 691
  ]
  edge [
    source 692
    target 693
  ]
  edge [
    source 692
    target 694
  ]
  edge [
    source 693
    target 694
  ]
  edge [
    source 695
    target 696
  ]
  edge [
    source 695
    target 697
  ]
  edge [
    source 696
    target 697
  ]
]
