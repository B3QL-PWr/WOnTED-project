graph [
  node [
    id 0
    label "dobra"
    origin "text"
  ]
  node [
    id 1
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "cham"
    origin "text"
  ]
  node [
    id 4
    label "Wilko"
  ]
  node [
    id 5
    label "centym"
  ]
  node [
    id 6
    label "frymark"
  ]
  node [
    id 7
    label "mienie"
  ]
  node [
    id 8
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 9
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 10
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 11
    label "jednostka_monetarna"
  ]
  node [
    id 12
    label "commodity"
  ]
  node [
    id 13
    label "liczba"
  ]
  node [
    id 14
    label "uk&#322;ad"
  ]
  node [
    id 15
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 16
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 17
    label "integer"
  ]
  node [
    id 18
    label "zlewanie_si&#281;"
  ]
  node [
    id 19
    label "ilo&#347;&#263;"
  ]
  node [
    id 20
    label "pe&#322;ny"
  ]
  node [
    id 21
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 22
    label "rzecz"
  ]
  node [
    id 23
    label "stan"
  ]
  node [
    id 24
    label "immoblizacja"
  ]
  node [
    id 25
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 26
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 27
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 28
    label "possession"
  ]
  node [
    id 29
    label "rodowo&#347;&#263;"
  ]
  node [
    id 30
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 31
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 32
    label "patent"
  ]
  node [
    id 33
    label "przej&#347;&#263;"
  ]
  node [
    id 34
    label "przej&#347;cie"
  ]
  node [
    id 35
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 36
    label "maj&#261;tek"
  ]
  node [
    id 37
    label "zamiana"
  ]
  node [
    id 38
    label "Iwaszkiewicz"
  ]
  node [
    id 39
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 40
    label "stand"
  ]
  node [
    id 41
    label "trwa&#263;"
  ]
  node [
    id 42
    label "equal"
  ]
  node [
    id 43
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 44
    label "chodzi&#263;"
  ]
  node [
    id 45
    label "uczestniczy&#263;"
  ]
  node [
    id 46
    label "obecno&#347;&#263;"
  ]
  node [
    id 47
    label "si&#281;ga&#263;"
  ]
  node [
    id 48
    label "mie&#263;_miejsce"
  ]
  node [
    id 49
    label "robi&#263;"
  ]
  node [
    id 50
    label "participate"
  ]
  node [
    id 51
    label "adhere"
  ]
  node [
    id 52
    label "pozostawa&#263;"
  ]
  node [
    id 53
    label "zostawa&#263;"
  ]
  node [
    id 54
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 55
    label "istnie&#263;"
  ]
  node [
    id 56
    label "compass"
  ]
  node [
    id 57
    label "exsert"
  ]
  node [
    id 58
    label "get"
  ]
  node [
    id 59
    label "u&#380;ywa&#263;"
  ]
  node [
    id 60
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 61
    label "osi&#261;ga&#263;"
  ]
  node [
    id 62
    label "korzysta&#263;"
  ]
  node [
    id 63
    label "appreciation"
  ]
  node [
    id 64
    label "dociera&#263;"
  ]
  node [
    id 65
    label "mierzy&#263;"
  ]
  node [
    id 66
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 67
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 68
    label "being"
  ]
  node [
    id 69
    label "cecha"
  ]
  node [
    id 70
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 71
    label "proceed"
  ]
  node [
    id 72
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 73
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 74
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 75
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 76
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 77
    label "str&#243;j"
  ]
  node [
    id 78
    label "para"
  ]
  node [
    id 79
    label "krok"
  ]
  node [
    id 80
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 81
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 82
    label "przebiega&#263;"
  ]
  node [
    id 83
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 84
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 85
    label "continue"
  ]
  node [
    id 86
    label "carry"
  ]
  node [
    id 87
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 88
    label "wk&#322;ada&#263;"
  ]
  node [
    id 89
    label "p&#322;ywa&#263;"
  ]
  node [
    id 90
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 91
    label "bangla&#263;"
  ]
  node [
    id 92
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 93
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 94
    label "bywa&#263;"
  ]
  node [
    id 95
    label "tryb"
  ]
  node [
    id 96
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 97
    label "dziama&#263;"
  ]
  node [
    id 98
    label "run"
  ]
  node [
    id 99
    label "stara&#263;_si&#281;"
  ]
  node [
    id 100
    label "Arakan"
  ]
  node [
    id 101
    label "Teksas"
  ]
  node [
    id 102
    label "Georgia"
  ]
  node [
    id 103
    label "Maryland"
  ]
  node [
    id 104
    label "warstwa"
  ]
  node [
    id 105
    label "Michigan"
  ]
  node [
    id 106
    label "Massachusetts"
  ]
  node [
    id 107
    label "Luizjana"
  ]
  node [
    id 108
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 109
    label "samopoczucie"
  ]
  node [
    id 110
    label "Floryda"
  ]
  node [
    id 111
    label "Ohio"
  ]
  node [
    id 112
    label "Alaska"
  ]
  node [
    id 113
    label "Nowy_Meksyk"
  ]
  node [
    id 114
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 115
    label "wci&#281;cie"
  ]
  node [
    id 116
    label "Kansas"
  ]
  node [
    id 117
    label "Alabama"
  ]
  node [
    id 118
    label "miejsce"
  ]
  node [
    id 119
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 120
    label "Kalifornia"
  ]
  node [
    id 121
    label "Wirginia"
  ]
  node [
    id 122
    label "punkt"
  ]
  node [
    id 123
    label "Nowy_York"
  ]
  node [
    id 124
    label "Waszyngton"
  ]
  node [
    id 125
    label "Pensylwania"
  ]
  node [
    id 126
    label "wektor"
  ]
  node [
    id 127
    label "Hawaje"
  ]
  node [
    id 128
    label "state"
  ]
  node [
    id 129
    label "poziom"
  ]
  node [
    id 130
    label "jednostka_administracyjna"
  ]
  node [
    id 131
    label "Illinois"
  ]
  node [
    id 132
    label "Oklahoma"
  ]
  node [
    id 133
    label "Oregon"
  ]
  node [
    id 134
    label "Arizona"
  ]
  node [
    id 135
    label "Jukatan"
  ]
  node [
    id 136
    label "shape"
  ]
  node [
    id 137
    label "Goa"
  ]
  node [
    id 138
    label "chamstwo"
  ]
  node [
    id 139
    label "ch&#322;op"
  ]
  node [
    id 140
    label "prostak"
  ]
  node [
    id 141
    label "chamski"
  ]
  node [
    id 142
    label "cz&#322;owiek"
  ]
  node [
    id 143
    label "istota_&#380;ywa"
  ]
  node [
    id 144
    label "schamienie"
  ]
  node [
    id 145
    label "bamber"
  ]
  node [
    id 146
    label "chamienie"
  ]
  node [
    id 147
    label "drobnomieszczanin"
  ]
  node [
    id 148
    label "kanciasty"
  ]
  node [
    id 149
    label "prostacki"
  ]
  node [
    id 150
    label "niegrzeczny"
  ]
  node [
    id 151
    label "prymitywizowanie"
  ]
  node [
    id 152
    label "niemi&#322;y"
  ]
  node [
    id 153
    label "chamsko"
  ]
  node [
    id 154
    label "pod&#322;o&#347;&#263;"
  ]
  node [
    id 155
    label "ch&#322;opstwo"
  ]
  node [
    id 156
    label "gminno&#347;&#263;"
  ]
  node [
    id 157
    label "niegrzeczno&#347;&#263;"
  ]
  node [
    id 158
    label "facet"
  ]
  node [
    id 159
    label "partner"
  ]
  node [
    id 160
    label "w&#322;o&#347;cianin"
  ]
  node [
    id 161
    label "prawo_wychodu"
  ]
  node [
    id 162
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 163
    label "uw&#322;aszczanie"
  ]
  node [
    id 164
    label "rolnik"
  ]
  node [
    id 165
    label "m&#261;&#380;"
  ]
  node [
    id 166
    label "przedstawiciel"
  ]
  node [
    id 167
    label "uw&#322;aszcza&#263;"
  ]
  node [
    id 168
    label "uw&#322;aszczy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
]
