graph [
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "prawda"
    origin "text"
  ]
  node [
    id 2
    label "s&#261;d"
  ]
  node [
    id 3
    label "za&#322;o&#380;enie"
  ]
  node [
    id 4
    label "nieprawdziwy"
  ]
  node [
    id 5
    label "prawdziwy"
  ]
  node [
    id 6
    label "truth"
  ]
  node [
    id 7
    label "realia"
  ]
  node [
    id 8
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 9
    label "message"
  ]
  node [
    id 10
    label "kontekst"
  ]
  node [
    id 11
    label "podwini&#281;cie"
  ]
  node [
    id 12
    label "zap&#322;acenie"
  ]
  node [
    id 13
    label "przyodzianie"
  ]
  node [
    id 14
    label "budowla"
  ]
  node [
    id 15
    label "pokrycie"
  ]
  node [
    id 16
    label "rozebranie"
  ]
  node [
    id 17
    label "zak&#322;adka"
  ]
  node [
    id 18
    label "struktura"
  ]
  node [
    id 19
    label "poubieranie"
  ]
  node [
    id 20
    label "infliction"
  ]
  node [
    id 21
    label "spowodowanie"
  ]
  node [
    id 22
    label "pozak&#322;adanie"
  ]
  node [
    id 23
    label "program"
  ]
  node [
    id 24
    label "przebranie"
  ]
  node [
    id 25
    label "przywdzianie"
  ]
  node [
    id 26
    label "obleczenie_si&#281;"
  ]
  node [
    id 27
    label "utworzenie"
  ]
  node [
    id 28
    label "str&#243;j"
  ]
  node [
    id 29
    label "twierdzenie"
  ]
  node [
    id 30
    label "obleczenie"
  ]
  node [
    id 31
    label "umieszczenie"
  ]
  node [
    id 32
    label "czynno&#347;&#263;"
  ]
  node [
    id 33
    label "przygotowywanie"
  ]
  node [
    id 34
    label "przymierzenie"
  ]
  node [
    id 35
    label "wyko&#324;czenie"
  ]
  node [
    id 36
    label "point"
  ]
  node [
    id 37
    label "przygotowanie"
  ]
  node [
    id 38
    label "proposition"
  ]
  node [
    id 39
    label "przewidzenie"
  ]
  node [
    id 40
    label "zrobienie"
  ]
  node [
    id 41
    label "zesp&#243;&#322;"
  ]
  node [
    id 42
    label "podejrzany"
  ]
  node [
    id 43
    label "s&#261;downictwo"
  ]
  node [
    id 44
    label "system"
  ]
  node [
    id 45
    label "biuro"
  ]
  node [
    id 46
    label "wytw&#243;r"
  ]
  node [
    id 47
    label "court"
  ]
  node [
    id 48
    label "forum"
  ]
  node [
    id 49
    label "bronienie"
  ]
  node [
    id 50
    label "urz&#261;d"
  ]
  node [
    id 51
    label "wydarzenie"
  ]
  node [
    id 52
    label "oskar&#380;yciel"
  ]
  node [
    id 53
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 54
    label "skazany"
  ]
  node [
    id 55
    label "post&#281;powanie"
  ]
  node [
    id 56
    label "broni&#263;"
  ]
  node [
    id 57
    label "my&#347;l"
  ]
  node [
    id 58
    label "pods&#261;dny"
  ]
  node [
    id 59
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 60
    label "obrona"
  ]
  node [
    id 61
    label "wypowied&#378;"
  ]
  node [
    id 62
    label "instytucja"
  ]
  node [
    id 63
    label "antylogizm"
  ]
  node [
    id 64
    label "konektyw"
  ]
  node [
    id 65
    label "&#347;wiadek"
  ]
  node [
    id 66
    label "procesowicz"
  ]
  node [
    id 67
    label "strona"
  ]
  node [
    id 68
    label "&#380;ywny"
  ]
  node [
    id 69
    label "szczery"
  ]
  node [
    id 70
    label "naturalny"
  ]
  node [
    id 71
    label "naprawd&#281;"
  ]
  node [
    id 72
    label "realnie"
  ]
  node [
    id 73
    label "podobny"
  ]
  node [
    id 74
    label "zgodny"
  ]
  node [
    id 75
    label "m&#261;dry"
  ]
  node [
    id 76
    label "prawdziwie"
  ]
  node [
    id 77
    label "nieprawdziwie"
  ]
  node [
    id 78
    label "niezgodny"
  ]
  node [
    id 79
    label "nierzeczywi&#347;cie"
  ]
  node [
    id 80
    label "udawany"
  ]
  node [
    id 81
    label "nieszczery"
  ]
  node [
    id 82
    label "niehistoryczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
]
