graph [
  node [
    id 0
    label "przyst&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "rozpatrzenie"
    origin "text"
  ]
  node [
    id 2
    label "punkt"
    origin "text"
  ]
  node [
    id 3
    label "porz&#261;dek"
    origin "text"
  ]
  node [
    id 4
    label "dzienny"
    origin "text"
  ]
  node [
    id 5
    label "sprawozdanie"
    origin "text"
  ]
  node [
    id 6
    label "komisja"
    origin "text"
  ]
  node [
    id 7
    label "infrastruktura"
    origin "text"
  ]
  node [
    id 8
    label "kultura"
    origin "text"
  ]
  node [
    id 9
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 10
    label "przekaz"
    origin "text"
  ]
  node [
    id 11
    label "poselski"
    origin "text"
  ]
  node [
    id 12
    label "projekt"
    origin "text"
  ]
  node [
    id 13
    label "uchwa&#322;a"
    origin "text"
  ]
  node [
    id 14
    label "sprawa"
    origin "text"
  ]
  node [
    id 15
    label "podj&#281;cie"
    origin "text"
  ]
  node [
    id 16
    label "przez"
    origin "text"
  ]
  node [
    id 17
    label "rada"
    origin "text"
  ]
  node [
    id 18
    label "minister"
    origin "text"
  ]
  node [
    id 19
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 20
    label "zmierza&#263;"
    origin "text"
  ]
  node [
    id 21
    label "wprowadzenie"
    origin "text"
  ]
  node [
    id 22
    label "naziemny"
    origin "text"
  ]
  node [
    id 23
    label "telewizja"
    origin "text"
  ]
  node [
    id 24
    label "cyfrowy"
    origin "text"
  ]
  node [
    id 25
    label "polska"
    origin "text"
  ]
  node [
    id 26
    label "druk"
    origin "text"
  ]
  node [
    id 27
    label "zaczyna&#263;"
  ]
  node [
    id 28
    label "set_about"
  ]
  node [
    id 29
    label "wchodzi&#263;"
  ]
  node [
    id 30
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 31
    label "submit"
  ]
  node [
    id 32
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 33
    label "zaziera&#263;"
  ]
  node [
    id 34
    label "move"
  ]
  node [
    id 35
    label "spotyka&#263;"
  ]
  node [
    id 36
    label "przenika&#263;"
  ]
  node [
    id 37
    label "osi&#261;ga&#263;"
  ]
  node [
    id 38
    label "nast&#281;powa&#263;"
  ]
  node [
    id 39
    label "mount"
  ]
  node [
    id 40
    label "bra&#263;"
  ]
  node [
    id 41
    label "go"
  ]
  node [
    id 42
    label "&#322;oi&#263;"
  ]
  node [
    id 43
    label "intervene"
  ]
  node [
    id 44
    label "scale"
  ]
  node [
    id 45
    label "poznawa&#263;"
  ]
  node [
    id 46
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 47
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 48
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 49
    label "dochodzi&#263;"
  ]
  node [
    id 50
    label "przekracza&#263;"
  ]
  node [
    id 51
    label "wnika&#263;"
  ]
  node [
    id 52
    label "atakowa&#263;"
  ]
  node [
    id 53
    label "invade"
  ]
  node [
    id 54
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 55
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 56
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 57
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 58
    label "odejmowa&#263;"
  ]
  node [
    id 59
    label "mie&#263;_miejsce"
  ]
  node [
    id 60
    label "bankrupt"
  ]
  node [
    id 61
    label "open"
  ]
  node [
    id 62
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 63
    label "begin"
  ]
  node [
    id 64
    label "post&#281;powa&#263;"
  ]
  node [
    id 65
    label "probe"
  ]
  node [
    id 66
    label "przemy&#347;lenie"
  ]
  node [
    id 67
    label "my&#347;l"
  ]
  node [
    id 68
    label "consideration"
  ]
  node [
    id 69
    label "pomy&#347;lenie"
  ]
  node [
    id 70
    label "namy&#347;lenie_si&#281;"
  ]
  node [
    id 71
    label "po&#322;o&#380;enie"
  ]
  node [
    id 72
    label "ust&#281;p"
  ]
  node [
    id 73
    label "plan"
  ]
  node [
    id 74
    label "obiekt_matematyczny"
  ]
  node [
    id 75
    label "problemat"
  ]
  node [
    id 76
    label "plamka"
  ]
  node [
    id 77
    label "stopie&#324;_pisma"
  ]
  node [
    id 78
    label "jednostka"
  ]
  node [
    id 79
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 80
    label "miejsce"
  ]
  node [
    id 81
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 82
    label "mark"
  ]
  node [
    id 83
    label "chwila"
  ]
  node [
    id 84
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 85
    label "prosta"
  ]
  node [
    id 86
    label "problematyka"
  ]
  node [
    id 87
    label "obiekt"
  ]
  node [
    id 88
    label "zapunktowa&#263;"
  ]
  node [
    id 89
    label "podpunkt"
  ]
  node [
    id 90
    label "wojsko"
  ]
  node [
    id 91
    label "kres"
  ]
  node [
    id 92
    label "przestrze&#324;"
  ]
  node [
    id 93
    label "point"
  ]
  node [
    id 94
    label "pozycja"
  ]
  node [
    id 95
    label "warunek_lokalowy"
  ]
  node [
    id 96
    label "plac"
  ]
  node [
    id 97
    label "location"
  ]
  node [
    id 98
    label "uwaga"
  ]
  node [
    id 99
    label "status"
  ]
  node [
    id 100
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 101
    label "cia&#322;o"
  ]
  node [
    id 102
    label "cecha"
  ]
  node [
    id 103
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 104
    label "praca"
  ]
  node [
    id 105
    label "rz&#261;d"
  ]
  node [
    id 106
    label "debit"
  ]
  node [
    id 107
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 108
    label "szata_graficzna"
  ]
  node [
    id 109
    label "wydawa&#263;"
  ]
  node [
    id 110
    label "szermierka"
  ]
  node [
    id 111
    label "spis"
  ]
  node [
    id 112
    label "wyda&#263;"
  ]
  node [
    id 113
    label "ustawienie"
  ]
  node [
    id 114
    label "publikacja"
  ]
  node [
    id 115
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 116
    label "adres"
  ]
  node [
    id 117
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 118
    label "rozmieszczenie"
  ]
  node [
    id 119
    label "sytuacja"
  ]
  node [
    id 120
    label "redaktor"
  ]
  node [
    id 121
    label "awansowa&#263;"
  ]
  node [
    id 122
    label "bearing"
  ]
  node [
    id 123
    label "znaczenie"
  ]
  node [
    id 124
    label "awans"
  ]
  node [
    id 125
    label "awansowanie"
  ]
  node [
    id 126
    label "poster"
  ]
  node [
    id 127
    label "le&#380;e&#263;"
  ]
  node [
    id 128
    label "przyswoi&#263;"
  ]
  node [
    id 129
    label "ludzko&#347;&#263;"
  ]
  node [
    id 130
    label "one"
  ]
  node [
    id 131
    label "poj&#281;cie"
  ]
  node [
    id 132
    label "ewoluowanie"
  ]
  node [
    id 133
    label "supremum"
  ]
  node [
    id 134
    label "skala"
  ]
  node [
    id 135
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 136
    label "przyswajanie"
  ]
  node [
    id 137
    label "wyewoluowanie"
  ]
  node [
    id 138
    label "reakcja"
  ]
  node [
    id 139
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 140
    label "przeliczy&#263;"
  ]
  node [
    id 141
    label "wyewoluowa&#263;"
  ]
  node [
    id 142
    label "ewoluowa&#263;"
  ]
  node [
    id 143
    label "matematyka"
  ]
  node [
    id 144
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 145
    label "rzut"
  ]
  node [
    id 146
    label "liczba_naturalna"
  ]
  node [
    id 147
    label "czynnik_biotyczny"
  ]
  node [
    id 148
    label "g&#322;owa"
  ]
  node [
    id 149
    label "figura"
  ]
  node [
    id 150
    label "individual"
  ]
  node [
    id 151
    label "portrecista"
  ]
  node [
    id 152
    label "przyswaja&#263;"
  ]
  node [
    id 153
    label "przyswojenie"
  ]
  node [
    id 154
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 155
    label "profanum"
  ]
  node [
    id 156
    label "mikrokosmos"
  ]
  node [
    id 157
    label "starzenie_si&#281;"
  ]
  node [
    id 158
    label "duch"
  ]
  node [
    id 159
    label "przeliczanie"
  ]
  node [
    id 160
    label "osoba"
  ]
  node [
    id 161
    label "oddzia&#322;ywanie"
  ]
  node [
    id 162
    label "antropochoria"
  ]
  node [
    id 163
    label "funkcja"
  ]
  node [
    id 164
    label "homo_sapiens"
  ]
  node [
    id 165
    label "przelicza&#263;"
  ]
  node [
    id 166
    label "infimum"
  ]
  node [
    id 167
    label "przeliczenie"
  ]
  node [
    id 168
    label "time"
  ]
  node [
    id 169
    label "czas"
  ]
  node [
    id 170
    label "przenocowanie"
  ]
  node [
    id 171
    label "pora&#380;ka"
  ]
  node [
    id 172
    label "nak&#322;adzenie"
  ]
  node [
    id 173
    label "pouk&#322;adanie"
  ]
  node [
    id 174
    label "pokrycie"
  ]
  node [
    id 175
    label "zepsucie"
  ]
  node [
    id 176
    label "spowodowanie"
  ]
  node [
    id 177
    label "trim"
  ]
  node [
    id 178
    label "ugoszczenie"
  ]
  node [
    id 179
    label "le&#380;enie"
  ]
  node [
    id 180
    label "zbudowanie"
  ]
  node [
    id 181
    label "umieszczenie"
  ]
  node [
    id 182
    label "reading"
  ]
  node [
    id 183
    label "czynno&#347;&#263;"
  ]
  node [
    id 184
    label "zabicie"
  ]
  node [
    id 185
    label "wygranie"
  ]
  node [
    id 186
    label "presentation"
  ]
  node [
    id 187
    label "passage"
  ]
  node [
    id 188
    label "toaleta"
  ]
  node [
    id 189
    label "fragment"
  ]
  node [
    id 190
    label "tekst"
  ]
  node [
    id 191
    label "artyku&#322;"
  ]
  node [
    id 192
    label "urywek"
  ]
  node [
    id 193
    label "co&#347;"
  ]
  node [
    id 194
    label "budynek"
  ]
  node [
    id 195
    label "thing"
  ]
  node [
    id 196
    label "program"
  ]
  node [
    id 197
    label "rzecz"
  ]
  node [
    id 198
    label "strona"
  ]
  node [
    id 199
    label "kognicja"
  ]
  node [
    id 200
    label "object"
  ]
  node [
    id 201
    label "rozprawa"
  ]
  node [
    id 202
    label "temat"
  ]
  node [
    id 203
    label "wydarzenie"
  ]
  node [
    id 204
    label "szczeg&#243;&#322;"
  ]
  node [
    id 205
    label "proposition"
  ]
  node [
    id 206
    label "przes&#322;anka"
  ]
  node [
    id 207
    label "idea"
  ]
  node [
    id 208
    label "rozdzielanie"
  ]
  node [
    id 209
    label "bezbrze&#380;e"
  ]
  node [
    id 210
    label "czasoprzestrze&#324;"
  ]
  node [
    id 211
    label "zbi&#243;r"
  ]
  node [
    id 212
    label "niezmierzony"
  ]
  node [
    id 213
    label "przedzielenie"
  ]
  node [
    id 214
    label "nielito&#347;ciwy"
  ]
  node [
    id 215
    label "rozdziela&#263;"
  ]
  node [
    id 216
    label "oktant"
  ]
  node [
    id 217
    label "przedzieli&#263;"
  ]
  node [
    id 218
    label "przestw&#243;r"
  ]
  node [
    id 219
    label "model"
  ]
  node [
    id 220
    label "intencja"
  ]
  node [
    id 221
    label "rysunek"
  ]
  node [
    id 222
    label "miejsce_pracy"
  ]
  node [
    id 223
    label "wytw&#243;r"
  ]
  node [
    id 224
    label "device"
  ]
  node [
    id 225
    label "pomys&#322;"
  ]
  node [
    id 226
    label "obraz"
  ]
  node [
    id 227
    label "reprezentacja"
  ]
  node [
    id 228
    label "agreement"
  ]
  node [
    id 229
    label "dekoracja"
  ]
  node [
    id 230
    label "perspektywa"
  ]
  node [
    id 231
    label "krzywa"
  ]
  node [
    id 232
    label "odcinek"
  ]
  node [
    id 233
    label "straight_line"
  ]
  node [
    id 234
    label "trasa"
  ]
  node [
    id 235
    label "proste_sko&#347;ne"
  ]
  node [
    id 236
    label "problem"
  ]
  node [
    id 237
    label "ostatnie_podrygi"
  ]
  node [
    id 238
    label "dzia&#322;anie"
  ]
  node [
    id 239
    label "koniec"
  ]
  node [
    id 240
    label "pokry&#263;"
  ]
  node [
    id 241
    label "farba"
  ]
  node [
    id 242
    label "zdoby&#263;"
  ]
  node [
    id 243
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 244
    label "zyska&#263;"
  ]
  node [
    id 245
    label "przymocowa&#263;"
  ]
  node [
    id 246
    label "zaskutkowa&#263;"
  ]
  node [
    id 247
    label "unieruchomi&#263;"
  ]
  node [
    id 248
    label "op&#322;aci&#263;_si&#281;"
  ]
  node [
    id 249
    label "zrejterowanie"
  ]
  node [
    id 250
    label "zmobilizowa&#263;"
  ]
  node [
    id 251
    label "przedmiot"
  ]
  node [
    id 252
    label "dezerter"
  ]
  node [
    id 253
    label "oddzia&#322;_karny"
  ]
  node [
    id 254
    label "rezerwa"
  ]
  node [
    id 255
    label "tabor"
  ]
  node [
    id 256
    label "wermacht"
  ]
  node [
    id 257
    label "cofni&#281;cie"
  ]
  node [
    id 258
    label "potencja"
  ]
  node [
    id 259
    label "fala"
  ]
  node [
    id 260
    label "struktura"
  ]
  node [
    id 261
    label "szko&#322;a"
  ]
  node [
    id 262
    label "korpus"
  ]
  node [
    id 263
    label "soldateska"
  ]
  node [
    id 264
    label "ods&#322;ugiwanie"
  ]
  node [
    id 265
    label "werbowanie_si&#281;"
  ]
  node [
    id 266
    label "zdemobilizowanie"
  ]
  node [
    id 267
    label "oddzia&#322;"
  ]
  node [
    id 268
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 269
    label "s&#322;u&#380;ba"
  ]
  node [
    id 270
    label "or&#281;&#380;"
  ]
  node [
    id 271
    label "Legia_Cudzoziemska"
  ]
  node [
    id 272
    label "Armia_Czerwona"
  ]
  node [
    id 273
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 274
    label "rejterowanie"
  ]
  node [
    id 275
    label "Czerwona_Gwardia"
  ]
  node [
    id 276
    label "si&#322;a"
  ]
  node [
    id 277
    label "zrejterowa&#263;"
  ]
  node [
    id 278
    label "sztabslekarz"
  ]
  node [
    id 279
    label "zmobilizowanie"
  ]
  node [
    id 280
    label "wojo"
  ]
  node [
    id 281
    label "pospolite_ruszenie"
  ]
  node [
    id 282
    label "Eurokorpus"
  ]
  node [
    id 283
    label "mobilizowanie"
  ]
  node [
    id 284
    label "rejterowa&#263;"
  ]
  node [
    id 285
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 286
    label "mobilizowa&#263;"
  ]
  node [
    id 287
    label "Armia_Krajowa"
  ]
  node [
    id 288
    label "obrona"
  ]
  node [
    id 289
    label "dryl"
  ]
  node [
    id 290
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 291
    label "petarda"
  ]
  node [
    id 292
    label "zdemobilizowa&#263;"
  ]
  node [
    id 293
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 294
    label "relacja"
  ]
  node [
    id 295
    label "uk&#322;ad"
  ]
  node [
    id 296
    label "stan"
  ]
  node [
    id 297
    label "zasada"
  ]
  node [
    id 298
    label "styl_architektoniczny"
  ]
  node [
    id 299
    label "normalizacja"
  ]
  node [
    id 300
    label "mechanika"
  ]
  node [
    id 301
    label "o&#347;"
  ]
  node [
    id 302
    label "usenet"
  ]
  node [
    id 303
    label "rozprz&#261;c"
  ]
  node [
    id 304
    label "zachowanie"
  ]
  node [
    id 305
    label "cybernetyk"
  ]
  node [
    id 306
    label "podsystem"
  ]
  node [
    id 307
    label "system"
  ]
  node [
    id 308
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 309
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 310
    label "sk&#322;ad"
  ]
  node [
    id 311
    label "systemat"
  ]
  node [
    id 312
    label "konstrukcja"
  ]
  node [
    id 313
    label "konstelacja"
  ]
  node [
    id 314
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 315
    label "ustosunkowywa&#263;"
  ]
  node [
    id 316
    label "wi&#261;zanie"
  ]
  node [
    id 317
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 318
    label "sprawko"
  ]
  node [
    id 319
    label "bratnia_dusza"
  ]
  node [
    id 320
    label "zwi&#261;zanie"
  ]
  node [
    id 321
    label "ustosunkowywanie"
  ]
  node [
    id 322
    label "marriage"
  ]
  node [
    id 323
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 324
    label "message"
  ]
  node [
    id 325
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 326
    label "ustosunkowa&#263;"
  ]
  node [
    id 327
    label "korespondent"
  ]
  node [
    id 328
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 329
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 330
    label "zwi&#261;za&#263;"
  ]
  node [
    id 331
    label "podzbi&#243;r"
  ]
  node [
    id 332
    label "ustosunkowanie"
  ]
  node [
    id 333
    label "wypowied&#378;"
  ]
  node [
    id 334
    label "zwi&#261;zek"
  ]
  node [
    id 335
    label "charakterystyka"
  ]
  node [
    id 336
    label "m&#322;ot"
  ]
  node [
    id 337
    label "znak"
  ]
  node [
    id 338
    label "drzewo"
  ]
  node [
    id 339
    label "pr&#243;ba"
  ]
  node [
    id 340
    label "attribute"
  ]
  node [
    id 341
    label "marka"
  ]
  node [
    id 342
    label "Ohio"
  ]
  node [
    id 343
    label "wci&#281;cie"
  ]
  node [
    id 344
    label "Nowy_York"
  ]
  node [
    id 345
    label "warstwa"
  ]
  node [
    id 346
    label "samopoczucie"
  ]
  node [
    id 347
    label "Illinois"
  ]
  node [
    id 348
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 349
    label "state"
  ]
  node [
    id 350
    label "Jukatan"
  ]
  node [
    id 351
    label "Kalifornia"
  ]
  node [
    id 352
    label "Wirginia"
  ]
  node [
    id 353
    label "wektor"
  ]
  node [
    id 354
    label "by&#263;"
  ]
  node [
    id 355
    label "Goa"
  ]
  node [
    id 356
    label "Teksas"
  ]
  node [
    id 357
    label "Waszyngton"
  ]
  node [
    id 358
    label "Massachusetts"
  ]
  node [
    id 359
    label "Alaska"
  ]
  node [
    id 360
    label "Arakan"
  ]
  node [
    id 361
    label "Hawaje"
  ]
  node [
    id 362
    label "Maryland"
  ]
  node [
    id 363
    label "Michigan"
  ]
  node [
    id 364
    label "Arizona"
  ]
  node [
    id 365
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 366
    label "Georgia"
  ]
  node [
    id 367
    label "poziom"
  ]
  node [
    id 368
    label "Pensylwania"
  ]
  node [
    id 369
    label "shape"
  ]
  node [
    id 370
    label "Luizjana"
  ]
  node [
    id 371
    label "Nowy_Meksyk"
  ]
  node [
    id 372
    label "Alabama"
  ]
  node [
    id 373
    label "ilo&#347;&#263;"
  ]
  node [
    id 374
    label "Kansas"
  ]
  node [
    id 375
    label "Oregon"
  ]
  node [
    id 376
    label "Oklahoma"
  ]
  node [
    id 377
    label "Floryda"
  ]
  node [
    id 378
    label "jednostka_administracyjna"
  ]
  node [
    id 379
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 380
    label "treaty"
  ]
  node [
    id 381
    label "umowa"
  ]
  node [
    id 382
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 383
    label "przestawi&#263;"
  ]
  node [
    id 384
    label "alliance"
  ]
  node [
    id 385
    label "ONZ"
  ]
  node [
    id 386
    label "NATO"
  ]
  node [
    id 387
    label "zawarcie"
  ]
  node [
    id 388
    label "zawrze&#263;"
  ]
  node [
    id 389
    label "organ"
  ]
  node [
    id 390
    label "wi&#281;&#378;"
  ]
  node [
    id 391
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 392
    label "traktat_wersalski"
  ]
  node [
    id 393
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 394
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 395
    label "regu&#322;a_Allena"
  ]
  node [
    id 396
    label "base"
  ]
  node [
    id 397
    label "obserwacja"
  ]
  node [
    id 398
    label "zasada_d'Alemberta"
  ]
  node [
    id 399
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 400
    label "moralno&#347;&#263;"
  ]
  node [
    id 401
    label "criterion"
  ]
  node [
    id 402
    label "opis"
  ]
  node [
    id 403
    label "regu&#322;a_Glogera"
  ]
  node [
    id 404
    label "prawo_Mendla"
  ]
  node [
    id 405
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 406
    label "twierdzenie"
  ]
  node [
    id 407
    label "prawo"
  ]
  node [
    id 408
    label "standard"
  ]
  node [
    id 409
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 410
    label "spos&#243;b"
  ]
  node [
    id 411
    label "dominion"
  ]
  node [
    id 412
    label "qualification"
  ]
  node [
    id 413
    label "occupation"
  ]
  node [
    id 414
    label "podstawa"
  ]
  node [
    id 415
    label "substancja"
  ]
  node [
    id 416
    label "prawid&#322;o"
  ]
  node [
    id 417
    label "calibration"
  ]
  node [
    id 418
    label "operacja"
  ]
  node [
    id 419
    label "proces"
  ]
  node [
    id 420
    label "dominance"
  ]
  node [
    id 421
    label "zabieg"
  ]
  node [
    id 422
    label "standardization"
  ]
  node [
    id 423
    label "zmiana"
  ]
  node [
    id 424
    label "kilkudziesi&#281;ciogodzinny"
  ]
  node [
    id 425
    label "typowy"
  ]
  node [
    id 426
    label "stacjonarnie"
  ]
  node [
    id 427
    label "student"
  ]
  node [
    id 428
    label "specjalny"
  ]
  node [
    id 429
    label "stacjonarny"
  ]
  node [
    id 430
    label "nieruchomy"
  ]
  node [
    id 431
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 432
    label "zwyczajny"
  ]
  node [
    id 433
    label "typowo"
  ]
  node [
    id 434
    label "cz&#281;sty"
  ]
  node [
    id 435
    label "zwyk&#322;y"
  ]
  node [
    id 436
    label "indeks"
  ]
  node [
    id 437
    label "s&#322;uchacz"
  ]
  node [
    id 438
    label "immatrykulowanie"
  ]
  node [
    id 439
    label "absolwent"
  ]
  node [
    id 440
    label "immatrykulowa&#263;"
  ]
  node [
    id 441
    label "akademik"
  ]
  node [
    id 442
    label "tutor"
  ]
  node [
    id 443
    label "intencjonalny"
  ]
  node [
    id 444
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 445
    label "niedorozw&#243;j"
  ]
  node [
    id 446
    label "szczeg&#243;lny"
  ]
  node [
    id 447
    label "specjalnie"
  ]
  node [
    id 448
    label "nieetatowy"
  ]
  node [
    id 449
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 450
    label "nienormalny"
  ]
  node [
    id 451
    label "umy&#347;lnie"
  ]
  node [
    id 452
    label "odpowiedni"
  ]
  node [
    id 453
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 454
    label "pos&#322;uchanie"
  ]
  node [
    id 455
    label "s&#261;d"
  ]
  node [
    id 456
    label "sparafrazowanie"
  ]
  node [
    id 457
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 458
    label "strawestowa&#263;"
  ]
  node [
    id 459
    label "sparafrazowa&#263;"
  ]
  node [
    id 460
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 461
    label "trawestowa&#263;"
  ]
  node [
    id 462
    label "sformu&#322;owanie"
  ]
  node [
    id 463
    label "parafrazowanie"
  ]
  node [
    id 464
    label "ozdobnik"
  ]
  node [
    id 465
    label "delimitacja"
  ]
  node [
    id 466
    label "parafrazowa&#263;"
  ]
  node [
    id 467
    label "stylizacja"
  ]
  node [
    id 468
    label "komunikat"
  ]
  node [
    id 469
    label "trawestowanie"
  ]
  node [
    id 470
    label "strawestowanie"
  ]
  node [
    id 471
    label "rezultat"
  ]
  node [
    id 472
    label "reporter"
  ]
  node [
    id 473
    label "podkomisja"
  ]
  node [
    id 474
    label "zesp&#243;&#322;"
  ]
  node [
    id 475
    label "obrady"
  ]
  node [
    id 476
    label "Komisja_Europejska"
  ]
  node [
    id 477
    label "dyskusja"
  ]
  node [
    id 478
    label "conference"
  ]
  node [
    id 479
    label "konsylium"
  ]
  node [
    id 480
    label "Mazowsze"
  ]
  node [
    id 481
    label "odm&#322;adzanie"
  ]
  node [
    id 482
    label "&#346;wietliki"
  ]
  node [
    id 483
    label "whole"
  ]
  node [
    id 484
    label "skupienie"
  ]
  node [
    id 485
    label "The_Beatles"
  ]
  node [
    id 486
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 487
    label "odm&#322;adza&#263;"
  ]
  node [
    id 488
    label "zabudowania"
  ]
  node [
    id 489
    label "group"
  ]
  node [
    id 490
    label "zespolik"
  ]
  node [
    id 491
    label "schorzenie"
  ]
  node [
    id 492
    label "ro&#347;lina"
  ]
  node [
    id 493
    label "grupa"
  ]
  node [
    id 494
    label "Depeche_Mode"
  ]
  node [
    id 495
    label "batch"
  ]
  node [
    id 496
    label "odm&#322;odzenie"
  ]
  node [
    id 497
    label "tkanka"
  ]
  node [
    id 498
    label "jednostka_organizacyjna"
  ]
  node [
    id 499
    label "budowa"
  ]
  node [
    id 500
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 501
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 502
    label "tw&#243;r"
  ]
  node [
    id 503
    label "organogeneza"
  ]
  node [
    id 504
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 505
    label "struktura_anatomiczna"
  ]
  node [
    id 506
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 507
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 508
    label "Izba_Konsyliarska"
  ]
  node [
    id 509
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 510
    label "stomia"
  ]
  node [
    id 511
    label "dekortykacja"
  ]
  node [
    id 512
    label "okolica"
  ]
  node [
    id 513
    label "Komitet_Region&#243;w"
  ]
  node [
    id 514
    label "subcommittee"
  ]
  node [
    id 515
    label "zaplecze"
  ]
  node [
    id 516
    label "radiofonia"
  ]
  node [
    id 517
    label "telefonia"
  ]
  node [
    id 518
    label "radio"
  ]
  node [
    id 519
    label "radiokomunikacja"
  ]
  node [
    id 520
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 521
    label "radiofonizacja"
  ]
  node [
    id 522
    label "telekomunikacja"
  ]
  node [
    id 523
    label "provider"
  ]
  node [
    id 524
    label "instalacja"
  ]
  node [
    id 525
    label "telephone"
  ]
  node [
    id 526
    label "droga"
  ]
  node [
    id 527
    label "przebieg"
  ]
  node [
    id 528
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 529
    label "w&#281;ze&#322;"
  ]
  node [
    id 530
    label "marszrutyzacja"
  ]
  node [
    id 531
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 532
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 533
    label "podbieg"
  ]
  node [
    id 534
    label "sklep"
  ]
  node [
    id 535
    label "wyposa&#380;enie"
  ]
  node [
    id 536
    label "pomieszczenie"
  ]
  node [
    id 537
    label "asymilowanie_si&#281;"
  ]
  node [
    id 538
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 539
    label "Wsch&#243;d"
  ]
  node [
    id 540
    label "praca_rolnicza"
  ]
  node [
    id 541
    label "przejmowanie"
  ]
  node [
    id 542
    label "zjawisko"
  ]
  node [
    id 543
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 544
    label "makrokosmos"
  ]
  node [
    id 545
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 546
    label "konwencja"
  ]
  node [
    id 547
    label "propriety"
  ]
  node [
    id 548
    label "przejmowa&#263;"
  ]
  node [
    id 549
    label "brzoskwiniarnia"
  ]
  node [
    id 550
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 551
    label "sztuka"
  ]
  node [
    id 552
    label "zwyczaj"
  ]
  node [
    id 553
    label "jako&#347;&#263;"
  ]
  node [
    id 554
    label "kuchnia"
  ]
  node [
    id 555
    label "tradycja"
  ]
  node [
    id 556
    label "populace"
  ]
  node [
    id 557
    label "hodowla"
  ]
  node [
    id 558
    label "religia"
  ]
  node [
    id 559
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 560
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 561
    label "przej&#281;cie"
  ]
  node [
    id 562
    label "przej&#261;&#263;"
  ]
  node [
    id 563
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 564
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 565
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 566
    label "warto&#347;&#263;"
  ]
  node [
    id 567
    label "quality"
  ]
  node [
    id 568
    label "syf"
  ]
  node [
    id 569
    label "absolutorium"
  ]
  node [
    id 570
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 571
    label "activity"
  ]
  node [
    id 572
    label "boski"
  ]
  node [
    id 573
    label "krajobraz"
  ]
  node [
    id 574
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 575
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 576
    label "przywidzenie"
  ]
  node [
    id 577
    label "presence"
  ]
  node [
    id 578
    label "charakter"
  ]
  node [
    id 579
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 580
    label "potrzymanie"
  ]
  node [
    id 581
    label "rolnictwo"
  ]
  node [
    id 582
    label "pod&#243;j"
  ]
  node [
    id 583
    label "filiacja"
  ]
  node [
    id 584
    label "licencjonowanie"
  ]
  node [
    id 585
    label "opasa&#263;"
  ]
  node [
    id 586
    label "ch&#243;w"
  ]
  node [
    id 587
    label "licencja"
  ]
  node [
    id 588
    label "sokolarnia"
  ]
  node [
    id 589
    label "potrzyma&#263;"
  ]
  node [
    id 590
    label "rozp&#322;&#243;d"
  ]
  node [
    id 591
    label "grupa_organizm&#243;w"
  ]
  node [
    id 592
    label "wypas"
  ]
  node [
    id 593
    label "wychowalnia"
  ]
  node [
    id 594
    label "pstr&#261;garnia"
  ]
  node [
    id 595
    label "krzy&#380;owanie"
  ]
  node [
    id 596
    label "licencjonowa&#263;"
  ]
  node [
    id 597
    label "odch&#243;w"
  ]
  node [
    id 598
    label "tucz"
  ]
  node [
    id 599
    label "ud&#243;j"
  ]
  node [
    id 600
    label "klatka"
  ]
  node [
    id 601
    label "opasienie"
  ]
  node [
    id 602
    label "wych&#243;w"
  ]
  node [
    id 603
    label "obrz&#261;dek"
  ]
  node [
    id 604
    label "opasanie"
  ]
  node [
    id 605
    label "polish"
  ]
  node [
    id 606
    label "akwarium"
  ]
  node [
    id 607
    label "biotechnika"
  ]
  node [
    id 608
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 609
    label "styl"
  ]
  node [
    id 610
    label "line"
  ]
  node [
    id 611
    label "kanon"
  ]
  node [
    id 612
    label "zjazd"
  ]
  node [
    id 613
    label "biom"
  ]
  node [
    id 614
    label "szata_ro&#347;linna"
  ]
  node [
    id 615
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 616
    label "formacja_ro&#347;linna"
  ]
  node [
    id 617
    label "przyroda"
  ]
  node [
    id 618
    label "zielono&#347;&#263;"
  ]
  node [
    id 619
    label "pi&#281;tro"
  ]
  node [
    id 620
    label "plant"
  ]
  node [
    id 621
    label "geosystem"
  ]
  node [
    id 622
    label "kult"
  ]
  node [
    id 623
    label "mitologia"
  ]
  node [
    id 624
    label "wyznanie"
  ]
  node [
    id 625
    label "ideologia"
  ]
  node [
    id 626
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 627
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 628
    label "nawracanie_si&#281;"
  ]
  node [
    id 629
    label "duchowny"
  ]
  node [
    id 630
    label "rela"
  ]
  node [
    id 631
    label "kultura_duchowa"
  ]
  node [
    id 632
    label "kosmologia"
  ]
  node [
    id 633
    label "kosmogonia"
  ]
  node [
    id 634
    label "nawraca&#263;"
  ]
  node [
    id 635
    label "mistyka"
  ]
  node [
    id 636
    label "pr&#243;bowanie"
  ]
  node [
    id 637
    label "rola"
  ]
  node [
    id 638
    label "cz&#322;owiek"
  ]
  node [
    id 639
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 640
    label "realizacja"
  ]
  node [
    id 641
    label "scena"
  ]
  node [
    id 642
    label "didaskalia"
  ]
  node [
    id 643
    label "czyn"
  ]
  node [
    id 644
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 645
    label "environment"
  ]
  node [
    id 646
    label "head"
  ]
  node [
    id 647
    label "scenariusz"
  ]
  node [
    id 648
    label "egzemplarz"
  ]
  node [
    id 649
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 650
    label "utw&#243;r"
  ]
  node [
    id 651
    label "fortel"
  ]
  node [
    id 652
    label "theatrical_performance"
  ]
  node [
    id 653
    label "ambala&#380;"
  ]
  node [
    id 654
    label "sprawno&#347;&#263;"
  ]
  node [
    id 655
    label "kobieta"
  ]
  node [
    id 656
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 657
    label "Faust"
  ]
  node [
    id 658
    label "scenografia"
  ]
  node [
    id 659
    label "ods&#322;ona"
  ]
  node [
    id 660
    label "turn"
  ]
  node [
    id 661
    label "pokaz"
  ]
  node [
    id 662
    label "przedstawienie"
  ]
  node [
    id 663
    label "przedstawi&#263;"
  ]
  node [
    id 664
    label "Apollo"
  ]
  node [
    id 665
    label "przedstawianie"
  ]
  node [
    id 666
    label "przedstawia&#263;"
  ]
  node [
    id 667
    label "towar"
  ]
  node [
    id 668
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 669
    label "ceremony"
  ]
  node [
    id 670
    label "dorobek"
  ]
  node [
    id 671
    label "tworzenie"
  ]
  node [
    id 672
    label "kreacja"
  ]
  node [
    id 673
    label "creation"
  ]
  node [
    id 674
    label "staro&#347;cina_weselna"
  ]
  node [
    id 675
    label "folklor"
  ]
  node [
    id 676
    label "objawienie"
  ]
  node [
    id 677
    label "zaj&#281;cie"
  ]
  node [
    id 678
    label "instytucja"
  ]
  node [
    id 679
    label "tajniki"
  ]
  node [
    id 680
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 681
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 682
    label "jedzenie"
  ]
  node [
    id 683
    label "zlewozmywak"
  ]
  node [
    id 684
    label "gotowa&#263;"
  ]
  node [
    id 685
    label "ciemna_materia"
  ]
  node [
    id 686
    label "planeta"
  ]
  node [
    id 687
    label "ekosfera"
  ]
  node [
    id 688
    label "czarna_dziura"
  ]
  node [
    id 689
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 690
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 691
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 692
    label "kosmos"
  ]
  node [
    id 693
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 694
    label "poprawno&#347;&#263;"
  ]
  node [
    id 695
    label "og&#322;ada"
  ]
  node [
    id 696
    label "service"
  ]
  node [
    id 697
    label "stosowno&#347;&#263;"
  ]
  node [
    id 698
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 699
    label "Ukraina"
  ]
  node [
    id 700
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 701
    label "blok_wschodni"
  ]
  node [
    id 702
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 703
    label "wsch&#243;d"
  ]
  node [
    id 704
    label "Europa_Wschodnia"
  ]
  node [
    id 705
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 706
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 707
    label "wra&#380;enie"
  ]
  node [
    id 708
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 709
    label "interception"
  ]
  node [
    id 710
    label "wzbudzenie"
  ]
  node [
    id 711
    label "emotion"
  ]
  node [
    id 712
    label "movement"
  ]
  node [
    id 713
    label "zaczerpni&#281;cie"
  ]
  node [
    id 714
    label "wzi&#281;cie"
  ]
  node [
    id 715
    label "bang"
  ]
  node [
    id 716
    label "wzi&#261;&#263;"
  ]
  node [
    id 717
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 718
    label "stimulate"
  ]
  node [
    id 719
    label "ogarn&#261;&#263;"
  ]
  node [
    id 720
    label "wzbudzi&#263;"
  ]
  node [
    id 721
    label "thrill"
  ]
  node [
    id 722
    label "treat"
  ]
  node [
    id 723
    label "czerpa&#263;"
  ]
  node [
    id 724
    label "handle"
  ]
  node [
    id 725
    label "wzbudza&#263;"
  ]
  node [
    id 726
    label "ogarnia&#263;"
  ]
  node [
    id 727
    label "czerpanie"
  ]
  node [
    id 728
    label "acquisition"
  ]
  node [
    id 729
    label "branie"
  ]
  node [
    id 730
    label "caparison"
  ]
  node [
    id 731
    label "wzbudzanie"
  ]
  node [
    id 732
    label "ogarnianie"
  ]
  node [
    id 733
    label "wpadni&#281;cie"
  ]
  node [
    id 734
    label "mienie"
  ]
  node [
    id 735
    label "istota"
  ]
  node [
    id 736
    label "wpa&#347;&#263;"
  ]
  node [
    id 737
    label "wpadanie"
  ]
  node [
    id 738
    label "wpada&#263;"
  ]
  node [
    id 739
    label "zboczenie"
  ]
  node [
    id 740
    label "om&#243;wienie"
  ]
  node [
    id 741
    label "sponiewieranie"
  ]
  node [
    id 742
    label "discipline"
  ]
  node [
    id 743
    label "omawia&#263;"
  ]
  node [
    id 744
    label "kr&#261;&#380;enie"
  ]
  node [
    id 745
    label "tre&#347;&#263;"
  ]
  node [
    id 746
    label "robienie"
  ]
  node [
    id 747
    label "sponiewiera&#263;"
  ]
  node [
    id 748
    label "element"
  ]
  node [
    id 749
    label "entity"
  ]
  node [
    id 750
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 751
    label "tematyka"
  ]
  node [
    id 752
    label "w&#261;tek"
  ]
  node [
    id 753
    label "zbaczanie"
  ]
  node [
    id 754
    label "program_nauczania"
  ]
  node [
    id 755
    label "om&#243;wi&#263;"
  ]
  node [
    id 756
    label "omawianie"
  ]
  node [
    id 757
    label "zbacza&#263;"
  ]
  node [
    id 758
    label "zboczy&#263;"
  ]
  node [
    id 759
    label "uprawa"
  ]
  node [
    id 760
    label "abstrakcja"
  ]
  node [
    id 761
    label "chemikalia"
  ]
  node [
    id 762
    label "poprzedzanie"
  ]
  node [
    id 763
    label "laba"
  ]
  node [
    id 764
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 765
    label "chronometria"
  ]
  node [
    id 766
    label "rachuba_czasu"
  ]
  node [
    id 767
    label "przep&#322;ywanie"
  ]
  node [
    id 768
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 769
    label "czasokres"
  ]
  node [
    id 770
    label "odczyt"
  ]
  node [
    id 771
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 772
    label "dzieje"
  ]
  node [
    id 773
    label "kategoria_gramatyczna"
  ]
  node [
    id 774
    label "poprzedzenie"
  ]
  node [
    id 775
    label "trawienie"
  ]
  node [
    id 776
    label "pochodzi&#263;"
  ]
  node [
    id 777
    label "period"
  ]
  node [
    id 778
    label "okres_czasu"
  ]
  node [
    id 779
    label "poprzedza&#263;"
  ]
  node [
    id 780
    label "schy&#322;ek"
  ]
  node [
    id 781
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 782
    label "odwlekanie_si&#281;"
  ]
  node [
    id 783
    label "zegar"
  ]
  node [
    id 784
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 785
    label "czwarty_wymiar"
  ]
  node [
    id 786
    label "pochodzenie"
  ]
  node [
    id 787
    label "koniugacja"
  ]
  node [
    id 788
    label "Zeitgeist"
  ]
  node [
    id 789
    label "trawi&#263;"
  ]
  node [
    id 790
    label "pogoda"
  ]
  node [
    id 791
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 792
    label "poprzedzi&#263;"
  ]
  node [
    id 793
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 794
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 795
    label "time_period"
  ]
  node [
    id 796
    label "narz&#281;dzie"
  ]
  node [
    id 797
    label "tryb"
  ]
  node [
    id 798
    label "nature"
  ]
  node [
    id 799
    label "przenikanie"
  ]
  node [
    id 800
    label "byt"
  ]
  node [
    id 801
    label "materia"
  ]
  node [
    id 802
    label "cz&#261;steczka"
  ]
  node [
    id 803
    label "temperatura_krytyczna"
  ]
  node [
    id 804
    label "smolisty"
  ]
  node [
    id 805
    label "proces_my&#347;lowy"
  ]
  node [
    id 806
    label "abstractedness"
  ]
  node [
    id 807
    label "abstraction"
  ]
  node [
    id 808
    label "spalenie"
  ]
  node [
    id 809
    label "spalanie"
  ]
  node [
    id 810
    label "kwota"
  ]
  node [
    id 811
    label "explicite"
  ]
  node [
    id 812
    label "blankiet"
  ]
  node [
    id 813
    label "draft"
  ]
  node [
    id 814
    label "transakcja"
  ]
  node [
    id 815
    label "implicite"
  ]
  node [
    id 816
    label "dokument"
  ]
  node [
    id 817
    label "order"
  ]
  node [
    id 818
    label "odk&#322;adanie"
  ]
  node [
    id 819
    label "condition"
  ]
  node [
    id 820
    label "liczenie"
  ]
  node [
    id 821
    label "stawianie"
  ]
  node [
    id 822
    label "bycie"
  ]
  node [
    id 823
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 824
    label "assay"
  ]
  node [
    id 825
    label "wskazywanie"
  ]
  node [
    id 826
    label "wyraz"
  ]
  node [
    id 827
    label "gravity"
  ]
  node [
    id 828
    label "weight"
  ]
  node [
    id 829
    label "command"
  ]
  node [
    id 830
    label "odgrywanie_roli"
  ]
  node [
    id 831
    label "informacja"
  ]
  node [
    id 832
    label "okre&#347;lanie"
  ]
  node [
    id 833
    label "kto&#347;"
  ]
  node [
    id 834
    label "wyra&#380;enie"
  ]
  node [
    id 835
    label "ekscerpcja"
  ]
  node [
    id 836
    label "j&#281;zykowo"
  ]
  node [
    id 837
    label "redakcja"
  ]
  node [
    id 838
    label "pomini&#281;cie"
  ]
  node [
    id 839
    label "dzie&#322;o"
  ]
  node [
    id 840
    label "preparacja"
  ]
  node [
    id 841
    label "odmianka"
  ]
  node [
    id 842
    label "opu&#347;ci&#263;"
  ]
  node [
    id 843
    label "koniektura"
  ]
  node [
    id 844
    label "pisa&#263;"
  ]
  node [
    id 845
    label "obelga"
  ]
  node [
    id 846
    label "zapis"
  ]
  node [
    id 847
    label "&#347;wiadectwo"
  ]
  node [
    id 848
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 849
    label "parafa"
  ]
  node [
    id 850
    label "plik"
  ]
  node [
    id 851
    label "raport&#243;wka"
  ]
  node [
    id 852
    label "record"
  ]
  node [
    id 853
    label "registratura"
  ]
  node [
    id 854
    label "dokumentacja"
  ]
  node [
    id 855
    label "fascyku&#322;"
  ]
  node [
    id 856
    label "writing"
  ]
  node [
    id 857
    label "sygnatariusz"
  ]
  node [
    id 858
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 859
    label "zam&#243;wienie"
  ]
  node [
    id 860
    label "cena_transferowa"
  ]
  node [
    id 861
    label "arbitra&#380;"
  ]
  node [
    id 862
    label "kontrakt_terminowy"
  ]
  node [
    id 863
    label "facjenda"
  ]
  node [
    id 864
    label "lacuna"
  ]
  node [
    id 865
    label "tabela"
  ]
  node [
    id 866
    label "booklet"
  ]
  node [
    id 867
    label "wynie&#347;&#263;"
  ]
  node [
    id 868
    label "pieni&#261;dze"
  ]
  node [
    id 869
    label "limit"
  ]
  node [
    id 870
    label "wynosi&#263;"
  ]
  node [
    id 871
    label "legislacyjnie"
  ]
  node [
    id 872
    label "nast&#281;pstwo"
  ]
  node [
    id 873
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 874
    label "komunikacja"
  ]
  node [
    id 875
    label "bezpo&#347;rednio"
  ]
  node [
    id 876
    label "po&#347;rednio"
  ]
  node [
    id 877
    label "wyb&#243;r"
  ]
  node [
    id 878
    label "wydruk"
  ]
  node [
    id 879
    label "odznaka"
  ]
  node [
    id 880
    label "kawaler"
  ]
  node [
    id 881
    label "program_u&#380;ytkowy"
  ]
  node [
    id 882
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 883
    label "thinking"
  ]
  node [
    id 884
    label "materia&#322;"
  ]
  node [
    id 885
    label "operat"
  ]
  node [
    id 886
    label "kosztorys"
  ]
  node [
    id 887
    label "pocz&#261;tki"
  ]
  node [
    id 888
    label "ukra&#347;&#263;"
  ]
  node [
    id 889
    label "ukradzenie"
  ]
  node [
    id 890
    label "resolution"
  ]
  node [
    id 891
    label "akt"
  ]
  node [
    id 892
    label "podnieci&#263;"
  ]
  node [
    id 893
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 894
    label "numer"
  ]
  node [
    id 895
    label "po&#380;ycie"
  ]
  node [
    id 896
    label "podniecenie"
  ]
  node [
    id 897
    label "nago&#347;&#263;"
  ]
  node [
    id 898
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 899
    label "seks"
  ]
  node [
    id 900
    label "podniecanie"
  ]
  node [
    id 901
    label "imisja"
  ]
  node [
    id 902
    label "rozmna&#380;anie"
  ]
  node [
    id 903
    label "ruch_frykcyjny"
  ]
  node [
    id 904
    label "ontologia"
  ]
  node [
    id 905
    label "na_pieska"
  ]
  node [
    id 906
    label "pozycja_misjonarska"
  ]
  node [
    id 907
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 908
    label "z&#322;&#261;czenie"
  ]
  node [
    id 909
    label "gra_wst&#281;pna"
  ]
  node [
    id 910
    label "erotyka"
  ]
  node [
    id 911
    label "urzeczywistnienie"
  ]
  node [
    id 912
    label "baraszki"
  ]
  node [
    id 913
    label "certificate"
  ]
  node [
    id 914
    label "po&#380;&#261;danie"
  ]
  node [
    id 915
    label "wzw&#243;d"
  ]
  node [
    id 916
    label "act"
  ]
  node [
    id 917
    label "arystotelizm"
  ]
  node [
    id 918
    label "podnieca&#263;"
  ]
  node [
    id 919
    label "przebiec"
  ]
  node [
    id 920
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 921
    label "motyw"
  ]
  node [
    id 922
    label "przebiegni&#281;cie"
  ]
  node [
    id 923
    label "fabu&#322;a"
  ]
  node [
    id 924
    label "intelekt"
  ]
  node [
    id 925
    label "Kant"
  ]
  node [
    id 926
    label "p&#322;&#243;d"
  ]
  node [
    id 927
    label "cel"
  ]
  node [
    id 928
    label "ideacja"
  ]
  node [
    id 929
    label "rozumowanie"
  ]
  node [
    id 930
    label "opracowanie"
  ]
  node [
    id 931
    label "cytat"
  ]
  node [
    id 932
    label "obja&#347;nienie"
  ]
  node [
    id 933
    label "s&#261;dzenie"
  ]
  node [
    id 934
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 935
    label "niuansowa&#263;"
  ]
  node [
    id 936
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 937
    label "sk&#322;adnik"
  ]
  node [
    id 938
    label "zniuansowa&#263;"
  ]
  node [
    id 939
    label "fakt"
  ]
  node [
    id 940
    label "przyczyna"
  ]
  node [
    id 941
    label "wnioskowanie"
  ]
  node [
    id 942
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 943
    label "wyraz_pochodny"
  ]
  node [
    id 944
    label "fraza"
  ]
  node [
    id 945
    label "forum"
  ]
  node [
    id 946
    label "topik"
  ]
  node [
    id 947
    label "forma"
  ]
  node [
    id 948
    label "melodia"
  ]
  node [
    id 949
    label "otoczka"
  ]
  node [
    id 950
    label "entertainment"
  ]
  node [
    id 951
    label "consumption"
  ]
  node [
    id 952
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 953
    label "erecting"
  ]
  node [
    id 954
    label "zacz&#281;cie"
  ]
  node [
    id 955
    label "zareagowanie"
  ]
  node [
    id 956
    label "zrobienie"
  ]
  node [
    id 957
    label "narobienie"
  ]
  node [
    id 958
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 959
    label "porobienie"
  ]
  node [
    id 960
    label "campaign"
  ]
  node [
    id 961
    label "causing"
  ]
  node [
    id 962
    label "bezproblemowy"
  ]
  node [
    id 963
    label "discourtesy"
  ]
  node [
    id 964
    label "odj&#281;cie"
  ]
  node [
    id 965
    label "post&#261;pienie"
  ]
  node [
    id 966
    label "opening"
  ]
  node [
    id 967
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 968
    label "zdarzenie_si&#281;"
  ]
  node [
    id 969
    label "po&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 970
    label "poczucie"
  ]
  node [
    id 971
    label "Rada_Europy"
  ]
  node [
    id 972
    label "posiedzenie"
  ]
  node [
    id 973
    label "wskaz&#243;wka"
  ]
  node [
    id 974
    label "Rada_Europejska"
  ]
  node [
    id 975
    label "zgromadzenie"
  ]
  node [
    id 976
    label "Rada_Unii_Europejskiej"
  ]
  node [
    id 977
    label "odsiedzenie"
  ]
  node [
    id 978
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 979
    label "pobycie"
  ]
  node [
    id 980
    label "convention"
  ]
  node [
    id 981
    label "adjustment"
  ]
  node [
    id 982
    label "concourse"
  ]
  node [
    id 983
    label "gathering"
  ]
  node [
    id 984
    label "wsp&#243;lnota"
  ]
  node [
    id 985
    label "spotkanie"
  ]
  node [
    id 986
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 987
    label "gromadzenie"
  ]
  node [
    id 988
    label "templum"
  ]
  node [
    id 989
    label "konwentykiel"
  ]
  node [
    id 990
    label "klasztor"
  ]
  node [
    id 991
    label "caucus"
  ]
  node [
    id 992
    label "pozyskanie"
  ]
  node [
    id 993
    label "kongregacja"
  ]
  node [
    id 994
    label "liga"
  ]
  node [
    id 995
    label "jednostka_systematyczna"
  ]
  node [
    id 996
    label "asymilowanie"
  ]
  node [
    id 997
    label "gromada"
  ]
  node [
    id 998
    label "asymilowa&#263;"
  ]
  node [
    id 999
    label "Entuzjastki"
  ]
  node [
    id 1000
    label "kompozycja"
  ]
  node [
    id 1001
    label "Terranie"
  ]
  node [
    id 1002
    label "category"
  ]
  node [
    id 1003
    label "pakiet_klimatyczny"
  ]
  node [
    id 1004
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1005
    label "stage_set"
  ]
  node [
    id 1006
    label "type"
  ]
  node [
    id 1007
    label "specgrupa"
  ]
  node [
    id 1008
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1009
    label "Eurogrupa"
  ]
  node [
    id 1010
    label "formacja_geologiczna"
  ]
  node [
    id 1011
    label "harcerze_starsi"
  ]
  node [
    id 1012
    label "rozmowa"
  ]
  node [
    id 1013
    label "sympozjon"
  ]
  node [
    id 1014
    label "tarcza"
  ]
  node [
    id 1015
    label "solucja"
  ]
  node [
    id 1016
    label "implikowa&#263;"
  ]
  node [
    id 1017
    label "konsultacja"
  ]
  node [
    id 1018
    label "dostojnik"
  ]
  node [
    id 1019
    label "Goebbels"
  ]
  node [
    id 1020
    label "Sto&#322;ypin"
  ]
  node [
    id 1021
    label "przybli&#380;enie"
  ]
  node [
    id 1022
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 1023
    label "kategoria"
  ]
  node [
    id 1024
    label "szpaler"
  ]
  node [
    id 1025
    label "lon&#380;a"
  ]
  node [
    id 1026
    label "uporz&#261;dkowanie"
  ]
  node [
    id 1027
    label "egzekutywa"
  ]
  node [
    id 1028
    label "premier"
  ]
  node [
    id 1029
    label "Londyn"
  ]
  node [
    id 1030
    label "gabinet_cieni"
  ]
  node [
    id 1031
    label "number"
  ]
  node [
    id 1032
    label "Konsulat"
  ]
  node [
    id 1033
    label "tract"
  ]
  node [
    id 1034
    label "klasa"
  ]
  node [
    id 1035
    label "w&#322;adza"
  ]
  node [
    id 1036
    label "urz&#281;dnik"
  ]
  node [
    id 1037
    label "notabl"
  ]
  node [
    id 1038
    label "oficja&#322;"
  ]
  node [
    id 1039
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 1040
    label "strategia"
  ]
  node [
    id 1041
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1042
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 1043
    label "metoda"
  ]
  node [
    id 1044
    label "gra"
  ]
  node [
    id 1045
    label "wzorzec_projektowy"
  ]
  node [
    id 1046
    label "dziedzina"
  ]
  node [
    id 1047
    label "doktryna"
  ]
  node [
    id 1048
    label "wrinkle"
  ]
  node [
    id 1049
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 1050
    label "try"
  ]
  node [
    id 1051
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1052
    label "describe"
  ]
  node [
    id 1053
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 1054
    label "robi&#263;"
  ]
  node [
    id 1055
    label "przybiera&#263;"
  ]
  node [
    id 1056
    label "i&#347;&#263;"
  ]
  node [
    id 1057
    label "use"
  ]
  node [
    id 1058
    label "rynek"
  ]
  node [
    id 1059
    label "nuklearyzacja"
  ]
  node [
    id 1060
    label "deduction"
  ]
  node [
    id 1061
    label "entrance"
  ]
  node [
    id 1062
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 1063
    label "wst&#281;p"
  ]
  node [
    id 1064
    label "wej&#347;cie"
  ]
  node [
    id 1065
    label "issue"
  ]
  node [
    id 1066
    label "doprowadzenie"
  ]
  node [
    id 1067
    label "umo&#380;liwienie"
  ]
  node [
    id 1068
    label "wpisanie"
  ]
  node [
    id 1069
    label "podstawy"
  ]
  node [
    id 1070
    label "evocation"
  ]
  node [
    id 1071
    label "zapoznanie"
  ]
  node [
    id 1072
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1073
    label "przewietrzenie"
  ]
  node [
    id 1074
    label "mo&#380;liwy"
  ]
  node [
    id 1075
    label "upowa&#380;nienie"
  ]
  node [
    id 1076
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 1077
    label "obejrzenie"
  ]
  node [
    id 1078
    label "involvement"
  ]
  node [
    id 1079
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 1080
    label "za&#347;wiecenie"
  ]
  node [
    id 1081
    label "nastawienie"
  ]
  node [
    id 1082
    label "uruchomienie"
  ]
  node [
    id 1083
    label "funkcjonowanie"
  ]
  node [
    id 1084
    label "wy&#322;&#261;czenie"
  ]
  node [
    id 1085
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1086
    label "wnikni&#281;cie"
  ]
  node [
    id 1087
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 1088
    label "poznanie"
  ]
  node [
    id 1089
    label "pojawienie_si&#281;"
  ]
  node [
    id 1090
    label "przenikni&#281;cie"
  ]
  node [
    id 1091
    label "wpuszczenie"
  ]
  node [
    id 1092
    label "zaatakowanie"
  ]
  node [
    id 1093
    label "trespass"
  ]
  node [
    id 1094
    label "dost&#281;p"
  ]
  node [
    id 1095
    label "doj&#347;cie"
  ]
  node [
    id 1096
    label "przekroczenie"
  ]
  node [
    id 1097
    label "otw&#243;r"
  ]
  node [
    id 1098
    label "vent"
  ]
  node [
    id 1099
    label "stimulation"
  ]
  node [
    id 1100
    label "dostanie_si&#281;"
  ]
  node [
    id 1101
    label "pocz&#261;tek"
  ]
  node [
    id 1102
    label "approach"
  ]
  node [
    id 1103
    label "release"
  ]
  node [
    id 1104
    label "wnij&#347;cie"
  ]
  node [
    id 1105
    label "bramka"
  ]
  node [
    id 1106
    label "wzniesienie_si&#281;"
  ]
  node [
    id 1107
    label "podw&#243;rze"
  ]
  node [
    id 1108
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1109
    label "dom"
  ]
  node [
    id 1110
    label "wch&#243;d"
  ]
  node [
    id 1111
    label "nast&#261;pienie"
  ]
  node [
    id 1112
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1113
    label "cz&#322;onek"
  ]
  node [
    id 1114
    label "stanie_si&#281;"
  ]
  node [
    id 1115
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 1116
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1117
    label "urz&#261;dzenie"
  ]
  node [
    id 1118
    label "przeszkoda"
  ]
  node [
    id 1119
    label "perturbation"
  ]
  node [
    id 1120
    label "aberration"
  ]
  node [
    id 1121
    label "sygna&#322;"
  ]
  node [
    id 1122
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1123
    label "hindrance"
  ]
  node [
    id 1124
    label "disorder"
  ]
  node [
    id 1125
    label "naruszenie"
  ]
  node [
    id 1126
    label "inscription"
  ]
  node [
    id 1127
    label "wype&#322;nienie"
  ]
  node [
    id 1128
    label "napisanie"
  ]
  node [
    id 1129
    label "wiedza"
  ]
  node [
    id 1130
    label "detail"
  ]
  node [
    id 1131
    label "zapowied&#378;"
  ]
  node [
    id 1132
    label "g&#322;oska"
  ]
  node [
    id 1133
    label "wymowa"
  ]
  node [
    id 1134
    label "spe&#322;nienie"
  ]
  node [
    id 1135
    label "lead"
  ]
  node [
    id 1136
    label "pos&#322;anie"
  ]
  node [
    id 1137
    label "znalezienie_si&#281;"
  ]
  node [
    id 1138
    label "introduction"
  ]
  node [
    id 1139
    label "sp&#281;dzenie"
  ]
  node [
    id 1140
    label "zainstalowanie"
  ]
  node [
    id 1141
    label "poumieszczanie"
  ]
  node [
    id 1142
    label "ustalenie"
  ]
  node [
    id 1143
    label "uplasowanie"
  ]
  node [
    id 1144
    label "ulokowanie_si&#281;"
  ]
  node [
    id 1145
    label "prze&#322;adowanie"
  ]
  node [
    id 1146
    label "layout"
  ]
  node [
    id 1147
    label "siedzenie"
  ]
  node [
    id 1148
    label "zakrycie"
  ]
  node [
    id 1149
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 1150
    label "representation"
  ]
  node [
    id 1151
    label "znajomy"
  ]
  node [
    id 1152
    label "obznajomienie"
  ]
  node [
    id 1153
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 1154
    label "poinformowanie"
  ]
  node [
    id 1155
    label "knowing"
  ]
  node [
    id 1156
    label "refresher_course"
  ]
  node [
    id 1157
    label "powietrze"
  ]
  node [
    id 1158
    label "oczyszczenie"
  ]
  node [
    id 1159
    label "wymienienie"
  ]
  node [
    id 1160
    label "vaporization"
  ]
  node [
    id 1161
    label "potraktowanie"
  ]
  node [
    id 1162
    label "przewietrzenie_si&#281;"
  ]
  node [
    id 1163
    label "ventilation"
  ]
  node [
    id 1164
    label "rozpowszechnianie"
  ]
  node [
    id 1165
    label "stoisko"
  ]
  node [
    id 1166
    label "rynek_podstawowy"
  ]
  node [
    id 1167
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 1168
    label "konsument"
  ]
  node [
    id 1169
    label "obiekt_handlowy"
  ]
  node [
    id 1170
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 1171
    label "wytw&#243;rca"
  ]
  node [
    id 1172
    label "rynek_wt&#243;rny"
  ]
  node [
    id 1173
    label "wprowadzanie"
  ]
  node [
    id 1174
    label "wprowadza&#263;"
  ]
  node [
    id 1175
    label "kram"
  ]
  node [
    id 1176
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 1177
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1178
    label "emitowa&#263;"
  ]
  node [
    id 1179
    label "wprowadzi&#263;"
  ]
  node [
    id 1180
    label "emitowanie"
  ]
  node [
    id 1181
    label "gospodarka"
  ]
  node [
    id 1182
    label "biznes"
  ]
  node [
    id 1183
    label "segment_rynku"
  ]
  node [
    id 1184
    label "targowica"
  ]
  node [
    id 1185
    label "naziemnie"
  ]
  node [
    id 1186
    label "nadziemny"
  ]
  node [
    id 1187
    label "ekran"
  ]
  node [
    id 1188
    label "Interwizja"
  ]
  node [
    id 1189
    label "BBC"
  ]
  node [
    id 1190
    label "paj&#281;czarz"
  ]
  node [
    id 1191
    label "programowiec"
  ]
  node [
    id 1192
    label "Polsat"
  ]
  node [
    id 1193
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 1194
    label "odbiornik"
  ]
  node [
    id 1195
    label "muza"
  ]
  node [
    id 1196
    label "media"
  ]
  node [
    id 1197
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 1198
    label "odbieranie"
  ]
  node [
    id 1199
    label "studio"
  ]
  node [
    id 1200
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 1201
    label "odbiera&#263;"
  ]
  node [
    id 1202
    label "technologia"
  ]
  node [
    id 1203
    label "technika"
  ]
  node [
    id 1204
    label "teletransmisja"
  ]
  node [
    id 1205
    label "telemetria"
  ]
  node [
    id 1206
    label "telegrafia"
  ]
  node [
    id 1207
    label "transmisja_danych"
  ]
  node [
    id 1208
    label "kontrola_parzysto&#347;ci"
  ]
  node [
    id 1209
    label "trunking"
  ]
  node [
    id 1210
    label "telekomutacja"
  ]
  node [
    id 1211
    label "teletechnika"
  ]
  node [
    id 1212
    label "teleks"
  ]
  node [
    id 1213
    label "telematyka"
  ]
  node [
    id 1214
    label "teleinformatyka"
  ]
  node [
    id 1215
    label "mikrotechnologia"
  ]
  node [
    id 1216
    label "technologia_nieorganiczna"
  ]
  node [
    id 1217
    label "engineering"
  ]
  node [
    id 1218
    label "biotechnologia"
  ]
  node [
    id 1219
    label "osoba_prawna"
  ]
  node [
    id 1220
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1221
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1222
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1223
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1224
    label "biuro"
  ]
  node [
    id 1225
    label "organizacja"
  ]
  node [
    id 1226
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1227
    label "Fundusze_Unijne"
  ]
  node [
    id 1228
    label "zamyka&#263;"
  ]
  node [
    id 1229
    label "establishment"
  ]
  node [
    id 1230
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1231
    label "urz&#261;d"
  ]
  node [
    id 1232
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1233
    label "afiliowa&#263;"
  ]
  node [
    id 1234
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1235
    label "zamykanie"
  ]
  node [
    id 1236
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1237
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1238
    label "antena"
  ]
  node [
    id 1239
    label "amplituner"
  ]
  node [
    id 1240
    label "tuner"
  ]
  node [
    id 1241
    label "inspiratorka"
  ]
  node [
    id 1242
    label "banan"
  ]
  node [
    id 1243
    label "talent"
  ]
  node [
    id 1244
    label "Melpomena"
  ]
  node [
    id 1245
    label "natchnienie"
  ]
  node [
    id 1246
    label "bogini"
  ]
  node [
    id 1247
    label "muzyka"
  ]
  node [
    id 1248
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 1249
    label "palma"
  ]
  node [
    id 1250
    label "mass-media"
  ]
  node [
    id 1251
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 1252
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 1253
    label "przekazior"
  ]
  node [
    id 1254
    label "uzbrajanie"
  ]
  node [
    id 1255
    label "medium"
  ]
  node [
    id 1256
    label "siedziba"
  ]
  node [
    id 1257
    label "composition"
  ]
  node [
    id 1258
    label "wydawnictwo"
  ]
  node [
    id 1259
    label "redaction"
  ]
  node [
    id 1260
    label "obr&#243;bka"
  ]
  node [
    id 1261
    label "p&#322;aszczyzna"
  ]
  node [
    id 1262
    label "naszywka"
  ]
  node [
    id 1263
    label "kominek"
  ]
  node [
    id 1264
    label "zas&#322;ona"
  ]
  node [
    id 1265
    label "os&#322;ona"
  ]
  node [
    id 1266
    label "dochodzenie"
  ]
  node [
    id 1267
    label "rozsi&#261;dni&#281;cie_si&#281;"
  ]
  node [
    id 1268
    label "powodowanie"
  ]
  node [
    id 1269
    label "collection"
  ]
  node [
    id 1270
    label "konfiskowanie"
  ]
  node [
    id 1271
    label "rozsiadanie_si&#281;"
  ]
  node [
    id 1272
    label "zabieranie"
  ]
  node [
    id 1273
    label "zlecenie"
  ]
  node [
    id 1274
    label "przyjmowanie"
  ]
  node [
    id 1275
    label "solicitation"
  ]
  node [
    id 1276
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 1277
    label "zniewalanie"
  ]
  node [
    id 1278
    label "telewizor"
  ]
  node [
    id 1279
    label "przyp&#322;ywanie"
  ]
  node [
    id 1280
    label "odzyskiwanie"
  ]
  node [
    id 1281
    label "perception"
  ]
  node [
    id 1282
    label "odp&#322;ywanie"
  ]
  node [
    id 1283
    label "u&#380;ytkownik"
  ]
  node [
    id 1284
    label "oszust"
  ]
  node [
    id 1285
    label "pirat"
  ]
  node [
    id 1286
    label "zabiera&#263;"
  ]
  node [
    id 1287
    label "odzyskiwa&#263;"
  ]
  node [
    id 1288
    label "przyjmowa&#263;"
  ]
  node [
    id 1289
    label "fall"
  ]
  node [
    id 1290
    label "liszy&#263;"
  ]
  node [
    id 1291
    label "pozbawia&#263;"
  ]
  node [
    id 1292
    label "konfiskowa&#263;"
  ]
  node [
    id 1293
    label "deprive"
  ]
  node [
    id 1294
    label "accept"
  ]
  node [
    id 1295
    label "doznawa&#263;"
  ]
  node [
    id 1296
    label "elektroniczny"
  ]
  node [
    id 1297
    label "cyfrowo"
  ]
  node [
    id 1298
    label "sygna&#322;_cyfrowy"
  ]
  node [
    id 1299
    label "cyfryzacja"
  ]
  node [
    id 1300
    label "modernizacja"
  ]
  node [
    id 1301
    label "elektrycznie"
  ]
  node [
    id 1302
    label "elektronicznie"
  ]
  node [
    id 1303
    label "impression"
  ]
  node [
    id 1304
    label "pismo"
  ]
  node [
    id 1305
    label "glif"
  ]
  node [
    id 1306
    label "dese&#324;"
  ]
  node [
    id 1307
    label "prohibita"
  ]
  node [
    id 1308
    label "cymelium"
  ]
  node [
    id 1309
    label "tkanina"
  ]
  node [
    id 1310
    label "zaproszenie"
  ]
  node [
    id 1311
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 1312
    label "formatowa&#263;"
  ]
  node [
    id 1313
    label "formatowanie"
  ]
  node [
    id 1314
    label "zdobnik"
  ]
  node [
    id 1315
    label "character"
  ]
  node [
    id 1316
    label "printing"
  ]
  node [
    id 1317
    label "cywilizacja"
  ]
  node [
    id 1318
    label "fotowoltaika"
  ]
  node [
    id 1319
    label "mechanika_precyzyjna"
  ]
  node [
    id 1320
    label "work"
  ]
  node [
    id 1321
    label "psychotest"
  ]
  node [
    id 1322
    label "wk&#322;ad"
  ]
  node [
    id 1323
    label "handwriting"
  ]
  node [
    id 1324
    label "paleograf"
  ]
  node [
    id 1325
    label "interpunkcja"
  ]
  node [
    id 1326
    label "dzia&#322;"
  ]
  node [
    id 1327
    label "grafia"
  ]
  node [
    id 1328
    label "communication"
  ]
  node [
    id 1329
    label "script"
  ]
  node [
    id 1330
    label "zajawka"
  ]
  node [
    id 1331
    label "list"
  ]
  node [
    id 1332
    label "Zwrotnica"
  ]
  node [
    id 1333
    label "czasopismo"
  ]
  node [
    id 1334
    label "ok&#322;adka"
  ]
  node [
    id 1335
    label "ortografia"
  ]
  node [
    id 1336
    label "letter"
  ]
  node [
    id 1337
    label "paleografia"
  ]
  node [
    id 1338
    label "j&#281;zyk"
  ]
  node [
    id 1339
    label "prasa"
  ]
  node [
    id 1340
    label "wz&#243;r"
  ]
  node [
    id 1341
    label "design"
  ]
  node [
    id 1342
    label "produkcja"
  ]
  node [
    id 1343
    label "notification"
  ]
  node [
    id 1344
    label "maglownia"
  ]
  node [
    id 1345
    label "pru&#263;_si&#281;"
  ]
  node [
    id 1346
    label "opalarnia"
  ]
  node [
    id 1347
    label "prucie_si&#281;"
  ]
  node [
    id 1348
    label "apretura"
  ]
  node [
    id 1349
    label "splot"
  ]
  node [
    id 1350
    label "karbonizowa&#263;"
  ]
  node [
    id 1351
    label "karbonizacja"
  ]
  node [
    id 1352
    label "rozprucie_si&#281;"
  ]
  node [
    id 1353
    label "rozpru&#263;_si&#281;"
  ]
  node [
    id 1354
    label "kr&#243;j"
  ]
  node [
    id 1355
    label "splay"
  ]
  node [
    id 1356
    label "czcionka"
  ]
  node [
    id 1357
    label "symbol"
  ]
  node [
    id 1358
    label "pro&#347;ba"
  ]
  node [
    id 1359
    label "invitation"
  ]
  node [
    id 1360
    label "karteczka"
  ]
  node [
    id 1361
    label "zaproponowanie"
  ]
  node [
    id 1362
    label "propozycja"
  ]
  node [
    id 1363
    label "wzajemno&#347;&#263;"
  ]
  node [
    id 1364
    label "podw&#243;jno&#347;&#263;"
  ]
  node [
    id 1365
    label "edytowa&#263;"
  ]
  node [
    id 1366
    label "dostosowywa&#263;"
  ]
  node [
    id 1367
    label "sk&#322;ada&#263;"
  ]
  node [
    id 1368
    label "przygotowywa&#263;"
  ]
  node [
    id 1369
    label "format"
  ]
  node [
    id 1370
    label "zmienianie"
  ]
  node [
    id 1371
    label "przygotowywanie"
  ]
  node [
    id 1372
    label "sk&#322;adanie"
  ]
  node [
    id 1373
    label "edytowanie"
  ]
  node [
    id 1374
    label "dostosowywanie"
  ]
  node [
    id 1375
    label "rarytas"
  ]
  node [
    id 1376
    label "r&#281;kopis"
  ]
  node [
    id 1377
    label "i"
  ]
  node [
    id 1378
    label "&#347;rodki"
  ]
  node [
    id 1379
    label "El&#380;bieta"
  ]
  node [
    id 1380
    label "kruk"
  ]
  node [
    id 1381
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 1382
    label "rzeczpospolita"
  ]
  node [
    id 1383
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 6
  ]
  edge [
    source 6
    target 1377
  ]
  edge [
    source 6
    target 1378
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 748
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 1377
  ]
  edge [
    source 8
    target 1378
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 9
    target 800
  ]
  edge [
    source 9
    target 801
  ]
  edge [
    source 9
    target 802
  ]
  edge [
    source 9
    target 803
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 805
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 9
    target 807
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 808
  ]
  edge [
    source 9
    target 809
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 823
  ]
  edge [
    source 10
    target 824
  ]
  edge [
    source 10
    target 825
  ]
  edge [
    source 10
    target 826
  ]
  edge [
    source 10
    target 827
  ]
  edge [
    source 10
    target 828
  ]
  edge [
    source 10
    target 829
  ]
  edge [
    source 10
    target 830
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 831
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 832
  ]
  edge [
    source 10
    target 833
  ]
  edge [
    source 10
    target 834
  ]
  edge [
    source 10
    target 835
  ]
  edge [
    source 10
    target 836
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 10
    target 839
  ]
  edge [
    source 10
    target 840
  ]
  edge [
    source 10
    target 841
  ]
  edge [
    source 10
    target 842
  ]
  edge [
    source 10
    target 843
  ]
  edge [
    source 10
    target 844
  ]
  edge [
    source 10
    target 845
  ]
  edge [
    source 10
    target 846
  ]
  edge [
    source 10
    target 847
  ]
  edge [
    source 10
    target 848
  ]
  edge [
    source 10
    target 849
  ]
  edge [
    source 10
    target 850
  ]
  edge [
    source 10
    target 851
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 852
  ]
  edge [
    source 10
    target 853
  ]
  edge [
    source 10
    target 854
  ]
  edge [
    source 10
    target 855
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 856
  ]
  edge [
    source 10
    target 857
  ]
  edge [
    source 10
    target 858
  ]
  edge [
    source 10
    target 859
  ]
  edge [
    source 10
    target 860
  ]
  edge [
    source 10
    target 861
  ]
  edge [
    source 10
    target 862
  ]
  edge [
    source 10
    target 863
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 864
  ]
  edge [
    source 10
    target 865
  ]
  edge [
    source 10
    target 866
  ]
  edge [
    source 10
    target 26
  ]
  edge [
    source 10
    target 867
  ]
  edge [
    source 10
    target 868
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 869
  ]
  edge [
    source 10
    target 870
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 871
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 872
  ]
  edge [
    source 10
    target 873
  ]
  edge [
    source 10
    target 874
  ]
  edge [
    source 10
    target 875
  ]
  edge [
    source 10
    target 876
  ]
  edge [
    source 10
    target 877
  ]
  edge [
    source 10
    target 878
  ]
  edge [
    source 10
    target 879
  ]
  edge [
    source 10
    target 880
  ]
  edge [
    source 10
    target 1377
  ]
  edge [
    source 10
    target 1378
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 220
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 224
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 225
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 228
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 223
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 221
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 12
    target 229
  ]
  edge [
    source 12
    target 230
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 207
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 917
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 578
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 920
  ]
  edge [
    source 14
    target 921
  ]
  edge [
    source 14
    target 922
  ]
  edge [
    source 14
    target 923
  ]
  edge [
    source 14
    target 625
  ]
  edge [
    source 14
    target 800
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 14
    target 925
  ]
  edge [
    source 14
    target 926
  ]
  edge [
    source 14
    target 927
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 735
  ]
  edge [
    source 14
    target 225
  ]
  edge [
    source 14
    target 928
  ]
  edge [
    source 14
    target 251
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 617
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 736
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 738
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 929
  ]
  edge [
    source 14
    target 930
  ]
  edge [
    source 14
    target 419
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 931
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 932
  ]
  edge [
    source 14
    target 933
  ]
  edge [
    source 14
    target 934
  ]
  edge [
    source 14
    target 935
  ]
  edge [
    source 14
    target 748
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 937
  ]
  edge [
    source 14
    target 938
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 365
  ]
  edge [
    source 14
    target 940
  ]
  edge [
    source 14
    target 941
  ]
  edge [
    source 14
    target 942
  ]
  edge [
    source 14
    target 943
  ]
  edge [
    source 14
    target 739
  ]
  edge [
    source 14
    target 740
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 743
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 749
  ]
  edge [
    source 14
    target 945
  ]
  edge [
    source 14
    target 946
  ]
  edge [
    source 14
    target 751
  ]
  edge [
    source 14
    target 752
  ]
  edge [
    source 14
    target 753
  ]
  edge [
    source 14
    target 947
  ]
  edge [
    source 14
    target 755
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 948
  ]
  edge [
    source 14
    target 949
  ]
  edge [
    source 14
    target 757
  ]
  edge [
    source 14
    target 758
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 950
  ]
  edge [
    source 15
    target 951
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 952
  ]
  edge [
    source 15
    target 953
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 954
  ]
  edge [
    source 15
    target 955
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 956
  ]
  edge [
    source 15
    target 957
  ]
  edge [
    source 15
    target 958
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 959
  ]
  edge [
    source 15
    target 960
  ]
  edge [
    source 15
    target 961
  ]
  edge [
    source 15
    target 571
  ]
  edge [
    source 15
    target 962
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 15
    target 963
  ]
  edge [
    source 15
    target 964
  ]
  edge [
    source 15
    target 965
  ]
  edge [
    source 15
    target 966
  ]
  edge [
    source 15
    target 967
  ]
  edge [
    source 15
    target 834
  ]
  edge [
    source 15
    target 968
  ]
  edge [
    source 15
    target 969
  ]
  edge [
    source 15
    target 970
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 972
  ]
  edge [
    source 17
    target 973
  ]
  edge [
    source 17
    target 389
  ]
  edge [
    source 17
    target 974
  ]
  edge [
    source 17
    target 479
  ]
  edge [
    source 17
    target 975
  ]
  edge [
    source 17
    target 478
  ]
  edge [
    source 17
    target 976
  ]
  edge [
    source 17
    target 477
  ]
  edge [
    source 17
    target 493
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 17
    target 978
  ]
  edge [
    source 17
    target 979
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 980
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 484
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 17
    target 988
  ]
  edge [
    source 17
    target 989
  ]
  edge [
    source 17
    target 990
  ]
  edge [
    source 17
    target 991
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 992
  ]
  edge [
    source 17
    target 993
  ]
  edge [
    source 17
    target 497
  ]
  edge [
    source 17
    target 498
  ]
  edge [
    source 17
    target 500
  ]
  edge [
    source 17
    target 501
  ]
  edge [
    source 17
    target 502
  ]
  edge [
    source 17
    target 503
  ]
  edge [
    source 17
    target 474
  ]
  edge [
    source 17
    target 504
  ]
  edge [
    source 17
    target 505
  ]
  edge [
    source 17
    target 295
  ]
  edge [
    source 17
    target 506
  ]
  edge [
    source 17
    target 511
  ]
  edge [
    source 17
    target 508
  ]
  edge [
    source 17
    target 507
  ]
  edge [
    source 17
    target 509
  ]
  edge [
    source 17
    target 510
  ]
  edge [
    source 17
    target 499
  ]
  edge [
    source 17
    target 512
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 513
  ]
  edge [
    source 17
    target 481
  ]
  edge [
    source 17
    target 994
  ]
  edge [
    source 17
    target 995
  ]
  edge [
    source 17
    target 996
  ]
  edge [
    source 17
    target 997
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 998
  ]
  edge [
    source 17
    target 648
  ]
  edge [
    source 17
    target 999
  ]
  edge [
    source 17
    target 211
  ]
  edge [
    source 17
    target 1000
  ]
  edge [
    source 17
    target 1001
  ]
  edge [
    source 17
    target 486
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 267
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 482
  ]
  edge [
    source 17
    target 496
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 487
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 475
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1018
  ]
  edge [
    source 18
    target 1019
  ]
  edge [
    source 18
    target 1020
  ]
  edge [
    source 18
    target 105
  ]
  edge [
    source 18
    target 1021
  ]
  edge [
    source 18
    target 1022
  ]
  edge [
    source 18
    target 1023
  ]
  edge [
    source 18
    target 1024
  ]
  edge [
    source 18
    target 1025
  ]
  edge [
    source 18
    target 1026
  ]
  edge [
    source 18
    target 1027
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 1028
  ]
  edge [
    source 18
    target 1029
  ]
  edge [
    source 18
    target 1030
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 1031
  ]
  edge [
    source 18
    target 1032
  ]
  edge [
    source 18
    target 1033
  ]
  edge [
    source 18
    target 1034
  ]
  edge [
    source 18
    target 1035
  ]
  edge [
    source 18
    target 1036
  ]
  edge [
    source 18
    target 1037
  ]
  edge [
    source 18
    target 1038
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1039
  ]
  edge [
    source 19
    target 550
  ]
  edge [
    source 19
    target 1040
  ]
  edge [
    source 19
    target 1041
  ]
  edge [
    source 19
    target 1042
  ]
  edge [
    source 19
    target 73
  ]
  edge [
    source 19
    target 418
  ]
  edge [
    source 19
    target 1043
  ]
  edge [
    source 19
    target 1044
  ]
  edge [
    source 19
    target 887
  ]
  edge [
    source 19
    target 1045
  ]
  edge [
    source 19
    target 1046
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 1047
  ]
  edge [
    source 19
    target 1048
  ]
  edge [
    source 19
    target 816
  ]
  edge [
    source 19
    target 1049
  ]
  edge [
    source 19
    target 569
  ]
  edge [
    source 19
    target 570
  ]
  edge [
    source 19
    target 238
  ]
  edge [
    source 19
    target 571
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1050
  ]
  edge [
    source 20
    target 1051
  ]
  edge [
    source 20
    target 64
  ]
  edge [
    source 20
    target 1052
  ]
  edge [
    source 20
    target 1053
  ]
  edge [
    source 20
    target 1054
  ]
  edge [
    source 20
    target 41
  ]
  edge [
    source 20
    target 1055
  ]
  edge [
    source 20
    target 916
  ]
  edge [
    source 20
    target 1056
  ]
  edge [
    source 20
    target 1057
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1058
  ]
  edge [
    source 21
    target 1059
  ]
  edge [
    source 21
    target 1060
  ]
  edge [
    source 21
    target 1061
  ]
  edge [
    source 21
    target 1062
  ]
  edge [
    source 21
    target 1063
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 21
    target 1064
  ]
  edge [
    source 21
    target 1065
  ]
  edge [
    source 21
    target 1066
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 1067
  ]
  edge [
    source 21
    target 1068
  ]
  edge [
    source 21
    target 1069
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 1070
  ]
  edge [
    source 21
    target 1071
  ]
  edge [
    source 21
    target 1072
  ]
  edge [
    source 21
    target 954
  ]
  edge [
    source 21
    target 1073
  ]
  edge [
    source 21
    target 956
  ]
  edge [
    source 21
    target 1074
  ]
  edge [
    source 21
    target 1075
  ]
  edge [
    source 21
    target 1076
  ]
  edge [
    source 21
    target 454
  ]
  edge [
    source 21
    target 1077
  ]
  edge [
    source 21
    target 1078
  ]
  edge [
    source 21
    target 1079
  ]
  edge [
    source 21
    target 1080
  ]
  edge [
    source 21
    target 1081
  ]
  edge [
    source 21
    target 1082
  ]
  edge [
    source 21
    target 1083
  ]
  edge [
    source 21
    target 1084
  ]
  edge [
    source 21
    target 1085
  ]
  edge [
    source 21
    target 957
  ]
  edge [
    source 21
    target 958
  ]
  edge [
    source 21
    target 673
  ]
  edge [
    source 21
    target 959
  ]
  edge [
    source 21
    target 1086
  ]
  edge [
    source 21
    target 1087
  ]
  edge [
    source 21
    target 985
  ]
  edge [
    source 21
    target 1088
  ]
  edge [
    source 21
    target 1089
  ]
  edge [
    source 21
    target 968
  ]
  edge [
    source 21
    target 1090
  ]
  edge [
    source 21
    target 1091
  ]
  edge [
    source 21
    target 1092
  ]
  edge [
    source 21
    target 1093
  ]
  edge [
    source 21
    target 1094
  ]
  edge [
    source 21
    target 1095
  ]
  edge [
    source 21
    target 1096
  ]
  edge [
    source 21
    target 1097
  ]
  edge [
    source 21
    target 714
  ]
  edge [
    source 21
    target 1098
  ]
  edge [
    source 21
    target 1099
  ]
  edge [
    source 21
    target 1100
  ]
  edge [
    source 21
    target 1101
  ]
  edge [
    source 21
    target 1102
  ]
  edge [
    source 21
    target 1103
  ]
  edge [
    source 21
    target 1104
  ]
  edge [
    source 21
    target 1105
  ]
  edge [
    source 21
    target 1106
  ]
  edge [
    source 21
    target 1107
  ]
  edge [
    source 21
    target 1108
  ]
  edge [
    source 21
    target 1109
  ]
  edge [
    source 21
    target 1110
  ]
  edge [
    source 21
    target 1111
  ]
  edge [
    source 21
    target 1112
  ]
  edge [
    source 21
    target 1113
  ]
  edge [
    source 21
    target 1114
  ]
  edge [
    source 21
    target 1115
  ]
  edge [
    source 21
    target 1116
  ]
  edge [
    source 21
    target 1117
  ]
  edge [
    source 21
    target 960
  ]
  edge [
    source 21
    target 961
  ]
  edge [
    source 21
    target 314
  ]
  edge [
    source 21
    target 1118
  ]
  edge [
    source 21
    target 1119
  ]
  edge [
    source 21
    target 1120
  ]
  edge [
    source 21
    target 1121
  ]
  edge [
    source 21
    target 1122
  ]
  edge [
    source 21
    target 1123
  ]
  edge [
    source 21
    target 1124
  ]
  edge [
    source 21
    target 542
  ]
  edge [
    source 21
    target 1125
  ]
  edge [
    source 21
    target 119
  ]
  edge [
    source 21
    target 963
  ]
  edge [
    source 21
    target 964
  ]
  edge [
    source 21
    target 965
  ]
  edge [
    source 21
    target 966
  ]
  edge [
    source 21
    target 967
  ]
  edge [
    source 21
    target 834
  ]
  edge [
    source 21
    target 1126
  ]
  edge [
    source 21
    target 1127
  ]
  edge [
    source 21
    target 1128
  ]
  edge [
    source 21
    target 852
  ]
  edge [
    source 21
    target 1129
  ]
  edge [
    source 21
    target 1130
  ]
  edge [
    source 21
    target 571
  ]
  edge [
    source 21
    target 962
  ]
  edge [
    source 21
    target 203
  ]
  edge [
    source 21
    target 1131
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 650
  ]
  edge [
    source 21
    target 1132
  ]
  edge [
    source 21
    target 1133
  ]
  edge [
    source 21
    target 103
  ]
  edge [
    source 21
    target 1134
  ]
  edge [
    source 21
    target 1135
  ]
  edge [
    source 21
    target 710
  ]
  edge [
    source 21
    target 1136
  ]
  edge [
    source 21
    target 1137
  ]
  edge [
    source 21
    target 1138
  ]
  edge [
    source 21
    target 1139
  ]
  edge [
    source 21
    target 1140
  ]
  edge [
    source 21
    target 1141
  ]
  edge [
    source 21
    target 1142
  ]
  edge [
    source 21
    target 1143
  ]
  edge [
    source 21
    target 1144
  ]
  edge [
    source 21
    target 1145
  ]
  edge [
    source 21
    target 1146
  ]
  edge [
    source 21
    target 536
  ]
  edge [
    source 21
    target 1147
  ]
  edge [
    source 21
    target 1148
  ]
  edge [
    source 21
    target 1149
  ]
  edge [
    source 21
    target 1150
  ]
  edge [
    source 21
    target 387
  ]
  edge [
    source 21
    target 1151
  ]
  edge [
    source 21
    target 1152
  ]
  edge [
    source 21
    target 1153
  ]
  edge [
    source 21
    target 983
  ]
  edge [
    source 21
    target 1154
  ]
  edge [
    source 21
    target 1155
  ]
  edge [
    source 21
    target 1156
  ]
  edge [
    source 21
    target 1157
  ]
  edge [
    source 21
    target 1158
  ]
  edge [
    source 21
    target 1159
  ]
  edge [
    source 21
    target 1160
  ]
  edge [
    source 21
    target 1161
  ]
  edge [
    source 21
    target 1162
  ]
  edge [
    source 21
    target 1163
  ]
  edge [
    source 21
    target 1164
  ]
  edge [
    source 21
    target 419
  ]
  edge [
    source 21
    target 1165
  ]
  edge [
    source 21
    target 1166
  ]
  edge [
    source 21
    target 1167
  ]
  edge [
    source 21
    target 1168
  ]
  edge [
    source 21
    target 1169
  ]
  edge [
    source 21
    target 1170
  ]
  edge [
    source 21
    target 1171
  ]
  edge [
    source 21
    target 1172
  ]
  edge [
    source 21
    target 1173
  ]
  edge [
    source 21
    target 1174
  ]
  edge [
    source 21
    target 1175
  ]
  edge [
    source 21
    target 96
  ]
  edge [
    source 21
    target 1176
  ]
  edge [
    source 21
    target 1177
  ]
  edge [
    source 21
    target 1178
  ]
  edge [
    source 21
    target 1179
  ]
  edge [
    source 21
    target 1180
  ]
  edge [
    source 21
    target 1181
  ]
  edge [
    source 21
    target 1182
  ]
  edge [
    source 21
    target 1183
  ]
  edge [
    source 21
    target 1184
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 1186
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 522
  ]
  edge [
    source 23
    target 1187
  ]
  edge [
    source 23
    target 1188
  ]
  edge [
    source 23
    target 1189
  ]
  edge [
    source 23
    target 1190
  ]
  edge [
    source 23
    target 1191
  ]
  edge [
    source 23
    target 837
  ]
  edge [
    source 23
    target 678
  ]
  edge [
    source 23
    target 1192
  ]
  edge [
    source 23
    target 1193
  ]
  edge [
    source 23
    target 1194
  ]
  edge [
    source 23
    target 1195
  ]
  edge [
    source 23
    target 1196
  ]
  edge [
    source 23
    target 1197
  ]
  edge [
    source 23
    target 1198
  ]
  edge [
    source 23
    target 1199
  ]
  edge [
    source 23
    target 1200
  ]
  edge [
    source 23
    target 1201
  ]
  edge [
    source 23
    target 1202
  ]
  edge [
    source 23
    target 1203
  ]
  edge [
    source 23
    target 1204
  ]
  edge [
    source 23
    target 1205
  ]
  edge [
    source 23
    target 1206
  ]
  edge [
    source 23
    target 1207
  ]
  edge [
    source 23
    target 1208
  ]
  edge [
    source 23
    target 1209
  ]
  edge [
    source 23
    target 874
  ]
  edge [
    source 23
    target 1210
  ]
  edge [
    source 23
    target 1211
  ]
  edge [
    source 23
    target 1212
  ]
  edge [
    source 23
    target 1213
  ]
  edge [
    source 23
    target 1214
  ]
  edge [
    source 23
    target 517
  ]
  edge [
    source 23
    target 1215
  ]
  edge [
    source 23
    target 410
  ]
  edge [
    source 23
    target 1216
  ]
  edge [
    source 23
    target 1217
  ]
  edge [
    source 23
    target 1218
  ]
  edge [
    source 23
    target 1219
  ]
  edge [
    source 23
    target 1220
  ]
  edge [
    source 23
    target 1221
  ]
  edge [
    source 23
    target 131
  ]
  edge [
    source 23
    target 1222
  ]
  edge [
    source 23
    target 1223
  ]
  edge [
    source 23
    target 1224
  ]
  edge [
    source 23
    target 1225
  ]
  edge [
    source 23
    target 1226
  ]
  edge [
    source 23
    target 1227
  ]
  edge [
    source 23
    target 1228
  ]
  edge [
    source 23
    target 1229
  ]
  edge [
    source 23
    target 1230
  ]
  edge [
    source 23
    target 1231
  ]
  edge [
    source 23
    target 1232
  ]
  edge [
    source 23
    target 1233
  ]
  edge [
    source 23
    target 1234
  ]
  edge [
    source 23
    target 408
  ]
  edge [
    source 23
    target 1235
  ]
  edge [
    source 23
    target 1236
  ]
  edge [
    source 23
    target 1237
  ]
  edge [
    source 23
    target 1238
  ]
  edge [
    source 23
    target 1117
  ]
  edge [
    source 23
    target 1239
  ]
  edge [
    source 23
    target 1240
  ]
  edge [
    source 23
    target 1241
  ]
  edge [
    source 23
    target 538
  ]
  edge [
    source 23
    target 638
  ]
  edge [
    source 23
    target 1242
  ]
  edge [
    source 23
    target 1243
  ]
  edge [
    source 23
    target 655
  ]
  edge [
    source 23
    target 1244
  ]
  edge [
    source 23
    target 1245
  ]
  edge [
    source 23
    target 649
  ]
  edge [
    source 23
    target 1246
  ]
  edge [
    source 23
    target 492
  ]
  edge [
    source 23
    target 1247
  ]
  edge [
    source 23
    target 1248
  ]
  edge [
    source 23
    target 1249
  ]
  edge [
    source 23
    target 1250
  ]
  edge [
    source 23
    target 1251
  ]
  edge [
    source 23
    target 1252
  ]
  edge [
    source 23
    target 1253
  ]
  edge [
    source 23
    target 1254
  ]
  edge [
    source 23
    target 1255
  ]
  edge [
    source 23
    target 382
  ]
  edge [
    source 23
    target 120
  ]
  edge [
    source 23
    target 518
  ]
  edge [
    source 23
    target 474
  ]
  edge [
    source 23
    target 1256
  ]
  edge [
    source 23
    target 1257
  ]
  edge [
    source 23
    target 1258
  ]
  edge [
    source 23
    target 1259
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 1260
  ]
  edge [
    source 23
    target 536
  ]
  edge [
    source 23
    target 1261
  ]
  edge [
    source 23
    target 1262
  ]
  edge [
    source 23
    target 1263
  ]
  edge [
    source 23
    target 1264
  ]
  edge [
    source 23
    target 1265
  ]
  edge [
    source 23
    target 1266
  ]
  edge [
    source 23
    target 1267
  ]
  edge [
    source 23
    target 1268
  ]
  edge [
    source 23
    target 733
  ]
  edge [
    source 23
    target 1269
  ]
  edge [
    source 23
    target 1270
  ]
  edge [
    source 23
    target 1271
  ]
  edge [
    source 23
    target 1272
  ]
  edge [
    source 23
    target 1273
  ]
  edge [
    source 23
    target 1274
  ]
  edge [
    source 23
    target 1275
  ]
  edge [
    source 23
    target 1276
  ]
  edge [
    source 23
    target 746
  ]
  edge [
    source 23
    target 1277
  ]
  edge [
    source 23
    target 1278
  ]
  edge [
    source 23
    target 1095
  ]
  edge [
    source 23
    target 1279
  ]
  edge [
    source 23
    target 1280
  ]
  edge [
    source 23
    target 183
  ]
  edge [
    source 23
    target 729
  ]
  edge [
    source 23
    target 1281
  ]
  edge [
    source 23
    target 1282
  ]
  edge [
    source 23
    target 737
  ]
  edge [
    source 23
    target 1283
  ]
  edge [
    source 23
    target 1284
  ]
  edge [
    source 23
    target 1285
  ]
  edge [
    source 23
    target 1286
  ]
  edge [
    source 23
    target 1287
  ]
  edge [
    source 23
    target 1288
  ]
  edge [
    source 23
    target 40
  ]
  edge [
    source 23
    target 1289
  ]
  edge [
    source 23
    target 1290
  ]
  edge [
    source 23
    target 1291
  ]
  edge [
    source 23
    target 1292
  ]
  edge [
    source 23
    target 1293
  ]
  edge [
    source 23
    target 1294
  ]
  edge [
    source 23
    target 1295
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1296
  ]
  edge [
    source 24
    target 1297
  ]
  edge [
    source 24
    target 1298
  ]
  edge [
    source 24
    target 1299
  ]
  edge [
    source 24
    target 1300
  ]
  edge [
    source 24
    target 1301
  ]
  edge [
    source 24
    target 1302
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 1203
  ]
  edge [
    source 26
    target 1303
  ]
  edge [
    source 26
    target 1304
  ]
  edge [
    source 26
    target 114
  ]
  edge [
    source 26
    target 1305
  ]
  edge [
    source 26
    target 1306
  ]
  edge [
    source 26
    target 1307
  ]
  edge [
    source 26
    target 1308
  ]
  edge [
    source 26
    target 223
  ]
  edge [
    source 26
    target 1309
  ]
  edge [
    source 26
    target 1310
  ]
  edge [
    source 26
    target 1311
  ]
  edge [
    source 26
    target 190
  ]
  edge [
    source 26
    target 1312
  ]
  edge [
    source 26
    target 1313
  ]
  edge [
    source 26
    target 1314
  ]
  edge [
    source 26
    target 1315
  ]
  edge [
    source 26
    target 1316
  ]
  edge [
    source 26
    target 522
  ]
  edge [
    source 26
    target 1317
  ]
  edge [
    source 26
    target 251
  ]
  edge [
    source 26
    target 410
  ]
  edge [
    source 26
    target 1129
  ]
  edge [
    source 26
    target 654
  ]
  edge [
    source 26
    target 1217
  ]
  edge [
    source 26
    target 1318
  ]
  edge [
    source 26
    target 550
  ]
  edge [
    source 26
    target 1211
  ]
  edge [
    source 26
    target 1319
  ]
  edge [
    source 26
    target 1202
  ]
  edge [
    source 26
    target 926
  ]
  edge [
    source 26
    target 1320
  ]
  edge [
    source 26
    target 471
  ]
  edge [
    source 26
    target 1321
  ]
  edge [
    source 26
    target 1322
  ]
  edge [
    source 26
    target 1323
  ]
  edge [
    source 26
    target 839
  ]
  edge [
    source 26
    target 1324
  ]
  edge [
    source 26
    target 1325
  ]
  edge [
    source 26
    target 102
  ]
  edge [
    source 26
    target 1326
  ]
  edge [
    source 26
    target 1327
  ]
  edge [
    source 26
    target 648
  ]
  edge [
    source 26
    target 1328
  ]
  edge [
    source 26
    target 1329
  ]
  edge [
    source 26
    target 1330
  ]
  edge [
    source 26
    target 501
  ]
  edge [
    source 26
    target 1331
  ]
  edge [
    source 26
    target 116
  ]
  edge [
    source 26
    target 1332
  ]
  edge [
    source 26
    target 1333
  ]
  edge [
    source 26
    target 1334
  ]
  edge [
    source 26
    target 1335
  ]
  edge [
    source 26
    target 1336
  ]
  edge [
    source 26
    target 874
  ]
  edge [
    source 26
    target 1337
  ]
  edge [
    source 26
    target 1338
  ]
  edge [
    source 26
    target 816
  ]
  edge [
    source 26
    target 1339
  ]
  edge [
    source 26
    target 1340
  ]
  edge [
    source 26
    target 1341
  ]
  edge [
    source 26
    target 1342
  ]
  edge [
    source 26
    target 1343
  ]
  edge [
    source 26
    target 1344
  ]
  edge [
    source 26
    target 884
  ]
  edge [
    source 26
    target 1345
  ]
  edge [
    source 26
    target 1346
  ]
  edge [
    source 26
    target 1347
  ]
  edge [
    source 26
    target 1348
  ]
  edge [
    source 26
    target 1349
  ]
  edge [
    source 26
    target 1350
  ]
  edge [
    source 26
    target 1351
  ]
  edge [
    source 26
    target 1352
  ]
  edge [
    source 26
    target 667
  ]
  edge [
    source 26
    target 1353
  ]
  edge [
    source 26
    target 211
  ]
  edge [
    source 26
    target 835
  ]
  edge [
    source 26
    target 836
  ]
  edge [
    source 26
    target 333
  ]
  edge [
    source 26
    target 837
  ]
  edge [
    source 26
    target 838
  ]
  edge [
    source 26
    target 840
  ]
  edge [
    source 26
    target 841
  ]
  edge [
    source 26
    target 842
  ]
  edge [
    source 26
    target 843
  ]
  edge [
    source 26
    target 844
  ]
  edge [
    source 26
    target 845
  ]
  edge [
    source 26
    target 1354
  ]
  edge [
    source 26
    target 1355
  ]
  edge [
    source 26
    target 1356
  ]
  edge [
    source 26
    target 1357
  ]
  edge [
    source 26
    target 1358
  ]
  edge [
    source 26
    target 1359
  ]
  edge [
    source 26
    target 1360
  ]
  edge [
    source 26
    target 1361
  ]
  edge [
    source 26
    target 1362
  ]
  edge [
    source 26
    target 1363
  ]
  edge [
    source 26
    target 1364
  ]
  edge [
    source 26
    target 1365
  ]
  edge [
    source 26
    target 1366
  ]
  edge [
    source 26
    target 1367
  ]
  edge [
    source 26
    target 1368
  ]
  edge [
    source 26
    target 1369
  ]
  edge [
    source 26
    target 1370
  ]
  edge [
    source 26
    target 1371
  ]
  edge [
    source 26
    target 1372
  ]
  edge [
    source 26
    target 1373
  ]
  edge [
    source 26
    target 1374
  ]
  edge [
    source 26
    target 1375
  ]
  edge [
    source 26
    target 846
  ]
  edge [
    source 26
    target 1376
  ]
  edge [
    source 407
    target 1377
  ]
  edge [
    source 407
    target 1381
  ]
  edge [
    source 1377
    target 1378
  ]
  edge [
    source 1377
    target 1381
  ]
  edge [
    source 1379
    target 1380
  ]
  edge [
    source 1382
    target 1383
  ]
]
