graph [
  node [
    id 0
    label "cab"
    origin "text"
  ]
  node [
    id 1
    label "calloway"
    origin "text"
  ]
  node [
    id 2
    label "Cab"
  ]
  node [
    id 3
    label "Calloway"
  ]
  node [
    id 4
    label "The"
  ]
  node [
    id 5
    label "blues"
  ]
  node [
    id 6
    label "Brothers"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
]
