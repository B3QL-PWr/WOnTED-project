graph [
  node [
    id 0
    label "rada"
    origin "text"
  ]
  node [
    id 1
    label "miejski"
    origin "text"
  ]
  node [
    id 2
    label "radom"
    origin "text"
  ]
  node [
    id 3
    label "zwraca&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "apel"
    origin "text"
  ]
  node [
    id 6
    label "uwzgl&#281;dnienie"
    origin "text"
  ]
  node [
    id 7
    label "program"
    origin "text"
  ]
  node [
    id 8
    label "budowa"
    origin "text"
  ]
  node [
    id 9
    label "droga"
    origin "text"
  ]
  node [
    id 10
    label "krajowy"
    origin "text"
  ]
  node [
    id 11
    label "lata"
    origin "text"
  ]
  node [
    id 12
    label "inwestycja"
    origin "text"
  ]
  node [
    id 13
    label "drogowe"
    origin "text"
  ]
  node [
    id 14
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 15
    label "dla"
    origin "text"
  ]
  node [
    id 16
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 17
    label "region"
    origin "text"
  ]
  node [
    id 18
    label "radomskie"
    origin "text"
  ]
  node [
    id 19
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 20
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 21
    label "pomin&#261;&#263;"
    origin "text"
  ]
  node [
    id 22
    label "lub"
    origin "text"
  ]
  node [
    id 23
    label "przesun&#261;&#263;"
    origin "text"
  ]
  node [
    id 24
    label "daleki"
    origin "text"
  ]
  node [
    id 25
    label "okres"
    origin "text"
  ]
  node [
    id 26
    label "realizacja"
    origin "text"
  ]
  node [
    id 27
    label "konsylium"
  ]
  node [
    id 28
    label "Rada_Europejska"
  ]
  node [
    id 29
    label "dyskusja"
  ]
  node [
    id 30
    label "posiedzenie"
  ]
  node [
    id 31
    label "Rada_Europy"
  ]
  node [
    id 32
    label "Rada_Unii_Europejskiej"
  ]
  node [
    id 33
    label "wskaz&#243;wka"
  ]
  node [
    id 34
    label "conference"
  ]
  node [
    id 35
    label "zgromadzenie"
  ]
  node [
    id 36
    label "grupa"
  ]
  node [
    id 37
    label "organ"
  ]
  node [
    id 38
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 39
    label "convention"
  ]
  node [
    id 40
    label "adjustment"
  ]
  node [
    id 41
    label "pobycie"
  ]
  node [
    id 42
    label "porobienie"
  ]
  node [
    id 43
    label "odsiedzenie"
  ]
  node [
    id 44
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 45
    label "spotkanie"
  ]
  node [
    id 46
    label "spowodowanie"
  ]
  node [
    id 47
    label "pozyskanie"
  ]
  node [
    id 48
    label "kongregacja"
  ]
  node [
    id 49
    label "templum"
  ]
  node [
    id 50
    label "gromadzenie"
  ]
  node [
    id 51
    label "gathering"
  ]
  node [
    id 52
    label "konwentykiel"
  ]
  node [
    id 53
    label "klasztor"
  ]
  node [
    id 54
    label "caucus"
  ]
  node [
    id 55
    label "skupienie"
  ]
  node [
    id 56
    label "wsp&#243;lnota"
  ]
  node [
    id 57
    label "concourse"
  ]
  node [
    id 58
    label "czynno&#347;&#263;"
  ]
  node [
    id 59
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 60
    label "uk&#322;ad"
  ]
  node [
    id 61
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 62
    label "Komitet_Region&#243;w"
  ]
  node [
    id 63
    label "struktura_anatomiczna"
  ]
  node [
    id 64
    label "organogeneza"
  ]
  node [
    id 65
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 66
    label "tw&#243;r"
  ]
  node [
    id 67
    label "tkanka"
  ]
  node [
    id 68
    label "stomia"
  ]
  node [
    id 69
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 70
    label "dekortykacja"
  ]
  node [
    id 71
    label "okolica"
  ]
  node [
    id 72
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 73
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 74
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 75
    label "Izba_Konsyliarska"
  ]
  node [
    id 76
    label "zesp&#243;&#322;"
  ]
  node [
    id 77
    label "jednostka_organizacyjna"
  ]
  node [
    id 78
    label "asymilowa&#263;"
  ]
  node [
    id 79
    label "kompozycja"
  ]
  node [
    id 80
    label "pakiet_klimatyczny"
  ]
  node [
    id 81
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 82
    label "type"
  ]
  node [
    id 83
    label "cz&#261;steczka"
  ]
  node [
    id 84
    label "gromada"
  ]
  node [
    id 85
    label "specgrupa"
  ]
  node [
    id 86
    label "egzemplarz"
  ]
  node [
    id 87
    label "stage_set"
  ]
  node [
    id 88
    label "asymilowanie"
  ]
  node [
    id 89
    label "zbi&#243;r"
  ]
  node [
    id 90
    label "odm&#322;odzenie"
  ]
  node [
    id 91
    label "odm&#322;adza&#263;"
  ]
  node [
    id 92
    label "harcerze_starsi"
  ]
  node [
    id 93
    label "jednostka_systematyczna"
  ]
  node [
    id 94
    label "oddzia&#322;"
  ]
  node [
    id 95
    label "category"
  ]
  node [
    id 96
    label "liga"
  ]
  node [
    id 97
    label "&#346;wietliki"
  ]
  node [
    id 98
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 99
    label "formacja_geologiczna"
  ]
  node [
    id 100
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 101
    label "Eurogrupa"
  ]
  node [
    id 102
    label "Terranie"
  ]
  node [
    id 103
    label "odm&#322;adzanie"
  ]
  node [
    id 104
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 105
    label "Entuzjastki"
  ]
  node [
    id 106
    label "sympozjon"
  ]
  node [
    id 107
    label "rozmowa"
  ]
  node [
    id 108
    label "tarcza"
  ]
  node [
    id 109
    label "solucja"
  ]
  node [
    id 110
    label "implikowa&#263;"
  ]
  node [
    id 111
    label "informacja"
  ]
  node [
    id 112
    label "zegar"
  ]
  node [
    id 113
    label "fakt"
  ]
  node [
    id 114
    label "obrady"
  ]
  node [
    id 115
    label "konsultacja"
  ]
  node [
    id 116
    label "typowy"
  ]
  node [
    id 117
    label "publiczny"
  ]
  node [
    id 118
    label "miejsko"
  ]
  node [
    id 119
    label "miastowy"
  ]
  node [
    id 120
    label "upublicznienie"
  ]
  node [
    id 121
    label "publicznie"
  ]
  node [
    id 122
    label "upublicznianie"
  ]
  node [
    id 123
    label "jawny"
  ]
  node [
    id 124
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 125
    label "typowo"
  ]
  node [
    id 126
    label "zwyk&#322;y"
  ]
  node [
    id 127
    label "zwyczajny"
  ]
  node [
    id 128
    label "cz&#281;sty"
  ]
  node [
    id 129
    label "nowoczesny"
  ]
  node [
    id 130
    label "obywatel"
  ]
  node [
    id 131
    label "mieszczanin"
  ]
  node [
    id 132
    label "mieszcza&#324;stwo"
  ]
  node [
    id 133
    label "charakterystycznie"
  ]
  node [
    id 134
    label "wydala&#263;"
  ]
  node [
    id 135
    label "przeznacza&#263;"
  ]
  node [
    id 136
    label "haftowa&#263;"
  ]
  node [
    id 137
    label "ustawia&#263;"
  ]
  node [
    id 138
    label "give"
  ]
  node [
    id 139
    label "przekazywa&#263;"
  ]
  node [
    id 140
    label "indicate"
  ]
  node [
    id 141
    label "manipulate"
  ]
  node [
    id 142
    label "wp&#322;aca&#263;"
  ]
  node [
    id 143
    label "sygna&#322;"
  ]
  node [
    id 144
    label "powodowa&#263;"
  ]
  node [
    id 145
    label "wysy&#322;a&#263;"
  ]
  node [
    id 146
    label "podawa&#263;"
  ]
  node [
    id 147
    label "impart"
  ]
  node [
    id 148
    label "robi&#263;"
  ]
  node [
    id 149
    label "ustala&#263;"
  ]
  node [
    id 150
    label "blurt_out"
  ]
  node [
    id 151
    label "usuwa&#263;"
  ]
  node [
    id 152
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 153
    label "decydowa&#263;"
  ]
  node [
    id 154
    label "wyznacza&#263;"
  ]
  node [
    id 155
    label "kierowa&#263;"
  ]
  node [
    id 156
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 157
    label "zabezpiecza&#263;"
  ]
  node [
    id 158
    label "umieszcza&#263;"
  ]
  node [
    id 159
    label "go"
  ]
  node [
    id 160
    label "range"
  ]
  node [
    id 161
    label "nak&#322;ania&#263;"
  ]
  node [
    id 162
    label "nadawa&#263;"
  ]
  node [
    id 163
    label "porz&#261;dkowa&#263;"
  ]
  node [
    id 164
    label "stanowisko"
  ]
  node [
    id 165
    label "peddle"
  ]
  node [
    id 166
    label "poprawia&#263;"
  ]
  node [
    id 167
    label "przyznawa&#263;"
  ]
  node [
    id 168
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 169
    label "wskazywa&#263;"
  ]
  node [
    id 170
    label "train"
  ]
  node [
    id 171
    label "wyszywa&#263;"
  ]
  node [
    id 172
    label "embroider"
  ]
  node [
    id 173
    label "wymiotowa&#263;"
  ]
  node [
    id 174
    label "uderzenie"
  ]
  node [
    id 175
    label "szermierka"
  ]
  node [
    id 176
    label "pies_my&#347;liwski"
  ]
  node [
    id 177
    label "bid"
  ]
  node [
    id 178
    label "zdyscyplinowanie"
  ]
  node [
    id 179
    label "recoil"
  ]
  node [
    id 180
    label "pro&#347;ba"
  ]
  node [
    id 181
    label "zbi&#243;rka"
  ]
  node [
    id 182
    label "znak"
  ]
  node [
    id 183
    label "match"
  ]
  node [
    id 184
    label "spotkanie_si&#281;"
  ]
  node [
    id 185
    label "gather"
  ]
  node [
    id 186
    label "zawarcie"
  ]
  node [
    id 187
    label "zdarzenie_si&#281;"
  ]
  node [
    id 188
    label "po&#380;egnanie"
  ]
  node [
    id 189
    label "spotykanie"
  ]
  node [
    id 190
    label "wydarzenie"
  ]
  node [
    id 191
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 192
    label "powitanie"
  ]
  node [
    id 193
    label "doznanie"
  ]
  node [
    id 194
    label "znalezienie"
  ]
  node [
    id 195
    label "employment"
  ]
  node [
    id 196
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 197
    label "znajomy"
  ]
  node [
    id 198
    label "pogorszenie"
  ]
  node [
    id 199
    label "time"
  ]
  node [
    id 200
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 201
    label "d&#378;wi&#281;k"
  ]
  node [
    id 202
    label "odbicie"
  ]
  node [
    id 203
    label "odbicie_si&#281;"
  ]
  node [
    id 204
    label "dotkni&#281;cie"
  ]
  node [
    id 205
    label "zadanie"
  ]
  node [
    id 206
    label "pobicie"
  ]
  node [
    id 207
    label "skrytykowanie"
  ]
  node [
    id 208
    label "charge"
  ]
  node [
    id 209
    label "instrumentalizacja"
  ]
  node [
    id 210
    label "manewr"
  ]
  node [
    id 211
    label "st&#322;uczenie"
  ]
  node [
    id 212
    label "rush"
  ]
  node [
    id 213
    label "uderzanie"
  ]
  node [
    id 214
    label "walka"
  ]
  node [
    id 215
    label "pogoda"
  ]
  node [
    id 216
    label "stukni&#281;cie"
  ]
  node [
    id 217
    label "&#347;ci&#281;cie"
  ]
  node [
    id 218
    label "dotyk"
  ]
  node [
    id 219
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 220
    label "trafienie"
  ]
  node [
    id 221
    label "zagrywka"
  ]
  node [
    id 222
    label "dostanie"
  ]
  node [
    id 223
    label "poczucie"
  ]
  node [
    id 224
    label "reakcja"
  ]
  node [
    id 225
    label "cios"
  ]
  node [
    id 226
    label "stroke"
  ]
  node [
    id 227
    label "contact"
  ]
  node [
    id 228
    label "nast&#261;pienie"
  ]
  node [
    id 229
    label "bat"
  ]
  node [
    id 230
    label "flap"
  ]
  node [
    id 231
    label "wdarcie_si&#281;"
  ]
  node [
    id 232
    label "ruch"
  ]
  node [
    id 233
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 234
    label "zrobienie"
  ]
  node [
    id 235
    label "dawka"
  ]
  node [
    id 236
    label "coup"
  ]
  node [
    id 237
    label "zachowanie"
  ]
  node [
    id 238
    label "zachowywa&#263;"
  ]
  node [
    id 239
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 240
    label "zachowa&#263;"
  ]
  node [
    id 241
    label "mores"
  ]
  node [
    id 242
    label "podporz&#261;dkowanie"
  ]
  node [
    id 243
    label "porz&#261;dek"
  ]
  node [
    id 244
    label "nauczenie"
  ]
  node [
    id 245
    label "zachowywanie"
  ]
  node [
    id 246
    label "wypowied&#378;"
  ]
  node [
    id 247
    label "solicitation"
  ]
  node [
    id 248
    label "postawi&#263;"
  ]
  node [
    id 249
    label "mark"
  ]
  node [
    id 250
    label "kodzik"
  ]
  node [
    id 251
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 252
    label "oznakowanie"
  ]
  node [
    id 253
    label "attribute"
  ]
  node [
    id 254
    label "wytw&#243;r"
  ]
  node [
    id 255
    label "point"
  ]
  node [
    id 256
    label "dow&#243;d"
  ]
  node [
    id 257
    label "herb"
  ]
  node [
    id 258
    label "stawia&#263;"
  ]
  node [
    id 259
    label "pulsation"
  ]
  node [
    id 260
    label "wizja"
  ]
  node [
    id 261
    label "fala"
  ]
  node [
    id 262
    label "czynnik"
  ]
  node [
    id 263
    label "modulacja"
  ]
  node [
    id 264
    label "po&#322;&#261;czenie"
  ]
  node [
    id 265
    label "przewodzenie"
  ]
  node [
    id 266
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 267
    label "demodulacja"
  ]
  node [
    id 268
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 269
    label "medium_transmisyjne"
  ]
  node [
    id 270
    label "drift"
  ]
  node [
    id 271
    label "doj&#347;cie"
  ]
  node [
    id 272
    label "przekazywanie"
  ]
  node [
    id 273
    label "aliasing"
  ]
  node [
    id 274
    label "przekazanie"
  ]
  node [
    id 275
    label "przewodzi&#263;"
  ]
  node [
    id 276
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 277
    label "doj&#347;&#263;"
  ]
  node [
    id 278
    label "przekaza&#263;"
  ]
  node [
    id 279
    label "zapowied&#378;"
  ]
  node [
    id 280
    label "pozycja"
  ]
  node [
    id 281
    label "ripostowanie"
  ]
  node [
    id 282
    label "sport_walki"
  ]
  node [
    id 283
    label "tusz"
  ]
  node [
    id 284
    label "wi&#261;zanie"
  ]
  node [
    id 285
    label "plansza"
  ]
  node [
    id 286
    label "czarna_kartka"
  ]
  node [
    id 287
    label "fight"
  ]
  node [
    id 288
    label "sztuka"
  ]
  node [
    id 289
    label "manszeta"
  ]
  node [
    id 290
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 291
    label "ripostowa&#263;"
  ]
  node [
    id 292
    label "czerwona_kartka"
  ]
  node [
    id 293
    label "koszyk&#243;wka"
  ]
  node [
    id 294
    label "kwestowanie"
  ]
  node [
    id 295
    label "kwestarz"
  ]
  node [
    id 296
    label "collection"
  ]
  node [
    id 297
    label "chwyt"
  ]
  node [
    id 298
    label "wzi&#281;cie"
  ]
  node [
    id 299
    label "acknowledgment"
  ]
  node [
    id 300
    label "wyruchanie"
  ]
  node [
    id 301
    label "u&#380;ycie"
  ]
  node [
    id 302
    label "wej&#347;cie"
  ]
  node [
    id 303
    label "obj&#281;cie"
  ]
  node [
    id 304
    label "si&#281;gni&#281;cie"
  ]
  node [
    id 305
    label "nakazanie"
  ]
  node [
    id 306
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 307
    label "udanie_si&#281;"
  ]
  node [
    id 308
    label "zaopatrzenie_si&#281;"
  ]
  node [
    id 309
    label "powodzenie"
  ]
  node [
    id 310
    label "pokonanie"
  ]
  node [
    id 311
    label "niesienie"
  ]
  node [
    id 312
    label "ruszenie"
  ]
  node [
    id 313
    label "wygranie"
  ]
  node [
    id 314
    label "bite"
  ]
  node [
    id 315
    label "poczytanie"
  ]
  node [
    id 316
    label "zniesienie"
  ]
  node [
    id 317
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 318
    label "dmuchni&#281;cie"
  ]
  node [
    id 319
    label "wywiezienie"
  ]
  node [
    id 320
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 321
    label "pickings"
  ]
  node [
    id 322
    label "odziedziczenie"
  ]
  node [
    id 323
    label "branie"
  ]
  node [
    id 324
    label "take"
  ]
  node [
    id 325
    label "pozabieranie"
  ]
  node [
    id 326
    label "wych&#281;do&#380;enie"
  ]
  node [
    id 327
    label "w&#322;o&#380;enie"
  ]
  node [
    id 328
    label "uciekni&#281;cie"
  ]
  node [
    id 329
    label "otrzymanie"
  ]
  node [
    id 330
    label "capture"
  ]
  node [
    id 331
    label "pobranie"
  ]
  node [
    id 332
    label "wymienienie_si&#281;"
  ]
  node [
    id 333
    label "przyj&#281;cie"
  ]
  node [
    id 334
    label "wzi&#261;&#263;"
  ]
  node [
    id 335
    label "kupienie"
  ]
  node [
    id 336
    label "zacz&#281;cie"
  ]
  node [
    id 337
    label "za&#322;o&#380;enie"
  ]
  node [
    id 338
    label "dzia&#322;"
  ]
  node [
    id 339
    label "odinstalowa&#263;"
  ]
  node [
    id 340
    label "spis"
  ]
  node [
    id 341
    label "broszura"
  ]
  node [
    id 342
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 343
    label "informatyka"
  ]
  node [
    id 344
    label "odinstalowywa&#263;"
  ]
  node [
    id 345
    label "furkacja"
  ]
  node [
    id 346
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 347
    label "ogranicznik_referencyjny"
  ]
  node [
    id 348
    label "oprogramowanie"
  ]
  node [
    id 349
    label "blok"
  ]
  node [
    id 350
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 351
    label "prezentowa&#263;"
  ]
  node [
    id 352
    label "emitowa&#263;"
  ]
  node [
    id 353
    label "kana&#322;"
  ]
  node [
    id 354
    label "sekcja_krytyczna"
  ]
  node [
    id 355
    label "pirat"
  ]
  node [
    id 356
    label "folder"
  ]
  node [
    id 357
    label "zaprezentowa&#263;"
  ]
  node [
    id 358
    label "course_of_study"
  ]
  node [
    id 359
    label "punkt"
  ]
  node [
    id 360
    label "zainstalowa&#263;"
  ]
  node [
    id 361
    label "emitowanie"
  ]
  node [
    id 362
    label "teleferie"
  ]
  node [
    id 363
    label "podstawa"
  ]
  node [
    id 364
    label "deklaracja"
  ]
  node [
    id 365
    label "instrukcja"
  ]
  node [
    id 366
    label "zainstalowanie"
  ]
  node [
    id 367
    label "zaprezentowanie"
  ]
  node [
    id 368
    label "instalowa&#263;"
  ]
  node [
    id 369
    label "oferta"
  ]
  node [
    id 370
    label "odinstalowanie"
  ]
  node [
    id 371
    label "odinstalowywanie"
  ]
  node [
    id 372
    label "okno"
  ]
  node [
    id 373
    label "ram&#243;wka"
  ]
  node [
    id 374
    label "tryb"
  ]
  node [
    id 375
    label "menu"
  ]
  node [
    id 376
    label "podprogram"
  ]
  node [
    id 377
    label "instalowanie"
  ]
  node [
    id 378
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 379
    label "booklet"
  ]
  node [
    id 380
    label "struktura_organizacyjna"
  ]
  node [
    id 381
    label "interfejs"
  ]
  node [
    id 382
    label "prezentowanie"
  ]
  node [
    id 383
    label "wydawnictwo"
  ]
  node [
    id 384
    label "druk_ulotny"
  ]
  node [
    id 385
    label "zasi&#261;g"
  ]
  node [
    id 386
    label "distribution"
  ]
  node [
    id 387
    label "zakres"
  ]
  node [
    id 388
    label "rozmiar"
  ]
  node [
    id 389
    label "bridge"
  ]
  node [
    id 390
    label "izochronizm"
  ]
  node [
    id 391
    label "strategia"
  ]
  node [
    id 392
    label "background"
  ]
  node [
    id 393
    label "przedmiot"
  ]
  node [
    id 394
    label "punkt_odniesienia"
  ]
  node [
    id 395
    label "zasadzenie"
  ]
  node [
    id 396
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 397
    label "&#347;ciana"
  ]
  node [
    id 398
    label "podstawowy"
  ]
  node [
    id 399
    label "dzieci&#281;ctwo"
  ]
  node [
    id 400
    label "d&#243;&#322;"
  ]
  node [
    id 401
    label "documentation"
  ]
  node [
    id 402
    label "bok"
  ]
  node [
    id 403
    label "pomys&#322;"
  ]
  node [
    id 404
    label "zasadzi&#263;"
  ]
  node [
    id 405
    label "column"
  ]
  node [
    id 406
    label "pot&#281;ga"
  ]
  node [
    id 407
    label "rezultat"
  ]
  node [
    id 408
    label "p&#322;&#243;d"
  ]
  node [
    id 409
    label "work"
  ]
  node [
    id 410
    label "spos&#243;b"
  ]
  node [
    id 411
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 412
    label "modalno&#347;&#263;"
  ]
  node [
    id 413
    label "cecha"
  ]
  node [
    id 414
    label "z&#261;b"
  ]
  node [
    id 415
    label "koniugacja"
  ]
  node [
    id 416
    label "kategoria_gramatyczna"
  ]
  node [
    id 417
    label "funkcjonowa&#263;"
  ]
  node [
    id 418
    label "skala"
  ]
  node [
    id 419
    label "ko&#322;o"
  ]
  node [
    id 420
    label "propozycja"
  ]
  node [
    id 421
    label "offer"
  ]
  node [
    id 422
    label "formularz"
  ]
  node [
    id 423
    label "announcement"
  ]
  node [
    id 424
    label "o&#347;wiadczenie"
  ]
  node [
    id 425
    label "akt"
  ]
  node [
    id 426
    label "digest"
  ]
  node [
    id 427
    label "obietnica"
  ]
  node [
    id 428
    label "dokument"
  ]
  node [
    id 429
    label "konstrukcja"
  ]
  node [
    id 430
    label "o&#347;wiadczyny"
  ]
  node [
    id 431
    label "statement"
  ]
  node [
    id 432
    label "gara&#380;"
  ]
  node [
    id 433
    label "syfon"
  ]
  node [
    id 434
    label "przew&#243;d"
  ]
  node [
    id 435
    label "chody"
  ]
  node [
    id 436
    label "urz&#261;dzenie"
  ]
  node [
    id 437
    label "ciek"
  ]
  node [
    id 438
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 439
    label "miejsce"
  ]
  node [
    id 440
    label "grodzisko"
  ]
  node [
    id 441
    label "szaniec"
  ]
  node [
    id 442
    label "warsztat"
  ]
  node [
    id 443
    label "zrzutowy"
  ]
  node [
    id 444
    label "kanalizacja"
  ]
  node [
    id 445
    label "teatr"
  ]
  node [
    id 446
    label "klarownia"
  ]
  node [
    id 447
    label "pit"
  ]
  node [
    id 448
    label "piaskownik"
  ]
  node [
    id 449
    label "bystrza"
  ]
  node [
    id 450
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 451
    label "topologia_magistrali"
  ]
  node [
    id 452
    label "tarapaty"
  ]
  node [
    id 453
    label "odwa&#322;"
  ]
  node [
    id 454
    label "odk&#322;ad"
  ]
  node [
    id 455
    label "catalog"
  ]
  node [
    id 456
    label "figurowa&#263;"
  ]
  node [
    id 457
    label "tekst"
  ]
  node [
    id 458
    label "wyliczanka"
  ]
  node [
    id 459
    label "sumariusz"
  ]
  node [
    id 460
    label "stock"
  ]
  node [
    id 461
    label "book"
  ]
  node [
    id 462
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 463
    label "usuwanie"
  ]
  node [
    id 464
    label "usuni&#281;cie"
  ]
  node [
    id 465
    label "zamek"
  ]
  node [
    id 466
    label "infa"
  ]
  node [
    id 467
    label "gramatyka_formalna"
  ]
  node [
    id 468
    label "baza_danych"
  ]
  node [
    id 469
    label "HP"
  ]
  node [
    id 470
    label "kryptologia"
  ]
  node [
    id 471
    label "przetwarzanie_informacji"
  ]
  node [
    id 472
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 473
    label "dost&#281;p"
  ]
  node [
    id 474
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 475
    label "dziedzina_informatyki"
  ]
  node [
    id 476
    label "kierunek"
  ]
  node [
    id 477
    label "sztuczna_inteligencja"
  ]
  node [
    id 478
    label "artefakt"
  ]
  node [
    id 479
    label "komputer"
  ]
  node [
    id 480
    label "zrobi&#263;"
  ]
  node [
    id 481
    label "install"
  ]
  node [
    id 482
    label "umie&#347;ci&#263;"
  ]
  node [
    id 483
    label "dostosowa&#263;"
  ]
  node [
    id 484
    label "dostosowywa&#263;"
  ]
  node [
    id 485
    label "supply"
  ]
  node [
    id 486
    label "fit"
  ]
  node [
    id 487
    label "accommodate"
  ]
  node [
    id 488
    label "zdolno&#347;&#263;"
  ]
  node [
    id 489
    label "usun&#261;&#263;"
  ]
  node [
    id 490
    label "layout"
  ]
  node [
    id 491
    label "installation"
  ]
  node [
    id 492
    label "proposition"
  ]
  node [
    id 493
    label "pozak&#322;adanie"
  ]
  node [
    id 494
    label "dostosowanie"
  ]
  node [
    id 495
    label "umieszczenie"
  ]
  node [
    id 496
    label "parapet"
  ]
  node [
    id 497
    label "okiennica"
  ]
  node [
    id 498
    label "lufcik"
  ]
  node [
    id 499
    label "futryna"
  ]
  node [
    id 500
    label "prze&#347;wit"
  ]
  node [
    id 501
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 502
    label "inspekt"
  ]
  node [
    id 503
    label "szyba"
  ]
  node [
    id 504
    label "nora"
  ]
  node [
    id 505
    label "pulpit"
  ]
  node [
    id 506
    label "nadokiennik"
  ]
  node [
    id 507
    label "skrzyd&#322;o"
  ]
  node [
    id 508
    label "transenna"
  ]
  node [
    id 509
    label "kwatera_okienna"
  ]
  node [
    id 510
    label "otw&#243;r"
  ]
  node [
    id 511
    label "menad&#380;er_okien"
  ]
  node [
    id 512
    label "casement"
  ]
  node [
    id 513
    label "wmontowanie"
  ]
  node [
    id 514
    label "wmontowywanie"
  ]
  node [
    id 515
    label "dostosowywanie"
  ]
  node [
    id 516
    label "umieszczanie"
  ]
  node [
    id 517
    label "robienie"
  ]
  node [
    id 518
    label "fitting"
  ]
  node [
    id 519
    label "uprzedzi&#263;"
  ]
  node [
    id 520
    label "testify"
  ]
  node [
    id 521
    label "pokaza&#263;"
  ]
  node [
    id 522
    label "zapozna&#263;"
  ]
  node [
    id 523
    label "attest"
  ]
  node [
    id 524
    label "typify"
  ]
  node [
    id 525
    label "represent"
  ]
  node [
    id 526
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 527
    label "przedstawi&#263;"
  ]
  node [
    id 528
    label "present"
  ]
  node [
    id 529
    label "wyra&#380;anie"
  ]
  node [
    id 530
    label "presentation"
  ]
  node [
    id 531
    label "granie"
  ]
  node [
    id 532
    label "zapoznawanie"
  ]
  node [
    id 533
    label "demonstrowanie"
  ]
  node [
    id 534
    label "display"
  ]
  node [
    id 535
    label "representation"
  ]
  node [
    id 536
    label "uprzedzanie"
  ]
  node [
    id 537
    label "przedstawianie"
  ]
  node [
    id 538
    label "rozb&#243;jnik"
  ]
  node [
    id 539
    label "kopiowa&#263;"
  ]
  node [
    id 540
    label "podr&#243;bka"
  ]
  node [
    id 541
    label "&#380;agl&#243;wka"
  ]
  node [
    id 542
    label "rum"
  ]
  node [
    id 543
    label "przest&#281;pca"
  ]
  node [
    id 544
    label "postrzeleniec"
  ]
  node [
    id 545
    label "kieruj&#261;cy"
  ]
  node [
    id 546
    label "uprzedzenie"
  ]
  node [
    id 547
    label "przedstawienie"
  ]
  node [
    id 548
    label "exhibit"
  ]
  node [
    id 549
    label "pokazanie"
  ]
  node [
    id 550
    label "zapoznanie_si&#281;"
  ]
  node [
    id 551
    label "wyst&#261;pienie"
  ]
  node [
    id 552
    label "zapoznanie"
  ]
  node [
    id 553
    label "uprzedza&#263;"
  ]
  node [
    id 554
    label "gra&#263;"
  ]
  node [
    id 555
    label "przedstawia&#263;"
  ]
  node [
    id 556
    label "wyra&#380;a&#263;"
  ]
  node [
    id 557
    label "zapoznawa&#263;"
  ]
  node [
    id 558
    label "tembr"
  ]
  node [
    id 559
    label "wprowadzanie"
  ]
  node [
    id 560
    label "wydzielanie"
  ]
  node [
    id 561
    label "wydzielenie"
  ]
  node [
    id 562
    label "wysy&#322;anie"
  ]
  node [
    id 563
    label "wprowadzenie"
  ]
  node [
    id 564
    label "energia"
  ]
  node [
    id 565
    label "wydobywanie"
  ]
  node [
    id 566
    label "issue"
  ]
  node [
    id 567
    label "nadanie"
  ]
  node [
    id 568
    label "wydobycie"
  ]
  node [
    id 569
    label "emission"
  ]
  node [
    id 570
    label "wys&#322;anie"
  ]
  node [
    id 571
    label "rynek"
  ]
  node [
    id 572
    label "nadawanie"
  ]
  node [
    id 573
    label "wys&#322;a&#263;"
  ]
  node [
    id 574
    label "emit"
  ]
  node [
    id 575
    label "air"
  ]
  node [
    id 576
    label "wydoby&#263;"
  ]
  node [
    id 577
    label "nada&#263;"
  ]
  node [
    id 578
    label "wprowadzi&#263;"
  ]
  node [
    id 579
    label "wprowadza&#263;"
  ]
  node [
    id 580
    label "wydobywa&#263;"
  ]
  node [
    id 581
    label "wydziela&#263;"
  ]
  node [
    id 582
    label "wydzieli&#263;"
  ]
  node [
    id 583
    label "instruktarz"
  ]
  node [
    id 584
    label "ulotka"
  ]
  node [
    id 585
    label "routine"
  ]
  node [
    id 586
    label "proceduralnie"
  ]
  node [
    id 587
    label "danie"
  ]
  node [
    id 588
    label "cennik"
  ]
  node [
    id 589
    label "chart"
  ]
  node [
    id 590
    label "restauracja"
  ]
  node [
    id 591
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 592
    label "zestaw"
  ]
  node [
    id 593
    label "karta"
  ]
  node [
    id 594
    label "skorupa_ziemska"
  ]
  node [
    id 595
    label "budynek"
  ]
  node [
    id 596
    label "przeszkoda"
  ]
  node [
    id 597
    label "bry&#322;a"
  ]
  node [
    id 598
    label "j&#261;kanie"
  ]
  node [
    id 599
    label "square"
  ]
  node [
    id 600
    label "bloking"
  ]
  node [
    id 601
    label "kontynent"
  ]
  node [
    id 602
    label "ok&#322;adka"
  ]
  node [
    id 603
    label "kr&#261;g"
  ]
  node [
    id 604
    label "start"
  ]
  node [
    id 605
    label "blockage"
  ]
  node [
    id 606
    label "blokowisko"
  ]
  node [
    id 607
    label "artyku&#322;"
  ]
  node [
    id 608
    label "blokada"
  ]
  node [
    id 609
    label "whole"
  ]
  node [
    id 610
    label "stok_kontynentalny"
  ]
  node [
    id 611
    label "bajt"
  ]
  node [
    id 612
    label "barak"
  ]
  node [
    id 613
    label "referat"
  ]
  node [
    id 614
    label "nastawnia"
  ]
  node [
    id 615
    label "obrona"
  ]
  node [
    id 616
    label "organizacja"
  ]
  node [
    id 617
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 618
    label "dom_wielorodzinny"
  ]
  node [
    id 619
    label "zeszyt"
  ]
  node [
    id 620
    label "siatk&#243;wka"
  ]
  node [
    id 621
    label "block"
  ]
  node [
    id 622
    label "bie&#380;nia"
  ]
  node [
    id 623
    label "urz&#261;d"
  ]
  node [
    id 624
    label "miejsce_pracy"
  ]
  node [
    id 625
    label "poddzia&#322;"
  ]
  node [
    id 626
    label "bezdro&#380;e"
  ]
  node [
    id 627
    label "insourcing"
  ]
  node [
    id 628
    label "stopie&#324;"
  ]
  node [
    id 629
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 630
    label "competence"
  ]
  node [
    id 631
    label "sfera"
  ]
  node [
    id 632
    label "infliction"
  ]
  node [
    id 633
    label "przygotowanie"
  ]
  node [
    id 634
    label "poubieranie"
  ]
  node [
    id 635
    label "rozebranie"
  ]
  node [
    id 636
    label "str&#243;j"
  ]
  node [
    id 637
    label "budowla"
  ]
  node [
    id 638
    label "przewidzenie"
  ]
  node [
    id 639
    label "zak&#322;adka"
  ]
  node [
    id 640
    label "twierdzenie"
  ]
  node [
    id 641
    label "przygotowywanie"
  ]
  node [
    id 642
    label "podwini&#281;cie"
  ]
  node [
    id 643
    label "zap&#322;acenie"
  ]
  node [
    id 644
    label "wyko&#324;czenie"
  ]
  node [
    id 645
    label "struktura"
  ]
  node [
    id 646
    label "utworzenie"
  ]
  node [
    id 647
    label "przebranie"
  ]
  node [
    id 648
    label "obleczenie"
  ]
  node [
    id 649
    label "przymierzenie"
  ]
  node [
    id 650
    label "obleczenie_si&#281;"
  ]
  node [
    id 651
    label "przywdzianie"
  ]
  node [
    id 652
    label "przyodzianie"
  ]
  node [
    id 653
    label "pokrycie"
  ]
  node [
    id 654
    label "obiekt_matematyczny"
  ]
  node [
    id 655
    label "stopie&#324;_pisma"
  ]
  node [
    id 656
    label "problemat"
  ]
  node [
    id 657
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 658
    label "obiekt"
  ]
  node [
    id 659
    label "plamka"
  ]
  node [
    id 660
    label "przestrze&#324;"
  ]
  node [
    id 661
    label "ust&#281;p"
  ]
  node [
    id 662
    label "po&#322;o&#380;enie"
  ]
  node [
    id 663
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 664
    label "kres"
  ]
  node [
    id 665
    label "plan"
  ]
  node [
    id 666
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 667
    label "chwila"
  ]
  node [
    id 668
    label "podpunkt"
  ]
  node [
    id 669
    label "jednostka"
  ]
  node [
    id 670
    label "sprawa"
  ]
  node [
    id 671
    label "problematyka"
  ]
  node [
    id 672
    label "prosta"
  ]
  node [
    id 673
    label "wojsko"
  ]
  node [
    id 674
    label "zapunktowa&#263;"
  ]
  node [
    id 675
    label "reengineering"
  ]
  node [
    id 676
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 677
    label "okienko"
  ]
  node [
    id 678
    label "scheduling"
  ]
  node [
    id 679
    label "figura"
  ]
  node [
    id 680
    label "constitution"
  ]
  node [
    id 681
    label "wjazd"
  ]
  node [
    id 682
    label "zwierz&#281;"
  ]
  node [
    id 683
    label "praca"
  ]
  node [
    id 684
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 685
    label "r&#243;w"
  ]
  node [
    id 686
    label "mechanika"
  ]
  node [
    id 687
    label "kreacja"
  ]
  node [
    id 688
    label "posesja"
  ]
  node [
    id 689
    label "zaw&#243;d"
  ]
  node [
    id 690
    label "zmiana"
  ]
  node [
    id 691
    label "pracowanie"
  ]
  node [
    id 692
    label "pracowa&#263;"
  ]
  node [
    id 693
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 694
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 695
    label "czynnik_produkcji"
  ]
  node [
    id 696
    label "stosunek_pracy"
  ]
  node [
    id 697
    label "kierownictwo"
  ]
  node [
    id 698
    label "najem"
  ]
  node [
    id 699
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 700
    label "siedziba"
  ]
  node [
    id 701
    label "zak&#322;ad"
  ]
  node [
    id 702
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 703
    label "tynkarski"
  ]
  node [
    id 704
    label "tyrka"
  ]
  node [
    id 705
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 706
    label "benedykty&#324;ski"
  ]
  node [
    id 707
    label "poda&#380;_pracy"
  ]
  node [
    id 708
    label "zobowi&#261;zanie"
  ]
  node [
    id 709
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 710
    label "charakterystyka"
  ]
  node [
    id 711
    label "m&#322;ot"
  ]
  node [
    id 712
    label "marka"
  ]
  node [
    id 713
    label "pr&#243;ba"
  ]
  node [
    id 714
    label "drzewo"
  ]
  node [
    id 715
    label "kostium"
  ]
  node [
    id 716
    label "production"
  ]
  node [
    id 717
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 718
    label "plisa"
  ]
  node [
    id 719
    label "reinterpretowanie"
  ]
  node [
    id 720
    label "element"
  ]
  node [
    id 721
    label "aktorstwo"
  ]
  node [
    id 722
    label "zagra&#263;"
  ]
  node [
    id 723
    label "ustawienie"
  ]
  node [
    id 724
    label "zreinterpretowa&#263;"
  ]
  node [
    id 725
    label "reinterpretowa&#263;"
  ]
  node [
    id 726
    label "posta&#263;"
  ]
  node [
    id 727
    label "function"
  ]
  node [
    id 728
    label "ustawi&#263;"
  ]
  node [
    id 729
    label "tren"
  ]
  node [
    id 730
    label "toaleta"
  ]
  node [
    id 731
    label "zreinterpretowanie"
  ]
  node [
    id 732
    label "zagranie"
  ]
  node [
    id 733
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 734
    label "obszar"
  ]
  node [
    id 735
    label "o&#347;"
  ]
  node [
    id 736
    label "podsystem"
  ]
  node [
    id 737
    label "systemat"
  ]
  node [
    id 738
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 739
    label "system"
  ]
  node [
    id 740
    label "rozprz&#261;c"
  ]
  node [
    id 741
    label "cybernetyk"
  ]
  node [
    id 742
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 743
    label "konstelacja"
  ]
  node [
    id 744
    label "usenet"
  ]
  node [
    id 745
    label "sk&#322;ad"
  ]
  node [
    id 746
    label "monogamia"
  ]
  node [
    id 747
    label "grzbiet"
  ]
  node [
    id 748
    label "bestia"
  ]
  node [
    id 749
    label "treser"
  ]
  node [
    id 750
    label "niecz&#322;owiek"
  ]
  node [
    id 751
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 752
    label "agresja"
  ]
  node [
    id 753
    label "skubni&#281;cie"
  ]
  node [
    id 754
    label "skuba&#263;"
  ]
  node [
    id 755
    label "tresowa&#263;"
  ]
  node [
    id 756
    label "oz&#243;r"
  ]
  node [
    id 757
    label "istota_&#380;ywa"
  ]
  node [
    id 758
    label "wylinka"
  ]
  node [
    id 759
    label "poskramia&#263;"
  ]
  node [
    id 760
    label "fukni&#281;cie"
  ]
  node [
    id 761
    label "siedzenie"
  ]
  node [
    id 762
    label "wios&#322;owa&#263;"
  ]
  node [
    id 763
    label "zwyrol"
  ]
  node [
    id 764
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 765
    label "budowa_cia&#322;a"
  ]
  node [
    id 766
    label "sodomita"
  ]
  node [
    id 767
    label "wiwarium"
  ]
  node [
    id 768
    label "oswaja&#263;"
  ]
  node [
    id 769
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 770
    label "degenerat"
  ]
  node [
    id 771
    label "le&#380;e&#263;"
  ]
  node [
    id 772
    label "przyssawka"
  ]
  node [
    id 773
    label "animalista"
  ]
  node [
    id 774
    label "fauna"
  ]
  node [
    id 775
    label "hodowla"
  ]
  node [
    id 776
    label "popapraniec"
  ]
  node [
    id 777
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 778
    label "cz&#322;owiek"
  ]
  node [
    id 779
    label "le&#380;enie"
  ]
  node [
    id 780
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 781
    label "poligamia"
  ]
  node [
    id 782
    label "siedzie&#263;"
  ]
  node [
    id 783
    label "napasienie_si&#281;"
  ]
  node [
    id 784
    label "&#322;eb"
  ]
  node [
    id 785
    label "paszcza"
  ]
  node [
    id 786
    label "czerniak"
  ]
  node [
    id 787
    label "zwierz&#281;ta"
  ]
  node [
    id 788
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 789
    label "skubn&#261;&#263;"
  ]
  node [
    id 790
    label "wios&#322;owanie"
  ]
  node [
    id 791
    label "skubanie"
  ]
  node [
    id 792
    label "okrutnik"
  ]
  node [
    id 793
    label "pasienie_si&#281;"
  ]
  node [
    id 794
    label "farba"
  ]
  node [
    id 795
    label "weterynarz"
  ]
  node [
    id 796
    label "gad"
  ]
  node [
    id 797
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 798
    label "fukanie"
  ]
  node [
    id 799
    label "gestaltyzm"
  ]
  node [
    id 800
    label "ornamentyka"
  ]
  node [
    id 801
    label "stylistyka"
  ]
  node [
    id 802
    label "podzbi&#243;r"
  ]
  node [
    id 803
    label "Osjan"
  ]
  node [
    id 804
    label "kto&#347;"
  ]
  node [
    id 805
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 806
    label "styl"
  ]
  node [
    id 807
    label "antycypacja"
  ]
  node [
    id 808
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 809
    label "wiersz"
  ]
  node [
    id 810
    label "facet"
  ]
  node [
    id 811
    label "popis"
  ]
  node [
    id 812
    label "Aspazja"
  ]
  node [
    id 813
    label "obraz"
  ]
  node [
    id 814
    label "p&#322;aszczyzna"
  ]
  node [
    id 815
    label "symetria"
  ]
  node [
    id 816
    label "figure"
  ]
  node [
    id 817
    label "rzecz"
  ]
  node [
    id 818
    label "perspektywa"
  ]
  node [
    id 819
    label "lingwistyka_kognitywna"
  ]
  node [
    id 820
    label "character"
  ]
  node [
    id 821
    label "wygl&#261;d"
  ]
  node [
    id 822
    label "rze&#378;ba"
  ]
  node [
    id 823
    label "kompleksja"
  ]
  node [
    id 824
    label "shape"
  ]
  node [
    id 825
    label "bierka_szachowa"
  ]
  node [
    id 826
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 827
    label "mechanika_klasyczna"
  ]
  node [
    id 828
    label "hydromechanika"
  ]
  node [
    id 829
    label "telemechanika"
  ]
  node [
    id 830
    label "elektromechanika"
  ]
  node [
    id 831
    label "mechanika_o&#347;rodk&#243;w_ci&#261;g&#322;ych"
  ]
  node [
    id 832
    label "mechanika_g&#243;rotworu"
  ]
  node [
    id 833
    label "fizyka"
  ]
  node [
    id 834
    label "mechanika_gruntu"
  ]
  node [
    id 835
    label "aeromechanika"
  ]
  node [
    id 836
    label "mechanika_teoretyczna"
  ]
  node [
    id 837
    label "nauka"
  ]
  node [
    id 838
    label "wykre&#347;lanie"
  ]
  node [
    id 839
    label "practice"
  ]
  node [
    id 840
    label "element_konstrukcyjny"
  ]
  node [
    id 841
    label "obni&#380;enie"
  ]
  node [
    id 842
    label "przedpiersie"
  ]
  node [
    id 843
    label "fortyfikacja"
  ]
  node [
    id 844
    label "blinda&#380;"
  ]
  node [
    id 845
    label "antaba"
  ]
  node [
    id 846
    label "zawiasy"
  ]
  node [
    id 847
    label "ogrodzenie"
  ]
  node [
    id 848
    label "wrzeci&#261;dz"
  ]
  node [
    id 849
    label "ekwipunek"
  ]
  node [
    id 850
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 851
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 852
    label "podbieg"
  ]
  node [
    id 853
    label "wyb&#243;j"
  ]
  node [
    id 854
    label "journey"
  ]
  node [
    id 855
    label "pobocze"
  ]
  node [
    id 856
    label "ekskursja"
  ]
  node [
    id 857
    label "drogowskaz"
  ]
  node [
    id 858
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 859
    label "rajza"
  ]
  node [
    id 860
    label "passage"
  ]
  node [
    id 861
    label "marszrutyzacja"
  ]
  node [
    id 862
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 863
    label "trasa"
  ]
  node [
    id 864
    label "zbior&#243;wka"
  ]
  node [
    id 865
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 866
    label "turystyka"
  ]
  node [
    id 867
    label "wylot"
  ]
  node [
    id 868
    label "bezsilnikowy"
  ]
  node [
    id 869
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 870
    label "nawierzchnia"
  ]
  node [
    id 871
    label "korona_drogi"
  ]
  node [
    id 872
    label "przebieg"
  ]
  node [
    id 873
    label "infrastruktura"
  ]
  node [
    id 874
    label "w&#281;ze&#322;"
  ]
  node [
    id 875
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 876
    label "stan_surowy"
  ]
  node [
    id 877
    label "postanie"
  ]
  node [
    id 878
    label "zbudowa&#263;"
  ]
  node [
    id 879
    label "obudowywa&#263;"
  ]
  node [
    id 880
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 881
    label "obudowywanie"
  ]
  node [
    id 882
    label "Sukiennice"
  ]
  node [
    id 883
    label "kolumnada"
  ]
  node [
    id 884
    label "korpus"
  ]
  node [
    id 885
    label "zbudowanie"
  ]
  node [
    id 886
    label "fundament"
  ]
  node [
    id 887
    label "obudowa&#263;"
  ]
  node [
    id 888
    label "obudowanie"
  ]
  node [
    id 889
    label "model"
  ]
  node [
    id 890
    label "narz&#281;dzie"
  ]
  node [
    id 891
    label "nature"
  ]
  node [
    id 892
    label "odcinek"
  ]
  node [
    id 893
    label "ton"
  ]
  node [
    id 894
    label "ambitus"
  ]
  node [
    id 895
    label "czas"
  ]
  node [
    id 896
    label "move"
  ]
  node [
    id 897
    label "aktywno&#347;&#263;"
  ]
  node [
    id 898
    label "utrzymywanie"
  ]
  node [
    id 899
    label "utrzymywa&#263;"
  ]
  node [
    id 900
    label "taktyka"
  ]
  node [
    id 901
    label "d&#322;ugi"
  ]
  node [
    id 902
    label "natural_process"
  ]
  node [
    id 903
    label "kanciasty"
  ]
  node [
    id 904
    label "utrzyma&#263;"
  ]
  node [
    id 905
    label "myk"
  ]
  node [
    id 906
    label "utrzymanie"
  ]
  node [
    id 907
    label "tumult"
  ]
  node [
    id 908
    label "stopek"
  ]
  node [
    id 909
    label "movement"
  ]
  node [
    id 910
    label "strumie&#324;"
  ]
  node [
    id 911
    label "komunikacja"
  ]
  node [
    id 912
    label "lokomocja"
  ]
  node [
    id 913
    label "commercial_enterprise"
  ]
  node [
    id 914
    label "zjawisko"
  ]
  node [
    id 915
    label "apraksja"
  ]
  node [
    id 916
    label "proces"
  ]
  node [
    id 917
    label "poruszenie"
  ]
  node [
    id 918
    label "travel"
  ]
  node [
    id 919
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 920
    label "dyssypacja_energii"
  ]
  node [
    id 921
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 922
    label "kr&#243;tki"
  ]
  node [
    id 923
    label "r&#281;kaw"
  ]
  node [
    id 924
    label "koniec"
  ]
  node [
    id 925
    label "kontusz"
  ]
  node [
    id 926
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 927
    label "warstwa"
  ]
  node [
    id 928
    label "tablica"
  ]
  node [
    id 929
    label "fingerpost"
  ]
  node [
    id 930
    label "przydro&#380;e"
  ]
  node [
    id 931
    label "autostrada"
  ]
  node [
    id 932
    label "bieg"
  ]
  node [
    id 933
    label "operacja"
  ]
  node [
    id 934
    label "podr&#243;&#380;"
  ]
  node [
    id 935
    label "mieszanie_si&#281;"
  ]
  node [
    id 936
    label "chodzenie"
  ]
  node [
    id 937
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 938
    label "stray"
  ]
  node [
    id 939
    label "pozostawa&#263;"
  ]
  node [
    id 940
    label "s&#261;dzi&#263;"
  ]
  node [
    id 941
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 942
    label "digress"
  ]
  node [
    id 943
    label "chodzi&#263;"
  ]
  node [
    id 944
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 945
    label "wyposa&#380;enie"
  ]
  node [
    id 946
    label "nie&#347;miertelnik"
  ]
  node [
    id 947
    label "moderunek"
  ]
  node [
    id 948
    label "kocher"
  ]
  node [
    id 949
    label "dormitorium"
  ]
  node [
    id 950
    label "fotografia"
  ]
  node [
    id 951
    label "sk&#322;adanka"
  ]
  node [
    id 952
    label "polowanie"
  ]
  node [
    id 953
    label "wyprawa"
  ]
  node [
    id 954
    label "pomieszczenie"
  ]
  node [
    id 955
    label "beznap&#281;dowy"
  ]
  node [
    id 956
    label "na_pieska"
  ]
  node [
    id 957
    label "erotyka"
  ]
  node [
    id 958
    label "zajawka"
  ]
  node [
    id 959
    label "love"
  ]
  node [
    id 960
    label "podniecanie"
  ]
  node [
    id 961
    label "po&#380;ycie"
  ]
  node [
    id 962
    label "ukochanie"
  ]
  node [
    id 963
    label "baraszki"
  ]
  node [
    id 964
    label "numer"
  ]
  node [
    id 965
    label "ruch_frykcyjny"
  ]
  node [
    id 966
    label "tendency"
  ]
  node [
    id 967
    label "wzw&#243;d"
  ]
  node [
    id 968
    label "serce"
  ]
  node [
    id 969
    label "wi&#281;&#378;"
  ]
  node [
    id 970
    label "seks"
  ]
  node [
    id 971
    label "pozycja_misjonarska"
  ]
  node [
    id 972
    label "rozmna&#380;anie"
  ]
  node [
    id 973
    label "feblik"
  ]
  node [
    id 974
    label "z&#322;&#261;czenie"
  ]
  node [
    id 975
    label "imisja"
  ]
  node [
    id 976
    label "podniecenie"
  ]
  node [
    id 977
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 978
    label "podnieca&#263;"
  ]
  node [
    id 979
    label "zakochanie"
  ]
  node [
    id 980
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 981
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 982
    label "gra_wst&#281;pna"
  ]
  node [
    id 983
    label "drogi"
  ]
  node [
    id 984
    label "po&#380;&#261;danie"
  ]
  node [
    id 985
    label "podnieci&#263;"
  ]
  node [
    id 986
    label "emocja"
  ]
  node [
    id 987
    label "afekt"
  ]
  node [
    id 988
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 989
    label "kochanka"
  ]
  node [
    id 990
    label "psiapsi&#243;&#322;ka"
  ]
  node [
    id 991
    label "kultura_fizyczna"
  ]
  node [
    id 992
    label "turyzm"
  ]
  node [
    id 993
    label "rodzimy"
  ]
  node [
    id 994
    label "w&#322;asny"
  ]
  node [
    id 995
    label "tutejszy"
  ]
  node [
    id 996
    label "summer"
  ]
  node [
    id 997
    label "chronometria"
  ]
  node [
    id 998
    label "odczyt"
  ]
  node [
    id 999
    label "laba"
  ]
  node [
    id 1000
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1001
    label "time_period"
  ]
  node [
    id 1002
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1003
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1004
    label "Zeitgeist"
  ]
  node [
    id 1005
    label "pochodzenie"
  ]
  node [
    id 1006
    label "przep&#322;ywanie"
  ]
  node [
    id 1007
    label "schy&#322;ek"
  ]
  node [
    id 1008
    label "czwarty_wymiar"
  ]
  node [
    id 1009
    label "poprzedzi&#263;"
  ]
  node [
    id 1010
    label "czasokres"
  ]
  node [
    id 1011
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1012
    label "poprzedzenie"
  ]
  node [
    id 1013
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1014
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1015
    label "dzieje"
  ]
  node [
    id 1016
    label "trawi&#263;"
  ]
  node [
    id 1017
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1018
    label "poprzedza&#263;"
  ]
  node [
    id 1019
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1020
    label "trawienie"
  ]
  node [
    id 1021
    label "rachuba_czasu"
  ]
  node [
    id 1022
    label "poprzedzanie"
  ]
  node [
    id 1023
    label "okres_czasu"
  ]
  node [
    id 1024
    label "period"
  ]
  node [
    id 1025
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1026
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1027
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1028
    label "pochodzi&#263;"
  ]
  node [
    id 1029
    label "wydatki_maj&#261;tkowe"
  ]
  node [
    id 1030
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 1031
    label "inwestycje"
  ]
  node [
    id 1032
    label "inwestowanie"
  ]
  node [
    id 1033
    label "sentyment_inwestycyjny"
  ]
  node [
    id 1034
    label "kapita&#322;"
  ]
  node [
    id 1035
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 1036
    label "wk&#322;ad"
  ]
  node [
    id 1037
    label "bud&#380;et_domowy"
  ]
  node [
    id 1038
    label "przyczyna"
  ]
  node [
    id 1039
    label "typ"
  ]
  node [
    id 1040
    label "dzia&#322;anie"
  ]
  node [
    id 1041
    label "event"
  ]
  node [
    id 1042
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 1043
    label "zas&#243;b"
  ]
  node [
    id 1044
    label "zaleta"
  ]
  node [
    id 1045
    label "&#347;rodowisko"
  ]
  node [
    id 1046
    label "absolutorium"
  ]
  node [
    id 1047
    label "podupadanie"
  ]
  node [
    id 1048
    label "nap&#322;ywanie"
  ]
  node [
    id 1049
    label "podupada&#263;"
  ]
  node [
    id 1050
    label "uruchamia&#263;"
  ]
  node [
    id 1051
    label "uruchamianie"
  ]
  node [
    id 1052
    label "mienie"
  ]
  node [
    id 1053
    label "kapitalista"
  ]
  node [
    id 1054
    label "supernadz&#243;r"
  ]
  node [
    id 1055
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 1056
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 1057
    label "kwestor"
  ]
  node [
    id 1058
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 1059
    label "uruchomienie"
  ]
  node [
    id 1060
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1061
    label "consumption"
  ]
  node [
    id 1062
    label "startup"
  ]
  node [
    id 1063
    label "uczestnictwo"
  ]
  node [
    id 1064
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1065
    label "input"
  ]
  node [
    id 1066
    label "kwota"
  ]
  node [
    id 1067
    label "lokata"
  ]
  node [
    id 1068
    label "czasopismo"
  ]
  node [
    id 1069
    label "kartka"
  ]
  node [
    id 1070
    label "analiza_bilansu"
  ]
  node [
    id 1071
    label "produkt_krajowy_brutto"
  ]
  node [
    id 1072
    label "inwestorski"
  ]
  node [
    id 1073
    label "kompletny"
  ]
  node [
    id 1074
    label "zdr&#243;w"
  ]
  node [
    id 1075
    label "ca&#322;o"
  ]
  node [
    id 1076
    label "du&#380;y"
  ]
  node [
    id 1077
    label "calu&#347;ko"
  ]
  node [
    id 1078
    label "podobny"
  ]
  node [
    id 1079
    label "&#380;ywy"
  ]
  node [
    id 1080
    label "pe&#322;ny"
  ]
  node [
    id 1081
    label "jedyny"
  ]
  node [
    id 1082
    label "w_pizdu"
  ]
  node [
    id 1083
    label "zupe&#322;ny"
  ]
  node [
    id 1084
    label "kompletnie"
  ]
  node [
    id 1085
    label "taki"
  ]
  node [
    id 1086
    label "drugi"
  ]
  node [
    id 1087
    label "upodobnienie_si&#281;"
  ]
  node [
    id 1088
    label "przypominanie"
  ]
  node [
    id 1089
    label "podobnie"
  ]
  node [
    id 1090
    label "charakterystyczny"
  ]
  node [
    id 1091
    label "upodabnianie_si&#281;"
  ]
  node [
    id 1092
    label "zasymilowanie"
  ]
  node [
    id 1093
    label "upodobnienie"
  ]
  node [
    id 1094
    label "optymalnie"
  ]
  node [
    id 1095
    label "najlepszy"
  ]
  node [
    id 1096
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 1097
    label "ukochany"
  ]
  node [
    id 1098
    label "znaczny"
  ]
  node [
    id 1099
    label "du&#380;o"
  ]
  node [
    id 1100
    label "wa&#380;ny"
  ]
  node [
    id 1101
    label "niema&#322;o"
  ]
  node [
    id 1102
    label "wiele"
  ]
  node [
    id 1103
    label "prawdziwy"
  ]
  node [
    id 1104
    label "rozwini&#281;ty"
  ]
  node [
    id 1105
    label "doros&#322;y"
  ]
  node [
    id 1106
    label "dorodny"
  ]
  node [
    id 1107
    label "realistyczny"
  ]
  node [
    id 1108
    label "silny"
  ]
  node [
    id 1109
    label "o&#380;ywianie"
  ]
  node [
    id 1110
    label "zgrabny"
  ]
  node [
    id 1111
    label "&#380;ycie"
  ]
  node [
    id 1112
    label "g&#322;&#281;boki"
  ]
  node [
    id 1113
    label "energiczny"
  ]
  node [
    id 1114
    label "naturalny"
  ]
  node [
    id 1115
    label "ciekawy"
  ]
  node [
    id 1116
    label "&#380;ywo"
  ]
  node [
    id 1117
    label "wyra&#378;ny"
  ]
  node [
    id 1118
    label "&#380;ywotny"
  ]
  node [
    id 1119
    label "czynny"
  ]
  node [
    id 1120
    label "aktualny"
  ]
  node [
    id 1121
    label "szybki"
  ]
  node [
    id 1122
    label "zdrowy"
  ]
  node [
    id 1123
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 1124
    label "nieograniczony"
  ]
  node [
    id 1125
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 1126
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 1127
    label "bezwzgl&#281;dny"
  ]
  node [
    id 1128
    label "satysfakcja"
  ]
  node [
    id 1129
    label "pe&#322;no"
  ]
  node [
    id 1130
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 1131
    label "r&#243;wny"
  ]
  node [
    id 1132
    label "wype&#322;nienie"
  ]
  node [
    id 1133
    label "otwarty"
  ]
  node [
    id 1134
    label "odpowiednio"
  ]
  node [
    id 1135
    label "nieuszkodzony"
  ]
  node [
    id 1136
    label "Krajina"
  ]
  node [
    id 1137
    label "Lotaryngia"
  ]
  node [
    id 1138
    label "Lubuskie"
  ]
  node [
    id 1139
    label "&#379;mud&#378;"
  ]
  node [
    id 1140
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1141
    label "Ko&#322;yma"
  ]
  node [
    id 1142
    label "okr&#281;g"
  ]
  node [
    id 1143
    label "Skandynawia"
  ]
  node [
    id 1144
    label "Kampania"
  ]
  node [
    id 1145
    label "Zakarpacie"
  ]
  node [
    id 1146
    label "Podlasie"
  ]
  node [
    id 1147
    label "Wielkopolska"
  ]
  node [
    id 1148
    label "Indochiny"
  ]
  node [
    id 1149
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1150
    label "Bo&#347;nia"
  ]
  node [
    id 1151
    label "Kaukaz"
  ]
  node [
    id 1152
    label "Opolszczyzna"
  ]
  node [
    id 1153
    label "Armagnac"
  ]
  node [
    id 1154
    label "Polesie"
  ]
  node [
    id 1155
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1156
    label "Yorkshire"
  ]
  node [
    id 1157
    label "Bawaria"
  ]
  node [
    id 1158
    label "Syjon"
  ]
  node [
    id 1159
    label "Apulia"
  ]
  node [
    id 1160
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1161
    label "Noworosja"
  ]
  node [
    id 1162
    label "Nadrenia"
  ]
  node [
    id 1163
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1164
    label "Kurpie"
  ]
  node [
    id 1165
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 1166
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 1167
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1168
    label "Naddniestrze"
  ]
  node [
    id 1169
    label "Azja_Wschodnia"
  ]
  node [
    id 1170
    label "jednostka_administracyjna"
  ]
  node [
    id 1171
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1172
    label "Baszkiria"
  ]
  node [
    id 1173
    label "Afryka_Wschodnia"
  ]
  node [
    id 1174
    label "Andaluzja"
  ]
  node [
    id 1175
    label "Afryka_Zachodnia"
  ]
  node [
    id 1176
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1177
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1178
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 1179
    label "Opolskie"
  ]
  node [
    id 1180
    label "Kociewie"
  ]
  node [
    id 1181
    label "Anglia"
  ]
  node [
    id 1182
    label "Bordeaux"
  ]
  node [
    id 1183
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1184
    label "Mazowsze"
  ]
  node [
    id 1185
    label "Laponia"
  ]
  node [
    id 1186
    label "Amazonia"
  ]
  node [
    id 1187
    label "Lasko"
  ]
  node [
    id 1188
    label "Hercegowina"
  ]
  node [
    id 1189
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 1190
    label "Liguria"
  ]
  node [
    id 1191
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 1192
    label "Lubelszczyzna"
  ]
  node [
    id 1193
    label "Tonkin"
  ]
  node [
    id 1194
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1195
    label "Oceania"
  ]
  node [
    id 1196
    label "Pamir"
  ]
  node [
    id 1197
    label "Podkarpacie"
  ]
  node [
    id 1198
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1199
    label "Tyrol"
  ]
  node [
    id 1200
    label "podregion"
  ]
  node [
    id 1201
    label "Bory_Tucholskie"
  ]
  node [
    id 1202
    label "Podhale"
  ]
  node [
    id 1203
    label "Chiny_Wschodnie"
  ]
  node [
    id 1204
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 1205
    label "Polinezja"
  ]
  node [
    id 1206
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1207
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1208
    label "Mazury"
  ]
  node [
    id 1209
    label "Europa_Wschodnia"
  ]
  node [
    id 1210
    label "Europa_Zachodnia"
  ]
  node [
    id 1211
    label "Zabajkale"
  ]
  node [
    id 1212
    label "Kielecczyzna"
  ]
  node [
    id 1213
    label "Turyngia"
  ]
  node [
    id 1214
    label "Ba&#322;kany"
  ]
  node [
    id 1215
    label "Kaszuby"
  ]
  node [
    id 1216
    label "Szlezwik"
  ]
  node [
    id 1217
    label "Mikronezja"
  ]
  node [
    id 1218
    label "Umbria"
  ]
  node [
    id 1219
    label "Oksytania"
  ]
  node [
    id 1220
    label "Mezoameryka"
  ]
  node [
    id 1221
    label "Turkiestan"
  ]
  node [
    id 1222
    label "Kurdystan"
  ]
  node [
    id 1223
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 1224
    label "Karaiby"
  ]
  node [
    id 1225
    label "Biskupizna"
  ]
  node [
    id 1226
    label "Podbeskidzie"
  ]
  node [
    id 1227
    label "Zag&#243;rze"
  ]
  node [
    id 1228
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1229
    label "Kalabria"
  ]
  node [
    id 1230
    label "Ma&#322;opolska"
  ]
  node [
    id 1231
    label "Szkocja"
  ]
  node [
    id 1232
    label "subregion"
  ]
  node [
    id 1233
    label "Huculszczyzna"
  ]
  node [
    id 1234
    label "Kraina"
  ]
  node [
    id 1235
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1236
    label "Sand&#380;ak"
  ]
  node [
    id 1237
    label "Kerala"
  ]
  node [
    id 1238
    label "S&#261;decczyzna"
  ]
  node [
    id 1239
    label "Lombardia"
  ]
  node [
    id 1240
    label "Toskania"
  ]
  node [
    id 1241
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1242
    label "Galicja"
  ]
  node [
    id 1243
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 1244
    label "Palestyna"
  ]
  node [
    id 1245
    label "Kabylia"
  ]
  node [
    id 1246
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 1247
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1248
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 1249
    label "country"
  ]
  node [
    id 1250
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1251
    label "Lauda"
  ]
  node [
    id 1252
    label "Kujawy"
  ]
  node [
    id 1253
    label "Warmia"
  ]
  node [
    id 1254
    label "Maghreb"
  ]
  node [
    id 1255
    label "Kaszmir"
  ]
  node [
    id 1256
    label "Chiny_Zachodnie"
  ]
  node [
    id 1257
    label "Bojkowszczyzna"
  ]
  node [
    id 1258
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 1259
    label "Amhara"
  ]
  node [
    id 1260
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1261
    label "Zamojszczyzna"
  ]
  node [
    id 1262
    label "Walia"
  ]
  node [
    id 1263
    label "Flandria"
  ]
  node [
    id 1264
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1265
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1266
    label "Burgundia"
  ]
  node [
    id 1267
    label "Powi&#347;le"
  ]
  node [
    id 1268
    label "Kosowo"
  ]
  node [
    id 1269
    label "zach&#243;d"
  ]
  node [
    id 1270
    label "Zabu&#380;e"
  ]
  node [
    id 1271
    label "wymiar"
  ]
  node [
    id 1272
    label "antroposfera"
  ]
  node [
    id 1273
    label "Arktyka"
  ]
  node [
    id 1274
    label "Notogea"
  ]
  node [
    id 1275
    label "Piotrowo"
  ]
  node [
    id 1276
    label "akrecja"
  ]
  node [
    id 1277
    label "Ludwin&#243;w"
  ]
  node [
    id 1278
    label "Ruda_Pabianicka"
  ]
  node [
    id 1279
    label "po&#322;udnie"
  ]
  node [
    id 1280
    label "wsch&#243;d"
  ]
  node [
    id 1281
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1282
    label "Pow&#261;zki"
  ]
  node [
    id 1283
    label "&#321;&#281;g"
  ]
  node [
    id 1284
    label "p&#243;&#322;noc"
  ]
  node [
    id 1285
    label "Rakowice"
  ]
  node [
    id 1286
    label "Syberia_Wschodnia"
  ]
  node [
    id 1287
    label "Zab&#322;ocie"
  ]
  node [
    id 1288
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1289
    label "Kresy_Zachodnie"
  ]
  node [
    id 1290
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1291
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1292
    label "holarktyka"
  ]
  node [
    id 1293
    label "terytorium"
  ]
  node [
    id 1294
    label "pas_planetoid"
  ]
  node [
    id 1295
    label "Antarktyka"
  ]
  node [
    id 1296
    label "Syberia_Zachodnia"
  ]
  node [
    id 1297
    label "Neogea"
  ]
  node [
    id 1298
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1299
    label "Olszanica"
  ]
  node [
    id 1300
    label "Judea"
  ]
  node [
    id 1301
    label "Kanaan"
  ]
  node [
    id 1302
    label "moszaw"
  ]
  node [
    id 1303
    label "Algieria"
  ]
  node [
    id 1304
    label "Anguilla"
  ]
  node [
    id 1305
    label "Portoryko"
  ]
  node [
    id 1306
    label "Kuba"
  ]
  node [
    id 1307
    label "Bahamy"
  ]
  node [
    id 1308
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1309
    label "Antyle"
  ]
  node [
    id 1310
    label "Aruba"
  ]
  node [
    id 1311
    label "Kajmany"
  ]
  node [
    id 1312
    label "Haiti"
  ]
  node [
    id 1313
    label "Jamajka"
  ]
  node [
    id 1314
    label "Polska"
  ]
  node [
    id 1315
    label "Mogielnica"
  ]
  node [
    id 1316
    label "Indie"
  ]
  node [
    id 1317
    label "jezioro"
  ]
  node [
    id 1318
    label "Niemcy"
  ]
  node [
    id 1319
    label "Rumelia"
  ]
  node [
    id 1320
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1321
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1322
    label "Poprad"
  ]
  node [
    id 1323
    label "Podtatrze"
  ]
  node [
    id 1324
    label "Tatry"
  ]
  node [
    id 1325
    label "Podole"
  ]
  node [
    id 1326
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1327
    label "Austro-W&#281;gry"
  ]
  node [
    id 1328
    label "Hiszpania"
  ]
  node [
    id 1329
    label "W&#322;ochy"
  ]
  node [
    id 1330
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1331
    label "Ropa"
  ]
  node [
    id 1332
    label "Rogo&#378;nik"
  ]
  node [
    id 1333
    label "Iwanowice"
  ]
  node [
    id 1334
    label "Biskupice"
  ]
  node [
    id 1335
    label "Wietnam"
  ]
  node [
    id 1336
    label "Etiopia"
  ]
  node [
    id 1337
    label "Alpy"
  ]
  node [
    id 1338
    label "Austria"
  ]
  node [
    id 1339
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1340
    label "Francja"
  ]
  node [
    id 1341
    label "dolar"
  ]
  node [
    id 1342
    label "Nauru"
  ]
  node [
    id 1343
    label "Wyspy_Marshalla"
  ]
  node [
    id 1344
    label "Mariany"
  ]
  node [
    id 1345
    label "Karpaty"
  ]
  node [
    id 1346
    label "Tuwalu"
  ]
  node [
    id 1347
    label "Samoa"
  ]
  node [
    id 1348
    label "Tonga"
  ]
  node [
    id 1349
    label "Hawaje"
  ]
  node [
    id 1350
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1351
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1352
    label "Rosja"
  ]
  node [
    id 1353
    label "Beskid_Niski"
  ]
  node [
    id 1354
    label "Etruria"
  ]
  node [
    id 1355
    label "Wilkowo_Polskie"
  ]
  node [
    id 1356
    label "Obra"
  ]
  node [
    id 1357
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1358
    label "Bojanowo"
  ]
  node [
    id 1359
    label "Dobra"
  ]
  node [
    id 1360
    label "Buriacja"
  ]
  node [
    id 1361
    label "Rozewie"
  ]
  node [
    id 1362
    label "Czechy"
  ]
  node [
    id 1363
    label "&#346;l&#261;sk"
  ]
  node [
    id 1364
    label "Ukraina"
  ]
  node [
    id 1365
    label "Mo&#322;dawia"
  ]
  node [
    id 1366
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1367
    label "Finlandia"
  ]
  node [
    id 1368
    label "Norwegia"
  ]
  node [
    id 1369
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1370
    label "Szwecja"
  ]
  node [
    id 1371
    label "Wiktoria"
  ]
  node [
    id 1372
    label "Guernsey"
  ]
  node [
    id 1373
    label "Conrad"
  ]
  node [
    id 1374
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1375
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1376
    label "funt_szterling"
  ]
  node [
    id 1377
    label "NATO"
  ]
  node [
    id 1378
    label "Unia_Europejska"
  ]
  node [
    id 1379
    label "Portland"
  ]
  node [
    id 1380
    label "El&#380;bieta_I"
  ]
  node [
    id 1381
    label "Kornwalia"
  ]
  node [
    id 1382
    label "Wielka_Brytania"
  ]
  node [
    id 1383
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1384
    label "Amazonka"
  ]
  node [
    id 1385
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1386
    label "Libia"
  ]
  node [
    id 1387
    label "Maroko"
  ]
  node [
    id 1388
    label "Sahara_Zachodnia"
  ]
  node [
    id 1389
    label "Tunezja"
  ]
  node [
    id 1390
    label "Mauretania"
  ]
  node [
    id 1391
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1392
    label "Anglosas"
  ]
  node [
    id 1393
    label "Moza"
  ]
  node [
    id 1394
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1395
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1396
    label "Paj&#281;czno"
  ]
  node [
    id 1397
    label "Melanezja"
  ]
  node [
    id 1398
    label "Nowa_Zelandia"
  ]
  node [
    id 1399
    label "Ocean_Spokojny"
  ]
  node [
    id 1400
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1401
    label "Palau"
  ]
  node [
    id 1402
    label "Czarnog&#243;ra"
  ]
  node [
    id 1403
    label "Serbia"
  ]
  node [
    id 1404
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1405
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1406
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1407
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1408
    label "Gop&#322;o"
  ]
  node [
    id 1409
    label "Jerozolima"
  ]
  node [
    id 1410
    label "Dolna_Frankonia"
  ]
  node [
    id 1411
    label "funt_szkocki"
  ]
  node [
    id 1412
    label "Kaledonia"
  ]
  node [
    id 1413
    label "Eurazja"
  ]
  node [
    id 1414
    label "Inguszetia"
  ]
  node [
    id 1415
    label "Czeczenia"
  ]
  node [
    id 1416
    label "Abchazja"
  ]
  node [
    id 1417
    label "Sarmata"
  ]
  node [
    id 1418
    label "Dagestan"
  ]
  node [
    id 1419
    label "Mariensztat"
  ]
  node [
    id 1420
    label "Warszawa"
  ]
  node [
    id 1421
    label "Pakistan"
  ]
  node [
    id 1422
    label "j&#281;zyk_flamandzki"
  ]
  node [
    id 1423
    label "Belgia"
  ]
  node [
    id 1424
    label "Litwa"
  ]
  node [
    id 1425
    label "&#379;mujd&#378;"
  ]
  node [
    id 1426
    label "pa&#324;stwo"
  ]
  node [
    id 1427
    label "okr&#281;g_przemys&#322;owy"
  ]
  node [
    id 1428
    label "bluegrass"
  ]
  node [
    id 1429
    label "muzyka_rozrywkowa"
  ]
  node [
    id 1430
    label "proceed"
  ]
  node [
    id 1431
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1432
    label "pozosta&#263;"
  ]
  node [
    id 1433
    label "change"
  ]
  node [
    id 1434
    label "osta&#263;_si&#281;"
  ]
  node [
    id 1435
    label "catch"
  ]
  node [
    id 1436
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1437
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1438
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 1439
    label "support"
  ]
  node [
    id 1440
    label "prze&#380;y&#263;"
  ]
  node [
    id 1441
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 1442
    label "omin&#261;&#263;"
  ]
  node [
    id 1443
    label "spowodowa&#263;"
  ]
  node [
    id 1444
    label "zby&#263;"
  ]
  node [
    id 1445
    label "dispose"
  ]
  node [
    id 1446
    label "sprzeda&#263;"
  ]
  node [
    id 1447
    label "zareagowa&#263;"
  ]
  node [
    id 1448
    label "potraktowa&#263;"
  ]
  node [
    id 1449
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 1450
    label "obej&#347;&#263;"
  ]
  node [
    id 1451
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 1452
    label "wymin&#261;&#263;"
  ]
  node [
    id 1453
    label "sidestep"
  ]
  node [
    id 1454
    label "unikn&#261;&#263;"
  ]
  node [
    id 1455
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1456
    label "shed"
  ]
  node [
    id 1457
    label "straci&#263;"
  ]
  node [
    id 1458
    label "zlekcewa&#380;y&#263;"
  ]
  node [
    id 1459
    label "przej&#347;&#263;"
  ]
  node [
    id 1460
    label "act"
  ]
  node [
    id 1461
    label "przenie&#347;&#263;"
  ]
  node [
    id 1462
    label "zmieni&#263;"
  ]
  node [
    id 1463
    label "shift"
  ]
  node [
    id 1464
    label "motivate"
  ]
  node [
    id 1465
    label "deepen"
  ]
  node [
    id 1466
    label "ruszy&#263;"
  ]
  node [
    id 1467
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 1468
    label "transfer"
  ]
  node [
    id 1469
    label "come_up"
  ]
  node [
    id 1470
    label "sprawi&#263;"
  ]
  node [
    id 1471
    label "zyska&#263;"
  ]
  node [
    id 1472
    label "zast&#261;pi&#263;"
  ]
  node [
    id 1473
    label "rozpowszechni&#263;"
  ]
  node [
    id 1474
    label "skopiowa&#263;"
  ]
  node [
    id 1475
    label "przelecie&#263;"
  ]
  node [
    id 1476
    label "relocate"
  ]
  node [
    id 1477
    label "strzeli&#263;"
  ]
  node [
    id 1478
    label "pocisk"
  ]
  node [
    id 1479
    label "zacz&#261;&#263;"
  ]
  node [
    id 1480
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 1481
    label "zabra&#263;"
  ]
  node [
    id 1482
    label "cut"
  ]
  node [
    id 1483
    label "allude"
  ]
  node [
    id 1484
    label "wzbudzi&#263;"
  ]
  node [
    id 1485
    label "stimulate"
  ]
  node [
    id 1486
    label "adjust"
  ]
  node [
    id 1487
    label "klawisz"
  ]
  node [
    id 1488
    label "ilo&#347;&#263;"
  ]
  node [
    id 1489
    label "zamiana"
  ]
  node [
    id 1490
    label "przekaz"
  ]
  node [
    id 1491
    label "lista_transferowa"
  ]
  node [
    id 1492
    label "release"
  ]
  node [
    id 1493
    label "przysz&#322;y"
  ]
  node [
    id 1494
    label "odlegle"
  ]
  node [
    id 1495
    label "nieobecny"
  ]
  node [
    id 1496
    label "zwi&#261;zany"
  ]
  node [
    id 1497
    label "odleg&#322;y"
  ]
  node [
    id 1498
    label "dawny"
  ]
  node [
    id 1499
    label "ogl&#281;dny"
  ]
  node [
    id 1500
    label "obcy"
  ]
  node [
    id 1501
    label "oddalony"
  ]
  node [
    id 1502
    label "daleko"
  ]
  node [
    id 1503
    label "r&#243;&#380;ny"
  ]
  node [
    id 1504
    label "s&#322;aby"
  ]
  node [
    id 1505
    label "cudzy"
  ]
  node [
    id 1506
    label "obco"
  ]
  node [
    id 1507
    label "pozaludzki"
  ]
  node [
    id 1508
    label "zaziemsko"
  ]
  node [
    id 1509
    label "inny"
  ]
  node [
    id 1510
    label "nadprzyrodzony"
  ]
  node [
    id 1511
    label "osoba"
  ]
  node [
    id 1512
    label "tameczny"
  ]
  node [
    id 1513
    label "nieznajomo"
  ]
  node [
    id 1514
    label "nieznany"
  ]
  node [
    id 1515
    label "delikatny"
  ]
  node [
    id 1516
    label "kolejny"
  ]
  node [
    id 1517
    label "stan"
  ]
  node [
    id 1518
    label "opuszczenie"
  ]
  node [
    id 1519
    label "opuszczanie"
  ]
  node [
    id 1520
    label "nieprzytomny"
  ]
  node [
    id 1521
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1522
    label "zro&#347;ni&#281;cie_si&#281;"
  ]
  node [
    id 1523
    label "lura"
  ]
  node [
    id 1524
    label "niedoskona&#322;y"
  ]
  node [
    id 1525
    label "przemijaj&#261;cy"
  ]
  node [
    id 1526
    label "zawodny"
  ]
  node [
    id 1527
    label "niefajny"
  ]
  node [
    id 1528
    label "md&#322;y"
  ]
  node [
    id 1529
    label "kiepsko"
  ]
  node [
    id 1530
    label "nietrwa&#322;y"
  ]
  node [
    id 1531
    label "nieumiej&#281;tny"
  ]
  node [
    id 1532
    label "mizerny"
  ]
  node [
    id 1533
    label "s&#322;abo"
  ]
  node [
    id 1534
    label "po&#347;ledni"
  ]
  node [
    id 1535
    label "nieznaczny"
  ]
  node [
    id 1536
    label "marnie"
  ]
  node [
    id 1537
    label "s&#322;abowity"
  ]
  node [
    id 1538
    label "nieudany"
  ]
  node [
    id 1539
    label "niemocny"
  ]
  node [
    id 1540
    label "&#322;agodny"
  ]
  node [
    id 1541
    label "z&#322;y"
  ]
  node [
    id 1542
    label "niezdrowy"
  ]
  node [
    id 1543
    label "od_dawna"
  ]
  node [
    id 1544
    label "anachroniczny"
  ]
  node [
    id 1545
    label "dawniej"
  ]
  node [
    id 1546
    label "przesz&#322;y"
  ]
  node [
    id 1547
    label "d&#322;ugoletni"
  ]
  node [
    id 1548
    label "poprzedni"
  ]
  node [
    id 1549
    label "dawno"
  ]
  node [
    id 1550
    label "przestarza&#322;y"
  ]
  node [
    id 1551
    label "kombatant"
  ]
  node [
    id 1552
    label "niegdysiejszy"
  ]
  node [
    id 1553
    label "wcze&#347;niejszy"
  ]
  node [
    id 1554
    label "stary"
  ]
  node [
    id 1555
    label "stosowny"
  ]
  node [
    id 1556
    label "okr&#261;g&#322;y"
  ]
  node [
    id 1557
    label "og&#243;lny"
  ]
  node [
    id 1558
    label "cnotliwy"
  ]
  node [
    id 1559
    label "byle_jaki"
  ]
  node [
    id 1560
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 1561
    label "ogl&#281;dnie"
  ]
  node [
    id 1562
    label "niedok&#322;adny"
  ]
  node [
    id 1563
    label "ch&#322;odny"
  ]
  node [
    id 1564
    label "g&#322;&#281;boko"
  ]
  node [
    id 1565
    label "intensywny"
  ]
  node [
    id 1566
    label "m&#261;dry"
  ]
  node [
    id 1567
    label "wyrazisty"
  ]
  node [
    id 1568
    label "niezrozumia&#322;y"
  ]
  node [
    id 1569
    label "ukryty"
  ]
  node [
    id 1570
    label "gruntowny"
  ]
  node [
    id 1571
    label "dog&#322;&#281;bny"
  ]
  node [
    id 1572
    label "mocny"
  ]
  node [
    id 1573
    label "szczery"
  ]
  node [
    id 1574
    label "niski"
  ]
  node [
    id 1575
    label "oderwany"
  ]
  node [
    id 1576
    label "jaki&#347;"
  ]
  node [
    id 1577
    label "r&#243;&#380;nie"
  ]
  node [
    id 1578
    label "het"
  ]
  node [
    id 1579
    label "znacznie"
  ]
  node [
    id 1580
    label "wysoko"
  ]
  node [
    id 1581
    label "nieobecnie"
  ]
  node [
    id 1582
    label "nisko"
  ]
  node [
    id 1583
    label "d&#322;ugo"
  ]
  node [
    id 1584
    label "zdanie"
  ]
  node [
    id 1585
    label "podokres"
  ]
  node [
    id 1586
    label "neogen"
  ]
  node [
    id 1587
    label "trias"
  ]
  node [
    id 1588
    label "riak"
  ]
  node [
    id 1589
    label "trzeciorz&#281;d"
  ]
  node [
    id 1590
    label "kreda"
  ]
  node [
    id 1591
    label "orosir"
  ]
  node [
    id 1592
    label "okres_noachijski"
  ]
  node [
    id 1593
    label "epoka"
  ]
  node [
    id 1594
    label "preglacja&#322;"
  ]
  node [
    id 1595
    label "cykl"
  ]
  node [
    id 1596
    label "rok_akademicki"
  ]
  node [
    id 1597
    label "paleogen"
  ]
  node [
    id 1598
    label "stater"
  ]
  node [
    id 1599
    label "interstadia&#322;"
  ]
  node [
    id 1600
    label "rok_szkolny"
  ]
  node [
    id 1601
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 1602
    label "choroba_przyrodzona"
  ]
  node [
    id 1603
    label "sider"
  ]
  node [
    id 1604
    label "czwartorz&#281;d"
  ]
  node [
    id 1605
    label "pierwszorz&#281;d"
  ]
  node [
    id 1606
    label "ciota"
  ]
  node [
    id 1607
    label "spell"
  ]
  node [
    id 1608
    label "condition"
  ]
  node [
    id 1609
    label "postglacja&#322;"
  ]
  node [
    id 1610
    label "semester"
  ]
  node [
    id 1611
    label "dewon"
  ]
  node [
    id 1612
    label "era"
  ]
  node [
    id 1613
    label "okres_hesperyjski"
  ]
  node [
    id 1614
    label "jednostka_geologiczna"
  ]
  node [
    id 1615
    label "prekambr"
  ]
  node [
    id 1616
    label "kalim"
  ]
  node [
    id 1617
    label "p&#243;&#322;okres"
  ]
  node [
    id 1618
    label "sten"
  ]
  node [
    id 1619
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 1620
    label "nast&#281;pnik"
  ]
  node [
    id 1621
    label "flow"
  ]
  node [
    id 1622
    label "sylur"
  ]
  node [
    id 1623
    label "karbon"
  ]
  node [
    id 1624
    label "jura"
  ]
  node [
    id 1625
    label "proces_fizjologiczny"
  ]
  node [
    id 1626
    label "poprzednik"
  ]
  node [
    id 1627
    label "glacja&#322;"
  ]
  node [
    id 1628
    label "pulsacja"
  ]
  node [
    id 1629
    label "drugorz&#281;d"
  ]
  node [
    id 1630
    label "kriogen"
  ]
  node [
    id 1631
    label "okres_amazo&#324;ski"
  ]
  node [
    id 1632
    label "okres_halsztacki"
  ]
  node [
    id 1633
    label "ordowik"
  ]
  node [
    id 1634
    label "kambr"
  ]
  node [
    id 1635
    label "retoryka"
  ]
  node [
    id 1636
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 1637
    label "ektas"
  ]
  node [
    id 1638
    label "ediakar"
  ]
  node [
    id 1639
    label "faza"
  ]
  node [
    id 1640
    label "perm"
  ]
  node [
    id 1641
    label "oferma"
  ]
  node [
    id 1642
    label "gej"
  ]
  node [
    id 1643
    label "zniewie&#347;cialec"
  ]
  node [
    id 1644
    label "miesi&#261;czka"
  ]
  node [
    id 1645
    label "mazgaj"
  ]
  node [
    id 1646
    label "pedalstwo"
  ]
  node [
    id 1647
    label "szko&#322;a"
  ]
  node [
    id 1648
    label "adjudication"
  ]
  node [
    id 1649
    label "prison_term"
  ]
  node [
    id 1650
    label "pass"
  ]
  node [
    id 1651
    label "powierzenie"
  ]
  node [
    id 1652
    label "fraza"
  ]
  node [
    id 1653
    label "wyra&#380;enie"
  ]
  node [
    id 1654
    label "konektyw"
  ]
  node [
    id 1655
    label "zaliczenie"
  ]
  node [
    id 1656
    label "wypowiedzenie"
  ]
  node [
    id 1657
    label "zmuszenie"
  ]
  node [
    id 1658
    label "attitude"
  ]
  node [
    id 1659
    label "antylogizm"
  ]
  node [
    id 1660
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 1661
    label "bajos"
  ]
  node [
    id 1662
    label "kelowej"
  ]
  node [
    id 1663
    label "paleocen"
  ]
  node [
    id 1664
    label "&#347;rodkowy_trias"
  ]
  node [
    id 1665
    label "miocen"
  ]
  node [
    id 1666
    label "plejstocen"
  ]
  node [
    id 1667
    label "aalen"
  ]
  node [
    id 1668
    label "jura_&#347;rodkowa"
  ]
  node [
    id 1669
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 1670
    label "jura_wczesna"
  ]
  node [
    id 1671
    label "eocen"
  ]
  node [
    id 1672
    label "term"
  ]
  node [
    id 1673
    label "wczesny_trias"
  ]
  node [
    id 1674
    label "holocen"
  ]
  node [
    id 1675
    label "pliocen"
  ]
  node [
    id 1676
    label "oligocen"
  ]
  node [
    id 1677
    label "argument"
  ]
  node [
    id 1678
    label "implikacja"
  ]
  node [
    id 1679
    label "stream"
  ]
  node [
    id 1680
    label "rozbijanie_si&#281;"
  ]
  node [
    id 1681
    label "efekt_Dopplera"
  ]
  node [
    id 1682
    label "przemoc"
  ]
  node [
    id 1683
    label "grzywa_fali"
  ]
  node [
    id 1684
    label "obcinka"
  ]
  node [
    id 1685
    label "zafalowanie"
  ]
  node [
    id 1686
    label "znak_diakrytyczny"
  ]
  node [
    id 1687
    label "clutter"
  ]
  node [
    id 1688
    label "rozbicie_si&#281;"
  ]
  node [
    id 1689
    label "zafalowa&#263;"
  ]
  node [
    id 1690
    label "woda"
  ]
  node [
    id 1691
    label "t&#322;um"
  ]
  node [
    id 1692
    label "kot"
  ]
  node [
    id 1693
    label "mn&#243;stwo"
  ]
  node [
    id 1694
    label "pasemko"
  ]
  node [
    id 1695
    label "karb"
  ]
  node [
    id 1696
    label "kszta&#322;t"
  ]
  node [
    id 1697
    label "czo&#322;o_fali"
  ]
  node [
    id 1698
    label "komutowanie"
  ]
  node [
    id 1699
    label "dw&#243;jnik"
  ]
  node [
    id 1700
    label "przerywacz"
  ]
  node [
    id 1701
    label "obsesja"
  ]
  node [
    id 1702
    label "nastr&#243;j"
  ]
  node [
    id 1703
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1704
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 1705
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 1706
    label "cykl_astronomiczny"
  ]
  node [
    id 1707
    label "coil"
  ]
  node [
    id 1708
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 1709
    label "stan_skupienia"
  ]
  node [
    id 1710
    label "komutowa&#263;"
  ]
  node [
    id 1711
    label "degree"
  ]
  node [
    id 1712
    label "obw&#243;d"
  ]
  node [
    id 1713
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 1714
    label "fotoelement"
  ]
  node [
    id 1715
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 1716
    label "kraw&#281;d&#378;"
  ]
  node [
    id 1717
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 1718
    label "ripple"
  ]
  node [
    id 1719
    label "zabicie"
  ]
  node [
    id 1720
    label "edycja"
  ]
  node [
    id 1721
    label "cycle"
  ]
  node [
    id 1722
    label "sekwencja"
  ]
  node [
    id 1723
    label "set"
  ]
  node [
    id 1724
    label "owulacja"
  ]
  node [
    id 1725
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 1726
    label "charakter"
  ]
  node [
    id 1727
    label "nauka_humanistyczna"
  ]
  node [
    id 1728
    label "elokucja"
  ]
  node [
    id 1729
    label "tropika"
  ]
  node [
    id 1730
    label "chironomia"
  ]
  node [
    id 1731
    label "elokwencja"
  ]
  node [
    id 1732
    label "erystyka"
  ]
  node [
    id 1733
    label "era_paleozoiczna"
  ]
  node [
    id 1734
    label "ludlow"
  ]
  node [
    id 1735
    label "moneta"
  ]
  node [
    id 1736
    label "paleoproterozoik"
  ]
  node [
    id 1737
    label "zlodowacenie"
  ]
  node [
    id 1738
    label "asteroksylon"
  ]
  node [
    id 1739
    label "pluwia&#322;"
  ]
  node [
    id 1740
    label "mezoproterozoik"
  ]
  node [
    id 1741
    label "era_kenozoiczna"
  ]
  node [
    id 1742
    label "era_mezozoiczna"
  ]
  node [
    id 1743
    label "ret"
  ]
  node [
    id 1744
    label "konodont"
  ]
  node [
    id 1745
    label "p&#243;&#378;ny_trias"
  ]
  node [
    id 1746
    label "kajper"
  ]
  node [
    id 1747
    label "neoproterozoik"
  ]
  node [
    id 1748
    label "pobia&#322;ka"
  ]
  node [
    id 1749
    label "pteranodon"
  ]
  node [
    id 1750
    label "apt"
  ]
  node [
    id 1751
    label "alb"
  ]
  node [
    id 1752
    label "chalk"
  ]
  node [
    id 1753
    label "cenoman"
  ]
  node [
    id 1754
    label "turon"
  ]
  node [
    id 1755
    label "pastel"
  ]
  node [
    id 1756
    label "santon"
  ]
  node [
    id 1757
    label "ska&#322;a_organogeniczna"
  ]
  node [
    id 1758
    label "neokom"
  ]
  node [
    id 1759
    label "solmizacja"
  ]
  node [
    id 1760
    label "glinka"
  ]
  node [
    id 1761
    label "formality"
  ]
  node [
    id 1762
    label "repetycja"
  ]
  node [
    id 1763
    label "tone"
  ]
  node [
    id 1764
    label "akcent"
  ]
  node [
    id 1765
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1766
    label "r&#243;&#380;nica"
  ]
  node [
    id 1767
    label "note"
  ]
  node [
    id 1768
    label "heksachord"
  ]
  node [
    id 1769
    label "ubarwienie"
  ]
  node [
    id 1770
    label "seria"
  ]
  node [
    id 1771
    label "zabarwienie"
  ]
  node [
    id 1772
    label "zwyczaj"
  ]
  node [
    id 1773
    label "rejestr"
  ]
  node [
    id 1774
    label "wieloton"
  ]
  node [
    id 1775
    label "kolorystyka"
  ]
  node [
    id 1776
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1777
    label "modalizm"
  ]
  node [
    id 1778
    label "tu&#324;czyk"
  ]
  node [
    id 1779
    label "interwa&#322;"
  ]
  node [
    id 1780
    label "sound"
  ]
  node [
    id 1781
    label "pistolet_maszynowy"
  ]
  node [
    id 1782
    label "jednostka_si&#322;y"
  ]
  node [
    id 1783
    label "pozauk&#322;adowa_jednostka_miary"
  ]
  node [
    id 1784
    label "wapie&#324;_pi&#324;czowski"
  ]
  node [
    id 1785
    label "tworzywo"
  ]
  node [
    id 1786
    label "pensylwan"
  ]
  node [
    id 1787
    label "mezozaur"
  ]
  node [
    id 1788
    label "pikaia"
  ]
  node [
    id 1789
    label "huron"
  ]
  node [
    id 1790
    label "rand"
  ]
  node [
    id 1791
    label "era_eozoiczna"
  ]
  node [
    id 1792
    label "era_archaiczna"
  ]
  node [
    id 1793
    label "cechsztyn"
  ]
  node [
    id 1794
    label "Permian"
  ]
  node [
    id 1795
    label "euoplocefal"
  ]
  node [
    id 1796
    label "dogger"
  ]
  node [
    id 1797
    label "plezjozaur"
  ]
  node [
    id 1798
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1799
    label "eon"
  ]
  node [
    id 1800
    label "performance"
  ]
  node [
    id 1801
    label "monta&#380;"
  ]
  node [
    id 1802
    label "postprodukcja"
  ]
  node [
    id 1803
    label "dzie&#322;o"
  ]
  node [
    id 1804
    label "fabrication"
  ]
  node [
    id 1805
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 1806
    label "proces_my&#347;lowy"
  ]
  node [
    id 1807
    label "liczenie"
  ]
  node [
    id 1808
    label "laparotomia"
  ]
  node [
    id 1809
    label "czyn"
  ]
  node [
    id 1810
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1811
    label "liczy&#263;"
  ]
  node [
    id 1812
    label "matematyka"
  ]
  node [
    id 1813
    label "funkcja"
  ]
  node [
    id 1814
    label "rzut"
  ]
  node [
    id 1815
    label "zabieg"
  ]
  node [
    id 1816
    label "mathematical_process"
  ]
  node [
    id 1817
    label "szew"
  ]
  node [
    id 1818
    label "torakotomia"
  ]
  node [
    id 1819
    label "supremum"
  ]
  node [
    id 1820
    label "chirurg"
  ]
  node [
    id 1821
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 1822
    label "infimum"
  ]
  node [
    id 1823
    label "komunikat"
  ]
  node [
    id 1824
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 1825
    label "dorobek"
  ]
  node [
    id 1826
    label "tre&#347;&#263;"
  ]
  node [
    id 1827
    label "works"
  ]
  node [
    id 1828
    label "obrazowanie"
  ]
  node [
    id 1829
    label "retrospektywa"
  ]
  node [
    id 1830
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 1831
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1832
    label "forma"
  ]
  node [
    id 1833
    label "creation"
  ]
  node [
    id 1834
    label "tetralogia"
  ]
  node [
    id 1835
    label "rozprawa"
  ]
  node [
    id 1836
    label "kognicja"
  ]
  node [
    id 1837
    label "przes&#322;anka"
  ]
  node [
    id 1838
    label "legislacyjnie"
  ]
  node [
    id 1839
    label "nast&#281;pstwo"
  ]
  node [
    id 1840
    label "audycja"
  ]
  node [
    id 1841
    label "spos&#243;b_dzia&#322;ania"
  ]
  node [
    id 1842
    label "film"
  ]
  node [
    id 1843
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 748
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 760
  ]
  edge [
    source 8
    target 761
  ]
  edge [
    source 8
    target 762
  ]
  edge [
    source 8
    target 763
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 769
  ]
  edge [
    source 8
    target 770
  ]
  edge [
    source 8
    target 771
  ]
  edge [
    source 8
    target 772
  ]
  edge [
    source 8
    target 773
  ]
  edge [
    source 8
    target 774
  ]
  edge [
    source 8
    target 775
  ]
  edge [
    source 8
    target 776
  ]
  edge [
    source 8
    target 777
  ]
  edge [
    source 8
    target 778
  ]
  edge [
    source 8
    target 779
  ]
  edge [
    source 8
    target 780
  ]
  edge [
    source 8
    target 781
  ]
  edge [
    source 8
    target 782
  ]
  edge [
    source 8
    target 783
  ]
  edge [
    source 8
    target 784
  ]
  edge [
    source 8
    target 785
  ]
  edge [
    source 8
    target 786
  ]
  edge [
    source 8
    target 787
  ]
  edge [
    source 8
    target 788
  ]
  edge [
    source 8
    target 789
  ]
  edge [
    source 8
    target 790
  ]
  edge [
    source 8
    target 791
  ]
  edge [
    source 8
    target 792
  ]
  edge [
    source 8
    target 793
  ]
  edge [
    source 8
    target 794
  ]
  edge [
    source 8
    target 795
  ]
  edge [
    source 8
    target 796
  ]
  edge [
    source 8
    target 797
  ]
  edge [
    source 8
    target 798
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 799
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 800
  ]
  edge [
    source 8
    target 801
  ]
  edge [
    source 8
    target 802
  ]
  edge [
    source 8
    target 803
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 804
  ]
  edge [
    source 8
    target 805
  ]
  edge [
    source 8
    target 806
  ]
  edge [
    source 8
    target 807
  ]
  edge [
    source 8
    target 808
  ]
  edge [
    source 8
    target 809
  ]
  edge [
    source 8
    target 810
  ]
  edge [
    source 8
    target 811
  ]
  edge [
    source 8
    target 812
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 813
  ]
  edge [
    source 8
    target 814
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 815
  ]
  edge [
    source 8
    target 816
  ]
  edge [
    source 8
    target 817
  ]
  edge [
    source 8
    target 818
  ]
  edge [
    source 8
    target 819
  ]
  edge [
    source 8
    target 820
  ]
  edge [
    source 8
    target 821
  ]
  edge [
    source 8
    target 822
  ]
  edge [
    source 8
    target 823
  ]
  edge [
    source 8
    target 824
  ]
  edge [
    source 8
    target 825
  ]
  edge [
    source 8
    target 826
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 827
  ]
  edge [
    source 8
    target 828
  ]
  edge [
    source 8
    target 829
  ]
  edge [
    source 8
    target 830
  ]
  edge [
    source 8
    target 831
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 832
  ]
  edge [
    source 8
    target 833
  ]
  edge [
    source 8
    target 834
  ]
  edge [
    source 8
    target 835
  ]
  edge [
    source 8
    target 836
  ]
  edge [
    source 8
    target 837
  ]
  edge [
    source 8
    target 838
  ]
  edge [
    source 8
    target 839
  ]
  edge [
    source 8
    target 840
  ]
  edge [
    source 8
    target 841
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 842
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 843
  ]
  edge [
    source 8
    target 844
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 845
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 846
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 847
  ]
  edge [
    source 8
    target 848
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 849
  ]
  edge [
    source 9
    target 850
  ]
  edge [
    source 9
    target 851
  ]
  edge [
    source 9
    target 852
  ]
  edge [
    source 9
    target 853
  ]
  edge [
    source 9
    target 854
  ]
  edge [
    source 9
    target 855
  ]
  edge [
    source 9
    target 856
  ]
  edge [
    source 9
    target 857
  ]
  edge [
    source 9
    target 858
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 859
  ]
  edge [
    source 9
    target 860
  ]
  edge [
    source 9
    target 861
  ]
  edge [
    source 9
    target 862
  ]
  edge [
    source 9
    target 863
  ]
  edge [
    source 9
    target 864
  ]
  edge [
    source 9
    target 865
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 866
  ]
  edge [
    source 9
    target 867
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 868
  ]
  edge [
    source 9
    target 869
  ]
  edge [
    source 9
    target 870
  ]
  edge [
    source 9
    target 871
  ]
  edge [
    source 9
    target 872
  ]
  edge [
    source 9
    target 873
  ]
  edge [
    source 9
    target 874
  ]
  edge [
    source 9
    target 817
  ]
  edge [
    source 9
    target 875
  ]
  edge [
    source 9
    target 876
  ]
  edge [
    source 9
    target 877
  ]
  edge [
    source 9
    target 878
  ]
  edge [
    source 9
    target 879
  ]
  edge [
    source 9
    target 880
  ]
  edge [
    source 9
    target 881
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 882
  ]
  edge [
    source 9
    target 883
  ]
  edge [
    source 9
    target 884
  ]
  edge [
    source 9
    target 885
  ]
  edge [
    source 9
    target 886
  ]
  edge [
    source 9
    target 887
  ]
  edge [
    source 9
    target 888
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 889
  ]
  edge [
    source 9
    target 890
  ]
  edge [
    source 9
    target 891
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 892
  ]
  edge [
    source 9
    target 893
  ]
  edge [
    source 9
    target 894
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 895
  ]
  edge [
    source 9
    target 896
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 897
  ]
  edge [
    source 9
    target 898
  ]
  edge [
    source 9
    target 899
  ]
  edge [
    source 9
    target 900
  ]
  edge [
    source 9
    target 901
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 902
  ]
  edge [
    source 9
    target 903
  ]
  edge [
    source 9
    target 904
  ]
  edge [
    source 9
    target 905
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 906
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 907
  ]
  edge [
    source 9
    target 908
  ]
  edge [
    source 9
    target 909
  ]
  edge [
    source 9
    target 910
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 911
  ]
  edge [
    source 9
    target 912
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 913
  ]
  edge [
    source 9
    target 914
  ]
  edge [
    source 9
    target 915
  ]
  edge [
    source 9
    target 916
  ]
  edge [
    source 9
    target 917
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 918
  ]
  edge [
    source 9
    target 919
  ]
  edge [
    source 9
    target 920
  ]
  edge [
    source 9
    target 921
  ]
  edge [
    source 9
    target 922
  ]
  edge [
    source 9
    target 923
  ]
  edge [
    source 9
    target 924
  ]
  edge [
    source 9
    target 925
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 926
  ]
  edge [
    source 9
    target 927
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 928
  ]
  edge [
    source 9
    target 929
  ]
  edge [
    source 9
    target 930
  ]
  edge [
    source 9
    target 931
  ]
  edge [
    source 9
    target 932
  ]
  edge [
    source 9
    target 933
  ]
  edge [
    source 9
    target 934
  ]
  edge [
    source 9
    target 935
  ]
  edge [
    source 9
    target 936
  ]
  edge [
    source 9
    target 937
  ]
  edge [
    source 9
    target 938
  ]
  edge [
    source 9
    target 939
  ]
  edge [
    source 9
    target 940
  ]
  edge [
    source 9
    target 941
  ]
  edge [
    source 9
    target 942
  ]
  edge [
    source 9
    target 943
  ]
  edge [
    source 9
    target 944
  ]
  edge [
    source 9
    target 945
  ]
  edge [
    source 9
    target 946
  ]
  edge [
    source 9
    target 947
  ]
  edge [
    source 9
    target 948
  ]
  edge [
    source 9
    target 949
  ]
  edge [
    source 9
    target 950
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 951
  ]
  edge [
    source 9
    target 952
  ]
  edge [
    source 9
    target 953
  ]
  edge [
    source 9
    target 954
  ]
  edge [
    source 9
    target 955
  ]
  edge [
    source 9
    target 956
  ]
  edge [
    source 9
    target 957
  ]
  edge [
    source 9
    target 958
  ]
  edge [
    source 9
    target 959
  ]
  edge [
    source 9
    target 960
  ]
  edge [
    source 9
    target 961
  ]
  edge [
    source 9
    target 962
  ]
  edge [
    source 9
    target 963
  ]
  edge [
    source 9
    target 964
  ]
  edge [
    source 9
    target 965
  ]
  edge [
    source 9
    target 966
  ]
  edge [
    source 9
    target 967
  ]
  edge [
    source 9
    target 968
  ]
  edge [
    source 9
    target 969
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 970
  ]
  edge [
    source 9
    target 971
  ]
  edge [
    source 9
    target 972
  ]
  edge [
    source 9
    target 973
  ]
  edge [
    source 9
    target 974
  ]
  edge [
    source 9
    target 975
  ]
  edge [
    source 9
    target 976
  ]
  edge [
    source 9
    target 977
  ]
  edge [
    source 9
    target 978
  ]
  edge [
    source 9
    target 979
  ]
  edge [
    source 9
    target 980
  ]
  edge [
    source 9
    target 981
  ]
  edge [
    source 9
    target 982
  ]
  edge [
    source 9
    target 983
  ]
  edge [
    source 9
    target 984
  ]
  edge [
    source 9
    target 985
  ]
  edge [
    source 9
    target 986
  ]
  edge [
    source 9
    target 987
  ]
  edge [
    source 9
    target 988
  ]
  edge [
    source 9
    target 989
  ]
  edge [
    source 9
    target 990
  ]
  edge [
    source 9
    target 991
  ]
  edge [
    source 9
    target 992
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 993
  ]
  edge [
    source 10
    target 994
  ]
  edge [
    source 10
    target 995
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 895
  ]
  edge [
    source 11
    target 996
  ]
  edge [
    source 11
    target 997
  ]
  edge [
    source 11
    target 998
  ]
  edge [
    source 11
    target 999
  ]
  edge [
    source 11
    target 1000
  ]
  edge [
    source 11
    target 1001
  ]
  edge [
    source 11
    target 1002
  ]
  edge [
    source 11
    target 1003
  ]
  edge [
    source 11
    target 1004
  ]
  edge [
    source 11
    target 1005
  ]
  edge [
    source 11
    target 1006
  ]
  edge [
    source 11
    target 1007
  ]
  edge [
    source 11
    target 1008
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 1009
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 1010
  ]
  edge [
    source 11
    target 1011
  ]
  edge [
    source 11
    target 1012
  ]
  edge [
    source 11
    target 1013
  ]
  edge [
    source 11
    target 1014
  ]
  edge [
    source 11
    target 1015
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 1016
  ]
  edge [
    source 11
    target 1017
  ]
  edge [
    source 11
    target 1018
  ]
  edge [
    source 11
    target 1019
  ]
  edge [
    source 11
    target 1020
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 1021
  ]
  edge [
    source 11
    target 1022
  ]
  edge [
    source 11
    target 1023
  ]
  edge [
    source 11
    target 1024
  ]
  edge [
    source 11
    target 1025
  ]
  edge [
    source 11
    target 1026
  ]
  edge [
    source 11
    target 1027
  ]
  edge [
    source 11
    target 1028
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1029
  ]
  edge [
    source 12
    target 1030
  ]
  edge [
    source 12
    target 1031
  ]
  edge [
    source 12
    target 407
  ]
  edge [
    source 12
    target 1032
  ]
  edge [
    source 12
    target 1033
  ]
  edge [
    source 12
    target 233
  ]
  edge [
    source 12
    target 1034
  ]
  edge [
    source 12
    target 1035
  ]
  edge [
    source 12
    target 1036
  ]
  edge [
    source 12
    target 1037
  ]
  edge [
    source 12
    target 1038
  ]
  edge [
    source 12
    target 1039
  ]
  edge [
    source 12
    target 1040
  ]
  edge [
    source 12
    target 1041
  ]
  edge [
    source 12
    target 1042
  ]
  edge [
    source 12
    target 1043
  ]
  edge [
    source 12
    target 1044
  ]
  edge [
    source 12
    target 1045
  ]
  edge [
    source 12
    target 1046
  ]
  edge [
    source 12
    target 1047
  ]
  edge [
    source 12
    target 1048
  ]
  edge [
    source 12
    target 1049
  ]
  edge [
    source 12
    target 1050
  ]
  edge [
    source 12
    target 1051
  ]
  edge [
    source 12
    target 1052
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 1053
  ]
  edge [
    source 12
    target 1054
  ]
  edge [
    source 12
    target 1055
  ]
  edge [
    source 12
    target 1056
  ]
  edge [
    source 12
    target 1057
  ]
  edge [
    source 12
    target 1058
  ]
  edge [
    source 12
    target 1059
  ]
  edge [
    source 12
    target 1060
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 1061
  ]
  edge [
    source 12
    target 1062
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 1063
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 1064
  ]
  edge [
    source 12
    target 1065
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 1066
  ]
  edge [
    source 12
    target 1067
  ]
  edge [
    source 12
    target 1068
  ]
  edge [
    source 12
    target 1069
  ]
  edge [
    source 12
    target 1070
  ]
  edge [
    source 12
    target 1071
  ]
  edge [
    source 12
    target 1072
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1073
  ]
  edge [
    source 16
    target 1074
  ]
  edge [
    source 16
    target 1075
  ]
  edge [
    source 16
    target 1076
  ]
  edge [
    source 16
    target 1077
  ]
  edge [
    source 16
    target 1078
  ]
  edge [
    source 16
    target 1079
  ]
  edge [
    source 16
    target 1080
  ]
  edge [
    source 16
    target 1081
  ]
  edge [
    source 16
    target 1082
  ]
  edge [
    source 16
    target 1083
  ]
  edge [
    source 16
    target 1084
  ]
  edge [
    source 16
    target 1085
  ]
  edge [
    source 16
    target 1086
  ]
  edge [
    source 16
    target 1087
  ]
  edge [
    source 16
    target 88
  ]
  edge [
    source 16
    target 1088
  ]
  edge [
    source 16
    target 1089
  ]
  edge [
    source 16
    target 1090
  ]
  edge [
    source 16
    target 1091
  ]
  edge [
    source 16
    target 1092
  ]
  edge [
    source 16
    target 1093
  ]
  edge [
    source 16
    target 1094
  ]
  edge [
    source 16
    target 1095
  ]
  edge [
    source 16
    target 1096
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 1097
  ]
  edge [
    source 16
    target 1098
  ]
  edge [
    source 16
    target 1099
  ]
  edge [
    source 16
    target 1100
  ]
  edge [
    source 16
    target 1101
  ]
  edge [
    source 16
    target 1102
  ]
  edge [
    source 16
    target 1103
  ]
  edge [
    source 16
    target 1104
  ]
  edge [
    source 16
    target 1105
  ]
  edge [
    source 16
    target 1106
  ]
  edge [
    source 16
    target 1107
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 1108
  ]
  edge [
    source 16
    target 1109
  ]
  edge [
    source 16
    target 1110
  ]
  edge [
    source 16
    target 1111
  ]
  edge [
    source 16
    target 1112
  ]
  edge [
    source 16
    target 1113
  ]
  edge [
    source 16
    target 1114
  ]
  edge [
    source 16
    target 1115
  ]
  edge [
    source 16
    target 1116
  ]
  edge [
    source 16
    target 1117
  ]
  edge [
    source 16
    target 1118
  ]
  edge [
    source 16
    target 1119
  ]
  edge [
    source 16
    target 1120
  ]
  edge [
    source 16
    target 1121
  ]
  edge [
    source 16
    target 1122
  ]
  edge [
    source 16
    target 1123
  ]
  edge [
    source 16
    target 1124
  ]
  edge [
    source 16
    target 1125
  ]
  edge [
    source 16
    target 1126
  ]
  edge [
    source 16
    target 1127
  ]
  edge [
    source 16
    target 1128
  ]
  edge [
    source 16
    target 1129
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 1130
  ]
  edge [
    source 16
    target 1131
  ]
  edge [
    source 16
    target 1132
  ]
  edge [
    source 16
    target 1133
  ]
  edge [
    source 16
    target 1134
  ]
  edge [
    source 16
    target 1135
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1136
  ]
  edge [
    source 17
    target 1137
  ]
  edge [
    source 17
    target 1138
  ]
  edge [
    source 17
    target 1139
  ]
  edge [
    source 17
    target 1140
  ]
  edge [
    source 17
    target 1141
  ]
  edge [
    source 17
    target 1142
  ]
  edge [
    source 17
    target 1143
  ]
  edge [
    source 17
    target 1144
  ]
  edge [
    source 17
    target 1145
  ]
  edge [
    source 17
    target 1146
  ]
  edge [
    source 17
    target 1147
  ]
  edge [
    source 17
    target 1148
  ]
  edge [
    source 17
    target 1149
  ]
  edge [
    source 17
    target 1150
  ]
  edge [
    source 17
    target 1151
  ]
  edge [
    source 17
    target 1152
  ]
  edge [
    source 17
    target 1153
  ]
  edge [
    source 17
    target 1154
  ]
  edge [
    source 17
    target 1155
  ]
  edge [
    source 17
    target 1156
  ]
  edge [
    source 17
    target 1157
  ]
  edge [
    source 17
    target 1158
  ]
  edge [
    source 17
    target 1159
  ]
  edge [
    source 17
    target 1160
  ]
  edge [
    source 17
    target 1161
  ]
  edge [
    source 17
    target 1162
  ]
  edge [
    source 17
    target 1163
  ]
  edge [
    source 17
    target 1164
  ]
  edge [
    source 17
    target 1165
  ]
  edge [
    source 17
    target 1166
  ]
  edge [
    source 17
    target 1167
  ]
  edge [
    source 17
    target 1168
  ]
  edge [
    source 17
    target 1169
  ]
  edge [
    source 17
    target 1170
  ]
  edge [
    source 17
    target 1171
  ]
  edge [
    source 17
    target 1172
  ]
  edge [
    source 17
    target 1173
  ]
  edge [
    source 17
    target 1174
  ]
  edge [
    source 17
    target 1175
  ]
  edge [
    source 17
    target 1176
  ]
  edge [
    source 17
    target 1177
  ]
  edge [
    source 17
    target 1178
  ]
  edge [
    source 17
    target 1179
  ]
  edge [
    source 17
    target 1180
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 1181
  ]
  edge [
    source 17
    target 1182
  ]
  edge [
    source 17
    target 1183
  ]
  edge [
    source 17
    target 1184
  ]
  edge [
    source 17
    target 1185
  ]
  edge [
    source 17
    target 1186
  ]
  edge [
    source 17
    target 1187
  ]
  edge [
    source 17
    target 1188
  ]
  edge [
    source 17
    target 1189
  ]
  edge [
    source 17
    target 1190
  ]
  edge [
    source 17
    target 1191
  ]
  edge [
    source 17
    target 1192
  ]
  edge [
    source 17
    target 1193
  ]
  edge [
    source 17
    target 1194
  ]
  edge [
    source 17
    target 1195
  ]
  edge [
    source 17
    target 1196
  ]
  edge [
    source 17
    target 1197
  ]
  edge [
    source 17
    target 1198
  ]
  edge [
    source 17
    target 1199
  ]
  edge [
    source 17
    target 1200
  ]
  edge [
    source 17
    target 1201
  ]
  edge [
    source 17
    target 1202
  ]
  edge [
    source 17
    target 1203
  ]
  edge [
    source 17
    target 1204
  ]
  edge [
    source 17
    target 1205
  ]
  edge [
    source 17
    target 1206
  ]
  edge [
    source 17
    target 1207
  ]
  edge [
    source 17
    target 1208
  ]
  edge [
    source 17
    target 1209
  ]
  edge [
    source 17
    target 1210
  ]
  edge [
    source 17
    target 1211
  ]
  edge [
    source 17
    target 1212
  ]
  edge [
    source 17
    target 1213
  ]
  edge [
    source 17
    target 1214
  ]
  edge [
    source 17
    target 1215
  ]
  edge [
    source 17
    target 1216
  ]
  edge [
    source 17
    target 1217
  ]
  edge [
    source 17
    target 1218
  ]
  edge [
    source 17
    target 1219
  ]
  edge [
    source 17
    target 1220
  ]
  edge [
    source 17
    target 1221
  ]
  edge [
    source 17
    target 1222
  ]
  edge [
    source 17
    target 1223
  ]
  edge [
    source 17
    target 1224
  ]
  edge [
    source 17
    target 1225
  ]
  edge [
    source 17
    target 1226
  ]
  edge [
    source 17
    target 1227
  ]
  edge [
    source 17
    target 1228
  ]
  edge [
    source 17
    target 1229
  ]
  edge [
    source 17
    target 1230
  ]
  edge [
    source 17
    target 1231
  ]
  edge [
    source 17
    target 1232
  ]
  edge [
    source 17
    target 1233
  ]
  edge [
    source 17
    target 1234
  ]
  edge [
    source 17
    target 1235
  ]
  edge [
    source 17
    target 1236
  ]
  edge [
    source 17
    target 1237
  ]
  edge [
    source 17
    target 1238
  ]
  edge [
    source 17
    target 1239
  ]
  edge [
    source 17
    target 1240
  ]
  edge [
    source 17
    target 1241
  ]
  edge [
    source 17
    target 1242
  ]
  edge [
    source 17
    target 1243
  ]
  edge [
    source 17
    target 1244
  ]
  edge [
    source 17
    target 1245
  ]
  edge [
    source 17
    target 1246
  ]
  edge [
    source 17
    target 1247
  ]
  edge [
    source 17
    target 1248
  ]
  edge [
    source 17
    target 1249
  ]
  edge [
    source 17
    target 1250
  ]
  edge [
    source 17
    target 1251
  ]
  edge [
    source 17
    target 1252
  ]
  edge [
    source 17
    target 1253
  ]
  edge [
    source 17
    target 1254
  ]
  edge [
    source 17
    target 1255
  ]
  edge [
    source 17
    target 1256
  ]
  edge [
    source 17
    target 1257
  ]
  edge [
    source 17
    target 1258
  ]
  edge [
    source 17
    target 1259
  ]
  edge [
    source 17
    target 1260
  ]
  edge [
    source 17
    target 1261
  ]
  edge [
    source 17
    target 1262
  ]
  edge [
    source 17
    target 1263
  ]
  edge [
    source 17
    target 1264
  ]
  edge [
    source 17
    target 1265
  ]
  edge [
    source 17
    target 1266
  ]
  edge [
    source 17
    target 1267
  ]
  edge [
    source 17
    target 1268
  ]
  edge [
    source 17
    target 1269
  ]
  edge [
    source 17
    target 1270
  ]
  edge [
    source 17
    target 1271
  ]
  edge [
    source 17
    target 1272
  ]
  edge [
    source 17
    target 1273
  ]
  edge [
    source 17
    target 1274
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 1275
  ]
  edge [
    source 17
    target 89
  ]
  edge [
    source 17
    target 1276
  ]
  edge [
    source 17
    target 387
  ]
  edge [
    source 17
    target 1277
  ]
  edge [
    source 17
    target 1278
  ]
  edge [
    source 17
    target 1279
  ]
  edge [
    source 17
    target 439
  ]
  edge [
    source 17
    target 1280
  ]
  edge [
    source 17
    target 1281
  ]
  edge [
    source 17
    target 1282
  ]
  edge [
    source 17
    target 1283
  ]
  edge [
    source 17
    target 1284
  ]
  edge [
    source 17
    target 1285
  ]
  edge [
    source 17
    target 1286
  ]
  edge [
    source 17
    target 1287
  ]
  edge [
    source 17
    target 1288
  ]
  edge [
    source 17
    target 1289
  ]
  edge [
    source 17
    target 98
  ]
  edge [
    source 17
    target 1290
  ]
  edge [
    source 17
    target 1291
  ]
  edge [
    source 17
    target 1292
  ]
  edge [
    source 17
    target 1293
  ]
  edge [
    source 17
    target 1294
  ]
  edge [
    source 17
    target 1295
  ]
  edge [
    source 17
    target 1296
  ]
  edge [
    source 17
    target 1297
  ]
  edge [
    source 17
    target 1298
  ]
  edge [
    source 17
    target 1299
  ]
  edge [
    source 17
    target 1300
  ]
  edge [
    source 17
    target 1301
  ]
  edge [
    source 17
    target 1302
  ]
  edge [
    source 17
    target 1303
  ]
  edge [
    source 17
    target 1304
  ]
  edge [
    source 17
    target 1305
  ]
  edge [
    source 17
    target 1306
  ]
  edge [
    source 17
    target 1307
  ]
  edge [
    source 17
    target 1308
  ]
  edge [
    source 17
    target 1309
  ]
  edge [
    source 17
    target 1310
  ]
  edge [
    source 17
    target 1311
  ]
  edge [
    source 17
    target 1312
  ]
  edge [
    source 17
    target 1313
  ]
  edge [
    source 17
    target 1314
  ]
  edge [
    source 17
    target 1315
  ]
  edge [
    source 17
    target 1316
  ]
  edge [
    source 17
    target 1317
  ]
  edge [
    source 17
    target 1318
  ]
  edge [
    source 17
    target 1319
  ]
  edge [
    source 17
    target 1320
  ]
  edge [
    source 17
    target 1321
  ]
  edge [
    source 17
    target 1322
  ]
  edge [
    source 17
    target 1323
  ]
  edge [
    source 17
    target 1324
  ]
  edge [
    source 17
    target 1325
  ]
  edge [
    source 17
    target 1326
  ]
  edge [
    source 17
    target 1327
  ]
  edge [
    source 17
    target 1328
  ]
  edge [
    source 17
    target 1329
  ]
  edge [
    source 17
    target 1330
  ]
  edge [
    source 17
    target 1331
  ]
  edge [
    source 17
    target 1332
  ]
  edge [
    source 17
    target 1333
  ]
  edge [
    source 17
    target 1334
  ]
  edge [
    source 17
    target 1335
  ]
  edge [
    source 17
    target 1336
  ]
  edge [
    source 17
    target 1337
  ]
  edge [
    source 17
    target 1338
  ]
  edge [
    source 17
    target 1339
  ]
  edge [
    source 17
    target 1340
  ]
  edge [
    source 17
    target 1341
  ]
  edge [
    source 17
    target 1342
  ]
  edge [
    source 17
    target 1343
  ]
  edge [
    source 17
    target 1344
  ]
  edge [
    source 17
    target 1345
  ]
  edge [
    source 17
    target 1346
  ]
  edge [
    source 17
    target 1347
  ]
  edge [
    source 17
    target 1348
  ]
  edge [
    source 17
    target 1349
  ]
  edge [
    source 17
    target 1350
  ]
  edge [
    source 17
    target 1351
  ]
  edge [
    source 17
    target 1352
  ]
  edge [
    source 17
    target 1353
  ]
  edge [
    source 17
    target 1354
  ]
  edge [
    source 17
    target 1355
  ]
  edge [
    source 17
    target 1356
  ]
  edge [
    source 17
    target 1357
  ]
  edge [
    source 17
    target 1358
  ]
  edge [
    source 17
    target 1359
  ]
  edge [
    source 17
    target 1360
  ]
  edge [
    source 17
    target 1361
  ]
  edge [
    source 17
    target 1362
  ]
  edge [
    source 17
    target 1363
  ]
  edge [
    source 17
    target 1364
  ]
  edge [
    source 17
    target 1365
  ]
  edge [
    source 17
    target 1366
  ]
  edge [
    source 17
    target 1367
  ]
  edge [
    source 17
    target 1368
  ]
  edge [
    source 17
    target 1369
  ]
  edge [
    source 17
    target 1370
  ]
  edge [
    source 17
    target 1371
  ]
  edge [
    source 17
    target 1372
  ]
  edge [
    source 17
    target 1373
  ]
  edge [
    source 17
    target 1374
  ]
  edge [
    source 17
    target 1375
  ]
  edge [
    source 17
    target 1376
  ]
  edge [
    source 17
    target 1377
  ]
  edge [
    source 17
    target 1378
  ]
  edge [
    source 17
    target 1379
  ]
  edge [
    source 17
    target 1380
  ]
  edge [
    source 17
    target 1381
  ]
  edge [
    source 17
    target 1382
  ]
  edge [
    source 17
    target 1383
  ]
  edge [
    source 17
    target 1384
  ]
  edge [
    source 17
    target 1385
  ]
  edge [
    source 17
    target 1386
  ]
  edge [
    source 17
    target 1387
  ]
  edge [
    source 17
    target 1388
  ]
  edge [
    source 17
    target 1389
  ]
  edge [
    source 17
    target 1390
  ]
  edge [
    source 17
    target 1391
  ]
  edge [
    source 17
    target 1392
  ]
  edge [
    source 17
    target 1393
  ]
  edge [
    source 17
    target 1394
  ]
  edge [
    source 17
    target 1395
  ]
  edge [
    source 17
    target 1396
  ]
  edge [
    source 17
    target 1397
  ]
  edge [
    source 17
    target 1398
  ]
  edge [
    source 17
    target 1399
  ]
  edge [
    source 17
    target 1400
  ]
  edge [
    source 17
    target 1401
  ]
  edge [
    source 17
    target 1402
  ]
  edge [
    source 17
    target 1403
  ]
  edge [
    source 17
    target 1404
  ]
  edge [
    source 17
    target 1405
  ]
  edge [
    source 17
    target 1406
  ]
  edge [
    source 17
    target 1407
  ]
  edge [
    source 17
    target 1408
  ]
  edge [
    source 17
    target 1409
  ]
  edge [
    source 17
    target 1410
  ]
  edge [
    source 17
    target 1411
  ]
  edge [
    source 17
    target 1412
  ]
  edge [
    source 17
    target 1413
  ]
  edge [
    source 17
    target 1414
  ]
  edge [
    source 17
    target 1415
  ]
  edge [
    source 17
    target 1416
  ]
  edge [
    source 17
    target 1417
  ]
  edge [
    source 17
    target 1418
  ]
  edge [
    source 17
    target 1419
  ]
  edge [
    source 17
    target 1420
  ]
  edge [
    source 17
    target 1421
  ]
  edge [
    source 17
    target 1422
  ]
  edge [
    source 17
    target 1423
  ]
  edge [
    source 17
    target 1424
  ]
  edge [
    source 17
    target 1425
  ]
  edge [
    source 17
    target 1426
  ]
  edge [
    source 17
    target 1427
  ]
  edge [
    source 17
    target 1428
  ]
  edge [
    source 17
    target 1429
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1430
  ]
  edge [
    source 20
    target 1431
  ]
  edge [
    source 20
    target 1432
  ]
  edge [
    source 20
    target 1433
  ]
  edge [
    source 20
    target 1434
  ]
  edge [
    source 20
    target 1435
  ]
  edge [
    source 20
    target 1436
  ]
  edge [
    source 20
    target 1437
  ]
  edge [
    source 20
    target 1438
  ]
  edge [
    source 20
    target 1439
  ]
  edge [
    source 20
    target 1440
  ]
  edge [
    source 20
    target 1441
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1442
  ]
  edge [
    source 21
    target 1443
  ]
  edge [
    source 21
    target 1444
  ]
  edge [
    source 21
    target 1445
  ]
  edge [
    source 21
    target 1446
  ]
  edge [
    source 21
    target 1447
  ]
  edge [
    source 21
    target 1448
  ]
  edge [
    source 21
    target 1449
  ]
  edge [
    source 21
    target 1450
  ]
  edge [
    source 21
    target 1451
  ]
  edge [
    source 21
    target 1452
  ]
  edge [
    source 21
    target 1453
  ]
  edge [
    source 21
    target 1454
  ]
  edge [
    source 21
    target 1455
  ]
  edge [
    source 21
    target 1456
  ]
  edge [
    source 21
    target 1457
  ]
  edge [
    source 21
    target 1458
  ]
  edge [
    source 21
    target 1459
  ]
  edge [
    source 21
    target 1436
  ]
  edge [
    source 21
    target 1460
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1461
  ]
  edge [
    source 23
    target 1462
  ]
  edge [
    source 23
    target 1463
  ]
  edge [
    source 23
    target 1464
  ]
  edge [
    source 23
    target 1465
  ]
  edge [
    source 23
    target 1466
  ]
  edge [
    source 23
    target 1467
  ]
  edge [
    source 23
    target 1468
  ]
  edge [
    source 23
    target 483
  ]
  edge [
    source 23
    target 480
  ]
  edge [
    source 23
    target 1469
  ]
  edge [
    source 23
    target 1470
  ]
  edge [
    source 23
    target 1471
  ]
  edge [
    source 23
    target 1433
  ]
  edge [
    source 23
    target 1457
  ]
  edge [
    source 23
    target 1472
  ]
  edge [
    source 23
    target 1459
  ]
  edge [
    source 23
    target 1473
  ]
  edge [
    source 23
    target 1474
  ]
  edge [
    source 23
    target 1475
  ]
  edge [
    source 23
    target 1476
  ]
  edge [
    source 23
    target 1477
  ]
  edge [
    source 23
    target 159
  ]
  edge [
    source 23
    target 1478
  ]
  edge [
    source 23
    target 482
  ]
  edge [
    source 23
    target 1479
  ]
  edge [
    source 23
    target 1480
  ]
  edge [
    source 23
    target 1481
  ]
  edge [
    source 23
    target 1482
  ]
  edge [
    source 23
    target 1443
  ]
  edge [
    source 23
    target 1483
  ]
  edge [
    source 23
    target 1484
  ]
  edge [
    source 23
    target 1485
  ]
  edge [
    source 23
    target 1486
  ]
  edge [
    source 23
    target 918
  ]
  edge [
    source 23
    target 1487
  ]
  edge [
    source 23
    target 1488
  ]
  edge [
    source 23
    target 1489
  ]
  edge [
    source 23
    target 1490
  ]
  edge [
    source 23
    target 1491
  ]
  edge [
    source 23
    target 1492
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1493
  ]
  edge [
    source 24
    target 1494
  ]
  edge [
    source 24
    target 1495
  ]
  edge [
    source 24
    target 1496
  ]
  edge [
    source 24
    target 1497
  ]
  edge [
    source 24
    target 1076
  ]
  edge [
    source 24
    target 1498
  ]
  edge [
    source 24
    target 1499
  ]
  edge [
    source 24
    target 1500
  ]
  edge [
    source 24
    target 1501
  ]
  edge [
    source 24
    target 1502
  ]
  edge [
    source 24
    target 1112
  ]
  edge [
    source 24
    target 1503
  ]
  edge [
    source 24
    target 901
  ]
  edge [
    source 24
    target 1504
  ]
  edge [
    source 24
    target 778
  ]
  edge [
    source 24
    target 757
  ]
  edge [
    source 24
    target 1505
  ]
  edge [
    source 24
    target 1506
  ]
  edge [
    source 24
    target 1507
  ]
  edge [
    source 24
    target 1508
  ]
  edge [
    source 24
    target 1509
  ]
  edge [
    source 24
    target 1510
  ]
  edge [
    source 24
    target 1511
  ]
  edge [
    source 24
    target 1512
  ]
  edge [
    source 24
    target 1513
  ]
  edge [
    source 24
    target 1514
  ]
  edge [
    source 24
    target 1098
  ]
  edge [
    source 24
    target 1099
  ]
  edge [
    source 24
    target 1100
  ]
  edge [
    source 24
    target 1101
  ]
  edge [
    source 24
    target 1102
  ]
  edge [
    source 24
    target 1103
  ]
  edge [
    source 24
    target 1104
  ]
  edge [
    source 24
    target 1105
  ]
  edge [
    source 24
    target 1106
  ]
  edge [
    source 24
    target 1515
  ]
  edge [
    source 24
    target 1516
  ]
  edge [
    source 24
    target 1517
  ]
  edge [
    source 24
    target 1518
  ]
  edge [
    source 24
    target 1519
  ]
  edge [
    source 24
    target 1520
  ]
  edge [
    source 24
    target 1521
  ]
  edge [
    source 24
    target 264
  ]
  edge [
    source 24
    target 1522
  ]
  edge [
    source 24
    target 1523
  ]
  edge [
    source 24
    target 1524
  ]
  edge [
    source 24
    target 1525
  ]
  edge [
    source 24
    target 1526
  ]
  edge [
    source 24
    target 1527
  ]
  edge [
    source 24
    target 1528
  ]
  edge [
    source 24
    target 1529
  ]
  edge [
    source 24
    target 1530
  ]
  edge [
    source 24
    target 1531
  ]
  edge [
    source 24
    target 1532
  ]
  edge [
    source 24
    target 1533
  ]
  edge [
    source 24
    target 1534
  ]
  edge [
    source 24
    target 1535
  ]
  edge [
    source 24
    target 1536
  ]
  edge [
    source 24
    target 1537
  ]
  edge [
    source 24
    target 1538
  ]
  edge [
    source 24
    target 1539
  ]
  edge [
    source 24
    target 1540
  ]
  edge [
    source 24
    target 1541
  ]
  edge [
    source 24
    target 1542
  ]
  edge [
    source 24
    target 1543
  ]
  edge [
    source 24
    target 1544
  ]
  edge [
    source 24
    target 1545
  ]
  edge [
    source 24
    target 1546
  ]
  edge [
    source 24
    target 1547
  ]
  edge [
    source 24
    target 1548
  ]
  edge [
    source 24
    target 1549
  ]
  edge [
    source 24
    target 1550
  ]
  edge [
    source 24
    target 1551
  ]
  edge [
    source 24
    target 1552
  ]
  edge [
    source 24
    target 1553
  ]
  edge [
    source 24
    target 1554
  ]
  edge [
    source 24
    target 1555
  ]
  edge [
    source 24
    target 1556
  ]
  edge [
    source 24
    target 1557
  ]
  edge [
    source 24
    target 1558
  ]
  edge [
    source 24
    target 1559
  ]
  edge [
    source 24
    target 1560
  ]
  edge [
    source 24
    target 1561
  ]
  edge [
    source 24
    target 1562
  ]
  edge [
    source 24
    target 1563
  ]
  edge [
    source 24
    target 1108
  ]
  edge [
    source 24
    target 1564
  ]
  edge [
    source 24
    target 1565
  ]
  edge [
    source 24
    target 1566
  ]
  edge [
    source 24
    target 1567
  ]
  edge [
    source 24
    target 1568
  ]
  edge [
    source 24
    target 1569
  ]
  edge [
    source 24
    target 1570
  ]
  edge [
    source 24
    target 1571
  ]
  edge [
    source 24
    target 1572
  ]
  edge [
    source 24
    target 1573
  ]
  edge [
    source 24
    target 1574
  ]
  edge [
    source 24
    target 1575
  ]
  edge [
    source 24
    target 1576
  ]
  edge [
    source 24
    target 1577
  ]
  edge [
    source 24
    target 1578
  ]
  edge [
    source 24
    target 1579
  ]
  edge [
    source 24
    target 1580
  ]
  edge [
    source 24
    target 1581
  ]
  edge [
    source 24
    target 1582
  ]
  edge [
    source 24
    target 1583
  ]
  edge [
    source 24
    target 232
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1584
  ]
  edge [
    source 25
    target 1585
  ]
  edge [
    source 25
    target 1586
  ]
  edge [
    source 25
    target 1587
  ]
  edge [
    source 25
    target 1588
  ]
  edge [
    source 25
    target 1589
  ]
  edge [
    source 25
    target 1590
  ]
  edge [
    source 25
    target 1591
  ]
  edge [
    source 25
    target 1592
  ]
  edge [
    source 25
    target 1593
  ]
  edge [
    source 25
    target 1594
  ]
  edge [
    source 25
    target 1595
  ]
  edge [
    source 25
    target 1596
  ]
  edge [
    source 25
    target 1597
  ]
  edge [
    source 25
    target 1598
  ]
  edge [
    source 25
    target 1599
  ]
  edge [
    source 25
    target 261
  ]
  edge [
    source 25
    target 1600
  ]
  edge [
    source 25
    target 1601
  ]
  edge [
    source 25
    target 1001
  ]
  edge [
    source 25
    target 1602
  ]
  edge [
    source 25
    target 1603
  ]
  edge [
    source 25
    target 390
  ]
  edge [
    source 25
    target 1604
  ]
  edge [
    source 25
    target 1004
  ]
  edge [
    source 25
    target 1007
  ]
  edge [
    source 25
    target 1605
  ]
  edge [
    source 25
    target 1606
  ]
  edge [
    source 25
    target 1607
  ]
  edge [
    source 25
    target 1608
  ]
  edge [
    source 25
    target 1609
  ]
  edge [
    source 25
    target 1610
  ]
  edge [
    source 25
    target 1611
  ]
  edge [
    source 25
    target 1612
  ]
  edge [
    source 25
    target 1613
  ]
  edge [
    source 25
    target 1614
  ]
  edge [
    source 25
    target 1615
  ]
  edge [
    source 25
    target 1616
  ]
  edge [
    source 25
    target 1617
  ]
  edge [
    source 25
    target 1618
  ]
  edge [
    source 25
    target 1619
  ]
  edge [
    source 25
    target 1620
  ]
  edge [
    source 25
    target 1621
  ]
  edge [
    source 25
    target 1622
  ]
  edge [
    source 25
    target 1623
  ]
  edge [
    source 25
    target 1015
  ]
  edge [
    source 25
    target 1624
  ]
  edge [
    source 25
    target 1625
  ]
  edge [
    source 25
    target 1626
  ]
  edge [
    source 25
    target 1017
  ]
  edge [
    source 25
    target 1627
  ]
  edge [
    source 25
    target 1628
  ]
  edge [
    source 25
    target 1629
  ]
  edge [
    source 25
    target 1630
  ]
  edge [
    source 25
    target 1631
  ]
  edge [
    source 25
    target 1632
  ]
  edge [
    source 25
    target 1633
  ]
  edge [
    source 25
    target 893
  ]
  edge [
    source 25
    target 1634
  ]
  edge [
    source 25
    target 1635
  ]
  edge [
    source 25
    target 1024
  ]
  edge [
    source 25
    target 1023
  ]
  edge [
    source 25
    target 1636
  ]
  edge [
    source 25
    target 1637
  ]
  edge [
    source 25
    target 1638
  ]
  edge [
    source 25
    target 895
  ]
  edge [
    source 25
    target 1639
  ]
  edge [
    source 25
    target 1640
  ]
  edge [
    source 25
    target 1641
  ]
  edge [
    source 25
    target 1642
  ]
  edge [
    source 25
    target 1643
  ]
  edge [
    source 25
    target 1644
  ]
  edge [
    source 25
    target 1645
  ]
  edge [
    source 25
    target 1096
  ]
  edge [
    source 25
    target 1646
  ]
  edge [
    source 25
    target 997
  ]
  edge [
    source 25
    target 998
  ]
  edge [
    source 25
    target 999
  ]
  edge [
    source 25
    target 1000
  ]
  edge [
    source 25
    target 1002
  ]
  edge [
    source 25
    target 1003
  ]
  edge [
    source 25
    target 1005
  ]
  edge [
    source 25
    target 1006
  ]
  edge [
    source 25
    target 1008
  ]
  edge [
    source 25
    target 416
  ]
  edge [
    source 25
    target 1009
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 1010
  ]
  edge [
    source 25
    target 1011
  ]
  edge [
    source 25
    target 1012
  ]
  edge [
    source 25
    target 1013
  ]
  edge [
    source 25
    target 1014
  ]
  edge [
    source 25
    target 112
  ]
  edge [
    source 25
    target 415
  ]
  edge [
    source 25
    target 1016
  ]
  edge [
    source 25
    target 1018
  ]
  edge [
    source 25
    target 1019
  ]
  edge [
    source 25
    target 1020
  ]
  edge [
    source 25
    target 667
  ]
  edge [
    source 25
    target 1021
  ]
  edge [
    source 25
    target 1022
  ]
  edge [
    source 25
    target 1025
  ]
  edge [
    source 25
    target 1026
  ]
  edge [
    source 25
    target 1027
  ]
  edge [
    source 25
    target 1028
  ]
  edge [
    source 25
    target 1647
  ]
  edge [
    source 25
    target 274
  ]
  edge [
    source 25
    target 1648
  ]
  edge [
    source 25
    target 1649
  ]
  edge [
    source 25
    target 547
  ]
  edge [
    source 25
    target 1650
  ]
  edge [
    source 25
    target 1651
  ]
  edge [
    source 25
    target 1652
  ]
  edge [
    source 25
    target 1653
  ]
  edge [
    source 25
    target 1654
  ]
  edge [
    source 25
    target 164
  ]
  edge [
    source 25
    target 1655
  ]
  edge [
    source 25
    target 1656
  ]
  edge [
    source 25
    target 1657
  ]
  edge [
    source 25
    target 1658
  ]
  edge [
    source 25
    target 739
  ]
  edge [
    source 25
    target 1659
  ]
  edge [
    source 25
    target 1660
  ]
  edge [
    source 25
    target 664
  ]
  edge [
    source 25
    target 1661
  ]
  edge [
    source 25
    target 1662
  ]
  edge [
    source 25
    target 1663
  ]
  edge [
    source 25
    target 1664
  ]
  edge [
    source 25
    target 1665
  ]
  edge [
    source 25
    target 1666
  ]
  edge [
    source 25
    target 1667
  ]
  edge [
    source 25
    target 1668
  ]
  edge [
    source 25
    target 1669
  ]
  edge [
    source 25
    target 1670
  ]
  edge [
    source 25
    target 1671
  ]
  edge [
    source 25
    target 1672
  ]
  edge [
    source 25
    target 1673
  ]
  edge [
    source 25
    target 1674
  ]
  edge [
    source 25
    target 1675
  ]
  edge [
    source 25
    target 1676
  ]
  edge [
    source 25
    target 817
  ]
  edge [
    source 25
    target 778
  ]
  edge [
    source 25
    target 1677
  ]
  edge [
    source 25
    target 1678
  ]
  edge [
    source 25
    target 1679
  ]
  edge [
    source 25
    target 1680
  ]
  edge [
    source 25
    target 1681
  ]
  edge [
    source 25
    target 1682
  ]
  edge [
    source 25
    target 1683
  ]
  edge [
    source 25
    target 910
  ]
  edge [
    source 25
    target 1684
  ]
  edge [
    source 25
    target 1685
  ]
  edge [
    source 25
    target 914
  ]
  edge [
    source 25
    target 1686
  ]
  edge [
    source 25
    target 1687
  ]
  edge [
    source 25
    target 486
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 1688
  ]
  edge [
    source 25
    target 1689
  ]
  edge [
    source 25
    target 1690
  ]
  edge [
    source 25
    target 1691
  ]
  edge [
    source 25
    target 1692
  ]
  edge [
    source 25
    target 673
  ]
  edge [
    source 25
    target 1693
  ]
  edge [
    source 25
    target 1694
  ]
  edge [
    source 25
    target 1695
  ]
  edge [
    source 25
    target 1696
  ]
  edge [
    source 25
    target 1697
  ]
  edge [
    source 25
    target 1698
  ]
  edge [
    source 25
    target 1699
  ]
  edge [
    source 25
    target 1700
  ]
  edge [
    source 25
    target 434
  ]
  edge [
    source 25
    target 1701
  ]
  edge [
    source 25
    target 1702
  ]
  edge [
    source 25
    target 1703
  ]
  edge [
    source 25
    target 1704
  ]
  edge [
    source 25
    target 1705
  ]
  edge [
    source 25
    target 1706
  ]
  edge [
    source 25
    target 699
  ]
  edge [
    source 25
    target 1707
  ]
  edge [
    source 25
    target 1708
  ]
  edge [
    source 25
    target 1709
  ]
  edge [
    source 25
    target 1710
  ]
  edge [
    source 25
    target 1711
  ]
  edge [
    source 25
    target 1712
  ]
  edge [
    source 25
    target 1713
  ]
  edge [
    source 25
    target 1714
  ]
  edge [
    source 25
    target 1715
  ]
  edge [
    source 25
    target 1716
  ]
  edge [
    source 25
    target 1717
  ]
  edge [
    source 25
    target 350
  ]
  edge [
    source 25
    target 413
  ]
  edge [
    source 25
    target 1718
  ]
  edge [
    source 25
    target 1719
  ]
  edge [
    source 25
    target 691
  ]
  edge [
    source 25
    target 968
  ]
  edge [
    source 25
    target 1720
  ]
  edge [
    source 25
    target 1721
  ]
  edge [
    source 25
    target 872
  ]
  edge [
    source 25
    target 1722
  ]
  edge [
    source 25
    target 1723
  ]
  edge [
    source 25
    target 1724
  ]
  edge [
    source 25
    target 98
  ]
  edge [
    source 25
    target 1725
  ]
  edge [
    source 25
    target 1726
  ]
  edge [
    source 25
    target 1727
  ]
  edge [
    source 25
    target 1728
  ]
  edge [
    source 25
    target 1729
  ]
  edge [
    source 25
    target 288
  ]
  edge [
    source 25
    target 1730
  ]
  edge [
    source 25
    target 1731
  ]
  edge [
    source 25
    target 1732
  ]
  edge [
    source 25
    target 99
  ]
  edge [
    source 25
    target 1733
  ]
  edge [
    source 25
    target 1734
  ]
  edge [
    source 25
    target 1735
  ]
  edge [
    source 25
    target 1736
  ]
  edge [
    source 25
    target 1737
  ]
  edge [
    source 25
    target 1738
  ]
  edge [
    source 25
    target 1739
  ]
  edge [
    source 25
    target 1740
  ]
  edge [
    source 25
    target 1741
  ]
  edge [
    source 25
    target 1742
  ]
  edge [
    source 25
    target 1743
  ]
  edge [
    source 25
    target 1744
  ]
  edge [
    source 25
    target 1745
  ]
  edge [
    source 25
    target 1746
  ]
  edge [
    source 25
    target 1747
  ]
  edge [
    source 25
    target 1748
  ]
  edge [
    source 25
    target 1749
  ]
  edge [
    source 25
    target 1750
  ]
  edge [
    source 25
    target 1751
  ]
  edge [
    source 25
    target 1752
  ]
  edge [
    source 25
    target 1753
  ]
  edge [
    source 25
    target 1754
  ]
  edge [
    source 25
    target 1755
  ]
  edge [
    source 25
    target 1756
  ]
  edge [
    source 25
    target 890
  ]
  edge [
    source 25
    target 1757
  ]
  edge [
    source 25
    target 1758
  ]
  edge [
    source 25
    target 1759
  ]
  edge [
    source 25
    target 201
  ]
  edge [
    source 25
    target 1760
  ]
  edge [
    source 25
    target 1761
  ]
  edge [
    source 25
    target 1762
  ]
  edge [
    source 25
    target 1763
  ]
  edge [
    source 25
    target 1764
  ]
  edge [
    source 25
    target 1765
  ]
  edge [
    source 25
    target 1766
  ]
  edge [
    source 25
    target 1767
  ]
  edge [
    source 25
    target 1768
  ]
  edge [
    source 25
    target 1769
  ]
  edge [
    source 25
    target 1770
  ]
  edge [
    source 25
    target 1771
  ]
  edge [
    source 25
    target 1772
  ]
  edge [
    source 25
    target 1773
  ]
  edge [
    source 25
    target 1774
  ]
  edge [
    source 25
    target 1775
  ]
  edge [
    source 25
    target 1776
  ]
  edge [
    source 25
    target 669
  ]
  edge [
    source 25
    target 1777
  ]
  edge [
    source 25
    target 1778
  ]
  edge [
    source 25
    target 1779
  ]
  edge [
    source 25
    target 1780
  ]
  edge [
    source 25
    target 1781
  ]
  edge [
    source 25
    target 1782
  ]
  edge [
    source 25
    target 1783
  ]
  edge [
    source 25
    target 1784
  ]
  edge [
    source 25
    target 1785
  ]
  edge [
    source 25
    target 1786
  ]
  edge [
    source 25
    target 1787
  ]
  edge [
    source 25
    target 1788
  ]
  edge [
    source 25
    target 1789
  ]
  edge [
    source 25
    target 1790
  ]
  edge [
    source 25
    target 1791
  ]
  edge [
    source 25
    target 1792
  ]
  edge [
    source 25
    target 1793
  ]
  edge [
    source 25
    target 608
  ]
  edge [
    source 25
    target 1794
  ]
  edge [
    source 25
    target 1795
  ]
  edge [
    source 25
    target 1796
  ]
  edge [
    source 25
    target 1797
  ]
  edge [
    source 25
    target 1798
  ]
  edge [
    source 25
    target 1799
  ]
  edge [
    source 26
    target 1800
  ]
  edge [
    source 26
    target 1060
  ]
  edge [
    source 26
    target 933
  ]
  edge [
    source 26
    target 1801
  ]
  edge [
    source 26
    target 916
  ]
  edge [
    source 26
    target 1802
  ]
  edge [
    source 26
    target 1803
  ]
  edge [
    source 26
    target 678
  ]
  edge [
    source 26
    target 687
  ]
  edge [
    source 26
    target 1804
  ]
  edge [
    source 26
    target 715
  ]
  edge [
    source 26
    target 716
  ]
  edge [
    source 26
    target 717
  ]
  edge [
    source 26
    target 718
  ]
  edge [
    source 26
    target 719
  ]
  edge [
    source 26
    target 393
  ]
  edge [
    source 26
    target 636
  ]
  edge [
    source 26
    target 720
  ]
  edge [
    source 26
    target 721
  ]
  edge [
    source 26
    target 722
  ]
  edge [
    source 26
    target 723
  ]
  edge [
    source 26
    target 724
  ]
  edge [
    source 26
    target 725
  ]
  edge [
    source 26
    target 726
  ]
  edge [
    source 26
    target 554
  ]
  edge [
    source 26
    target 727
  ]
  edge [
    source 26
    target 728
  ]
  edge [
    source 26
    target 729
  ]
  edge [
    source 26
    target 730
  ]
  edge [
    source 26
    target 731
  ]
  edge [
    source 26
    target 531
  ]
  edge [
    source 26
    target 732
  ]
  edge [
    source 26
    target 254
  ]
  edge [
    source 26
    target 733
  ]
  edge [
    source 26
    target 1805
  ]
  edge [
    source 26
    target 1806
  ]
  edge [
    source 26
    target 1807
  ]
  edge [
    source 26
    target 1808
  ]
  edge [
    source 26
    target 1809
  ]
  edge [
    source 26
    target 1810
  ]
  edge [
    source 26
    target 1811
  ]
  edge [
    source 26
    target 1812
  ]
  edge [
    source 26
    target 1813
  ]
  edge [
    source 26
    target 1814
  ]
  edge [
    source 26
    target 1815
  ]
  edge [
    source 26
    target 1816
  ]
  edge [
    source 26
    target 391
  ]
  edge [
    source 26
    target 1817
  ]
  edge [
    source 26
    target 1818
  ]
  edge [
    source 26
    target 669
  ]
  edge [
    source 26
    target 1819
  ]
  edge [
    source 26
    target 1820
  ]
  edge [
    source 26
    target 1821
  ]
  edge [
    source 26
    target 1822
  ]
  edge [
    source 26
    target 1823
  ]
  edge [
    source 26
    target 457
  ]
  edge [
    source 26
    target 1824
  ]
  edge [
    source 26
    target 1825
  ]
  edge [
    source 26
    target 683
  ]
  edge [
    source 26
    target 1826
  ]
  edge [
    source 26
    target 1827
  ]
  edge [
    source 26
    target 1828
  ]
  edge [
    source 26
    target 1829
  ]
  edge [
    source 26
    target 1830
  ]
  edge [
    source 26
    target 1831
  ]
  edge [
    source 26
    target 1832
  ]
  edge [
    source 26
    target 1833
  ]
  edge [
    source 26
    target 1834
  ]
  edge [
    source 26
    target 1703
  ]
  edge [
    source 26
    target 872
  ]
  edge [
    source 26
    target 1835
  ]
  edge [
    source 26
    target 1836
  ]
  edge [
    source 26
    target 190
  ]
  edge [
    source 26
    target 914
  ]
  edge [
    source 26
    target 1837
  ]
  edge [
    source 26
    target 1838
  ]
  edge [
    source 26
    target 1839
  ]
  edge [
    source 26
    target 1840
  ]
  edge [
    source 26
    target 547
  ]
  edge [
    source 26
    target 363
  ]
  edge [
    source 26
    target 1841
  ]
  edge [
    source 26
    target 429
  ]
  edge [
    source 26
    target 58
  ]
  edge [
    source 26
    target 1842
  ]
  edge [
    source 26
    target 1639
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 1843
  ]
]
