graph [
  node [
    id 0
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 1
    label "jan"
    origin "text"
  ]
  node [
    id 2
    label "filip"
    origin "text"
  ]
  node [
    id 3
    label "libicki"
    origin "text"
  ]
  node [
    id 4
    label "ablegat"
  ]
  node [
    id 5
    label "izba_ni&#380;sza"
  ]
  node [
    id 6
    label "Korwin"
  ]
  node [
    id 7
    label "dyscyplina_partyjna"
  ]
  node [
    id 8
    label "Miko&#322;ajczyk"
  ]
  node [
    id 9
    label "kurier_dyplomatyczny"
  ]
  node [
    id 10
    label "wys&#322;annik"
  ]
  node [
    id 11
    label "poselstwo"
  ]
  node [
    id 12
    label "parlamentarzysta"
  ]
  node [
    id 13
    label "przedstawiciel"
  ]
  node [
    id 14
    label "dyplomata"
  ]
  node [
    id 15
    label "klubista"
  ]
  node [
    id 16
    label "reprezentacja"
  ]
  node [
    id 17
    label "klub"
  ]
  node [
    id 18
    label "cz&#322;onek"
  ]
  node [
    id 19
    label "mandatariusz"
  ]
  node [
    id 20
    label "grupa_bilateralna"
  ]
  node [
    id 21
    label "polityk"
  ]
  node [
    id 22
    label "parlament"
  ]
  node [
    id 23
    label "mi&#322;y"
  ]
  node [
    id 24
    label "cz&#322;owiek"
  ]
  node [
    id 25
    label "korpus_dyplomatyczny"
  ]
  node [
    id 26
    label "dyplomatyczny"
  ]
  node [
    id 27
    label "takt"
  ]
  node [
    id 28
    label "Metternich"
  ]
  node [
    id 29
    label "dostojnik"
  ]
  node [
    id 30
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 31
    label "przyk&#322;ad"
  ]
  node [
    id 32
    label "substytuowa&#263;"
  ]
  node [
    id 33
    label "substytuowanie"
  ]
  node [
    id 34
    label "zast&#281;pca"
  ]
  node [
    id 35
    label "Jan"
  ]
  node [
    id 36
    label "Libicki"
  ]
  node [
    id 37
    label "Benedykt"
  ]
  node [
    id 38
    label "XVI"
  ]
  node [
    id 39
    label "benedykta"
  ]
  node [
    id 40
    label "Vittorio"
  ]
  node [
    id 41
    label "Messorim"
  ]
  node [
    id 42
    label "kongregacja"
  ]
  node [
    id 43
    label "nauka"
  ]
  node [
    id 44
    label "wiara"
  ]
  node [
    id 45
    label "raport"
  ]
  node [
    id 46
    label "ojciec"
  ]
  node [
    id 47
    label "sta&#324;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 46
    target 47
  ]
]
