graph [
  node [
    id 0
    label "lotnictwo"
    origin "text"
  ]
  node [
    id 1
    label "bombowy"
    origin "text"
  ]
  node [
    id 2
    label "konwojer"
  ]
  node [
    id 3
    label "awiacja"
  ]
  node [
    id 4
    label "transport"
  ]
  node [
    id 5
    label "balenit"
  ]
  node [
    id 6
    label "skr&#281;tomierz"
  ]
  node [
    id 7
    label "skrzyd&#322;o"
  ]
  node [
    id 8
    label "wojsko"
  ]
  node [
    id 9
    label "Si&#322;y_Powietrzne"
  ]
  node [
    id 10
    label "nawigacja"
  ]
  node [
    id 11
    label "nauka"
  ]
  node [
    id 12
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 13
    label "pozycja"
  ]
  node [
    id 14
    label "rezerwa"
  ]
  node [
    id 15
    label "werbowanie_si&#281;"
  ]
  node [
    id 16
    label "Eurokorpus"
  ]
  node [
    id 17
    label "Armia_Czerwona"
  ]
  node [
    id 18
    label "ods&#322;ugiwanie"
  ]
  node [
    id 19
    label "dryl"
  ]
  node [
    id 20
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 21
    label "fala"
  ]
  node [
    id 22
    label "potencja"
  ]
  node [
    id 23
    label "soldateska"
  ]
  node [
    id 24
    label "przedmiot"
  ]
  node [
    id 25
    label "pospolite_ruszenie"
  ]
  node [
    id 26
    label "mobilizowa&#263;"
  ]
  node [
    id 27
    label "mobilizowanie"
  ]
  node [
    id 28
    label "tabor"
  ]
  node [
    id 29
    label "zrejterowanie"
  ]
  node [
    id 30
    label "dezerter"
  ]
  node [
    id 31
    label "s&#322;u&#380;ba"
  ]
  node [
    id 32
    label "korpus"
  ]
  node [
    id 33
    label "zdemobilizowanie"
  ]
  node [
    id 34
    label "petarda"
  ]
  node [
    id 35
    label "zmobilizowanie"
  ]
  node [
    id 36
    label "szko&#322;a"
  ]
  node [
    id 37
    label "oddzia&#322;"
  ]
  node [
    id 38
    label "wojo"
  ]
  node [
    id 39
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 40
    label "oddzia&#322;_karny"
  ]
  node [
    id 41
    label "or&#281;&#380;"
  ]
  node [
    id 42
    label "si&#322;a"
  ]
  node [
    id 43
    label "obrona"
  ]
  node [
    id 44
    label "Armia_Krajowa"
  ]
  node [
    id 45
    label "wermacht"
  ]
  node [
    id 46
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 47
    label "Czerwona_Gwardia"
  ]
  node [
    id 48
    label "sztabslekarz"
  ]
  node [
    id 49
    label "zmobilizowa&#263;"
  ]
  node [
    id 50
    label "struktura"
  ]
  node [
    id 51
    label "Legia_Cudzoziemska"
  ]
  node [
    id 52
    label "cofni&#281;cie"
  ]
  node [
    id 53
    label "zrejterowa&#263;"
  ]
  node [
    id 54
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 55
    label "zdemobilizowa&#263;"
  ]
  node [
    id 56
    label "rejterowa&#263;"
  ]
  node [
    id 57
    label "rejterowanie"
  ]
  node [
    id 58
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 59
    label "typologia"
  ]
  node [
    id 60
    label "nomotetyczny"
  ]
  node [
    id 61
    label "wiedza"
  ]
  node [
    id 62
    label "dziedzina"
  ]
  node [
    id 63
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 64
    label "&#322;awa_szkolna"
  ]
  node [
    id 65
    label "nauki_o_poznaniu"
  ]
  node [
    id 66
    label "kultura_duchowa"
  ]
  node [
    id 67
    label "teoria_naukowa"
  ]
  node [
    id 68
    label "nauki_penalne"
  ]
  node [
    id 69
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 70
    label "nauki_o_Ziemi"
  ]
  node [
    id 71
    label "imagineskopia"
  ]
  node [
    id 72
    label "metodologia"
  ]
  node [
    id 73
    label "fotowoltaika"
  ]
  node [
    id 74
    label "proces"
  ]
  node [
    id 75
    label "inwentyka"
  ]
  node [
    id 76
    label "systematyka"
  ]
  node [
    id 77
    label "porada"
  ]
  node [
    id 78
    label "miasteczko_rowerowe"
  ]
  node [
    id 79
    label "przem&#243;wienie"
  ]
  node [
    id 80
    label "traffic"
  ]
  node [
    id 81
    label "zawarto&#347;&#263;"
  ]
  node [
    id 82
    label "komunikacja"
  ]
  node [
    id 83
    label "jednoszynowy"
  ]
  node [
    id 84
    label "tyfon"
  ]
  node [
    id 85
    label "gospodarka"
  ]
  node [
    id 86
    label "prze&#322;adunek"
  ]
  node [
    id 87
    label "cedu&#322;a"
  ]
  node [
    id 88
    label "unos"
  ]
  node [
    id 89
    label "towar"
  ]
  node [
    id 90
    label "us&#322;uga"
  ]
  node [
    id 91
    label "za&#322;adunek"
  ]
  node [
    id 92
    label "sprz&#281;t"
  ]
  node [
    id 93
    label "roz&#322;adunek"
  ]
  node [
    id 94
    label "grupa"
  ]
  node [
    id 95
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 96
    label "zbroja"
  ]
  node [
    id 97
    label "budynek"
  ]
  node [
    id 98
    label "p&#243;&#322;tusza"
  ]
  node [
    id 99
    label "&#380;y&#322;kowanie"
  ]
  node [
    id 100
    label "element"
  ]
  node [
    id 101
    label "skrzele"
  ]
  node [
    id 102
    label "brama"
  ]
  node [
    id 103
    label "samolot"
  ]
  node [
    id 104
    label "boisko"
  ]
  node [
    id 105
    label "tuszka"
  ]
  node [
    id 106
    label "strz&#281;pina"
  ]
  node [
    id 107
    label "wing"
  ]
  node [
    id 108
    label "dr&#243;bka"
  ]
  node [
    id 109
    label "szyk"
  ]
  node [
    id 110
    label "dr&#243;b"
  ]
  node [
    id 111
    label "drzwi"
  ]
  node [
    id 112
    label "keson"
  ]
  node [
    id 113
    label "klapa"
  ]
  node [
    id 114
    label "sterolotka"
  ]
  node [
    id 115
    label "szybowiec"
  ]
  node [
    id 116
    label "mi&#281;so"
  ]
  node [
    id 117
    label "narz&#261;d_ruchu"
  ]
  node [
    id 118
    label "lotka"
  ]
  node [
    id 119
    label "wirolot"
  ]
  node [
    id 120
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 121
    label "okno"
  ]
  node [
    id 122
    label "ugrupowanie"
  ]
  node [
    id 123
    label "organizacja"
  ]
  node [
    id 124
    label "husarz"
  ]
  node [
    id 125
    label "husaria"
  ]
  node [
    id 126
    label "o&#322;tarz"
  ]
  node [
    id 127
    label "wo&#322;owina"
  ]
  node [
    id 128
    label "winglet"
  ]
  node [
    id 129
    label "si&#322;y_powietrzne"
  ]
  node [
    id 130
    label "skrzyd&#322;owiec"
  ]
  node [
    id 131
    label "dywizjon_lotniczy"
  ]
  node [
    id 132
    label "nautyka"
  ]
  node [
    id 133
    label "czynno&#347;&#263;"
  ]
  node [
    id 134
    label "fiszbin"
  ]
  node [
    id 135
    label "sklejka"
  ]
  node [
    id 136
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 137
    label "miernik"
  ]
  node [
    id 138
    label "przyrz&#261;d_nawigacyjny"
  ]
  node [
    id 139
    label "prz&#281;dza"
  ]
  node [
    id 140
    label "manewr"
  ]
  node [
    id 141
    label "tortury"
  ]
  node [
    id 142
    label "kapitalny"
  ]
  node [
    id 143
    label "bombowo"
  ]
  node [
    id 144
    label "&#347;wietny"
  ]
  node [
    id 145
    label "fundamentalny"
  ]
  node [
    id 146
    label "kluczowy"
  ]
  node [
    id 147
    label "dobry"
  ]
  node [
    id 148
    label "superancki"
  ]
  node [
    id 149
    label "kapitalnie"
  ]
  node [
    id 150
    label "wspania&#322;y"
  ]
  node [
    id 151
    label "enormously"
  ]
  node [
    id 152
    label "wonderfully"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
]
