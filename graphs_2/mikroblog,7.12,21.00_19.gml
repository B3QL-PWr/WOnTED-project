graph [
  node [
    id 0
    label "byleby&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 2
    label "zje&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "siebie"
    origin "text"
  ]
  node [
    id 4
    label "sp&#243;&#378;niony"
    origin "text"
  ]
  node [
    id 5
    label "&#347;niadanie"
    origin "text"
  ]
  node [
    id 6
    label "mcdonald's"
    origin "text"
  ]
  node [
    id 7
    label "wycieczka"
    origin "text"
  ]
  node [
    id 8
    label "doba"
  ]
  node [
    id 9
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 10
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 11
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 12
    label "dzi&#347;"
  ]
  node [
    id 13
    label "teraz"
  ]
  node [
    id 14
    label "czas"
  ]
  node [
    id 15
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 16
    label "jednocze&#347;nie"
  ]
  node [
    id 17
    label "tydzie&#324;"
  ]
  node [
    id 18
    label "noc"
  ]
  node [
    id 19
    label "dzie&#324;"
  ]
  node [
    id 20
    label "godzina"
  ]
  node [
    id 21
    label "long_time"
  ]
  node [
    id 22
    label "jednostka_geologiczna"
  ]
  node [
    id 23
    label "ze&#380;re&#263;"
  ]
  node [
    id 24
    label "absorb"
  ]
  node [
    id 25
    label "spo&#380;y&#263;"
  ]
  node [
    id 26
    label "zrobi&#263;"
  ]
  node [
    id 27
    label "post&#261;pi&#263;"
  ]
  node [
    id 28
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 29
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 30
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 31
    label "zorganizowa&#263;"
  ]
  node [
    id 32
    label "appoint"
  ]
  node [
    id 33
    label "wystylizowa&#263;"
  ]
  node [
    id 34
    label "cause"
  ]
  node [
    id 35
    label "przerobi&#263;"
  ]
  node [
    id 36
    label "nabra&#263;"
  ]
  node [
    id 37
    label "make"
  ]
  node [
    id 38
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 39
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 40
    label "wydali&#263;"
  ]
  node [
    id 41
    label "skonsumowa&#263;"
  ]
  node [
    id 42
    label "chow"
  ]
  node [
    id 43
    label "jeer"
  ]
  node [
    id 44
    label "zu&#380;y&#263;"
  ]
  node [
    id 45
    label "digest"
  ]
  node [
    id 46
    label "dokuczy&#263;"
  ]
  node [
    id 47
    label "zniszczy&#263;"
  ]
  node [
    id 48
    label "p&#243;&#378;ny"
  ]
  node [
    id 49
    label "do_p&#243;&#378;na"
  ]
  node [
    id 50
    label "p&#243;&#378;no"
  ]
  node [
    id 51
    label "posi&#322;ek"
  ]
  node [
    id 52
    label "danie"
  ]
  node [
    id 53
    label "jedzenie"
  ]
  node [
    id 54
    label "odpoczynek"
  ]
  node [
    id 55
    label "grupa"
  ]
  node [
    id 56
    label "wyjazd"
  ]
  node [
    id 57
    label "chadzka"
  ]
  node [
    id 58
    label "podr&#243;&#380;"
  ]
  node [
    id 59
    label "digression"
  ]
  node [
    id 60
    label "rozrywka"
  ]
  node [
    id 61
    label "stan"
  ]
  node [
    id 62
    label "wyraj"
  ]
  node [
    id 63
    label "wczas"
  ]
  node [
    id 64
    label "diversion"
  ]
  node [
    id 65
    label "odm&#322;adzanie"
  ]
  node [
    id 66
    label "liga"
  ]
  node [
    id 67
    label "jednostka_systematyczna"
  ]
  node [
    id 68
    label "asymilowanie"
  ]
  node [
    id 69
    label "gromada"
  ]
  node [
    id 70
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 71
    label "asymilowa&#263;"
  ]
  node [
    id 72
    label "egzemplarz"
  ]
  node [
    id 73
    label "Entuzjastki"
  ]
  node [
    id 74
    label "zbi&#243;r"
  ]
  node [
    id 75
    label "kompozycja"
  ]
  node [
    id 76
    label "Terranie"
  ]
  node [
    id 77
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 78
    label "category"
  ]
  node [
    id 79
    label "pakiet_klimatyczny"
  ]
  node [
    id 80
    label "oddzia&#322;"
  ]
  node [
    id 81
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 82
    label "cz&#261;steczka"
  ]
  node [
    id 83
    label "stage_set"
  ]
  node [
    id 84
    label "type"
  ]
  node [
    id 85
    label "specgrupa"
  ]
  node [
    id 86
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 87
    label "&#346;wietliki"
  ]
  node [
    id 88
    label "odm&#322;odzenie"
  ]
  node [
    id 89
    label "Eurogrupa"
  ]
  node [
    id 90
    label "odm&#322;adza&#263;"
  ]
  node [
    id 91
    label "formacja_geologiczna"
  ]
  node [
    id 92
    label "harcerze_starsi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
]
