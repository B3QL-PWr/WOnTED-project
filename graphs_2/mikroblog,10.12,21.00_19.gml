graph [
  node [
    id 0
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wykop"
    origin "text"
  ]
  node [
    id 2
    label "postrzega&#263;"
  ]
  node [
    id 3
    label "perceive"
  ]
  node [
    id 4
    label "aprobowa&#263;"
  ]
  node [
    id 5
    label "wzrok"
  ]
  node [
    id 6
    label "zmale&#263;"
  ]
  node [
    id 7
    label "punkt_widzenia"
  ]
  node [
    id 8
    label "male&#263;"
  ]
  node [
    id 9
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 10
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 11
    label "spotka&#263;"
  ]
  node [
    id 12
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 13
    label "ogl&#261;da&#263;"
  ]
  node [
    id 14
    label "dostrzega&#263;"
  ]
  node [
    id 15
    label "spowodowa&#263;"
  ]
  node [
    id 16
    label "notice"
  ]
  node [
    id 17
    label "go_steady"
  ]
  node [
    id 18
    label "reagowa&#263;"
  ]
  node [
    id 19
    label "os&#261;dza&#263;"
  ]
  node [
    id 20
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 21
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 22
    label "react"
  ]
  node [
    id 23
    label "answer"
  ]
  node [
    id 24
    label "odpowiada&#263;"
  ]
  node [
    id 25
    label "uczestniczy&#263;"
  ]
  node [
    id 26
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 27
    label "obacza&#263;"
  ]
  node [
    id 28
    label "dochodzi&#263;"
  ]
  node [
    id 29
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 30
    label "doj&#347;&#263;"
  ]
  node [
    id 31
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 32
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 33
    label "styka&#263;_si&#281;"
  ]
  node [
    id 34
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 35
    label "insert"
  ]
  node [
    id 36
    label "visualize"
  ]
  node [
    id 37
    label "pozna&#263;"
  ]
  node [
    id 38
    label "befall"
  ]
  node [
    id 39
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 40
    label "znale&#378;&#263;"
  ]
  node [
    id 41
    label "orientowa&#263;_si&#281;"
  ]
  node [
    id 42
    label "approbate"
  ]
  node [
    id 43
    label "uznawa&#263;"
  ]
  node [
    id 44
    label "act"
  ]
  node [
    id 45
    label "strike"
  ]
  node [
    id 46
    label "robi&#263;"
  ]
  node [
    id 47
    label "s&#261;dzi&#263;"
  ]
  node [
    id 48
    label "powodowa&#263;"
  ]
  node [
    id 49
    label "znajdowa&#263;"
  ]
  node [
    id 50
    label "hold"
  ]
  node [
    id 51
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 52
    label "nagradza&#263;"
  ]
  node [
    id 53
    label "forytowa&#263;"
  ]
  node [
    id 54
    label "traktowa&#263;"
  ]
  node [
    id 55
    label "sign"
  ]
  node [
    id 56
    label "m&#281;tnienie"
  ]
  node [
    id 57
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 58
    label "widzenie"
  ]
  node [
    id 59
    label "okulista"
  ]
  node [
    id 60
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 61
    label "zmys&#322;"
  ]
  node [
    id 62
    label "expression"
  ]
  node [
    id 63
    label "oko"
  ]
  node [
    id 64
    label "m&#281;tnie&#263;"
  ]
  node [
    id 65
    label "kontakt"
  ]
  node [
    id 66
    label "sta&#263;_si&#281;"
  ]
  node [
    id 67
    label "reduce"
  ]
  node [
    id 68
    label "zmniejszy&#263;_si&#281;"
  ]
  node [
    id 69
    label "worsen"
  ]
  node [
    id 70
    label "slack"
  ]
  node [
    id 71
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 72
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 73
    label "relax"
  ]
  node [
    id 74
    label "zmniejsza&#263;_si&#281;"
  ]
  node [
    id 75
    label "budowa"
  ]
  node [
    id 76
    label "zrzutowy"
  ]
  node [
    id 77
    label "odk&#322;ad"
  ]
  node [
    id 78
    label "chody"
  ]
  node [
    id 79
    label "szaniec"
  ]
  node [
    id 80
    label "wyrobisko"
  ]
  node [
    id 81
    label "kopniak"
  ]
  node [
    id 82
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 83
    label "odwa&#322;"
  ]
  node [
    id 84
    label "grodzisko"
  ]
  node [
    id 85
    label "cios"
  ]
  node [
    id 86
    label "kick"
  ]
  node [
    id 87
    label "kopni&#281;cie"
  ]
  node [
    id 88
    label "&#347;rodkowiec"
  ]
  node [
    id 89
    label "podsadzka"
  ]
  node [
    id 90
    label "obudowa"
  ]
  node [
    id 91
    label "sp&#261;g"
  ]
  node [
    id 92
    label "strop"
  ]
  node [
    id 93
    label "rabowarka"
  ]
  node [
    id 94
    label "opinka"
  ]
  node [
    id 95
    label "stojak_cierny"
  ]
  node [
    id 96
    label "kopalnia"
  ]
  node [
    id 97
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 98
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 99
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 100
    label "immersion"
  ]
  node [
    id 101
    label "umieszczenie"
  ]
  node [
    id 102
    label "las"
  ]
  node [
    id 103
    label "nora"
  ]
  node [
    id 104
    label "pies_my&#347;liwski"
  ]
  node [
    id 105
    label "miejsce"
  ]
  node [
    id 106
    label "trasa"
  ]
  node [
    id 107
    label "doj&#347;cie"
  ]
  node [
    id 108
    label "zesp&#243;&#322;"
  ]
  node [
    id 109
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 110
    label "horodyszcze"
  ]
  node [
    id 111
    label "Wyszogr&#243;d"
  ]
  node [
    id 112
    label "usypisko"
  ]
  node [
    id 113
    label "r&#243;w"
  ]
  node [
    id 114
    label "wa&#322;"
  ]
  node [
    id 115
    label "redoubt"
  ]
  node [
    id 116
    label "fortyfikacja"
  ]
  node [
    id 117
    label "mechanika"
  ]
  node [
    id 118
    label "struktura"
  ]
  node [
    id 119
    label "figura"
  ]
  node [
    id 120
    label "miejsce_pracy"
  ]
  node [
    id 121
    label "cecha"
  ]
  node [
    id 122
    label "organ"
  ]
  node [
    id 123
    label "kreacja"
  ]
  node [
    id 124
    label "zwierz&#281;"
  ]
  node [
    id 125
    label "posesja"
  ]
  node [
    id 126
    label "konstrukcja"
  ]
  node [
    id 127
    label "wjazd"
  ]
  node [
    id 128
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 129
    label "praca"
  ]
  node [
    id 130
    label "constitution"
  ]
  node [
    id 131
    label "gleba"
  ]
  node [
    id 132
    label "p&#281;d"
  ]
  node [
    id 133
    label "zbi&#243;r"
  ]
  node [
    id 134
    label "ablegier"
  ]
  node [
    id 135
    label "rozmna&#380;anie_bezp&#322;ciowe"
  ]
  node [
    id 136
    label "layer"
  ]
  node [
    id 137
    label "r&#243;j"
  ]
  node [
    id 138
    label "mrowisko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
]
