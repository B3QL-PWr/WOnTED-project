graph [
  node [
    id 0
    label "rok"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "p&#243;&#322;rocze"
  ]
  node [
    id 3
    label "martwy_sezon"
  ]
  node [
    id 4
    label "kalendarz"
  ]
  node [
    id 5
    label "cykl_astronomiczny"
  ]
  node [
    id 6
    label "lata"
  ]
  node [
    id 7
    label "pora_roku"
  ]
  node [
    id 8
    label "stulecie"
  ]
  node [
    id 9
    label "kurs"
  ]
  node [
    id 10
    label "czas"
  ]
  node [
    id 11
    label "jubileusz"
  ]
  node [
    id 12
    label "grupa"
  ]
  node [
    id 13
    label "kwarta&#322;"
  ]
  node [
    id 14
    label "miesi&#261;c"
  ]
  node [
    id 15
    label "summer"
  ]
  node [
    id 16
    label "odm&#322;adzanie"
  ]
  node [
    id 17
    label "liga"
  ]
  node [
    id 18
    label "jednostka_systematyczna"
  ]
  node [
    id 19
    label "asymilowanie"
  ]
  node [
    id 20
    label "gromada"
  ]
  node [
    id 21
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 22
    label "asymilowa&#263;"
  ]
  node [
    id 23
    label "egzemplarz"
  ]
  node [
    id 24
    label "Entuzjastki"
  ]
  node [
    id 25
    label "zbi&#243;r"
  ]
  node [
    id 26
    label "kompozycja"
  ]
  node [
    id 27
    label "Terranie"
  ]
  node [
    id 28
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 29
    label "category"
  ]
  node [
    id 30
    label "pakiet_klimatyczny"
  ]
  node [
    id 31
    label "oddzia&#322;"
  ]
  node [
    id 32
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 33
    label "cz&#261;steczka"
  ]
  node [
    id 34
    label "stage_set"
  ]
  node [
    id 35
    label "type"
  ]
  node [
    id 36
    label "specgrupa"
  ]
  node [
    id 37
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 38
    label "&#346;wietliki"
  ]
  node [
    id 39
    label "odm&#322;odzenie"
  ]
  node [
    id 40
    label "Eurogrupa"
  ]
  node [
    id 41
    label "odm&#322;adza&#263;"
  ]
  node [
    id 42
    label "formacja_geologiczna"
  ]
  node [
    id 43
    label "harcerze_starsi"
  ]
  node [
    id 44
    label "poprzedzanie"
  ]
  node [
    id 45
    label "czasoprzestrze&#324;"
  ]
  node [
    id 46
    label "laba"
  ]
  node [
    id 47
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 48
    label "chronometria"
  ]
  node [
    id 49
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 50
    label "rachuba_czasu"
  ]
  node [
    id 51
    label "przep&#322;ywanie"
  ]
  node [
    id 52
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 53
    label "czasokres"
  ]
  node [
    id 54
    label "odczyt"
  ]
  node [
    id 55
    label "chwila"
  ]
  node [
    id 56
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 57
    label "dzieje"
  ]
  node [
    id 58
    label "kategoria_gramatyczna"
  ]
  node [
    id 59
    label "poprzedzenie"
  ]
  node [
    id 60
    label "trawienie"
  ]
  node [
    id 61
    label "pochodzi&#263;"
  ]
  node [
    id 62
    label "period"
  ]
  node [
    id 63
    label "okres_czasu"
  ]
  node [
    id 64
    label "poprzedza&#263;"
  ]
  node [
    id 65
    label "schy&#322;ek"
  ]
  node [
    id 66
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 67
    label "odwlekanie_si&#281;"
  ]
  node [
    id 68
    label "zegar"
  ]
  node [
    id 69
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 70
    label "czwarty_wymiar"
  ]
  node [
    id 71
    label "pochodzenie"
  ]
  node [
    id 72
    label "koniugacja"
  ]
  node [
    id 73
    label "Zeitgeist"
  ]
  node [
    id 74
    label "trawi&#263;"
  ]
  node [
    id 75
    label "pogoda"
  ]
  node [
    id 76
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 77
    label "poprzedzi&#263;"
  ]
  node [
    id 78
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 79
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 80
    label "time_period"
  ]
  node [
    id 81
    label "term"
  ]
  node [
    id 82
    label "rok_akademicki"
  ]
  node [
    id 83
    label "rok_szkolny"
  ]
  node [
    id 84
    label "semester"
  ]
  node [
    id 85
    label "anniwersarz"
  ]
  node [
    id 86
    label "rocznica"
  ]
  node [
    id 87
    label "obszar"
  ]
  node [
    id 88
    label "tydzie&#324;"
  ]
  node [
    id 89
    label "miech"
  ]
  node [
    id 90
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 91
    label "kalendy"
  ]
  node [
    id 92
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 93
    label "long_time"
  ]
  node [
    id 94
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 95
    label "almanac"
  ]
  node [
    id 96
    label "rozk&#322;ad"
  ]
  node [
    id 97
    label "wydawnictwo"
  ]
  node [
    id 98
    label "Juliusz_Cezar"
  ]
  node [
    id 99
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 100
    label "zwy&#380;kowanie"
  ]
  node [
    id 101
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 102
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 103
    label "zaj&#281;cia"
  ]
  node [
    id 104
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 105
    label "trasa"
  ]
  node [
    id 106
    label "przeorientowywanie"
  ]
  node [
    id 107
    label "przejazd"
  ]
  node [
    id 108
    label "kierunek"
  ]
  node [
    id 109
    label "przeorientowywa&#263;"
  ]
  node [
    id 110
    label "nauka"
  ]
  node [
    id 111
    label "przeorientowanie"
  ]
  node [
    id 112
    label "klasa"
  ]
  node [
    id 113
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 114
    label "przeorientowa&#263;"
  ]
  node [
    id 115
    label "manner"
  ]
  node [
    id 116
    label "course"
  ]
  node [
    id 117
    label "passage"
  ]
  node [
    id 118
    label "zni&#380;kowanie"
  ]
  node [
    id 119
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 120
    label "seria"
  ]
  node [
    id 121
    label "stawka"
  ]
  node [
    id 122
    label "way"
  ]
  node [
    id 123
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 124
    label "spos&#243;b"
  ]
  node [
    id 125
    label "deprecjacja"
  ]
  node [
    id 126
    label "cedu&#322;a"
  ]
  node [
    id 127
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 128
    label "drive"
  ]
  node [
    id 129
    label "bearing"
  ]
  node [
    id 130
    label "Lira"
  ]
  node [
    id 131
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 132
    label "mie&#263;_miejsce"
  ]
  node [
    id 133
    label "equal"
  ]
  node [
    id 134
    label "trwa&#263;"
  ]
  node [
    id 135
    label "chodzi&#263;"
  ]
  node [
    id 136
    label "si&#281;ga&#263;"
  ]
  node [
    id 137
    label "stan"
  ]
  node [
    id 138
    label "obecno&#347;&#263;"
  ]
  node [
    id 139
    label "stand"
  ]
  node [
    id 140
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 141
    label "uczestniczy&#263;"
  ]
  node [
    id 142
    label "participate"
  ]
  node [
    id 143
    label "robi&#263;"
  ]
  node [
    id 144
    label "istnie&#263;"
  ]
  node [
    id 145
    label "pozostawa&#263;"
  ]
  node [
    id 146
    label "zostawa&#263;"
  ]
  node [
    id 147
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 148
    label "adhere"
  ]
  node [
    id 149
    label "compass"
  ]
  node [
    id 150
    label "korzysta&#263;"
  ]
  node [
    id 151
    label "appreciation"
  ]
  node [
    id 152
    label "osi&#261;ga&#263;"
  ]
  node [
    id 153
    label "dociera&#263;"
  ]
  node [
    id 154
    label "get"
  ]
  node [
    id 155
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 156
    label "mierzy&#263;"
  ]
  node [
    id 157
    label "u&#380;ywa&#263;"
  ]
  node [
    id 158
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 159
    label "exsert"
  ]
  node [
    id 160
    label "being"
  ]
  node [
    id 161
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 162
    label "cecha"
  ]
  node [
    id 163
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 164
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 165
    label "p&#322;ywa&#263;"
  ]
  node [
    id 166
    label "run"
  ]
  node [
    id 167
    label "bangla&#263;"
  ]
  node [
    id 168
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 169
    label "przebiega&#263;"
  ]
  node [
    id 170
    label "wk&#322;ada&#263;"
  ]
  node [
    id 171
    label "proceed"
  ]
  node [
    id 172
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 173
    label "carry"
  ]
  node [
    id 174
    label "bywa&#263;"
  ]
  node [
    id 175
    label "dziama&#263;"
  ]
  node [
    id 176
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 177
    label "stara&#263;_si&#281;"
  ]
  node [
    id 178
    label "para"
  ]
  node [
    id 179
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 180
    label "str&#243;j"
  ]
  node [
    id 181
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 182
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 183
    label "krok"
  ]
  node [
    id 184
    label "tryb"
  ]
  node [
    id 185
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 186
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 187
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 188
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 189
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 190
    label "continue"
  ]
  node [
    id 191
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 192
    label "Ohio"
  ]
  node [
    id 193
    label "wci&#281;cie"
  ]
  node [
    id 194
    label "Nowy_York"
  ]
  node [
    id 195
    label "warstwa"
  ]
  node [
    id 196
    label "samopoczucie"
  ]
  node [
    id 197
    label "Illinois"
  ]
  node [
    id 198
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 199
    label "state"
  ]
  node [
    id 200
    label "Jukatan"
  ]
  node [
    id 201
    label "Kalifornia"
  ]
  node [
    id 202
    label "Wirginia"
  ]
  node [
    id 203
    label "wektor"
  ]
  node [
    id 204
    label "Goa"
  ]
  node [
    id 205
    label "Teksas"
  ]
  node [
    id 206
    label "Waszyngton"
  ]
  node [
    id 207
    label "miejsce"
  ]
  node [
    id 208
    label "Massachusetts"
  ]
  node [
    id 209
    label "Alaska"
  ]
  node [
    id 210
    label "Arakan"
  ]
  node [
    id 211
    label "Hawaje"
  ]
  node [
    id 212
    label "Maryland"
  ]
  node [
    id 213
    label "punkt"
  ]
  node [
    id 214
    label "Michigan"
  ]
  node [
    id 215
    label "Arizona"
  ]
  node [
    id 216
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 217
    label "Georgia"
  ]
  node [
    id 218
    label "poziom"
  ]
  node [
    id 219
    label "Pensylwania"
  ]
  node [
    id 220
    label "shape"
  ]
  node [
    id 221
    label "Luizjana"
  ]
  node [
    id 222
    label "Nowy_Meksyk"
  ]
  node [
    id 223
    label "Alabama"
  ]
  node [
    id 224
    label "ilo&#347;&#263;"
  ]
  node [
    id 225
    label "Kansas"
  ]
  node [
    id 226
    label "Oregon"
  ]
  node [
    id 227
    label "Oklahoma"
  ]
  node [
    id 228
    label "Floryda"
  ]
  node [
    id 229
    label "jednostka_administracyjna"
  ]
  node [
    id 230
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
]
