graph [
  node [
    id 0
    label "odst&#281;p"
    origin "text"
  ]
  node [
    id 1
    label "kilka"
    origin "text"
  ]
  node [
    id 2
    label "dni"
    origin "text"
  ]
  node [
    id 3
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "dwa"
    origin "text"
  ]
  node [
    id 6
    label "zupe&#322;nie"
    origin "text"
  ]
  node [
    id 7
    label "sprzeczny"
    origin "text"
  ]
  node [
    id 8
    label "diagnoza"
    origin "text"
  ]
  node [
    id 9
    label "wp&#322;yw"
    origin "text"
  ]
  node [
    id 10
    label "kryzys"
    origin "text"
  ]
  node [
    id 11
    label "wolne"
    origin "text"
  ]
  node [
    id 12
    label "oprogramowanie"
    origin "text"
  ]
  node [
    id 13
    label "wolny"
    origin "text"
  ]
  node [
    id 14
    label "kultura"
    origin "text"
  ]
  node [
    id 15
    label "naro&#380;nik"
    origin "text"
  ]
  node [
    id 16
    label "czerwona"
    origin "text"
  ]
  node [
    id 17
    label "strona"
    origin "text"
  ]
  node [
    id 18
    label "popyt"
    origin "text"
  ]
  node [
    id 19
    label "szef"
    origin "text"
  ]
  node [
    id 20
    label "reda"
    origin "text"
  ]
  node [
    id 21
    label "hata"
    origin "text"
  ]
  node [
    id 22
    label "jim"
    origin "text"
  ]
  node [
    id 23
    label "whitehurst"
    origin "text"
  ]
  node [
    id 24
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 25
    label "wszystek"
    origin "text"
  ]
  node [
    id 26
    label "zam&#243;wienie"
    origin "text"
  ]
  node [
    id 27
    label "bez"
    origin "text"
  ]
  node [
    id 28
    label "op&#322;ata"
    origin "text"
  ]
  node [
    id 29
    label "licencyjny"
    origin "text"
  ]
  node [
    id 30
    label "miejsce"
  ]
  node [
    id 31
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 32
    label "abcug"
  ]
  node [
    id 33
    label "ton"
  ]
  node [
    id 34
    label "rozmiar"
  ]
  node [
    id 35
    label "odcinek"
  ]
  node [
    id 36
    label "ambitus"
  ]
  node [
    id 37
    label "czas"
  ]
  node [
    id 38
    label "skala"
  ]
  node [
    id 39
    label "warunek_lokalowy"
  ]
  node [
    id 40
    label "plac"
  ]
  node [
    id 41
    label "location"
  ]
  node [
    id 42
    label "uwaga"
  ]
  node [
    id 43
    label "przestrze&#324;"
  ]
  node [
    id 44
    label "status"
  ]
  node [
    id 45
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 46
    label "chwila"
  ]
  node [
    id 47
    label "cia&#322;o"
  ]
  node [
    id 48
    label "cecha"
  ]
  node [
    id 49
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 50
    label "praca"
  ]
  node [
    id 51
    label "rz&#261;d"
  ]
  node [
    id 52
    label "zagranie"
  ]
  node [
    id 53
    label "ryba"
  ]
  node [
    id 54
    label "&#347;ledziowate"
  ]
  node [
    id 55
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 56
    label "kr&#281;gowiec"
  ]
  node [
    id 57
    label "cz&#322;owiek"
  ]
  node [
    id 58
    label "systemik"
  ]
  node [
    id 59
    label "doniczkowiec"
  ]
  node [
    id 60
    label "mi&#281;so"
  ]
  node [
    id 61
    label "system"
  ]
  node [
    id 62
    label "patroszy&#263;"
  ]
  node [
    id 63
    label "rakowato&#347;&#263;"
  ]
  node [
    id 64
    label "w&#281;dkarstwo"
  ]
  node [
    id 65
    label "ryby"
  ]
  node [
    id 66
    label "fish"
  ]
  node [
    id 67
    label "linia_boczna"
  ]
  node [
    id 68
    label "tar&#322;o"
  ]
  node [
    id 69
    label "wyrostek_filtracyjny"
  ]
  node [
    id 70
    label "m&#281;tnooki"
  ]
  node [
    id 71
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 72
    label "pokrywa_skrzelowa"
  ]
  node [
    id 73
    label "ikra"
  ]
  node [
    id 74
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 75
    label "szczelina_skrzelowa"
  ]
  node [
    id 76
    label "&#347;ledziokszta&#322;tne"
  ]
  node [
    id 77
    label "poprzedzanie"
  ]
  node [
    id 78
    label "czasoprzestrze&#324;"
  ]
  node [
    id 79
    label "laba"
  ]
  node [
    id 80
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 81
    label "chronometria"
  ]
  node [
    id 82
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 83
    label "rachuba_czasu"
  ]
  node [
    id 84
    label "przep&#322;ywanie"
  ]
  node [
    id 85
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 86
    label "czasokres"
  ]
  node [
    id 87
    label "odczyt"
  ]
  node [
    id 88
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 89
    label "dzieje"
  ]
  node [
    id 90
    label "kategoria_gramatyczna"
  ]
  node [
    id 91
    label "poprzedzenie"
  ]
  node [
    id 92
    label "trawienie"
  ]
  node [
    id 93
    label "pochodzi&#263;"
  ]
  node [
    id 94
    label "period"
  ]
  node [
    id 95
    label "okres_czasu"
  ]
  node [
    id 96
    label "poprzedza&#263;"
  ]
  node [
    id 97
    label "schy&#322;ek"
  ]
  node [
    id 98
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 99
    label "odwlekanie_si&#281;"
  ]
  node [
    id 100
    label "zegar"
  ]
  node [
    id 101
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 102
    label "czwarty_wymiar"
  ]
  node [
    id 103
    label "pochodzenie"
  ]
  node [
    id 104
    label "koniugacja"
  ]
  node [
    id 105
    label "Zeitgeist"
  ]
  node [
    id 106
    label "trawi&#263;"
  ]
  node [
    id 107
    label "pogoda"
  ]
  node [
    id 108
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 109
    label "poprzedzi&#263;"
  ]
  node [
    id 110
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 111
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 112
    label "time_period"
  ]
  node [
    id 113
    label "kompletny"
  ]
  node [
    id 114
    label "zupe&#322;ny"
  ]
  node [
    id 115
    label "wniwecz"
  ]
  node [
    id 116
    label "kompletnie"
  ]
  node [
    id 117
    label "w_pizdu"
  ]
  node [
    id 118
    label "pe&#322;ny"
  ]
  node [
    id 119
    label "og&#243;lnie"
  ]
  node [
    id 120
    label "ca&#322;y"
  ]
  node [
    id 121
    label "&#322;&#261;czny"
  ]
  node [
    id 122
    label "niezgodny"
  ]
  node [
    id 123
    label "sprzecznie"
  ]
  node [
    id 124
    label "r&#243;&#380;ny"
  ]
  node [
    id 125
    label "niespokojny"
  ]
  node [
    id 126
    label "odmienny"
  ]
  node [
    id 127
    label "k&#322;&#243;tny"
  ]
  node [
    id 128
    label "niezgodnie"
  ]
  node [
    id 129
    label "napi&#281;ty"
  ]
  node [
    id 130
    label "diagnosis"
  ]
  node [
    id 131
    label "sprawdzian"
  ]
  node [
    id 132
    label "schorzenie"
  ]
  node [
    id 133
    label "rozpoznanie"
  ]
  node [
    id 134
    label "dokument"
  ]
  node [
    id 135
    label "ocena"
  ]
  node [
    id 136
    label "zapis"
  ]
  node [
    id 137
    label "&#347;wiadectwo"
  ]
  node [
    id 138
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 139
    label "wytw&#243;r"
  ]
  node [
    id 140
    label "parafa"
  ]
  node [
    id 141
    label "plik"
  ]
  node [
    id 142
    label "raport&#243;wka"
  ]
  node [
    id 143
    label "utw&#243;r"
  ]
  node [
    id 144
    label "record"
  ]
  node [
    id 145
    label "fascyku&#322;"
  ]
  node [
    id 146
    label "dokumentacja"
  ]
  node [
    id 147
    label "registratura"
  ]
  node [
    id 148
    label "artyku&#322;"
  ]
  node [
    id 149
    label "writing"
  ]
  node [
    id 150
    label "sygnatariusz"
  ]
  node [
    id 151
    label "pogl&#261;d"
  ]
  node [
    id 152
    label "decyzja"
  ]
  node [
    id 153
    label "sofcik"
  ]
  node [
    id 154
    label "kryterium"
  ]
  node [
    id 155
    label "informacja"
  ]
  node [
    id 156
    label "appraisal"
  ]
  node [
    id 157
    label "faza"
  ]
  node [
    id 158
    label "podchodzi&#263;"
  ]
  node [
    id 159
    label "&#263;wiczenie"
  ]
  node [
    id 160
    label "pytanie"
  ]
  node [
    id 161
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 162
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 163
    label "praca_pisemna"
  ]
  node [
    id 164
    label "kontrola"
  ]
  node [
    id 165
    label "dydaktyka"
  ]
  node [
    id 166
    label "pr&#243;ba"
  ]
  node [
    id 167
    label "examination"
  ]
  node [
    id 168
    label "badanie"
  ]
  node [
    id 169
    label "designation"
  ]
  node [
    id 170
    label "recce"
  ]
  node [
    id 171
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 172
    label "rozwa&#380;enie"
  ]
  node [
    id 173
    label "ognisko"
  ]
  node [
    id 174
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 175
    label "powalenie"
  ]
  node [
    id 176
    label "odezwanie_si&#281;"
  ]
  node [
    id 177
    label "atakowanie"
  ]
  node [
    id 178
    label "grupa_ryzyka"
  ]
  node [
    id 179
    label "przypadek"
  ]
  node [
    id 180
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 181
    label "nabawienie_si&#281;"
  ]
  node [
    id 182
    label "inkubacja"
  ]
  node [
    id 183
    label "powali&#263;"
  ]
  node [
    id 184
    label "remisja"
  ]
  node [
    id 185
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 186
    label "zajmowa&#263;"
  ]
  node [
    id 187
    label "zaburzenie"
  ]
  node [
    id 188
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 189
    label "badanie_histopatologiczne"
  ]
  node [
    id 190
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 191
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 192
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 193
    label "odzywanie_si&#281;"
  ]
  node [
    id 194
    label "atakowa&#263;"
  ]
  node [
    id 195
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 196
    label "nabawianie_si&#281;"
  ]
  node [
    id 197
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 198
    label "zajmowanie"
  ]
  node [
    id 199
    label "kwota"
  ]
  node [
    id 200
    label "&#347;lad"
  ]
  node [
    id 201
    label "zjawisko"
  ]
  node [
    id 202
    label "rezultat"
  ]
  node [
    id 203
    label "lobbysta"
  ]
  node [
    id 204
    label "doch&#243;d_narodowy"
  ]
  node [
    id 205
    label "dzia&#322;anie"
  ]
  node [
    id 206
    label "typ"
  ]
  node [
    id 207
    label "event"
  ]
  node [
    id 208
    label "przyczyna"
  ]
  node [
    id 209
    label "proces"
  ]
  node [
    id 210
    label "boski"
  ]
  node [
    id 211
    label "krajobraz"
  ]
  node [
    id 212
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 213
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 214
    label "przywidzenie"
  ]
  node [
    id 215
    label "presence"
  ]
  node [
    id 216
    label "charakter"
  ]
  node [
    id 217
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 218
    label "wynie&#347;&#263;"
  ]
  node [
    id 219
    label "pieni&#261;dze"
  ]
  node [
    id 220
    label "ilo&#347;&#263;"
  ]
  node [
    id 221
    label "limit"
  ]
  node [
    id 222
    label "wynosi&#263;"
  ]
  node [
    id 223
    label "sznurowanie"
  ]
  node [
    id 224
    label "odrobina"
  ]
  node [
    id 225
    label "skutek"
  ]
  node [
    id 226
    label "sznurowa&#263;"
  ]
  node [
    id 227
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 228
    label "attribute"
  ]
  node [
    id 229
    label "odcisk"
  ]
  node [
    id 230
    label "przedstawiciel"
  ]
  node [
    id 231
    label "grupa_nacisku"
  ]
  node [
    id 232
    label "July"
  ]
  node [
    id 233
    label "k&#322;opot"
  ]
  node [
    id 234
    label "cykl_koniunkturalny"
  ]
  node [
    id 235
    label "zwrot"
  ]
  node [
    id 236
    label "Marzec_'68"
  ]
  node [
    id 237
    label "pogorszenie"
  ]
  node [
    id 238
    label "sytuacja"
  ]
  node [
    id 239
    label "head"
  ]
  node [
    id 240
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 241
    label "warunki"
  ]
  node [
    id 242
    label "szczeg&#243;&#322;"
  ]
  node [
    id 243
    label "state"
  ]
  node [
    id 244
    label "motyw"
  ]
  node [
    id 245
    label "realia"
  ]
  node [
    id 246
    label "zmiana"
  ]
  node [
    id 247
    label "aggravation"
  ]
  node [
    id 248
    label "worsening"
  ]
  node [
    id 249
    label "zmienienie"
  ]
  node [
    id 250
    label "gorszy"
  ]
  node [
    id 251
    label "punkt"
  ]
  node [
    id 252
    label "turn"
  ]
  node [
    id 253
    label "turning"
  ]
  node [
    id 254
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 255
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 256
    label "skr&#281;t"
  ]
  node [
    id 257
    label "obr&#243;t"
  ]
  node [
    id 258
    label "fraza_czasownikowa"
  ]
  node [
    id 259
    label "jednostka_leksykalna"
  ]
  node [
    id 260
    label "wyra&#380;enie"
  ]
  node [
    id 261
    label "problem"
  ]
  node [
    id 262
    label "subiekcja"
  ]
  node [
    id 263
    label "czas_wolny"
  ]
  node [
    id 264
    label "reengineering"
  ]
  node [
    id 265
    label "program"
  ]
  node [
    id 266
    label "zbi&#243;r"
  ]
  node [
    id 267
    label "egzemplarz"
  ]
  node [
    id 268
    label "series"
  ]
  node [
    id 269
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 270
    label "uprawianie"
  ]
  node [
    id 271
    label "praca_rolnicza"
  ]
  node [
    id 272
    label "collection"
  ]
  node [
    id 273
    label "dane"
  ]
  node [
    id 274
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 275
    label "pakiet_klimatyczny"
  ]
  node [
    id 276
    label "poj&#281;cie"
  ]
  node [
    id 277
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 278
    label "sum"
  ]
  node [
    id 279
    label "gathering"
  ]
  node [
    id 280
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 281
    label "album"
  ]
  node [
    id 282
    label "instalowa&#263;"
  ]
  node [
    id 283
    label "odinstalowywa&#263;"
  ]
  node [
    id 284
    label "spis"
  ]
  node [
    id 285
    label "zaprezentowanie"
  ]
  node [
    id 286
    label "podprogram"
  ]
  node [
    id 287
    label "ogranicznik_referencyjny"
  ]
  node [
    id 288
    label "course_of_study"
  ]
  node [
    id 289
    label "booklet"
  ]
  node [
    id 290
    label "dzia&#322;"
  ]
  node [
    id 291
    label "odinstalowanie"
  ]
  node [
    id 292
    label "broszura"
  ]
  node [
    id 293
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 294
    label "kana&#322;"
  ]
  node [
    id 295
    label "teleferie"
  ]
  node [
    id 296
    label "zainstalowanie"
  ]
  node [
    id 297
    label "struktura_organizacyjna"
  ]
  node [
    id 298
    label "pirat"
  ]
  node [
    id 299
    label "zaprezentowa&#263;"
  ]
  node [
    id 300
    label "prezentowanie"
  ]
  node [
    id 301
    label "prezentowa&#263;"
  ]
  node [
    id 302
    label "interfejs"
  ]
  node [
    id 303
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 304
    label "okno"
  ]
  node [
    id 305
    label "blok"
  ]
  node [
    id 306
    label "folder"
  ]
  node [
    id 307
    label "zainstalowa&#263;"
  ]
  node [
    id 308
    label "za&#322;o&#380;enie"
  ]
  node [
    id 309
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 310
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 311
    label "ram&#243;wka"
  ]
  node [
    id 312
    label "tryb"
  ]
  node [
    id 313
    label "emitowa&#263;"
  ]
  node [
    id 314
    label "emitowanie"
  ]
  node [
    id 315
    label "odinstalowywanie"
  ]
  node [
    id 316
    label "instrukcja"
  ]
  node [
    id 317
    label "informatyka"
  ]
  node [
    id 318
    label "deklaracja"
  ]
  node [
    id 319
    label "sekcja_krytyczna"
  ]
  node [
    id 320
    label "menu"
  ]
  node [
    id 321
    label "furkacja"
  ]
  node [
    id 322
    label "podstawa"
  ]
  node [
    id 323
    label "instalowanie"
  ]
  node [
    id 324
    label "oferta"
  ]
  node [
    id 325
    label "odinstalowa&#263;"
  ]
  node [
    id 326
    label "przer&#243;bka"
  ]
  node [
    id 327
    label "firma"
  ]
  node [
    id 328
    label "odmienienie"
  ]
  node [
    id 329
    label "strategia"
  ]
  node [
    id 330
    label "zmienia&#263;"
  ]
  node [
    id 331
    label "rzedni&#281;cie"
  ]
  node [
    id 332
    label "niespieszny"
  ]
  node [
    id 333
    label "zwalnianie_si&#281;"
  ]
  node [
    id 334
    label "wakowa&#263;"
  ]
  node [
    id 335
    label "rozwadnianie"
  ]
  node [
    id 336
    label "niezale&#380;ny"
  ]
  node [
    id 337
    label "rozwodnienie"
  ]
  node [
    id 338
    label "zrzedni&#281;cie"
  ]
  node [
    id 339
    label "swobodnie"
  ]
  node [
    id 340
    label "rozrzedzanie"
  ]
  node [
    id 341
    label "rozrzedzenie"
  ]
  node [
    id 342
    label "strza&#322;"
  ]
  node [
    id 343
    label "wolnie"
  ]
  node [
    id 344
    label "zwolnienie_si&#281;"
  ]
  node [
    id 345
    label "wolno"
  ]
  node [
    id 346
    label "lu&#378;no"
  ]
  node [
    id 347
    label "niespiesznie"
  ]
  node [
    id 348
    label "spokojny"
  ]
  node [
    id 349
    label "trafny"
  ]
  node [
    id 350
    label "shot"
  ]
  node [
    id 351
    label "przykro&#347;&#263;"
  ]
  node [
    id 352
    label "huk"
  ]
  node [
    id 353
    label "bum-bum"
  ]
  node [
    id 354
    label "pi&#322;ka"
  ]
  node [
    id 355
    label "uderzenie"
  ]
  node [
    id 356
    label "eksplozja"
  ]
  node [
    id 357
    label "wyrzut"
  ]
  node [
    id 358
    label "usi&#322;owanie"
  ]
  node [
    id 359
    label "shooting"
  ]
  node [
    id 360
    label "odgadywanie"
  ]
  node [
    id 361
    label "usamodzielnianie_si&#281;"
  ]
  node [
    id 362
    label "usamodzielnienie"
  ]
  node [
    id 363
    label "usamodzielnianie"
  ]
  node [
    id 364
    label "niezale&#380;nie"
  ]
  node [
    id 365
    label "thinly"
  ]
  node [
    id 366
    label "wolniej"
  ]
  node [
    id 367
    label "swobodny"
  ]
  node [
    id 368
    label "free"
  ]
  node [
    id 369
    label "lu&#378;ny"
  ]
  node [
    id 370
    label "dowolnie"
  ]
  node [
    id 371
    label "naturalnie"
  ]
  node [
    id 372
    label "rzadki"
  ]
  node [
    id 373
    label "stawanie_si&#281;"
  ]
  node [
    id 374
    label "lekko"
  ]
  node [
    id 375
    label "&#322;atwo"
  ]
  node [
    id 376
    label "odlegle"
  ]
  node [
    id 377
    label "przyjemnie"
  ]
  node [
    id 378
    label "nieformalnie"
  ]
  node [
    id 379
    label "rarefaction"
  ]
  node [
    id 380
    label "czynno&#347;&#263;"
  ]
  node [
    id 381
    label "spowodowanie"
  ]
  node [
    id 382
    label "dilution"
  ]
  node [
    id 383
    label "powodowanie"
  ]
  node [
    id 384
    label "rozcie&#324;czanie"
  ]
  node [
    id 385
    label "chrzczenie"
  ]
  node [
    id 386
    label "stanie_si&#281;"
  ]
  node [
    id 387
    label "ochrzczenie"
  ]
  node [
    id 388
    label "rozcie&#324;czenie"
  ]
  node [
    id 389
    label "stanowisko"
  ]
  node [
    id 390
    label "by&#263;"
  ]
  node [
    id 391
    label "asymilowanie_si&#281;"
  ]
  node [
    id 392
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 393
    label "Wsch&#243;d"
  ]
  node [
    id 394
    label "przedmiot"
  ]
  node [
    id 395
    label "przejmowanie"
  ]
  node [
    id 396
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 397
    label "makrokosmos"
  ]
  node [
    id 398
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 399
    label "konwencja"
  ]
  node [
    id 400
    label "rzecz"
  ]
  node [
    id 401
    label "propriety"
  ]
  node [
    id 402
    label "przejmowa&#263;"
  ]
  node [
    id 403
    label "brzoskwiniarnia"
  ]
  node [
    id 404
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 405
    label "sztuka"
  ]
  node [
    id 406
    label "zwyczaj"
  ]
  node [
    id 407
    label "jako&#347;&#263;"
  ]
  node [
    id 408
    label "kuchnia"
  ]
  node [
    id 409
    label "tradycja"
  ]
  node [
    id 410
    label "populace"
  ]
  node [
    id 411
    label "hodowla"
  ]
  node [
    id 412
    label "religia"
  ]
  node [
    id 413
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 414
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 415
    label "przej&#281;cie"
  ]
  node [
    id 416
    label "przej&#261;&#263;"
  ]
  node [
    id 417
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 418
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 419
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 420
    label "warto&#347;&#263;"
  ]
  node [
    id 421
    label "quality"
  ]
  node [
    id 422
    label "co&#347;"
  ]
  node [
    id 423
    label "syf"
  ]
  node [
    id 424
    label "absolutorium"
  ]
  node [
    id 425
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 426
    label "activity"
  ]
  node [
    id 427
    label "potrzymanie"
  ]
  node [
    id 428
    label "rolnictwo"
  ]
  node [
    id 429
    label "pod&#243;j"
  ]
  node [
    id 430
    label "filiacja"
  ]
  node [
    id 431
    label "licencjonowanie"
  ]
  node [
    id 432
    label "opasa&#263;"
  ]
  node [
    id 433
    label "ch&#243;w"
  ]
  node [
    id 434
    label "licencja"
  ]
  node [
    id 435
    label "sokolarnia"
  ]
  node [
    id 436
    label "potrzyma&#263;"
  ]
  node [
    id 437
    label "rozp&#322;&#243;d"
  ]
  node [
    id 438
    label "grupa_organizm&#243;w"
  ]
  node [
    id 439
    label "wypas"
  ]
  node [
    id 440
    label "wychowalnia"
  ]
  node [
    id 441
    label "pstr&#261;garnia"
  ]
  node [
    id 442
    label "krzy&#380;owanie"
  ]
  node [
    id 443
    label "licencjonowa&#263;"
  ]
  node [
    id 444
    label "odch&#243;w"
  ]
  node [
    id 445
    label "tucz"
  ]
  node [
    id 446
    label "ud&#243;j"
  ]
  node [
    id 447
    label "klatka"
  ]
  node [
    id 448
    label "opasienie"
  ]
  node [
    id 449
    label "wych&#243;w"
  ]
  node [
    id 450
    label "obrz&#261;dek"
  ]
  node [
    id 451
    label "opasanie"
  ]
  node [
    id 452
    label "polish"
  ]
  node [
    id 453
    label "akwarium"
  ]
  node [
    id 454
    label "biotechnika"
  ]
  node [
    id 455
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 456
    label "uk&#322;ad"
  ]
  node [
    id 457
    label "styl"
  ]
  node [
    id 458
    label "line"
  ]
  node [
    id 459
    label "kanon"
  ]
  node [
    id 460
    label "zjazd"
  ]
  node [
    id 461
    label "charakterystyka"
  ]
  node [
    id 462
    label "m&#322;ot"
  ]
  node [
    id 463
    label "znak"
  ]
  node [
    id 464
    label "drzewo"
  ]
  node [
    id 465
    label "marka"
  ]
  node [
    id 466
    label "biom"
  ]
  node [
    id 467
    label "szata_ro&#347;linna"
  ]
  node [
    id 468
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 469
    label "formacja_ro&#347;linna"
  ]
  node [
    id 470
    label "przyroda"
  ]
  node [
    id 471
    label "zielono&#347;&#263;"
  ]
  node [
    id 472
    label "pi&#281;tro"
  ]
  node [
    id 473
    label "plant"
  ]
  node [
    id 474
    label "ro&#347;lina"
  ]
  node [
    id 475
    label "geosystem"
  ]
  node [
    id 476
    label "kult"
  ]
  node [
    id 477
    label "mitologia"
  ]
  node [
    id 478
    label "wyznanie"
  ]
  node [
    id 479
    label "ideologia"
  ]
  node [
    id 480
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 481
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 482
    label "nawracanie_si&#281;"
  ]
  node [
    id 483
    label "duchowny"
  ]
  node [
    id 484
    label "rela"
  ]
  node [
    id 485
    label "kultura_duchowa"
  ]
  node [
    id 486
    label "kosmologia"
  ]
  node [
    id 487
    label "kosmogonia"
  ]
  node [
    id 488
    label "nawraca&#263;"
  ]
  node [
    id 489
    label "mistyka"
  ]
  node [
    id 490
    label "pr&#243;bowanie"
  ]
  node [
    id 491
    label "rola"
  ]
  node [
    id 492
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 493
    label "realizacja"
  ]
  node [
    id 494
    label "scena"
  ]
  node [
    id 495
    label "didaskalia"
  ]
  node [
    id 496
    label "czyn"
  ]
  node [
    id 497
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 498
    label "environment"
  ]
  node [
    id 499
    label "scenariusz"
  ]
  node [
    id 500
    label "jednostka"
  ]
  node [
    id 501
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 502
    label "fortel"
  ]
  node [
    id 503
    label "theatrical_performance"
  ]
  node [
    id 504
    label "ambala&#380;"
  ]
  node [
    id 505
    label "sprawno&#347;&#263;"
  ]
  node [
    id 506
    label "kobieta"
  ]
  node [
    id 507
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 508
    label "Faust"
  ]
  node [
    id 509
    label "scenografia"
  ]
  node [
    id 510
    label "ods&#322;ona"
  ]
  node [
    id 511
    label "pokaz"
  ]
  node [
    id 512
    label "przedstawienie"
  ]
  node [
    id 513
    label "przedstawi&#263;"
  ]
  node [
    id 514
    label "Apollo"
  ]
  node [
    id 515
    label "przedstawianie"
  ]
  node [
    id 516
    label "przedstawia&#263;"
  ]
  node [
    id 517
    label "towar"
  ]
  node [
    id 518
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 519
    label "zachowanie"
  ]
  node [
    id 520
    label "ceremony"
  ]
  node [
    id 521
    label "dorobek"
  ]
  node [
    id 522
    label "tworzenie"
  ]
  node [
    id 523
    label "kreacja"
  ]
  node [
    id 524
    label "creation"
  ]
  node [
    id 525
    label "staro&#347;cina_weselna"
  ]
  node [
    id 526
    label "folklor"
  ]
  node [
    id 527
    label "objawienie"
  ]
  node [
    id 528
    label "zaj&#281;cie"
  ]
  node [
    id 529
    label "instytucja"
  ]
  node [
    id 530
    label "tajniki"
  ]
  node [
    id 531
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 532
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 533
    label "jedzenie"
  ]
  node [
    id 534
    label "zaplecze"
  ]
  node [
    id 535
    label "pomieszczenie"
  ]
  node [
    id 536
    label "zlewozmywak"
  ]
  node [
    id 537
    label "gotowa&#263;"
  ]
  node [
    id 538
    label "ciemna_materia"
  ]
  node [
    id 539
    label "planeta"
  ]
  node [
    id 540
    label "mikrokosmos"
  ]
  node [
    id 541
    label "ekosfera"
  ]
  node [
    id 542
    label "czarna_dziura"
  ]
  node [
    id 543
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 544
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 545
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 546
    label "kosmos"
  ]
  node [
    id 547
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 548
    label "poprawno&#347;&#263;"
  ]
  node [
    id 549
    label "og&#322;ada"
  ]
  node [
    id 550
    label "service"
  ]
  node [
    id 551
    label "stosowno&#347;&#263;"
  ]
  node [
    id 552
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 553
    label "Ukraina"
  ]
  node [
    id 554
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 555
    label "blok_wschodni"
  ]
  node [
    id 556
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 557
    label "wsch&#243;d"
  ]
  node [
    id 558
    label "Europa_Wschodnia"
  ]
  node [
    id 559
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 560
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 561
    label "wra&#380;enie"
  ]
  node [
    id 562
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 563
    label "interception"
  ]
  node [
    id 564
    label "wzbudzenie"
  ]
  node [
    id 565
    label "emotion"
  ]
  node [
    id 566
    label "movement"
  ]
  node [
    id 567
    label "zaczerpni&#281;cie"
  ]
  node [
    id 568
    label "wzi&#281;cie"
  ]
  node [
    id 569
    label "bang"
  ]
  node [
    id 570
    label "wzi&#261;&#263;"
  ]
  node [
    id 571
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 572
    label "stimulate"
  ]
  node [
    id 573
    label "ogarn&#261;&#263;"
  ]
  node [
    id 574
    label "wzbudzi&#263;"
  ]
  node [
    id 575
    label "thrill"
  ]
  node [
    id 576
    label "treat"
  ]
  node [
    id 577
    label "czerpa&#263;"
  ]
  node [
    id 578
    label "bra&#263;"
  ]
  node [
    id 579
    label "go"
  ]
  node [
    id 580
    label "handle"
  ]
  node [
    id 581
    label "wzbudza&#263;"
  ]
  node [
    id 582
    label "ogarnia&#263;"
  ]
  node [
    id 583
    label "czerpanie"
  ]
  node [
    id 584
    label "acquisition"
  ]
  node [
    id 585
    label "branie"
  ]
  node [
    id 586
    label "caparison"
  ]
  node [
    id 587
    label "wzbudzanie"
  ]
  node [
    id 588
    label "ogarnianie"
  ]
  node [
    id 589
    label "object"
  ]
  node [
    id 590
    label "temat"
  ]
  node [
    id 591
    label "wpadni&#281;cie"
  ]
  node [
    id 592
    label "mienie"
  ]
  node [
    id 593
    label "istota"
  ]
  node [
    id 594
    label "obiekt"
  ]
  node [
    id 595
    label "wpa&#347;&#263;"
  ]
  node [
    id 596
    label "wpadanie"
  ]
  node [
    id 597
    label "wpada&#263;"
  ]
  node [
    id 598
    label "zboczenie"
  ]
  node [
    id 599
    label "om&#243;wienie"
  ]
  node [
    id 600
    label "sponiewieranie"
  ]
  node [
    id 601
    label "discipline"
  ]
  node [
    id 602
    label "omawia&#263;"
  ]
  node [
    id 603
    label "kr&#261;&#380;enie"
  ]
  node [
    id 604
    label "tre&#347;&#263;"
  ]
  node [
    id 605
    label "robienie"
  ]
  node [
    id 606
    label "sponiewiera&#263;"
  ]
  node [
    id 607
    label "element"
  ]
  node [
    id 608
    label "entity"
  ]
  node [
    id 609
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 610
    label "tematyka"
  ]
  node [
    id 611
    label "w&#261;tek"
  ]
  node [
    id 612
    label "zbaczanie"
  ]
  node [
    id 613
    label "program_nauczania"
  ]
  node [
    id 614
    label "om&#243;wi&#263;"
  ]
  node [
    id 615
    label "omawianie"
  ]
  node [
    id 616
    label "thing"
  ]
  node [
    id 617
    label "zbacza&#263;"
  ]
  node [
    id 618
    label "zboczy&#263;"
  ]
  node [
    id 619
    label "uprawa"
  ]
  node [
    id 620
    label "figura_zaszczytna"
  ]
  node [
    id 621
    label "mebel"
  ]
  node [
    id 622
    label "kraw&#281;d&#378;"
  ]
  node [
    id 623
    label "okucie"
  ]
  node [
    id 624
    label "corner"
  ]
  node [
    id 625
    label "graf"
  ]
  node [
    id 626
    label "para"
  ]
  node [
    id 627
    label "narta"
  ]
  node [
    id 628
    label "ochraniacz"
  ]
  node [
    id 629
    label "end"
  ]
  node [
    id 630
    label "koniec"
  ]
  node [
    id 631
    label "przeszklenie"
  ]
  node [
    id 632
    label "ramiak"
  ]
  node [
    id 633
    label "obudowanie"
  ]
  node [
    id 634
    label "obudowywa&#263;"
  ]
  node [
    id 635
    label "obudowa&#263;"
  ]
  node [
    id 636
    label "sprz&#281;t"
  ]
  node [
    id 637
    label "gzyms"
  ]
  node [
    id 638
    label "nadstawa"
  ]
  node [
    id 639
    label "element_wyposa&#380;enia"
  ]
  node [
    id 640
    label "obudowywanie"
  ]
  node [
    id 641
    label "umeblowanie"
  ]
  node [
    id 642
    label "fitting"
  ]
  node [
    id 643
    label "horseshoe"
  ]
  node [
    id 644
    label "mocowanie"
  ]
  node [
    id 645
    label "okowa"
  ]
  node [
    id 646
    label "ochrona"
  ]
  node [
    id 647
    label "obicie"
  ]
  node [
    id 648
    label "ozdoba"
  ]
  node [
    id 649
    label "jacht"
  ]
  node [
    id 650
    label "kartka"
  ]
  node [
    id 651
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 652
    label "logowanie"
  ]
  node [
    id 653
    label "s&#261;d"
  ]
  node [
    id 654
    label "adres_internetowy"
  ]
  node [
    id 655
    label "linia"
  ]
  node [
    id 656
    label "serwis_internetowy"
  ]
  node [
    id 657
    label "posta&#263;"
  ]
  node [
    id 658
    label "bok"
  ]
  node [
    id 659
    label "skr&#281;canie"
  ]
  node [
    id 660
    label "skr&#281;ca&#263;"
  ]
  node [
    id 661
    label "orientowanie"
  ]
  node [
    id 662
    label "skr&#281;ci&#263;"
  ]
  node [
    id 663
    label "uj&#281;cie"
  ]
  node [
    id 664
    label "zorientowanie"
  ]
  node [
    id 665
    label "ty&#322;"
  ]
  node [
    id 666
    label "fragment"
  ]
  node [
    id 667
    label "layout"
  ]
  node [
    id 668
    label "zorientowa&#263;"
  ]
  node [
    id 669
    label "pagina"
  ]
  node [
    id 670
    label "podmiot"
  ]
  node [
    id 671
    label "g&#243;ra"
  ]
  node [
    id 672
    label "orientowa&#263;"
  ]
  node [
    id 673
    label "voice"
  ]
  node [
    id 674
    label "orientacja"
  ]
  node [
    id 675
    label "prz&#243;d"
  ]
  node [
    id 676
    label "internet"
  ]
  node [
    id 677
    label "powierzchnia"
  ]
  node [
    id 678
    label "forma"
  ]
  node [
    id 679
    label "skr&#281;cenie"
  ]
  node [
    id 680
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 681
    label "byt"
  ]
  node [
    id 682
    label "osobowo&#347;&#263;"
  ]
  node [
    id 683
    label "organizacja"
  ]
  node [
    id 684
    label "prawo"
  ]
  node [
    id 685
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 686
    label "nauka_prawa"
  ]
  node [
    id 687
    label "zaistnie&#263;"
  ]
  node [
    id 688
    label "Osjan"
  ]
  node [
    id 689
    label "kto&#347;"
  ]
  node [
    id 690
    label "wygl&#261;d"
  ]
  node [
    id 691
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 692
    label "trim"
  ]
  node [
    id 693
    label "poby&#263;"
  ]
  node [
    id 694
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 695
    label "Aspazja"
  ]
  node [
    id 696
    label "punkt_widzenia"
  ]
  node [
    id 697
    label "kompleksja"
  ]
  node [
    id 698
    label "wytrzyma&#263;"
  ]
  node [
    id 699
    label "budowa"
  ]
  node [
    id 700
    label "formacja"
  ]
  node [
    id 701
    label "pozosta&#263;"
  ]
  node [
    id 702
    label "point"
  ]
  node [
    id 703
    label "go&#347;&#263;"
  ]
  node [
    id 704
    label "kszta&#322;t"
  ]
  node [
    id 705
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 706
    label "armia"
  ]
  node [
    id 707
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 708
    label "poprowadzi&#263;"
  ]
  node [
    id 709
    label "cord"
  ]
  node [
    id 710
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 711
    label "trasa"
  ]
  node [
    id 712
    label "po&#322;&#261;czenie"
  ]
  node [
    id 713
    label "tract"
  ]
  node [
    id 714
    label "materia&#322;_zecerski"
  ]
  node [
    id 715
    label "przeorientowywanie"
  ]
  node [
    id 716
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 717
    label "curve"
  ]
  node [
    id 718
    label "figura_geometryczna"
  ]
  node [
    id 719
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 720
    label "jard"
  ]
  node [
    id 721
    label "szczep"
  ]
  node [
    id 722
    label "phreaker"
  ]
  node [
    id 723
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 724
    label "prowadzi&#263;"
  ]
  node [
    id 725
    label "przeorientowywa&#263;"
  ]
  node [
    id 726
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 727
    label "access"
  ]
  node [
    id 728
    label "przeorientowanie"
  ]
  node [
    id 729
    label "przeorientowa&#263;"
  ]
  node [
    id 730
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 731
    label "billing"
  ]
  node [
    id 732
    label "granica"
  ]
  node [
    id 733
    label "szpaler"
  ]
  node [
    id 734
    label "sztrych"
  ]
  node [
    id 735
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 736
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 737
    label "drzewo_genealogiczne"
  ]
  node [
    id 738
    label "transporter"
  ]
  node [
    id 739
    label "przew&#243;d"
  ]
  node [
    id 740
    label "granice"
  ]
  node [
    id 741
    label "kontakt"
  ]
  node [
    id 742
    label "przewo&#378;nik"
  ]
  node [
    id 743
    label "przystanek"
  ]
  node [
    id 744
    label "linijka"
  ]
  node [
    id 745
    label "spos&#243;b"
  ]
  node [
    id 746
    label "uporz&#261;dkowanie"
  ]
  node [
    id 747
    label "coalescence"
  ]
  node [
    id 748
    label "Ural"
  ]
  node [
    id 749
    label "bearing"
  ]
  node [
    id 750
    label "prowadzenie"
  ]
  node [
    id 751
    label "tekst"
  ]
  node [
    id 752
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 753
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 754
    label "podkatalog"
  ]
  node [
    id 755
    label "nadpisa&#263;"
  ]
  node [
    id 756
    label "nadpisanie"
  ]
  node [
    id 757
    label "bundle"
  ]
  node [
    id 758
    label "nadpisywanie"
  ]
  node [
    id 759
    label "paczka"
  ]
  node [
    id 760
    label "nadpisywa&#263;"
  ]
  node [
    id 761
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 762
    label "Rzym_Zachodni"
  ]
  node [
    id 763
    label "whole"
  ]
  node [
    id 764
    label "Rzym_Wschodni"
  ]
  node [
    id 765
    label "urz&#261;dzenie"
  ]
  node [
    id 766
    label "obszar"
  ]
  node [
    id 767
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 768
    label "zwierciad&#322;o"
  ]
  node [
    id 769
    label "capacity"
  ]
  node [
    id 770
    label "plane"
  ]
  node [
    id 771
    label "jednostka_systematyczna"
  ]
  node [
    id 772
    label "poznanie"
  ]
  node [
    id 773
    label "leksem"
  ]
  node [
    id 774
    label "dzie&#322;o"
  ]
  node [
    id 775
    label "stan"
  ]
  node [
    id 776
    label "blaszka"
  ]
  node [
    id 777
    label "kantyzm"
  ]
  node [
    id 778
    label "zdolno&#347;&#263;"
  ]
  node [
    id 779
    label "do&#322;ek"
  ]
  node [
    id 780
    label "zawarto&#347;&#263;"
  ]
  node [
    id 781
    label "gwiazda"
  ]
  node [
    id 782
    label "formality"
  ]
  node [
    id 783
    label "struktura"
  ]
  node [
    id 784
    label "mode"
  ]
  node [
    id 785
    label "morfem"
  ]
  node [
    id 786
    label "rdze&#324;"
  ]
  node [
    id 787
    label "kielich"
  ]
  node [
    id 788
    label "ornamentyka"
  ]
  node [
    id 789
    label "pasmo"
  ]
  node [
    id 790
    label "g&#322;owa"
  ]
  node [
    id 791
    label "naczynie"
  ]
  node [
    id 792
    label "p&#322;at"
  ]
  node [
    id 793
    label "maszyna_drukarska"
  ]
  node [
    id 794
    label "style"
  ]
  node [
    id 795
    label "linearno&#347;&#263;"
  ]
  node [
    id 796
    label "spirala"
  ]
  node [
    id 797
    label "dyspozycja"
  ]
  node [
    id 798
    label "odmiana"
  ]
  node [
    id 799
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 800
    label "wz&#243;r"
  ]
  node [
    id 801
    label "October"
  ]
  node [
    id 802
    label "p&#281;tla"
  ]
  node [
    id 803
    label "arystotelizm"
  ]
  node [
    id 804
    label "szablon"
  ]
  node [
    id 805
    label "miniatura"
  ]
  node [
    id 806
    label "zesp&#243;&#322;"
  ]
  node [
    id 807
    label "podejrzany"
  ]
  node [
    id 808
    label "s&#261;downictwo"
  ]
  node [
    id 809
    label "biuro"
  ]
  node [
    id 810
    label "court"
  ]
  node [
    id 811
    label "forum"
  ]
  node [
    id 812
    label "bronienie"
  ]
  node [
    id 813
    label "urz&#261;d"
  ]
  node [
    id 814
    label "wydarzenie"
  ]
  node [
    id 815
    label "oskar&#380;yciel"
  ]
  node [
    id 816
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 817
    label "skazany"
  ]
  node [
    id 818
    label "post&#281;powanie"
  ]
  node [
    id 819
    label "broni&#263;"
  ]
  node [
    id 820
    label "my&#347;l"
  ]
  node [
    id 821
    label "pods&#261;dny"
  ]
  node [
    id 822
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 823
    label "obrona"
  ]
  node [
    id 824
    label "wypowied&#378;"
  ]
  node [
    id 825
    label "antylogizm"
  ]
  node [
    id 826
    label "konektyw"
  ]
  node [
    id 827
    label "&#347;wiadek"
  ]
  node [
    id 828
    label "procesowicz"
  ]
  node [
    id 829
    label "pochwytanie"
  ]
  node [
    id 830
    label "wording"
  ]
  node [
    id 831
    label "withdrawal"
  ]
  node [
    id 832
    label "capture"
  ]
  node [
    id 833
    label "podniesienie"
  ]
  node [
    id 834
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 835
    label "film"
  ]
  node [
    id 836
    label "zapisanie"
  ]
  node [
    id 837
    label "prezentacja"
  ]
  node [
    id 838
    label "rzucenie"
  ]
  node [
    id 839
    label "zamkni&#281;cie"
  ]
  node [
    id 840
    label "zabranie"
  ]
  node [
    id 841
    label "poinformowanie"
  ]
  node [
    id 842
    label "zaaresztowanie"
  ]
  node [
    id 843
    label "eastern_hemisphere"
  ]
  node [
    id 844
    label "kierunek"
  ]
  node [
    id 845
    label "kierowa&#263;"
  ]
  node [
    id 846
    label "inform"
  ]
  node [
    id 847
    label "marshal"
  ]
  node [
    id 848
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 849
    label "wyznacza&#263;"
  ]
  node [
    id 850
    label "pomaga&#263;"
  ]
  node [
    id 851
    label "tu&#322;&#243;w"
  ]
  node [
    id 852
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 853
    label "wielok&#261;t"
  ]
  node [
    id 854
    label "strzelba"
  ]
  node [
    id 855
    label "lufa"
  ]
  node [
    id 856
    label "&#347;ciana"
  ]
  node [
    id 857
    label "wyznaczenie"
  ]
  node [
    id 858
    label "przyczynienie_si&#281;"
  ]
  node [
    id 859
    label "zwr&#243;cenie"
  ]
  node [
    id 860
    label "zrozumienie"
  ]
  node [
    id 861
    label "po&#322;o&#380;enie"
  ]
  node [
    id 862
    label "seksualno&#347;&#263;"
  ]
  node [
    id 863
    label "wiedza"
  ]
  node [
    id 864
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 865
    label "zorientowanie_si&#281;"
  ]
  node [
    id 866
    label "pogubienie_si&#281;"
  ]
  node [
    id 867
    label "orientation"
  ]
  node [
    id 868
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 869
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 870
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 871
    label "gubienie_si&#281;"
  ]
  node [
    id 872
    label "wrench"
  ]
  node [
    id 873
    label "nawini&#281;cie"
  ]
  node [
    id 874
    label "os&#322;abienie"
  ]
  node [
    id 875
    label "uszkodzenie"
  ]
  node [
    id 876
    label "odbicie"
  ]
  node [
    id 877
    label "poskr&#281;canie"
  ]
  node [
    id 878
    label "uraz"
  ]
  node [
    id 879
    label "odchylenie_si&#281;"
  ]
  node [
    id 880
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 881
    label "z&#322;&#261;czenie"
  ]
  node [
    id 882
    label "splecenie"
  ]
  node [
    id 883
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 884
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 885
    label "sple&#347;&#263;"
  ]
  node [
    id 886
    label "os&#322;abi&#263;"
  ]
  node [
    id 887
    label "nawin&#261;&#263;"
  ]
  node [
    id 888
    label "scali&#263;"
  ]
  node [
    id 889
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 890
    label "twist"
  ]
  node [
    id 891
    label "splay"
  ]
  node [
    id 892
    label "uszkodzi&#263;"
  ]
  node [
    id 893
    label "break"
  ]
  node [
    id 894
    label "flex"
  ]
  node [
    id 895
    label "zaty&#322;"
  ]
  node [
    id 896
    label "pupa"
  ]
  node [
    id 897
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 898
    label "os&#322;abia&#263;"
  ]
  node [
    id 899
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 900
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 901
    label "splata&#263;"
  ]
  node [
    id 902
    label "throw"
  ]
  node [
    id 903
    label "screw"
  ]
  node [
    id 904
    label "scala&#263;"
  ]
  node [
    id 905
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 906
    label "przelezienie"
  ]
  node [
    id 907
    label "&#347;piew"
  ]
  node [
    id 908
    label "Synaj"
  ]
  node [
    id 909
    label "Kreml"
  ]
  node [
    id 910
    label "d&#378;wi&#281;k"
  ]
  node [
    id 911
    label "wysoki"
  ]
  node [
    id 912
    label "wzniesienie"
  ]
  node [
    id 913
    label "grupa"
  ]
  node [
    id 914
    label "Ropa"
  ]
  node [
    id 915
    label "kupa"
  ]
  node [
    id 916
    label "przele&#378;&#263;"
  ]
  node [
    id 917
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 918
    label "karczek"
  ]
  node [
    id 919
    label "rami&#261;czko"
  ]
  node [
    id 920
    label "Jaworze"
  ]
  node [
    id 921
    label "set"
  ]
  node [
    id 922
    label "orient"
  ]
  node [
    id 923
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 924
    label "aim"
  ]
  node [
    id 925
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 926
    label "wyznaczy&#263;"
  ]
  node [
    id 927
    label "pomaganie"
  ]
  node [
    id 928
    label "przyczynianie_si&#281;"
  ]
  node [
    id 929
    label "zwracanie"
  ]
  node [
    id 930
    label "rozeznawanie"
  ]
  node [
    id 931
    label "oznaczanie"
  ]
  node [
    id 932
    label "odchylanie_si&#281;"
  ]
  node [
    id 933
    label "kszta&#322;towanie"
  ]
  node [
    id 934
    label "os&#322;abianie"
  ]
  node [
    id 935
    label "uprz&#281;dzenie"
  ]
  node [
    id 936
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 937
    label "scalanie"
  ]
  node [
    id 938
    label "lewoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 939
    label "snucie"
  ]
  node [
    id 940
    label "prawoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 941
    label "tortuosity"
  ]
  node [
    id 942
    label "odbijanie"
  ]
  node [
    id 943
    label "contortion"
  ]
  node [
    id 944
    label "splatanie"
  ]
  node [
    id 945
    label "figura"
  ]
  node [
    id 946
    label "graficzny_interfejs_u&#380;ytkownika"
  ]
  node [
    id 947
    label "uk&#322;ad_graficzny"
  ]
  node [
    id 948
    label "uwierzytelnienie"
  ]
  node [
    id 949
    label "liczba"
  ]
  node [
    id 950
    label "circumference"
  ]
  node [
    id 951
    label "cyrkumferencja"
  ]
  node [
    id 952
    label "provider"
  ]
  node [
    id 953
    label "hipertekst"
  ]
  node [
    id 954
    label "cyberprzestrze&#324;"
  ]
  node [
    id 955
    label "mem"
  ]
  node [
    id 956
    label "gra_sieciowa"
  ]
  node [
    id 957
    label "grooming"
  ]
  node [
    id 958
    label "media"
  ]
  node [
    id 959
    label "biznes_elektroniczny"
  ]
  node [
    id 960
    label "sie&#263;_komputerowa"
  ]
  node [
    id 961
    label "punkt_dost&#281;pu"
  ]
  node [
    id 962
    label "us&#322;uga_internetowa"
  ]
  node [
    id 963
    label "netbook"
  ]
  node [
    id 964
    label "e-hazard"
  ]
  node [
    id 965
    label "podcast"
  ]
  node [
    id 966
    label "budynek"
  ]
  node [
    id 967
    label "faul"
  ]
  node [
    id 968
    label "wk&#322;ad"
  ]
  node [
    id 969
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 970
    label "s&#281;dzia"
  ]
  node [
    id 971
    label "bon"
  ]
  node [
    id 972
    label "ticket"
  ]
  node [
    id 973
    label "arkusz"
  ]
  node [
    id 974
    label "kartonik"
  ]
  node [
    id 975
    label "kara"
  ]
  node [
    id 976
    label "pagination"
  ]
  node [
    id 977
    label "numer"
  ]
  node [
    id 978
    label "mechanizm_mno&#380;nikowy"
  ]
  node [
    id 979
    label "proces_ekonomiczny"
  ]
  node [
    id 980
    label "cena_r&#243;wnowagi_rynkowej"
  ]
  node [
    id 981
    label "krzywa_popytu"
  ]
  node [
    id 982
    label "handel"
  ]
  node [
    id 983
    label "nawis_inflacyjny"
  ]
  node [
    id 984
    label "elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 985
    label "popyt_zagregowany"
  ]
  node [
    id 986
    label "lepko&#347;&#263;_cen"
  ]
  node [
    id 987
    label "czynnik_niecenowy"
  ]
  node [
    id 988
    label "business"
  ]
  node [
    id 989
    label "dzia&#322;alno&#347;&#263;_gospodarcza"
  ]
  node [
    id 990
    label "komercja"
  ]
  node [
    id 991
    label "pryncypa&#322;"
  ]
  node [
    id 992
    label "kierownictwo"
  ]
  node [
    id 993
    label "ludzko&#347;&#263;"
  ]
  node [
    id 994
    label "asymilowanie"
  ]
  node [
    id 995
    label "wapniak"
  ]
  node [
    id 996
    label "asymilowa&#263;"
  ]
  node [
    id 997
    label "hominid"
  ]
  node [
    id 998
    label "podw&#322;adny"
  ]
  node [
    id 999
    label "portrecista"
  ]
  node [
    id 1000
    label "dwun&#243;g"
  ]
  node [
    id 1001
    label "profanum"
  ]
  node [
    id 1002
    label "nasada"
  ]
  node [
    id 1003
    label "duch"
  ]
  node [
    id 1004
    label "antropochoria"
  ]
  node [
    id 1005
    label "osoba"
  ]
  node [
    id 1006
    label "senior"
  ]
  node [
    id 1007
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1008
    label "Adam"
  ]
  node [
    id 1009
    label "homo_sapiens"
  ]
  node [
    id 1010
    label "polifag"
  ]
  node [
    id 1011
    label "lead"
  ]
  node [
    id 1012
    label "siedziba"
  ]
  node [
    id 1013
    label "w&#322;adza"
  ]
  node [
    id 1014
    label "g&#322;os"
  ]
  node [
    id 1015
    label "zwierzchnik"
  ]
  node [
    id 1016
    label "sterowa&#263;"
  ]
  node [
    id 1017
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1018
    label "manipulate"
  ]
  node [
    id 1019
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1020
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1021
    label "ustawia&#263;"
  ]
  node [
    id 1022
    label "give"
  ]
  node [
    id 1023
    label "przeznacza&#263;"
  ]
  node [
    id 1024
    label "control"
  ]
  node [
    id 1025
    label "match"
  ]
  node [
    id 1026
    label "motywowa&#263;"
  ]
  node [
    id 1027
    label "administrowa&#263;"
  ]
  node [
    id 1028
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 1029
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1030
    label "order"
  ]
  node [
    id 1031
    label "indicate"
  ]
  node [
    id 1032
    label "falochron"
  ]
  node [
    id 1033
    label "akwatorium"
  ]
  node [
    id 1034
    label "morze"
  ]
  node [
    id 1035
    label "zbiornik_wodny"
  ]
  node [
    id 1036
    label "nabrze&#380;e"
  ]
  node [
    id 1037
    label "budowla_hydrotechniczna"
  ]
  node [
    id 1038
    label "przymorze"
  ]
  node [
    id 1039
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 1040
    label "bezmiar"
  ]
  node [
    id 1041
    label "pe&#322;ne_morze"
  ]
  node [
    id 1042
    label "latarnia_morska"
  ]
  node [
    id 1043
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1044
    label "nereida"
  ]
  node [
    id 1045
    label "okeanida"
  ]
  node [
    id 1046
    label "marina"
  ]
  node [
    id 1047
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 1048
    label "Morze_Czerwone"
  ]
  node [
    id 1049
    label "talasoterapia"
  ]
  node [
    id 1050
    label "Morze_Bia&#322;e"
  ]
  node [
    id 1051
    label "paliszcze"
  ]
  node [
    id 1052
    label "Neptun"
  ]
  node [
    id 1053
    label "Morze_Czarne"
  ]
  node [
    id 1054
    label "laguna"
  ]
  node [
    id 1055
    label "Morze_Egejskie"
  ]
  node [
    id 1056
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 1057
    label "Ziemia"
  ]
  node [
    id 1058
    label "Morze_Adriatyckie"
  ]
  node [
    id 1059
    label "postrzega&#263;"
  ]
  node [
    id 1060
    label "perceive"
  ]
  node [
    id 1061
    label "aprobowa&#263;"
  ]
  node [
    id 1062
    label "wzrok"
  ]
  node [
    id 1063
    label "zmale&#263;"
  ]
  node [
    id 1064
    label "male&#263;"
  ]
  node [
    id 1065
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 1066
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 1067
    label "spotka&#263;"
  ]
  node [
    id 1068
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 1069
    label "ogl&#261;da&#263;"
  ]
  node [
    id 1070
    label "dostrzega&#263;"
  ]
  node [
    id 1071
    label "spowodowa&#263;"
  ]
  node [
    id 1072
    label "notice"
  ]
  node [
    id 1073
    label "go_steady"
  ]
  node [
    id 1074
    label "reagowa&#263;"
  ]
  node [
    id 1075
    label "os&#261;dza&#263;"
  ]
  node [
    id 1076
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 1077
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 1078
    label "react"
  ]
  node [
    id 1079
    label "answer"
  ]
  node [
    id 1080
    label "odpowiada&#263;"
  ]
  node [
    id 1081
    label "uczestniczy&#263;"
  ]
  node [
    id 1082
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 1083
    label "obacza&#263;"
  ]
  node [
    id 1084
    label "dochodzi&#263;"
  ]
  node [
    id 1085
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 1086
    label "doj&#347;&#263;"
  ]
  node [
    id 1087
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 1088
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 1089
    label "styka&#263;_si&#281;"
  ]
  node [
    id 1090
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1091
    label "insert"
  ]
  node [
    id 1092
    label "visualize"
  ]
  node [
    id 1093
    label "pozna&#263;"
  ]
  node [
    id 1094
    label "befall"
  ]
  node [
    id 1095
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 1096
    label "znale&#378;&#263;"
  ]
  node [
    id 1097
    label "orientowa&#263;_si&#281;"
  ]
  node [
    id 1098
    label "approbate"
  ]
  node [
    id 1099
    label "uznawa&#263;"
  ]
  node [
    id 1100
    label "act"
  ]
  node [
    id 1101
    label "strike"
  ]
  node [
    id 1102
    label "robi&#263;"
  ]
  node [
    id 1103
    label "s&#261;dzi&#263;"
  ]
  node [
    id 1104
    label "powodowa&#263;"
  ]
  node [
    id 1105
    label "znajdowa&#263;"
  ]
  node [
    id 1106
    label "hold"
  ]
  node [
    id 1107
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 1108
    label "nagradza&#263;"
  ]
  node [
    id 1109
    label "forytowa&#263;"
  ]
  node [
    id 1110
    label "traktowa&#263;"
  ]
  node [
    id 1111
    label "sign"
  ]
  node [
    id 1112
    label "m&#281;tnienie"
  ]
  node [
    id 1113
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 1114
    label "widzenie"
  ]
  node [
    id 1115
    label "okulista"
  ]
  node [
    id 1116
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 1117
    label "zmys&#322;"
  ]
  node [
    id 1118
    label "expression"
  ]
  node [
    id 1119
    label "oko"
  ]
  node [
    id 1120
    label "m&#281;tnie&#263;"
  ]
  node [
    id 1121
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1122
    label "reduce"
  ]
  node [
    id 1123
    label "zmniejszy&#263;_si&#281;"
  ]
  node [
    id 1124
    label "worsen"
  ]
  node [
    id 1125
    label "slack"
  ]
  node [
    id 1126
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 1127
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 1128
    label "relax"
  ]
  node [
    id 1129
    label "zmniejsza&#263;_si&#281;"
  ]
  node [
    id 1130
    label "jedyny"
  ]
  node [
    id 1131
    label "du&#380;y"
  ]
  node [
    id 1132
    label "zdr&#243;w"
  ]
  node [
    id 1133
    label "calu&#347;ko"
  ]
  node [
    id 1134
    label "&#380;ywy"
  ]
  node [
    id 1135
    label "podobny"
  ]
  node [
    id 1136
    label "ca&#322;o"
  ]
  node [
    id 1137
    label "zaczarowanie"
  ]
  node [
    id 1138
    label "zam&#243;wi&#263;"
  ]
  node [
    id 1139
    label "indent"
  ]
  node [
    id 1140
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 1141
    label "zlecenie"
  ]
  node [
    id 1142
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 1143
    label "polecenie"
  ]
  node [
    id 1144
    label "zamawia&#263;"
  ]
  node [
    id 1145
    label "rozdysponowanie"
  ]
  node [
    id 1146
    label "perpetration"
  ]
  node [
    id 1147
    label "zamawianie"
  ]
  node [
    id 1148
    label "transakcja"
  ]
  node [
    id 1149
    label "zg&#322;oszenie"
  ]
  node [
    id 1150
    label "zarezerwowanie"
  ]
  node [
    id 1151
    label "pismo"
  ]
  node [
    id 1152
    label "submission"
  ]
  node [
    id 1153
    label "przed&#322;o&#380;enie"
  ]
  node [
    id 1154
    label "zawiadomienie"
  ]
  node [
    id 1155
    label "zameldowanie"
  ]
  node [
    id 1156
    label "announcement"
  ]
  node [
    id 1157
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1158
    label "captivation"
  ]
  node [
    id 1159
    label "odprawienie"
  ]
  node [
    id 1160
    label "restriction"
  ]
  node [
    id 1161
    label "wym&#243;wienie"
  ]
  node [
    id 1162
    label "reservation"
  ]
  node [
    id 1163
    label "zapewnienie"
  ]
  node [
    id 1164
    label "spadni&#281;cie"
  ]
  node [
    id 1165
    label "undertaking"
  ]
  node [
    id 1166
    label "odebra&#263;"
  ]
  node [
    id 1167
    label "odebranie"
  ]
  node [
    id 1168
    label "zbiegni&#281;cie"
  ]
  node [
    id 1169
    label "odbieranie"
  ]
  node [
    id 1170
    label "odbiera&#263;"
  ]
  node [
    id 1171
    label "decree"
  ]
  node [
    id 1172
    label "ukaz"
  ]
  node [
    id 1173
    label "pognanie"
  ]
  node [
    id 1174
    label "rekomendacja"
  ]
  node [
    id 1175
    label "pobiegni&#281;cie"
  ]
  node [
    id 1176
    label "education"
  ]
  node [
    id 1177
    label "doradzenie"
  ]
  node [
    id 1178
    label "statement"
  ]
  node [
    id 1179
    label "recommendation"
  ]
  node [
    id 1180
    label "zadanie"
  ]
  node [
    id 1181
    label "zaordynowanie"
  ]
  node [
    id 1182
    label "powierzenie"
  ]
  node [
    id 1183
    label "przesadzenie"
  ]
  node [
    id 1184
    label "consign"
  ]
  node [
    id 1185
    label "engagement"
  ]
  node [
    id 1186
    label "umawianie_si&#281;"
  ]
  node [
    id 1187
    label "czarowanie"
  ]
  node [
    id 1188
    label "zlecanie"
  ]
  node [
    id 1189
    label "zg&#322;aszanie"
  ]
  node [
    id 1190
    label "szeptun"
  ]
  node [
    id 1191
    label "polecanie"
  ]
  node [
    id 1192
    label "szeptucha"
  ]
  node [
    id 1193
    label "rezerwowanie"
  ]
  node [
    id 1194
    label "poleci&#263;"
  ]
  node [
    id 1195
    label "zaczarowa&#263;"
  ]
  node [
    id 1196
    label "indenture"
  ]
  node [
    id 1197
    label "zg&#322;osi&#263;"
  ]
  node [
    id 1198
    label "appoint"
  ]
  node [
    id 1199
    label "bespeak"
  ]
  node [
    id 1200
    label "zarezerwowa&#263;"
  ]
  node [
    id 1201
    label "zleci&#263;"
  ]
  node [
    id 1202
    label "um&#243;wi&#263;_si&#281;"
  ]
  node [
    id 1203
    label "rezerwowa&#263;"
  ]
  node [
    id 1204
    label "poleca&#263;"
  ]
  node [
    id 1205
    label "zleca&#263;"
  ]
  node [
    id 1206
    label "umawia&#263;_si&#281;"
  ]
  node [
    id 1207
    label "zg&#322;asza&#263;"
  ]
  node [
    id 1208
    label "czarowa&#263;"
  ]
  node [
    id 1209
    label "odznaka"
  ]
  node [
    id 1210
    label "kawaler"
  ]
  node [
    id 1211
    label "rozdzielenie"
  ]
  node [
    id 1212
    label "arbitra&#380;"
  ]
  node [
    id 1213
    label "cena_transferowa"
  ]
  node [
    id 1214
    label "kontrakt_terminowy"
  ]
  node [
    id 1215
    label "facjenda"
  ]
  node [
    id 1216
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 1217
    label "krzew"
  ]
  node [
    id 1218
    label "delfinidyna"
  ]
  node [
    id 1219
    label "pi&#380;maczkowate"
  ]
  node [
    id 1220
    label "ki&#347;&#263;"
  ]
  node [
    id 1221
    label "hy&#263;ka"
  ]
  node [
    id 1222
    label "pestkowiec"
  ]
  node [
    id 1223
    label "kwiat"
  ]
  node [
    id 1224
    label "owoc"
  ]
  node [
    id 1225
    label "oliwkowate"
  ]
  node [
    id 1226
    label "lilac"
  ]
  node [
    id 1227
    label "kostka"
  ]
  node [
    id 1228
    label "kita"
  ]
  node [
    id 1229
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 1230
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 1231
    label "d&#322;o&#324;"
  ]
  node [
    id 1232
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 1233
    label "powerball"
  ]
  node [
    id 1234
    label "&#380;ubr"
  ]
  node [
    id 1235
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 1236
    label "p&#281;k"
  ]
  node [
    id 1237
    label "r&#281;ka"
  ]
  node [
    id 1238
    label "ogon"
  ]
  node [
    id 1239
    label "zako&#324;czenie"
  ]
  node [
    id 1240
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 1241
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 1242
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 1243
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 1244
    label "flakon"
  ]
  node [
    id 1245
    label "przykoronek"
  ]
  node [
    id 1246
    label "dno_kwiatowe"
  ]
  node [
    id 1247
    label "organ_ro&#347;linny"
  ]
  node [
    id 1248
    label "warga"
  ]
  node [
    id 1249
    label "korona"
  ]
  node [
    id 1250
    label "rurka"
  ]
  node [
    id 1251
    label "&#322;yko"
  ]
  node [
    id 1252
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 1253
    label "karczowa&#263;"
  ]
  node [
    id 1254
    label "wykarczowanie"
  ]
  node [
    id 1255
    label "skupina"
  ]
  node [
    id 1256
    label "wykarczowa&#263;"
  ]
  node [
    id 1257
    label "karczowanie"
  ]
  node [
    id 1258
    label "fanerofit"
  ]
  node [
    id 1259
    label "zbiorowisko"
  ]
  node [
    id 1260
    label "ro&#347;liny"
  ]
  node [
    id 1261
    label "p&#281;d"
  ]
  node [
    id 1262
    label "wegetowanie"
  ]
  node [
    id 1263
    label "zadziorek"
  ]
  node [
    id 1264
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1265
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1266
    label "do&#322;owa&#263;"
  ]
  node [
    id 1267
    label "wegetacja"
  ]
  node [
    id 1268
    label "strzyc"
  ]
  node [
    id 1269
    label "w&#322;&#243;kno"
  ]
  node [
    id 1270
    label "g&#322;uszenie"
  ]
  node [
    id 1271
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1272
    label "fitotron"
  ]
  node [
    id 1273
    label "bulwka"
  ]
  node [
    id 1274
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1275
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1276
    label "epiderma"
  ]
  node [
    id 1277
    label "gumoza"
  ]
  node [
    id 1278
    label "strzy&#380;enie"
  ]
  node [
    id 1279
    label "wypotnik"
  ]
  node [
    id 1280
    label "flawonoid"
  ]
  node [
    id 1281
    label "wyro&#347;le"
  ]
  node [
    id 1282
    label "do&#322;owanie"
  ]
  node [
    id 1283
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1284
    label "pora&#380;a&#263;"
  ]
  node [
    id 1285
    label "fitocenoza"
  ]
  node [
    id 1286
    label "fotoautotrof"
  ]
  node [
    id 1287
    label "nieuleczalnie_chory"
  ]
  node [
    id 1288
    label "wegetowa&#263;"
  ]
  node [
    id 1289
    label "pochewka"
  ]
  node [
    id 1290
    label "sok"
  ]
  node [
    id 1291
    label "system_korzeniowy"
  ]
  node [
    id 1292
    label "zawi&#261;zek"
  ]
  node [
    id 1293
    label "pestka"
  ]
  node [
    id 1294
    label "mi&#261;&#380;sz"
  ]
  node [
    id 1295
    label "frukt"
  ]
  node [
    id 1296
    label "drylowanie"
  ]
  node [
    id 1297
    label "produkt"
  ]
  node [
    id 1298
    label "owocnia"
  ]
  node [
    id 1299
    label "fruktoza"
  ]
  node [
    id 1300
    label "gniazdo_nasienne"
  ]
  node [
    id 1301
    label "glukoza"
  ]
  node [
    id 1302
    label "antocyjanidyn"
  ]
  node [
    id 1303
    label "szczeciowce"
  ]
  node [
    id 1304
    label "jasnotowce"
  ]
  node [
    id 1305
    label "Oleaceae"
  ]
  node [
    id 1306
    label "wielkopolski"
  ]
  node [
    id 1307
    label "bez_czarny"
  ]
  node [
    id 1308
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 1309
    label "rozliczenie"
  ]
  node [
    id 1310
    label "Hata"
  ]
  node [
    id 1311
    label "Jim"
  ]
  node [
    id 1312
    label "Whitehurst"
  ]
  node [
    id 1313
    label "Andrew"
  ]
  node [
    id 1314
    label "Keen"
  ]
  node [
    id 1315
    label "weba"
  ]
  node [
    id 1316
    label "2"
  ]
  node [
    id 1317
    label "0"
  ]
  node [
    id 1318
    label "Hat"
  ]
  node [
    id 1319
    label "bill"
  ]
  node [
    id 1320
    label "Gates"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 26
  ]
  edge [
    source 12
    target 27
  ]
  edge [
    source 12
    target 264
  ]
  edge [
    source 12
    target 265
  ]
  edge [
    source 12
    target 266
  ]
  edge [
    source 12
    target 267
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 270
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 274
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 276
  ]
  edge [
    source 12
    target 277
  ]
  edge [
    source 12
    target 278
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 280
  ]
  edge [
    source 12
    target 281
  ]
  edge [
    source 12
    target 282
  ]
  edge [
    source 12
    target 283
  ]
  edge [
    source 12
    target 284
  ]
  edge [
    source 12
    target 285
  ]
  edge [
    source 12
    target 286
  ]
  edge [
    source 12
    target 287
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 290
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 305
  ]
  edge [
    source 12
    target 251
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 12
    target 309
  ]
  edge [
    source 12
    target 310
  ]
  edge [
    source 12
    target 311
  ]
  edge [
    source 12
    target 312
  ]
  edge [
    source 12
    target 313
  ]
  edge [
    source 12
    target 314
  ]
  edge [
    source 12
    target 315
  ]
  edge [
    source 12
    target 316
  ]
  edge [
    source 12
    target 317
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 319
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 321
  ]
  edge [
    source 12
    target 322
  ]
  edge [
    source 12
    target 323
  ]
  edge [
    source 12
    target 324
  ]
  edge [
    source 12
    target 325
  ]
  edge [
    source 12
    target 326
  ]
  edge [
    source 12
    target 327
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 334
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 336
  ]
  edge [
    source 13
    target 337
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 339
  ]
  edge [
    source 13
    target 340
  ]
  edge [
    source 13
    target 341
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 13
    target 343
  ]
  edge [
    source 13
    target 344
  ]
  edge [
    source 13
    target 345
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 352
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 355
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 179
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 391
  ]
  edge [
    source 14
    target 392
  ]
  edge [
    source 14
    target 393
  ]
  edge [
    source 14
    target 394
  ]
  edge [
    source 14
    target 271
  ]
  edge [
    source 14
    target 395
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 48
  ]
  edge [
    source 14
    target 396
  ]
  edge [
    source 14
    target 397
  ]
  edge [
    source 14
    target 398
  ]
  edge [
    source 14
    target 399
  ]
  edge [
    source 14
    target 400
  ]
  edge [
    source 14
    target 401
  ]
  edge [
    source 14
    target 402
  ]
  edge [
    source 14
    target 403
  ]
  edge [
    source 14
    target 404
  ]
  edge [
    source 14
    target 405
  ]
  edge [
    source 14
    target 406
  ]
  edge [
    source 14
    target 407
  ]
  edge [
    source 14
    target 408
  ]
  edge [
    source 14
    target 409
  ]
  edge [
    source 14
    target 410
  ]
  edge [
    source 14
    target 411
  ]
  edge [
    source 14
    target 412
  ]
  edge [
    source 14
    target 413
  ]
  edge [
    source 14
    target 414
  ]
  edge [
    source 14
    target 415
  ]
  edge [
    source 14
    target 416
  ]
  edge [
    source 14
    target 417
  ]
  edge [
    source 14
    target 418
  ]
  edge [
    source 14
    target 419
  ]
  edge [
    source 14
    target 420
  ]
  edge [
    source 14
    target 421
  ]
  edge [
    source 14
    target 422
  ]
  edge [
    source 14
    target 243
  ]
  edge [
    source 14
    target 423
  ]
  edge [
    source 14
    target 424
  ]
  edge [
    source 14
    target 425
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 426
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 210
  ]
  edge [
    source 14
    target 211
  ]
  edge [
    source 14
    target 212
  ]
  edge [
    source 14
    target 213
  ]
  edge [
    source 14
    target 214
  ]
  edge [
    source 14
    target 215
  ]
  edge [
    source 14
    target 216
  ]
  edge [
    source 14
    target 217
  ]
  edge [
    source 14
    target 427
  ]
  edge [
    source 14
    target 428
  ]
  edge [
    source 14
    target 429
  ]
  edge [
    source 14
    target 430
  ]
  edge [
    source 14
    target 431
  ]
  edge [
    source 14
    target 432
  ]
  edge [
    source 14
    target 433
  ]
  edge [
    source 14
    target 434
  ]
  edge [
    source 14
    target 435
  ]
  edge [
    source 14
    target 436
  ]
  edge [
    source 14
    target 437
  ]
  edge [
    source 14
    target 438
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 266
  ]
  edge [
    source 14
    target 456
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 458
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 228
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 467
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 469
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 471
  ]
  edge [
    source 14
    target 472
  ]
  edge [
    source 14
    target 473
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 57
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 239
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 267
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 502
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 505
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 14
    target 507
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 509
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 252
  ]
  edge [
    source 14
    target 511
  ]
  edge [
    source 14
    target 220
  ]
  edge [
    source 14
    target 512
  ]
  edge [
    source 14
    target 513
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 522
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 14
    target 529
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 531
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 43
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 567
  ]
  edge [
    source 14
    target 568
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 570
  ]
  edge [
    source 14
    target 571
  ]
  edge [
    source 14
    target 572
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 574
  ]
  edge [
    source 14
    target 575
  ]
  edge [
    source 14
    target 576
  ]
  edge [
    source 14
    target 577
  ]
  edge [
    source 14
    target 578
  ]
  edge [
    source 14
    target 579
  ]
  edge [
    source 14
    target 580
  ]
  edge [
    source 14
    target 581
  ]
  edge [
    source 14
    target 582
  ]
  edge [
    source 14
    target 583
  ]
  edge [
    source 14
    target 584
  ]
  edge [
    source 14
    target 585
  ]
  edge [
    source 14
    target 586
  ]
  edge [
    source 14
    target 587
  ]
  edge [
    source 14
    target 380
  ]
  edge [
    source 14
    target 588
  ]
  edge [
    source 14
    target 589
  ]
  edge [
    source 14
    target 590
  ]
  edge [
    source 14
    target 591
  ]
  edge [
    source 14
    target 592
  ]
  edge [
    source 14
    target 593
  ]
  edge [
    source 14
    target 594
  ]
  edge [
    source 14
    target 595
  ]
  edge [
    source 14
    target 596
  ]
  edge [
    source 14
    target 597
  ]
  edge [
    source 14
    target 598
  ]
  edge [
    source 14
    target 599
  ]
  edge [
    source 14
    target 600
  ]
  edge [
    source 14
    target 601
  ]
  edge [
    source 14
    target 602
  ]
  edge [
    source 14
    target 603
  ]
  edge [
    source 14
    target 604
  ]
  edge [
    source 14
    target 605
  ]
  edge [
    source 14
    target 606
  ]
  edge [
    source 14
    target 607
  ]
  edge [
    source 14
    target 608
  ]
  edge [
    source 14
    target 609
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 611
  ]
  edge [
    source 14
    target 612
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 615
  ]
  edge [
    source 14
    target 616
  ]
  edge [
    source 14
    target 617
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 30
  ]
  edge [
    source 14
    target 619
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 276
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 238
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 650
  ]
  edge [
    source 17
    target 651
  ]
  edge [
    source 17
    target 652
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 653
  ]
  edge [
    source 17
    target 654
  ]
  edge [
    source 17
    target 655
  ]
  edge [
    source 17
    target 656
  ]
  edge [
    source 17
    target 657
  ]
  edge [
    source 17
    target 658
  ]
  edge [
    source 17
    target 659
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 661
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 664
  ]
  edge [
    source 17
    target 665
  ]
  edge [
    source 17
    target 45
  ]
  edge [
    source 17
    target 666
  ]
  edge [
    source 17
    target 667
  ]
  edge [
    source 17
    target 594
  ]
  edge [
    source 17
    target 668
  ]
  edge [
    source 17
    target 669
  ]
  edge [
    source 17
    target 670
  ]
  edge [
    source 17
    target 671
  ]
  edge [
    source 17
    target 672
  ]
  edge [
    source 17
    target 673
  ]
  edge [
    source 17
    target 674
  ]
  edge [
    source 17
    target 675
  ]
  edge [
    source 17
    target 676
  ]
  edge [
    source 17
    target 677
  ]
  edge [
    source 17
    target 49
  ]
  edge [
    source 17
    target 678
  ]
  edge [
    source 17
    target 679
  ]
  edge [
    source 17
    target 680
  ]
  edge [
    source 17
    target 681
  ]
  edge [
    source 17
    target 57
  ]
  edge [
    source 17
    target 682
  ]
  edge [
    source 17
    target 683
  ]
  edge [
    source 17
    target 684
  ]
  edge [
    source 17
    target 685
  ]
  edge [
    source 17
    target 686
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 461
  ]
  edge [
    source 17
    target 687
  ]
  edge [
    source 17
    target 688
  ]
  edge [
    source 17
    target 48
  ]
  edge [
    source 17
    target 689
  ]
  edge [
    source 17
    target 690
  ]
  edge [
    source 17
    target 691
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 692
  ]
  edge [
    source 17
    target 693
  ]
  edge [
    source 17
    target 694
  ]
  edge [
    source 17
    target 695
  ]
  edge [
    source 17
    target 696
  ]
  edge [
    source 17
    target 697
  ]
  edge [
    source 17
    target 698
  ]
  edge [
    source 17
    target 699
  ]
  edge [
    source 17
    target 700
  ]
  edge [
    source 17
    target 701
  ]
  edge [
    source 17
    target 702
  ]
  edge [
    source 17
    target 512
  ]
  edge [
    source 17
    target 703
  ]
  edge [
    source 17
    target 704
  ]
  edge [
    source 17
    target 705
  ]
  edge [
    source 17
    target 706
  ]
  edge [
    source 17
    target 707
  ]
  edge [
    source 17
    target 708
  ]
  edge [
    source 17
    target 709
  ]
  edge [
    source 17
    target 710
  ]
  edge [
    source 17
    target 711
  ]
  edge [
    source 17
    target 712
  ]
  edge [
    source 17
    target 713
  ]
  edge [
    source 17
    target 714
  ]
  edge [
    source 17
    target 715
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 717
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 266
  ]
  edge [
    source 17
    target 719
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 722
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 438
  ]
  edge [
    source 17
    target 724
  ]
  edge [
    source 17
    target 725
  ]
  edge [
    source 17
    target 726
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 728
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 458
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 51
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 630
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 306
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 280
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 607
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 34
  ]
  edge [
    source 17
    target 766
  ]
  edge [
    source 17
    target 276
  ]
  edge [
    source 17
    target 767
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 770
  ]
  edge [
    source 17
    target 590
  ]
  edge [
    source 17
    target 771
  ]
  edge [
    source 17
    target 772
  ]
  edge [
    source 17
    target 773
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 776
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 406
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 260
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 524
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 61
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 529
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 564
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 494
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 568
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 35
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 17
    target 866
  ]
  edge [
    source 17
    target 867
  ]
  edge [
    source 17
    target 868
  ]
  edge [
    source 17
    target 869
  ]
  edge [
    source 17
    target 870
  ]
  edge [
    source 17
    target 871
  ]
  edge [
    source 17
    target 252
  ]
  edge [
    source 17
    target 872
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 253
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 894
  ]
  edge [
    source 17
    target 43
  ]
  edge [
    source 17
    target 895
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 17
    target 47
  ]
  edge [
    source 17
    target 897
  ]
  edge [
    source 17
    target 898
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 900
  ]
  edge [
    source 17
    target 901
  ]
  edge [
    source 17
    target 902
  ]
  edge [
    source 17
    target 903
  ]
  edge [
    source 17
    target 82
  ]
  edge [
    source 17
    target 904
  ]
  edge [
    source 17
    target 905
  ]
  edge [
    source 17
    target 394
  ]
  edge [
    source 17
    target 906
  ]
  edge [
    source 17
    target 907
  ]
  edge [
    source 17
    target 908
  ]
  edge [
    source 17
    target 909
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 911
  ]
  edge [
    source 17
    target 912
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 472
  ]
  edge [
    source 17
    target 914
  ]
  edge [
    source 17
    target 915
  ]
  edge [
    source 17
    target 916
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 918
  ]
  edge [
    source 17
    target 919
  ]
  edge [
    source 17
    target 920
  ]
  edge [
    source 17
    target 921
  ]
  edge [
    source 17
    target 922
  ]
  edge [
    source 17
    target 923
  ]
  edge [
    source 17
    target 924
  ]
  edge [
    source 17
    target 925
  ]
  edge [
    source 17
    target 926
  ]
  edge [
    source 17
    target 927
  ]
  edge [
    source 17
    target 928
  ]
  edge [
    source 17
    target 929
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 30
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 422
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 17
    target 616
  ]
  edge [
    source 17
    target 265
  ]
  edge [
    source 17
    target 400
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 968
  ]
  edge [
    source 17
    target 969
  ]
  edge [
    source 17
    target 970
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 972
  ]
  edge [
    source 17
    target 973
  ]
  edge [
    source 17
    target 974
  ]
  edge [
    source 17
    target 975
  ]
  edge [
    source 17
    target 976
  ]
  edge [
    source 17
    target 274
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 845
  ]
  edge [
    source 19
    target 57
  ]
  edge [
    source 19
    target 235
  ]
  edge [
    source 19
    target 251
  ]
  edge [
    source 19
    target 252
  ]
  edge [
    source 19
    target 253
  ]
  edge [
    source 19
    target 254
  ]
  edge [
    source 19
    target 255
  ]
  edge [
    source 19
    target 256
  ]
  edge [
    source 19
    target 257
  ]
  edge [
    source 19
    target 258
  ]
  edge [
    source 19
    target 259
  ]
  edge [
    source 19
    target 246
  ]
  edge [
    source 19
    target 260
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 898
  ]
  edge [
    source 19
    target 657
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 934
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 19
    target 945
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 540
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 800
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 809
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 806
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 50
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 19
    target 1026
  ]
  edge [
    source 19
    target 1027
  ]
  edge [
    source 19
    target 1028
  ]
  edge [
    source 19
    target 1029
  ]
  edge [
    source 19
    target 1030
  ]
  edge [
    source 19
    target 1031
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1032
  ]
  edge [
    source 20
    target 1033
  ]
  edge [
    source 20
    target 1034
  ]
  edge [
    source 20
    target 1035
  ]
  edge [
    source 20
    target 766
  ]
  edge [
    source 20
    target 646
  ]
  edge [
    source 20
    target 1036
  ]
  edge [
    source 20
    target 1037
  ]
  edge [
    source 20
    target 1038
  ]
  edge [
    source 20
    target 1039
  ]
  edge [
    source 20
    target 1040
  ]
  edge [
    source 20
    target 1041
  ]
  edge [
    source 20
    target 1042
  ]
  edge [
    source 20
    target 1043
  ]
  edge [
    source 20
    target 1044
  ]
  edge [
    source 20
    target 1045
  ]
  edge [
    source 20
    target 1046
  ]
  edge [
    source 20
    target 1047
  ]
  edge [
    source 20
    target 1048
  ]
  edge [
    source 20
    target 1049
  ]
  edge [
    source 20
    target 1050
  ]
  edge [
    source 20
    target 1051
  ]
  edge [
    source 20
    target 1052
  ]
  edge [
    source 20
    target 1053
  ]
  edge [
    source 20
    target 1054
  ]
  edge [
    source 20
    target 1055
  ]
  edge [
    source 20
    target 1056
  ]
  edge [
    source 20
    target 1057
  ]
  edge [
    source 20
    target 1058
  ]
  edge [
    source 20
    target 1310
  ]
  edge [
    source 20
    target 1318
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1059
  ]
  edge [
    source 24
    target 1060
  ]
  edge [
    source 24
    target 1061
  ]
  edge [
    source 24
    target 1062
  ]
  edge [
    source 24
    target 1063
  ]
  edge [
    source 24
    target 696
  ]
  edge [
    source 24
    target 1064
  ]
  edge [
    source 24
    target 1065
  ]
  edge [
    source 24
    target 1066
  ]
  edge [
    source 24
    target 1067
  ]
  edge [
    source 24
    target 1068
  ]
  edge [
    source 24
    target 1069
  ]
  edge [
    source 24
    target 1070
  ]
  edge [
    source 24
    target 1071
  ]
  edge [
    source 24
    target 1072
  ]
  edge [
    source 24
    target 1073
  ]
  edge [
    source 24
    target 1074
  ]
  edge [
    source 24
    target 1075
  ]
  edge [
    source 24
    target 1076
  ]
  edge [
    source 24
    target 1077
  ]
  edge [
    source 24
    target 1078
  ]
  edge [
    source 24
    target 1079
  ]
  edge [
    source 24
    target 1080
  ]
  edge [
    source 24
    target 1081
  ]
  edge [
    source 24
    target 1082
  ]
  edge [
    source 24
    target 1083
  ]
  edge [
    source 24
    target 1084
  ]
  edge [
    source 24
    target 1085
  ]
  edge [
    source 24
    target 1086
  ]
  edge [
    source 24
    target 1087
  ]
  edge [
    source 24
    target 1088
  ]
  edge [
    source 24
    target 1089
  ]
  edge [
    source 24
    target 1090
  ]
  edge [
    source 24
    target 1091
  ]
  edge [
    source 24
    target 1092
  ]
  edge [
    source 24
    target 1093
  ]
  edge [
    source 24
    target 1094
  ]
  edge [
    source 24
    target 1095
  ]
  edge [
    source 24
    target 1096
  ]
  edge [
    source 24
    target 1097
  ]
  edge [
    source 24
    target 1098
  ]
  edge [
    source 24
    target 1099
  ]
  edge [
    source 24
    target 1100
  ]
  edge [
    source 24
    target 1101
  ]
  edge [
    source 24
    target 1102
  ]
  edge [
    source 24
    target 1103
  ]
  edge [
    source 24
    target 1104
  ]
  edge [
    source 24
    target 1105
  ]
  edge [
    source 24
    target 1106
  ]
  edge [
    source 24
    target 1107
  ]
  edge [
    source 24
    target 1108
  ]
  edge [
    source 24
    target 1109
  ]
  edge [
    source 24
    target 1110
  ]
  edge [
    source 24
    target 1111
  ]
  edge [
    source 24
    target 1112
  ]
  edge [
    source 24
    target 1113
  ]
  edge [
    source 24
    target 1114
  ]
  edge [
    source 24
    target 1115
  ]
  edge [
    source 24
    target 1116
  ]
  edge [
    source 24
    target 1117
  ]
  edge [
    source 24
    target 1118
  ]
  edge [
    source 24
    target 1119
  ]
  edge [
    source 24
    target 1120
  ]
  edge [
    source 24
    target 741
  ]
  edge [
    source 24
    target 1121
  ]
  edge [
    source 24
    target 1122
  ]
  edge [
    source 24
    target 1123
  ]
  edge [
    source 24
    target 1124
  ]
  edge [
    source 24
    target 1125
  ]
  edge [
    source 24
    target 1126
  ]
  edge [
    source 24
    target 1127
  ]
  edge [
    source 24
    target 1128
  ]
  edge [
    source 24
    target 1129
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 120
  ]
  edge [
    source 25
    target 1130
  ]
  edge [
    source 25
    target 1131
  ]
  edge [
    source 25
    target 1132
  ]
  edge [
    source 25
    target 1133
  ]
  edge [
    source 25
    target 113
  ]
  edge [
    source 25
    target 1134
  ]
  edge [
    source 25
    target 118
  ]
  edge [
    source 25
    target 1135
  ]
  edge [
    source 25
    target 1136
  ]
  edge [
    source 26
    target 1137
  ]
  edge [
    source 26
    target 1138
  ]
  edge [
    source 26
    target 1139
  ]
  edge [
    source 26
    target 1140
  ]
  edge [
    source 26
    target 1141
  ]
  edge [
    source 26
    target 1142
  ]
  edge [
    source 26
    target 1143
  ]
  edge [
    source 26
    target 1144
  ]
  edge [
    source 26
    target 1145
  ]
  edge [
    source 26
    target 1146
  ]
  edge [
    source 26
    target 1147
  ]
  edge [
    source 26
    target 1148
  ]
  edge [
    source 26
    target 1149
  ]
  edge [
    source 26
    target 1030
  ]
  edge [
    source 26
    target 1150
  ]
  edge [
    source 26
    target 1151
  ]
  edge [
    source 26
    target 1152
  ]
  edge [
    source 26
    target 1153
  ]
  edge [
    source 26
    target 1154
  ]
  edge [
    source 26
    target 1155
  ]
  edge [
    source 26
    target 1156
  ]
  edge [
    source 26
    target 144
  ]
  edge [
    source 26
    target 1157
  ]
  edge [
    source 26
    target 841
  ]
  edge [
    source 26
    target 1158
  ]
  edge [
    source 26
    target 328
  ]
  edge [
    source 26
    target 1159
  ]
  edge [
    source 26
    target 1160
  ]
  edge [
    source 26
    target 519
  ]
  edge [
    source 26
    target 1161
  ]
  edge [
    source 26
    target 1162
  ]
  edge [
    source 26
    target 1163
  ]
  edge [
    source 26
    target 1164
  ]
  edge [
    source 26
    target 1165
  ]
  edge [
    source 26
    target 1166
  ]
  edge [
    source 26
    target 1167
  ]
  edge [
    source 26
    target 880
  ]
  edge [
    source 26
    target 1168
  ]
  edge [
    source 26
    target 1169
  ]
  edge [
    source 26
    target 1170
  ]
  edge [
    source 26
    target 1171
  ]
  edge [
    source 26
    target 50
  ]
  edge [
    source 26
    target 1172
  ]
  edge [
    source 26
    target 1173
  ]
  edge [
    source 26
    target 1174
  ]
  edge [
    source 26
    target 824
  ]
  edge [
    source 26
    target 1175
  ]
  edge [
    source 26
    target 1176
  ]
  edge [
    source 26
    target 1177
  ]
  edge [
    source 26
    target 1178
  ]
  edge [
    source 26
    target 1179
  ]
  edge [
    source 26
    target 1180
  ]
  edge [
    source 26
    target 1181
  ]
  edge [
    source 26
    target 1182
  ]
  edge [
    source 26
    target 1183
  ]
  edge [
    source 26
    target 1184
  ]
  edge [
    source 26
    target 1185
  ]
  edge [
    source 26
    target 1186
  ]
  edge [
    source 26
    target 1187
  ]
  edge [
    source 26
    target 1188
  ]
  edge [
    source 26
    target 1189
  ]
  edge [
    source 26
    target 1190
  ]
  edge [
    source 26
    target 1191
  ]
  edge [
    source 26
    target 1192
  ]
  edge [
    source 26
    target 1193
  ]
  edge [
    source 26
    target 1194
  ]
  edge [
    source 26
    target 1195
  ]
  edge [
    source 26
    target 1196
  ]
  edge [
    source 26
    target 1197
  ]
  edge [
    source 26
    target 1198
  ]
  edge [
    source 26
    target 1199
  ]
  edge [
    source 26
    target 1200
  ]
  edge [
    source 26
    target 1201
  ]
  edge [
    source 26
    target 1202
  ]
  edge [
    source 26
    target 1203
  ]
  edge [
    source 26
    target 1204
  ]
  edge [
    source 26
    target 1205
  ]
  edge [
    source 26
    target 1206
  ]
  edge [
    source 26
    target 1207
  ]
  edge [
    source 26
    target 1208
  ]
  edge [
    source 26
    target 1209
  ]
  edge [
    source 26
    target 1210
  ]
  edge [
    source 26
    target 1211
  ]
  edge [
    source 26
    target 1212
  ]
  edge [
    source 26
    target 1213
  ]
  edge [
    source 26
    target 1214
  ]
  edge [
    source 26
    target 1215
  ]
  edge [
    source 26
    target 380
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1216
  ]
  edge [
    source 27
    target 1217
  ]
  edge [
    source 27
    target 1218
  ]
  edge [
    source 27
    target 1219
  ]
  edge [
    source 27
    target 1220
  ]
  edge [
    source 27
    target 1221
  ]
  edge [
    source 27
    target 1222
  ]
  edge [
    source 27
    target 1223
  ]
  edge [
    source 27
    target 474
  ]
  edge [
    source 27
    target 1224
  ]
  edge [
    source 27
    target 1225
  ]
  edge [
    source 27
    target 1226
  ]
  edge [
    source 27
    target 1227
  ]
  edge [
    source 27
    target 1228
  ]
  edge [
    source 27
    target 1229
  ]
  edge [
    source 27
    target 1230
  ]
  edge [
    source 27
    target 1231
  ]
  edge [
    source 27
    target 1232
  ]
  edge [
    source 27
    target 1233
  ]
  edge [
    source 27
    target 1234
  ]
  edge [
    source 27
    target 1235
  ]
  edge [
    source 27
    target 1236
  ]
  edge [
    source 27
    target 1237
  ]
  edge [
    source 27
    target 1238
  ]
  edge [
    source 27
    target 1239
  ]
  edge [
    source 27
    target 1240
  ]
  edge [
    source 27
    target 1241
  ]
  edge [
    source 27
    target 1242
  ]
  edge [
    source 27
    target 1243
  ]
  edge [
    source 27
    target 1244
  ]
  edge [
    source 27
    target 1245
  ]
  edge [
    source 27
    target 787
  ]
  edge [
    source 27
    target 1246
  ]
  edge [
    source 27
    target 1247
  ]
  edge [
    source 27
    target 1248
  ]
  edge [
    source 27
    target 1249
  ]
  edge [
    source 27
    target 1250
  ]
  edge [
    source 27
    target 648
  ]
  edge [
    source 27
    target 1251
  ]
  edge [
    source 27
    target 1252
  ]
  edge [
    source 27
    target 1253
  ]
  edge [
    source 27
    target 1254
  ]
  edge [
    source 27
    target 1255
  ]
  edge [
    source 27
    target 1256
  ]
  edge [
    source 27
    target 1257
  ]
  edge [
    source 27
    target 1258
  ]
  edge [
    source 27
    target 1259
  ]
  edge [
    source 27
    target 1260
  ]
  edge [
    source 27
    target 1261
  ]
  edge [
    source 27
    target 1262
  ]
  edge [
    source 27
    target 1263
  ]
  edge [
    source 27
    target 1264
  ]
  edge [
    source 27
    target 1265
  ]
  edge [
    source 27
    target 1266
  ]
  edge [
    source 27
    target 1267
  ]
  edge [
    source 27
    target 396
  ]
  edge [
    source 27
    target 1268
  ]
  edge [
    source 27
    target 1269
  ]
  edge [
    source 27
    target 1270
  ]
  edge [
    source 27
    target 1271
  ]
  edge [
    source 27
    target 1272
  ]
  edge [
    source 27
    target 1273
  ]
  edge [
    source 27
    target 1274
  ]
  edge [
    source 27
    target 1275
  ]
  edge [
    source 27
    target 1276
  ]
  edge [
    source 27
    target 1277
  ]
  edge [
    source 27
    target 1278
  ]
  edge [
    source 27
    target 1279
  ]
  edge [
    source 27
    target 1280
  ]
  edge [
    source 27
    target 1281
  ]
  edge [
    source 27
    target 1282
  ]
  edge [
    source 27
    target 1283
  ]
  edge [
    source 27
    target 1284
  ]
  edge [
    source 27
    target 1285
  ]
  edge [
    source 27
    target 411
  ]
  edge [
    source 27
    target 1286
  ]
  edge [
    source 27
    target 1287
  ]
  edge [
    source 27
    target 1288
  ]
  edge [
    source 27
    target 1289
  ]
  edge [
    source 27
    target 1290
  ]
  edge [
    source 27
    target 1291
  ]
  edge [
    source 27
    target 1292
  ]
  edge [
    source 27
    target 1293
  ]
  edge [
    source 27
    target 1294
  ]
  edge [
    source 27
    target 1295
  ]
  edge [
    source 27
    target 1296
  ]
  edge [
    source 27
    target 1297
  ]
  edge [
    source 27
    target 1298
  ]
  edge [
    source 27
    target 1299
  ]
  edge [
    source 27
    target 594
  ]
  edge [
    source 27
    target 1300
  ]
  edge [
    source 27
    target 202
  ]
  edge [
    source 27
    target 1301
  ]
  edge [
    source 27
    target 1302
  ]
  edge [
    source 27
    target 1303
  ]
  edge [
    source 27
    target 1304
  ]
  edge [
    source 27
    target 1305
  ]
  edge [
    source 27
    target 1306
  ]
  edge [
    source 27
    target 1307
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 199
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 1148
  ]
  edge [
    source 28
    target 1308
  ]
  edge [
    source 28
    target 1309
  ]
  edge [
    source 28
    target 218
  ]
  edge [
    source 28
    target 219
  ]
  edge [
    source 28
    target 220
  ]
  edge [
    source 28
    target 221
  ]
  edge [
    source 28
    target 222
  ]
  edge [
    source 1311
    target 1312
  ]
  edge [
    source 1313
    target 1314
  ]
  edge [
    source 1315
    target 1316
  ]
  edge [
    source 1315
    target 1317
  ]
  edge [
    source 1316
    target 1317
  ]
  edge [
    source 1319
    target 1320
  ]
]
