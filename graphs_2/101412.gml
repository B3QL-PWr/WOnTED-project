graph [
  node [
    id 0
    label "droga"
    origin "text"
  ]
  node [
    id 1
    label "krajowy"
    origin "text"
  ]
  node [
    id 2
    label "niemcy"
    origin "text"
  ]
  node [
    id 3
    label "ekwipunek"
  ]
  node [
    id 4
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 5
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 6
    label "podbieg"
  ]
  node [
    id 7
    label "wyb&#243;j"
  ]
  node [
    id 8
    label "journey"
  ]
  node [
    id 9
    label "pobocze"
  ]
  node [
    id 10
    label "ekskursja"
  ]
  node [
    id 11
    label "drogowskaz"
  ]
  node [
    id 12
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 13
    label "budowla"
  ]
  node [
    id 14
    label "rajza"
  ]
  node [
    id 15
    label "passage"
  ]
  node [
    id 16
    label "marszrutyzacja"
  ]
  node [
    id 17
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 18
    label "trasa"
  ]
  node [
    id 19
    label "zbior&#243;wka"
  ]
  node [
    id 20
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 21
    label "spos&#243;b"
  ]
  node [
    id 22
    label "turystyka"
  ]
  node [
    id 23
    label "wylot"
  ]
  node [
    id 24
    label "ruch"
  ]
  node [
    id 25
    label "bezsilnikowy"
  ]
  node [
    id 26
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 27
    label "nawierzchnia"
  ]
  node [
    id 28
    label "korona_drogi"
  ]
  node [
    id 29
    label "przebieg"
  ]
  node [
    id 30
    label "infrastruktura"
  ]
  node [
    id 31
    label "w&#281;ze&#322;"
  ]
  node [
    id 32
    label "rzecz"
  ]
  node [
    id 33
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 34
    label "stan_surowy"
  ]
  node [
    id 35
    label "postanie"
  ]
  node [
    id 36
    label "zbudowa&#263;"
  ]
  node [
    id 37
    label "obudowywanie"
  ]
  node [
    id 38
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 39
    label "obudowywa&#263;"
  ]
  node [
    id 40
    label "konstrukcja"
  ]
  node [
    id 41
    label "Sukiennice"
  ]
  node [
    id 42
    label "kolumnada"
  ]
  node [
    id 43
    label "korpus"
  ]
  node [
    id 44
    label "zbudowanie"
  ]
  node [
    id 45
    label "fundament"
  ]
  node [
    id 46
    label "obudowa&#263;"
  ]
  node [
    id 47
    label "obudowanie"
  ]
  node [
    id 48
    label "zbi&#243;r"
  ]
  node [
    id 49
    label "model"
  ]
  node [
    id 50
    label "narz&#281;dzie"
  ]
  node [
    id 51
    label "nature"
  ]
  node [
    id 52
    label "tryb"
  ]
  node [
    id 53
    label "odcinek"
  ]
  node [
    id 54
    label "ton"
  ]
  node [
    id 55
    label "ambitus"
  ]
  node [
    id 56
    label "rozmiar"
  ]
  node [
    id 57
    label "skala"
  ]
  node [
    id 58
    label "czas"
  ]
  node [
    id 59
    label "move"
  ]
  node [
    id 60
    label "zmiana"
  ]
  node [
    id 61
    label "aktywno&#347;&#263;"
  ]
  node [
    id 62
    label "utrzymywanie"
  ]
  node [
    id 63
    label "utrzymywa&#263;"
  ]
  node [
    id 64
    label "taktyka"
  ]
  node [
    id 65
    label "d&#322;ugi"
  ]
  node [
    id 66
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 67
    label "natural_process"
  ]
  node [
    id 68
    label "kanciasty"
  ]
  node [
    id 69
    label "utrzyma&#263;"
  ]
  node [
    id 70
    label "myk"
  ]
  node [
    id 71
    label "manewr"
  ]
  node [
    id 72
    label "utrzymanie"
  ]
  node [
    id 73
    label "wydarzenie"
  ]
  node [
    id 74
    label "tumult"
  ]
  node [
    id 75
    label "stopek"
  ]
  node [
    id 76
    label "movement"
  ]
  node [
    id 77
    label "strumie&#324;"
  ]
  node [
    id 78
    label "czynno&#347;&#263;"
  ]
  node [
    id 79
    label "komunikacja"
  ]
  node [
    id 80
    label "lokomocja"
  ]
  node [
    id 81
    label "drift"
  ]
  node [
    id 82
    label "commercial_enterprise"
  ]
  node [
    id 83
    label "zjawisko"
  ]
  node [
    id 84
    label "apraksja"
  ]
  node [
    id 85
    label "proces"
  ]
  node [
    id 86
    label "poruszenie"
  ]
  node [
    id 87
    label "mechanika"
  ]
  node [
    id 88
    label "travel"
  ]
  node [
    id 89
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 90
    label "dyssypacja_energii"
  ]
  node [
    id 91
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 92
    label "kr&#243;tki"
  ]
  node [
    id 93
    label "warstwa"
  ]
  node [
    id 94
    label "pokrycie"
  ]
  node [
    id 95
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 96
    label "tablica"
  ]
  node [
    id 97
    label "fingerpost"
  ]
  node [
    id 98
    label "r&#281;kaw"
  ]
  node [
    id 99
    label "koniec"
  ]
  node [
    id 100
    label "kontusz"
  ]
  node [
    id 101
    label "otw&#243;r"
  ]
  node [
    id 102
    label "przydro&#380;e"
  ]
  node [
    id 103
    label "autostrada"
  ]
  node [
    id 104
    label "operacja"
  ]
  node [
    id 105
    label "bieg"
  ]
  node [
    id 106
    label "podr&#243;&#380;"
  ]
  node [
    id 107
    label "stray"
  ]
  node [
    id 108
    label "pozostawa&#263;"
  ]
  node [
    id 109
    label "s&#261;dzi&#263;"
  ]
  node [
    id 110
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 111
    label "digress"
  ]
  node [
    id 112
    label "chodzi&#263;"
  ]
  node [
    id 113
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 114
    label "mieszanie_si&#281;"
  ]
  node [
    id 115
    label "chodzenie"
  ]
  node [
    id 116
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 117
    label "beznap&#281;dowy"
  ]
  node [
    id 118
    label "dormitorium"
  ]
  node [
    id 119
    label "fotografia"
  ]
  node [
    id 120
    label "spis"
  ]
  node [
    id 121
    label "sk&#322;adanka"
  ]
  node [
    id 122
    label "polowanie"
  ]
  node [
    id 123
    label "wyprawa"
  ]
  node [
    id 124
    label "pomieszczenie"
  ]
  node [
    id 125
    label "wyposa&#380;enie"
  ]
  node [
    id 126
    label "nie&#347;miertelnik"
  ]
  node [
    id 127
    label "moderunek"
  ]
  node [
    id 128
    label "kocher"
  ]
  node [
    id 129
    label "na_pieska"
  ]
  node [
    id 130
    label "erotyka"
  ]
  node [
    id 131
    label "zajawka"
  ]
  node [
    id 132
    label "love"
  ]
  node [
    id 133
    label "podniecanie"
  ]
  node [
    id 134
    label "po&#380;ycie"
  ]
  node [
    id 135
    label "ukochanie"
  ]
  node [
    id 136
    label "baraszki"
  ]
  node [
    id 137
    label "numer"
  ]
  node [
    id 138
    label "ruch_frykcyjny"
  ]
  node [
    id 139
    label "tendency"
  ]
  node [
    id 140
    label "wzw&#243;d"
  ]
  node [
    id 141
    label "serce"
  ]
  node [
    id 142
    label "wi&#281;&#378;"
  ]
  node [
    id 143
    label "cz&#322;owiek"
  ]
  node [
    id 144
    label "seks"
  ]
  node [
    id 145
    label "pozycja_misjonarska"
  ]
  node [
    id 146
    label "rozmna&#380;anie"
  ]
  node [
    id 147
    label "feblik"
  ]
  node [
    id 148
    label "z&#322;&#261;czenie"
  ]
  node [
    id 149
    label "imisja"
  ]
  node [
    id 150
    label "podniecenie"
  ]
  node [
    id 151
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 152
    label "podnieca&#263;"
  ]
  node [
    id 153
    label "zakochanie"
  ]
  node [
    id 154
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 155
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 156
    label "gra_wst&#281;pna"
  ]
  node [
    id 157
    label "drogi"
  ]
  node [
    id 158
    label "po&#380;&#261;danie"
  ]
  node [
    id 159
    label "podnieci&#263;"
  ]
  node [
    id 160
    label "emocja"
  ]
  node [
    id 161
    label "afekt"
  ]
  node [
    id 162
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 163
    label "kochanka"
  ]
  node [
    id 164
    label "psiapsi&#243;&#322;ka"
  ]
  node [
    id 165
    label "kultura_fizyczna"
  ]
  node [
    id 166
    label "turyzm"
  ]
  node [
    id 167
    label "rodzimy"
  ]
  node [
    id 168
    label "w&#322;asny"
  ]
  node [
    id 169
    label "tutejszy"
  ]
  node [
    id 170
    label "B210"
  ]
  node [
    id 171
    label "210"
  ]
  node [
    id 172
    label "by&#322;y"
  ]
  node [
    id 173
    label "Bundesstra&#223;e"
  ]
  node [
    id 174
    label "dolny"
  ]
  node [
    id 175
    label "Saksonia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 157
    target 170
  ]
  edge [
    source 157
    target 171
  ]
  edge [
    source 171
    target 172
  ]
  edge [
    source 171
    target 173
  ]
  edge [
    source 174
    target 175
  ]
]
