graph [
  node [
    id 0
    label "rom"
    origin "text"
  ]
  node [
    id 1
    label "k&#322;odzki"
    origin "text"
  ]
  node [
    id 2
    label "chodzenie"
    origin "text"
  ]
  node [
    id 3
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "dostawa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "stypendium"
    origin "text"
  ]
  node [
    id 6
    label "motywacyjny"
    origin "text"
  ]
  node [
    id 7
    label "polski"
  ]
  node [
    id 8
    label "po_k&#322;odzku"
  ]
  node [
    id 9
    label "regionalny"
  ]
  node [
    id 10
    label "przedmiot"
  ]
  node [
    id 11
    label "Polish"
  ]
  node [
    id 12
    label "goniony"
  ]
  node [
    id 13
    label "oberek"
  ]
  node [
    id 14
    label "ryba_po_grecku"
  ]
  node [
    id 15
    label "sztajer"
  ]
  node [
    id 16
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 17
    label "krakowiak"
  ]
  node [
    id 18
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 19
    label "pierogi_ruskie"
  ]
  node [
    id 20
    label "lacki"
  ]
  node [
    id 21
    label "polak"
  ]
  node [
    id 22
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 23
    label "chodzony"
  ]
  node [
    id 24
    label "po_polsku"
  ]
  node [
    id 25
    label "mazur"
  ]
  node [
    id 26
    label "polsko"
  ]
  node [
    id 27
    label "skoczny"
  ]
  node [
    id 28
    label "drabant"
  ]
  node [
    id 29
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 30
    label "j&#281;zyk"
  ]
  node [
    id 31
    label "tradycyjny"
  ]
  node [
    id 32
    label "regionalnie"
  ]
  node [
    id 33
    label "lokalny"
  ]
  node [
    id 34
    label "typowy"
  ]
  node [
    id 35
    label "dochodzenie"
  ]
  node [
    id 36
    label "uganianie_si&#281;"
  ]
  node [
    id 37
    label "ko&#322;atanie_si&#281;"
  ]
  node [
    id 38
    label "rozdeptywanie"
  ]
  node [
    id 39
    label "donoszenie"
  ]
  node [
    id 40
    label "obchodzenie"
  ]
  node [
    id 41
    label "staranie_si&#281;"
  ]
  node [
    id 42
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 43
    label "uruchamianie"
  ]
  node [
    id 44
    label "znoszenie"
  ]
  node [
    id 45
    label "zdeptanie"
  ]
  node [
    id 46
    label "deptanie"
  ]
  node [
    id 47
    label "rozdeptanie"
  ]
  node [
    id 48
    label "ubieranie_si&#281;"
  ]
  node [
    id 49
    label "wydeptywanie"
  ]
  node [
    id 50
    label "uruchomienie"
  ]
  node [
    id 51
    label "nakr&#281;canie"
  ]
  node [
    id 52
    label "wydeptanie"
  ]
  node [
    id 53
    label "noszenie"
  ]
  node [
    id 54
    label "przenoszenie"
  ]
  node [
    id 55
    label "bywanie"
  ]
  node [
    id 56
    label "tr&#243;jstronny"
  ]
  node [
    id 57
    label "uchodzenie_si&#281;"
  ]
  node [
    id 58
    label "nakr&#281;cenie"
  ]
  node [
    id 59
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 60
    label "zatrzymanie"
  ]
  node [
    id 61
    label "wear"
  ]
  node [
    id 62
    label "donaszanie"
  ]
  node [
    id 63
    label "podtrzymywanie"
  ]
  node [
    id 64
    label "w&#322;&#261;czanie"
  ]
  node [
    id 65
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 66
    label "zmierzanie"
  ]
  node [
    id 67
    label "ubieranie"
  ]
  node [
    id 68
    label "dzianie_si&#281;"
  ]
  node [
    id 69
    label "jitter"
  ]
  node [
    id 70
    label "pochodzenie"
  ]
  node [
    id 71
    label "ucz&#281;szczanie"
  ]
  node [
    id 72
    label "przechodzenie"
  ]
  node [
    id 73
    label "p&#322;ywanie"
  ]
  node [
    id 74
    label "poruszanie_si&#281;"
  ]
  node [
    id 75
    label "funkcja"
  ]
  node [
    id 76
    label "impact"
  ]
  node [
    id 77
    label "w&#322;&#261;czenie"
  ]
  node [
    id 78
    label "overlap"
  ]
  node [
    id 79
    label "pokazywanie_si&#281;"
  ]
  node [
    id 80
    label "czynno&#347;&#263;"
  ]
  node [
    id 81
    label "robienie"
  ]
  node [
    id 82
    label "recourse"
  ]
  node [
    id 83
    label "crawl"
  ]
  node [
    id 84
    label "zadryfowanie"
  ]
  node [
    id 85
    label "zagruntowanie"
  ]
  node [
    id 86
    label "gruntowanie"
  ]
  node [
    id 87
    label "obfitowanie"
  ]
  node [
    id 88
    label "podr&#243;&#380;owanie"
  ]
  node [
    id 89
    label "sport_wodny"
  ]
  node [
    id 90
    label "swimming"
  ]
  node [
    id 91
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 92
    label "unoszenie_si&#281;"
  ]
  node [
    id 93
    label "p&#322;ywactwo"
  ]
  node [
    id 94
    label "m&#243;wienie"
  ]
  node [
    id 95
    label "pracowanie"
  ]
  node [
    id 96
    label "pop&#322;ywanie"
  ]
  node [
    id 97
    label "czyn"
  ]
  node [
    id 98
    label "supremum"
  ]
  node [
    id 99
    label "addytywno&#347;&#263;"
  ]
  node [
    id 100
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 101
    label "jednostka"
  ]
  node [
    id 102
    label "function"
  ]
  node [
    id 103
    label "zastosowanie"
  ]
  node [
    id 104
    label "matematyka"
  ]
  node [
    id 105
    label "funkcjonowanie"
  ]
  node [
    id 106
    label "praca"
  ]
  node [
    id 107
    label "rzut"
  ]
  node [
    id 108
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 109
    label "powierzanie"
  ]
  node [
    id 110
    label "cel"
  ]
  node [
    id 111
    label "dziedzina"
  ]
  node [
    id 112
    label "przeciwdziedzina"
  ]
  node [
    id 113
    label "awansowa&#263;"
  ]
  node [
    id 114
    label "stawia&#263;"
  ]
  node [
    id 115
    label "wakowa&#263;"
  ]
  node [
    id 116
    label "znaczenie"
  ]
  node [
    id 117
    label "postawi&#263;"
  ]
  node [
    id 118
    label "awansowanie"
  ]
  node [
    id 119
    label "infimum"
  ]
  node [
    id 120
    label "widzenie"
  ]
  node [
    id 121
    label "wy&#322;&#261;czanie"
  ]
  node [
    id 122
    label "incorporation"
  ]
  node [
    id 123
    label "attachment"
  ]
  node [
    id 124
    label "zaczynanie"
  ]
  node [
    id 125
    label "nastawianie"
  ]
  node [
    id 126
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 127
    label "zapalanie"
  ]
  node [
    id 128
    label "inclusion"
  ]
  node [
    id 129
    label "przes&#322;uchiwanie"
  ]
  node [
    id 130
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 131
    label "uczestniczenie"
  ]
  node [
    id 132
    label "powodowanie"
  ]
  node [
    id 133
    label "przefiltrowanie"
  ]
  node [
    id 134
    label "zamkni&#281;cie"
  ]
  node [
    id 135
    label "career"
  ]
  node [
    id 136
    label "zaaresztowanie"
  ]
  node [
    id 137
    label "przechowanie"
  ]
  node [
    id 138
    label "spowodowanie"
  ]
  node [
    id 139
    label "closure"
  ]
  node [
    id 140
    label "observation"
  ]
  node [
    id 141
    label "uniemo&#380;liwienie"
  ]
  node [
    id 142
    label "pochowanie"
  ]
  node [
    id 143
    label "discontinuance"
  ]
  node [
    id 144
    label "przerwanie"
  ]
  node [
    id 145
    label "zaczepienie"
  ]
  node [
    id 146
    label "pozajmowanie"
  ]
  node [
    id 147
    label "hipostaza"
  ]
  node [
    id 148
    label "capture"
  ]
  node [
    id 149
    label "przetrzymanie"
  ]
  node [
    id 150
    label "oddzia&#322;anie"
  ]
  node [
    id 151
    label "&#322;apanie"
  ]
  node [
    id 152
    label "z&#322;apanie"
  ]
  node [
    id 153
    label "check"
  ]
  node [
    id 154
    label "unieruchomienie"
  ]
  node [
    id 155
    label "zabranie"
  ]
  node [
    id 156
    label "przestanie"
  ]
  node [
    id 157
    label "kapita&#322;"
  ]
  node [
    id 158
    label "zacz&#281;cie"
  ]
  node [
    id 159
    label "propulsion"
  ]
  node [
    id 160
    label "zrobienie"
  ]
  node [
    id 161
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 162
    label "pos&#322;uchanie"
  ]
  node [
    id 163
    label "obejrzenie"
  ]
  node [
    id 164
    label "involvement"
  ]
  node [
    id 165
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 166
    label "za&#347;wiecenie"
  ]
  node [
    id 167
    label "nastawienie"
  ]
  node [
    id 168
    label "wy&#322;&#261;czenie"
  ]
  node [
    id 169
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 170
    label "ukr&#281;cenie"
  ]
  node [
    id 171
    label "gyration"
  ]
  node [
    id 172
    label "ruszanie"
  ]
  node [
    id 173
    label "dokr&#281;cenie"
  ]
  node [
    id 174
    label "kr&#281;cenie"
  ]
  node [
    id 175
    label "zakr&#281;canie"
  ]
  node [
    id 176
    label "tworzenie"
  ]
  node [
    id 177
    label "nagrywanie"
  ]
  node [
    id 178
    label "wind"
  ]
  node [
    id 179
    label "nak&#322;adanie"
  ]
  node [
    id 180
    label "okr&#281;canie"
  ]
  node [
    id 181
    label "wzmaganie"
  ]
  node [
    id 182
    label "wzbudzanie"
  ]
  node [
    id 183
    label "dokr&#281;canie"
  ]
  node [
    id 184
    label "pozawijanie"
  ]
  node [
    id 185
    label "stworzenie"
  ]
  node [
    id 186
    label "nagranie"
  ]
  node [
    id 187
    label "wzbudzenie"
  ]
  node [
    id 188
    label "ruszenie"
  ]
  node [
    id 189
    label "zakr&#281;cenie"
  ]
  node [
    id 190
    label "naniesienie"
  ]
  node [
    id 191
    label "suppression"
  ]
  node [
    id 192
    label "wzmo&#380;enie"
  ]
  node [
    id 193
    label "okr&#281;cenie"
  ]
  node [
    id 194
    label "nak&#322;amanie"
  ]
  node [
    id 195
    label "utrzymywanie"
  ]
  node [
    id 196
    label "obstawanie"
  ]
  node [
    id 197
    label "bycie"
  ]
  node [
    id 198
    label "preservation"
  ]
  node [
    id 199
    label "boost"
  ]
  node [
    id 200
    label "continuance"
  ]
  node [
    id 201
    label "pocieszanie"
  ]
  node [
    id 202
    label "umieszczanie"
  ]
  node [
    id 203
    label "upi&#281;kszanie"
  ]
  node [
    id 204
    label "odziewanie"
  ]
  node [
    id 205
    label "dressing"
  ]
  node [
    id 206
    label "przebieranie"
  ]
  node [
    id 207
    label "k&#322;adzenie"
  ]
  node [
    id 208
    label "pi&#281;kniejszy"
  ]
  node [
    id 209
    label "przymierzanie"
  ]
  node [
    id 210
    label "adornment"
  ]
  node [
    id 211
    label "oblekanie"
  ]
  node [
    id 212
    label "bacteriophage"
  ]
  node [
    id 213
    label "infection"
  ]
  node [
    id 214
    label "zmienianie"
  ]
  node [
    id 215
    label "strzelanie"
  ]
  node [
    id 216
    label "move"
  ]
  node [
    id 217
    label "transportation"
  ]
  node [
    id 218
    label "przesadzanie"
  ]
  node [
    id 219
    label "kopiowanie"
  ]
  node [
    id 220
    label "transmission"
  ]
  node [
    id 221
    label "pocisk"
  ]
  node [
    id 222
    label "dostosowywanie"
  ]
  node [
    id 223
    label "drift"
  ]
  node [
    id 224
    label "przetrwanie"
  ]
  node [
    id 225
    label "przesuwanie_si&#281;"
  ]
  node [
    id 226
    label "przemieszczanie"
  ]
  node [
    id 227
    label "przelatywanie"
  ]
  node [
    id 228
    label "translation"
  ]
  node [
    id 229
    label "ponoszenie"
  ]
  node [
    id 230
    label "rozpowszechnianie"
  ]
  node [
    id 231
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 232
    label "zaczynanie_si&#281;"
  ]
  node [
    id 233
    label "str&#243;j"
  ]
  node [
    id 234
    label "wynikanie"
  ]
  node [
    id 235
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 236
    label "origin"
  ]
  node [
    id 237
    label "background"
  ]
  node [
    id 238
    label "czas"
  ]
  node [
    id 239
    label "geneza"
  ]
  node [
    id 240
    label "beginning"
  ]
  node [
    id 241
    label "zu&#380;ywanie"
  ]
  node [
    id 242
    label "toleration"
  ]
  node [
    id 243
    label "collection"
  ]
  node [
    id 244
    label "wytrzymywanie"
  ]
  node [
    id 245
    label "take"
  ]
  node [
    id 246
    label "ranny"
  ]
  node [
    id 247
    label "jajko"
  ]
  node [
    id 248
    label "usuwanie"
  ]
  node [
    id 249
    label "porywanie"
  ]
  node [
    id 250
    label "wygrywanie"
  ]
  node [
    id 251
    label "abrogation"
  ]
  node [
    id 252
    label "gromadzenie"
  ]
  node [
    id 253
    label "poddawanie_si&#281;"
  ]
  node [
    id 254
    label "zniszczenie"
  ]
  node [
    id 255
    label "uniewa&#380;nianie"
  ]
  node [
    id 256
    label "rodzenie"
  ]
  node [
    id 257
    label "tolerowanie"
  ]
  node [
    id 258
    label "niszczenie"
  ]
  node [
    id 259
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 260
    label "stand"
  ]
  node [
    id 261
    label "do&#322;&#261;czanie"
  ]
  node [
    id 262
    label "zu&#380;ycie"
  ]
  node [
    id 263
    label "dosi&#281;ganie"
  ]
  node [
    id 264
    label "zanoszenie"
  ]
  node [
    id 265
    label "przebycie"
  ]
  node [
    id 266
    label "sk&#322;adanie"
  ]
  node [
    id 267
    label "informowanie"
  ]
  node [
    id 268
    label "ci&#261;&#380;a"
  ]
  node [
    id 269
    label "urodzenie"
  ]
  node [
    id 270
    label "naci&#347;ni&#281;cie"
  ]
  node [
    id 271
    label "zniewa&#380;enie"
  ]
  node [
    id 272
    label "rozci&#261;gni&#281;cie"
  ]
  node [
    id 273
    label "rozprowadzenie"
  ]
  node [
    id 274
    label "lu&#378;ny"
  ]
  node [
    id 275
    label "niesienie"
  ]
  node [
    id 276
    label "przepajanie"
  ]
  node [
    id 277
    label "miewanie"
  ]
  node [
    id 278
    label "nazywanie_si&#281;"
  ]
  node [
    id 279
    label "branie"
  ]
  node [
    id 280
    label "bycie_w_posiadaniu"
  ]
  node [
    id 281
    label "enjoyment"
  ]
  node [
    id 282
    label "przej&#347;cie"
  ]
  node [
    id 283
    label "kszta&#322;towanie"
  ]
  node [
    id 284
    label "pozyskiwanie"
  ]
  node [
    id 285
    label "odwiedzanie"
  ]
  node [
    id 286
    label "&#347;wi&#281;towanie"
  ]
  node [
    id 287
    label "okr&#261;&#380;anie"
  ]
  node [
    id 288
    label "oborywanie"
  ]
  node [
    id 289
    label "przychodzenie"
  ]
  node [
    id 290
    label "campaign"
  ]
  node [
    id 291
    label "wyprzedzenie"
  ]
  node [
    id 292
    label "podchodzenie"
  ]
  node [
    id 293
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 294
    label "przyj&#347;cie"
  ]
  node [
    id 295
    label "przemierzanie"
  ]
  node [
    id 296
    label "udawanie_si&#281;"
  ]
  node [
    id 297
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 298
    label "post&#281;powanie"
  ]
  node [
    id 299
    label "odchodzenie"
  ]
  node [
    id 300
    label "wyprzedzanie"
  ]
  node [
    id 301
    label "spotykanie"
  ]
  node [
    id 302
    label "podej&#347;cie"
  ]
  node [
    id 303
    label "przemierzenie"
  ]
  node [
    id 304
    label "wodzenie"
  ]
  node [
    id 305
    label "egress"
  ]
  node [
    id 306
    label "ukszta&#322;towanie"
  ]
  node [
    id 307
    label "skombinowanie"
  ]
  node [
    id 308
    label "zniewa&#380;anie"
  ]
  node [
    id 309
    label "wdeptywanie"
  ]
  node [
    id 310
    label "naruszanie"
  ]
  node [
    id 311
    label "p&#322;odzenie"
  ]
  node [
    id 312
    label "udeptywanie"
  ]
  node [
    id 313
    label "wdeptanie"
  ]
  node [
    id 314
    label "brudzenie"
  ]
  node [
    id 315
    label "tratowanie"
  ]
  node [
    id 316
    label "nast&#281;powanie"
  ]
  node [
    id 317
    label "trample"
  ]
  node [
    id 318
    label "stratowanie"
  ]
  node [
    id 319
    label "pace"
  ]
  node [
    id 320
    label "przemakanie"
  ]
  node [
    id 321
    label "przestawanie"
  ]
  node [
    id 322
    label "nasycanie_si&#281;"
  ]
  node [
    id 323
    label "popychanie"
  ]
  node [
    id 324
    label "dostawanie_si&#281;"
  ]
  node [
    id 325
    label "stawanie_si&#281;"
  ]
  node [
    id 326
    label "przep&#322;ywanie"
  ]
  node [
    id 327
    label "przepuszczanie"
  ]
  node [
    id 328
    label "zaliczanie"
  ]
  node [
    id 329
    label "nas&#261;czanie"
  ]
  node [
    id 330
    label "impregnation"
  ]
  node [
    id 331
    label "uznanie"
  ]
  node [
    id 332
    label "passage"
  ]
  node [
    id 333
    label "trwanie"
  ]
  node [
    id 334
    label "przedostawanie_si&#281;"
  ]
  node [
    id 335
    label "wytyczenie"
  ]
  node [
    id 336
    label "popchni&#281;cie"
  ]
  node [
    id 337
    label "zaznawanie"
  ]
  node [
    id 338
    label "pass"
  ]
  node [
    id 339
    label "nale&#380;enie"
  ]
  node [
    id 340
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 341
    label "potr&#261;canie"
  ]
  node [
    id 342
    label "przebywanie"
  ]
  node [
    id 343
    label "zast&#281;powanie"
  ]
  node [
    id 344
    label "test"
  ]
  node [
    id 345
    label "passing"
  ]
  node [
    id 346
    label "mijanie"
  ]
  node [
    id 347
    label "przerabianie"
  ]
  node [
    id 348
    label "odmienianie"
  ]
  node [
    id 349
    label "rozci&#261;ganie"
  ]
  node [
    id 350
    label "rozprowadzanie"
  ]
  node [
    id 351
    label "przy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 352
    label "inquest"
  ]
  node [
    id 353
    label "maturation"
  ]
  node [
    id 354
    label "dop&#322;ywanie"
  ]
  node [
    id 355
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 356
    label "inquisition"
  ]
  node [
    id 357
    label "dor&#281;czanie"
  ]
  node [
    id 358
    label "trial"
  ]
  node [
    id 359
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 360
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 361
    label "przesy&#322;ka"
  ]
  node [
    id 362
    label "postrzeganie"
  ]
  node [
    id 363
    label "dodatek"
  ]
  node [
    id 364
    label "wydarzenie"
  ]
  node [
    id 365
    label "assay"
  ]
  node [
    id 366
    label "doje&#380;d&#380;anie"
  ]
  node [
    id 367
    label "dochrapywanie_si&#281;"
  ]
  node [
    id 368
    label "ro&#347;ni&#281;cie"
  ]
  node [
    id 369
    label "Inquisition"
  ]
  node [
    id 370
    label "roszczenie"
  ]
  node [
    id 371
    label "dolatywanie"
  ]
  node [
    id 372
    label "strzelenie"
  ]
  node [
    id 373
    label "orgazm"
  ]
  node [
    id 374
    label "detektyw"
  ]
  node [
    id 375
    label "&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 376
    label "doczekanie"
  ]
  node [
    id 377
    label "rozwijanie_si&#281;"
  ]
  node [
    id 378
    label "uzyskiwanie"
  ]
  node [
    id 379
    label "docieranie"
  ]
  node [
    id 380
    label "dojrza&#322;y"
  ]
  node [
    id 381
    label "osi&#261;ganie"
  ]
  node [
    id 382
    label "dop&#322;ata"
  ]
  node [
    id 383
    label "examination"
  ]
  node [
    id 384
    label "do&#347;wiadczenie"
  ]
  node [
    id 385
    label "teren_szko&#322;y"
  ]
  node [
    id 386
    label "wiedza"
  ]
  node [
    id 387
    label "Mickiewicz"
  ]
  node [
    id 388
    label "kwalifikacje"
  ]
  node [
    id 389
    label "podr&#281;cznik"
  ]
  node [
    id 390
    label "absolwent"
  ]
  node [
    id 391
    label "praktyka"
  ]
  node [
    id 392
    label "school"
  ]
  node [
    id 393
    label "system"
  ]
  node [
    id 394
    label "zda&#263;"
  ]
  node [
    id 395
    label "gabinet"
  ]
  node [
    id 396
    label "urszulanki"
  ]
  node [
    id 397
    label "sztuba"
  ]
  node [
    id 398
    label "&#322;awa_szkolna"
  ]
  node [
    id 399
    label "nauka"
  ]
  node [
    id 400
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 401
    label "przepisa&#263;"
  ]
  node [
    id 402
    label "muzyka"
  ]
  node [
    id 403
    label "grupa"
  ]
  node [
    id 404
    label "form"
  ]
  node [
    id 405
    label "klasa"
  ]
  node [
    id 406
    label "lekcja"
  ]
  node [
    id 407
    label "metoda"
  ]
  node [
    id 408
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 409
    label "przepisanie"
  ]
  node [
    id 410
    label "skolaryzacja"
  ]
  node [
    id 411
    label "zdanie"
  ]
  node [
    id 412
    label "stopek"
  ]
  node [
    id 413
    label "sekretariat"
  ]
  node [
    id 414
    label "ideologia"
  ]
  node [
    id 415
    label "lesson"
  ]
  node [
    id 416
    label "instytucja"
  ]
  node [
    id 417
    label "niepokalanki"
  ]
  node [
    id 418
    label "siedziba"
  ]
  node [
    id 419
    label "szkolenie"
  ]
  node [
    id 420
    label "kara"
  ]
  node [
    id 421
    label "tablica"
  ]
  node [
    id 422
    label "wyprawka"
  ]
  node [
    id 423
    label "pomoc_naukowa"
  ]
  node [
    id 424
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 425
    label "odm&#322;adzanie"
  ]
  node [
    id 426
    label "liga"
  ]
  node [
    id 427
    label "jednostka_systematyczna"
  ]
  node [
    id 428
    label "asymilowanie"
  ]
  node [
    id 429
    label "gromada"
  ]
  node [
    id 430
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 431
    label "asymilowa&#263;"
  ]
  node [
    id 432
    label "egzemplarz"
  ]
  node [
    id 433
    label "Entuzjastki"
  ]
  node [
    id 434
    label "zbi&#243;r"
  ]
  node [
    id 435
    label "kompozycja"
  ]
  node [
    id 436
    label "Terranie"
  ]
  node [
    id 437
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 438
    label "category"
  ]
  node [
    id 439
    label "pakiet_klimatyczny"
  ]
  node [
    id 440
    label "oddzia&#322;"
  ]
  node [
    id 441
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 442
    label "cz&#261;steczka"
  ]
  node [
    id 443
    label "stage_set"
  ]
  node [
    id 444
    label "type"
  ]
  node [
    id 445
    label "specgrupa"
  ]
  node [
    id 446
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 447
    label "&#346;wietliki"
  ]
  node [
    id 448
    label "odm&#322;odzenie"
  ]
  node [
    id 449
    label "Eurogrupa"
  ]
  node [
    id 450
    label "odm&#322;adza&#263;"
  ]
  node [
    id 451
    label "formacja_geologiczna"
  ]
  node [
    id 452
    label "harcerze_starsi"
  ]
  node [
    id 453
    label "course"
  ]
  node [
    id 454
    label "pomaganie"
  ]
  node [
    id 455
    label "training"
  ]
  node [
    id 456
    label "zapoznawanie"
  ]
  node [
    id 457
    label "seria"
  ]
  node [
    id 458
    label "zaj&#281;cia"
  ]
  node [
    id 459
    label "pouczenie"
  ]
  node [
    id 460
    label "o&#347;wiecanie"
  ]
  node [
    id 461
    label "Lira"
  ]
  node [
    id 462
    label "kliker"
  ]
  node [
    id 463
    label "miasteczko_rowerowe"
  ]
  node [
    id 464
    label "porada"
  ]
  node [
    id 465
    label "fotowoltaika"
  ]
  node [
    id 466
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 467
    label "przem&#243;wienie"
  ]
  node [
    id 468
    label "nauki_o_poznaniu"
  ]
  node [
    id 469
    label "nomotetyczny"
  ]
  node [
    id 470
    label "systematyka"
  ]
  node [
    id 471
    label "proces"
  ]
  node [
    id 472
    label "typologia"
  ]
  node [
    id 473
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 474
    label "kultura_duchowa"
  ]
  node [
    id 475
    label "nauki_penalne"
  ]
  node [
    id 476
    label "imagineskopia"
  ]
  node [
    id 477
    label "teoria_naukowa"
  ]
  node [
    id 478
    label "inwentyka"
  ]
  node [
    id 479
    label "metodologia"
  ]
  node [
    id 480
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 481
    label "nauki_o_Ziemi"
  ]
  node [
    id 482
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 483
    label "eliminacje"
  ]
  node [
    id 484
    label "osoba_prawna"
  ]
  node [
    id 485
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 486
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 487
    label "poj&#281;cie"
  ]
  node [
    id 488
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 489
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 490
    label "biuro"
  ]
  node [
    id 491
    label "organizacja"
  ]
  node [
    id 492
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 493
    label "Fundusze_Unijne"
  ]
  node [
    id 494
    label "zamyka&#263;"
  ]
  node [
    id 495
    label "establishment"
  ]
  node [
    id 496
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 497
    label "urz&#261;d"
  ]
  node [
    id 498
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 499
    label "afiliowa&#263;"
  ]
  node [
    id 500
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 501
    label "standard"
  ]
  node [
    id 502
    label "zamykanie"
  ]
  node [
    id 503
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 504
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 505
    label "materia&#322;"
  ]
  node [
    id 506
    label "spos&#243;b"
  ]
  node [
    id 507
    label "obrz&#261;dek"
  ]
  node [
    id 508
    label "Biblia"
  ]
  node [
    id 509
    label "tekst"
  ]
  node [
    id 510
    label "lektor"
  ]
  node [
    id 511
    label "kwota"
  ]
  node [
    id 512
    label "nemezis"
  ]
  node [
    id 513
    label "konsekwencja"
  ]
  node [
    id 514
    label "punishment"
  ]
  node [
    id 515
    label "klacz"
  ]
  node [
    id 516
    label "forfeit"
  ]
  node [
    id 517
    label "roboty_przymusowe"
  ]
  node [
    id 518
    label "poprzedzanie"
  ]
  node [
    id 519
    label "czasoprzestrze&#324;"
  ]
  node [
    id 520
    label "laba"
  ]
  node [
    id 521
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 522
    label "chronometria"
  ]
  node [
    id 523
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 524
    label "rachuba_czasu"
  ]
  node [
    id 525
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 526
    label "czasokres"
  ]
  node [
    id 527
    label "odczyt"
  ]
  node [
    id 528
    label "chwila"
  ]
  node [
    id 529
    label "dzieje"
  ]
  node [
    id 530
    label "kategoria_gramatyczna"
  ]
  node [
    id 531
    label "poprzedzenie"
  ]
  node [
    id 532
    label "trawienie"
  ]
  node [
    id 533
    label "pochodzi&#263;"
  ]
  node [
    id 534
    label "period"
  ]
  node [
    id 535
    label "okres_czasu"
  ]
  node [
    id 536
    label "poprzedza&#263;"
  ]
  node [
    id 537
    label "schy&#322;ek"
  ]
  node [
    id 538
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 539
    label "odwlekanie_si&#281;"
  ]
  node [
    id 540
    label "zegar"
  ]
  node [
    id 541
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 542
    label "czwarty_wymiar"
  ]
  node [
    id 543
    label "koniugacja"
  ]
  node [
    id 544
    label "Zeitgeist"
  ]
  node [
    id 545
    label "trawi&#263;"
  ]
  node [
    id 546
    label "pogoda"
  ]
  node [
    id 547
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 548
    label "poprzedzi&#263;"
  ]
  node [
    id 549
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 550
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 551
    label "time_period"
  ]
  node [
    id 552
    label "j&#261;dro"
  ]
  node [
    id 553
    label "systemik"
  ]
  node [
    id 554
    label "rozprz&#261;c"
  ]
  node [
    id 555
    label "oprogramowanie"
  ]
  node [
    id 556
    label "systemat"
  ]
  node [
    id 557
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 558
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 559
    label "model"
  ]
  node [
    id 560
    label "struktura"
  ]
  node [
    id 561
    label "usenet"
  ]
  node [
    id 562
    label "s&#261;d"
  ]
  node [
    id 563
    label "porz&#261;dek"
  ]
  node [
    id 564
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 565
    label "przyn&#281;ta"
  ]
  node [
    id 566
    label "p&#322;&#243;d"
  ]
  node [
    id 567
    label "net"
  ]
  node [
    id 568
    label "w&#281;dkarstwo"
  ]
  node [
    id 569
    label "eratem"
  ]
  node [
    id 570
    label "doktryna"
  ]
  node [
    id 571
    label "pulpit"
  ]
  node [
    id 572
    label "konstelacja"
  ]
  node [
    id 573
    label "jednostka_geologiczna"
  ]
  node [
    id 574
    label "o&#347;"
  ]
  node [
    id 575
    label "podsystem"
  ]
  node [
    id 576
    label "ryba"
  ]
  node [
    id 577
    label "Leopard"
  ]
  node [
    id 578
    label "Android"
  ]
  node [
    id 579
    label "zachowanie"
  ]
  node [
    id 580
    label "cybernetyk"
  ]
  node [
    id 581
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 582
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 583
    label "method"
  ]
  node [
    id 584
    label "sk&#322;ad"
  ]
  node [
    id 585
    label "podstawa"
  ]
  node [
    id 586
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 587
    label "practice"
  ]
  node [
    id 588
    label "znawstwo"
  ]
  node [
    id 589
    label "skill"
  ]
  node [
    id 590
    label "zwyczaj"
  ]
  node [
    id 591
    label "eksperiencja"
  ]
  node [
    id 592
    label "&#321;ubianka"
  ]
  node [
    id 593
    label "miejsce_pracy"
  ]
  node [
    id 594
    label "dzia&#322;_personalny"
  ]
  node [
    id 595
    label "Kreml"
  ]
  node [
    id 596
    label "Bia&#322;y_Dom"
  ]
  node [
    id 597
    label "budynek"
  ]
  node [
    id 598
    label "miejsce"
  ]
  node [
    id 599
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 600
    label "sadowisko"
  ]
  node [
    id 601
    label "wokalistyka"
  ]
  node [
    id 602
    label "wykonywanie"
  ]
  node [
    id 603
    label "muza"
  ]
  node [
    id 604
    label "wykonywa&#263;"
  ]
  node [
    id 605
    label "zjawisko"
  ]
  node [
    id 606
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 607
    label "beatbox"
  ]
  node [
    id 608
    label "komponowa&#263;"
  ]
  node [
    id 609
    label "komponowanie"
  ]
  node [
    id 610
    label "wytw&#243;r"
  ]
  node [
    id 611
    label "pasa&#380;"
  ]
  node [
    id 612
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 613
    label "notacja_muzyczna"
  ]
  node [
    id 614
    label "kontrapunkt"
  ]
  node [
    id 615
    label "sztuka"
  ]
  node [
    id 616
    label "instrumentalistyka"
  ]
  node [
    id 617
    label "harmonia"
  ]
  node [
    id 618
    label "set"
  ]
  node [
    id 619
    label "wys&#322;uchanie"
  ]
  node [
    id 620
    label "kapela"
  ]
  node [
    id 621
    label "britpop"
  ]
  node [
    id 622
    label "badanie"
  ]
  node [
    id 623
    label "obserwowanie"
  ]
  node [
    id 624
    label "wy&#347;wiadczenie"
  ]
  node [
    id 625
    label "checkup"
  ]
  node [
    id 626
    label "spotkanie"
  ]
  node [
    id 627
    label "do&#347;wiadczanie"
  ]
  node [
    id 628
    label "zbadanie"
  ]
  node [
    id 629
    label "potraktowanie"
  ]
  node [
    id 630
    label "poczucie"
  ]
  node [
    id 631
    label "proporcja"
  ]
  node [
    id 632
    label "wykszta&#322;cenie"
  ]
  node [
    id 633
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 634
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 635
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 636
    label "urszulanki_szare"
  ]
  node [
    id 637
    label "cognition"
  ]
  node [
    id 638
    label "intelekt"
  ]
  node [
    id 639
    label "pozwolenie"
  ]
  node [
    id 640
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 641
    label "zaawansowanie"
  ]
  node [
    id 642
    label "przekaza&#263;"
  ]
  node [
    id 643
    label "supply"
  ]
  node [
    id 644
    label "zaleci&#263;"
  ]
  node [
    id 645
    label "rewrite"
  ]
  node [
    id 646
    label "zrzec_si&#281;"
  ]
  node [
    id 647
    label "testament"
  ]
  node [
    id 648
    label "skopiowa&#263;"
  ]
  node [
    id 649
    label "zadanie"
  ]
  node [
    id 650
    label "lekarstwo"
  ]
  node [
    id 651
    label "przenie&#347;&#263;"
  ]
  node [
    id 652
    label "ucze&#324;"
  ]
  node [
    id 653
    label "student"
  ]
  node [
    id 654
    label "cz&#322;owiek"
  ]
  node [
    id 655
    label "zaliczy&#263;"
  ]
  node [
    id 656
    label "powierzy&#263;"
  ]
  node [
    id 657
    label "zmusi&#263;"
  ]
  node [
    id 658
    label "translate"
  ]
  node [
    id 659
    label "give"
  ]
  node [
    id 660
    label "picture"
  ]
  node [
    id 661
    label "przedstawi&#263;"
  ]
  node [
    id 662
    label "z&#322;o&#380;y&#263;_egzamin"
  ]
  node [
    id 663
    label "convey"
  ]
  node [
    id 664
    label "przekazanie"
  ]
  node [
    id 665
    label "skopiowanie"
  ]
  node [
    id 666
    label "arrangement"
  ]
  node [
    id 667
    label "przeniesienie"
  ]
  node [
    id 668
    label "answer"
  ]
  node [
    id 669
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 670
    label "transcription"
  ]
  node [
    id 671
    label "zalecenie"
  ]
  node [
    id 672
    label "fraza"
  ]
  node [
    id 673
    label "stanowisko"
  ]
  node [
    id 674
    label "wypowiedzenie"
  ]
  node [
    id 675
    label "prison_term"
  ]
  node [
    id 676
    label "okres"
  ]
  node [
    id 677
    label "przedstawienie"
  ]
  node [
    id 678
    label "wyra&#380;enie"
  ]
  node [
    id 679
    label "zaliczenie"
  ]
  node [
    id 680
    label "antylogizm"
  ]
  node [
    id 681
    label "zmuszenie"
  ]
  node [
    id 682
    label "konektyw"
  ]
  node [
    id 683
    label "attitude"
  ]
  node [
    id 684
    label "powierzenie"
  ]
  node [
    id 685
    label "adjudication"
  ]
  node [
    id 686
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 687
    label "political_orientation"
  ]
  node [
    id 688
    label "idea"
  ]
  node [
    id 689
    label "stra&#380;nik"
  ]
  node [
    id 690
    label "przedszkole"
  ]
  node [
    id 691
    label "opiekun"
  ]
  node [
    id 692
    label "ruch"
  ]
  node [
    id 693
    label "rozmiar&#243;wka"
  ]
  node [
    id 694
    label "p&#322;aszczyzna"
  ]
  node [
    id 695
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 696
    label "tarcza"
  ]
  node [
    id 697
    label "kosz"
  ]
  node [
    id 698
    label "transparent"
  ]
  node [
    id 699
    label "uk&#322;ad"
  ]
  node [
    id 700
    label "rubryka"
  ]
  node [
    id 701
    label "kontener"
  ]
  node [
    id 702
    label "spis"
  ]
  node [
    id 703
    label "plate"
  ]
  node [
    id 704
    label "konstrukcja"
  ]
  node [
    id 705
    label "szachownica_Punnetta"
  ]
  node [
    id 706
    label "chart"
  ]
  node [
    id 707
    label "izba"
  ]
  node [
    id 708
    label "biurko"
  ]
  node [
    id 709
    label "boks"
  ]
  node [
    id 710
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 711
    label "egzekutywa"
  ]
  node [
    id 712
    label "premier"
  ]
  node [
    id 713
    label "Londyn"
  ]
  node [
    id 714
    label "palestra"
  ]
  node [
    id 715
    label "pok&#243;j"
  ]
  node [
    id 716
    label "pracownia"
  ]
  node [
    id 717
    label "gabinet_cieni"
  ]
  node [
    id 718
    label "pomieszczenie"
  ]
  node [
    id 719
    label "Konsulat"
  ]
  node [
    id 720
    label "wagon"
  ]
  node [
    id 721
    label "mecz_mistrzowski"
  ]
  node [
    id 722
    label "class"
  ]
  node [
    id 723
    label "&#322;awka"
  ]
  node [
    id 724
    label "wykrzyknik"
  ]
  node [
    id 725
    label "zaleta"
  ]
  node [
    id 726
    label "programowanie_obiektowe"
  ]
  node [
    id 727
    label "warstwa"
  ]
  node [
    id 728
    label "rezerwa"
  ]
  node [
    id 729
    label "Ekwici"
  ]
  node [
    id 730
    label "&#347;rodowisko"
  ]
  node [
    id 731
    label "sala"
  ]
  node [
    id 732
    label "pomoc"
  ]
  node [
    id 733
    label "jako&#347;&#263;"
  ]
  node [
    id 734
    label "znak_jako&#347;ci"
  ]
  node [
    id 735
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 736
    label "poziom"
  ]
  node [
    id 737
    label "promocja"
  ]
  node [
    id 738
    label "kurs"
  ]
  node [
    id 739
    label "obiekt"
  ]
  node [
    id 740
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 741
    label "dziennik_lekcyjny"
  ]
  node [
    id 742
    label "typ"
  ]
  node [
    id 743
    label "fakcja"
  ]
  node [
    id 744
    label "obrona"
  ]
  node [
    id 745
    label "atak"
  ]
  node [
    id 746
    label "botanika"
  ]
  node [
    id 747
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 748
    label "Wallenrod"
  ]
  node [
    id 749
    label "mie&#263;_miejsce"
  ]
  node [
    id 750
    label "by&#263;"
  ]
  node [
    id 751
    label "nabywa&#263;"
  ]
  node [
    id 752
    label "uzyskiwa&#263;"
  ]
  node [
    id 753
    label "bra&#263;"
  ]
  node [
    id 754
    label "winnings"
  ]
  node [
    id 755
    label "opanowywa&#263;"
  ]
  node [
    id 756
    label "si&#281;ga&#263;"
  ]
  node [
    id 757
    label "otrzymywa&#263;"
  ]
  node [
    id 758
    label "range"
  ]
  node [
    id 759
    label "wystarcza&#263;"
  ]
  node [
    id 760
    label "kupowa&#263;"
  ]
  node [
    id 761
    label "obskakiwa&#263;"
  ]
  node [
    id 762
    label "return"
  ]
  node [
    id 763
    label "wytwarza&#263;"
  ]
  node [
    id 764
    label "kupywa&#263;"
  ]
  node [
    id 765
    label "pozyskiwa&#263;"
  ]
  node [
    id 766
    label "przyjmowa&#263;"
  ]
  node [
    id 767
    label "ustawia&#263;"
  ]
  node [
    id 768
    label "get"
  ]
  node [
    id 769
    label "gra&#263;"
  ]
  node [
    id 770
    label "uznawa&#263;"
  ]
  node [
    id 771
    label "wierzy&#263;"
  ]
  node [
    id 772
    label "compass"
  ]
  node [
    id 773
    label "korzysta&#263;"
  ]
  node [
    id 774
    label "appreciation"
  ]
  node [
    id 775
    label "osi&#261;ga&#263;"
  ]
  node [
    id 776
    label "dociera&#263;"
  ]
  node [
    id 777
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 778
    label "mierzy&#263;"
  ]
  node [
    id 779
    label "u&#380;ywa&#263;"
  ]
  node [
    id 780
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 781
    label "exsert"
  ]
  node [
    id 782
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 783
    label "equal"
  ]
  node [
    id 784
    label "trwa&#263;"
  ]
  node [
    id 785
    label "chodzi&#263;"
  ]
  node [
    id 786
    label "stan"
  ]
  node [
    id 787
    label "obecno&#347;&#263;"
  ]
  node [
    id 788
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 789
    label "uczestniczy&#263;"
  ]
  node [
    id 790
    label "mark"
  ]
  node [
    id 791
    label "powodowa&#263;"
  ]
  node [
    id 792
    label "robi&#263;"
  ]
  node [
    id 793
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 794
    label "porywa&#263;"
  ]
  node [
    id 795
    label "wchodzi&#263;"
  ]
  node [
    id 796
    label "poczytywa&#263;"
  ]
  node [
    id 797
    label "levy"
  ]
  node [
    id 798
    label "wk&#322;ada&#263;"
  ]
  node [
    id 799
    label "raise"
  ]
  node [
    id 800
    label "pokonywa&#263;"
  ]
  node [
    id 801
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 802
    label "rucha&#263;"
  ]
  node [
    id 803
    label "prowadzi&#263;"
  ]
  node [
    id 804
    label "za&#380;ywa&#263;"
  ]
  node [
    id 805
    label "&#263;pa&#263;"
  ]
  node [
    id 806
    label "interpretowa&#263;"
  ]
  node [
    id 807
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 808
    label "rusza&#263;"
  ]
  node [
    id 809
    label "chwyta&#263;"
  ]
  node [
    id 810
    label "grza&#263;"
  ]
  node [
    id 811
    label "wch&#322;ania&#263;"
  ]
  node [
    id 812
    label "wygrywa&#263;"
  ]
  node [
    id 813
    label "ucieka&#263;"
  ]
  node [
    id 814
    label "arise"
  ]
  node [
    id 815
    label "uprawia&#263;_seks"
  ]
  node [
    id 816
    label "abstract"
  ]
  node [
    id 817
    label "towarzystwo"
  ]
  node [
    id 818
    label "atakowa&#263;"
  ]
  node [
    id 819
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 820
    label "zalicza&#263;"
  ]
  node [
    id 821
    label "open"
  ]
  node [
    id 822
    label "wzi&#261;&#263;"
  ]
  node [
    id 823
    label "&#322;apa&#263;"
  ]
  node [
    id 824
    label "przewa&#380;a&#263;"
  ]
  node [
    id 825
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 826
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 827
    label "okrada&#263;"
  ]
  node [
    id 828
    label "obiega&#263;"
  ]
  node [
    id 829
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 830
    label "op&#281;dza&#263;"
  ]
  node [
    id 831
    label "osacza&#263;"
  ]
  node [
    id 832
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 833
    label "environment"
  ]
  node [
    id 834
    label "radzi&#263;_sobie"
  ]
  node [
    id 835
    label "manipulate"
  ]
  node [
    id 836
    label "niewoli&#263;"
  ]
  node [
    id 837
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 838
    label "powstrzymywa&#263;"
  ]
  node [
    id 839
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 840
    label "meet"
  ]
  node [
    id 841
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 842
    label "rede"
  ]
  node [
    id 843
    label "zaspokaja&#263;"
  ]
  node [
    id 844
    label "suffice"
  ]
  node [
    id 845
    label "stawa&#263;"
  ]
  node [
    id 846
    label "&#347;wiadczenie"
  ]
  node [
    id 847
    label "czynienie_dobra"
  ]
  node [
    id 848
    label "zobowi&#261;zanie"
  ]
  node [
    id 849
    label "p&#322;acenie"
  ]
  node [
    id 850
    label "wyraz"
  ]
  node [
    id 851
    label "koszt_rodzajowy"
  ]
  node [
    id 852
    label "service"
  ]
  node [
    id 853
    label "us&#322;uga"
  ]
  node [
    id 854
    label "przekonywanie"
  ]
  node [
    id 855
    label "command"
  ]
  node [
    id 856
    label "performance"
  ]
  node [
    id 857
    label "opowiadanie"
  ]
  node [
    id 858
    label "s&#322;owotw&#243;rczy"
  ]
  node [
    id 859
    label "motywacyjnie"
  ]
  node [
    id 860
    label "s&#322;owotw&#243;rczo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 3
    target 581
  ]
  edge [
    source 3
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 3
    target 586
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 588
  ]
  edge [
    source 3
    target 589
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 590
  ]
  edge [
    source 3
    target 591
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 592
  ]
  edge [
    source 3
    target 593
  ]
  edge [
    source 3
    target 594
  ]
  edge [
    source 3
    target 595
  ]
  edge [
    source 3
    target 596
  ]
  edge [
    source 3
    target 597
  ]
  edge [
    source 3
    target 598
  ]
  edge [
    source 3
    target 599
  ]
  edge [
    source 3
    target 600
  ]
  edge [
    source 3
    target 601
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 602
  ]
  edge [
    source 3
    target 603
  ]
  edge [
    source 3
    target 604
  ]
  edge [
    source 3
    target 605
  ]
  edge [
    source 3
    target 606
  ]
  edge [
    source 3
    target 607
  ]
  edge [
    source 3
    target 608
  ]
  edge [
    source 3
    target 609
  ]
  edge [
    source 3
    target 610
  ]
  edge [
    source 3
    target 611
  ]
  edge [
    source 3
    target 612
  ]
  edge [
    source 3
    target 613
  ]
  edge [
    source 3
    target 614
  ]
  edge [
    source 3
    target 615
  ]
  edge [
    source 3
    target 616
  ]
  edge [
    source 3
    target 617
  ]
  edge [
    source 3
    target 618
  ]
  edge [
    source 3
    target 619
  ]
  edge [
    source 3
    target 620
  ]
  edge [
    source 3
    target 621
  ]
  edge [
    source 3
    target 622
  ]
  edge [
    source 3
    target 623
  ]
  edge [
    source 3
    target 624
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 625
  ]
  edge [
    source 3
    target 626
  ]
  edge [
    source 3
    target 627
  ]
  edge [
    source 3
    target 628
  ]
  edge [
    source 3
    target 629
  ]
  edge [
    source 3
    target 630
  ]
  edge [
    source 3
    target 631
  ]
  edge [
    source 3
    target 632
  ]
  edge [
    source 3
    target 633
  ]
  edge [
    source 3
    target 634
  ]
  edge [
    source 3
    target 635
  ]
  edge [
    source 3
    target 636
  ]
  edge [
    source 3
    target 637
  ]
  edge [
    source 3
    target 638
  ]
  edge [
    source 3
    target 639
  ]
  edge [
    source 3
    target 640
  ]
  edge [
    source 3
    target 641
  ]
  edge [
    source 3
    target 642
  ]
  edge [
    source 3
    target 643
  ]
  edge [
    source 3
    target 644
  ]
  edge [
    source 3
    target 645
  ]
  edge [
    source 3
    target 646
  ]
  edge [
    source 3
    target 647
  ]
  edge [
    source 3
    target 648
  ]
  edge [
    source 3
    target 649
  ]
  edge [
    source 3
    target 650
  ]
  edge [
    source 3
    target 651
  ]
  edge [
    source 3
    target 652
  ]
  edge [
    source 3
    target 653
  ]
  edge [
    source 3
    target 654
  ]
  edge [
    source 3
    target 655
  ]
  edge [
    source 3
    target 656
  ]
  edge [
    source 3
    target 657
  ]
  edge [
    source 3
    target 658
  ]
  edge [
    source 3
    target 659
  ]
  edge [
    source 3
    target 660
  ]
  edge [
    source 3
    target 661
  ]
  edge [
    source 3
    target 662
  ]
  edge [
    source 3
    target 663
  ]
  edge [
    source 3
    target 664
  ]
  edge [
    source 3
    target 665
  ]
  edge [
    source 3
    target 666
  ]
  edge [
    source 3
    target 667
  ]
  edge [
    source 3
    target 668
  ]
  edge [
    source 3
    target 669
  ]
  edge [
    source 3
    target 670
  ]
  edge [
    source 3
    target 671
  ]
  edge [
    source 3
    target 672
  ]
  edge [
    source 3
    target 673
  ]
  edge [
    source 3
    target 674
  ]
  edge [
    source 3
    target 675
  ]
  edge [
    source 3
    target 676
  ]
  edge [
    source 3
    target 677
  ]
  edge [
    source 3
    target 678
  ]
  edge [
    source 3
    target 679
  ]
  edge [
    source 3
    target 680
  ]
  edge [
    source 3
    target 681
  ]
  edge [
    source 3
    target 682
  ]
  edge [
    source 3
    target 683
  ]
  edge [
    source 3
    target 684
  ]
  edge [
    source 3
    target 685
  ]
  edge [
    source 3
    target 686
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 687
  ]
  edge [
    source 3
    target 688
  ]
  edge [
    source 3
    target 689
  ]
  edge [
    source 3
    target 690
  ]
  edge [
    source 3
    target 691
  ]
  edge [
    source 3
    target 692
  ]
  edge [
    source 3
    target 693
  ]
  edge [
    source 3
    target 694
  ]
  edge [
    source 3
    target 695
  ]
  edge [
    source 3
    target 696
  ]
  edge [
    source 3
    target 697
  ]
  edge [
    source 3
    target 698
  ]
  edge [
    source 3
    target 699
  ]
  edge [
    source 3
    target 700
  ]
  edge [
    source 3
    target 701
  ]
  edge [
    source 3
    target 702
  ]
  edge [
    source 3
    target 703
  ]
  edge [
    source 3
    target 704
  ]
  edge [
    source 3
    target 705
  ]
  edge [
    source 3
    target 706
  ]
  edge [
    source 3
    target 707
  ]
  edge [
    source 3
    target 708
  ]
  edge [
    source 3
    target 709
  ]
  edge [
    source 3
    target 710
  ]
  edge [
    source 3
    target 711
  ]
  edge [
    source 3
    target 712
  ]
  edge [
    source 3
    target 713
  ]
  edge [
    source 3
    target 714
  ]
  edge [
    source 3
    target 715
  ]
  edge [
    source 3
    target 716
  ]
  edge [
    source 3
    target 717
  ]
  edge [
    source 3
    target 718
  ]
  edge [
    source 3
    target 719
  ]
  edge [
    source 3
    target 720
  ]
  edge [
    source 3
    target 721
  ]
  edge [
    source 3
    target 722
  ]
  edge [
    source 3
    target 723
  ]
  edge [
    source 3
    target 724
  ]
  edge [
    source 3
    target 725
  ]
  edge [
    source 3
    target 726
  ]
  edge [
    source 3
    target 727
  ]
  edge [
    source 3
    target 728
  ]
  edge [
    source 3
    target 729
  ]
  edge [
    source 3
    target 730
  ]
  edge [
    source 3
    target 731
  ]
  edge [
    source 3
    target 732
  ]
  edge [
    source 3
    target 733
  ]
  edge [
    source 3
    target 734
  ]
  edge [
    source 3
    target 735
  ]
  edge [
    source 3
    target 736
  ]
  edge [
    source 3
    target 737
  ]
  edge [
    source 3
    target 738
  ]
  edge [
    source 3
    target 739
  ]
  edge [
    source 3
    target 740
  ]
  edge [
    source 3
    target 741
  ]
  edge [
    source 3
    target 742
  ]
  edge [
    source 3
    target 743
  ]
  edge [
    source 3
    target 744
  ]
  edge [
    source 3
    target 745
  ]
  edge [
    source 3
    target 746
  ]
  edge [
    source 3
    target 747
  ]
  edge [
    source 3
    target 748
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 749
  ]
  edge [
    source 4
    target 750
  ]
  edge [
    source 4
    target 751
  ]
  edge [
    source 4
    target 752
  ]
  edge [
    source 4
    target 753
  ]
  edge [
    source 4
    target 754
  ]
  edge [
    source 4
    target 755
  ]
  edge [
    source 4
    target 756
  ]
  edge [
    source 4
    target 757
  ]
  edge [
    source 4
    target 758
  ]
  edge [
    source 4
    target 759
  ]
  edge [
    source 4
    target 760
  ]
  edge [
    source 4
    target 761
  ]
  edge [
    source 4
    target 762
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 763
  ]
  edge [
    source 4
    target 764
  ]
  edge [
    source 4
    target 765
  ]
  edge [
    source 4
    target 766
  ]
  edge [
    source 4
    target 767
  ]
  edge [
    source 4
    target 768
  ]
  edge [
    source 4
    target 769
  ]
  edge [
    source 4
    target 770
  ]
  edge [
    source 4
    target 771
  ]
  edge [
    source 4
    target 772
  ]
  edge [
    source 4
    target 773
  ]
  edge [
    source 4
    target 774
  ]
  edge [
    source 4
    target 775
  ]
  edge [
    source 4
    target 776
  ]
  edge [
    source 4
    target 777
  ]
  edge [
    source 4
    target 778
  ]
  edge [
    source 4
    target 779
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 780
  ]
  edge [
    source 4
    target 781
  ]
  edge [
    source 4
    target 782
  ]
  edge [
    source 4
    target 783
  ]
  edge [
    source 4
    target 784
  ]
  edge [
    source 4
    target 785
  ]
  edge [
    source 4
    target 786
  ]
  edge [
    source 4
    target 787
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 788
  ]
  edge [
    source 4
    target 789
  ]
  edge [
    source 4
    target 790
  ]
  edge [
    source 4
    target 791
  ]
  edge [
    source 4
    target 792
  ]
  edge [
    source 4
    target 793
  ]
  edge [
    source 4
    target 794
  ]
  edge [
    source 4
    target 795
  ]
  edge [
    source 4
    target 796
  ]
  edge [
    source 4
    target 797
  ]
  edge [
    source 4
    target 798
  ]
  edge [
    source 4
    target 799
  ]
  edge [
    source 4
    target 800
  ]
  edge [
    source 4
    target 801
  ]
  edge [
    source 4
    target 802
  ]
  edge [
    source 4
    target 803
  ]
  edge [
    source 4
    target 804
  ]
  edge [
    source 4
    target 805
  ]
  edge [
    source 4
    target 806
  ]
  edge [
    source 4
    target 807
  ]
  edge [
    source 4
    target 808
  ]
  edge [
    source 4
    target 809
  ]
  edge [
    source 4
    target 810
  ]
  edge [
    source 4
    target 811
  ]
  edge [
    source 4
    target 812
  ]
  edge [
    source 4
    target 813
  ]
  edge [
    source 4
    target 814
  ]
  edge [
    source 4
    target 815
  ]
  edge [
    source 4
    target 816
  ]
  edge [
    source 4
    target 817
  ]
  edge [
    source 4
    target 818
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 819
  ]
  edge [
    source 4
    target 820
  ]
  edge [
    source 4
    target 821
  ]
  edge [
    source 4
    target 822
  ]
  edge [
    source 4
    target 823
  ]
  edge [
    source 4
    target 824
  ]
  edge [
    source 4
    target 825
  ]
  edge [
    source 4
    target 826
  ]
  edge [
    source 4
    target 827
  ]
  edge [
    source 4
    target 828
  ]
  edge [
    source 4
    target 829
  ]
  edge [
    source 4
    target 830
  ]
  edge [
    source 4
    target 831
  ]
  edge [
    source 4
    target 832
  ]
  edge [
    source 4
    target 833
  ]
  edge [
    source 4
    target 834
  ]
  edge [
    source 4
    target 835
  ]
  edge [
    source 4
    target 836
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 837
  ]
  edge [
    source 4
    target 838
  ]
  edge [
    source 4
    target 839
  ]
  edge [
    source 4
    target 840
  ]
  edge [
    source 4
    target 841
  ]
  edge [
    source 4
    target 842
  ]
  edge [
    source 4
    target 843
  ]
  edge [
    source 4
    target 844
  ]
  edge [
    source 4
    target 845
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 846
  ]
  edge [
    source 5
    target 847
  ]
  edge [
    source 5
    target 848
  ]
  edge [
    source 5
    target 849
  ]
  edge [
    source 5
    target 850
  ]
  edge [
    source 5
    target 851
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 852
  ]
  edge [
    source 5
    target 853
  ]
  edge [
    source 5
    target 854
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 855
  ]
  edge [
    source 5
    target 856
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 857
  ]
  edge [
    source 6
    target 858
  ]
  edge [
    source 6
    target 859
  ]
  edge [
    source 6
    target 860
  ]
]
