graph [
  node [
    id 0
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "tym"
    origin "text"
  ]
  node [
    id 2
    label "wszyscy"
    origin "text"
  ]
  node [
    id 3
    label "zastanawia&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "ile"
    origin "text"
  ]
  node [
    id 6
    label "powszechny"
    origin "text"
  ]
  node [
    id 7
    label "przyzwyczajenie"
    origin "text"
  ]
  node [
    id 8
    label "sprzeciw"
    origin "text"
  ]
  node [
    id 9
    label "wp&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 11
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "przykleja&#263;"
    origin "text"
  ]
  node [
    id 13
    label "blog"
    origin "text"
  ]
  node [
    id 14
    label "etykietka"
    origin "text"
  ]
  node [
    id 15
    label "wsp&#243;&#322;praca"
    origin "text"
  ]
  node [
    id 16
    label "dzielenie"
    origin "text"
  ]
  node [
    id 17
    label "formu&#322;owa&#263;"
  ]
  node [
    id 18
    label "ozdabia&#263;"
  ]
  node [
    id 19
    label "stawia&#263;"
  ]
  node [
    id 20
    label "spell"
  ]
  node [
    id 21
    label "styl"
  ]
  node [
    id 22
    label "skryba"
  ]
  node [
    id 23
    label "read"
  ]
  node [
    id 24
    label "donosi&#263;"
  ]
  node [
    id 25
    label "code"
  ]
  node [
    id 26
    label "tekst"
  ]
  node [
    id 27
    label "dysgrafia"
  ]
  node [
    id 28
    label "dysortografia"
  ]
  node [
    id 29
    label "tworzy&#263;"
  ]
  node [
    id 30
    label "prasa"
  ]
  node [
    id 31
    label "robi&#263;"
  ]
  node [
    id 32
    label "pope&#322;nia&#263;"
  ]
  node [
    id 33
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 34
    label "wytwarza&#263;"
  ]
  node [
    id 35
    label "get"
  ]
  node [
    id 36
    label "consist"
  ]
  node [
    id 37
    label "stanowi&#263;"
  ]
  node [
    id 38
    label "raise"
  ]
  node [
    id 39
    label "spill_the_beans"
  ]
  node [
    id 40
    label "przeby&#263;"
  ]
  node [
    id 41
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 42
    label "zanosi&#263;"
  ]
  node [
    id 43
    label "inform"
  ]
  node [
    id 44
    label "give"
  ]
  node [
    id 45
    label "zu&#380;y&#263;"
  ]
  node [
    id 46
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 47
    label "introduce"
  ]
  node [
    id 48
    label "render"
  ]
  node [
    id 49
    label "ci&#261;&#380;a"
  ]
  node [
    id 50
    label "informowa&#263;"
  ]
  node [
    id 51
    label "komunikowa&#263;"
  ]
  node [
    id 52
    label "convey"
  ]
  node [
    id 53
    label "pozostawia&#263;"
  ]
  node [
    id 54
    label "czyni&#263;"
  ]
  node [
    id 55
    label "wydawa&#263;"
  ]
  node [
    id 56
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 57
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 58
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 59
    label "przewidywa&#263;"
  ]
  node [
    id 60
    label "przyznawa&#263;"
  ]
  node [
    id 61
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 62
    label "go"
  ]
  node [
    id 63
    label "obstawia&#263;"
  ]
  node [
    id 64
    label "umieszcza&#263;"
  ]
  node [
    id 65
    label "ocenia&#263;"
  ]
  node [
    id 66
    label "zastawia&#263;"
  ]
  node [
    id 67
    label "stanowisko"
  ]
  node [
    id 68
    label "znak"
  ]
  node [
    id 69
    label "wskazywa&#263;"
  ]
  node [
    id 70
    label "uruchamia&#263;"
  ]
  node [
    id 71
    label "fundowa&#263;"
  ]
  node [
    id 72
    label "zmienia&#263;"
  ]
  node [
    id 73
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 74
    label "deliver"
  ]
  node [
    id 75
    label "powodowa&#263;"
  ]
  node [
    id 76
    label "wyznacza&#263;"
  ]
  node [
    id 77
    label "przedstawia&#263;"
  ]
  node [
    id 78
    label "wydobywa&#263;"
  ]
  node [
    id 79
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 80
    label "trim"
  ]
  node [
    id 81
    label "gryzipi&#243;rek"
  ]
  node [
    id 82
    label "cz&#322;owiek"
  ]
  node [
    id 83
    label "pisarz"
  ]
  node [
    id 84
    label "ekscerpcja"
  ]
  node [
    id 85
    label "j&#281;zykowo"
  ]
  node [
    id 86
    label "wypowied&#378;"
  ]
  node [
    id 87
    label "redakcja"
  ]
  node [
    id 88
    label "wytw&#243;r"
  ]
  node [
    id 89
    label "pomini&#281;cie"
  ]
  node [
    id 90
    label "dzie&#322;o"
  ]
  node [
    id 91
    label "preparacja"
  ]
  node [
    id 92
    label "odmianka"
  ]
  node [
    id 93
    label "opu&#347;ci&#263;"
  ]
  node [
    id 94
    label "koniektura"
  ]
  node [
    id 95
    label "obelga"
  ]
  node [
    id 96
    label "zesp&#243;&#322;"
  ]
  node [
    id 97
    label "t&#322;oczysko"
  ]
  node [
    id 98
    label "depesza"
  ]
  node [
    id 99
    label "maszyna"
  ]
  node [
    id 100
    label "media"
  ]
  node [
    id 101
    label "napisa&#263;"
  ]
  node [
    id 102
    label "czasopismo"
  ]
  node [
    id 103
    label "dziennikarz_prasowy"
  ]
  node [
    id 104
    label "kiosk"
  ]
  node [
    id 105
    label "maszyna_rolnicza"
  ]
  node [
    id 106
    label "gazeta"
  ]
  node [
    id 107
    label "dysleksja"
  ]
  node [
    id 108
    label "pisanie"
  ]
  node [
    id 109
    label "trzonek"
  ]
  node [
    id 110
    label "reakcja"
  ]
  node [
    id 111
    label "narz&#281;dzie"
  ]
  node [
    id 112
    label "spos&#243;b"
  ]
  node [
    id 113
    label "zbi&#243;r"
  ]
  node [
    id 114
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 115
    label "zachowanie"
  ]
  node [
    id 116
    label "stylik"
  ]
  node [
    id 117
    label "dyscyplina_sportowa"
  ]
  node [
    id 118
    label "handle"
  ]
  node [
    id 119
    label "stroke"
  ]
  node [
    id 120
    label "line"
  ]
  node [
    id 121
    label "charakter"
  ]
  node [
    id 122
    label "natural_language"
  ]
  node [
    id 123
    label "kanon"
  ]
  node [
    id 124
    label "behawior"
  ]
  node [
    id 125
    label "dysgraphia"
  ]
  node [
    id 126
    label "interesowa&#263;"
  ]
  node [
    id 127
    label "strike"
  ]
  node [
    id 128
    label "sake"
  ]
  node [
    id 129
    label "rozciekawia&#263;"
  ]
  node [
    id 130
    label "znany"
  ]
  node [
    id 131
    label "zbiorowy"
  ]
  node [
    id 132
    label "cz&#281;sty"
  ]
  node [
    id 133
    label "powszechnie"
  ]
  node [
    id 134
    label "generalny"
  ]
  node [
    id 135
    label "wsp&#243;lny"
  ]
  node [
    id 136
    label "zbiorowo"
  ]
  node [
    id 137
    label "og&#243;lnie"
  ]
  node [
    id 138
    label "zwierzchni"
  ]
  node [
    id 139
    label "porz&#261;dny"
  ]
  node [
    id 140
    label "nadrz&#281;dny"
  ]
  node [
    id 141
    label "podstawowy"
  ]
  node [
    id 142
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 143
    label "zasadniczy"
  ]
  node [
    id 144
    label "generalnie"
  ]
  node [
    id 145
    label "cz&#281;sto"
  ]
  node [
    id 146
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 147
    label "wielki"
  ]
  node [
    id 148
    label "rozpowszechnianie"
  ]
  node [
    id 149
    label "og&#243;lny"
  ]
  node [
    id 150
    label "oddzia&#322;anie"
  ]
  node [
    id 151
    label "nawyknienie"
  ]
  node [
    id 152
    label "use"
  ]
  node [
    id 153
    label "zwyczaj"
  ]
  node [
    id 154
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 155
    label "kultura_duchowa"
  ]
  node [
    id 156
    label "kultura"
  ]
  node [
    id 157
    label "ceremony"
  ]
  node [
    id 158
    label "reply"
  ]
  node [
    id 159
    label "zahipnotyzowanie"
  ]
  node [
    id 160
    label "spowodowanie"
  ]
  node [
    id 161
    label "zdarzenie_si&#281;"
  ]
  node [
    id 162
    label "chemia"
  ]
  node [
    id 163
    label "wdarcie_si&#281;"
  ]
  node [
    id 164
    label "dotarcie"
  ]
  node [
    id 165
    label "reakcja_chemiczna"
  ]
  node [
    id 166
    label "czynno&#347;&#263;"
  ]
  node [
    id 167
    label "nawyk"
  ]
  node [
    id 168
    label "czerwona_kartka"
  ]
  node [
    id 169
    label "protestacja"
  ]
  node [
    id 170
    label "react"
  ]
  node [
    id 171
    label "reaction"
  ]
  node [
    id 172
    label "organizm"
  ]
  node [
    id 173
    label "rozmowa"
  ]
  node [
    id 174
    label "response"
  ]
  node [
    id 175
    label "rezultat"
  ]
  node [
    id 176
    label "respondent"
  ]
  node [
    id 177
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 178
    label "pour"
  ]
  node [
    id 179
    label "zasila&#263;"
  ]
  node [
    id 180
    label "determine"
  ]
  node [
    id 181
    label "ciek_wodny"
  ]
  node [
    id 182
    label "kapita&#322;"
  ]
  node [
    id 183
    label "work"
  ]
  node [
    id 184
    label "dochodzi&#263;"
  ]
  node [
    id 185
    label "przyp&#322;ywa&#263;"
  ]
  node [
    id 186
    label "flow"
  ]
  node [
    id 187
    label "nail"
  ]
  node [
    id 188
    label "statek"
  ]
  node [
    id 189
    label "dociera&#263;"
  ]
  node [
    id 190
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 191
    label "przybywa&#263;"
  ]
  node [
    id 192
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 193
    label "uzyskiwa&#263;"
  ]
  node [
    id 194
    label "claim"
  ]
  node [
    id 195
    label "osi&#261;ga&#263;"
  ]
  node [
    id 196
    label "ripen"
  ]
  node [
    id 197
    label "supervene"
  ]
  node [
    id 198
    label "doczeka&#263;"
  ]
  node [
    id 199
    label "przesy&#322;ka"
  ]
  node [
    id 200
    label "doznawa&#263;"
  ]
  node [
    id 201
    label "reach"
  ]
  node [
    id 202
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 203
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 204
    label "zachodzi&#263;"
  ]
  node [
    id 205
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 206
    label "postrzega&#263;"
  ]
  node [
    id 207
    label "orgazm"
  ]
  node [
    id 208
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 209
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 210
    label "dokoptowywa&#263;"
  ]
  node [
    id 211
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 212
    label "dolatywa&#263;"
  ]
  node [
    id 213
    label "submit"
  ]
  node [
    id 214
    label "dostarcza&#263;"
  ]
  node [
    id 215
    label "digest"
  ]
  node [
    id 216
    label "absolutorium"
  ]
  node [
    id 217
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 218
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 219
    label "&#347;rodowisko"
  ]
  node [
    id 220
    label "nap&#322;ywanie"
  ]
  node [
    id 221
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 222
    label "zaleta"
  ]
  node [
    id 223
    label "mienie"
  ]
  node [
    id 224
    label "podupadanie"
  ]
  node [
    id 225
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 226
    label "podupada&#263;"
  ]
  node [
    id 227
    label "kwestor"
  ]
  node [
    id 228
    label "zas&#243;b"
  ]
  node [
    id 229
    label "supernadz&#243;r"
  ]
  node [
    id 230
    label "uruchomienie"
  ]
  node [
    id 231
    label "kapitalista"
  ]
  node [
    id 232
    label "uruchamianie"
  ]
  node [
    id 233
    label "czynnik_produkcji"
  ]
  node [
    id 234
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 235
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 236
    label "strategia"
  ]
  node [
    id 237
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 238
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 239
    label "plan"
  ]
  node [
    id 240
    label "operacja"
  ]
  node [
    id 241
    label "metoda"
  ]
  node [
    id 242
    label "gra"
  ]
  node [
    id 243
    label "pocz&#261;tki"
  ]
  node [
    id 244
    label "wzorzec_projektowy"
  ]
  node [
    id 245
    label "dziedzina"
  ]
  node [
    id 246
    label "program"
  ]
  node [
    id 247
    label "doktryna"
  ]
  node [
    id 248
    label "wrinkle"
  ]
  node [
    id 249
    label "dokument"
  ]
  node [
    id 250
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 251
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 252
    label "dzia&#322;anie"
  ]
  node [
    id 253
    label "activity"
  ]
  node [
    id 254
    label "przymocowywa&#263;"
  ]
  node [
    id 255
    label "stick"
  ]
  node [
    id 256
    label "pin"
  ]
  node [
    id 257
    label "komcio"
  ]
  node [
    id 258
    label "blogosfera"
  ]
  node [
    id 259
    label "pami&#281;tnik"
  ]
  node [
    id 260
    label "strona"
  ]
  node [
    id 261
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 262
    label "pami&#261;tka"
  ]
  node [
    id 263
    label "notes"
  ]
  node [
    id 264
    label "zapiski"
  ]
  node [
    id 265
    label "raptularz"
  ]
  node [
    id 266
    label "album"
  ]
  node [
    id 267
    label "utw&#243;r_epicki"
  ]
  node [
    id 268
    label "kartka"
  ]
  node [
    id 269
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 270
    label "logowanie"
  ]
  node [
    id 271
    label "plik"
  ]
  node [
    id 272
    label "s&#261;d"
  ]
  node [
    id 273
    label "adres_internetowy"
  ]
  node [
    id 274
    label "linia"
  ]
  node [
    id 275
    label "serwis_internetowy"
  ]
  node [
    id 276
    label "posta&#263;"
  ]
  node [
    id 277
    label "bok"
  ]
  node [
    id 278
    label "skr&#281;canie"
  ]
  node [
    id 279
    label "skr&#281;ca&#263;"
  ]
  node [
    id 280
    label "orientowanie"
  ]
  node [
    id 281
    label "skr&#281;ci&#263;"
  ]
  node [
    id 282
    label "uj&#281;cie"
  ]
  node [
    id 283
    label "zorientowanie"
  ]
  node [
    id 284
    label "ty&#322;"
  ]
  node [
    id 285
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 286
    label "fragment"
  ]
  node [
    id 287
    label "layout"
  ]
  node [
    id 288
    label "obiekt"
  ]
  node [
    id 289
    label "zorientowa&#263;"
  ]
  node [
    id 290
    label "pagina"
  ]
  node [
    id 291
    label "podmiot"
  ]
  node [
    id 292
    label "g&#243;ra"
  ]
  node [
    id 293
    label "orientowa&#263;"
  ]
  node [
    id 294
    label "voice"
  ]
  node [
    id 295
    label "orientacja"
  ]
  node [
    id 296
    label "prz&#243;d"
  ]
  node [
    id 297
    label "internet"
  ]
  node [
    id 298
    label "powierzchnia"
  ]
  node [
    id 299
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 300
    label "forma"
  ]
  node [
    id 301
    label "skr&#281;cenie"
  ]
  node [
    id 302
    label "komentarz"
  ]
  node [
    id 303
    label "tab"
  ]
  node [
    id 304
    label "naklejka"
  ]
  node [
    id 305
    label "tabliczka"
  ]
  node [
    id 306
    label "exlibris"
  ]
  node [
    id 307
    label "przedmiot"
  ]
  node [
    id 308
    label "plate"
  ]
  node [
    id 309
    label "tablica"
  ]
  node [
    id 310
    label "wsp&#243;&#322;pracownictwo"
  ]
  node [
    id 311
    label "dzielnik"
  ]
  node [
    id 312
    label "stosowanie"
  ]
  node [
    id 313
    label "liczenie"
  ]
  node [
    id 314
    label "dzielna"
  ]
  node [
    id 315
    label "powodowanie"
  ]
  node [
    id 316
    label "przeszkoda"
  ]
  node [
    id 317
    label "rozprowadzanie"
  ]
  node [
    id 318
    label "robienie"
  ]
  node [
    id 319
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 320
    label "wyodr&#281;bnianie"
  ]
  node [
    id 321
    label "rozdawanie"
  ]
  node [
    id 322
    label "contribution"
  ]
  node [
    id 323
    label "division"
  ]
  node [
    id 324
    label "iloraz"
  ]
  node [
    id 325
    label "sk&#322;&#243;canie"
  ]
  node [
    id 326
    label "separation"
  ]
  node [
    id 327
    label "dzielenie_si&#281;"
  ]
  node [
    id 328
    label "je&#378;dziectwo"
  ]
  node [
    id 329
    label "obstruction"
  ]
  node [
    id 330
    label "trudno&#347;&#263;"
  ]
  node [
    id 331
    label "bieg_prze&#322;ajowy"
  ]
  node [
    id 332
    label "podzielenie"
  ]
  node [
    id 333
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 334
    label "dzieli&#263;"
  ]
  node [
    id 335
    label "quotient"
  ]
  node [
    id 336
    label "kreska"
  ]
  node [
    id 337
    label "znak_pisarski"
  ]
  node [
    id 338
    label "hyphen"
  ]
  node [
    id 339
    label "effusion"
  ]
  node [
    id 340
    label "oddzielanie"
  ]
  node [
    id 341
    label "elimination"
  ]
  node [
    id 342
    label "wykrawanie"
  ]
  node [
    id 343
    label "oznaczanie"
  ]
  node [
    id 344
    label "osobny"
  ]
  node [
    id 345
    label "dawanie"
  ]
  node [
    id 346
    label "obdzielanie"
  ]
  node [
    id 347
    label "potlacz"
  ]
  node [
    id 348
    label "obdzielenie"
  ]
  node [
    id 349
    label "distribution"
  ]
  node [
    id 350
    label "wadzenie"
  ]
  node [
    id 351
    label "wa&#347;nienie"
  ]
  node [
    id 352
    label "badanie"
  ]
  node [
    id 353
    label "rachowanie"
  ]
  node [
    id 354
    label "dyskalkulia"
  ]
  node [
    id 355
    label "wynagrodzenie"
  ]
  node [
    id 356
    label "rozliczanie"
  ]
  node [
    id 357
    label "wymienianie"
  ]
  node [
    id 358
    label "wychodzenie"
  ]
  node [
    id 359
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 360
    label "naliczenie_si&#281;"
  ]
  node [
    id 361
    label "wyznaczanie"
  ]
  node [
    id 362
    label "dodawanie"
  ]
  node [
    id 363
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 364
    label "bang"
  ]
  node [
    id 365
    label "spodziewanie_si&#281;"
  ]
  node [
    id 366
    label "kwotowanie"
  ]
  node [
    id 367
    label "rozliczenie"
  ]
  node [
    id 368
    label "mierzenie"
  ]
  node [
    id 369
    label "count"
  ]
  node [
    id 370
    label "wycenianie"
  ]
  node [
    id 371
    label "branie"
  ]
  node [
    id 372
    label "sprowadzanie"
  ]
  node [
    id 373
    label "przeliczanie"
  ]
  node [
    id 374
    label "uwzgl&#281;dnianie"
  ]
  node [
    id 375
    label "odliczanie"
  ]
  node [
    id 376
    label "przeliczenie"
  ]
  node [
    id 377
    label "przejaskrawianie"
  ]
  node [
    id 378
    label "zniszczenie"
  ]
  node [
    id 379
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 380
    label "zu&#380;ywanie"
  ]
  node [
    id 381
    label "fabrication"
  ]
  node [
    id 382
    label "bycie"
  ]
  node [
    id 383
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 384
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 385
    label "creation"
  ]
  node [
    id 386
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 387
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 388
    label "act"
  ]
  node [
    id 389
    label "porobienie"
  ]
  node [
    id 390
    label "tentegowanie"
  ]
  node [
    id 391
    label "bezproblemowy"
  ]
  node [
    id 392
    label "wydarzenie"
  ]
  node [
    id 393
    label "cause"
  ]
  node [
    id 394
    label "causal_agent"
  ]
  node [
    id 395
    label "pokrywanie"
  ]
  node [
    id 396
    label "dostarczanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 254
  ]
  edge [
    source 12
    target 255
  ]
  edge [
    source 12
    target 256
  ]
  edge [
    source 12
    target 46
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 257
  ]
  edge [
    source 13
    target 258
  ]
  edge [
    source 13
    target 259
  ]
  edge [
    source 13
    target 260
  ]
  edge [
    source 13
    target 261
  ]
  edge [
    source 13
    target 262
  ]
  edge [
    source 13
    target 263
  ]
  edge [
    source 13
    target 264
  ]
  edge [
    source 13
    target 265
  ]
  edge [
    source 13
    target 266
  ]
  edge [
    source 13
    target 267
  ]
  edge [
    source 13
    target 268
  ]
  edge [
    source 13
    target 269
  ]
  edge [
    source 13
    target 270
  ]
  edge [
    source 13
    target 271
  ]
  edge [
    source 13
    target 272
  ]
  edge [
    source 13
    target 273
  ]
  edge [
    source 13
    target 274
  ]
  edge [
    source 13
    target 275
  ]
  edge [
    source 13
    target 276
  ]
  edge [
    source 13
    target 277
  ]
  edge [
    source 13
    target 278
  ]
  edge [
    source 13
    target 279
  ]
  edge [
    source 13
    target 280
  ]
  edge [
    source 13
    target 281
  ]
  edge [
    source 13
    target 282
  ]
  edge [
    source 13
    target 283
  ]
  edge [
    source 13
    target 284
  ]
  edge [
    source 13
    target 285
  ]
  edge [
    source 13
    target 286
  ]
  edge [
    source 13
    target 287
  ]
  edge [
    source 13
    target 288
  ]
  edge [
    source 13
    target 289
  ]
  edge [
    source 13
    target 290
  ]
  edge [
    source 13
    target 291
  ]
  edge [
    source 13
    target 292
  ]
  edge [
    source 13
    target 293
  ]
  edge [
    source 13
    target 294
  ]
  edge [
    source 13
    target 295
  ]
  edge [
    source 13
    target 296
  ]
  edge [
    source 13
    target 297
  ]
  edge [
    source 13
    target 298
  ]
  edge [
    source 13
    target 299
  ]
  edge [
    source 13
    target 300
  ]
  edge [
    source 13
    target 301
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 302
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 303
  ]
  edge [
    source 14
    target 304
  ]
  edge [
    source 14
    target 305
  ]
  edge [
    source 14
    target 306
  ]
  edge [
    source 14
    target 307
  ]
  edge [
    source 14
    target 308
  ]
  edge [
    source 14
    target 309
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 310
  ]
  edge [
    source 15
    target 237
  ]
  edge [
    source 16
    target 311
  ]
  edge [
    source 16
    target 312
  ]
  edge [
    source 16
    target 313
  ]
  edge [
    source 16
    target 314
  ]
  edge [
    source 16
    target 315
  ]
  edge [
    source 16
    target 316
  ]
  edge [
    source 16
    target 317
  ]
  edge [
    source 16
    target 318
  ]
  edge [
    source 16
    target 319
  ]
  edge [
    source 16
    target 320
  ]
  edge [
    source 16
    target 321
  ]
  edge [
    source 16
    target 322
  ]
  edge [
    source 16
    target 323
  ]
  edge [
    source 16
    target 324
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 325
  ]
  edge [
    source 16
    target 326
  ]
  edge [
    source 16
    target 327
  ]
  edge [
    source 16
    target 328
  ]
  edge [
    source 16
    target 329
  ]
  edge [
    source 16
    target 330
  ]
  edge [
    source 16
    target 331
  ]
  edge [
    source 16
    target 332
  ]
  edge [
    source 16
    target 333
  ]
  edge [
    source 16
    target 334
  ]
  edge [
    source 16
    target 335
  ]
  edge [
    source 16
    target 336
  ]
  edge [
    source 16
    target 337
  ]
  edge [
    source 16
    target 338
  ]
  edge [
    source 16
    target 339
  ]
  edge [
    source 16
    target 340
  ]
  edge [
    source 16
    target 341
  ]
  edge [
    source 16
    target 342
  ]
  edge [
    source 16
    target 343
  ]
  edge [
    source 16
    target 344
  ]
  edge [
    source 16
    target 345
  ]
  edge [
    source 16
    target 346
  ]
  edge [
    source 16
    target 347
  ]
  edge [
    source 16
    target 348
  ]
  edge [
    source 16
    target 349
  ]
  edge [
    source 16
    target 350
  ]
  edge [
    source 16
    target 351
  ]
  edge [
    source 16
    target 352
  ]
  edge [
    source 16
    target 353
  ]
  edge [
    source 16
    target 354
  ]
  edge [
    source 16
    target 355
  ]
  edge [
    source 16
    target 356
  ]
  edge [
    source 16
    target 357
  ]
  edge [
    source 16
    target 358
  ]
  edge [
    source 16
    target 359
  ]
  edge [
    source 16
    target 360
  ]
  edge [
    source 16
    target 361
  ]
  edge [
    source 16
    target 362
  ]
  edge [
    source 16
    target 363
  ]
  edge [
    source 16
    target 364
  ]
  edge [
    source 16
    target 365
  ]
  edge [
    source 16
    target 366
  ]
  edge [
    source 16
    target 367
  ]
  edge [
    source 16
    target 368
  ]
  edge [
    source 16
    target 369
  ]
  edge [
    source 16
    target 370
  ]
  edge [
    source 16
    target 371
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 374
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 376
  ]
  edge [
    source 16
    target 377
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 16
    target 379
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 307
  ]
  edge [
    source 16
    target 382
  ]
  edge [
    source 16
    target 383
  ]
  edge [
    source 16
    target 384
  ]
  edge [
    source 16
    target 385
  ]
  edge [
    source 16
    target 386
  ]
  edge [
    source 16
    target 387
  ]
  edge [
    source 16
    target 388
  ]
  edge [
    source 16
    target 389
  ]
  edge [
    source 16
    target 390
  ]
  edge [
    source 16
    target 253
  ]
  edge [
    source 16
    target 391
  ]
  edge [
    source 16
    target 392
  ]
  edge [
    source 16
    target 393
  ]
  edge [
    source 16
    target 394
  ]
  edge [
    source 16
    target 395
  ]
  edge [
    source 16
    target 396
  ]
]
