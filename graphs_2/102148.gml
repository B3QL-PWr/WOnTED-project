graph [
  node [
    id 0
    label "przygotowywa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ucze&#324;"
    origin "text"
  ]
  node [
    id 2
    label "wykorzystanie"
    origin "text"
  ]
  node [
    id 3
    label "wiedza"
    origin "text"
  ]
  node [
    id 4
    label "matematyczny"
    origin "text"
  ]
  node [
    id 5
    label "rozwi&#261;zywa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "problem"
    origin "text"
  ]
  node [
    id 7
    label "zakres"
    origin "text"
  ]
  node [
    id 8
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 9
    label "dziedzina"
    origin "text"
  ]
  node [
    id 10
    label "kszta&#322;cenie"
    origin "text"
  ]
  node [
    id 11
    label "szkolny"
    origin "text"
  ]
  node [
    id 12
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 13
    label "codzienny"
    origin "text"
  ]
  node [
    id 14
    label "budowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "model"
    origin "text"
  ]
  node [
    id 16
    label "dla"
    origin "text"
  ]
  node [
    id 17
    label "konkretny"
    origin "text"
  ]
  node [
    id 18
    label "sytuacja"
    origin "text"
  ]
  node [
    id 19
    label "przyswaja&#263;"
    origin "text"
  ]
  node [
    id 20
    label "przez"
    origin "text"
  ]
  node [
    id 21
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 22
    label "matematyka"
    origin "text"
  ]
  node [
    id 23
    label "dostrzega&#263;"
    origin "text"
  ]
  node [
    id 24
    label "formu&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 25
    label "dyskutowa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "kszta&#322;towa&#263;"
    origin "text"
  ]
  node [
    id 27
    label "umiej&#281;tno&#347;&#263;"
    origin "text"
  ]
  node [
    id 28
    label "my&#347;lenie"
    origin "text"
  ]
  node [
    id 29
    label "jasny"
    origin "text"
  ]
  node [
    id 30
    label "wypowied&#378;"
    origin "text"
  ]
  node [
    id 31
    label "rozwija&#263;"
    origin "text"
  ]
  node [
    id 32
    label "rozumienie"
    origin "text"
  ]
  node [
    id 33
    label "tekst"
    origin "text"
  ]
  node [
    id 34
    label "sformu&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 35
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 36
    label "prosta"
    origin "text"
  ]
  node [
    id 37
    label "u&#322;atwia&#263;"
    origin "text"
  ]
  node [
    id 38
    label "badanie"
    origin "text"
  ]
  node [
    id 39
    label "przypadek"
    origin "text"
  ]
  node [
    id 40
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 41
    label "rozumowanie"
    origin "text"
  ]
  node [
    id 42
    label "sposobi&#263;"
  ]
  node [
    id 43
    label "robi&#263;"
  ]
  node [
    id 44
    label "wytwarza&#263;"
  ]
  node [
    id 45
    label "usposabia&#263;"
  ]
  node [
    id 46
    label "train"
  ]
  node [
    id 47
    label "arrange"
  ]
  node [
    id 48
    label "szkoli&#263;"
  ]
  node [
    id 49
    label "wykonywa&#263;"
  ]
  node [
    id 50
    label "pryczy&#263;"
  ]
  node [
    id 51
    label "organizowa&#263;"
  ]
  node [
    id 52
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 53
    label "czyni&#263;"
  ]
  node [
    id 54
    label "give"
  ]
  node [
    id 55
    label "stylizowa&#263;"
  ]
  node [
    id 56
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 57
    label "falowa&#263;"
  ]
  node [
    id 58
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 59
    label "peddle"
  ]
  node [
    id 60
    label "praca"
  ]
  node [
    id 61
    label "wydala&#263;"
  ]
  node [
    id 62
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 63
    label "tentegowa&#263;"
  ]
  node [
    id 64
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 65
    label "urz&#261;dza&#263;"
  ]
  node [
    id 66
    label "oszukiwa&#263;"
  ]
  node [
    id 67
    label "work"
  ]
  node [
    id 68
    label "ukazywa&#263;"
  ]
  node [
    id 69
    label "przerabia&#263;"
  ]
  node [
    id 70
    label "act"
  ]
  node [
    id 71
    label "post&#281;powa&#263;"
  ]
  node [
    id 72
    label "prowadzi&#263;"
  ]
  node [
    id 73
    label "doskonali&#263;"
  ]
  node [
    id 74
    label "o&#347;wieca&#263;"
  ]
  node [
    id 75
    label "pomaga&#263;"
  ]
  node [
    id 76
    label "create"
  ]
  node [
    id 77
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 78
    label "predispose"
  ]
  node [
    id 79
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 80
    label "rola"
  ]
  node [
    id 81
    label "muzyka"
  ]
  node [
    id 82
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 83
    label "d&#378;wiga&#263;"
  ]
  node [
    id 84
    label "gotowa&#263;"
  ]
  node [
    id 85
    label "bra&#263;"
  ]
  node [
    id 86
    label "rzemie&#347;lnik"
  ]
  node [
    id 87
    label "wyprawka"
  ]
  node [
    id 88
    label "mundurek"
  ]
  node [
    id 89
    label "szko&#322;a"
  ]
  node [
    id 90
    label "cz&#322;owiek"
  ]
  node [
    id 91
    label "tarcza"
  ]
  node [
    id 92
    label "elew"
  ]
  node [
    id 93
    label "kontynuator"
  ]
  node [
    id 94
    label "absolwent"
  ]
  node [
    id 95
    label "zwolennik"
  ]
  node [
    id 96
    label "praktykant"
  ]
  node [
    id 97
    label "klasa"
  ]
  node [
    id 98
    label "czeladnik"
  ]
  node [
    id 99
    label "wagon"
  ]
  node [
    id 100
    label "mecz_mistrzowski"
  ]
  node [
    id 101
    label "przedmiot"
  ]
  node [
    id 102
    label "arrangement"
  ]
  node [
    id 103
    label "class"
  ]
  node [
    id 104
    label "&#322;awka"
  ]
  node [
    id 105
    label "wykrzyknik"
  ]
  node [
    id 106
    label "zaleta"
  ]
  node [
    id 107
    label "jednostka_systematyczna"
  ]
  node [
    id 108
    label "programowanie_obiektowe"
  ]
  node [
    id 109
    label "tablica"
  ]
  node [
    id 110
    label "warstwa"
  ]
  node [
    id 111
    label "rezerwa"
  ]
  node [
    id 112
    label "gromada"
  ]
  node [
    id 113
    label "Ekwici"
  ]
  node [
    id 114
    label "&#347;rodowisko"
  ]
  node [
    id 115
    label "zbi&#243;r"
  ]
  node [
    id 116
    label "organizacja"
  ]
  node [
    id 117
    label "sala"
  ]
  node [
    id 118
    label "pomoc"
  ]
  node [
    id 119
    label "form"
  ]
  node [
    id 120
    label "grupa"
  ]
  node [
    id 121
    label "przepisa&#263;"
  ]
  node [
    id 122
    label "jako&#347;&#263;"
  ]
  node [
    id 123
    label "znak_jako&#347;ci"
  ]
  node [
    id 124
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 125
    label "poziom"
  ]
  node [
    id 126
    label "type"
  ]
  node [
    id 127
    label "promocja"
  ]
  node [
    id 128
    label "przepisanie"
  ]
  node [
    id 129
    label "kurs"
  ]
  node [
    id 130
    label "obiekt"
  ]
  node [
    id 131
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 132
    label "dziennik_lekcyjny"
  ]
  node [
    id 133
    label "typ"
  ]
  node [
    id 134
    label "fakcja"
  ]
  node [
    id 135
    label "obrona"
  ]
  node [
    id 136
    label "atak"
  ]
  node [
    id 137
    label "botanika"
  ]
  node [
    id 138
    label "do&#347;wiadczenie"
  ]
  node [
    id 139
    label "teren_szko&#322;y"
  ]
  node [
    id 140
    label "Mickiewicz"
  ]
  node [
    id 141
    label "kwalifikacje"
  ]
  node [
    id 142
    label "podr&#281;cznik"
  ]
  node [
    id 143
    label "praktyka"
  ]
  node [
    id 144
    label "school"
  ]
  node [
    id 145
    label "system"
  ]
  node [
    id 146
    label "zda&#263;"
  ]
  node [
    id 147
    label "gabinet"
  ]
  node [
    id 148
    label "urszulanki"
  ]
  node [
    id 149
    label "sztuba"
  ]
  node [
    id 150
    label "&#322;awa_szkolna"
  ]
  node [
    id 151
    label "nauka"
  ]
  node [
    id 152
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 153
    label "lekcja"
  ]
  node [
    id 154
    label "metoda"
  ]
  node [
    id 155
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 156
    label "czas"
  ]
  node [
    id 157
    label "skolaryzacja"
  ]
  node [
    id 158
    label "zdanie"
  ]
  node [
    id 159
    label "stopek"
  ]
  node [
    id 160
    label "sekretariat"
  ]
  node [
    id 161
    label "ideologia"
  ]
  node [
    id 162
    label "lesson"
  ]
  node [
    id 163
    label "instytucja"
  ]
  node [
    id 164
    label "niepokalanki"
  ]
  node [
    id 165
    label "siedziba"
  ]
  node [
    id 166
    label "szkolenie"
  ]
  node [
    id 167
    label "kara"
  ]
  node [
    id 168
    label "ludzko&#347;&#263;"
  ]
  node [
    id 169
    label "asymilowanie"
  ]
  node [
    id 170
    label "wapniak"
  ]
  node [
    id 171
    label "asymilowa&#263;"
  ]
  node [
    id 172
    label "os&#322;abia&#263;"
  ]
  node [
    id 173
    label "posta&#263;"
  ]
  node [
    id 174
    label "hominid"
  ]
  node [
    id 175
    label "podw&#322;adny"
  ]
  node [
    id 176
    label "os&#322;abianie"
  ]
  node [
    id 177
    label "g&#322;owa"
  ]
  node [
    id 178
    label "figura"
  ]
  node [
    id 179
    label "portrecista"
  ]
  node [
    id 180
    label "dwun&#243;g"
  ]
  node [
    id 181
    label "profanum"
  ]
  node [
    id 182
    label "mikrokosmos"
  ]
  node [
    id 183
    label "nasada"
  ]
  node [
    id 184
    label "duch"
  ]
  node [
    id 185
    label "antropochoria"
  ]
  node [
    id 186
    label "osoba"
  ]
  node [
    id 187
    label "wz&#243;r"
  ]
  node [
    id 188
    label "senior"
  ]
  node [
    id 189
    label "oddzia&#322;ywanie"
  ]
  node [
    id 190
    label "Adam"
  ]
  node [
    id 191
    label "homo_sapiens"
  ]
  node [
    id 192
    label "polifag"
  ]
  node [
    id 193
    label "remiecha"
  ]
  node [
    id 194
    label "nowicjusz"
  ]
  node [
    id 195
    label "nast&#281;pca"
  ]
  node [
    id 196
    label "na&#347;ladowca"
  ]
  node [
    id 197
    label "&#380;o&#322;nierz"
  ]
  node [
    id 198
    label "student"
  ]
  node [
    id 199
    label "harcerz"
  ]
  node [
    id 200
    label "uniform"
  ]
  node [
    id 201
    label "naszywka"
  ]
  node [
    id 202
    label "kszta&#322;t"
  ]
  node [
    id 203
    label "wskaz&#243;wka"
  ]
  node [
    id 204
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 205
    label "obro&#324;ca"
  ]
  node [
    id 206
    label "bro&#324;_ochronna"
  ]
  node [
    id 207
    label "odznaka"
  ]
  node [
    id 208
    label "bro&#324;"
  ]
  node [
    id 209
    label "denture"
  ]
  node [
    id 210
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 211
    label "telefon"
  ]
  node [
    id 212
    label "or&#281;&#380;"
  ]
  node [
    id 213
    label "ochrona"
  ]
  node [
    id 214
    label "target"
  ]
  node [
    id 215
    label "cel"
  ]
  node [
    id 216
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 217
    label "maszyna"
  ]
  node [
    id 218
    label "obszar"
  ]
  node [
    id 219
    label "powierzchnia"
  ]
  node [
    id 220
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 221
    label "zapomoga"
  ]
  node [
    id 222
    label "komplet"
  ]
  node [
    id 223
    label "layette"
  ]
  node [
    id 224
    label "wyprawa"
  ]
  node [
    id 225
    label "niemowl&#281;"
  ]
  node [
    id 226
    label "wiano"
  ]
  node [
    id 227
    label "rzemie&#347;lniczek"
  ]
  node [
    id 228
    label "stosowanie"
  ]
  node [
    id 229
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 230
    label "u&#380;ycie"
  ]
  node [
    id 231
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 232
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 233
    label "exploitation"
  ]
  node [
    id 234
    label "u&#380;yteczny"
  ]
  node [
    id 235
    label "use"
  ]
  node [
    id 236
    label "zrobienie"
  ]
  node [
    id 237
    label "doznanie"
  ]
  node [
    id 238
    label "zabawa"
  ]
  node [
    id 239
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 240
    label "enjoyment"
  ]
  node [
    id 241
    label "narobienie"
  ]
  node [
    id 242
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 243
    label "creation"
  ]
  node [
    id 244
    label "porobienie"
  ]
  node [
    id 245
    label "czynno&#347;&#263;"
  ]
  node [
    id 246
    label "wykorzystywanie"
  ]
  node [
    id 247
    label "wyzyskanie"
  ]
  node [
    id 248
    label "przydanie_si&#281;"
  ]
  node [
    id 249
    label "przydawanie_si&#281;"
  ]
  node [
    id 250
    label "u&#380;ytecznie"
  ]
  node [
    id 251
    label "przydatny"
  ]
  node [
    id 252
    label "przejaskrawianie"
  ]
  node [
    id 253
    label "zniszczenie"
  ]
  node [
    id 254
    label "robienie"
  ]
  node [
    id 255
    label "zu&#380;ywanie"
  ]
  node [
    id 256
    label "cognition"
  ]
  node [
    id 257
    label "intelekt"
  ]
  node [
    id 258
    label "pozwolenie"
  ]
  node [
    id 259
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 260
    label "zaawansowanie"
  ]
  node [
    id 261
    label "wykszta&#322;cenie"
  ]
  node [
    id 262
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 263
    label "zdolno&#347;&#263;"
  ]
  node [
    id 264
    label "cecha"
  ]
  node [
    id 265
    label "ekstraspekcja"
  ]
  node [
    id 266
    label "feeling"
  ]
  node [
    id 267
    label "zemdle&#263;"
  ]
  node [
    id 268
    label "psychika"
  ]
  node [
    id 269
    label "stan"
  ]
  node [
    id 270
    label "Freud"
  ]
  node [
    id 271
    label "psychoanaliza"
  ]
  node [
    id 272
    label "conscience"
  ]
  node [
    id 273
    label "integer"
  ]
  node [
    id 274
    label "liczba"
  ]
  node [
    id 275
    label "zlewanie_si&#281;"
  ]
  node [
    id 276
    label "ilo&#347;&#263;"
  ]
  node [
    id 277
    label "uk&#322;ad"
  ]
  node [
    id 278
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 279
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 280
    label "pe&#322;ny"
  ]
  node [
    id 281
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 282
    label "rozwini&#281;cie"
  ]
  node [
    id 283
    label "zapoznanie"
  ]
  node [
    id 284
    label "wys&#322;anie"
  ]
  node [
    id 285
    label "udoskonalenie"
  ]
  node [
    id 286
    label "pomo&#380;enie"
  ]
  node [
    id 287
    label "training"
  ]
  node [
    id 288
    label "o&#347;wiecenie"
  ]
  node [
    id 289
    label "sophistication"
  ]
  node [
    id 290
    label "umys&#322;"
  ]
  node [
    id 291
    label "noosfera"
  ]
  node [
    id 292
    label "stopie&#324;"
  ]
  node [
    id 293
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 294
    label "decyzja"
  ]
  node [
    id 295
    label "zwalnianie_si&#281;"
  ]
  node [
    id 296
    label "authorization"
  ]
  node [
    id 297
    label "koncesjonowanie"
  ]
  node [
    id 298
    label "zwolnienie_si&#281;"
  ]
  node [
    id 299
    label "pozwole&#324;stwo"
  ]
  node [
    id 300
    label "bycie_w_stanie"
  ]
  node [
    id 301
    label "odwieszenie"
  ]
  node [
    id 302
    label "odpowied&#378;"
  ]
  node [
    id 303
    label "pofolgowanie"
  ]
  node [
    id 304
    label "license"
  ]
  node [
    id 305
    label "franchise"
  ]
  node [
    id 306
    label "umo&#380;liwienie"
  ]
  node [
    id 307
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 308
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 309
    label "dokument"
  ]
  node [
    id 310
    label "uznanie"
  ]
  node [
    id 311
    label "matematycznie"
  ]
  node [
    id 312
    label "dok&#322;adny"
  ]
  node [
    id 313
    label "sprecyzowanie"
  ]
  node [
    id 314
    label "dok&#322;adnie"
  ]
  node [
    id 315
    label "precyzyjny"
  ]
  node [
    id 316
    label "miliamperomierz"
  ]
  node [
    id 317
    label "precyzowanie"
  ]
  node [
    id 318
    label "rzetelny"
  ]
  node [
    id 319
    label "usuwa&#263;"
  ]
  node [
    id 320
    label "odkrywa&#263;"
  ]
  node [
    id 321
    label "urzeczywistnia&#263;"
  ]
  node [
    id 322
    label "undo"
  ]
  node [
    id 323
    label "przestawa&#263;"
  ]
  node [
    id 324
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 325
    label "cope"
  ]
  node [
    id 326
    label "&#380;y&#263;"
  ]
  node [
    id 327
    label "coating"
  ]
  node [
    id 328
    label "przebywa&#263;"
  ]
  node [
    id 329
    label "determine"
  ]
  node [
    id 330
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 331
    label "ko&#324;czy&#263;"
  ]
  node [
    id 332
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 333
    label "finish_up"
  ]
  node [
    id 334
    label "zrywa&#263;"
  ]
  node [
    id 335
    label "retract"
  ]
  node [
    id 336
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 337
    label "poznawa&#263;"
  ]
  node [
    id 338
    label "podnosi&#263;"
  ]
  node [
    id 339
    label "&#322;owca_talent&#243;w"
  ]
  node [
    id 340
    label "testify"
  ]
  node [
    id 341
    label "demaskator"
  ]
  node [
    id 342
    label "discovery"
  ]
  node [
    id 343
    label "zsuwa&#263;"
  ]
  node [
    id 344
    label "objawia&#263;"
  ]
  node [
    id 345
    label "znajdowa&#263;"
  ]
  node [
    id 346
    label "unwrap"
  ]
  node [
    id 347
    label "informowa&#263;"
  ]
  node [
    id 348
    label "issue"
  ]
  node [
    id 349
    label "indicate"
  ]
  node [
    id 350
    label "przeprowadza&#263;"
  ]
  node [
    id 351
    label "prosecute"
  ]
  node [
    id 352
    label "prawdzi&#263;"
  ]
  node [
    id 353
    label "pozbywa&#263;_si&#281;"
  ]
  node [
    id 354
    label "zabija&#263;"
  ]
  node [
    id 355
    label "przesuwa&#263;"
  ]
  node [
    id 356
    label "rugowa&#263;"
  ]
  node [
    id 357
    label "powodowa&#263;"
  ]
  node [
    id 358
    label "blurt_out"
  ]
  node [
    id 359
    label "przenosi&#263;"
  ]
  node [
    id 360
    label "sprawa"
  ]
  node [
    id 361
    label "subiekcja"
  ]
  node [
    id 362
    label "problemat"
  ]
  node [
    id 363
    label "jajko_Kolumba"
  ]
  node [
    id 364
    label "obstruction"
  ]
  node [
    id 365
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 366
    label "problematyka"
  ]
  node [
    id 367
    label "trudno&#347;&#263;"
  ]
  node [
    id 368
    label "pierepa&#322;ka"
  ]
  node [
    id 369
    label "ambaras"
  ]
  node [
    id 370
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 371
    label "napotka&#263;"
  ]
  node [
    id 372
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 373
    label "k&#322;opotliwy"
  ]
  node [
    id 374
    label "napotkanie"
  ]
  node [
    id 375
    label "difficulty"
  ]
  node [
    id 376
    label "obstacle"
  ]
  node [
    id 377
    label "kognicja"
  ]
  node [
    id 378
    label "object"
  ]
  node [
    id 379
    label "rozprawa"
  ]
  node [
    id 380
    label "temat"
  ]
  node [
    id 381
    label "wydarzenie"
  ]
  node [
    id 382
    label "szczeg&#243;&#322;"
  ]
  node [
    id 383
    label "proposition"
  ]
  node [
    id 384
    label "przes&#322;anka"
  ]
  node [
    id 385
    label "rzecz"
  ]
  node [
    id 386
    label "idea"
  ]
  node [
    id 387
    label "k&#322;opot"
  ]
  node [
    id 388
    label "granica"
  ]
  node [
    id 389
    label "sfera"
  ]
  node [
    id 390
    label "wielko&#347;&#263;"
  ]
  node [
    id 391
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 392
    label "podzakres"
  ]
  node [
    id 393
    label "desygnat"
  ]
  node [
    id 394
    label "circle"
  ]
  node [
    id 395
    label "rozmiar"
  ]
  node [
    id 396
    label "izochronizm"
  ]
  node [
    id 397
    label "zasi&#261;g"
  ]
  node [
    id 398
    label "bridge"
  ]
  node [
    id 399
    label "distribution"
  ]
  node [
    id 400
    label "warunek_lokalowy"
  ]
  node [
    id 401
    label "rzadko&#347;&#263;"
  ]
  node [
    id 402
    label "measure"
  ]
  node [
    id 403
    label "znaczenie"
  ]
  node [
    id 404
    label "opinia"
  ]
  node [
    id 405
    label "dymensja"
  ]
  node [
    id 406
    label "poj&#281;cie"
  ]
  node [
    id 407
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 408
    label "potencja"
  ]
  node [
    id 409
    label "property"
  ]
  node [
    id 410
    label "egzemplarz"
  ]
  node [
    id 411
    label "series"
  ]
  node [
    id 412
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 413
    label "uprawianie"
  ]
  node [
    id 414
    label "praca_rolnicza"
  ]
  node [
    id 415
    label "collection"
  ]
  node [
    id 416
    label "dane"
  ]
  node [
    id 417
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 418
    label "pakiet_klimatyczny"
  ]
  node [
    id 419
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 420
    label "sum"
  ]
  node [
    id 421
    label "gathering"
  ]
  node [
    id 422
    label "album"
  ]
  node [
    id 423
    label "przej&#347;cie"
  ]
  node [
    id 424
    label "kres"
  ]
  node [
    id 425
    label "granica_pa&#324;stwa"
  ]
  node [
    id 426
    label "Ural"
  ]
  node [
    id 427
    label "miara"
  ]
  node [
    id 428
    label "end"
  ]
  node [
    id 429
    label "pu&#322;ap"
  ]
  node [
    id 430
    label "koniec"
  ]
  node [
    id 431
    label "granice"
  ]
  node [
    id 432
    label "frontier"
  ]
  node [
    id 433
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 434
    label "funkcja"
  ]
  node [
    id 435
    label "bezdro&#380;e"
  ]
  node [
    id 436
    label "poddzia&#322;"
  ]
  node [
    id 437
    label "wymiar"
  ]
  node [
    id 438
    label "strefa"
  ]
  node [
    id 439
    label "kula"
  ]
  node [
    id 440
    label "sector"
  ]
  node [
    id 441
    label "przestrze&#324;"
  ]
  node [
    id 442
    label "p&#243;&#322;kula"
  ]
  node [
    id 443
    label "huczek"
  ]
  node [
    id 444
    label "p&#243;&#322;sfera"
  ]
  node [
    id 445
    label "kolur"
  ]
  node [
    id 446
    label "odpowiednik"
  ]
  node [
    id 447
    label "designatum"
  ]
  node [
    id 448
    label "nazwa_rzetelna"
  ]
  node [
    id 449
    label "nazwa_pozorna"
  ]
  node [
    id 450
    label "denotacja"
  ]
  node [
    id 451
    label "inny"
  ]
  node [
    id 452
    label "jaki&#347;"
  ]
  node [
    id 453
    label "r&#243;&#380;nie"
  ]
  node [
    id 454
    label "przyzwoity"
  ]
  node [
    id 455
    label "ciekawy"
  ]
  node [
    id 456
    label "jako&#347;"
  ]
  node [
    id 457
    label "jako_tako"
  ]
  node [
    id 458
    label "niez&#322;y"
  ]
  node [
    id 459
    label "dziwny"
  ]
  node [
    id 460
    label "charakterystyczny"
  ]
  node [
    id 461
    label "kolejny"
  ]
  node [
    id 462
    label "osobno"
  ]
  node [
    id 463
    label "inszy"
  ]
  node [
    id 464
    label "inaczej"
  ]
  node [
    id 465
    label "osobnie"
  ]
  node [
    id 466
    label "czyn"
  ]
  node [
    id 467
    label "supremum"
  ]
  node [
    id 468
    label "addytywno&#347;&#263;"
  ]
  node [
    id 469
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 470
    label "jednostka"
  ]
  node [
    id 471
    label "function"
  ]
  node [
    id 472
    label "zastosowanie"
  ]
  node [
    id 473
    label "funkcjonowanie"
  ]
  node [
    id 474
    label "rzut"
  ]
  node [
    id 475
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 476
    label "powierzanie"
  ]
  node [
    id 477
    label "przeciwdziedzina"
  ]
  node [
    id 478
    label "awansowa&#263;"
  ]
  node [
    id 479
    label "stawia&#263;"
  ]
  node [
    id 480
    label "wakowa&#263;"
  ]
  node [
    id 481
    label "postawi&#263;"
  ]
  node [
    id 482
    label "awansowanie"
  ]
  node [
    id 483
    label "infimum"
  ]
  node [
    id 484
    label "wilderness"
  ]
  node [
    id 485
    label "formation"
  ]
  node [
    id 486
    label "rozwijanie"
  ]
  node [
    id 487
    label "wysy&#322;anie"
  ]
  node [
    id 488
    label "pomaganie"
  ]
  node [
    id 489
    label "heureza"
  ]
  node [
    id 490
    label "zapoznawanie"
  ]
  node [
    id 491
    label "pouczenie"
  ]
  node [
    id 492
    label "o&#347;wiecanie"
  ]
  node [
    id 493
    label "kliker"
  ]
  node [
    id 494
    label "miasteczko_rowerowe"
  ]
  node [
    id 495
    label "porada"
  ]
  node [
    id 496
    label "fotowoltaika"
  ]
  node [
    id 497
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 498
    label "przem&#243;wienie"
  ]
  node [
    id 499
    label "nauki_o_poznaniu"
  ]
  node [
    id 500
    label "nomotetyczny"
  ]
  node [
    id 501
    label "systematyka"
  ]
  node [
    id 502
    label "proces"
  ]
  node [
    id 503
    label "typologia"
  ]
  node [
    id 504
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 505
    label "kultura_duchowa"
  ]
  node [
    id 506
    label "nauki_penalne"
  ]
  node [
    id 507
    label "imagineskopia"
  ]
  node [
    id 508
    label "teoria_naukowa"
  ]
  node [
    id 509
    label "inwentyka"
  ]
  node [
    id 510
    label "metodologia"
  ]
  node [
    id 511
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 512
    label "nauki_o_Ziemi"
  ]
  node [
    id 513
    label "nakazywanie"
  ]
  node [
    id 514
    label "cover"
  ]
  node [
    id 515
    label "wytwarzanie"
  ]
  node [
    id 516
    label "transmission"
  ]
  node [
    id 517
    label "przekazywanie"
  ]
  node [
    id 518
    label "stawianie"
  ]
  node [
    id 519
    label "opowiadanie"
  ]
  node [
    id 520
    label "rozk&#322;adanie"
  ]
  node [
    id 521
    label "puszczanie"
  ]
  node [
    id 522
    label "rozstawianie"
  ]
  node [
    id 523
    label "rozpakowywanie"
  ]
  node [
    id 524
    label "rozwijanie_si&#281;"
  ]
  node [
    id 525
    label "development"
  ]
  node [
    id 526
    label "dodawanie"
  ]
  node [
    id 527
    label "zwi&#281;kszanie"
  ]
  node [
    id 528
    label "znajomy"
  ]
  node [
    id 529
    label "umo&#380;liwianie"
  ]
  node [
    id 530
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 531
    label "recognition"
  ]
  node [
    id 532
    label "informowanie"
  ]
  node [
    id 533
    label "obznajamianie"
  ]
  node [
    id 534
    label "merging"
  ]
  node [
    id 535
    label "zawieranie"
  ]
  node [
    id 536
    label "aid"
  ]
  node [
    id 537
    label "helping"
  ]
  node [
    id 538
    label "wyci&#261;ganie_pomocnej_d&#322;oni"
  ]
  node [
    id 539
    label "sprowadzanie"
  ]
  node [
    id 540
    label "care"
  ]
  node [
    id 541
    label "u&#322;atwianie"
  ]
  node [
    id 542
    label "dzianie_si&#281;"
  ]
  node [
    id 543
    label "skutkowanie"
  ]
  node [
    id 544
    label "nauczanie"
  ]
  node [
    id 545
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 546
    label "information"
  ]
  node [
    id 547
    label "upomnienie"
  ]
  node [
    id 548
    label "poinformowanie"
  ]
  node [
    id 549
    label "light"
  ]
  node [
    id 550
    label "o&#347;wietlanie"
  ]
  node [
    id 551
    label "narz&#281;dzie"
  ]
  node [
    id 552
    label "tresura"
  ]
  node [
    id 553
    label "szkolnie"
  ]
  node [
    id 554
    label "podstawowy"
  ]
  node [
    id 555
    label "prosty"
  ]
  node [
    id 556
    label "szkoleniowy"
  ]
  node [
    id 557
    label "skromny"
  ]
  node [
    id 558
    label "po_prostu"
  ]
  node [
    id 559
    label "naturalny"
  ]
  node [
    id 560
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 561
    label "rozprostowanie"
  ]
  node [
    id 562
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 563
    label "prosto"
  ]
  node [
    id 564
    label "prostowanie_si&#281;"
  ]
  node [
    id 565
    label "niepozorny"
  ]
  node [
    id 566
    label "cios"
  ]
  node [
    id 567
    label "prostoduszny"
  ]
  node [
    id 568
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 569
    label "naiwny"
  ]
  node [
    id 570
    label "&#322;atwy"
  ]
  node [
    id 571
    label "prostowanie"
  ]
  node [
    id 572
    label "zwyk&#322;y"
  ]
  node [
    id 573
    label "naukowy"
  ]
  node [
    id 574
    label "niezaawansowany"
  ]
  node [
    id 575
    label "najwa&#380;niejszy"
  ]
  node [
    id 576
    label "pocz&#261;tkowy"
  ]
  node [
    id 577
    label "podstawowo"
  ]
  node [
    id 578
    label "raj_utracony"
  ]
  node [
    id 579
    label "umieranie"
  ]
  node [
    id 580
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 581
    label "prze&#380;ywanie"
  ]
  node [
    id 582
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 583
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 584
    label "po&#322;&#243;g"
  ]
  node [
    id 585
    label "umarcie"
  ]
  node [
    id 586
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 587
    label "subsistence"
  ]
  node [
    id 588
    label "power"
  ]
  node [
    id 589
    label "okres_noworodkowy"
  ]
  node [
    id 590
    label "prze&#380;ycie"
  ]
  node [
    id 591
    label "wiek_matuzalemowy"
  ]
  node [
    id 592
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 593
    label "entity"
  ]
  node [
    id 594
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 595
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 596
    label "do&#380;ywanie"
  ]
  node [
    id 597
    label "byt"
  ]
  node [
    id 598
    label "andropauza"
  ]
  node [
    id 599
    label "dzieci&#324;stwo"
  ]
  node [
    id 600
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 601
    label "rozw&#243;j"
  ]
  node [
    id 602
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 603
    label "menopauza"
  ]
  node [
    id 604
    label "&#347;mier&#263;"
  ]
  node [
    id 605
    label "koleje_losu"
  ]
  node [
    id 606
    label "bycie"
  ]
  node [
    id 607
    label "zegar_biologiczny"
  ]
  node [
    id 608
    label "szwung"
  ]
  node [
    id 609
    label "przebywanie"
  ]
  node [
    id 610
    label "warunki"
  ]
  node [
    id 611
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 612
    label "niemowl&#281;ctwo"
  ]
  node [
    id 613
    label "&#380;ywy"
  ]
  node [
    id 614
    label "life"
  ]
  node [
    id 615
    label "staro&#347;&#263;"
  ]
  node [
    id 616
    label "energy"
  ]
  node [
    id 617
    label "trwanie"
  ]
  node [
    id 618
    label "wra&#380;enie"
  ]
  node [
    id 619
    label "poradzenie_sobie"
  ]
  node [
    id 620
    label "przetrwanie"
  ]
  node [
    id 621
    label "survival"
  ]
  node [
    id 622
    label "przechodzenie"
  ]
  node [
    id 623
    label "wytrzymywanie"
  ]
  node [
    id 624
    label "zaznawanie"
  ]
  node [
    id 625
    label "obejrzenie"
  ]
  node [
    id 626
    label "widzenie"
  ]
  node [
    id 627
    label "urzeczywistnianie"
  ]
  node [
    id 628
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 629
    label "przeszkodzenie"
  ]
  node [
    id 630
    label "produkowanie"
  ]
  node [
    id 631
    label "being"
  ]
  node [
    id 632
    label "znikni&#281;cie"
  ]
  node [
    id 633
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 634
    label "przeszkadzanie"
  ]
  node [
    id 635
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 636
    label "wyprodukowanie"
  ]
  node [
    id 637
    label "utrzymywanie"
  ]
  node [
    id 638
    label "subsystencja"
  ]
  node [
    id 639
    label "utrzyma&#263;"
  ]
  node [
    id 640
    label "egzystencja"
  ]
  node [
    id 641
    label "wy&#380;ywienie"
  ]
  node [
    id 642
    label "ontologicznie"
  ]
  node [
    id 643
    label "utrzymanie"
  ]
  node [
    id 644
    label "utrzymywa&#263;"
  ]
  node [
    id 645
    label "status"
  ]
  node [
    id 646
    label "poprzedzanie"
  ]
  node [
    id 647
    label "czasoprzestrze&#324;"
  ]
  node [
    id 648
    label "laba"
  ]
  node [
    id 649
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 650
    label "chronometria"
  ]
  node [
    id 651
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 652
    label "rachuba_czasu"
  ]
  node [
    id 653
    label "przep&#322;ywanie"
  ]
  node [
    id 654
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 655
    label "czasokres"
  ]
  node [
    id 656
    label "odczyt"
  ]
  node [
    id 657
    label "chwila"
  ]
  node [
    id 658
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 659
    label "dzieje"
  ]
  node [
    id 660
    label "kategoria_gramatyczna"
  ]
  node [
    id 661
    label "poprzedzenie"
  ]
  node [
    id 662
    label "trawienie"
  ]
  node [
    id 663
    label "pochodzi&#263;"
  ]
  node [
    id 664
    label "period"
  ]
  node [
    id 665
    label "okres_czasu"
  ]
  node [
    id 666
    label "poprzedza&#263;"
  ]
  node [
    id 667
    label "schy&#322;ek"
  ]
  node [
    id 668
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 669
    label "odwlekanie_si&#281;"
  ]
  node [
    id 670
    label "zegar"
  ]
  node [
    id 671
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 672
    label "czwarty_wymiar"
  ]
  node [
    id 673
    label "pochodzenie"
  ]
  node [
    id 674
    label "koniugacja"
  ]
  node [
    id 675
    label "Zeitgeist"
  ]
  node [
    id 676
    label "trawi&#263;"
  ]
  node [
    id 677
    label "pogoda"
  ]
  node [
    id 678
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 679
    label "poprzedzi&#263;"
  ]
  node [
    id 680
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 681
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 682
    label "time_period"
  ]
  node [
    id 683
    label "ocieranie_si&#281;"
  ]
  node [
    id 684
    label "otoczenie_si&#281;"
  ]
  node [
    id 685
    label "posiedzenie"
  ]
  node [
    id 686
    label "otarcie_si&#281;"
  ]
  node [
    id 687
    label "atakowanie"
  ]
  node [
    id 688
    label "otaczanie_si&#281;"
  ]
  node [
    id 689
    label "wyj&#347;cie"
  ]
  node [
    id 690
    label "zmierzanie"
  ]
  node [
    id 691
    label "residency"
  ]
  node [
    id 692
    label "sojourn"
  ]
  node [
    id 693
    label "wychodzenie"
  ]
  node [
    id 694
    label "tkwienie"
  ]
  node [
    id 695
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 696
    label "absolutorium"
  ]
  node [
    id 697
    label "dzia&#322;anie"
  ]
  node [
    id 698
    label "activity"
  ]
  node [
    id 699
    label "ton"
  ]
  node [
    id 700
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 701
    label "korkowanie"
  ]
  node [
    id 702
    label "death"
  ]
  node [
    id 703
    label "zabijanie"
  ]
  node [
    id 704
    label "martwy"
  ]
  node [
    id 705
    label "przestawanie"
  ]
  node [
    id 706
    label "odumieranie"
  ]
  node [
    id 707
    label "zdychanie"
  ]
  node [
    id 708
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 709
    label "zanikanie"
  ]
  node [
    id 710
    label "ko&#324;czenie"
  ]
  node [
    id 711
    label "nieuleczalnie_chory"
  ]
  node [
    id 712
    label "szybki"
  ]
  node [
    id 713
    label "&#380;ywotny"
  ]
  node [
    id 714
    label "&#380;ywo"
  ]
  node [
    id 715
    label "o&#380;ywianie"
  ]
  node [
    id 716
    label "silny"
  ]
  node [
    id 717
    label "g&#322;&#281;boki"
  ]
  node [
    id 718
    label "wyra&#378;ny"
  ]
  node [
    id 719
    label "czynny"
  ]
  node [
    id 720
    label "aktualny"
  ]
  node [
    id 721
    label "zgrabny"
  ]
  node [
    id 722
    label "prawdziwy"
  ]
  node [
    id 723
    label "realistyczny"
  ]
  node [
    id 724
    label "energiczny"
  ]
  node [
    id 725
    label "odumarcie"
  ]
  node [
    id 726
    label "przestanie"
  ]
  node [
    id 727
    label "dysponowanie_si&#281;"
  ]
  node [
    id 728
    label "pomarcie"
  ]
  node [
    id 729
    label "die"
  ]
  node [
    id 730
    label "sko&#324;czenie"
  ]
  node [
    id 731
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 732
    label "zdechni&#281;cie"
  ]
  node [
    id 733
    label "zabicie"
  ]
  node [
    id 734
    label "procedura"
  ]
  node [
    id 735
    label "proces_biologiczny"
  ]
  node [
    id 736
    label "z&#322;ote_czasy"
  ]
  node [
    id 737
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 738
    label "process"
  ]
  node [
    id 739
    label "cycle"
  ]
  node [
    id 740
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 741
    label "adolescence"
  ]
  node [
    id 742
    label "wiek"
  ]
  node [
    id 743
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 744
    label "zielone_lata"
  ]
  node [
    id 745
    label "rozwi&#261;zanie"
  ]
  node [
    id 746
    label "zlec"
  ]
  node [
    id 747
    label "zlegni&#281;cie"
  ]
  node [
    id 748
    label "defenestracja"
  ]
  node [
    id 749
    label "agonia"
  ]
  node [
    id 750
    label "mogi&#322;a"
  ]
  node [
    id 751
    label "kres_&#380;ycia"
  ]
  node [
    id 752
    label "upadek"
  ]
  node [
    id 753
    label "szeol"
  ]
  node [
    id 754
    label "pogrzebanie"
  ]
  node [
    id 755
    label "istota_nadprzyrodzona"
  ]
  node [
    id 756
    label "&#380;a&#322;oba"
  ]
  node [
    id 757
    label "pogrzeb"
  ]
  node [
    id 758
    label "majority"
  ]
  node [
    id 759
    label "osiemnastoletni"
  ]
  node [
    id 760
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 761
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 762
    label "age"
  ]
  node [
    id 763
    label "kobieta"
  ]
  node [
    id 764
    label "przekwitanie"
  ]
  node [
    id 765
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 766
    label "dzieci&#281;ctwo"
  ]
  node [
    id 767
    label "energia"
  ]
  node [
    id 768
    label "zapa&#322;"
  ]
  node [
    id 769
    label "regularny"
  ]
  node [
    id 770
    label "cykliczny"
  ]
  node [
    id 771
    label "sta&#322;y"
  ]
  node [
    id 772
    label "prozaiczny"
  ]
  node [
    id 773
    label "powszedny"
  ]
  node [
    id 774
    label "codziennie"
  ]
  node [
    id 775
    label "cz&#281;sty"
  ]
  node [
    id 776
    label "pospolity"
  ]
  node [
    id 777
    label "zorganizowany"
  ]
  node [
    id 778
    label "powtarzalny"
  ]
  node [
    id 779
    label "regularnie"
  ]
  node [
    id 780
    label "zwyczajnie"
  ]
  node [
    id 781
    label "harmonijny"
  ]
  node [
    id 782
    label "cz&#281;sto"
  ]
  node [
    id 783
    label "jednakowy"
  ]
  node [
    id 784
    label "stale"
  ]
  node [
    id 785
    label "pospolicie"
  ]
  node [
    id 786
    label "zwyczajny"
  ]
  node [
    id 787
    label "wsp&#243;lny"
  ]
  node [
    id 788
    label "jak_ps&#243;w"
  ]
  node [
    id 789
    label "niewyszukany"
  ]
  node [
    id 790
    label "zwi&#261;zek_heterocykliczny"
  ]
  node [
    id 791
    label "zwi&#261;zek_cykliczny"
  ]
  node [
    id 792
    label "tyrocydyna"
  ]
  node [
    id 793
    label "cyklicznie"
  ]
  node [
    id 794
    label "daily"
  ]
  node [
    id 795
    label "prozaicznie"
  ]
  node [
    id 796
    label "literacki"
  ]
  node [
    id 797
    label "prozatorsko"
  ]
  node [
    id 798
    label "planowa&#263;"
  ]
  node [
    id 799
    label "consist"
  ]
  node [
    id 800
    label "dodawa&#263;_otuchy"
  ]
  node [
    id 801
    label "tworzy&#263;"
  ]
  node [
    id 802
    label "stanowi&#263;"
  ]
  node [
    id 803
    label "raise"
  ]
  node [
    id 804
    label "by&#263;"
  ]
  node [
    id 805
    label "decide"
  ]
  node [
    id 806
    label "pies_my&#347;liwski"
  ]
  node [
    id 807
    label "decydowa&#263;"
  ]
  node [
    id 808
    label "represent"
  ]
  node [
    id 809
    label "zatrzymywa&#263;"
  ]
  node [
    id 810
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 811
    label "typify"
  ]
  node [
    id 812
    label "pope&#322;nia&#263;"
  ]
  node [
    id 813
    label "get"
  ]
  node [
    id 814
    label "mean"
  ]
  node [
    id 815
    label "lot_&#347;lizgowy"
  ]
  node [
    id 816
    label "organize"
  ]
  node [
    id 817
    label "project"
  ]
  node [
    id 818
    label "my&#347;le&#263;"
  ]
  node [
    id 819
    label "volunteer"
  ]
  node [
    id 820
    label "opracowywa&#263;"
  ]
  node [
    id 821
    label "opuszcza&#263;_si&#281;"
  ]
  node [
    id 822
    label "spos&#243;b"
  ]
  node [
    id 823
    label "prezenter"
  ]
  node [
    id 824
    label "mildew"
  ]
  node [
    id 825
    label "zi&#243;&#322;ko"
  ]
  node [
    id 826
    label "motif"
  ]
  node [
    id 827
    label "pozowanie"
  ]
  node [
    id 828
    label "ideal"
  ]
  node [
    id 829
    label "matryca"
  ]
  node [
    id 830
    label "adaptation"
  ]
  node [
    id 831
    label "ruch"
  ]
  node [
    id 832
    label "pozowa&#263;"
  ]
  node [
    id 833
    label "imitacja"
  ]
  node [
    id 834
    label "orygina&#322;"
  ]
  node [
    id 835
    label "facet"
  ]
  node [
    id 836
    label "miniatura"
  ]
  node [
    id 837
    label "gablotka"
  ]
  node [
    id 838
    label "pokaz"
  ]
  node [
    id 839
    label "szkatu&#322;ka"
  ]
  node [
    id 840
    label "pude&#322;ko"
  ]
  node [
    id 841
    label "bran&#380;owiec"
  ]
  node [
    id 842
    label "prowadz&#261;cy"
  ]
  node [
    id 843
    label "kopia"
  ]
  node [
    id 844
    label "utw&#243;r"
  ]
  node [
    id 845
    label "obraz"
  ]
  node [
    id 846
    label "ilustracja"
  ]
  node [
    id 847
    label "miniature"
  ]
  node [
    id 848
    label "zapis"
  ]
  node [
    id 849
    label "figure"
  ]
  node [
    id 850
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 851
    label "rule"
  ]
  node [
    id 852
    label "dekal"
  ]
  node [
    id 853
    label "motyw"
  ]
  node [
    id 854
    label "projekt"
  ]
  node [
    id 855
    label "technika"
  ]
  node [
    id 856
    label "na&#347;ladownictwo"
  ]
  node [
    id 857
    label "tryb"
  ]
  node [
    id 858
    label "nature"
  ]
  node [
    id 859
    label "bratek"
  ]
  node [
    id 860
    label "kod_genetyczny"
  ]
  node [
    id 861
    label "t&#322;ocznik"
  ]
  node [
    id 862
    label "aparat_cyfrowy"
  ]
  node [
    id 863
    label "detector"
  ]
  node [
    id 864
    label "forma"
  ]
  node [
    id 865
    label "kr&#243;lestwo"
  ]
  node [
    id 866
    label "autorament"
  ]
  node [
    id 867
    label "variety"
  ]
  node [
    id 868
    label "antycypacja"
  ]
  node [
    id 869
    label "przypuszczenie"
  ]
  node [
    id 870
    label "cynk"
  ]
  node [
    id 871
    label "obstawia&#263;"
  ]
  node [
    id 872
    label "sztuka"
  ]
  node [
    id 873
    label "rezultat"
  ]
  node [
    id 874
    label "design"
  ]
  node [
    id 875
    label "sfotografowanie_si&#281;"
  ]
  node [
    id 876
    label "na&#347;ladowanie"
  ]
  node [
    id 877
    label "fotografowanie_si&#281;"
  ]
  node [
    id 878
    label "pretense"
  ]
  node [
    id 879
    label "sit"
  ]
  node [
    id 880
    label "dally"
  ]
  node [
    id 881
    label "mechanika"
  ]
  node [
    id 882
    label "move"
  ]
  node [
    id 883
    label "poruszenie"
  ]
  node [
    id 884
    label "movement"
  ]
  node [
    id 885
    label "myk"
  ]
  node [
    id 886
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 887
    label "zjawisko"
  ]
  node [
    id 888
    label "travel"
  ]
  node [
    id 889
    label "kanciasty"
  ]
  node [
    id 890
    label "commercial_enterprise"
  ]
  node [
    id 891
    label "strumie&#324;"
  ]
  node [
    id 892
    label "aktywno&#347;&#263;"
  ]
  node [
    id 893
    label "kr&#243;tki"
  ]
  node [
    id 894
    label "taktyka"
  ]
  node [
    id 895
    label "apraksja"
  ]
  node [
    id 896
    label "natural_process"
  ]
  node [
    id 897
    label "d&#322;ugi"
  ]
  node [
    id 898
    label "dyssypacja_energii"
  ]
  node [
    id 899
    label "tumult"
  ]
  node [
    id 900
    label "zmiana"
  ]
  node [
    id 901
    label "manewr"
  ]
  node [
    id 902
    label "lokomocja"
  ]
  node [
    id 903
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 904
    label "komunikacja"
  ]
  node [
    id 905
    label "drift"
  ]
  node [
    id 906
    label "nicpo&#324;"
  ]
  node [
    id 907
    label "agent"
  ]
  node [
    id 908
    label "po&#380;ywny"
  ]
  node [
    id 909
    label "solidnie"
  ]
  node [
    id 910
    label "ogarni&#281;ty"
  ]
  node [
    id 911
    label "posilny"
  ]
  node [
    id 912
    label "&#322;adny"
  ]
  node [
    id 913
    label "tre&#347;ciwy"
  ]
  node [
    id 914
    label "konkretnie"
  ]
  node [
    id 915
    label "abstrakcyjny"
  ]
  node [
    id 916
    label "okre&#347;lony"
  ]
  node [
    id 917
    label "skupiony"
  ]
  node [
    id 918
    label "intensywny"
  ]
  node [
    id 919
    label "udolny"
  ]
  node [
    id 920
    label "skuteczny"
  ]
  node [
    id 921
    label "&#347;mieszny"
  ]
  node [
    id 922
    label "niczegowaty"
  ]
  node [
    id 923
    label "dobrze"
  ]
  node [
    id 924
    label "nieszpetny"
  ]
  node [
    id 925
    label "spory"
  ]
  node [
    id 926
    label "pozytywny"
  ]
  node [
    id 927
    label "korzystny"
  ]
  node [
    id 928
    label "nie&#378;le"
  ]
  node [
    id 929
    label "g&#322;adki"
  ]
  node [
    id 930
    label "ch&#281;dogi"
  ]
  node [
    id 931
    label "obyczajny"
  ]
  node [
    id 932
    label "ca&#322;y"
  ]
  node [
    id 933
    label "&#347;warny"
  ]
  node [
    id 934
    label "harny"
  ]
  node [
    id 935
    label "przyjemny"
  ]
  node [
    id 936
    label "po&#380;&#261;dany"
  ]
  node [
    id 937
    label "&#322;adnie"
  ]
  node [
    id 938
    label "dobry"
  ]
  node [
    id 939
    label "z&#322;y"
  ]
  node [
    id 940
    label "syc&#261;cy"
  ]
  node [
    id 941
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 942
    label "tre&#347;ciwie"
  ]
  node [
    id 943
    label "g&#281;sty"
  ]
  node [
    id 944
    label "wzmacniaj&#261;cy"
  ]
  node [
    id 945
    label "posilnie"
  ]
  node [
    id 946
    label "zupa_rumfordzka"
  ]
  node [
    id 947
    label "po&#380;ywnie"
  ]
  node [
    id 948
    label "bogaty"
  ]
  node [
    id 949
    label "wiadomy"
  ]
  node [
    id 950
    label "rozgarni&#281;ty"
  ]
  node [
    id 951
    label "uwa&#380;ny"
  ]
  node [
    id 952
    label "o&#347;wietlenie"
  ]
  node [
    id 953
    label "szczery"
  ]
  node [
    id 954
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 955
    label "jasno"
  ]
  node [
    id 956
    label "przytomny"
  ]
  node [
    id 957
    label "zrozumia&#322;y"
  ]
  node [
    id 958
    label "niezm&#261;cony"
  ]
  node [
    id 959
    label "bia&#322;y"
  ]
  node [
    id 960
    label "klarowny"
  ]
  node [
    id 961
    label "jednoznaczny"
  ]
  node [
    id 962
    label "pogodny"
  ]
  node [
    id 963
    label "my&#347;lowy"
  ]
  node [
    id 964
    label "niekonwencjonalny"
  ]
  node [
    id 965
    label "nierealistyczny"
  ]
  node [
    id 966
    label "teoretyczny"
  ]
  node [
    id 967
    label "oryginalny"
  ]
  node [
    id 968
    label "abstrakcyjnie"
  ]
  node [
    id 969
    label "przekonuj&#261;co"
  ]
  node [
    id 970
    label "porz&#261;dnie"
  ]
  node [
    id 971
    label "solidny"
  ]
  node [
    id 972
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 973
    label "state"
  ]
  node [
    id 974
    label "realia"
  ]
  node [
    id 975
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 976
    label "niuansowa&#263;"
  ]
  node [
    id 977
    label "element"
  ]
  node [
    id 978
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 979
    label "sk&#322;adnik"
  ]
  node [
    id 980
    label "zniuansowa&#263;"
  ]
  node [
    id 981
    label "fraza"
  ]
  node [
    id 982
    label "melodia"
  ]
  node [
    id 983
    label "przyczyna"
  ]
  node [
    id 984
    label "ozdoba"
  ]
  node [
    id 985
    label "message"
  ]
  node [
    id 986
    label "kontekst"
  ]
  node [
    id 987
    label "treat"
  ]
  node [
    id 988
    label "czerpa&#263;"
  ]
  node [
    id 989
    label "organizm"
  ]
  node [
    id 990
    label "translate"
  ]
  node [
    id 991
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 992
    label "kultura"
  ]
  node [
    id 993
    label "pobiera&#263;"
  ]
  node [
    id 994
    label "rede"
  ]
  node [
    id 995
    label "korzysta&#263;"
  ]
  node [
    id 996
    label "wch&#322;ania&#263;"
  ]
  node [
    id 997
    label "wycina&#263;"
  ]
  node [
    id 998
    label "kopiowa&#263;"
  ]
  node [
    id 999
    label "pr&#243;bka"
  ]
  node [
    id 1000
    label "open"
  ]
  node [
    id 1001
    label "otrzymywa&#263;"
  ]
  node [
    id 1002
    label "arise"
  ]
  node [
    id 1003
    label "asymilowanie_si&#281;"
  ]
  node [
    id 1004
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1005
    label "Wsch&#243;d"
  ]
  node [
    id 1006
    label "przejmowanie"
  ]
  node [
    id 1007
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1008
    label "makrokosmos"
  ]
  node [
    id 1009
    label "konwencja"
  ]
  node [
    id 1010
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 1011
    label "propriety"
  ]
  node [
    id 1012
    label "przejmowa&#263;"
  ]
  node [
    id 1013
    label "brzoskwiniarnia"
  ]
  node [
    id 1014
    label "zwyczaj"
  ]
  node [
    id 1015
    label "kuchnia"
  ]
  node [
    id 1016
    label "tradycja"
  ]
  node [
    id 1017
    label "populace"
  ]
  node [
    id 1018
    label "hodowla"
  ]
  node [
    id 1019
    label "religia"
  ]
  node [
    id 1020
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 1021
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 1022
    label "przej&#281;cie"
  ]
  node [
    id 1023
    label "przej&#261;&#263;"
  ]
  node [
    id 1024
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 1025
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 1026
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 1027
    label "p&#322;aszczyzna"
  ]
  node [
    id 1028
    label "odwadnia&#263;"
  ]
  node [
    id 1029
    label "przyswoi&#263;"
  ]
  node [
    id 1030
    label "sk&#243;ra"
  ]
  node [
    id 1031
    label "odwodni&#263;"
  ]
  node [
    id 1032
    label "ewoluowanie"
  ]
  node [
    id 1033
    label "staw"
  ]
  node [
    id 1034
    label "ow&#322;osienie"
  ]
  node [
    id 1035
    label "unerwienie"
  ]
  node [
    id 1036
    label "reakcja"
  ]
  node [
    id 1037
    label "wyewoluowanie"
  ]
  node [
    id 1038
    label "przyswajanie"
  ]
  node [
    id 1039
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 1040
    label "wyewoluowa&#263;"
  ]
  node [
    id 1041
    label "miejsce"
  ]
  node [
    id 1042
    label "biorytm"
  ]
  node [
    id 1043
    label "ewoluowa&#263;"
  ]
  node [
    id 1044
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1045
    label "istota_&#380;ywa"
  ]
  node [
    id 1046
    label "otworzy&#263;"
  ]
  node [
    id 1047
    label "otwiera&#263;"
  ]
  node [
    id 1048
    label "czynnik_biotyczny"
  ]
  node [
    id 1049
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 1050
    label "otworzenie"
  ]
  node [
    id 1051
    label "otwieranie"
  ]
  node [
    id 1052
    label "individual"
  ]
  node [
    id 1053
    label "szkielet"
  ]
  node [
    id 1054
    label "ty&#322;"
  ]
  node [
    id 1055
    label "przyswojenie"
  ]
  node [
    id 1056
    label "odwadnianie"
  ]
  node [
    id 1057
    label "odwodnienie"
  ]
  node [
    id 1058
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 1059
    label "starzenie_si&#281;"
  ]
  node [
    id 1060
    label "prz&#243;d"
  ]
  node [
    id 1061
    label "temperatura"
  ]
  node [
    id 1062
    label "l&#281;d&#378;wie"
  ]
  node [
    id 1063
    label "cia&#322;o"
  ]
  node [
    id 1064
    label "cz&#322;onek"
  ]
  node [
    id 1065
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 1066
    label "artykulator"
  ]
  node [
    id 1067
    label "kod"
  ]
  node [
    id 1068
    label "kawa&#322;ek"
  ]
  node [
    id 1069
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1070
    label "gramatyka"
  ]
  node [
    id 1071
    label "stylik"
  ]
  node [
    id 1072
    label "przet&#322;umaczenie"
  ]
  node [
    id 1073
    label "formalizowanie"
  ]
  node [
    id 1074
    label "ssanie"
  ]
  node [
    id 1075
    label "ssa&#263;"
  ]
  node [
    id 1076
    label "language"
  ]
  node [
    id 1077
    label "liza&#263;"
  ]
  node [
    id 1078
    label "napisa&#263;"
  ]
  node [
    id 1079
    label "konsonantyzm"
  ]
  node [
    id 1080
    label "wokalizm"
  ]
  node [
    id 1081
    label "pisa&#263;"
  ]
  node [
    id 1082
    label "fonetyka"
  ]
  node [
    id 1083
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1084
    label "jeniec"
  ]
  node [
    id 1085
    label "but"
  ]
  node [
    id 1086
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1087
    label "po_koroniarsku"
  ]
  node [
    id 1088
    label "t&#322;umaczenie"
  ]
  node [
    id 1089
    label "m&#243;wienie"
  ]
  node [
    id 1090
    label "pype&#263;"
  ]
  node [
    id 1091
    label "lizanie"
  ]
  node [
    id 1092
    label "pismo"
  ]
  node [
    id 1093
    label "formalizowa&#263;"
  ]
  node [
    id 1094
    label "rozumie&#263;"
  ]
  node [
    id 1095
    label "organ"
  ]
  node [
    id 1096
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1097
    label "makroglosja"
  ]
  node [
    id 1098
    label "m&#243;wi&#263;"
  ]
  node [
    id 1099
    label "jama_ustna"
  ]
  node [
    id 1100
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1101
    label "formacja_geologiczna"
  ]
  node [
    id 1102
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1103
    label "natural_language"
  ]
  node [
    id 1104
    label "s&#322;ownictwo"
  ]
  node [
    id 1105
    label "urz&#261;dzenie"
  ]
  node [
    id 1106
    label "kawa&#322;"
  ]
  node [
    id 1107
    label "plot"
  ]
  node [
    id 1108
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 1109
    label "piece"
  ]
  node [
    id 1110
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 1111
    label "podp&#322;ywanie"
  ]
  node [
    id 1112
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 1113
    label "struktura"
  ]
  node [
    id 1114
    label "code"
  ]
  node [
    id 1115
    label "szyfrowanie"
  ]
  node [
    id 1116
    label "ci&#261;g"
  ]
  node [
    id 1117
    label "szablon"
  ]
  node [
    id 1118
    label "internowanie"
  ]
  node [
    id 1119
    label "ojczyc"
  ]
  node [
    id 1120
    label "pojmaniec"
  ]
  node [
    id 1121
    label "niewolnik"
  ]
  node [
    id 1122
    label "internowa&#263;"
  ]
  node [
    id 1123
    label "aparat_artykulacyjny"
  ]
  node [
    id 1124
    label "tkanka"
  ]
  node [
    id 1125
    label "jednostka_organizacyjna"
  ]
  node [
    id 1126
    label "budowa"
  ]
  node [
    id 1127
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1128
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1129
    label "tw&#243;r"
  ]
  node [
    id 1130
    label "organogeneza"
  ]
  node [
    id 1131
    label "zesp&#243;&#322;"
  ]
  node [
    id 1132
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 1133
    label "struktura_anatomiczna"
  ]
  node [
    id 1134
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 1135
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1136
    label "Izba_Konsyliarska"
  ]
  node [
    id 1137
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1138
    label "stomia"
  ]
  node [
    id 1139
    label "dekortykacja"
  ]
  node [
    id 1140
    label "okolica"
  ]
  node [
    id 1141
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1142
    label "zboczenie"
  ]
  node [
    id 1143
    label "om&#243;wienie"
  ]
  node [
    id 1144
    label "sponiewieranie"
  ]
  node [
    id 1145
    label "discipline"
  ]
  node [
    id 1146
    label "omawia&#263;"
  ]
  node [
    id 1147
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1148
    label "tre&#347;&#263;"
  ]
  node [
    id 1149
    label "sponiewiera&#263;"
  ]
  node [
    id 1150
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1151
    label "tematyka"
  ]
  node [
    id 1152
    label "w&#261;tek"
  ]
  node [
    id 1153
    label "charakter"
  ]
  node [
    id 1154
    label "zbaczanie"
  ]
  node [
    id 1155
    label "program_nauczania"
  ]
  node [
    id 1156
    label "om&#243;wi&#263;"
  ]
  node [
    id 1157
    label "omawianie"
  ]
  node [
    id 1158
    label "thing"
  ]
  node [
    id 1159
    label "istota"
  ]
  node [
    id 1160
    label "zbacza&#263;"
  ]
  node [
    id 1161
    label "zboczy&#263;"
  ]
  node [
    id 1162
    label "zapi&#281;tek"
  ]
  node [
    id 1163
    label "sznurowad&#322;o"
  ]
  node [
    id 1164
    label "rozbijarka"
  ]
  node [
    id 1165
    label "podeszwa"
  ]
  node [
    id 1166
    label "obcas"
  ]
  node [
    id 1167
    label "wytw&#243;r"
  ]
  node [
    id 1168
    label "wzuwanie"
  ]
  node [
    id 1169
    label "wzu&#263;"
  ]
  node [
    id 1170
    label "przyszwa"
  ]
  node [
    id 1171
    label "raki"
  ]
  node [
    id 1172
    label "cholewa"
  ]
  node [
    id 1173
    label "cholewka"
  ]
  node [
    id 1174
    label "zel&#243;wka"
  ]
  node [
    id 1175
    label "obuwie"
  ]
  node [
    id 1176
    label "napi&#281;tek"
  ]
  node [
    id 1177
    label "wzucie"
  ]
  node [
    id 1178
    label "kom&#243;rka"
  ]
  node [
    id 1179
    label "furnishing"
  ]
  node [
    id 1180
    label "zabezpieczenie"
  ]
  node [
    id 1181
    label "wyrz&#261;dzenie"
  ]
  node [
    id 1182
    label "zagospodarowanie"
  ]
  node [
    id 1183
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 1184
    label "ig&#322;a"
  ]
  node [
    id 1185
    label "wirnik"
  ]
  node [
    id 1186
    label "aparatura"
  ]
  node [
    id 1187
    label "system_energetyczny"
  ]
  node [
    id 1188
    label "impulsator"
  ]
  node [
    id 1189
    label "mechanizm"
  ]
  node [
    id 1190
    label "sprz&#281;t"
  ]
  node [
    id 1191
    label "blokowanie"
  ]
  node [
    id 1192
    label "set"
  ]
  node [
    id 1193
    label "zablokowanie"
  ]
  node [
    id 1194
    label "przygotowanie"
  ]
  node [
    id 1195
    label "komora"
  ]
  node [
    id 1196
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 1197
    label "public_speaking"
  ]
  node [
    id 1198
    label "powiadanie"
  ]
  node [
    id 1199
    label "przepowiadanie"
  ]
  node [
    id 1200
    label "wyznawanie"
  ]
  node [
    id 1201
    label "wypowiadanie"
  ]
  node [
    id 1202
    label "wydobywanie"
  ]
  node [
    id 1203
    label "gaworzenie"
  ]
  node [
    id 1204
    label "wyra&#380;anie"
  ]
  node [
    id 1205
    label "formu&#322;owanie"
  ]
  node [
    id 1206
    label "dowalenie"
  ]
  node [
    id 1207
    label "przerywanie"
  ]
  node [
    id 1208
    label "wydawanie"
  ]
  node [
    id 1209
    label "dogadywanie_si&#281;"
  ]
  node [
    id 1210
    label "prawienie"
  ]
  node [
    id 1211
    label "ozywanie_si&#281;"
  ]
  node [
    id 1212
    label "zapeszanie"
  ]
  node [
    id 1213
    label "zwracanie_si&#281;"
  ]
  node [
    id 1214
    label "dysfonia"
  ]
  node [
    id 1215
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 1216
    label "speaking"
  ]
  node [
    id 1217
    label "zauwa&#380;enie"
  ]
  node [
    id 1218
    label "mawianie"
  ]
  node [
    id 1219
    label "opowiedzenie"
  ]
  node [
    id 1220
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 1221
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 1222
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 1223
    label "dogadanie_si&#281;"
  ]
  node [
    id 1224
    label "wygadanie"
  ]
  node [
    id 1225
    label "psychotest"
  ]
  node [
    id 1226
    label "wk&#322;ad"
  ]
  node [
    id 1227
    label "handwriting"
  ]
  node [
    id 1228
    label "przekaz"
  ]
  node [
    id 1229
    label "dzie&#322;o"
  ]
  node [
    id 1230
    label "paleograf"
  ]
  node [
    id 1231
    label "interpunkcja"
  ]
  node [
    id 1232
    label "dzia&#322;"
  ]
  node [
    id 1233
    label "grafia"
  ]
  node [
    id 1234
    label "communication"
  ]
  node [
    id 1235
    label "script"
  ]
  node [
    id 1236
    label "zajawka"
  ]
  node [
    id 1237
    label "list"
  ]
  node [
    id 1238
    label "adres"
  ]
  node [
    id 1239
    label "Zwrotnica"
  ]
  node [
    id 1240
    label "czasopismo"
  ]
  node [
    id 1241
    label "ok&#322;adka"
  ]
  node [
    id 1242
    label "ortografia"
  ]
  node [
    id 1243
    label "letter"
  ]
  node [
    id 1244
    label "paleografia"
  ]
  node [
    id 1245
    label "prasa"
  ]
  node [
    id 1246
    label "terminology"
  ]
  node [
    id 1247
    label "termin"
  ]
  node [
    id 1248
    label "fleksja"
  ]
  node [
    id 1249
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 1250
    label "sk&#322;adnia"
  ]
  node [
    id 1251
    label "morfologia"
  ]
  node [
    id 1252
    label "g&#322;osownia"
  ]
  node [
    id 1253
    label "zasymilowa&#263;"
  ]
  node [
    id 1254
    label "phonetics"
  ]
  node [
    id 1255
    label "palatogram"
  ]
  node [
    id 1256
    label "transkrypcja"
  ]
  node [
    id 1257
    label "zasymilowanie"
  ]
  node [
    id 1258
    label "styl"
  ]
  node [
    id 1259
    label "ozdabia&#263;"
  ]
  node [
    id 1260
    label "spell"
  ]
  node [
    id 1261
    label "skryba"
  ]
  node [
    id 1262
    label "read"
  ]
  node [
    id 1263
    label "donosi&#263;"
  ]
  node [
    id 1264
    label "dysgrafia"
  ]
  node [
    id 1265
    label "dysortografia"
  ]
  node [
    id 1266
    label "stworzy&#263;"
  ]
  node [
    id 1267
    label "write"
  ]
  node [
    id 1268
    label "donie&#347;&#263;"
  ]
  node [
    id 1269
    label "explanation"
  ]
  node [
    id 1270
    label "bronienie"
  ]
  node [
    id 1271
    label "remark"
  ]
  node [
    id 1272
    label "przek&#322;adanie"
  ]
  node [
    id 1273
    label "przekonywanie"
  ]
  node [
    id 1274
    label "uzasadnianie"
  ]
  node [
    id 1275
    label "rozwianie"
  ]
  node [
    id 1276
    label "rozwiewanie"
  ]
  node [
    id 1277
    label "gossip"
  ]
  node [
    id 1278
    label "przedstawianie"
  ]
  node [
    id 1279
    label "rendition"
  ]
  node [
    id 1280
    label "kr&#281;ty"
  ]
  node [
    id 1281
    label "zinterpretowa&#263;"
  ]
  node [
    id 1282
    label "put"
  ]
  node [
    id 1283
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 1284
    label "zrobi&#263;"
  ]
  node [
    id 1285
    label "przekona&#263;"
  ]
  node [
    id 1286
    label "frame"
  ]
  node [
    id 1287
    label "poja&#347;nia&#263;"
  ]
  node [
    id 1288
    label "elaborate"
  ]
  node [
    id 1289
    label "suplikowa&#263;"
  ]
  node [
    id 1290
    label "przek&#322;ada&#263;"
  ]
  node [
    id 1291
    label "przekonywa&#263;"
  ]
  node [
    id 1292
    label "interpretowa&#263;"
  ]
  node [
    id 1293
    label "broni&#263;"
  ]
  node [
    id 1294
    label "explain"
  ]
  node [
    id 1295
    label "przedstawia&#263;"
  ]
  node [
    id 1296
    label "sprawowa&#263;"
  ]
  node [
    id 1297
    label "uzasadnia&#263;"
  ]
  node [
    id 1298
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 1299
    label "gaworzy&#263;"
  ]
  node [
    id 1300
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1301
    label "rozmawia&#263;"
  ]
  node [
    id 1302
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1303
    label "umie&#263;"
  ]
  node [
    id 1304
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 1305
    label "dziama&#263;"
  ]
  node [
    id 1306
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 1307
    label "express"
  ]
  node [
    id 1308
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1309
    label "talk"
  ]
  node [
    id 1310
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1311
    label "prawi&#263;"
  ]
  node [
    id 1312
    label "powiada&#263;"
  ]
  node [
    id 1313
    label "tell"
  ]
  node [
    id 1314
    label "chew_the_fat"
  ]
  node [
    id 1315
    label "say"
  ]
  node [
    id 1316
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 1317
    label "wydobywa&#263;"
  ]
  node [
    id 1318
    label "okre&#347;la&#263;"
  ]
  node [
    id 1319
    label "hermeneutyka"
  ]
  node [
    id 1320
    label "apprehension"
  ]
  node [
    id 1321
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 1322
    label "interpretation"
  ]
  node [
    id 1323
    label "obja&#347;nienie"
  ]
  node [
    id 1324
    label "czucie"
  ]
  node [
    id 1325
    label "realization"
  ]
  node [
    id 1326
    label "kumanie"
  ]
  node [
    id 1327
    label "wnioskowanie"
  ]
  node [
    id 1328
    label "wiedzie&#263;"
  ]
  node [
    id 1329
    label "kuma&#263;"
  ]
  node [
    id 1330
    label "czu&#263;"
  ]
  node [
    id 1331
    label "match"
  ]
  node [
    id 1332
    label "empatia"
  ]
  node [
    id 1333
    label "odbiera&#263;"
  ]
  node [
    id 1334
    label "see"
  ]
  node [
    id 1335
    label "zna&#263;"
  ]
  node [
    id 1336
    label "validate"
  ]
  node [
    id 1337
    label "nadawa&#263;"
  ]
  node [
    id 1338
    label "precyzowa&#263;"
  ]
  node [
    id 1339
    label "nadawanie"
  ]
  node [
    id 1340
    label "formalny"
  ]
  node [
    id 1341
    label "picie"
  ]
  node [
    id 1342
    label "usta"
  ]
  node [
    id 1343
    label "ruszanie"
  ]
  node [
    id 1344
    label "&#347;lina"
  ]
  node [
    id 1345
    label "consumption"
  ]
  node [
    id 1346
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 1347
    label "rozpuszczanie"
  ]
  node [
    id 1348
    label "aspiration"
  ]
  node [
    id 1349
    label "wci&#261;ganie"
  ]
  node [
    id 1350
    label "odci&#261;ganie"
  ]
  node [
    id 1351
    label "wessanie"
  ]
  node [
    id 1352
    label "ga&#378;nik"
  ]
  node [
    id 1353
    label "wysysanie"
  ]
  node [
    id 1354
    label "wyssanie"
  ]
  node [
    id 1355
    label "wada_wrodzona"
  ]
  node [
    id 1356
    label "znami&#281;"
  ]
  node [
    id 1357
    label "krosta"
  ]
  node [
    id 1358
    label "spot"
  ]
  node [
    id 1359
    label "schorzenie"
  ]
  node [
    id 1360
    label "brodawka"
  ]
  node [
    id 1361
    label "pip"
  ]
  node [
    id 1362
    label "dotykanie"
  ]
  node [
    id 1363
    label "przesuwanie"
  ]
  node [
    id 1364
    label "zlizanie"
  ]
  node [
    id 1365
    label "g&#322;askanie"
  ]
  node [
    id 1366
    label "wylizywanie"
  ]
  node [
    id 1367
    label "zlizywanie"
  ]
  node [
    id 1368
    label "wylizanie"
  ]
  node [
    id 1369
    label "pi&#263;"
  ]
  node [
    id 1370
    label "sponge"
  ]
  node [
    id 1371
    label "mleko"
  ]
  node [
    id 1372
    label "rozpuszcza&#263;"
  ]
  node [
    id 1373
    label "wci&#261;ga&#263;"
  ]
  node [
    id 1374
    label "rusza&#263;"
  ]
  node [
    id 1375
    label "sucking"
  ]
  node [
    id 1376
    label "smoczek"
  ]
  node [
    id 1377
    label "salt_lick"
  ]
  node [
    id 1378
    label "dotyka&#263;"
  ]
  node [
    id 1379
    label "muska&#263;"
  ]
  node [
    id 1380
    label "rachunek_operatorowy"
  ]
  node [
    id 1381
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 1382
    label "kryptologia"
  ]
  node [
    id 1383
    label "logicyzm"
  ]
  node [
    id 1384
    label "logika"
  ]
  node [
    id 1385
    label "matematyka_czysta"
  ]
  node [
    id 1386
    label "forsing"
  ]
  node [
    id 1387
    label "modelowanie_matematyczne"
  ]
  node [
    id 1388
    label "matma"
  ]
  node [
    id 1389
    label "teoria_katastrof"
  ]
  node [
    id 1390
    label "kierunek"
  ]
  node [
    id 1391
    label "fizyka_matematyczna"
  ]
  node [
    id 1392
    label "teoria_graf&#243;w"
  ]
  node [
    id 1393
    label "rachunki"
  ]
  node [
    id 1394
    label "topologia_algebraiczna"
  ]
  node [
    id 1395
    label "matematyka_stosowana"
  ]
  node [
    id 1396
    label "przebieg"
  ]
  node [
    id 1397
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1398
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1399
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1400
    label "przeorientowywanie"
  ]
  node [
    id 1401
    label "studia"
  ]
  node [
    id 1402
    label "linia"
  ]
  node [
    id 1403
    label "bok"
  ]
  node [
    id 1404
    label "skr&#281;canie"
  ]
  node [
    id 1405
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1406
    label "przeorientowywa&#263;"
  ]
  node [
    id 1407
    label "orientowanie"
  ]
  node [
    id 1408
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1409
    label "przeorientowanie"
  ]
  node [
    id 1410
    label "zorientowanie"
  ]
  node [
    id 1411
    label "przeorientowa&#263;"
  ]
  node [
    id 1412
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1413
    label "zorientowa&#263;"
  ]
  node [
    id 1414
    label "g&#243;ra"
  ]
  node [
    id 1415
    label "orientowa&#263;"
  ]
  node [
    id 1416
    label "orientacja"
  ]
  node [
    id 1417
    label "bearing"
  ]
  node [
    id 1418
    label "skr&#281;cenie"
  ]
  node [
    id 1419
    label "arithmetic"
  ]
  node [
    id 1420
    label "dow&#243;d"
  ]
  node [
    id 1421
    label "one"
  ]
  node [
    id 1422
    label "skala"
  ]
  node [
    id 1423
    label "przeliczy&#263;"
  ]
  node [
    id 1424
    label "liczba_naturalna"
  ]
  node [
    id 1425
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 1426
    label "przeliczanie"
  ]
  node [
    id 1427
    label "przelicza&#263;"
  ]
  node [
    id 1428
    label "przeliczenie"
  ]
  node [
    id 1429
    label "ograniczenie"
  ]
  node [
    id 1430
    label "armia"
  ]
  node [
    id 1431
    label "nawr&#243;t_choroby"
  ]
  node [
    id 1432
    label "potomstwo"
  ]
  node [
    id 1433
    label "odwzorowanie"
  ]
  node [
    id 1434
    label "rysunek"
  ]
  node [
    id 1435
    label "scene"
  ]
  node [
    id 1436
    label "throw"
  ]
  node [
    id 1437
    label "float"
  ]
  node [
    id 1438
    label "punkt"
  ]
  node [
    id 1439
    label "projection"
  ]
  node [
    id 1440
    label "injection"
  ]
  node [
    id 1441
    label "blow"
  ]
  node [
    id 1442
    label "pomys&#322;"
  ]
  node [
    id 1443
    label "k&#322;ad"
  ]
  node [
    id 1444
    label "mold"
  ]
  node [
    id 1445
    label "logicism"
  ]
  node [
    id 1446
    label "doktryna_filozoficzna"
  ]
  node [
    id 1447
    label "filozofia_matematyki"
  ]
  node [
    id 1448
    label "informatyka"
  ]
  node [
    id 1449
    label "kryptografia"
  ]
  node [
    id 1450
    label "kryptoanaliza"
  ]
  node [
    id 1451
    label "izomorfizm"
  ]
  node [
    id 1452
    label "teoremat"
  ]
  node [
    id 1453
    label "logizacja"
  ]
  node [
    id 1454
    label "m&#261;dro&#347;&#263;"
  ]
  node [
    id 1455
    label "analityka"
  ]
  node [
    id 1456
    label "predykat"
  ]
  node [
    id 1457
    label "rozumno&#347;&#263;"
  ]
  node [
    id 1458
    label "metamatematyka"
  ]
  node [
    id 1459
    label "filozofia"
  ]
  node [
    id 1460
    label "operacja_logiczna"
  ]
  node [
    id 1461
    label "sylogistyka"
  ]
  node [
    id 1462
    label "dialektyka"
  ]
  node [
    id 1463
    label "podzia&#322;_logiczny"
  ]
  node [
    id 1464
    label "logistics"
  ]
  node [
    id 1465
    label "obacza&#263;"
  ]
  node [
    id 1466
    label "widzie&#263;"
  ]
  node [
    id 1467
    label "orientowa&#263;_si&#281;"
  ]
  node [
    id 1468
    label "notice"
  ]
  node [
    id 1469
    label "perceive"
  ]
  node [
    id 1470
    label "postrzega&#263;"
  ]
  node [
    id 1471
    label "aprobowa&#263;"
  ]
  node [
    id 1472
    label "wzrok"
  ]
  node [
    id 1473
    label "zmale&#263;"
  ]
  node [
    id 1474
    label "punkt_widzenia"
  ]
  node [
    id 1475
    label "male&#263;"
  ]
  node [
    id 1476
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 1477
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 1478
    label "spotka&#263;"
  ]
  node [
    id 1479
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 1480
    label "ogl&#261;da&#263;"
  ]
  node [
    id 1481
    label "spowodowa&#263;"
  ]
  node [
    id 1482
    label "go_steady"
  ]
  node [
    id 1483
    label "reagowa&#263;"
  ]
  node [
    id 1484
    label "os&#261;dza&#263;"
  ]
  node [
    id 1485
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 1486
    label "zauwa&#380;a&#263;"
  ]
  node [
    id 1487
    label "komunikowa&#263;"
  ]
  node [
    id 1488
    label "convey"
  ]
  node [
    id 1489
    label "communicate"
  ]
  node [
    id 1490
    label "argue"
  ]
  node [
    id 1491
    label "dostosowywa&#263;"
  ]
  node [
    id 1492
    label "shape"
  ]
  node [
    id 1493
    label "dawa&#263;"
  ]
  node [
    id 1494
    label "assign"
  ]
  node [
    id 1495
    label "gada&#263;"
  ]
  node [
    id 1496
    label "rekomendowa&#263;"
  ]
  node [
    id 1497
    label "za&#322;atwia&#263;"
  ]
  node [
    id 1498
    label "obgadywa&#263;"
  ]
  node [
    id 1499
    label "sprawia&#263;"
  ]
  node [
    id 1500
    label "przesy&#322;a&#263;"
  ]
  node [
    id 1501
    label "zmienia&#263;"
  ]
  node [
    id 1502
    label "equal"
  ]
  node [
    id 1503
    label "reakcja_chemiczna"
  ]
  node [
    id 1504
    label "charakterystyka"
  ]
  node [
    id 1505
    label "m&#322;ot"
  ]
  node [
    id 1506
    label "znak"
  ]
  node [
    id 1507
    label "drzewo"
  ]
  node [
    id 1508
    label "pr&#243;ba"
  ]
  node [
    id 1509
    label "attribute"
  ]
  node [
    id 1510
    label "marka"
  ]
  node [
    id 1511
    label "posiada&#263;"
  ]
  node [
    id 1512
    label "potencja&#322;"
  ]
  node [
    id 1513
    label "zapomnienie"
  ]
  node [
    id 1514
    label "zapomina&#263;"
  ]
  node [
    id 1515
    label "zapominanie"
  ]
  node [
    id 1516
    label "ability"
  ]
  node [
    id 1517
    label "obliczeniowo"
  ]
  node [
    id 1518
    label "zapomnie&#263;"
  ]
  node [
    id 1519
    label "proces_my&#347;lowy"
  ]
  node [
    id 1520
    label "domy&#347;lanie_si&#281;"
  ]
  node [
    id 1521
    label "pilnowanie"
  ]
  node [
    id 1522
    label "s&#261;dzenie"
  ]
  node [
    id 1523
    label "judgment"
  ]
  node [
    id 1524
    label "troskanie_si&#281;"
  ]
  node [
    id 1525
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 1526
    label "treatment"
  ]
  node [
    id 1527
    label "walczenie"
  ]
  node [
    id 1528
    label "zinterpretowanie"
  ]
  node [
    id 1529
    label "walczy&#263;"
  ]
  node [
    id 1530
    label "skupianie_si&#281;"
  ]
  node [
    id 1531
    label "reflection"
  ]
  node [
    id 1532
    label "wzlecie&#263;"
  ]
  node [
    id 1533
    label "wzlecenie"
  ]
  node [
    id 1534
    label "legislacyjnie"
  ]
  node [
    id 1535
    label "nast&#281;pstwo"
  ]
  node [
    id 1536
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1537
    label "boski"
  ]
  node [
    id 1538
    label "krajobraz"
  ]
  node [
    id 1539
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1540
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1541
    label "przywidzenie"
  ]
  node [
    id 1542
    label "presence"
  ]
  node [
    id 1543
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1544
    label "fabrication"
  ]
  node [
    id 1545
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 1546
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 1547
    label "tentegowanie"
  ]
  node [
    id 1548
    label "bezproblemowy"
  ]
  node [
    id 1549
    label "eskortowanie"
  ]
  node [
    id 1550
    label "str&#243;&#380;owanie"
  ]
  node [
    id 1551
    label "guard_duty"
  ]
  node [
    id 1552
    label "appreciation"
  ]
  node [
    id 1553
    label "zagranie"
  ]
  node [
    id 1554
    label "ocenienie"
  ]
  node [
    id 1555
    label "zanalizowanie"
  ]
  node [
    id 1556
    label "proszenie"
  ]
  node [
    id 1557
    label "dochodzenie"
  ]
  node [
    id 1558
    label "lead"
  ]
  node [
    id 1559
    label "konkluzja"
  ]
  node [
    id 1560
    label "sk&#322;adanie"
  ]
  node [
    id 1561
    label "wniosek"
  ]
  node [
    id 1562
    label "sprzeciwianie_si&#281;"
  ]
  node [
    id 1563
    label "obstawanie"
  ]
  node [
    id 1564
    label "adwokatowanie"
  ]
  node [
    id 1565
    label "opieranie_si&#281;"
  ]
  node [
    id 1566
    label "powalczenie"
  ]
  node [
    id 1567
    label "staranie_si&#281;"
  ]
  node [
    id 1568
    label "rywalizowanie"
  ]
  node [
    id 1569
    label "bojownik"
  ]
  node [
    id 1570
    label "staczanie"
  ]
  node [
    id 1571
    label "oblegni&#281;cie"
  ]
  node [
    id 1572
    label "zwalczenie"
  ]
  node [
    id 1573
    label "usi&#322;owanie"
  ]
  node [
    id 1574
    label "struggle"
  ]
  node [
    id 1575
    label "obleganie"
  ]
  node [
    id 1576
    label "playing"
  ]
  node [
    id 1577
    label "ust&#281;powanie"
  ]
  node [
    id 1578
    label "zawody"
  ]
  node [
    id 1579
    label "zwojowanie"
  ]
  node [
    id 1580
    label "wywalczenie"
  ]
  node [
    id 1581
    label "nawojowanie_si&#281;"
  ]
  node [
    id 1582
    label "or&#281;dowanie"
  ]
  node [
    id 1583
    label "Ascension"
  ]
  node [
    id 1584
    label "oderwanie_si&#281;"
  ]
  node [
    id 1585
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1586
    label "rise"
  ]
  node [
    id 1587
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1588
    label "zmaga&#263;_si&#281;"
  ]
  node [
    id 1589
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 1590
    label "opowiada&#263;_si&#281;"
  ]
  node [
    id 1591
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 1592
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1593
    label "fight"
  ]
  node [
    id 1594
    label "wrestle"
  ]
  node [
    id 1595
    label "contend"
  ]
  node [
    id 1596
    label "oderwa&#263;_si&#281;"
  ]
  node [
    id 1597
    label "ascend"
  ]
  node [
    id 1598
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 1599
    label "powodowanie"
  ]
  node [
    id 1600
    label "zas&#261;dzanie"
  ]
  node [
    id 1601
    label "zarz&#261;dzanie"
  ]
  node [
    id 1602
    label "traktowanie"
  ]
  node [
    id 1603
    label "znajdowanie"
  ]
  node [
    id 1604
    label "dostawanie"
  ]
  node [
    id 1605
    label "wyrokowanie"
  ]
  node [
    id 1606
    label "skazanie"
  ]
  node [
    id 1607
    label "orzekanie"
  ]
  node [
    id 1608
    label "skazywanie"
  ]
  node [
    id 1609
    label "appraisal"
  ]
  node [
    id 1610
    label "orzekni&#281;cie"
  ]
  node [
    id 1611
    label "zas&#261;dzenie"
  ]
  node [
    id 1612
    label "zawyrokowanie"
  ]
  node [
    id 1613
    label "s&#281;dziowanie"
  ]
  node [
    id 1614
    label "wsp&#243;&#322;rz&#261;dzenie"
  ]
  node [
    id 1615
    label "szczodry"
  ]
  node [
    id 1616
    label "s&#322;uszny"
  ]
  node [
    id 1617
    label "uczciwy"
  ]
  node [
    id 1618
    label "przekonuj&#261;cy"
  ]
  node [
    id 1619
    label "szczyry"
  ]
  node [
    id 1620
    label "szczerze"
  ]
  node [
    id 1621
    label "czysty"
  ]
  node [
    id 1622
    label "spokojny"
  ]
  node [
    id 1623
    label "udany"
  ]
  node [
    id 1624
    label "pogodnie"
  ]
  node [
    id 1625
    label "skrawy"
  ]
  node [
    id 1626
    label "b&#322;yszcz&#261;co"
  ]
  node [
    id 1627
    label "przepe&#322;niony"
  ]
  node [
    id 1628
    label "nienaruszony"
  ]
  node [
    id 1629
    label "doskona&#322;y"
  ]
  node [
    id 1630
    label "sprawny"
  ]
  node [
    id 1631
    label "jednoznacznie"
  ]
  node [
    id 1632
    label "identyczny"
  ]
  node [
    id 1633
    label "pojmowalny"
  ]
  node [
    id 1634
    label "uzasadniony"
  ]
  node [
    id 1635
    label "wyja&#347;nienie"
  ]
  node [
    id 1636
    label "wyja&#347;nianie_si&#281;"
  ]
  node [
    id 1637
    label "rozja&#347;nienie"
  ]
  node [
    id 1638
    label "przeja&#347;nienie_si&#281;"
  ]
  node [
    id 1639
    label "zrozumiale"
  ]
  node [
    id 1640
    label "przeja&#347;nianie_si&#281;"
  ]
  node [
    id 1641
    label "sensowny"
  ]
  node [
    id 1642
    label "rozja&#347;nianie"
  ]
  node [
    id 1643
    label "wyja&#347;nienie_si&#281;"
  ]
  node [
    id 1644
    label "prze&#378;roczy"
  ]
  node [
    id 1645
    label "przezroczy&#347;cie"
  ]
  node [
    id 1646
    label "klarowanie"
  ]
  node [
    id 1647
    label "klarowanie_si&#281;"
  ]
  node [
    id 1648
    label "sklarowanie"
  ]
  node [
    id 1649
    label "klarownie"
  ]
  node [
    id 1650
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 1651
    label "dobroczynny"
  ]
  node [
    id 1652
    label "czw&#243;rka"
  ]
  node [
    id 1653
    label "mi&#322;y"
  ]
  node [
    id 1654
    label "grzeczny"
  ]
  node [
    id 1655
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1656
    label "powitanie"
  ]
  node [
    id 1657
    label "zwrot"
  ]
  node [
    id 1658
    label "pomy&#347;lny"
  ]
  node [
    id 1659
    label "moralny"
  ]
  node [
    id 1660
    label "drogi"
  ]
  node [
    id 1661
    label "odpowiedni"
  ]
  node [
    id 1662
    label "pos&#322;uszny"
  ]
  node [
    id 1663
    label "ja&#347;niej"
  ]
  node [
    id 1664
    label "ja&#347;nie"
  ]
  node [
    id 1665
    label "skutecznie"
  ]
  node [
    id 1666
    label "sprawnie"
  ]
  node [
    id 1667
    label "czujny"
  ]
  node [
    id 1668
    label "przytomnie"
  ]
  node [
    id 1669
    label "instalacja"
  ]
  node [
    id 1670
    label "lighting"
  ]
  node [
    id 1671
    label "lighter"
  ]
  node [
    id 1672
    label "spowodowanie"
  ]
  node [
    id 1673
    label "interpretacja"
  ]
  node [
    id 1674
    label "nat&#281;&#380;enie"
  ]
  node [
    id 1675
    label "&#347;wiecenie"
  ]
  node [
    id 1676
    label "prze&#347;wietlanie"
  ]
  node [
    id 1677
    label "carat"
  ]
  node [
    id 1678
    label "bia&#322;y_murzyn"
  ]
  node [
    id 1679
    label "Rosjanin"
  ]
  node [
    id 1680
    label "bia&#322;e"
  ]
  node [
    id 1681
    label "jasnosk&#243;ry"
  ]
  node [
    id 1682
    label "bierka_szachowa"
  ]
  node [
    id 1683
    label "bia&#322;y_taniec"
  ]
  node [
    id 1684
    label "dzia&#322;acz"
  ]
  node [
    id 1685
    label "bezbarwny"
  ]
  node [
    id 1686
    label "siwy"
  ]
  node [
    id 1687
    label "bia&#322;a_n&#281;dza"
  ]
  node [
    id 1688
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1689
    label "Polak"
  ]
  node [
    id 1690
    label "medyczny"
  ]
  node [
    id 1691
    label "bia&#322;o"
  ]
  node [
    id 1692
    label "typ_orientalny"
  ]
  node [
    id 1693
    label "libera&#322;"
  ]
  node [
    id 1694
    label "&#347;nie&#380;nie"
  ]
  node [
    id 1695
    label "konserwatysta"
  ]
  node [
    id 1696
    label "&#347;nie&#380;no"
  ]
  node [
    id 1697
    label "bia&#322;as"
  ]
  node [
    id 1698
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 1699
    label "blady"
  ]
  node [
    id 1700
    label "bia&#322;osk&#243;ry"
  ]
  node [
    id 1701
    label "nacjonalista"
  ]
  node [
    id 1702
    label "pos&#322;uchanie"
  ]
  node [
    id 1703
    label "s&#261;d"
  ]
  node [
    id 1704
    label "sparafrazowanie"
  ]
  node [
    id 1705
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 1706
    label "strawestowa&#263;"
  ]
  node [
    id 1707
    label "sparafrazowa&#263;"
  ]
  node [
    id 1708
    label "trawestowa&#263;"
  ]
  node [
    id 1709
    label "sformu&#322;owanie"
  ]
  node [
    id 1710
    label "parafrazowanie"
  ]
  node [
    id 1711
    label "ozdobnik"
  ]
  node [
    id 1712
    label "delimitacja"
  ]
  node [
    id 1713
    label "parafrazowa&#263;"
  ]
  node [
    id 1714
    label "stylizacja"
  ]
  node [
    id 1715
    label "komunikat"
  ]
  node [
    id 1716
    label "trawestowanie"
  ]
  node [
    id 1717
    label "strawestowanie"
  ]
  node [
    id 1718
    label "event"
  ]
  node [
    id 1719
    label "kreacjonista"
  ]
  node [
    id 1720
    label "roi&#263;_si&#281;"
  ]
  node [
    id 1721
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 1722
    label "wygl&#261;d"
  ]
  node [
    id 1723
    label "stylization"
  ]
  node [
    id 1724
    label "otoczka"
  ]
  node [
    id 1725
    label "modyfikacja"
  ]
  node [
    id 1726
    label "zestawienie"
  ]
  node [
    id 1727
    label "szafiarka"
  ]
  node [
    id 1728
    label "wording"
  ]
  node [
    id 1729
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 1730
    label "statement"
  ]
  node [
    id 1731
    label "zapisanie"
  ]
  node [
    id 1732
    label "rzucenie"
  ]
  node [
    id 1733
    label "dekor"
  ]
  node [
    id 1734
    label "okre&#347;lenie"
  ]
  node [
    id 1735
    label "ornamentyka"
  ]
  node [
    id 1736
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1737
    label "wyra&#380;enie"
  ]
  node [
    id 1738
    label "dekoracja"
  ]
  node [
    id 1739
    label "boundary_line"
  ]
  node [
    id 1740
    label "podzia&#322;"
  ]
  node [
    id 1741
    label "satyra"
  ]
  node [
    id 1742
    label "parodiowanie"
  ]
  node [
    id 1743
    label "rozwlec_si&#281;"
  ]
  node [
    id 1744
    label "rozwlekanie_si&#281;"
  ]
  node [
    id 1745
    label "rozwleczenie_si&#281;"
  ]
  node [
    id 1746
    label "zmodyfikowa&#263;"
  ]
  node [
    id 1747
    label "powt&#243;rzy&#263;"
  ]
  node [
    id 1748
    label "rozwleka&#263;_si&#281;"
  ]
  node [
    id 1749
    label "przetworzy&#263;"
  ]
  node [
    id 1750
    label "powtarza&#263;"
  ]
  node [
    id 1751
    label "modyfikowa&#263;"
  ]
  node [
    id 1752
    label "paraphrase"
  ]
  node [
    id 1753
    label "przetwarza&#263;"
  ]
  node [
    id 1754
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 1755
    label "spotkanie"
  ]
  node [
    id 1756
    label "wys&#322;uchanie"
  ]
  node [
    id 1757
    label "audience"
  ]
  node [
    id 1758
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1759
    label "podejrzany"
  ]
  node [
    id 1760
    label "s&#261;downictwo"
  ]
  node [
    id 1761
    label "biuro"
  ]
  node [
    id 1762
    label "court"
  ]
  node [
    id 1763
    label "forum"
  ]
  node [
    id 1764
    label "urz&#261;d"
  ]
  node [
    id 1765
    label "oskar&#380;yciel"
  ]
  node [
    id 1766
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1767
    label "skazany"
  ]
  node [
    id 1768
    label "post&#281;powanie"
  ]
  node [
    id 1769
    label "my&#347;l"
  ]
  node [
    id 1770
    label "pods&#261;dny"
  ]
  node [
    id 1771
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1772
    label "antylogizm"
  ]
  node [
    id 1773
    label "konektyw"
  ]
  node [
    id 1774
    label "&#347;wiadek"
  ]
  node [
    id 1775
    label "procesowicz"
  ]
  node [
    id 1776
    label "strona"
  ]
  node [
    id 1777
    label "return"
  ]
  node [
    id 1778
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 1779
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1780
    label "porobi&#263;"
  ]
  node [
    id 1781
    label "sparodiowanie"
  ]
  node [
    id 1782
    label "parodiowa&#263;"
  ]
  node [
    id 1783
    label "farce"
  ]
  node [
    id 1784
    label "przetworzenie"
  ]
  node [
    id 1785
    label "powt&#243;rzenie"
  ]
  node [
    id 1786
    label "sparodiowa&#263;"
  ]
  node [
    id 1787
    label "powtarzanie"
  ]
  node [
    id 1788
    label "przetwarzanie"
  ]
  node [
    id 1789
    label "puszcza&#263;"
  ]
  node [
    id 1790
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 1791
    label "rozpakowywa&#263;"
  ]
  node [
    id 1792
    label "rozstawia&#263;"
  ]
  node [
    id 1793
    label "dopowiada&#263;"
  ]
  node [
    id 1794
    label "inflate"
  ]
  node [
    id 1795
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 1796
    label "wydawa&#263;"
  ]
  node [
    id 1797
    label "dissolve"
  ]
  node [
    id 1798
    label "zwalnia&#263;"
  ]
  node [
    id 1799
    label "go"
  ]
  node [
    id 1800
    label "dematerializowa&#263;_si&#281;"
  ]
  node [
    id 1801
    label "lease"
  ]
  node [
    id 1802
    label "dzier&#380;awi&#263;"
  ]
  node [
    id 1803
    label "odbarwia&#263;_si&#281;"
  ]
  node [
    id 1804
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 1805
    label "oddawa&#263;"
  ]
  node [
    id 1806
    label "ust&#281;powa&#263;"
  ]
  node [
    id 1807
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 1808
    label "wypuszcza&#263;"
  ]
  node [
    id 1809
    label "permit"
  ]
  node [
    id 1810
    label "zezwala&#263;"
  ]
  node [
    id 1811
    label "increase"
  ]
  node [
    id 1812
    label "discourse"
  ]
  node [
    id 1813
    label "wt&#243;rowa&#263;"
  ]
  node [
    id 1814
    label "dorabia&#263;"
  ]
  node [
    id 1815
    label "bind"
  ]
  node [
    id 1816
    label "ujawnia&#263;"
  ]
  node [
    id 1817
    label "dodawa&#263;"
  ]
  node [
    id 1818
    label "pozostawia&#263;"
  ]
  node [
    id 1819
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1820
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 1821
    label "przewidywa&#263;"
  ]
  node [
    id 1822
    label "przyznawa&#263;"
  ]
  node [
    id 1823
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 1824
    label "umieszcza&#263;"
  ]
  node [
    id 1825
    label "ocenia&#263;"
  ]
  node [
    id 1826
    label "zastawia&#263;"
  ]
  node [
    id 1827
    label "stanowisko"
  ]
  node [
    id 1828
    label "wskazywa&#263;"
  ]
  node [
    id 1829
    label "introduce"
  ]
  node [
    id 1830
    label "uruchamia&#263;"
  ]
  node [
    id 1831
    label "fundowa&#263;"
  ]
  node [
    id 1832
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 1833
    label "deliver"
  ]
  node [
    id 1834
    label "wyznacza&#263;"
  ]
  node [
    id 1835
    label "rozsuwa&#263;"
  ]
  node [
    id 1836
    label "order"
  ]
  node [
    id 1837
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 1838
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 1839
    label "rozmieszcza&#263;"
  ]
  node [
    id 1840
    label "wyjmowa&#263;"
  ]
  node [
    id 1841
    label "opr&#243;&#380;nia&#263;"
  ]
  node [
    id 1842
    label "przywraca&#263;"
  ]
  node [
    id 1843
    label "publicize"
  ]
  node [
    id 1844
    label "psu&#263;"
  ]
  node [
    id 1845
    label "wygrywa&#263;"
  ]
  node [
    id 1846
    label "exsert"
  ]
  node [
    id 1847
    label "dzieli&#263;"
  ]
  node [
    id 1848
    label "oddala&#263;"
  ]
  node [
    id 1849
    label "postrzeganie"
  ]
  node [
    id 1850
    label "przewidywanie"
  ]
  node [
    id 1851
    label "sztywnienie"
  ]
  node [
    id 1852
    label "zmys&#322;"
  ]
  node [
    id 1853
    label "smell"
  ]
  node [
    id 1854
    label "emotion"
  ]
  node [
    id 1855
    label "sztywnie&#263;"
  ]
  node [
    id 1856
    label "uczuwanie"
  ]
  node [
    id 1857
    label "owiewanie"
  ]
  node [
    id 1858
    label "ogarnianie"
  ]
  node [
    id 1859
    label "tactile_property"
  ]
  node [
    id 1860
    label "p&#322;&#243;d"
  ]
  node [
    id 1861
    label "report"
  ]
  node [
    id 1862
    label "przedstawienie"
  ]
  node [
    id 1863
    label "informacja"
  ]
  node [
    id 1864
    label "odniesienie"
  ]
  node [
    id 1865
    label "otoczenie"
  ]
  node [
    id 1866
    label "background"
  ]
  node [
    id 1867
    label "causal_agent"
  ]
  node [
    id 1868
    label "context"
  ]
  node [
    id 1869
    label "fragment"
  ]
  node [
    id 1870
    label "hermeneutics"
  ]
  node [
    id 1871
    label "ekscerpcja"
  ]
  node [
    id 1872
    label "j&#281;zykowo"
  ]
  node [
    id 1873
    label "redakcja"
  ]
  node [
    id 1874
    label "pomini&#281;cie"
  ]
  node [
    id 1875
    label "preparacja"
  ]
  node [
    id 1876
    label "odmianka"
  ]
  node [
    id 1877
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1878
    label "koniektura"
  ]
  node [
    id 1879
    label "obelga"
  ]
  node [
    id 1880
    label "obrazowanie"
  ]
  node [
    id 1881
    label "dorobek"
  ]
  node [
    id 1882
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 1883
    label "retrospektywa"
  ]
  node [
    id 1884
    label "works"
  ]
  node [
    id 1885
    label "tetralogia"
  ]
  node [
    id 1886
    label "cholera"
  ]
  node [
    id 1887
    label "ubliga"
  ]
  node [
    id 1888
    label "niedorobek"
  ]
  node [
    id 1889
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 1890
    label "chuj"
  ]
  node [
    id 1891
    label "bluzg"
  ]
  node [
    id 1892
    label "wyzwisko"
  ]
  node [
    id 1893
    label "indignation"
  ]
  node [
    id 1894
    label "pies"
  ]
  node [
    id 1895
    label "wrzuta"
  ]
  node [
    id 1896
    label "chujowy"
  ]
  node [
    id 1897
    label "krzywda"
  ]
  node [
    id 1898
    label "szmata"
  ]
  node [
    id 1899
    label "odmiana"
  ]
  node [
    id 1900
    label "preparation"
  ]
  node [
    id 1901
    label "proces_technologiczny"
  ]
  node [
    id 1902
    label "uj&#281;cie"
  ]
  node [
    id 1903
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 1904
    label "pozostawi&#263;"
  ]
  node [
    id 1905
    label "obni&#380;y&#263;"
  ]
  node [
    id 1906
    label "zostawi&#263;"
  ]
  node [
    id 1907
    label "przesta&#263;"
  ]
  node [
    id 1908
    label "potani&#263;"
  ]
  node [
    id 1909
    label "drop"
  ]
  node [
    id 1910
    label "evacuate"
  ]
  node [
    id 1911
    label "humiliate"
  ]
  node [
    id 1912
    label "leave"
  ]
  node [
    id 1913
    label "straci&#263;"
  ]
  node [
    id 1914
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1915
    label "authorize"
  ]
  node [
    id 1916
    label "omin&#261;&#263;"
  ]
  node [
    id 1917
    label "u&#380;ytkownik"
  ]
  node [
    id 1918
    label "komunikacyjnie"
  ]
  node [
    id 1919
    label "redaktor"
  ]
  node [
    id 1920
    label "radio"
  ]
  node [
    id 1921
    label "composition"
  ]
  node [
    id 1922
    label "wydawnictwo"
  ]
  node [
    id 1923
    label "redaction"
  ]
  node [
    id 1924
    label "telewizja"
  ]
  node [
    id 1925
    label "obr&#243;bka"
  ]
  node [
    id 1926
    label "conjecture"
  ]
  node [
    id 1927
    label "wyb&#243;r"
  ]
  node [
    id 1928
    label "dokumentacja"
  ]
  node [
    id 1929
    label "ellipsis"
  ]
  node [
    id 1930
    label "wykluczenie"
  ]
  node [
    id 1931
    label "figura_my&#347;li"
  ]
  node [
    id 1932
    label "zakomunikowa&#263;"
  ]
  node [
    id 1933
    label "zapoznawa&#263;"
  ]
  node [
    id 1934
    label "zawiera&#263;"
  ]
  node [
    id 1935
    label "obznajamia&#263;"
  ]
  node [
    id 1936
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1937
    label "krzywa"
  ]
  node [
    id 1938
    label "odcinek"
  ]
  node [
    id 1939
    label "straight_line"
  ]
  node [
    id 1940
    label "trasa"
  ]
  node [
    id 1941
    label "proste_sko&#347;ne"
  ]
  node [
    id 1942
    label "figura_geometryczna"
  ]
  node [
    id 1943
    label "poprowadzi&#263;"
  ]
  node [
    id 1944
    label "curvature"
  ]
  node [
    id 1945
    label "curve"
  ]
  node [
    id 1946
    label "teren"
  ]
  node [
    id 1947
    label "pole"
  ]
  node [
    id 1948
    label "part"
  ]
  node [
    id 1949
    label "line"
  ]
  node [
    id 1950
    label "coupon"
  ]
  node [
    id 1951
    label "pokwitowanie"
  ]
  node [
    id 1952
    label "moneta"
  ]
  node [
    id 1953
    label "epizod"
  ]
  node [
    id 1954
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1955
    label "ust&#281;p"
  ]
  node [
    id 1956
    label "plan"
  ]
  node [
    id 1957
    label "obiekt_matematyczny"
  ]
  node [
    id 1958
    label "plamka"
  ]
  node [
    id 1959
    label "stopie&#324;_pisma"
  ]
  node [
    id 1960
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1961
    label "mark"
  ]
  node [
    id 1962
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1963
    label "zapunktowa&#263;"
  ]
  node [
    id 1964
    label "podpunkt"
  ]
  node [
    id 1965
    label "wojsko"
  ]
  node [
    id 1966
    label "point"
  ]
  node [
    id 1967
    label "pozycja"
  ]
  node [
    id 1968
    label "droga"
  ]
  node [
    id 1969
    label "infrastruktura"
  ]
  node [
    id 1970
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 1971
    label "w&#281;ze&#322;"
  ]
  node [
    id 1972
    label "marszrutyzacja"
  ]
  node [
    id 1973
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1974
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 1975
    label "podbieg"
  ]
  node [
    id 1976
    label "&#322;atwi&#263;"
  ]
  node [
    id 1977
    label "ease"
  ]
  node [
    id 1978
    label "mie&#263;_miejsce"
  ]
  node [
    id 1979
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1980
    label "motywowa&#263;"
  ]
  node [
    id 1981
    label "obserwowanie"
  ]
  node [
    id 1982
    label "zrecenzowanie"
  ]
  node [
    id 1983
    label "kontrola"
  ]
  node [
    id 1984
    label "analysis"
  ]
  node [
    id 1985
    label "rektalny"
  ]
  node [
    id 1986
    label "ustalenie"
  ]
  node [
    id 1987
    label "macanie"
  ]
  node [
    id 1988
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 1989
    label "udowadnianie"
  ]
  node [
    id 1990
    label "bia&#322;a_niedziela"
  ]
  node [
    id 1991
    label "diagnostyka"
  ]
  node [
    id 1992
    label "dociekanie"
  ]
  node [
    id 1993
    label "sprawdzanie"
  ]
  node [
    id 1994
    label "penetrowanie"
  ]
  node [
    id 1995
    label "krytykowanie"
  ]
  node [
    id 1996
    label "ustalanie"
  ]
  node [
    id 1997
    label "rozpatrywanie"
  ]
  node [
    id 1998
    label "investigation"
  ]
  node [
    id 1999
    label "wziernikowanie"
  ]
  node [
    id 2000
    label "examination"
  ]
  node [
    id 2001
    label "czepianie_si&#281;"
  ]
  node [
    id 2002
    label "opiniowanie"
  ]
  node [
    id 2003
    label "ocenianie"
  ]
  node [
    id 2004
    label "discussion"
  ]
  node [
    id 2005
    label "dyskutowanie"
  ]
  node [
    id 2006
    label "zaopiniowanie"
  ]
  node [
    id 2007
    label "colony"
  ]
  node [
    id 2008
    label "colonization"
  ]
  node [
    id 2009
    label "decydowanie"
  ]
  node [
    id 2010
    label "umacnianie"
  ]
  node [
    id 2011
    label "liquidation"
  ]
  node [
    id 2012
    label "umocnienie"
  ]
  node [
    id 2013
    label "appointment"
  ]
  node [
    id 2014
    label "localization"
  ]
  node [
    id 2015
    label "zdecydowanie"
  ]
  node [
    id 2016
    label "przeszukiwanie"
  ]
  node [
    id 2017
    label "docieranie"
  ]
  node [
    id 2018
    label "penetration"
  ]
  node [
    id 2019
    label "przemy&#347;liwanie"
  ]
  node [
    id 2020
    label "legalizacja_ponowna"
  ]
  node [
    id 2021
    label "w&#322;adza"
  ]
  node [
    id 2022
    label "perlustracja"
  ]
  node [
    id 2023
    label "legalizacja_pierwotna"
  ]
  node [
    id 2024
    label "podejmowanie"
  ]
  node [
    id 2025
    label "effort"
  ]
  node [
    id 2026
    label "essay"
  ]
  node [
    id 2027
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 2028
    label "redagowanie"
  ]
  node [
    id 2029
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 2030
    label "przymierzanie"
  ]
  node [
    id 2031
    label "przymierzenie"
  ]
  node [
    id 2032
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 2033
    label "najem"
  ]
  node [
    id 2034
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 2035
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 2036
    label "zak&#322;ad"
  ]
  node [
    id 2037
    label "stosunek_pracy"
  ]
  node [
    id 2038
    label "benedykty&#324;ski"
  ]
  node [
    id 2039
    label "poda&#380;_pracy"
  ]
  node [
    id 2040
    label "pracowanie"
  ]
  node [
    id 2041
    label "tyrka"
  ]
  node [
    id 2042
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 2043
    label "zaw&#243;d"
  ]
  node [
    id 2044
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 2045
    label "tynkarski"
  ]
  node [
    id 2046
    label "pracowa&#263;"
  ]
  node [
    id 2047
    label "czynnik_produkcji"
  ]
  node [
    id 2048
    label "zobowi&#261;zanie"
  ]
  node [
    id 2049
    label "kierownictwo"
  ]
  node [
    id 2050
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 2051
    label "analizowanie"
  ]
  node [
    id 2052
    label "presentation"
  ]
  node [
    id 2053
    label "pokazywanie"
  ]
  node [
    id 2054
    label "endoscopy"
  ]
  node [
    id 2055
    label "rozmy&#347;lanie"
  ]
  node [
    id 2056
    label "quest"
  ]
  node [
    id 2057
    label "dop&#322;ywanie"
  ]
  node [
    id 2058
    label "examen"
  ]
  node [
    id 2059
    label "diagnosis"
  ]
  node [
    id 2060
    label "medycyna"
  ]
  node [
    id 2061
    label "anamneza"
  ]
  node [
    id 2062
    label "dr&#243;b"
  ]
  node [
    id 2063
    label "pomacanie"
  ]
  node [
    id 2064
    label "feel"
  ]
  node [
    id 2065
    label "palpation"
  ]
  node [
    id 2066
    label "namacanie"
  ]
  node [
    id 2067
    label "hodowanie"
  ]
  node [
    id 2068
    label "patrzenie"
  ]
  node [
    id 2069
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 2070
    label "doszukanie_si&#281;"
  ]
  node [
    id 2071
    label "dostrzeganie"
  ]
  node [
    id 2072
    label "poobserwowanie"
  ]
  node [
    id 2073
    label "observation"
  ]
  node [
    id 2074
    label "bocianie_gniazdo"
  ]
  node [
    id 2075
    label "pacjent"
  ]
  node [
    id 2076
    label "happening"
  ]
  node [
    id 2077
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 2078
    label "przyk&#322;ad"
  ]
  node [
    id 2079
    label "przeznaczenie"
  ]
  node [
    id 2080
    label "fakt"
  ]
  node [
    id 2081
    label "przedstawiciel"
  ]
  node [
    id 2082
    label "przebiec"
  ]
  node [
    id 2083
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 2084
    label "przebiegni&#281;cie"
  ]
  node [
    id 2085
    label "fabu&#322;a"
  ]
  node [
    id 2086
    label "rzuci&#263;"
  ]
  node [
    id 2087
    label "destiny"
  ]
  node [
    id 2088
    label "si&#322;a"
  ]
  node [
    id 2089
    label "przymus"
  ]
  node [
    id 2090
    label "przydzielenie"
  ]
  node [
    id 2091
    label "p&#243;j&#347;cie"
  ]
  node [
    id 2092
    label "oblat"
  ]
  node [
    id 2093
    label "obowi&#261;zek"
  ]
  node [
    id 2094
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 2095
    label "wybranie"
  ]
  node [
    id 2096
    label "ognisko"
  ]
  node [
    id 2097
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 2098
    label "powalenie"
  ]
  node [
    id 2099
    label "odezwanie_si&#281;"
  ]
  node [
    id 2100
    label "grupa_ryzyka"
  ]
  node [
    id 2101
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 2102
    label "nabawienie_si&#281;"
  ]
  node [
    id 2103
    label "inkubacja"
  ]
  node [
    id 2104
    label "kryzys"
  ]
  node [
    id 2105
    label "powali&#263;"
  ]
  node [
    id 2106
    label "remisja"
  ]
  node [
    id 2107
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 2108
    label "zajmowa&#263;"
  ]
  node [
    id 2109
    label "zaburzenie"
  ]
  node [
    id 2110
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 2111
    label "badanie_histopatologiczne"
  ]
  node [
    id 2112
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 2113
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 2114
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 2115
    label "odzywanie_si&#281;"
  ]
  node [
    id 2116
    label "diagnoza"
  ]
  node [
    id 2117
    label "atakowa&#263;"
  ]
  node [
    id 2118
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 2119
    label "nabawianie_si&#281;"
  ]
  node [
    id 2120
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 2121
    label "zajmowanie"
  ]
  node [
    id 2122
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 2123
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 2124
    label "klient"
  ]
  node [
    id 2125
    label "piel&#281;gniarz"
  ]
  node [
    id 2126
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 2127
    label "od&#322;&#261;czanie"
  ]
  node [
    id 2128
    label "od&#322;&#261;czenie"
  ]
  node [
    id 2129
    label "chory"
  ]
  node [
    id 2130
    label "szpitalnik"
  ]
  node [
    id 2131
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 2132
    label "dysponowanie"
  ]
  node [
    id 2133
    label "sterowanie"
  ]
  node [
    id 2134
    label "management"
  ]
  node [
    id 2135
    label "kierowanie"
  ]
  node [
    id 2136
    label "ukierunkowywanie"
  ]
  node [
    id 2137
    label "przywodzenie"
  ]
  node [
    id 2138
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 2139
    label "doprowadzanie"
  ]
  node [
    id 2140
    label "kre&#347;lenie"
  ]
  node [
    id 2141
    label "eksponowanie"
  ]
  node [
    id 2142
    label "linia_melodyczna"
  ]
  node [
    id 2143
    label "prowadzanie"
  ]
  node [
    id 2144
    label "wprowadzanie"
  ]
  node [
    id 2145
    label "doprowadzenie"
  ]
  node [
    id 2146
    label "poprowadzenie"
  ]
  node [
    id 2147
    label "kszta&#322;towanie"
  ]
  node [
    id 2148
    label "aim"
  ]
  node [
    id 2149
    label "zwracanie"
  ]
  node [
    id 2150
    label "przecinanie"
  ]
  node [
    id 2151
    label "ta&#324;czenie"
  ]
  node [
    id 2152
    label "przewy&#380;szanie"
  ]
  node [
    id 2153
    label "g&#243;rowanie"
  ]
  node [
    id 2154
    label "zaprowadzanie"
  ]
  node [
    id 2155
    label "dawanie"
  ]
  node [
    id 2156
    label "trzymanie"
  ]
  node [
    id 2157
    label "oprowadzanie"
  ]
  node [
    id 2158
    label "wprowadzenie"
  ]
  node [
    id 2159
    label "drive"
  ]
  node [
    id 2160
    label "oprowadzenie"
  ]
  node [
    id 2161
    label "przeci&#281;cie"
  ]
  node [
    id 2162
    label "przeci&#261;ganie"
  ]
  node [
    id 2163
    label "pozarz&#261;dzanie"
  ]
  node [
    id 2164
    label "granie"
  ]
  node [
    id 2165
    label "uniewa&#380;nianie"
  ]
  node [
    id 2166
    label "skre&#347;lanie"
  ]
  node [
    id 2167
    label "pokre&#347;lenie"
  ]
  node [
    id 2168
    label "anointing"
  ]
  node [
    id 2169
    label "usuwanie"
  ]
  node [
    id 2170
    label "sporz&#261;dzanie"
  ]
  node [
    id 2171
    label "rozdysponowywanie"
  ]
  node [
    id 2172
    label "namaszczenie_chorych"
  ]
  node [
    id 2173
    label "disposal"
  ]
  node [
    id 2174
    label "rozporz&#261;dzanie"
  ]
  node [
    id 2175
    label "przygotowywanie"
  ]
  node [
    id 2176
    label "radiation"
  ]
  node [
    id 2177
    label "podkre&#347;lanie"
  ]
  node [
    id 2178
    label "uwydatnianie_si&#281;"
  ]
  node [
    id 2179
    label "demonstrowanie"
  ]
  node [
    id 2180
    label "napromieniowywanie"
  ]
  node [
    id 2181
    label "orientation"
  ]
  node [
    id 2182
    label "oznaczanie"
  ]
  node [
    id 2183
    label "strike"
  ]
  node [
    id 2184
    label "dominance"
  ]
  node [
    id 2185
    label "lepszy"
  ]
  node [
    id 2186
    label "wygrywanie"
  ]
  node [
    id 2187
    label "control"
  ]
  node [
    id 2188
    label "obs&#322;ugiwanie"
  ]
  node [
    id 2189
    label "przesterowanie"
  ]
  node [
    id 2190
    label "steering"
  ]
  node [
    id 2191
    label "haftowanie"
  ]
  node [
    id 2192
    label "vomit"
  ]
  node [
    id 2193
    label "przeznaczanie"
  ]
  node [
    id 2194
    label "powracanie"
  ]
  node [
    id 2195
    label "ustawianie"
  ]
  node [
    id 2196
    label "wydalanie"
  ]
  node [
    id 2197
    label "supply"
  ]
  node [
    id 2198
    label "znajdowanie_si&#281;"
  ]
  node [
    id 2199
    label "spe&#322;nianie"
  ]
  node [
    id 2200
    label "provision"
  ]
  node [
    id 2201
    label "wzbudzanie"
  ]
  node [
    id 2202
    label "montowanie"
  ]
  node [
    id 2203
    label "cause"
  ]
  node [
    id 2204
    label "wyre&#380;yserowanie"
  ]
  node [
    id 2205
    label "re&#380;yserowanie"
  ]
  node [
    id 2206
    label "nakierowanie_si&#281;"
  ]
  node [
    id 2207
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 2208
    label "sterczenie"
  ]
  node [
    id 2209
    label "kierowa&#263;"
  ]
  node [
    id 2210
    label "g&#243;rowa&#263;"
  ]
  node [
    id 2211
    label "string"
  ]
  node [
    id 2212
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 2213
    label "ukierunkowywa&#263;"
  ]
  node [
    id 2214
    label "sterowa&#263;"
  ]
  node [
    id 2215
    label "kre&#347;li&#263;"
  ]
  node [
    id 2216
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 2217
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 2218
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 2219
    label "eksponowa&#263;"
  ]
  node [
    id 2220
    label "navigate"
  ]
  node [
    id 2221
    label "manipulate"
  ]
  node [
    id 2222
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 2223
    label "partner"
  ]
  node [
    id 2224
    label "artyku&#322;"
  ]
  node [
    id 2225
    label "akapit"
  ]
  node [
    id 2226
    label "p&#322;acenie"
  ]
  node [
    id 2227
    label "wyrzekanie_si&#281;"
  ]
  node [
    id 2228
    label "puszczanie_si&#281;"
  ]
  node [
    id 2229
    label "&#322;adowanie"
  ]
  node [
    id 2230
    label "zezwalanie"
  ]
  node [
    id 2231
    label "nalewanie"
  ]
  node [
    id 2232
    label "urz&#261;dzanie"
  ]
  node [
    id 2233
    label "wyst&#281;powanie"
  ]
  node [
    id 2234
    label "udost&#281;pnianie"
  ]
  node [
    id 2235
    label "giving"
  ]
  node [
    id 2236
    label "pra&#380;enie"
  ]
  node [
    id 2237
    label "emission"
  ]
  node [
    id 2238
    label "dostarczanie"
  ]
  node [
    id 2239
    label "r&#380;ni&#281;cie"
  ]
  node [
    id 2240
    label "obiecywanie"
  ]
  node [
    id 2241
    label "odst&#281;powanie"
  ]
  node [
    id 2242
    label "administration"
  ]
  node [
    id 2243
    label "wymienianie_si&#281;"
  ]
  node [
    id 2244
    label "bycie_w_posiadaniu"
  ]
  node [
    id 2245
    label "pr&#243;bowanie"
  ]
  node [
    id 2246
    label "instrumentalizacja"
  ]
  node [
    id 2247
    label "wykonywanie"
  ]
  node [
    id 2248
    label "uzewn&#281;trznianie_si&#281;"
  ]
  node [
    id 2249
    label "pasowanie"
  ]
  node [
    id 2250
    label "wybijanie"
  ]
  node [
    id 2251
    label "odegranie_si&#281;"
  ]
  node [
    id 2252
    label "instrument_muzyczny"
  ]
  node [
    id 2253
    label "dogrywanie"
  ]
  node [
    id 2254
    label "rozgrywanie"
  ]
  node [
    id 2255
    label "grywanie"
  ]
  node [
    id 2256
    label "przygrywanie"
  ]
  node [
    id 2257
    label "lewa"
  ]
  node [
    id 2258
    label "uderzenie"
  ]
  node [
    id 2259
    label "gra_w_karty"
  ]
  node [
    id 2260
    label "mienienie_si&#281;"
  ]
  node [
    id 2261
    label "prezentowanie"
  ]
  node [
    id 2262
    label "dogranie"
  ]
  node [
    id 2263
    label "wybicie"
  ]
  node [
    id 2264
    label "rozegranie_si&#281;"
  ]
  node [
    id 2265
    label "otwarcie"
  ]
  node [
    id 2266
    label "glitter"
  ]
  node [
    id 2267
    label "igranie"
  ]
  node [
    id 2268
    label "odgrywanie_si&#281;"
  ]
  node [
    id 2269
    label "pogranie"
  ]
  node [
    id 2270
    label "wyr&#243;wnywanie"
  ]
  node [
    id 2271
    label "szczekanie"
  ]
  node [
    id 2272
    label "brzmienie"
  ]
  node [
    id 2273
    label "wyr&#243;wnanie"
  ]
  node [
    id 2274
    label "nagranie_si&#281;"
  ]
  node [
    id 2275
    label "migotanie"
  ]
  node [
    id 2276
    label "&#347;ciganie"
  ]
  node [
    id 2277
    label "nakre&#347;lenie"
  ]
  node [
    id 2278
    label "guidance"
  ]
  node [
    id 2279
    label "proverb"
  ]
  node [
    id 2280
    label "wytyczenie"
  ]
  node [
    id 2281
    label "gibanie"
  ]
  node [
    id 2282
    label "przeta&#324;czenie"
  ]
  node [
    id 2283
    label "nata&#324;czenie_si&#281;"
  ]
  node [
    id 2284
    label "rozta&#324;czenie_si&#281;"
  ]
  node [
    id 2285
    label "pota&#324;czenie"
  ]
  node [
    id 2286
    label "poruszanie_si&#281;"
  ]
  node [
    id 2287
    label "chwianie_si&#281;"
  ]
  node [
    id 2288
    label "dancing"
  ]
  node [
    id 2289
    label "spe&#322;nienie"
  ]
  node [
    id 2290
    label "wzbudzenie"
  ]
  node [
    id 2291
    label "pos&#322;anie"
  ]
  node [
    id 2292
    label "znalezienie_si&#281;"
  ]
  node [
    id 2293
    label "introduction"
  ]
  node [
    id 2294
    label "sp&#281;dzenie"
  ]
  node [
    id 2295
    label "zainstalowanie"
  ]
  node [
    id 2296
    label "rynek"
  ]
  node [
    id 2297
    label "nuklearyzacja"
  ]
  node [
    id 2298
    label "deduction"
  ]
  node [
    id 2299
    label "entrance"
  ]
  node [
    id 2300
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 2301
    label "wst&#281;p"
  ]
  node [
    id 2302
    label "wej&#347;cie"
  ]
  node [
    id 2303
    label "umieszczenie"
  ]
  node [
    id 2304
    label "wpisanie"
  ]
  node [
    id 2305
    label "podstawy"
  ]
  node [
    id 2306
    label "evocation"
  ]
  node [
    id 2307
    label "zacz&#281;cie"
  ]
  node [
    id 2308
    label "przewietrzenie"
  ]
  node [
    id 2309
    label "umieszczanie"
  ]
  node [
    id 2310
    label "w&#322;&#261;czanie"
  ]
  node [
    id 2311
    label "initiation"
  ]
  node [
    id 2312
    label "zak&#322;&#243;canie"
  ]
  node [
    id 2313
    label "zaczynanie"
  ]
  node [
    id 2314
    label "trigger"
  ]
  node [
    id 2315
    label "wpisywanie"
  ]
  node [
    id 2316
    label "mental_hospital"
  ]
  node [
    id 2317
    label "wchodzenie"
  ]
  node [
    id 2318
    label "retraction"
  ]
  node [
    id 2319
    label "przewietrzanie"
  ]
  node [
    id 2320
    label "pokazanie"
  ]
  node [
    id 2321
    label "przypominanie"
  ]
  node [
    id 2322
    label "kojarzenie_si&#281;"
  ]
  node [
    id 2323
    label "sk&#322;anianie"
  ]
  node [
    id 2324
    label "adduction"
  ]
  node [
    id 2325
    label "pull"
  ]
  node [
    id 2326
    label "j&#261;kanie"
  ]
  node [
    id 2327
    label "przetykanie"
  ]
  node [
    id 2328
    label "zaci&#261;ganie"
  ]
  node [
    id 2329
    label "przymocowywanie"
  ]
  node [
    id 2330
    label "przemieszczanie"
  ]
  node [
    id 2331
    label "przed&#322;u&#380;anie"
  ]
  node [
    id 2332
    label "rozci&#261;ganie"
  ]
  node [
    id 2333
    label "wymawianie"
  ]
  node [
    id 2334
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 2335
    label "time"
  ]
  node [
    id 2336
    label "dzielenie"
  ]
  node [
    id 2337
    label "&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 2338
    label "kaleczenie"
  ]
  node [
    id 2339
    label "film_editing"
  ]
  node [
    id 2340
    label "intersection"
  ]
  node [
    id 2341
    label "poprzecinanie"
  ]
  node [
    id 2342
    label "przerwanie"
  ]
  node [
    id 2343
    label "carving"
  ]
  node [
    id 2344
    label "cut"
  ]
  node [
    id 2345
    label "zranienie"
  ]
  node [
    id 2346
    label "snub"
  ]
  node [
    id 2347
    label "podzielenie"
  ]
  node [
    id 2348
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 2349
    label "wypuszczenie"
  ]
  node [
    id 2350
    label "niesienie"
  ]
  node [
    id 2351
    label "potrzymanie"
  ]
  node [
    id 2352
    label "utrzymywanie_si&#281;"
  ]
  node [
    id 2353
    label "sprawowanie"
  ]
  node [
    id 2354
    label "wypuszczanie"
  ]
  node [
    id 2355
    label "poise"
  ]
  node [
    id 2356
    label "retention"
  ]
  node [
    id 2357
    label "dzier&#380;enie"
  ]
  node [
    id 2358
    label "noszenie"
  ]
  node [
    id 2359
    label "uniemo&#380;liwianie"
  ]
  node [
    id 2360
    label "podtrzymywanie"
  ]
  node [
    id 2361
    label "zachowywanie"
  ]
  node [
    id 2362
    label "clasp"
  ]
  node [
    id 2363
    label "przetrzymywanie"
  ]
  node [
    id 2364
    label "detention"
  ]
  node [
    id 2365
    label "przetrzymanie"
  ]
  node [
    id 2366
    label "zmuszanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 254
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 262
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 264
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 798
  ]
  edge [
    source 14
    target 44
  ]
  edge [
    source 14
    target 46
  ]
  edge [
    source 14
    target 799
  ]
  edge [
    source 14
    target 800
  ]
  edge [
    source 14
    target 801
  ]
  edge [
    source 14
    target 802
  ]
  edge [
    source 14
    target 803
  ]
  edge [
    source 14
    target 804
  ]
  edge [
    source 14
    target 805
  ]
  edge [
    source 14
    target 806
  ]
  edge [
    source 14
    target 807
  ]
  edge [
    source 14
    target 808
  ]
  edge [
    source 14
    target 809
  ]
  edge [
    source 14
    target 810
  ]
  edge [
    source 14
    target 811
  ]
  edge [
    source 14
    target 77
  ]
  edge [
    source 14
    target 76
  ]
  edge [
    source 14
    target 54
  ]
  edge [
    source 14
    target 43
  ]
  edge [
    source 14
    target 812
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 14
    target 813
  ]
  edge [
    source 14
    target 814
  ]
  edge [
    source 14
    target 815
  ]
  edge [
    source 14
    target 816
  ]
  edge [
    source 14
    target 817
  ]
  edge [
    source 14
    target 818
  ]
  edge [
    source 14
    target 819
  ]
  edge [
    source 14
    target 820
  ]
  edge [
    source 14
    target 821
  ]
  edge [
    source 15
    target 822
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 823
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 824
  ]
  edge [
    source 15
    target 825
  ]
  edge [
    source 15
    target 826
  ]
  edge [
    source 15
    target 827
  ]
  edge [
    source 15
    target 828
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 15
    target 829
  ]
  edge [
    source 15
    target 830
  ]
  edge [
    source 15
    target 831
  ]
  edge [
    source 15
    target 832
  ]
  edge [
    source 15
    target 833
  ]
  edge [
    source 15
    target 834
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 836
  ]
  edge [
    source 15
    target 551
  ]
  edge [
    source 15
    target 837
  ]
  edge [
    source 15
    target 838
  ]
  edge [
    source 15
    target 839
  ]
  edge [
    source 15
    target 840
  ]
  edge [
    source 15
    target 841
  ]
  edge [
    source 15
    target 842
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 189
  ]
  edge [
    source 15
    target 190
  ]
  edge [
    source 15
    target 191
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 202
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 843
  ]
  edge [
    source 15
    target 844
  ]
  edge [
    source 15
    target 845
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 846
  ]
  edge [
    source 15
    target 847
  ]
  edge [
    source 15
    target 848
  ]
  edge [
    source 15
    target 849
  ]
  edge [
    source 15
    target 850
  ]
  edge [
    source 15
    target 851
  ]
  edge [
    source 15
    target 852
  ]
  edge [
    source 15
    target 853
  ]
  edge [
    source 15
    target 854
  ]
  edge [
    source 15
    target 855
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 856
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 857
  ]
  edge [
    source 15
    target 858
  ]
  edge [
    source 15
    target 859
  ]
  edge [
    source 15
    target 860
  ]
  edge [
    source 15
    target 861
  ]
  edge [
    source 15
    target 862
  ]
  edge [
    source 15
    target 863
  ]
  edge [
    source 15
    target 864
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 865
  ]
  edge [
    source 15
    target 866
  ]
  edge [
    source 15
    target 867
  ]
  edge [
    source 15
    target 868
  ]
  edge [
    source 15
    target 869
  ]
  edge [
    source 15
    target 870
  ]
  edge [
    source 15
    target 871
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 872
  ]
  edge [
    source 15
    target 873
  ]
  edge [
    source 15
    target 874
  ]
  edge [
    source 15
    target 875
  ]
  edge [
    source 15
    target 876
  ]
  edge [
    source 15
    target 254
  ]
  edge [
    source 15
    target 877
  ]
  edge [
    source 15
    target 245
  ]
  edge [
    source 15
    target 878
  ]
  edge [
    source 15
    target 879
  ]
  edge [
    source 15
    target 52
  ]
  edge [
    source 15
    target 43
  ]
  edge [
    source 15
    target 880
  ]
  edge [
    source 15
    target 881
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 882
  ]
  edge [
    source 15
    target 883
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 887
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 890
  ]
  edge [
    source 15
    target 891
  ]
  edge [
    source 15
    target 502
  ]
  edge [
    source 15
    target 892
  ]
  edge [
    source 15
    target 893
  ]
  edge [
    source 15
    target 894
  ]
  edge [
    source 15
    target 586
  ]
  edge [
    source 15
    target 895
  ]
  edge [
    source 15
    target 896
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 897
  ]
  edge [
    source 15
    target 381
  ]
  edge [
    source 15
    target 898
  ]
  edge [
    source 15
    target 899
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 15
    target 901
  ]
  edge [
    source 15
    target 902
  ]
  edge [
    source 15
    target 903
  ]
  edge [
    source 15
    target 904
  ]
  edge [
    source 15
    target 905
  ]
  edge [
    source 15
    target 906
  ]
  edge [
    source 15
    target 907
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 38
  ]
  edge [
    source 17
    target 39
  ]
  edge [
    source 17
    target 908
  ]
  edge [
    source 17
    target 909
  ]
  edge [
    source 17
    target 458
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 452
  ]
  edge [
    source 17
    target 911
  ]
  edge [
    source 17
    target 912
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 914
  ]
  edge [
    source 17
    target 915
  ]
  edge [
    source 17
    target 916
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 29
  ]
  edge [
    source 17
    target 918
  ]
  edge [
    source 17
    target 919
  ]
  edge [
    source 17
    target 920
  ]
  edge [
    source 17
    target 921
  ]
  edge [
    source 17
    target 922
  ]
  edge [
    source 17
    target 923
  ]
  edge [
    source 17
    target 924
  ]
  edge [
    source 17
    target 925
  ]
  edge [
    source 17
    target 926
  ]
  edge [
    source 17
    target 927
  ]
  edge [
    source 17
    target 928
  ]
  edge [
    source 17
    target 454
  ]
  edge [
    source 17
    target 929
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 234
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 455
  ]
  edge [
    source 17
    target 456
  ]
  edge [
    source 17
    target 457
  ]
  edge [
    source 17
    target 459
  ]
  edge [
    source 17
    target 460
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 550
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 968
  ]
  edge [
    source 17
    target 969
  ]
  edge [
    source 17
    target 970
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 318
  ]
  edge [
    source 17
    target 314
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 37
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 610
  ]
  edge [
    source 18
    target 382
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 645
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 381
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 380
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 264
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 433
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 39
  ]
  edge [
    source 18
    target 41
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 987
  ]
  edge [
    source 19
    target 988
  ]
  edge [
    source 19
    target 989
  ]
  edge [
    source 19
    target 990
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 85
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 813
  ]
  edge [
    source 19
    target 803
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 514
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 101
  ]
  edge [
    source 19
    target 414
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 887
  ]
  edge [
    source 19
    target 264
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 385
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 586
  ]
  edge [
    source 19
    target 872
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 19
    target 1026
  ]
  edge [
    source 19
    target 1027
  ]
  edge [
    source 19
    target 1028
  ]
  edge [
    source 19
    target 1029
  ]
  edge [
    source 19
    target 1030
  ]
  edge [
    source 19
    target 1031
  ]
  edge [
    source 19
    target 1032
  ]
  edge [
    source 19
    target 1033
  ]
  edge [
    source 19
    target 1034
  ]
  edge [
    source 19
    target 1035
  ]
  edge [
    source 19
    target 262
  ]
  edge [
    source 19
    target 1036
  ]
  edge [
    source 19
    target 1037
  ]
  edge [
    source 19
    target 1038
  ]
  edge [
    source 19
    target 1039
  ]
  edge [
    source 19
    target 1040
  ]
  edge [
    source 19
    target 1041
  ]
  edge [
    source 19
    target 1042
  ]
  edge [
    source 19
    target 1043
  ]
  edge [
    source 19
    target 1044
  ]
  edge [
    source 19
    target 1045
  ]
  edge [
    source 19
    target 1046
  ]
  edge [
    source 19
    target 1047
  ]
  edge [
    source 19
    target 1048
  ]
  edge [
    source 19
    target 1049
  ]
  edge [
    source 19
    target 1050
  ]
  edge [
    source 19
    target 1051
  ]
  edge [
    source 19
    target 1052
  ]
  edge [
    source 19
    target 1053
  ]
  edge [
    source 19
    target 1054
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 1055
  ]
  edge [
    source 19
    target 1056
  ]
  edge [
    source 19
    target 1057
  ]
  edge [
    source 19
    target 1058
  ]
  edge [
    source 19
    target 1059
  ]
  edge [
    source 19
    target 1060
  ]
  edge [
    source 19
    target 277
  ]
  edge [
    source 19
    target 1061
  ]
  edge [
    source 19
    target 1062
  ]
  edge [
    source 19
    target 1063
  ]
  edge [
    source 19
    target 1064
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 37
  ]
  edge [
    source 20
    target 39
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 21
    target 1065
  ]
  edge [
    source 21
    target 1066
  ]
  edge [
    source 21
    target 1067
  ]
  edge [
    source 21
    target 1068
  ]
  edge [
    source 21
    target 101
  ]
  edge [
    source 21
    target 1069
  ]
  edge [
    source 21
    target 1070
  ]
  edge [
    source 21
    target 1071
  ]
  edge [
    source 21
    target 1072
  ]
  edge [
    source 21
    target 1073
  ]
  edge [
    source 21
    target 1074
  ]
  edge [
    source 21
    target 1075
  ]
  edge [
    source 21
    target 1076
  ]
  edge [
    source 21
    target 1077
  ]
  edge [
    source 21
    target 1078
  ]
  edge [
    source 21
    target 1079
  ]
  edge [
    source 21
    target 1080
  ]
  edge [
    source 21
    target 1081
  ]
  edge [
    source 21
    target 1082
  ]
  edge [
    source 21
    target 1083
  ]
  edge [
    source 21
    target 1084
  ]
  edge [
    source 21
    target 1085
  ]
  edge [
    source 21
    target 1086
  ]
  edge [
    source 21
    target 1087
  ]
  edge [
    source 21
    target 505
  ]
  edge [
    source 21
    target 1088
  ]
  edge [
    source 21
    target 1089
  ]
  edge [
    source 21
    target 1090
  ]
  edge [
    source 21
    target 1091
  ]
  edge [
    source 21
    target 1092
  ]
  edge [
    source 21
    target 1093
  ]
  edge [
    source 21
    target 1094
  ]
  edge [
    source 21
    target 1095
  ]
  edge [
    source 21
    target 1096
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 822
  ]
  edge [
    source 21
    target 1097
  ]
  edge [
    source 21
    target 1098
  ]
  edge [
    source 21
    target 1099
  ]
  edge [
    source 21
    target 1100
  ]
  edge [
    source 21
    target 1101
  ]
  edge [
    source 21
    target 1102
  ]
  edge [
    source 21
    target 1103
  ]
  edge [
    source 21
    target 1104
  ]
  edge [
    source 21
    target 1105
  ]
  edge [
    source 21
    target 1106
  ]
  edge [
    source 21
    target 1107
  ]
  edge [
    source 21
    target 1108
  ]
  edge [
    source 21
    target 844
  ]
  edge [
    source 21
    target 1109
  ]
  edge [
    source 21
    target 1110
  ]
  edge [
    source 21
    target 220
  ]
  edge [
    source 21
    target 1111
  ]
  edge [
    source 21
    target 1112
  ]
  edge [
    source 21
    target 551
  ]
  edge [
    source 21
    target 115
  ]
  edge [
    source 21
    target 857
  ]
  edge [
    source 21
    target 858
  ]
  edge [
    source 21
    target 1113
  ]
  edge [
    source 21
    target 1114
  ]
  edge [
    source 21
    target 1115
  ]
  edge [
    source 21
    target 1116
  ]
  edge [
    source 21
    target 1117
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 1118
  ]
  edge [
    source 21
    target 1119
  ]
  edge [
    source 21
    target 1120
  ]
  edge [
    source 21
    target 1121
  ]
  edge [
    source 21
    target 1122
  ]
  edge [
    source 21
    target 1123
  ]
  edge [
    source 21
    target 1124
  ]
  edge [
    source 21
    target 1125
  ]
  edge [
    source 21
    target 1126
  ]
  edge [
    source 21
    target 1127
  ]
  edge [
    source 21
    target 1128
  ]
  edge [
    source 21
    target 1129
  ]
  edge [
    source 21
    target 1130
  ]
  edge [
    source 21
    target 1131
  ]
  edge [
    source 21
    target 1132
  ]
  edge [
    source 21
    target 1133
  ]
  edge [
    source 21
    target 277
  ]
  edge [
    source 21
    target 1134
  ]
  edge [
    source 21
    target 1135
  ]
  edge [
    source 21
    target 1136
  ]
  edge [
    source 21
    target 1137
  ]
  edge [
    source 21
    target 1138
  ]
  edge [
    source 21
    target 1139
  ]
  edge [
    source 21
    target 1140
  ]
  edge [
    source 21
    target 1141
  ]
  edge [
    source 21
    target 1142
  ]
  edge [
    source 21
    target 1143
  ]
  edge [
    source 21
    target 1144
  ]
  edge [
    source 21
    target 1145
  ]
  edge [
    source 21
    target 385
  ]
  edge [
    source 21
    target 1146
  ]
  edge [
    source 21
    target 1147
  ]
  edge [
    source 21
    target 1148
  ]
  edge [
    source 21
    target 254
  ]
  edge [
    source 21
    target 1149
  ]
  edge [
    source 21
    target 977
  ]
  edge [
    source 21
    target 593
  ]
  edge [
    source 21
    target 1150
  ]
  edge [
    source 21
    target 1151
  ]
  edge [
    source 21
    target 1152
  ]
  edge [
    source 21
    target 1153
  ]
  edge [
    source 21
    target 1154
  ]
  edge [
    source 21
    target 1155
  ]
  edge [
    source 21
    target 1156
  ]
  edge [
    source 21
    target 1157
  ]
  edge [
    source 21
    target 1158
  ]
  edge [
    source 21
    target 992
  ]
  edge [
    source 21
    target 1159
  ]
  edge [
    source 21
    target 1160
  ]
  edge [
    source 21
    target 1161
  ]
  edge [
    source 21
    target 1162
  ]
  edge [
    source 21
    target 1163
  ]
  edge [
    source 21
    target 1164
  ]
  edge [
    source 21
    target 1165
  ]
  edge [
    source 21
    target 1166
  ]
  edge [
    source 21
    target 1167
  ]
  edge [
    source 21
    target 1168
  ]
  edge [
    source 21
    target 1169
  ]
  edge [
    source 21
    target 1170
  ]
  edge [
    source 21
    target 1171
  ]
  edge [
    source 21
    target 1172
  ]
  edge [
    source 21
    target 1173
  ]
  edge [
    source 21
    target 1174
  ]
  edge [
    source 21
    target 1175
  ]
  edge [
    source 21
    target 1176
  ]
  edge [
    source 21
    target 1177
  ]
  edge [
    source 21
    target 1178
  ]
  edge [
    source 21
    target 1179
  ]
  edge [
    source 21
    target 1180
  ]
  edge [
    source 21
    target 236
  ]
  edge [
    source 21
    target 1181
  ]
  edge [
    source 21
    target 1182
  ]
  edge [
    source 21
    target 1183
  ]
  edge [
    source 21
    target 1184
  ]
  edge [
    source 21
    target 1185
  ]
  edge [
    source 21
    target 1186
  ]
  edge [
    source 21
    target 1187
  ]
  edge [
    source 21
    target 1188
  ]
  edge [
    source 21
    target 1189
  ]
  edge [
    source 21
    target 1190
  ]
  edge [
    source 21
    target 245
  ]
  edge [
    source 21
    target 1191
  ]
  edge [
    source 21
    target 1192
  ]
  edge [
    source 21
    target 1193
  ]
  edge [
    source 21
    target 1194
  ]
  edge [
    source 21
    target 1195
  ]
  edge [
    source 21
    target 1196
  ]
  edge [
    source 21
    target 1197
  ]
  edge [
    source 21
    target 1198
  ]
  edge [
    source 21
    target 1199
  ]
  edge [
    source 21
    target 1200
  ]
  edge [
    source 21
    target 1201
  ]
  edge [
    source 21
    target 1202
  ]
  edge [
    source 21
    target 1203
  ]
  edge [
    source 21
    target 228
  ]
  edge [
    source 21
    target 1204
  ]
  edge [
    source 21
    target 1205
  ]
  edge [
    source 21
    target 1206
  ]
  edge [
    source 21
    target 1207
  ]
  edge [
    source 21
    target 1208
  ]
  edge [
    source 21
    target 1209
  ]
  edge [
    source 21
    target 526
  ]
  edge [
    source 21
    target 1210
  ]
  edge [
    source 21
    target 519
  ]
  edge [
    source 21
    target 1211
  ]
  edge [
    source 21
    target 1212
  ]
  edge [
    source 21
    target 1213
  ]
  edge [
    source 21
    target 1214
  ]
  edge [
    source 21
    target 1215
  ]
  edge [
    source 21
    target 1216
  ]
  edge [
    source 21
    target 1217
  ]
  edge [
    source 21
    target 1218
  ]
  edge [
    source 21
    target 1219
  ]
  edge [
    source 21
    target 1220
  ]
  edge [
    source 21
    target 1221
  ]
  edge [
    source 21
    target 1222
  ]
  edge [
    source 21
    target 532
  ]
  edge [
    source 21
    target 1223
  ]
  edge [
    source 21
    target 1224
  ]
  edge [
    source 21
    target 1225
  ]
  edge [
    source 21
    target 1226
  ]
  edge [
    source 21
    target 1227
  ]
  edge [
    source 21
    target 1228
  ]
  edge [
    source 21
    target 1229
  ]
  edge [
    source 21
    target 1230
  ]
  edge [
    source 21
    target 1231
  ]
  edge [
    source 21
    target 264
  ]
  edge [
    source 21
    target 1232
  ]
  edge [
    source 21
    target 1233
  ]
  edge [
    source 21
    target 410
  ]
  edge [
    source 21
    target 1234
  ]
  edge [
    source 21
    target 1235
  ]
  edge [
    source 21
    target 1236
  ]
  edge [
    source 21
    target 1237
  ]
  edge [
    source 21
    target 1238
  ]
  edge [
    source 21
    target 1239
  ]
  edge [
    source 21
    target 1240
  ]
  edge [
    source 21
    target 1241
  ]
  edge [
    source 21
    target 1242
  ]
  edge [
    source 21
    target 1243
  ]
  edge [
    source 21
    target 904
  ]
  edge [
    source 21
    target 1244
  ]
  edge [
    source 21
    target 309
  ]
  edge [
    source 21
    target 1245
  ]
  edge [
    source 21
    target 1246
  ]
  edge [
    source 21
    target 1247
  ]
  edge [
    source 21
    target 1248
  ]
  edge [
    source 21
    target 1249
  ]
  edge [
    source 21
    target 1250
  ]
  edge [
    source 21
    target 660
  ]
  edge [
    source 21
    target 1251
  ]
  edge [
    source 21
    target 171
  ]
  edge [
    source 21
    target 1252
  ]
  edge [
    source 21
    target 1253
  ]
  edge [
    source 21
    target 1254
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 1255
  ]
  edge [
    source 21
    target 1256
  ]
  edge [
    source 21
    target 1257
  ]
  edge [
    source 21
    target 1258
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 1259
  ]
  edge [
    source 21
    target 479
  ]
  edge [
    source 21
    target 1260
  ]
  edge [
    source 21
    target 1261
  ]
  edge [
    source 21
    target 1262
  ]
  edge [
    source 21
    target 1263
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 21
    target 1264
  ]
  edge [
    source 21
    target 1265
  ]
  edge [
    source 21
    target 801
  ]
  edge [
    source 21
    target 1266
  ]
  edge [
    source 21
    target 481
  ]
  edge [
    source 21
    target 1267
  ]
  edge [
    source 21
    target 1268
  ]
  edge [
    source 21
    target 1269
  ]
  edge [
    source 21
    target 1270
  ]
  edge [
    source 21
    target 1271
  ]
  edge [
    source 21
    target 1272
  ]
  edge [
    source 21
    target 957
  ]
  edge [
    source 21
    target 1273
  ]
  edge [
    source 21
    target 1274
  ]
  edge [
    source 21
    target 1275
  ]
  edge [
    source 21
    target 1276
  ]
  edge [
    source 21
    target 1277
  ]
  edge [
    source 21
    target 1278
  ]
  edge [
    source 21
    target 1279
  ]
  edge [
    source 21
    target 1280
  ]
  edge [
    source 21
    target 1281
  ]
  edge [
    source 21
    target 1282
  ]
  edge [
    source 21
    target 1283
  ]
  edge [
    source 21
    target 1284
  ]
  edge [
    source 21
    target 1285
  ]
  edge [
    source 21
    target 1286
  ]
  edge [
    source 21
    target 1287
  ]
  edge [
    source 21
    target 43
  ]
  edge [
    source 21
    target 37
  ]
  edge [
    source 21
    target 1288
  ]
  edge [
    source 21
    target 54
  ]
  edge [
    source 21
    target 1289
  ]
  edge [
    source 21
    target 1290
  ]
  edge [
    source 21
    target 1291
  ]
  edge [
    source 21
    target 1292
  ]
  edge [
    source 21
    target 1293
  ]
  edge [
    source 21
    target 1294
  ]
  edge [
    source 21
    target 1295
  ]
  edge [
    source 21
    target 1296
  ]
  edge [
    source 21
    target 1297
  ]
  edge [
    source 21
    target 1298
  ]
  edge [
    source 21
    target 1299
  ]
  edge [
    source 21
    target 1300
  ]
  edge [
    source 21
    target 1301
  ]
  edge [
    source 21
    target 1302
  ]
  edge [
    source 21
    target 1303
  ]
  edge [
    source 21
    target 1304
  ]
  edge [
    source 21
    target 1305
  ]
  edge [
    source 21
    target 1306
  ]
  edge [
    source 21
    target 1307
  ]
  edge [
    source 21
    target 1308
  ]
  edge [
    source 21
    target 1309
  ]
  edge [
    source 21
    target 1310
  ]
  edge [
    source 21
    target 1311
  ]
  edge [
    source 21
    target 77
  ]
  edge [
    source 21
    target 1312
  ]
  edge [
    source 21
    target 1313
  ]
  edge [
    source 21
    target 1314
  ]
  edge [
    source 21
    target 1315
  ]
  edge [
    source 21
    target 1316
  ]
  edge [
    source 21
    target 347
  ]
  edge [
    source 21
    target 1317
  ]
  edge [
    source 21
    target 1318
  ]
  edge [
    source 21
    target 1319
  ]
  edge [
    source 21
    target 606
  ]
  edge [
    source 21
    target 986
  ]
  edge [
    source 21
    target 1320
  ]
  edge [
    source 21
    target 1321
  ]
  edge [
    source 21
    target 1322
  ]
  edge [
    source 21
    target 1323
  ]
  edge [
    source 21
    target 1324
  ]
  edge [
    source 21
    target 1325
  ]
  edge [
    source 21
    target 1326
  ]
  edge [
    source 21
    target 1327
  ]
  edge [
    source 21
    target 1328
  ]
  edge [
    source 21
    target 1329
  ]
  edge [
    source 21
    target 1330
  ]
  edge [
    source 21
    target 1331
  ]
  edge [
    source 21
    target 1332
  ]
  edge [
    source 21
    target 1333
  ]
  edge [
    source 21
    target 1334
  ]
  edge [
    source 21
    target 1335
  ]
  edge [
    source 21
    target 1336
  ]
  edge [
    source 21
    target 1337
  ]
  edge [
    source 21
    target 1338
  ]
  edge [
    source 21
    target 1339
  ]
  edge [
    source 21
    target 317
  ]
  edge [
    source 21
    target 1340
  ]
  edge [
    source 21
    target 1341
  ]
  edge [
    source 21
    target 1342
  ]
  edge [
    source 21
    target 1343
  ]
  edge [
    source 21
    target 1344
  ]
  edge [
    source 21
    target 1345
  ]
  edge [
    source 21
    target 1346
  ]
  edge [
    source 21
    target 1347
  ]
  edge [
    source 21
    target 1348
  ]
  edge [
    source 21
    target 1349
  ]
  edge [
    source 21
    target 1350
  ]
  edge [
    source 21
    target 1351
  ]
  edge [
    source 21
    target 1352
  ]
  edge [
    source 21
    target 1353
  ]
  edge [
    source 21
    target 1354
  ]
  edge [
    source 21
    target 1355
  ]
  edge [
    source 21
    target 1356
  ]
  edge [
    source 21
    target 1357
  ]
  edge [
    source 21
    target 1358
  ]
  edge [
    source 21
    target 1359
  ]
  edge [
    source 21
    target 1360
  ]
  edge [
    source 21
    target 1361
  ]
  edge [
    source 21
    target 1362
  ]
  edge [
    source 21
    target 1363
  ]
  edge [
    source 21
    target 1364
  ]
  edge [
    source 21
    target 1365
  ]
  edge [
    source 21
    target 1366
  ]
  edge [
    source 21
    target 1367
  ]
  edge [
    source 21
    target 1368
  ]
  edge [
    source 21
    target 1369
  ]
  edge [
    source 21
    target 1370
  ]
  edge [
    source 21
    target 1371
  ]
  edge [
    source 21
    target 1372
  ]
  edge [
    source 21
    target 1373
  ]
  edge [
    source 21
    target 1374
  ]
  edge [
    source 21
    target 1375
  ]
  edge [
    source 21
    target 1376
  ]
  edge [
    source 21
    target 1377
  ]
  edge [
    source 21
    target 1378
  ]
  edge [
    source 21
    target 1379
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 31
  ]
  edge [
    source 22
    target 36
  ]
  edge [
    source 22
    target 1380
  ]
  edge [
    source 22
    target 101
  ]
  edge [
    source 22
    target 1381
  ]
  edge [
    source 22
    target 1382
  ]
  edge [
    source 22
    target 1383
  ]
  edge [
    source 22
    target 1384
  ]
  edge [
    source 22
    target 1385
  ]
  edge [
    source 22
    target 1386
  ]
  edge [
    source 22
    target 467
  ]
  edge [
    source 22
    target 1387
  ]
  edge [
    source 22
    target 1388
  ]
  edge [
    source 22
    target 1389
  ]
  edge [
    source 22
    target 469
  ]
  edge [
    source 22
    target 1390
  ]
  edge [
    source 22
    target 470
  ]
  edge [
    source 22
    target 1391
  ]
  edge [
    source 22
    target 1392
  ]
  edge [
    source 22
    target 474
  ]
  edge [
    source 22
    target 1393
  ]
  edge [
    source 22
    target 1394
  ]
  edge [
    source 22
    target 1395
  ]
  edge [
    source 22
    target 434
  ]
  edge [
    source 22
    target 483
  ]
  edge [
    source 22
    target 1142
  ]
  edge [
    source 22
    target 1143
  ]
  edge [
    source 22
    target 1144
  ]
  edge [
    source 22
    target 1145
  ]
  edge [
    source 22
    target 385
  ]
  edge [
    source 22
    target 1146
  ]
  edge [
    source 22
    target 1147
  ]
  edge [
    source 22
    target 1148
  ]
  edge [
    source 22
    target 254
  ]
  edge [
    source 22
    target 1149
  ]
  edge [
    source 22
    target 977
  ]
  edge [
    source 22
    target 593
  ]
  edge [
    source 22
    target 1150
  ]
  edge [
    source 22
    target 1151
  ]
  edge [
    source 22
    target 1152
  ]
  edge [
    source 22
    target 1153
  ]
  edge [
    source 22
    target 1154
  ]
  edge [
    source 22
    target 1155
  ]
  edge [
    source 22
    target 1156
  ]
  edge [
    source 22
    target 1157
  ]
  edge [
    source 22
    target 1158
  ]
  edge [
    source 22
    target 992
  ]
  edge [
    source 22
    target 1159
  ]
  edge [
    source 22
    target 1160
  ]
  edge [
    source 22
    target 1161
  ]
  edge [
    source 22
    target 1396
  ]
  edge [
    source 22
    target 1397
  ]
  edge [
    source 22
    target 1398
  ]
  edge [
    source 22
    target 1399
  ]
  edge [
    source 22
    target 143
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 1400
  ]
  edge [
    source 22
    target 1401
  ]
  edge [
    source 22
    target 1402
  ]
  edge [
    source 22
    target 1403
  ]
  edge [
    source 22
    target 1404
  ]
  edge [
    source 22
    target 1405
  ]
  edge [
    source 22
    target 1406
  ]
  edge [
    source 22
    target 1407
  ]
  edge [
    source 22
    target 1408
  ]
  edge [
    source 22
    target 1409
  ]
  edge [
    source 22
    target 1410
  ]
  edge [
    source 22
    target 1411
  ]
  edge [
    source 22
    target 1412
  ]
  edge [
    source 22
    target 154
  ]
  edge [
    source 22
    target 1054
  ]
  edge [
    source 22
    target 1413
  ]
  edge [
    source 22
    target 1414
  ]
  edge [
    source 22
    target 1415
  ]
  edge [
    source 22
    target 822
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 1416
  ]
  edge [
    source 22
    target 1060
  ]
  edge [
    source 22
    target 1417
  ]
  edge [
    source 22
    target 1418
  ]
  edge [
    source 22
    target 1419
  ]
  edge [
    source 22
    target 466
  ]
  edge [
    source 22
    target 468
  ]
  edge [
    source 22
    target 471
  ]
  edge [
    source 22
    target 472
  ]
  edge [
    source 22
    target 473
  ]
  edge [
    source 22
    target 60
  ]
  edge [
    source 22
    target 475
  ]
  edge [
    source 22
    target 476
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 477
  ]
  edge [
    source 22
    target 478
  ]
  edge [
    source 22
    target 479
  ]
  edge [
    source 22
    target 480
  ]
  edge [
    source 22
    target 403
  ]
  edge [
    source 22
    target 481
  ]
  edge [
    source 22
    target 482
  ]
  edge [
    source 22
    target 1420
  ]
  edge [
    source 22
    target 1029
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 1421
  ]
  edge [
    source 22
    target 406
  ]
  edge [
    source 22
    target 1032
  ]
  edge [
    source 22
    target 1422
  ]
  edge [
    source 22
    target 262
  ]
  edge [
    source 22
    target 1038
  ]
  edge [
    source 22
    target 1037
  ]
  edge [
    source 22
    target 1036
  ]
  edge [
    source 22
    target 1423
  ]
  edge [
    source 22
    target 1040
  ]
  edge [
    source 22
    target 1043
  ]
  edge [
    source 22
    target 1044
  ]
  edge [
    source 22
    target 1424
  ]
  edge [
    source 22
    target 1048
  ]
  edge [
    source 22
    target 177
  ]
  edge [
    source 22
    target 178
  ]
  edge [
    source 22
    target 1052
  ]
  edge [
    source 22
    target 179
  ]
  edge [
    source 22
    target 130
  ]
  edge [
    source 22
    target 1055
  ]
  edge [
    source 22
    target 1425
  ]
  edge [
    source 22
    target 181
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 1059
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 1426
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 1427
  ]
  edge [
    source 22
    target 220
  ]
  edge [
    source 22
    target 1428
  ]
  edge [
    source 22
    target 1429
  ]
  edge [
    source 22
    target 1430
  ]
  edge [
    source 22
    target 1431
  ]
  edge [
    source 22
    target 1432
  ]
  edge [
    source 22
    target 1433
  ]
  edge [
    source 22
    target 1434
  ]
  edge [
    source 22
    target 1435
  ]
  edge [
    source 22
    target 1436
  ]
  edge [
    source 22
    target 1437
  ]
  edge [
    source 22
    target 1438
  ]
  edge [
    source 22
    target 1439
  ]
  edge [
    source 22
    target 1440
  ]
  edge [
    source 22
    target 1441
  ]
  edge [
    source 22
    target 1442
  ]
  edge [
    source 22
    target 831
  ]
  edge [
    source 22
    target 1443
  ]
  edge [
    source 22
    target 1444
  ]
  edge [
    source 22
    target 1445
  ]
  edge [
    source 22
    target 1446
  ]
  edge [
    source 22
    target 1447
  ]
  edge [
    source 22
    target 1448
  ]
  edge [
    source 22
    target 1449
  ]
  edge [
    source 22
    target 151
  ]
  edge [
    source 22
    target 1450
  ]
  edge [
    source 22
    target 1451
  ]
  edge [
    source 22
    target 1452
  ]
  edge [
    source 22
    target 1453
  ]
  edge [
    source 22
    target 1454
  ]
  edge [
    source 22
    target 1455
  ]
  edge [
    source 22
    target 1456
  ]
  edge [
    source 22
    target 1457
  ]
  edge [
    source 22
    target 1458
  ]
  edge [
    source 22
    target 1459
  ]
  edge [
    source 22
    target 1460
  ]
  edge [
    source 22
    target 1461
  ]
  edge [
    source 22
    target 1462
  ]
  edge [
    source 22
    target 1463
  ]
  edge [
    source 22
    target 1464
  ]
  edge [
    source 22
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 37
  ]
  edge [
    source 23
    target 336
  ]
  edge [
    source 23
    target 1465
  ]
  edge [
    source 23
    target 1466
  ]
  edge [
    source 23
    target 1467
  ]
  edge [
    source 23
    target 1468
  ]
  edge [
    source 23
    target 1469
  ]
  edge [
    source 23
    target 1470
  ]
  edge [
    source 23
    target 1471
  ]
  edge [
    source 23
    target 1472
  ]
  edge [
    source 23
    target 1473
  ]
  edge [
    source 23
    target 1474
  ]
  edge [
    source 23
    target 1475
  ]
  edge [
    source 23
    target 1476
  ]
  edge [
    source 23
    target 1477
  ]
  edge [
    source 23
    target 1478
  ]
  edge [
    source 23
    target 1479
  ]
  edge [
    source 23
    target 1480
  ]
  edge [
    source 23
    target 1481
  ]
  edge [
    source 23
    target 1482
  ]
  edge [
    source 23
    target 1483
  ]
  edge [
    source 23
    target 1484
  ]
  edge [
    source 23
    target 1485
  ]
  edge [
    source 23
    target 1486
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 36
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 1487
  ]
  edge [
    source 24
    target 1488
  ]
  edge [
    source 24
    target 1489
  ]
  edge [
    source 24
    target 357
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 25
    target 1490
  ]
  edge [
    source 25
    target 1301
  ]
  edge [
    source 25
    target 1309
  ]
  edge [
    source 25
    target 1299
  ]
  edge [
    source 25
    target 1300
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 77
  ]
  edge [
    source 26
    target 1491
  ]
  edge [
    source 26
    target 1337
  ]
  edge [
    source 26
    target 1492
  ]
  edge [
    source 26
    target 1493
  ]
  edge [
    source 26
    target 1494
  ]
  edge [
    source 26
    target 1495
  ]
  edge [
    source 26
    target 54
  ]
  edge [
    source 26
    target 1263
  ]
  edge [
    source 26
    target 1496
  ]
  edge [
    source 26
    target 1497
  ]
  edge [
    source 26
    target 1498
  ]
  edge [
    source 26
    target 1499
  ]
  edge [
    source 26
    target 1500
  ]
  edge [
    source 26
    target 1501
  ]
  edge [
    source 26
    target 1502
  ]
  edge [
    source 26
    target 43
  ]
  edge [
    source 26
    target 329
  ]
  edge [
    source 26
    target 67
  ]
  edge [
    source 26
    target 357
  ]
  edge [
    source 26
    target 1503
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 26
    target 38
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 31
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 27
    target 263
  ]
  edge [
    source 27
    target 264
  ]
  edge [
    source 27
    target 1504
  ]
  edge [
    source 27
    target 1505
  ]
  edge [
    source 27
    target 1506
  ]
  edge [
    source 27
    target 1507
  ]
  edge [
    source 27
    target 1508
  ]
  edge [
    source 27
    target 1509
  ]
  edge [
    source 27
    target 1510
  ]
  edge [
    source 27
    target 1511
  ]
  edge [
    source 27
    target 1512
  ]
  edge [
    source 27
    target 1513
  ]
  edge [
    source 27
    target 1514
  ]
  edge [
    source 27
    target 1515
  ]
  edge [
    source 27
    target 1516
  ]
  edge [
    source 27
    target 1517
  ]
  edge [
    source 27
    target 1518
  ]
  edge [
    source 27
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1519
  ]
  edge [
    source 28
    target 1520
  ]
  edge [
    source 28
    target 1521
  ]
  edge [
    source 28
    target 887
  ]
  edge [
    source 28
    target 1522
  ]
  edge [
    source 28
    target 1523
  ]
  edge [
    source 28
    target 1524
  ]
  edge [
    source 28
    target 1327
  ]
  edge [
    source 28
    target 1525
  ]
  edge [
    source 28
    target 502
  ]
  edge [
    source 28
    target 1526
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 1086
  ]
  edge [
    source 28
    target 1527
  ]
  edge [
    source 28
    target 1528
  ]
  edge [
    source 28
    target 1529
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 1530
  ]
  edge [
    source 28
    target 1531
  ]
  edge [
    source 28
    target 1532
  ]
  edge [
    source 28
    target 1533
  ]
  edge [
    source 28
    target 377
  ]
  edge [
    source 28
    target 1396
  ]
  edge [
    source 28
    target 379
  ]
  edge [
    source 28
    target 381
  ]
  edge [
    source 28
    target 1534
  ]
  edge [
    source 28
    target 384
  ]
  edge [
    source 28
    target 1535
  ]
  edge [
    source 28
    target 1536
  ]
  edge [
    source 28
    target 1537
  ]
  edge [
    source 28
    target 1538
  ]
  edge [
    source 28
    target 1539
  ]
  edge [
    source 28
    target 1540
  ]
  edge [
    source 28
    target 1541
  ]
  edge [
    source 28
    target 1542
  ]
  edge [
    source 28
    target 1153
  ]
  edge [
    source 28
    target 1543
  ]
  edge [
    source 28
    target 1544
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 101
  ]
  edge [
    source 28
    target 606
  ]
  edge [
    source 28
    target 1222
  ]
  edge [
    source 28
    target 1215
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 1545
  ]
  edge [
    source 28
    target 70
  ]
  edge [
    source 28
    target 1546
  ]
  edge [
    source 28
    target 1547
  ]
  edge [
    source 28
    target 698
  ]
  edge [
    source 28
    target 1548
  ]
  edge [
    source 28
    target 1549
  ]
  edge [
    source 28
    target 1550
  ]
  edge [
    source 28
    target 1551
  ]
  edge [
    source 28
    target 1271
  ]
  edge [
    source 28
    target 1552
  ]
  edge [
    source 28
    target 1553
  ]
  edge [
    source 28
    target 1554
  ]
  edge [
    source 28
    target 1555
  ]
  edge [
    source 28
    target 1556
  ]
  edge [
    source 28
    target 1557
  ]
  edge [
    source 28
    target 1558
  ]
  edge [
    source 28
    target 1559
  ]
  edge [
    source 28
    target 1560
  ]
  edge [
    source 28
    target 1561
  ]
  edge [
    source 28
    target 1562
  ]
  edge [
    source 28
    target 1563
  ]
  edge [
    source 28
    target 1564
  ]
  edge [
    source 28
    target 1565
  ]
  edge [
    source 28
    target 1566
  ]
  edge [
    source 28
    target 1567
  ]
  edge [
    source 28
    target 1568
  ]
  edge [
    source 28
    target 697
  ]
  edge [
    source 28
    target 1569
  ]
  edge [
    source 28
    target 1570
  ]
  edge [
    source 28
    target 1571
  ]
  edge [
    source 28
    target 1572
  ]
  edge [
    source 28
    target 1573
  ]
  edge [
    source 28
    target 1574
  ]
  edge [
    source 28
    target 1088
  ]
  edge [
    source 28
    target 1575
  ]
  edge [
    source 28
    target 1576
  ]
  edge [
    source 28
    target 1577
  ]
  edge [
    source 28
    target 1578
  ]
  edge [
    source 28
    target 1579
  ]
  edge [
    source 28
    target 1580
  ]
  edge [
    source 28
    target 1581
  ]
  edge [
    source 28
    target 1582
  ]
  edge [
    source 28
    target 1583
  ]
  edge [
    source 28
    target 1584
  ]
  edge [
    source 28
    target 1585
  ]
  edge [
    source 28
    target 1586
  ]
  edge [
    source 28
    target 1587
  ]
  edge [
    source 28
    target 1588
  ]
  edge [
    source 28
    target 43
  ]
  edge [
    source 28
    target 1589
  ]
  edge [
    source 28
    target 1590
  ]
  edge [
    source 28
    target 1591
  ]
  edge [
    source 28
    target 1592
  ]
  edge [
    source 28
    target 1593
  ]
  edge [
    source 28
    target 1594
  ]
  edge [
    source 28
    target 325
  ]
  edge [
    source 28
    target 1595
  ]
  edge [
    source 28
    target 1490
  ]
  edge [
    source 28
    target 1596
  ]
  edge [
    source 28
    target 1597
  ]
  edge [
    source 28
    target 1598
  ]
  edge [
    source 28
    target 1599
  ]
  edge [
    source 28
    target 1600
  ]
  edge [
    source 28
    target 1601
  ]
  edge [
    source 28
    target 1602
  ]
  edge [
    source 28
    target 1603
  ]
  edge [
    source 28
    target 1604
  ]
  edge [
    source 28
    target 1605
  ]
  edge [
    source 28
    target 1606
  ]
  edge [
    source 28
    target 1607
  ]
  edge [
    source 28
    target 1608
  ]
  edge [
    source 28
    target 1609
  ]
  edge [
    source 28
    target 1610
  ]
  edge [
    source 28
    target 1611
  ]
  edge [
    source 28
    target 1612
  ]
  edge [
    source 28
    target 1613
  ]
  edge [
    source 28
    target 1614
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 29
    target 952
  ]
  edge [
    source 29
    target 953
  ]
  edge [
    source 29
    target 954
  ]
  edge [
    source 29
    target 955
  ]
  edge [
    source 29
    target 550
  ]
  edge [
    source 29
    target 956
  ]
  edge [
    source 29
    target 957
  ]
  edge [
    source 29
    target 958
  ]
  edge [
    source 29
    target 959
  ]
  edge [
    source 29
    target 961
  ]
  edge [
    source 29
    target 960
  ]
  edge [
    source 29
    target 962
  ]
  edge [
    source 29
    target 938
  ]
  edge [
    source 29
    target 1615
  ]
  edge [
    source 29
    target 1616
  ]
  edge [
    source 29
    target 1617
  ]
  edge [
    source 29
    target 1618
  ]
  edge [
    source 29
    target 567
  ]
  edge [
    source 29
    target 1619
  ]
  edge [
    source 29
    target 1620
  ]
  edge [
    source 29
    target 1621
  ]
  edge [
    source 29
    target 1622
  ]
  edge [
    source 29
    target 912
  ]
  edge [
    source 29
    target 1623
  ]
  edge [
    source 29
    target 926
  ]
  edge [
    source 29
    target 1624
  ]
  edge [
    source 29
    target 935
  ]
  edge [
    source 29
    target 1625
  ]
  edge [
    source 29
    target 1626
  ]
  edge [
    source 29
    target 1627
  ]
  edge [
    source 29
    target 1628
  ]
  edge [
    source 29
    target 1629
  ]
  edge [
    source 29
    target 1630
  ]
  edge [
    source 29
    target 1631
  ]
  edge [
    source 29
    target 916
  ]
  edge [
    source 29
    target 1632
  ]
  edge [
    source 29
    target 555
  ]
  edge [
    source 29
    target 1633
  ]
  edge [
    source 29
    target 1634
  ]
  edge [
    source 29
    target 1635
  ]
  edge [
    source 29
    target 1636
  ]
  edge [
    source 29
    target 1637
  ]
  edge [
    source 29
    target 1638
  ]
  edge [
    source 29
    target 1639
  ]
  edge [
    source 29
    target 1088
  ]
  edge [
    source 29
    target 1640
  ]
  edge [
    source 29
    target 1641
  ]
  edge [
    source 29
    target 1642
  ]
  edge [
    source 29
    target 1643
  ]
  edge [
    source 29
    target 1644
  ]
  edge [
    source 29
    target 1645
  ]
  edge [
    source 29
    target 1646
  ]
  edge [
    source 29
    target 1647
  ]
  edge [
    source 29
    target 1648
  ]
  edge [
    source 29
    target 1649
  ]
  edge [
    source 29
    target 1650
  ]
  edge [
    source 29
    target 1651
  ]
  edge [
    source 29
    target 1652
  ]
  edge [
    source 29
    target 920
  ]
  edge [
    source 29
    target 921
  ]
  edge [
    source 29
    target 1653
  ]
  edge [
    source 29
    target 1654
  ]
  edge [
    source 29
    target 1655
  ]
  edge [
    source 29
    target 1656
  ]
  edge [
    source 29
    target 923
  ]
  edge [
    source 29
    target 932
  ]
  edge [
    source 29
    target 1657
  ]
  edge [
    source 29
    target 1658
  ]
  edge [
    source 29
    target 1659
  ]
  edge [
    source 29
    target 1660
  ]
  edge [
    source 29
    target 1661
  ]
  edge [
    source 29
    target 927
  ]
  edge [
    source 29
    target 1662
  ]
  edge [
    source 29
    target 1663
  ]
  edge [
    source 29
    target 1664
  ]
  edge [
    source 29
    target 1665
  ]
  edge [
    source 29
    target 1666
  ]
  edge [
    source 29
    target 1667
  ]
  edge [
    source 29
    target 1668
  ]
  edge [
    source 29
    target 1474
  ]
  edge [
    source 29
    target 1669
  ]
  edge [
    source 29
    target 1670
  ]
  edge [
    source 29
    target 1671
  ]
  edge [
    source 29
    target 1672
  ]
  edge [
    source 29
    target 549
  ]
  edge [
    source 29
    target 1673
  ]
  edge [
    source 29
    target 1674
  ]
  edge [
    source 29
    target 264
  ]
  edge [
    source 29
    target 245
  ]
  edge [
    source 29
    target 1675
  ]
  edge [
    source 29
    target 492
  ]
  edge [
    source 29
    target 1676
  ]
  edge [
    source 29
    target 1677
  ]
  edge [
    source 29
    target 1678
  ]
  edge [
    source 29
    target 1679
  ]
  edge [
    source 29
    target 90
  ]
  edge [
    source 29
    target 1680
  ]
  edge [
    source 29
    target 1681
  ]
  edge [
    source 29
    target 1682
  ]
  edge [
    source 29
    target 1683
  ]
  edge [
    source 29
    target 1684
  ]
  edge [
    source 29
    target 1685
  ]
  edge [
    source 29
    target 1686
  ]
  edge [
    source 29
    target 1687
  ]
  edge [
    source 29
    target 1688
  ]
  edge [
    source 29
    target 1689
  ]
  edge [
    source 29
    target 1690
  ]
  edge [
    source 29
    target 1691
  ]
  edge [
    source 29
    target 1692
  ]
  edge [
    source 29
    target 1693
  ]
  edge [
    source 29
    target 1694
  ]
  edge [
    source 29
    target 1695
  ]
  edge [
    source 29
    target 1696
  ]
  edge [
    source 29
    target 1697
  ]
  edge [
    source 29
    target 1698
  ]
  edge [
    source 29
    target 1699
  ]
  edge [
    source 29
    target 1700
  ]
  edge [
    source 29
    target 1701
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1702
  ]
  edge [
    source 30
    target 1703
  ]
  edge [
    source 30
    target 1704
  ]
  edge [
    source 30
    target 1705
  ]
  edge [
    source 30
    target 1706
  ]
  edge [
    source 30
    target 1707
  ]
  edge [
    source 30
    target 1221
  ]
  edge [
    source 30
    target 1708
  ]
  edge [
    source 30
    target 1709
  ]
  edge [
    source 30
    target 1710
  ]
  edge [
    source 30
    target 1711
  ]
  edge [
    source 30
    target 1712
  ]
  edge [
    source 30
    target 1713
  ]
  edge [
    source 30
    target 1714
  ]
  edge [
    source 30
    target 1715
  ]
  edge [
    source 30
    target 1716
  ]
  edge [
    source 30
    target 1717
  ]
  edge [
    source 30
    target 873
  ]
  edge [
    source 30
    target 697
  ]
  edge [
    source 30
    target 133
  ]
  edge [
    source 30
    target 1718
  ]
  edge [
    source 30
    target 983
  ]
  edge [
    source 30
    target 1234
  ]
  edge [
    source 30
    target 1719
  ]
  edge [
    source 30
    target 1720
  ]
  edge [
    source 30
    target 1167
  ]
  edge [
    source 30
    target 1721
  ]
  edge [
    source 30
    target 1004
  ]
  edge [
    source 30
    target 1722
  ]
  edge [
    source 30
    target 856
  ]
  edge [
    source 30
    target 1258
  ]
  edge [
    source 30
    target 1723
  ]
  edge [
    source 30
    target 1724
  ]
  edge [
    source 30
    target 1725
  ]
  edge [
    source 30
    target 833
  ]
  edge [
    source 30
    target 1726
  ]
  edge [
    source 30
    target 1727
  ]
  edge [
    source 30
    target 1728
  ]
  edge [
    source 30
    target 1729
  ]
  edge [
    source 30
    target 1730
  ]
  edge [
    source 30
    target 1731
  ]
  edge [
    source 30
    target 1732
  ]
  edge [
    source 30
    target 548
  ]
  edge [
    source 30
    target 101
  ]
  edge [
    source 30
    target 1733
  ]
  edge [
    source 30
    target 1069
  ]
  edge [
    source 30
    target 1734
  ]
  edge [
    source 30
    target 1735
  ]
  edge [
    source 30
    target 846
  ]
  edge [
    source 30
    target 1736
  ]
  edge [
    source 30
    target 1737
  ]
  edge [
    source 30
    target 1738
  ]
  edge [
    source 30
    target 1739
  ]
  edge [
    source 30
    target 1740
  ]
  edge [
    source 30
    target 1741
  ]
  edge [
    source 30
    target 1742
  ]
  edge [
    source 30
    target 1743
  ]
  edge [
    source 30
    target 1744
  ]
  edge [
    source 30
    target 1745
  ]
  edge [
    source 30
    target 1746
  ]
  edge [
    source 30
    target 1747
  ]
  edge [
    source 30
    target 1748
  ]
  edge [
    source 30
    target 1749
  ]
  edge [
    source 30
    target 1750
  ]
  edge [
    source 30
    target 1751
  ]
  edge [
    source 30
    target 1752
  ]
  edge [
    source 30
    target 1753
  ]
  edge [
    source 30
    target 1754
  ]
  edge [
    source 30
    target 1755
  ]
  edge [
    source 30
    target 1756
  ]
  edge [
    source 30
    target 244
  ]
  edge [
    source 30
    target 1757
  ]
  edge [
    source 30
    target 1758
  ]
  edge [
    source 30
    target 236
  ]
  edge [
    source 30
    target 1131
  ]
  edge [
    source 30
    target 1759
  ]
  edge [
    source 30
    target 1760
  ]
  edge [
    source 30
    target 145
  ]
  edge [
    source 30
    target 1761
  ]
  edge [
    source 30
    target 1762
  ]
  edge [
    source 30
    target 1763
  ]
  edge [
    source 30
    target 1270
  ]
  edge [
    source 30
    target 1764
  ]
  edge [
    source 30
    target 381
  ]
  edge [
    source 30
    target 1765
  ]
  edge [
    source 30
    target 1766
  ]
  edge [
    source 30
    target 1767
  ]
  edge [
    source 30
    target 1768
  ]
  edge [
    source 30
    target 1293
  ]
  edge [
    source 30
    target 1769
  ]
  edge [
    source 30
    target 1770
  ]
  edge [
    source 30
    target 1771
  ]
  edge [
    source 30
    target 135
  ]
  edge [
    source 30
    target 163
  ]
  edge [
    source 30
    target 1772
  ]
  edge [
    source 30
    target 1773
  ]
  edge [
    source 30
    target 1774
  ]
  edge [
    source 30
    target 1775
  ]
  edge [
    source 30
    target 1776
  ]
  edge [
    source 30
    target 1777
  ]
  edge [
    source 30
    target 1778
  ]
  edge [
    source 30
    target 1779
  ]
  edge [
    source 30
    target 1284
  ]
  edge [
    source 30
    target 1780
  ]
  edge [
    source 30
    target 1781
  ]
  edge [
    source 30
    target 1782
  ]
  edge [
    source 30
    target 1783
  ]
  edge [
    source 30
    target 1784
  ]
  edge [
    source 30
    target 1785
  ]
  edge [
    source 30
    target 1786
  ]
  edge [
    source 30
    target 1787
  ]
  edge [
    source 30
    target 1788
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 31
    target 1146
  ]
  edge [
    source 31
    target 1789
  ]
  edge [
    source 31
    target 1790
  ]
  edge [
    source 31
    target 479
  ]
  edge [
    source 31
    target 1791
  ]
  edge [
    source 31
    target 46
  ]
  edge [
    source 31
    target 1792
  ]
  edge [
    source 31
    target 1793
  ]
  edge [
    source 31
    target 1794
  ]
  edge [
    source 31
    target 77
  ]
  edge [
    source 31
    target 1795
  ]
  edge [
    source 31
    target 43
  ]
  edge [
    source 31
    target 1796
  ]
  edge [
    source 31
    target 1797
  ]
  edge [
    source 31
    target 810
  ]
  edge [
    source 31
    target 1798
  ]
  edge [
    source 31
    target 1337
  ]
  edge [
    source 31
    target 1799
  ]
  edge [
    source 31
    target 72
  ]
  edge [
    source 31
    target 1800
  ]
  edge [
    source 31
    target 1437
  ]
  edge [
    source 31
    target 1801
  ]
  edge [
    source 31
    target 1802
  ]
  edge [
    source 31
    target 323
  ]
  edge [
    source 31
    target 1803
  ]
  edge [
    source 31
    target 1804
  ]
  edge [
    source 31
    target 1805
  ]
  edge [
    source 31
    target 1806
  ]
  edge [
    source 31
    target 1807
  ]
  edge [
    source 31
    target 1808
  ]
  edge [
    source 31
    target 1809
  ]
  edge [
    source 31
    target 357
  ]
  edge [
    source 31
    target 1810
  ]
  edge [
    source 31
    target 329
  ]
  edge [
    source 31
    target 67
  ]
  edge [
    source 31
    target 1503
  ]
  edge [
    source 31
    target 1501
  ]
  edge [
    source 31
    target 1811
  ]
  edge [
    source 31
    target 380
  ]
  edge [
    source 31
    target 1812
  ]
  edge [
    source 31
    target 1813
  ]
  edge [
    source 31
    target 1814
  ]
  edge [
    source 31
    target 331
  ]
  edge [
    source 31
    target 1098
  ]
  edge [
    source 31
    target 1815
  ]
  edge [
    source 31
    target 1816
  ]
  edge [
    source 31
    target 1817
  ]
  edge [
    source 31
    target 1818
  ]
  edge [
    source 31
    target 53
  ]
  edge [
    source 31
    target 1479
  ]
  edge [
    source 31
    target 1819
  ]
  edge [
    source 31
    target 1820
  ]
  edge [
    source 31
    target 803
  ]
  edge [
    source 31
    target 1821
  ]
  edge [
    source 31
    target 1822
  ]
  edge [
    source 31
    target 1823
  ]
  edge [
    source 31
    target 871
  ]
  edge [
    source 31
    target 1824
  ]
  edge [
    source 31
    target 1825
  ]
  edge [
    source 31
    target 1826
  ]
  edge [
    source 31
    target 1827
  ]
  edge [
    source 31
    target 1506
  ]
  edge [
    source 31
    target 1828
  ]
  edge [
    source 31
    target 1829
  ]
  edge [
    source 31
    target 1830
  ]
  edge [
    source 31
    target 44
  ]
  edge [
    source 31
    target 1831
  ]
  edge [
    source 31
    target 1832
  ]
  edge [
    source 31
    target 1833
  ]
  edge [
    source 31
    target 1834
  ]
  edge [
    source 31
    target 1295
  ]
  edge [
    source 31
    target 1317
  ]
  edge [
    source 31
    target 1835
  ]
  edge [
    source 31
    target 1836
  ]
  edge [
    source 31
    target 1837
  ]
  edge [
    source 31
    target 1838
  ]
  edge [
    source 31
    target 1839
  ]
  edge [
    source 31
    target 1840
  ]
  edge [
    source 31
    target 416
  ]
  edge [
    source 31
    target 1841
  ]
  edge [
    source 31
    target 346
  ]
  edge [
    source 31
    target 1842
  ]
  edge [
    source 31
    target 1192
  ]
  edge [
    source 31
    target 172
  ]
  edge [
    source 31
    target 1843
  ]
  edge [
    source 31
    target 1844
  ]
  edge [
    source 31
    target 1845
  ]
  edge [
    source 31
    target 1846
  ]
  edge [
    source 31
    target 1847
  ]
  edge [
    source 31
    target 1848
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1319
  ]
  edge [
    source 32
    target 606
  ]
  edge [
    source 32
    target 986
  ]
  edge [
    source 32
    target 1320
  ]
  edge [
    source 32
    target 1167
  ]
  edge [
    source 32
    target 254
  ]
  edge [
    source 32
    target 1321
  ]
  edge [
    source 32
    target 1322
  ]
  edge [
    source 32
    target 1323
  ]
  edge [
    source 32
    target 1324
  ]
  edge [
    source 32
    target 1325
  ]
  edge [
    source 32
    target 245
  ]
  edge [
    source 32
    target 1326
  ]
  edge [
    source 32
    target 1327
  ]
  edge [
    source 32
    target 1544
  ]
  edge [
    source 32
    target 101
  ]
  edge [
    source 32
    target 1222
  ]
  edge [
    source 32
    target 1215
  ]
  edge [
    source 32
    target 243
  ]
  edge [
    source 32
    target 1546
  ]
  edge [
    source 32
    target 1545
  ]
  edge [
    source 32
    target 70
  ]
  edge [
    source 32
    target 244
  ]
  edge [
    source 32
    target 1547
  ]
  edge [
    source 32
    target 698
  ]
  edge [
    source 32
    target 1548
  ]
  edge [
    source 32
    target 381
  ]
  edge [
    source 32
    target 1849
  ]
  edge [
    source 32
    target 237
  ]
  edge [
    source 32
    target 1850
  ]
  edge [
    source 32
    target 1851
  ]
  edge [
    source 32
    target 1852
  ]
  edge [
    source 32
    target 1853
  ]
  edge [
    source 32
    target 1854
  ]
  edge [
    source 32
    target 1855
  ]
  edge [
    source 32
    target 1856
  ]
  edge [
    source 32
    target 1857
  ]
  edge [
    source 32
    target 1858
  ]
  edge [
    source 32
    target 1859
  ]
  edge [
    source 32
    target 1860
  ]
  edge [
    source 32
    target 67
  ]
  edge [
    source 32
    target 873
  ]
  edge [
    source 32
    target 1269
  ]
  edge [
    source 32
    target 1271
  ]
  edge [
    source 32
    target 1861
  ]
  edge [
    source 32
    target 957
  ]
  edge [
    source 32
    target 1862
  ]
  edge [
    source 32
    target 1863
  ]
  edge [
    source 32
    target 548
  ]
  edge [
    source 32
    target 625
  ]
  edge [
    source 32
    target 626
  ]
  edge [
    source 32
    target 627
  ]
  edge [
    source 32
    target 628
  ]
  edge [
    source 32
    target 630
  ]
  edge [
    source 32
    target 629
  ]
  edge [
    source 32
    target 597
  ]
  edge [
    source 32
    target 631
  ]
  edge [
    source 32
    target 632
  ]
  edge [
    source 32
    target 633
  ]
  edge [
    source 32
    target 634
  ]
  edge [
    source 32
    target 635
  ]
  edge [
    source 32
    target 636
  ]
  edge [
    source 32
    target 1556
  ]
  edge [
    source 32
    target 1557
  ]
  edge [
    source 32
    target 1519
  ]
  edge [
    source 32
    target 1558
  ]
  edge [
    source 32
    target 1559
  ]
  edge [
    source 32
    target 1560
  ]
  edge [
    source 32
    target 384
  ]
  edge [
    source 32
    target 1561
  ]
  edge [
    source 32
    target 114
  ]
  edge [
    source 32
    target 1864
  ]
  edge [
    source 32
    target 972
  ]
  edge [
    source 32
    target 1865
  ]
  edge [
    source 32
    target 1866
  ]
  edge [
    source 32
    target 1867
  ]
  edge [
    source 32
    target 1868
  ]
  edge [
    source 32
    target 610
  ]
  edge [
    source 32
    target 1869
  ]
  edge [
    source 32
    target 1673
  ]
  edge [
    source 32
    target 89
  ]
  edge [
    source 32
    target 1870
  ]
  edge [
    source 32
    target 1065
  ]
  edge [
    source 32
    target 1066
  ]
  edge [
    source 32
    target 1067
  ]
  edge [
    source 32
    target 1068
  ]
  edge [
    source 32
    target 1069
  ]
  edge [
    source 32
    target 1070
  ]
  edge [
    source 32
    target 1071
  ]
  edge [
    source 32
    target 1072
  ]
  edge [
    source 32
    target 1073
  ]
  edge [
    source 32
    target 1075
  ]
  edge [
    source 32
    target 1074
  ]
  edge [
    source 32
    target 1076
  ]
  edge [
    source 32
    target 1077
  ]
  edge [
    source 32
    target 1078
  ]
  edge [
    source 32
    target 1079
  ]
  edge [
    source 32
    target 1080
  ]
  edge [
    source 32
    target 1081
  ]
  edge [
    source 32
    target 1082
  ]
  edge [
    source 32
    target 1083
  ]
  edge [
    source 32
    target 1084
  ]
  edge [
    source 32
    target 1085
  ]
  edge [
    source 32
    target 1086
  ]
  edge [
    source 32
    target 1087
  ]
  edge [
    source 32
    target 505
  ]
  edge [
    source 32
    target 1088
  ]
  edge [
    source 32
    target 1089
  ]
  edge [
    source 32
    target 1090
  ]
  edge [
    source 32
    target 1091
  ]
  edge [
    source 32
    target 1092
  ]
  edge [
    source 32
    target 1093
  ]
  edge [
    source 32
    target 1094
  ]
  edge [
    source 32
    target 1095
  ]
  edge [
    source 32
    target 1096
  ]
  edge [
    source 32
    target 822
  ]
  edge [
    source 32
    target 1097
  ]
  edge [
    source 32
    target 1098
  ]
  edge [
    source 32
    target 1099
  ]
  edge [
    source 32
    target 1100
  ]
  edge [
    source 32
    target 1101
  ]
  edge [
    source 32
    target 1102
  ]
  edge [
    source 32
    target 1103
  ]
  edge [
    source 32
    target 1104
  ]
  edge [
    source 32
    target 1105
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1871
  ]
  edge [
    source 33
    target 1872
  ]
  edge [
    source 33
    target 1873
  ]
  edge [
    source 33
    target 1167
  ]
  edge [
    source 33
    target 1874
  ]
  edge [
    source 33
    target 1229
  ]
  edge [
    source 33
    target 1875
  ]
  edge [
    source 33
    target 1876
  ]
  edge [
    source 33
    target 1877
  ]
  edge [
    source 33
    target 1878
  ]
  edge [
    source 33
    target 1081
  ]
  edge [
    source 33
    target 1879
  ]
  edge [
    source 33
    target 101
  ]
  edge [
    source 33
    target 1860
  ]
  edge [
    source 33
    target 67
  ]
  edge [
    source 33
    target 873
  ]
  edge [
    source 33
    target 1880
  ]
  edge [
    source 33
    target 1004
  ]
  edge [
    source 33
    target 1881
  ]
  edge [
    source 33
    target 864
  ]
  edge [
    source 33
    target 1148
  ]
  edge [
    source 33
    target 1882
  ]
  edge [
    source 33
    target 1883
  ]
  edge [
    source 33
    target 1884
  ]
  edge [
    source 33
    target 243
  ]
  edge [
    source 33
    target 1885
  ]
  edge [
    source 33
    target 1715
  ]
  edge [
    source 33
    target 232
  ]
  edge [
    source 33
    target 60
  ]
  edge [
    source 33
    target 1702
  ]
  edge [
    source 33
    target 1703
  ]
  edge [
    source 33
    target 1704
  ]
  edge [
    source 33
    target 1705
  ]
  edge [
    source 33
    target 1706
  ]
  edge [
    source 33
    target 1707
  ]
  edge [
    source 33
    target 1221
  ]
  edge [
    source 33
    target 1708
  ]
  edge [
    source 33
    target 1709
  ]
  edge [
    source 33
    target 1710
  ]
  edge [
    source 33
    target 1711
  ]
  edge [
    source 33
    target 1712
  ]
  edge [
    source 33
    target 1713
  ]
  edge [
    source 33
    target 1714
  ]
  edge [
    source 33
    target 1716
  ]
  edge [
    source 33
    target 1717
  ]
  edge [
    source 33
    target 1886
  ]
  edge [
    source 33
    target 1887
  ]
  edge [
    source 33
    target 1888
  ]
  edge [
    source 33
    target 1889
  ]
  edge [
    source 33
    target 1890
  ]
  edge [
    source 33
    target 1891
  ]
  edge [
    source 33
    target 1892
  ]
  edge [
    source 33
    target 1893
  ]
  edge [
    source 33
    target 1894
  ]
  edge [
    source 33
    target 1895
  ]
  edge [
    source 33
    target 1896
  ]
  edge [
    source 33
    target 1897
  ]
  edge [
    source 33
    target 1898
  ]
  edge [
    source 33
    target 1259
  ]
  edge [
    source 33
    target 479
  ]
  edge [
    source 33
    target 1260
  ]
  edge [
    source 33
    target 1258
  ]
  edge [
    source 33
    target 1261
  ]
  edge [
    source 33
    target 1262
  ]
  edge [
    source 33
    target 1263
  ]
  edge [
    source 33
    target 1114
  ]
  edge [
    source 33
    target 1264
  ]
  edge [
    source 33
    target 1265
  ]
  edge [
    source 33
    target 801
  ]
  edge [
    source 33
    target 1245
  ]
  edge [
    source 33
    target 1899
  ]
  edge [
    source 33
    target 1900
  ]
  edge [
    source 33
    target 1901
  ]
  edge [
    source 33
    target 1902
  ]
  edge [
    source 33
    target 1903
  ]
  edge [
    source 33
    target 1904
  ]
  edge [
    source 33
    target 1905
  ]
  edge [
    source 33
    target 1906
  ]
  edge [
    source 33
    target 1907
  ]
  edge [
    source 33
    target 1908
  ]
  edge [
    source 33
    target 1909
  ]
  edge [
    source 33
    target 1910
  ]
  edge [
    source 33
    target 1911
  ]
  edge [
    source 33
    target 1912
  ]
  edge [
    source 33
    target 1913
  ]
  edge [
    source 33
    target 1914
  ]
  edge [
    source 33
    target 1915
  ]
  edge [
    source 33
    target 1916
  ]
  edge [
    source 33
    target 1917
  ]
  edge [
    source 33
    target 1918
  ]
  edge [
    source 33
    target 1919
  ]
  edge [
    source 33
    target 1920
  ]
  edge [
    source 33
    target 1131
  ]
  edge [
    source 33
    target 165
  ]
  edge [
    source 33
    target 1921
  ]
  edge [
    source 33
    target 1922
  ]
  edge [
    source 33
    target 1923
  ]
  edge [
    source 33
    target 1924
  ]
  edge [
    source 33
    target 1925
  ]
  edge [
    source 33
    target 869
  ]
  edge [
    source 33
    target 1926
  ]
  edge [
    source 33
    target 1561
  ]
  edge [
    source 33
    target 1927
  ]
  edge [
    source 33
    target 1928
  ]
  edge [
    source 33
    target 1929
  ]
  edge [
    source 33
    target 1930
  ]
  edge [
    source 33
    target 1931
  ]
  edge [
    source 33
    target 236
  ]
  edge [
    source 34
    target 1932
  ]
  edge [
    source 34
    target 340
  ]
  edge [
    source 34
    target 1481
  ]
  edge [
    source 35
    target 1933
  ]
  edge [
    source 35
    target 808
  ]
  edge [
    source 35
    target 1934
  ]
  edge [
    source 35
    target 337
  ]
  edge [
    source 35
    target 1935
  ]
  edge [
    source 35
    target 1936
  ]
  edge [
    source 35
    target 1482
  ]
  edge [
    source 35
    target 347
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 36
    target 41
  ]
  edge [
    source 36
    target 1438
  ]
  edge [
    source 36
    target 1937
  ]
  edge [
    source 36
    target 1938
  ]
  edge [
    source 36
    target 1939
  ]
  edge [
    source 36
    target 156
  ]
  edge [
    source 36
    target 1940
  ]
  edge [
    source 36
    target 1941
  ]
  edge [
    source 36
    target 1942
  ]
  edge [
    source 36
    target 1402
  ]
  edge [
    source 36
    target 1943
  ]
  edge [
    source 36
    target 72
  ]
  edge [
    source 36
    target 1944
  ]
  edge [
    source 36
    target 1945
  ]
  edge [
    source 36
    target 646
  ]
  edge [
    source 36
    target 647
  ]
  edge [
    source 36
    target 648
  ]
  edge [
    source 36
    target 649
  ]
  edge [
    source 36
    target 650
  ]
  edge [
    source 36
    target 651
  ]
  edge [
    source 36
    target 652
  ]
  edge [
    source 36
    target 653
  ]
  edge [
    source 36
    target 654
  ]
  edge [
    source 36
    target 655
  ]
  edge [
    source 36
    target 656
  ]
  edge [
    source 36
    target 657
  ]
  edge [
    source 36
    target 658
  ]
  edge [
    source 36
    target 659
  ]
  edge [
    source 36
    target 660
  ]
  edge [
    source 36
    target 661
  ]
  edge [
    source 36
    target 662
  ]
  edge [
    source 36
    target 663
  ]
  edge [
    source 36
    target 664
  ]
  edge [
    source 36
    target 665
  ]
  edge [
    source 36
    target 666
  ]
  edge [
    source 36
    target 667
  ]
  edge [
    source 36
    target 668
  ]
  edge [
    source 36
    target 669
  ]
  edge [
    source 36
    target 670
  ]
  edge [
    source 36
    target 671
  ]
  edge [
    source 36
    target 672
  ]
  edge [
    source 36
    target 673
  ]
  edge [
    source 36
    target 674
  ]
  edge [
    source 36
    target 675
  ]
  edge [
    source 36
    target 676
  ]
  edge [
    source 36
    target 677
  ]
  edge [
    source 36
    target 678
  ]
  edge [
    source 36
    target 679
  ]
  edge [
    source 36
    target 680
  ]
  edge [
    source 36
    target 681
  ]
  edge [
    source 36
    target 682
  ]
  edge [
    source 36
    target 1946
  ]
  edge [
    source 36
    target 1947
  ]
  edge [
    source 36
    target 1068
  ]
  edge [
    source 36
    target 1948
  ]
  edge [
    source 36
    target 1949
  ]
  edge [
    source 36
    target 1950
  ]
  edge [
    source 36
    target 1869
  ]
  edge [
    source 36
    target 1951
  ]
  edge [
    source 36
    target 1952
  ]
  edge [
    source 36
    target 220
  ]
  edge [
    source 36
    target 1953
  ]
  edge [
    source 36
    target 1954
  ]
  edge [
    source 36
    target 360
  ]
  edge [
    source 36
    target 1955
  ]
  edge [
    source 36
    target 1956
  ]
  edge [
    source 36
    target 1957
  ]
  edge [
    source 36
    target 362
  ]
  edge [
    source 36
    target 1958
  ]
  edge [
    source 36
    target 1959
  ]
  edge [
    source 36
    target 470
  ]
  edge [
    source 36
    target 365
  ]
  edge [
    source 36
    target 1041
  ]
  edge [
    source 36
    target 1960
  ]
  edge [
    source 36
    target 1961
  ]
  edge [
    source 36
    target 1962
  ]
  edge [
    source 36
    target 366
  ]
  edge [
    source 36
    target 130
  ]
  edge [
    source 36
    target 1963
  ]
  edge [
    source 36
    target 1964
  ]
  edge [
    source 36
    target 1965
  ]
  edge [
    source 36
    target 424
  ]
  edge [
    source 36
    target 441
  ]
  edge [
    source 36
    target 1966
  ]
  edge [
    source 36
    target 1967
  ]
  edge [
    source 36
    target 1968
  ]
  edge [
    source 36
    target 1396
  ]
  edge [
    source 36
    target 1969
  ]
  edge [
    source 36
    target 1970
  ]
  edge [
    source 36
    target 1971
  ]
  edge [
    source 36
    target 1972
  ]
  edge [
    source 36
    target 1973
  ]
  edge [
    source 36
    target 1974
  ]
  edge [
    source 36
    target 1975
  ]
  edge [
    source 37
    target 1976
  ]
  edge [
    source 37
    target 357
  ]
  edge [
    source 37
    target 1977
  ]
  edge [
    source 37
    target 43
  ]
  edge [
    source 37
    target 51
  ]
  edge [
    source 37
    target 52
  ]
  edge [
    source 37
    target 53
  ]
  edge [
    source 37
    target 54
  ]
  edge [
    source 37
    target 55
  ]
  edge [
    source 37
    target 56
  ]
  edge [
    source 37
    target 57
  ]
  edge [
    source 37
    target 58
  ]
  edge [
    source 37
    target 59
  ]
  edge [
    source 37
    target 60
  ]
  edge [
    source 37
    target 61
  ]
  edge [
    source 37
    target 62
  ]
  edge [
    source 37
    target 63
  ]
  edge [
    source 37
    target 64
  ]
  edge [
    source 37
    target 65
  ]
  edge [
    source 37
    target 66
  ]
  edge [
    source 37
    target 67
  ]
  edge [
    source 37
    target 68
  ]
  edge [
    source 37
    target 69
  ]
  edge [
    source 37
    target 70
  ]
  edge [
    source 37
    target 71
  ]
  edge [
    source 37
    target 1978
  ]
  edge [
    source 37
    target 1979
  ]
  edge [
    source 37
    target 1980
  ]
  edge [
    source 37
    target 77
  ]
  edge [
    source 38
    target 1981
  ]
  edge [
    source 38
    target 1982
  ]
  edge [
    source 38
    target 1983
  ]
  edge [
    source 38
    target 1984
  ]
  edge [
    source 38
    target 1985
  ]
  edge [
    source 38
    target 1986
  ]
  edge [
    source 38
    target 1987
  ]
  edge [
    source 38
    target 1988
  ]
  edge [
    source 38
    target 1573
  ]
  edge [
    source 38
    target 1989
  ]
  edge [
    source 38
    target 60
  ]
  edge [
    source 38
    target 1990
  ]
  edge [
    source 38
    target 1991
  ]
  edge [
    source 38
    target 1992
  ]
  edge [
    source 38
    target 873
  ]
  edge [
    source 38
    target 1993
  ]
  edge [
    source 38
    target 1994
  ]
  edge [
    source 38
    target 245
  ]
  edge [
    source 38
    target 1995
  ]
  edge [
    source 38
    target 1157
  ]
  edge [
    source 38
    target 1996
  ]
  edge [
    source 38
    target 1997
  ]
  edge [
    source 38
    target 1998
  ]
  edge [
    source 38
    target 1999
  ]
  edge [
    source 38
    target 2000
  ]
  edge [
    source 38
    target 2001
  ]
  edge [
    source 38
    target 2002
  ]
  edge [
    source 38
    target 2003
  ]
  edge [
    source 38
    target 2004
  ]
  edge [
    source 38
    target 2005
  ]
  edge [
    source 38
    target 380
  ]
  edge [
    source 38
    target 2006
  ]
  edge [
    source 38
    target 2007
  ]
  edge [
    source 38
    target 1599
  ]
  edge [
    source 38
    target 254
  ]
  edge [
    source 38
    target 2008
  ]
  edge [
    source 38
    target 2009
  ]
  edge [
    source 38
    target 2010
  ]
  edge [
    source 38
    target 2011
  ]
  edge [
    source 38
    target 294
  ]
  edge [
    source 38
    target 2012
  ]
  edge [
    source 38
    target 2013
  ]
  edge [
    source 38
    target 1672
  ]
  edge [
    source 38
    target 2014
  ]
  edge [
    source 38
    target 1863
  ]
  edge [
    source 38
    target 2015
  ]
  edge [
    source 38
    target 236
  ]
  edge [
    source 38
    target 2016
  ]
  edge [
    source 38
    target 2017
  ]
  edge [
    source 38
    target 2018
  ]
  edge [
    source 38
    target 2019
  ]
  edge [
    source 38
    target 697
  ]
  edge [
    source 38
    target 133
  ]
  edge [
    source 38
    target 1718
  ]
  edge [
    source 38
    target 983
  ]
  edge [
    source 38
    target 698
  ]
  edge [
    source 38
    target 1548
  ]
  edge [
    source 38
    target 381
  ]
  edge [
    source 38
    target 2020
  ]
  edge [
    source 38
    target 163
  ]
  edge [
    source 38
    target 2021
  ]
  edge [
    source 38
    target 2022
  ]
  edge [
    source 38
    target 2023
  ]
  edge [
    source 38
    target 2024
  ]
  edge [
    source 38
    target 2025
  ]
  edge [
    source 38
    target 1567
  ]
  edge [
    source 38
    target 2026
  ]
  edge [
    source 38
    target 2027
  ]
  edge [
    source 38
    target 2028
  ]
  edge [
    source 38
    target 2029
  ]
  edge [
    source 38
    target 2030
  ]
  edge [
    source 38
    target 2031
  ]
  edge [
    source 38
    target 2032
  ]
  edge [
    source 38
    target 2033
  ]
  edge [
    source 38
    target 2034
  ]
  edge [
    source 38
    target 2035
  ]
  edge [
    source 38
    target 2036
  ]
  edge [
    source 38
    target 2037
  ]
  edge [
    source 38
    target 2038
  ]
  edge [
    source 38
    target 2039
  ]
  edge [
    source 38
    target 2040
  ]
  edge [
    source 38
    target 2041
  ]
  edge [
    source 38
    target 2042
  ]
  edge [
    source 38
    target 1167
  ]
  edge [
    source 38
    target 1041
  ]
  edge [
    source 38
    target 2043
  ]
  edge [
    source 38
    target 2044
  ]
  edge [
    source 38
    target 2045
  ]
  edge [
    source 38
    target 2046
  ]
  edge [
    source 38
    target 900
  ]
  edge [
    source 38
    target 2047
  ]
  edge [
    source 38
    target 2048
  ]
  edge [
    source 38
    target 2049
  ]
  edge [
    source 38
    target 165
  ]
  edge [
    source 38
    target 2050
  ]
  edge [
    source 38
    target 2051
  ]
  edge [
    source 38
    target 1274
  ]
  edge [
    source 38
    target 2052
  ]
  edge [
    source 38
    target 2053
  ]
  edge [
    source 38
    target 2054
  ]
  edge [
    source 38
    target 2055
  ]
  edge [
    source 38
    target 2056
  ]
  edge [
    source 38
    target 2057
  ]
  edge [
    source 38
    target 2058
  ]
  edge [
    source 38
    target 2059
  ]
  edge [
    source 38
    target 2060
  ]
  edge [
    source 38
    target 2061
  ]
  edge [
    source 38
    target 1362
  ]
  edge [
    source 38
    target 2062
  ]
  edge [
    source 38
    target 2063
  ]
  edge [
    source 38
    target 2064
  ]
  edge [
    source 38
    target 2065
  ]
  edge [
    source 38
    target 2066
  ]
  edge [
    source 38
    target 2067
  ]
  edge [
    source 38
    target 2068
  ]
  edge [
    source 38
    target 2069
  ]
  edge [
    source 38
    target 2070
  ]
  edge [
    source 38
    target 2071
  ]
  edge [
    source 38
    target 2072
  ]
  edge [
    source 38
    target 2073
  ]
  edge [
    source 38
    target 2074
  ]
  edge [
    source 39
    target 381
  ]
  edge [
    source 39
    target 2075
  ]
  edge [
    source 39
    target 2076
  ]
  edge [
    source 39
    target 2077
  ]
  edge [
    source 39
    target 1359
  ]
  edge [
    source 39
    target 2078
  ]
  edge [
    source 39
    target 660
  ]
  edge [
    source 39
    target 2079
  ]
  edge [
    source 39
    target 2080
  ]
  edge [
    source 39
    target 90
  ]
  edge [
    source 39
    target 466
  ]
  edge [
    source 39
    target 846
  ]
  edge [
    source 39
    target 2081
  ]
  edge [
    source 39
    target 2082
  ]
  edge [
    source 39
    target 1153
  ]
  edge [
    source 39
    target 245
  ]
  edge [
    source 39
    target 2083
  ]
  edge [
    source 39
    target 853
  ]
  edge [
    source 39
    target 2084
  ]
  edge [
    source 39
    target 2085
  ]
  edge [
    source 39
    target 2086
  ]
  edge [
    source 39
    target 2087
  ]
  edge [
    source 39
    target 2088
  ]
  edge [
    source 39
    target 1986
  ]
  edge [
    source 39
    target 2089
  ]
  edge [
    source 39
    target 2090
  ]
  edge [
    source 39
    target 2091
  ]
  edge [
    source 39
    target 2092
  ]
  edge [
    source 39
    target 2093
  ]
  edge [
    source 39
    target 1732
  ]
  edge [
    source 39
    target 2094
  ]
  edge [
    source 39
    target 2095
  ]
  edge [
    source 39
    target 236
  ]
  edge [
    source 39
    target 2096
  ]
  edge [
    source 39
    target 2097
  ]
  edge [
    source 39
    target 2098
  ]
  edge [
    source 39
    target 2099
  ]
  edge [
    source 39
    target 687
  ]
  edge [
    source 39
    target 2100
  ]
  edge [
    source 39
    target 2101
  ]
  edge [
    source 39
    target 2102
  ]
  edge [
    source 39
    target 2103
  ]
  edge [
    source 39
    target 2104
  ]
  edge [
    source 39
    target 2105
  ]
  edge [
    source 39
    target 2106
  ]
  edge [
    source 39
    target 2107
  ]
  edge [
    source 39
    target 2108
  ]
  edge [
    source 39
    target 2109
  ]
  edge [
    source 39
    target 2110
  ]
  edge [
    source 39
    target 2111
  ]
  edge [
    source 39
    target 2112
  ]
  edge [
    source 39
    target 2113
  ]
  edge [
    source 39
    target 2114
  ]
  edge [
    source 39
    target 2115
  ]
  edge [
    source 39
    target 2116
  ]
  edge [
    source 39
    target 2117
  ]
  edge [
    source 39
    target 2118
  ]
  edge [
    source 39
    target 2119
  ]
  edge [
    source 39
    target 2120
  ]
  edge [
    source 39
    target 2121
  ]
  edge [
    source 39
    target 2122
  ]
  edge [
    source 39
    target 2123
  ]
  edge [
    source 39
    target 2124
  ]
  edge [
    source 39
    target 2125
  ]
  edge [
    source 39
    target 2126
  ]
  edge [
    source 39
    target 2127
  ]
  edge [
    source 39
    target 2128
  ]
  edge [
    source 39
    target 2129
  ]
  edge [
    source 39
    target 2130
  ]
  edge [
    source 39
    target 2131
  ]
  edge [
    source 39
    target 1862
  ]
  edge [
    source 40
    target 2132
  ]
  edge [
    source 40
    target 2133
  ]
  edge [
    source 40
    target 1599
  ]
  edge [
    source 40
    target 2134
  ]
  edge [
    source 40
    target 2135
  ]
  edge [
    source 40
    target 2136
  ]
  edge [
    source 40
    target 2137
  ]
  edge [
    source 40
    target 2138
  ]
  edge [
    source 40
    target 2139
  ]
  edge [
    source 40
    target 2140
  ]
  edge [
    source 40
    target 1558
  ]
  edge [
    source 40
    target 1937
  ]
  edge [
    source 40
    target 2141
  ]
  edge [
    source 40
    target 2142
  ]
  edge [
    source 40
    target 254
  ]
  edge [
    source 40
    target 72
  ]
  edge [
    source 40
    target 2143
  ]
  edge [
    source 40
    target 2144
  ]
  edge [
    source 40
    target 2145
  ]
  edge [
    source 40
    target 2146
  ]
  edge [
    source 40
    target 2147
  ]
  edge [
    source 40
    target 2148
  ]
  edge [
    source 40
    target 2149
  ]
  edge [
    source 40
    target 2150
  ]
  edge [
    source 40
    target 245
  ]
  edge [
    source 40
    target 2151
  ]
  edge [
    source 40
    target 2152
  ]
  edge [
    source 40
    target 2153
  ]
  edge [
    source 40
    target 2154
  ]
  edge [
    source 40
    target 2155
  ]
  edge [
    source 40
    target 2156
  ]
  edge [
    source 40
    target 2157
  ]
  edge [
    source 40
    target 2158
  ]
  edge [
    source 40
    target 2159
  ]
  edge [
    source 40
    target 2160
  ]
  edge [
    source 40
    target 2161
  ]
  edge [
    source 40
    target 2162
  ]
  edge [
    source 40
    target 2163
  ]
  edge [
    source 40
    target 2164
  ]
  edge [
    source 40
    target 2165
  ]
  edge [
    source 40
    target 2166
  ]
  edge [
    source 40
    target 2167
  ]
  edge [
    source 40
    target 2168
  ]
  edge [
    source 40
    target 2169
  ]
  edge [
    source 40
    target 2170
  ]
  edge [
    source 40
    target 519
  ]
  edge [
    source 40
    target 727
  ]
  edge [
    source 40
    target 2171
  ]
  edge [
    source 40
    target 1601
  ]
  edge [
    source 40
    target 2172
  ]
  edge [
    source 40
    target 2173
  ]
  edge [
    source 40
    target 1767
  ]
  edge [
    source 40
    target 2174
  ]
  edge [
    source 40
    target 2175
  ]
  edge [
    source 40
    target 1544
  ]
  edge [
    source 40
    target 101
  ]
  edge [
    source 40
    target 606
  ]
  edge [
    source 40
    target 1222
  ]
  edge [
    source 40
    target 1215
  ]
  edge [
    source 40
    target 243
  ]
  edge [
    source 40
    target 1546
  ]
  edge [
    source 40
    target 1545
  ]
  edge [
    source 40
    target 70
  ]
  edge [
    source 40
    target 244
  ]
  edge [
    source 40
    target 1547
  ]
  edge [
    source 40
    target 698
  ]
  edge [
    source 40
    target 1548
  ]
  edge [
    source 40
    target 381
  ]
  edge [
    source 40
    target 2176
  ]
  edge [
    source 40
    target 2177
  ]
  edge [
    source 40
    target 2178
  ]
  edge [
    source 40
    target 2179
  ]
  edge [
    source 40
    target 2180
  ]
  edge [
    source 40
    target 1409
  ]
  edge [
    source 40
    target 2181
  ]
  edge [
    source 40
    target 2182
  ]
  edge [
    source 40
    target 2183
  ]
  edge [
    source 40
    target 2184
  ]
  edge [
    source 40
    target 2185
  ]
  edge [
    source 40
    target 2186
  ]
  edge [
    source 40
    target 2187
  ]
  edge [
    source 40
    target 2188
  ]
  edge [
    source 40
    target 2189
  ]
  edge [
    source 40
    target 2190
  ]
  edge [
    source 40
    target 2191
  ]
  edge [
    source 40
    target 2192
  ]
  edge [
    source 40
    target 2193
  ]
  edge [
    source 40
    target 517
  ]
  edge [
    source 40
    target 2194
  ]
  edge [
    source 40
    target 1279
  ]
  edge [
    source 40
    target 2195
  ]
  edge [
    source 40
    target 2196
  ]
  edge [
    source 40
    target 2197
  ]
  edge [
    source 40
    target 2198
  ]
  edge [
    source 40
    target 2199
  ]
  edge [
    source 40
    target 2200
  ]
  edge [
    source 40
    target 2201
  ]
  edge [
    source 40
    target 2202
  ]
  edge [
    source 40
    target 485
  ]
  edge [
    source 40
    target 486
  ]
  edge [
    source 40
    target 287
  ]
  edge [
    source 40
    target 2203
  ]
  edge [
    source 40
    target 1867
  ]
  edge [
    source 40
    target 487
  ]
  edge [
    source 40
    target 2204
  ]
  edge [
    source 40
    target 189
  ]
  edge [
    source 40
    target 2205
  ]
  edge [
    source 40
    target 2206
  ]
  edge [
    source 40
    target 2207
  ]
  edge [
    source 40
    target 1942
  ]
  edge [
    source 40
    target 1402
  ]
  edge [
    source 40
    target 1943
  ]
  edge [
    source 40
    target 1944
  ]
  edge [
    source 40
    target 1945
  ]
  edge [
    source 40
    target 2208
  ]
  edge [
    source 40
    target 326
  ]
  edge [
    source 40
    target 43
  ]
  edge [
    source 40
    target 2209
  ]
  edge [
    source 40
    target 2210
  ]
  edge [
    source 40
    target 801
  ]
  edge [
    source 40
    target 2211
  ]
  edge [
    source 40
    target 2212
  ]
  edge [
    source 40
    target 2213
  ]
  edge [
    source 40
    target 2214
  ]
  edge [
    source 40
    target 2215
  ]
  edge [
    source 40
    target 2216
  ]
  edge [
    source 40
    target 62
  ]
  edge [
    source 40
    target 985
  ]
  edge [
    source 40
    target 2217
  ]
  edge [
    source 40
    target 2218
  ]
  edge [
    source 40
    target 2219
  ]
  edge [
    source 40
    target 2220
  ]
  edge [
    source 40
    target 2221
  ]
  edge [
    source 40
    target 2222
  ]
  edge [
    source 40
    target 355
  ]
  edge [
    source 40
    target 2223
  ]
  edge [
    source 40
    target 357
  ]
  edge [
    source 40
    target 2224
  ]
  edge [
    source 40
    target 2225
  ]
  edge [
    source 40
    target 1131
  ]
  edge [
    source 40
    target 2226
  ]
  edge [
    source 40
    target 529
  ]
  edge [
    source 40
    target 2227
  ]
  edge [
    source 40
    target 2228
  ]
  edge [
    source 40
    target 2229
  ]
  edge [
    source 40
    target 2230
  ]
  edge [
    source 40
    target 2231
  ]
  edge [
    source 40
    target 1234
  ]
  edge [
    source 40
    target 2232
  ]
  edge [
    source 40
    target 2233
  ]
  edge [
    source 40
    target 2234
  ]
  edge [
    source 40
    target 526
  ]
  edge [
    source 40
    target 2235
  ]
  edge [
    source 40
    target 2236
  ]
  edge [
    source 40
    target 476
  ]
  edge [
    source 40
    target 2237
  ]
  edge [
    source 40
    target 2238
  ]
  edge [
    source 40
    target 2239
  ]
  edge [
    source 40
    target 2240
  ]
  edge [
    source 40
    target 2241
  ]
  edge [
    source 40
    target 2242
  ]
  edge [
    source 40
    target 2243
  ]
  edge [
    source 40
    target 2244
  ]
  edge [
    source 40
    target 545
  ]
  edge [
    source 40
    target 2245
  ]
  edge [
    source 40
    target 2246
  ]
  edge [
    source 40
    target 2247
  ]
  edge [
    source 40
    target 2248
  ]
  edge [
    source 40
    target 2249
  ]
  edge [
    source 40
    target 1567
  ]
  edge [
    source 40
    target 2250
  ]
  edge [
    source 40
    target 2251
  ]
  edge [
    source 40
    target 2252
  ]
  edge [
    source 40
    target 2253
  ]
  edge [
    source 40
    target 2254
  ]
  edge [
    source 40
    target 2255
  ]
  edge [
    source 40
    target 2256
  ]
  edge [
    source 40
    target 2257
  ]
  edge [
    source 40
    target 2258
  ]
  edge [
    source 40
    target 1572
  ]
  edge [
    source 40
    target 2259
  ]
  edge [
    source 40
    target 2260
  ]
  edge [
    source 40
    target 1208
  ]
  edge [
    source 40
    target 878
  ]
  edge [
    source 40
    target 2261
  ]
  edge [
    source 40
    target 876
  ]
  edge [
    source 40
    target 2262
  ]
  edge [
    source 40
    target 2263
  ]
  edge [
    source 40
    target 1576
  ]
  edge [
    source 40
    target 2264
  ]
  edge [
    source 40
    target 1577
  ]
  edge [
    source 40
    target 542
  ]
  edge [
    source 40
    target 2265
  ]
  edge [
    source 40
    target 2266
  ]
  edge [
    source 40
    target 2267
  ]
  edge [
    source 40
    target 2268
  ]
  edge [
    source 40
    target 2269
  ]
  edge [
    source 40
    target 2270
  ]
  edge [
    source 40
    target 2271
  ]
  edge [
    source 40
    target 2272
  ]
  edge [
    source 40
    target 1278
  ]
  edge [
    source 40
    target 2273
  ]
  edge [
    source 40
    target 80
  ]
  edge [
    source 40
    target 2274
  ]
  edge [
    source 40
    target 2275
  ]
  edge [
    source 40
    target 2276
  ]
  edge [
    source 40
    target 2277
  ]
  edge [
    source 40
    target 2278
  ]
  edge [
    source 40
    target 2279
  ]
  edge [
    source 40
    target 2280
  ]
  edge [
    source 40
    target 2281
  ]
  edge [
    source 40
    target 2282
  ]
  edge [
    source 40
    target 2283
  ]
  edge [
    source 40
    target 2284
  ]
  edge [
    source 40
    target 2285
  ]
  edge [
    source 40
    target 2286
  ]
  edge [
    source 40
    target 2287
  ]
  edge [
    source 40
    target 2288
  ]
  edge [
    source 40
    target 2053
  ]
  edge [
    source 40
    target 2289
  ]
  edge [
    source 40
    target 2290
  ]
  edge [
    source 40
    target 1672
  ]
  edge [
    source 40
    target 2291
  ]
  edge [
    source 40
    target 2292
  ]
  edge [
    source 40
    target 2293
  ]
  edge [
    source 40
    target 2294
  ]
  edge [
    source 40
    target 2295
  ]
  edge [
    source 40
    target 2296
  ]
  edge [
    source 40
    target 2297
  ]
  edge [
    source 40
    target 2298
  ]
  edge [
    source 40
    target 2299
  ]
  edge [
    source 40
    target 2300
  ]
  edge [
    source 40
    target 2301
  ]
  edge [
    source 40
    target 2302
  ]
  edge [
    source 40
    target 348
  ]
  edge [
    source 40
    target 2303
  ]
  edge [
    source 40
    target 306
  ]
  edge [
    source 40
    target 2304
  ]
  edge [
    source 40
    target 2305
  ]
  edge [
    source 40
    target 2306
  ]
  edge [
    source 40
    target 283
  ]
  edge [
    source 40
    target 1758
  ]
  edge [
    source 40
    target 2307
  ]
  edge [
    source 40
    target 2308
  ]
  edge [
    source 40
    target 236
  ]
  edge [
    source 40
    target 2309
  ]
  edge [
    source 40
    target 2310
  ]
  edge [
    source 40
    target 2311
  ]
  edge [
    source 40
    target 2312
  ]
  edge [
    source 40
    target 490
  ]
  edge [
    source 40
    target 2313
  ]
  edge [
    source 40
    target 2314
  ]
  edge [
    source 40
    target 2315
  ]
  edge [
    source 40
    target 2316
  ]
  edge [
    source 40
    target 2317
  ]
  edge [
    source 40
    target 2318
  ]
  edge [
    source 40
    target 2319
  ]
  edge [
    source 40
    target 2320
  ]
  edge [
    source 40
    target 2321
  ]
  edge [
    source 40
    target 2322
  ]
  edge [
    source 40
    target 2323
  ]
  edge [
    source 40
    target 2324
  ]
  edge [
    source 40
    target 2325
  ]
  edge [
    source 40
    target 1363
  ]
  edge [
    source 40
    target 2326
  ]
  edge [
    source 40
    target 2327
  ]
  edge [
    source 40
    target 2328
  ]
  edge [
    source 40
    target 2329
  ]
  edge [
    source 40
    target 2330
  ]
  edge [
    source 40
    target 2331
  ]
  edge [
    source 40
    target 658
  ]
  edge [
    source 40
    target 2332
  ]
  edge [
    source 40
    target 2333
  ]
  edge [
    source 40
    target 2334
  ]
  edge [
    source 40
    target 695
  ]
  edge [
    source 40
    target 2335
  ]
  edge [
    source 40
    target 2336
  ]
  edge [
    source 40
    target 2337
  ]
  edge [
    source 40
    target 2338
  ]
  edge [
    source 40
    target 2339
  ]
  edge [
    source 40
    target 2340
  ]
  edge [
    source 40
    target 1207
  ]
  edge [
    source 40
    target 2341
  ]
  edge [
    source 40
    target 2342
  ]
  edge [
    source 40
    target 1041
  ]
  edge [
    source 40
    target 2343
  ]
  edge [
    source 40
    target 2344
  ]
  edge [
    source 40
    target 1971
  ]
  edge [
    source 40
    target 1585
  ]
  edge [
    source 40
    target 2345
  ]
  edge [
    source 40
    target 2346
  ]
  edge [
    source 40
    target 2347
  ]
  edge [
    source 40
    target 2348
  ]
  edge [
    source 40
    target 2349
  ]
  edge [
    source 40
    target 2350
  ]
  edge [
    source 40
    target 637
  ]
  edge [
    source 40
    target 2351
  ]
  edge [
    source 40
    target 2352
  ]
  edge [
    source 40
    target 643
  ]
  edge [
    source 40
    target 2353
  ]
  edge [
    source 40
    target 2354
  ]
  edge [
    source 40
    target 2355
  ]
  edge [
    source 40
    target 2356
  ]
  edge [
    source 40
    target 2357
  ]
  edge [
    source 40
    target 2358
  ]
  edge [
    source 40
    target 2359
  ]
  edge [
    source 40
    target 2360
  ]
  edge [
    source 40
    target 2361
  ]
  edge [
    source 40
    target 2362
  ]
  edge [
    source 40
    target 2363
  ]
  edge [
    source 40
    target 2364
  ]
  edge [
    source 40
    target 2365
  ]
  edge [
    source 40
    target 2366
  ]
  edge [
    source 40
    target 2067
  ]
  edge [
    source 41
    target 1525
  ]
  edge [
    source 41
    target 1519
  ]
  edge [
    source 41
    target 1327
  ]
  edge [
    source 41
    target 1520
  ]
  edge [
    source 41
    target 254
  ]
  edge [
    source 41
    target 1528
  ]
  edge [
    source 41
    target 245
  ]
  edge [
    source 41
    target 1523
  ]
  edge [
    source 41
    target 1530
  ]
  edge [
    source 41
    target 1544
  ]
  edge [
    source 41
    target 244
  ]
  edge [
    source 41
    target 101
  ]
  edge [
    source 41
    target 606
  ]
  edge [
    source 41
    target 1222
  ]
  edge [
    source 41
    target 1215
  ]
  edge [
    source 41
    target 243
  ]
  edge [
    source 41
    target 1545
  ]
  edge [
    source 41
    target 70
  ]
  edge [
    source 41
    target 1546
  ]
  edge [
    source 41
    target 1547
  ]
  edge [
    source 41
    target 698
  ]
  edge [
    source 41
    target 1548
  ]
  edge [
    source 41
    target 381
  ]
  edge [
    source 41
    target 1271
  ]
  edge [
    source 41
    target 1552
  ]
  edge [
    source 41
    target 1553
  ]
  edge [
    source 41
    target 1554
  ]
  edge [
    source 41
    target 1555
  ]
  edge [
    source 41
    target 1556
  ]
  edge [
    source 41
    target 1557
  ]
  edge [
    source 41
    target 1558
  ]
  edge [
    source 41
    target 1559
  ]
  edge [
    source 41
    target 1560
  ]
  edge [
    source 41
    target 384
  ]
  edge [
    source 41
    target 1561
  ]
]
