graph [
  node [
    id 0
    label "ostrze&#380;enie"
    origin "text"
  ]
  node [
    id 1
    label "sklep"
    origin "text"
  ]
  node [
    id 2
    label "internetowy"
    origin "text"
  ]
  node [
    id 3
    label "morel"
    origin "text"
  ]
  node [
    id 4
    label "oszukiwa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "klient"
    origin "text"
  ]
  node [
    id 6
    label "sprzedawa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "telefon"
    origin "text"
  ]
  node [
    id 8
    label "samsung"
    origin "text"
  ]
  node [
    id 9
    label "zagraniczny"
    origin "text"
  ]
  node [
    id 10
    label "dystrybucja"
    origin "text"
  ]
  node [
    id 11
    label "spe&#322;nia&#263;"
    origin "text"
  ]
  node [
    id 12
    label "warunek"
    origin "text"
  ]
  node [
    id 13
    label "promocja"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "reklamowa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "oficjalny"
    origin "text"
  ]
  node [
    id 17
    label "partner"
    origin "text"
  ]
  node [
    id 18
    label "firma"
    origin "text"
  ]
  node [
    id 19
    label "uwaga"
  ]
  node [
    id 20
    label "admonition"
  ]
  node [
    id 21
    label "uprzedzenie"
  ]
  node [
    id 22
    label "niech&#281;&#263;"
  ]
  node [
    id 23
    label "anticipation"
  ]
  node [
    id 24
    label "poinformowanie"
  ]
  node [
    id 25
    label "bias"
  ]
  node [
    id 26
    label "przygotowanie"
  ]
  node [
    id 27
    label "progress"
  ]
  node [
    id 28
    label "zrobienie"
  ]
  node [
    id 29
    label "og&#322;oszenie"
  ]
  node [
    id 30
    label "stan"
  ]
  node [
    id 31
    label "tekst"
  ]
  node [
    id 32
    label "wzgl&#261;d"
  ]
  node [
    id 33
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 34
    label "nagana"
  ]
  node [
    id 35
    label "upomnienie"
  ]
  node [
    id 36
    label "wypowied&#378;"
  ]
  node [
    id 37
    label "gossip"
  ]
  node [
    id 38
    label "dzienniczek"
  ]
  node [
    id 39
    label "obiekt_handlowy"
  ]
  node [
    id 40
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 41
    label "zaplecze"
  ]
  node [
    id 42
    label "p&#243;&#322;ka"
  ]
  node [
    id 43
    label "stoisko"
  ]
  node [
    id 44
    label "witryna"
  ]
  node [
    id 45
    label "sk&#322;ad"
  ]
  node [
    id 46
    label "MAN_SE"
  ]
  node [
    id 47
    label "Spo&#322;em"
  ]
  node [
    id 48
    label "Orbis"
  ]
  node [
    id 49
    label "HP"
  ]
  node [
    id 50
    label "Canon"
  ]
  node [
    id 51
    label "nazwa_w&#322;asna"
  ]
  node [
    id 52
    label "zasoby"
  ]
  node [
    id 53
    label "zasoby_ludzkie"
  ]
  node [
    id 54
    label "klasa"
  ]
  node [
    id 55
    label "Baltona"
  ]
  node [
    id 56
    label "zaufanie"
  ]
  node [
    id 57
    label "paczkarnia"
  ]
  node [
    id 58
    label "reengineering"
  ]
  node [
    id 59
    label "cz&#322;owiek"
  ]
  node [
    id 60
    label "siedziba"
  ]
  node [
    id 61
    label "Orlen"
  ]
  node [
    id 62
    label "miejsce_pracy"
  ]
  node [
    id 63
    label "Pewex"
  ]
  node [
    id 64
    label "biurowiec"
  ]
  node [
    id 65
    label "Apeks"
  ]
  node [
    id 66
    label "MAC"
  ]
  node [
    id 67
    label "networking"
  ]
  node [
    id 68
    label "podmiot_gospodarczy"
  ]
  node [
    id 69
    label "Google"
  ]
  node [
    id 70
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 71
    label "Hortex"
  ]
  node [
    id 72
    label "interes"
  ]
  node [
    id 73
    label "YouTube"
  ]
  node [
    id 74
    label "szyba"
  ]
  node [
    id 75
    label "gablota"
  ]
  node [
    id 76
    label "wytw&#243;r"
  ]
  node [
    id 77
    label "strona"
  ]
  node [
    id 78
    label "okno"
  ]
  node [
    id 79
    label "meblo&#347;cianka"
  ]
  node [
    id 80
    label "mebel"
  ]
  node [
    id 81
    label "stela&#380;"
  ]
  node [
    id 82
    label "szafa"
  ]
  node [
    id 83
    label "wyposa&#380;enie"
  ]
  node [
    id 84
    label "pomieszczenie"
  ]
  node [
    id 85
    label "infrastruktura"
  ]
  node [
    id 86
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 87
    label "fabryka"
  ]
  node [
    id 88
    label "pole"
  ]
  node [
    id 89
    label "pas"
  ]
  node [
    id 90
    label "blokada"
  ]
  node [
    id 91
    label "miejsce"
  ]
  node [
    id 92
    label "tabulacja"
  ]
  node [
    id 93
    label "hurtownia"
  ]
  node [
    id 94
    label "basic"
  ]
  node [
    id 95
    label "obr&#243;bka"
  ]
  node [
    id 96
    label "rank_and_file"
  ]
  node [
    id 97
    label "syf"
  ]
  node [
    id 98
    label "struktura"
  ]
  node [
    id 99
    label "sk&#322;adnik"
  ]
  node [
    id 100
    label "constitution"
  ]
  node [
    id 101
    label "set"
  ]
  node [
    id 102
    label "zesp&#243;&#322;"
  ]
  node [
    id 103
    label "&#347;wiat&#322;o"
  ]
  node [
    id 104
    label "nowoczesny"
  ]
  node [
    id 105
    label "internetowo"
  ]
  node [
    id 106
    label "elektroniczny"
  ]
  node [
    id 107
    label "sieciowo"
  ]
  node [
    id 108
    label "netowy"
  ]
  node [
    id 109
    label "siatkowy"
  ]
  node [
    id 110
    label "elektronicznie"
  ]
  node [
    id 111
    label "sieciowy"
  ]
  node [
    id 112
    label "nowo&#380;ytny"
  ]
  node [
    id 113
    label "nowy"
  ]
  node [
    id 114
    label "nowocze&#347;nie"
  ]
  node [
    id 115
    label "otwarty"
  ]
  node [
    id 116
    label "elektrycznie"
  ]
  node [
    id 117
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 118
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 119
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 120
    label "oszwabia&#263;"
  ]
  node [
    id 121
    label "orzyna&#263;"
  ]
  node [
    id 122
    label "cheat"
  ]
  node [
    id 123
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 124
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 125
    label "wyprzedza&#263;"
  ]
  node [
    id 126
    label "trenowa&#263;"
  ]
  node [
    id 127
    label "evade"
  ]
  node [
    id 128
    label "zje&#380;d&#380;a&#263;"
  ]
  node [
    id 129
    label "osacza&#263;"
  ]
  node [
    id 130
    label "omija&#263;"
  ]
  node [
    id 131
    label "zapewnia&#263;"
  ]
  node [
    id 132
    label "opieprza&#263;"
  ]
  node [
    id 133
    label "podr&#243;&#380;owa&#263;"
  ]
  node [
    id 134
    label "przeje&#380;d&#380;a&#263;"
  ]
  node [
    id 135
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 136
    label "wywodzi&#263;"
  ]
  node [
    id 137
    label "przesuwa&#263;"
  ]
  node [
    id 138
    label "ogrywa&#263;"
  ]
  node [
    id 139
    label "wk&#322;ada&#263;"
  ]
  node [
    id 140
    label "zag&#322;&#281;bia&#263;"
  ]
  node [
    id 141
    label "umieszcza&#263;"
  ]
  node [
    id 142
    label "za&#322;atwia&#263;"
  ]
  node [
    id 143
    label "obywatel"
  ]
  node [
    id 144
    label "us&#322;ugobiorca"
  ]
  node [
    id 145
    label "szlachcic"
  ]
  node [
    id 146
    label "agent_rozliczeniowy"
  ]
  node [
    id 147
    label "program"
  ]
  node [
    id 148
    label "bratek"
  ]
  node [
    id 149
    label "klientela"
  ]
  node [
    id 150
    label "Rzymianin"
  ]
  node [
    id 151
    label "komputer_cyfrowy"
  ]
  node [
    id 152
    label "szlachciura"
  ]
  node [
    id 153
    label "szlachta"
  ]
  node [
    id 154
    label "przedstawiciel"
  ]
  node [
    id 155
    label "notabl"
  ]
  node [
    id 156
    label "pa&#324;stwo"
  ]
  node [
    id 157
    label "mieszkaniec"
  ]
  node [
    id 158
    label "miastowy"
  ]
  node [
    id 159
    label "Horacy"
  ]
  node [
    id 160
    label "Cyceron"
  ]
  node [
    id 161
    label "W&#322;och"
  ]
  node [
    id 162
    label "asymilowa&#263;"
  ]
  node [
    id 163
    label "nasada"
  ]
  node [
    id 164
    label "profanum"
  ]
  node [
    id 165
    label "wz&#243;r"
  ]
  node [
    id 166
    label "senior"
  ]
  node [
    id 167
    label "asymilowanie"
  ]
  node [
    id 168
    label "os&#322;abia&#263;"
  ]
  node [
    id 169
    label "homo_sapiens"
  ]
  node [
    id 170
    label "osoba"
  ]
  node [
    id 171
    label "ludzko&#347;&#263;"
  ]
  node [
    id 172
    label "Adam"
  ]
  node [
    id 173
    label "hominid"
  ]
  node [
    id 174
    label "posta&#263;"
  ]
  node [
    id 175
    label "portrecista"
  ]
  node [
    id 176
    label "polifag"
  ]
  node [
    id 177
    label "podw&#322;adny"
  ]
  node [
    id 178
    label "dwun&#243;g"
  ]
  node [
    id 179
    label "wapniak"
  ]
  node [
    id 180
    label "duch"
  ]
  node [
    id 181
    label "os&#322;abianie"
  ]
  node [
    id 182
    label "antropochoria"
  ]
  node [
    id 183
    label "figura"
  ]
  node [
    id 184
    label "g&#322;owa"
  ]
  node [
    id 185
    label "mikrokosmos"
  ]
  node [
    id 186
    label "oddzia&#322;ywanie"
  ]
  node [
    id 187
    label "podmiot"
  ]
  node [
    id 188
    label "za&#322;o&#380;enie"
  ]
  node [
    id 189
    label "dzia&#322;"
  ]
  node [
    id 190
    label "odinstalowa&#263;"
  ]
  node [
    id 191
    label "spis"
  ]
  node [
    id 192
    label "broszura"
  ]
  node [
    id 193
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 194
    label "informatyka"
  ]
  node [
    id 195
    label "odinstalowywa&#263;"
  ]
  node [
    id 196
    label "furkacja"
  ]
  node [
    id 197
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 198
    label "ogranicznik_referencyjny"
  ]
  node [
    id 199
    label "oprogramowanie"
  ]
  node [
    id 200
    label "blok"
  ]
  node [
    id 201
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 202
    label "prezentowa&#263;"
  ]
  node [
    id 203
    label "emitowa&#263;"
  ]
  node [
    id 204
    label "kana&#322;"
  ]
  node [
    id 205
    label "sekcja_krytyczna"
  ]
  node [
    id 206
    label "pirat"
  ]
  node [
    id 207
    label "folder"
  ]
  node [
    id 208
    label "zaprezentowa&#263;"
  ]
  node [
    id 209
    label "course_of_study"
  ]
  node [
    id 210
    label "punkt"
  ]
  node [
    id 211
    label "zainstalowa&#263;"
  ]
  node [
    id 212
    label "emitowanie"
  ]
  node [
    id 213
    label "teleferie"
  ]
  node [
    id 214
    label "podstawa"
  ]
  node [
    id 215
    label "deklaracja"
  ]
  node [
    id 216
    label "instrukcja"
  ]
  node [
    id 217
    label "zainstalowanie"
  ]
  node [
    id 218
    label "zaprezentowanie"
  ]
  node [
    id 219
    label "instalowa&#263;"
  ]
  node [
    id 220
    label "oferta"
  ]
  node [
    id 221
    label "odinstalowanie"
  ]
  node [
    id 222
    label "odinstalowywanie"
  ]
  node [
    id 223
    label "ram&#243;wka"
  ]
  node [
    id 224
    label "tryb"
  ]
  node [
    id 225
    label "menu"
  ]
  node [
    id 226
    label "podprogram"
  ]
  node [
    id 227
    label "instalowanie"
  ]
  node [
    id 228
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 229
    label "booklet"
  ]
  node [
    id 230
    label "struktura_organizacyjna"
  ]
  node [
    id 231
    label "interfejs"
  ]
  node [
    id 232
    label "prezentowanie"
  ]
  node [
    id 233
    label "facet"
  ]
  node [
    id 234
    label "kochanek"
  ]
  node [
    id 235
    label "fio&#322;ek"
  ]
  node [
    id 236
    label "brat"
  ]
  node [
    id 237
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 238
    label "clientele"
  ]
  node [
    id 239
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 240
    label "handlowa&#263;"
  ]
  node [
    id 241
    label "sell"
  ]
  node [
    id 242
    label "op&#281;dza&#263;"
  ]
  node [
    id 243
    label "oferowa&#263;"
  ]
  node [
    id 244
    label "oddawa&#263;"
  ]
  node [
    id 245
    label "inspekcja_handlowa"
  ]
  node [
    id 246
    label "mienia&#263;"
  ]
  node [
    id 247
    label "odst&#281;powa&#263;"
  ]
  node [
    id 248
    label "wymienia&#263;"
  ]
  node [
    id 249
    label "rezygnowa&#263;"
  ]
  node [
    id 250
    label "uzyskiwa&#263;"
  ]
  node [
    id 251
    label "deal"
  ]
  node [
    id 252
    label "volunteer"
  ]
  node [
    id 253
    label "zach&#281;ca&#263;"
  ]
  node [
    id 254
    label "dostarcza&#263;"
  ]
  node [
    id 255
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 256
    label "odpowiada&#263;"
  ]
  node [
    id 257
    label "dawa&#263;"
  ]
  node [
    id 258
    label "give"
  ]
  node [
    id 259
    label "przekazywa&#263;"
  ]
  node [
    id 260
    label "przedstawia&#263;"
  ]
  node [
    id 261
    label "reflect"
  ]
  node [
    id 262
    label "blurt_out"
  ]
  node [
    id 263
    label "impart"
  ]
  node [
    id 264
    label "surrender"
  ]
  node [
    id 265
    label "sacrifice"
  ]
  node [
    id 266
    label "render"
  ]
  node [
    id 267
    label "deliver"
  ]
  node [
    id 268
    label "robi&#263;"
  ]
  node [
    id 269
    label "radzi&#263;_sobie"
  ]
  node [
    id 270
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 271
    label "mikrotelefon"
  ]
  node [
    id 272
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 273
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 274
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 275
    label "zadzwoni&#263;"
  ]
  node [
    id 276
    label "urz&#261;dzenie"
  ]
  node [
    id 277
    label "instalacja"
  ]
  node [
    id 278
    label "numer"
  ]
  node [
    id 279
    label "po&#322;&#261;czenie"
  ]
  node [
    id 280
    label "coalescence"
  ]
  node [
    id 281
    label "dzwonienie"
  ]
  node [
    id 282
    label "billing"
  ]
  node [
    id 283
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 284
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 285
    label "dzwoni&#263;"
  ]
  node [
    id 286
    label "phreaker"
  ]
  node [
    id 287
    label "wy&#347;wietlacz"
  ]
  node [
    id 288
    label "provider"
  ]
  node [
    id 289
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 290
    label "kontakt"
  ]
  node [
    id 291
    label "liczba"
  ]
  node [
    id 292
    label "manewr"
  ]
  node [
    id 293
    label "oznaczenie"
  ]
  node [
    id 294
    label "wyst&#281;p"
  ]
  node [
    id 295
    label "turn"
  ]
  node [
    id 296
    label "pok&#243;j"
  ]
  node [
    id 297
    label "akt_p&#322;ciowy"
  ]
  node [
    id 298
    label "&#380;art"
  ]
  node [
    id 299
    label "publikacja"
  ]
  node [
    id 300
    label "czasopismo"
  ]
  node [
    id 301
    label "orygina&#322;"
  ]
  node [
    id 302
    label "zi&#243;&#322;ko"
  ]
  node [
    id 303
    label "impression"
  ]
  node [
    id 304
    label "sztos"
  ]
  node [
    id 305
    label "hotel"
  ]
  node [
    id 306
    label "komora"
  ]
  node [
    id 307
    label "wyrz&#261;dzenie"
  ]
  node [
    id 308
    label "kom&#243;rka"
  ]
  node [
    id 309
    label "impulsator"
  ]
  node [
    id 310
    label "furnishing"
  ]
  node [
    id 311
    label "zabezpieczenie"
  ]
  node [
    id 312
    label "sprz&#281;t"
  ]
  node [
    id 313
    label "aparatura"
  ]
  node [
    id 314
    label "ig&#322;a"
  ]
  node [
    id 315
    label "wirnik"
  ]
  node [
    id 316
    label "przedmiot"
  ]
  node [
    id 317
    label "zablokowanie"
  ]
  node [
    id 318
    label "blokowanie"
  ]
  node [
    id 319
    label "j&#281;zyk"
  ]
  node [
    id 320
    label "czynno&#347;&#263;"
  ]
  node [
    id 321
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 322
    label "system_energetyczny"
  ]
  node [
    id 323
    label "narz&#281;dzie"
  ]
  node [
    id 324
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 325
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 326
    label "zagospodarowanie"
  ]
  node [
    id 327
    label "mechanizm"
  ]
  node [
    id 328
    label "styk"
  ]
  node [
    id 329
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 330
    label "&#322;&#261;cznik"
  ]
  node [
    id 331
    label "soczewka"
  ]
  node [
    id 332
    label "association"
  ]
  node [
    id 333
    label "wydarzenie"
  ]
  node [
    id 334
    label "zwi&#261;zek"
  ]
  node [
    id 335
    label "z&#322;&#261;czenie"
  ]
  node [
    id 336
    label "socket"
  ]
  node [
    id 337
    label "regulator"
  ]
  node [
    id 338
    label "&#322;&#261;cznik_mechanizmowy"
  ]
  node [
    id 339
    label "communication"
  ]
  node [
    id 340
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 341
    label "instalacja_elektryczna"
  ]
  node [
    id 342
    label "linkage"
  ]
  node [
    id 343
    label "formacja_geologiczna"
  ]
  node [
    id 344
    label "contact"
  ]
  node [
    id 345
    label "katalizator"
  ]
  node [
    id 346
    label "kompozycja"
  ]
  node [
    id 347
    label "uzbrajanie"
  ]
  node [
    id 348
    label "proces"
  ]
  node [
    id 349
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 350
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 351
    label "ekran"
  ]
  node [
    id 352
    label "handset"
  ]
  node [
    id 353
    label "brzmienie"
  ]
  node [
    id 354
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 355
    label "dodzwonienie_si&#281;"
  ]
  node [
    id 356
    label "dryndanie"
  ]
  node [
    id 357
    label "wybijanie"
  ]
  node [
    id 358
    label "naciskanie"
  ]
  node [
    id 359
    label "jingle"
  ]
  node [
    id 360
    label "wydzwonienie"
  ]
  node [
    id 361
    label "wydzwanianie"
  ]
  node [
    id 362
    label "dzwonek"
  ]
  node [
    id 363
    label "sound"
  ]
  node [
    id 364
    label "dodzwanianie_si&#281;"
  ]
  node [
    id 365
    label "nacisn&#261;&#263;"
  ]
  node [
    id 366
    label "zadrynda&#263;"
  ]
  node [
    id 367
    label "call"
  ]
  node [
    id 368
    label "zabrz&#281;cze&#263;"
  ]
  node [
    id 369
    label "zabi&#263;"
  ]
  node [
    id 370
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 371
    label "zabrzmie&#263;"
  ]
  node [
    id 372
    label "brzmie&#263;"
  ]
  node [
    id 373
    label "brz&#281;cze&#263;"
  ]
  node [
    id 374
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 375
    label "bi&#263;"
  ]
  node [
    id 376
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 377
    label "drynda&#263;"
  ]
  node [
    id 378
    label "relate"
  ]
  node [
    id 379
    label "zrobi&#263;"
  ]
  node [
    id 380
    label "zjednoczy&#263;"
  ]
  node [
    id 381
    label "incorporate"
  ]
  node [
    id 382
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 383
    label "connect"
  ]
  node [
    id 384
    label "spowodowa&#263;"
  ]
  node [
    id 385
    label "stworzy&#263;"
  ]
  node [
    id 386
    label "z&#322;odziej"
  ]
  node [
    id 387
    label "paj&#281;czarz"
  ]
  node [
    id 388
    label "dissociation"
  ]
  node [
    id 389
    label "od&#322;&#261;czenie"
  ]
  node [
    id 390
    label "przerwanie"
  ]
  node [
    id 391
    label "severance"
  ]
  node [
    id 392
    label "rozdzielenie"
  ]
  node [
    id 393
    label "oddzielenie"
  ]
  node [
    id 394
    label "prze&#322;&#261;czenie"
  ]
  node [
    id 395
    label "biling"
  ]
  node [
    id 396
    label "rozdzieli&#263;"
  ]
  node [
    id 397
    label "amputate"
  ]
  node [
    id 398
    label "abstract"
  ]
  node [
    id 399
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 400
    label "przerwa&#263;"
  ]
  node [
    id 401
    label "detach"
  ]
  node [
    id 402
    label "oddzieli&#263;"
  ]
  node [
    id 403
    label "przerywanie"
  ]
  node [
    id 404
    label "prze&#322;&#261;czanie"
  ]
  node [
    id 405
    label "separation"
  ]
  node [
    id 406
    label "oddzielanie"
  ]
  node [
    id 407
    label "rozsuwanie"
  ]
  node [
    id 408
    label "od&#322;&#261;czanie"
  ]
  node [
    id 409
    label "rozdzielanie"
  ]
  node [
    id 410
    label "umo&#380;liwienie"
  ]
  node [
    id 411
    label "spowodowanie"
  ]
  node [
    id 412
    label "zestawienie"
  ]
  node [
    id 413
    label "stworzenie"
  ]
  node [
    id 414
    label "port"
  ]
  node [
    id 415
    label "mention"
  ]
  node [
    id 416
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 417
    label "zgrzeina"
  ]
  node [
    id 418
    label "element"
  ]
  node [
    id 419
    label "zjednoczenie"
  ]
  node [
    id 420
    label "zespolenie"
  ]
  node [
    id 421
    label "komunikacja"
  ]
  node [
    id 422
    label "pomy&#347;lenie"
  ]
  node [
    id 423
    label "joining"
  ]
  node [
    id 424
    label "rzucenie"
  ]
  node [
    id 425
    label "dressing"
  ]
  node [
    id 426
    label "zwi&#261;zany"
  ]
  node [
    id 427
    label "alliance"
  ]
  node [
    id 428
    label "przerywa&#263;"
  ]
  node [
    id 429
    label "part"
  ]
  node [
    id 430
    label "cover"
  ]
  node [
    id 431
    label "rozdziela&#263;"
  ]
  node [
    id 432
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 433
    label "oddziela&#263;"
  ]
  node [
    id 434
    label "gulf"
  ]
  node [
    id 435
    label "dostawca"
  ]
  node [
    id 436
    label "internet"
  ]
  node [
    id 437
    label "telefonia"
  ]
  node [
    id 438
    label "radiofonia"
  ]
  node [
    id 439
    label "trasa"
  ]
  node [
    id 440
    label "zagranicznie"
  ]
  node [
    id 441
    label "obcy"
  ]
  node [
    id 442
    label "istota_&#380;ywa"
  ]
  node [
    id 443
    label "cudzy"
  ]
  node [
    id 444
    label "obco"
  ]
  node [
    id 445
    label "pozaludzki"
  ]
  node [
    id 446
    label "zaziemsko"
  ]
  node [
    id 447
    label "inny"
  ]
  node [
    id 448
    label "nadprzyrodzony"
  ]
  node [
    id 449
    label "tameczny"
  ]
  node [
    id 450
    label "nieznajomo"
  ]
  node [
    id 451
    label "nieznany"
  ]
  node [
    id 452
    label "podzia&#322;"
  ]
  node [
    id 453
    label "distribution"
  ]
  node [
    id 454
    label "stopie&#324;"
  ]
  node [
    id 455
    label "competence"
  ]
  node [
    id 456
    label "fission"
  ]
  node [
    id 457
    label "blastogeneza"
  ]
  node [
    id 458
    label "eksdywizja"
  ]
  node [
    id 459
    label "close"
  ]
  node [
    id 460
    label "perform"
  ]
  node [
    id 461
    label "urzeczywistnia&#263;"
  ]
  node [
    id 462
    label "tentegowa&#263;"
  ]
  node [
    id 463
    label "urz&#261;dza&#263;"
  ]
  node [
    id 464
    label "praca"
  ]
  node [
    id 465
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 466
    label "czyni&#263;"
  ]
  node [
    id 467
    label "work"
  ]
  node [
    id 468
    label "przerabia&#263;"
  ]
  node [
    id 469
    label "act"
  ]
  node [
    id 470
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 471
    label "post&#281;powa&#263;"
  ]
  node [
    id 472
    label "peddle"
  ]
  node [
    id 473
    label "organizowa&#263;"
  ]
  node [
    id 474
    label "falowa&#263;"
  ]
  node [
    id 475
    label "stylizowa&#263;"
  ]
  node [
    id 476
    label "wydala&#263;"
  ]
  node [
    id 477
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 478
    label "ukazywa&#263;"
  ]
  node [
    id 479
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 480
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 481
    label "przeprowadza&#263;"
  ]
  node [
    id 482
    label "prawdzi&#263;"
  ]
  node [
    id 483
    label "prosecute"
  ]
  node [
    id 484
    label "faktor"
  ]
  node [
    id 485
    label "umowa"
  ]
  node [
    id 486
    label "ekspozycja"
  ]
  node [
    id 487
    label "condition"
  ]
  node [
    id 488
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 489
    label "agent"
  ]
  node [
    id 490
    label "infliction"
  ]
  node [
    id 491
    label "proposition"
  ]
  node [
    id 492
    label "pozak&#322;adanie"
  ]
  node [
    id 493
    label "point"
  ]
  node [
    id 494
    label "poubieranie"
  ]
  node [
    id 495
    label "rozebranie"
  ]
  node [
    id 496
    label "str&#243;j"
  ]
  node [
    id 497
    label "budowla"
  ]
  node [
    id 498
    label "przewidzenie"
  ]
  node [
    id 499
    label "zak&#322;adka"
  ]
  node [
    id 500
    label "twierdzenie"
  ]
  node [
    id 501
    label "przygotowywanie"
  ]
  node [
    id 502
    label "podwini&#281;cie"
  ]
  node [
    id 503
    label "zap&#322;acenie"
  ]
  node [
    id 504
    label "wyko&#324;czenie"
  ]
  node [
    id 505
    label "utworzenie"
  ]
  node [
    id 506
    label "przebranie"
  ]
  node [
    id 507
    label "obleczenie"
  ]
  node [
    id 508
    label "przymierzenie"
  ]
  node [
    id 509
    label "obleczenie_si&#281;"
  ]
  node [
    id 510
    label "przywdzianie"
  ]
  node [
    id 511
    label "umieszczenie"
  ]
  node [
    id 512
    label "przyodzianie"
  ]
  node [
    id 513
    label "pokrycie"
  ]
  node [
    id 514
    label "sytuacja"
  ]
  node [
    id 515
    label "warunki"
  ]
  node [
    id 516
    label "czyn"
  ]
  node [
    id 517
    label "zawarcie"
  ]
  node [
    id 518
    label "zawrze&#263;"
  ]
  node [
    id 519
    label "contract"
  ]
  node [
    id 520
    label "porozumienie"
  ]
  node [
    id 521
    label "gestia_transportowa"
  ]
  node [
    id 522
    label "klauzula"
  ]
  node [
    id 523
    label "programowanie_agentowe"
  ]
  node [
    id 524
    label "wojsko"
  ]
  node [
    id 525
    label "system_wieloagentowy"
  ]
  node [
    id 526
    label "kontrakt"
  ]
  node [
    id 527
    label "wywiad"
  ]
  node [
    id 528
    label "dzier&#380;awca"
  ]
  node [
    id 529
    label "rep"
  ]
  node [
    id 530
    label "&#347;ledziciel"
  ]
  node [
    id 531
    label "informator"
  ]
  node [
    id 532
    label "agentura"
  ]
  node [
    id 533
    label "funkcjonariusz"
  ]
  node [
    id 534
    label "detektyw"
  ]
  node [
    id 535
    label "po&#347;rednik"
  ]
  node [
    id 536
    label "czynnik"
  ]
  node [
    id 537
    label "filtr"
  ]
  node [
    id 538
    label "kustosz"
  ]
  node [
    id 539
    label "galeria"
  ]
  node [
    id 540
    label "fotografia"
  ]
  node [
    id 541
    label "spot"
  ]
  node [
    id 542
    label "wprowadzenie"
  ]
  node [
    id 543
    label "po&#322;o&#380;enie"
  ]
  node [
    id 544
    label "parametr"
  ]
  node [
    id 545
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 546
    label "scena"
  ]
  node [
    id 547
    label "muzeum"
  ]
  node [
    id 548
    label "impreza"
  ]
  node [
    id 549
    label "Arsena&#322;"
  ]
  node [
    id 550
    label "akcja"
  ]
  node [
    id 551
    label "wystawienie"
  ]
  node [
    id 552
    label "wst&#281;p"
  ]
  node [
    id 553
    label "kurator"
  ]
  node [
    id 554
    label "operacja"
  ]
  node [
    id 555
    label "strona_&#347;wiata"
  ]
  node [
    id 556
    label "kolekcja"
  ]
  node [
    id 557
    label "wernisa&#380;"
  ]
  node [
    id 558
    label "Agropromocja"
  ]
  node [
    id 559
    label "wystawa"
  ]
  node [
    id 560
    label "wspinaczka"
  ]
  node [
    id 561
    label "udzieli&#263;"
  ]
  node [
    id 562
    label "brief"
  ]
  node [
    id 563
    label "zamiana"
  ]
  node [
    id 564
    label "szachy"
  ]
  node [
    id 565
    label "warcaby"
  ]
  node [
    id 566
    label "sprzeda&#380;"
  ]
  node [
    id 567
    label "uzyska&#263;"
  ]
  node [
    id 568
    label "nominacja"
  ]
  node [
    id 569
    label "promotion"
  ]
  node [
    id 570
    label "promowa&#263;"
  ]
  node [
    id 571
    label "graduacja"
  ]
  node [
    id 572
    label "bran&#380;a"
  ]
  node [
    id 573
    label "gradation"
  ]
  node [
    id 574
    label "wypromowa&#263;"
  ]
  node [
    id 575
    label "informacja"
  ]
  node [
    id 576
    label "decyzja"
  ]
  node [
    id 577
    label "damka"
  ]
  node [
    id 578
    label "&#347;wiadectwo"
  ]
  node [
    id 579
    label "popularyzacja"
  ]
  node [
    id 580
    label "commencement"
  ]
  node [
    id 581
    label "okazja"
  ]
  node [
    id 582
    label "popularization"
  ]
  node [
    id 583
    label "zjawisko"
  ]
  node [
    id 584
    label "zdecydowanie"
  ]
  node [
    id 585
    label "management"
  ]
  node [
    id 586
    label "resolution"
  ]
  node [
    id 587
    label "dokument"
  ]
  node [
    id 588
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 589
    label "zatrudnienie"
  ]
  node [
    id 590
    label "dziedzina"
  ]
  node [
    id 591
    label "zmianka"
  ]
  node [
    id 592
    label "odmienianie"
  ]
  node [
    id 593
    label "exchange"
  ]
  node [
    id 594
    label "zmiana"
  ]
  node [
    id 595
    label "party"
  ]
  node [
    id 596
    label "impra"
  ]
  node [
    id 597
    label "rozrywka"
  ]
  node [
    id 598
    label "przyj&#281;cie"
  ]
  node [
    id 599
    label "przebieg"
  ]
  node [
    id 600
    label "zagrywka"
  ]
  node [
    id 601
    label "commotion"
  ]
  node [
    id 602
    label "udzia&#322;"
  ]
  node [
    id 603
    label "gra"
  ]
  node [
    id 604
    label "dywidenda"
  ]
  node [
    id 605
    label "w&#281;ze&#322;"
  ]
  node [
    id 606
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 607
    label "instrument_strunowy"
  ]
  node [
    id 608
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 609
    label "stock"
  ]
  node [
    id 610
    label "wysoko&#347;&#263;"
  ]
  node [
    id 611
    label "jazda"
  ]
  node [
    id 612
    label "occupation"
  ]
  node [
    id 613
    label "powzi&#281;cie"
  ]
  node [
    id 614
    label "obieganie"
  ]
  node [
    id 615
    label "sygna&#322;"
  ]
  node [
    id 616
    label "doj&#347;&#263;"
  ]
  node [
    id 617
    label "obiec"
  ]
  node [
    id 618
    label "wiedza"
  ]
  node [
    id 619
    label "powzi&#261;&#263;"
  ]
  node [
    id 620
    label "doj&#347;cie"
  ]
  node [
    id 621
    label "obiega&#263;"
  ]
  node [
    id 622
    label "obiegni&#281;cie"
  ]
  node [
    id 623
    label "dane"
  ]
  node [
    id 624
    label "okazka"
  ]
  node [
    id 625
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 626
    label "podw&#243;zka"
  ]
  node [
    id 627
    label "adeptness"
  ]
  node [
    id 628
    label "atrakcyjny"
  ]
  node [
    id 629
    label "autostop"
  ]
  node [
    id 630
    label "rabat"
  ]
  node [
    id 631
    label "transakcja"
  ]
  node [
    id 632
    label "przeniesienie_praw"
  ]
  node [
    id 633
    label "przeda&#380;"
  ]
  node [
    id 634
    label "sprzedaj&#261;cy"
  ]
  node [
    id 635
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 636
    label "wypromowywa&#263;"
  ]
  node [
    id 637
    label "nadawa&#263;"
  ]
  node [
    id 638
    label "udziela&#263;"
  ]
  node [
    id 639
    label "nada&#263;"
  ]
  node [
    id 640
    label "reklama"
  ]
  node [
    id 641
    label "advance"
  ]
  node [
    id 642
    label "doprowadza&#263;"
  ]
  node [
    id 643
    label "pomaga&#263;"
  ]
  node [
    id 644
    label "rozpowszechnia&#263;"
  ]
  node [
    id 645
    label "rozpowszechni&#263;"
  ]
  node [
    id 646
    label "doprowadzi&#263;"
  ]
  node [
    id 647
    label "pom&#243;c"
  ]
  node [
    id 648
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 649
    label "zach&#281;ci&#263;"
  ]
  node [
    id 650
    label "certificate"
  ]
  node [
    id 651
    label "o&#347;wiadczenie"
  ]
  node [
    id 652
    label "za&#347;wiadczenie"
  ]
  node [
    id 653
    label "dow&#243;d"
  ]
  node [
    id 654
    label "realize"
  ]
  node [
    id 655
    label "wytworzy&#263;"
  ]
  node [
    id 656
    label "give_birth"
  ]
  node [
    id 657
    label "make"
  ]
  node [
    id 658
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 659
    label "picture"
  ]
  node [
    id 660
    label "odst&#261;pi&#263;"
  ]
  node [
    id 661
    label "da&#263;"
  ]
  node [
    id 662
    label "przyzna&#263;"
  ]
  node [
    id 663
    label "udost&#281;pni&#263;"
  ]
  node [
    id 664
    label "zaleta"
  ]
  node [
    id 665
    label "rezerwa"
  ]
  node [
    id 666
    label "botanika"
  ]
  node [
    id 667
    label "jako&#347;&#263;"
  ]
  node [
    id 668
    label "type"
  ]
  node [
    id 669
    label "warstwa"
  ]
  node [
    id 670
    label "obiekt"
  ]
  node [
    id 671
    label "gromada"
  ]
  node [
    id 672
    label "atak"
  ]
  node [
    id 673
    label "mecz_mistrzowski"
  ]
  node [
    id 674
    label "class"
  ]
  node [
    id 675
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 676
    label "zbi&#243;r"
  ]
  node [
    id 677
    label "Ekwici"
  ]
  node [
    id 678
    label "sala"
  ]
  node [
    id 679
    label "jednostka_systematyczna"
  ]
  node [
    id 680
    label "wagon"
  ]
  node [
    id 681
    label "przepisa&#263;"
  ]
  node [
    id 682
    label "arrangement"
  ]
  node [
    id 683
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 684
    label "szko&#322;a"
  ]
  node [
    id 685
    label "programowanie_obiektowe"
  ]
  node [
    id 686
    label "&#347;rodowisko"
  ]
  node [
    id 687
    label "wykrzyknik"
  ]
  node [
    id 688
    label "obrona"
  ]
  node [
    id 689
    label "dziennik_lekcyjny"
  ]
  node [
    id 690
    label "typ"
  ]
  node [
    id 691
    label "znak_jako&#347;ci"
  ]
  node [
    id 692
    label "&#322;awka"
  ]
  node [
    id 693
    label "organizacja"
  ]
  node [
    id 694
    label "poziom"
  ]
  node [
    id 695
    label "grupa"
  ]
  node [
    id 696
    label "tablica"
  ]
  node [
    id 697
    label "przepisanie"
  ]
  node [
    id 698
    label "fakcja"
  ]
  node [
    id 699
    label "pomoc"
  ]
  node [
    id 700
    label "form"
  ]
  node [
    id 701
    label "kurs"
  ]
  node [
    id 702
    label "warcabnica"
  ]
  node [
    id 703
    label "gra_planszowa"
  ]
  node [
    id 704
    label "sport_umys&#322;owy"
  ]
  node [
    id 705
    label "rower"
  ]
  node [
    id 706
    label "pion"
  ]
  node [
    id 707
    label "linia_przemiany"
  ]
  node [
    id 708
    label "przes&#322;ona"
  ]
  node [
    id 709
    label "mat"
  ]
  node [
    id 710
    label "niedoczas"
  ]
  node [
    id 711
    label "szachownica"
  ]
  node [
    id 712
    label "szach"
  ]
  node [
    id 713
    label "bicie_w_przelocie"
  ]
  node [
    id 714
    label "dual"
  ]
  node [
    id 715
    label "p&#243;&#322;ruch"
  ]
  node [
    id 716
    label "roszada"
  ]
  node [
    id 717
    label "tempo"
  ]
  node [
    id 718
    label "zegar_szachowy"
  ]
  node [
    id 719
    label "wywy&#380;sza&#263;"
  ]
  node [
    id 720
    label "nagradza&#263;"
  ]
  node [
    id 721
    label "publicize"
  ]
  node [
    id 722
    label "zachwala&#263;"
  ]
  node [
    id 723
    label "glorify"
  ]
  node [
    id 724
    label "ask"
  ]
  node [
    id 725
    label "sk&#322;ada&#263;"
  ]
  node [
    id 726
    label "domaga&#263;_si&#281;"
  ]
  node [
    id 727
    label "chwali&#263;"
  ]
  node [
    id 728
    label "zbiera&#263;"
  ]
  node [
    id 729
    label "opracowywa&#263;"
  ]
  node [
    id 730
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 731
    label "uk&#322;ada&#263;"
  ]
  node [
    id 732
    label "convey"
  ]
  node [
    id 733
    label "zmienia&#263;"
  ]
  node [
    id 734
    label "dzieli&#263;"
  ]
  node [
    id 735
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 736
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 737
    label "zestaw"
  ]
  node [
    id 738
    label "przywraca&#263;"
  ]
  node [
    id 739
    label "scala&#263;"
  ]
  node [
    id 740
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 741
    label "train"
  ]
  node [
    id 742
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 743
    label "raise"
  ]
  node [
    id 744
    label "nadgradza&#263;"
  ]
  node [
    id 745
    label "reinforce"
  ]
  node [
    id 746
    label "formalizowanie"
  ]
  node [
    id 747
    label "legalny"
  ]
  node [
    id 748
    label "sformalizowanie"
  ]
  node [
    id 749
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 750
    label "jawny"
  ]
  node [
    id 751
    label "oficjalnie"
  ]
  node [
    id 752
    label "formalnie"
  ]
  node [
    id 753
    label "pow&#347;ci&#261;gliwie"
  ]
  node [
    id 754
    label "ujawnianie"
  ]
  node [
    id 755
    label "ujawnienie_si&#281;"
  ]
  node [
    id 756
    label "ujawnianie_si&#281;"
  ]
  node [
    id 757
    label "jawnie"
  ]
  node [
    id 758
    label "ewidentny"
  ]
  node [
    id 759
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 760
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 761
    label "zdecydowany"
  ]
  node [
    id 762
    label "znajomy"
  ]
  node [
    id 763
    label "ujawnienie"
  ]
  node [
    id 764
    label "gajny"
  ]
  node [
    id 765
    label "legalnie"
  ]
  node [
    id 766
    label "regularly"
  ]
  node [
    id 767
    label "formalny"
  ]
  node [
    id 768
    label "precyzowanie"
  ]
  node [
    id 769
    label "nadawanie"
  ]
  node [
    id 770
    label "nadanie"
  ]
  node [
    id 771
    label "sprecyzowanie"
  ]
  node [
    id 772
    label "pozornie"
  ]
  node [
    id 773
    label "kompletnie"
  ]
  node [
    id 774
    label "aktor"
  ]
  node [
    id 775
    label "sp&#243;lnik"
  ]
  node [
    id 776
    label "kolaborator"
  ]
  node [
    id 777
    label "prowadzi&#263;"
  ]
  node [
    id 778
    label "uczestniczenie"
  ]
  node [
    id 779
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 780
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 781
    label "pracownik"
  ]
  node [
    id 782
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 783
    label "przedsi&#281;biorca"
  ]
  node [
    id 784
    label "wykupywanie"
  ]
  node [
    id 785
    label "wykupienie"
  ]
  node [
    id 786
    label "bycie_w_posiadaniu"
  ]
  node [
    id 787
    label "uczestnik"
  ]
  node [
    id 788
    label "fanfaron"
  ]
  node [
    id 789
    label "odtw&#243;rca"
  ]
  node [
    id 790
    label "obsada"
  ]
  node [
    id 791
    label "Allen"
  ]
  node [
    id 792
    label "wykonawca"
  ]
  node [
    id 793
    label "Daniel_Olbrychski"
  ]
  node [
    id 794
    label "Roland_Topor"
  ]
  node [
    id 795
    label "Stuhr"
  ]
  node [
    id 796
    label "bajerant"
  ]
  node [
    id 797
    label "teatr"
  ]
  node [
    id 798
    label "interpretator"
  ]
  node [
    id 799
    label "Eastwood"
  ]
  node [
    id 800
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 801
    label "delegowa&#263;"
  ]
  node [
    id 802
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 803
    label "salariat"
  ]
  node [
    id 804
    label "pracu&#347;"
  ]
  node [
    id 805
    label "r&#281;ka"
  ]
  node [
    id 806
    label "delegowanie"
  ]
  node [
    id 807
    label "wsp&#243;lnik"
  ]
  node [
    id 808
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 809
    label "osoba_fizyczna"
  ]
  node [
    id 810
    label "klasa_&#347;rednia"
  ]
  node [
    id 811
    label "wydawca"
  ]
  node [
    id 812
    label "kapitalista"
  ]
  node [
    id 813
    label "bycie"
  ]
  node [
    id 814
    label "w&#322;&#261;czanie"
  ]
  node [
    id 815
    label "participation"
  ]
  node [
    id 816
    label "robienie"
  ]
  node [
    id 817
    label "eksponowa&#263;"
  ]
  node [
    id 818
    label "g&#243;rowa&#263;"
  ]
  node [
    id 819
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 820
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 821
    label "sterowa&#263;"
  ]
  node [
    id 822
    label "kierowa&#263;"
  ]
  node [
    id 823
    label "string"
  ]
  node [
    id 824
    label "control"
  ]
  node [
    id 825
    label "kre&#347;li&#263;"
  ]
  node [
    id 826
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 827
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 828
    label "&#380;y&#263;"
  ]
  node [
    id 829
    label "linia_melodyczna"
  ]
  node [
    id 830
    label "prowadzenie"
  ]
  node [
    id 831
    label "ukierunkowywa&#263;"
  ]
  node [
    id 832
    label "tworzy&#263;"
  ]
  node [
    id 833
    label "powodowa&#263;"
  ]
  node [
    id 834
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 835
    label "message"
  ]
  node [
    id 836
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 837
    label "navigate"
  ]
  node [
    id 838
    label "krzywa"
  ]
  node [
    id 839
    label "manipulate"
  ]
  node [
    id 840
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 841
    label "budynek"
  ]
  node [
    id 842
    label "&#321;ubianka"
  ]
  node [
    id 843
    label "Bia&#322;y_Dom"
  ]
  node [
    id 844
    label "dzia&#322;_personalny"
  ]
  node [
    id 845
    label "Kreml"
  ]
  node [
    id 846
    label "sadowisko"
  ]
  node [
    id 847
    label "magazyn"
  ]
  node [
    id 848
    label "zasoby_kopalin"
  ]
  node [
    id 849
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 850
    label "z&#322;o&#380;e"
  ]
  node [
    id 851
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 852
    label "driveway"
  ]
  node [
    id 853
    label "ropa_naftowa"
  ]
  node [
    id 854
    label "paliwo"
  ]
  node [
    id 855
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 856
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 857
    label "strategia"
  ]
  node [
    id 858
    label "odmienienie"
  ]
  node [
    id 859
    label "przer&#243;bka"
  ]
  node [
    id 860
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 861
    label "dobro"
  ]
  node [
    id 862
    label "penis"
  ]
  node [
    id 863
    label "object"
  ]
  node [
    id 864
    label "sprawa"
  ]
  node [
    id 865
    label "postawa"
  ]
  node [
    id 866
    label "faith"
  ]
  node [
    id 867
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 868
    label "opoka"
  ]
  node [
    id 869
    label "credit"
  ]
  node [
    id 870
    label "zacz&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 26
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 28
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 233
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 320
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 210
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 299
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 220
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 253
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 258
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 316
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 243
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 244
  ]
  edge [
    source 15
    target 257
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 259
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 266
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 319
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 771
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 59
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 776
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 320
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 470
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 268
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 18
    target 46
  ]
  edge [
    source 18
    target 47
  ]
  edge [
    source 18
    target 48
  ]
  edge [
    source 18
    target 50
  ]
  edge [
    source 18
    target 49
  ]
  edge [
    source 18
    target 51
  ]
  edge [
    source 18
    target 52
  ]
  edge [
    source 18
    target 53
  ]
  edge [
    source 18
    target 54
  ]
  edge [
    source 18
    target 55
  ]
  edge [
    source 18
    target 56
  ]
  edge [
    source 18
    target 57
  ]
  edge [
    source 18
    target 58
  ]
  edge [
    source 18
    target 59
  ]
  edge [
    source 18
    target 60
  ]
  edge [
    source 18
    target 61
  ]
  edge [
    source 18
    target 62
  ]
  edge [
    source 18
    target 63
  ]
  edge [
    source 18
    target 64
  ]
  edge [
    source 18
    target 65
  ]
  edge [
    source 18
    target 66
  ]
  edge [
    source 18
    target 67
  ]
  edge [
    source 18
    target 68
  ]
  edge [
    source 18
    target 69
  ]
  edge [
    source 18
    target 70
  ]
  edge [
    source 18
    target 71
  ]
  edge [
    source 18
    target 72
  ]
  edge [
    source 18
    target 664
  ]
  edge [
    source 18
    target 665
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 667
  ]
  edge [
    source 18
    target 668
  ]
  edge [
    source 18
    target 669
  ]
  edge [
    source 18
    target 670
  ]
  edge [
    source 18
    target 671
  ]
  edge [
    source 18
    target 672
  ]
  edge [
    source 18
    target 673
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 316
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 679
  ]
  edge [
    source 18
    target 680
  ]
  edge [
    source 18
    target 681
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 685
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 91
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 194
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 733
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 870
  ]
]
