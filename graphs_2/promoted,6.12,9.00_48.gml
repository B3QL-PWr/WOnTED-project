graph [
  node [
    id 0
    label "polski"
    origin "text"
  ]
  node [
    id 1
    label "g&#243;rnictwo"
    origin "text"
  ]
  node [
    id 2
    label "dostawa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "miliard"
    origin "text"
  ]
  node [
    id 4
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 5
    label "wsparcie"
    origin "text"
  ]
  node [
    id 6
    label "przedmiot"
  ]
  node [
    id 7
    label "Polish"
  ]
  node [
    id 8
    label "goniony"
  ]
  node [
    id 9
    label "oberek"
  ]
  node [
    id 10
    label "ryba_po_grecku"
  ]
  node [
    id 11
    label "sztajer"
  ]
  node [
    id 12
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 13
    label "krakowiak"
  ]
  node [
    id 14
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 15
    label "pierogi_ruskie"
  ]
  node [
    id 16
    label "lacki"
  ]
  node [
    id 17
    label "polak"
  ]
  node [
    id 18
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 19
    label "chodzony"
  ]
  node [
    id 20
    label "po_polsku"
  ]
  node [
    id 21
    label "mazur"
  ]
  node [
    id 22
    label "polsko"
  ]
  node [
    id 23
    label "skoczny"
  ]
  node [
    id 24
    label "drabant"
  ]
  node [
    id 25
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 26
    label "j&#281;zyk"
  ]
  node [
    id 27
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 28
    label "artykulator"
  ]
  node [
    id 29
    label "kod"
  ]
  node [
    id 30
    label "kawa&#322;ek"
  ]
  node [
    id 31
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 32
    label "gramatyka"
  ]
  node [
    id 33
    label "stylik"
  ]
  node [
    id 34
    label "przet&#322;umaczenie"
  ]
  node [
    id 35
    label "formalizowanie"
  ]
  node [
    id 36
    label "ssa&#263;"
  ]
  node [
    id 37
    label "ssanie"
  ]
  node [
    id 38
    label "language"
  ]
  node [
    id 39
    label "liza&#263;"
  ]
  node [
    id 40
    label "napisa&#263;"
  ]
  node [
    id 41
    label "konsonantyzm"
  ]
  node [
    id 42
    label "wokalizm"
  ]
  node [
    id 43
    label "pisa&#263;"
  ]
  node [
    id 44
    label "fonetyka"
  ]
  node [
    id 45
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 46
    label "jeniec"
  ]
  node [
    id 47
    label "but"
  ]
  node [
    id 48
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 49
    label "po_koroniarsku"
  ]
  node [
    id 50
    label "kultura_duchowa"
  ]
  node [
    id 51
    label "t&#322;umaczenie"
  ]
  node [
    id 52
    label "m&#243;wienie"
  ]
  node [
    id 53
    label "pype&#263;"
  ]
  node [
    id 54
    label "lizanie"
  ]
  node [
    id 55
    label "pismo"
  ]
  node [
    id 56
    label "formalizowa&#263;"
  ]
  node [
    id 57
    label "rozumie&#263;"
  ]
  node [
    id 58
    label "organ"
  ]
  node [
    id 59
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 60
    label "rozumienie"
  ]
  node [
    id 61
    label "spos&#243;b"
  ]
  node [
    id 62
    label "makroglosja"
  ]
  node [
    id 63
    label "m&#243;wi&#263;"
  ]
  node [
    id 64
    label "jama_ustna"
  ]
  node [
    id 65
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 66
    label "formacja_geologiczna"
  ]
  node [
    id 67
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 68
    label "natural_language"
  ]
  node [
    id 69
    label "s&#322;ownictwo"
  ]
  node [
    id 70
    label "urz&#261;dzenie"
  ]
  node [
    id 71
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 72
    label "wschodnioeuropejski"
  ]
  node [
    id 73
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 74
    label "poga&#324;ski"
  ]
  node [
    id 75
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 76
    label "topielec"
  ]
  node [
    id 77
    label "europejski"
  ]
  node [
    id 78
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 79
    label "langosz"
  ]
  node [
    id 80
    label "zboczenie"
  ]
  node [
    id 81
    label "om&#243;wienie"
  ]
  node [
    id 82
    label "sponiewieranie"
  ]
  node [
    id 83
    label "discipline"
  ]
  node [
    id 84
    label "rzecz"
  ]
  node [
    id 85
    label "omawia&#263;"
  ]
  node [
    id 86
    label "kr&#261;&#380;enie"
  ]
  node [
    id 87
    label "tre&#347;&#263;"
  ]
  node [
    id 88
    label "robienie"
  ]
  node [
    id 89
    label "sponiewiera&#263;"
  ]
  node [
    id 90
    label "element"
  ]
  node [
    id 91
    label "entity"
  ]
  node [
    id 92
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 93
    label "tematyka"
  ]
  node [
    id 94
    label "w&#261;tek"
  ]
  node [
    id 95
    label "charakter"
  ]
  node [
    id 96
    label "zbaczanie"
  ]
  node [
    id 97
    label "program_nauczania"
  ]
  node [
    id 98
    label "om&#243;wi&#263;"
  ]
  node [
    id 99
    label "omawianie"
  ]
  node [
    id 100
    label "thing"
  ]
  node [
    id 101
    label "kultura"
  ]
  node [
    id 102
    label "istota"
  ]
  node [
    id 103
    label "zbacza&#263;"
  ]
  node [
    id 104
    label "zboczy&#263;"
  ]
  node [
    id 105
    label "gwardzista"
  ]
  node [
    id 106
    label "melodia"
  ]
  node [
    id 107
    label "taniec"
  ]
  node [
    id 108
    label "taniec_ludowy"
  ]
  node [
    id 109
    label "&#347;redniowieczny"
  ]
  node [
    id 110
    label "specjalny"
  ]
  node [
    id 111
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 112
    label "weso&#322;y"
  ]
  node [
    id 113
    label "sprawny"
  ]
  node [
    id 114
    label "rytmiczny"
  ]
  node [
    id 115
    label "skocznie"
  ]
  node [
    id 116
    label "energiczny"
  ]
  node [
    id 117
    label "lendler"
  ]
  node [
    id 118
    label "austriacki"
  ]
  node [
    id 119
    label "polka"
  ]
  node [
    id 120
    label "europejsko"
  ]
  node [
    id 121
    label "przytup"
  ]
  node [
    id 122
    label "ho&#322;ubiec"
  ]
  node [
    id 123
    label "wodzi&#263;"
  ]
  node [
    id 124
    label "ludowy"
  ]
  node [
    id 125
    label "pie&#347;&#324;"
  ]
  node [
    id 126
    label "mieszkaniec"
  ]
  node [
    id 127
    label "centu&#347;"
  ]
  node [
    id 128
    label "lalka"
  ]
  node [
    id 129
    label "Ma&#322;opolanin"
  ]
  node [
    id 130
    label "krakauer"
  ]
  node [
    id 131
    label "wydobywa&#263;"
  ]
  node [
    id 132
    label "odstrzeliwa&#263;"
  ]
  node [
    id 133
    label "rozpierak"
  ]
  node [
    id 134
    label "krzeska"
  ]
  node [
    id 135
    label "wydoby&#263;"
  ]
  node [
    id 136
    label "przemys&#322;_ci&#281;&#380;ki"
  ]
  node [
    id 137
    label "obrywak"
  ]
  node [
    id 138
    label "wydobycie"
  ]
  node [
    id 139
    label "wydobywanie"
  ]
  node [
    id 140
    label "&#322;adownik"
  ]
  node [
    id 141
    label "zgarniacz"
  ]
  node [
    id 142
    label "nauka"
  ]
  node [
    id 143
    label "wcinka"
  ]
  node [
    id 144
    label "solnictwo"
  ]
  node [
    id 145
    label "odstrzeliwanie"
  ]
  node [
    id 146
    label "aerologia_g&#243;rnicza"
  ]
  node [
    id 147
    label "wiertnictwo"
  ]
  node [
    id 148
    label "przesyp"
  ]
  node [
    id 149
    label "wiedza"
  ]
  node [
    id 150
    label "miasteczko_rowerowe"
  ]
  node [
    id 151
    label "porada"
  ]
  node [
    id 152
    label "fotowoltaika"
  ]
  node [
    id 153
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 154
    label "przem&#243;wienie"
  ]
  node [
    id 155
    label "nauki_o_poznaniu"
  ]
  node [
    id 156
    label "nomotetyczny"
  ]
  node [
    id 157
    label "systematyka"
  ]
  node [
    id 158
    label "proces"
  ]
  node [
    id 159
    label "typologia"
  ]
  node [
    id 160
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 161
    label "&#322;awa_szkolna"
  ]
  node [
    id 162
    label "nauki_penalne"
  ]
  node [
    id 163
    label "dziedzina"
  ]
  node [
    id 164
    label "imagineskopia"
  ]
  node [
    id 165
    label "teoria_naukowa"
  ]
  node [
    id 166
    label "inwentyka"
  ]
  node [
    id 167
    label "metodologia"
  ]
  node [
    id 168
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 169
    label "nauki_o_Ziemi"
  ]
  node [
    id 170
    label "draw"
  ]
  node [
    id 171
    label "doby&#263;"
  ]
  node [
    id 172
    label "wyeksploatowa&#263;"
  ]
  node [
    id 173
    label "extract"
  ]
  node [
    id 174
    label "obtain"
  ]
  node [
    id 175
    label "wyj&#261;&#263;"
  ]
  node [
    id 176
    label "ocali&#263;"
  ]
  node [
    id 177
    label "uzyska&#263;"
  ]
  node [
    id 178
    label "wyda&#263;"
  ]
  node [
    id 179
    label "wydosta&#263;"
  ]
  node [
    id 180
    label "uwydatni&#263;"
  ]
  node [
    id 181
    label "distill"
  ]
  node [
    id 182
    label "raise"
  ]
  node [
    id 183
    label "od&#322;upywanie"
  ]
  node [
    id 184
    label "urywanie"
  ]
  node [
    id 185
    label "zabijanie"
  ]
  node [
    id 186
    label "dobywanie"
  ]
  node [
    id 187
    label "powodowanie"
  ]
  node [
    id 188
    label "u&#380;ytkowanie"
  ]
  node [
    id 189
    label "eksploatowanie"
  ]
  node [
    id 190
    label "wydostawanie"
  ]
  node [
    id 191
    label "wyjmowanie"
  ]
  node [
    id 192
    label "ratowanie"
  ]
  node [
    id 193
    label "uzyskiwanie"
  ]
  node [
    id 194
    label "evocation"
  ]
  node [
    id 195
    label "czynno&#347;&#263;"
  ]
  node [
    id 196
    label "uwydatnianie"
  ]
  node [
    id 197
    label "extraction"
  ]
  node [
    id 198
    label "uwydatnia&#263;"
  ]
  node [
    id 199
    label "eksploatowa&#263;"
  ]
  node [
    id 200
    label "uzyskiwa&#263;"
  ]
  node [
    id 201
    label "wydostawa&#263;"
  ]
  node [
    id 202
    label "wyjmowa&#263;"
  ]
  node [
    id 203
    label "train"
  ]
  node [
    id 204
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 205
    label "wydawa&#263;"
  ]
  node [
    id 206
    label "dobywa&#263;"
  ]
  node [
    id 207
    label "ocala&#263;"
  ]
  node [
    id 208
    label "excavate"
  ]
  node [
    id 209
    label "wyeksploatowanie"
  ]
  node [
    id 210
    label "uwydatnienie"
  ]
  node [
    id 211
    label "uzyskanie"
  ]
  node [
    id 212
    label "fusillade"
  ]
  node [
    id 213
    label "spowodowanie"
  ]
  node [
    id 214
    label "wyratowanie"
  ]
  node [
    id 215
    label "wyj&#281;cie"
  ]
  node [
    id 216
    label "powyci&#261;ganie"
  ]
  node [
    id 217
    label "wydostanie"
  ]
  node [
    id 218
    label "dobycie"
  ]
  node [
    id 219
    label "explosion"
  ]
  node [
    id 220
    label "produkcja"
  ]
  node [
    id 221
    label "zrobienie"
  ]
  node [
    id 222
    label "zabija&#263;"
  ]
  node [
    id 223
    label "od&#322;upywa&#263;"
  ]
  node [
    id 224
    label "urywa&#263;"
  ]
  node [
    id 225
    label "fire"
  ]
  node [
    id 226
    label "toporek"
  ]
  node [
    id 227
    label "wa&#322;"
  ]
  node [
    id 228
    label "ilo&#347;&#263;"
  ]
  node [
    id 229
    label "miejsce"
  ]
  node [
    id 230
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 231
    label "&#322;adowarka"
  ]
  node [
    id 232
    label "robotnik"
  ]
  node [
    id 233
    label "zasobnik"
  ]
  node [
    id 234
    label "mocowanie"
  ]
  node [
    id 235
    label "bro&#324;_maszynowa"
  ]
  node [
    id 236
    label "rolnictwo"
  ]
  node [
    id 237
    label "budownictwo"
  ]
  node [
    id 238
    label "scraper"
  ]
  node [
    id 239
    label "przerywnik"
  ]
  node [
    id 240
    label "przy&#322;&#261;cze"
  ]
  node [
    id 241
    label "zagrywka"
  ]
  node [
    id 242
    label "wn&#281;ka"
  ]
  node [
    id 243
    label "wci&#281;cie"
  ]
  node [
    id 244
    label "film"
  ]
  node [
    id 245
    label "sztuczka"
  ]
  node [
    id 246
    label "fortel"
  ]
  node [
    id 247
    label "&#322;om"
  ]
  node [
    id 248
    label "podci&#261;gnik"
  ]
  node [
    id 249
    label "mie&#263;_miejsce"
  ]
  node [
    id 250
    label "by&#263;"
  ]
  node [
    id 251
    label "nabywa&#263;"
  ]
  node [
    id 252
    label "bra&#263;"
  ]
  node [
    id 253
    label "winnings"
  ]
  node [
    id 254
    label "opanowywa&#263;"
  ]
  node [
    id 255
    label "si&#281;ga&#263;"
  ]
  node [
    id 256
    label "otrzymywa&#263;"
  ]
  node [
    id 257
    label "range"
  ]
  node [
    id 258
    label "wystarcza&#263;"
  ]
  node [
    id 259
    label "kupowa&#263;"
  ]
  node [
    id 260
    label "obskakiwa&#263;"
  ]
  node [
    id 261
    label "return"
  ]
  node [
    id 262
    label "take"
  ]
  node [
    id 263
    label "wytwarza&#263;"
  ]
  node [
    id 264
    label "kupywa&#263;"
  ]
  node [
    id 265
    label "pozyskiwa&#263;"
  ]
  node [
    id 266
    label "przyjmowa&#263;"
  ]
  node [
    id 267
    label "ustawia&#263;"
  ]
  node [
    id 268
    label "get"
  ]
  node [
    id 269
    label "gra&#263;"
  ]
  node [
    id 270
    label "uznawa&#263;"
  ]
  node [
    id 271
    label "wierzy&#263;"
  ]
  node [
    id 272
    label "compass"
  ]
  node [
    id 273
    label "korzysta&#263;"
  ]
  node [
    id 274
    label "appreciation"
  ]
  node [
    id 275
    label "osi&#261;ga&#263;"
  ]
  node [
    id 276
    label "dociera&#263;"
  ]
  node [
    id 277
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 278
    label "mierzy&#263;"
  ]
  node [
    id 279
    label "u&#380;ywa&#263;"
  ]
  node [
    id 280
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 281
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 282
    label "exsert"
  ]
  node [
    id 283
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 284
    label "equal"
  ]
  node [
    id 285
    label "trwa&#263;"
  ]
  node [
    id 286
    label "chodzi&#263;"
  ]
  node [
    id 287
    label "stan"
  ]
  node [
    id 288
    label "obecno&#347;&#263;"
  ]
  node [
    id 289
    label "stand"
  ]
  node [
    id 290
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 291
    label "uczestniczy&#263;"
  ]
  node [
    id 292
    label "mark"
  ]
  node [
    id 293
    label "powodowa&#263;"
  ]
  node [
    id 294
    label "robi&#263;"
  ]
  node [
    id 295
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 296
    label "porywa&#263;"
  ]
  node [
    id 297
    label "wchodzi&#263;"
  ]
  node [
    id 298
    label "poczytywa&#263;"
  ]
  node [
    id 299
    label "levy"
  ]
  node [
    id 300
    label "wk&#322;ada&#263;"
  ]
  node [
    id 301
    label "pokonywa&#263;"
  ]
  node [
    id 302
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 303
    label "rucha&#263;"
  ]
  node [
    id 304
    label "prowadzi&#263;"
  ]
  node [
    id 305
    label "za&#380;ywa&#263;"
  ]
  node [
    id 306
    label "&#263;pa&#263;"
  ]
  node [
    id 307
    label "interpretowa&#263;"
  ]
  node [
    id 308
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 309
    label "rusza&#263;"
  ]
  node [
    id 310
    label "chwyta&#263;"
  ]
  node [
    id 311
    label "grza&#263;"
  ]
  node [
    id 312
    label "wch&#322;ania&#263;"
  ]
  node [
    id 313
    label "wygrywa&#263;"
  ]
  node [
    id 314
    label "ucieka&#263;"
  ]
  node [
    id 315
    label "arise"
  ]
  node [
    id 316
    label "uprawia&#263;_seks"
  ]
  node [
    id 317
    label "abstract"
  ]
  node [
    id 318
    label "towarzystwo"
  ]
  node [
    id 319
    label "atakowa&#263;"
  ]
  node [
    id 320
    label "branie"
  ]
  node [
    id 321
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 322
    label "zalicza&#263;"
  ]
  node [
    id 323
    label "open"
  ]
  node [
    id 324
    label "wzi&#261;&#263;"
  ]
  node [
    id 325
    label "&#322;apa&#263;"
  ]
  node [
    id 326
    label "przewa&#380;a&#263;"
  ]
  node [
    id 327
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 328
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 329
    label "okrada&#263;"
  ]
  node [
    id 330
    label "obiega&#263;"
  ]
  node [
    id 331
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 332
    label "op&#281;dza&#263;"
  ]
  node [
    id 333
    label "osacza&#263;"
  ]
  node [
    id 334
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 335
    label "environment"
  ]
  node [
    id 336
    label "radzi&#263;_sobie"
  ]
  node [
    id 337
    label "manipulate"
  ]
  node [
    id 338
    label "niewoli&#263;"
  ]
  node [
    id 339
    label "capture"
  ]
  node [
    id 340
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 341
    label "powstrzymywa&#263;"
  ]
  node [
    id 342
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 343
    label "meet"
  ]
  node [
    id 344
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 345
    label "rede"
  ]
  node [
    id 346
    label "zaspokaja&#263;"
  ]
  node [
    id 347
    label "suffice"
  ]
  node [
    id 348
    label "stawa&#263;"
  ]
  node [
    id 349
    label "liczba"
  ]
  node [
    id 350
    label "kategoria"
  ]
  node [
    id 351
    label "pierwiastek"
  ]
  node [
    id 352
    label "rozmiar"
  ]
  node [
    id 353
    label "poj&#281;cie"
  ]
  node [
    id 354
    label "number"
  ]
  node [
    id 355
    label "cecha"
  ]
  node [
    id 356
    label "kategoria_gramatyczna"
  ]
  node [
    id 357
    label "grupa"
  ]
  node [
    id 358
    label "kwadrat_magiczny"
  ]
  node [
    id 359
    label "wyra&#380;enie"
  ]
  node [
    id 360
    label "koniugacja"
  ]
  node [
    id 361
    label "jednostka_monetarna"
  ]
  node [
    id 362
    label "wspania&#322;y"
  ]
  node [
    id 363
    label "metaliczny"
  ]
  node [
    id 364
    label "Polska"
  ]
  node [
    id 365
    label "szlachetny"
  ]
  node [
    id 366
    label "kochany"
  ]
  node [
    id 367
    label "doskona&#322;y"
  ]
  node [
    id 368
    label "grosz"
  ]
  node [
    id 369
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 370
    label "poz&#322;ocenie"
  ]
  node [
    id 371
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 372
    label "utytu&#322;owany"
  ]
  node [
    id 373
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 374
    label "z&#322;ocenie"
  ]
  node [
    id 375
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 376
    label "prominentny"
  ]
  node [
    id 377
    label "znany"
  ]
  node [
    id 378
    label "wybitny"
  ]
  node [
    id 379
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 380
    label "naj"
  ]
  node [
    id 381
    label "&#347;wietny"
  ]
  node [
    id 382
    label "pe&#322;ny"
  ]
  node [
    id 383
    label "doskonale"
  ]
  node [
    id 384
    label "szlachetnie"
  ]
  node [
    id 385
    label "uczciwy"
  ]
  node [
    id 386
    label "zacny"
  ]
  node [
    id 387
    label "harmonijny"
  ]
  node [
    id 388
    label "gatunkowy"
  ]
  node [
    id 389
    label "pi&#281;kny"
  ]
  node [
    id 390
    label "dobry"
  ]
  node [
    id 391
    label "typowy"
  ]
  node [
    id 392
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 393
    label "metaloplastyczny"
  ]
  node [
    id 394
    label "metalicznie"
  ]
  node [
    id 395
    label "kochanek"
  ]
  node [
    id 396
    label "wybranek"
  ]
  node [
    id 397
    label "umi&#322;owany"
  ]
  node [
    id 398
    label "drogi"
  ]
  node [
    id 399
    label "kochanie"
  ]
  node [
    id 400
    label "wspaniale"
  ]
  node [
    id 401
    label "pomy&#347;lny"
  ]
  node [
    id 402
    label "pozytywny"
  ]
  node [
    id 403
    label "&#347;wietnie"
  ]
  node [
    id 404
    label "spania&#322;y"
  ]
  node [
    id 405
    label "och&#281;do&#380;ny"
  ]
  node [
    id 406
    label "warto&#347;ciowy"
  ]
  node [
    id 407
    label "zajebisty"
  ]
  node [
    id 408
    label "bogato"
  ]
  node [
    id 409
    label "typ_mongoloidalny"
  ]
  node [
    id 410
    label "kolorowy"
  ]
  node [
    id 411
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 412
    label "ciep&#322;y"
  ]
  node [
    id 413
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 414
    label "jasny"
  ]
  node [
    id 415
    label "kwota"
  ]
  node [
    id 416
    label "groszak"
  ]
  node [
    id 417
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 418
    label "szyling_austryjacki"
  ]
  node [
    id 419
    label "moneta"
  ]
  node [
    id 420
    label "Mazowsze"
  ]
  node [
    id 421
    label "Pa&#322;uki"
  ]
  node [
    id 422
    label "Pomorze_Zachodnie"
  ]
  node [
    id 423
    label "Powi&#347;le"
  ]
  node [
    id 424
    label "Wolin"
  ]
  node [
    id 425
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 426
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 427
    label "So&#322;a"
  ]
  node [
    id 428
    label "Unia_Europejska"
  ]
  node [
    id 429
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 430
    label "Opolskie"
  ]
  node [
    id 431
    label "Suwalszczyzna"
  ]
  node [
    id 432
    label "Krajna"
  ]
  node [
    id 433
    label "barwy_polskie"
  ]
  node [
    id 434
    label "Nadbu&#380;e"
  ]
  node [
    id 435
    label "Podlasie"
  ]
  node [
    id 436
    label "Izera"
  ]
  node [
    id 437
    label "Ma&#322;opolska"
  ]
  node [
    id 438
    label "Warmia"
  ]
  node [
    id 439
    label "Mazury"
  ]
  node [
    id 440
    label "NATO"
  ]
  node [
    id 441
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 442
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 443
    label "Lubelszczyzna"
  ]
  node [
    id 444
    label "Kaczawa"
  ]
  node [
    id 445
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 446
    label "Kielecczyzna"
  ]
  node [
    id 447
    label "Lubuskie"
  ]
  node [
    id 448
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 449
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 450
    label "&#321;&#243;dzkie"
  ]
  node [
    id 451
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 452
    label "Kujawy"
  ]
  node [
    id 453
    label "Podkarpacie"
  ]
  node [
    id 454
    label "Wielkopolska"
  ]
  node [
    id 455
    label "Wis&#322;a"
  ]
  node [
    id 456
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 457
    label "Bory_Tucholskie"
  ]
  node [
    id 458
    label "platerowanie"
  ]
  node [
    id 459
    label "z&#322;ocisty"
  ]
  node [
    id 460
    label "barwienie"
  ]
  node [
    id 461
    label "gilt"
  ]
  node [
    id 462
    label "plating"
  ]
  node [
    id 463
    label "zdobienie"
  ]
  node [
    id 464
    label "club"
  ]
  node [
    id 465
    label "powleczenie"
  ]
  node [
    id 466
    label "zabarwienie"
  ]
  node [
    id 467
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 468
    label "&#347;rodek"
  ]
  node [
    id 469
    label "oparcie"
  ]
  node [
    id 470
    label "darowizna"
  ]
  node [
    id 471
    label "zapomoga"
  ]
  node [
    id 472
    label "comfort"
  ]
  node [
    id 473
    label "doch&#243;d"
  ]
  node [
    id 474
    label "pocieszenie"
  ]
  node [
    id 475
    label "telefon_zaufania"
  ]
  node [
    id 476
    label "dar"
  ]
  node [
    id 477
    label "support"
  ]
  node [
    id 478
    label "u&#322;atwienie"
  ]
  node [
    id 479
    label "pomoc"
  ]
  node [
    id 480
    label "income"
  ]
  node [
    id 481
    label "stopa_procentowa"
  ]
  node [
    id 482
    label "krzywa_Engla"
  ]
  node [
    id 483
    label "korzy&#347;&#263;"
  ]
  node [
    id 484
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 485
    label "wp&#322;yw"
  ]
  node [
    id 486
    label "dyspozycja"
  ]
  node [
    id 487
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 488
    label "da&#324;"
  ]
  node [
    id 489
    label "faculty"
  ]
  node [
    id 490
    label "stygmat"
  ]
  node [
    id 491
    label "dobro"
  ]
  node [
    id 492
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 493
    label "liga"
  ]
  node [
    id 494
    label "pomocnik"
  ]
  node [
    id 495
    label "zgodzi&#263;"
  ]
  node [
    id 496
    label "property"
  ]
  node [
    id 497
    label "ukojenie"
  ]
  node [
    id 498
    label "pomo&#380;enie"
  ]
  node [
    id 499
    label "facilitation"
  ]
  node [
    id 500
    label "ulepszenie"
  ]
  node [
    id 501
    label "punkt"
  ]
  node [
    id 502
    label "abstrakcja"
  ]
  node [
    id 503
    label "czas"
  ]
  node [
    id 504
    label "chemikalia"
  ]
  node [
    id 505
    label "substancja"
  ]
  node [
    id 506
    label "ustawienie"
  ]
  node [
    id 507
    label "back"
  ]
  node [
    id 508
    label "podpora"
  ]
  node [
    id 509
    label "anchor"
  ]
  node [
    id 510
    label "zaczerpni&#281;cie"
  ]
  node [
    id 511
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 512
    label "podstawa"
  ]
  node [
    id 513
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 514
    label "przeniesienie_praw"
  ]
  node [
    id 515
    label "transakcja"
  ]
  node [
    id 516
    label "wykonawca"
  ]
  node [
    id 517
    label "interpretator"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
]
