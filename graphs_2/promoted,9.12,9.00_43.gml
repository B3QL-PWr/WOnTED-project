graph [
  node [
    id 0
    label "podawa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;osek"
    origin "text"
  ]
  node [
    id 2
    label "miesi&#281;cznik"
    origin "text"
  ]
  node [
    id 3
    label "katolicki"
    origin "text"
  ]
  node [
    id 4
    label "namawia&#263;"
    origin "text"
  ]
  node [
    id 5
    label "m&#322;oda"
    origin "text"
  ]
  node [
    id 6
    label "chrze&#347;cijanin"
    origin "text"
  ]
  node [
    id 7
    label "aby"
    origin "text"
  ]
  node [
    id 8
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 9
    label "wiara"
    origin "text"
  ]
  node [
    id 10
    label "oferowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "stypendium"
    origin "text"
  ]
  node [
    id 12
    label "tani"
    origin "text"
  ]
  node [
    id 13
    label "dobry"
    origin "text"
  ]
  node [
    id 14
    label "stabilny"
    origin "text"
  ]
  node [
    id 15
    label "siatk&#243;wka"
  ]
  node [
    id 16
    label "kelner"
  ]
  node [
    id 17
    label "cover"
  ]
  node [
    id 18
    label "tenis"
  ]
  node [
    id 19
    label "informowa&#263;"
  ]
  node [
    id 20
    label "jedzenie"
  ]
  node [
    id 21
    label "tender"
  ]
  node [
    id 22
    label "dawa&#263;"
  ]
  node [
    id 23
    label "faszerowa&#263;"
  ]
  node [
    id 24
    label "introduce"
  ]
  node [
    id 25
    label "serwowa&#263;"
  ]
  node [
    id 26
    label "stawia&#263;"
  ]
  node [
    id 27
    label "rozgrywa&#263;"
  ]
  node [
    id 28
    label "deal"
  ]
  node [
    id 29
    label "powiada&#263;"
  ]
  node [
    id 30
    label "komunikowa&#263;"
  ]
  node [
    id 31
    label "inform"
  ]
  node [
    id 32
    label "zaczyna&#263;"
  ]
  node [
    id 33
    label "pi&#322;ka"
  ]
  node [
    id 34
    label "give"
  ]
  node [
    id 35
    label "gra&#263;"
  ]
  node [
    id 36
    label "play"
  ]
  node [
    id 37
    label "przeprowadza&#263;"
  ]
  node [
    id 38
    label "robi&#263;"
  ]
  node [
    id 39
    label "exsert"
  ]
  node [
    id 40
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 41
    label "umieszcza&#263;"
  ]
  node [
    id 42
    label "powierza&#263;"
  ]
  node [
    id 43
    label "mie&#263;_miejsce"
  ]
  node [
    id 44
    label "wpiernicza&#263;"
  ]
  node [
    id 45
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 46
    label "odst&#281;powa&#263;"
  ]
  node [
    id 47
    label "hold_out"
  ]
  node [
    id 48
    label "rap"
  ]
  node [
    id 49
    label "obiecywa&#263;"
  ]
  node [
    id 50
    label "&#322;adowa&#263;"
  ]
  node [
    id 51
    label "t&#322;uc"
  ]
  node [
    id 52
    label "nalewa&#263;"
  ]
  node [
    id 53
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 54
    label "hold"
  ]
  node [
    id 55
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 56
    label "przekazywa&#263;"
  ]
  node [
    id 57
    label "zezwala&#263;"
  ]
  node [
    id 58
    label "train"
  ]
  node [
    id 59
    label "render"
  ]
  node [
    id 60
    label "dostarcza&#263;"
  ]
  node [
    id 61
    label "przeznacza&#263;"
  ]
  node [
    id 62
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 63
    label "p&#322;aci&#263;"
  ]
  node [
    id 64
    label "surrender"
  ]
  node [
    id 65
    label "traktowa&#263;"
  ]
  node [
    id 66
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 67
    label "wyznacza&#263;"
  ]
  node [
    id 68
    label "raise"
  ]
  node [
    id 69
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 70
    label "ocenia&#263;"
  ]
  node [
    id 71
    label "obstawia&#263;"
  ]
  node [
    id 72
    label "pozostawia&#263;"
  ]
  node [
    id 73
    label "wytwarza&#263;"
  ]
  node [
    id 74
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 75
    label "go"
  ]
  node [
    id 76
    label "przedstawia&#263;"
  ]
  node [
    id 77
    label "czyni&#263;"
  ]
  node [
    id 78
    label "wydawa&#263;"
  ]
  node [
    id 79
    label "fundowa&#263;"
  ]
  node [
    id 80
    label "zmienia&#263;"
  ]
  node [
    id 81
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 82
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 83
    label "uruchamia&#263;"
  ]
  node [
    id 84
    label "powodowa&#263;"
  ]
  node [
    id 85
    label "przewidywa&#263;"
  ]
  node [
    id 86
    label "zastawia&#263;"
  ]
  node [
    id 87
    label "stanowisko"
  ]
  node [
    id 88
    label "deliver"
  ]
  node [
    id 89
    label "znak"
  ]
  node [
    id 90
    label "przyznawa&#263;"
  ]
  node [
    id 91
    label "wskazywa&#263;"
  ]
  node [
    id 92
    label "wydobywa&#263;"
  ]
  node [
    id 93
    label "charge"
  ]
  node [
    id 94
    label "nadziewa&#263;"
  ]
  node [
    id 95
    label "przesadza&#263;"
  ]
  node [
    id 96
    label "blok"
  ]
  node [
    id 97
    label "podanie"
  ]
  node [
    id 98
    label "retinopatia"
  ]
  node [
    id 99
    label "dno_oka"
  ]
  node [
    id 100
    label "zeaksantyna"
  ]
  node [
    id 101
    label "plamka_&#380;&#243;&#322;ta"
  ]
  node [
    id 102
    label "przelobowa&#263;"
  ]
  node [
    id 103
    label "&#347;cina&#263;"
  ]
  node [
    id 104
    label "barwnik_&#347;wiat&#322;oczu&#322;y"
  ]
  node [
    id 105
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 106
    label "&#347;cinanie"
  ]
  node [
    id 107
    label "poda&#263;"
  ]
  node [
    id 108
    label "cia&#322;o_szkliste"
  ]
  node [
    id 109
    label "podawanie"
  ]
  node [
    id 110
    label "lobowanie"
  ]
  node [
    id 111
    label "lobowa&#263;"
  ]
  node [
    id 112
    label "&#347;ci&#281;cie"
  ]
  node [
    id 113
    label "przelobowanie"
  ]
  node [
    id 114
    label "pelota"
  ]
  node [
    id 115
    label "forhend"
  ]
  node [
    id 116
    label "deblowy"
  ]
  node [
    id 117
    label "wolej"
  ]
  node [
    id 118
    label "bekhend"
  ]
  node [
    id 119
    label "pr&#261;&#380;kowany"
  ]
  node [
    id 120
    label "mikst"
  ]
  node [
    id 121
    label "tkanina_we&#322;niana"
  ]
  node [
    id 122
    label "miksista"
  ]
  node [
    id 123
    label "ubrani&#243;wka"
  ]
  node [
    id 124
    label "Wimbledon"
  ]
  node [
    id 125
    label "supervisor"
  ]
  node [
    id 126
    label "slajs"
  ]
  node [
    id 127
    label "sport_rakietowy"
  ]
  node [
    id 128
    label "tkanina"
  ]
  node [
    id 129
    label "p&#243;&#322;wolej"
  ]
  node [
    id 130
    label "singlowy"
  ]
  node [
    id 131
    label "deblista"
  ]
  node [
    id 132
    label "singlista"
  ]
  node [
    id 133
    label "przejadanie"
  ]
  node [
    id 134
    label "jadanie"
  ]
  node [
    id 135
    label "posilanie"
  ]
  node [
    id 136
    label "przejedzenie"
  ]
  node [
    id 137
    label "szama"
  ]
  node [
    id 138
    label "odpasienie_si&#281;"
  ]
  node [
    id 139
    label "papusianie"
  ]
  node [
    id 140
    label "ufetowanie_si&#281;"
  ]
  node [
    id 141
    label "wyjadanie"
  ]
  node [
    id 142
    label "wpieprzanie"
  ]
  node [
    id 143
    label "wmuszanie"
  ]
  node [
    id 144
    label "objadanie"
  ]
  node [
    id 145
    label "odpasanie_si&#281;"
  ]
  node [
    id 146
    label "mlaskanie"
  ]
  node [
    id 147
    label "czynno&#347;&#263;"
  ]
  node [
    id 148
    label "posilenie"
  ]
  node [
    id 149
    label "polowanie"
  ]
  node [
    id 150
    label "&#380;arcie"
  ]
  node [
    id 151
    label "przejadanie_si&#281;"
  ]
  node [
    id 152
    label "koryto"
  ]
  node [
    id 153
    label "jad&#322;o"
  ]
  node [
    id 154
    label "przejedzenie_si&#281;"
  ]
  node [
    id 155
    label "eating"
  ]
  node [
    id 156
    label "wiwenda"
  ]
  node [
    id 157
    label "rzecz"
  ]
  node [
    id 158
    label "wyjedzenie"
  ]
  node [
    id 159
    label "robienie"
  ]
  node [
    id 160
    label "smakowanie"
  ]
  node [
    id 161
    label "zatruwanie_si&#281;"
  ]
  node [
    id 162
    label "ober"
  ]
  node [
    id 163
    label "pracownik"
  ]
  node [
    id 164
    label "aran&#380;acja"
  ]
  node [
    id 165
    label "kawa&#322;ek"
  ]
  node [
    id 166
    label "statek"
  ]
  node [
    id 167
    label "parow&#243;z_tendrowy"
  ]
  node [
    id 168
    label "okr&#281;t"
  ]
  node [
    id 169
    label "wagon"
  ]
  node [
    id 170
    label "pojazd_kolejowy"
  ]
  node [
    id 171
    label "poci&#261;g"
  ]
  node [
    id 172
    label "obiekt_naturalny"
  ]
  node [
    id 173
    label "w&#322;os"
  ]
  node [
    id 174
    label "mieszek_w&#322;osowy"
  ]
  node [
    id 175
    label "cebulka"
  ]
  node [
    id 176
    label "wytw&#243;r"
  ]
  node [
    id 177
    label "powierzchnia"
  ]
  node [
    id 178
    label "miesi&#281;cznikowate"
  ]
  node [
    id 179
    label "pn&#261;cze"
  ]
  node [
    id 180
    label "czasopismo"
  ]
  node [
    id 181
    label "Supe&#322;ek"
  ]
  node [
    id 182
    label "ro&#347;lina"
  ]
  node [
    id 183
    label "vine"
  ]
  node [
    id 184
    label "kszta&#322;t"
  ]
  node [
    id 185
    label "ok&#322;adka"
  ]
  node [
    id 186
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 187
    label "prasa"
  ]
  node [
    id 188
    label "dzia&#322;"
  ]
  node [
    id 189
    label "zajawka"
  ]
  node [
    id 190
    label "psychotest"
  ]
  node [
    id 191
    label "wk&#322;ad"
  ]
  node [
    id 192
    label "communication"
  ]
  node [
    id 193
    label "Zwrotnica"
  ]
  node [
    id 194
    label "egzemplarz"
  ]
  node [
    id 195
    label "pismo"
  ]
  node [
    id 196
    label "jaskrowce"
  ]
  node [
    id 197
    label "zgodny"
  ]
  node [
    id 198
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 199
    label "typowy"
  ]
  node [
    id 200
    label "chrze&#347;cija&#324;ski"
  ]
  node [
    id 201
    label "po_katolicku"
  ]
  node [
    id 202
    label "wyznaniowy"
  ]
  node [
    id 203
    label "powszechny"
  ]
  node [
    id 204
    label "spokojny"
  ]
  node [
    id 205
    label "zgodnie"
  ]
  node [
    id 206
    label "zbie&#380;ny"
  ]
  node [
    id 207
    label "generalny"
  ]
  node [
    id 208
    label "powszechnie"
  ]
  node [
    id 209
    label "znany"
  ]
  node [
    id 210
    label "zbiorowy"
  ]
  node [
    id 211
    label "cz&#281;sty"
  ]
  node [
    id 212
    label "typowo"
  ]
  node [
    id 213
    label "zwyk&#322;y"
  ]
  node [
    id 214
    label "zwyczajny"
  ]
  node [
    id 215
    label "taki"
  ]
  node [
    id 216
    label "stosownie"
  ]
  node [
    id 217
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 218
    label "prawdziwy"
  ]
  node [
    id 219
    label "zasadniczy"
  ]
  node [
    id 220
    label "charakterystyczny"
  ]
  node [
    id 221
    label "uprawniony"
  ]
  node [
    id 222
    label "nale&#380;yty"
  ]
  node [
    id 223
    label "ten"
  ]
  node [
    id 224
    label "nale&#380;ny"
  ]
  node [
    id 225
    label "religijny"
  ]
  node [
    id 226
    label "krze&#347;cija&#324;ski"
  ]
  node [
    id 227
    label "chrze&#347;cija&#324;sko"
  ]
  node [
    id 228
    label "przekonywa&#263;"
  ]
  node [
    id 229
    label "prompt"
  ]
  node [
    id 230
    label "nak&#322;ania&#263;"
  ]
  node [
    id 231
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 232
    label "argue"
  ]
  node [
    id 233
    label "cz&#322;owiek"
  ]
  node [
    id 234
    label "suknia_&#347;lubna"
  ]
  node [
    id 235
    label "&#380;ona"
  ]
  node [
    id 236
    label "niezam&#281;&#380;na"
  ]
  node [
    id 237
    label "kobieta"
  ]
  node [
    id 238
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 239
    label "asymilowa&#263;"
  ]
  node [
    id 240
    label "nasada"
  ]
  node [
    id 241
    label "profanum"
  ]
  node [
    id 242
    label "wz&#243;r"
  ]
  node [
    id 243
    label "senior"
  ]
  node [
    id 244
    label "asymilowanie"
  ]
  node [
    id 245
    label "os&#322;abia&#263;"
  ]
  node [
    id 246
    label "homo_sapiens"
  ]
  node [
    id 247
    label "osoba"
  ]
  node [
    id 248
    label "ludzko&#347;&#263;"
  ]
  node [
    id 249
    label "Adam"
  ]
  node [
    id 250
    label "hominid"
  ]
  node [
    id 251
    label "posta&#263;"
  ]
  node [
    id 252
    label "portrecista"
  ]
  node [
    id 253
    label "polifag"
  ]
  node [
    id 254
    label "podw&#322;adny"
  ]
  node [
    id 255
    label "dwun&#243;g"
  ]
  node [
    id 256
    label "wapniak"
  ]
  node [
    id 257
    label "duch"
  ]
  node [
    id 258
    label "os&#322;abianie"
  ]
  node [
    id 259
    label "antropochoria"
  ]
  node [
    id 260
    label "figura"
  ]
  node [
    id 261
    label "g&#322;owa"
  ]
  node [
    id 262
    label "mikrokosmos"
  ]
  node [
    id 263
    label "oddzia&#322;ywanie"
  ]
  node [
    id 264
    label "ulega&#263;"
  ]
  node [
    id 265
    label "partnerka"
  ]
  node [
    id 266
    label "pa&#324;stwo"
  ]
  node [
    id 267
    label "ulegni&#281;cie"
  ]
  node [
    id 268
    label "m&#281;&#380;yna"
  ]
  node [
    id 269
    label "samica"
  ]
  node [
    id 270
    label "babka"
  ]
  node [
    id 271
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 272
    label "doros&#322;y"
  ]
  node [
    id 273
    label "uleganie"
  ]
  node [
    id 274
    label "&#322;ono"
  ]
  node [
    id 275
    label "przekwitanie"
  ]
  node [
    id 276
    label "menopauza"
  ]
  node [
    id 277
    label "ulec"
  ]
  node [
    id 278
    label "kobita"
  ]
  node [
    id 279
    label "panna_m&#322;oda"
  ]
  node [
    id 280
    label "&#347;lubna"
  ]
  node [
    id 281
    label "ma&#322;&#380;onek"
  ]
  node [
    id 282
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 283
    label "monoteista"
  ]
  node [
    id 284
    label "krze&#347;cijanin"
  ]
  node [
    id 285
    label "&#347;wiadek_Jehowy"
  ]
  node [
    id 286
    label "wyznawca"
  ]
  node [
    id 287
    label "wierzenie"
  ]
  node [
    id 288
    label "zwolennik"
  ]
  node [
    id 289
    label "czciciel"
  ]
  node [
    id 290
    label "religia"
  ]
  node [
    id 291
    label "teista"
  ]
  node [
    id 292
    label "troch&#281;"
  ]
  node [
    id 293
    label "bli&#378;ni"
  ]
  node [
    id 294
    label "swojak"
  ]
  node [
    id 295
    label "odpowiedni"
  ]
  node [
    id 296
    label "samodzielny"
  ]
  node [
    id 297
    label "osobny"
  ]
  node [
    id 298
    label "samodzielnie"
  ]
  node [
    id 299
    label "indywidualny"
  ]
  node [
    id 300
    label "niepodleg&#322;y"
  ]
  node [
    id 301
    label "czyj&#347;"
  ]
  node [
    id 302
    label "autonomicznie"
  ]
  node [
    id 303
    label "odr&#281;bny"
  ]
  node [
    id 304
    label "sobieradzki"
  ]
  node [
    id 305
    label "w&#322;asny"
  ]
  node [
    id 306
    label "zdarzony"
  ]
  node [
    id 307
    label "odpowiednio"
  ]
  node [
    id 308
    label "specjalny"
  ]
  node [
    id 309
    label "odpowiadanie"
  ]
  node [
    id 310
    label "postawa"
  ]
  node [
    id 311
    label "faith"
  ]
  node [
    id 312
    label "konwikcja"
  ]
  node [
    id 313
    label "belief"
  ]
  node [
    id 314
    label "pogl&#261;d"
  ]
  node [
    id 315
    label "przes&#261;dny"
  ]
  node [
    id 316
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 317
    label "teoria_Arrheniusa"
  ]
  node [
    id 318
    label "zderzenie_si&#281;"
  ]
  node [
    id 319
    label "s&#261;d"
  ]
  node [
    id 320
    label "teologicznie"
  ]
  node [
    id 321
    label "nastawienie"
  ]
  node [
    id 322
    label "pozycja"
  ]
  node [
    id 323
    label "attitude"
  ]
  node [
    id 324
    label "stan"
  ]
  node [
    id 325
    label "pewno&#347;&#263;"
  ]
  node [
    id 326
    label "przes&#261;dnie"
  ]
  node [
    id 327
    label "nieracjonalny"
  ]
  node [
    id 328
    label "volunteer"
  ]
  node [
    id 329
    label "zach&#281;ca&#263;"
  ]
  node [
    id 330
    label "pozyskiwa&#263;"
  ]
  node [
    id 331
    label "act"
  ]
  node [
    id 332
    label "&#347;wiadczenie"
  ]
  node [
    id 333
    label "performance"
  ]
  node [
    id 334
    label "pracowanie"
  ]
  node [
    id 335
    label "sk&#322;adanie"
  ]
  node [
    id 336
    label "wyraz"
  ]
  node [
    id 337
    label "koszt_rodzajowy"
  ]
  node [
    id 338
    label "opowiadanie"
  ]
  node [
    id 339
    label "command"
  ]
  node [
    id 340
    label "us&#322;uga"
  ]
  node [
    id 341
    label "znaczenie"
  ]
  node [
    id 342
    label "informowanie"
  ]
  node [
    id 343
    label "zobowi&#261;zanie"
  ]
  node [
    id 344
    label "przekonywanie"
  ]
  node [
    id 345
    label "service"
  ]
  node [
    id 346
    label "czynienie_dobra"
  ]
  node [
    id 347
    label "p&#322;acenie"
  ]
  node [
    id 348
    label "tanio"
  ]
  node [
    id 349
    label "tandetny"
  ]
  node [
    id 350
    label "niedrogo"
  ]
  node [
    id 351
    label "p&#322;atnie"
  ]
  node [
    id 352
    label "najtaniej"
  ]
  node [
    id 353
    label "taniej"
  ]
  node [
    id 354
    label "niedrogi"
  ]
  node [
    id 355
    label "tandetnie"
  ]
  node [
    id 356
    label "odra&#380;aj&#261;cy"
  ]
  node [
    id 357
    label "&#380;a&#322;osny"
  ]
  node [
    id 358
    label "banalny"
  ]
  node [
    id 359
    label "kiczowaty"
  ]
  node [
    id 360
    label "nieelegancki"
  ]
  node [
    id 361
    label "kiepski"
  ]
  node [
    id 362
    label "nikczemny"
  ]
  node [
    id 363
    label "skuteczny"
  ]
  node [
    id 364
    label "ca&#322;y"
  ]
  node [
    id 365
    label "czw&#243;rka"
  ]
  node [
    id 366
    label "pos&#322;uszny"
  ]
  node [
    id 367
    label "korzystny"
  ]
  node [
    id 368
    label "drogi"
  ]
  node [
    id 369
    label "pozytywny"
  ]
  node [
    id 370
    label "moralny"
  ]
  node [
    id 371
    label "pomy&#347;lny"
  ]
  node [
    id 372
    label "powitanie"
  ]
  node [
    id 373
    label "grzeczny"
  ]
  node [
    id 374
    label "&#347;mieszny"
  ]
  node [
    id 375
    label "zwrot"
  ]
  node [
    id 376
    label "dobrze"
  ]
  node [
    id 377
    label "dobroczynny"
  ]
  node [
    id 378
    label "mi&#322;y"
  ]
  node [
    id 379
    label "etycznie"
  ]
  node [
    id 380
    label "moralnie"
  ]
  node [
    id 381
    label "warto&#347;ciowy"
  ]
  node [
    id 382
    label "pozytywnie"
  ]
  node [
    id 383
    label "fajny"
  ]
  node [
    id 384
    label "przyjemny"
  ]
  node [
    id 385
    label "po&#380;&#261;dany"
  ]
  node [
    id 386
    label "dodatnio"
  ]
  node [
    id 387
    label "o&#347;mieszenie"
  ]
  node [
    id 388
    label "o&#347;mieszanie"
  ]
  node [
    id 389
    label "&#347;miesznie"
  ]
  node [
    id 390
    label "nieadekwatny"
  ]
  node [
    id 391
    label "bawny"
  ]
  node [
    id 392
    label "niepowa&#380;ny"
  ]
  node [
    id 393
    label "dziwny"
  ]
  node [
    id 394
    label "uspokojenie_si&#281;"
  ]
  node [
    id 395
    label "wolny"
  ]
  node [
    id 396
    label "bezproblemowy"
  ]
  node [
    id 397
    label "uspokajanie_si&#281;"
  ]
  node [
    id 398
    label "spokojnie"
  ]
  node [
    id 399
    label "uspokojenie"
  ]
  node [
    id 400
    label "nietrudny"
  ]
  node [
    id 401
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 402
    label "cicho"
  ]
  node [
    id 403
    label "uspokajanie"
  ]
  node [
    id 404
    label "pos&#322;usznie"
  ]
  node [
    id 405
    label "zale&#380;ny"
  ]
  node [
    id 406
    label "uleg&#322;y"
  ]
  node [
    id 407
    label "konserwatywny"
  ]
  node [
    id 408
    label "stosowny"
  ]
  node [
    id 409
    label "grzecznie"
  ]
  node [
    id 410
    label "nijaki"
  ]
  node [
    id 411
    label "niewinny"
  ]
  node [
    id 412
    label "korzystnie"
  ]
  node [
    id 413
    label "przyjaciel"
  ]
  node [
    id 414
    label "bliski"
  ]
  node [
    id 415
    label "drogo"
  ]
  node [
    id 416
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 417
    label "kompletny"
  ]
  node [
    id 418
    label "zdr&#243;w"
  ]
  node [
    id 419
    label "ca&#322;o"
  ]
  node [
    id 420
    label "du&#380;y"
  ]
  node [
    id 421
    label "calu&#347;ko"
  ]
  node [
    id 422
    label "podobny"
  ]
  node [
    id 423
    label "&#380;ywy"
  ]
  node [
    id 424
    label "pe&#322;ny"
  ]
  node [
    id 425
    label "jedyny"
  ]
  node [
    id 426
    label "sprawny"
  ]
  node [
    id 427
    label "skutkowanie"
  ]
  node [
    id 428
    label "poskutkowanie"
  ]
  node [
    id 429
    label "skutecznie"
  ]
  node [
    id 430
    label "pomy&#347;lnie"
  ]
  node [
    id 431
    label "zbi&#243;r"
  ]
  node [
    id 432
    label "przedtrzonowiec"
  ]
  node [
    id 433
    label "trafienie"
  ]
  node [
    id 434
    label "osada"
  ]
  node [
    id 435
    label "blotka"
  ]
  node [
    id 436
    label "p&#322;yta_winylowa"
  ]
  node [
    id 437
    label "cyfra"
  ]
  node [
    id 438
    label "pok&#243;j"
  ]
  node [
    id 439
    label "obiekt"
  ]
  node [
    id 440
    label "stopie&#324;"
  ]
  node [
    id 441
    label "arkusz_drukarski"
  ]
  node [
    id 442
    label "zaprz&#281;g"
  ]
  node [
    id 443
    label "toto-lotek"
  ]
  node [
    id 444
    label "&#263;wiartka"
  ]
  node [
    id 445
    label "&#322;&#243;dka"
  ]
  node [
    id 446
    label "four"
  ]
  node [
    id 447
    label "minialbum"
  ]
  node [
    id 448
    label "hotel"
  ]
  node [
    id 449
    label "punkt"
  ]
  node [
    id 450
    label "zmiana"
  ]
  node [
    id 451
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 452
    label "turn"
  ]
  node [
    id 453
    label "wyra&#380;enie"
  ]
  node [
    id 454
    label "fraza_czasownikowa"
  ]
  node [
    id 455
    label "turning"
  ]
  node [
    id 456
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 457
    label "skr&#281;t"
  ]
  node [
    id 458
    label "jednostka_leksykalna"
  ]
  node [
    id 459
    label "obr&#243;t"
  ]
  node [
    id 460
    label "spotkanie"
  ]
  node [
    id 461
    label "pozdrowienie"
  ]
  node [
    id 462
    label "welcome"
  ]
  node [
    id 463
    label "zwyczaj"
  ]
  node [
    id 464
    label "greeting"
  ]
  node [
    id 465
    label "wybranek"
  ]
  node [
    id 466
    label "sk&#322;onny"
  ]
  node [
    id 467
    label "kochanek"
  ]
  node [
    id 468
    label "mi&#322;o"
  ]
  node [
    id 469
    label "dyplomata"
  ]
  node [
    id 470
    label "umi&#322;owany"
  ]
  node [
    id 471
    label "kochanie"
  ]
  node [
    id 472
    label "przyjemnie"
  ]
  node [
    id 473
    label "wiele"
  ]
  node [
    id 474
    label "lepiej"
  ]
  node [
    id 475
    label "dobroczynnie"
  ]
  node [
    id 476
    label "spo&#322;eczny"
  ]
  node [
    id 477
    label "pewny"
  ]
  node [
    id 478
    label "porz&#261;dny"
  ]
  node [
    id 479
    label "stabilnie"
  ]
  node [
    id 480
    label "trwa&#322;y"
  ]
  node [
    id 481
    label "sta&#322;y"
  ]
  node [
    id 482
    label "schludny"
  ]
  node [
    id 483
    label "porz&#261;dnie"
  ]
  node [
    id 484
    label "intensywny"
  ]
  node [
    id 485
    label "ch&#281;dogi"
  ]
  node [
    id 486
    label "przyzwoity"
  ]
  node [
    id 487
    label "upewnienie_si&#281;"
  ]
  node [
    id 488
    label "wiarygodny"
  ]
  node [
    id 489
    label "mo&#380;liwy"
  ]
  node [
    id 490
    label "ufanie"
  ]
  node [
    id 491
    label "upewnianie_si&#281;"
  ]
  node [
    id 492
    label "bezpieczny"
  ]
  node [
    id 493
    label "pewnie"
  ]
  node [
    id 494
    label "jednakowy"
  ]
  node [
    id 495
    label "regularny"
  ]
  node [
    id 496
    label "stale"
  ]
  node [
    id 497
    label "ustalenie_si&#281;"
  ]
  node [
    id 498
    label "umocnienie"
  ]
  node [
    id 499
    label "trwale"
  ]
  node [
    id 500
    label "ustalanie_si&#281;"
  ]
  node [
    id 501
    label "utrwalanie_si&#281;"
  ]
  node [
    id 502
    label "nieruchomy"
  ]
  node [
    id 503
    label "utrwalenie_si&#281;"
  ]
  node [
    id 504
    label "mocny"
  ]
  node [
    id 505
    label "wieczny"
  ]
  node [
    id 506
    label "umacnianie"
  ]
  node [
    id 507
    label "Arabia"
  ]
  node [
    id 508
    label "saudyjski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 295
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 215
  ]
  edge [
    source 13
    target 216
  ]
  edge [
    source 13
    target 217
  ]
  edge [
    source 13
    target 218
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 219
  ]
  edge [
    source 13
    target 220
  ]
  edge [
    source 13
    target 221
  ]
  edge [
    source 13
    target 222
  ]
  edge [
    source 13
    target 223
  ]
  edge [
    source 13
    target 224
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 406
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 233
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 433
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 306
  ]
  edge [
    source 13
    target 307
  ]
  edge [
    source 13
    target 308
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 218
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 287
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 213
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 502
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 505
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 507
    target 508
  ]
]
