graph [
  node [
    id 0
    label "czas"
    origin "text"
  ]
  node [
    id 1
    label "sesja"
    origin "text"
  ]
  node [
    id 2
    label "zada&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pytanie"
    origin "text"
  ]
  node [
    id 4
    label "rzetelno&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "relacja"
    origin "text"
  ]
  node [
    id 6
    label "reporterski"
    origin "text"
  ]
  node [
    id 7
    label "sprawi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "sam"
    origin "text"
  ]
  node [
    id 9
    label "zgromadzenie"
    origin "text"
  ]
  node [
    id 10
    label "jednia"
    origin "text"
  ]
  node [
    id 11
    label "obserwator"
    origin "text"
  ]
  node [
    id 12
    label "odebra&#263;"
    origin "text"
  ]
  node [
    id 13
    label "jako"
    origin "text"
  ]
  node [
    id 14
    label "happening"
    origin "text"
  ]
  node [
    id 15
    label "manifestacja"
    origin "text"
  ]
  node [
    id 16
    label "pogarda"
    origin "text"
  ]
  node [
    id 17
    label "zastanawia&#263;"
    origin "text"
  ]
  node [
    id 18
    label "si&#281;"
    origin "text"
  ]
  node [
    id 19
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 20
    label "nad"
    origin "text"
  ]
  node [
    id 21
    label "wzajemny"
    origin "text"
  ]
  node [
    id 22
    label "odniesienie"
    origin "text"
  ]
  node [
    id 23
    label "informacja"
    origin "text"
  ]
  node [
    id 24
    label "komentarz"
    origin "text"
  ]
  node [
    id 25
    label "redaktor"
    origin "text"
  ]
  node [
    id 26
    label "naczelne"
    origin "text"
  ]
  node [
    id 27
    label "zapyta&#263;"
    origin "text"
  ]
  node [
    id 28
    label "polityka"
    origin "text"
  ]
  node [
    id 29
    label "redakcyjny"
    origin "text"
  ]
  node [
    id 30
    label "jaka"
    origin "text"
  ]
  node [
    id 31
    label "przyj&#261;&#263;"
    origin "text"
  ]
  node [
    id 32
    label "wobec"
    origin "text"
  ]
  node [
    id 33
    label "krzy&#380;"
    origin "text"
  ]
  node [
    id 34
    label "uzna&#263;"
    origin "text"
  ]
  node [
    id 35
    label "ten"
    origin "text"
  ]
  node [
    id 36
    label "sprawa"
    origin "text"
  ]
  node [
    id 37
    label "istotny"
    origin "text"
  ]
  node [
    id 38
    label "dla"
    origin "text"
  ]
  node [
    id 39
    label "swoje"
    origin "text"
  ]
  node [
    id 40
    label "odbiorca"
    origin "text"
  ]
  node [
    id 41
    label "le&#380;e&#263;"
    origin "text"
  ]
  node [
    id 42
    label "podstawa"
    origin "text"
  ]
  node [
    id 43
    label "taki"
    origin "text"
  ]
  node [
    id 44
    label "decyzja"
    origin "text"
  ]
  node [
    id 45
    label "te&#380;"
    origin "text"
  ]
  node [
    id 46
    label "dlaczego"
    origin "text"
  ]
  node [
    id 47
    label "pewne"
    origin "text"
  ]
  node [
    id 48
    label "w&#261;tek"
    origin "text"
  ]
  node [
    id 49
    label "lub"
    origin "text"
  ]
  node [
    id 50
    label "uwypukli&#263;"
    origin "text"
  ]
  node [
    id 51
    label "inny"
    origin "text"
  ]
  node [
    id 52
    label "nieistotny"
    origin "text"
  ]
  node [
    id 53
    label "zdecydowa&#263;"
    origin "text"
  ]
  node [
    id 54
    label "przekaza&#263;"
    origin "text"
  ]
  node [
    id 55
    label "poprzedzanie"
  ]
  node [
    id 56
    label "czasoprzestrze&#324;"
  ]
  node [
    id 57
    label "laba"
  ]
  node [
    id 58
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 59
    label "chronometria"
  ]
  node [
    id 60
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 61
    label "rachuba_czasu"
  ]
  node [
    id 62
    label "przep&#322;ywanie"
  ]
  node [
    id 63
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 64
    label "czasokres"
  ]
  node [
    id 65
    label "odczyt"
  ]
  node [
    id 66
    label "chwila"
  ]
  node [
    id 67
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 68
    label "dzieje"
  ]
  node [
    id 69
    label "kategoria_gramatyczna"
  ]
  node [
    id 70
    label "poprzedzenie"
  ]
  node [
    id 71
    label "trawienie"
  ]
  node [
    id 72
    label "pochodzi&#263;"
  ]
  node [
    id 73
    label "period"
  ]
  node [
    id 74
    label "okres_czasu"
  ]
  node [
    id 75
    label "poprzedza&#263;"
  ]
  node [
    id 76
    label "schy&#322;ek"
  ]
  node [
    id 77
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 78
    label "odwlekanie_si&#281;"
  ]
  node [
    id 79
    label "zegar"
  ]
  node [
    id 80
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 81
    label "czwarty_wymiar"
  ]
  node [
    id 82
    label "pochodzenie"
  ]
  node [
    id 83
    label "koniugacja"
  ]
  node [
    id 84
    label "Zeitgeist"
  ]
  node [
    id 85
    label "trawi&#263;"
  ]
  node [
    id 86
    label "pogoda"
  ]
  node [
    id 87
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 88
    label "poprzedzi&#263;"
  ]
  node [
    id 89
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 90
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 91
    label "time_period"
  ]
  node [
    id 92
    label "time"
  ]
  node [
    id 93
    label "blok"
  ]
  node [
    id 94
    label "handout"
  ]
  node [
    id 95
    label "pomiar"
  ]
  node [
    id 96
    label "lecture"
  ]
  node [
    id 97
    label "reading"
  ]
  node [
    id 98
    label "podawanie"
  ]
  node [
    id 99
    label "wyk&#322;ad"
  ]
  node [
    id 100
    label "potrzyma&#263;"
  ]
  node [
    id 101
    label "warunki"
  ]
  node [
    id 102
    label "pok&#243;j"
  ]
  node [
    id 103
    label "atak"
  ]
  node [
    id 104
    label "program"
  ]
  node [
    id 105
    label "zjawisko"
  ]
  node [
    id 106
    label "meteorology"
  ]
  node [
    id 107
    label "weather"
  ]
  node [
    id 108
    label "prognoza_meteorologiczna"
  ]
  node [
    id 109
    label "czas_wolny"
  ]
  node [
    id 110
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 111
    label "metrologia"
  ]
  node [
    id 112
    label "godzinnik"
  ]
  node [
    id 113
    label "bicie"
  ]
  node [
    id 114
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 115
    label "wahad&#322;o"
  ]
  node [
    id 116
    label "kurant"
  ]
  node [
    id 117
    label "cyferblat"
  ]
  node [
    id 118
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 119
    label "nabicie"
  ]
  node [
    id 120
    label "werk"
  ]
  node [
    id 121
    label "czasomierz"
  ]
  node [
    id 122
    label "tyka&#263;"
  ]
  node [
    id 123
    label "tykn&#261;&#263;"
  ]
  node [
    id 124
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 125
    label "urz&#261;dzenie"
  ]
  node [
    id 126
    label "kotwica"
  ]
  node [
    id 127
    label "fleksja"
  ]
  node [
    id 128
    label "liczba"
  ]
  node [
    id 129
    label "coupling"
  ]
  node [
    id 130
    label "osoba"
  ]
  node [
    id 131
    label "tryb"
  ]
  node [
    id 132
    label "czasownik"
  ]
  node [
    id 133
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 134
    label "orz&#281;sek"
  ]
  node [
    id 135
    label "usuwa&#263;"
  ]
  node [
    id 136
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 137
    label "lutowa&#263;"
  ]
  node [
    id 138
    label "marnowa&#263;"
  ]
  node [
    id 139
    label "przetrawia&#263;"
  ]
  node [
    id 140
    label "poch&#322;ania&#263;"
  ]
  node [
    id 141
    label "digest"
  ]
  node [
    id 142
    label "metal"
  ]
  node [
    id 143
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 144
    label "sp&#281;dza&#263;"
  ]
  node [
    id 145
    label "digestion"
  ]
  node [
    id 146
    label "unicestwianie"
  ]
  node [
    id 147
    label "sp&#281;dzanie"
  ]
  node [
    id 148
    label "contemplation"
  ]
  node [
    id 149
    label "rozk&#322;adanie"
  ]
  node [
    id 150
    label "marnowanie"
  ]
  node [
    id 151
    label "proces_fizjologiczny"
  ]
  node [
    id 152
    label "przetrawianie"
  ]
  node [
    id 153
    label "perystaltyka"
  ]
  node [
    id 154
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 155
    label "zaczynanie_si&#281;"
  ]
  node [
    id 156
    label "str&#243;j"
  ]
  node [
    id 157
    label "wynikanie"
  ]
  node [
    id 158
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 159
    label "origin"
  ]
  node [
    id 160
    label "background"
  ]
  node [
    id 161
    label "geneza"
  ]
  node [
    id 162
    label "beginning"
  ]
  node [
    id 163
    label "przeby&#263;"
  ]
  node [
    id 164
    label "min&#261;&#263;"
  ]
  node [
    id 165
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 166
    label "swimming"
  ]
  node [
    id 167
    label "zago&#347;ci&#263;"
  ]
  node [
    id 168
    label "cross"
  ]
  node [
    id 169
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 170
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 171
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 172
    label "przebywa&#263;"
  ]
  node [
    id 173
    label "pour"
  ]
  node [
    id 174
    label "carry"
  ]
  node [
    id 175
    label "sail"
  ]
  node [
    id 176
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 177
    label "go&#347;ci&#263;"
  ]
  node [
    id 178
    label "mija&#263;"
  ]
  node [
    id 179
    label "proceed"
  ]
  node [
    id 180
    label "mini&#281;cie"
  ]
  node [
    id 181
    label "doznanie"
  ]
  node [
    id 182
    label "zaistnienie"
  ]
  node [
    id 183
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 184
    label "przebycie"
  ]
  node [
    id 185
    label "cruise"
  ]
  node [
    id 186
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 187
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 188
    label "zjawianie_si&#281;"
  ]
  node [
    id 189
    label "przebywanie"
  ]
  node [
    id 190
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 191
    label "mijanie"
  ]
  node [
    id 192
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 193
    label "zaznawanie"
  ]
  node [
    id 194
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 195
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 196
    label "flux"
  ]
  node [
    id 197
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 198
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 199
    label "zrobi&#263;"
  ]
  node [
    id 200
    label "opatrzy&#263;"
  ]
  node [
    id 201
    label "overwhelm"
  ]
  node [
    id 202
    label "opatrywanie"
  ]
  node [
    id 203
    label "odej&#347;cie"
  ]
  node [
    id 204
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 205
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 206
    label "zanikni&#281;cie"
  ]
  node [
    id 207
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 208
    label "ciecz"
  ]
  node [
    id 209
    label "opuszczenie"
  ]
  node [
    id 210
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 211
    label "departure"
  ]
  node [
    id 212
    label "oddalenie_si&#281;"
  ]
  node [
    id 213
    label "date"
  ]
  node [
    id 214
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 215
    label "wynika&#263;"
  ]
  node [
    id 216
    label "fall"
  ]
  node [
    id 217
    label "poby&#263;"
  ]
  node [
    id 218
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 219
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 220
    label "bolt"
  ]
  node [
    id 221
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 222
    label "spowodowa&#263;"
  ]
  node [
    id 223
    label "uda&#263;_si&#281;"
  ]
  node [
    id 224
    label "opatrzenie"
  ]
  node [
    id 225
    label "zdarzenie_si&#281;"
  ]
  node [
    id 226
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 227
    label "progress"
  ]
  node [
    id 228
    label "opatrywa&#263;"
  ]
  node [
    id 229
    label "epoka"
  ]
  node [
    id 230
    label "charakter"
  ]
  node [
    id 231
    label "flow"
  ]
  node [
    id 232
    label "choroba_przyrodzona"
  ]
  node [
    id 233
    label "ciota"
  ]
  node [
    id 234
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 235
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 236
    label "kres"
  ]
  node [
    id 237
    label "przestrze&#324;"
  ]
  node [
    id 238
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 239
    label "egzamin"
  ]
  node [
    id 240
    label "dzie&#324;_pracy"
  ]
  node [
    id 241
    label "dogrywka"
  ]
  node [
    id 242
    label "spotkanie"
  ]
  node [
    id 243
    label "seria"
  ]
  node [
    id 244
    label "rok_akademicki"
  ]
  node [
    id 245
    label "konsylium"
  ]
  node [
    id 246
    label "psychoterapia"
  ]
  node [
    id 247
    label "obiekt"
  ]
  node [
    id 248
    label "sesyjka"
  ]
  node [
    id 249
    label "conference"
  ]
  node [
    id 250
    label "dyskusja"
  ]
  node [
    id 251
    label "gie&#322;da_papier&#243;w_warto&#347;ciowych"
  ]
  node [
    id 252
    label "gathering"
  ]
  node [
    id 253
    label "zawarcie"
  ]
  node [
    id 254
    label "wydarzenie"
  ]
  node [
    id 255
    label "znajomy"
  ]
  node [
    id 256
    label "powitanie"
  ]
  node [
    id 257
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 258
    label "spowodowanie"
  ]
  node [
    id 259
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 260
    label "znalezienie"
  ]
  node [
    id 261
    label "match"
  ]
  node [
    id 262
    label "employment"
  ]
  node [
    id 263
    label "po&#380;egnanie"
  ]
  node [
    id 264
    label "gather"
  ]
  node [
    id 265
    label "spotykanie"
  ]
  node [
    id 266
    label "spotkanie_si&#281;"
  ]
  node [
    id 267
    label "co&#347;"
  ]
  node [
    id 268
    label "budynek"
  ]
  node [
    id 269
    label "thing"
  ]
  node [
    id 270
    label "poj&#281;cie"
  ]
  node [
    id 271
    label "rzecz"
  ]
  node [
    id 272
    label "strona"
  ]
  node [
    id 273
    label "set"
  ]
  node [
    id 274
    label "przebieg"
  ]
  node [
    id 275
    label "zbi&#243;r"
  ]
  node [
    id 276
    label "jednostka"
  ]
  node [
    id 277
    label "jednostka_systematyczna"
  ]
  node [
    id 278
    label "stage_set"
  ]
  node [
    id 279
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 280
    label "d&#378;wi&#281;k"
  ]
  node [
    id 281
    label "komplet"
  ]
  node [
    id 282
    label "line"
  ]
  node [
    id 283
    label "sekwencja"
  ]
  node [
    id 284
    label "zestawienie"
  ]
  node [
    id 285
    label "partia"
  ]
  node [
    id 286
    label "produkcja"
  ]
  node [
    id 287
    label "rozmowa"
  ]
  node [
    id 288
    label "sympozjon"
  ]
  node [
    id 289
    label "terapia"
  ]
  node [
    id 290
    label "oblewanie"
  ]
  node [
    id 291
    label "faza"
  ]
  node [
    id 292
    label "sesja_egzaminacyjna"
  ]
  node [
    id 293
    label "oblewa&#263;"
  ]
  node [
    id 294
    label "praca_pisemna"
  ]
  node [
    id 295
    label "sprawdzian"
  ]
  node [
    id 296
    label "magiel"
  ]
  node [
    id 297
    label "pr&#243;ba"
  ]
  node [
    id 298
    label "arkusz"
  ]
  node [
    id 299
    label "examination"
  ]
  node [
    id 300
    label "dodatek"
  ]
  node [
    id 301
    label "rozgrywka"
  ]
  node [
    id 302
    label "konsultacja"
  ]
  node [
    id 303
    label "obrady"
  ]
  node [
    id 304
    label "d&#378;wign&#261;&#263;"
  ]
  node [
    id 305
    label "zaszkodzi&#263;"
  ]
  node [
    id 306
    label "put"
  ]
  node [
    id 307
    label "deal"
  ]
  node [
    id 308
    label "zaj&#261;&#263;"
  ]
  node [
    id 309
    label "distribute"
  ]
  node [
    id 310
    label "nakarmi&#263;"
  ]
  node [
    id 311
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 312
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 313
    label "obowi&#261;za&#263;"
  ]
  node [
    id 314
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 315
    label "perpetrate"
  ]
  node [
    id 316
    label "hurt"
  ]
  node [
    id 317
    label "injury"
  ]
  node [
    id 318
    label "wyr&#281;czy&#263;"
  ]
  node [
    id 319
    label "da&#263;"
  ]
  node [
    id 320
    label "feed"
  ]
  node [
    id 321
    label "poda&#263;"
  ]
  node [
    id 322
    label "utrzyma&#263;"
  ]
  node [
    id 323
    label "zapoda&#263;"
  ]
  node [
    id 324
    label "wm&#243;wi&#263;"
  ]
  node [
    id 325
    label "invest"
  ]
  node [
    id 326
    label "plant"
  ]
  node [
    id 327
    label "load"
  ]
  node [
    id 328
    label "ubra&#263;"
  ]
  node [
    id 329
    label "oblec_si&#281;"
  ]
  node [
    id 330
    label "oblec"
  ]
  node [
    id 331
    label "pokry&#263;"
  ]
  node [
    id 332
    label "podwin&#261;&#263;"
  ]
  node [
    id 333
    label "przewidzie&#263;"
  ]
  node [
    id 334
    label "przyodzia&#263;"
  ]
  node [
    id 335
    label "jell"
  ]
  node [
    id 336
    label "umie&#347;ci&#263;"
  ]
  node [
    id 337
    label "insert"
  ]
  node [
    id 338
    label "utworzy&#263;"
  ]
  node [
    id 339
    label "zap&#322;aci&#263;"
  ]
  node [
    id 340
    label "create"
  ]
  node [
    id 341
    label "install"
  ]
  node [
    id 342
    label "map"
  ]
  node [
    id 343
    label "zapanowa&#263;"
  ]
  node [
    id 344
    label "rozciekawi&#263;"
  ]
  node [
    id 345
    label "skorzysta&#263;"
  ]
  node [
    id 346
    label "komornik"
  ]
  node [
    id 347
    label "ulokowa&#263;_si&#281;"
  ]
  node [
    id 348
    label "klasyfikacja"
  ]
  node [
    id 349
    label "wype&#322;ni&#263;"
  ]
  node [
    id 350
    label "topographic_point"
  ]
  node [
    id 351
    label "obj&#261;&#263;"
  ]
  node [
    id 352
    label "seize"
  ]
  node [
    id 353
    label "interest"
  ]
  node [
    id 354
    label "anektowa&#263;"
  ]
  node [
    id 355
    label "prosecute"
  ]
  node [
    id 356
    label "dostarczy&#263;"
  ]
  node [
    id 357
    label "usytuowa&#263;_si&#281;"
  ]
  node [
    id 358
    label "bankrupt"
  ]
  node [
    id 359
    label "sorb"
  ]
  node [
    id 360
    label "zabra&#263;"
  ]
  node [
    id 361
    label "wzi&#261;&#263;"
  ]
  node [
    id 362
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 363
    label "do"
  ]
  node [
    id 364
    label "wzbudzi&#263;"
  ]
  node [
    id 365
    label "ulepszy&#263;"
  ]
  node [
    id 366
    label "znie&#347;&#263;"
  ]
  node [
    id 367
    label "heft"
  ]
  node [
    id 368
    label "podnie&#347;&#263;"
  ]
  node [
    id 369
    label "arise"
  ]
  node [
    id 370
    label "odbudowa&#263;"
  ]
  node [
    id 371
    label "ud&#378;wign&#261;&#263;"
  ]
  node [
    id 372
    label "raise"
  ]
  node [
    id 373
    label "cover"
  ]
  node [
    id 374
    label "gem"
  ]
  node [
    id 375
    label "kompozycja"
  ]
  node [
    id 376
    label "runda"
  ]
  node [
    id 377
    label "muzyka"
  ]
  node [
    id 378
    label "zestaw"
  ]
  node [
    id 379
    label "wypytanie"
  ]
  node [
    id 380
    label "egzaminowanie"
  ]
  node [
    id 381
    label "zwracanie_si&#281;"
  ]
  node [
    id 382
    label "wywo&#322;ywanie"
  ]
  node [
    id 383
    label "rozpytywanie"
  ]
  node [
    id 384
    label "wypowiedzenie"
  ]
  node [
    id 385
    label "wypowied&#378;"
  ]
  node [
    id 386
    label "problemat"
  ]
  node [
    id 387
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 388
    label "problematyka"
  ]
  node [
    id 389
    label "zadanie"
  ]
  node [
    id 390
    label "odpowiada&#263;"
  ]
  node [
    id 391
    label "przes&#322;uchiwanie"
  ]
  node [
    id 392
    label "question"
  ]
  node [
    id 393
    label "sprawdzanie"
  ]
  node [
    id 394
    label "odpowiadanie"
  ]
  node [
    id 395
    label "survey"
  ]
  node [
    id 396
    label "konwersja"
  ]
  node [
    id 397
    label "notice"
  ]
  node [
    id 398
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 399
    label "przepowiedzenie"
  ]
  node [
    id 400
    label "rozwi&#261;zanie"
  ]
  node [
    id 401
    label "generowa&#263;"
  ]
  node [
    id 402
    label "wydanie"
  ]
  node [
    id 403
    label "message"
  ]
  node [
    id 404
    label "generowanie"
  ]
  node [
    id 405
    label "wydobycie"
  ]
  node [
    id 406
    label "zwerbalizowanie"
  ]
  node [
    id 407
    label "szyk"
  ]
  node [
    id 408
    label "notification"
  ]
  node [
    id 409
    label "powiedzenie"
  ]
  node [
    id 410
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 411
    label "denunciation"
  ]
  node [
    id 412
    label "wyra&#380;enie"
  ]
  node [
    id 413
    label "pos&#322;uchanie"
  ]
  node [
    id 414
    label "s&#261;d"
  ]
  node [
    id 415
    label "sparafrazowanie"
  ]
  node [
    id 416
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 417
    label "strawestowa&#263;"
  ]
  node [
    id 418
    label "sparafrazowa&#263;"
  ]
  node [
    id 419
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 420
    label "trawestowa&#263;"
  ]
  node [
    id 421
    label "sformu&#322;owanie"
  ]
  node [
    id 422
    label "parafrazowanie"
  ]
  node [
    id 423
    label "ozdobnik"
  ]
  node [
    id 424
    label "delimitacja"
  ]
  node [
    id 425
    label "parafrazowa&#263;"
  ]
  node [
    id 426
    label "stylizacja"
  ]
  node [
    id 427
    label "komunikat"
  ]
  node [
    id 428
    label "trawestowanie"
  ]
  node [
    id 429
    label "strawestowanie"
  ]
  node [
    id 430
    label "rezultat"
  ]
  node [
    id 431
    label "zaj&#281;cie"
  ]
  node [
    id 432
    label "yield"
  ]
  node [
    id 433
    label "zaszkodzenie"
  ]
  node [
    id 434
    label "za&#322;o&#380;enie"
  ]
  node [
    id 435
    label "duty"
  ]
  node [
    id 436
    label "powierzanie"
  ]
  node [
    id 437
    label "work"
  ]
  node [
    id 438
    label "problem"
  ]
  node [
    id 439
    label "przepisanie"
  ]
  node [
    id 440
    label "nakarmienie"
  ]
  node [
    id 441
    label "przepisa&#263;"
  ]
  node [
    id 442
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 443
    label "czynno&#347;&#263;"
  ]
  node [
    id 444
    label "zobowi&#261;zanie"
  ]
  node [
    id 445
    label "kognicja"
  ]
  node [
    id 446
    label "object"
  ]
  node [
    id 447
    label "rozprawa"
  ]
  node [
    id 448
    label "temat"
  ]
  node [
    id 449
    label "szczeg&#243;&#322;"
  ]
  node [
    id 450
    label "proposition"
  ]
  node [
    id 451
    label "przes&#322;anka"
  ]
  node [
    id 452
    label "idea"
  ]
  node [
    id 453
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 454
    label "ustalenie"
  ]
  node [
    id 455
    label "redagowanie"
  ]
  node [
    id 456
    label "ustalanie"
  ]
  node [
    id 457
    label "dociekanie"
  ]
  node [
    id 458
    label "robienie"
  ]
  node [
    id 459
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 460
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 461
    label "investigation"
  ]
  node [
    id 462
    label "macanie"
  ]
  node [
    id 463
    label "usi&#322;owanie"
  ]
  node [
    id 464
    label "penetrowanie"
  ]
  node [
    id 465
    label "przymierzanie"
  ]
  node [
    id 466
    label "przymierzenie"
  ]
  node [
    id 467
    label "zbadanie"
  ]
  node [
    id 468
    label "wypytywanie"
  ]
  node [
    id 469
    label "react"
  ]
  node [
    id 470
    label "dawa&#263;"
  ]
  node [
    id 471
    label "by&#263;"
  ]
  node [
    id 472
    label "ponosi&#263;"
  ]
  node [
    id 473
    label "report"
  ]
  node [
    id 474
    label "equate"
  ]
  node [
    id 475
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 476
    label "answer"
  ]
  node [
    id 477
    label "powodowa&#263;"
  ]
  node [
    id 478
    label "tone"
  ]
  node [
    id 479
    label "contend"
  ]
  node [
    id 480
    label "reagowa&#263;"
  ]
  node [
    id 481
    label "impart"
  ]
  node [
    id 482
    label "reagowanie"
  ]
  node [
    id 483
    label "dawanie"
  ]
  node [
    id 484
    label "powodowanie"
  ]
  node [
    id 485
    label "bycie"
  ]
  node [
    id 486
    label "pokutowanie"
  ]
  node [
    id 487
    label "odpowiedzialny"
  ]
  node [
    id 488
    label "winny"
  ]
  node [
    id 489
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 490
    label "picie_piwa"
  ]
  node [
    id 491
    label "odpowiedni"
  ]
  node [
    id 492
    label "parry"
  ]
  node [
    id 493
    label "fit"
  ]
  node [
    id 494
    label "dzianie_si&#281;"
  ]
  node [
    id 495
    label "rendition"
  ]
  node [
    id 496
    label "ponoszenie"
  ]
  node [
    id 497
    label "rozmawianie"
  ]
  node [
    id 498
    label "podchodzi&#263;"
  ]
  node [
    id 499
    label "&#263;wiczenie"
  ]
  node [
    id 500
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 501
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 502
    label "kontrola"
  ]
  node [
    id 503
    label "dydaktyka"
  ]
  node [
    id 504
    label "przepytywanie"
  ]
  node [
    id 505
    label "zdawanie"
  ]
  node [
    id 506
    label "oznajmianie"
  ]
  node [
    id 507
    label "wzywanie"
  ]
  node [
    id 508
    label "development"
  ]
  node [
    id 509
    label "exploitation"
  ]
  node [
    id 510
    label "w&#322;&#261;czanie"
  ]
  node [
    id 511
    label "s&#322;uchanie"
  ]
  node [
    id 512
    label "nastawienie"
  ]
  node [
    id 513
    label "ustawienie"
  ]
  node [
    id 514
    label "z&#322;amanie"
  ]
  node [
    id 515
    label "gotowanie_si&#281;"
  ]
  node [
    id 516
    label "oddzia&#322;anie"
  ]
  node [
    id 517
    label "ponastawianie"
  ]
  node [
    id 518
    label "bearing"
  ]
  node [
    id 519
    label "powaga"
  ]
  node [
    id 520
    label "z&#322;o&#380;enie"
  ]
  node [
    id 521
    label "podej&#347;cie"
  ]
  node [
    id 522
    label "umieszczenie"
  ]
  node [
    id 523
    label "cecha"
  ]
  node [
    id 524
    label "w&#322;&#261;czenie"
  ]
  node [
    id 525
    label "ukierunkowanie"
  ]
  node [
    id 526
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 527
    label "ustosunkowywa&#263;"
  ]
  node [
    id 528
    label "wi&#261;zanie"
  ]
  node [
    id 529
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 530
    label "sprawko"
  ]
  node [
    id 531
    label "bratnia_dusza"
  ]
  node [
    id 532
    label "trasa"
  ]
  node [
    id 533
    label "zwi&#261;zanie"
  ]
  node [
    id 534
    label "ustosunkowywanie"
  ]
  node [
    id 535
    label "marriage"
  ]
  node [
    id 536
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 537
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 538
    label "ustosunkowa&#263;"
  ]
  node [
    id 539
    label "korespondent"
  ]
  node [
    id 540
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 541
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 542
    label "zwi&#261;za&#263;"
  ]
  node [
    id 543
    label "podzbi&#243;r"
  ]
  node [
    id 544
    label "ustosunkowanie"
  ]
  node [
    id 545
    label "zwi&#261;zek"
  ]
  node [
    id 546
    label "zrelatywizowa&#263;"
  ]
  node [
    id 547
    label "zrelatywizowanie"
  ]
  node [
    id 548
    label "podporz&#261;dkowanie"
  ]
  node [
    id 549
    label "niesamodzielno&#347;&#263;"
  ]
  node [
    id 550
    label "status"
  ]
  node [
    id 551
    label "relatywizowa&#263;"
  ]
  node [
    id 552
    label "relatywizowanie"
  ]
  node [
    id 553
    label "odwadnia&#263;"
  ]
  node [
    id 554
    label "odwodni&#263;"
  ]
  node [
    id 555
    label "powi&#261;zanie"
  ]
  node [
    id 556
    label "konstytucja"
  ]
  node [
    id 557
    label "organizacja"
  ]
  node [
    id 558
    label "odwadnianie"
  ]
  node [
    id 559
    label "odwodnienie"
  ]
  node [
    id 560
    label "marketing_afiliacyjny"
  ]
  node [
    id 561
    label "substancja_chemiczna"
  ]
  node [
    id 562
    label "koligacja"
  ]
  node [
    id 563
    label "lokant"
  ]
  node [
    id 564
    label "azeotrop"
  ]
  node [
    id 565
    label "droga"
  ]
  node [
    id 566
    label "infrastruktura"
  ]
  node [
    id 567
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 568
    label "w&#281;ze&#322;"
  ]
  node [
    id 569
    label "marszrutyzacja"
  ]
  node [
    id 570
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 571
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 572
    label "podbieg"
  ]
  node [
    id 573
    label "sublimit"
  ]
  node [
    id 574
    label "nadzbi&#243;r"
  ]
  node [
    id 575
    label "subset"
  ]
  node [
    id 576
    label "przedstawienie"
  ]
  node [
    id 577
    label "formu&#322;owanie"
  ]
  node [
    id 578
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 579
    label "odmienno&#347;&#263;"
  ]
  node [
    id 580
    label "ciche_dni"
  ]
  node [
    id 581
    label "zaburzenie"
  ]
  node [
    id 582
    label "contrariety"
  ]
  node [
    id 583
    label "stan"
  ]
  node [
    id 584
    label "konflikt"
  ]
  node [
    id 585
    label "brak"
  ]
  node [
    id 586
    label "formu&#322;owa&#263;"
  ]
  node [
    id 587
    label "ograniczenie"
  ]
  node [
    id 588
    label "po&#322;&#261;czenie"
  ]
  node [
    id 589
    label "do&#322;&#261;czenie"
  ]
  node [
    id 590
    label "opakowanie"
  ]
  node [
    id 591
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 592
    label "attachment"
  ]
  node [
    id 593
    label "obezw&#322;adnienie"
  ]
  node [
    id 594
    label "zawi&#261;zanie"
  ]
  node [
    id 595
    label "wi&#281;&#378;"
  ]
  node [
    id 596
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 597
    label "tying"
  ]
  node [
    id 598
    label "st&#281;&#380;enie"
  ]
  node [
    id 599
    label "affiliation"
  ]
  node [
    id 600
    label "fastening"
  ]
  node [
    id 601
    label "zaprawa"
  ]
  node [
    id 602
    label "nawi&#261;zanie_si&#281;"
  ]
  node [
    id 603
    label "z&#322;&#261;czenie"
  ]
  node [
    id 604
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 605
    label "consort"
  ]
  node [
    id 606
    label "cement"
  ]
  node [
    id 607
    label "opakowa&#263;"
  ]
  node [
    id 608
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 609
    label "relate"
  ]
  node [
    id 610
    label "form"
  ]
  node [
    id 611
    label "tobo&#322;ek"
  ]
  node [
    id 612
    label "unify"
  ]
  node [
    id 613
    label "incorporate"
  ]
  node [
    id 614
    label "bind"
  ]
  node [
    id 615
    label "zawi&#261;za&#263;"
  ]
  node [
    id 616
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 617
    label "powi&#261;za&#263;"
  ]
  node [
    id 618
    label "scali&#263;"
  ]
  node [
    id 619
    label "zatrzyma&#263;"
  ]
  node [
    id 620
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 621
    label "narta"
  ]
  node [
    id 622
    label "przedmiot"
  ]
  node [
    id 623
    label "podwi&#261;zywanie"
  ]
  node [
    id 624
    label "dressing"
  ]
  node [
    id 625
    label "socket"
  ]
  node [
    id 626
    label "szermierka"
  ]
  node [
    id 627
    label "przywi&#261;zywanie"
  ]
  node [
    id 628
    label "pakowanie"
  ]
  node [
    id 629
    label "proces_chemiczny"
  ]
  node [
    id 630
    label "my&#347;lenie"
  ]
  node [
    id 631
    label "do&#322;&#261;czanie"
  ]
  node [
    id 632
    label "communication"
  ]
  node [
    id 633
    label "wytwarzanie"
  ]
  node [
    id 634
    label "ceg&#322;a"
  ]
  node [
    id 635
    label "combination"
  ]
  node [
    id 636
    label "zobowi&#261;zywanie"
  ]
  node [
    id 637
    label "szcz&#281;ka"
  ]
  node [
    id 638
    label "anga&#380;owanie"
  ]
  node [
    id 639
    label "wi&#261;za&#263;"
  ]
  node [
    id 640
    label "twardnienie"
  ]
  node [
    id 641
    label "podwi&#261;zanie"
  ]
  node [
    id 642
    label "przywi&#261;zanie"
  ]
  node [
    id 643
    label "przymocowywanie"
  ]
  node [
    id 644
    label "scalanie"
  ]
  node [
    id 645
    label "mezomeria"
  ]
  node [
    id 646
    label "fusion"
  ]
  node [
    id 647
    label "kojarzenie_si&#281;"
  ]
  node [
    id 648
    label "&#322;&#261;czenie"
  ]
  node [
    id 649
    label "uchwyt"
  ]
  node [
    id 650
    label "rozmieszczenie"
  ]
  node [
    id 651
    label "warto&#347;ciowo&#347;&#263;"
  ]
  node [
    id 652
    label "zmiana"
  ]
  node [
    id 653
    label "element_konstrukcyjny"
  ]
  node [
    id 654
    label "obezw&#322;adnianie"
  ]
  node [
    id 655
    label "manewr"
  ]
  node [
    id 656
    label "miecz"
  ]
  node [
    id 657
    label "oddzia&#322;ywanie"
  ]
  node [
    id 658
    label "obwi&#261;zanie"
  ]
  node [
    id 659
    label "zawi&#261;zek"
  ]
  node [
    id 660
    label "obwi&#261;zywanie"
  ]
  node [
    id 661
    label "reporter"
  ]
  node [
    id 662
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 663
    label "charakterystyczny"
  ]
  node [
    id 664
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 665
    label "nale&#380;ny"
  ]
  node [
    id 666
    label "nale&#380;yty"
  ]
  node [
    id 667
    label "typowy"
  ]
  node [
    id 668
    label "uprawniony"
  ]
  node [
    id 669
    label "zasadniczy"
  ]
  node [
    id 670
    label "stosownie"
  ]
  node [
    id 671
    label "prawdziwy"
  ]
  node [
    id 672
    label "dobry"
  ]
  node [
    id 673
    label "charakterystycznie"
  ]
  node [
    id 674
    label "szczeg&#243;lny"
  ]
  node [
    id 675
    label "wyj&#261;tkowy"
  ]
  node [
    id 676
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 677
    label "podobny"
  ]
  node [
    id 678
    label "wyrobi&#263;"
  ]
  node [
    id 679
    label "catch"
  ]
  node [
    id 680
    label "frame"
  ]
  node [
    id 681
    label "przygotowa&#263;"
  ]
  node [
    id 682
    label "act"
  ]
  node [
    id 683
    label "odziedziczy&#263;"
  ]
  node [
    id 684
    label "ruszy&#263;"
  ]
  node [
    id 685
    label "take"
  ]
  node [
    id 686
    label "zaatakowa&#263;"
  ]
  node [
    id 687
    label "uciec"
  ]
  node [
    id 688
    label "receive"
  ]
  node [
    id 689
    label "nakaza&#263;"
  ]
  node [
    id 690
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 691
    label "obskoczy&#263;"
  ]
  node [
    id 692
    label "bra&#263;"
  ]
  node [
    id 693
    label "u&#380;y&#263;"
  ]
  node [
    id 694
    label "get"
  ]
  node [
    id 695
    label "wyrucha&#263;"
  ]
  node [
    id 696
    label "World_Health_Organization"
  ]
  node [
    id 697
    label "wyciupcia&#263;"
  ]
  node [
    id 698
    label "wygra&#263;"
  ]
  node [
    id 699
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 700
    label "withdraw"
  ]
  node [
    id 701
    label "wzi&#281;cie"
  ]
  node [
    id 702
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 703
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 704
    label "poczyta&#263;"
  ]
  node [
    id 705
    label "aim"
  ]
  node [
    id 706
    label "chwyci&#263;"
  ]
  node [
    id 707
    label "pokona&#263;"
  ]
  node [
    id 708
    label "zacz&#261;&#263;"
  ]
  node [
    id 709
    label "otrzyma&#263;"
  ]
  node [
    id 710
    label "wej&#347;&#263;"
  ]
  node [
    id 711
    label "poruszy&#263;"
  ]
  node [
    id 712
    label "dosta&#263;"
  ]
  node [
    id 713
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 714
    label "wykona&#263;"
  ]
  node [
    id 715
    label "cook"
  ]
  node [
    id 716
    label "wyszkoli&#263;"
  ]
  node [
    id 717
    label "train"
  ]
  node [
    id 718
    label "arrange"
  ]
  node [
    id 719
    label "wytworzy&#263;"
  ]
  node [
    id 720
    label "dress"
  ]
  node [
    id 721
    label "ukierunkowa&#263;"
  ]
  node [
    id 722
    label "spe&#322;ni&#263;"
  ]
  node [
    id 723
    label "zarobi&#263;"
  ]
  node [
    id 724
    label "zu&#380;y&#263;"
  ]
  node [
    id 725
    label "przepracowa&#263;"
  ]
  node [
    id 726
    label "wywi&#261;za&#263;_si&#281;"
  ]
  node [
    id 727
    label "zdoby&#263;"
  ]
  node [
    id 728
    label "wytrzyma&#263;"
  ]
  node [
    id 729
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 730
    label "ugnie&#347;&#263;"
  ]
  node [
    id 731
    label "rozwin&#261;&#263;"
  ]
  node [
    id 732
    label "wypracowa&#263;"
  ]
  node [
    id 733
    label "udoskonali&#263;"
  ]
  node [
    id 734
    label "wyprodukowa&#263;"
  ]
  node [
    id 735
    label "make"
  ]
  node [
    id 736
    label "manufacture"
  ]
  node [
    id 737
    label "sklep"
  ]
  node [
    id 738
    label "p&#243;&#322;ka"
  ]
  node [
    id 739
    label "firma"
  ]
  node [
    id 740
    label "stoisko"
  ]
  node [
    id 741
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 742
    label "sk&#322;ad"
  ]
  node [
    id 743
    label "obiekt_handlowy"
  ]
  node [
    id 744
    label "zaplecze"
  ]
  node [
    id 745
    label "witryna"
  ]
  node [
    id 746
    label "concourse"
  ]
  node [
    id 747
    label "skupienie"
  ]
  node [
    id 748
    label "wsp&#243;lnota"
  ]
  node [
    id 749
    label "organ"
  ]
  node [
    id 750
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 751
    label "grupa"
  ]
  node [
    id 752
    label "gromadzenie"
  ]
  node [
    id 753
    label "templum"
  ]
  node [
    id 754
    label "konwentykiel"
  ]
  node [
    id 755
    label "klasztor"
  ]
  node [
    id 756
    label "caucus"
  ]
  node [
    id 757
    label "pozyskanie"
  ]
  node [
    id 758
    label "kongregacja"
  ]
  node [
    id 759
    label "campaign"
  ]
  node [
    id 760
    label "causing"
  ]
  node [
    id 761
    label "activity"
  ]
  node [
    id 762
    label "bezproblemowy"
  ]
  node [
    id 763
    label "agglomeration"
  ]
  node [
    id 764
    label "uwaga"
  ]
  node [
    id 765
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 766
    label "przegrupowanie"
  ]
  node [
    id 767
    label "congestion"
  ]
  node [
    id 768
    label "kupienie"
  ]
  node [
    id 769
    label "concentration"
  ]
  node [
    id 770
    label "odm&#322;adzanie"
  ]
  node [
    id 771
    label "liga"
  ]
  node [
    id 772
    label "asymilowanie"
  ]
  node [
    id 773
    label "gromada"
  ]
  node [
    id 774
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 775
    label "asymilowa&#263;"
  ]
  node [
    id 776
    label "egzemplarz"
  ]
  node [
    id 777
    label "Entuzjastki"
  ]
  node [
    id 778
    label "Terranie"
  ]
  node [
    id 779
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 780
    label "category"
  ]
  node [
    id 781
    label "pakiet_klimatyczny"
  ]
  node [
    id 782
    label "oddzia&#322;"
  ]
  node [
    id 783
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 784
    label "cz&#261;steczka"
  ]
  node [
    id 785
    label "type"
  ]
  node [
    id 786
    label "specgrupa"
  ]
  node [
    id 787
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 788
    label "&#346;wietliki"
  ]
  node [
    id 789
    label "odm&#322;odzenie"
  ]
  node [
    id 790
    label "Eurogrupa"
  ]
  node [
    id 791
    label "odm&#322;adza&#263;"
  ]
  node [
    id 792
    label "formacja_geologiczna"
  ]
  node [
    id 793
    label "harcerze_starsi"
  ]
  node [
    id 794
    label "return"
  ]
  node [
    id 795
    label "uzyskanie"
  ]
  node [
    id 796
    label "obtainment"
  ]
  node [
    id 797
    label "wykonanie"
  ]
  node [
    id 798
    label "bycie_w_posiadaniu"
  ]
  node [
    id 799
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 800
    label "podobie&#324;stwo"
  ]
  node [
    id 801
    label "Skandynawia"
  ]
  node [
    id 802
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 803
    label "partnership"
  ]
  node [
    id 804
    label "Ba&#322;kany"
  ]
  node [
    id 805
    label "society"
  ]
  node [
    id 806
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 807
    label "Walencja"
  ]
  node [
    id 808
    label "tkanka"
  ]
  node [
    id 809
    label "jednostka_organizacyjna"
  ]
  node [
    id 810
    label "budowa"
  ]
  node [
    id 811
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 812
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 813
    label "tw&#243;r"
  ]
  node [
    id 814
    label "organogeneza"
  ]
  node [
    id 815
    label "zesp&#243;&#322;"
  ]
  node [
    id 816
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 817
    label "struktura_anatomiczna"
  ]
  node [
    id 818
    label "uk&#322;ad"
  ]
  node [
    id 819
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 820
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 821
    label "Izba_Konsyliarska"
  ]
  node [
    id 822
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 823
    label "stomia"
  ]
  node [
    id 824
    label "dekortykacja"
  ]
  node [
    id 825
    label "okolica"
  ]
  node [
    id 826
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 827
    label "Komitet_Region&#243;w"
  ]
  node [
    id 828
    label "&#347;ci&#347;ni&#281;cie"
  ]
  node [
    id 829
    label "zmniejszenie"
  ]
  node [
    id 830
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 831
    label "uzbieranie"
  ]
  node [
    id 832
    label "pozbieranie"
  ]
  node [
    id 833
    label "billboard"
  ]
  node [
    id 834
    label "kol&#281;dowanie"
  ]
  node [
    id 835
    label "miejsce"
  ]
  node [
    id 836
    label "augur"
  ]
  node [
    id 837
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 838
    label "sekta"
  ]
  node [
    id 839
    label "komisja"
  ]
  node [
    id 840
    label "duchowny"
  ]
  node [
    id 841
    label "zarz&#261;d"
  ]
  node [
    id 842
    label "rada"
  ]
  node [
    id 843
    label "zakon"
  ]
  node [
    id 844
    label "jednostka_administracyjna"
  ]
  node [
    id 845
    label "instytut_&#347;wiecki"
  ]
  node [
    id 846
    label "zjazd"
  ]
  node [
    id 847
    label "oratorium"
  ]
  node [
    id 848
    label "siedziba"
  ]
  node [
    id 849
    label "wirydarz"
  ]
  node [
    id 850
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 851
    label "refektarz"
  ]
  node [
    id 852
    label "kapitularz"
  ]
  node [
    id 853
    label "cela"
  ]
  node [
    id 854
    label "kustodia"
  ]
  node [
    id 855
    label "&#321;agiewniki"
  ]
  node [
    id 856
    label "integer"
  ]
  node [
    id 857
    label "zlewanie_si&#281;"
  ]
  node [
    id 858
    label "ilo&#347;&#263;"
  ]
  node [
    id 859
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 860
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 861
    label "pe&#322;ny"
  ]
  node [
    id 862
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 863
    label "ogl&#261;dacz"
  ]
  node [
    id 864
    label "widownia"
  ]
  node [
    id 865
    label "wys&#322;annik"
  ]
  node [
    id 866
    label "degustator"
  ]
  node [
    id 867
    label "cz&#322;owiek"
  ]
  node [
    id 868
    label "ablegat"
  ]
  node [
    id 869
    label "przedstawiciel"
  ]
  node [
    id 870
    label "teren"
  ]
  node [
    id 871
    label "balkon"
  ]
  node [
    id 872
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 873
    label "proscenium"
  ]
  node [
    id 874
    label "sektor"
  ]
  node [
    id 875
    label "audience"
  ]
  node [
    id 876
    label "publiczka"
  ]
  node [
    id 877
    label "widzownia"
  ]
  node [
    id 878
    label "lo&#380;a"
  ]
  node [
    id 879
    label "zlecenie"
  ]
  node [
    id 880
    label "pozbawi&#263;"
  ]
  node [
    id 881
    label "sketch"
  ]
  node [
    id 882
    label "dozna&#263;"
  ]
  node [
    id 883
    label "odzyska&#263;"
  ]
  node [
    id 884
    label "deliver"
  ]
  node [
    id 885
    label "deprive"
  ]
  node [
    id 886
    label "give_birth"
  ]
  node [
    id 887
    label "przybra&#263;"
  ]
  node [
    id 888
    label "strike"
  ]
  node [
    id 889
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 890
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 891
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 892
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 893
    label "obra&#263;"
  ]
  node [
    id 894
    label "draw"
  ]
  node [
    id 895
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 896
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 897
    label "przyj&#281;cie"
  ]
  node [
    id 898
    label "swallow"
  ]
  node [
    id 899
    label "absorb"
  ]
  node [
    id 900
    label "undertake"
  ]
  node [
    id 901
    label "recapture"
  ]
  node [
    id 902
    label "odnowi&#263;_si&#281;"
  ]
  node [
    id 903
    label "doprowadzi&#263;"
  ]
  node [
    id 904
    label "z&#322;apa&#263;"
  ]
  node [
    id 905
    label "consume"
  ]
  node [
    id 906
    label "przenie&#347;&#263;"
  ]
  node [
    id 907
    label "abstract"
  ]
  node [
    id 908
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 909
    label "przesun&#261;&#263;"
  ]
  node [
    id 910
    label "attack"
  ]
  node [
    id 911
    label "zrozumie&#263;"
  ]
  node [
    id 912
    label "fascinate"
  ]
  node [
    id 913
    label "ogarn&#261;&#263;"
  ]
  node [
    id 914
    label "feel"
  ]
  node [
    id 915
    label "spadni&#281;cie"
  ]
  node [
    id 916
    label "undertaking"
  ]
  node [
    id 917
    label "polecenie"
  ]
  node [
    id 918
    label "odebranie"
  ]
  node [
    id 919
    label "zbiegni&#281;cie"
  ]
  node [
    id 920
    label "odbieranie"
  ]
  node [
    id 921
    label "odbiera&#263;"
  ]
  node [
    id 922
    label "decree"
  ]
  node [
    id 923
    label "praca"
  ]
  node [
    id 924
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 925
    label "pr&#243;bowanie"
  ]
  node [
    id 926
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 927
    label "zademonstrowanie"
  ]
  node [
    id 928
    label "obgadanie"
  ]
  node [
    id 929
    label "realizacja"
  ]
  node [
    id 930
    label "scena"
  ]
  node [
    id 931
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 932
    label "narration"
  ]
  node [
    id 933
    label "cyrk"
  ]
  node [
    id 934
    label "wytw&#243;r"
  ]
  node [
    id 935
    label "posta&#263;"
  ]
  node [
    id 936
    label "theatrical_performance"
  ]
  node [
    id 937
    label "opisanie"
  ]
  node [
    id 938
    label "malarstwo"
  ]
  node [
    id 939
    label "scenografia"
  ]
  node [
    id 940
    label "teatr"
  ]
  node [
    id 941
    label "ukazanie"
  ]
  node [
    id 942
    label "zapoznanie"
  ]
  node [
    id 943
    label "pokaz"
  ]
  node [
    id 944
    label "podanie"
  ]
  node [
    id 945
    label "spos&#243;b"
  ]
  node [
    id 946
    label "ods&#322;ona"
  ]
  node [
    id 947
    label "exhibit"
  ]
  node [
    id 948
    label "pokazanie"
  ]
  node [
    id 949
    label "wyst&#261;pienie"
  ]
  node [
    id 950
    label "przedstawi&#263;"
  ]
  node [
    id 951
    label "przedstawianie"
  ]
  node [
    id 952
    label "przedstawia&#263;"
  ]
  node [
    id 953
    label "rola"
  ]
  node [
    id 954
    label "show"
  ]
  node [
    id 955
    label "exhibition"
  ]
  node [
    id 956
    label "pokaz&#243;wka"
  ]
  node [
    id 957
    label "prezenter"
  ]
  node [
    id 958
    label "wyraz"
  ]
  node [
    id 959
    label "impreza"
  ]
  node [
    id 960
    label "emocja"
  ]
  node [
    id 961
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 962
    label "ogrom"
  ]
  node [
    id 963
    label "iskrzy&#263;"
  ]
  node [
    id 964
    label "d&#322;awi&#263;"
  ]
  node [
    id 965
    label "ostygn&#261;&#263;"
  ]
  node [
    id 966
    label "stygn&#261;&#263;"
  ]
  node [
    id 967
    label "temperatura"
  ]
  node [
    id 968
    label "wpa&#347;&#263;"
  ]
  node [
    id 969
    label "afekt"
  ]
  node [
    id 970
    label "wpada&#263;"
  ]
  node [
    id 971
    label "interesowa&#263;"
  ]
  node [
    id 972
    label "sake"
  ]
  node [
    id 973
    label "rozciekawia&#263;"
  ]
  node [
    id 974
    label "wzajemnie"
  ]
  node [
    id 975
    label "zobop&#243;lny"
  ]
  node [
    id 976
    label "wsp&#243;lny"
  ]
  node [
    id 977
    label "zajemny"
  ]
  node [
    id 978
    label "spolny"
  ]
  node [
    id 979
    label "wsp&#243;lnie"
  ]
  node [
    id 980
    label "sp&#243;lny"
  ]
  node [
    id 981
    label "jeden"
  ]
  node [
    id 982
    label "uwsp&#243;lnienie"
  ]
  node [
    id 983
    label "uwsp&#243;lnianie"
  ]
  node [
    id 984
    label "dostarczenie"
  ]
  node [
    id 985
    label "skill"
  ]
  node [
    id 986
    label "od&#322;o&#380;enie"
  ]
  node [
    id 987
    label "dochrapanie_si&#281;"
  ]
  node [
    id 988
    label "po&#380;yczenie"
  ]
  node [
    id 989
    label "cel"
  ]
  node [
    id 990
    label "gaze"
  ]
  node [
    id 991
    label "deference"
  ]
  node [
    id 992
    label "oddanie"
  ]
  node [
    id 993
    label "mention"
  ]
  node [
    id 994
    label "commitment"
  ]
  node [
    id 995
    label "wierno&#347;&#263;"
  ]
  node [
    id 996
    label "ofiarno&#347;&#263;"
  ]
  node [
    id 997
    label "reciprocation"
  ]
  node [
    id 998
    label "prohibition"
  ]
  node [
    id 999
    label "za&#322;atwienie_si&#281;"
  ]
  node [
    id 1000
    label "powr&#243;cenie"
  ]
  node [
    id 1001
    label "doj&#347;cie"
  ]
  node [
    id 1002
    label "danie"
  ]
  node [
    id 1003
    label "przekazanie"
  ]
  node [
    id 1004
    label "odst&#261;pienie"
  ]
  node [
    id 1005
    label "prototype"
  ]
  node [
    id 1006
    label "render"
  ]
  node [
    id 1007
    label "pass"
  ]
  node [
    id 1008
    label "odpowiedzenie"
  ]
  node [
    id 1009
    label "sprzedanie"
  ]
  node [
    id 1010
    label "zrobienie"
  ]
  node [
    id 1011
    label "pragnienie"
  ]
  node [
    id 1012
    label "delivery"
  ]
  node [
    id 1013
    label "nawodnienie"
  ]
  node [
    id 1014
    label "przes&#322;anie"
  ]
  node [
    id 1015
    label "wytworzenie"
  ]
  node [
    id 1016
    label "wy&#347;wiadczenie"
  ]
  node [
    id 1017
    label "zmys&#322;"
  ]
  node [
    id 1018
    label "przeczulica"
  ]
  node [
    id 1019
    label "czucie"
  ]
  node [
    id 1020
    label "poczucie"
  ]
  node [
    id 1021
    label "punkt"
  ]
  node [
    id 1022
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 1023
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 1024
    label "pomy&#347;lenie"
  ]
  node [
    id 1025
    label "kontakt"
  ]
  node [
    id 1026
    label "congratulation"
  ]
  node [
    id 1027
    label "rozpo&#380;yczenie"
  ]
  node [
    id 1028
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1029
    label "op&#243;&#378;nienie"
  ]
  node [
    id 1030
    label "zachowanie"
  ]
  node [
    id 1031
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 1032
    label "zniesienie"
  ]
  node [
    id 1033
    label "wyznaczenie"
  ]
  node [
    id 1034
    label "pozostawienie"
  ]
  node [
    id 1035
    label "continuance"
  ]
  node [
    id 1036
    label "wstrzymanie_si&#281;"
  ]
  node [
    id 1037
    label "prorogation"
  ]
  node [
    id 1038
    label "publikacja"
  ]
  node [
    id 1039
    label "wiedza"
  ]
  node [
    id 1040
    label "obiega&#263;"
  ]
  node [
    id 1041
    label "powzi&#281;cie"
  ]
  node [
    id 1042
    label "dane"
  ]
  node [
    id 1043
    label "obiegni&#281;cie"
  ]
  node [
    id 1044
    label "sygna&#322;"
  ]
  node [
    id 1045
    label "obieganie"
  ]
  node [
    id 1046
    label "powzi&#261;&#263;"
  ]
  node [
    id 1047
    label "obiec"
  ]
  node [
    id 1048
    label "doj&#347;&#263;"
  ]
  node [
    id 1049
    label "ust&#281;p"
  ]
  node [
    id 1050
    label "plan"
  ]
  node [
    id 1051
    label "obiekt_matematyczny"
  ]
  node [
    id 1052
    label "plamka"
  ]
  node [
    id 1053
    label "stopie&#324;_pisma"
  ]
  node [
    id 1054
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1055
    label "mark"
  ]
  node [
    id 1056
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1057
    label "prosta"
  ]
  node [
    id 1058
    label "zapunktowa&#263;"
  ]
  node [
    id 1059
    label "podpunkt"
  ]
  node [
    id 1060
    label "wojsko"
  ]
  node [
    id 1061
    label "point"
  ]
  node [
    id 1062
    label "pozycja"
  ]
  node [
    id 1063
    label "cognition"
  ]
  node [
    id 1064
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 1065
    label "intelekt"
  ]
  node [
    id 1066
    label "pozwolenie"
  ]
  node [
    id 1067
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1068
    label "zaawansowanie"
  ]
  node [
    id 1069
    label "wykszta&#322;cenie"
  ]
  node [
    id 1070
    label "przekazywa&#263;"
  ]
  node [
    id 1071
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 1072
    label "pulsation"
  ]
  node [
    id 1073
    label "przekazywanie"
  ]
  node [
    id 1074
    label "przewodzenie"
  ]
  node [
    id 1075
    label "fala"
  ]
  node [
    id 1076
    label "przewodzi&#263;"
  ]
  node [
    id 1077
    label "znak"
  ]
  node [
    id 1078
    label "zapowied&#378;"
  ]
  node [
    id 1079
    label "medium_transmisyjne"
  ]
  node [
    id 1080
    label "demodulacja"
  ]
  node [
    id 1081
    label "czynnik"
  ]
  node [
    id 1082
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 1083
    label "aliasing"
  ]
  node [
    id 1084
    label "wizja"
  ]
  node [
    id 1085
    label "modulacja"
  ]
  node [
    id 1086
    label "drift"
  ]
  node [
    id 1087
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 1088
    label "tekst"
  ]
  node [
    id 1089
    label "druk"
  ]
  node [
    id 1090
    label "edytowa&#263;"
  ]
  node [
    id 1091
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 1092
    label "spakowanie"
  ]
  node [
    id 1093
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1094
    label "pakowa&#263;"
  ]
  node [
    id 1095
    label "rekord"
  ]
  node [
    id 1096
    label "korelator"
  ]
  node [
    id 1097
    label "wyci&#261;ganie"
  ]
  node [
    id 1098
    label "sekwencjonowa&#263;"
  ]
  node [
    id 1099
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 1100
    label "jednostka_informacji"
  ]
  node [
    id 1101
    label "evidence"
  ]
  node [
    id 1102
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 1103
    label "rozpakowywanie"
  ]
  node [
    id 1104
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1105
    label "rozpakowanie"
  ]
  node [
    id 1106
    label "rozpakowywa&#263;"
  ]
  node [
    id 1107
    label "nap&#322;ywanie"
  ]
  node [
    id 1108
    label "rozpakowa&#263;"
  ]
  node [
    id 1109
    label "spakowa&#263;"
  ]
  node [
    id 1110
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 1111
    label "edytowanie"
  ]
  node [
    id 1112
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 1113
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 1114
    label "sekwencjonowanie"
  ]
  node [
    id 1115
    label "odwiedza&#263;"
  ]
  node [
    id 1116
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 1117
    label "rotate"
  ]
  node [
    id 1118
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 1119
    label "authorize"
  ]
  node [
    id 1120
    label "podj&#261;&#263;"
  ]
  node [
    id 1121
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1122
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 1123
    label "supervene"
  ]
  node [
    id 1124
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 1125
    label "zaj&#347;&#263;"
  ]
  node [
    id 1126
    label "bodziec"
  ]
  node [
    id 1127
    label "przesy&#322;ka"
  ]
  node [
    id 1128
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 1129
    label "heed"
  ]
  node [
    id 1130
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 1131
    label "dokoptowa&#263;"
  ]
  node [
    id 1132
    label "postrzega&#263;"
  ]
  node [
    id 1133
    label "orgazm"
  ]
  node [
    id 1134
    label "dolecie&#263;"
  ]
  node [
    id 1135
    label "drive"
  ]
  node [
    id 1136
    label "dotrze&#263;"
  ]
  node [
    id 1137
    label "uzyska&#263;"
  ]
  node [
    id 1138
    label "dop&#322;ata"
  ]
  node [
    id 1139
    label "become"
  ]
  node [
    id 1140
    label "odwiedzi&#263;"
  ]
  node [
    id 1141
    label "rozpowszechni&#263;_si&#281;"
  ]
  node [
    id 1142
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 1143
    label "orb"
  ]
  node [
    id 1144
    label "podj&#281;cie"
  ]
  node [
    id 1145
    label "otrzymanie"
  ]
  node [
    id 1146
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1147
    label "dochodzenie"
  ]
  node [
    id 1148
    label "znajomo&#347;ci"
  ]
  node [
    id 1149
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 1150
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 1151
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1152
    label "entrance"
  ]
  node [
    id 1153
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1154
    label "dor&#281;czenie"
  ]
  node [
    id 1155
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 1156
    label "dost&#281;p"
  ]
  node [
    id 1157
    label "gotowy"
  ]
  node [
    id 1158
    label "avenue"
  ]
  node [
    id 1159
    label "postrzeganie"
  ]
  node [
    id 1160
    label "dojrza&#322;y"
  ]
  node [
    id 1161
    label "dojechanie"
  ]
  node [
    id 1162
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 1163
    label "ingress"
  ]
  node [
    id 1164
    label "strzelenie"
  ]
  node [
    id 1165
    label "orzekni&#281;cie"
  ]
  node [
    id 1166
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1167
    label "dolecenie"
  ]
  node [
    id 1168
    label "rozpowszechnienie"
  ]
  node [
    id 1169
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1170
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 1171
    label "stanie_si&#281;"
  ]
  node [
    id 1172
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 1173
    label "odwiedzanie"
  ]
  node [
    id 1174
    label "biegni&#281;cie"
  ]
  node [
    id 1175
    label "zakre&#347;lanie"
  ]
  node [
    id 1176
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 1177
    label "okr&#261;&#380;anie"
  ]
  node [
    id 1178
    label "rozpowszechnienie_si&#281;"
  ]
  node [
    id 1179
    label "zakre&#347;lenie"
  ]
  node [
    id 1180
    label "odwiedzenie"
  ]
  node [
    id 1181
    label "okr&#261;&#380;enie"
  ]
  node [
    id 1182
    label "comment"
  ]
  node [
    id 1183
    label "ocena"
  ]
  node [
    id 1184
    label "interpretacja"
  ]
  node [
    id 1185
    label "artyku&#322;"
  ]
  node [
    id 1186
    label "audycja"
  ]
  node [
    id 1187
    label "gossip"
  ]
  node [
    id 1188
    label "ekscerpcja"
  ]
  node [
    id 1189
    label "j&#281;zykowo"
  ]
  node [
    id 1190
    label "redakcja"
  ]
  node [
    id 1191
    label "pomini&#281;cie"
  ]
  node [
    id 1192
    label "dzie&#322;o"
  ]
  node [
    id 1193
    label "preparacja"
  ]
  node [
    id 1194
    label "odmianka"
  ]
  node [
    id 1195
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1196
    label "koniektura"
  ]
  node [
    id 1197
    label "pisa&#263;"
  ]
  node [
    id 1198
    label "obelga"
  ]
  node [
    id 1199
    label "pogl&#261;d"
  ]
  node [
    id 1200
    label "sofcik"
  ]
  node [
    id 1201
    label "kryterium"
  ]
  node [
    id 1202
    label "appraisal"
  ]
  node [
    id 1203
    label "explanation"
  ]
  node [
    id 1204
    label "hermeneutyka"
  ]
  node [
    id 1205
    label "wypracowanie"
  ]
  node [
    id 1206
    label "kontekst"
  ]
  node [
    id 1207
    label "interpretation"
  ]
  node [
    id 1208
    label "obja&#347;nienie"
  ]
  node [
    id 1209
    label "prawda"
  ]
  node [
    id 1210
    label "znak_j&#281;zykowy"
  ]
  node [
    id 1211
    label "nag&#322;&#243;wek"
  ]
  node [
    id 1212
    label "szkic"
  ]
  node [
    id 1213
    label "fragment"
  ]
  node [
    id 1214
    label "wyr&#243;b"
  ]
  node [
    id 1215
    label "rodzajnik"
  ]
  node [
    id 1216
    label "dokument"
  ]
  node [
    id 1217
    label "towar"
  ]
  node [
    id 1218
    label "paragraf"
  ]
  node [
    id 1219
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 1220
    label "wydawnictwo"
  ]
  node [
    id 1221
    label "bran&#380;owiec"
  ]
  node [
    id 1222
    label "edytor"
  ]
  node [
    id 1223
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1224
    label "wapniak"
  ]
  node [
    id 1225
    label "os&#322;abia&#263;"
  ]
  node [
    id 1226
    label "hominid"
  ]
  node [
    id 1227
    label "podw&#322;adny"
  ]
  node [
    id 1228
    label "os&#322;abianie"
  ]
  node [
    id 1229
    label "g&#322;owa"
  ]
  node [
    id 1230
    label "figura"
  ]
  node [
    id 1231
    label "portrecista"
  ]
  node [
    id 1232
    label "dwun&#243;g"
  ]
  node [
    id 1233
    label "profanum"
  ]
  node [
    id 1234
    label "mikrokosmos"
  ]
  node [
    id 1235
    label "nasada"
  ]
  node [
    id 1236
    label "duch"
  ]
  node [
    id 1237
    label "antropochoria"
  ]
  node [
    id 1238
    label "wz&#243;r"
  ]
  node [
    id 1239
    label "senior"
  ]
  node [
    id 1240
    label "Adam"
  ]
  node [
    id 1241
    label "homo_sapiens"
  ]
  node [
    id 1242
    label "polifag"
  ]
  node [
    id 1243
    label "pracownik"
  ]
  node [
    id 1244
    label "fachowiec"
  ]
  node [
    id 1245
    label "zwi&#261;zkowiec"
  ]
  node [
    id 1246
    label "przedsi&#281;biorca"
  ]
  node [
    id 1247
    label "tekstolog"
  ]
  node [
    id 1248
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 1249
    label "debit"
  ]
  node [
    id 1250
    label "szata_graficzna"
  ]
  node [
    id 1251
    label "wydawa&#263;"
  ]
  node [
    id 1252
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 1253
    label "wyda&#263;"
  ]
  node [
    id 1254
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 1255
    label "poster"
  ]
  node [
    id 1256
    label "radio"
  ]
  node [
    id 1257
    label "composition"
  ]
  node [
    id 1258
    label "redaction"
  ]
  node [
    id 1259
    label "telewizja"
  ]
  node [
    id 1260
    label "obr&#243;bka"
  ]
  node [
    id 1261
    label "kciuk"
  ]
  node [
    id 1262
    label "ssaki_wy&#380;sze"
  ]
  node [
    id 1263
    label "kciukas"
  ]
  node [
    id 1264
    label "du&#380;y_palec"
  ]
  node [
    id 1265
    label "przes&#322;ucha&#263;"
  ]
  node [
    id 1266
    label "sprawdzi&#263;"
  ]
  node [
    id 1267
    label "quiz"
  ]
  node [
    id 1268
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 1269
    label "ask"
  ]
  node [
    id 1270
    label "examine"
  ]
  node [
    id 1271
    label "zgaduj-zgadula"
  ]
  node [
    id 1272
    label "konkurs"
  ]
  node [
    id 1273
    label "odpyta&#263;"
  ]
  node [
    id 1274
    label "interrogate"
  ]
  node [
    id 1275
    label "wypyta&#263;"
  ]
  node [
    id 1276
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 1277
    label "metoda"
  ]
  node [
    id 1278
    label "policy"
  ]
  node [
    id 1279
    label "dyplomacja"
  ]
  node [
    id 1280
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1281
    label "Europejska_Polityka_S&#261;siedztwa"
  ]
  node [
    id 1282
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1283
    label "method"
  ]
  node [
    id 1284
    label "doktryna"
  ]
  node [
    id 1285
    label "absolutorium"
  ]
  node [
    id 1286
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1287
    label "dzia&#322;anie"
  ]
  node [
    id 1288
    label "statesmanship"
  ]
  node [
    id 1289
    label "notyfikowa&#263;"
  ]
  node [
    id 1290
    label "corps"
  ]
  node [
    id 1291
    label "notyfikowanie"
  ]
  node [
    id 1292
    label "korpus_dyplomatyczny"
  ]
  node [
    id 1293
    label "kaftan"
  ]
  node [
    id 1294
    label "okrycie"
  ]
  node [
    id 1295
    label "nastawi&#263;"
  ]
  node [
    id 1296
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 1297
    label "obejrze&#263;"
  ]
  node [
    id 1298
    label "impersonate"
  ]
  node [
    id 1299
    label "uruchomi&#263;"
  ]
  node [
    id 1300
    label "post&#261;pi&#263;"
  ]
  node [
    id 1301
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1302
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1303
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1304
    label "zorganizowa&#263;"
  ]
  node [
    id 1305
    label "appoint"
  ]
  node [
    id 1306
    label "wystylizowa&#263;"
  ]
  node [
    id 1307
    label "cause"
  ]
  node [
    id 1308
    label "przerobi&#263;"
  ]
  node [
    id 1309
    label "nabra&#263;"
  ]
  node [
    id 1310
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1311
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1312
    label "wydali&#263;"
  ]
  node [
    id 1313
    label "zwi&#281;kszy&#263;_si&#281;"
  ]
  node [
    id 1314
    label "turn"
  ]
  node [
    id 1315
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 1316
    label "nazwa&#263;"
  ]
  node [
    id 1317
    label "upi&#281;kszy&#263;"
  ]
  node [
    id 1318
    label "assume"
  ]
  node [
    id 1319
    label "increase"
  ]
  node [
    id 1320
    label "rise"
  ]
  node [
    id 1321
    label "pozwoli&#263;"
  ]
  node [
    id 1322
    label "pu&#347;ci&#263;"
  ]
  node [
    id 1323
    label "powo&#322;a&#263;"
  ]
  node [
    id 1324
    label "okroi&#263;"
  ]
  node [
    id 1325
    label "usun&#261;&#263;"
  ]
  node [
    id 1326
    label "shell"
  ]
  node [
    id 1327
    label "distill"
  ]
  node [
    id 1328
    label "wybra&#263;"
  ]
  node [
    id 1329
    label "ostruga&#263;"
  ]
  node [
    id 1330
    label "zainteresowa&#263;"
  ]
  node [
    id 1331
    label "spo&#380;y&#263;"
  ]
  node [
    id 1332
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 1333
    label "pozna&#263;"
  ]
  node [
    id 1334
    label "sparali&#380;owa&#263;"
  ]
  node [
    id 1335
    label "oceni&#263;"
  ]
  node [
    id 1336
    label "przyzna&#263;"
  ]
  node [
    id 1337
    label "stwierdzi&#263;"
  ]
  node [
    id 1338
    label "assent"
  ]
  node [
    id 1339
    label "rede"
  ]
  node [
    id 1340
    label "see"
  ]
  node [
    id 1341
    label "admit"
  ]
  node [
    id 1342
    label "wprowadzi&#263;"
  ]
  node [
    id 1343
    label "uplasowa&#263;"
  ]
  node [
    id 1344
    label "wpierniczy&#263;"
  ]
  node [
    id 1345
    label "okre&#347;li&#263;"
  ]
  node [
    id 1346
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 1347
    label "zmieni&#263;"
  ]
  node [
    id 1348
    label "umieszcza&#263;"
  ]
  node [
    id 1349
    label "give"
  ]
  node [
    id 1350
    label "picture"
  ]
  node [
    id 1351
    label "wpuszczenie"
  ]
  node [
    id 1352
    label "credence"
  ]
  node [
    id 1353
    label "zobowi&#261;zanie_si&#281;"
  ]
  node [
    id 1354
    label "dopuszczenie"
  ]
  node [
    id 1355
    label "zareagowanie"
  ]
  node [
    id 1356
    label "uznanie"
  ]
  node [
    id 1357
    label "presumption"
  ]
  node [
    id 1358
    label "entertainment"
  ]
  node [
    id 1359
    label "reception"
  ]
  node [
    id 1360
    label "poch&#322;oni&#281;cie"
  ]
  node [
    id 1361
    label "zgodzenie_si&#281;"
  ]
  node [
    id 1362
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 1363
    label "party"
  ]
  node [
    id 1364
    label "kszta&#322;t"
  ]
  node [
    id 1365
    label "traverse"
  ]
  node [
    id 1366
    label "kara_&#347;mierci"
  ]
  node [
    id 1367
    label "d&#322;o&#324;"
  ]
  node [
    id 1368
    label "cierpienie"
  ]
  node [
    id 1369
    label "symbol"
  ]
  node [
    id 1370
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 1371
    label "biblizm"
  ]
  node [
    id 1372
    label "order"
  ]
  node [
    id 1373
    label "gest"
  ]
  node [
    id 1374
    label "odznaka"
  ]
  node [
    id 1375
    label "kawaler"
  ]
  node [
    id 1376
    label "zboczenie"
  ]
  node [
    id 1377
    label "om&#243;wienie"
  ]
  node [
    id 1378
    label "sponiewieranie"
  ]
  node [
    id 1379
    label "discipline"
  ]
  node [
    id 1380
    label "omawia&#263;"
  ]
  node [
    id 1381
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1382
    label "tre&#347;&#263;"
  ]
  node [
    id 1383
    label "sponiewiera&#263;"
  ]
  node [
    id 1384
    label "element"
  ]
  node [
    id 1385
    label "entity"
  ]
  node [
    id 1386
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1387
    label "tematyka"
  ]
  node [
    id 1388
    label "zbaczanie"
  ]
  node [
    id 1389
    label "program_nauczania"
  ]
  node [
    id 1390
    label "om&#243;wi&#263;"
  ]
  node [
    id 1391
    label "omawianie"
  ]
  node [
    id 1392
    label "kultura"
  ]
  node [
    id 1393
    label "istota"
  ]
  node [
    id 1394
    label "zbacza&#263;"
  ]
  node [
    id 1395
    label "zboczy&#263;"
  ]
  node [
    id 1396
    label "akceptowanie"
  ]
  node [
    id 1397
    label "j&#281;czenie"
  ]
  node [
    id 1398
    label "cier&#324;"
  ]
  node [
    id 1399
    label "toleration"
  ]
  node [
    id 1400
    label "pain"
  ]
  node [
    id 1401
    label "grief"
  ]
  node [
    id 1402
    label "m&#281;ka_Pa&#324;ska"
  ]
  node [
    id 1403
    label "pochorowanie"
  ]
  node [
    id 1404
    label "drzazga"
  ]
  node [
    id 1405
    label "badgering"
  ]
  node [
    id 1406
    label "namartwienie_si&#281;"
  ]
  node [
    id 1407
    label "prze&#380;ycie"
  ]
  node [
    id 1408
    label "chory"
  ]
  node [
    id 1409
    label "znak_pisarski"
  ]
  node [
    id 1410
    label "notacja"
  ]
  node [
    id 1411
    label "wcielenie"
  ]
  node [
    id 1412
    label "brzoza_kr&#243;lewska"
  ]
  node [
    id 1413
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 1414
    label "character"
  ]
  node [
    id 1415
    label "symbolizowanie"
  ]
  node [
    id 1416
    label "dobrodziejstwo"
  ]
  node [
    id 1417
    label "narz&#281;dzie"
  ]
  node [
    id 1418
    label "gesture"
  ]
  node [
    id 1419
    label "gestykulacja"
  ]
  node [
    id 1420
    label "pantomima"
  ]
  node [
    id 1421
    label "ruch"
  ]
  node [
    id 1422
    label "motion"
  ]
  node [
    id 1423
    label "formacja"
  ]
  node [
    id 1424
    label "punkt_widzenia"
  ]
  node [
    id 1425
    label "wygl&#261;d"
  ]
  node [
    id 1426
    label "spirala"
  ]
  node [
    id 1427
    label "p&#322;at"
  ]
  node [
    id 1428
    label "comeliness"
  ]
  node [
    id 1429
    label "kielich"
  ]
  node [
    id 1430
    label "face"
  ]
  node [
    id 1431
    label "blaszka"
  ]
  node [
    id 1432
    label "p&#281;tla"
  ]
  node [
    id 1433
    label "pasmo"
  ]
  node [
    id 1434
    label "linearno&#347;&#263;"
  ]
  node [
    id 1435
    label "gwiazda"
  ]
  node [
    id 1436
    label "miniatura"
  ]
  node [
    id 1437
    label "alfa_i_omega"
  ]
  node [
    id 1438
    label "samarytanka"
  ]
  node [
    id 1439
    label "&#322;ono_Abrahama"
  ]
  node [
    id 1440
    label "niewola_egipska"
  ]
  node [
    id 1441
    label "dziecko_Beliala"
  ]
  node [
    id 1442
    label "je&#378;dziec_Apokalipsy"
  ]
  node [
    id 1443
    label "syn_marnotrawny"
  ]
  node [
    id 1444
    label "korona_cierniowa"
  ]
  node [
    id 1445
    label "niebieski_ptak"
  ]
  node [
    id 1446
    label "grdyka"
  ]
  node [
    id 1447
    label "droga_krzy&#380;owa"
  ]
  node [
    id 1448
    label "manna_z_nieba"
  ]
  node [
    id 1449
    label "niewierny_Tomasz"
  ]
  node [
    id 1450
    label "Herod"
  ]
  node [
    id 1451
    label "tr&#261;ba_jerycho&#324;ska"
  ]
  node [
    id 1452
    label "ucho_igielne"
  ]
  node [
    id 1453
    label "drabina_Jakubowa"
  ]
  node [
    id 1454
    label "krzew_gorej&#261;cy"
  ]
  node [
    id 1455
    label "&#380;ona_Lota"
  ]
  node [
    id 1456
    label "kozio&#322;_ofiarny"
  ]
  node [
    id 1457
    label "kolos_na_glinianych_nogach"
  ]
  node [
    id 1458
    label "ga&#322;&#261;zka_oliwna"
  ]
  node [
    id 1459
    label "list_Uriasza"
  ]
  node [
    id 1460
    label "odst&#281;pca"
  ]
  node [
    id 1461
    label "plaga_egipska"
  ]
  node [
    id 1462
    label "wdowi_grosz"
  ]
  node [
    id 1463
    label "&#380;ona_Putyfara"
  ]
  node [
    id 1464
    label "szatan"
  ]
  node [
    id 1465
    label "wiek_matuzalemowy"
  ]
  node [
    id 1466
    label "chleb_powszedni"
  ]
  node [
    id 1467
    label "judaszowe_srebrniki"
  ]
  node [
    id 1468
    label "Sodoma"
  ]
  node [
    id 1469
    label "arka_przymierza"
  ]
  node [
    id 1470
    label "tr&#261;by_jerycho&#324;skie"
  ]
  node [
    id 1471
    label "lewiatan"
  ]
  node [
    id 1472
    label "miedziane_czo&#322;o"
  ]
  node [
    id 1473
    label "kamie&#324;_w&#281;gielny"
  ]
  node [
    id 1474
    label "herod-baba"
  ]
  node [
    id 1475
    label "cnotliwa_Zuzanna"
  ]
  node [
    id 1476
    label "palec_bo&#380;y"
  ]
  node [
    id 1477
    label "zb&#322;&#261;kana_owca"
  ]
  node [
    id 1478
    label "listek_figowy"
  ]
  node [
    id 1479
    label "mury_Jerycha"
  ]
  node [
    id 1480
    label "Behemot"
  ]
  node [
    id 1481
    label "kainowe_pi&#281;tno"
  ]
  node [
    id 1482
    label "z&#322;oty_cielec"
  ]
  node [
    id 1483
    label "o&#347;lica_Balaama"
  ]
  node [
    id 1484
    label "arka_Noego"
  ]
  node [
    id 1485
    label "Gomora"
  ]
  node [
    id 1486
    label "gr&#243;b_pobielany"
  ]
  node [
    id 1487
    label "wie&#380;a_Babel"
  ]
  node [
    id 1488
    label "winnica_Nabota"
  ]
  node [
    id 1489
    label "miecz_obosieczny"
  ]
  node [
    id 1490
    label "fa&#322;szywy_prorok"
  ]
  node [
    id 1491
    label "s&#243;l_ziemi"
  ]
  node [
    id 1492
    label "miska_soczewicy"
  ]
  node [
    id 1493
    label "rze&#378;_niewini&#261;tek"
  ]
  node [
    id 1494
    label "linia_mi&#322;o&#347;ci"
  ]
  node [
    id 1495
    label "wyklepanie"
  ]
  node [
    id 1496
    label "palec"
  ]
  node [
    id 1497
    label "chiromancja"
  ]
  node [
    id 1498
    label "klepanie"
  ]
  node [
    id 1499
    label "wyklepa&#263;"
  ]
  node [
    id 1500
    label "nadgarstek"
  ]
  node [
    id 1501
    label "szeroko&#347;&#263;_d&#322;oni"
  ]
  node [
    id 1502
    label "dotykanie"
  ]
  node [
    id 1503
    label "graba"
  ]
  node [
    id 1504
    label "klepa&#263;"
  ]
  node [
    id 1505
    label "r&#261;czyna"
  ]
  node [
    id 1506
    label "cmoknonsens"
  ]
  node [
    id 1507
    label "chwyta&#263;"
  ]
  node [
    id 1508
    label "r&#281;ka"
  ]
  node [
    id 1509
    label "chwytanie"
  ]
  node [
    id 1510
    label "linia_&#380;ycia"
  ]
  node [
    id 1511
    label "hasta"
  ]
  node [
    id 1512
    label "linia_rozumu"
  ]
  node [
    id 1513
    label "poduszka"
  ]
  node [
    id 1514
    label "dotyka&#263;"
  ]
  node [
    id 1515
    label "segment_ruchowy"
  ]
  node [
    id 1516
    label "kr&#281;g"
  ]
  node [
    id 1517
    label "otw&#243;r_mi&#281;dzykr&#281;gowy"
  ]
  node [
    id 1518
    label "rdze&#324;"
  ]
  node [
    id 1519
    label "szkielet"
  ]
  node [
    id 1520
    label "stenoza"
  ]
  node [
    id 1521
    label "rozszczep_kr&#281;gos&#322;upa"
  ]
  node [
    id 1522
    label "dysk"
  ]
  node [
    id 1523
    label "kifoza_piersiowa"
  ]
  node [
    id 1524
    label "moralno&#347;&#263;"
  ]
  node [
    id 1525
    label "visualize"
  ]
  node [
    id 1526
    label "wystawi&#263;"
  ]
  node [
    id 1527
    label "evaluate"
  ]
  node [
    id 1528
    label "znale&#378;&#263;"
  ]
  node [
    id 1529
    label "pomy&#347;le&#263;"
  ]
  node [
    id 1530
    label "testify"
  ]
  node [
    id 1531
    label "powiedzie&#263;"
  ]
  node [
    id 1532
    label "oznajmi&#263;"
  ]
  node [
    id 1533
    label "declare"
  ]
  node [
    id 1534
    label "nada&#263;"
  ]
  node [
    id 1535
    label "okre&#347;lony"
  ]
  node [
    id 1536
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1537
    label "wiadomy"
  ]
  node [
    id 1538
    label "przebiec"
  ]
  node [
    id 1539
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1540
    label "motyw"
  ]
  node [
    id 1541
    label "przebiegni&#281;cie"
  ]
  node [
    id 1542
    label "fabu&#322;a"
  ]
  node [
    id 1543
    label "ideologia"
  ]
  node [
    id 1544
    label "byt"
  ]
  node [
    id 1545
    label "Kant"
  ]
  node [
    id 1546
    label "p&#322;&#243;d"
  ]
  node [
    id 1547
    label "pomys&#322;"
  ]
  node [
    id 1548
    label "ideacja"
  ]
  node [
    id 1549
    label "wpadni&#281;cie"
  ]
  node [
    id 1550
    label "mienie"
  ]
  node [
    id 1551
    label "przyroda"
  ]
  node [
    id 1552
    label "wpadanie"
  ]
  node [
    id 1553
    label "rozumowanie"
  ]
  node [
    id 1554
    label "opracowanie"
  ]
  node [
    id 1555
    label "proces"
  ]
  node [
    id 1556
    label "cytat"
  ]
  node [
    id 1557
    label "s&#261;dzenie"
  ]
  node [
    id 1558
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 1559
    label "niuansowa&#263;"
  ]
  node [
    id 1560
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 1561
    label "sk&#322;adnik"
  ]
  node [
    id 1562
    label "zniuansowa&#263;"
  ]
  node [
    id 1563
    label "fakt"
  ]
  node [
    id 1564
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1565
    label "przyczyna"
  ]
  node [
    id 1566
    label "wnioskowanie"
  ]
  node [
    id 1567
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 1568
    label "wyraz_pochodny"
  ]
  node [
    id 1569
    label "fraza"
  ]
  node [
    id 1570
    label "forum"
  ]
  node [
    id 1571
    label "topik"
  ]
  node [
    id 1572
    label "forma"
  ]
  node [
    id 1573
    label "melodia"
  ]
  node [
    id 1574
    label "otoczka"
  ]
  node [
    id 1575
    label "realny"
  ]
  node [
    id 1576
    label "du&#380;y"
  ]
  node [
    id 1577
    label "dono&#347;ny"
  ]
  node [
    id 1578
    label "silny"
  ]
  node [
    id 1579
    label "istotnie"
  ]
  node [
    id 1580
    label "znaczny"
  ]
  node [
    id 1581
    label "eksponowany"
  ]
  node [
    id 1582
    label "doros&#322;y"
  ]
  node [
    id 1583
    label "niema&#322;o"
  ]
  node [
    id 1584
    label "wiele"
  ]
  node [
    id 1585
    label "rozwini&#281;ty"
  ]
  node [
    id 1586
    label "dorodny"
  ]
  node [
    id 1587
    label "wa&#380;ny"
  ]
  node [
    id 1588
    label "du&#380;o"
  ]
  node [
    id 1589
    label "mo&#380;liwy"
  ]
  node [
    id 1590
    label "realnie"
  ]
  node [
    id 1591
    label "intensywny"
  ]
  node [
    id 1592
    label "krzepienie"
  ]
  node [
    id 1593
    label "&#380;ywotny"
  ]
  node [
    id 1594
    label "mocny"
  ]
  node [
    id 1595
    label "pokrzepienie"
  ]
  node [
    id 1596
    label "zdecydowany"
  ]
  node [
    id 1597
    label "niepodwa&#380;alny"
  ]
  node [
    id 1598
    label "mocno"
  ]
  node [
    id 1599
    label "przekonuj&#261;cy"
  ]
  node [
    id 1600
    label "wytrzyma&#322;y"
  ]
  node [
    id 1601
    label "konkretny"
  ]
  node [
    id 1602
    label "zdrowy"
  ]
  node [
    id 1603
    label "silnie"
  ]
  node [
    id 1604
    label "meflochina"
  ]
  node [
    id 1605
    label "zajebisty"
  ]
  node [
    id 1606
    label "znacznie"
  ]
  node [
    id 1607
    label "zauwa&#380;alny"
  ]
  node [
    id 1608
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 1609
    label "gromowy"
  ]
  node [
    id 1610
    label "dono&#347;nie"
  ]
  node [
    id 1611
    label "g&#322;o&#347;ny"
  ]
  node [
    id 1612
    label "importantly"
  ]
  node [
    id 1613
    label "recipient_role"
  ]
  node [
    id 1614
    label "otrzymywanie"
  ]
  node [
    id 1615
    label "dostanie"
  ]
  node [
    id 1616
    label "produkowanie"
  ]
  node [
    id 1617
    label "dostawanie"
  ]
  node [
    id 1618
    label "trwa&#263;"
  ]
  node [
    id 1619
    label "spoczywa&#263;"
  ]
  node [
    id 1620
    label "lie"
  ]
  node [
    id 1621
    label "pokrywa&#263;"
  ]
  node [
    id 1622
    label "zwierz&#281;"
  ]
  node [
    id 1623
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1624
    label "gr&#243;b"
  ]
  node [
    id 1625
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1626
    label "mie&#263;_miejsce"
  ]
  node [
    id 1627
    label "equal"
  ]
  node [
    id 1628
    label "chodzi&#263;"
  ]
  node [
    id 1629
    label "si&#281;ga&#263;"
  ]
  node [
    id 1630
    label "obecno&#347;&#263;"
  ]
  node [
    id 1631
    label "stand"
  ]
  node [
    id 1632
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1633
    label "uczestniczy&#263;"
  ]
  node [
    id 1634
    label "odpoczywa&#263;"
  ]
  node [
    id 1635
    label "nadzieja"
  ]
  node [
    id 1636
    label "istnie&#263;"
  ]
  node [
    id 1637
    label "pozostawa&#263;"
  ]
  node [
    id 1638
    label "zostawa&#263;"
  ]
  node [
    id 1639
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1640
    label "adhere"
  ]
  node [
    id 1641
    label "rozwija&#263;"
  ]
  node [
    id 1642
    label "przykrywa&#263;"
  ]
  node [
    id 1643
    label "zaspokaja&#263;"
  ]
  node [
    id 1644
    label "zap&#322;adnia&#263;"
  ]
  node [
    id 1645
    label "p&#322;aci&#263;"
  ]
  node [
    id 1646
    label "smother"
  ]
  node [
    id 1647
    label "maskowa&#263;"
  ]
  node [
    id 1648
    label "r&#243;wna&#263;"
  ]
  node [
    id 1649
    label "supernatural"
  ]
  node [
    id 1650
    label "defray"
  ]
  node [
    id 1651
    label "defenestracja"
  ]
  node [
    id 1652
    label "agonia"
  ]
  node [
    id 1653
    label "spocz&#261;&#263;"
  ]
  node [
    id 1654
    label "spocz&#281;cie"
  ]
  node [
    id 1655
    label "mogi&#322;a"
  ]
  node [
    id 1656
    label "kres_&#380;ycia"
  ]
  node [
    id 1657
    label "pochowanie"
  ]
  node [
    id 1658
    label "szeol"
  ]
  node [
    id 1659
    label "pogrzebanie"
  ]
  node [
    id 1660
    label "chowanie"
  ]
  node [
    id 1661
    label "park_sztywnych"
  ]
  node [
    id 1662
    label "pomnik"
  ]
  node [
    id 1663
    label "nagrobek"
  ]
  node [
    id 1664
    label "&#380;a&#322;oba"
  ]
  node [
    id 1665
    label "prochowisko"
  ]
  node [
    id 1666
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 1667
    label "spoczywanie"
  ]
  node [
    id 1668
    label "zabicie"
  ]
  node [
    id 1669
    label "przenocowanie"
  ]
  node [
    id 1670
    label "pora&#380;ka"
  ]
  node [
    id 1671
    label "nak&#322;adzenie"
  ]
  node [
    id 1672
    label "pouk&#322;adanie"
  ]
  node [
    id 1673
    label "pokrycie"
  ]
  node [
    id 1674
    label "zepsucie"
  ]
  node [
    id 1675
    label "trim"
  ]
  node [
    id 1676
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 1677
    label "ugoszczenie"
  ]
  node [
    id 1678
    label "le&#380;enie"
  ]
  node [
    id 1679
    label "adres"
  ]
  node [
    id 1680
    label "zbudowanie"
  ]
  node [
    id 1681
    label "sytuacja"
  ]
  node [
    id 1682
    label "wygranie"
  ]
  node [
    id 1683
    label "presentation"
  ]
  node [
    id 1684
    label "degenerat"
  ]
  node [
    id 1685
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 1686
    label "zwyrol"
  ]
  node [
    id 1687
    label "czerniak"
  ]
  node [
    id 1688
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1689
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 1690
    label "paszcza"
  ]
  node [
    id 1691
    label "popapraniec"
  ]
  node [
    id 1692
    label "skuba&#263;"
  ]
  node [
    id 1693
    label "skubanie"
  ]
  node [
    id 1694
    label "skubni&#281;cie"
  ]
  node [
    id 1695
    label "agresja"
  ]
  node [
    id 1696
    label "zwierz&#281;ta"
  ]
  node [
    id 1697
    label "fukni&#281;cie"
  ]
  node [
    id 1698
    label "farba"
  ]
  node [
    id 1699
    label "fukanie"
  ]
  node [
    id 1700
    label "istota_&#380;ywa"
  ]
  node [
    id 1701
    label "gad"
  ]
  node [
    id 1702
    label "siedzie&#263;"
  ]
  node [
    id 1703
    label "oswaja&#263;"
  ]
  node [
    id 1704
    label "tresowa&#263;"
  ]
  node [
    id 1705
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 1706
    label "poligamia"
  ]
  node [
    id 1707
    label "oz&#243;r"
  ]
  node [
    id 1708
    label "skubn&#261;&#263;"
  ]
  node [
    id 1709
    label "wios&#322;owa&#263;"
  ]
  node [
    id 1710
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 1711
    label "niecz&#322;owiek"
  ]
  node [
    id 1712
    label "wios&#322;owanie"
  ]
  node [
    id 1713
    label "napasienie_si&#281;"
  ]
  node [
    id 1714
    label "wiwarium"
  ]
  node [
    id 1715
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 1716
    label "animalista"
  ]
  node [
    id 1717
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 1718
    label "hodowla"
  ]
  node [
    id 1719
    label "pasienie_si&#281;"
  ]
  node [
    id 1720
    label "sodomita"
  ]
  node [
    id 1721
    label "monogamia"
  ]
  node [
    id 1722
    label "przyssawka"
  ]
  node [
    id 1723
    label "budowa_cia&#322;a"
  ]
  node [
    id 1724
    label "okrutnik"
  ]
  node [
    id 1725
    label "grzbiet"
  ]
  node [
    id 1726
    label "weterynarz"
  ]
  node [
    id 1727
    label "&#322;eb"
  ]
  node [
    id 1728
    label "wylinka"
  ]
  node [
    id 1729
    label "bestia"
  ]
  node [
    id 1730
    label "poskramia&#263;"
  ]
  node [
    id 1731
    label "fauna"
  ]
  node [
    id 1732
    label "treser"
  ]
  node [
    id 1733
    label "siedzenie"
  ]
  node [
    id 1734
    label "pot&#281;ga"
  ]
  node [
    id 1735
    label "documentation"
  ]
  node [
    id 1736
    label "column"
  ]
  node [
    id 1737
    label "zasadzenie"
  ]
  node [
    id 1738
    label "punkt_odniesienia"
  ]
  node [
    id 1739
    label "zasadzi&#263;"
  ]
  node [
    id 1740
    label "bok"
  ]
  node [
    id 1741
    label "d&#243;&#322;"
  ]
  node [
    id 1742
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1743
    label "podstawowy"
  ]
  node [
    id 1744
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1745
    label "strategia"
  ]
  node [
    id 1746
    label "&#347;ciana"
  ]
  node [
    id 1747
    label "podwini&#281;cie"
  ]
  node [
    id 1748
    label "zap&#322;acenie"
  ]
  node [
    id 1749
    label "przyodzianie"
  ]
  node [
    id 1750
    label "budowla"
  ]
  node [
    id 1751
    label "rozebranie"
  ]
  node [
    id 1752
    label "zak&#322;adka"
  ]
  node [
    id 1753
    label "struktura"
  ]
  node [
    id 1754
    label "poubieranie"
  ]
  node [
    id 1755
    label "infliction"
  ]
  node [
    id 1756
    label "pozak&#322;adanie"
  ]
  node [
    id 1757
    label "przebranie"
  ]
  node [
    id 1758
    label "przywdzianie"
  ]
  node [
    id 1759
    label "obleczenie_si&#281;"
  ]
  node [
    id 1760
    label "utworzenie"
  ]
  node [
    id 1761
    label "twierdzenie"
  ]
  node [
    id 1762
    label "obleczenie"
  ]
  node [
    id 1763
    label "przygotowywanie"
  ]
  node [
    id 1764
    label "wyko&#324;czenie"
  ]
  node [
    id 1765
    label "przygotowanie"
  ]
  node [
    id 1766
    label "przewidzenie"
  ]
  node [
    id 1767
    label "tu&#322;&#243;w"
  ]
  node [
    id 1768
    label "kierunek"
  ]
  node [
    id 1769
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 1770
    label "wielok&#261;t"
  ]
  node [
    id 1771
    label "odcinek"
  ]
  node [
    id 1772
    label "strzelba"
  ]
  node [
    id 1773
    label "lufa"
  ]
  node [
    id 1774
    label "profil"
  ]
  node [
    id 1775
    label "zbocze"
  ]
  node [
    id 1776
    label "przegroda"
  ]
  node [
    id 1777
    label "p&#322;aszczyzna"
  ]
  node [
    id 1778
    label "bariera"
  ]
  node [
    id 1779
    label "facebook"
  ]
  node [
    id 1780
    label "wielo&#347;cian"
  ]
  node [
    id 1781
    label "obstruction"
  ]
  node [
    id 1782
    label "pow&#322;oka"
  ]
  node [
    id 1783
    label "wyrobisko"
  ]
  node [
    id 1784
    label "trudno&#347;&#263;"
  ]
  node [
    id 1785
    label "wykopywa&#263;"
  ]
  node [
    id 1786
    label "wykopanie"
  ]
  node [
    id 1787
    label "&#347;piew"
  ]
  node [
    id 1788
    label "wykopywanie"
  ]
  node [
    id 1789
    label "hole"
  ]
  node [
    id 1790
    label "low"
  ]
  node [
    id 1791
    label "niski"
  ]
  node [
    id 1792
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 1793
    label "depressive_disorder"
  ]
  node [
    id 1794
    label "wykopa&#263;"
  ]
  node [
    id 1795
    label "za&#322;amanie"
  ]
  node [
    id 1796
    label "niezaawansowany"
  ]
  node [
    id 1797
    label "najwa&#380;niejszy"
  ]
  node [
    id 1798
    label "pocz&#261;tkowy"
  ]
  node [
    id 1799
    label "podstawowo"
  ]
  node [
    id 1800
    label "wetkni&#281;cie"
  ]
  node [
    id 1801
    label "przetkanie"
  ]
  node [
    id 1802
    label "anchor"
  ]
  node [
    id 1803
    label "przymocowanie"
  ]
  node [
    id 1804
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1805
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 1806
    label "interposition"
  ]
  node [
    id 1807
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1808
    label "establish"
  ]
  node [
    id 1809
    label "osnowa&#263;"
  ]
  node [
    id 1810
    label "przymocowa&#263;"
  ]
  node [
    id 1811
    label "wetkn&#261;&#263;"
  ]
  node [
    id 1812
    label "dzieci&#324;stwo"
  ]
  node [
    id 1813
    label "pocz&#261;tki"
  ]
  node [
    id 1814
    label "ukra&#347;&#263;"
  ]
  node [
    id 1815
    label "ukradzenie"
  ]
  node [
    id 1816
    label "system"
  ]
  node [
    id 1817
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 1818
    label "operacja"
  ]
  node [
    id 1819
    label "gra"
  ]
  node [
    id 1820
    label "wzorzec_projektowy"
  ]
  node [
    id 1821
    label "dziedzina"
  ]
  node [
    id 1822
    label "wrinkle"
  ]
  node [
    id 1823
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 1824
    label "wyk&#322;adnik_pot&#281;gi"
  ]
  node [
    id 1825
    label "violence"
  ]
  node [
    id 1826
    label "podstawa_pot&#281;gi"
  ]
  node [
    id 1827
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1828
    label "potencja"
  ]
  node [
    id 1829
    label "iloczyn"
  ]
  node [
    id 1830
    label "jaki&#347;"
  ]
  node [
    id 1831
    label "przyzwoity"
  ]
  node [
    id 1832
    label "ciekawy"
  ]
  node [
    id 1833
    label "jako&#347;"
  ]
  node [
    id 1834
    label "jako_tako"
  ]
  node [
    id 1835
    label "niez&#322;y"
  ]
  node [
    id 1836
    label "dziwny"
  ]
  node [
    id 1837
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 1838
    label "management"
  ]
  node [
    id 1839
    label "resolution"
  ]
  node [
    id 1840
    label "zdecydowanie"
  ]
  node [
    id 1841
    label "zapis"
  ]
  node [
    id 1842
    label "&#347;wiadectwo"
  ]
  node [
    id 1843
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1844
    label "parafa"
  ]
  node [
    id 1845
    label "plik"
  ]
  node [
    id 1846
    label "raport&#243;wka"
  ]
  node [
    id 1847
    label "utw&#243;r"
  ]
  node [
    id 1848
    label "record"
  ]
  node [
    id 1849
    label "registratura"
  ]
  node [
    id 1850
    label "dokumentacja"
  ]
  node [
    id 1851
    label "fascyku&#322;"
  ]
  node [
    id 1852
    label "writing"
  ]
  node [
    id 1853
    label "sygnatariusz"
  ]
  node [
    id 1854
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 1855
    label "pewnie"
  ]
  node [
    id 1856
    label "zauwa&#380;alnie"
  ]
  node [
    id 1857
    label "resoluteness"
  ]
  node [
    id 1858
    label "judgment"
  ]
  node [
    id 1859
    label "matter"
  ]
  node [
    id 1860
    label "splot"
  ]
  node [
    id 1861
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1862
    label "faktura"
  ]
  node [
    id 1863
    label "whirl"
  ]
  node [
    id 1864
    label "tkanina"
  ]
  node [
    id 1865
    label "mieszanka"
  ]
  node [
    id 1866
    label "concatenation"
  ]
  node [
    id 1867
    label "konstrukcja"
  ]
  node [
    id 1868
    label "osnowa"
  ]
  node [
    id 1869
    label "tor"
  ]
  node [
    id 1870
    label "perypetia"
  ]
  node [
    id 1871
    label "opowiadanie"
  ]
  node [
    id 1872
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1873
    label "porozmieszczanie"
  ]
  node [
    id 1874
    label "wyst&#281;powanie"
  ]
  node [
    id 1875
    label "layout"
  ]
  node [
    id 1876
    label "materia&#322;_budowlany"
  ]
  node [
    id 1877
    label "tile"
  ]
  node [
    id 1878
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1879
    label "g&#322;&#243;wka"
  ]
  node [
    id 1880
    label "woz&#243;wka"
  ]
  node [
    id 1881
    label "grupa_dyskusyjna"
  ]
  node [
    id 1882
    label "plac"
  ]
  node [
    id 1883
    label "bazylika"
  ]
  node [
    id 1884
    label "portal"
  ]
  node [
    id 1885
    label "konferencja"
  ]
  node [
    id 1886
    label "agora"
  ]
  node [
    id 1887
    label "paj&#261;k"
  ]
  node [
    id 1888
    label "przewodnik"
  ]
  node [
    id 1889
    label "topikowate"
  ]
  node [
    id 1890
    label "try"
  ]
  node [
    id 1891
    label "podkre&#347;li&#263;"
  ]
  node [
    id 1892
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 1893
    label "round"
  ]
  node [
    id 1894
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 1895
    label "wynik"
  ]
  node [
    id 1896
    label "kreska"
  ]
  node [
    id 1897
    label "narysowa&#263;"
  ]
  node [
    id 1898
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 1899
    label "kolejny"
  ]
  node [
    id 1900
    label "osobno"
  ]
  node [
    id 1901
    label "r&#243;&#380;ny"
  ]
  node [
    id 1902
    label "inszy"
  ]
  node [
    id 1903
    label "inaczej"
  ]
  node [
    id 1904
    label "odr&#281;bny"
  ]
  node [
    id 1905
    label "nast&#281;pnie"
  ]
  node [
    id 1906
    label "nastopny"
  ]
  node [
    id 1907
    label "kolejno"
  ]
  node [
    id 1908
    label "kt&#243;ry&#347;"
  ]
  node [
    id 1909
    label "r&#243;&#380;nie"
  ]
  node [
    id 1910
    label "niestandardowo"
  ]
  node [
    id 1911
    label "individually"
  ]
  node [
    id 1912
    label "udzielnie"
  ]
  node [
    id 1913
    label "osobnie"
  ]
  node [
    id 1914
    label "odr&#281;bnie"
  ]
  node [
    id 1915
    label "osobny"
  ]
  node [
    id 1916
    label "ma&#322;owa&#380;ny"
  ]
  node [
    id 1917
    label "nieznaczny"
  ]
  node [
    id 1918
    label "nieistotnie"
  ]
  node [
    id 1919
    label "s&#322;aby"
  ]
  node [
    id 1920
    label "nieznacznie"
  ]
  node [
    id 1921
    label "drobnostkowy"
  ]
  node [
    id 1922
    label "niewa&#380;ny"
  ]
  node [
    id 1923
    label "ma&#322;y"
  ]
  node [
    id 1924
    label "nietrwa&#322;y"
  ]
  node [
    id 1925
    label "mizerny"
  ]
  node [
    id 1926
    label "marnie"
  ]
  node [
    id 1927
    label "delikatny"
  ]
  node [
    id 1928
    label "po&#347;ledni"
  ]
  node [
    id 1929
    label "niezdrowy"
  ]
  node [
    id 1930
    label "z&#322;y"
  ]
  node [
    id 1931
    label "nieumiej&#281;tny"
  ]
  node [
    id 1932
    label "s&#322;abo"
  ]
  node [
    id 1933
    label "lura"
  ]
  node [
    id 1934
    label "nieudany"
  ]
  node [
    id 1935
    label "s&#322;abowity"
  ]
  node [
    id 1936
    label "zawodny"
  ]
  node [
    id 1937
    label "&#322;agodny"
  ]
  node [
    id 1938
    label "md&#322;y"
  ]
  node [
    id 1939
    label "niedoskona&#322;y"
  ]
  node [
    id 1940
    label "przemijaj&#261;cy"
  ]
  node [
    id 1941
    label "niemocny"
  ]
  node [
    id 1942
    label "niefajny"
  ]
  node [
    id 1943
    label "kiepsko"
  ]
  node [
    id 1944
    label "niepowa&#380;nie"
  ]
  node [
    id 1945
    label "decide"
  ]
  node [
    id 1946
    label "determine"
  ]
  node [
    id 1947
    label "zareagowa&#263;"
  ]
  node [
    id 1948
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 1949
    label "allude"
  ]
  node [
    id 1950
    label "chemia"
  ]
  node [
    id 1951
    label "reakcja_chemiczna"
  ]
  node [
    id 1952
    label "propagate"
  ]
  node [
    id 1953
    label "wp&#322;aci&#263;"
  ]
  node [
    id 1954
    label "transfer"
  ]
  node [
    id 1955
    label "wys&#322;a&#263;"
  ]
  node [
    id 1956
    label "tenis"
  ]
  node [
    id 1957
    label "supply"
  ]
  node [
    id 1958
    label "ustawi&#263;"
  ]
  node [
    id 1959
    label "siatk&#243;wka"
  ]
  node [
    id 1960
    label "zagra&#263;"
  ]
  node [
    id 1961
    label "jedzenie"
  ]
  node [
    id 1962
    label "poinformowa&#263;"
  ]
  node [
    id 1963
    label "introduce"
  ]
  node [
    id 1964
    label "nafaszerowa&#263;"
  ]
  node [
    id 1965
    label "zaserwowa&#263;"
  ]
  node [
    id 1966
    label "ship"
  ]
  node [
    id 1967
    label "post"
  ]
  node [
    id 1968
    label "convey"
  ]
  node [
    id 1969
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 1970
    label "przekaz"
  ]
  node [
    id 1971
    label "zamiana"
  ]
  node [
    id 1972
    label "release"
  ]
  node [
    id 1973
    label "lista_transferowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 5
    target 560
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 563
  ]
  edge [
    source 5
    target 564
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 565
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 566
  ]
  edge [
    source 5
    target 567
  ]
  edge [
    source 5
    target 568
  ]
  edge [
    source 5
    target 569
  ]
  edge [
    source 5
    target 570
  ]
  edge [
    source 5
    target 571
  ]
  edge [
    source 5
    target 572
  ]
  edge [
    source 5
    target 573
  ]
  edge [
    source 5
    target 574
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 575
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 576
  ]
  edge [
    source 5
    target 577
  ]
  edge [
    source 5
    target 578
  ]
  edge [
    source 5
    target 579
  ]
  edge [
    source 5
    target 580
  ]
  edge [
    source 5
    target 581
  ]
  edge [
    source 5
    target 582
  ]
  edge [
    source 5
    target 583
  ]
  edge [
    source 5
    target 584
  ]
  edge [
    source 5
    target 585
  ]
  edge [
    source 5
    target 586
  ]
  edge [
    source 5
    target 587
  ]
  edge [
    source 5
    target 588
  ]
  edge [
    source 5
    target 589
  ]
  edge [
    source 5
    target 590
  ]
  edge [
    source 5
    target 591
  ]
  edge [
    source 5
    target 592
  ]
  edge [
    source 5
    target 593
  ]
  edge [
    source 5
    target 594
  ]
  edge [
    source 5
    target 595
  ]
  edge [
    source 5
    target 596
  ]
  edge [
    source 5
    target 597
  ]
  edge [
    source 5
    target 598
  ]
  edge [
    source 5
    target 599
  ]
  edge [
    source 5
    target 600
  ]
  edge [
    source 5
    target 601
  ]
  edge [
    source 5
    target 602
  ]
  edge [
    source 5
    target 603
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 604
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 605
  ]
  edge [
    source 5
    target 606
  ]
  edge [
    source 5
    target 607
  ]
  edge [
    source 5
    target 608
  ]
  edge [
    source 5
    target 609
  ]
  edge [
    source 5
    target 610
  ]
  edge [
    source 5
    target 611
  ]
  edge [
    source 5
    target 612
  ]
  edge [
    source 5
    target 613
  ]
  edge [
    source 5
    target 614
  ]
  edge [
    source 5
    target 615
  ]
  edge [
    source 5
    target 616
  ]
  edge [
    source 5
    target 617
  ]
  edge [
    source 5
    target 618
  ]
  edge [
    source 5
    target 619
  ]
  edge [
    source 5
    target 620
  ]
  edge [
    source 5
    target 621
  ]
  edge [
    source 5
    target 622
  ]
  edge [
    source 5
    target 623
  ]
  edge [
    source 5
    target 624
  ]
  edge [
    source 5
    target 625
  ]
  edge [
    source 5
    target 626
  ]
  edge [
    source 5
    target 627
  ]
  edge [
    source 5
    target 628
  ]
  edge [
    source 5
    target 629
  ]
  edge [
    source 5
    target 630
  ]
  edge [
    source 5
    target 631
  ]
  edge [
    source 5
    target 632
  ]
  edge [
    source 5
    target 633
  ]
  edge [
    source 5
    target 634
  ]
  edge [
    source 5
    target 635
  ]
  edge [
    source 5
    target 636
  ]
  edge [
    source 5
    target 637
  ]
  edge [
    source 5
    target 638
  ]
  edge [
    source 5
    target 639
  ]
  edge [
    source 5
    target 640
  ]
  edge [
    source 5
    target 641
  ]
  edge [
    source 5
    target 642
  ]
  edge [
    source 5
    target 643
  ]
  edge [
    source 5
    target 644
  ]
  edge [
    source 5
    target 645
  ]
  edge [
    source 5
    target 646
  ]
  edge [
    source 5
    target 647
  ]
  edge [
    source 5
    target 648
  ]
  edge [
    source 5
    target 649
  ]
  edge [
    source 5
    target 650
  ]
  edge [
    source 5
    target 651
  ]
  edge [
    source 5
    target 652
  ]
  edge [
    source 5
    target 653
  ]
  edge [
    source 5
    target 654
  ]
  edge [
    source 5
    target 655
  ]
  edge [
    source 5
    target 656
  ]
  edge [
    source 5
    target 657
  ]
  edge [
    source 5
    target 658
  ]
  edge [
    source 5
    target 659
  ]
  edge [
    source 5
    target 660
  ]
  edge [
    source 5
    target 661
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 7
    target 702
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 704
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 706
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 707
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 708
  ]
  edge [
    source 7
    target 709
  ]
  edge [
    source 7
    target 710
  ]
  edge [
    source 7
    target 711
  ]
  edge [
    source 7
    target 712
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 713
  ]
  edge [
    source 7
    target 714
  ]
  edge [
    source 7
    target 715
  ]
  edge [
    source 7
    target 716
  ]
  edge [
    source 7
    target 717
  ]
  edge [
    source 7
    target 718
  ]
  edge [
    source 7
    target 719
  ]
  edge [
    source 7
    target 720
  ]
  edge [
    source 7
    target 721
  ]
  edge [
    source 7
    target 722
  ]
  edge [
    source 7
    target 723
  ]
  edge [
    source 7
    target 724
  ]
  edge [
    source 7
    target 725
  ]
  edge [
    source 7
    target 726
  ]
  edge [
    source 7
    target 727
  ]
  edge [
    source 7
    target 728
  ]
  edge [
    source 7
    target 729
  ]
  edge [
    source 7
    target 730
  ]
  edge [
    source 7
    target 731
  ]
  edge [
    source 7
    target 732
  ]
  edge [
    source 7
    target 733
  ]
  edge [
    source 7
    target 734
  ]
  edge [
    source 7
    target 735
  ]
  edge [
    source 7
    target 736
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 757
  ]
  edge [
    source 9
    target 758
  ]
  edge [
    source 9
    target 759
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 9
    target 800
  ]
  edge [
    source 9
    target 801
  ]
  edge [
    source 9
    target 802
  ]
  edge [
    source 9
    target 803
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 805
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 9
    target 807
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 808
  ]
  edge [
    source 9
    target 809
  ]
  edge [
    source 9
    target 810
  ]
  edge [
    source 9
    target 811
  ]
  edge [
    source 9
    target 812
  ]
  edge [
    source 9
    target 813
  ]
  edge [
    source 9
    target 814
  ]
  edge [
    source 9
    target 815
  ]
  edge [
    source 9
    target 816
  ]
  edge [
    source 9
    target 817
  ]
  edge [
    source 9
    target 818
  ]
  edge [
    source 9
    target 819
  ]
  edge [
    source 9
    target 820
  ]
  edge [
    source 9
    target 821
  ]
  edge [
    source 9
    target 822
  ]
  edge [
    source 9
    target 823
  ]
  edge [
    source 9
    target 824
  ]
  edge [
    source 9
    target 825
  ]
  edge [
    source 9
    target 826
  ]
  edge [
    source 9
    target 827
  ]
  edge [
    source 9
    target 828
  ]
  edge [
    source 9
    target 829
  ]
  edge [
    source 9
    target 830
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 831
  ]
  edge [
    source 9
    target 832
  ]
  edge [
    source 9
    target 833
  ]
  edge [
    source 9
    target 834
  ]
  edge [
    source 9
    target 835
  ]
  edge [
    source 9
    target 836
  ]
  edge [
    source 9
    target 837
  ]
  edge [
    source 9
    target 838
  ]
  edge [
    source 9
    target 839
  ]
  edge [
    source 9
    target 840
  ]
  edge [
    source 9
    target 841
  ]
  edge [
    source 9
    target 842
  ]
  edge [
    source 9
    target 843
  ]
  edge [
    source 9
    target 844
  ]
  edge [
    source 9
    target 845
  ]
  edge [
    source 9
    target 846
  ]
  edge [
    source 9
    target 847
  ]
  edge [
    source 9
    target 848
  ]
  edge [
    source 9
    target 849
  ]
  edge [
    source 9
    target 850
  ]
  edge [
    source 9
    target 851
  ]
  edge [
    source 9
    target 852
  ]
  edge [
    source 9
    target 853
  ]
  edge [
    source 9
    target 854
  ]
  edge [
    source 9
    target 855
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 856
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 857
  ]
  edge [
    source 10
    target 858
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 859
  ]
  edge [
    source 10
    target 860
  ]
  edge [
    source 10
    target 861
  ]
  edge [
    source 10
    target 862
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 12
    target 891
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 893
  ]
  edge [
    source 12
    target 199
  ]
  edge [
    source 12
    target 34
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 216
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 223
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 197
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 12
    target 914
  ]
  edge [
    source 12
    target 915
  ]
  edge [
    source 12
    target 916
  ]
  edge [
    source 12
    target 917
  ]
  edge [
    source 12
    target 918
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 919
  ]
  edge [
    source 12
    target 920
  ]
  edge [
    source 12
    target 921
  ]
  edge [
    source 12
    target 922
  ]
  edge [
    source 12
    target 923
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 14
    target 576
  ]
  edge [
    source 14
    target 925
  ]
  edge [
    source 14
    target 926
  ]
  edge [
    source 14
    target 927
  ]
  edge [
    source 14
    target 473
  ]
  edge [
    source 14
    target 928
  ]
  edge [
    source 14
    target 929
  ]
  edge [
    source 14
    target 930
  ]
  edge [
    source 14
    target 931
  ]
  edge [
    source 14
    target 932
  ]
  edge [
    source 14
    target 933
  ]
  edge [
    source 14
    target 934
  ]
  edge [
    source 14
    target 935
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 937
  ]
  edge [
    source 14
    target 938
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 940
  ]
  edge [
    source 14
    target 941
  ]
  edge [
    source 14
    target 942
  ]
  edge [
    source 14
    target 943
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 945
  ]
  edge [
    source 14
    target 946
  ]
  edge [
    source 14
    target 947
  ]
  edge [
    source 14
    target 948
  ]
  edge [
    source 14
    target 949
  ]
  edge [
    source 14
    target 950
  ]
  edge [
    source 14
    target 951
  ]
  edge [
    source 14
    target 952
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 943
  ]
  edge [
    source 15
    target 954
  ]
  edge [
    source 15
    target 955
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 277
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 275
  ]
  edge [
    source 15
    target 375
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 278
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 956
  ]
  edge [
    source 15
    target 957
  ]
  edge [
    source 15
    target 254
  ]
  edge [
    source 15
    target 958
  ]
  edge [
    source 15
    target 959
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 252
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 258
  ]
  edge [
    source 15
    target 242
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 443
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 576
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 961
  ]
  edge [
    source 16
    target 962
  ]
  edge [
    source 16
    target 963
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 966
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 967
  ]
  edge [
    source 16
    target 968
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 972
  ]
  edge [
    source 17
    target 973
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 53
  ]
  edge [
    source 18
    target 54
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 974
  ]
  edge [
    source 21
    target 975
  ]
  edge [
    source 21
    target 976
  ]
  edge [
    source 21
    target 977
  ]
  edge [
    source 21
    target 978
  ]
  edge [
    source 21
    target 979
  ]
  edge [
    source 21
    target 980
  ]
  edge [
    source 21
    target 981
  ]
  edge [
    source 21
    target 982
  ]
  edge [
    source 21
    target 983
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 181
  ]
  edge [
    source 22
    target 984
  ]
  edge [
    source 22
    target 795
  ]
  edge [
    source 22
    target 385
  ]
  edge [
    source 22
    target 985
  ]
  edge [
    source 22
    target 986
  ]
  edge [
    source 22
    target 987
  ]
  edge [
    source 22
    target 988
  ]
  edge [
    source 22
    target 989
  ]
  edge [
    source 22
    target 518
  ]
  edge [
    source 22
    target 990
  ]
  edge [
    source 22
    target 991
  ]
  edge [
    source 22
    target 992
  ]
  edge [
    source 22
    target 993
  ]
  edge [
    source 22
    target 555
  ]
  edge [
    source 22
    target 994
  ]
  edge [
    source 22
    target 995
  ]
  edge [
    source 22
    target 996
  ]
  edge [
    source 22
    target 997
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 998
  ]
  edge [
    source 22
    target 999
  ]
  edge [
    source 22
    target 1000
  ]
  edge [
    source 22
    target 1001
  ]
  edge [
    source 22
    target 1002
  ]
  edge [
    source 22
    target 1003
  ]
  edge [
    source 22
    target 1004
  ]
  edge [
    source 22
    target 765
  ]
  edge [
    source 22
    target 1005
  ]
  edge [
    source 22
    target 522
  ]
  edge [
    source 22
    target 1006
  ]
  edge [
    source 22
    target 1007
  ]
  edge [
    source 22
    target 1008
  ]
  edge [
    source 22
    target 1009
  ]
  edge [
    source 22
    target 576
  ]
  edge [
    source 22
    target 1010
  ]
  edge [
    source 22
    target 413
  ]
  edge [
    source 22
    target 414
  ]
  edge [
    source 22
    target 415
  ]
  edge [
    source 22
    target 416
  ]
  edge [
    source 22
    target 417
  ]
  edge [
    source 22
    target 418
  ]
  edge [
    source 22
    target 419
  ]
  edge [
    source 22
    target 420
  ]
  edge [
    source 22
    target 421
  ]
  edge [
    source 22
    target 422
  ]
  edge [
    source 22
    target 423
  ]
  edge [
    source 22
    target 424
  ]
  edge [
    source 22
    target 425
  ]
  edge [
    source 22
    target 426
  ]
  edge [
    source 22
    target 427
  ]
  edge [
    source 22
    target 428
  ]
  edge [
    source 22
    target 429
  ]
  edge [
    source 22
    target 430
  ]
  edge [
    source 22
    target 1011
  ]
  edge [
    source 22
    target 796
  ]
  edge [
    source 22
    target 797
  ]
  edge [
    source 22
    target 1012
  ]
  edge [
    source 22
    target 258
  ]
  edge [
    source 22
    target 1013
  ]
  edge [
    source 22
    target 1014
  ]
  edge [
    source 22
    target 443
  ]
  edge [
    source 22
    target 1015
  ]
  edge [
    source 22
    target 63
  ]
  edge [
    source 22
    target 1016
  ]
  edge [
    source 22
    target 1017
  ]
  edge [
    source 22
    target 1018
  ]
  edge [
    source 22
    target 242
  ]
  edge [
    source 22
    target 1019
  ]
  edge [
    source 22
    target 1020
  ]
  edge [
    source 22
    target 1021
  ]
  edge [
    source 22
    target 1022
  ]
  edge [
    source 22
    target 835
  ]
  edge [
    source 22
    target 269
  ]
  edge [
    source 22
    target 1023
  ]
  edge [
    source 22
    target 271
  ]
  edge [
    source 22
    target 546
  ]
  edge [
    source 22
    target 547
  ]
  edge [
    source 22
    target 595
  ]
  edge [
    source 22
    target 1024
  ]
  edge [
    source 22
    target 597
  ]
  edge [
    source 22
    target 551
  ]
  edge [
    source 22
    target 545
  ]
  edge [
    source 22
    target 588
  ]
  edge [
    source 22
    target 552
  ]
  edge [
    source 22
    target 1025
  ]
  edge [
    source 22
    target 520
  ]
  edge [
    source 22
    target 1026
  ]
  edge [
    source 22
    target 1027
  ]
  edge [
    source 22
    target 701
  ]
  edge [
    source 22
    target 1028
  ]
  edge [
    source 22
    target 1029
  ]
  edge [
    source 22
    target 1030
  ]
  edge [
    source 22
    target 1031
  ]
  edge [
    source 22
    target 1032
  ]
  edge [
    source 22
    target 1033
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 1034
  ]
  edge [
    source 22
    target 1035
  ]
  edge [
    source 22
    target 1036
  ]
  edge [
    source 22
    target 1037
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 49
  ]
  edge [
    source 23
    target 50
  ]
  edge [
    source 23
    target 1021
  ]
  edge [
    source 23
    target 1038
  ]
  edge [
    source 23
    target 1039
  ]
  edge [
    source 23
    target 1001
  ]
  edge [
    source 23
    target 1040
  ]
  edge [
    source 23
    target 1041
  ]
  edge [
    source 23
    target 1042
  ]
  edge [
    source 23
    target 1043
  ]
  edge [
    source 23
    target 1044
  ]
  edge [
    source 23
    target 1045
  ]
  edge [
    source 23
    target 1046
  ]
  edge [
    source 23
    target 1047
  ]
  edge [
    source 23
    target 1048
  ]
  edge [
    source 23
    target 1028
  ]
  edge [
    source 23
    target 36
  ]
  edge [
    source 23
    target 1049
  ]
  edge [
    source 23
    target 1050
  ]
  edge [
    source 23
    target 1051
  ]
  edge [
    source 23
    target 386
  ]
  edge [
    source 23
    target 1052
  ]
  edge [
    source 23
    target 1053
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 387
  ]
  edge [
    source 23
    target 835
  ]
  edge [
    source 23
    target 1054
  ]
  edge [
    source 23
    target 1055
  ]
  edge [
    source 23
    target 66
  ]
  edge [
    source 23
    target 1056
  ]
  edge [
    source 23
    target 1057
  ]
  edge [
    source 23
    target 388
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 1058
  ]
  edge [
    source 23
    target 1059
  ]
  edge [
    source 23
    target 1060
  ]
  edge [
    source 23
    target 236
  ]
  edge [
    source 23
    target 237
  ]
  edge [
    source 23
    target 1061
  ]
  edge [
    source 23
    target 1062
  ]
  edge [
    source 23
    target 1063
  ]
  edge [
    source 23
    target 1064
  ]
  edge [
    source 23
    target 1065
  ]
  edge [
    source 23
    target 1066
  ]
  edge [
    source 23
    target 1067
  ]
  edge [
    source 23
    target 1068
  ]
  edge [
    source 23
    target 1069
  ]
  edge [
    source 23
    target 774
  ]
  edge [
    source 23
    target 1070
  ]
  edge [
    source 23
    target 1071
  ]
  edge [
    source 23
    target 1072
  ]
  edge [
    source 23
    target 1073
  ]
  edge [
    source 23
    target 1074
  ]
  edge [
    source 23
    target 280
  ]
  edge [
    source 23
    target 588
  ]
  edge [
    source 23
    target 1075
  ]
  edge [
    source 23
    target 1003
  ]
  edge [
    source 23
    target 1076
  ]
  edge [
    source 23
    target 1077
  ]
  edge [
    source 23
    target 1078
  ]
  edge [
    source 23
    target 1079
  ]
  edge [
    source 23
    target 1080
  ]
  edge [
    source 23
    target 54
  ]
  edge [
    source 23
    target 1081
  ]
  edge [
    source 23
    target 1082
  ]
  edge [
    source 23
    target 1083
  ]
  edge [
    source 23
    target 1084
  ]
  edge [
    source 23
    target 1085
  ]
  edge [
    source 23
    target 1086
  ]
  edge [
    source 23
    target 1087
  ]
  edge [
    source 23
    target 1088
  ]
  edge [
    source 23
    target 1089
  ]
  edge [
    source 23
    target 286
  ]
  edge [
    source 23
    target 408
  ]
  edge [
    source 23
    target 1090
  ]
  edge [
    source 23
    target 1091
  ]
  edge [
    source 23
    target 1092
  ]
  edge [
    source 23
    target 1093
  ]
  edge [
    source 23
    target 1094
  ]
  edge [
    source 23
    target 1095
  ]
  edge [
    source 23
    target 1096
  ]
  edge [
    source 23
    target 1097
  ]
  edge [
    source 23
    target 628
  ]
  edge [
    source 23
    target 1098
  ]
  edge [
    source 23
    target 1099
  ]
  edge [
    source 23
    target 1100
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 1101
  ]
  edge [
    source 23
    target 1102
  ]
  edge [
    source 23
    target 1103
  ]
  edge [
    source 23
    target 1104
  ]
  edge [
    source 23
    target 1105
  ]
  edge [
    source 23
    target 1106
  ]
  edge [
    source 23
    target 396
  ]
  edge [
    source 23
    target 1107
  ]
  edge [
    source 23
    target 1108
  ]
  edge [
    source 23
    target 1109
  ]
  edge [
    source 23
    target 1110
  ]
  edge [
    source 23
    target 1111
  ]
  edge [
    source 23
    target 1112
  ]
  edge [
    source 23
    target 1113
  ]
  edge [
    source 23
    target 1114
  ]
  edge [
    source 23
    target 231
  ]
  edge [
    source 23
    target 1115
  ]
  edge [
    source 23
    target 1116
  ]
  edge [
    source 23
    target 1117
  ]
  edge [
    source 23
    target 1118
  ]
  edge [
    source 23
    target 1119
  ]
  edge [
    source 23
    target 1120
  ]
  edge [
    source 23
    target 708
  ]
  edge [
    source 23
    target 709
  ]
  edge [
    source 23
    target 1121
  ]
  edge [
    source 23
    target 1122
  ]
  edge [
    source 23
    target 1123
  ]
  edge [
    source 23
    target 1124
  ]
  edge [
    source 23
    target 1125
  ]
  edge [
    source 23
    target 679
  ]
  edge [
    source 23
    target 694
  ]
  edge [
    source 23
    target 1126
  ]
  edge [
    source 23
    target 1127
  ]
  edge [
    source 23
    target 300
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 536
  ]
  edge [
    source 23
    target 1128
  ]
  edge [
    source 23
    target 1129
  ]
  edge [
    source 23
    target 729
  ]
  edge [
    source 23
    target 1130
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 80
  ]
  edge [
    source 23
    target 882
  ]
  edge [
    source 23
    target 1131
  ]
  edge [
    source 23
    target 1132
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 1133
  ]
  edge [
    source 23
    target 1134
  ]
  edge [
    source 23
    target 1135
  ]
  edge [
    source 23
    target 1136
  ]
  edge [
    source 23
    target 1137
  ]
  edge [
    source 23
    target 1138
  ]
  edge [
    source 23
    target 1139
  ]
  edge [
    source 23
    target 1140
  ]
  edge [
    source 23
    target 1141
  ]
  edge [
    source 23
    target 1142
  ]
  edge [
    source 23
    target 1143
  ]
  edge [
    source 23
    target 1144
  ]
  edge [
    source 23
    target 1145
  ]
  edge [
    source 23
    target 1146
  ]
  edge [
    source 23
    target 1147
  ]
  edge [
    source 23
    target 795
  ]
  edge [
    source 23
    target 985
  ]
  edge [
    source 23
    target 987
  ]
  edge [
    source 23
    target 1148
  ]
  edge [
    source 23
    target 1149
  ]
  edge [
    source 23
    target 58
  ]
  edge [
    source 23
    target 1150
  ]
  edge [
    source 23
    target 1151
  ]
  edge [
    source 23
    target 555
  ]
  edge [
    source 23
    target 1152
  ]
  edge [
    source 23
    target 599
  ]
  edge [
    source 23
    target 1153
  ]
  edge [
    source 23
    target 1154
  ]
  edge [
    source 23
    target 1155
  ]
  edge [
    source 23
    target 258
  ]
  edge [
    source 23
    target 1156
  ]
  edge [
    source 23
    target 1157
  ]
  edge [
    source 23
    target 1158
  ]
  edge [
    source 23
    target 1159
  ]
  edge [
    source 23
    target 181
  ]
  edge [
    source 23
    target 1160
  ]
  edge [
    source 23
    target 1161
  ]
  edge [
    source 23
    target 1162
  ]
  edge [
    source 23
    target 1163
  ]
  edge [
    source 23
    target 443
  ]
  edge [
    source 23
    target 1164
  ]
  edge [
    source 23
    target 1165
  ]
  edge [
    source 23
    target 1166
  ]
  edge [
    source 23
    target 1167
  ]
  edge [
    source 23
    target 1168
  ]
  edge [
    source 23
    target 1169
  ]
  edge [
    source 23
    target 1170
  ]
  edge [
    source 23
    target 1171
  ]
  edge [
    source 23
    target 1172
  ]
  edge [
    source 23
    target 1010
  ]
  edge [
    source 23
    target 1173
  ]
  edge [
    source 23
    target 1174
  ]
  edge [
    source 23
    target 1175
  ]
  edge [
    source 23
    target 1176
  ]
  edge [
    source 23
    target 1177
  ]
  edge [
    source 23
    target 1178
  ]
  edge [
    source 23
    target 1179
  ]
  edge [
    source 23
    target 1180
  ]
  edge [
    source 23
    target 1181
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1182
  ]
  edge [
    source 24
    target 1183
  ]
  edge [
    source 24
    target 1184
  ]
  edge [
    source 24
    target 1088
  ]
  edge [
    source 24
    target 1185
  ]
  edge [
    source 24
    target 1186
  ]
  edge [
    source 24
    target 1187
  ]
  edge [
    source 24
    target 1188
  ]
  edge [
    source 24
    target 1189
  ]
  edge [
    source 24
    target 385
  ]
  edge [
    source 24
    target 1190
  ]
  edge [
    source 24
    target 934
  ]
  edge [
    source 24
    target 1191
  ]
  edge [
    source 24
    target 1192
  ]
  edge [
    source 24
    target 1193
  ]
  edge [
    source 24
    target 1194
  ]
  edge [
    source 24
    target 1195
  ]
  edge [
    source 24
    target 1196
  ]
  edge [
    source 24
    target 1197
  ]
  edge [
    source 24
    target 1198
  ]
  edge [
    source 24
    target 1199
  ]
  edge [
    source 24
    target 44
  ]
  edge [
    source 24
    target 1200
  ]
  edge [
    source 24
    target 1201
  ]
  edge [
    source 24
    target 1202
  ]
  edge [
    source 24
    target 1203
  ]
  edge [
    source 24
    target 1204
  ]
  edge [
    source 24
    target 945
  ]
  edge [
    source 24
    target 1205
  ]
  edge [
    source 24
    target 1206
  ]
  edge [
    source 24
    target 929
  ]
  edge [
    source 24
    target 1207
  ]
  edge [
    source 24
    target 1208
  ]
  edge [
    source 24
    target 104
  ]
  edge [
    source 24
    target 93
  ]
  edge [
    source 24
    target 1209
  ]
  edge [
    source 24
    target 1210
  ]
  edge [
    source 24
    target 1211
  ]
  edge [
    source 24
    target 1212
  ]
  edge [
    source 24
    target 282
  ]
  edge [
    source 24
    target 1213
  ]
  edge [
    source 24
    target 1214
  ]
  edge [
    source 24
    target 1215
  ]
  edge [
    source 24
    target 1216
  ]
  edge [
    source 24
    target 1217
  ]
  edge [
    source 24
    target 1218
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1219
  ]
  edge [
    source 25
    target 867
  ]
  edge [
    source 25
    target 1190
  ]
  edge [
    source 25
    target 1220
  ]
  edge [
    source 25
    target 1221
  ]
  edge [
    source 25
    target 1222
  ]
  edge [
    source 25
    target 1223
  ]
  edge [
    source 25
    target 772
  ]
  edge [
    source 25
    target 1224
  ]
  edge [
    source 25
    target 775
  ]
  edge [
    source 25
    target 1225
  ]
  edge [
    source 25
    target 935
  ]
  edge [
    source 25
    target 1226
  ]
  edge [
    source 25
    target 1227
  ]
  edge [
    source 25
    target 1228
  ]
  edge [
    source 25
    target 1229
  ]
  edge [
    source 25
    target 1230
  ]
  edge [
    source 25
    target 1231
  ]
  edge [
    source 25
    target 1232
  ]
  edge [
    source 25
    target 1233
  ]
  edge [
    source 25
    target 1234
  ]
  edge [
    source 25
    target 1235
  ]
  edge [
    source 25
    target 1236
  ]
  edge [
    source 25
    target 1237
  ]
  edge [
    source 25
    target 130
  ]
  edge [
    source 25
    target 1238
  ]
  edge [
    source 25
    target 1239
  ]
  edge [
    source 25
    target 657
  ]
  edge [
    source 25
    target 1240
  ]
  edge [
    source 25
    target 1241
  ]
  edge [
    source 25
    target 1242
  ]
  edge [
    source 25
    target 1243
  ]
  edge [
    source 25
    target 1244
  ]
  edge [
    source 25
    target 1245
  ]
  edge [
    source 25
    target 1246
  ]
  edge [
    source 25
    target 1247
  ]
  edge [
    source 25
    target 1248
  ]
  edge [
    source 25
    target 104
  ]
  edge [
    source 25
    target 1249
  ]
  edge [
    source 25
    target 1089
  ]
  edge [
    source 25
    target 1038
  ]
  edge [
    source 25
    target 1250
  ]
  edge [
    source 25
    target 739
  ]
  edge [
    source 25
    target 1251
  ]
  edge [
    source 25
    target 1252
  ]
  edge [
    source 25
    target 1253
  ]
  edge [
    source 25
    target 1254
  ]
  edge [
    source 25
    target 1255
  ]
  edge [
    source 25
    target 1256
  ]
  edge [
    source 25
    target 815
  ]
  edge [
    source 25
    target 848
  ]
  edge [
    source 25
    target 1257
  ]
  edge [
    source 25
    target 1258
  ]
  edge [
    source 25
    target 1088
  ]
  edge [
    source 25
    target 1259
  ]
  edge [
    source 25
    target 1260
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1261
  ]
  edge [
    source 26
    target 1262
  ]
  edge [
    source 26
    target 1263
  ]
  edge [
    source 26
    target 1264
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 44
  ]
  edge [
    source 27
    target 45
  ]
  edge [
    source 27
    target 1265
  ]
  edge [
    source 27
    target 1266
  ]
  edge [
    source 27
    target 1267
  ]
  edge [
    source 27
    target 1268
  ]
  edge [
    source 27
    target 1269
  ]
  edge [
    source 27
    target 1270
  ]
  edge [
    source 27
    target 199
  ]
  edge [
    source 27
    target 1271
  ]
  edge [
    source 27
    target 1272
  ]
  edge [
    source 27
    target 104
  ]
  edge [
    source 27
    target 1273
  ]
  edge [
    source 27
    target 1274
  ]
  edge [
    source 27
    target 1275
  ]
  edge [
    source 27
    target 392
  ]
  edge [
    source 27
    target 1276
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1277
  ]
  edge [
    source 28
    target 1278
  ]
  edge [
    source 28
    target 1279
  ]
  edge [
    source 28
    target 1280
  ]
  edge [
    source 28
    target 1281
  ]
  edge [
    source 28
    target 1282
  ]
  edge [
    source 28
    target 1283
  ]
  edge [
    source 28
    target 945
  ]
  edge [
    source 28
    target 1284
  ]
  edge [
    source 28
    target 1285
  ]
  edge [
    source 28
    target 1286
  ]
  edge [
    source 28
    target 1287
  ]
  edge [
    source 28
    target 761
  ]
  edge [
    source 28
    target 1288
  ]
  edge [
    source 28
    target 1289
  ]
  edge [
    source 28
    target 1290
  ]
  edge [
    source 28
    target 1291
  ]
  edge [
    source 28
    target 1292
  ]
  edge [
    source 28
    target 512
  ]
  edge [
    source 28
    target 751
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1293
  ]
  edge [
    source 30
    target 1294
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 887
  ]
  edge [
    source 31
    target 888
  ]
  edge [
    source 31
    target 889
  ]
  edge [
    source 31
    target 890
  ]
  edge [
    source 31
    target 891
  ]
  edge [
    source 31
    target 892
  ]
  edge [
    source 31
    target 688
  ]
  edge [
    source 31
    target 893
  ]
  edge [
    source 31
    target 199
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 894
  ]
  edge [
    source 31
    target 895
  ]
  edge [
    source 31
    target 896
  ]
  edge [
    source 31
    target 897
  ]
  edge [
    source 31
    target 216
  ]
  edge [
    source 31
    target 898
  ]
  edge [
    source 31
    target 356
  ]
  edge [
    source 31
    target 336
  ]
  edge [
    source 31
    target 361
  ]
  edge [
    source 31
    target 899
  ]
  edge [
    source 31
    target 900
  ]
  edge [
    source 31
    target 1295
  ]
  edge [
    source 31
    target 1296
  ]
  edge [
    source 31
    target 613
  ]
  edge [
    source 31
    target 1297
  ]
  edge [
    source 31
    target 1298
  ]
  edge [
    source 31
    target 1131
  ]
  edge [
    source 31
    target 355
  ]
  edge [
    source 31
    target 1299
  ]
  edge [
    source 31
    target 708
  ]
  edge [
    source 31
    target 1300
  ]
  edge [
    source 31
    target 1301
  ]
  edge [
    source 31
    target 1302
  ]
  edge [
    source 31
    target 1303
  ]
  edge [
    source 31
    target 1304
  ]
  edge [
    source 31
    target 1305
  ]
  edge [
    source 31
    target 1306
  ]
  edge [
    source 31
    target 1307
  ]
  edge [
    source 31
    target 1308
  ]
  edge [
    source 31
    target 1309
  ]
  edge [
    source 31
    target 735
  ]
  edge [
    source 31
    target 1310
  ]
  edge [
    source 31
    target 1311
  ]
  edge [
    source 31
    target 1312
  ]
  edge [
    source 31
    target 1313
  ]
  edge [
    source 31
    target 1314
  ]
  edge [
    source 31
    target 1315
  ]
  edge [
    source 31
    target 1121
  ]
  edge [
    source 31
    target 1316
  ]
  edge [
    source 31
    target 1317
  ]
  edge [
    source 31
    target 1318
  ]
  edge [
    source 31
    target 1319
  ]
  edge [
    source 31
    target 1320
  ]
  edge [
    source 31
    target 1321
  ]
  edge [
    source 31
    target 141
  ]
  edge [
    source 31
    target 1322
  ]
  edge [
    source 31
    target 879
  ]
  edge [
    source 31
    target 880
  ]
  edge [
    source 31
    target 360
  ]
  edge [
    source 31
    target 881
  ]
  edge [
    source 31
    target 706
  ]
  edge [
    source 31
    target 882
  ]
  edge [
    source 31
    target 222
  ]
  edge [
    source 31
    target 883
  ]
  edge [
    source 31
    target 884
  ]
  edge [
    source 31
    target 885
  ]
  edge [
    source 31
    target 886
  ]
  edge [
    source 31
    target 1323
  ]
  edge [
    source 31
    target 1324
  ]
  edge [
    source 31
    target 1325
  ]
  edge [
    source 31
    target 1326
  ]
  edge [
    source 31
    target 1327
  ]
  edge [
    source 31
    target 1328
  ]
  edge [
    source 31
    target 1329
  ]
  edge [
    source 31
    target 1330
  ]
  edge [
    source 31
    target 1331
  ]
  edge [
    source 31
    target 1332
  ]
  edge [
    source 31
    target 1333
  ]
  edge [
    source 31
    target 1334
  ]
  edge [
    source 31
    target 905
  ]
  edge [
    source 31
    target 683
  ]
  edge [
    source 31
    target 684
  ]
  edge [
    source 31
    target 685
  ]
  edge [
    source 31
    target 686
  ]
  edge [
    source 31
    target 345
  ]
  edge [
    source 31
    target 687
  ]
  edge [
    source 31
    target 689
  ]
  edge [
    source 31
    target 690
  ]
  edge [
    source 31
    target 691
  ]
  edge [
    source 31
    target 692
  ]
  edge [
    source 31
    target 693
  ]
  edge [
    source 31
    target 694
  ]
  edge [
    source 31
    target 695
  ]
  edge [
    source 31
    target 696
  ]
  edge [
    source 31
    target 697
  ]
  edge [
    source 31
    target 698
  ]
  edge [
    source 31
    target 699
  ]
  edge [
    source 31
    target 700
  ]
  edge [
    source 31
    target 701
  ]
  edge [
    source 31
    target 702
  ]
  edge [
    source 31
    target 703
  ]
  edge [
    source 31
    target 704
  ]
  edge [
    source 31
    target 351
  ]
  edge [
    source 31
    target 352
  ]
  edge [
    source 31
    target 705
  ]
  edge [
    source 31
    target 707
  ]
  edge [
    source 31
    target 369
  ]
  edge [
    source 31
    target 223
  ]
  edge [
    source 31
    target 709
  ]
  edge [
    source 31
    target 710
  ]
  edge [
    source 31
    target 711
  ]
  edge [
    source 31
    target 712
  ]
  edge [
    source 31
    target 1335
  ]
  edge [
    source 31
    target 1336
  ]
  edge [
    source 31
    target 1337
  ]
  edge [
    source 31
    target 1338
  ]
  edge [
    source 31
    target 1339
  ]
  edge [
    source 31
    target 1340
  ]
  edge [
    source 31
    target 1341
  ]
  edge [
    source 31
    target 1342
  ]
  edge [
    source 31
    target 273
  ]
  edge [
    source 31
    target 306
  ]
  edge [
    source 31
    target 1343
  ]
  edge [
    source 31
    target 1344
  ]
  edge [
    source 31
    target 1345
  ]
  edge [
    source 31
    target 1346
  ]
  edge [
    source 31
    target 1347
  ]
  edge [
    source 31
    target 1348
  ]
  edge [
    source 31
    target 719
  ]
  edge [
    source 31
    target 1349
  ]
  edge [
    source 31
    target 1350
  ]
  edge [
    source 31
    target 959
  ]
  edge [
    source 31
    target 242
  ]
  edge [
    source 31
    target 1351
  ]
  edge [
    source 31
    target 1352
  ]
  edge [
    source 31
    target 1353
  ]
  edge [
    source 31
    target 1354
  ]
  edge [
    source 31
    target 1355
  ]
  edge [
    source 31
    target 1356
  ]
  edge [
    source 31
    target 1357
  ]
  edge [
    source 31
    target 1358
  ]
  edge [
    source 31
    target 1359
  ]
  edge [
    source 31
    target 522
  ]
  edge [
    source 31
    target 1360
  ]
  edge [
    source 31
    target 1361
  ]
  edge [
    source 31
    target 1362
  ]
  edge [
    source 31
    target 1363
  ]
  edge [
    source 31
    target 1171
  ]
  edge [
    source 31
    target 524
  ]
  edge [
    source 31
    target 1010
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1364
  ]
  edge [
    source 33
    target 622
  ]
  edge [
    source 33
    target 1365
  ]
  edge [
    source 33
    target 1366
  ]
  edge [
    source 33
    target 1367
  ]
  edge [
    source 33
    target 1368
  ]
  edge [
    source 33
    target 1369
  ]
  edge [
    source 33
    target 1370
  ]
  edge [
    source 33
    target 1371
  ]
  edge [
    source 33
    target 1372
  ]
  edge [
    source 33
    target 1373
  ]
  edge [
    source 33
    target 1374
  ]
  edge [
    source 33
    target 1375
  ]
  edge [
    source 33
    target 1376
  ]
  edge [
    source 33
    target 1377
  ]
  edge [
    source 33
    target 1378
  ]
  edge [
    source 33
    target 1379
  ]
  edge [
    source 33
    target 271
  ]
  edge [
    source 33
    target 1380
  ]
  edge [
    source 33
    target 1381
  ]
  edge [
    source 33
    target 1382
  ]
  edge [
    source 33
    target 458
  ]
  edge [
    source 33
    target 1383
  ]
  edge [
    source 33
    target 1384
  ]
  edge [
    source 33
    target 1385
  ]
  edge [
    source 33
    target 1386
  ]
  edge [
    source 33
    target 1387
  ]
  edge [
    source 33
    target 48
  ]
  edge [
    source 33
    target 230
  ]
  edge [
    source 33
    target 1388
  ]
  edge [
    source 33
    target 1389
  ]
  edge [
    source 33
    target 1390
  ]
  edge [
    source 33
    target 1391
  ]
  edge [
    source 33
    target 269
  ]
  edge [
    source 33
    target 1392
  ]
  edge [
    source 33
    target 1393
  ]
  edge [
    source 33
    target 1394
  ]
  edge [
    source 33
    target 1395
  ]
  edge [
    source 33
    target 1396
  ]
  edge [
    source 33
    target 181
  ]
  edge [
    source 33
    target 1397
  ]
  edge [
    source 33
    target 485
  ]
  edge [
    source 33
    target 1019
  ]
  edge [
    source 33
    target 1398
  ]
  edge [
    source 33
    target 1399
  ]
  edge [
    source 33
    target 1400
  ]
  edge [
    source 33
    target 1401
  ]
  edge [
    source 33
    target 1402
  ]
  edge [
    source 33
    target 1403
  ]
  edge [
    source 33
    target 1404
  ]
  edge [
    source 33
    target 1405
  ]
  edge [
    source 33
    target 1406
  ]
  edge [
    source 33
    target 193
  ]
  edge [
    source 33
    target 1407
  ]
  edge [
    source 33
    target 1408
  ]
  edge [
    source 33
    target 1409
  ]
  edge [
    source 33
    target 1077
  ]
  edge [
    source 33
    target 1410
  ]
  edge [
    source 33
    target 1411
  ]
  edge [
    source 33
    target 1412
  ]
  edge [
    source 33
    target 1413
  ]
  edge [
    source 33
    target 1414
  ]
  edge [
    source 33
    target 1415
  ]
  edge [
    source 33
    target 1416
  ]
  edge [
    source 33
    target 1417
  ]
  edge [
    source 33
    target 1418
  ]
  edge [
    source 33
    target 1419
  ]
  edge [
    source 33
    target 1420
  ]
  edge [
    source 33
    target 1421
  ]
  edge [
    source 33
    target 1422
  ]
  edge [
    source 33
    target 1423
  ]
  edge [
    source 33
    target 1424
  ]
  edge [
    source 33
    target 1425
  ]
  edge [
    source 33
    target 1229
  ]
  edge [
    source 33
    target 1426
  ]
  edge [
    source 33
    target 1427
  ]
  edge [
    source 33
    target 1428
  ]
  edge [
    source 33
    target 1429
  ]
  edge [
    source 33
    target 1430
  ]
  edge [
    source 33
    target 1431
  ]
  edge [
    source 33
    target 1432
  ]
  edge [
    source 33
    target 247
  ]
  edge [
    source 33
    target 1433
  ]
  edge [
    source 33
    target 523
  ]
  edge [
    source 33
    target 1434
  ]
  edge [
    source 33
    target 1435
  ]
  edge [
    source 33
    target 1436
  ]
  edge [
    source 33
    target 1437
  ]
  edge [
    source 33
    target 1438
  ]
  edge [
    source 33
    target 1439
  ]
  edge [
    source 33
    target 1440
  ]
  edge [
    source 33
    target 1441
  ]
  edge [
    source 33
    target 1442
  ]
  edge [
    source 33
    target 1443
  ]
  edge [
    source 33
    target 1444
  ]
  edge [
    source 33
    target 1445
  ]
  edge [
    source 33
    target 1446
  ]
  edge [
    source 33
    target 1447
  ]
  edge [
    source 33
    target 1448
  ]
  edge [
    source 33
    target 1449
  ]
  edge [
    source 33
    target 1450
  ]
  edge [
    source 33
    target 1451
  ]
  edge [
    source 33
    target 1452
  ]
  edge [
    source 33
    target 1453
  ]
  edge [
    source 33
    target 1454
  ]
  edge [
    source 33
    target 1455
  ]
  edge [
    source 33
    target 1456
  ]
  edge [
    source 33
    target 1457
  ]
  edge [
    source 33
    target 1458
  ]
  edge [
    source 33
    target 1459
  ]
  edge [
    source 33
    target 1460
  ]
  edge [
    source 33
    target 1461
  ]
  edge [
    source 33
    target 1462
  ]
  edge [
    source 33
    target 1463
  ]
  edge [
    source 33
    target 1464
  ]
  edge [
    source 33
    target 1465
  ]
  edge [
    source 33
    target 1466
  ]
  edge [
    source 33
    target 1467
  ]
  edge [
    source 33
    target 1468
  ]
  edge [
    source 33
    target 1469
  ]
  edge [
    source 33
    target 1470
  ]
  edge [
    source 33
    target 1471
  ]
  edge [
    source 33
    target 1472
  ]
  edge [
    source 33
    target 1473
  ]
  edge [
    source 33
    target 1474
  ]
  edge [
    source 33
    target 1475
  ]
  edge [
    source 33
    target 1476
  ]
  edge [
    source 33
    target 1477
  ]
  edge [
    source 33
    target 1478
  ]
  edge [
    source 33
    target 1479
  ]
  edge [
    source 33
    target 1480
  ]
  edge [
    source 33
    target 1481
  ]
  edge [
    source 33
    target 1482
  ]
  edge [
    source 33
    target 1483
  ]
  edge [
    source 33
    target 1484
  ]
  edge [
    source 33
    target 412
  ]
  edge [
    source 33
    target 1485
  ]
  edge [
    source 33
    target 1486
  ]
  edge [
    source 33
    target 1487
  ]
  edge [
    source 33
    target 1488
  ]
  edge [
    source 33
    target 1489
  ]
  edge [
    source 33
    target 1490
  ]
  edge [
    source 33
    target 1491
  ]
  edge [
    source 33
    target 1492
  ]
  edge [
    source 33
    target 1493
  ]
  edge [
    source 33
    target 1494
  ]
  edge [
    source 33
    target 1495
  ]
  edge [
    source 33
    target 1496
  ]
  edge [
    source 33
    target 1497
  ]
  edge [
    source 33
    target 1498
  ]
  edge [
    source 33
    target 1499
  ]
  edge [
    source 33
    target 1500
  ]
  edge [
    source 33
    target 1501
  ]
  edge [
    source 33
    target 1502
  ]
  edge [
    source 33
    target 1503
  ]
  edge [
    source 33
    target 1504
  ]
  edge [
    source 33
    target 1505
  ]
  edge [
    source 33
    target 1506
  ]
  edge [
    source 33
    target 1507
  ]
  edge [
    source 33
    target 1508
  ]
  edge [
    source 33
    target 1509
  ]
  edge [
    source 33
    target 1510
  ]
  edge [
    source 33
    target 1511
  ]
  edge [
    source 33
    target 1512
  ]
  edge [
    source 33
    target 1513
  ]
  edge [
    source 33
    target 1514
  ]
  edge [
    source 33
    target 1515
  ]
  edge [
    source 33
    target 1516
  ]
  edge [
    source 33
    target 1517
  ]
  edge [
    source 33
    target 1518
  ]
  edge [
    source 33
    target 1519
  ]
  edge [
    source 33
    target 1520
  ]
  edge [
    source 33
    target 1521
  ]
  edge [
    source 33
    target 1522
  ]
  edge [
    source 33
    target 42
  ]
  edge [
    source 33
    target 1523
  ]
  edge [
    source 33
    target 1524
  ]
  edge [
    source 33
    target 54
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 51
  ]
  edge [
    source 34
    target 52
  ]
  edge [
    source 34
    target 1335
  ]
  edge [
    source 34
    target 1336
  ]
  edge [
    source 34
    target 1337
  ]
  edge [
    source 34
    target 1338
  ]
  edge [
    source 34
    target 1339
  ]
  edge [
    source 34
    target 1340
  ]
  edge [
    source 34
    target 1525
  ]
  edge [
    source 34
    target 1345
  ]
  edge [
    source 34
    target 199
  ]
  edge [
    source 34
    target 1526
  ]
  edge [
    source 34
    target 1527
  ]
  edge [
    source 34
    target 1528
  ]
  edge [
    source 34
    target 1529
  ]
  edge [
    source 34
    target 1530
  ]
  edge [
    source 34
    target 1531
  ]
  edge [
    source 34
    target 1532
  ]
  edge [
    source 34
    target 1533
  ]
  edge [
    source 34
    target 1534
  ]
  edge [
    source 34
    target 319
  ]
  edge [
    source 34
    target 1349
  ]
  edge [
    source 34
    target 1321
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1535
  ]
  edge [
    source 35
    target 1536
  ]
  edge [
    source 35
    target 1537
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 445
  ]
  edge [
    source 36
    target 446
  ]
  edge [
    source 36
    target 447
  ]
  edge [
    source 36
    target 448
  ]
  edge [
    source 36
    target 254
  ]
  edge [
    source 36
    target 449
  ]
  edge [
    source 36
    target 450
  ]
  edge [
    source 36
    target 451
  ]
  edge [
    source 36
    target 271
  ]
  edge [
    source 36
    target 452
  ]
  edge [
    source 36
    target 1538
  ]
  edge [
    source 36
    target 230
  ]
  edge [
    source 36
    target 443
  ]
  edge [
    source 36
    target 1539
  ]
  edge [
    source 36
    target 1540
  ]
  edge [
    source 36
    target 1541
  ]
  edge [
    source 36
    target 1542
  ]
  edge [
    source 36
    target 1543
  ]
  edge [
    source 36
    target 1544
  ]
  edge [
    source 36
    target 1065
  ]
  edge [
    source 36
    target 1545
  ]
  edge [
    source 36
    target 1546
  ]
  edge [
    source 36
    target 989
  ]
  edge [
    source 36
    target 270
  ]
  edge [
    source 36
    target 1393
  ]
  edge [
    source 36
    target 1547
  ]
  edge [
    source 36
    target 1548
  ]
  edge [
    source 36
    target 622
  ]
  edge [
    source 36
    target 1549
  ]
  edge [
    source 36
    target 1550
  ]
  edge [
    source 36
    target 1551
  ]
  edge [
    source 36
    target 247
  ]
  edge [
    source 36
    target 1392
  ]
  edge [
    source 36
    target 968
  ]
  edge [
    source 36
    target 1552
  ]
  edge [
    source 36
    target 970
  ]
  edge [
    source 36
    target 414
  ]
  edge [
    source 36
    target 1553
  ]
  edge [
    source 36
    target 1554
  ]
  edge [
    source 36
    target 1555
  ]
  edge [
    source 36
    target 303
  ]
  edge [
    source 36
    target 1556
  ]
  edge [
    source 36
    target 1088
  ]
  edge [
    source 36
    target 1208
  ]
  edge [
    source 36
    target 1557
  ]
  edge [
    source 36
    target 1558
  ]
  edge [
    source 36
    target 1559
  ]
  edge [
    source 36
    target 1384
  ]
  edge [
    source 36
    target 1560
  ]
  edge [
    source 36
    target 1561
  ]
  edge [
    source 36
    target 1562
  ]
  edge [
    source 36
    target 1563
  ]
  edge [
    source 36
    target 1564
  ]
  edge [
    source 36
    target 1565
  ]
  edge [
    source 36
    target 1566
  ]
  edge [
    source 36
    target 1567
  ]
  edge [
    source 36
    target 1568
  ]
  edge [
    source 36
    target 1376
  ]
  edge [
    source 36
    target 1377
  ]
  edge [
    source 36
    target 523
  ]
  edge [
    source 36
    target 1380
  ]
  edge [
    source 36
    target 1569
  ]
  edge [
    source 36
    target 1382
  ]
  edge [
    source 36
    target 1385
  ]
  edge [
    source 36
    target 1570
  ]
  edge [
    source 36
    target 1571
  ]
  edge [
    source 36
    target 1387
  ]
  edge [
    source 36
    target 48
  ]
  edge [
    source 36
    target 1388
  ]
  edge [
    source 36
    target 1572
  ]
  edge [
    source 36
    target 1390
  ]
  edge [
    source 36
    target 1391
  ]
  edge [
    source 36
    target 1573
  ]
  edge [
    source 36
    target 1574
  ]
  edge [
    source 36
    target 1394
  ]
  edge [
    source 36
    target 1395
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1575
  ]
  edge [
    source 37
    target 1576
  ]
  edge [
    source 37
    target 1577
  ]
  edge [
    source 37
    target 1578
  ]
  edge [
    source 37
    target 1579
  ]
  edge [
    source 37
    target 1580
  ]
  edge [
    source 37
    target 1581
  ]
  edge [
    source 37
    target 1582
  ]
  edge [
    source 37
    target 1583
  ]
  edge [
    source 37
    target 1584
  ]
  edge [
    source 37
    target 1585
  ]
  edge [
    source 37
    target 1586
  ]
  edge [
    source 37
    target 1587
  ]
  edge [
    source 37
    target 671
  ]
  edge [
    source 37
    target 1588
  ]
  edge [
    source 37
    target 677
  ]
  edge [
    source 37
    target 1589
  ]
  edge [
    source 37
    target 1590
  ]
  edge [
    source 37
    target 1591
  ]
  edge [
    source 37
    target 1592
  ]
  edge [
    source 37
    target 1593
  ]
  edge [
    source 37
    target 1594
  ]
  edge [
    source 37
    target 1595
  ]
  edge [
    source 37
    target 1596
  ]
  edge [
    source 37
    target 1597
  ]
  edge [
    source 37
    target 1598
  ]
  edge [
    source 37
    target 1599
  ]
  edge [
    source 37
    target 1600
  ]
  edge [
    source 37
    target 1601
  ]
  edge [
    source 37
    target 1602
  ]
  edge [
    source 37
    target 1603
  ]
  edge [
    source 37
    target 1604
  ]
  edge [
    source 37
    target 1605
  ]
  edge [
    source 37
    target 1606
  ]
  edge [
    source 37
    target 1607
  ]
  edge [
    source 37
    target 1608
  ]
  edge [
    source 37
    target 1609
  ]
  edge [
    source 37
    target 1610
  ]
  edge [
    source 37
    target 1611
  ]
  edge [
    source 37
    target 1612
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1613
  ]
  edge [
    source 40
    target 622
  ]
  edge [
    source 40
    target 867
  ]
  edge [
    source 40
    target 1145
  ]
  edge [
    source 40
    target 1614
  ]
  edge [
    source 40
    target 1223
  ]
  edge [
    source 40
    target 772
  ]
  edge [
    source 40
    target 1224
  ]
  edge [
    source 40
    target 775
  ]
  edge [
    source 40
    target 1225
  ]
  edge [
    source 40
    target 935
  ]
  edge [
    source 40
    target 1226
  ]
  edge [
    source 40
    target 1227
  ]
  edge [
    source 40
    target 1228
  ]
  edge [
    source 40
    target 1229
  ]
  edge [
    source 40
    target 1230
  ]
  edge [
    source 40
    target 1231
  ]
  edge [
    source 40
    target 1232
  ]
  edge [
    source 40
    target 1233
  ]
  edge [
    source 40
    target 1234
  ]
  edge [
    source 40
    target 1235
  ]
  edge [
    source 40
    target 1236
  ]
  edge [
    source 40
    target 1237
  ]
  edge [
    source 40
    target 130
  ]
  edge [
    source 40
    target 1238
  ]
  edge [
    source 40
    target 1239
  ]
  edge [
    source 40
    target 657
  ]
  edge [
    source 40
    target 1240
  ]
  edge [
    source 40
    target 1241
  ]
  edge [
    source 40
    target 1242
  ]
  edge [
    source 40
    target 1376
  ]
  edge [
    source 40
    target 1377
  ]
  edge [
    source 40
    target 1378
  ]
  edge [
    source 40
    target 1379
  ]
  edge [
    source 40
    target 271
  ]
  edge [
    source 40
    target 1380
  ]
  edge [
    source 40
    target 1381
  ]
  edge [
    source 40
    target 1382
  ]
  edge [
    source 40
    target 458
  ]
  edge [
    source 40
    target 1383
  ]
  edge [
    source 40
    target 1384
  ]
  edge [
    source 40
    target 1385
  ]
  edge [
    source 40
    target 1386
  ]
  edge [
    source 40
    target 1387
  ]
  edge [
    source 40
    target 48
  ]
  edge [
    source 40
    target 230
  ]
  edge [
    source 40
    target 1388
  ]
  edge [
    source 40
    target 1389
  ]
  edge [
    source 40
    target 1390
  ]
  edge [
    source 40
    target 1391
  ]
  edge [
    source 40
    target 269
  ]
  edge [
    source 40
    target 1392
  ]
  edge [
    source 40
    target 1393
  ]
  edge [
    source 40
    target 1394
  ]
  edge [
    source 40
    target 1395
  ]
  edge [
    source 40
    target 225
  ]
  edge [
    source 40
    target 796
  ]
  edge [
    source 40
    target 797
  ]
  edge [
    source 40
    target 1615
  ]
  edge [
    source 40
    target 1616
  ]
  edge [
    source 40
    target 1617
  ]
  edge [
    source 40
    target 494
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1028
  ]
  edge [
    source 41
    target 1618
  ]
  edge [
    source 41
    target 1619
  ]
  edge [
    source 41
    target 471
  ]
  edge [
    source 41
    target 1620
  ]
  edge [
    source 41
    target 1621
  ]
  edge [
    source 41
    target 1622
  ]
  edge [
    source 41
    target 474
  ]
  edge [
    source 41
    target 1623
  ]
  edge [
    source 41
    target 1624
  ]
  edge [
    source 41
    target 1625
  ]
  edge [
    source 41
    target 1626
  ]
  edge [
    source 41
    target 1627
  ]
  edge [
    source 41
    target 1628
  ]
  edge [
    source 41
    target 1629
  ]
  edge [
    source 41
    target 583
  ]
  edge [
    source 41
    target 1630
  ]
  edge [
    source 41
    target 1631
  ]
  edge [
    source 41
    target 1632
  ]
  edge [
    source 41
    target 1633
  ]
  edge [
    source 41
    target 1634
  ]
  edge [
    source 41
    target 1635
  ]
  edge [
    source 41
    target 1636
  ]
  edge [
    source 41
    target 1637
  ]
  edge [
    source 41
    target 1638
  ]
  edge [
    source 41
    target 1639
  ]
  edge [
    source 41
    target 1640
  ]
  edge [
    source 41
    target 1641
  ]
  edge [
    source 41
    target 373
  ]
  edge [
    source 41
    target 1642
  ]
  edge [
    source 41
    target 473
  ]
  edge [
    source 41
    target 1643
  ]
  edge [
    source 41
    target 1644
  ]
  edge [
    source 41
    target 1645
  ]
  edge [
    source 41
    target 1646
  ]
  edge [
    source 41
    target 1348
  ]
  edge [
    source 41
    target 1647
  ]
  edge [
    source 41
    target 1648
  ]
  edge [
    source 41
    target 1649
  ]
  edge [
    source 41
    target 1650
  ]
  edge [
    source 41
    target 1651
  ]
  edge [
    source 41
    target 1652
  ]
  edge [
    source 41
    target 1653
  ]
  edge [
    source 41
    target 1654
  ]
  edge [
    source 41
    target 236
  ]
  edge [
    source 41
    target 1655
  ]
  edge [
    source 41
    target 1656
  ]
  edge [
    source 41
    target 1657
  ]
  edge [
    source 41
    target 1658
  ]
  edge [
    source 41
    target 1659
  ]
  edge [
    source 41
    target 1660
  ]
  edge [
    source 41
    target 1661
  ]
  edge [
    source 41
    target 1662
  ]
  edge [
    source 41
    target 1663
  ]
  edge [
    source 41
    target 1664
  ]
  edge [
    source 41
    target 1665
  ]
  edge [
    source 41
    target 1666
  ]
  edge [
    source 41
    target 1667
  ]
  edge [
    source 41
    target 1668
  ]
  edge [
    source 41
    target 1669
  ]
  edge [
    source 41
    target 1670
  ]
  edge [
    source 41
    target 1671
  ]
  edge [
    source 41
    target 1672
  ]
  edge [
    source 41
    target 1673
  ]
  edge [
    source 41
    target 1674
  ]
  edge [
    source 41
    target 513
  ]
  edge [
    source 41
    target 258
  ]
  edge [
    source 41
    target 1675
  ]
  edge [
    source 41
    target 835
  ]
  edge [
    source 41
    target 1676
  ]
  edge [
    source 41
    target 1677
  ]
  edge [
    source 41
    target 1678
  ]
  edge [
    source 41
    target 1679
  ]
  edge [
    source 41
    target 1680
  ]
  edge [
    source 41
    target 522
  ]
  edge [
    source 41
    target 97
  ]
  edge [
    source 41
    target 443
  ]
  edge [
    source 41
    target 1681
  ]
  edge [
    source 41
    target 1682
  ]
  edge [
    source 41
    target 1683
  ]
  edge [
    source 41
    target 1684
  ]
  edge [
    source 41
    target 1685
  ]
  edge [
    source 41
    target 867
  ]
  edge [
    source 41
    target 1686
  ]
  edge [
    source 41
    target 1687
  ]
  edge [
    source 41
    target 1688
  ]
  edge [
    source 41
    target 1689
  ]
  edge [
    source 41
    target 1690
  ]
  edge [
    source 41
    target 1691
  ]
  edge [
    source 41
    target 1692
  ]
  edge [
    source 41
    target 1693
  ]
  edge [
    source 41
    target 1694
  ]
  edge [
    source 41
    target 1695
  ]
  edge [
    source 41
    target 1696
  ]
  edge [
    source 41
    target 1697
  ]
  edge [
    source 41
    target 1698
  ]
  edge [
    source 41
    target 1699
  ]
  edge [
    source 41
    target 1700
  ]
  edge [
    source 41
    target 1701
  ]
  edge [
    source 41
    target 1702
  ]
  edge [
    source 41
    target 1703
  ]
  edge [
    source 41
    target 1704
  ]
  edge [
    source 41
    target 1705
  ]
  edge [
    source 41
    target 1706
  ]
  edge [
    source 41
    target 1707
  ]
  edge [
    source 41
    target 1708
  ]
  edge [
    source 41
    target 1709
  ]
  edge [
    source 41
    target 1710
  ]
  edge [
    source 41
    target 1711
  ]
  edge [
    source 41
    target 1712
  ]
  edge [
    source 41
    target 1713
  ]
  edge [
    source 41
    target 1714
  ]
  edge [
    source 41
    target 1715
  ]
  edge [
    source 41
    target 1716
  ]
  edge [
    source 41
    target 1717
  ]
  edge [
    source 41
    target 810
  ]
  edge [
    source 41
    target 1718
  ]
  edge [
    source 41
    target 1719
  ]
  edge [
    source 41
    target 1720
  ]
  edge [
    source 41
    target 1721
  ]
  edge [
    source 41
    target 1722
  ]
  edge [
    source 41
    target 1030
  ]
  edge [
    source 41
    target 1723
  ]
  edge [
    source 41
    target 1724
  ]
  edge [
    source 41
    target 1725
  ]
  edge [
    source 41
    target 1726
  ]
  edge [
    source 41
    target 1727
  ]
  edge [
    source 41
    target 1728
  ]
  edge [
    source 41
    target 1729
  ]
  edge [
    source 41
    target 1730
  ]
  edge [
    source 41
    target 1731
  ]
  edge [
    source 41
    target 1732
  ]
  edge [
    source 41
    target 1733
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1734
  ]
  edge [
    source 42
    target 1735
  ]
  edge [
    source 42
    target 622
  ]
  edge [
    source 42
    target 1736
  ]
  edge [
    source 42
    target 1737
  ]
  edge [
    source 42
    target 434
  ]
  edge [
    source 42
    target 1738
  ]
  edge [
    source 42
    target 1739
  ]
  edge [
    source 42
    target 1740
  ]
  edge [
    source 42
    target 1741
  ]
  edge [
    source 42
    target 1742
  ]
  edge [
    source 42
    target 160
  ]
  edge [
    source 42
    target 1743
  ]
  edge [
    source 42
    target 1744
  ]
  edge [
    source 42
    target 1745
  ]
  edge [
    source 42
    target 1547
  ]
  edge [
    source 42
    target 1746
  ]
  edge [
    source 42
    target 1747
  ]
  edge [
    source 42
    target 1748
  ]
  edge [
    source 42
    target 1749
  ]
  edge [
    source 42
    target 1750
  ]
  edge [
    source 42
    target 1673
  ]
  edge [
    source 42
    target 1751
  ]
  edge [
    source 42
    target 1752
  ]
  edge [
    source 42
    target 1753
  ]
  edge [
    source 42
    target 1754
  ]
  edge [
    source 42
    target 1755
  ]
  edge [
    source 42
    target 258
  ]
  edge [
    source 42
    target 1756
  ]
  edge [
    source 42
    target 104
  ]
  edge [
    source 42
    target 1757
  ]
  edge [
    source 42
    target 1758
  ]
  edge [
    source 42
    target 1759
  ]
  edge [
    source 42
    target 1760
  ]
  edge [
    source 42
    target 156
  ]
  edge [
    source 42
    target 1761
  ]
  edge [
    source 42
    target 1762
  ]
  edge [
    source 42
    target 522
  ]
  edge [
    source 42
    target 443
  ]
  edge [
    source 42
    target 1763
  ]
  edge [
    source 42
    target 466
  ]
  edge [
    source 42
    target 1764
  ]
  edge [
    source 42
    target 1061
  ]
  edge [
    source 42
    target 1765
  ]
  edge [
    source 42
    target 450
  ]
  edge [
    source 42
    target 1766
  ]
  edge [
    source 42
    target 1010
  ]
  edge [
    source 42
    target 1767
  ]
  edge [
    source 42
    target 1768
  ]
  edge [
    source 42
    target 1769
  ]
  edge [
    source 42
    target 1770
  ]
  edge [
    source 42
    target 1771
  ]
  edge [
    source 42
    target 1772
  ]
  edge [
    source 42
    target 1773
  ]
  edge [
    source 42
    target 272
  ]
  edge [
    source 42
    target 1774
  ]
  edge [
    source 42
    target 1775
  ]
  edge [
    source 42
    target 1364
  ]
  edge [
    source 42
    target 1776
  ]
  edge [
    source 42
    target 1777
  ]
  edge [
    source 42
    target 1778
  ]
  edge [
    source 42
    target 236
  ]
  edge [
    source 42
    target 1779
  ]
  edge [
    source 42
    target 1780
  ]
  edge [
    source 42
    target 1781
  ]
  edge [
    source 42
    target 1782
  ]
  edge [
    source 42
    target 1783
  ]
  edge [
    source 42
    target 835
  ]
  edge [
    source 42
    target 1784
  ]
  edge [
    source 42
    target 826
  ]
  edge [
    source 42
    target 1376
  ]
  edge [
    source 42
    target 1377
  ]
  edge [
    source 42
    target 1378
  ]
  edge [
    source 42
    target 1379
  ]
  edge [
    source 42
    target 271
  ]
  edge [
    source 42
    target 1380
  ]
  edge [
    source 42
    target 1381
  ]
  edge [
    source 42
    target 1382
  ]
  edge [
    source 42
    target 458
  ]
  edge [
    source 42
    target 1383
  ]
  edge [
    source 42
    target 1384
  ]
  edge [
    source 42
    target 1385
  ]
  edge [
    source 42
    target 1386
  ]
  edge [
    source 42
    target 1387
  ]
  edge [
    source 42
    target 48
  ]
  edge [
    source 42
    target 230
  ]
  edge [
    source 42
    target 1388
  ]
  edge [
    source 42
    target 1389
  ]
  edge [
    source 42
    target 1390
  ]
  edge [
    source 42
    target 1391
  ]
  edge [
    source 42
    target 269
  ]
  edge [
    source 42
    target 1392
  ]
  edge [
    source 42
    target 1393
  ]
  edge [
    source 42
    target 1394
  ]
  edge [
    source 42
    target 1395
  ]
  edge [
    source 42
    target 1785
  ]
  edge [
    source 42
    target 1786
  ]
  edge [
    source 42
    target 1787
  ]
  edge [
    source 42
    target 1788
  ]
  edge [
    source 42
    target 1789
  ]
  edge [
    source 42
    target 1790
  ]
  edge [
    source 42
    target 1791
  ]
  edge [
    source 42
    target 1792
  ]
  edge [
    source 42
    target 1793
  ]
  edge [
    source 42
    target 280
  ]
  edge [
    source 42
    target 1794
  ]
  edge [
    source 42
    target 1795
  ]
  edge [
    source 42
    target 1796
  ]
  edge [
    source 42
    target 1797
  ]
  edge [
    source 42
    target 1798
  ]
  edge [
    source 42
    target 1799
  ]
  edge [
    source 42
    target 1800
  ]
  edge [
    source 42
    target 1801
  ]
  edge [
    source 42
    target 1802
  ]
  edge [
    source 42
    target 1803
  ]
  edge [
    source 42
    target 1804
  ]
  edge [
    source 42
    target 1805
  ]
  edge [
    source 42
    target 1806
  ]
  edge [
    source 42
    target 789
  ]
  edge [
    source 42
    target 1807
  ]
  edge [
    source 42
    target 1808
  ]
  edge [
    source 42
    target 326
  ]
  edge [
    source 42
    target 1809
  ]
  edge [
    source 42
    target 1810
  ]
  edge [
    source 42
    target 336
  ]
  edge [
    source 42
    target 1811
  ]
  edge [
    source 42
    target 1812
  ]
  edge [
    source 42
    target 1813
  ]
  edge [
    source 42
    target 82
  ]
  edge [
    source 42
    target 1206
  ]
  edge [
    source 42
    target 934
  ]
  edge [
    source 42
    target 1814
  ]
  edge [
    source 42
    target 1815
  ]
  edge [
    source 42
    target 452
  ]
  edge [
    source 42
    target 1816
  ]
  edge [
    source 42
    target 1817
  ]
  edge [
    source 42
    target 1050
  ]
  edge [
    source 42
    target 1818
  ]
  edge [
    source 42
    target 1277
  ]
  edge [
    source 42
    target 1819
  ]
  edge [
    source 42
    target 1820
  ]
  edge [
    source 42
    target 1821
  ]
  edge [
    source 42
    target 1284
  ]
  edge [
    source 42
    target 1822
  ]
  edge [
    source 42
    target 1216
  ]
  edge [
    source 42
    target 1823
  ]
  edge [
    source 42
    target 1060
  ]
  edge [
    source 42
    target 1824
  ]
  edge [
    source 42
    target 557
  ]
  edge [
    source 42
    target 1825
  ]
  edge [
    source 42
    target 1826
  ]
  edge [
    source 42
    target 1827
  ]
  edge [
    source 42
    target 1828
  ]
  edge [
    source 42
    target 1829
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1535
  ]
  edge [
    source 43
    target 1830
  ]
  edge [
    source 43
    target 1831
  ]
  edge [
    source 43
    target 1832
  ]
  edge [
    source 43
    target 1833
  ]
  edge [
    source 43
    target 1834
  ]
  edge [
    source 43
    target 1835
  ]
  edge [
    source 43
    target 1836
  ]
  edge [
    source 43
    target 663
  ]
  edge [
    source 43
    target 1537
  ]
  edge [
    source 44
    target 1837
  ]
  edge [
    source 44
    target 1838
  ]
  edge [
    source 44
    target 1839
  ]
  edge [
    source 44
    target 934
  ]
  edge [
    source 44
    target 1840
  ]
  edge [
    source 44
    target 1216
  ]
  edge [
    source 44
    target 1841
  ]
  edge [
    source 44
    target 1842
  ]
  edge [
    source 44
    target 1843
  ]
  edge [
    source 44
    target 1844
  ]
  edge [
    source 44
    target 1845
  ]
  edge [
    source 44
    target 1846
  ]
  edge [
    source 44
    target 1847
  ]
  edge [
    source 44
    target 1848
  ]
  edge [
    source 44
    target 1849
  ]
  edge [
    source 44
    target 1850
  ]
  edge [
    source 44
    target 1851
  ]
  edge [
    source 44
    target 1185
  ]
  edge [
    source 44
    target 1852
  ]
  edge [
    source 44
    target 1853
  ]
  edge [
    source 44
    target 622
  ]
  edge [
    source 44
    target 1546
  ]
  edge [
    source 44
    target 437
  ]
  edge [
    source 44
    target 430
  ]
  edge [
    source 44
    target 815
  ]
  edge [
    source 44
    target 1854
  ]
  edge [
    source 44
    target 1855
  ]
  edge [
    source 44
    target 1596
  ]
  edge [
    source 44
    target 1856
  ]
  edge [
    source 44
    target 516
  ]
  edge [
    source 44
    target 1144
  ]
  edge [
    source 44
    target 523
  ]
  edge [
    source 44
    target 1857
  ]
  edge [
    source 44
    target 1858
  ]
  edge [
    source 44
    target 1010
  ]
  edge [
    source 44
    target 52
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 1570
  ]
  edge [
    source 48
    target 1859
  ]
  edge [
    source 48
    target 1571
  ]
  edge [
    source 48
    target 448
  ]
  edge [
    source 48
    target 1860
  ]
  edge [
    source 48
    target 934
  ]
  edge [
    source 48
    target 634
  ]
  edge [
    source 48
    target 625
  ]
  edge [
    source 48
    target 650
  ]
  edge [
    source 48
    target 1542
  ]
  edge [
    source 48
    target 1861
  ]
  edge [
    source 48
    target 1364
  ]
  edge [
    source 48
    target 945
  ]
  edge [
    source 48
    target 1862
  ]
  edge [
    source 48
    target 1863
  ]
  edge [
    source 48
    target 1864
  ]
  edge [
    source 48
    target 1865
  ]
  edge [
    source 48
    target 835
  ]
  edge [
    source 48
    target 1866
  ]
  edge [
    source 48
    target 1867
  ]
  edge [
    source 48
    target 603
  ]
  edge [
    source 48
    target 826
  ]
  edge [
    source 48
    target 1868
  ]
  edge [
    source 48
    target 1869
  ]
  edge [
    source 48
    target 568
  ]
  edge [
    source 48
    target 1870
  ]
  edge [
    source 48
    target 1871
  ]
  edge [
    source 48
    target 1872
  ]
  edge [
    source 48
    target 1873
  ]
  edge [
    source 48
    target 1874
  ]
  edge [
    source 48
    target 818
  ]
  edge [
    source 48
    target 1875
  ]
  edge [
    source 48
    target 522
  ]
  edge [
    source 48
    target 1568
  ]
  edge [
    source 48
    target 1376
  ]
  edge [
    source 48
    target 1377
  ]
  edge [
    source 48
    target 523
  ]
  edge [
    source 48
    target 271
  ]
  edge [
    source 48
    target 1380
  ]
  edge [
    source 48
    target 1569
  ]
  edge [
    source 48
    target 1382
  ]
  edge [
    source 48
    target 1385
  ]
  edge [
    source 48
    target 1387
  ]
  edge [
    source 48
    target 1388
  ]
  edge [
    source 48
    target 1572
  ]
  edge [
    source 48
    target 1390
  ]
  edge [
    source 48
    target 1391
  ]
  edge [
    source 48
    target 1573
  ]
  edge [
    source 48
    target 1574
  ]
  edge [
    source 48
    target 1393
  ]
  edge [
    source 48
    target 1394
  ]
  edge [
    source 48
    target 1395
  ]
  edge [
    source 48
    target 622
  ]
  edge [
    source 48
    target 1546
  ]
  edge [
    source 48
    target 437
  ]
  edge [
    source 48
    target 430
  ]
  edge [
    source 48
    target 1876
  ]
  edge [
    source 48
    target 528
  ]
  edge [
    source 48
    target 1877
  ]
  edge [
    source 48
    target 1878
  ]
  edge [
    source 48
    target 1879
  ]
  edge [
    source 48
    target 1880
  ]
  edge [
    source 48
    target 1881
  ]
  edge [
    source 48
    target 414
  ]
  edge [
    source 48
    target 1882
  ]
  edge [
    source 48
    target 1883
  ]
  edge [
    source 48
    target 237
  ]
  edge [
    source 48
    target 1884
  ]
  edge [
    source 48
    target 1885
  ]
  edge [
    source 48
    target 1886
  ]
  edge [
    source 48
    target 751
  ]
  edge [
    source 48
    target 272
  ]
  edge [
    source 48
    target 1887
  ]
  edge [
    source 48
    target 1888
  ]
  edge [
    source 48
    target 1771
  ]
  edge [
    source 48
    target 1889
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 1890
  ]
  edge [
    source 50
    target 1891
  ]
  edge [
    source 50
    target 1892
  ]
  edge [
    source 50
    target 1893
  ]
  edge [
    source 50
    target 1894
  ]
  edge [
    source 50
    target 1895
  ]
  edge [
    source 50
    target 199
  ]
  edge [
    source 50
    target 1896
  ]
  edge [
    source 50
    target 1897
  ]
  edge [
    source 50
    target 1898
  ]
  edge [
    source 51
    target 1899
  ]
  edge [
    source 51
    target 1900
  ]
  edge [
    source 51
    target 1901
  ]
  edge [
    source 51
    target 1902
  ]
  edge [
    source 51
    target 1903
  ]
  edge [
    source 51
    target 1904
  ]
  edge [
    source 51
    target 1905
  ]
  edge [
    source 51
    target 1906
  ]
  edge [
    source 51
    target 1907
  ]
  edge [
    source 51
    target 1908
  ]
  edge [
    source 51
    target 1830
  ]
  edge [
    source 51
    target 1909
  ]
  edge [
    source 51
    target 1910
  ]
  edge [
    source 51
    target 1911
  ]
  edge [
    source 51
    target 1912
  ]
  edge [
    source 51
    target 1913
  ]
  edge [
    source 51
    target 1914
  ]
  edge [
    source 51
    target 1915
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 1916
  ]
  edge [
    source 52
    target 1917
  ]
  edge [
    source 52
    target 1918
  ]
  edge [
    source 52
    target 1919
  ]
  edge [
    source 52
    target 1920
  ]
  edge [
    source 52
    target 1921
  ]
  edge [
    source 52
    target 1922
  ]
  edge [
    source 52
    target 1923
  ]
  edge [
    source 52
    target 1924
  ]
  edge [
    source 52
    target 1925
  ]
  edge [
    source 52
    target 1926
  ]
  edge [
    source 52
    target 1927
  ]
  edge [
    source 52
    target 1928
  ]
  edge [
    source 52
    target 1929
  ]
  edge [
    source 52
    target 1930
  ]
  edge [
    source 52
    target 1931
  ]
  edge [
    source 52
    target 1932
  ]
  edge [
    source 52
    target 1933
  ]
  edge [
    source 52
    target 1934
  ]
  edge [
    source 52
    target 1935
  ]
  edge [
    source 52
    target 1936
  ]
  edge [
    source 52
    target 1937
  ]
  edge [
    source 52
    target 1938
  ]
  edge [
    source 52
    target 1939
  ]
  edge [
    source 52
    target 1940
  ]
  edge [
    source 52
    target 1941
  ]
  edge [
    source 52
    target 1942
  ]
  edge [
    source 52
    target 1943
  ]
  edge [
    source 52
    target 1944
  ]
  edge [
    source 53
    target 1121
  ]
  edge [
    source 53
    target 1120
  ]
  edge [
    source 53
    target 1945
  ]
  edge [
    source 53
    target 1946
  ]
  edge [
    source 53
    target 199
  ]
  edge [
    source 53
    target 314
  ]
  edge [
    source 53
    target 1947
  ]
  edge [
    source 53
    target 1948
  ]
  edge [
    source 53
    target 894
  ]
  edge [
    source 53
    target 1949
  ]
  edge [
    source 53
    target 1347
  ]
  edge [
    source 53
    target 708
  ]
  edge [
    source 53
    target 372
  ]
  edge [
    source 53
    target 1300
  ]
  edge [
    source 53
    target 1301
  ]
  edge [
    source 53
    target 1302
  ]
  edge [
    source 53
    target 1303
  ]
  edge [
    source 53
    target 1304
  ]
  edge [
    source 53
    target 1305
  ]
  edge [
    source 53
    target 1306
  ]
  edge [
    source 53
    target 1307
  ]
  edge [
    source 53
    target 1308
  ]
  edge [
    source 53
    target 1309
  ]
  edge [
    source 53
    target 735
  ]
  edge [
    source 53
    target 1310
  ]
  edge [
    source 53
    target 1311
  ]
  edge [
    source 53
    target 1312
  ]
  edge [
    source 53
    target 437
  ]
  edge [
    source 53
    target 1950
  ]
  edge [
    source 53
    target 222
  ]
  edge [
    source 53
    target 1951
  ]
  edge [
    source 53
    target 682
  ]
  edge [
    source 54
    target 1952
  ]
  edge [
    source 54
    target 1953
  ]
  edge [
    source 54
    target 1954
  ]
  edge [
    source 54
    target 1955
  ]
  edge [
    source 54
    target 1349
  ]
  edge [
    source 54
    target 199
  ]
  edge [
    source 54
    target 321
  ]
  edge [
    source 54
    target 1044
  ]
  edge [
    source 54
    target 481
  ]
  edge [
    source 54
    target 1956
  ]
  edge [
    source 54
    target 1957
  ]
  edge [
    source 54
    target 319
  ]
  edge [
    source 54
    target 1958
  ]
  edge [
    source 54
    target 1959
  ]
  edge [
    source 54
    target 1960
  ]
  edge [
    source 54
    target 1961
  ]
  edge [
    source 54
    target 1962
  ]
  edge [
    source 54
    target 1963
  ]
  edge [
    source 54
    target 1964
  ]
  edge [
    source 54
    target 1965
  ]
  edge [
    source 54
    target 339
  ]
  edge [
    source 54
    target 1300
  ]
  edge [
    source 54
    target 1301
  ]
  edge [
    source 54
    target 1302
  ]
  edge [
    source 54
    target 1303
  ]
  edge [
    source 54
    target 1304
  ]
  edge [
    source 54
    target 1305
  ]
  edge [
    source 54
    target 1306
  ]
  edge [
    source 54
    target 1307
  ]
  edge [
    source 54
    target 1308
  ]
  edge [
    source 54
    target 1309
  ]
  edge [
    source 54
    target 735
  ]
  edge [
    source 54
    target 1310
  ]
  edge [
    source 54
    target 1311
  ]
  edge [
    source 54
    target 1312
  ]
  edge [
    source 54
    target 689
  ]
  edge [
    source 54
    target 1966
  ]
  edge [
    source 54
    target 1967
  ]
  edge [
    source 54
    target 282
  ]
  edge [
    source 54
    target 719
  ]
  edge [
    source 54
    target 1968
  ]
  edge [
    source 54
    target 1969
  ]
  edge [
    source 54
    target 858
  ]
  edge [
    source 54
    target 1970
  ]
  edge [
    source 54
    target 1971
  ]
  edge [
    source 54
    target 1972
  ]
  edge [
    source 54
    target 1973
  ]
  edge [
    source 54
    target 1070
  ]
  edge [
    source 54
    target 1071
  ]
  edge [
    source 54
    target 1072
  ]
  edge [
    source 54
    target 1073
  ]
  edge [
    source 54
    target 1074
  ]
  edge [
    source 54
    target 280
  ]
  edge [
    source 54
    target 588
  ]
  edge [
    source 54
    target 1075
  ]
  edge [
    source 54
    target 1001
  ]
  edge [
    source 54
    target 1003
  ]
  edge [
    source 54
    target 1076
  ]
  edge [
    source 54
    target 1077
  ]
  edge [
    source 54
    target 1078
  ]
  edge [
    source 54
    target 1079
  ]
  edge [
    source 54
    target 1080
  ]
  edge [
    source 54
    target 1048
  ]
  edge [
    source 54
    target 1081
  ]
  edge [
    source 54
    target 1082
  ]
  edge [
    source 54
    target 1083
  ]
  edge [
    source 54
    target 1084
  ]
  edge [
    source 54
    target 1085
  ]
  edge [
    source 54
    target 1061
  ]
  edge [
    source 54
    target 1086
  ]
  edge [
    source 54
    target 1087
  ]
]
