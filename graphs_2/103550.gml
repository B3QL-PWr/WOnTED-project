graph [
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "wrona"
    origin "text"
  ]
  node [
    id 2
    label "murza"
  ]
  node [
    id 3
    label "belfer"
  ]
  node [
    id 4
    label "szkolnik"
  ]
  node [
    id 5
    label "pupil"
  ]
  node [
    id 6
    label "ojciec"
  ]
  node [
    id 7
    label "kszta&#322;ciciel"
  ]
  node [
    id 8
    label "Midas"
  ]
  node [
    id 9
    label "przyw&#243;dca"
  ]
  node [
    id 10
    label "opiekun"
  ]
  node [
    id 11
    label "Mieszko_I"
  ]
  node [
    id 12
    label "doros&#322;y"
  ]
  node [
    id 13
    label "pracodawca"
  ]
  node [
    id 14
    label "profesor"
  ]
  node [
    id 15
    label "m&#261;&#380;"
  ]
  node [
    id 16
    label "rz&#261;dzenie"
  ]
  node [
    id 17
    label "bogaty"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "pa&#324;stwo"
  ]
  node [
    id 20
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 21
    label "w&#322;odarz"
  ]
  node [
    id 22
    label "nabab"
  ]
  node [
    id 23
    label "samiec"
  ]
  node [
    id 24
    label "preceptor"
  ]
  node [
    id 25
    label "pedagog"
  ]
  node [
    id 26
    label "efendi"
  ]
  node [
    id 27
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 28
    label "popularyzator"
  ]
  node [
    id 29
    label "gra_w_karty"
  ]
  node [
    id 30
    label "zwrot"
  ]
  node [
    id 31
    label "jegomo&#347;&#263;"
  ]
  node [
    id 32
    label "androlog"
  ]
  node [
    id 33
    label "bratek"
  ]
  node [
    id 34
    label "andropauza"
  ]
  node [
    id 35
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 36
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 37
    label "ch&#322;opina"
  ]
  node [
    id 38
    label "w&#322;adza"
  ]
  node [
    id 39
    label "Sabataj_Cwi"
  ]
  node [
    id 40
    label "lider"
  ]
  node [
    id 41
    label "Mao"
  ]
  node [
    id 42
    label "Anders"
  ]
  node [
    id 43
    label "Fidel_Castro"
  ]
  node [
    id 44
    label "Miko&#322;ajczyk"
  ]
  node [
    id 45
    label "Tito"
  ]
  node [
    id 46
    label "Ko&#347;ciuszko"
  ]
  node [
    id 47
    label "p&#322;atnik"
  ]
  node [
    id 48
    label "zwierzchnik"
  ]
  node [
    id 49
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 50
    label "nadzorca"
  ]
  node [
    id 51
    label "funkcjonariusz"
  ]
  node [
    id 52
    label "podmiot"
  ]
  node [
    id 53
    label "wykupywanie"
  ]
  node [
    id 54
    label "wykupienie"
  ]
  node [
    id 55
    label "bycie_w_posiadaniu"
  ]
  node [
    id 56
    label "rozszerzyciel"
  ]
  node [
    id 57
    label "asymilowa&#263;"
  ]
  node [
    id 58
    label "nasada"
  ]
  node [
    id 59
    label "profanum"
  ]
  node [
    id 60
    label "wz&#243;r"
  ]
  node [
    id 61
    label "senior"
  ]
  node [
    id 62
    label "asymilowanie"
  ]
  node [
    id 63
    label "os&#322;abia&#263;"
  ]
  node [
    id 64
    label "homo_sapiens"
  ]
  node [
    id 65
    label "osoba"
  ]
  node [
    id 66
    label "ludzko&#347;&#263;"
  ]
  node [
    id 67
    label "Adam"
  ]
  node [
    id 68
    label "hominid"
  ]
  node [
    id 69
    label "posta&#263;"
  ]
  node [
    id 70
    label "portrecista"
  ]
  node [
    id 71
    label "polifag"
  ]
  node [
    id 72
    label "podw&#322;adny"
  ]
  node [
    id 73
    label "dwun&#243;g"
  ]
  node [
    id 74
    label "wapniak"
  ]
  node [
    id 75
    label "duch"
  ]
  node [
    id 76
    label "os&#322;abianie"
  ]
  node [
    id 77
    label "antropochoria"
  ]
  node [
    id 78
    label "figura"
  ]
  node [
    id 79
    label "g&#322;owa"
  ]
  node [
    id 80
    label "mikrokosmos"
  ]
  node [
    id 81
    label "oddzia&#322;ywanie"
  ]
  node [
    id 82
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 83
    label "du&#380;y"
  ]
  node [
    id 84
    label "dojrza&#322;y"
  ]
  node [
    id 85
    label "dojrzale"
  ]
  node [
    id 86
    label "wydoro&#347;lenie"
  ]
  node [
    id 87
    label "doro&#347;lenie"
  ]
  node [
    id 88
    label "m&#261;dry"
  ]
  node [
    id 89
    label "&#378;ra&#322;y"
  ]
  node [
    id 90
    label "doletni"
  ]
  node [
    id 91
    label "doro&#347;le"
  ]
  node [
    id 92
    label "punkt"
  ]
  node [
    id 93
    label "zmiana"
  ]
  node [
    id 94
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 95
    label "turn"
  ]
  node [
    id 96
    label "wyra&#380;enie"
  ]
  node [
    id 97
    label "fraza_czasownikowa"
  ]
  node [
    id 98
    label "turning"
  ]
  node [
    id 99
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 100
    label "skr&#281;t"
  ]
  node [
    id 101
    label "jednostka_leksykalna"
  ]
  node [
    id 102
    label "obr&#243;t"
  ]
  node [
    id 103
    label "starosta"
  ]
  node [
    id 104
    label "w&#322;adca"
  ]
  node [
    id 105
    label "zarz&#261;dca"
  ]
  node [
    id 106
    label "nauczyciel_akademicki"
  ]
  node [
    id 107
    label "tytu&#322;"
  ]
  node [
    id 108
    label "stopie&#324;_naukowy"
  ]
  node [
    id 109
    label "konsulent"
  ]
  node [
    id 110
    label "profesura"
  ]
  node [
    id 111
    label "nauczyciel"
  ]
  node [
    id 112
    label "wirtuoz"
  ]
  node [
    id 113
    label "autor"
  ]
  node [
    id 114
    label "szko&#322;a"
  ]
  node [
    id 115
    label "tarcza"
  ]
  node [
    id 116
    label "klasa"
  ]
  node [
    id 117
    label "elew"
  ]
  node [
    id 118
    label "wyprawka"
  ]
  node [
    id 119
    label "mundurek"
  ]
  node [
    id 120
    label "absolwent"
  ]
  node [
    id 121
    label "ochotnik"
  ]
  node [
    id 122
    label "nauczyciel_muzyki"
  ]
  node [
    id 123
    label "pomocnik"
  ]
  node [
    id 124
    label "zakonnik"
  ]
  node [
    id 125
    label "student"
  ]
  node [
    id 126
    label "ekspert"
  ]
  node [
    id 127
    label "bogacz"
  ]
  node [
    id 128
    label "dostojnik"
  ]
  node [
    id 129
    label "urz&#281;dnik"
  ]
  node [
    id 130
    label "&#347;w"
  ]
  node [
    id 131
    label "rodzic"
  ]
  node [
    id 132
    label "pomys&#322;odawca"
  ]
  node [
    id 133
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 134
    label "rodzice"
  ]
  node [
    id 135
    label "wykonawca"
  ]
  node [
    id 136
    label "stary"
  ]
  node [
    id 137
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 138
    label "kuwada"
  ]
  node [
    id 139
    label "ojczym"
  ]
  node [
    id 140
    label "papa"
  ]
  node [
    id 141
    label "przodek"
  ]
  node [
    id 142
    label "tworzyciel"
  ]
  node [
    id 143
    label "facet"
  ]
  node [
    id 144
    label "kochanek"
  ]
  node [
    id 145
    label "fio&#322;ek"
  ]
  node [
    id 146
    label "brat"
  ]
  node [
    id 147
    label "zwierz&#281;"
  ]
  node [
    id 148
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 149
    label "pan_i_w&#322;adca"
  ]
  node [
    id 150
    label "pan_m&#322;ody"
  ]
  node [
    id 151
    label "ch&#322;op"
  ]
  node [
    id 152
    label "&#347;lubny"
  ]
  node [
    id 153
    label "m&#243;j"
  ]
  node [
    id 154
    label "pan_domu"
  ]
  node [
    id 155
    label "ma&#322;&#380;onek"
  ]
  node [
    id 156
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 157
    label "mo&#347;&#263;"
  ]
  node [
    id 158
    label "Frygia"
  ]
  node [
    id 159
    label "dominowanie"
  ]
  node [
    id 160
    label "reign"
  ]
  node [
    id 161
    label "sprawowanie"
  ]
  node [
    id 162
    label "dominion"
  ]
  node [
    id 163
    label "rule"
  ]
  node [
    id 164
    label "zwierz&#281;_domowe"
  ]
  node [
    id 165
    label "John_Dewey"
  ]
  node [
    id 166
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 167
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 168
    label "J&#281;drzejewicz"
  ]
  node [
    id 169
    label "specjalista"
  ]
  node [
    id 170
    label "&#380;ycie"
  ]
  node [
    id 171
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 172
    label "Turek"
  ]
  node [
    id 173
    label "effendi"
  ]
  node [
    id 174
    label "och&#281;do&#380;ny"
  ]
  node [
    id 175
    label "zapa&#347;ny"
  ]
  node [
    id 176
    label "sytuowany"
  ]
  node [
    id 177
    label "obfituj&#261;cy"
  ]
  node [
    id 178
    label "forsiasty"
  ]
  node [
    id 179
    label "spania&#322;y"
  ]
  node [
    id 180
    label "obficie"
  ]
  node [
    id 181
    label "r&#243;&#380;norodny"
  ]
  node [
    id 182
    label "bogato"
  ]
  node [
    id 183
    label "Japonia"
  ]
  node [
    id 184
    label "Zair"
  ]
  node [
    id 185
    label "Belize"
  ]
  node [
    id 186
    label "San_Marino"
  ]
  node [
    id 187
    label "Tanzania"
  ]
  node [
    id 188
    label "Antigua_i_Barbuda"
  ]
  node [
    id 189
    label "granica_pa&#324;stwa"
  ]
  node [
    id 190
    label "Senegal"
  ]
  node [
    id 191
    label "Seszele"
  ]
  node [
    id 192
    label "Mauretania"
  ]
  node [
    id 193
    label "Indie"
  ]
  node [
    id 194
    label "Filipiny"
  ]
  node [
    id 195
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 196
    label "Zimbabwe"
  ]
  node [
    id 197
    label "Malezja"
  ]
  node [
    id 198
    label "Rumunia"
  ]
  node [
    id 199
    label "Surinam"
  ]
  node [
    id 200
    label "Ukraina"
  ]
  node [
    id 201
    label "Syria"
  ]
  node [
    id 202
    label "Wyspy_Marshalla"
  ]
  node [
    id 203
    label "Burkina_Faso"
  ]
  node [
    id 204
    label "Grecja"
  ]
  node [
    id 205
    label "Polska"
  ]
  node [
    id 206
    label "Wenezuela"
  ]
  node [
    id 207
    label "Suazi"
  ]
  node [
    id 208
    label "Nepal"
  ]
  node [
    id 209
    label "S&#322;owacja"
  ]
  node [
    id 210
    label "Algieria"
  ]
  node [
    id 211
    label "Chiny"
  ]
  node [
    id 212
    label "Grenada"
  ]
  node [
    id 213
    label "Barbados"
  ]
  node [
    id 214
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 215
    label "Pakistan"
  ]
  node [
    id 216
    label "Niemcy"
  ]
  node [
    id 217
    label "Bahrajn"
  ]
  node [
    id 218
    label "Komory"
  ]
  node [
    id 219
    label "Australia"
  ]
  node [
    id 220
    label "Rodezja"
  ]
  node [
    id 221
    label "Malawi"
  ]
  node [
    id 222
    label "Gwinea"
  ]
  node [
    id 223
    label "Wehrlen"
  ]
  node [
    id 224
    label "Meksyk"
  ]
  node [
    id 225
    label "Liechtenstein"
  ]
  node [
    id 226
    label "Czarnog&#243;ra"
  ]
  node [
    id 227
    label "Wielka_Brytania"
  ]
  node [
    id 228
    label "Kuwejt"
  ]
  node [
    id 229
    label "Monako"
  ]
  node [
    id 230
    label "Angola"
  ]
  node [
    id 231
    label "Jemen"
  ]
  node [
    id 232
    label "Etiopia"
  ]
  node [
    id 233
    label "Madagaskar"
  ]
  node [
    id 234
    label "terytorium"
  ]
  node [
    id 235
    label "Kolumbia"
  ]
  node [
    id 236
    label "Portoryko"
  ]
  node [
    id 237
    label "Mauritius"
  ]
  node [
    id 238
    label "Kostaryka"
  ]
  node [
    id 239
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 240
    label "Tajlandia"
  ]
  node [
    id 241
    label "Argentyna"
  ]
  node [
    id 242
    label "Zambia"
  ]
  node [
    id 243
    label "Sri_Lanka"
  ]
  node [
    id 244
    label "Gwatemala"
  ]
  node [
    id 245
    label "Kirgistan"
  ]
  node [
    id 246
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 247
    label "Hiszpania"
  ]
  node [
    id 248
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 249
    label "Salwador"
  ]
  node [
    id 250
    label "Korea"
  ]
  node [
    id 251
    label "Macedonia"
  ]
  node [
    id 252
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 253
    label "Brunei"
  ]
  node [
    id 254
    label "Mozambik"
  ]
  node [
    id 255
    label "Turcja"
  ]
  node [
    id 256
    label "Kambod&#380;a"
  ]
  node [
    id 257
    label "Benin"
  ]
  node [
    id 258
    label "Bhutan"
  ]
  node [
    id 259
    label "Tunezja"
  ]
  node [
    id 260
    label "Austria"
  ]
  node [
    id 261
    label "Izrael"
  ]
  node [
    id 262
    label "Sierra_Leone"
  ]
  node [
    id 263
    label "Jamajka"
  ]
  node [
    id 264
    label "Rosja"
  ]
  node [
    id 265
    label "Rwanda"
  ]
  node [
    id 266
    label "holoarktyka"
  ]
  node [
    id 267
    label "Nigeria"
  ]
  node [
    id 268
    label "USA"
  ]
  node [
    id 269
    label "Oman"
  ]
  node [
    id 270
    label "Luksemburg"
  ]
  node [
    id 271
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 272
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 273
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 274
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 275
    label "Dominikana"
  ]
  node [
    id 276
    label "Irlandia"
  ]
  node [
    id 277
    label "Liban"
  ]
  node [
    id 278
    label "Hanower"
  ]
  node [
    id 279
    label "Estonia"
  ]
  node [
    id 280
    label "Iran"
  ]
  node [
    id 281
    label "Nowa_Zelandia"
  ]
  node [
    id 282
    label "Gabon"
  ]
  node [
    id 283
    label "Samoa"
  ]
  node [
    id 284
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 285
    label "S&#322;owenia"
  ]
  node [
    id 286
    label "Kiribati"
  ]
  node [
    id 287
    label "Egipt"
  ]
  node [
    id 288
    label "Togo"
  ]
  node [
    id 289
    label "Mongolia"
  ]
  node [
    id 290
    label "Sudan"
  ]
  node [
    id 291
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 292
    label "Bahamy"
  ]
  node [
    id 293
    label "Bangladesz"
  ]
  node [
    id 294
    label "partia"
  ]
  node [
    id 295
    label "Serbia"
  ]
  node [
    id 296
    label "Czechy"
  ]
  node [
    id 297
    label "Holandia"
  ]
  node [
    id 298
    label "Birma"
  ]
  node [
    id 299
    label "Albania"
  ]
  node [
    id 300
    label "Mikronezja"
  ]
  node [
    id 301
    label "Gambia"
  ]
  node [
    id 302
    label "Kazachstan"
  ]
  node [
    id 303
    label "interior"
  ]
  node [
    id 304
    label "Uzbekistan"
  ]
  node [
    id 305
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 306
    label "Malta"
  ]
  node [
    id 307
    label "Lesoto"
  ]
  node [
    id 308
    label "para"
  ]
  node [
    id 309
    label "Antarktis"
  ]
  node [
    id 310
    label "Andora"
  ]
  node [
    id 311
    label "Nauru"
  ]
  node [
    id 312
    label "Kuba"
  ]
  node [
    id 313
    label "Wietnam"
  ]
  node [
    id 314
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 315
    label "ziemia"
  ]
  node [
    id 316
    label "Kamerun"
  ]
  node [
    id 317
    label "Chorwacja"
  ]
  node [
    id 318
    label "Urugwaj"
  ]
  node [
    id 319
    label "Niger"
  ]
  node [
    id 320
    label "Turkmenistan"
  ]
  node [
    id 321
    label "Szwajcaria"
  ]
  node [
    id 322
    label "organizacja"
  ]
  node [
    id 323
    label "grupa"
  ]
  node [
    id 324
    label "Palau"
  ]
  node [
    id 325
    label "Litwa"
  ]
  node [
    id 326
    label "Gruzja"
  ]
  node [
    id 327
    label "Tajwan"
  ]
  node [
    id 328
    label "Kongo"
  ]
  node [
    id 329
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 330
    label "Honduras"
  ]
  node [
    id 331
    label "Boliwia"
  ]
  node [
    id 332
    label "Uganda"
  ]
  node [
    id 333
    label "Namibia"
  ]
  node [
    id 334
    label "Azerbejd&#380;an"
  ]
  node [
    id 335
    label "Erytrea"
  ]
  node [
    id 336
    label "Gujana"
  ]
  node [
    id 337
    label "Panama"
  ]
  node [
    id 338
    label "Somalia"
  ]
  node [
    id 339
    label "Burundi"
  ]
  node [
    id 340
    label "Tuwalu"
  ]
  node [
    id 341
    label "Libia"
  ]
  node [
    id 342
    label "Katar"
  ]
  node [
    id 343
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 344
    label "Sahara_Zachodnia"
  ]
  node [
    id 345
    label "Trynidad_i_Tobago"
  ]
  node [
    id 346
    label "Gwinea_Bissau"
  ]
  node [
    id 347
    label "Bu&#322;garia"
  ]
  node [
    id 348
    label "Fid&#380;i"
  ]
  node [
    id 349
    label "Nikaragua"
  ]
  node [
    id 350
    label "Tonga"
  ]
  node [
    id 351
    label "Timor_Wschodni"
  ]
  node [
    id 352
    label "Laos"
  ]
  node [
    id 353
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 354
    label "Ghana"
  ]
  node [
    id 355
    label "Brazylia"
  ]
  node [
    id 356
    label "Belgia"
  ]
  node [
    id 357
    label "Irak"
  ]
  node [
    id 358
    label "Peru"
  ]
  node [
    id 359
    label "Arabia_Saudyjska"
  ]
  node [
    id 360
    label "Indonezja"
  ]
  node [
    id 361
    label "Malediwy"
  ]
  node [
    id 362
    label "Afganistan"
  ]
  node [
    id 363
    label "Jordania"
  ]
  node [
    id 364
    label "Kenia"
  ]
  node [
    id 365
    label "Czad"
  ]
  node [
    id 366
    label "Liberia"
  ]
  node [
    id 367
    label "W&#281;gry"
  ]
  node [
    id 368
    label "Chile"
  ]
  node [
    id 369
    label "Mali"
  ]
  node [
    id 370
    label "Armenia"
  ]
  node [
    id 371
    label "Kanada"
  ]
  node [
    id 372
    label "Cypr"
  ]
  node [
    id 373
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 374
    label "Ekwador"
  ]
  node [
    id 375
    label "Mo&#322;dawia"
  ]
  node [
    id 376
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 377
    label "W&#322;ochy"
  ]
  node [
    id 378
    label "Wyspy_Salomona"
  ]
  node [
    id 379
    label "&#321;otwa"
  ]
  node [
    id 380
    label "D&#380;ibuti"
  ]
  node [
    id 381
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 382
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 383
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 384
    label "Portugalia"
  ]
  node [
    id 385
    label "Botswana"
  ]
  node [
    id 386
    label "Maroko"
  ]
  node [
    id 387
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 388
    label "Francja"
  ]
  node [
    id 389
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 390
    label "Dominika"
  ]
  node [
    id 391
    label "Paragwaj"
  ]
  node [
    id 392
    label "Tad&#380;ykistan"
  ]
  node [
    id 393
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 394
    label "Haiti"
  ]
  node [
    id 395
    label "Khitai"
  ]
  node [
    id 396
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 397
    label "kraka&#263;"
  ]
  node [
    id 398
    label "gapa"
  ]
  node [
    id 399
    label "ptak_pospolity"
  ]
  node [
    id 400
    label "krukowate"
  ]
  node [
    id 401
    label "zakraka&#263;"
  ]
  node [
    id 402
    label "klacz"
  ]
  node [
    id 403
    label "mare"
  ]
  node [
    id 404
    label "ko&#324;"
  ]
  node [
    id 405
    label "samica"
  ]
  node [
    id 406
    label "&#347;piewaj&#261;ce"
  ]
  node [
    id 407
    label "gamo&#324;"
  ]
  node [
    id 408
    label "orze&#322;"
  ]
  node [
    id 409
    label "kania"
  ]
  node [
    id 410
    label "gawron"
  ]
  node [
    id 411
    label "odznaka"
  ]
  node [
    id 412
    label "caw"
  ]
  node [
    id 413
    label "kruk"
  ]
  node [
    id 414
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 415
    label "przepowiada&#263;"
  ]
  node [
    id 416
    label "gaworzy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 1
    target 397
  ]
  edge [
    source 1
    target 398
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 400
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 1
    target 402
  ]
  edge [
    source 1
    target 403
  ]
  edge [
    source 1
    target 404
  ]
  edge [
    source 1
    target 405
  ]
  edge [
    source 1
    target 406
  ]
  edge [
    source 1
    target 407
  ]
  edge [
    source 1
    target 408
  ]
  edge [
    source 1
    target 409
  ]
  edge [
    source 1
    target 410
  ]
  edge [
    source 1
    target 411
  ]
  edge [
    source 1
    target 412
  ]
  edge [
    source 1
    target 413
  ]
  edge [
    source 1
    target 414
  ]
  edge [
    source 1
    target 415
  ]
  edge [
    source 1
    target 416
  ]
]
