graph [
  node [
    id 0
    label "r&#243;&#380;nica"
    origin "text"
  ]
  node [
    id 1
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 2
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 3
    label "liczba"
    origin "text"
  ]
  node [
    id 4
    label "narodziny"
    origin "text"
  ]
  node [
    id 5
    label "ch&#322;opiec"
    origin "text"
  ]
  node [
    id 6
    label "dziewczynka"
    origin "text"
  ]
  node [
    id 7
    label "uwzgl&#281;dnia&#263;"
    origin "text"
  ]
  node [
    id 8
    label "imigrant"
    origin "text"
  ]
  node [
    id 9
    label "r&#243;&#380;nienie"
  ]
  node [
    id 10
    label "kontrastowy"
  ]
  node [
    id 11
    label "discord"
  ]
  node [
    id 12
    label "cecha"
  ]
  node [
    id 13
    label "wynik"
  ]
  node [
    id 14
    label "charakterystyka"
  ]
  node [
    id 15
    label "m&#322;ot"
  ]
  node [
    id 16
    label "znak"
  ]
  node [
    id 17
    label "drzewo"
  ]
  node [
    id 18
    label "pr&#243;ba"
  ]
  node [
    id 19
    label "attribute"
  ]
  node [
    id 20
    label "marka"
  ]
  node [
    id 21
    label "zaokr&#261;glenie"
  ]
  node [
    id 22
    label "dzia&#322;anie"
  ]
  node [
    id 23
    label "typ"
  ]
  node [
    id 24
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 25
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 26
    label "rezultat"
  ]
  node [
    id 27
    label "event"
  ]
  node [
    id 28
    label "przyczyna"
  ]
  node [
    id 29
    label "kontrastowo"
  ]
  node [
    id 30
    label "zdecydowany"
  ]
  node [
    id 31
    label "r&#243;&#380;ny"
  ]
  node [
    id 32
    label "wyrazisty"
  ]
  node [
    id 33
    label "ostry"
  ]
  node [
    id 34
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 35
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 36
    label "appear"
  ]
  node [
    id 37
    label "rise"
  ]
  node [
    id 38
    label "doros&#322;y"
  ]
  node [
    id 39
    label "znaczny"
  ]
  node [
    id 40
    label "niema&#322;o"
  ]
  node [
    id 41
    label "wiele"
  ]
  node [
    id 42
    label "rozwini&#281;ty"
  ]
  node [
    id 43
    label "dorodny"
  ]
  node [
    id 44
    label "wa&#380;ny"
  ]
  node [
    id 45
    label "prawdziwy"
  ]
  node [
    id 46
    label "du&#380;o"
  ]
  node [
    id 47
    label "&#380;ywny"
  ]
  node [
    id 48
    label "szczery"
  ]
  node [
    id 49
    label "naturalny"
  ]
  node [
    id 50
    label "naprawd&#281;"
  ]
  node [
    id 51
    label "realnie"
  ]
  node [
    id 52
    label "podobny"
  ]
  node [
    id 53
    label "zgodny"
  ]
  node [
    id 54
    label "m&#261;dry"
  ]
  node [
    id 55
    label "prawdziwie"
  ]
  node [
    id 56
    label "znacznie"
  ]
  node [
    id 57
    label "zauwa&#380;alny"
  ]
  node [
    id 58
    label "wynios&#322;y"
  ]
  node [
    id 59
    label "dono&#347;ny"
  ]
  node [
    id 60
    label "silny"
  ]
  node [
    id 61
    label "wa&#380;nie"
  ]
  node [
    id 62
    label "istotnie"
  ]
  node [
    id 63
    label "eksponowany"
  ]
  node [
    id 64
    label "dobry"
  ]
  node [
    id 65
    label "ukszta&#322;towany"
  ]
  node [
    id 66
    label "do&#347;cig&#322;y"
  ]
  node [
    id 67
    label "&#378;ra&#322;y"
  ]
  node [
    id 68
    label "zdr&#243;w"
  ]
  node [
    id 69
    label "dorodnie"
  ]
  node [
    id 70
    label "okaza&#322;y"
  ]
  node [
    id 71
    label "mocno"
  ]
  node [
    id 72
    label "wiela"
  ]
  node [
    id 73
    label "bardzo"
  ]
  node [
    id 74
    label "cz&#281;sto"
  ]
  node [
    id 75
    label "wydoro&#347;lenie"
  ]
  node [
    id 76
    label "cz&#322;owiek"
  ]
  node [
    id 77
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 78
    label "doro&#347;lenie"
  ]
  node [
    id 79
    label "doro&#347;le"
  ]
  node [
    id 80
    label "senior"
  ]
  node [
    id 81
    label "dojrzale"
  ]
  node [
    id 82
    label "wapniak"
  ]
  node [
    id 83
    label "dojrza&#322;y"
  ]
  node [
    id 84
    label "doletni"
  ]
  node [
    id 85
    label "kategoria"
  ]
  node [
    id 86
    label "pierwiastek"
  ]
  node [
    id 87
    label "rozmiar"
  ]
  node [
    id 88
    label "poj&#281;cie"
  ]
  node [
    id 89
    label "number"
  ]
  node [
    id 90
    label "kategoria_gramatyczna"
  ]
  node [
    id 91
    label "grupa"
  ]
  node [
    id 92
    label "kwadrat_magiczny"
  ]
  node [
    id 93
    label "wyra&#380;enie"
  ]
  node [
    id 94
    label "koniugacja"
  ]
  node [
    id 95
    label "zbi&#243;r"
  ]
  node [
    id 96
    label "wytw&#243;r"
  ]
  node [
    id 97
    label "type"
  ]
  node [
    id 98
    label "teoria"
  ]
  node [
    id 99
    label "forma"
  ]
  node [
    id 100
    label "klasa"
  ]
  node [
    id 101
    label "odm&#322;adzanie"
  ]
  node [
    id 102
    label "liga"
  ]
  node [
    id 103
    label "jednostka_systematyczna"
  ]
  node [
    id 104
    label "asymilowanie"
  ]
  node [
    id 105
    label "gromada"
  ]
  node [
    id 106
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 107
    label "asymilowa&#263;"
  ]
  node [
    id 108
    label "egzemplarz"
  ]
  node [
    id 109
    label "Entuzjastki"
  ]
  node [
    id 110
    label "kompozycja"
  ]
  node [
    id 111
    label "Terranie"
  ]
  node [
    id 112
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 113
    label "category"
  ]
  node [
    id 114
    label "pakiet_klimatyczny"
  ]
  node [
    id 115
    label "oddzia&#322;"
  ]
  node [
    id 116
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 117
    label "cz&#261;steczka"
  ]
  node [
    id 118
    label "stage_set"
  ]
  node [
    id 119
    label "specgrupa"
  ]
  node [
    id 120
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 121
    label "&#346;wietliki"
  ]
  node [
    id 122
    label "odm&#322;odzenie"
  ]
  node [
    id 123
    label "Eurogrupa"
  ]
  node [
    id 124
    label "odm&#322;adza&#263;"
  ]
  node [
    id 125
    label "formacja_geologiczna"
  ]
  node [
    id 126
    label "harcerze_starsi"
  ]
  node [
    id 127
    label "pos&#322;uchanie"
  ]
  node [
    id 128
    label "skumanie"
  ]
  node [
    id 129
    label "orientacja"
  ]
  node [
    id 130
    label "zorientowanie"
  ]
  node [
    id 131
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 132
    label "clasp"
  ]
  node [
    id 133
    label "przem&#243;wienie"
  ]
  node [
    id 134
    label "warunek_lokalowy"
  ]
  node [
    id 135
    label "circumference"
  ]
  node [
    id 136
    label "odzie&#380;"
  ]
  node [
    id 137
    label "ilo&#347;&#263;"
  ]
  node [
    id 138
    label "znaczenie"
  ]
  node [
    id 139
    label "dymensja"
  ]
  node [
    id 140
    label "fleksja"
  ]
  node [
    id 141
    label "coupling"
  ]
  node [
    id 142
    label "osoba"
  ]
  node [
    id 143
    label "tryb"
  ]
  node [
    id 144
    label "czas"
  ]
  node [
    id 145
    label "czasownik"
  ]
  node [
    id 146
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 147
    label "orz&#281;sek"
  ]
  node [
    id 148
    label "leksem"
  ]
  node [
    id 149
    label "sformu&#322;owanie"
  ]
  node [
    id 150
    label "zdarzenie_si&#281;"
  ]
  node [
    id 151
    label "poinformowanie"
  ]
  node [
    id 152
    label "wording"
  ]
  node [
    id 153
    label "oznaczenie"
  ]
  node [
    id 154
    label "znak_j&#281;zykowy"
  ]
  node [
    id 155
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 156
    label "ozdobnik"
  ]
  node [
    id 157
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 158
    label "grupa_imienna"
  ]
  node [
    id 159
    label "jednostka_leksykalna"
  ]
  node [
    id 160
    label "term"
  ]
  node [
    id 161
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 162
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 163
    label "ujawnienie"
  ]
  node [
    id 164
    label "affirmation"
  ]
  node [
    id 165
    label "zapisanie"
  ]
  node [
    id 166
    label "rzucenie"
  ]
  node [
    id 167
    label "substancja_chemiczna"
  ]
  node [
    id 168
    label "morfem"
  ]
  node [
    id 169
    label "sk&#322;adnik"
  ]
  node [
    id 170
    label "root"
  ]
  node [
    id 171
    label "pocz&#261;tek"
  ]
  node [
    id 172
    label "pierworodztwo"
  ]
  node [
    id 173
    label "faza"
  ]
  node [
    id 174
    label "miejsce"
  ]
  node [
    id 175
    label "upgrade"
  ]
  node [
    id 176
    label "nast&#281;pstwo"
  ]
  node [
    id 177
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 178
    label "g&#243;wniarz"
  ]
  node [
    id 179
    label "synek"
  ]
  node [
    id 180
    label "boyfriend"
  ]
  node [
    id 181
    label "okrzos"
  ]
  node [
    id 182
    label "dziecko"
  ]
  node [
    id 183
    label "sympatia"
  ]
  node [
    id 184
    label "usynowienie"
  ]
  node [
    id 185
    label "pomocnik"
  ]
  node [
    id 186
    label "kawaler"
  ]
  node [
    id 187
    label "&#347;l&#261;ski"
  ]
  node [
    id 188
    label "m&#322;odzieniec"
  ]
  node [
    id 189
    label "kajtek"
  ]
  node [
    id 190
    label "pederasta"
  ]
  node [
    id 191
    label "usynawianie"
  ]
  node [
    id 192
    label "utulenie"
  ]
  node [
    id 193
    label "pediatra"
  ]
  node [
    id 194
    label "dzieciak"
  ]
  node [
    id 195
    label "utulanie"
  ]
  node [
    id 196
    label "dzieciarnia"
  ]
  node [
    id 197
    label "niepe&#322;noletni"
  ]
  node [
    id 198
    label "organizm"
  ]
  node [
    id 199
    label "utula&#263;"
  ]
  node [
    id 200
    label "cz&#322;owieczek"
  ]
  node [
    id 201
    label "fledgling"
  ]
  node [
    id 202
    label "zwierz&#281;"
  ]
  node [
    id 203
    label "utuli&#263;"
  ]
  node [
    id 204
    label "m&#322;odzik"
  ]
  node [
    id 205
    label "pedofil"
  ]
  node [
    id 206
    label "m&#322;odziak"
  ]
  node [
    id 207
    label "potomek"
  ]
  node [
    id 208
    label "entliczek-pentliczek"
  ]
  node [
    id 209
    label "potomstwo"
  ]
  node [
    id 210
    label "sraluch"
  ]
  node [
    id 211
    label "ludzko&#347;&#263;"
  ]
  node [
    id 212
    label "os&#322;abia&#263;"
  ]
  node [
    id 213
    label "posta&#263;"
  ]
  node [
    id 214
    label "hominid"
  ]
  node [
    id 215
    label "podw&#322;adny"
  ]
  node [
    id 216
    label "os&#322;abianie"
  ]
  node [
    id 217
    label "g&#322;owa"
  ]
  node [
    id 218
    label "figura"
  ]
  node [
    id 219
    label "portrecista"
  ]
  node [
    id 220
    label "dwun&#243;g"
  ]
  node [
    id 221
    label "profanum"
  ]
  node [
    id 222
    label "mikrokosmos"
  ]
  node [
    id 223
    label "nasada"
  ]
  node [
    id 224
    label "duch"
  ]
  node [
    id 225
    label "antropochoria"
  ]
  node [
    id 226
    label "wz&#243;r"
  ]
  node [
    id 227
    label "oddzia&#322;ywanie"
  ]
  node [
    id 228
    label "Adam"
  ]
  node [
    id 229
    label "homo_sapiens"
  ]
  node [
    id 230
    label "polifag"
  ]
  node [
    id 231
    label "emocja"
  ]
  node [
    id 232
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 233
    label "partner"
  ]
  node [
    id 234
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 235
    label "love"
  ]
  node [
    id 236
    label "kredens"
  ]
  node [
    id 237
    label "zawodnik"
  ]
  node [
    id 238
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 239
    label "bylina"
  ]
  node [
    id 240
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 241
    label "gracz"
  ]
  node [
    id 242
    label "r&#281;ka"
  ]
  node [
    id 243
    label "pomoc"
  ]
  node [
    id 244
    label "wrzosowate"
  ]
  node [
    id 245
    label "pomagacz"
  ]
  node [
    id 246
    label "junior"
  ]
  node [
    id 247
    label "junak"
  ]
  node [
    id 248
    label "m&#322;odzie&#380;"
  ]
  node [
    id 249
    label "mo&#322;ojec"
  ]
  node [
    id 250
    label "kawa&#322;ek"
  ]
  node [
    id 251
    label "m&#322;okos"
  ]
  node [
    id 252
    label "smarkateria"
  ]
  node [
    id 253
    label "ch&#322;opak"
  ]
  node [
    id 254
    label "cug"
  ]
  node [
    id 255
    label "krepel"
  ]
  node [
    id 256
    label "mietlorz"
  ]
  node [
    id 257
    label "francuz"
  ]
  node [
    id 258
    label "etnolekt"
  ]
  node [
    id 259
    label "sza&#322;ot"
  ]
  node [
    id 260
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 261
    label "polski"
  ]
  node [
    id 262
    label "regionalny"
  ]
  node [
    id 263
    label "halba"
  ]
  node [
    id 264
    label "buchta"
  ]
  node [
    id 265
    label "czarne_kluski"
  ]
  node [
    id 266
    label "szpajza"
  ]
  node [
    id 267
    label "szl&#261;ski"
  ]
  node [
    id 268
    label "&#347;lonski"
  ]
  node [
    id 269
    label "po_&#347;l&#261;sku"
  ]
  node [
    id 270
    label "waloszek"
  ]
  node [
    id 271
    label "gej"
  ]
  node [
    id 272
    label "tytu&#322;"
  ]
  node [
    id 273
    label "order"
  ]
  node [
    id 274
    label "zalotnik"
  ]
  node [
    id 275
    label "kawalerka"
  ]
  node [
    id 276
    label "rycerz"
  ]
  node [
    id 277
    label "odznaczenie"
  ]
  node [
    id 278
    label "nie&#380;onaty"
  ]
  node [
    id 279
    label "zakon_rycerski"
  ]
  node [
    id 280
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 281
    label "zakonnik"
  ]
  node [
    id 282
    label "syn"
  ]
  node [
    id 283
    label "przysposabianie"
  ]
  node [
    id 284
    label "przysposobienie"
  ]
  node [
    id 285
    label "adoption"
  ]
  node [
    id 286
    label "prostytutka"
  ]
  node [
    id 287
    label "potomkini"
  ]
  node [
    id 288
    label "krewna"
  ]
  node [
    id 289
    label "kurwa"
  ]
  node [
    id 290
    label "pigalak"
  ]
  node [
    id 291
    label "ma&#322;pa"
  ]
  node [
    id 292
    label "dziewczyna_lekkich_obyczaj&#243;w"
  ]
  node [
    id 293
    label "rozpustnica"
  ]
  node [
    id 294
    label "diva"
  ]
  node [
    id 295
    label "jawnogrzesznica"
  ]
  node [
    id 296
    label "my&#347;le&#263;"
  ]
  node [
    id 297
    label "involve"
  ]
  node [
    id 298
    label "robi&#263;"
  ]
  node [
    id 299
    label "take_care"
  ]
  node [
    id 300
    label "troska&#263;_si&#281;"
  ]
  node [
    id 301
    label "deliver"
  ]
  node [
    id 302
    label "rozpatrywa&#263;"
  ]
  node [
    id 303
    label "zamierza&#263;"
  ]
  node [
    id 304
    label "argue"
  ]
  node [
    id 305
    label "os&#261;dza&#263;"
  ]
  node [
    id 306
    label "cudzoziemiec"
  ]
  node [
    id 307
    label "przybysz"
  ]
  node [
    id 308
    label "migrant"
  ]
  node [
    id 309
    label "imigracja"
  ]
  node [
    id 310
    label "ochrona_uzupe&#322;niaj&#261;ca"
  ]
  node [
    id 311
    label "nowy"
  ]
  node [
    id 312
    label "miesi&#261;c_ksi&#281;&#380;ycowy"
  ]
  node [
    id 313
    label "obcy"
  ]
  node [
    id 314
    label "obcokrajowy"
  ]
  node [
    id 315
    label "etran&#380;er"
  ]
  node [
    id 316
    label "mieszkaniec"
  ]
  node [
    id 317
    label "zagraniczny"
  ]
  node [
    id 318
    label "cudzoziemski"
  ]
  node [
    id 319
    label "nap&#322;yw"
  ]
  node [
    id 320
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 321
    label "immigration"
  ]
  node [
    id 322
    label "migracja"
  ]
  node [
    id 323
    label "Bengalczyk&#243;w"
  ]
  node [
    id 324
    label "98"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 323
    target 324
  ]
]
