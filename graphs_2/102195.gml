graph [
  node [
    id 0
    label "daleko"
    origin "text"
  ]
  node [
    id 1
    label "omarz&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "noga"
    origin "text"
  ]
  node [
    id 3
    label "martwy"
    origin "text"
  ]
  node [
    id 4
    label "szkieletowy"
    origin "text"
  ]
  node [
    id 5
    label "wola"
    origin "text"
  ]
  node [
    id 6
    label "raczej"
    origin "text"
  ]
  node [
    id 7
    label "niepoj&#281;ty"
    origin "text"
  ]
  node [
    id 8
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 9
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 10
    label "prowadzony"
    origin "text"
  ]
  node [
    id 11
    label "st&#261;pa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "&#347;lad"
    origin "text"
  ]
  node [
    id 13
    label "skostnia&#322;y"
    origin "text"
  ]
  node [
    id 14
    label "dziecko"
    origin "text"
  ]
  node [
    id 15
    label "otuli&#263;"
    origin "text"
  ]
  node [
    id 16
    label "paruch&#261;"
    origin "text"
  ]
  node [
    id 17
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 18
    label "matka"
    origin "text"
  ]
  node [
    id 19
    label "zdj&#261;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "siebie"
    origin "text"
  ]
  node [
    id 21
    label "nisko"
  ]
  node [
    id 22
    label "znacznie"
  ]
  node [
    id 23
    label "het"
  ]
  node [
    id 24
    label "dawno"
  ]
  node [
    id 25
    label "daleki"
  ]
  node [
    id 26
    label "g&#322;&#281;boko"
  ]
  node [
    id 27
    label "nieobecnie"
  ]
  node [
    id 28
    label "wysoko"
  ]
  node [
    id 29
    label "du&#380;o"
  ]
  node [
    id 30
    label "dawny"
  ]
  node [
    id 31
    label "ogl&#281;dny"
  ]
  node [
    id 32
    label "d&#322;ugi"
  ]
  node [
    id 33
    label "du&#380;y"
  ]
  node [
    id 34
    label "odleg&#322;y"
  ]
  node [
    id 35
    label "zwi&#261;zany"
  ]
  node [
    id 36
    label "r&#243;&#380;ny"
  ]
  node [
    id 37
    label "s&#322;aby"
  ]
  node [
    id 38
    label "odlegle"
  ]
  node [
    id 39
    label "oddalony"
  ]
  node [
    id 40
    label "g&#322;&#281;boki"
  ]
  node [
    id 41
    label "obcy"
  ]
  node [
    id 42
    label "nieobecny"
  ]
  node [
    id 43
    label "przysz&#322;y"
  ]
  node [
    id 44
    label "niepo&#347;lednio"
  ]
  node [
    id 45
    label "wysoki"
  ]
  node [
    id 46
    label "g&#243;rno"
  ]
  node [
    id 47
    label "chwalebnie"
  ]
  node [
    id 48
    label "wznio&#347;le"
  ]
  node [
    id 49
    label "szczytny"
  ]
  node [
    id 50
    label "d&#322;ugotrwale"
  ]
  node [
    id 51
    label "wcze&#347;niej"
  ]
  node [
    id 52
    label "ongi&#347;"
  ]
  node [
    id 53
    label "dawnie"
  ]
  node [
    id 54
    label "zamy&#347;lony"
  ]
  node [
    id 55
    label "uni&#380;enie"
  ]
  node [
    id 56
    label "pospolicie"
  ]
  node [
    id 57
    label "blisko"
  ]
  node [
    id 58
    label "wstydliwie"
  ]
  node [
    id 59
    label "ma&#322;o"
  ]
  node [
    id 60
    label "vilely"
  ]
  node [
    id 61
    label "despicably"
  ]
  node [
    id 62
    label "niski"
  ]
  node [
    id 63
    label "po&#347;lednio"
  ]
  node [
    id 64
    label "ma&#322;y"
  ]
  node [
    id 65
    label "mocno"
  ]
  node [
    id 66
    label "gruntownie"
  ]
  node [
    id 67
    label "szczerze"
  ]
  node [
    id 68
    label "silnie"
  ]
  node [
    id 69
    label "intensywnie"
  ]
  node [
    id 70
    label "wiela"
  ]
  node [
    id 71
    label "bardzo"
  ]
  node [
    id 72
    label "cz&#281;sto"
  ]
  node [
    id 73
    label "zauwa&#380;alnie"
  ]
  node [
    id 74
    label "znaczny"
  ]
  node [
    id 75
    label "dogrywa&#263;"
  ]
  node [
    id 76
    label "s&#322;abeusz"
  ]
  node [
    id 77
    label "mi&#281;sie&#324;_krawiecki"
  ]
  node [
    id 78
    label "mi&#281;sie&#324;_po&#347;ladkowy_wielki"
  ]
  node [
    id 79
    label "czpas"
  ]
  node [
    id 80
    label "nerw_udowy"
  ]
  node [
    id 81
    label "bezbramkowy"
  ]
  node [
    id 82
    label "podpora"
  ]
  node [
    id 83
    label "faulowa&#263;"
  ]
  node [
    id 84
    label "mi&#281;sie&#324;_po&#347;ladkowy_&#347;redni"
  ]
  node [
    id 85
    label "zamurowanie"
  ]
  node [
    id 86
    label "depta&#263;"
  ]
  node [
    id 87
    label "mi&#281;czak"
  ]
  node [
    id 88
    label "stopa"
  ]
  node [
    id 89
    label "d&#322;uga_pi&#322;ka"
  ]
  node [
    id 90
    label "ko&#324;czyna_dolna"
  ]
  node [
    id 91
    label "mato&#322;"
  ]
  node [
    id 92
    label "ekstraklasa"
  ]
  node [
    id 93
    label "sfaulowa&#263;"
  ]
  node [
    id 94
    label "&#322;&#261;czyna"
  ]
  node [
    id 95
    label "lobowanie"
  ]
  node [
    id 96
    label "dogrywanie"
  ]
  node [
    id 97
    label "napinacz"
  ]
  node [
    id 98
    label "dublet"
  ]
  node [
    id 99
    label "sfaulowanie"
  ]
  node [
    id 100
    label "lobowa&#263;"
  ]
  node [
    id 101
    label "gira"
  ]
  node [
    id 102
    label "bramkarz"
  ]
  node [
    id 103
    label "faulowanie"
  ]
  node [
    id 104
    label "zamurowywanie"
  ]
  node [
    id 105
    label "kopni&#281;cie"
  ]
  node [
    id 106
    label "&#322;amaga"
  ]
  node [
    id 107
    label "kopn&#261;&#263;"
  ]
  node [
    id 108
    label "dogranie"
  ]
  node [
    id 109
    label "kopanie"
  ]
  node [
    id 110
    label "pi&#322;ka"
  ]
  node [
    id 111
    label "przelobowa&#263;"
  ]
  node [
    id 112
    label "mundial"
  ]
  node [
    id 113
    label "kopa&#263;"
  ]
  node [
    id 114
    label "r&#281;ka"
  ]
  node [
    id 115
    label "catenaccio"
  ]
  node [
    id 116
    label "dogra&#263;"
  ]
  node [
    id 117
    label "ko&#324;czyna"
  ]
  node [
    id 118
    label "tackle"
  ]
  node [
    id 119
    label "nerw_udowo-goleniowy"
  ]
  node [
    id 120
    label "narz&#261;d_ruchu"
  ]
  node [
    id 121
    label "zamurowywa&#263;"
  ]
  node [
    id 122
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 123
    label "mi&#281;sie&#324;_po&#347;ladkowy_ma&#322;y"
  ]
  node [
    id 124
    label "interliga"
  ]
  node [
    id 125
    label "przelobowanie"
  ]
  node [
    id 126
    label "czerwona_kartka"
  ]
  node [
    id 127
    label "Wis&#322;a"
  ]
  node [
    id 128
    label "zamurowa&#263;"
  ]
  node [
    id 129
    label "jedenastka"
  ]
  node [
    id 130
    label "badyl"
  ]
  node [
    id 131
    label "abduktor"
  ]
  node [
    id 132
    label "przywodziciel"
  ]
  node [
    id 133
    label "paraplegia"
  ]
  node [
    id 134
    label "immobilizacja"
  ]
  node [
    id 135
    label "cz&#322;onek"
  ]
  node [
    id 136
    label "nadzieja"
  ]
  node [
    id 137
    label "podstawa"
  ]
  node [
    id 138
    label "element_konstrukcyjny"
  ]
  node [
    id 139
    label "column"
  ]
  node [
    id 140
    label "oferma"
  ]
  node [
    id 141
    label "poprawkowicz"
  ]
  node [
    id 142
    label "cz&#322;owiek"
  ]
  node [
    id 143
    label "char&#322;ak"
  ]
  node [
    id 144
    label "ucze&#324;"
  ]
  node [
    id 145
    label "miernota"
  ]
  node [
    id 146
    label "po&#347;miewisko"
  ]
  node [
    id 147
    label "istota_&#380;ywa"
  ]
  node [
    id 148
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 149
    label "mlon"
  ]
  node [
    id 150
    label "t&#281;pak"
  ]
  node [
    id 151
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 152
    label "ignorant"
  ]
  node [
    id 153
    label "baran"
  ]
  node [
    id 154
    label "kula"
  ]
  node [
    id 155
    label "zagrywka"
  ]
  node [
    id 156
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 157
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 158
    label "do&#347;rodkowywanie"
  ]
  node [
    id 159
    label "odbicie"
  ]
  node [
    id 160
    label "gra"
  ]
  node [
    id 161
    label "musket_ball"
  ]
  node [
    id 162
    label "aut"
  ]
  node [
    id 163
    label "serwowa&#263;"
  ]
  node [
    id 164
    label "sport_zespo&#322;owy"
  ]
  node [
    id 165
    label "sport"
  ]
  node [
    id 166
    label "serwowanie"
  ]
  node [
    id 167
    label "orb"
  ]
  node [
    id 168
    label "&#347;wieca"
  ]
  node [
    id 169
    label "zaserwowanie"
  ]
  node [
    id 170
    label "zaserwowa&#263;"
  ]
  node [
    id 171
    label "rzucanka"
  ]
  node [
    id 172
    label "jama_ustna"
  ]
  node [
    id 173
    label "mi&#281;sie&#324;"
  ]
  node [
    id 174
    label "ucho"
  ]
  node [
    id 175
    label "podbicie"
  ]
  node [
    id 176
    label "wska&#378;nik"
  ]
  node [
    id 177
    label "arsa"
  ]
  node [
    id 178
    label "podeszwa"
  ]
  node [
    id 179
    label "t&#281;tnica_&#322;ukowata"
  ]
  node [
    id 180
    label "palce"
  ]
  node [
    id 181
    label "footing"
  ]
  node [
    id 182
    label "zawarto&#347;&#263;"
  ]
  node [
    id 183
    label "ko&#347;&#263;_sze&#347;cienna"
  ]
  node [
    id 184
    label "ko&#324;sko&#347;&#263;"
  ]
  node [
    id 185
    label "sylaba"
  ]
  node [
    id 186
    label "&#347;r&#243;dstopie"
  ]
  node [
    id 187
    label "struktura_anatomiczna"
  ]
  node [
    id 188
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 189
    label "trypodia"
  ]
  node [
    id 190
    label "mechanizm"
  ]
  node [
    id 191
    label "hi-hat"
  ]
  node [
    id 192
    label "poziom"
  ]
  node [
    id 193
    label "st&#281;p"
  ]
  node [
    id 194
    label "paluch"
  ]
  node [
    id 195
    label "iloczas"
  ]
  node [
    id 196
    label "metrical_foot"
  ]
  node [
    id 197
    label "ko&#347;&#263;_klinowata"
  ]
  node [
    id 198
    label "odn&#243;&#380;e"
  ]
  node [
    id 199
    label "centrala"
  ]
  node [
    id 200
    label "pi&#281;ta"
  ]
  node [
    id 201
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 202
    label "brudzi&#263;"
  ]
  node [
    id 203
    label "zap&#322;adnia&#263;"
  ]
  node [
    id 204
    label "niszczy&#263;"
  ]
  node [
    id 205
    label "zniewa&#380;a&#263;"
  ]
  node [
    id 206
    label "narusza&#263;"
  ]
  node [
    id 207
    label "trample"
  ]
  node [
    id 208
    label "nast&#281;powa&#263;"
  ]
  node [
    id 209
    label "ochraniacz"
  ]
  node [
    id 210
    label "je&#378;dziec"
  ]
  node [
    id 211
    label "st&#243;&#322;"
  ]
  node [
    id 212
    label "krzes&#322;o"
  ]
  node [
    id 213
    label "rozgrywanie"
  ]
  node [
    id 214
    label "murowanie"
  ]
  node [
    id 215
    label "zamykanie"
  ]
  node [
    id 216
    label "tworzenie"
  ]
  node [
    id 217
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 218
    label "zabudowywanie"
  ]
  node [
    id 219
    label "pora&#380;enie"
  ]
  node [
    id 220
    label "recoil"
  ]
  node [
    id 221
    label "cios"
  ]
  node [
    id 222
    label "uderzenie"
  ]
  node [
    id 223
    label "kopniak"
  ]
  node [
    id 224
    label "kick"
  ]
  node [
    id 225
    label "wkopanie"
  ]
  node [
    id 226
    label "siatk&#243;wka"
  ]
  node [
    id 227
    label "przerzucenie"
  ]
  node [
    id 228
    label "koszyk&#243;wka"
  ]
  node [
    id 229
    label "obrona"
  ]
  node [
    id 230
    label "hokej"
  ]
  node [
    id 231
    label "zawodnik"
  ]
  node [
    id 232
    label "gracz"
  ]
  node [
    id 233
    label "pi&#322;ka_r&#281;czna"
  ]
  node [
    id 234
    label "bileter"
  ]
  node [
    id 235
    label "wykidaj&#322;o"
  ]
  node [
    id 236
    label "San"
  ]
  node [
    id 237
    label "Brda"
  ]
  node [
    id 238
    label "Pilica"
  ]
  node [
    id 239
    label "Drw&#281;ca"
  ]
  node [
    id 240
    label "Narew"
  ]
  node [
    id 241
    label "Polska"
  ]
  node [
    id 242
    label "Mot&#322;awa"
  ]
  node [
    id 243
    label "Wis&#322;oka"
  ]
  node [
    id 244
    label "Bzura"
  ]
  node [
    id 245
    label "Wieprz"
  ]
  node [
    id 246
    label "Nida"
  ]
  node [
    id 247
    label "Wda"
  ]
  node [
    id 248
    label "Dunajec"
  ]
  node [
    id 249
    label "Kamienna"
  ]
  node [
    id 250
    label "strategia"
  ]
  node [
    id 251
    label "wygranie"
  ]
  node [
    id 252
    label "naruszenie"
  ]
  node [
    id 253
    label "remisowy"
  ]
  node [
    id 254
    label "bezbramkowo"
  ]
  node [
    id 255
    label "mistrzostwa"
  ]
  node [
    id 256
    label "przelecie&#263;"
  ]
  node [
    id 257
    label "przebi&#263;"
  ]
  node [
    id 258
    label "podawa&#263;"
  ]
  node [
    id 259
    label "ko&#324;czy&#263;"
  ]
  node [
    id 260
    label "umawia&#263;_si&#281;"
  ]
  node [
    id 261
    label "nagrywa&#263;"
  ]
  node [
    id 262
    label "krzy&#380;"
  ]
  node [
    id 263
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 264
    label "handwriting"
  ]
  node [
    id 265
    label "d&#322;o&#324;"
  ]
  node [
    id 266
    label "gestykulowa&#263;"
  ]
  node [
    id 267
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 268
    label "palec"
  ]
  node [
    id 269
    label "przedrami&#281;"
  ]
  node [
    id 270
    label "cecha"
  ]
  node [
    id 271
    label "hand"
  ]
  node [
    id 272
    label "&#322;okie&#263;"
  ]
  node [
    id 273
    label "hazena"
  ]
  node [
    id 274
    label "nadgarstek"
  ]
  node [
    id 275
    label "graba"
  ]
  node [
    id 276
    label "pracownik"
  ]
  node [
    id 277
    label "r&#261;czyna"
  ]
  node [
    id 278
    label "k&#322;&#261;b"
  ]
  node [
    id 279
    label "chwyta&#263;"
  ]
  node [
    id 280
    label "cmoknonsens"
  ]
  node [
    id 281
    label "pomocnik"
  ]
  node [
    id 282
    label "gestykulowanie"
  ]
  node [
    id 283
    label "chwytanie"
  ]
  node [
    id 284
    label "obietnica"
  ]
  node [
    id 285
    label "spos&#243;b"
  ]
  node [
    id 286
    label "kroki"
  ]
  node [
    id 287
    label "hasta"
  ]
  node [
    id 288
    label "wykroczenie"
  ]
  node [
    id 289
    label "paw"
  ]
  node [
    id 290
    label "rami&#281;"
  ]
  node [
    id 291
    label "foul"
  ]
  node [
    id 292
    label "wygrywa&#263;"
  ]
  node [
    id 293
    label "dru&#380;yna"
  ]
  node [
    id 294
    label "egzemplarz"
  ]
  node [
    id 295
    label "uk&#322;ad_optyczny"
  ]
  node [
    id 296
    label "kamie&#324;_jubilerski"
  ]
  node [
    id 297
    label "kaftan"
  ]
  node [
    id 298
    label "zwyci&#281;stwo"
  ]
  node [
    id 299
    label "naruszy&#263;"
  ]
  node [
    id 300
    label "wygra&#263;"
  ]
  node [
    id 301
    label "lecie&#263;"
  ]
  node [
    id 302
    label "tenis"
  ]
  node [
    id 303
    label "lob"
  ]
  node [
    id 304
    label "przebija&#263;"
  ]
  node [
    id 305
    label "sko&#324;czy&#263;"
  ]
  node [
    id 306
    label "poda&#263;"
  ]
  node [
    id 307
    label "nagra&#263;"
  ]
  node [
    id 308
    label "um&#243;wi&#263;_si&#281;"
  ]
  node [
    id 309
    label "dig"
  ]
  node [
    id 310
    label "porazi&#263;"
  ]
  node [
    id 311
    label "uderzy&#263;"
  ]
  node [
    id 312
    label "liga"
  ]
  node [
    id 313
    label "zabudowa&#263;"
  ]
  node [
    id 314
    label "uciszy&#263;"
  ]
  node [
    id 315
    label "wmurowa&#263;"
  ]
  node [
    id 316
    label "utworzy&#263;"
  ]
  node [
    id 317
    label "rozegra&#263;"
  ]
  node [
    id 318
    label "zamkn&#261;&#263;"
  ]
  node [
    id 319
    label "wygrywanie"
  ]
  node [
    id 320
    label "naruszanie"
  ]
  node [
    id 321
    label "przerzucanie"
  ]
  node [
    id 322
    label "liczba"
  ]
  node [
    id 323
    label "zbi&#243;r"
  ]
  node [
    id 324
    label "rzut_karny"
  ]
  node [
    id 325
    label "obiekt"
  ]
  node [
    id 326
    label "hack"
  ]
  node [
    id 327
    label "spulchnia&#263;"
  ]
  node [
    id 328
    label "chow"
  ]
  node [
    id 329
    label "robi&#263;"
  ]
  node [
    id 330
    label "d&#243;&#322;"
  ]
  node [
    id 331
    label "krytykowa&#263;"
  ]
  node [
    id 332
    label "pora&#380;a&#263;"
  ]
  node [
    id 333
    label "wykopywa&#263;"
  ]
  node [
    id 334
    label "uderza&#263;"
  ]
  node [
    id 335
    label "przeszukiwa&#263;"
  ]
  node [
    id 336
    label "porozumiewanie_si&#281;"
  ]
  node [
    id 337
    label "nagrywanie"
  ]
  node [
    id 338
    label "podawanie"
  ]
  node [
    id 339
    label "ko&#324;czenie"
  ]
  node [
    id 340
    label "zabudowywa&#263;"
  ]
  node [
    id 341
    label "rozgrywa&#263;"
  ]
  node [
    id 342
    label "zamyka&#263;"
  ]
  node [
    id 343
    label "murowa&#263;"
  ]
  node [
    id 344
    label "extraklasa"
  ]
  node [
    id 345
    label "grupa"
  ]
  node [
    id 346
    label "odkopanie"
  ]
  node [
    id 347
    label "przeszukiwanie"
  ]
  node [
    id 348
    label "skopywanie"
  ]
  node [
    id 349
    label "pora&#380;anie"
  ]
  node [
    id 350
    label "rozkopanie_si&#281;"
  ]
  node [
    id 351
    label "rozkopywanie_si&#281;"
  ]
  node [
    id 352
    label "excavation"
  ]
  node [
    id 353
    label "rozkopywanie"
  ]
  node [
    id 354
    label "robienie"
  ]
  node [
    id 355
    label "podkopanie_si&#281;"
  ]
  node [
    id 356
    label "odkopanie_si&#281;"
  ]
  node [
    id 357
    label "wykopanie"
  ]
  node [
    id 358
    label "podkopywanie"
  ]
  node [
    id 359
    label "wkopywanie_si&#281;"
  ]
  node [
    id 360
    label "wykopywanie"
  ]
  node [
    id 361
    label "skopanie"
  ]
  node [
    id 362
    label "pokopanie"
  ]
  node [
    id 363
    label "podkopywanie_si&#281;"
  ]
  node [
    id 364
    label "wkopywanie"
  ]
  node [
    id 365
    label "wkopanie_si&#281;"
  ]
  node [
    id 366
    label "podkopanie"
  ]
  node [
    id 367
    label "krytykowanie"
  ]
  node [
    id 368
    label "uderzanie"
  ]
  node [
    id 369
    label "odkopywanie_si&#281;"
  ]
  node [
    id 370
    label "odbijanie"
  ]
  node [
    id 371
    label "odkopywanie"
  ]
  node [
    id 372
    label "rozkopanie"
  ]
  node [
    id 373
    label "spulchnianie"
  ]
  node [
    id 374
    label "utworzenie"
  ]
  node [
    id 375
    label "zabudowanie"
  ]
  node [
    id 376
    label "zamkni&#281;cie"
  ]
  node [
    id 377
    label "uciszenie"
  ]
  node [
    id 378
    label "rozegranie"
  ]
  node [
    id 379
    label "podanie"
  ]
  node [
    id 380
    label "nagranie"
  ]
  node [
    id 381
    label "zako&#324;czenie"
  ]
  node [
    id 382
    label "dogadanie_si&#281;"
  ]
  node [
    id 383
    label "bezkr&#281;gowiec"
  ]
  node [
    id 384
    label "jama_p&#322;aszczowa"
  ]
  node [
    id 385
    label "mollusk"
  ]
  node [
    id 386
    label "tarka"
  ]
  node [
    id 387
    label "mi&#281;czaki"
  ]
  node [
    id 388
    label "martwo"
  ]
  node [
    id 389
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 390
    label "umarlak"
  ]
  node [
    id 391
    label "bezmy&#347;lny"
  ]
  node [
    id 392
    label "nieumar&#322;y"
  ]
  node [
    id 393
    label "chowanie"
  ]
  node [
    id 394
    label "duch"
  ]
  node [
    id 395
    label "umieranie"
  ]
  node [
    id 396
    label "nieaktualny"
  ]
  node [
    id 397
    label "wiszenie"
  ]
  node [
    id 398
    label "niesprawny"
  ]
  node [
    id 399
    label "umarcie"
  ]
  node [
    id 400
    label "zw&#322;oki"
  ]
  node [
    id 401
    label "obumarcie"
  ]
  node [
    id 402
    label "obumieranie"
  ]
  node [
    id 403
    label "trwa&#322;y"
  ]
  node [
    id 404
    label "dezaktualizowanie"
  ]
  node [
    id 405
    label "niewsp&#243;&#322;czesny"
  ]
  node [
    id 406
    label "nieaktualnie"
  ]
  node [
    id 407
    label "niewa&#380;ny"
  ]
  node [
    id 408
    label "zdezaktualizowanie"
  ]
  node [
    id 409
    label "mocny"
  ]
  node [
    id 410
    label "wieczny"
  ]
  node [
    id 411
    label "umocnienie"
  ]
  node [
    id 412
    label "ustalanie_si&#281;"
  ]
  node [
    id 413
    label "trwale"
  ]
  node [
    id 414
    label "ustalenie_si&#281;"
  ]
  node [
    id 415
    label "sta&#322;y"
  ]
  node [
    id 416
    label "nieruchomy"
  ]
  node [
    id 417
    label "utrwalenie_si&#281;"
  ]
  node [
    id 418
    label "umacnianie"
  ]
  node [
    id 419
    label "utrwalanie_si&#281;"
  ]
  node [
    id 420
    label "ludzko&#347;&#263;"
  ]
  node [
    id 421
    label "asymilowanie"
  ]
  node [
    id 422
    label "wapniak"
  ]
  node [
    id 423
    label "asymilowa&#263;"
  ]
  node [
    id 424
    label "os&#322;abia&#263;"
  ]
  node [
    id 425
    label "posta&#263;"
  ]
  node [
    id 426
    label "hominid"
  ]
  node [
    id 427
    label "podw&#322;adny"
  ]
  node [
    id 428
    label "os&#322;abianie"
  ]
  node [
    id 429
    label "g&#322;owa"
  ]
  node [
    id 430
    label "figura"
  ]
  node [
    id 431
    label "portrecista"
  ]
  node [
    id 432
    label "dwun&#243;g"
  ]
  node [
    id 433
    label "profanum"
  ]
  node [
    id 434
    label "mikrokosmos"
  ]
  node [
    id 435
    label "nasada"
  ]
  node [
    id 436
    label "antropochoria"
  ]
  node [
    id 437
    label "osoba"
  ]
  node [
    id 438
    label "wz&#243;r"
  ]
  node [
    id 439
    label "senior"
  ]
  node [
    id 440
    label "oddzia&#322;ywanie"
  ]
  node [
    id 441
    label "Adam"
  ]
  node [
    id 442
    label "homo_sapiens"
  ]
  node [
    id 443
    label "polifag"
  ]
  node [
    id 444
    label "bezmy&#347;lnie"
  ]
  node [
    id 445
    label "bezkrytyczny"
  ]
  node [
    id 446
    label "bezm&#243;zgi"
  ]
  node [
    id 447
    label "nierozumny"
  ]
  node [
    id 448
    label "bezrefleksyjny"
  ]
  node [
    id 449
    label "nierozwa&#380;ny"
  ]
  node [
    id 450
    label "bezwiedny"
  ]
  node [
    id 451
    label "niesprawnie"
  ]
  node [
    id 452
    label "nieudany"
  ]
  node [
    id 453
    label "niezdrowy"
  ]
  node [
    id 454
    label "niezgrabny"
  ]
  node [
    id 455
    label "chory"
  ]
  node [
    id 456
    label "pu&#347;ciute&#324;ko"
  ]
  node [
    id 457
    label "oboj&#281;tnie"
  ]
  node [
    id 458
    label "niezmiennie"
  ]
  node [
    id 459
    label "nieruchomo"
  ]
  node [
    id 460
    label "pusto"
  ]
  node [
    id 461
    label "przygn&#281;biaj&#261;co"
  ]
  node [
    id 462
    label "cicho"
  ]
  node [
    id 463
    label "apatycznie"
  ]
  node [
    id 464
    label "umieszczanie"
  ]
  node [
    id 465
    label "potrzymanie"
  ]
  node [
    id 466
    label "dochowanie_si&#281;"
  ]
  node [
    id 467
    label "burial"
  ]
  node [
    id 468
    label "gr&#243;b"
  ]
  node [
    id 469
    label "wk&#322;adanie"
  ]
  node [
    id 470
    label "concealment"
  ]
  node [
    id 471
    label "ukrywanie"
  ]
  node [
    id 472
    label "zmar&#322;y"
  ]
  node [
    id 473
    label "sk&#322;adanie"
  ]
  node [
    id 474
    label "opiekowanie_si&#281;"
  ]
  node [
    id 475
    label "zachowywanie"
  ]
  node [
    id 476
    label "education"
  ]
  node [
    id 477
    label "czucie"
  ]
  node [
    id 478
    label "clasp"
  ]
  node [
    id 479
    label "wychowywanie_si&#281;"
  ]
  node [
    id 480
    label "przetrzymywanie"
  ]
  node [
    id 481
    label "boarding"
  ]
  node [
    id 482
    label "niewidoczny"
  ]
  node [
    id 483
    label "hodowanie"
  ]
  node [
    id 484
    label "nekromancja"
  ]
  node [
    id 485
    label "istota_fantastyczna"
  ]
  node [
    id 486
    label "piek&#322;o"
  ]
  node [
    id 487
    label "human_body"
  ]
  node [
    id 488
    label "ofiarowywanie"
  ]
  node [
    id 489
    label "sfera_afektywna"
  ]
  node [
    id 490
    label "Po&#347;wist"
  ]
  node [
    id 491
    label "podekscytowanie"
  ]
  node [
    id 492
    label "deformowanie"
  ]
  node [
    id 493
    label "sumienie"
  ]
  node [
    id 494
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 495
    label "deformowa&#263;"
  ]
  node [
    id 496
    label "osobowo&#347;&#263;"
  ]
  node [
    id 497
    label "psychika"
  ]
  node [
    id 498
    label "zjawa"
  ]
  node [
    id 499
    label "istota_nadprzyrodzona"
  ]
  node [
    id 500
    label "power"
  ]
  node [
    id 501
    label "entity"
  ]
  node [
    id 502
    label "ofiarowywa&#263;"
  ]
  node [
    id 503
    label "oddech"
  ]
  node [
    id 504
    label "seksualno&#347;&#263;"
  ]
  node [
    id 505
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 506
    label "byt"
  ]
  node [
    id 507
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 508
    label "ego"
  ]
  node [
    id 509
    label "ofiarowanie"
  ]
  node [
    id 510
    label "kompleksja"
  ]
  node [
    id 511
    label "charakter"
  ]
  node [
    id 512
    label "fizjonomia"
  ]
  node [
    id 513
    label "kompleks"
  ]
  node [
    id 514
    label "shape"
  ]
  node [
    id 515
    label "zapalno&#347;&#263;"
  ]
  node [
    id 516
    label "T&#281;sknica"
  ]
  node [
    id 517
    label "ofiarowa&#263;"
  ]
  node [
    id 518
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 519
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 520
    label "passion"
  ]
  node [
    id 521
    label "necrobiosis"
  ]
  node [
    id 522
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 523
    label "stawanie_si&#281;"
  ]
  node [
    id 524
    label "sag"
  ]
  node [
    id 525
    label "powieszenie"
  ]
  node [
    id 526
    label "trwanie"
  ]
  node [
    id 527
    label "majtanie_si&#281;"
  ]
  node [
    id 528
    label "unoszenie_si&#281;"
  ]
  node [
    id 529
    label "dyndanie"
  ]
  node [
    id 530
    label "odumarcie"
  ]
  node [
    id 531
    label "przestanie"
  ]
  node [
    id 532
    label "dysponowanie_si&#281;"
  ]
  node [
    id 533
    label "&#380;ycie"
  ]
  node [
    id 534
    label "pomarcie"
  ]
  node [
    id 535
    label "die"
  ]
  node [
    id 536
    label "sko&#324;czenie"
  ]
  node [
    id 537
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 538
    label "zdechni&#281;cie"
  ]
  node [
    id 539
    label "zabicie"
  ]
  node [
    id 540
    label "korkowanie"
  ]
  node [
    id 541
    label "death"
  ]
  node [
    id 542
    label "&#347;mier&#263;"
  ]
  node [
    id 543
    label "zabijanie"
  ]
  node [
    id 544
    label "przestawanie"
  ]
  node [
    id 545
    label "odumieranie"
  ]
  node [
    id 546
    label "zdychanie"
  ]
  node [
    id 547
    label "stan"
  ]
  node [
    id 548
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 549
    label "zanikanie"
  ]
  node [
    id 550
    label "nieuleczalnie_chory"
  ]
  node [
    id 551
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 552
    label "necrosis"
  ]
  node [
    id 553
    label "stanie_si&#281;"
  ]
  node [
    id 554
    label "posusz"
  ]
  node [
    id 555
    label "ekshumowanie"
  ]
  node [
    id 556
    label "pochowanie"
  ]
  node [
    id 557
    label "zabalsamowanie"
  ]
  node [
    id 558
    label "kremacja"
  ]
  node [
    id 559
    label "pijany"
  ]
  node [
    id 560
    label "zm&#281;czony"
  ]
  node [
    id 561
    label "pochowa&#263;"
  ]
  node [
    id 562
    label "balsamowa&#263;"
  ]
  node [
    id 563
    label "tanatoplastyka"
  ]
  node [
    id 564
    label "ekshumowa&#263;"
  ]
  node [
    id 565
    label "cia&#322;o"
  ]
  node [
    id 566
    label "tanatoplastyk"
  ]
  node [
    id 567
    label "sekcja"
  ]
  node [
    id 568
    label "balsamowanie"
  ]
  node [
    id 569
    label "zabalsamowa&#263;"
  ]
  node [
    id 570
    label "pogrzeb"
  ]
  node [
    id 571
    label "nadgni&#322;y"
  ]
  node [
    id 572
    label "o&#380;ywieniec"
  ]
  node [
    id 573
    label "zajawka"
  ]
  node [
    id 574
    label "emocja"
  ]
  node [
    id 575
    label "oskoma"
  ]
  node [
    id 576
    label "mniemanie"
  ]
  node [
    id 577
    label "inclination"
  ]
  node [
    id 578
    label "wish"
  ]
  node [
    id 579
    label "treatment"
  ]
  node [
    id 580
    label "pogl&#261;d"
  ]
  node [
    id 581
    label "my&#347;lenie"
  ]
  node [
    id 582
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 583
    label "ogrom"
  ]
  node [
    id 584
    label "iskrzy&#263;"
  ]
  node [
    id 585
    label "d&#322;awi&#263;"
  ]
  node [
    id 586
    label "ostygn&#261;&#263;"
  ]
  node [
    id 587
    label "stygn&#261;&#263;"
  ]
  node [
    id 588
    label "temperatura"
  ]
  node [
    id 589
    label "wpa&#347;&#263;"
  ]
  node [
    id 590
    label "afekt"
  ]
  node [
    id 591
    label "wpada&#263;"
  ]
  node [
    id 592
    label "ch&#281;&#263;"
  ]
  node [
    id 593
    label "smak"
  ]
  node [
    id 594
    label "streszczenie"
  ]
  node [
    id 595
    label "harbinger"
  ]
  node [
    id 596
    label "zapowied&#378;"
  ]
  node [
    id 597
    label "zami&#322;owanie"
  ]
  node [
    id 598
    label "czasopismo"
  ]
  node [
    id 599
    label "reklama"
  ]
  node [
    id 600
    label "gadka"
  ]
  node [
    id 601
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 602
    label "olbrzymi"
  ]
  node [
    id 603
    label "niezrozumia&#322;y"
  ]
  node [
    id 604
    label "tajemniczy"
  ]
  node [
    id 605
    label "niepoj&#281;cie"
  ]
  node [
    id 606
    label "ciekawy"
  ]
  node [
    id 607
    label "nieznany"
  ]
  node [
    id 608
    label "intryguj&#261;cy"
  ]
  node [
    id 609
    label "nastrojowy"
  ]
  node [
    id 610
    label "niejednoznaczny"
  ]
  node [
    id 611
    label "dziwny"
  ]
  node [
    id 612
    label "tajemniczo"
  ]
  node [
    id 613
    label "skryty"
  ]
  node [
    id 614
    label "niewyja&#347;niony"
  ]
  node [
    id 615
    label "skomplikowanie_si&#281;"
  ]
  node [
    id 616
    label "niezrozumiale"
  ]
  node [
    id 617
    label "nieprzyst&#281;pny"
  ]
  node [
    id 618
    label "komplikowanie_si&#281;"
  ]
  node [
    id 619
    label "nieuzasadniony"
  ]
  node [
    id 620
    label "powik&#322;anie"
  ]
  node [
    id 621
    label "komplikowanie"
  ]
  node [
    id 622
    label "jebitny"
  ]
  node [
    id 623
    label "olbrzymio"
  ]
  node [
    id 624
    label "ogromnie"
  ]
  node [
    id 625
    label "faza"
  ]
  node [
    id 626
    label "nizina"
  ]
  node [
    id 627
    label "zjawisko"
  ]
  node [
    id 628
    label "depression"
  ]
  node [
    id 629
    label "Wybrze&#380;e_Koromandelskie"
  ]
  node [
    id 630
    label "l&#261;d"
  ]
  node [
    id 631
    label "obszar"
  ]
  node [
    id 632
    label "Pampa"
  ]
  node [
    id 633
    label "Nizina_Nadwi&#347;la&#324;ska"
  ]
  node [
    id 634
    label "proces"
  ]
  node [
    id 635
    label "boski"
  ]
  node [
    id 636
    label "krajobraz"
  ]
  node [
    id 637
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 638
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 639
    label "przywidzenie"
  ]
  node [
    id 640
    label "presence"
  ]
  node [
    id 641
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 642
    label "cykl_astronomiczny"
  ]
  node [
    id 643
    label "coil"
  ]
  node [
    id 644
    label "fotoelement"
  ]
  node [
    id 645
    label "komutowanie"
  ]
  node [
    id 646
    label "stan_skupienia"
  ]
  node [
    id 647
    label "nastr&#243;j"
  ]
  node [
    id 648
    label "przerywacz"
  ]
  node [
    id 649
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 650
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 651
    label "kraw&#281;d&#378;"
  ]
  node [
    id 652
    label "obsesja"
  ]
  node [
    id 653
    label "dw&#243;jnik"
  ]
  node [
    id 654
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 655
    label "okres"
  ]
  node [
    id 656
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 657
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 658
    label "przew&#243;d"
  ]
  node [
    id 659
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 660
    label "czas"
  ]
  node [
    id 661
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 662
    label "obw&#243;d"
  ]
  node [
    id 663
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 664
    label "degree"
  ]
  node [
    id 665
    label "komutowa&#263;"
  ]
  node [
    id 666
    label "po&#322;o&#380;enie"
  ]
  node [
    id 667
    label "jako&#347;&#263;"
  ]
  node [
    id 668
    label "p&#322;aszczyzna"
  ]
  node [
    id 669
    label "punkt_widzenia"
  ]
  node [
    id 670
    label "kierunek"
  ]
  node [
    id 671
    label "wyk&#322;adnik"
  ]
  node [
    id 672
    label "szczebel"
  ]
  node [
    id 673
    label "budynek"
  ]
  node [
    id 674
    label "wysoko&#347;&#263;"
  ]
  node [
    id 675
    label "ranga"
  ]
  node [
    id 676
    label "energia"
  ]
  node [
    id 677
    label "parametr"
  ]
  node [
    id 678
    label "rozwi&#261;zanie"
  ]
  node [
    id 679
    label "wojsko"
  ]
  node [
    id 680
    label "wuchta"
  ]
  node [
    id 681
    label "zaleta"
  ]
  node [
    id 682
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 683
    label "moment_si&#322;y"
  ]
  node [
    id 684
    label "mn&#243;stwo"
  ]
  node [
    id 685
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 686
    label "zdolno&#347;&#263;"
  ]
  node [
    id 687
    label "capacity"
  ]
  node [
    id 688
    label "magnitude"
  ]
  node [
    id 689
    label "potencja"
  ]
  node [
    id 690
    label "przemoc"
  ]
  node [
    id 691
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 692
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 693
    label "emitowa&#263;"
  ]
  node [
    id 694
    label "egzergia"
  ]
  node [
    id 695
    label "kwant_energii"
  ]
  node [
    id 696
    label "szwung"
  ]
  node [
    id 697
    label "emitowanie"
  ]
  node [
    id 698
    label "energy"
  ]
  node [
    id 699
    label "wymiar"
  ]
  node [
    id 700
    label "zmienna"
  ]
  node [
    id 701
    label "charakterystyka"
  ]
  node [
    id 702
    label "wielko&#347;&#263;"
  ]
  node [
    id 703
    label "po&#322;&#243;g"
  ]
  node [
    id 704
    label "spe&#322;nienie"
  ]
  node [
    id 705
    label "dula"
  ]
  node [
    id 706
    label "usuni&#281;cie"
  ]
  node [
    id 707
    label "wymy&#347;lenie"
  ]
  node [
    id 708
    label "po&#322;o&#380;na"
  ]
  node [
    id 709
    label "wyj&#347;cie"
  ]
  node [
    id 710
    label "uniewa&#380;nienie"
  ]
  node [
    id 711
    label "proces_fizjologiczny"
  ]
  node [
    id 712
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 713
    label "pomys&#322;"
  ]
  node [
    id 714
    label "szok_poporodowy"
  ]
  node [
    id 715
    label "event"
  ]
  node [
    id 716
    label "marc&#243;wka"
  ]
  node [
    id 717
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 718
    label "birth"
  ]
  node [
    id 719
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 720
    label "wynik"
  ]
  node [
    id 721
    label "patologia"
  ]
  node [
    id 722
    label "agresja"
  ]
  node [
    id 723
    label "przewaga"
  ]
  node [
    id 724
    label "drastyczny"
  ]
  node [
    id 725
    label "ilo&#347;&#263;"
  ]
  node [
    id 726
    label "enormousness"
  ]
  node [
    id 727
    label "posiada&#263;"
  ]
  node [
    id 728
    label "potencja&#322;"
  ]
  node [
    id 729
    label "zapomnienie"
  ]
  node [
    id 730
    label "zapomina&#263;"
  ]
  node [
    id 731
    label "zapominanie"
  ]
  node [
    id 732
    label "ability"
  ]
  node [
    id 733
    label "obliczeniowo"
  ]
  node [
    id 734
    label "zapomnie&#263;"
  ]
  node [
    id 735
    label "facylitacja"
  ]
  node [
    id 736
    label "warto&#347;&#263;"
  ]
  node [
    id 737
    label "zrewaluowa&#263;"
  ]
  node [
    id 738
    label "rewaluowanie"
  ]
  node [
    id 739
    label "korzy&#347;&#263;"
  ]
  node [
    id 740
    label "strona"
  ]
  node [
    id 741
    label "rewaluowa&#263;"
  ]
  node [
    id 742
    label "wabik"
  ]
  node [
    id 743
    label "zrewaluowanie"
  ]
  node [
    id 744
    label "m&#322;ot"
  ]
  node [
    id 745
    label "znak"
  ]
  node [
    id 746
    label "drzewo"
  ]
  node [
    id 747
    label "pr&#243;ba"
  ]
  node [
    id 748
    label "attribute"
  ]
  node [
    id 749
    label "marka"
  ]
  node [
    id 750
    label "moc"
  ]
  node [
    id 751
    label "potency"
  ]
  node [
    id 752
    label "tomizm"
  ]
  node [
    id 753
    label "wydolno&#347;&#263;"
  ]
  node [
    id 754
    label "poj&#281;cie"
  ]
  node [
    id 755
    label "arystotelizm"
  ]
  node [
    id 756
    label "gotowo&#347;&#263;"
  ]
  node [
    id 757
    label "zrejterowanie"
  ]
  node [
    id 758
    label "zmobilizowa&#263;"
  ]
  node [
    id 759
    label "przedmiot"
  ]
  node [
    id 760
    label "dezerter"
  ]
  node [
    id 761
    label "oddzia&#322;_karny"
  ]
  node [
    id 762
    label "rezerwa"
  ]
  node [
    id 763
    label "tabor"
  ]
  node [
    id 764
    label "wermacht"
  ]
  node [
    id 765
    label "cofni&#281;cie"
  ]
  node [
    id 766
    label "fala"
  ]
  node [
    id 767
    label "struktura"
  ]
  node [
    id 768
    label "szko&#322;a"
  ]
  node [
    id 769
    label "korpus"
  ]
  node [
    id 770
    label "soldateska"
  ]
  node [
    id 771
    label "ods&#322;ugiwanie"
  ]
  node [
    id 772
    label "werbowanie_si&#281;"
  ]
  node [
    id 773
    label "zdemobilizowanie"
  ]
  node [
    id 774
    label "oddzia&#322;"
  ]
  node [
    id 775
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 776
    label "s&#322;u&#380;ba"
  ]
  node [
    id 777
    label "or&#281;&#380;"
  ]
  node [
    id 778
    label "Legia_Cudzoziemska"
  ]
  node [
    id 779
    label "Armia_Czerwona"
  ]
  node [
    id 780
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 781
    label "rejterowanie"
  ]
  node [
    id 782
    label "Czerwona_Gwardia"
  ]
  node [
    id 783
    label "zrejterowa&#263;"
  ]
  node [
    id 784
    label "sztabslekarz"
  ]
  node [
    id 785
    label "zmobilizowanie"
  ]
  node [
    id 786
    label "wojo"
  ]
  node [
    id 787
    label "pospolite_ruszenie"
  ]
  node [
    id 788
    label "Eurokorpus"
  ]
  node [
    id 789
    label "mobilizowanie"
  ]
  node [
    id 790
    label "rejterowa&#263;"
  ]
  node [
    id 791
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 792
    label "mobilizowa&#263;"
  ]
  node [
    id 793
    label "Armia_Krajowa"
  ]
  node [
    id 794
    label "dryl"
  ]
  node [
    id 795
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 796
    label "petarda"
  ]
  node [
    id 797
    label "pozycja"
  ]
  node [
    id 798
    label "zdemobilizowa&#263;"
  ]
  node [
    id 799
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 800
    label "chodzi&#263;"
  ]
  node [
    id 801
    label "pace"
  ]
  node [
    id 802
    label "mie&#263;_miejsce"
  ]
  node [
    id 803
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 804
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 805
    label "p&#322;ywa&#263;"
  ]
  node [
    id 806
    label "run"
  ]
  node [
    id 807
    label "bangla&#263;"
  ]
  node [
    id 808
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 809
    label "przebiega&#263;"
  ]
  node [
    id 810
    label "wk&#322;ada&#263;"
  ]
  node [
    id 811
    label "proceed"
  ]
  node [
    id 812
    label "by&#263;"
  ]
  node [
    id 813
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 814
    label "carry"
  ]
  node [
    id 815
    label "bywa&#263;"
  ]
  node [
    id 816
    label "dziama&#263;"
  ]
  node [
    id 817
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 818
    label "stara&#263;_si&#281;"
  ]
  node [
    id 819
    label "para"
  ]
  node [
    id 820
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 821
    label "str&#243;j"
  ]
  node [
    id 822
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 823
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 824
    label "krok"
  ]
  node [
    id 825
    label "tryb"
  ]
  node [
    id 826
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 827
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 828
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 829
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 830
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 831
    label "continue"
  ]
  node [
    id 832
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 833
    label "sznurowanie"
  ]
  node [
    id 834
    label "odrobina"
  ]
  node [
    id 835
    label "skutek"
  ]
  node [
    id 836
    label "sznurowa&#263;"
  ]
  node [
    id 837
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 838
    label "odcisk"
  ]
  node [
    id 839
    label "wp&#322;yw"
  ]
  node [
    id 840
    label "dash"
  ]
  node [
    id 841
    label "grain"
  ]
  node [
    id 842
    label "intensywno&#347;&#263;"
  ]
  node [
    id 843
    label "reszta"
  ]
  node [
    id 844
    label "trace"
  ]
  node [
    id 845
    label "&#347;wiadectwo"
  ]
  node [
    id 846
    label "rezultat"
  ]
  node [
    id 847
    label "zmiana"
  ]
  node [
    id 848
    label "rozrost"
  ]
  node [
    id 849
    label "zgrubienie"
  ]
  node [
    id 850
    label "kwota"
  ]
  node [
    id 851
    label "lobbysta"
  ]
  node [
    id 852
    label "doch&#243;d_narodowy"
  ]
  node [
    id 853
    label "biegni&#281;cie"
  ]
  node [
    id 854
    label "wi&#261;zanie"
  ]
  node [
    id 855
    label "zawi&#261;zywanie"
  ]
  node [
    id 856
    label "lace"
  ]
  node [
    id 857
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 858
    label "biec"
  ]
  node [
    id 859
    label "sk&#322;ada&#263;"
  ]
  node [
    id 860
    label "bind"
  ]
  node [
    id 861
    label "wi&#261;za&#263;"
  ]
  node [
    id 862
    label "przestarza&#322;y"
  ]
  node [
    id 863
    label "zmarzni&#281;ty"
  ]
  node [
    id 864
    label "zastyg&#322;y"
  ]
  node [
    id 865
    label "stwardnia&#322;y"
  ]
  node [
    id 866
    label "kostnienie"
  ]
  node [
    id 867
    label "surowy"
  ]
  node [
    id 868
    label "twardy"
  ]
  node [
    id 869
    label "marzni&#281;cie"
  ]
  node [
    id 870
    label "wymro&#380;enie"
  ]
  node [
    id 871
    label "wymra&#380;anie"
  ]
  node [
    id 872
    label "wymarzni&#281;cie"
  ]
  node [
    id 873
    label "zmarzni&#281;cie"
  ]
  node [
    id 874
    label "wymarzanie"
  ]
  node [
    id 875
    label "wymarzn&#261;&#263;"
  ]
  node [
    id 876
    label "wymarza&#263;"
  ]
  node [
    id 877
    label "zimny"
  ]
  node [
    id 878
    label "zestarzenie_si&#281;"
  ]
  node [
    id 879
    label "starzenie_si&#281;"
  ]
  node [
    id 880
    label "archaicznie"
  ]
  node [
    id 881
    label "zgrzybienie"
  ]
  node [
    id 882
    label "niedzisiejszy"
  ]
  node [
    id 883
    label "staro&#347;wiecki"
  ]
  node [
    id 884
    label "przestarzale"
  ]
  node [
    id 885
    label "stary"
  ]
  node [
    id 886
    label "sztywnienie"
  ]
  node [
    id 887
    label "tkanka_kostna"
  ]
  node [
    id 888
    label "ossification"
  ]
  node [
    id 889
    label "utulenie"
  ]
  node [
    id 890
    label "pediatra"
  ]
  node [
    id 891
    label "dzieciak"
  ]
  node [
    id 892
    label "utulanie"
  ]
  node [
    id 893
    label "dzieciarnia"
  ]
  node [
    id 894
    label "niepe&#322;noletni"
  ]
  node [
    id 895
    label "organizm"
  ]
  node [
    id 896
    label "utula&#263;"
  ]
  node [
    id 897
    label "cz&#322;owieczek"
  ]
  node [
    id 898
    label "fledgling"
  ]
  node [
    id 899
    label "zwierz&#281;"
  ]
  node [
    id 900
    label "utuli&#263;"
  ]
  node [
    id 901
    label "m&#322;odzik"
  ]
  node [
    id 902
    label "pedofil"
  ]
  node [
    id 903
    label "m&#322;odziak"
  ]
  node [
    id 904
    label "potomek"
  ]
  node [
    id 905
    label "entliczek-pentliczek"
  ]
  node [
    id 906
    label "potomstwo"
  ]
  node [
    id 907
    label "sraluch"
  ]
  node [
    id 908
    label "czeladka"
  ]
  node [
    id 909
    label "dzietno&#347;&#263;"
  ]
  node [
    id 910
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 911
    label "bawienie_si&#281;"
  ]
  node [
    id 912
    label "pomiot"
  ]
  node [
    id 913
    label "kinderbal"
  ]
  node [
    id 914
    label "krewny"
  ]
  node [
    id 915
    label "ma&#322;oletny"
  ]
  node [
    id 916
    label "m&#322;ody"
  ]
  node [
    id 917
    label "odwadnia&#263;"
  ]
  node [
    id 918
    label "przyswoi&#263;"
  ]
  node [
    id 919
    label "sk&#243;ra"
  ]
  node [
    id 920
    label "odwodni&#263;"
  ]
  node [
    id 921
    label "ewoluowanie"
  ]
  node [
    id 922
    label "staw"
  ]
  node [
    id 923
    label "ow&#322;osienie"
  ]
  node [
    id 924
    label "unerwienie"
  ]
  node [
    id 925
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 926
    label "reakcja"
  ]
  node [
    id 927
    label "wyewoluowanie"
  ]
  node [
    id 928
    label "przyswajanie"
  ]
  node [
    id 929
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 930
    label "wyewoluowa&#263;"
  ]
  node [
    id 931
    label "miejsce"
  ]
  node [
    id 932
    label "biorytm"
  ]
  node [
    id 933
    label "ewoluowa&#263;"
  ]
  node [
    id 934
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 935
    label "otworzy&#263;"
  ]
  node [
    id 936
    label "otwiera&#263;"
  ]
  node [
    id 937
    label "czynnik_biotyczny"
  ]
  node [
    id 938
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 939
    label "otworzenie"
  ]
  node [
    id 940
    label "otwieranie"
  ]
  node [
    id 941
    label "individual"
  ]
  node [
    id 942
    label "szkielet"
  ]
  node [
    id 943
    label "ty&#322;"
  ]
  node [
    id 944
    label "przyswaja&#263;"
  ]
  node [
    id 945
    label "przyswojenie"
  ]
  node [
    id 946
    label "odwadnianie"
  ]
  node [
    id 947
    label "odwodnienie"
  ]
  node [
    id 948
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 949
    label "prz&#243;d"
  ]
  node [
    id 950
    label "uk&#322;ad"
  ]
  node [
    id 951
    label "l&#281;d&#378;wie"
  ]
  node [
    id 952
    label "degenerat"
  ]
  node [
    id 953
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 954
    label "zwyrol"
  ]
  node [
    id 955
    label "czerniak"
  ]
  node [
    id 956
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 957
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 958
    label "paszcza"
  ]
  node [
    id 959
    label "popapraniec"
  ]
  node [
    id 960
    label "skuba&#263;"
  ]
  node [
    id 961
    label "skubanie"
  ]
  node [
    id 962
    label "skubni&#281;cie"
  ]
  node [
    id 963
    label "zwierz&#281;ta"
  ]
  node [
    id 964
    label "fukni&#281;cie"
  ]
  node [
    id 965
    label "farba"
  ]
  node [
    id 966
    label "fukanie"
  ]
  node [
    id 967
    label "gad"
  ]
  node [
    id 968
    label "siedzie&#263;"
  ]
  node [
    id 969
    label "oswaja&#263;"
  ]
  node [
    id 970
    label "tresowa&#263;"
  ]
  node [
    id 971
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 972
    label "poligamia"
  ]
  node [
    id 973
    label "oz&#243;r"
  ]
  node [
    id 974
    label "skubn&#261;&#263;"
  ]
  node [
    id 975
    label "wios&#322;owa&#263;"
  ]
  node [
    id 976
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 977
    label "le&#380;enie"
  ]
  node [
    id 978
    label "niecz&#322;owiek"
  ]
  node [
    id 979
    label "wios&#322;owanie"
  ]
  node [
    id 980
    label "napasienie_si&#281;"
  ]
  node [
    id 981
    label "wiwarium"
  ]
  node [
    id 982
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 983
    label "animalista"
  ]
  node [
    id 984
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 985
    label "budowa"
  ]
  node [
    id 986
    label "hodowla"
  ]
  node [
    id 987
    label "pasienie_si&#281;"
  ]
  node [
    id 988
    label "sodomita"
  ]
  node [
    id 989
    label "monogamia"
  ]
  node [
    id 990
    label "przyssawka"
  ]
  node [
    id 991
    label "zachowanie"
  ]
  node [
    id 992
    label "budowa_cia&#322;a"
  ]
  node [
    id 993
    label "okrutnik"
  ]
  node [
    id 994
    label "grzbiet"
  ]
  node [
    id 995
    label "weterynarz"
  ]
  node [
    id 996
    label "&#322;eb"
  ]
  node [
    id 997
    label "wylinka"
  ]
  node [
    id 998
    label "bestia"
  ]
  node [
    id 999
    label "poskramia&#263;"
  ]
  node [
    id 1000
    label "fauna"
  ]
  node [
    id 1001
    label "treser"
  ]
  node [
    id 1002
    label "siedzenie"
  ]
  node [
    id 1003
    label "le&#380;e&#263;"
  ]
  node [
    id 1004
    label "uspokojenie"
  ]
  node [
    id 1005
    label "utulenie_si&#281;"
  ]
  node [
    id 1006
    label "u&#347;pienie"
  ]
  node [
    id 1007
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 1008
    label "uspokoi&#263;"
  ]
  node [
    id 1009
    label "utulanie_si&#281;"
  ]
  node [
    id 1010
    label "usypianie"
  ]
  node [
    id 1011
    label "pocieszanie"
  ]
  node [
    id 1012
    label "uspokajanie"
  ]
  node [
    id 1013
    label "usypia&#263;"
  ]
  node [
    id 1014
    label "uspokaja&#263;"
  ]
  node [
    id 1015
    label "wyliczanka"
  ]
  node [
    id 1016
    label "specjalista"
  ]
  node [
    id 1017
    label "harcerz"
  ]
  node [
    id 1018
    label "ch&#322;opta&#347;"
  ]
  node [
    id 1019
    label "go&#322;ow&#261;s"
  ]
  node [
    id 1020
    label "m&#322;ode"
  ]
  node [
    id 1021
    label "stopie&#324;_harcerski"
  ]
  node [
    id 1022
    label "g&#243;wniarz"
  ]
  node [
    id 1023
    label "beniaminek"
  ]
  node [
    id 1024
    label "dewiant"
  ]
  node [
    id 1025
    label "istotka"
  ]
  node [
    id 1026
    label "bech"
  ]
  node [
    id 1027
    label "dziecinny"
  ]
  node [
    id 1028
    label "naiwniak"
  ]
  node [
    id 1029
    label "otoczy&#263;"
  ]
  node [
    id 1030
    label "muffle"
  ]
  node [
    id 1031
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1032
    label "obdarowa&#263;"
  ]
  node [
    id 1033
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1034
    label "span"
  ]
  node [
    id 1035
    label "zrobi&#263;"
  ]
  node [
    id 1036
    label "admit"
  ]
  node [
    id 1037
    label "spowodowa&#263;"
  ]
  node [
    id 1038
    label "involve"
  ]
  node [
    id 1039
    label "roztoczy&#263;"
  ]
  node [
    id 1040
    label "zacz&#261;&#263;"
  ]
  node [
    id 1041
    label "kom&#243;rka_jajowa"
  ]
  node [
    id 1042
    label "dwa_ognie"
  ]
  node [
    id 1043
    label "rozsadnik"
  ]
  node [
    id 1044
    label "rodzice"
  ]
  node [
    id 1045
    label "staruszka"
  ]
  node [
    id 1046
    label "ro&#347;lina"
  ]
  node [
    id 1047
    label "przyczyna"
  ]
  node [
    id 1048
    label "macocha"
  ]
  node [
    id 1049
    label "matka_zast&#281;pcza"
  ]
  node [
    id 1050
    label "pi&#322;ka_palantowa"
  ]
  node [
    id 1051
    label "samica"
  ]
  node [
    id 1052
    label "matczysko"
  ]
  node [
    id 1053
    label "macierz"
  ]
  node [
    id 1054
    label "Matka_Boska"
  ]
  node [
    id 1055
    label "przodkini"
  ]
  node [
    id 1056
    label "zakonnica"
  ]
  node [
    id 1057
    label "stara"
  ]
  node [
    id 1058
    label "rodzic"
  ]
  node [
    id 1059
    label "owad"
  ]
  node [
    id 1060
    label "zbiorowisko"
  ]
  node [
    id 1061
    label "ro&#347;liny"
  ]
  node [
    id 1062
    label "p&#281;d"
  ]
  node [
    id 1063
    label "wegetowanie"
  ]
  node [
    id 1064
    label "zadziorek"
  ]
  node [
    id 1065
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1066
    label "do&#322;owa&#263;"
  ]
  node [
    id 1067
    label "wegetacja"
  ]
  node [
    id 1068
    label "owoc"
  ]
  node [
    id 1069
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1070
    label "strzyc"
  ]
  node [
    id 1071
    label "w&#322;&#243;kno"
  ]
  node [
    id 1072
    label "g&#322;uszenie"
  ]
  node [
    id 1073
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1074
    label "fitotron"
  ]
  node [
    id 1075
    label "bulwka"
  ]
  node [
    id 1076
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1077
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1078
    label "epiderma"
  ]
  node [
    id 1079
    label "gumoza"
  ]
  node [
    id 1080
    label "strzy&#380;enie"
  ]
  node [
    id 1081
    label "wypotnik"
  ]
  node [
    id 1082
    label "flawonoid"
  ]
  node [
    id 1083
    label "wyro&#347;le"
  ]
  node [
    id 1084
    label "do&#322;owanie"
  ]
  node [
    id 1085
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1086
    label "fitocenoza"
  ]
  node [
    id 1087
    label "fotoautotrof"
  ]
  node [
    id 1088
    label "wegetowa&#263;"
  ]
  node [
    id 1089
    label "pochewka"
  ]
  node [
    id 1090
    label "sok"
  ]
  node [
    id 1091
    label "system_korzeniowy"
  ]
  node [
    id 1092
    label "zawi&#261;zek"
  ]
  node [
    id 1093
    label "wyznawczyni"
  ]
  node [
    id 1094
    label "pingwin"
  ]
  node [
    id 1095
    label "kornet"
  ]
  node [
    id 1096
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1097
    label "czo&#322;&#243;wka"
  ]
  node [
    id 1098
    label "uczestnik"
  ]
  node [
    id 1099
    label "lista_startowa"
  ]
  node [
    id 1100
    label "sportowiec"
  ]
  node [
    id 1101
    label "orygina&#322;"
  ]
  node [
    id 1102
    label "facet"
  ]
  node [
    id 1103
    label "bohater"
  ]
  node [
    id 1104
    label "spryciarz"
  ]
  node [
    id 1105
    label "rozdawa&#263;_karty"
  ]
  node [
    id 1106
    label "samka"
  ]
  node [
    id 1107
    label "p&#322;e&#263;_pi&#281;kna"
  ]
  node [
    id 1108
    label "drogi_rodne"
  ]
  node [
    id 1109
    label "kobieta"
  ]
  node [
    id 1110
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 1111
    label "female"
  ]
  node [
    id 1112
    label "zabrz&#281;czenie"
  ]
  node [
    id 1113
    label "bzyka&#263;"
  ]
  node [
    id 1114
    label "hukni&#281;cie"
  ]
  node [
    id 1115
    label "owady"
  ]
  node [
    id 1116
    label "parabioza"
  ]
  node [
    id 1117
    label "prostoskrzyd&#322;y"
  ]
  node [
    id 1118
    label "skrzyd&#322;o"
  ]
  node [
    id 1119
    label "cierkanie"
  ]
  node [
    id 1120
    label "stawon&#243;g"
  ]
  node [
    id 1121
    label "bzykni&#281;cie"
  ]
  node [
    id 1122
    label "r&#243;&#380;noskrzyd&#322;y"
  ]
  node [
    id 1123
    label "brz&#281;czenie"
  ]
  node [
    id 1124
    label "bzykn&#261;&#263;"
  ]
  node [
    id 1125
    label "aparat_g&#281;bowy"
  ]
  node [
    id 1126
    label "entomofauna"
  ]
  node [
    id 1127
    label "bzykanie"
  ]
  node [
    id 1128
    label "krewna"
  ]
  node [
    id 1129
    label "opiekun"
  ]
  node [
    id 1130
    label "rodzic_chrzestny"
  ]
  node [
    id 1131
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 1132
    label "co&#347;"
  ]
  node [
    id 1133
    label "thing"
  ]
  node [
    id 1134
    label "program"
  ]
  node [
    id 1135
    label "rzecz"
  ]
  node [
    id 1136
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1137
    label "subject"
  ]
  node [
    id 1138
    label "czynnik"
  ]
  node [
    id 1139
    label "matuszka"
  ]
  node [
    id 1140
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 1141
    label "geneza"
  ]
  node [
    id 1142
    label "poci&#261;ganie"
  ]
  node [
    id 1143
    label "pepiniera"
  ]
  node [
    id 1144
    label "roznosiciel"
  ]
  node [
    id 1145
    label "kolebka"
  ]
  node [
    id 1146
    label "las"
  ]
  node [
    id 1147
    label "starzy"
  ]
  node [
    id 1148
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1149
    label "pokolenie"
  ]
  node [
    id 1150
    label "wapniaki"
  ]
  node [
    id 1151
    label "m&#281;&#380;atka"
  ]
  node [
    id 1152
    label "baba"
  ]
  node [
    id 1153
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 1154
    label "&#380;ona"
  ]
  node [
    id 1155
    label "partnerka"
  ]
  node [
    id 1156
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 1157
    label "parametryzacja"
  ]
  node [
    id 1158
    label "pa&#324;stwo"
  ]
  node [
    id 1159
    label "mod"
  ]
  node [
    id 1160
    label "patriota"
  ]
  node [
    id 1161
    label "zabroni&#263;"
  ]
  node [
    id 1162
    label "pull"
  ]
  node [
    id 1163
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 1164
    label "draw"
  ]
  node [
    id 1165
    label "wyuzda&#263;"
  ]
  node [
    id 1166
    label "wzi&#261;&#263;"
  ]
  node [
    id 1167
    label "odsun&#261;&#263;"
  ]
  node [
    id 1168
    label "cenzura"
  ]
  node [
    id 1169
    label "uwolni&#263;"
  ]
  node [
    id 1170
    label "abolicjonista"
  ]
  node [
    id 1171
    label "lift"
  ]
  node [
    id 1172
    label "pom&#243;c"
  ]
  node [
    id 1173
    label "deliver"
  ]
  node [
    id 1174
    label "release"
  ]
  node [
    id 1175
    label "wytworzy&#263;"
  ]
  node [
    id 1176
    label "wzbudzi&#263;"
  ]
  node [
    id 1177
    label "odziedziczy&#263;"
  ]
  node [
    id 1178
    label "ruszy&#263;"
  ]
  node [
    id 1179
    label "take"
  ]
  node [
    id 1180
    label "zaatakowa&#263;"
  ]
  node [
    id 1181
    label "skorzysta&#263;"
  ]
  node [
    id 1182
    label "uciec"
  ]
  node [
    id 1183
    label "receive"
  ]
  node [
    id 1184
    label "nakaza&#263;"
  ]
  node [
    id 1185
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 1186
    label "obskoczy&#263;"
  ]
  node [
    id 1187
    label "bra&#263;"
  ]
  node [
    id 1188
    label "u&#380;y&#263;"
  ]
  node [
    id 1189
    label "get"
  ]
  node [
    id 1190
    label "wyrucha&#263;"
  ]
  node [
    id 1191
    label "World_Health_Organization"
  ]
  node [
    id 1192
    label "wyciupcia&#263;"
  ]
  node [
    id 1193
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 1194
    label "withdraw"
  ]
  node [
    id 1195
    label "wzi&#281;cie"
  ]
  node [
    id 1196
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 1197
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 1198
    label "poczyta&#263;"
  ]
  node [
    id 1199
    label "obj&#261;&#263;"
  ]
  node [
    id 1200
    label "seize"
  ]
  node [
    id 1201
    label "aim"
  ]
  node [
    id 1202
    label "chwyci&#263;"
  ]
  node [
    id 1203
    label "przyj&#261;&#263;"
  ]
  node [
    id 1204
    label "pokona&#263;"
  ]
  node [
    id 1205
    label "arise"
  ]
  node [
    id 1206
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1207
    label "otrzyma&#263;"
  ]
  node [
    id 1208
    label "wej&#347;&#263;"
  ]
  node [
    id 1209
    label "poruszy&#263;"
  ]
  node [
    id 1210
    label "dosta&#263;"
  ]
  node [
    id 1211
    label "jam"
  ]
  node [
    id 1212
    label "zerwa&#263;"
  ]
  node [
    id 1213
    label "kill"
  ]
  node [
    id 1214
    label "post&#261;pi&#263;"
  ]
  node [
    id 1215
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1216
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1217
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1218
    label "zorganizowa&#263;"
  ]
  node [
    id 1219
    label "appoint"
  ]
  node [
    id 1220
    label "wystylizowa&#263;"
  ]
  node [
    id 1221
    label "cause"
  ]
  node [
    id 1222
    label "przerobi&#263;"
  ]
  node [
    id 1223
    label "nabra&#263;"
  ]
  node [
    id 1224
    label "make"
  ]
  node [
    id 1225
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1226
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1227
    label "wydali&#263;"
  ]
  node [
    id 1228
    label "bow_out"
  ]
  node [
    id 1229
    label "przesta&#263;"
  ]
  node [
    id 1230
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 1231
    label "decelerate"
  ]
  node [
    id 1232
    label "oddali&#263;"
  ]
  node [
    id 1233
    label "odw&#322;&#243;czy&#263;"
  ]
  node [
    id 1234
    label "przenie&#347;&#263;"
  ]
  node [
    id 1235
    label "od&#322;o&#380;y&#263;"
  ]
  node [
    id 1236
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 1237
    label "przesun&#261;&#263;"
  ]
  node [
    id 1238
    label "urz&#261;d"
  ]
  node [
    id 1239
    label "drugi_obieg"
  ]
  node [
    id 1240
    label "zdejmowanie"
  ]
  node [
    id 1241
    label "bell_ringer"
  ]
  node [
    id 1242
    label "krytyka"
  ]
  node [
    id 1243
    label "crisscross"
  ]
  node [
    id 1244
    label "p&#243;&#322;kownik"
  ]
  node [
    id 1245
    label "ekskomunikowa&#263;"
  ]
  node [
    id 1246
    label "kontrola"
  ]
  node [
    id 1247
    label "mark"
  ]
  node [
    id 1248
    label "zdejmowa&#263;"
  ]
  node [
    id 1249
    label "zdj&#281;cie"
  ]
  node [
    id 1250
    label "kara"
  ]
  node [
    id 1251
    label "ekskomunikowanie"
  ]
  node [
    id 1252
    label "przeciwnik"
  ]
  node [
    id 1253
    label "znie&#347;&#263;"
  ]
  node [
    id 1254
    label "zniesienie"
  ]
  node [
    id 1255
    label "zwolennik"
  ]
  node [
    id 1256
    label "czarnosk&#243;ry"
  ]
  node [
    id 1257
    label "rozpasa&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 573
  ]
  edge [
    source 5
    target 574
  ]
  edge [
    source 5
    target 575
  ]
  edge [
    source 5
    target 576
  ]
  edge [
    source 5
    target 577
  ]
  edge [
    source 5
    target 578
  ]
  edge [
    source 5
    target 579
  ]
  edge [
    source 5
    target 580
  ]
  edge [
    source 5
    target 581
  ]
  edge [
    source 5
    target 582
  ]
  edge [
    source 5
    target 583
  ]
  edge [
    source 5
    target 584
  ]
  edge [
    source 5
    target 585
  ]
  edge [
    source 5
    target 586
  ]
  edge [
    source 5
    target 587
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 588
  ]
  edge [
    source 5
    target 589
  ]
  edge [
    source 5
    target 590
  ]
  edge [
    source 5
    target 591
  ]
  edge [
    source 5
    target 592
  ]
  edge [
    source 5
    target 593
  ]
  edge [
    source 5
    target 594
  ]
  edge [
    source 5
    target 595
  ]
  edge [
    source 5
    target 596
  ]
  edge [
    source 5
    target 597
  ]
  edge [
    source 5
    target 598
  ]
  edge [
    source 5
    target 599
  ]
  edge [
    source 5
    target 600
  ]
  edge [
    source 5
    target 601
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 757
  ]
  edge [
    source 9
    target 758
  ]
  edge [
    source 9
    target 759
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 824
  ]
  edge [
    source 11
    target 825
  ]
  edge [
    source 11
    target 826
  ]
  edge [
    source 11
    target 827
  ]
  edge [
    source 11
    target 828
  ]
  edge [
    source 11
    target 829
  ]
  edge [
    source 11
    target 830
  ]
  edge [
    source 11
    target 831
  ]
  edge [
    source 11
    target 832
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 325
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 889
  ]
  edge [
    source 14
    target 890
  ]
  edge [
    source 14
    target 891
  ]
  edge [
    source 14
    target 892
  ]
  edge [
    source 14
    target 893
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 894
  ]
  edge [
    source 14
    target 895
  ]
  edge [
    source 14
    target 896
  ]
  edge [
    source 14
    target 897
  ]
  edge [
    source 14
    target 898
  ]
  edge [
    source 14
    target 899
  ]
  edge [
    source 14
    target 900
  ]
  edge [
    source 14
    target 901
  ]
  edge [
    source 14
    target 902
  ]
  edge [
    source 14
    target 903
  ]
  edge [
    source 14
    target 904
  ]
  edge [
    source 14
    target 905
  ]
  edge [
    source 14
    target 906
  ]
  edge [
    source 14
    target 907
  ]
  edge [
    source 14
    target 323
  ]
  edge [
    source 14
    target 908
  ]
  edge [
    source 14
    target 909
  ]
  edge [
    source 14
    target 910
  ]
  edge [
    source 14
    target 911
  ]
  edge [
    source 14
    target 912
  ]
  edge [
    source 14
    target 345
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 913
  ]
  edge [
    source 14
    target 914
  ]
  edge [
    source 14
    target 420
  ]
  edge [
    source 14
    target 421
  ]
  edge [
    source 14
    target 422
  ]
  edge [
    source 14
    target 423
  ]
  edge [
    source 14
    target 424
  ]
  edge [
    source 14
    target 425
  ]
  edge [
    source 14
    target 426
  ]
  edge [
    source 14
    target 427
  ]
  edge [
    source 14
    target 428
  ]
  edge [
    source 14
    target 429
  ]
  edge [
    source 14
    target 430
  ]
  edge [
    source 14
    target 431
  ]
  edge [
    source 14
    target 432
  ]
  edge [
    source 14
    target 433
  ]
  edge [
    source 14
    target 434
  ]
  edge [
    source 14
    target 435
  ]
  edge [
    source 14
    target 394
  ]
  edge [
    source 14
    target 436
  ]
  edge [
    source 14
    target 437
  ]
  edge [
    source 14
    target 438
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 915
  ]
  edge [
    source 14
    target 916
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 917
  ]
  edge [
    source 14
    target 918
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 920
  ]
  edge [
    source 14
    target 921
  ]
  edge [
    source 14
    target 922
  ]
  edge [
    source 14
    target 923
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 14
    target 925
  ]
  edge [
    source 14
    target 926
  ]
  edge [
    source 14
    target 927
  ]
  edge [
    source 14
    target 928
  ]
  edge [
    source 14
    target 929
  ]
  edge [
    source 14
    target 930
  ]
  edge [
    source 14
    target 931
  ]
  edge [
    source 14
    target 932
  ]
  edge [
    source 14
    target 933
  ]
  edge [
    source 14
    target 934
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 935
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 937
  ]
  edge [
    source 14
    target 938
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 940
  ]
  edge [
    source 14
    target 941
  ]
  edge [
    source 14
    target 942
  ]
  edge [
    source 14
    target 943
  ]
  edge [
    source 14
    target 325
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 945
  ]
  edge [
    source 14
    target 946
  ]
  edge [
    source 14
    target 947
  ]
  edge [
    source 14
    target 948
  ]
  edge [
    source 14
    target 879
  ]
  edge [
    source 14
    target 949
  ]
  edge [
    source 14
    target 950
  ]
  edge [
    source 14
    target 588
  ]
  edge [
    source 14
    target 951
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 952
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 14
    target 954
  ]
  edge [
    source 14
    target 955
  ]
  edge [
    source 14
    target 956
  ]
  edge [
    source 14
    target 957
  ]
  edge [
    source 14
    target 958
  ]
  edge [
    source 14
    target 959
  ]
  edge [
    source 14
    target 960
  ]
  edge [
    source 14
    target 961
  ]
  edge [
    source 14
    target 962
  ]
  edge [
    source 14
    target 722
  ]
  edge [
    source 14
    target 963
  ]
  edge [
    source 14
    target 964
  ]
  edge [
    source 14
    target 965
  ]
  edge [
    source 14
    target 966
  ]
  edge [
    source 14
    target 967
  ]
  edge [
    source 14
    target 968
  ]
  edge [
    source 14
    target 969
  ]
  edge [
    source 14
    target 970
  ]
  edge [
    source 14
    target 971
  ]
  edge [
    source 14
    target 972
  ]
  edge [
    source 14
    target 973
  ]
  edge [
    source 14
    target 974
  ]
  edge [
    source 14
    target 975
  ]
  edge [
    source 14
    target 976
  ]
  edge [
    source 14
    target 977
  ]
  edge [
    source 14
    target 978
  ]
  edge [
    source 14
    target 979
  ]
  edge [
    source 14
    target 980
  ]
  edge [
    source 14
    target 981
  ]
  edge [
    source 14
    target 982
  ]
  edge [
    source 14
    target 983
  ]
  edge [
    source 14
    target 984
  ]
  edge [
    source 14
    target 985
  ]
  edge [
    source 14
    target 986
  ]
  edge [
    source 14
    target 987
  ]
  edge [
    source 14
    target 988
  ]
  edge [
    source 14
    target 989
  ]
  edge [
    source 14
    target 990
  ]
  edge [
    source 14
    target 991
  ]
  edge [
    source 14
    target 992
  ]
  edge [
    source 14
    target 993
  ]
  edge [
    source 14
    target 994
  ]
  edge [
    source 14
    target 995
  ]
  edge [
    source 14
    target 996
  ]
  edge [
    source 14
    target 997
  ]
  edge [
    source 14
    target 998
  ]
  edge [
    source 14
    target 999
  ]
  edge [
    source 14
    target 1000
  ]
  edge [
    source 14
    target 1001
  ]
  edge [
    source 14
    target 1002
  ]
  edge [
    source 14
    target 1003
  ]
  edge [
    source 14
    target 1004
  ]
  edge [
    source 14
    target 1005
  ]
  edge [
    source 14
    target 1006
  ]
  edge [
    source 14
    target 1007
  ]
  edge [
    source 14
    target 1008
  ]
  edge [
    source 14
    target 1009
  ]
  edge [
    source 14
    target 1010
  ]
  edge [
    source 14
    target 1011
  ]
  edge [
    source 14
    target 1012
  ]
  edge [
    source 14
    target 1013
  ]
  edge [
    source 14
    target 1014
  ]
  edge [
    source 14
    target 1015
  ]
  edge [
    source 14
    target 1016
  ]
  edge [
    source 14
    target 1017
  ]
  edge [
    source 14
    target 1018
  ]
  edge [
    source 14
    target 231
  ]
  edge [
    source 14
    target 1019
  ]
  edge [
    source 14
    target 1020
  ]
  edge [
    source 14
    target 1021
  ]
  edge [
    source 14
    target 1022
  ]
  edge [
    source 14
    target 1023
  ]
  edge [
    source 14
    target 1024
  ]
  edge [
    source 14
    target 1025
  ]
  edge [
    source 14
    target 1026
  ]
  edge [
    source 14
    target 1027
  ]
  edge [
    source 14
    target 1028
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1029
  ]
  edge [
    source 15
    target 1030
  ]
  edge [
    source 15
    target 1031
  ]
  edge [
    source 15
    target 1032
  ]
  edge [
    source 15
    target 1033
  ]
  edge [
    source 15
    target 1034
  ]
  edge [
    source 15
    target 1035
  ]
  edge [
    source 15
    target 1036
  ]
  edge [
    source 15
    target 1037
  ]
  edge [
    source 15
    target 1038
  ]
  edge [
    source 15
    target 1039
  ]
  edge [
    source 15
    target 1040
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1041
  ]
  edge [
    source 18
    target 1042
  ]
  edge [
    source 18
    target 232
  ]
  edge [
    source 18
    target 1043
  ]
  edge [
    source 18
    target 1044
  ]
  edge [
    source 18
    target 1045
  ]
  edge [
    source 18
    target 1046
  ]
  edge [
    source 18
    target 1047
  ]
  edge [
    source 18
    target 1048
  ]
  edge [
    source 18
    target 1049
  ]
  edge [
    source 18
    target 1050
  ]
  edge [
    source 18
    target 1051
  ]
  edge [
    source 18
    target 231
  ]
  edge [
    source 18
    target 1052
  ]
  edge [
    source 18
    target 1053
  ]
  edge [
    source 18
    target 1054
  ]
  edge [
    source 18
    target 325
  ]
  edge [
    source 18
    target 1055
  ]
  edge [
    source 18
    target 1056
  ]
  edge [
    source 18
    target 1057
  ]
  edge [
    source 18
    target 1058
  ]
  edge [
    source 18
    target 1059
  ]
  edge [
    source 18
    target 1060
  ]
  edge [
    source 18
    target 1061
  ]
  edge [
    source 18
    target 1062
  ]
  edge [
    source 18
    target 1063
  ]
  edge [
    source 18
    target 1064
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 1065
  ]
  edge [
    source 18
    target 1066
  ]
  edge [
    source 18
    target 1067
  ]
  edge [
    source 18
    target 1068
  ]
  edge [
    source 18
    target 1069
  ]
  edge [
    source 18
    target 1070
  ]
  edge [
    source 18
    target 1071
  ]
  edge [
    source 18
    target 1072
  ]
  edge [
    source 18
    target 1073
  ]
  edge [
    source 18
    target 1074
  ]
  edge [
    source 18
    target 1075
  ]
  edge [
    source 18
    target 1076
  ]
  edge [
    source 18
    target 1077
  ]
  edge [
    source 18
    target 1078
  ]
  edge [
    source 18
    target 1079
  ]
  edge [
    source 18
    target 1080
  ]
  edge [
    source 18
    target 1081
  ]
  edge [
    source 18
    target 1082
  ]
  edge [
    source 18
    target 1083
  ]
  edge [
    source 18
    target 1084
  ]
  edge [
    source 18
    target 1085
  ]
  edge [
    source 18
    target 332
  ]
  edge [
    source 18
    target 1086
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 1087
  ]
  edge [
    source 18
    target 550
  ]
  edge [
    source 18
    target 1088
  ]
  edge [
    source 18
    target 1089
  ]
  edge [
    source 18
    target 1090
  ]
  edge [
    source 18
    target 1091
  ]
  edge [
    source 18
    target 1092
  ]
  edge [
    source 18
    target 1093
  ]
  edge [
    source 18
    target 1094
  ]
  edge [
    source 18
    target 1095
  ]
  edge [
    source 18
    target 1096
  ]
  edge [
    source 18
    target 1097
  ]
  edge [
    source 18
    target 1098
  ]
  edge [
    source 18
    target 1099
  ]
  edge [
    source 18
    target 1100
  ]
  edge [
    source 18
    target 1101
  ]
  edge [
    source 18
    target 1102
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 425
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 1103
  ]
  edge [
    source 18
    target 1104
  ]
  edge [
    source 18
    target 1105
  ]
  edge [
    source 18
    target 1106
  ]
  edge [
    source 18
    target 1107
  ]
  edge [
    source 18
    target 1108
  ]
  edge [
    source 18
    target 1109
  ]
  edge [
    source 18
    target 1110
  ]
  edge [
    source 18
    target 1111
  ]
  edge [
    source 18
    target 1112
  ]
  edge [
    source 18
    target 1113
  ]
  edge [
    source 18
    target 1114
  ]
  edge [
    source 18
    target 1115
  ]
  edge [
    source 18
    target 1116
  ]
  edge [
    source 18
    target 1117
  ]
  edge [
    source 18
    target 1118
  ]
  edge [
    source 18
    target 1119
  ]
  edge [
    source 18
    target 1120
  ]
  edge [
    source 18
    target 1121
  ]
  edge [
    source 18
    target 1122
  ]
  edge [
    source 18
    target 1123
  ]
  edge [
    source 18
    target 1124
  ]
  edge [
    source 18
    target 1125
  ]
  edge [
    source 18
    target 1126
  ]
  edge [
    source 18
    target 1127
  ]
  edge [
    source 18
    target 1128
  ]
  edge [
    source 18
    target 1129
  ]
  edge [
    source 18
    target 422
  ]
  edge [
    source 18
    target 1130
  ]
  edge [
    source 18
    target 1131
  ]
  edge [
    source 18
    target 1132
  ]
  edge [
    source 18
    target 673
  ]
  edge [
    source 18
    target 1133
  ]
  edge [
    source 18
    target 754
  ]
  edge [
    source 18
    target 1134
  ]
  edge [
    source 18
    target 1135
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 1136
  ]
  edge [
    source 18
    target 1137
  ]
  edge [
    source 18
    target 1138
  ]
  edge [
    source 18
    target 1139
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 1140
  ]
  edge [
    source 18
    target 1141
  ]
  edge [
    source 18
    target 1142
  ]
  edge [
    source 18
    target 1143
  ]
  edge [
    source 18
    target 1144
  ]
  edge [
    source 18
    target 1145
  ]
  edge [
    source 18
    target 1146
  ]
  edge [
    source 18
    target 1147
  ]
  edge [
    source 18
    target 1148
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 1149
  ]
  edge [
    source 18
    target 1150
  ]
  edge [
    source 18
    target 1151
  ]
  edge [
    source 18
    target 1152
  ]
  edge [
    source 18
    target 1153
  ]
  edge [
    source 18
    target 1154
  ]
  edge [
    source 18
    target 1155
  ]
  edge [
    source 18
    target 1156
  ]
  edge [
    source 18
    target 1157
  ]
  edge [
    source 18
    target 1158
  ]
  edge [
    source 18
    target 1159
  ]
  edge [
    source 18
    target 1160
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1161
  ]
  edge [
    source 19
    target 1162
  ]
  edge [
    source 19
    target 1163
  ]
  edge [
    source 19
    target 1164
  ]
  edge [
    source 19
    target 1165
  ]
  edge [
    source 19
    target 1166
  ]
  edge [
    source 19
    target 1035
  ]
  edge [
    source 19
    target 1167
  ]
  edge [
    source 19
    target 1168
  ]
  edge [
    source 19
    target 1169
  ]
  edge [
    source 19
    target 1170
  ]
  edge [
    source 19
    target 1171
  ]
  edge [
    source 19
    target 1172
  ]
  edge [
    source 19
    target 1173
  ]
  edge [
    source 19
    target 1037
  ]
  edge [
    source 19
    target 1174
  ]
  edge [
    source 19
    target 1175
  ]
  edge [
    source 19
    target 1176
  ]
  edge [
    source 19
    target 1177
  ]
  edge [
    source 19
    target 1178
  ]
  edge [
    source 19
    target 1179
  ]
  edge [
    source 19
    target 1180
  ]
  edge [
    source 19
    target 1181
  ]
  edge [
    source 19
    target 1182
  ]
  edge [
    source 19
    target 1183
  ]
  edge [
    source 19
    target 1184
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 1186
  ]
  edge [
    source 19
    target 1187
  ]
  edge [
    source 19
    target 1188
  ]
  edge [
    source 19
    target 1189
  ]
  edge [
    source 19
    target 1190
  ]
  edge [
    source 19
    target 1191
  ]
  edge [
    source 19
    target 1192
  ]
  edge [
    source 19
    target 300
  ]
  edge [
    source 19
    target 1193
  ]
  edge [
    source 19
    target 1194
  ]
  edge [
    source 19
    target 1195
  ]
  edge [
    source 19
    target 1196
  ]
  edge [
    source 19
    target 1197
  ]
  edge [
    source 19
    target 1198
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 1200
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 1202
  ]
  edge [
    source 19
    target 1203
  ]
  edge [
    source 19
    target 1204
  ]
  edge [
    source 19
    target 1205
  ]
  edge [
    source 19
    target 1206
  ]
  edge [
    source 19
    target 1040
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1208
  ]
  edge [
    source 19
    target 1209
  ]
  edge [
    source 19
    target 1210
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 1212
  ]
  edge [
    source 19
    target 1213
  ]
  edge [
    source 19
    target 1214
  ]
  edge [
    source 19
    target 1215
  ]
  edge [
    source 19
    target 1216
  ]
  edge [
    source 19
    target 1217
  ]
  edge [
    source 19
    target 1218
  ]
  edge [
    source 19
    target 1219
  ]
  edge [
    source 19
    target 1220
  ]
  edge [
    source 19
    target 1221
  ]
  edge [
    source 19
    target 1222
  ]
  edge [
    source 19
    target 1223
  ]
  edge [
    source 19
    target 1224
  ]
  edge [
    source 19
    target 1225
  ]
  edge [
    source 19
    target 1226
  ]
  edge [
    source 19
    target 1227
  ]
  edge [
    source 19
    target 1228
  ]
  edge [
    source 19
    target 1229
  ]
  edge [
    source 19
    target 1230
  ]
  edge [
    source 19
    target 1231
  ]
  edge [
    source 19
    target 1232
  ]
  edge [
    source 19
    target 1233
  ]
  edge [
    source 19
    target 1234
  ]
  edge [
    source 19
    target 1235
  ]
  edge [
    source 19
    target 1236
  ]
  edge [
    source 19
    target 1237
  ]
  edge [
    source 19
    target 1238
  ]
  edge [
    source 19
    target 845
  ]
  edge [
    source 19
    target 634
  ]
  edge [
    source 19
    target 1239
  ]
  edge [
    source 19
    target 1240
  ]
  edge [
    source 19
    target 1241
  ]
  edge [
    source 19
    target 1242
  ]
  edge [
    source 19
    target 1243
  ]
  edge [
    source 19
    target 1244
  ]
  edge [
    source 19
    target 1245
  ]
  edge [
    source 19
    target 1246
  ]
  edge [
    source 19
    target 1247
  ]
  edge [
    source 19
    target 1248
  ]
  edge [
    source 19
    target 627
  ]
  edge [
    source 19
    target 1249
  ]
  edge [
    source 19
    target 1250
  ]
  edge [
    source 19
    target 1251
  ]
  edge [
    source 19
    target 1252
  ]
  edge [
    source 19
    target 1253
  ]
  edge [
    source 19
    target 1254
  ]
  edge [
    source 19
    target 1255
  ]
  edge [
    source 19
    target 1256
  ]
  edge [
    source 19
    target 1257
  ]
]
