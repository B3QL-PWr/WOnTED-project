graph [
  node [
    id 0
    label "pegau"
    origin "text"
  ]
  node [
    id 1
    label "Lipsk"
  ]
  node [
    id 2
    label "lando"
  ]
  node [
    id 3
    label "bia&#322;y"
  ]
  node [
    id 4
    label "Elstera"
  ]
  node [
    id 5
    label "Johann"
  ]
  node [
    id 6
    label "David"
  ]
  node [
    id 7
    label "Heinichen"
  ]
  node [
    id 8
    label "Christian"
  ]
  node [
    id 9
    label "Gottlieb"
  ]
  node [
    id 10
    label "Straus"
  ]
  node [
    id 11
    label "Stanis&#322;awa"
  ]
  node [
    id 12
    label "august"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
]
