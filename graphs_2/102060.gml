graph [
  node [
    id 0
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "serdecznie"
    origin "text"
  ]
  node [
    id 2
    label "gala"
    origin "text"
  ]
  node [
    id 3
    label "pi&#281;tnasta"
    origin "text"
  ]
  node [
    id 4
    label "edycja"
    origin "text"
  ]
  node [
    id 5
    label "mama"
    origin "text"
  ]
  node [
    id 6
    label "forum"
    origin "text"
  ]
  node [
    id 7
    label "pismak"
    origin "text"
  ]
  node [
    id 8
    label "og&#243;lnopolski"
    origin "text"
  ]
  node [
    id 9
    label "konkurs"
    origin "text"
  ]
  node [
    id 10
    label "dla"
    origin "text"
  ]
  node [
    id 11
    label "redakcja"
    origin "text"
  ]
  node [
    id 12
    label "gazetka"
    origin "text"
  ]
  node [
    id 13
    label "szkolny"
    origin "text"
  ]
  node [
    id 14
    label "pod"
    origin "text"
  ]
  node [
    id 15
    label "patronat"
    origin "text"
  ]
  node [
    id 16
    label "pan"
    origin "text"
  ]
  node [
    id 17
    label "katarzyna"
    origin "text"
  ]
  node [
    id 18
    label "hall"
    origin "text"
  ]
  node [
    id 19
    label "minister"
    origin "text"
  ]
  node [
    id 20
    label "edukacja"
    origin "text"
  ]
  node [
    id 21
    label "narodowy"
    origin "text"
  ]
  node [
    id 22
    label "nowa"
    origin "text"
  ]
  node [
    id 23
    label "strona"
    origin "text"
  ]
  node [
    id 24
    label "fundacja"
    origin "text"
  ]
  node [
    id 25
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 26
    label "invite"
  ]
  node [
    id 27
    label "ask"
  ]
  node [
    id 28
    label "oferowa&#263;"
  ]
  node [
    id 29
    label "zach&#281;ca&#263;"
  ]
  node [
    id 30
    label "volunteer"
  ]
  node [
    id 31
    label "szczerze"
  ]
  node [
    id 32
    label "serdeczny"
  ]
  node [
    id 33
    label "mi&#322;o"
  ]
  node [
    id 34
    label "siarczy&#347;cie"
  ]
  node [
    id 35
    label "przyja&#378;nie"
  ]
  node [
    id 36
    label "przychylnie"
  ]
  node [
    id 37
    label "mi&#322;y"
  ]
  node [
    id 38
    label "dobrze"
  ]
  node [
    id 39
    label "pleasantly"
  ]
  node [
    id 40
    label "deliciously"
  ]
  node [
    id 41
    label "przyjemny"
  ]
  node [
    id 42
    label "gratifyingly"
  ]
  node [
    id 43
    label "szczery"
  ]
  node [
    id 44
    label "s&#322;usznie"
  ]
  node [
    id 45
    label "szczero"
  ]
  node [
    id 46
    label "hojnie"
  ]
  node [
    id 47
    label "honestly"
  ]
  node [
    id 48
    label "przekonuj&#261;co"
  ]
  node [
    id 49
    label "outspokenly"
  ]
  node [
    id 50
    label "artlessly"
  ]
  node [
    id 51
    label "uczciwie"
  ]
  node [
    id 52
    label "bluffly"
  ]
  node [
    id 53
    label "dosadnie"
  ]
  node [
    id 54
    label "silnie"
  ]
  node [
    id 55
    label "siarczysty"
  ]
  node [
    id 56
    label "drogi"
  ]
  node [
    id 57
    label "ciep&#322;y"
  ]
  node [
    id 58
    label "&#380;yczliwy"
  ]
  node [
    id 59
    label "jab&#322;ko"
  ]
  node [
    id 60
    label "ceremonia"
  ]
  node [
    id 61
    label "jab&#322;o&#324;_domowa"
  ]
  node [
    id 62
    label "uwaga"
  ]
  node [
    id 63
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 64
    label "dba&#322;o&#347;&#263;"
  ]
  node [
    id 65
    label "klejnot"
  ]
  node [
    id 66
    label "wielopestkowiec"
  ]
  node [
    id 67
    label "regalia"
  ]
  node [
    id 68
    label "sk&#243;ra"
  ]
  node [
    id 69
    label "ogonek"
  ]
  node [
    id 70
    label "owoc"
  ]
  node [
    id 71
    label "godzina"
  ]
  node [
    id 72
    label "time"
  ]
  node [
    id 73
    label "doba"
  ]
  node [
    id 74
    label "p&#243;&#322;godzina"
  ]
  node [
    id 75
    label "jednostka_czasu"
  ]
  node [
    id 76
    label "czas"
  ]
  node [
    id 77
    label "minuta"
  ]
  node [
    id 78
    label "kwadrans"
  ]
  node [
    id 79
    label "egzemplarz"
  ]
  node [
    id 80
    label "impression"
  ]
  node [
    id 81
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 82
    label "odmiana"
  ]
  node [
    id 83
    label "cykl"
  ]
  node [
    id 84
    label "notification"
  ]
  node [
    id 85
    label "zmiana"
  ]
  node [
    id 86
    label "produkcja"
  ]
  node [
    id 87
    label "mutant"
  ]
  node [
    id 88
    label "rewizja"
  ]
  node [
    id 89
    label "gramatyka"
  ]
  node [
    id 90
    label "typ"
  ]
  node [
    id 91
    label "paradygmat"
  ]
  node [
    id 92
    label "jednostka_systematyczna"
  ]
  node [
    id 93
    label "change"
  ]
  node [
    id 94
    label "podgatunek"
  ]
  node [
    id 95
    label "posta&#263;"
  ]
  node [
    id 96
    label "ferment"
  ]
  node [
    id 97
    label "rasa"
  ]
  node [
    id 98
    label "zjawisko"
  ]
  node [
    id 99
    label "impreza"
  ]
  node [
    id 100
    label "realizacja"
  ]
  node [
    id 101
    label "tingel-tangel"
  ]
  node [
    id 102
    label "wydawa&#263;"
  ]
  node [
    id 103
    label "numer"
  ]
  node [
    id 104
    label "monta&#380;"
  ]
  node [
    id 105
    label "wyda&#263;"
  ]
  node [
    id 106
    label "postprodukcja"
  ]
  node [
    id 107
    label "performance"
  ]
  node [
    id 108
    label "fabrication"
  ]
  node [
    id 109
    label "zbi&#243;r"
  ]
  node [
    id 110
    label "product"
  ]
  node [
    id 111
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 112
    label "uzysk"
  ]
  node [
    id 113
    label "rozw&#243;j"
  ]
  node [
    id 114
    label "odtworzenie"
  ]
  node [
    id 115
    label "dorobek"
  ]
  node [
    id 116
    label "kreacja"
  ]
  node [
    id 117
    label "trema"
  ]
  node [
    id 118
    label "creation"
  ]
  node [
    id 119
    label "kooperowa&#263;"
  ]
  node [
    id 120
    label "czynnik_biotyczny"
  ]
  node [
    id 121
    label "wyewoluowanie"
  ]
  node [
    id 122
    label "reakcja"
  ]
  node [
    id 123
    label "individual"
  ]
  node [
    id 124
    label "przyswoi&#263;"
  ]
  node [
    id 125
    label "wytw&#243;r"
  ]
  node [
    id 126
    label "starzenie_si&#281;"
  ]
  node [
    id 127
    label "wyewoluowa&#263;"
  ]
  node [
    id 128
    label "okaz"
  ]
  node [
    id 129
    label "part"
  ]
  node [
    id 130
    label "ewoluowa&#263;"
  ]
  node [
    id 131
    label "przyswojenie"
  ]
  node [
    id 132
    label "ewoluowanie"
  ]
  node [
    id 133
    label "obiekt"
  ]
  node [
    id 134
    label "sztuka"
  ]
  node [
    id 135
    label "agent"
  ]
  node [
    id 136
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 137
    label "przyswaja&#263;"
  ]
  node [
    id 138
    label "nicpo&#324;"
  ]
  node [
    id 139
    label "przyswajanie"
  ]
  node [
    id 140
    label "passage"
  ]
  node [
    id 141
    label "oznaka"
  ]
  node [
    id 142
    label "komplet"
  ]
  node [
    id 143
    label "anatomopatolog"
  ]
  node [
    id 144
    label "zmianka"
  ]
  node [
    id 145
    label "amendment"
  ]
  node [
    id 146
    label "praca"
  ]
  node [
    id 147
    label "odmienianie"
  ]
  node [
    id 148
    label "tura"
  ]
  node [
    id 149
    label "set"
  ]
  node [
    id 150
    label "przebieg"
  ]
  node [
    id 151
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 152
    label "miesi&#261;czka"
  ]
  node [
    id 153
    label "okres"
  ]
  node [
    id 154
    label "owulacja"
  ]
  node [
    id 155
    label "sekwencja"
  ]
  node [
    id 156
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 157
    label "cycle"
  ]
  node [
    id 158
    label "przodkini"
  ]
  node [
    id 159
    label "matka_zast&#281;pcza"
  ]
  node [
    id 160
    label "matczysko"
  ]
  node [
    id 161
    label "rodzice"
  ]
  node [
    id 162
    label "stara"
  ]
  node [
    id 163
    label "macierz"
  ]
  node [
    id 164
    label "rodzic"
  ]
  node [
    id 165
    label "Matka_Boska"
  ]
  node [
    id 166
    label "macocha"
  ]
  node [
    id 167
    label "starzy"
  ]
  node [
    id 168
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 169
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 170
    label "pokolenie"
  ]
  node [
    id 171
    label "wapniaki"
  ]
  node [
    id 172
    label "opiekun"
  ]
  node [
    id 173
    label "wapniak"
  ]
  node [
    id 174
    label "rodzic_chrzestny"
  ]
  node [
    id 175
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 176
    label "krewna"
  ]
  node [
    id 177
    label "matka"
  ]
  node [
    id 178
    label "&#380;ona"
  ]
  node [
    id 179
    label "kobieta"
  ]
  node [
    id 180
    label "partnerka"
  ]
  node [
    id 181
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 182
    label "matuszka"
  ]
  node [
    id 183
    label "parametryzacja"
  ]
  node [
    id 184
    label "pa&#324;stwo"
  ]
  node [
    id 185
    label "poj&#281;cie"
  ]
  node [
    id 186
    label "mod"
  ]
  node [
    id 187
    label "patriota"
  ]
  node [
    id 188
    label "m&#281;&#380;atka"
  ]
  node [
    id 189
    label "grupa_dyskusyjna"
  ]
  node [
    id 190
    label "s&#261;d"
  ]
  node [
    id 191
    label "plac"
  ]
  node [
    id 192
    label "bazylika"
  ]
  node [
    id 193
    label "przestrze&#324;"
  ]
  node [
    id 194
    label "miejsce"
  ]
  node [
    id 195
    label "portal"
  ]
  node [
    id 196
    label "konferencja"
  ]
  node [
    id 197
    label "agora"
  ]
  node [
    id 198
    label "grupa"
  ]
  node [
    id 199
    label "odm&#322;adzanie"
  ]
  node [
    id 200
    label "liga"
  ]
  node [
    id 201
    label "asymilowanie"
  ]
  node [
    id 202
    label "gromada"
  ]
  node [
    id 203
    label "asymilowa&#263;"
  ]
  node [
    id 204
    label "Entuzjastki"
  ]
  node [
    id 205
    label "kompozycja"
  ]
  node [
    id 206
    label "Terranie"
  ]
  node [
    id 207
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 208
    label "category"
  ]
  node [
    id 209
    label "pakiet_klimatyczny"
  ]
  node [
    id 210
    label "oddzia&#322;"
  ]
  node [
    id 211
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 212
    label "cz&#261;steczka"
  ]
  node [
    id 213
    label "stage_set"
  ]
  node [
    id 214
    label "type"
  ]
  node [
    id 215
    label "specgrupa"
  ]
  node [
    id 216
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 217
    label "&#346;wietliki"
  ]
  node [
    id 218
    label "odm&#322;odzenie"
  ]
  node [
    id 219
    label "Eurogrupa"
  ]
  node [
    id 220
    label "odm&#322;adza&#263;"
  ]
  node [
    id 221
    label "formacja_geologiczna"
  ]
  node [
    id 222
    label "harcerze_starsi"
  ]
  node [
    id 223
    label "warunek_lokalowy"
  ]
  node [
    id 224
    label "location"
  ]
  node [
    id 225
    label "status"
  ]
  node [
    id 226
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 227
    label "chwila"
  ]
  node [
    id 228
    label "cia&#322;o"
  ]
  node [
    id 229
    label "cecha"
  ]
  node [
    id 230
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 231
    label "rz&#261;d"
  ]
  node [
    id 232
    label "&#321;ubianka"
  ]
  node [
    id 233
    label "area"
  ]
  node [
    id 234
    label "Majdan"
  ]
  node [
    id 235
    label "pole_bitwy"
  ]
  node [
    id 236
    label "stoisko"
  ]
  node [
    id 237
    label "obszar"
  ]
  node [
    id 238
    label "pierzeja"
  ]
  node [
    id 239
    label "obiekt_handlowy"
  ]
  node [
    id 240
    label "zgromadzenie"
  ]
  node [
    id 241
    label "miasto"
  ]
  node [
    id 242
    label "targowica"
  ]
  node [
    id 243
    label "kram"
  ]
  node [
    id 244
    label "rozdzielanie"
  ]
  node [
    id 245
    label "bezbrze&#380;e"
  ]
  node [
    id 246
    label "punkt"
  ]
  node [
    id 247
    label "czasoprzestrze&#324;"
  ]
  node [
    id 248
    label "niezmierzony"
  ]
  node [
    id 249
    label "przedzielenie"
  ]
  node [
    id 250
    label "nielito&#347;ciwy"
  ]
  node [
    id 251
    label "rozdziela&#263;"
  ]
  node [
    id 252
    label "oktant"
  ]
  node [
    id 253
    label "przedzieli&#263;"
  ]
  node [
    id 254
    label "przestw&#243;r"
  ]
  node [
    id 255
    label "kartka"
  ]
  node [
    id 256
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 257
    label "logowanie"
  ]
  node [
    id 258
    label "plik"
  ]
  node [
    id 259
    label "adres_internetowy"
  ]
  node [
    id 260
    label "linia"
  ]
  node [
    id 261
    label "serwis_internetowy"
  ]
  node [
    id 262
    label "bok"
  ]
  node [
    id 263
    label "skr&#281;canie"
  ]
  node [
    id 264
    label "skr&#281;ca&#263;"
  ]
  node [
    id 265
    label "orientowanie"
  ]
  node [
    id 266
    label "skr&#281;ci&#263;"
  ]
  node [
    id 267
    label "uj&#281;cie"
  ]
  node [
    id 268
    label "zorientowanie"
  ]
  node [
    id 269
    label "ty&#322;"
  ]
  node [
    id 270
    label "fragment"
  ]
  node [
    id 271
    label "layout"
  ]
  node [
    id 272
    label "zorientowa&#263;"
  ]
  node [
    id 273
    label "pagina"
  ]
  node [
    id 274
    label "podmiot"
  ]
  node [
    id 275
    label "g&#243;ra"
  ]
  node [
    id 276
    label "orientowa&#263;"
  ]
  node [
    id 277
    label "voice"
  ]
  node [
    id 278
    label "orientacja"
  ]
  node [
    id 279
    label "prz&#243;d"
  ]
  node [
    id 280
    label "internet"
  ]
  node [
    id 281
    label "powierzchnia"
  ]
  node [
    id 282
    label "forma"
  ]
  node [
    id 283
    label "skr&#281;cenie"
  ]
  node [
    id 284
    label "Ja&#322;ta"
  ]
  node [
    id 285
    label "spotkanie"
  ]
  node [
    id 286
    label "konferencyjka"
  ]
  node [
    id 287
    label "conference"
  ]
  node [
    id 288
    label "grusza_pospolita"
  ]
  node [
    id 289
    label "Poczdam"
  ]
  node [
    id 290
    label "obramienie"
  ]
  node [
    id 291
    label "Onet"
  ]
  node [
    id 292
    label "archiwolta"
  ]
  node [
    id 293
    label "wej&#347;cie"
  ]
  node [
    id 294
    label "hala"
  ]
  node [
    id 295
    label "westwerk"
  ]
  node [
    id 296
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 297
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 298
    label "zesp&#243;&#322;"
  ]
  node [
    id 299
    label "podejrzany"
  ]
  node [
    id 300
    label "s&#261;downictwo"
  ]
  node [
    id 301
    label "system"
  ]
  node [
    id 302
    label "biuro"
  ]
  node [
    id 303
    label "court"
  ]
  node [
    id 304
    label "bronienie"
  ]
  node [
    id 305
    label "urz&#261;d"
  ]
  node [
    id 306
    label "wydarzenie"
  ]
  node [
    id 307
    label "oskar&#380;yciel"
  ]
  node [
    id 308
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 309
    label "skazany"
  ]
  node [
    id 310
    label "post&#281;powanie"
  ]
  node [
    id 311
    label "broni&#263;"
  ]
  node [
    id 312
    label "my&#347;l"
  ]
  node [
    id 313
    label "pods&#261;dny"
  ]
  node [
    id 314
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 315
    label "obrona"
  ]
  node [
    id 316
    label "wypowied&#378;"
  ]
  node [
    id 317
    label "instytucja"
  ]
  node [
    id 318
    label "antylogizm"
  ]
  node [
    id 319
    label "konektyw"
  ]
  node [
    id 320
    label "&#347;wiadek"
  ]
  node [
    id 321
    label "procesowicz"
  ]
  node [
    id 322
    label "gryzipi&#243;rek"
  ]
  node [
    id 323
    label "urz&#281;dnik"
  ]
  node [
    id 324
    label "grafoman"
  ]
  node [
    id 325
    label "dziennikarz_prasowy"
  ]
  node [
    id 326
    label "trybik"
  ]
  node [
    id 327
    label "og&#243;lnokrajowy"
  ]
  node [
    id 328
    label "og&#243;lnopolsko"
  ]
  node [
    id 329
    label "og&#243;lnokrajowo"
  ]
  node [
    id 330
    label "generalny"
  ]
  node [
    id 331
    label "casting"
  ]
  node [
    id 332
    label "nab&#243;r"
  ]
  node [
    id 333
    label "Eurowizja"
  ]
  node [
    id 334
    label "eliminacje"
  ]
  node [
    id 335
    label "emulation"
  ]
  node [
    id 336
    label "Interwizja"
  ]
  node [
    id 337
    label "impra"
  ]
  node [
    id 338
    label "rozrywka"
  ]
  node [
    id 339
    label "przyj&#281;cie"
  ]
  node [
    id 340
    label "okazja"
  ]
  node [
    id 341
    label "party"
  ]
  node [
    id 342
    label "recruitment"
  ]
  node [
    id 343
    label "wyb&#243;r"
  ]
  node [
    id 344
    label "faza"
  ]
  node [
    id 345
    label "runda"
  ]
  node [
    id 346
    label "turniej"
  ]
  node [
    id 347
    label "retirement"
  ]
  node [
    id 348
    label "przes&#322;uchanie"
  ]
  node [
    id 349
    label "w&#281;dkarstwo"
  ]
  node [
    id 350
    label "redaktor"
  ]
  node [
    id 351
    label "radio"
  ]
  node [
    id 352
    label "siedziba"
  ]
  node [
    id 353
    label "composition"
  ]
  node [
    id 354
    label "wydawnictwo"
  ]
  node [
    id 355
    label "redaction"
  ]
  node [
    id 356
    label "tekst"
  ]
  node [
    id 357
    label "telewizja"
  ]
  node [
    id 358
    label "obr&#243;bka"
  ]
  node [
    id 359
    label "Mazowsze"
  ]
  node [
    id 360
    label "whole"
  ]
  node [
    id 361
    label "skupienie"
  ]
  node [
    id 362
    label "The_Beatles"
  ]
  node [
    id 363
    label "zabudowania"
  ]
  node [
    id 364
    label "group"
  ]
  node [
    id 365
    label "zespolik"
  ]
  node [
    id 366
    label "schorzenie"
  ]
  node [
    id 367
    label "ro&#347;lina"
  ]
  node [
    id 368
    label "Depeche_Mode"
  ]
  node [
    id 369
    label "batch"
  ]
  node [
    id 370
    label "miejsce_pracy"
  ]
  node [
    id 371
    label "dzia&#322;_personalny"
  ]
  node [
    id 372
    label "Kreml"
  ]
  node [
    id 373
    label "Bia&#322;y_Dom"
  ]
  node [
    id 374
    label "budynek"
  ]
  node [
    id 375
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 376
    label "sadowisko"
  ]
  node [
    id 377
    label "proces_technologiczny"
  ]
  node [
    id 378
    label "czynno&#347;&#263;"
  ]
  node [
    id 379
    label "proces"
  ]
  node [
    id 380
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 381
    label "cz&#322;owiek"
  ]
  node [
    id 382
    label "bran&#380;owiec"
  ]
  node [
    id 383
    label "edytor"
  ]
  node [
    id 384
    label "debit"
  ]
  node [
    id 385
    label "druk"
  ]
  node [
    id 386
    label "publikacja"
  ]
  node [
    id 387
    label "szata_graficzna"
  ]
  node [
    id 388
    label "firma"
  ]
  node [
    id 389
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 390
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 391
    label "poster"
  ]
  node [
    id 392
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 393
    label "paj&#281;czarz"
  ]
  node [
    id 394
    label "radiola"
  ]
  node [
    id 395
    label "programowiec"
  ]
  node [
    id 396
    label "spot"
  ]
  node [
    id 397
    label "stacja"
  ]
  node [
    id 398
    label "uk&#322;ad"
  ]
  node [
    id 399
    label "odbiornik"
  ]
  node [
    id 400
    label "eliminator"
  ]
  node [
    id 401
    label "radiolinia"
  ]
  node [
    id 402
    label "media"
  ]
  node [
    id 403
    label "fala_radiowa"
  ]
  node [
    id 404
    label "radiofonia"
  ]
  node [
    id 405
    label "odbieranie"
  ]
  node [
    id 406
    label "studio"
  ]
  node [
    id 407
    label "dyskryminator"
  ]
  node [
    id 408
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 409
    label "odbiera&#263;"
  ]
  node [
    id 410
    label "telekomunikacja"
  ]
  node [
    id 411
    label "ekran"
  ]
  node [
    id 412
    label "BBC"
  ]
  node [
    id 413
    label "Polsat"
  ]
  node [
    id 414
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 415
    label "muza"
  ]
  node [
    id 416
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 417
    label "technologia"
  ]
  node [
    id 418
    label "ekscerpcja"
  ]
  node [
    id 419
    label "j&#281;zykowo"
  ]
  node [
    id 420
    label "pomini&#281;cie"
  ]
  node [
    id 421
    label "dzie&#322;o"
  ]
  node [
    id 422
    label "preparacja"
  ]
  node [
    id 423
    label "odmianka"
  ]
  node [
    id 424
    label "opu&#347;ci&#263;"
  ]
  node [
    id 425
    label "koniektura"
  ]
  node [
    id 426
    label "pisa&#263;"
  ]
  node [
    id 427
    label "obelga"
  ]
  node [
    id 428
    label "czasopismo"
  ]
  node [
    id 429
    label "psychotest"
  ]
  node [
    id 430
    label "pismo"
  ]
  node [
    id 431
    label "communication"
  ]
  node [
    id 432
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 433
    label "wk&#322;ad"
  ]
  node [
    id 434
    label "zajawka"
  ]
  node [
    id 435
    label "ok&#322;adka"
  ]
  node [
    id 436
    label "Zwrotnica"
  ]
  node [
    id 437
    label "dzia&#322;"
  ]
  node [
    id 438
    label "prasa"
  ]
  node [
    id 439
    label "szkolnie"
  ]
  node [
    id 440
    label "podstawowy"
  ]
  node [
    id 441
    label "prosty"
  ]
  node [
    id 442
    label "szkoleniowy"
  ]
  node [
    id 443
    label "skromny"
  ]
  node [
    id 444
    label "po_prostu"
  ]
  node [
    id 445
    label "naturalny"
  ]
  node [
    id 446
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 447
    label "rozprostowanie"
  ]
  node [
    id 448
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 449
    label "prosto"
  ]
  node [
    id 450
    label "prostowanie_si&#281;"
  ]
  node [
    id 451
    label "niepozorny"
  ]
  node [
    id 452
    label "cios"
  ]
  node [
    id 453
    label "prostoduszny"
  ]
  node [
    id 454
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 455
    label "naiwny"
  ]
  node [
    id 456
    label "&#322;atwy"
  ]
  node [
    id 457
    label "prostowanie"
  ]
  node [
    id 458
    label "zwyk&#322;y"
  ]
  node [
    id 459
    label "naukowy"
  ]
  node [
    id 460
    label "niezaawansowany"
  ]
  node [
    id 461
    label "najwa&#380;niejszy"
  ]
  node [
    id 462
    label "pocz&#261;tkowy"
  ]
  node [
    id 463
    label "podstawowo"
  ]
  node [
    id 464
    label "licencja"
  ]
  node [
    id 465
    label "opieka"
  ]
  node [
    id 466
    label "nadz&#243;r"
  ]
  node [
    id 467
    label "sponsorship"
  ]
  node [
    id 468
    label "zezwolenie"
  ]
  node [
    id 469
    label "za&#347;wiadczenie"
  ]
  node [
    id 470
    label "licencjonowa&#263;"
  ]
  node [
    id 471
    label "rasowy"
  ]
  node [
    id 472
    label "pozwolenie"
  ]
  node [
    id 473
    label "hodowla"
  ]
  node [
    id 474
    label "prawo"
  ]
  node [
    id 475
    label "license"
  ]
  node [
    id 476
    label "examination"
  ]
  node [
    id 477
    label "pomoc"
  ]
  node [
    id 478
    label "opieku&#324;cze_skrzyd&#322;a"
  ]
  node [
    id 479
    label "staranie"
  ]
  node [
    id 480
    label "belfer"
  ]
  node [
    id 481
    label "murza"
  ]
  node [
    id 482
    label "ojciec"
  ]
  node [
    id 483
    label "samiec"
  ]
  node [
    id 484
    label "androlog"
  ]
  node [
    id 485
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 486
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 487
    label "efendi"
  ]
  node [
    id 488
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 489
    label "bratek"
  ]
  node [
    id 490
    label "Mieszko_I"
  ]
  node [
    id 491
    label "Midas"
  ]
  node [
    id 492
    label "m&#261;&#380;"
  ]
  node [
    id 493
    label "bogaty"
  ]
  node [
    id 494
    label "popularyzator"
  ]
  node [
    id 495
    label "pracodawca"
  ]
  node [
    id 496
    label "kszta&#322;ciciel"
  ]
  node [
    id 497
    label "preceptor"
  ]
  node [
    id 498
    label "nabab"
  ]
  node [
    id 499
    label "pupil"
  ]
  node [
    id 500
    label "andropauza"
  ]
  node [
    id 501
    label "zwrot"
  ]
  node [
    id 502
    label "przyw&#243;dca"
  ]
  node [
    id 503
    label "doros&#322;y"
  ]
  node [
    id 504
    label "pedagog"
  ]
  node [
    id 505
    label "rz&#261;dzenie"
  ]
  node [
    id 506
    label "jegomo&#347;&#263;"
  ]
  node [
    id 507
    label "szkolnik"
  ]
  node [
    id 508
    label "ch&#322;opina"
  ]
  node [
    id 509
    label "w&#322;odarz"
  ]
  node [
    id 510
    label "profesor"
  ]
  node [
    id 511
    label "gra_w_karty"
  ]
  node [
    id 512
    label "w&#322;adza"
  ]
  node [
    id 513
    label "Fidel_Castro"
  ]
  node [
    id 514
    label "Anders"
  ]
  node [
    id 515
    label "Ko&#347;ciuszko"
  ]
  node [
    id 516
    label "Tito"
  ]
  node [
    id 517
    label "Miko&#322;ajczyk"
  ]
  node [
    id 518
    label "lider"
  ]
  node [
    id 519
    label "Mao"
  ]
  node [
    id 520
    label "Sabataj_Cwi"
  ]
  node [
    id 521
    label "p&#322;atnik"
  ]
  node [
    id 522
    label "zwierzchnik"
  ]
  node [
    id 523
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 524
    label "nadzorca"
  ]
  node [
    id 525
    label "funkcjonariusz"
  ]
  node [
    id 526
    label "wykupienie"
  ]
  node [
    id 527
    label "bycie_w_posiadaniu"
  ]
  node [
    id 528
    label "wykupywanie"
  ]
  node [
    id 529
    label "rozszerzyciel"
  ]
  node [
    id 530
    label "ludzko&#347;&#263;"
  ]
  node [
    id 531
    label "os&#322;abia&#263;"
  ]
  node [
    id 532
    label "hominid"
  ]
  node [
    id 533
    label "podw&#322;adny"
  ]
  node [
    id 534
    label "os&#322;abianie"
  ]
  node [
    id 535
    label "g&#322;owa"
  ]
  node [
    id 536
    label "figura"
  ]
  node [
    id 537
    label "portrecista"
  ]
  node [
    id 538
    label "dwun&#243;g"
  ]
  node [
    id 539
    label "profanum"
  ]
  node [
    id 540
    label "mikrokosmos"
  ]
  node [
    id 541
    label "nasada"
  ]
  node [
    id 542
    label "duch"
  ]
  node [
    id 543
    label "antropochoria"
  ]
  node [
    id 544
    label "osoba"
  ]
  node [
    id 545
    label "wz&#243;r"
  ]
  node [
    id 546
    label "senior"
  ]
  node [
    id 547
    label "oddzia&#322;ywanie"
  ]
  node [
    id 548
    label "Adam"
  ]
  node [
    id 549
    label "homo_sapiens"
  ]
  node [
    id 550
    label "polifag"
  ]
  node [
    id 551
    label "wydoro&#347;lenie"
  ]
  node [
    id 552
    label "du&#380;y"
  ]
  node [
    id 553
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 554
    label "doro&#347;lenie"
  ]
  node [
    id 555
    label "&#378;ra&#322;y"
  ]
  node [
    id 556
    label "doro&#347;le"
  ]
  node [
    id 557
    label "dojrzale"
  ]
  node [
    id 558
    label "dojrza&#322;y"
  ]
  node [
    id 559
    label "m&#261;dry"
  ]
  node [
    id 560
    label "doletni"
  ]
  node [
    id 561
    label "turn"
  ]
  node [
    id 562
    label "turning"
  ]
  node [
    id 563
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 564
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 565
    label "skr&#281;t"
  ]
  node [
    id 566
    label "obr&#243;t"
  ]
  node [
    id 567
    label "fraza_czasownikowa"
  ]
  node [
    id 568
    label "jednostka_leksykalna"
  ]
  node [
    id 569
    label "wyra&#380;enie"
  ]
  node [
    id 570
    label "starosta"
  ]
  node [
    id 571
    label "zarz&#261;dca"
  ]
  node [
    id 572
    label "w&#322;adca"
  ]
  node [
    id 573
    label "nauczyciel"
  ]
  node [
    id 574
    label "stopie&#324;_naukowy"
  ]
  node [
    id 575
    label "nauczyciel_akademicki"
  ]
  node [
    id 576
    label "tytu&#322;"
  ]
  node [
    id 577
    label "profesura"
  ]
  node [
    id 578
    label "konsulent"
  ]
  node [
    id 579
    label "wirtuoz"
  ]
  node [
    id 580
    label "autor"
  ]
  node [
    id 581
    label "wyprawka"
  ]
  node [
    id 582
    label "mundurek"
  ]
  node [
    id 583
    label "szko&#322;a"
  ]
  node [
    id 584
    label "tarcza"
  ]
  node [
    id 585
    label "elew"
  ]
  node [
    id 586
    label "absolwent"
  ]
  node [
    id 587
    label "klasa"
  ]
  node [
    id 588
    label "ekspert"
  ]
  node [
    id 589
    label "ochotnik"
  ]
  node [
    id 590
    label "pomocnik"
  ]
  node [
    id 591
    label "student"
  ]
  node [
    id 592
    label "nauczyciel_muzyki"
  ]
  node [
    id 593
    label "zakonnik"
  ]
  node [
    id 594
    label "bogacz"
  ]
  node [
    id 595
    label "dostojnik"
  ]
  node [
    id 596
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 597
    label "kuwada"
  ]
  node [
    id 598
    label "tworzyciel"
  ]
  node [
    id 599
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 600
    label "&#347;w"
  ]
  node [
    id 601
    label "pomys&#322;odawca"
  ]
  node [
    id 602
    label "wykonawca"
  ]
  node [
    id 603
    label "ojczym"
  ]
  node [
    id 604
    label "przodek"
  ]
  node [
    id 605
    label "papa"
  ]
  node [
    id 606
    label "stary"
  ]
  node [
    id 607
    label "kochanek"
  ]
  node [
    id 608
    label "fio&#322;ek"
  ]
  node [
    id 609
    label "facet"
  ]
  node [
    id 610
    label "brat"
  ]
  node [
    id 611
    label "zwierz&#281;"
  ]
  node [
    id 612
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 613
    label "ma&#322;&#380;onek"
  ]
  node [
    id 614
    label "m&#243;j"
  ]
  node [
    id 615
    label "ch&#322;op"
  ]
  node [
    id 616
    label "pan_m&#322;ody"
  ]
  node [
    id 617
    label "&#347;lubny"
  ]
  node [
    id 618
    label "pan_domu"
  ]
  node [
    id 619
    label "pan_i_w&#322;adca"
  ]
  node [
    id 620
    label "mo&#347;&#263;"
  ]
  node [
    id 621
    label "Frygia"
  ]
  node [
    id 622
    label "sprawowanie"
  ]
  node [
    id 623
    label "dominion"
  ]
  node [
    id 624
    label "dominowanie"
  ]
  node [
    id 625
    label "reign"
  ]
  node [
    id 626
    label "rule"
  ]
  node [
    id 627
    label "zwierz&#281;_domowe"
  ]
  node [
    id 628
    label "J&#281;drzejewicz"
  ]
  node [
    id 629
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 630
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 631
    label "John_Dewey"
  ]
  node [
    id 632
    label "specjalista"
  ]
  node [
    id 633
    label "&#380;ycie"
  ]
  node [
    id 634
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 635
    label "Turek"
  ]
  node [
    id 636
    label "effendi"
  ]
  node [
    id 637
    label "obfituj&#261;cy"
  ]
  node [
    id 638
    label "r&#243;&#380;norodny"
  ]
  node [
    id 639
    label "spania&#322;y"
  ]
  node [
    id 640
    label "obficie"
  ]
  node [
    id 641
    label "sytuowany"
  ]
  node [
    id 642
    label "och&#281;do&#380;ny"
  ]
  node [
    id 643
    label "forsiasty"
  ]
  node [
    id 644
    label "zapa&#347;ny"
  ]
  node [
    id 645
    label "bogato"
  ]
  node [
    id 646
    label "Katar"
  ]
  node [
    id 647
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 648
    label "Libia"
  ]
  node [
    id 649
    label "Gwatemala"
  ]
  node [
    id 650
    label "Afganistan"
  ]
  node [
    id 651
    label "Ekwador"
  ]
  node [
    id 652
    label "Tad&#380;ykistan"
  ]
  node [
    id 653
    label "Bhutan"
  ]
  node [
    id 654
    label "Argentyna"
  ]
  node [
    id 655
    label "D&#380;ibuti"
  ]
  node [
    id 656
    label "Wenezuela"
  ]
  node [
    id 657
    label "Ukraina"
  ]
  node [
    id 658
    label "Gabon"
  ]
  node [
    id 659
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 660
    label "Rwanda"
  ]
  node [
    id 661
    label "Liechtenstein"
  ]
  node [
    id 662
    label "organizacja"
  ]
  node [
    id 663
    label "Sri_Lanka"
  ]
  node [
    id 664
    label "Madagaskar"
  ]
  node [
    id 665
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 666
    label "Tonga"
  ]
  node [
    id 667
    label "Kongo"
  ]
  node [
    id 668
    label "Bangladesz"
  ]
  node [
    id 669
    label "Kanada"
  ]
  node [
    id 670
    label "Wehrlen"
  ]
  node [
    id 671
    label "Algieria"
  ]
  node [
    id 672
    label "Surinam"
  ]
  node [
    id 673
    label "Chile"
  ]
  node [
    id 674
    label "Sahara_Zachodnia"
  ]
  node [
    id 675
    label "Uganda"
  ]
  node [
    id 676
    label "W&#281;gry"
  ]
  node [
    id 677
    label "Birma"
  ]
  node [
    id 678
    label "Kazachstan"
  ]
  node [
    id 679
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 680
    label "Armenia"
  ]
  node [
    id 681
    label "Tuwalu"
  ]
  node [
    id 682
    label "Timor_Wschodni"
  ]
  node [
    id 683
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 684
    label "Izrael"
  ]
  node [
    id 685
    label "Estonia"
  ]
  node [
    id 686
    label "Komory"
  ]
  node [
    id 687
    label "Kamerun"
  ]
  node [
    id 688
    label "Haiti"
  ]
  node [
    id 689
    label "Belize"
  ]
  node [
    id 690
    label "Sierra_Leone"
  ]
  node [
    id 691
    label "Luksemburg"
  ]
  node [
    id 692
    label "USA"
  ]
  node [
    id 693
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 694
    label "Barbados"
  ]
  node [
    id 695
    label "San_Marino"
  ]
  node [
    id 696
    label "Bu&#322;garia"
  ]
  node [
    id 697
    label "Wietnam"
  ]
  node [
    id 698
    label "Indonezja"
  ]
  node [
    id 699
    label "Malawi"
  ]
  node [
    id 700
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 701
    label "Francja"
  ]
  node [
    id 702
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 703
    label "partia"
  ]
  node [
    id 704
    label "Zambia"
  ]
  node [
    id 705
    label "Angola"
  ]
  node [
    id 706
    label "Grenada"
  ]
  node [
    id 707
    label "Nepal"
  ]
  node [
    id 708
    label "Panama"
  ]
  node [
    id 709
    label "Rumunia"
  ]
  node [
    id 710
    label "Czarnog&#243;ra"
  ]
  node [
    id 711
    label "Malediwy"
  ]
  node [
    id 712
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 713
    label "S&#322;owacja"
  ]
  node [
    id 714
    label "para"
  ]
  node [
    id 715
    label "Egipt"
  ]
  node [
    id 716
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 717
    label "Kolumbia"
  ]
  node [
    id 718
    label "Mozambik"
  ]
  node [
    id 719
    label "Laos"
  ]
  node [
    id 720
    label "Burundi"
  ]
  node [
    id 721
    label "Suazi"
  ]
  node [
    id 722
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 723
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 724
    label "Czechy"
  ]
  node [
    id 725
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 726
    label "Wyspy_Marshalla"
  ]
  node [
    id 727
    label "Trynidad_i_Tobago"
  ]
  node [
    id 728
    label "Dominika"
  ]
  node [
    id 729
    label "Palau"
  ]
  node [
    id 730
    label "Syria"
  ]
  node [
    id 731
    label "Gwinea_Bissau"
  ]
  node [
    id 732
    label "Liberia"
  ]
  node [
    id 733
    label "Zimbabwe"
  ]
  node [
    id 734
    label "Polska"
  ]
  node [
    id 735
    label "Jamajka"
  ]
  node [
    id 736
    label "Dominikana"
  ]
  node [
    id 737
    label "Senegal"
  ]
  node [
    id 738
    label "Gruzja"
  ]
  node [
    id 739
    label "Togo"
  ]
  node [
    id 740
    label "Chorwacja"
  ]
  node [
    id 741
    label "Meksyk"
  ]
  node [
    id 742
    label "Macedonia"
  ]
  node [
    id 743
    label "Gujana"
  ]
  node [
    id 744
    label "Zair"
  ]
  node [
    id 745
    label "Albania"
  ]
  node [
    id 746
    label "Kambod&#380;a"
  ]
  node [
    id 747
    label "Mauritius"
  ]
  node [
    id 748
    label "Monako"
  ]
  node [
    id 749
    label "Gwinea"
  ]
  node [
    id 750
    label "Mali"
  ]
  node [
    id 751
    label "Nigeria"
  ]
  node [
    id 752
    label "Kostaryka"
  ]
  node [
    id 753
    label "Hanower"
  ]
  node [
    id 754
    label "Paragwaj"
  ]
  node [
    id 755
    label "W&#322;ochy"
  ]
  node [
    id 756
    label "Wyspy_Salomona"
  ]
  node [
    id 757
    label "Seszele"
  ]
  node [
    id 758
    label "Hiszpania"
  ]
  node [
    id 759
    label "Boliwia"
  ]
  node [
    id 760
    label "Kirgistan"
  ]
  node [
    id 761
    label "Irlandia"
  ]
  node [
    id 762
    label "Czad"
  ]
  node [
    id 763
    label "Irak"
  ]
  node [
    id 764
    label "Lesoto"
  ]
  node [
    id 765
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 766
    label "Malta"
  ]
  node [
    id 767
    label "Andora"
  ]
  node [
    id 768
    label "Chiny"
  ]
  node [
    id 769
    label "Filipiny"
  ]
  node [
    id 770
    label "Antarktis"
  ]
  node [
    id 771
    label "Niemcy"
  ]
  node [
    id 772
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 773
    label "Brazylia"
  ]
  node [
    id 774
    label "terytorium"
  ]
  node [
    id 775
    label "Nikaragua"
  ]
  node [
    id 776
    label "Pakistan"
  ]
  node [
    id 777
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 778
    label "Kenia"
  ]
  node [
    id 779
    label "Niger"
  ]
  node [
    id 780
    label "Tunezja"
  ]
  node [
    id 781
    label "Portugalia"
  ]
  node [
    id 782
    label "Fid&#380;i"
  ]
  node [
    id 783
    label "Maroko"
  ]
  node [
    id 784
    label "Botswana"
  ]
  node [
    id 785
    label "Tajlandia"
  ]
  node [
    id 786
    label "Australia"
  ]
  node [
    id 787
    label "Burkina_Faso"
  ]
  node [
    id 788
    label "interior"
  ]
  node [
    id 789
    label "Benin"
  ]
  node [
    id 790
    label "Tanzania"
  ]
  node [
    id 791
    label "Indie"
  ]
  node [
    id 792
    label "&#321;otwa"
  ]
  node [
    id 793
    label "Kiribati"
  ]
  node [
    id 794
    label "Antigua_i_Barbuda"
  ]
  node [
    id 795
    label "Rodezja"
  ]
  node [
    id 796
    label "Cypr"
  ]
  node [
    id 797
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 798
    label "Peru"
  ]
  node [
    id 799
    label "Austria"
  ]
  node [
    id 800
    label "Urugwaj"
  ]
  node [
    id 801
    label "Jordania"
  ]
  node [
    id 802
    label "Grecja"
  ]
  node [
    id 803
    label "Azerbejd&#380;an"
  ]
  node [
    id 804
    label "Turcja"
  ]
  node [
    id 805
    label "Samoa"
  ]
  node [
    id 806
    label "Sudan"
  ]
  node [
    id 807
    label "Oman"
  ]
  node [
    id 808
    label "ziemia"
  ]
  node [
    id 809
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 810
    label "Uzbekistan"
  ]
  node [
    id 811
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 812
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 813
    label "Honduras"
  ]
  node [
    id 814
    label "Mongolia"
  ]
  node [
    id 815
    label "Portoryko"
  ]
  node [
    id 816
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 817
    label "Serbia"
  ]
  node [
    id 818
    label "Tajwan"
  ]
  node [
    id 819
    label "Wielka_Brytania"
  ]
  node [
    id 820
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 821
    label "Liban"
  ]
  node [
    id 822
    label "Japonia"
  ]
  node [
    id 823
    label "Ghana"
  ]
  node [
    id 824
    label "Bahrajn"
  ]
  node [
    id 825
    label "Belgia"
  ]
  node [
    id 826
    label "Etiopia"
  ]
  node [
    id 827
    label "Mikronezja"
  ]
  node [
    id 828
    label "Kuwejt"
  ]
  node [
    id 829
    label "Bahamy"
  ]
  node [
    id 830
    label "Rosja"
  ]
  node [
    id 831
    label "Mo&#322;dawia"
  ]
  node [
    id 832
    label "Litwa"
  ]
  node [
    id 833
    label "S&#322;owenia"
  ]
  node [
    id 834
    label "Szwajcaria"
  ]
  node [
    id 835
    label "Erytrea"
  ]
  node [
    id 836
    label "Kuba"
  ]
  node [
    id 837
    label "Arabia_Saudyjska"
  ]
  node [
    id 838
    label "granica_pa&#324;stwa"
  ]
  node [
    id 839
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 840
    label "Malezja"
  ]
  node [
    id 841
    label "Korea"
  ]
  node [
    id 842
    label "Jemen"
  ]
  node [
    id 843
    label "Nowa_Zelandia"
  ]
  node [
    id 844
    label "Namibia"
  ]
  node [
    id 845
    label "Nauru"
  ]
  node [
    id 846
    label "holoarktyka"
  ]
  node [
    id 847
    label "Brunei"
  ]
  node [
    id 848
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 849
    label "Khitai"
  ]
  node [
    id 850
    label "Mauretania"
  ]
  node [
    id 851
    label "Iran"
  ]
  node [
    id 852
    label "Gambia"
  ]
  node [
    id 853
    label "Somalia"
  ]
  node [
    id 854
    label "Holandia"
  ]
  node [
    id 855
    label "Turkmenistan"
  ]
  node [
    id 856
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 857
    label "Salwador"
  ]
  node [
    id 858
    label "przedpok&#243;j"
  ]
  node [
    id 859
    label "korytarz"
  ]
  node [
    id 860
    label "przej&#347;cie"
  ]
  node [
    id 861
    label "chody"
  ]
  node [
    id 862
    label "jaskinia"
  ]
  node [
    id 863
    label "pomieszczenie"
  ]
  node [
    id 864
    label "trasa"
  ]
  node [
    id 865
    label "kurytarz"
  ]
  node [
    id 866
    label "Goebbels"
  ]
  node [
    id 867
    label "Sto&#322;ypin"
  ]
  node [
    id 868
    label "przybli&#380;enie"
  ]
  node [
    id 869
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 870
    label "kategoria"
  ]
  node [
    id 871
    label "szpaler"
  ]
  node [
    id 872
    label "lon&#380;a"
  ]
  node [
    id 873
    label "uporz&#261;dkowanie"
  ]
  node [
    id 874
    label "egzekutywa"
  ]
  node [
    id 875
    label "premier"
  ]
  node [
    id 876
    label "Londyn"
  ]
  node [
    id 877
    label "gabinet_cieni"
  ]
  node [
    id 878
    label "number"
  ]
  node [
    id 879
    label "Konsulat"
  ]
  node [
    id 880
    label "tract"
  ]
  node [
    id 881
    label "notabl"
  ]
  node [
    id 882
    label "oficja&#322;"
  ]
  node [
    id 883
    label "formation"
  ]
  node [
    id 884
    label "Karta_Nauczyciela"
  ]
  node [
    id 885
    label "wiedza"
  ]
  node [
    id 886
    label "heureza"
  ]
  node [
    id 887
    label "miasteczko_rowerowe"
  ]
  node [
    id 888
    label "urszulanki"
  ]
  node [
    id 889
    label "niepokalanki"
  ]
  node [
    id 890
    label "kwalifikacje"
  ]
  node [
    id 891
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 892
    label "&#322;awa_szkolna"
  ]
  node [
    id 893
    label "nauka"
  ]
  node [
    id 894
    label "skolaryzacja"
  ]
  node [
    id 895
    label "program_nauczania"
  ]
  node [
    id 896
    label "szkolnictwo"
  ]
  node [
    id 897
    label "form"
  ]
  node [
    id 898
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 899
    label "gospodarka"
  ]
  node [
    id 900
    label "porada"
  ]
  node [
    id 901
    label "fotowoltaika"
  ]
  node [
    id 902
    label "przem&#243;wienie"
  ]
  node [
    id 903
    label "nauki_o_poznaniu"
  ]
  node [
    id 904
    label "nomotetyczny"
  ]
  node [
    id 905
    label "systematyka"
  ]
  node [
    id 906
    label "typologia"
  ]
  node [
    id 907
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 908
    label "kultura_duchowa"
  ]
  node [
    id 909
    label "nauki_penalne"
  ]
  node [
    id 910
    label "dziedzina"
  ]
  node [
    id 911
    label "imagineskopia"
  ]
  node [
    id 912
    label "teoria_naukowa"
  ]
  node [
    id 913
    label "inwentyka"
  ]
  node [
    id 914
    label "metodologia"
  ]
  node [
    id 915
    label "nauki_o_Ziemi"
  ]
  node [
    id 916
    label "kognicja"
  ]
  node [
    id 917
    label "rozprawa"
  ]
  node [
    id 918
    label "legislacyjnie"
  ]
  node [
    id 919
    label "przes&#322;anka"
  ]
  node [
    id 920
    label "nast&#281;pstwo"
  ]
  node [
    id 921
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 922
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 923
    label "metoda"
  ]
  node [
    id 924
    label "nauczanie"
  ]
  node [
    id 925
    label "inwentarz"
  ]
  node [
    id 926
    label "rynek"
  ]
  node [
    id 927
    label "mieszkalnictwo"
  ]
  node [
    id 928
    label "agregat_ekonomiczny"
  ]
  node [
    id 929
    label "farmaceutyka"
  ]
  node [
    id 930
    label "produkowanie"
  ]
  node [
    id 931
    label "rolnictwo"
  ]
  node [
    id 932
    label "transport"
  ]
  node [
    id 933
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 934
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 935
    label "obronno&#347;&#263;"
  ]
  node [
    id 936
    label "sektor_prywatny"
  ]
  node [
    id 937
    label "sch&#322;adza&#263;"
  ]
  node [
    id 938
    label "czerwona_strefa"
  ]
  node [
    id 939
    label "struktura"
  ]
  node [
    id 940
    label "pole"
  ]
  node [
    id 941
    label "sektor_publiczny"
  ]
  node [
    id 942
    label "bankowo&#347;&#263;"
  ]
  node [
    id 943
    label "gospodarowanie"
  ]
  node [
    id 944
    label "obora"
  ]
  node [
    id 945
    label "gospodarka_wodna"
  ]
  node [
    id 946
    label "gospodarka_le&#347;na"
  ]
  node [
    id 947
    label "gospodarowa&#263;"
  ]
  node [
    id 948
    label "fabryka"
  ]
  node [
    id 949
    label "wytw&#243;rnia"
  ]
  node [
    id 950
    label "stodo&#322;a"
  ]
  node [
    id 951
    label "przemys&#322;"
  ]
  node [
    id 952
    label "spichlerz"
  ]
  node [
    id 953
    label "sch&#322;adzanie"
  ]
  node [
    id 954
    label "administracja"
  ]
  node [
    id 955
    label "sch&#322;odzenie"
  ]
  node [
    id 956
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 957
    label "zasada"
  ]
  node [
    id 958
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 959
    label "regulacja_cen"
  ]
  node [
    id 960
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 961
    label "proporcja"
  ]
  node [
    id 962
    label "wykszta&#322;cenie"
  ]
  node [
    id 963
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 964
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 965
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 966
    label "urszulanki_szare"
  ]
  node [
    id 967
    label "cognition"
  ]
  node [
    id 968
    label "intelekt"
  ]
  node [
    id 969
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 970
    label "zaawansowanie"
  ]
  node [
    id 971
    label "nacjonalistyczny"
  ]
  node [
    id 972
    label "narodowo"
  ]
  node [
    id 973
    label "wa&#380;ny"
  ]
  node [
    id 974
    label "wynios&#322;y"
  ]
  node [
    id 975
    label "dono&#347;ny"
  ]
  node [
    id 976
    label "silny"
  ]
  node [
    id 977
    label "wa&#380;nie"
  ]
  node [
    id 978
    label "istotnie"
  ]
  node [
    id 979
    label "znaczny"
  ]
  node [
    id 980
    label "eksponowany"
  ]
  node [
    id 981
    label "dobry"
  ]
  node [
    id 982
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 983
    label "nale&#380;ny"
  ]
  node [
    id 984
    label "nale&#380;yty"
  ]
  node [
    id 985
    label "typowy"
  ]
  node [
    id 986
    label "uprawniony"
  ]
  node [
    id 987
    label "zasadniczy"
  ]
  node [
    id 988
    label "stosownie"
  ]
  node [
    id 989
    label "taki"
  ]
  node [
    id 990
    label "charakterystyczny"
  ]
  node [
    id 991
    label "prawdziwy"
  ]
  node [
    id 992
    label "ten"
  ]
  node [
    id 993
    label "polityczny"
  ]
  node [
    id 994
    label "nacjonalistycznie"
  ]
  node [
    id 995
    label "narodowo&#347;ciowy"
  ]
  node [
    id 996
    label "gwiazda"
  ]
  node [
    id 997
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 998
    label "Arktur"
  ]
  node [
    id 999
    label "kszta&#322;t"
  ]
  node [
    id 1000
    label "Gwiazda_Polarna"
  ]
  node [
    id 1001
    label "agregatka"
  ]
  node [
    id 1002
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 1003
    label "S&#322;o&#324;ce"
  ]
  node [
    id 1004
    label "Nibiru"
  ]
  node [
    id 1005
    label "konstelacja"
  ]
  node [
    id 1006
    label "ornament"
  ]
  node [
    id 1007
    label "delta_Scuti"
  ]
  node [
    id 1008
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1009
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 1010
    label "s&#322;awa"
  ]
  node [
    id 1011
    label "promie&#324;"
  ]
  node [
    id 1012
    label "star"
  ]
  node [
    id 1013
    label "gwiazdosz"
  ]
  node [
    id 1014
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 1015
    label "asocjacja_gwiazd"
  ]
  node [
    id 1016
    label "supergrupa"
  ]
  node [
    id 1017
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 1018
    label "byt"
  ]
  node [
    id 1019
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1020
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 1021
    label "nauka_prawa"
  ]
  node [
    id 1022
    label "utw&#243;r"
  ]
  node [
    id 1023
    label "charakterystyka"
  ]
  node [
    id 1024
    label "zaistnie&#263;"
  ]
  node [
    id 1025
    label "Osjan"
  ]
  node [
    id 1026
    label "kto&#347;"
  ]
  node [
    id 1027
    label "wygl&#261;d"
  ]
  node [
    id 1028
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1029
    label "trim"
  ]
  node [
    id 1030
    label "poby&#263;"
  ]
  node [
    id 1031
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1032
    label "Aspazja"
  ]
  node [
    id 1033
    label "punkt_widzenia"
  ]
  node [
    id 1034
    label "kompleksja"
  ]
  node [
    id 1035
    label "wytrzyma&#263;"
  ]
  node [
    id 1036
    label "budowa"
  ]
  node [
    id 1037
    label "formacja"
  ]
  node [
    id 1038
    label "pozosta&#263;"
  ]
  node [
    id 1039
    label "point"
  ]
  node [
    id 1040
    label "przedstawienie"
  ]
  node [
    id 1041
    label "go&#347;&#263;"
  ]
  node [
    id 1042
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1043
    label "armia"
  ]
  node [
    id 1044
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1045
    label "poprowadzi&#263;"
  ]
  node [
    id 1046
    label "cord"
  ]
  node [
    id 1047
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1048
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1049
    label "materia&#322;_zecerski"
  ]
  node [
    id 1050
    label "przeorientowywanie"
  ]
  node [
    id 1051
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1052
    label "curve"
  ]
  node [
    id 1053
    label "figura_geometryczna"
  ]
  node [
    id 1054
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 1055
    label "jard"
  ]
  node [
    id 1056
    label "szczep"
  ]
  node [
    id 1057
    label "phreaker"
  ]
  node [
    id 1058
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 1059
    label "grupa_organizm&#243;w"
  ]
  node [
    id 1060
    label "prowadzi&#263;"
  ]
  node [
    id 1061
    label "przeorientowywa&#263;"
  ]
  node [
    id 1062
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1063
    label "access"
  ]
  node [
    id 1064
    label "przeorientowanie"
  ]
  node [
    id 1065
    label "przeorientowa&#263;"
  ]
  node [
    id 1066
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 1067
    label "billing"
  ]
  node [
    id 1068
    label "granica"
  ]
  node [
    id 1069
    label "sztrych"
  ]
  node [
    id 1070
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 1071
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1072
    label "drzewo_genealogiczne"
  ]
  node [
    id 1073
    label "transporter"
  ]
  node [
    id 1074
    label "line"
  ]
  node [
    id 1075
    label "przew&#243;d"
  ]
  node [
    id 1076
    label "granice"
  ]
  node [
    id 1077
    label "kontakt"
  ]
  node [
    id 1078
    label "przewo&#378;nik"
  ]
  node [
    id 1079
    label "przystanek"
  ]
  node [
    id 1080
    label "linijka"
  ]
  node [
    id 1081
    label "spos&#243;b"
  ]
  node [
    id 1082
    label "coalescence"
  ]
  node [
    id 1083
    label "Ural"
  ]
  node [
    id 1084
    label "bearing"
  ]
  node [
    id 1085
    label "prowadzenie"
  ]
  node [
    id 1086
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 1087
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1088
    label "koniec"
  ]
  node [
    id 1089
    label "podkatalog"
  ]
  node [
    id 1090
    label "nadpisa&#263;"
  ]
  node [
    id 1091
    label "nadpisanie"
  ]
  node [
    id 1092
    label "bundle"
  ]
  node [
    id 1093
    label "folder"
  ]
  node [
    id 1094
    label "nadpisywanie"
  ]
  node [
    id 1095
    label "paczka"
  ]
  node [
    id 1096
    label "nadpisywa&#263;"
  ]
  node [
    id 1097
    label "dokument"
  ]
  node [
    id 1098
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 1099
    label "Rzym_Zachodni"
  ]
  node [
    id 1100
    label "ilo&#347;&#263;"
  ]
  node [
    id 1101
    label "element"
  ]
  node [
    id 1102
    label "Rzym_Wschodni"
  ]
  node [
    id 1103
    label "urz&#261;dzenie"
  ]
  node [
    id 1104
    label "rozmiar"
  ]
  node [
    id 1105
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 1106
    label "zwierciad&#322;o"
  ]
  node [
    id 1107
    label "capacity"
  ]
  node [
    id 1108
    label "plane"
  ]
  node [
    id 1109
    label "temat"
  ]
  node [
    id 1110
    label "poznanie"
  ]
  node [
    id 1111
    label "leksem"
  ]
  node [
    id 1112
    label "stan"
  ]
  node [
    id 1113
    label "blaszka"
  ]
  node [
    id 1114
    label "kantyzm"
  ]
  node [
    id 1115
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1116
    label "do&#322;ek"
  ]
  node [
    id 1117
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1118
    label "formality"
  ]
  node [
    id 1119
    label "mode"
  ]
  node [
    id 1120
    label "morfem"
  ]
  node [
    id 1121
    label "rdze&#324;"
  ]
  node [
    id 1122
    label "kielich"
  ]
  node [
    id 1123
    label "ornamentyka"
  ]
  node [
    id 1124
    label "pasmo"
  ]
  node [
    id 1125
    label "zwyczaj"
  ]
  node [
    id 1126
    label "naczynie"
  ]
  node [
    id 1127
    label "p&#322;at"
  ]
  node [
    id 1128
    label "maszyna_drukarska"
  ]
  node [
    id 1129
    label "style"
  ]
  node [
    id 1130
    label "linearno&#347;&#263;"
  ]
  node [
    id 1131
    label "spirala"
  ]
  node [
    id 1132
    label "dyspozycja"
  ]
  node [
    id 1133
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1134
    label "October"
  ]
  node [
    id 1135
    label "p&#281;tla"
  ]
  node [
    id 1136
    label "arystotelizm"
  ]
  node [
    id 1137
    label "szablon"
  ]
  node [
    id 1138
    label "miniatura"
  ]
  node [
    id 1139
    label "pochwytanie"
  ]
  node [
    id 1140
    label "wording"
  ]
  node [
    id 1141
    label "wzbudzenie"
  ]
  node [
    id 1142
    label "withdrawal"
  ]
  node [
    id 1143
    label "capture"
  ]
  node [
    id 1144
    label "podniesienie"
  ]
  node [
    id 1145
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 1146
    label "film"
  ]
  node [
    id 1147
    label "scena"
  ]
  node [
    id 1148
    label "zapisanie"
  ]
  node [
    id 1149
    label "prezentacja"
  ]
  node [
    id 1150
    label "rzucenie"
  ]
  node [
    id 1151
    label "zamkni&#281;cie"
  ]
  node [
    id 1152
    label "zabranie"
  ]
  node [
    id 1153
    label "poinformowanie"
  ]
  node [
    id 1154
    label "zaaresztowanie"
  ]
  node [
    id 1155
    label "wzi&#281;cie"
  ]
  node [
    id 1156
    label "eastern_hemisphere"
  ]
  node [
    id 1157
    label "kierunek"
  ]
  node [
    id 1158
    label "kierowa&#263;"
  ]
  node [
    id 1159
    label "inform"
  ]
  node [
    id 1160
    label "marshal"
  ]
  node [
    id 1161
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1162
    label "wyznacza&#263;"
  ]
  node [
    id 1163
    label "pomaga&#263;"
  ]
  node [
    id 1164
    label "tu&#322;&#243;w"
  ]
  node [
    id 1165
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 1166
    label "wielok&#261;t"
  ]
  node [
    id 1167
    label "odcinek"
  ]
  node [
    id 1168
    label "strzelba"
  ]
  node [
    id 1169
    label "lufa"
  ]
  node [
    id 1170
    label "&#347;ciana"
  ]
  node [
    id 1171
    label "wyznaczenie"
  ]
  node [
    id 1172
    label "przyczynienie_si&#281;"
  ]
  node [
    id 1173
    label "zwr&#243;cenie"
  ]
  node [
    id 1174
    label "zrozumienie"
  ]
  node [
    id 1175
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1176
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1177
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 1178
    label "zorientowanie_si&#281;"
  ]
  node [
    id 1179
    label "pogubienie_si&#281;"
  ]
  node [
    id 1180
    label "orientation"
  ]
  node [
    id 1181
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 1182
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 1183
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1184
    label "gubienie_si&#281;"
  ]
  node [
    id 1185
    label "wrench"
  ]
  node [
    id 1186
    label "nawini&#281;cie"
  ]
  node [
    id 1187
    label "os&#322;abienie"
  ]
  node [
    id 1188
    label "uszkodzenie"
  ]
  node [
    id 1189
    label "odbicie"
  ]
  node [
    id 1190
    label "poskr&#281;canie"
  ]
  node [
    id 1191
    label "uraz"
  ]
  node [
    id 1192
    label "odchylenie_si&#281;"
  ]
  node [
    id 1193
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1194
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1195
    label "splecenie"
  ]
  node [
    id 1196
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 1197
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1198
    label "sple&#347;&#263;"
  ]
  node [
    id 1199
    label "os&#322;abi&#263;"
  ]
  node [
    id 1200
    label "nawin&#261;&#263;"
  ]
  node [
    id 1201
    label "scali&#263;"
  ]
  node [
    id 1202
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 1203
    label "twist"
  ]
  node [
    id 1204
    label "splay"
  ]
  node [
    id 1205
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1206
    label "uszkodzi&#263;"
  ]
  node [
    id 1207
    label "break"
  ]
  node [
    id 1208
    label "flex"
  ]
  node [
    id 1209
    label "zaty&#322;"
  ]
  node [
    id 1210
    label "pupa"
  ]
  node [
    id 1211
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1212
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1213
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 1214
    label "splata&#263;"
  ]
  node [
    id 1215
    label "throw"
  ]
  node [
    id 1216
    label "screw"
  ]
  node [
    id 1217
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1218
    label "scala&#263;"
  ]
  node [
    id 1219
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 1220
    label "przedmiot"
  ]
  node [
    id 1221
    label "przelezienie"
  ]
  node [
    id 1222
    label "&#347;piew"
  ]
  node [
    id 1223
    label "Synaj"
  ]
  node [
    id 1224
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1225
    label "wysoki"
  ]
  node [
    id 1226
    label "wzniesienie"
  ]
  node [
    id 1227
    label "pi&#281;tro"
  ]
  node [
    id 1228
    label "Ropa"
  ]
  node [
    id 1229
    label "kupa"
  ]
  node [
    id 1230
    label "przele&#378;&#263;"
  ]
  node [
    id 1231
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 1232
    label "karczek"
  ]
  node [
    id 1233
    label "rami&#261;czko"
  ]
  node [
    id 1234
    label "Jaworze"
  ]
  node [
    id 1235
    label "orient"
  ]
  node [
    id 1236
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 1237
    label "aim"
  ]
  node [
    id 1238
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 1239
    label "wyznaczy&#263;"
  ]
  node [
    id 1240
    label "pomaganie"
  ]
  node [
    id 1241
    label "przyczynianie_si&#281;"
  ]
  node [
    id 1242
    label "zwracanie"
  ]
  node [
    id 1243
    label "rozeznawanie"
  ]
  node [
    id 1244
    label "oznaczanie"
  ]
  node [
    id 1245
    label "odchylanie_si&#281;"
  ]
  node [
    id 1246
    label "kszta&#322;towanie"
  ]
  node [
    id 1247
    label "uprz&#281;dzenie"
  ]
  node [
    id 1248
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1249
    label "scalanie"
  ]
  node [
    id 1250
    label "lewoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 1251
    label "snucie"
  ]
  node [
    id 1252
    label "prawoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 1253
    label "tortuosity"
  ]
  node [
    id 1254
    label "odbijanie"
  ]
  node [
    id 1255
    label "contortion"
  ]
  node [
    id 1256
    label "splatanie"
  ]
  node [
    id 1257
    label "graficzny_interfejs_u&#380;ytkownika"
  ]
  node [
    id 1258
    label "uk&#322;ad_graficzny"
  ]
  node [
    id 1259
    label "uwierzytelnienie"
  ]
  node [
    id 1260
    label "liczba"
  ]
  node [
    id 1261
    label "circumference"
  ]
  node [
    id 1262
    label "cyrkumferencja"
  ]
  node [
    id 1263
    label "provider"
  ]
  node [
    id 1264
    label "hipertekst"
  ]
  node [
    id 1265
    label "cyberprzestrze&#324;"
  ]
  node [
    id 1266
    label "mem"
  ]
  node [
    id 1267
    label "gra_sieciowa"
  ]
  node [
    id 1268
    label "grooming"
  ]
  node [
    id 1269
    label "biznes_elektroniczny"
  ]
  node [
    id 1270
    label "sie&#263;_komputerowa"
  ]
  node [
    id 1271
    label "punkt_dost&#281;pu"
  ]
  node [
    id 1272
    label "us&#322;uga_internetowa"
  ]
  node [
    id 1273
    label "netbook"
  ]
  node [
    id 1274
    label "e-hazard"
  ]
  node [
    id 1275
    label "podcast"
  ]
  node [
    id 1276
    label "co&#347;"
  ]
  node [
    id 1277
    label "thing"
  ]
  node [
    id 1278
    label "program"
  ]
  node [
    id 1279
    label "rzecz"
  ]
  node [
    id 1280
    label "faul"
  ]
  node [
    id 1281
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 1282
    label "s&#281;dzia"
  ]
  node [
    id 1283
    label "bon"
  ]
  node [
    id 1284
    label "ticket"
  ]
  node [
    id 1285
    label "arkusz"
  ]
  node [
    id 1286
    label "kartonik"
  ]
  node [
    id 1287
    label "kara"
  ]
  node [
    id 1288
    label "pagination"
  ]
  node [
    id 1289
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1290
    label "darowizna"
  ]
  node [
    id 1291
    label "foundation"
  ]
  node [
    id 1292
    label "dar"
  ]
  node [
    id 1293
    label "pocz&#261;tek"
  ]
  node [
    id 1294
    label "osoba_prawna"
  ]
  node [
    id 1295
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1296
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1297
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1298
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1299
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1300
    label "Fundusze_Unijne"
  ]
  node [
    id 1301
    label "zamyka&#263;"
  ]
  node [
    id 1302
    label "establishment"
  ]
  node [
    id 1303
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1304
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1305
    label "afiliowa&#263;"
  ]
  node [
    id 1306
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1307
    label "standard"
  ]
  node [
    id 1308
    label "zamykanie"
  ]
  node [
    id 1309
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1310
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1311
    label "pierworodztwo"
  ]
  node [
    id 1312
    label "upgrade"
  ]
  node [
    id 1313
    label "przeniesienie_praw"
  ]
  node [
    id 1314
    label "zapomoga"
  ]
  node [
    id 1315
    label "transakcja"
  ]
  node [
    id 1316
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1317
    label "da&#324;"
  ]
  node [
    id 1318
    label "faculty"
  ]
  node [
    id 1319
    label "stygmat"
  ]
  node [
    id 1320
    label "dobro"
  ]
  node [
    id 1321
    label "&#322;atwo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 217
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 220
  ]
  edge [
    source 11
    target 207
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 464
  ]
  edge [
    source 15
    target 465
  ]
  edge [
    source 15
    target 466
  ]
  edge [
    source 15
    target 467
  ]
  edge [
    source 15
    target 468
  ]
  edge [
    source 15
    target 469
  ]
  edge [
    source 15
    target 470
  ]
  edge [
    source 15
    target 471
  ]
  edge [
    source 15
    target 472
  ]
  edge [
    source 15
    target 473
  ]
  edge [
    source 15
    target 474
  ]
  edge [
    source 15
    target 475
  ]
  edge [
    source 15
    target 317
  ]
  edge [
    source 15
    target 378
  ]
  edge [
    source 15
    target 476
  ]
  edge [
    source 15
    target 477
  ]
  edge [
    source 15
    target 478
  ]
  edge [
    source 15
    target 479
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 480
  ]
  edge [
    source 16
    target 481
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 482
  ]
  edge [
    source 16
    target 483
  ]
  edge [
    source 16
    target 484
  ]
  edge [
    source 16
    target 485
  ]
  edge [
    source 16
    target 486
  ]
  edge [
    source 16
    target 487
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 488
  ]
  edge [
    source 16
    target 489
  ]
  edge [
    source 16
    target 490
  ]
  edge [
    source 16
    target 491
  ]
  edge [
    source 16
    target 492
  ]
  edge [
    source 16
    target 493
  ]
  edge [
    source 16
    target 494
  ]
  edge [
    source 16
    target 495
  ]
  edge [
    source 16
    target 496
  ]
  edge [
    source 16
    target 497
  ]
  edge [
    source 16
    target 498
  ]
  edge [
    source 16
    target 499
  ]
  edge [
    source 16
    target 500
  ]
  edge [
    source 16
    target 501
  ]
  edge [
    source 16
    target 502
  ]
  edge [
    source 16
    target 503
  ]
  edge [
    source 16
    target 504
  ]
  edge [
    source 16
    target 505
  ]
  edge [
    source 16
    target 506
  ]
  edge [
    source 16
    target 507
  ]
  edge [
    source 16
    target 508
  ]
  edge [
    source 16
    target 509
  ]
  edge [
    source 16
    target 510
  ]
  edge [
    source 16
    target 511
  ]
  edge [
    source 16
    target 512
  ]
  edge [
    source 16
    target 513
  ]
  edge [
    source 16
    target 514
  ]
  edge [
    source 16
    target 515
  ]
  edge [
    source 16
    target 516
  ]
  edge [
    source 16
    target 517
  ]
  edge [
    source 16
    target 518
  ]
  edge [
    source 16
    target 519
  ]
  edge [
    source 16
    target 520
  ]
  edge [
    source 16
    target 521
  ]
  edge [
    source 16
    target 522
  ]
  edge [
    source 16
    target 523
  ]
  edge [
    source 16
    target 524
  ]
  edge [
    source 16
    target 525
  ]
  edge [
    source 16
    target 274
  ]
  edge [
    source 16
    target 526
  ]
  edge [
    source 16
    target 527
  ]
  edge [
    source 16
    target 528
  ]
  edge [
    source 16
    target 529
  ]
  edge [
    source 16
    target 530
  ]
  edge [
    source 16
    target 201
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 203
  ]
  edge [
    source 16
    target 531
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 532
  ]
  edge [
    source 16
    target 533
  ]
  edge [
    source 16
    target 534
  ]
  edge [
    source 16
    target 535
  ]
  edge [
    source 16
    target 536
  ]
  edge [
    source 16
    target 537
  ]
  edge [
    source 16
    target 538
  ]
  edge [
    source 16
    target 539
  ]
  edge [
    source 16
    target 540
  ]
  edge [
    source 16
    target 541
  ]
  edge [
    source 16
    target 542
  ]
  edge [
    source 16
    target 543
  ]
  edge [
    source 16
    target 544
  ]
  edge [
    source 16
    target 545
  ]
  edge [
    source 16
    target 546
  ]
  edge [
    source 16
    target 547
  ]
  edge [
    source 16
    target 548
  ]
  edge [
    source 16
    target 549
  ]
  edge [
    source 16
    target 550
  ]
  edge [
    source 16
    target 551
  ]
  edge [
    source 16
    target 552
  ]
  edge [
    source 16
    target 553
  ]
  edge [
    source 16
    target 554
  ]
  edge [
    source 16
    target 555
  ]
  edge [
    source 16
    target 556
  ]
  edge [
    source 16
    target 557
  ]
  edge [
    source 16
    target 558
  ]
  edge [
    source 16
    target 559
  ]
  edge [
    source 16
    target 560
  ]
  edge [
    source 16
    target 246
  ]
  edge [
    source 16
    target 561
  ]
  edge [
    source 16
    target 562
  ]
  edge [
    source 16
    target 563
  ]
  edge [
    source 16
    target 564
  ]
  edge [
    source 16
    target 565
  ]
  edge [
    source 16
    target 566
  ]
  edge [
    source 16
    target 567
  ]
  edge [
    source 16
    target 568
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 569
  ]
  edge [
    source 16
    target 570
  ]
  edge [
    source 16
    target 571
  ]
  edge [
    source 16
    target 572
  ]
  edge [
    source 16
    target 573
  ]
  edge [
    source 16
    target 574
  ]
  edge [
    source 16
    target 575
  ]
  edge [
    source 16
    target 576
  ]
  edge [
    source 16
    target 577
  ]
  edge [
    source 16
    target 578
  ]
  edge [
    source 16
    target 579
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 586
  ]
  edge [
    source 16
    target 587
  ]
  edge [
    source 16
    target 588
  ]
  edge [
    source 16
    target 589
  ]
  edge [
    source 16
    target 590
  ]
  edge [
    source 16
    target 591
  ]
  edge [
    source 16
    target 592
  ]
  edge [
    source 16
    target 593
  ]
  edge [
    source 16
    target 323
  ]
  edge [
    source 16
    target 594
  ]
  edge [
    source 16
    target 595
  ]
  edge [
    source 16
    target 596
  ]
  edge [
    source 16
    target 597
  ]
  edge [
    source 16
    target 598
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 599
  ]
  edge [
    source 16
    target 600
  ]
  edge [
    source 16
    target 601
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 602
  ]
  edge [
    source 16
    target 603
  ]
  edge [
    source 16
    target 604
  ]
  edge [
    source 16
    target 605
  ]
  edge [
    source 16
    target 606
  ]
  edge [
    source 16
    target 607
  ]
  edge [
    source 16
    target 608
  ]
  edge [
    source 16
    target 609
  ]
  edge [
    source 16
    target 610
  ]
  edge [
    source 16
    target 611
  ]
  edge [
    source 16
    target 612
  ]
  edge [
    source 16
    target 613
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 614
  ]
  edge [
    source 16
    target 615
  ]
  edge [
    source 16
    target 616
  ]
  edge [
    source 16
    target 617
  ]
  edge [
    source 16
    target 618
  ]
  edge [
    source 16
    target 619
  ]
  edge [
    source 16
    target 620
  ]
  edge [
    source 16
    target 621
  ]
  edge [
    source 16
    target 622
  ]
  edge [
    source 16
    target 623
  ]
  edge [
    source 16
    target 624
  ]
  edge [
    source 16
    target 625
  ]
  edge [
    source 16
    target 626
  ]
  edge [
    source 16
    target 627
  ]
  edge [
    source 16
    target 628
  ]
  edge [
    source 16
    target 629
  ]
  edge [
    source 16
    target 630
  ]
  edge [
    source 16
    target 631
  ]
  edge [
    source 16
    target 632
  ]
  edge [
    source 16
    target 633
  ]
  edge [
    source 16
    target 634
  ]
  edge [
    source 16
    target 635
  ]
  edge [
    source 16
    target 636
  ]
  edge [
    source 16
    target 637
  ]
  edge [
    source 16
    target 638
  ]
  edge [
    source 16
    target 639
  ]
  edge [
    source 16
    target 640
  ]
  edge [
    source 16
    target 641
  ]
  edge [
    source 16
    target 642
  ]
  edge [
    source 16
    target 643
  ]
  edge [
    source 16
    target 644
  ]
  edge [
    source 16
    target 645
  ]
  edge [
    source 16
    target 646
  ]
  edge [
    source 16
    target 647
  ]
  edge [
    source 16
    target 648
  ]
  edge [
    source 16
    target 649
  ]
  edge [
    source 16
    target 650
  ]
  edge [
    source 16
    target 651
  ]
  edge [
    source 16
    target 652
  ]
  edge [
    source 16
    target 653
  ]
  edge [
    source 16
    target 654
  ]
  edge [
    source 16
    target 655
  ]
  edge [
    source 16
    target 656
  ]
  edge [
    source 16
    target 657
  ]
  edge [
    source 16
    target 658
  ]
  edge [
    source 16
    target 659
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 661
  ]
  edge [
    source 16
    target 662
  ]
  edge [
    source 16
    target 663
  ]
  edge [
    source 16
    target 664
  ]
  edge [
    source 16
    target 665
  ]
  edge [
    source 16
    target 666
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 668
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 670
  ]
  edge [
    source 16
    target 671
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 673
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 677
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 679
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 682
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 684
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 694
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 698
  ]
  edge [
    source 16
    target 699
  ]
  edge [
    source 16
    target 700
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 702
  ]
  edge [
    source 16
    target 703
  ]
  edge [
    source 16
    target 704
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 771
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 16
    target 777
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 779
  ]
  edge [
    source 16
    target 780
  ]
  edge [
    source 16
    target 781
  ]
  edge [
    source 16
    target 782
  ]
  edge [
    source 16
    target 783
  ]
  edge [
    source 16
    target 784
  ]
  edge [
    source 16
    target 785
  ]
  edge [
    source 16
    target 786
  ]
  edge [
    source 16
    target 787
  ]
  edge [
    source 16
    target 788
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 198
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 595
  ]
  edge [
    source 19
    target 866
  ]
  edge [
    source 19
    target 867
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 19
    target 868
  ]
  edge [
    source 19
    target 869
  ]
  edge [
    source 19
    target 870
  ]
  edge [
    source 19
    target 871
  ]
  edge [
    source 19
    target 872
  ]
  edge [
    source 19
    target 873
  ]
  edge [
    source 19
    target 317
  ]
  edge [
    source 19
    target 92
  ]
  edge [
    source 19
    target 874
  ]
  edge [
    source 19
    target 875
  ]
  edge [
    source 19
    target 876
  ]
  edge [
    source 19
    target 877
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 878
  ]
  edge [
    source 19
    target 879
  ]
  edge [
    source 19
    target 880
  ]
  edge [
    source 19
    target 587
  ]
  edge [
    source 19
    target 512
  ]
  edge [
    source 19
    target 323
  ]
  edge [
    source 19
    target 881
  ]
  edge [
    source 19
    target 882
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 883
  ]
  edge [
    source 20
    target 884
  ]
  edge [
    source 20
    target 885
  ]
  edge [
    source 20
    target 886
  ]
  edge [
    source 20
    target 379
  ]
  edge [
    source 20
    target 887
  ]
  edge [
    source 20
    target 888
  ]
  edge [
    source 20
    target 889
  ]
  edge [
    source 20
    target 890
  ]
  edge [
    source 20
    target 891
  ]
  edge [
    source 20
    target 892
  ]
  edge [
    source 20
    target 893
  ]
  edge [
    source 20
    target 894
  ]
  edge [
    source 20
    target 895
  ]
  edge [
    source 20
    target 896
  ]
  edge [
    source 20
    target 897
  ]
  edge [
    source 20
    target 898
  ]
  edge [
    source 20
    target 899
  ]
  edge [
    source 20
    target 900
  ]
  edge [
    source 20
    target 901
  ]
  edge [
    source 20
    target 902
  ]
  edge [
    source 20
    target 903
  ]
  edge [
    source 20
    target 904
  ]
  edge [
    source 20
    target 905
  ]
  edge [
    source 20
    target 906
  ]
  edge [
    source 20
    target 907
  ]
  edge [
    source 20
    target 908
  ]
  edge [
    source 20
    target 909
  ]
  edge [
    source 20
    target 910
  ]
  edge [
    source 20
    target 911
  ]
  edge [
    source 20
    target 912
  ]
  edge [
    source 20
    target 913
  ]
  edge [
    source 20
    target 914
  ]
  edge [
    source 20
    target 915
  ]
  edge [
    source 20
    target 916
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 917
  ]
  edge [
    source 20
    target 306
  ]
  edge [
    source 20
    target 918
  ]
  edge [
    source 20
    target 919
  ]
  edge [
    source 20
    target 98
  ]
  edge [
    source 20
    target 920
  ]
  edge [
    source 20
    target 921
  ]
  edge [
    source 20
    target 922
  ]
  edge [
    source 20
    target 334
  ]
  edge [
    source 20
    target 923
  ]
  edge [
    source 20
    target 924
  ]
  edge [
    source 20
    target 925
  ]
  edge [
    source 20
    target 926
  ]
  edge [
    source 20
    target 927
  ]
  edge [
    source 20
    target 928
  ]
  edge [
    source 20
    target 370
  ]
  edge [
    source 20
    target 929
  ]
  edge [
    source 20
    target 930
  ]
  edge [
    source 20
    target 931
  ]
  edge [
    source 20
    target 932
  ]
  edge [
    source 20
    target 933
  ]
  edge [
    source 20
    target 934
  ]
  edge [
    source 20
    target 935
  ]
  edge [
    source 20
    target 936
  ]
  edge [
    source 20
    target 937
  ]
  edge [
    source 20
    target 938
  ]
  edge [
    source 20
    target 939
  ]
  edge [
    source 20
    target 940
  ]
  edge [
    source 20
    target 941
  ]
  edge [
    source 20
    target 942
  ]
  edge [
    source 20
    target 943
  ]
  edge [
    source 20
    target 944
  ]
  edge [
    source 20
    target 945
  ]
  edge [
    source 20
    target 946
  ]
  edge [
    source 20
    target 947
  ]
  edge [
    source 20
    target 948
  ]
  edge [
    source 20
    target 949
  ]
  edge [
    source 20
    target 950
  ]
  edge [
    source 20
    target 951
  ]
  edge [
    source 20
    target 952
  ]
  edge [
    source 20
    target 953
  ]
  edge [
    source 20
    target 954
  ]
  edge [
    source 20
    target 955
  ]
  edge [
    source 20
    target 956
  ]
  edge [
    source 20
    target 957
  ]
  edge [
    source 20
    target 958
  ]
  edge [
    source 20
    target 959
  ]
  edge [
    source 20
    target 960
  ]
  edge [
    source 20
    target 961
  ]
  edge [
    source 20
    target 962
  ]
  edge [
    source 20
    target 963
  ]
  edge [
    source 20
    target 964
  ]
  edge [
    source 20
    target 965
  ]
  edge [
    source 20
    target 966
  ]
  edge [
    source 20
    target 967
  ]
  edge [
    source 20
    target 968
  ]
  edge [
    source 20
    target 472
  ]
  edge [
    source 20
    target 969
  ]
  edge [
    source 20
    target 970
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 553
  ]
  edge [
    source 21
    target 971
  ]
  edge [
    source 21
    target 972
  ]
  edge [
    source 21
    target 973
  ]
  edge [
    source 21
    target 974
  ]
  edge [
    source 21
    target 975
  ]
  edge [
    source 21
    target 976
  ]
  edge [
    source 21
    target 977
  ]
  edge [
    source 21
    target 978
  ]
  edge [
    source 21
    target 979
  ]
  edge [
    source 21
    target 980
  ]
  edge [
    source 21
    target 981
  ]
  edge [
    source 21
    target 982
  ]
  edge [
    source 21
    target 983
  ]
  edge [
    source 21
    target 984
  ]
  edge [
    source 21
    target 985
  ]
  edge [
    source 21
    target 986
  ]
  edge [
    source 21
    target 987
  ]
  edge [
    source 21
    target 988
  ]
  edge [
    source 21
    target 989
  ]
  edge [
    source 21
    target 990
  ]
  edge [
    source 21
    target 991
  ]
  edge [
    source 21
    target 992
  ]
  edge [
    source 21
    target 993
  ]
  edge [
    source 21
    target 994
  ]
  edge [
    source 21
    target 995
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 996
  ]
  edge [
    source 22
    target 997
  ]
  edge [
    source 22
    target 998
  ]
  edge [
    source 22
    target 999
  ]
  edge [
    source 22
    target 1000
  ]
  edge [
    source 22
    target 1001
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 22
    target 1002
  ]
  edge [
    source 22
    target 1003
  ]
  edge [
    source 22
    target 1004
  ]
  edge [
    source 22
    target 1005
  ]
  edge [
    source 22
    target 1006
  ]
  edge [
    source 22
    target 1007
  ]
  edge [
    source 22
    target 1008
  ]
  edge [
    source 22
    target 1009
  ]
  edge [
    source 22
    target 133
  ]
  edge [
    source 22
    target 1010
  ]
  edge [
    source 22
    target 1011
  ]
  edge [
    source 22
    target 1012
  ]
  edge [
    source 22
    target 1013
  ]
  edge [
    source 22
    target 1014
  ]
  edge [
    source 22
    target 1015
  ]
  edge [
    source 22
    target 1016
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 255
  ]
  edge [
    source 23
    target 256
  ]
  edge [
    source 23
    target 257
  ]
  edge [
    source 23
    target 258
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 259
  ]
  edge [
    source 23
    target 260
  ]
  edge [
    source 23
    target 261
  ]
  edge [
    source 23
    target 95
  ]
  edge [
    source 23
    target 262
  ]
  edge [
    source 23
    target 263
  ]
  edge [
    source 23
    target 264
  ]
  edge [
    source 23
    target 265
  ]
  edge [
    source 23
    target 266
  ]
  edge [
    source 23
    target 267
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 226
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 23
    target 133
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 273
  ]
  edge [
    source 23
    target 274
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 277
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 23
    target 279
  ]
  edge [
    source 23
    target 280
  ]
  edge [
    source 23
    target 281
  ]
  edge [
    source 23
    target 230
  ]
  edge [
    source 23
    target 282
  ]
  edge [
    source 23
    target 283
  ]
  edge [
    source 23
    target 1017
  ]
  edge [
    source 23
    target 1018
  ]
  edge [
    source 23
    target 381
  ]
  edge [
    source 23
    target 1019
  ]
  edge [
    source 23
    target 662
  ]
  edge [
    source 23
    target 474
  ]
  edge [
    source 23
    target 1020
  ]
  edge [
    source 23
    target 1021
  ]
  edge [
    source 23
    target 1022
  ]
  edge [
    source 23
    target 1023
  ]
  edge [
    source 23
    target 1024
  ]
  edge [
    source 23
    target 229
  ]
  edge [
    source 23
    target 1025
  ]
  edge [
    source 23
    target 1026
  ]
  edge [
    source 23
    target 1027
  ]
  edge [
    source 23
    target 1028
  ]
  edge [
    source 23
    target 125
  ]
  edge [
    source 23
    target 1029
  ]
  edge [
    source 23
    target 1030
  ]
  edge [
    source 23
    target 1031
  ]
  edge [
    source 23
    target 1032
  ]
  edge [
    source 23
    target 1033
  ]
  edge [
    source 23
    target 1034
  ]
  edge [
    source 23
    target 1035
  ]
  edge [
    source 23
    target 1036
  ]
  edge [
    source 23
    target 1037
  ]
  edge [
    source 23
    target 1038
  ]
  edge [
    source 23
    target 1039
  ]
  edge [
    source 23
    target 1040
  ]
  edge [
    source 23
    target 1041
  ]
  edge [
    source 23
    target 999
  ]
  edge [
    source 23
    target 1042
  ]
  edge [
    source 23
    target 1043
  ]
  edge [
    source 23
    target 1044
  ]
  edge [
    source 23
    target 1045
  ]
  edge [
    source 23
    target 1046
  ]
  edge [
    source 23
    target 1047
  ]
  edge [
    source 23
    target 864
  ]
  edge [
    source 23
    target 1048
  ]
  edge [
    source 23
    target 880
  ]
  edge [
    source 23
    target 1049
  ]
  edge [
    source 23
    target 1050
  ]
  edge [
    source 23
    target 1051
  ]
  edge [
    source 23
    target 1052
  ]
  edge [
    source 23
    target 1053
  ]
  edge [
    source 23
    target 109
  ]
  edge [
    source 23
    target 1054
  ]
  edge [
    source 23
    target 1055
  ]
  edge [
    source 23
    target 1056
  ]
  edge [
    source 23
    target 1057
  ]
  edge [
    source 23
    target 1058
  ]
  edge [
    source 23
    target 1059
  ]
  edge [
    source 23
    target 1060
  ]
  edge [
    source 23
    target 1061
  ]
  edge [
    source 23
    target 1062
  ]
  edge [
    source 23
    target 1063
  ]
  edge [
    source 23
    target 1064
  ]
  edge [
    source 23
    target 1065
  ]
  edge [
    source 23
    target 1066
  ]
  edge [
    source 23
    target 1067
  ]
  edge [
    source 23
    target 1068
  ]
  edge [
    source 23
    target 871
  ]
  edge [
    source 23
    target 1069
  ]
  edge [
    source 23
    target 1070
  ]
  edge [
    source 23
    target 1071
  ]
  edge [
    source 23
    target 1072
  ]
  edge [
    source 23
    target 1073
  ]
  edge [
    source 23
    target 1074
  ]
  edge [
    source 23
    target 1075
  ]
  edge [
    source 23
    target 1076
  ]
  edge [
    source 23
    target 1077
  ]
  edge [
    source 23
    target 231
  ]
  edge [
    source 23
    target 1078
  ]
  edge [
    source 23
    target 1079
  ]
  edge [
    source 23
    target 1080
  ]
  edge [
    source 23
    target 1081
  ]
  edge [
    source 23
    target 873
  ]
  edge [
    source 23
    target 1082
  ]
  edge [
    source 23
    target 1083
  ]
  edge [
    source 23
    target 1084
  ]
  edge [
    source 23
    target 1085
  ]
  edge [
    source 23
    target 356
  ]
  edge [
    source 23
    target 1086
  ]
  edge [
    source 23
    target 1087
  ]
  edge [
    source 23
    target 1088
  ]
  edge [
    source 23
    target 1089
  ]
  edge [
    source 23
    target 1090
  ]
  edge [
    source 23
    target 1091
  ]
  edge [
    source 23
    target 1092
  ]
  edge [
    source 23
    target 1093
  ]
  edge [
    source 23
    target 1094
  ]
  edge [
    source 23
    target 1095
  ]
  edge [
    source 23
    target 1096
  ]
  edge [
    source 23
    target 1097
  ]
  edge [
    source 23
    target 156
  ]
  edge [
    source 23
    target 1098
  ]
  edge [
    source 23
    target 1099
  ]
  edge [
    source 23
    target 360
  ]
  edge [
    source 23
    target 1100
  ]
  edge [
    source 23
    target 1101
  ]
  edge [
    source 23
    target 1102
  ]
  edge [
    source 23
    target 1103
  ]
  edge [
    source 23
    target 1104
  ]
  edge [
    source 23
    target 237
  ]
  edge [
    source 23
    target 185
  ]
  edge [
    source 23
    target 1105
  ]
  edge [
    source 23
    target 1106
  ]
  edge [
    source 23
    target 1107
  ]
  edge [
    source 23
    target 1108
  ]
  edge [
    source 23
    target 1109
  ]
  edge [
    source 23
    target 92
  ]
  edge [
    source 23
    target 1110
  ]
  edge [
    source 23
    target 1111
  ]
  edge [
    source 23
    target 421
  ]
  edge [
    source 23
    target 1112
  ]
  edge [
    source 23
    target 1113
  ]
  edge [
    source 23
    target 1114
  ]
  edge [
    source 23
    target 1115
  ]
  edge [
    source 23
    target 1116
  ]
  edge [
    source 23
    target 1117
  ]
  edge [
    source 23
    target 996
  ]
  edge [
    source 23
    target 1118
  ]
  edge [
    source 23
    target 939
  ]
  edge [
    source 23
    target 1119
  ]
  edge [
    source 23
    target 1120
  ]
  edge [
    source 23
    target 1121
  ]
  edge [
    source 23
    target 1122
  ]
  edge [
    source 23
    target 1123
  ]
  edge [
    source 23
    target 1124
  ]
  edge [
    source 23
    target 1125
  ]
  edge [
    source 23
    target 535
  ]
  edge [
    source 23
    target 1126
  ]
  edge [
    source 23
    target 1127
  ]
  edge [
    source 23
    target 1128
  ]
  edge [
    source 23
    target 1129
  ]
  edge [
    source 23
    target 1130
  ]
  edge [
    source 23
    target 569
  ]
  edge [
    source 23
    target 1131
  ]
  edge [
    source 23
    target 1132
  ]
  edge [
    source 23
    target 82
  ]
  edge [
    source 23
    target 1133
  ]
  edge [
    source 23
    target 545
  ]
  edge [
    source 23
    target 1134
  ]
  edge [
    source 23
    target 118
  ]
  edge [
    source 23
    target 1135
  ]
  edge [
    source 23
    target 1136
  ]
  edge [
    source 23
    target 1137
  ]
  edge [
    source 23
    target 1138
  ]
  edge [
    source 23
    target 298
  ]
  edge [
    source 23
    target 299
  ]
  edge [
    source 23
    target 300
  ]
  edge [
    source 23
    target 301
  ]
  edge [
    source 23
    target 302
  ]
  edge [
    source 23
    target 303
  ]
  edge [
    source 23
    target 304
  ]
  edge [
    source 23
    target 305
  ]
  edge [
    source 23
    target 306
  ]
  edge [
    source 23
    target 307
  ]
  edge [
    source 23
    target 308
  ]
  edge [
    source 23
    target 309
  ]
  edge [
    source 23
    target 310
  ]
  edge [
    source 23
    target 311
  ]
  edge [
    source 23
    target 312
  ]
  edge [
    source 23
    target 313
  ]
  edge [
    source 23
    target 314
  ]
  edge [
    source 23
    target 315
  ]
  edge [
    source 23
    target 316
  ]
  edge [
    source 23
    target 317
  ]
  edge [
    source 23
    target 318
  ]
  edge [
    source 23
    target 319
  ]
  edge [
    source 23
    target 320
  ]
  edge [
    source 23
    target 321
  ]
  edge [
    source 23
    target 1139
  ]
  edge [
    source 23
    target 1140
  ]
  edge [
    source 23
    target 1141
  ]
  edge [
    source 23
    target 1142
  ]
  edge [
    source 23
    target 1143
  ]
  edge [
    source 23
    target 1144
  ]
  edge [
    source 23
    target 1145
  ]
  edge [
    source 23
    target 1146
  ]
  edge [
    source 23
    target 1147
  ]
  edge [
    source 23
    target 1148
  ]
  edge [
    source 23
    target 1149
  ]
  edge [
    source 23
    target 1150
  ]
  edge [
    source 23
    target 1151
  ]
  edge [
    source 23
    target 1152
  ]
  edge [
    source 23
    target 1153
  ]
  edge [
    source 23
    target 1154
  ]
  edge [
    source 23
    target 1155
  ]
  edge [
    source 23
    target 1156
  ]
  edge [
    source 23
    target 1157
  ]
  edge [
    source 23
    target 1158
  ]
  edge [
    source 23
    target 1159
  ]
  edge [
    source 23
    target 1160
  ]
  edge [
    source 23
    target 1161
  ]
  edge [
    source 23
    target 1162
  ]
  edge [
    source 23
    target 1163
  ]
  edge [
    source 23
    target 1164
  ]
  edge [
    source 23
    target 1165
  ]
  edge [
    source 23
    target 1166
  ]
  edge [
    source 23
    target 1167
  ]
  edge [
    source 23
    target 1168
  ]
  edge [
    source 23
    target 1169
  ]
  edge [
    source 23
    target 1170
  ]
  edge [
    source 23
    target 1171
  ]
  edge [
    source 23
    target 1172
  ]
  edge [
    source 23
    target 1173
  ]
  edge [
    source 23
    target 1174
  ]
  edge [
    source 23
    target 1175
  ]
  edge [
    source 23
    target 1176
  ]
  edge [
    source 23
    target 885
  ]
  edge [
    source 23
    target 1177
  ]
  edge [
    source 23
    target 1178
  ]
  edge [
    source 23
    target 1179
  ]
  edge [
    source 23
    target 1180
  ]
  edge [
    source 23
    target 1181
  ]
  edge [
    source 23
    target 1182
  ]
  edge [
    source 23
    target 1183
  ]
  edge [
    source 23
    target 1184
  ]
  edge [
    source 23
    target 561
  ]
  edge [
    source 23
    target 1185
  ]
  edge [
    source 23
    target 1186
  ]
  edge [
    source 23
    target 1187
  ]
  edge [
    source 23
    target 1188
  ]
  edge [
    source 23
    target 1189
  ]
  edge [
    source 23
    target 1190
  ]
  edge [
    source 23
    target 1191
  ]
  edge [
    source 23
    target 1192
  ]
  edge [
    source 23
    target 1193
  ]
  edge [
    source 23
    target 1194
  ]
  edge [
    source 23
    target 1195
  ]
  edge [
    source 23
    target 562
  ]
  edge [
    source 23
    target 1196
  ]
  edge [
    source 23
    target 1197
  ]
  edge [
    source 23
    target 1198
  ]
  edge [
    source 23
    target 1199
  ]
  edge [
    source 23
    target 1200
  ]
  edge [
    source 23
    target 1201
  ]
  edge [
    source 23
    target 1202
  ]
  edge [
    source 23
    target 1203
  ]
  edge [
    source 23
    target 1204
  ]
  edge [
    source 23
    target 1205
  ]
  edge [
    source 23
    target 1206
  ]
  edge [
    source 23
    target 1207
  ]
  edge [
    source 23
    target 1208
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 1209
  ]
  edge [
    source 23
    target 1210
  ]
  edge [
    source 23
    target 228
  ]
  edge [
    source 23
    target 1211
  ]
  edge [
    source 23
    target 531
  ]
  edge [
    source 23
    target 1212
  ]
  edge [
    source 23
    target 1213
  ]
  edge [
    source 23
    target 1214
  ]
  edge [
    source 23
    target 1215
  ]
  edge [
    source 23
    target 1216
  ]
  edge [
    source 23
    target 1217
  ]
  edge [
    source 23
    target 1218
  ]
  edge [
    source 23
    target 1219
  ]
  edge [
    source 23
    target 1220
  ]
  edge [
    source 23
    target 1221
  ]
  edge [
    source 23
    target 1222
  ]
  edge [
    source 23
    target 1223
  ]
  edge [
    source 23
    target 372
  ]
  edge [
    source 23
    target 1224
  ]
  edge [
    source 23
    target 1225
  ]
  edge [
    source 23
    target 1226
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 1227
  ]
  edge [
    source 23
    target 1228
  ]
  edge [
    source 23
    target 1229
  ]
  edge [
    source 23
    target 1230
  ]
  edge [
    source 23
    target 1231
  ]
  edge [
    source 23
    target 1232
  ]
  edge [
    source 23
    target 1233
  ]
  edge [
    source 23
    target 1234
  ]
  edge [
    source 23
    target 149
  ]
  edge [
    source 23
    target 1235
  ]
  edge [
    source 23
    target 1236
  ]
  edge [
    source 23
    target 1237
  ]
  edge [
    source 23
    target 1238
  ]
  edge [
    source 23
    target 1239
  ]
  edge [
    source 23
    target 1240
  ]
  edge [
    source 23
    target 1241
  ]
  edge [
    source 23
    target 1242
  ]
  edge [
    source 23
    target 1243
  ]
  edge [
    source 23
    target 1244
  ]
  edge [
    source 23
    target 1245
  ]
  edge [
    source 23
    target 1246
  ]
  edge [
    source 23
    target 534
  ]
  edge [
    source 23
    target 1247
  ]
  edge [
    source 23
    target 1248
  ]
  edge [
    source 23
    target 1249
  ]
  edge [
    source 23
    target 1250
  ]
  edge [
    source 23
    target 1251
  ]
  edge [
    source 23
    target 1252
  ]
  edge [
    source 23
    target 1253
  ]
  edge [
    source 23
    target 1254
  ]
  edge [
    source 23
    target 1255
  ]
  edge [
    source 23
    target 1256
  ]
  edge [
    source 23
    target 536
  ]
  edge [
    source 23
    target 1257
  ]
  edge [
    source 23
    target 1258
  ]
  edge [
    source 23
    target 1259
  ]
  edge [
    source 23
    target 1260
  ]
  edge [
    source 23
    target 1261
  ]
  edge [
    source 23
    target 1262
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 1263
  ]
  edge [
    source 23
    target 1264
  ]
  edge [
    source 23
    target 1265
  ]
  edge [
    source 23
    target 1266
  ]
  edge [
    source 23
    target 1267
  ]
  edge [
    source 23
    target 1268
  ]
  edge [
    source 23
    target 402
  ]
  edge [
    source 23
    target 1269
  ]
  edge [
    source 23
    target 1270
  ]
  edge [
    source 23
    target 1271
  ]
  edge [
    source 23
    target 1272
  ]
  edge [
    source 23
    target 1273
  ]
  edge [
    source 23
    target 1274
  ]
  edge [
    source 23
    target 1275
  ]
  edge [
    source 23
    target 1276
  ]
  edge [
    source 23
    target 374
  ]
  edge [
    source 23
    target 1277
  ]
  edge [
    source 23
    target 1278
  ]
  edge [
    source 23
    target 1279
  ]
  edge [
    source 23
    target 1280
  ]
  edge [
    source 23
    target 433
  ]
  edge [
    source 23
    target 1281
  ]
  edge [
    source 23
    target 1282
  ]
  edge [
    source 23
    target 1283
  ]
  edge [
    source 23
    target 1284
  ]
  edge [
    source 23
    target 1285
  ]
  edge [
    source 23
    target 1286
  ]
  edge [
    source 23
    target 1287
  ]
  edge [
    source 23
    target 1288
  ]
  edge [
    source 23
    target 1289
  ]
  edge [
    source 23
    target 103
  ]
  edge [
    source 24
    target 1290
  ]
  edge [
    source 24
    target 1291
  ]
  edge [
    source 24
    target 317
  ]
  edge [
    source 24
    target 1292
  ]
  edge [
    source 24
    target 1293
  ]
  edge [
    source 24
    target 1294
  ]
  edge [
    source 24
    target 1295
  ]
  edge [
    source 24
    target 1296
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 1297
  ]
  edge [
    source 24
    target 1298
  ]
  edge [
    source 24
    target 302
  ]
  edge [
    source 24
    target 662
  ]
  edge [
    source 24
    target 1299
  ]
  edge [
    source 24
    target 1300
  ]
  edge [
    source 24
    target 1301
  ]
  edge [
    source 24
    target 1302
  ]
  edge [
    source 24
    target 1303
  ]
  edge [
    source 24
    target 305
  ]
  edge [
    source 24
    target 1304
  ]
  edge [
    source 24
    target 1305
  ]
  edge [
    source 24
    target 1306
  ]
  edge [
    source 24
    target 1307
  ]
  edge [
    source 24
    target 1308
  ]
  edge [
    source 24
    target 1309
  ]
  edge [
    source 24
    target 1310
  ]
  edge [
    source 24
    target 1311
  ]
  edge [
    source 24
    target 344
  ]
  edge [
    source 24
    target 194
  ]
  edge [
    source 24
    target 1312
  ]
  edge [
    source 24
    target 920
  ]
  edge [
    source 24
    target 230
  ]
  edge [
    source 24
    target 1313
  ]
  edge [
    source 24
    target 1314
  ]
  edge [
    source 24
    target 1315
  ]
  edge [
    source 24
    target 1132
  ]
  edge [
    source 24
    target 1316
  ]
  edge [
    source 24
    target 1317
  ]
  edge [
    source 24
    target 1318
  ]
  edge [
    source 24
    target 1319
  ]
  edge [
    source 24
    target 1320
  ]
  edge [
    source 24
    target 1279
  ]
  edge [
    source 24
    target 1321
  ]
]
