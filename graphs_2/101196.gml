graph [
  node [
    id 0
    label "alojza"
    origin "text"
  ]
  node [
    id 1
    label "szcz&#281;&#347;niak"
    origin "text"
  ]
  node [
    id 2
    label "Alojzy"
  ]
  node [
    id 3
    label "Szcz&#281;&#347;niak"
  ]
  node [
    id 4
    label "zjednoczy&#263;"
  ]
  node [
    id 5
    label "stronnictwo"
  ]
  node [
    id 6
    label "ludowy"
  ]
  node [
    id 7
    label "komisja"
  ]
  node [
    id 8
    label "ochrona"
  ]
  node [
    id 9
    label "&#347;rodowisko"
  ]
  node [
    id 10
    label "i"
  ]
  node [
    id 11
    label "zas&#243;b"
  ]
  node [
    id 12
    label "naturalny"
  ]
  node [
    id 13
    label "edukacja"
  ]
  node [
    id 14
    label "narodowy"
  ]
  node [
    id 15
    label "m&#322;odzie&#380;"
  ]
  node [
    id 16
    label "polskie"
  ]
  node [
    id 17
    label "suchy"
  ]
  node [
    id 18
    label "beskidzki"
  ]
  node [
    id 19
    label "zesp&#243;&#322;"
  ]
  node [
    id 20
    label "szko&#322;a"
  ]
  node [
    id 21
    label "on"
  ]
  node [
    id 22
    label "Wincenty"
  ]
  node [
    id 23
    label "Witos"
  ]
  node [
    id 24
    label "wyspa"
  ]
  node [
    id 25
    label "ZS"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
]
