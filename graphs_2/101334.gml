graph [
  node [
    id 0
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 1
    label "bojowy"
    origin "text"
  ]
  node [
    id 2
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 3
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 4
    label "strategia"
  ]
  node [
    id 5
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 6
    label "operacja"
  ]
  node [
    id 7
    label "doktryna"
  ]
  node [
    id 8
    label "plan"
  ]
  node [
    id 9
    label "gra"
  ]
  node [
    id 10
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 11
    label "pocz&#261;tki"
  ]
  node [
    id 12
    label "dokument"
  ]
  node [
    id 13
    label "dziedzina"
  ]
  node [
    id 14
    label "metoda"
  ]
  node [
    id 15
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 16
    label "program"
  ]
  node [
    id 17
    label "wrinkle"
  ]
  node [
    id 18
    label "wzorzec_projektowy"
  ]
  node [
    id 19
    label "activity"
  ]
  node [
    id 20
    label "absolutorium"
  ]
  node [
    id 21
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 22
    label "dzia&#322;anie"
  ]
  node [
    id 23
    label "pewny"
  ]
  node [
    id 24
    label "bojowo"
  ]
  node [
    id 25
    label "bojowniczy"
  ]
  node [
    id 26
    label "zadziorny"
  ]
  node [
    id 27
    label "&#347;mia&#322;y"
  ]
  node [
    id 28
    label "waleczny"
  ]
  node [
    id 29
    label "&#347;mia&#322;o"
  ]
  node [
    id 30
    label "dziarski"
  ]
  node [
    id 31
    label "odwa&#380;ny"
  ]
  node [
    id 32
    label "zadziornie"
  ]
  node [
    id 33
    label "temperamentny"
  ]
  node [
    id 34
    label "niepokorny"
  ]
  node [
    id 35
    label "charakterny"
  ]
  node [
    id 36
    label "ochoczy"
  ]
  node [
    id 37
    label "walecznie"
  ]
  node [
    id 38
    label "upewnienie_si&#281;"
  ]
  node [
    id 39
    label "wiarygodny"
  ]
  node [
    id 40
    label "spokojny"
  ]
  node [
    id 41
    label "mo&#380;liwy"
  ]
  node [
    id 42
    label "ufanie"
  ]
  node [
    id 43
    label "wierzenie"
  ]
  node [
    id 44
    label "upewnianie_si&#281;"
  ]
  node [
    id 45
    label "bezpieczny"
  ]
  node [
    id 46
    label "pewnie"
  ]
  node [
    id 47
    label "wojennie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
]
