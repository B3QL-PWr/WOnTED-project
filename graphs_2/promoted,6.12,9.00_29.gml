graph [
  node [
    id 0
    label "pa&#324;stwowy"
    origin "text"
  ]
  node [
    id 1
    label "pe&#322;nia"
    origin "text"
  ]
  node [
    id 2
    label "legalny"
    origin "text"
  ]
  node [
    id 3
    label "narodowy"
    origin "text"
  ]
  node [
    id 4
    label "kasyno"
    origin "text"
  ]
  node [
    id 5
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 6
    label "dzia&#322;o"
    origin "text"
  ]
  node [
    id 7
    label "upa&#324;stwowienie"
  ]
  node [
    id 8
    label "wsp&#243;lny"
  ]
  node [
    id 9
    label "pa&#324;stwowo"
  ]
  node [
    id 10
    label "upa&#324;stwawianie"
  ]
  node [
    id 11
    label "spolny"
  ]
  node [
    id 12
    label "wsp&#243;lnie"
  ]
  node [
    id 13
    label "sp&#243;lny"
  ]
  node [
    id 14
    label "jeden"
  ]
  node [
    id 15
    label "uwsp&#243;lnienie"
  ]
  node [
    id 16
    label "uwsp&#243;lnianie"
  ]
  node [
    id 17
    label "przebudowywanie"
  ]
  node [
    id 18
    label "darmowo"
  ]
  node [
    id 19
    label "zreorganizowanie"
  ]
  node [
    id 20
    label "nationalization"
  ]
  node [
    id 21
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 22
    label "szczyt"
  ]
  node [
    id 23
    label "zakres"
  ]
  node [
    id 24
    label "zwie&#324;czenie"
  ]
  node [
    id 25
    label "Wielka_Racza"
  ]
  node [
    id 26
    label "koniec"
  ]
  node [
    id 27
    label "&#346;winica"
  ]
  node [
    id 28
    label "&#346;l&#281;&#380;a"
  ]
  node [
    id 29
    label "Che&#322;miec"
  ]
  node [
    id 30
    label "wierzcho&#322;"
  ]
  node [
    id 31
    label "wierzcho&#322;ek"
  ]
  node [
    id 32
    label "Radunia"
  ]
  node [
    id 33
    label "Barania_G&#243;ra"
  ]
  node [
    id 34
    label "Groniczki"
  ]
  node [
    id 35
    label "wierch"
  ]
  node [
    id 36
    label "konferencja"
  ]
  node [
    id 37
    label "Czupel"
  ]
  node [
    id 38
    label "&#347;ciana"
  ]
  node [
    id 39
    label "Jaworz"
  ]
  node [
    id 40
    label "Okr&#261;glica"
  ]
  node [
    id 41
    label "Walig&#243;ra"
  ]
  node [
    id 42
    label "bok"
  ]
  node [
    id 43
    label "Wielka_Sowa"
  ]
  node [
    id 44
    label "Ko&#322;owr&#243;t"
  ]
  node [
    id 45
    label "&#321;omnica"
  ]
  node [
    id 46
    label "wzniesienie"
  ]
  node [
    id 47
    label "Beskid"
  ]
  node [
    id 48
    label "fasada"
  ]
  node [
    id 49
    label "Wo&#322;ek"
  ]
  node [
    id 50
    label "summit"
  ]
  node [
    id 51
    label "Rysianka"
  ]
  node [
    id 52
    label "Mody&#324;"
  ]
  node [
    id 53
    label "poziom"
  ]
  node [
    id 54
    label "wzmo&#380;enie"
  ]
  node [
    id 55
    label "czas"
  ]
  node [
    id 56
    label "Obidowa"
  ]
  node [
    id 57
    label "Jaworzyna"
  ]
  node [
    id 58
    label "godzina_szczytu"
  ]
  node [
    id 59
    label "Turbacz"
  ]
  node [
    id 60
    label "Rudawiec"
  ]
  node [
    id 61
    label "g&#243;ra"
  ]
  node [
    id 62
    label "Ja&#322;owiec"
  ]
  node [
    id 63
    label "Wielki_Chocz"
  ]
  node [
    id 64
    label "Orlica"
  ]
  node [
    id 65
    label "Szrenica"
  ]
  node [
    id 66
    label "&#346;nie&#380;nik"
  ]
  node [
    id 67
    label "Cubryna"
  ]
  node [
    id 68
    label "Wielki_Bukowiec"
  ]
  node [
    id 69
    label "Magura"
  ]
  node [
    id 70
    label "korona"
  ]
  node [
    id 71
    label "Czarna_G&#243;ra"
  ]
  node [
    id 72
    label "Lubogoszcz"
  ]
  node [
    id 73
    label "sfera"
  ]
  node [
    id 74
    label "granica"
  ]
  node [
    id 75
    label "zbi&#243;r"
  ]
  node [
    id 76
    label "wielko&#347;&#263;"
  ]
  node [
    id 77
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 78
    label "podzakres"
  ]
  node [
    id 79
    label "dziedzina"
  ]
  node [
    id 80
    label "desygnat"
  ]
  node [
    id 81
    label "circle"
  ]
  node [
    id 82
    label "gajny"
  ]
  node [
    id 83
    label "legalnie"
  ]
  node [
    id 84
    label "gajowy"
  ]
  node [
    id 85
    label "legally"
  ]
  node [
    id 86
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 87
    label "nacjonalistyczny"
  ]
  node [
    id 88
    label "narodowo"
  ]
  node [
    id 89
    label "wa&#380;ny"
  ]
  node [
    id 90
    label "wynios&#322;y"
  ]
  node [
    id 91
    label "dono&#347;ny"
  ]
  node [
    id 92
    label "silny"
  ]
  node [
    id 93
    label "wa&#380;nie"
  ]
  node [
    id 94
    label "istotnie"
  ]
  node [
    id 95
    label "znaczny"
  ]
  node [
    id 96
    label "eksponowany"
  ]
  node [
    id 97
    label "dobry"
  ]
  node [
    id 98
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 99
    label "nale&#380;ny"
  ]
  node [
    id 100
    label "nale&#380;yty"
  ]
  node [
    id 101
    label "typowy"
  ]
  node [
    id 102
    label "uprawniony"
  ]
  node [
    id 103
    label "zasadniczy"
  ]
  node [
    id 104
    label "stosownie"
  ]
  node [
    id 105
    label "taki"
  ]
  node [
    id 106
    label "charakterystyczny"
  ]
  node [
    id 107
    label "prawdziwy"
  ]
  node [
    id 108
    label "ten"
  ]
  node [
    id 109
    label "polityczny"
  ]
  node [
    id 110
    label "nacjonalistycznie"
  ]
  node [
    id 111
    label "narodowo&#347;ciowy"
  ]
  node [
    id 112
    label "gastronomia"
  ]
  node [
    id 113
    label "zak&#322;ad"
  ]
  node [
    id 114
    label "krupier"
  ]
  node [
    id 115
    label "lokal"
  ]
  node [
    id 116
    label "dom"
  ]
  node [
    id 117
    label "miejsce"
  ]
  node [
    id 118
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 119
    label "rodzina"
  ]
  node [
    id 120
    label "substancja_mieszkaniowa"
  ]
  node [
    id 121
    label "instytucja"
  ]
  node [
    id 122
    label "siedziba"
  ]
  node [
    id 123
    label "dom_rodzinny"
  ]
  node [
    id 124
    label "budynek"
  ]
  node [
    id 125
    label "grupa"
  ]
  node [
    id 126
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 127
    label "poj&#281;cie"
  ]
  node [
    id 128
    label "stead"
  ]
  node [
    id 129
    label "garderoba"
  ]
  node [
    id 130
    label "wiecha"
  ]
  node [
    id 131
    label "fratria"
  ]
  node [
    id 132
    label "kuchnia"
  ]
  node [
    id 133
    label "horeca"
  ]
  node [
    id 134
    label "sztuka"
  ]
  node [
    id 135
    label "us&#322;ugi"
  ]
  node [
    id 136
    label "zak&#322;adka"
  ]
  node [
    id 137
    label "jednostka_organizacyjna"
  ]
  node [
    id 138
    label "miejsce_pracy"
  ]
  node [
    id 139
    label "wyko&#324;czenie"
  ]
  node [
    id 140
    label "firma"
  ]
  node [
    id 141
    label "czyn"
  ]
  node [
    id 142
    label "company"
  ]
  node [
    id 143
    label "instytut"
  ]
  node [
    id 144
    label "umowa"
  ]
  node [
    id 145
    label "bankier"
  ]
  node [
    id 146
    label "dom_gry"
  ]
  node [
    id 147
    label "artyleria"
  ]
  node [
    id 148
    label "laweta"
  ]
  node [
    id 149
    label "cannon"
  ]
  node [
    id 150
    label "waln&#261;&#263;"
  ]
  node [
    id 151
    label "bateria_artylerii"
  ]
  node [
    id 152
    label "bro&#324;_artyleryjska"
  ]
  node [
    id 153
    label "oporopowrotnik"
  ]
  node [
    id 154
    label "przedmuchiwacz"
  ]
  node [
    id 155
    label "bateria"
  ]
  node [
    id 156
    label "bro&#324;"
  ]
  node [
    id 157
    label "amunicja"
  ]
  node [
    id 158
    label "karta_przetargowa"
  ]
  node [
    id 159
    label "rozbrojenie"
  ]
  node [
    id 160
    label "rozbroi&#263;"
  ]
  node [
    id 161
    label "osprz&#281;t"
  ]
  node [
    id 162
    label "uzbrojenie"
  ]
  node [
    id 163
    label "przyrz&#261;d"
  ]
  node [
    id 164
    label "rozbrajanie"
  ]
  node [
    id 165
    label "rozbraja&#263;"
  ]
  node [
    id 166
    label "or&#281;&#380;"
  ]
  node [
    id 167
    label "urz&#261;dzenie"
  ]
  node [
    id 168
    label "przyczepa"
  ]
  node [
    id 169
    label "podstawa"
  ]
  node [
    id 170
    label "lemiesz"
  ]
  node [
    id 171
    label "powrotnik"
  ]
  node [
    id 172
    label "formacja"
  ]
  node [
    id 173
    label "armia"
  ]
  node [
    id 174
    label "artillery"
  ]
  node [
    id 175
    label "munition"
  ]
  node [
    id 176
    label "zaw&#243;r"
  ]
  node [
    id 177
    label "kolekcja"
  ]
  node [
    id 178
    label "oficer_ogniowy"
  ]
  node [
    id 179
    label "dywizjon_artylerii"
  ]
  node [
    id 180
    label "kran"
  ]
  node [
    id 181
    label "pododdzia&#322;"
  ]
  node [
    id 182
    label "cell"
  ]
  node [
    id 183
    label "pluton"
  ]
  node [
    id 184
    label "odkr&#281;ca&#263;_wod&#281;"
  ]
  node [
    id 185
    label "odkr&#281;ci&#263;_wod&#281;"
  ]
  node [
    id 186
    label "dzia&#322;obitnia"
  ]
  node [
    id 187
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 188
    label "sygn&#261;&#263;"
  ]
  node [
    id 189
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 190
    label "peddle"
  ]
  node [
    id 191
    label "jebn&#261;&#263;"
  ]
  node [
    id 192
    label "fall"
  ]
  node [
    id 193
    label "rap"
  ]
  node [
    id 194
    label "uderzy&#263;"
  ]
  node [
    id 195
    label "zrobi&#263;"
  ]
  node [
    id 196
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 197
    label "zmieni&#263;"
  ]
  node [
    id 198
    label "majdn&#261;&#263;"
  ]
  node [
    id 199
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 200
    label "paln&#261;&#263;"
  ]
  node [
    id 201
    label "strzeli&#263;"
  ]
  node [
    id 202
    label "lumber"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
]
