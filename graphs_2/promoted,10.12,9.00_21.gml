graph [
  node [
    id 0
    label "strategia"
    origin "text"
  ]
  node [
    id 1
    label "reprodukcyjny"
    origin "text"
  ]
  node [
    id 2
    label "rodzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "zjawisko"
    origin "text"
  ]
  node [
    id 5
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 6
    label "jaki"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "samotny"
    origin "text"
  ]
  node [
    id 9
    label "matka"
    origin "text"
  ]
  node [
    id 10
    label "brak"
    origin "text"
  ]
  node [
    id 11
    label "ojciec"
    origin "text"
  ]
  node [
    id 12
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 13
    label "prywatny"
    origin "text"
  ]
  node [
    id 14
    label "problem"
    origin "text"
  ]
  node [
    id 15
    label "dziecko"
    origin "text"
  ]
  node [
    id 16
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 17
    label "prawo"
    origin "text"
  ]
  node [
    id 18
    label "m&#281;ski"
    origin "text"
  ]
  node [
    id 19
    label "wzorzec"
    origin "text"
  ]
  node [
    id 20
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 21
    label "plan"
  ]
  node [
    id 22
    label "operacja"
  ]
  node [
    id 23
    label "metoda"
  ]
  node [
    id 24
    label "gra"
  ]
  node [
    id 25
    label "pocz&#261;tki"
  ]
  node [
    id 26
    label "wzorzec_projektowy"
  ]
  node [
    id 27
    label "dziedzina"
  ]
  node [
    id 28
    label "program"
  ]
  node [
    id 29
    label "doktryna"
  ]
  node [
    id 30
    label "wrinkle"
  ]
  node [
    id 31
    label "dokument"
  ]
  node [
    id 32
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 33
    label "zmienno&#347;&#263;"
  ]
  node [
    id 34
    label "play"
  ]
  node [
    id 35
    label "rozgrywka"
  ]
  node [
    id 36
    label "apparent_motion"
  ]
  node [
    id 37
    label "wydarzenie"
  ]
  node [
    id 38
    label "contest"
  ]
  node [
    id 39
    label "akcja"
  ]
  node [
    id 40
    label "komplet"
  ]
  node [
    id 41
    label "zabawa"
  ]
  node [
    id 42
    label "zasada"
  ]
  node [
    id 43
    label "rywalizacja"
  ]
  node [
    id 44
    label "zbijany"
  ]
  node [
    id 45
    label "post&#281;powanie"
  ]
  node [
    id 46
    label "game"
  ]
  node [
    id 47
    label "odg&#322;os"
  ]
  node [
    id 48
    label "Pok&#233;mon"
  ]
  node [
    id 49
    label "czynno&#347;&#263;"
  ]
  node [
    id 50
    label "synteza"
  ]
  node [
    id 51
    label "odtworzenie"
  ]
  node [
    id 52
    label "rekwizyt_do_gry"
  ]
  node [
    id 53
    label "instalowa&#263;"
  ]
  node [
    id 54
    label "oprogramowanie"
  ]
  node [
    id 55
    label "odinstalowywa&#263;"
  ]
  node [
    id 56
    label "spis"
  ]
  node [
    id 57
    label "zaprezentowanie"
  ]
  node [
    id 58
    label "podprogram"
  ]
  node [
    id 59
    label "ogranicznik_referencyjny"
  ]
  node [
    id 60
    label "course_of_study"
  ]
  node [
    id 61
    label "booklet"
  ]
  node [
    id 62
    label "dzia&#322;"
  ]
  node [
    id 63
    label "odinstalowanie"
  ]
  node [
    id 64
    label "broszura"
  ]
  node [
    id 65
    label "wytw&#243;r"
  ]
  node [
    id 66
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 67
    label "kana&#322;"
  ]
  node [
    id 68
    label "teleferie"
  ]
  node [
    id 69
    label "zainstalowanie"
  ]
  node [
    id 70
    label "struktura_organizacyjna"
  ]
  node [
    id 71
    label "pirat"
  ]
  node [
    id 72
    label "zaprezentowa&#263;"
  ]
  node [
    id 73
    label "prezentowanie"
  ]
  node [
    id 74
    label "prezentowa&#263;"
  ]
  node [
    id 75
    label "interfejs"
  ]
  node [
    id 76
    label "okno"
  ]
  node [
    id 77
    label "blok"
  ]
  node [
    id 78
    label "punkt"
  ]
  node [
    id 79
    label "folder"
  ]
  node [
    id 80
    label "zainstalowa&#263;"
  ]
  node [
    id 81
    label "za&#322;o&#380;enie"
  ]
  node [
    id 82
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 83
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 84
    label "ram&#243;wka"
  ]
  node [
    id 85
    label "tryb"
  ]
  node [
    id 86
    label "emitowa&#263;"
  ]
  node [
    id 87
    label "emitowanie"
  ]
  node [
    id 88
    label "odinstalowywanie"
  ]
  node [
    id 89
    label "instrukcja"
  ]
  node [
    id 90
    label "informatyka"
  ]
  node [
    id 91
    label "deklaracja"
  ]
  node [
    id 92
    label "sekcja_krytyczna"
  ]
  node [
    id 93
    label "menu"
  ]
  node [
    id 94
    label "furkacja"
  ]
  node [
    id 95
    label "podstawa"
  ]
  node [
    id 96
    label "instalowanie"
  ]
  node [
    id 97
    label "oferta"
  ]
  node [
    id 98
    label "odinstalowa&#263;"
  ]
  node [
    id 99
    label "zapis"
  ]
  node [
    id 100
    label "&#347;wiadectwo"
  ]
  node [
    id 101
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 102
    label "parafa"
  ]
  node [
    id 103
    label "plik"
  ]
  node [
    id 104
    label "raport&#243;wka"
  ]
  node [
    id 105
    label "utw&#243;r"
  ]
  node [
    id 106
    label "record"
  ]
  node [
    id 107
    label "registratura"
  ]
  node [
    id 108
    label "dokumentacja"
  ]
  node [
    id 109
    label "fascyku&#322;"
  ]
  node [
    id 110
    label "artyku&#322;"
  ]
  node [
    id 111
    label "writing"
  ]
  node [
    id 112
    label "sygnatariusz"
  ]
  node [
    id 113
    label "model"
  ]
  node [
    id 114
    label "intencja"
  ]
  node [
    id 115
    label "rysunek"
  ]
  node [
    id 116
    label "miejsce_pracy"
  ]
  node [
    id 117
    label "przestrze&#324;"
  ]
  node [
    id 118
    label "device"
  ]
  node [
    id 119
    label "pomys&#322;"
  ]
  node [
    id 120
    label "obraz"
  ]
  node [
    id 121
    label "reprezentacja"
  ]
  node [
    id 122
    label "agreement"
  ]
  node [
    id 123
    label "dekoracja"
  ]
  node [
    id 124
    label "perspektywa"
  ]
  node [
    id 125
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 126
    label "sfera"
  ]
  node [
    id 127
    label "zbi&#243;r"
  ]
  node [
    id 128
    label "zakres"
  ]
  node [
    id 129
    label "funkcja"
  ]
  node [
    id 130
    label "bezdro&#380;e"
  ]
  node [
    id 131
    label "poddzia&#322;"
  ]
  node [
    id 132
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 133
    label "method"
  ]
  node [
    id 134
    label "spos&#243;b"
  ]
  node [
    id 135
    label "proces_my&#347;lowy"
  ]
  node [
    id 136
    label "liczenie"
  ]
  node [
    id 137
    label "czyn"
  ]
  node [
    id 138
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 139
    label "supremum"
  ]
  node [
    id 140
    label "laparotomia"
  ]
  node [
    id 141
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 142
    label "jednostka"
  ]
  node [
    id 143
    label "matematyka"
  ]
  node [
    id 144
    label "rzut"
  ]
  node [
    id 145
    label "liczy&#263;"
  ]
  node [
    id 146
    label "torakotomia"
  ]
  node [
    id 147
    label "chirurg"
  ]
  node [
    id 148
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 149
    label "zabieg"
  ]
  node [
    id 150
    label "szew"
  ]
  node [
    id 151
    label "mathematical_process"
  ]
  node [
    id 152
    label "infimum"
  ]
  node [
    id 153
    label "teoria"
  ]
  node [
    id 154
    label "doctrine"
  ]
  node [
    id 155
    label "background"
  ]
  node [
    id 156
    label "dzieci&#281;ctwo"
  ]
  node [
    id 157
    label "struktura"
  ]
  node [
    id 158
    label "podsektor"
  ]
  node [
    id 159
    label "balistyka"
  ]
  node [
    id 160
    label "fortyfikacja"
  ]
  node [
    id 161
    label "taktyka"
  ]
  node [
    id 162
    label "or&#281;&#380;"
  ]
  node [
    id 163
    label "create"
  ]
  node [
    id 164
    label "plon"
  ]
  node [
    id 165
    label "give"
  ]
  node [
    id 166
    label "wytwarza&#263;"
  ]
  node [
    id 167
    label "robi&#263;"
  ]
  node [
    id 168
    label "return"
  ]
  node [
    id 169
    label "wydawa&#263;"
  ]
  node [
    id 170
    label "metr"
  ]
  node [
    id 171
    label "wyda&#263;"
  ]
  node [
    id 172
    label "rezultat"
  ]
  node [
    id 173
    label "produkcja"
  ]
  node [
    id 174
    label "naturalia"
  ]
  node [
    id 175
    label "proces"
  ]
  node [
    id 176
    label "boski"
  ]
  node [
    id 177
    label "krajobraz"
  ]
  node [
    id 178
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 179
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 180
    label "przywidzenie"
  ]
  node [
    id 181
    label "presence"
  ]
  node [
    id 182
    label "charakter"
  ]
  node [
    id 183
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 184
    label "jako&#347;&#263;"
  ]
  node [
    id 185
    label "wygl&#261;d"
  ]
  node [
    id 186
    label "cz&#322;owiek"
  ]
  node [
    id 187
    label "gust"
  ]
  node [
    id 188
    label "drobiazg"
  ]
  node [
    id 189
    label "kobieta"
  ]
  node [
    id 190
    label "beauty"
  ]
  node [
    id 191
    label "kalokagatia"
  ]
  node [
    id 192
    label "cecha"
  ]
  node [
    id 193
    label "prettiness"
  ]
  node [
    id 194
    label "ozdoba"
  ]
  node [
    id 195
    label "rzadko&#347;&#263;"
  ]
  node [
    id 196
    label "nadprzyrodzony"
  ]
  node [
    id 197
    label "&#347;mieszny"
  ]
  node [
    id 198
    label "wspania&#322;y"
  ]
  node [
    id 199
    label "bezpretensjonalny"
  ]
  node [
    id 200
    label "bosko"
  ]
  node [
    id 201
    label "nale&#380;ny"
  ]
  node [
    id 202
    label "wyj&#261;tkowy"
  ]
  node [
    id 203
    label "fantastyczny"
  ]
  node [
    id 204
    label "arcypi&#281;kny"
  ]
  node [
    id 205
    label "cudnie"
  ]
  node [
    id 206
    label "cudowny"
  ]
  node [
    id 207
    label "udany"
  ]
  node [
    id 208
    label "wielki"
  ]
  node [
    id 209
    label "teren"
  ]
  node [
    id 210
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 211
    label "human_body"
  ]
  node [
    id 212
    label "obszar"
  ]
  node [
    id 213
    label "dzie&#322;o"
  ]
  node [
    id 214
    label "przyroda"
  ]
  node [
    id 215
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 216
    label "widok"
  ]
  node [
    id 217
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 218
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 219
    label "zaj&#347;cie"
  ]
  node [
    id 220
    label "kontekst"
  ]
  node [
    id 221
    label "message"
  ]
  node [
    id 222
    label "&#347;wiat"
  ]
  node [
    id 223
    label "dar"
  ]
  node [
    id 224
    label "real"
  ]
  node [
    id 225
    label "przedmiot"
  ]
  node [
    id 226
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 227
    label "osobowo&#347;&#263;"
  ]
  node [
    id 228
    label "psychika"
  ]
  node [
    id 229
    label "posta&#263;"
  ]
  node [
    id 230
    label "kompleksja"
  ]
  node [
    id 231
    label "fizjonomia"
  ]
  node [
    id 232
    label "entity"
  ]
  node [
    id 233
    label "kognicja"
  ]
  node [
    id 234
    label "przebieg"
  ]
  node [
    id 235
    label "rozprawa"
  ]
  node [
    id 236
    label "legislacyjnie"
  ]
  node [
    id 237
    label "przes&#322;anka"
  ]
  node [
    id 238
    label "nast&#281;pstwo"
  ]
  node [
    id 239
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 240
    label "z&#322;uda"
  ]
  node [
    id 241
    label "sojourn"
  ]
  node [
    id 242
    label "z&#322;udzenie"
  ]
  node [
    id 243
    label "spo&#322;ecznie"
  ]
  node [
    id 244
    label "publiczny"
  ]
  node [
    id 245
    label "niepubliczny"
  ]
  node [
    id 246
    label "publicznie"
  ]
  node [
    id 247
    label "upublicznianie"
  ]
  node [
    id 248
    label "jawny"
  ]
  node [
    id 249
    label "upublicznienie"
  ]
  node [
    id 250
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 251
    label "mie&#263;_miejsce"
  ]
  node [
    id 252
    label "equal"
  ]
  node [
    id 253
    label "trwa&#263;"
  ]
  node [
    id 254
    label "chodzi&#263;"
  ]
  node [
    id 255
    label "si&#281;ga&#263;"
  ]
  node [
    id 256
    label "stan"
  ]
  node [
    id 257
    label "obecno&#347;&#263;"
  ]
  node [
    id 258
    label "stand"
  ]
  node [
    id 259
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 260
    label "uczestniczy&#263;"
  ]
  node [
    id 261
    label "participate"
  ]
  node [
    id 262
    label "istnie&#263;"
  ]
  node [
    id 263
    label "pozostawa&#263;"
  ]
  node [
    id 264
    label "zostawa&#263;"
  ]
  node [
    id 265
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 266
    label "adhere"
  ]
  node [
    id 267
    label "compass"
  ]
  node [
    id 268
    label "korzysta&#263;"
  ]
  node [
    id 269
    label "appreciation"
  ]
  node [
    id 270
    label "osi&#261;ga&#263;"
  ]
  node [
    id 271
    label "dociera&#263;"
  ]
  node [
    id 272
    label "get"
  ]
  node [
    id 273
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 274
    label "mierzy&#263;"
  ]
  node [
    id 275
    label "u&#380;ywa&#263;"
  ]
  node [
    id 276
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 277
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 278
    label "exsert"
  ]
  node [
    id 279
    label "being"
  ]
  node [
    id 280
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 281
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 282
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 283
    label "p&#322;ywa&#263;"
  ]
  node [
    id 284
    label "run"
  ]
  node [
    id 285
    label "bangla&#263;"
  ]
  node [
    id 286
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 287
    label "przebiega&#263;"
  ]
  node [
    id 288
    label "wk&#322;ada&#263;"
  ]
  node [
    id 289
    label "proceed"
  ]
  node [
    id 290
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 291
    label "carry"
  ]
  node [
    id 292
    label "bywa&#263;"
  ]
  node [
    id 293
    label "dziama&#263;"
  ]
  node [
    id 294
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 295
    label "stara&#263;_si&#281;"
  ]
  node [
    id 296
    label "para"
  ]
  node [
    id 297
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 298
    label "str&#243;j"
  ]
  node [
    id 299
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 300
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 301
    label "krok"
  ]
  node [
    id 302
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 303
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 304
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 305
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 306
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 307
    label "continue"
  ]
  node [
    id 308
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 309
    label "Ohio"
  ]
  node [
    id 310
    label "wci&#281;cie"
  ]
  node [
    id 311
    label "Nowy_York"
  ]
  node [
    id 312
    label "warstwa"
  ]
  node [
    id 313
    label "samopoczucie"
  ]
  node [
    id 314
    label "Illinois"
  ]
  node [
    id 315
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 316
    label "state"
  ]
  node [
    id 317
    label "Jukatan"
  ]
  node [
    id 318
    label "Kalifornia"
  ]
  node [
    id 319
    label "Wirginia"
  ]
  node [
    id 320
    label "wektor"
  ]
  node [
    id 321
    label "Goa"
  ]
  node [
    id 322
    label "Teksas"
  ]
  node [
    id 323
    label "Waszyngton"
  ]
  node [
    id 324
    label "miejsce"
  ]
  node [
    id 325
    label "Massachusetts"
  ]
  node [
    id 326
    label "Alaska"
  ]
  node [
    id 327
    label "Arakan"
  ]
  node [
    id 328
    label "Hawaje"
  ]
  node [
    id 329
    label "Maryland"
  ]
  node [
    id 330
    label "Michigan"
  ]
  node [
    id 331
    label "Arizona"
  ]
  node [
    id 332
    label "Georgia"
  ]
  node [
    id 333
    label "poziom"
  ]
  node [
    id 334
    label "Pensylwania"
  ]
  node [
    id 335
    label "shape"
  ]
  node [
    id 336
    label "Luizjana"
  ]
  node [
    id 337
    label "Nowy_Meksyk"
  ]
  node [
    id 338
    label "Alabama"
  ]
  node [
    id 339
    label "ilo&#347;&#263;"
  ]
  node [
    id 340
    label "Kansas"
  ]
  node [
    id 341
    label "Oregon"
  ]
  node [
    id 342
    label "Oklahoma"
  ]
  node [
    id 343
    label "Floryda"
  ]
  node [
    id 344
    label "jednostka_administracyjna"
  ]
  node [
    id 345
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 346
    label "samodzielny"
  ]
  node [
    id 347
    label "wolny"
  ]
  node [
    id 348
    label "osamotnienie"
  ]
  node [
    id 349
    label "pojedynczy"
  ]
  node [
    id 350
    label "sam"
  ]
  node [
    id 351
    label "samotnie"
  ]
  node [
    id 352
    label "osamotnianie"
  ]
  node [
    id 353
    label "odosobniony"
  ]
  node [
    id 354
    label "odr&#281;bny"
  ]
  node [
    id 355
    label "sw&#243;j"
  ]
  node [
    id 356
    label "sobieradzki"
  ]
  node [
    id 357
    label "niepodleg&#322;y"
  ]
  node [
    id 358
    label "czyj&#347;"
  ]
  node [
    id 359
    label "autonomicznie"
  ]
  node [
    id 360
    label "indywidualny"
  ]
  node [
    id 361
    label "samodzielnie"
  ]
  node [
    id 362
    label "w&#322;asny"
  ]
  node [
    id 363
    label "osobny"
  ]
  node [
    id 364
    label "jednodzielny"
  ]
  node [
    id 365
    label "rzadki"
  ]
  node [
    id 366
    label "pojedynczo"
  ]
  node [
    id 367
    label "unikalny"
  ]
  node [
    id 368
    label "sklep"
  ]
  node [
    id 369
    label "rozrzedzenie"
  ]
  node [
    id 370
    label "rzedni&#281;cie"
  ]
  node [
    id 371
    label "niespieszny"
  ]
  node [
    id 372
    label "zwalnianie_si&#281;"
  ]
  node [
    id 373
    label "wakowa&#263;"
  ]
  node [
    id 374
    label "rozwadnianie"
  ]
  node [
    id 375
    label "niezale&#380;ny"
  ]
  node [
    id 376
    label "zrzedni&#281;cie"
  ]
  node [
    id 377
    label "swobodnie"
  ]
  node [
    id 378
    label "rozrzedzanie"
  ]
  node [
    id 379
    label "rozwodnienie"
  ]
  node [
    id 380
    label "strza&#322;"
  ]
  node [
    id 381
    label "wolnie"
  ]
  node [
    id 382
    label "zwolnienie_si&#281;"
  ]
  node [
    id 383
    label "wolno"
  ]
  node [
    id 384
    label "lu&#378;no"
  ]
  node [
    id 385
    label "osobno"
  ]
  node [
    id 386
    label "samotno"
  ]
  node [
    id 387
    label "solitarily"
  ]
  node [
    id 388
    label "zostawianie"
  ]
  node [
    id 389
    label "retirement"
  ]
  node [
    id 390
    label "izolacja"
  ]
  node [
    id 391
    label "zostawienie"
  ]
  node [
    id 392
    label "kom&#243;rka_jajowa"
  ]
  node [
    id 393
    label "dwa_ognie"
  ]
  node [
    id 394
    label "gracz"
  ]
  node [
    id 395
    label "rozsadnik"
  ]
  node [
    id 396
    label "rodzice"
  ]
  node [
    id 397
    label "staruszka"
  ]
  node [
    id 398
    label "ro&#347;lina"
  ]
  node [
    id 399
    label "przyczyna"
  ]
  node [
    id 400
    label "macocha"
  ]
  node [
    id 401
    label "matka_zast&#281;pcza"
  ]
  node [
    id 402
    label "pi&#322;ka_palantowa"
  ]
  node [
    id 403
    label "samica"
  ]
  node [
    id 404
    label "zawodnik"
  ]
  node [
    id 405
    label "matczysko"
  ]
  node [
    id 406
    label "macierz"
  ]
  node [
    id 407
    label "Matka_Boska"
  ]
  node [
    id 408
    label "obiekt"
  ]
  node [
    id 409
    label "przodkini"
  ]
  node [
    id 410
    label "zakonnica"
  ]
  node [
    id 411
    label "stara"
  ]
  node [
    id 412
    label "rodzic"
  ]
  node [
    id 413
    label "owad"
  ]
  node [
    id 414
    label "zbiorowisko"
  ]
  node [
    id 415
    label "ro&#347;liny"
  ]
  node [
    id 416
    label "p&#281;d"
  ]
  node [
    id 417
    label "wegetowanie"
  ]
  node [
    id 418
    label "zadziorek"
  ]
  node [
    id 419
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 420
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 421
    label "do&#322;owa&#263;"
  ]
  node [
    id 422
    label "wegetacja"
  ]
  node [
    id 423
    label "owoc"
  ]
  node [
    id 424
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 425
    label "strzyc"
  ]
  node [
    id 426
    label "w&#322;&#243;kno"
  ]
  node [
    id 427
    label "g&#322;uszenie"
  ]
  node [
    id 428
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 429
    label "fitotron"
  ]
  node [
    id 430
    label "bulwka"
  ]
  node [
    id 431
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 432
    label "odn&#243;&#380;ka"
  ]
  node [
    id 433
    label "epiderma"
  ]
  node [
    id 434
    label "gumoza"
  ]
  node [
    id 435
    label "strzy&#380;enie"
  ]
  node [
    id 436
    label "wypotnik"
  ]
  node [
    id 437
    label "flawonoid"
  ]
  node [
    id 438
    label "wyro&#347;le"
  ]
  node [
    id 439
    label "do&#322;owanie"
  ]
  node [
    id 440
    label "g&#322;uszy&#263;"
  ]
  node [
    id 441
    label "pora&#380;a&#263;"
  ]
  node [
    id 442
    label "fitocenoza"
  ]
  node [
    id 443
    label "hodowla"
  ]
  node [
    id 444
    label "fotoautotrof"
  ]
  node [
    id 445
    label "nieuleczalnie_chory"
  ]
  node [
    id 446
    label "wegetowa&#263;"
  ]
  node [
    id 447
    label "pochewka"
  ]
  node [
    id 448
    label "sok"
  ]
  node [
    id 449
    label "system_korzeniowy"
  ]
  node [
    id 450
    label "zawi&#261;zek"
  ]
  node [
    id 451
    label "wyznawczyni"
  ]
  node [
    id 452
    label "pingwin"
  ]
  node [
    id 453
    label "kornet"
  ]
  node [
    id 454
    label "zi&#243;&#322;ko"
  ]
  node [
    id 455
    label "czo&#322;&#243;wka"
  ]
  node [
    id 456
    label "uczestnik"
  ]
  node [
    id 457
    label "lista_startowa"
  ]
  node [
    id 458
    label "sportowiec"
  ]
  node [
    id 459
    label "orygina&#322;"
  ]
  node [
    id 460
    label "facet"
  ]
  node [
    id 461
    label "zwierz&#281;"
  ]
  node [
    id 462
    label "bohater"
  ]
  node [
    id 463
    label "spryciarz"
  ]
  node [
    id 464
    label "rozdawa&#263;_karty"
  ]
  node [
    id 465
    label "samka"
  ]
  node [
    id 466
    label "p&#322;e&#263;_pi&#281;kna"
  ]
  node [
    id 467
    label "drogi_rodne"
  ]
  node [
    id 468
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 469
    label "female"
  ]
  node [
    id 470
    label "zabrz&#281;czenie"
  ]
  node [
    id 471
    label "bzyka&#263;"
  ]
  node [
    id 472
    label "hukni&#281;cie"
  ]
  node [
    id 473
    label "owady"
  ]
  node [
    id 474
    label "parabioza"
  ]
  node [
    id 475
    label "prostoskrzyd&#322;y"
  ]
  node [
    id 476
    label "skrzyd&#322;o"
  ]
  node [
    id 477
    label "cierkanie"
  ]
  node [
    id 478
    label "stawon&#243;g"
  ]
  node [
    id 479
    label "bzykni&#281;cie"
  ]
  node [
    id 480
    label "r&#243;&#380;noskrzyd&#322;y"
  ]
  node [
    id 481
    label "brz&#281;czenie"
  ]
  node [
    id 482
    label "bzykn&#261;&#263;"
  ]
  node [
    id 483
    label "aparat_g&#281;bowy"
  ]
  node [
    id 484
    label "entomofauna"
  ]
  node [
    id 485
    label "bzykanie"
  ]
  node [
    id 486
    label "krewna"
  ]
  node [
    id 487
    label "opiekun"
  ]
  node [
    id 488
    label "wapniak"
  ]
  node [
    id 489
    label "rodzic_chrzestny"
  ]
  node [
    id 490
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 491
    label "co&#347;"
  ]
  node [
    id 492
    label "budynek"
  ]
  node [
    id 493
    label "thing"
  ]
  node [
    id 494
    label "poj&#281;cie"
  ]
  node [
    id 495
    label "rzecz"
  ]
  node [
    id 496
    label "strona"
  ]
  node [
    id 497
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 498
    label "subject"
  ]
  node [
    id 499
    label "czynnik"
  ]
  node [
    id 500
    label "matuszka"
  ]
  node [
    id 501
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 502
    label "geneza"
  ]
  node [
    id 503
    label "poci&#261;ganie"
  ]
  node [
    id 504
    label "pepiniera"
  ]
  node [
    id 505
    label "roznosiciel"
  ]
  node [
    id 506
    label "kolebka"
  ]
  node [
    id 507
    label "las"
  ]
  node [
    id 508
    label "starzy"
  ]
  node [
    id 509
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 510
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 511
    label "pokolenie"
  ]
  node [
    id 512
    label "wapniaki"
  ]
  node [
    id 513
    label "m&#281;&#380;atka"
  ]
  node [
    id 514
    label "baba"
  ]
  node [
    id 515
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 516
    label "&#380;ona"
  ]
  node [
    id 517
    label "partnerka"
  ]
  node [
    id 518
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 519
    label "parametryzacja"
  ]
  node [
    id 520
    label "pa&#324;stwo"
  ]
  node [
    id 521
    label "mod"
  ]
  node [
    id 522
    label "patriota"
  ]
  node [
    id 523
    label "nieistnienie"
  ]
  node [
    id 524
    label "odej&#347;cie"
  ]
  node [
    id 525
    label "defect"
  ]
  node [
    id 526
    label "gap"
  ]
  node [
    id 527
    label "odej&#347;&#263;"
  ]
  node [
    id 528
    label "kr&#243;tki"
  ]
  node [
    id 529
    label "wada"
  ]
  node [
    id 530
    label "odchodzi&#263;"
  ]
  node [
    id 531
    label "wyr&#243;b"
  ]
  node [
    id 532
    label "odchodzenie"
  ]
  node [
    id 533
    label "prywatywny"
  ]
  node [
    id 534
    label "niebyt"
  ]
  node [
    id 535
    label "nonexistence"
  ]
  node [
    id 536
    label "faintness"
  ]
  node [
    id 537
    label "schorzenie"
  ]
  node [
    id 538
    label "imperfection"
  ]
  node [
    id 539
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 540
    label "produkt"
  ]
  node [
    id 541
    label "creation"
  ]
  node [
    id 542
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 543
    label "p&#322;uczkarnia"
  ]
  node [
    id 544
    label "znakowarka"
  ]
  node [
    id 545
    label "szybki"
  ]
  node [
    id 546
    label "jednowyrazowy"
  ]
  node [
    id 547
    label "bliski"
  ]
  node [
    id 548
    label "s&#322;aby"
  ]
  node [
    id 549
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 550
    label "kr&#243;tko"
  ]
  node [
    id 551
    label "drobny"
  ]
  node [
    id 552
    label "ruch"
  ]
  node [
    id 553
    label "z&#322;y"
  ]
  node [
    id 554
    label "mini&#281;cie"
  ]
  node [
    id 555
    label "odumarcie"
  ]
  node [
    id 556
    label "dysponowanie_si&#281;"
  ]
  node [
    id 557
    label "ruszenie"
  ]
  node [
    id 558
    label "ust&#261;pienie"
  ]
  node [
    id 559
    label "mogi&#322;a"
  ]
  node [
    id 560
    label "pomarcie"
  ]
  node [
    id 561
    label "opuszczenie"
  ]
  node [
    id 562
    label "zb&#281;dny"
  ]
  node [
    id 563
    label "spisanie_"
  ]
  node [
    id 564
    label "oddalenie_si&#281;"
  ]
  node [
    id 565
    label "defenestracja"
  ]
  node [
    id 566
    label "danie_sobie_spokoju"
  ]
  node [
    id 567
    label "odrzut"
  ]
  node [
    id 568
    label "kres_&#380;ycia"
  ]
  node [
    id 569
    label "zdechni&#281;cie"
  ]
  node [
    id 570
    label "exit"
  ]
  node [
    id 571
    label "stracenie"
  ]
  node [
    id 572
    label "przestanie"
  ]
  node [
    id 573
    label "martwy"
  ]
  node [
    id 574
    label "wr&#243;cenie"
  ]
  node [
    id 575
    label "szeol"
  ]
  node [
    id 576
    label "die"
  ]
  node [
    id 577
    label "oddzielenie_si&#281;"
  ]
  node [
    id 578
    label "deviation"
  ]
  node [
    id 579
    label "wydalenie"
  ]
  node [
    id 580
    label "&#380;a&#322;oba"
  ]
  node [
    id 581
    label "pogrzebanie"
  ]
  node [
    id 582
    label "sko&#324;czenie"
  ]
  node [
    id 583
    label "withdrawal"
  ]
  node [
    id 584
    label "porozchodzenie_si&#281;"
  ]
  node [
    id 585
    label "zabicie"
  ]
  node [
    id 586
    label "agonia"
  ]
  node [
    id 587
    label "po&#322;o&#380;enie_lachy"
  ]
  node [
    id 588
    label "kres"
  ]
  node [
    id 589
    label "usuni&#281;cie"
  ]
  node [
    id 590
    label "relinquishment"
  ]
  node [
    id 591
    label "p&#243;j&#347;cie"
  ]
  node [
    id 592
    label "poniechanie"
  ]
  node [
    id 593
    label "zako&#324;czenie"
  ]
  node [
    id 594
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 595
    label "wypisanie_si&#281;"
  ]
  node [
    id 596
    label "zrobienie"
  ]
  node [
    id 597
    label "blend"
  ]
  node [
    id 598
    label "rozgrywa&#263;_si&#281;"
  ]
  node [
    id 599
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 600
    label "opuszcza&#263;"
  ]
  node [
    id 601
    label "impart"
  ]
  node [
    id 602
    label "wyrusza&#263;"
  ]
  node [
    id 603
    label "go"
  ]
  node [
    id 604
    label "seclude"
  ]
  node [
    id 605
    label "gasn&#261;&#263;"
  ]
  node [
    id 606
    label "przestawa&#263;"
  ]
  node [
    id 607
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 608
    label "odstawa&#263;"
  ]
  node [
    id 609
    label "rezygnowa&#263;"
  ]
  node [
    id 610
    label "i&#347;&#263;"
  ]
  node [
    id 611
    label "mija&#263;"
  ]
  node [
    id 612
    label "korkowanie"
  ]
  node [
    id 613
    label "death"
  ]
  node [
    id 614
    label "k&#322;adzenie_lachy"
  ]
  node [
    id 615
    label "przestawanie"
  ]
  node [
    id 616
    label "machanie_r&#281;k&#261;"
  ]
  node [
    id 617
    label "zdychanie"
  ]
  node [
    id 618
    label "spisywanie_"
  ]
  node [
    id 619
    label "usuwanie"
  ]
  node [
    id 620
    label "tracenie"
  ]
  node [
    id 621
    label "ko&#324;czenie"
  ]
  node [
    id 622
    label "robienie"
  ]
  node [
    id 623
    label "opuszczanie"
  ]
  node [
    id 624
    label "wydalanie"
  ]
  node [
    id 625
    label "odrzucanie"
  ]
  node [
    id 626
    label "odstawianie"
  ]
  node [
    id 627
    label "ust&#281;powanie"
  ]
  node [
    id 628
    label "egress"
  ]
  node [
    id 629
    label "zrzekanie_si&#281;"
  ]
  node [
    id 630
    label "dzianie_si&#281;"
  ]
  node [
    id 631
    label "oddzielanie_si&#281;"
  ]
  node [
    id 632
    label "bycie"
  ]
  node [
    id 633
    label "wyruszanie"
  ]
  node [
    id 634
    label "odumieranie"
  ]
  node [
    id 635
    label "odstawanie"
  ]
  node [
    id 636
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 637
    label "mijanie"
  ]
  node [
    id 638
    label "wracanie"
  ]
  node [
    id 639
    label "oddalanie_si&#281;"
  ]
  node [
    id 640
    label "kursowanie"
  ]
  node [
    id 641
    label "drop"
  ]
  node [
    id 642
    label "zrezygnowa&#263;"
  ]
  node [
    id 643
    label "ruszy&#263;"
  ]
  node [
    id 644
    label "min&#261;&#263;"
  ]
  node [
    id 645
    label "zrobi&#263;"
  ]
  node [
    id 646
    label "leave_office"
  ]
  node [
    id 647
    label "retract"
  ]
  node [
    id 648
    label "opu&#347;ci&#263;"
  ]
  node [
    id 649
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 650
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 651
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 652
    label "przesta&#263;"
  ]
  node [
    id 653
    label "ciekawski"
  ]
  node [
    id 654
    label "statysta"
  ]
  node [
    id 655
    label "kszta&#322;ciciel"
  ]
  node [
    id 656
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 657
    label "kuwada"
  ]
  node [
    id 658
    label "tworzyciel"
  ]
  node [
    id 659
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 660
    label "&#347;w"
  ]
  node [
    id 661
    label "pomys&#322;odawca"
  ]
  node [
    id 662
    label "wykonawca"
  ]
  node [
    id 663
    label "ojczym"
  ]
  node [
    id 664
    label "samiec"
  ]
  node [
    id 665
    label "przodek"
  ]
  node [
    id 666
    label "papa"
  ]
  node [
    id 667
    label "zakonnik"
  ]
  node [
    id 668
    label "stary"
  ]
  node [
    id 669
    label "br"
  ]
  node [
    id 670
    label "mnich"
  ]
  node [
    id 671
    label "zakon"
  ]
  node [
    id 672
    label "wyznawca"
  ]
  node [
    id 673
    label "ojcowie"
  ]
  node [
    id 674
    label "linea&#380;"
  ]
  node [
    id 675
    label "krewny"
  ]
  node [
    id 676
    label "chodnik"
  ]
  node [
    id 677
    label "w&#243;z"
  ]
  node [
    id 678
    label "p&#322;ug"
  ]
  node [
    id 679
    label "wyrobisko"
  ]
  node [
    id 680
    label "dziad"
  ]
  node [
    id 681
    label "antecesor"
  ]
  node [
    id 682
    label "post&#281;p"
  ]
  node [
    id 683
    label "inicjator"
  ]
  node [
    id 684
    label "podmiot_gospodarczy"
  ]
  node [
    id 685
    label "artysta"
  ]
  node [
    id 686
    label "muzyk"
  ]
  node [
    id 687
    label "materia&#322;_budowlany"
  ]
  node [
    id 688
    label "twarz"
  ]
  node [
    id 689
    label "gun_muzzle"
  ]
  node [
    id 690
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 691
    label "nienowoczesny"
  ]
  node [
    id 692
    label "gruba_ryba"
  ]
  node [
    id 693
    label "zestarzenie_si&#281;"
  ]
  node [
    id 694
    label "poprzedni"
  ]
  node [
    id 695
    label "dawno"
  ]
  node [
    id 696
    label "staro"
  ]
  node [
    id 697
    label "m&#261;&#380;"
  ]
  node [
    id 698
    label "dotychczasowy"
  ]
  node [
    id 699
    label "p&#243;&#378;ny"
  ]
  node [
    id 700
    label "d&#322;ugoletni"
  ]
  node [
    id 701
    label "charakterystyczny"
  ]
  node [
    id 702
    label "brat"
  ]
  node [
    id 703
    label "po_staro&#347;wiecku"
  ]
  node [
    id 704
    label "zwierzchnik"
  ]
  node [
    id 705
    label "znajomy"
  ]
  node [
    id 706
    label "odleg&#322;y"
  ]
  node [
    id 707
    label "starzenie_si&#281;"
  ]
  node [
    id 708
    label "starczo"
  ]
  node [
    id 709
    label "dawniej"
  ]
  node [
    id 710
    label "niegdysiejszy"
  ]
  node [
    id 711
    label "dojrza&#322;y"
  ]
  node [
    id 712
    label "nauczyciel"
  ]
  node [
    id 713
    label "autor"
  ]
  node [
    id 714
    label "doros&#322;y"
  ]
  node [
    id 715
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 716
    label "jegomo&#347;&#263;"
  ]
  node [
    id 717
    label "andropauza"
  ]
  node [
    id 718
    label "bratek"
  ]
  node [
    id 719
    label "ch&#322;opina"
  ]
  node [
    id 720
    label "twardziel"
  ]
  node [
    id 721
    label "androlog"
  ]
  node [
    id 722
    label "syndrom_kuwady"
  ]
  node [
    id 723
    label "na&#347;ladownictwo"
  ]
  node [
    id 724
    label "zwyczaj"
  ]
  node [
    id 725
    label "ci&#261;&#380;a"
  ]
  node [
    id 726
    label "&#380;onaty"
  ]
  node [
    id 727
    label "raj_utracony"
  ]
  node [
    id 728
    label "umieranie"
  ]
  node [
    id 729
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 730
    label "prze&#380;ywanie"
  ]
  node [
    id 731
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 732
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 733
    label "po&#322;&#243;g"
  ]
  node [
    id 734
    label "umarcie"
  ]
  node [
    id 735
    label "subsistence"
  ]
  node [
    id 736
    label "power"
  ]
  node [
    id 737
    label "okres_noworodkowy"
  ]
  node [
    id 738
    label "prze&#380;ycie"
  ]
  node [
    id 739
    label "wiek_matuzalemowy"
  ]
  node [
    id 740
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 741
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 742
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 743
    label "do&#380;ywanie"
  ]
  node [
    id 744
    label "byt"
  ]
  node [
    id 745
    label "dzieci&#324;stwo"
  ]
  node [
    id 746
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 747
    label "rozw&#243;j"
  ]
  node [
    id 748
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 749
    label "czas"
  ]
  node [
    id 750
    label "menopauza"
  ]
  node [
    id 751
    label "&#347;mier&#263;"
  ]
  node [
    id 752
    label "koleje_losu"
  ]
  node [
    id 753
    label "zegar_biologiczny"
  ]
  node [
    id 754
    label "szwung"
  ]
  node [
    id 755
    label "przebywanie"
  ]
  node [
    id 756
    label "warunki"
  ]
  node [
    id 757
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 758
    label "niemowl&#281;ctwo"
  ]
  node [
    id 759
    label "&#380;ywy"
  ]
  node [
    id 760
    label "life"
  ]
  node [
    id 761
    label "staro&#347;&#263;"
  ]
  node [
    id 762
    label "energy"
  ]
  node [
    id 763
    label "trwanie"
  ]
  node [
    id 764
    label "wra&#380;enie"
  ]
  node [
    id 765
    label "przej&#347;cie"
  ]
  node [
    id 766
    label "doznanie"
  ]
  node [
    id 767
    label "poradzenie_sobie"
  ]
  node [
    id 768
    label "przetrwanie"
  ]
  node [
    id 769
    label "survival"
  ]
  node [
    id 770
    label "przechodzenie"
  ]
  node [
    id 771
    label "wytrzymywanie"
  ]
  node [
    id 772
    label "zaznawanie"
  ]
  node [
    id 773
    label "obejrzenie"
  ]
  node [
    id 774
    label "widzenie"
  ]
  node [
    id 775
    label "urzeczywistnianie"
  ]
  node [
    id 776
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 777
    label "przeszkodzenie"
  ]
  node [
    id 778
    label "produkowanie"
  ]
  node [
    id 779
    label "znikni&#281;cie"
  ]
  node [
    id 780
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 781
    label "przeszkadzanie"
  ]
  node [
    id 782
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 783
    label "wyprodukowanie"
  ]
  node [
    id 784
    label "utrzymywanie"
  ]
  node [
    id 785
    label "subsystencja"
  ]
  node [
    id 786
    label "utrzyma&#263;"
  ]
  node [
    id 787
    label "egzystencja"
  ]
  node [
    id 788
    label "wy&#380;ywienie"
  ]
  node [
    id 789
    label "ontologicznie"
  ]
  node [
    id 790
    label "utrzymanie"
  ]
  node [
    id 791
    label "potencja"
  ]
  node [
    id 792
    label "utrzymywa&#263;"
  ]
  node [
    id 793
    label "status"
  ]
  node [
    id 794
    label "sytuacja"
  ]
  node [
    id 795
    label "poprzedzanie"
  ]
  node [
    id 796
    label "czasoprzestrze&#324;"
  ]
  node [
    id 797
    label "laba"
  ]
  node [
    id 798
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 799
    label "chronometria"
  ]
  node [
    id 800
    label "rachuba_czasu"
  ]
  node [
    id 801
    label "przep&#322;ywanie"
  ]
  node [
    id 802
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 803
    label "czasokres"
  ]
  node [
    id 804
    label "odczyt"
  ]
  node [
    id 805
    label "chwila"
  ]
  node [
    id 806
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 807
    label "dzieje"
  ]
  node [
    id 808
    label "kategoria_gramatyczna"
  ]
  node [
    id 809
    label "poprzedzenie"
  ]
  node [
    id 810
    label "trawienie"
  ]
  node [
    id 811
    label "pochodzi&#263;"
  ]
  node [
    id 812
    label "period"
  ]
  node [
    id 813
    label "okres_czasu"
  ]
  node [
    id 814
    label "poprzedza&#263;"
  ]
  node [
    id 815
    label "schy&#322;ek"
  ]
  node [
    id 816
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 817
    label "odwlekanie_si&#281;"
  ]
  node [
    id 818
    label "zegar"
  ]
  node [
    id 819
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 820
    label "czwarty_wymiar"
  ]
  node [
    id 821
    label "pochodzenie"
  ]
  node [
    id 822
    label "koniugacja"
  ]
  node [
    id 823
    label "Zeitgeist"
  ]
  node [
    id 824
    label "trawi&#263;"
  ]
  node [
    id 825
    label "pogoda"
  ]
  node [
    id 826
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 827
    label "poprzedzi&#263;"
  ]
  node [
    id 828
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 829
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 830
    label "time_period"
  ]
  node [
    id 831
    label "ocieranie_si&#281;"
  ]
  node [
    id 832
    label "otoczenie_si&#281;"
  ]
  node [
    id 833
    label "posiedzenie"
  ]
  node [
    id 834
    label "otarcie_si&#281;"
  ]
  node [
    id 835
    label "atakowanie"
  ]
  node [
    id 836
    label "otaczanie_si&#281;"
  ]
  node [
    id 837
    label "wyj&#347;cie"
  ]
  node [
    id 838
    label "zmierzanie"
  ]
  node [
    id 839
    label "residency"
  ]
  node [
    id 840
    label "wychodzenie"
  ]
  node [
    id 841
    label "tkwienie"
  ]
  node [
    id 842
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 843
    label "absolutorium"
  ]
  node [
    id 844
    label "dzia&#322;anie"
  ]
  node [
    id 845
    label "activity"
  ]
  node [
    id 846
    label "ton"
  ]
  node [
    id 847
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 848
    label "zabijanie"
  ]
  node [
    id 849
    label "zanikanie"
  ]
  node [
    id 850
    label "ciekawy"
  ]
  node [
    id 851
    label "&#380;ywotny"
  ]
  node [
    id 852
    label "naturalny"
  ]
  node [
    id 853
    label "&#380;ywo"
  ]
  node [
    id 854
    label "o&#380;ywianie"
  ]
  node [
    id 855
    label "silny"
  ]
  node [
    id 856
    label "g&#322;&#281;boki"
  ]
  node [
    id 857
    label "wyra&#378;ny"
  ]
  node [
    id 858
    label "czynny"
  ]
  node [
    id 859
    label "aktualny"
  ]
  node [
    id 860
    label "zgrabny"
  ]
  node [
    id 861
    label "prawdziwy"
  ]
  node [
    id 862
    label "realistyczny"
  ]
  node [
    id 863
    label "energiczny"
  ]
  node [
    id 864
    label "procedura"
  ]
  node [
    id 865
    label "proces_biologiczny"
  ]
  node [
    id 866
    label "z&#322;ote_czasy"
  ]
  node [
    id 867
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 868
    label "process"
  ]
  node [
    id 869
    label "cycle"
  ]
  node [
    id 870
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 871
    label "adolescence"
  ]
  node [
    id 872
    label "wiek"
  ]
  node [
    id 873
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 874
    label "zielone_lata"
  ]
  node [
    id 875
    label "rozwi&#261;zanie"
  ]
  node [
    id 876
    label "zlec"
  ]
  node [
    id 877
    label "zlegni&#281;cie"
  ]
  node [
    id 878
    label "upadek"
  ]
  node [
    id 879
    label "istota_nadprzyrodzona"
  ]
  node [
    id 880
    label "pogrzeb"
  ]
  node [
    id 881
    label "majority"
  ]
  node [
    id 882
    label "osiemnastoletni"
  ]
  node [
    id 883
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 884
    label "age"
  ]
  node [
    id 885
    label "przekwitanie"
  ]
  node [
    id 886
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 887
    label "energia"
  ]
  node [
    id 888
    label "zapa&#322;"
  ]
  node [
    id 889
    label "personalny"
  ]
  node [
    id 890
    label "prywatnie"
  ]
  node [
    id 891
    label "nieformalny"
  ]
  node [
    id 892
    label "zwi&#261;zany"
  ]
  node [
    id 893
    label "swoisty"
  ]
  node [
    id 894
    label "nieoficjalny"
  ]
  node [
    id 895
    label "nieformalnie"
  ]
  node [
    id 896
    label "personalnie"
  ]
  node [
    id 897
    label "sprawa"
  ]
  node [
    id 898
    label "subiekcja"
  ]
  node [
    id 899
    label "problemat"
  ]
  node [
    id 900
    label "jajko_Kolumba"
  ]
  node [
    id 901
    label "obstruction"
  ]
  node [
    id 902
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 903
    label "problematyka"
  ]
  node [
    id 904
    label "trudno&#347;&#263;"
  ]
  node [
    id 905
    label "pierepa&#322;ka"
  ]
  node [
    id 906
    label "ambaras"
  ]
  node [
    id 907
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 908
    label "napotka&#263;"
  ]
  node [
    id 909
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 910
    label "k&#322;opotliwy"
  ]
  node [
    id 911
    label "napotkanie"
  ]
  node [
    id 912
    label "difficulty"
  ]
  node [
    id 913
    label "obstacle"
  ]
  node [
    id 914
    label "object"
  ]
  node [
    id 915
    label "temat"
  ]
  node [
    id 916
    label "szczeg&#243;&#322;"
  ]
  node [
    id 917
    label "proposition"
  ]
  node [
    id 918
    label "idea"
  ]
  node [
    id 919
    label "k&#322;opot"
  ]
  node [
    id 920
    label "utulenie"
  ]
  node [
    id 921
    label "pediatra"
  ]
  node [
    id 922
    label "dzieciak"
  ]
  node [
    id 923
    label "utulanie"
  ]
  node [
    id 924
    label "dzieciarnia"
  ]
  node [
    id 925
    label "niepe&#322;noletni"
  ]
  node [
    id 926
    label "organizm"
  ]
  node [
    id 927
    label "utula&#263;"
  ]
  node [
    id 928
    label "cz&#322;owieczek"
  ]
  node [
    id 929
    label "fledgling"
  ]
  node [
    id 930
    label "utuli&#263;"
  ]
  node [
    id 931
    label "m&#322;odzik"
  ]
  node [
    id 932
    label "pedofil"
  ]
  node [
    id 933
    label "m&#322;odziak"
  ]
  node [
    id 934
    label "potomek"
  ]
  node [
    id 935
    label "entliczek-pentliczek"
  ]
  node [
    id 936
    label "potomstwo"
  ]
  node [
    id 937
    label "sraluch"
  ]
  node [
    id 938
    label "czeladka"
  ]
  node [
    id 939
    label "dzietno&#347;&#263;"
  ]
  node [
    id 940
    label "bawienie_si&#281;"
  ]
  node [
    id 941
    label "pomiot"
  ]
  node [
    id 942
    label "grupa"
  ]
  node [
    id 943
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 944
    label "kinderbal"
  ]
  node [
    id 945
    label "ludzko&#347;&#263;"
  ]
  node [
    id 946
    label "asymilowanie"
  ]
  node [
    id 947
    label "asymilowa&#263;"
  ]
  node [
    id 948
    label "os&#322;abia&#263;"
  ]
  node [
    id 949
    label "hominid"
  ]
  node [
    id 950
    label "podw&#322;adny"
  ]
  node [
    id 951
    label "os&#322;abianie"
  ]
  node [
    id 952
    label "g&#322;owa"
  ]
  node [
    id 953
    label "figura"
  ]
  node [
    id 954
    label "portrecista"
  ]
  node [
    id 955
    label "dwun&#243;g"
  ]
  node [
    id 956
    label "profanum"
  ]
  node [
    id 957
    label "mikrokosmos"
  ]
  node [
    id 958
    label "nasada"
  ]
  node [
    id 959
    label "duch"
  ]
  node [
    id 960
    label "antropochoria"
  ]
  node [
    id 961
    label "osoba"
  ]
  node [
    id 962
    label "wz&#243;r"
  ]
  node [
    id 963
    label "senior"
  ]
  node [
    id 964
    label "oddzia&#322;ywanie"
  ]
  node [
    id 965
    label "Adam"
  ]
  node [
    id 966
    label "homo_sapiens"
  ]
  node [
    id 967
    label "polifag"
  ]
  node [
    id 968
    label "ma&#322;oletny"
  ]
  node [
    id 969
    label "m&#322;ody"
  ]
  node [
    id 970
    label "degenerat"
  ]
  node [
    id 971
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 972
    label "zwyrol"
  ]
  node [
    id 973
    label "czerniak"
  ]
  node [
    id 974
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 975
    label "paszcza"
  ]
  node [
    id 976
    label "popapraniec"
  ]
  node [
    id 977
    label "skuba&#263;"
  ]
  node [
    id 978
    label "skubanie"
  ]
  node [
    id 979
    label "skubni&#281;cie"
  ]
  node [
    id 980
    label "agresja"
  ]
  node [
    id 981
    label "zwierz&#281;ta"
  ]
  node [
    id 982
    label "fukni&#281;cie"
  ]
  node [
    id 983
    label "farba"
  ]
  node [
    id 984
    label "fukanie"
  ]
  node [
    id 985
    label "istota_&#380;ywa"
  ]
  node [
    id 986
    label "gad"
  ]
  node [
    id 987
    label "siedzie&#263;"
  ]
  node [
    id 988
    label "oswaja&#263;"
  ]
  node [
    id 989
    label "tresowa&#263;"
  ]
  node [
    id 990
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 991
    label "poligamia"
  ]
  node [
    id 992
    label "oz&#243;r"
  ]
  node [
    id 993
    label "skubn&#261;&#263;"
  ]
  node [
    id 994
    label "wios&#322;owa&#263;"
  ]
  node [
    id 995
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 996
    label "le&#380;enie"
  ]
  node [
    id 997
    label "niecz&#322;owiek"
  ]
  node [
    id 998
    label "wios&#322;owanie"
  ]
  node [
    id 999
    label "napasienie_si&#281;"
  ]
  node [
    id 1000
    label "wiwarium"
  ]
  node [
    id 1001
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 1002
    label "animalista"
  ]
  node [
    id 1003
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 1004
    label "budowa"
  ]
  node [
    id 1005
    label "pasienie_si&#281;"
  ]
  node [
    id 1006
    label "sodomita"
  ]
  node [
    id 1007
    label "monogamia"
  ]
  node [
    id 1008
    label "przyssawka"
  ]
  node [
    id 1009
    label "zachowanie"
  ]
  node [
    id 1010
    label "budowa_cia&#322;a"
  ]
  node [
    id 1011
    label "okrutnik"
  ]
  node [
    id 1012
    label "grzbiet"
  ]
  node [
    id 1013
    label "weterynarz"
  ]
  node [
    id 1014
    label "&#322;eb"
  ]
  node [
    id 1015
    label "wylinka"
  ]
  node [
    id 1016
    label "bestia"
  ]
  node [
    id 1017
    label "poskramia&#263;"
  ]
  node [
    id 1018
    label "fauna"
  ]
  node [
    id 1019
    label "treser"
  ]
  node [
    id 1020
    label "siedzenie"
  ]
  node [
    id 1021
    label "le&#380;e&#263;"
  ]
  node [
    id 1022
    label "p&#322;aszczyzna"
  ]
  node [
    id 1023
    label "odwadnia&#263;"
  ]
  node [
    id 1024
    label "przyswoi&#263;"
  ]
  node [
    id 1025
    label "sk&#243;ra"
  ]
  node [
    id 1026
    label "odwodni&#263;"
  ]
  node [
    id 1027
    label "ewoluowanie"
  ]
  node [
    id 1028
    label "staw"
  ]
  node [
    id 1029
    label "ow&#322;osienie"
  ]
  node [
    id 1030
    label "unerwienie"
  ]
  node [
    id 1031
    label "reakcja"
  ]
  node [
    id 1032
    label "wyewoluowanie"
  ]
  node [
    id 1033
    label "przyswajanie"
  ]
  node [
    id 1034
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 1035
    label "wyewoluowa&#263;"
  ]
  node [
    id 1036
    label "biorytm"
  ]
  node [
    id 1037
    label "ewoluowa&#263;"
  ]
  node [
    id 1038
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1039
    label "otworzy&#263;"
  ]
  node [
    id 1040
    label "otwiera&#263;"
  ]
  node [
    id 1041
    label "czynnik_biotyczny"
  ]
  node [
    id 1042
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 1043
    label "otworzenie"
  ]
  node [
    id 1044
    label "otwieranie"
  ]
  node [
    id 1045
    label "individual"
  ]
  node [
    id 1046
    label "szkielet"
  ]
  node [
    id 1047
    label "ty&#322;"
  ]
  node [
    id 1048
    label "przyswaja&#263;"
  ]
  node [
    id 1049
    label "przyswojenie"
  ]
  node [
    id 1050
    label "odwadnianie"
  ]
  node [
    id 1051
    label "odwodnienie"
  ]
  node [
    id 1052
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 1053
    label "prz&#243;d"
  ]
  node [
    id 1054
    label "uk&#322;ad"
  ]
  node [
    id 1055
    label "temperatura"
  ]
  node [
    id 1056
    label "l&#281;d&#378;wie"
  ]
  node [
    id 1057
    label "cia&#322;o"
  ]
  node [
    id 1058
    label "cz&#322;onek"
  ]
  node [
    id 1059
    label "utulanie_si&#281;"
  ]
  node [
    id 1060
    label "usypianie"
  ]
  node [
    id 1061
    label "pocieszanie"
  ]
  node [
    id 1062
    label "uspokajanie"
  ]
  node [
    id 1063
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 1064
    label "uspokoi&#263;"
  ]
  node [
    id 1065
    label "uspokojenie"
  ]
  node [
    id 1066
    label "utulenie_si&#281;"
  ]
  node [
    id 1067
    label "u&#347;pienie"
  ]
  node [
    id 1068
    label "usypia&#263;"
  ]
  node [
    id 1069
    label "uspokaja&#263;"
  ]
  node [
    id 1070
    label "dewiant"
  ]
  node [
    id 1071
    label "specjalista"
  ]
  node [
    id 1072
    label "wyliczanka"
  ]
  node [
    id 1073
    label "harcerz"
  ]
  node [
    id 1074
    label "ch&#322;opta&#347;"
  ]
  node [
    id 1075
    label "go&#322;ow&#261;s"
  ]
  node [
    id 1076
    label "m&#322;ode"
  ]
  node [
    id 1077
    label "stopie&#324;_harcerski"
  ]
  node [
    id 1078
    label "g&#243;wniarz"
  ]
  node [
    id 1079
    label "beniaminek"
  ]
  node [
    id 1080
    label "istotka"
  ]
  node [
    id 1081
    label "bech"
  ]
  node [
    id 1082
    label "dziecinny"
  ]
  node [
    id 1083
    label "naiwniak"
  ]
  node [
    id 1084
    label "do&#347;wiadczenie"
  ]
  node [
    id 1085
    label "teren_szko&#322;y"
  ]
  node [
    id 1086
    label "wiedza"
  ]
  node [
    id 1087
    label "Mickiewicz"
  ]
  node [
    id 1088
    label "kwalifikacje"
  ]
  node [
    id 1089
    label "podr&#281;cznik"
  ]
  node [
    id 1090
    label "absolwent"
  ]
  node [
    id 1091
    label "praktyka"
  ]
  node [
    id 1092
    label "school"
  ]
  node [
    id 1093
    label "system"
  ]
  node [
    id 1094
    label "zda&#263;"
  ]
  node [
    id 1095
    label "gabinet"
  ]
  node [
    id 1096
    label "urszulanki"
  ]
  node [
    id 1097
    label "sztuba"
  ]
  node [
    id 1098
    label "&#322;awa_szkolna"
  ]
  node [
    id 1099
    label "nauka"
  ]
  node [
    id 1100
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 1101
    label "przepisa&#263;"
  ]
  node [
    id 1102
    label "muzyka"
  ]
  node [
    id 1103
    label "form"
  ]
  node [
    id 1104
    label "klasa"
  ]
  node [
    id 1105
    label "lekcja"
  ]
  node [
    id 1106
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 1107
    label "przepisanie"
  ]
  node [
    id 1108
    label "skolaryzacja"
  ]
  node [
    id 1109
    label "zdanie"
  ]
  node [
    id 1110
    label "stopek"
  ]
  node [
    id 1111
    label "sekretariat"
  ]
  node [
    id 1112
    label "ideologia"
  ]
  node [
    id 1113
    label "lesson"
  ]
  node [
    id 1114
    label "instytucja"
  ]
  node [
    id 1115
    label "niepokalanki"
  ]
  node [
    id 1116
    label "siedziba"
  ]
  node [
    id 1117
    label "szkolenie"
  ]
  node [
    id 1118
    label "kara"
  ]
  node [
    id 1119
    label "tablica"
  ]
  node [
    id 1120
    label "wyprawka"
  ]
  node [
    id 1121
    label "pomoc_naukowa"
  ]
  node [
    id 1122
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1123
    label "odm&#322;adzanie"
  ]
  node [
    id 1124
    label "liga"
  ]
  node [
    id 1125
    label "jednostka_systematyczna"
  ]
  node [
    id 1126
    label "gromada"
  ]
  node [
    id 1127
    label "egzemplarz"
  ]
  node [
    id 1128
    label "Entuzjastki"
  ]
  node [
    id 1129
    label "kompozycja"
  ]
  node [
    id 1130
    label "Terranie"
  ]
  node [
    id 1131
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1132
    label "category"
  ]
  node [
    id 1133
    label "pakiet_klimatyczny"
  ]
  node [
    id 1134
    label "oddzia&#322;"
  ]
  node [
    id 1135
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1136
    label "cz&#261;steczka"
  ]
  node [
    id 1137
    label "stage_set"
  ]
  node [
    id 1138
    label "type"
  ]
  node [
    id 1139
    label "specgrupa"
  ]
  node [
    id 1140
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1141
    label "&#346;wietliki"
  ]
  node [
    id 1142
    label "odm&#322;odzenie"
  ]
  node [
    id 1143
    label "Eurogrupa"
  ]
  node [
    id 1144
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1145
    label "formacja_geologiczna"
  ]
  node [
    id 1146
    label "harcerze_starsi"
  ]
  node [
    id 1147
    label "course"
  ]
  node [
    id 1148
    label "pomaganie"
  ]
  node [
    id 1149
    label "training"
  ]
  node [
    id 1150
    label "zapoznawanie"
  ]
  node [
    id 1151
    label "seria"
  ]
  node [
    id 1152
    label "zaj&#281;cia"
  ]
  node [
    id 1153
    label "pouczenie"
  ]
  node [
    id 1154
    label "o&#347;wiecanie"
  ]
  node [
    id 1155
    label "Lira"
  ]
  node [
    id 1156
    label "kliker"
  ]
  node [
    id 1157
    label "miasteczko_rowerowe"
  ]
  node [
    id 1158
    label "porada"
  ]
  node [
    id 1159
    label "fotowoltaika"
  ]
  node [
    id 1160
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 1161
    label "przem&#243;wienie"
  ]
  node [
    id 1162
    label "nauki_o_poznaniu"
  ]
  node [
    id 1163
    label "nomotetyczny"
  ]
  node [
    id 1164
    label "systematyka"
  ]
  node [
    id 1165
    label "typologia"
  ]
  node [
    id 1166
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 1167
    label "kultura_duchowa"
  ]
  node [
    id 1168
    label "nauki_penalne"
  ]
  node [
    id 1169
    label "imagineskopia"
  ]
  node [
    id 1170
    label "teoria_naukowa"
  ]
  node [
    id 1171
    label "inwentyka"
  ]
  node [
    id 1172
    label "metodologia"
  ]
  node [
    id 1173
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 1174
    label "nauki_o_Ziemi"
  ]
  node [
    id 1175
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 1176
    label "eliminacje"
  ]
  node [
    id 1177
    label "osoba_prawna"
  ]
  node [
    id 1178
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1179
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1180
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1181
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1182
    label "biuro"
  ]
  node [
    id 1183
    label "organizacja"
  ]
  node [
    id 1184
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1185
    label "Fundusze_Unijne"
  ]
  node [
    id 1186
    label "zamyka&#263;"
  ]
  node [
    id 1187
    label "establishment"
  ]
  node [
    id 1188
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1189
    label "urz&#261;d"
  ]
  node [
    id 1190
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1191
    label "afiliowa&#263;"
  ]
  node [
    id 1192
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1193
    label "standard"
  ]
  node [
    id 1194
    label "zamykanie"
  ]
  node [
    id 1195
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1196
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1197
    label "kwota"
  ]
  node [
    id 1198
    label "nemezis"
  ]
  node [
    id 1199
    label "konsekwencja"
  ]
  node [
    id 1200
    label "punishment"
  ]
  node [
    id 1201
    label "klacz"
  ]
  node [
    id 1202
    label "forfeit"
  ]
  node [
    id 1203
    label "roboty_przymusowe"
  ]
  node [
    id 1204
    label "materia&#322;"
  ]
  node [
    id 1205
    label "obrz&#261;dek"
  ]
  node [
    id 1206
    label "Biblia"
  ]
  node [
    id 1207
    label "tekst"
  ]
  node [
    id 1208
    label "lektor"
  ]
  node [
    id 1209
    label "practice"
  ]
  node [
    id 1210
    label "znawstwo"
  ]
  node [
    id 1211
    label "skill"
  ]
  node [
    id 1212
    label "eksperiencja"
  ]
  node [
    id 1213
    label "praca"
  ]
  node [
    id 1214
    label "j&#261;dro"
  ]
  node [
    id 1215
    label "systemik"
  ]
  node [
    id 1216
    label "rozprz&#261;c"
  ]
  node [
    id 1217
    label "systemat"
  ]
  node [
    id 1218
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1219
    label "usenet"
  ]
  node [
    id 1220
    label "s&#261;d"
  ]
  node [
    id 1221
    label "porz&#261;dek"
  ]
  node [
    id 1222
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1223
    label "przyn&#281;ta"
  ]
  node [
    id 1224
    label "p&#322;&#243;d"
  ]
  node [
    id 1225
    label "net"
  ]
  node [
    id 1226
    label "w&#281;dkarstwo"
  ]
  node [
    id 1227
    label "eratem"
  ]
  node [
    id 1228
    label "pulpit"
  ]
  node [
    id 1229
    label "konstelacja"
  ]
  node [
    id 1230
    label "jednostka_geologiczna"
  ]
  node [
    id 1231
    label "o&#347;"
  ]
  node [
    id 1232
    label "podsystem"
  ]
  node [
    id 1233
    label "ryba"
  ]
  node [
    id 1234
    label "Leopard"
  ]
  node [
    id 1235
    label "Android"
  ]
  node [
    id 1236
    label "cybernetyk"
  ]
  node [
    id 1237
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1238
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1239
    label "sk&#322;ad"
  ]
  node [
    id 1240
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1241
    label "&#321;ubianka"
  ]
  node [
    id 1242
    label "dzia&#322;_personalny"
  ]
  node [
    id 1243
    label "Kreml"
  ]
  node [
    id 1244
    label "Bia&#322;y_Dom"
  ]
  node [
    id 1245
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 1246
    label "sadowisko"
  ]
  node [
    id 1247
    label "wokalistyka"
  ]
  node [
    id 1248
    label "wykonywanie"
  ]
  node [
    id 1249
    label "muza"
  ]
  node [
    id 1250
    label "wykonywa&#263;"
  ]
  node [
    id 1251
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 1252
    label "beatbox"
  ]
  node [
    id 1253
    label "komponowa&#263;"
  ]
  node [
    id 1254
    label "komponowanie"
  ]
  node [
    id 1255
    label "pasa&#380;"
  ]
  node [
    id 1256
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1257
    label "notacja_muzyczna"
  ]
  node [
    id 1258
    label "kontrapunkt"
  ]
  node [
    id 1259
    label "sztuka"
  ]
  node [
    id 1260
    label "instrumentalistyka"
  ]
  node [
    id 1261
    label "harmonia"
  ]
  node [
    id 1262
    label "set"
  ]
  node [
    id 1263
    label "wys&#322;uchanie"
  ]
  node [
    id 1264
    label "kapela"
  ]
  node [
    id 1265
    label "britpop"
  ]
  node [
    id 1266
    label "badanie"
  ]
  node [
    id 1267
    label "obserwowanie"
  ]
  node [
    id 1268
    label "wy&#347;wiadczenie"
  ]
  node [
    id 1269
    label "assay"
  ]
  node [
    id 1270
    label "checkup"
  ]
  node [
    id 1271
    label "spotkanie"
  ]
  node [
    id 1272
    label "do&#347;wiadczanie"
  ]
  node [
    id 1273
    label "zbadanie"
  ]
  node [
    id 1274
    label "potraktowanie"
  ]
  node [
    id 1275
    label "poczucie"
  ]
  node [
    id 1276
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 1277
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 1278
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 1279
    label "wykszta&#322;cenie"
  ]
  node [
    id 1280
    label "urszulanki_szare"
  ]
  node [
    id 1281
    label "proporcja"
  ]
  node [
    id 1282
    label "cognition"
  ]
  node [
    id 1283
    label "intelekt"
  ]
  node [
    id 1284
    label "pozwolenie"
  ]
  node [
    id 1285
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1286
    label "zaawansowanie"
  ]
  node [
    id 1287
    label "przekazanie"
  ]
  node [
    id 1288
    label "skopiowanie"
  ]
  node [
    id 1289
    label "arrangement"
  ]
  node [
    id 1290
    label "przeniesienie"
  ]
  node [
    id 1291
    label "testament"
  ]
  node [
    id 1292
    label "lekarstwo"
  ]
  node [
    id 1293
    label "zadanie"
  ]
  node [
    id 1294
    label "answer"
  ]
  node [
    id 1295
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 1296
    label "transcription"
  ]
  node [
    id 1297
    label "zalecenie"
  ]
  node [
    id 1298
    label "ucze&#324;"
  ]
  node [
    id 1299
    label "student"
  ]
  node [
    id 1300
    label "zaliczy&#263;"
  ]
  node [
    id 1301
    label "przekaza&#263;"
  ]
  node [
    id 1302
    label "powierzy&#263;"
  ]
  node [
    id 1303
    label "zmusi&#263;"
  ]
  node [
    id 1304
    label "translate"
  ]
  node [
    id 1305
    label "picture"
  ]
  node [
    id 1306
    label "przedstawi&#263;"
  ]
  node [
    id 1307
    label "z&#322;o&#380;y&#263;_egzamin"
  ]
  node [
    id 1308
    label "convey"
  ]
  node [
    id 1309
    label "fraza"
  ]
  node [
    id 1310
    label "stanowisko"
  ]
  node [
    id 1311
    label "wypowiedzenie"
  ]
  node [
    id 1312
    label "prison_term"
  ]
  node [
    id 1313
    label "okres"
  ]
  node [
    id 1314
    label "przedstawienie"
  ]
  node [
    id 1315
    label "wyra&#380;enie"
  ]
  node [
    id 1316
    label "zaliczenie"
  ]
  node [
    id 1317
    label "antylogizm"
  ]
  node [
    id 1318
    label "zmuszenie"
  ]
  node [
    id 1319
    label "konektyw"
  ]
  node [
    id 1320
    label "attitude"
  ]
  node [
    id 1321
    label "powierzenie"
  ]
  node [
    id 1322
    label "adjudication"
  ]
  node [
    id 1323
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 1324
    label "pass"
  ]
  node [
    id 1325
    label "supply"
  ]
  node [
    id 1326
    label "zaleci&#263;"
  ]
  node [
    id 1327
    label "rewrite"
  ]
  node [
    id 1328
    label "zrzec_si&#281;"
  ]
  node [
    id 1329
    label "skopiowa&#263;"
  ]
  node [
    id 1330
    label "przenie&#347;&#263;"
  ]
  node [
    id 1331
    label "political_orientation"
  ]
  node [
    id 1332
    label "stra&#380;nik"
  ]
  node [
    id 1333
    label "przedszkole"
  ]
  node [
    id 1334
    label "rozmiar&#243;wka"
  ]
  node [
    id 1335
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 1336
    label "tarcza"
  ]
  node [
    id 1337
    label "kosz"
  ]
  node [
    id 1338
    label "transparent"
  ]
  node [
    id 1339
    label "rubryka"
  ]
  node [
    id 1340
    label "kontener"
  ]
  node [
    id 1341
    label "plate"
  ]
  node [
    id 1342
    label "konstrukcja"
  ]
  node [
    id 1343
    label "szachownica_Punnetta"
  ]
  node [
    id 1344
    label "chart"
  ]
  node [
    id 1345
    label "izba"
  ]
  node [
    id 1346
    label "biurko"
  ]
  node [
    id 1347
    label "boks"
  ]
  node [
    id 1348
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 1349
    label "egzekutywa"
  ]
  node [
    id 1350
    label "premier"
  ]
  node [
    id 1351
    label "Londyn"
  ]
  node [
    id 1352
    label "palestra"
  ]
  node [
    id 1353
    label "pok&#243;j"
  ]
  node [
    id 1354
    label "pracownia"
  ]
  node [
    id 1355
    label "gabinet_cieni"
  ]
  node [
    id 1356
    label "pomieszczenie"
  ]
  node [
    id 1357
    label "Konsulat"
  ]
  node [
    id 1358
    label "wagon"
  ]
  node [
    id 1359
    label "mecz_mistrzowski"
  ]
  node [
    id 1360
    label "class"
  ]
  node [
    id 1361
    label "&#322;awka"
  ]
  node [
    id 1362
    label "wykrzyknik"
  ]
  node [
    id 1363
    label "zaleta"
  ]
  node [
    id 1364
    label "programowanie_obiektowe"
  ]
  node [
    id 1365
    label "rezerwa"
  ]
  node [
    id 1366
    label "Ekwici"
  ]
  node [
    id 1367
    label "&#347;rodowisko"
  ]
  node [
    id 1368
    label "sala"
  ]
  node [
    id 1369
    label "pomoc"
  ]
  node [
    id 1370
    label "znak_jako&#347;ci"
  ]
  node [
    id 1371
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1372
    label "promocja"
  ]
  node [
    id 1373
    label "kurs"
  ]
  node [
    id 1374
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 1375
    label "dziennik_lekcyjny"
  ]
  node [
    id 1376
    label "typ"
  ]
  node [
    id 1377
    label "fakcja"
  ]
  node [
    id 1378
    label "obrona"
  ]
  node [
    id 1379
    label "atak"
  ]
  node [
    id 1380
    label "botanika"
  ]
  node [
    id 1381
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1382
    label "Wallenrod"
  ]
  node [
    id 1383
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 1384
    label "umocowa&#263;"
  ]
  node [
    id 1385
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 1386
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1387
    label "procesualistyka"
  ]
  node [
    id 1388
    label "regu&#322;a_Allena"
  ]
  node [
    id 1389
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1390
    label "kryminalistyka"
  ]
  node [
    id 1391
    label "kierunek"
  ]
  node [
    id 1392
    label "zasada_d'Alemberta"
  ]
  node [
    id 1393
    label "obserwacja"
  ]
  node [
    id 1394
    label "normatywizm"
  ]
  node [
    id 1395
    label "jurisprudence"
  ]
  node [
    id 1396
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 1397
    label "przepis"
  ]
  node [
    id 1398
    label "prawo_karne_procesowe"
  ]
  node [
    id 1399
    label "criterion"
  ]
  node [
    id 1400
    label "kazuistyka"
  ]
  node [
    id 1401
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 1402
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 1403
    label "kryminologia"
  ]
  node [
    id 1404
    label "opis"
  ]
  node [
    id 1405
    label "regu&#322;a_Glogera"
  ]
  node [
    id 1406
    label "prawo_Mendla"
  ]
  node [
    id 1407
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 1408
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 1409
    label "prawo_karne"
  ]
  node [
    id 1410
    label "twierdzenie"
  ]
  node [
    id 1411
    label "cywilistyka"
  ]
  node [
    id 1412
    label "judykatura"
  ]
  node [
    id 1413
    label "kanonistyka"
  ]
  node [
    id 1414
    label "nauka_prawa"
  ]
  node [
    id 1415
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 1416
    label "podmiot"
  ]
  node [
    id 1417
    label "law"
  ]
  node [
    id 1418
    label "qualification"
  ]
  node [
    id 1419
    label "dominion"
  ]
  node [
    id 1420
    label "wykonawczy"
  ]
  node [
    id 1421
    label "normalizacja"
  ]
  node [
    id 1422
    label "wypowied&#378;"
  ]
  node [
    id 1423
    label "exposition"
  ]
  node [
    id 1424
    label "obja&#347;nienie"
  ]
  node [
    id 1425
    label "organizowa&#263;"
  ]
  node [
    id 1426
    label "ordinariness"
  ]
  node [
    id 1427
    label "zorganizowa&#263;"
  ]
  node [
    id 1428
    label "taniec_towarzyski"
  ]
  node [
    id 1429
    label "organizowanie"
  ]
  node [
    id 1430
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 1431
    label "zorganizowanie"
  ]
  node [
    id 1432
    label "mechanika"
  ]
  node [
    id 1433
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1434
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1435
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1436
    label "przeorientowywanie"
  ]
  node [
    id 1437
    label "studia"
  ]
  node [
    id 1438
    label "linia"
  ]
  node [
    id 1439
    label "bok"
  ]
  node [
    id 1440
    label "skr&#281;canie"
  ]
  node [
    id 1441
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1442
    label "przeorientowywa&#263;"
  ]
  node [
    id 1443
    label "orientowanie"
  ]
  node [
    id 1444
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1445
    label "przeorientowanie"
  ]
  node [
    id 1446
    label "zorientowanie"
  ]
  node [
    id 1447
    label "przeorientowa&#263;"
  ]
  node [
    id 1448
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1449
    label "zorientowa&#263;"
  ]
  node [
    id 1450
    label "g&#243;ra"
  ]
  node [
    id 1451
    label "orientowa&#263;"
  ]
  node [
    id 1452
    label "orientacja"
  ]
  node [
    id 1453
    label "bearing"
  ]
  node [
    id 1454
    label "skr&#281;cenie"
  ]
  node [
    id 1455
    label "posiada&#263;"
  ]
  node [
    id 1456
    label "potencja&#322;"
  ]
  node [
    id 1457
    label "wyb&#243;r"
  ]
  node [
    id 1458
    label "prospect"
  ]
  node [
    id 1459
    label "ability"
  ]
  node [
    id 1460
    label "obliczeniowo"
  ]
  node [
    id 1461
    label "alternatywa"
  ]
  node [
    id 1462
    label "operator_modalny"
  ]
  node [
    id 1463
    label "remark"
  ]
  node [
    id 1464
    label "stwierdzenie"
  ]
  node [
    id 1465
    label "observation"
  ]
  node [
    id 1466
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 1467
    label "alternatywa_Fredholma"
  ]
  node [
    id 1468
    label "oznajmianie"
  ]
  node [
    id 1469
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 1470
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 1471
    label "paradoks_Leontiefa"
  ]
  node [
    id 1472
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 1473
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 1474
    label "teza"
  ]
  node [
    id 1475
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 1476
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 1477
    label "twierdzenie_Pettisa"
  ]
  node [
    id 1478
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 1479
    label "twierdzenie_Maya"
  ]
  node [
    id 1480
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 1481
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 1482
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 1483
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 1484
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 1485
    label "zapewnianie"
  ]
  node [
    id 1486
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 1487
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 1488
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 1489
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 1490
    label "twierdzenie_Stokesa"
  ]
  node [
    id 1491
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 1492
    label "twierdzenie_Cevy"
  ]
  node [
    id 1493
    label "twierdzenie_Pascala"
  ]
  node [
    id 1494
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 1495
    label "komunikowanie"
  ]
  node [
    id 1496
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 1497
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 1498
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 1499
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 1500
    label "relacja"
  ]
  node [
    id 1501
    label "calibration"
  ]
  node [
    id 1502
    label "dominance"
  ]
  node [
    id 1503
    label "standardization"
  ]
  node [
    id 1504
    label "zmiana"
  ]
  node [
    id 1505
    label "orzecznictwo"
  ]
  node [
    id 1506
    label "wykonawczo"
  ]
  node [
    id 1507
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 1508
    label "nada&#263;"
  ]
  node [
    id 1509
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 1510
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 1511
    label "cook"
  ]
  node [
    id 1512
    label "norma_prawna"
  ]
  node [
    id 1513
    label "przedawnienie_si&#281;"
  ]
  node [
    id 1514
    label "przedawnianie_si&#281;"
  ]
  node [
    id 1515
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 1516
    label "regulation"
  ]
  node [
    id 1517
    label "recepta"
  ]
  node [
    id 1518
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 1519
    label "kodeks"
  ]
  node [
    id 1520
    label "base"
  ]
  node [
    id 1521
    label "umowa"
  ]
  node [
    id 1522
    label "moralno&#347;&#263;"
  ]
  node [
    id 1523
    label "occupation"
  ]
  node [
    id 1524
    label "substancja"
  ]
  node [
    id 1525
    label "prawid&#322;o"
  ]
  node [
    id 1526
    label "casuistry"
  ]
  node [
    id 1527
    label "manipulacja"
  ]
  node [
    id 1528
    label "probabilizm"
  ]
  node [
    id 1529
    label "dermatoglifika"
  ]
  node [
    id 1530
    label "mikro&#347;lad"
  ]
  node [
    id 1531
    label "technika_&#347;ledcza"
  ]
  node [
    id 1532
    label "po_m&#281;sku"
  ]
  node [
    id 1533
    label "zdecydowany"
  ]
  node [
    id 1534
    label "stosowny"
  ]
  node [
    id 1535
    label "toaleta"
  ]
  node [
    id 1536
    label "typowy"
  ]
  node [
    id 1537
    label "m&#281;sko"
  ]
  node [
    id 1538
    label "podobny"
  ]
  node [
    id 1539
    label "typowo"
  ]
  node [
    id 1540
    label "stosownie"
  ]
  node [
    id 1541
    label "zdecydowanie"
  ]
  node [
    id 1542
    label "odr&#281;bnie"
  ]
  node [
    id 1543
    label "prawdziwie"
  ]
  node [
    id 1544
    label "nale&#380;yty"
  ]
  node [
    id 1545
    label "ust&#281;p"
  ]
  node [
    id 1546
    label "dressing"
  ]
  node [
    id 1547
    label "kibel"
  ]
  node [
    id 1548
    label "klozetka"
  ]
  node [
    id 1549
    label "prewet"
  ]
  node [
    id 1550
    label "sypialnia"
  ]
  node [
    id 1551
    label "kreacja"
  ]
  node [
    id 1552
    label "mycie"
  ]
  node [
    id 1553
    label "kosmetyka"
  ]
  node [
    id 1554
    label "podw&#322;o&#347;nik"
  ]
  node [
    id 1555
    label "mebel"
  ]
  node [
    id 1556
    label "sracz"
  ]
  node [
    id 1557
    label "&#380;ywny"
  ]
  node [
    id 1558
    label "szczery"
  ]
  node [
    id 1559
    label "naprawd&#281;"
  ]
  node [
    id 1560
    label "realnie"
  ]
  node [
    id 1561
    label "zgodny"
  ]
  node [
    id 1562
    label "m&#261;dry"
  ]
  node [
    id 1563
    label "pewny"
  ]
  node [
    id 1564
    label "zauwa&#380;alny"
  ]
  node [
    id 1565
    label "gotowy"
  ]
  node [
    id 1566
    label "wydoro&#347;lenie"
  ]
  node [
    id 1567
    label "du&#380;y"
  ]
  node [
    id 1568
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1569
    label "doro&#347;lenie"
  ]
  node [
    id 1570
    label "&#378;ra&#322;y"
  ]
  node [
    id 1571
    label "doro&#347;le"
  ]
  node [
    id 1572
    label "dojrzale"
  ]
  node [
    id 1573
    label "doletni"
  ]
  node [
    id 1574
    label "zwyczajny"
  ]
  node [
    id 1575
    label "cz&#281;sty"
  ]
  node [
    id 1576
    label "zwyk&#322;y"
  ]
  node [
    id 1577
    label "przypominanie"
  ]
  node [
    id 1578
    label "podobnie"
  ]
  node [
    id 1579
    label "upodabnianie_si&#281;"
  ]
  node [
    id 1580
    label "upodobnienie"
  ]
  node [
    id 1581
    label "drugi"
  ]
  node [
    id 1582
    label "taki"
  ]
  node [
    id 1583
    label "upodobnienie_si&#281;"
  ]
  node [
    id 1584
    label "zasymilowanie"
  ]
  node [
    id 1585
    label "wyodr&#281;bnienie_si&#281;"
  ]
  node [
    id 1586
    label "kolejny"
  ]
  node [
    id 1587
    label "wydzielenie"
  ]
  node [
    id 1588
    label "inszy"
  ]
  node [
    id 1589
    label "wyodr&#281;bnianie"
  ]
  node [
    id 1590
    label "inny"
  ]
  node [
    id 1591
    label "wyodr&#281;bnianie_si&#281;"
  ]
  node [
    id 1592
    label "mildew"
  ]
  node [
    id 1593
    label "punkt_odniesienia"
  ]
  node [
    id 1594
    label "ideal"
  ]
  node [
    id 1595
    label "narz&#281;dzie"
  ]
  node [
    id 1596
    label "nature"
  ]
  node [
    id 1597
    label "move"
  ]
  node [
    id 1598
    label "poruszenie"
  ]
  node [
    id 1599
    label "movement"
  ]
  node [
    id 1600
    label "myk"
  ]
  node [
    id 1601
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1602
    label "travel"
  ]
  node [
    id 1603
    label "kanciasty"
  ]
  node [
    id 1604
    label "commercial_enterprise"
  ]
  node [
    id 1605
    label "strumie&#324;"
  ]
  node [
    id 1606
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1607
    label "apraksja"
  ]
  node [
    id 1608
    label "natural_process"
  ]
  node [
    id 1609
    label "d&#322;ugi"
  ]
  node [
    id 1610
    label "dyssypacja_energii"
  ]
  node [
    id 1611
    label "tumult"
  ]
  node [
    id 1612
    label "manewr"
  ]
  node [
    id 1613
    label "lokomocja"
  ]
  node [
    id 1614
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1615
    label "komunikacja"
  ]
  node [
    id 1616
    label "drift"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 232
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 276
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 241
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 256
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 863
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 864
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 865
  ]
  edge [
    source 12
    target 866
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 871
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 874
  ]
  edge [
    source 12
    target 875
  ]
  edge [
    source 12
    target 876
  ]
  edge [
    source 12
    target 877
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 878
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 245
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 897
  ]
  edge [
    source 14
    target 898
  ]
  edge [
    source 14
    target 899
  ]
  edge [
    source 14
    target 900
  ]
  edge [
    source 14
    target 901
  ]
  edge [
    source 14
    target 902
  ]
  edge [
    source 14
    target 903
  ]
  edge [
    source 14
    target 904
  ]
  edge [
    source 14
    target 905
  ]
  edge [
    source 14
    target 906
  ]
  edge [
    source 14
    target 907
  ]
  edge [
    source 14
    target 908
  ]
  edge [
    source 14
    target 909
  ]
  edge [
    source 14
    target 910
  ]
  edge [
    source 14
    target 911
  ]
  edge [
    source 14
    target 333
  ]
  edge [
    source 14
    target 912
  ]
  edge [
    source 14
    target 913
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 14
    target 794
  ]
  edge [
    source 14
    target 233
  ]
  edge [
    source 14
    target 914
  ]
  edge [
    source 14
    target 235
  ]
  edge [
    source 14
    target 915
  ]
  edge [
    source 14
    target 37
  ]
  edge [
    source 14
    target 916
  ]
  edge [
    source 14
    target 917
  ]
  edge [
    source 14
    target 237
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 918
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 920
  ]
  edge [
    source 15
    target 921
  ]
  edge [
    source 15
    target 922
  ]
  edge [
    source 15
    target 923
  ]
  edge [
    source 15
    target 924
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 925
  ]
  edge [
    source 15
    target 926
  ]
  edge [
    source 15
    target 927
  ]
  edge [
    source 15
    target 928
  ]
  edge [
    source 15
    target 929
  ]
  edge [
    source 15
    target 461
  ]
  edge [
    source 15
    target 930
  ]
  edge [
    source 15
    target 931
  ]
  edge [
    source 15
    target 932
  ]
  edge [
    source 15
    target 933
  ]
  edge [
    source 15
    target 934
  ]
  edge [
    source 15
    target 935
  ]
  edge [
    source 15
    target 936
  ]
  edge [
    source 15
    target 937
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 938
  ]
  edge [
    source 15
    target 939
  ]
  edge [
    source 15
    target 510
  ]
  edge [
    source 15
    target 940
  ]
  edge [
    source 15
    target 941
  ]
  edge [
    source 15
    target 942
  ]
  edge [
    source 15
    target 943
  ]
  edge [
    source 15
    target 944
  ]
  edge [
    source 15
    target 675
  ]
  edge [
    source 15
    target 945
  ]
  edge [
    source 15
    target 946
  ]
  edge [
    source 15
    target 488
  ]
  edge [
    source 15
    target 947
  ]
  edge [
    source 15
    target 948
  ]
  edge [
    source 15
    target 229
  ]
  edge [
    source 15
    target 949
  ]
  edge [
    source 15
    target 950
  ]
  edge [
    source 15
    target 951
  ]
  edge [
    source 15
    target 952
  ]
  edge [
    source 15
    target 953
  ]
  edge [
    source 15
    target 954
  ]
  edge [
    source 15
    target 955
  ]
  edge [
    source 15
    target 956
  ]
  edge [
    source 15
    target 957
  ]
  edge [
    source 15
    target 958
  ]
  edge [
    source 15
    target 959
  ]
  edge [
    source 15
    target 960
  ]
  edge [
    source 15
    target 961
  ]
  edge [
    source 15
    target 962
  ]
  edge [
    source 15
    target 963
  ]
  edge [
    source 15
    target 964
  ]
  edge [
    source 15
    target 965
  ]
  edge [
    source 15
    target 966
  ]
  edge [
    source 15
    target 967
  ]
  edge [
    source 15
    target 968
  ]
  edge [
    source 15
    target 969
  ]
  edge [
    source 15
    target 970
  ]
  edge [
    source 15
    target 971
  ]
  edge [
    source 15
    target 972
  ]
  edge [
    source 15
    target 973
  ]
  edge [
    source 15
    target 419
  ]
  edge [
    source 15
    target 974
  ]
  edge [
    source 15
    target 975
  ]
  edge [
    source 15
    target 976
  ]
  edge [
    source 15
    target 977
  ]
  edge [
    source 15
    target 978
  ]
  edge [
    source 15
    target 979
  ]
  edge [
    source 15
    target 980
  ]
  edge [
    source 15
    target 981
  ]
  edge [
    source 15
    target 982
  ]
  edge [
    source 15
    target 983
  ]
  edge [
    source 15
    target 984
  ]
  edge [
    source 15
    target 985
  ]
  edge [
    source 15
    target 986
  ]
  edge [
    source 15
    target 987
  ]
  edge [
    source 15
    target 988
  ]
  edge [
    source 15
    target 989
  ]
  edge [
    source 15
    target 990
  ]
  edge [
    source 15
    target 991
  ]
  edge [
    source 15
    target 992
  ]
  edge [
    source 15
    target 993
  ]
  edge [
    source 15
    target 994
  ]
  edge [
    source 15
    target 995
  ]
  edge [
    source 15
    target 996
  ]
  edge [
    source 15
    target 997
  ]
  edge [
    source 15
    target 998
  ]
  edge [
    source 15
    target 999
  ]
  edge [
    source 15
    target 1000
  ]
  edge [
    source 15
    target 1001
  ]
  edge [
    source 15
    target 1002
  ]
  edge [
    source 15
    target 1003
  ]
  edge [
    source 15
    target 1004
  ]
  edge [
    source 15
    target 443
  ]
  edge [
    source 15
    target 1005
  ]
  edge [
    source 15
    target 1006
  ]
  edge [
    source 15
    target 1007
  ]
  edge [
    source 15
    target 1008
  ]
  edge [
    source 15
    target 1009
  ]
  edge [
    source 15
    target 1010
  ]
  edge [
    source 15
    target 1011
  ]
  edge [
    source 15
    target 1012
  ]
  edge [
    source 15
    target 1013
  ]
  edge [
    source 15
    target 1014
  ]
  edge [
    source 15
    target 1015
  ]
  edge [
    source 15
    target 1016
  ]
  edge [
    source 15
    target 1017
  ]
  edge [
    source 15
    target 1018
  ]
  edge [
    source 15
    target 1019
  ]
  edge [
    source 15
    target 1020
  ]
  edge [
    source 15
    target 1021
  ]
  edge [
    source 15
    target 1022
  ]
  edge [
    source 15
    target 1023
  ]
  edge [
    source 15
    target 1024
  ]
  edge [
    source 15
    target 1025
  ]
  edge [
    source 15
    target 1026
  ]
  edge [
    source 15
    target 1027
  ]
  edge [
    source 15
    target 1028
  ]
  edge [
    source 15
    target 1029
  ]
  edge [
    source 15
    target 1030
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 1031
  ]
  edge [
    source 15
    target 1032
  ]
  edge [
    source 15
    target 1033
  ]
  edge [
    source 15
    target 1034
  ]
  edge [
    source 15
    target 1035
  ]
  edge [
    source 15
    target 324
  ]
  edge [
    source 15
    target 1036
  ]
  edge [
    source 15
    target 1037
  ]
  edge [
    source 15
    target 1038
  ]
  edge [
    source 15
    target 1039
  ]
  edge [
    source 15
    target 1040
  ]
  edge [
    source 15
    target 1041
  ]
  edge [
    source 15
    target 1042
  ]
  edge [
    source 15
    target 1043
  ]
  edge [
    source 15
    target 1044
  ]
  edge [
    source 15
    target 1045
  ]
  edge [
    source 15
    target 1046
  ]
  edge [
    source 15
    target 1047
  ]
  edge [
    source 15
    target 408
  ]
  edge [
    source 15
    target 1048
  ]
  edge [
    source 15
    target 1049
  ]
  edge [
    source 15
    target 1050
  ]
  edge [
    source 15
    target 1051
  ]
  edge [
    source 15
    target 1052
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 1053
  ]
  edge [
    source 15
    target 1054
  ]
  edge [
    source 15
    target 1055
  ]
  edge [
    source 15
    target 1056
  ]
  edge [
    source 15
    target 1057
  ]
  edge [
    source 15
    target 1058
  ]
  edge [
    source 15
    target 1059
  ]
  edge [
    source 15
    target 1060
  ]
  edge [
    source 15
    target 1061
  ]
  edge [
    source 15
    target 1062
  ]
  edge [
    source 15
    target 1063
  ]
  edge [
    source 15
    target 1064
  ]
  edge [
    source 15
    target 1065
  ]
  edge [
    source 15
    target 1066
  ]
  edge [
    source 15
    target 1067
  ]
  edge [
    source 15
    target 1068
  ]
  edge [
    source 15
    target 1069
  ]
  edge [
    source 15
    target 1070
  ]
  edge [
    source 15
    target 1071
  ]
  edge [
    source 15
    target 1072
  ]
  edge [
    source 15
    target 1073
  ]
  edge [
    source 15
    target 1074
  ]
  edge [
    source 15
    target 404
  ]
  edge [
    source 15
    target 1075
  ]
  edge [
    source 15
    target 1076
  ]
  edge [
    source 15
    target 1077
  ]
  edge [
    source 15
    target 1078
  ]
  edge [
    source 15
    target 1079
  ]
  edge [
    source 15
    target 1080
  ]
  edge [
    source 15
    target 1081
  ]
  edge [
    source 15
    target 1082
  ]
  edge [
    source 15
    target 1083
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1084
  ]
  edge [
    source 16
    target 1085
  ]
  edge [
    source 16
    target 1086
  ]
  edge [
    source 16
    target 1087
  ]
  edge [
    source 16
    target 1088
  ]
  edge [
    source 16
    target 1089
  ]
  edge [
    source 16
    target 1090
  ]
  edge [
    source 16
    target 1091
  ]
  edge [
    source 16
    target 1092
  ]
  edge [
    source 16
    target 1093
  ]
  edge [
    source 16
    target 1094
  ]
  edge [
    source 16
    target 1095
  ]
  edge [
    source 16
    target 1096
  ]
  edge [
    source 16
    target 1097
  ]
  edge [
    source 16
    target 1098
  ]
  edge [
    source 16
    target 1099
  ]
  edge [
    source 16
    target 1100
  ]
  edge [
    source 16
    target 1101
  ]
  edge [
    source 16
    target 1102
  ]
  edge [
    source 16
    target 942
  ]
  edge [
    source 16
    target 1103
  ]
  edge [
    source 16
    target 1104
  ]
  edge [
    source 16
    target 1105
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 16
    target 1106
  ]
  edge [
    source 16
    target 1107
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 1108
  ]
  edge [
    source 16
    target 1109
  ]
  edge [
    source 16
    target 1110
  ]
  edge [
    source 16
    target 1111
  ]
  edge [
    source 16
    target 1112
  ]
  edge [
    source 16
    target 1113
  ]
  edge [
    source 16
    target 1114
  ]
  edge [
    source 16
    target 1115
  ]
  edge [
    source 16
    target 1116
  ]
  edge [
    source 16
    target 1117
  ]
  edge [
    source 16
    target 1118
  ]
  edge [
    source 16
    target 1119
  ]
  edge [
    source 16
    target 1120
  ]
  edge [
    source 16
    target 1121
  ]
  edge [
    source 16
    target 1122
  ]
  edge [
    source 16
    target 1123
  ]
  edge [
    source 16
    target 1124
  ]
  edge [
    source 16
    target 1125
  ]
  edge [
    source 16
    target 946
  ]
  edge [
    source 16
    target 1126
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 947
  ]
  edge [
    source 16
    target 1127
  ]
  edge [
    source 16
    target 1128
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 1129
  ]
  edge [
    source 16
    target 1130
  ]
  edge [
    source 16
    target 1131
  ]
  edge [
    source 16
    target 1132
  ]
  edge [
    source 16
    target 1133
  ]
  edge [
    source 16
    target 1134
  ]
  edge [
    source 16
    target 1135
  ]
  edge [
    source 16
    target 1136
  ]
  edge [
    source 16
    target 1137
  ]
  edge [
    source 16
    target 1138
  ]
  edge [
    source 16
    target 1139
  ]
  edge [
    source 16
    target 1140
  ]
  edge [
    source 16
    target 1141
  ]
  edge [
    source 16
    target 1142
  ]
  edge [
    source 16
    target 1143
  ]
  edge [
    source 16
    target 1144
  ]
  edge [
    source 16
    target 1145
  ]
  edge [
    source 16
    target 1146
  ]
  edge [
    source 16
    target 1147
  ]
  edge [
    source 16
    target 1148
  ]
  edge [
    source 16
    target 1149
  ]
  edge [
    source 16
    target 1150
  ]
  edge [
    source 16
    target 1151
  ]
  edge [
    source 16
    target 1152
  ]
  edge [
    source 16
    target 1153
  ]
  edge [
    source 16
    target 1154
  ]
  edge [
    source 16
    target 1155
  ]
  edge [
    source 16
    target 1156
  ]
  edge [
    source 16
    target 1157
  ]
  edge [
    source 16
    target 1158
  ]
  edge [
    source 16
    target 1159
  ]
  edge [
    source 16
    target 1160
  ]
  edge [
    source 16
    target 1161
  ]
  edge [
    source 16
    target 1162
  ]
  edge [
    source 16
    target 1163
  ]
  edge [
    source 16
    target 1164
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 1165
  ]
  edge [
    source 16
    target 1166
  ]
  edge [
    source 16
    target 1167
  ]
  edge [
    source 16
    target 1168
  ]
  edge [
    source 16
    target 27
  ]
  edge [
    source 16
    target 1169
  ]
  edge [
    source 16
    target 1170
  ]
  edge [
    source 16
    target 1171
  ]
  edge [
    source 16
    target 1172
  ]
  edge [
    source 16
    target 1173
  ]
  edge [
    source 16
    target 1174
  ]
  edge [
    source 16
    target 1175
  ]
  edge [
    source 16
    target 1176
  ]
  edge [
    source 16
    target 1177
  ]
  edge [
    source 16
    target 1178
  ]
  edge [
    source 16
    target 1179
  ]
  edge [
    source 16
    target 494
  ]
  edge [
    source 16
    target 1180
  ]
  edge [
    source 16
    target 1181
  ]
  edge [
    source 16
    target 1182
  ]
  edge [
    source 16
    target 1183
  ]
  edge [
    source 16
    target 1184
  ]
  edge [
    source 16
    target 1185
  ]
  edge [
    source 16
    target 1186
  ]
  edge [
    source 16
    target 1187
  ]
  edge [
    source 16
    target 1188
  ]
  edge [
    source 16
    target 1189
  ]
  edge [
    source 16
    target 1190
  ]
  edge [
    source 16
    target 1191
  ]
  edge [
    source 16
    target 1192
  ]
  edge [
    source 16
    target 1193
  ]
  edge [
    source 16
    target 1194
  ]
  edge [
    source 16
    target 1195
  ]
  edge [
    source 16
    target 1196
  ]
  edge [
    source 16
    target 1197
  ]
  edge [
    source 16
    target 1198
  ]
  edge [
    source 16
    target 1199
  ]
  edge [
    source 16
    target 1200
  ]
  edge [
    source 16
    target 1201
  ]
  edge [
    source 16
    target 1202
  ]
  edge [
    source 16
    target 1203
  ]
  edge [
    source 16
    target 1204
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 1205
  ]
  edge [
    source 16
    target 1206
  ]
  edge [
    source 16
    target 1207
  ]
  edge [
    source 16
    target 1208
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 276
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 1209
  ]
  edge [
    source 16
    target 1210
  ]
  edge [
    source 16
    target 1211
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 1212
  ]
  edge [
    source 16
    target 1213
  ]
  edge [
    source 16
    target 1214
  ]
  edge [
    source 16
    target 1215
  ]
  edge [
    source 16
    target 1216
  ]
  edge [
    source 16
    target 54
  ]
  edge [
    source 16
    target 1217
  ]
  edge [
    source 16
    target 1218
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 1219
  ]
  edge [
    source 16
    target 1220
  ]
  edge [
    source 16
    target 1221
  ]
  edge [
    source 16
    target 1222
  ]
  edge [
    source 16
    target 1223
  ]
  edge [
    source 16
    target 1224
  ]
  edge [
    source 16
    target 1225
  ]
  edge [
    source 16
    target 1226
  ]
  edge [
    source 16
    target 1227
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 16
    target 1228
  ]
  edge [
    source 16
    target 1229
  ]
  edge [
    source 16
    target 1230
  ]
  edge [
    source 16
    target 1231
  ]
  edge [
    source 16
    target 1232
  ]
  edge [
    source 16
    target 1233
  ]
  edge [
    source 16
    target 1234
  ]
  edge [
    source 16
    target 1235
  ]
  edge [
    source 16
    target 1009
  ]
  edge [
    source 16
    target 1236
  ]
  edge [
    source 16
    target 1237
  ]
  edge [
    source 16
    target 1238
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 1239
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 1240
  ]
  edge [
    source 16
    target 1241
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 1242
  ]
  edge [
    source 16
    target 1243
  ]
  edge [
    source 16
    target 1244
  ]
  edge [
    source 16
    target 492
  ]
  edge [
    source 16
    target 324
  ]
  edge [
    source 16
    target 1245
  ]
  edge [
    source 16
    target 1246
  ]
  edge [
    source 16
    target 1247
  ]
  edge [
    source 16
    target 225
  ]
  edge [
    source 16
    target 1248
  ]
  edge [
    source 16
    target 1249
  ]
  edge [
    source 16
    target 1250
  ]
  edge [
    source 16
    target 1251
  ]
  edge [
    source 16
    target 1252
  ]
  edge [
    source 16
    target 1253
  ]
  edge [
    source 16
    target 1254
  ]
  edge [
    source 16
    target 65
  ]
  edge [
    source 16
    target 1255
  ]
  edge [
    source 16
    target 1256
  ]
  edge [
    source 16
    target 1257
  ]
  edge [
    source 16
    target 1258
  ]
  edge [
    source 16
    target 1259
  ]
  edge [
    source 16
    target 1260
  ]
  edge [
    source 16
    target 1261
  ]
  edge [
    source 16
    target 1262
  ]
  edge [
    source 16
    target 1263
  ]
  edge [
    source 16
    target 1264
  ]
  edge [
    source 16
    target 1265
  ]
  edge [
    source 16
    target 1266
  ]
  edge [
    source 16
    target 1267
  ]
  edge [
    source 16
    target 1268
  ]
  edge [
    source 16
    target 37
  ]
  edge [
    source 16
    target 1269
  ]
  edge [
    source 16
    target 1270
  ]
  edge [
    source 16
    target 1271
  ]
  edge [
    source 16
    target 1272
  ]
  edge [
    source 16
    target 1273
  ]
  edge [
    source 16
    target 1274
  ]
  edge [
    source 16
    target 1275
  ]
  edge [
    source 16
    target 1276
  ]
  edge [
    source 16
    target 1277
  ]
  edge [
    source 16
    target 1278
  ]
  edge [
    source 16
    target 1279
  ]
  edge [
    source 16
    target 1280
  ]
  edge [
    source 16
    target 1281
  ]
  edge [
    source 16
    target 1282
  ]
  edge [
    source 16
    target 1283
  ]
  edge [
    source 16
    target 1284
  ]
  edge [
    source 16
    target 1285
  ]
  edge [
    source 16
    target 1286
  ]
  edge [
    source 16
    target 1287
  ]
  edge [
    source 16
    target 1288
  ]
  edge [
    source 16
    target 1289
  ]
  edge [
    source 16
    target 1290
  ]
  edge [
    source 16
    target 1291
  ]
  edge [
    source 16
    target 1292
  ]
  edge [
    source 16
    target 1293
  ]
  edge [
    source 16
    target 1294
  ]
  edge [
    source 16
    target 1295
  ]
  edge [
    source 16
    target 1296
  ]
  edge [
    source 16
    target 1297
  ]
  edge [
    source 16
    target 1298
  ]
  edge [
    source 16
    target 1299
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 1300
  ]
  edge [
    source 16
    target 1301
  ]
  edge [
    source 16
    target 1302
  ]
  edge [
    source 16
    target 1303
  ]
  edge [
    source 16
    target 1304
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 1305
  ]
  edge [
    source 16
    target 1306
  ]
  edge [
    source 16
    target 1307
  ]
  edge [
    source 16
    target 1308
  ]
  edge [
    source 16
    target 1309
  ]
  edge [
    source 16
    target 1310
  ]
  edge [
    source 16
    target 1311
  ]
  edge [
    source 16
    target 1312
  ]
  edge [
    source 16
    target 1313
  ]
  edge [
    source 16
    target 1314
  ]
  edge [
    source 16
    target 1315
  ]
  edge [
    source 16
    target 1316
  ]
  edge [
    source 16
    target 1317
  ]
  edge [
    source 16
    target 1318
  ]
  edge [
    source 16
    target 1319
  ]
  edge [
    source 16
    target 1320
  ]
  edge [
    source 16
    target 1321
  ]
  edge [
    source 16
    target 1322
  ]
  edge [
    source 16
    target 1323
  ]
  edge [
    source 16
    target 1324
  ]
  edge [
    source 16
    target 1325
  ]
  edge [
    source 16
    target 1326
  ]
  edge [
    source 16
    target 1327
  ]
  edge [
    source 16
    target 1328
  ]
  edge [
    source 16
    target 1329
  ]
  edge [
    source 16
    target 1330
  ]
  edge [
    source 16
    target 1331
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 1332
  ]
  edge [
    source 16
    target 1333
  ]
  edge [
    source 16
    target 487
  ]
  edge [
    source 16
    target 552
  ]
  edge [
    source 16
    target 1334
  ]
  edge [
    source 16
    target 1022
  ]
  edge [
    source 16
    target 1335
  ]
  edge [
    source 16
    target 1336
  ]
  edge [
    source 16
    target 1337
  ]
  edge [
    source 16
    target 1338
  ]
  edge [
    source 16
    target 1054
  ]
  edge [
    source 16
    target 1339
  ]
  edge [
    source 16
    target 1340
  ]
  edge [
    source 16
    target 56
  ]
  edge [
    source 16
    target 1341
  ]
  edge [
    source 16
    target 1342
  ]
  edge [
    source 16
    target 1343
  ]
  edge [
    source 16
    target 1344
  ]
  edge [
    source 16
    target 1345
  ]
  edge [
    source 16
    target 1346
  ]
  edge [
    source 16
    target 1347
  ]
  edge [
    source 16
    target 1348
  ]
  edge [
    source 16
    target 1349
  ]
  edge [
    source 16
    target 1350
  ]
  edge [
    source 16
    target 1351
  ]
  edge [
    source 16
    target 1352
  ]
  edge [
    source 16
    target 1353
  ]
  edge [
    source 16
    target 1354
  ]
  edge [
    source 16
    target 1355
  ]
  edge [
    source 16
    target 1356
  ]
  edge [
    source 16
    target 1357
  ]
  edge [
    source 16
    target 1358
  ]
  edge [
    source 16
    target 1359
  ]
  edge [
    source 16
    target 1360
  ]
  edge [
    source 16
    target 1361
  ]
  edge [
    source 16
    target 1362
  ]
  edge [
    source 16
    target 1363
  ]
  edge [
    source 16
    target 1364
  ]
  edge [
    source 16
    target 312
  ]
  edge [
    source 16
    target 1365
  ]
  edge [
    source 16
    target 1366
  ]
  edge [
    source 16
    target 1367
  ]
  edge [
    source 16
    target 1368
  ]
  edge [
    source 16
    target 1369
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 1370
  ]
  edge [
    source 16
    target 1371
  ]
  edge [
    source 16
    target 333
  ]
  edge [
    source 16
    target 1372
  ]
  edge [
    source 16
    target 1373
  ]
  edge [
    source 16
    target 408
  ]
  edge [
    source 16
    target 1374
  ]
  edge [
    source 16
    target 1375
  ]
  edge [
    source 16
    target 1376
  ]
  edge [
    source 16
    target 1377
  ]
  edge [
    source 16
    target 1378
  ]
  edge [
    source 16
    target 1379
  ]
  edge [
    source 16
    target 1380
  ]
  edge [
    source 16
    target 1381
  ]
  edge [
    source 16
    target 1382
  ]
  edge [
    source 17
    target 1383
  ]
  edge [
    source 17
    target 1384
  ]
  edge [
    source 17
    target 1385
  ]
  edge [
    source 17
    target 1386
  ]
  edge [
    source 17
    target 1387
  ]
  edge [
    source 17
    target 1388
  ]
  edge [
    source 17
    target 1389
  ]
  edge [
    source 17
    target 1390
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 1391
  ]
  edge [
    source 17
    target 1392
  ]
  edge [
    source 17
    target 1393
  ]
  edge [
    source 17
    target 1394
  ]
  edge [
    source 17
    target 1395
  ]
  edge [
    source 17
    target 1396
  ]
  edge [
    source 17
    target 1167
  ]
  edge [
    source 17
    target 1397
  ]
  edge [
    source 17
    target 1398
  ]
  edge [
    source 17
    target 1399
  ]
  edge [
    source 17
    target 1400
  ]
  edge [
    source 17
    target 1401
  ]
  edge [
    source 17
    target 1402
  ]
  edge [
    source 17
    target 1403
  ]
  edge [
    source 17
    target 1404
  ]
  edge [
    source 17
    target 1405
  ]
  edge [
    source 17
    target 1406
  ]
  edge [
    source 17
    target 1407
  ]
  edge [
    source 17
    target 1408
  ]
  edge [
    source 17
    target 1409
  ]
  edge [
    source 17
    target 236
  ]
  edge [
    source 17
    target 1410
  ]
  edge [
    source 17
    target 1411
  ]
  edge [
    source 17
    target 1412
  ]
  edge [
    source 17
    target 1413
  ]
  edge [
    source 17
    target 1193
  ]
  edge [
    source 17
    target 1414
  ]
  edge [
    source 17
    target 1415
  ]
  edge [
    source 17
    target 1416
  ]
  edge [
    source 17
    target 1417
  ]
  edge [
    source 17
    target 1418
  ]
  edge [
    source 17
    target 1419
  ]
  edge [
    source 17
    target 1420
  ]
  edge [
    source 17
    target 42
  ]
  edge [
    source 17
    target 1421
  ]
  edge [
    source 17
    target 1422
  ]
  edge [
    source 17
    target 1423
  ]
  edge [
    source 17
    target 49
  ]
  edge [
    source 17
    target 1424
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 1425
  ]
  edge [
    source 17
    target 1426
  ]
  edge [
    source 17
    target 1114
  ]
  edge [
    source 17
    target 1427
  ]
  edge [
    source 17
    target 1428
  ]
  edge [
    source 17
    target 1429
  ]
  edge [
    source 17
    target 1430
  ]
  edge [
    source 17
    target 1431
  ]
  edge [
    source 17
    target 1432
  ]
  edge [
    source 17
    target 1231
  ]
  edge [
    source 17
    target 1219
  ]
  edge [
    source 17
    target 1216
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1236
  ]
  edge [
    source 17
    target 1232
  ]
  edge [
    source 17
    target 1093
  ]
  edge [
    source 17
    target 1237
  ]
  edge [
    source 17
    target 1238
  ]
  edge [
    source 17
    target 1239
  ]
  edge [
    source 17
    target 1217
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 1342
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 1229
  ]
  edge [
    source 17
    target 234
  ]
  edge [
    source 17
    target 1433
  ]
  edge [
    source 17
    target 1434
  ]
  edge [
    source 17
    target 1435
  ]
  edge [
    source 17
    target 1091
  ]
  edge [
    source 17
    target 1436
  ]
  edge [
    source 17
    target 1437
  ]
  edge [
    source 17
    target 1438
  ]
  edge [
    source 17
    target 1439
  ]
  edge [
    source 17
    target 1440
  ]
  edge [
    source 17
    target 1441
  ]
  edge [
    source 17
    target 1442
  ]
  edge [
    source 17
    target 1443
  ]
  edge [
    source 17
    target 1444
  ]
  edge [
    source 17
    target 1445
  ]
  edge [
    source 17
    target 1446
  ]
  edge [
    source 17
    target 1447
  ]
  edge [
    source 17
    target 1448
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 1449
  ]
  edge [
    source 17
    target 1450
  ]
  edge [
    source 17
    target 1451
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 1112
  ]
  edge [
    source 17
    target 1452
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 1453
  ]
  edge [
    source 17
    target 1454
  ]
  edge [
    source 17
    target 1084
  ]
  edge [
    source 17
    target 1085
  ]
  edge [
    source 17
    target 1086
  ]
  edge [
    source 17
    target 1087
  ]
  edge [
    source 17
    target 1088
  ]
  edge [
    source 17
    target 1089
  ]
  edge [
    source 17
    target 1090
  ]
  edge [
    source 17
    target 1092
  ]
  edge [
    source 17
    target 1094
  ]
  edge [
    source 17
    target 1095
  ]
  edge [
    source 17
    target 1096
  ]
  edge [
    source 17
    target 1097
  ]
  edge [
    source 17
    target 1098
  ]
  edge [
    source 17
    target 1099
  ]
  edge [
    source 17
    target 1100
  ]
  edge [
    source 17
    target 1101
  ]
  edge [
    source 17
    target 1102
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 1103
  ]
  edge [
    source 17
    target 1104
  ]
  edge [
    source 17
    target 1105
  ]
  edge [
    source 17
    target 1106
  ]
  edge [
    source 17
    target 1107
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 1108
  ]
  edge [
    source 17
    target 1109
  ]
  edge [
    source 17
    target 1110
  ]
  edge [
    source 17
    target 1111
  ]
  edge [
    source 17
    target 1113
  ]
  edge [
    source 17
    target 1115
  ]
  edge [
    source 17
    target 1116
  ]
  edge [
    source 17
    target 1117
  ]
  edge [
    source 17
    target 1118
  ]
  edge [
    source 17
    target 1119
  ]
  edge [
    source 17
    target 1455
  ]
  edge [
    source 17
    target 210
  ]
  edge [
    source 17
    target 37
  ]
  edge [
    source 17
    target 1349
  ]
  edge [
    source 17
    target 1456
  ]
  edge [
    source 17
    target 1457
  ]
  edge [
    source 17
    target 1458
  ]
  edge [
    source 17
    target 1459
  ]
  edge [
    source 17
    target 1460
  ]
  edge [
    source 17
    target 1461
  ]
  edge [
    source 17
    target 1462
  ]
  edge [
    source 17
    target 1266
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 1463
  ]
  edge [
    source 17
    target 1464
  ]
  edge [
    source 17
    target 1465
  ]
  edge [
    source 17
    target 1466
  ]
  edge [
    source 17
    target 1467
  ]
  edge [
    source 17
    target 1468
  ]
  edge [
    source 17
    target 1469
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 1470
  ]
  edge [
    source 17
    target 1471
  ]
  edge [
    source 17
    target 1220
  ]
  edge [
    source 17
    target 1472
  ]
  edge [
    source 17
    target 1473
  ]
  edge [
    source 17
    target 1474
  ]
  edge [
    source 17
    target 1475
  ]
  edge [
    source 17
    target 1476
  ]
  edge [
    source 17
    target 1477
  ]
  edge [
    source 17
    target 1478
  ]
  edge [
    source 17
    target 1479
  ]
  edge [
    source 17
    target 1480
  ]
  edge [
    source 17
    target 1481
  ]
  edge [
    source 17
    target 1482
  ]
  edge [
    source 17
    target 1483
  ]
  edge [
    source 17
    target 1484
  ]
  edge [
    source 17
    target 1485
  ]
  edge [
    source 17
    target 1486
  ]
  edge [
    source 17
    target 1487
  ]
  edge [
    source 17
    target 1488
  ]
  edge [
    source 17
    target 1489
  ]
  edge [
    source 17
    target 1490
  ]
  edge [
    source 17
    target 1491
  ]
  edge [
    source 17
    target 1492
  ]
  edge [
    source 17
    target 1493
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 1494
  ]
  edge [
    source 17
    target 1495
  ]
  edge [
    source 17
    target 1496
  ]
  edge [
    source 17
    target 1497
  ]
  edge [
    source 17
    target 1498
  ]
  edge [
    source 17
    target 1499
  ]
  edge [
    source 17
    target 1500
  ]
  edge [
    source 17
    target 1501
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 1221
  ]
  edge [
    source 17
    target 1502
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 1503
  ]
  edge [
    source 17
    target 1504
  ]
  edge [
    source 17
    target 1505
  ]
  edge [
    source 17
    target 1506
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 1183
  ]
  edge [
    source 17
    target 1507
  ]
  edge [
    source 17
    target 1262
  ]
  edge [
    source 17
    target 1508
  ]
  edge [
    source 17
    target 1509
  ]
  edge [
    source 17
    target 1510
  ]
  edge [
    source 17
    target 1511
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 1512
  ]
  edge [
    source 17
    target 1513
  ]
  edge [
    source 17
    target 1514
  ]
  edge [
    source 17
    target 1158
  ]
  edge [
    source 17
    target 1515
  ]
  edge [
    source 17
    target 1516
  ]
  edge [
    source 17
    target 1517
  ]
  edge [
    source 17
    target 1518
  ]
  edge [
    source 17
    target 1519
  ]
  edge [
    source 17
    target 1520
  ]
  edge [
    source 17
    target 1521
  ]
  edge [
    source 17
    target 1222
  ]
  edge [
    source 17
    target 1522
  ]
  edge [
    source 17
    target 1523
  ]
  edge [
    source 17
    target 95
  ]
  edge [
    source 17
    target 1524
  ]
  edge [
    source 17
    target 1525
  ]
  edge [
    source 17
    target 1526
  ]
  edge [
    source 17
    target 1527
  ]
  edge [
    source 17
    target 1528
  ]
  edge [
    source 17
    target 1529
  ]
  edge [
    source 17
    target 1530
  ]
  edge [
    source 17
    target 1531
  ]
  edge [
    source 17
    target 62
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 714
  ]
  edge [
    source 18
    target 354
  ]
  edge [
    source 18
    target 1532
  ]
  edge [
    source 18
    target 1533
  ]
  edge [
    source 18
    target 1534
  ]
  edge [
    source 18
    target 1535
  ]
  edge [
    source 18
    target 1536
  ]
  edge [
    source 18
    target 1537
  ]
  edge [
    source 18
    target 1538
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 1539
  ]
  edge [
    source 18
    target 1540
  ]
  edge [
    source 18
    target 1541
  ]
  edge [
    source 18
    target 1542
  ]
  edge [
    source 18
    target 1543
  ]
  edge [
    source 18
    target 1544
  ]
  edge [
    source 18
    target 1545
  ]
  edge [
    source 18
    target 1546
  ]
  edge [
    source 18
    target 1547
  ]
  edge [
    source 18
    target 1548
  ]
  edge [
    source 18
    target 1549
  ]
  edge [
    source 18
    target 1550
  ]
  edge [
    source 18
    target 1551
  ]
  edge [
    source 18
    target 1552
  ]
  edge [
    source 18
    target 1553
  ]
  edge [
    source 18
    target 1356
  ]
  edge [
    source 18
    target 1554
  ]
  edge [
    source 18
    target 1555
  ]
  edge [
    source 18
    target 49
  ]
  edge [
    source 18
    target 1556
  ]
  edge [
    source 18
    target 1557
  ]
  edge [
    source 18
    target 1558
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 1559
  ]
  edge [
    source 18
    target 1560
  ]
  edge [
    source 18
    target 1561
  ]
  edge [
    source 18
    target 1562
  ]
  edge [
    source 18
    target 1563
  ]
  edge [
    source 18
    target 1564
  ]
  edge [
    source 18
    target 1565
  ]
  edge [
    source 18
    target 1566
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 1567
  ]
  edge [
    source 18
    target 1568
  ]
  edge [
    source 18
    target 1569
  ]
  edge [
    source 18
    target 1570
  ]
  edge [
    source 18
    target 1571
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 1572
  ]
  edge [
    source 18
    target 488
  ]
  edge [
    source 18
    target 711
  ]
  edge [
    source 18
    target 1573
  ]
  edge [
    source 18
    target 1574
  ]
  edge [
    source 18
    target 1575
  ]
  edge [
    source 18
    target 1576
  ]
  edge [
    source 18
    target 1577
  ]
  edge [
    source 18
    target 1578
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 1579
  ]
  edge [
    source 18
    target 1580
  ]
  edge [
    source 18
    target 1581
  ]
  edge [
    source 18
    target 1582
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 1583
  ]
  edge [
    source 18
    target 1584
  ]
  edge [
    source 18
    target 1585
  ]
  edge [
    source 18
    target 1586
  ]
  edge [
    source 18
    target 1587
  ]
  edge [
    source 18
    target 385
  ]
  edge [
    source 18
    target 1588
  ]
  edge [
    source 18
    target 1589
  ]
  edge [
    source 18
    target 1590
  ]
  edge [
    source 18
    target 1591
  ]
  edge [
    source 18
    target 363
  ]
  edge [
    source 19
    target 1592
  ]
  edge [
    source 19
    target 134
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 1593
  ]
  edge [
    source 19
    target 1594
  ]
  edge [
    source 19
    target 552
  ]
  edge [
    source 19
    target 113
  ]
  edge [
    source 19
    target 1595
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 85
  ]
  edge [
    source 19
    target 1596
  ]
  edge [
    source 19
    target 945
  ]
  edge [
    source 19
    target 946
  ]
  edge [
    source 19
    target 488
  ]
  edge [
    source 19
    target 947
  ]
  edge [
    source 19
    target 948
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 949
  ]
  edge [
    source 19
    target 950
  ]
  edge [
    source 19
    target 951
  ]
  edge [
    source 19
    target 952
  ]
  edge [
    source 19
    target 953
  ]
  edge [
    source 19
    target 954
  ]
  edge [
    source 19
    target 955
  ]
  edge [
    source 19
    target 956
  ]
  edge [
    source 19
    target 957
  ]
  edge [
    source 19
    target 958
  ]
  edge [
    source 19
    target 959
  ]
  edge [
    source 19
    target 960
  ]
  edge [
    source 19
    target 961
  ]
  edge [
    source 19
    target 962
  ]
  edge [
    source 19
    target 963
  ]
  edge [
    source 19
    target 964
  ]
  edge [
    source 19
    target 965
  ]
  edge [
    source 19
    target 966
  ]
  edge [
    source 19
    target 967
  ]
  edge [
    source 19
    target 1432
  ]
  edge [
    source 19
    target 784
  ]
  edge [
    source 19
    target 1597
  ]
  edge [
    source 19
    target 1598
  ]
  edge [
    source 19
    target 1599
  ]
  edge [
    source 19
    target 1600
  ]
  edge [
    source 19
    target 786
  ]
  edge [
    source 19
    target 1601
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 19
    target 1602
  ]
  edge [
    source 19
    target 1603
  ]
  edge [
    source 19
    target 1604
  ]
  edge [
    source 19
    target 1605
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 1606
  ]
  edge [
    source 19
    target 528
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 542
  ]
  edge [
    source 19
    target 1607
  ]
  edge [
    source 19
    target 1608
  ]
  edge [
    source 19
    target 792
  ]
  edge [
    source 19
    target 1609
  ]
  edge [
    source 19
    target 37
  ]
  edge [
    source 19
    target 1610
  ]
  edge [
    source 19
    target 1611
  ]
  edge [
    source 19
    target 1110
  ]
  edge [
    source 19
    target 49
  ]
  edge [
    source 19
    target 1504
  ]
  edge [
    source 19
    target 1612
  ]
  edge [
    source 19
    target 1613
  ]
  edge [
    source 19
    target 1614
  ]
  edge [
    source 19
    target 1615
  ]
  edge [
    source 19
    target 1616
  ]
]
