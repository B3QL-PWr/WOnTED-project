graph [
  node [
    id 0
    label "uber"
    origin "text"
  ]
  node [
    id 1
    label "twierdzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "moja"
    origin "text"
  ]
  node [
    id 3
    label "partnerka"
    origin "text"
  ]
  node [
    id 4
    label "zarzyga&#263;"
    origin "text"
  ]
  node [
    id 5
    label "auto"
    origin "text"
  ]
  node [
    id 6
    label "kierowca"
    origin "text"
  ]
  node [
    id 7
    label "oznajmia&#263;"
  ]
  node [
    id 8
    label "zapewnia&#263;"
  ]
  node [
    id 9
    label "attest"
  ]
  node [
    id 10
    label "komunikowa&#263;"
  ]
  node [
    id 11
    label "argue"
  ]
  node [
    id 12
    label "communicate"
  ]
  node [
    id 13
    label "powodowa&#263;"
  ]
  node [
    id 14
    label "inform"
  ]
  node [
    id 15
    label "informowa&#263;"
  ]
  node [
    id 16
    label "dostarcza&#263;"
  ]
  node [
    id 17
    label "deliver"
  ]
  node [
    id 18
    label "utrzymywa&#263;"
  ]
  node [
    id 19
    label "cz&#322;owiek"
  ]
  node [
    id 20
    label "aktorka"
  ]
  node [
    id 21
    label "kobieta"
  ]
  node [
    id 22
    label "partner"
  ]
  node [
    id 23
    label "kobita"
  ]
  node [
    id 24
    label "pracownik"
  ]
  node [
    id 25
    label "wykonawczyni"
  ]
  node [
    id 26
    label "przedsi&#281;biorca"
  ]
  node [
    id 27
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 28
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 29
    label "kolaborator"
  ]
  node [
    id 30
    label "prowadzi&#263;"
  ]
  node [
    id 31
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 32
    label "sp&#243;lnik"
  ]
  node [
    id 33
    label "aktor"
  ]
  node [
    id 34
    label "uczestniczenie"
  ]
  node [
    id 35
    label "doros&#322;y"
  ]
  node [
    id 36
    label "&#380;ona"
  ]
  node [
    id 37
    label "samica"
  ]
  node [
    id 38
    label "uleganie"
  ]
  node [
    id 39
    label "ulec"
  ]
  node [
    id 40
    label "m&#281;&#380;yna"
  ]
  node [
    id 41
    label "ulegni&#281;cie"
  ]
  node [
    id 42
    label "pa&#324;stwo"
  ]
  node [
    id 43
    label "&#322;ono"
  ]
  node [
    id 44
    label "menopauza"
  ]
  node [
    id 45
    label "przekwitanie"
  ]
  node [
    id 46
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 47
    label "babka"
  ]
  node [
    id 48
    label "ulega&#263;"
  ]
  node [
    id 49
    label "ludzko&#347;&#263;"
  ]
  node [
    id 50
    label "asymilowanie"
  ]
  node [
    id 51
    label "wapniak"
  ]
  node [
    id 52
    label "asymilowa&#263;"
  ]
  node [
    id 53
    label "os&#322;abia&#263;"
  ]
  node [
    id 54
    label "posta&#263;"
  ]
  node [
    id 55
    label "hominid"
  ]
  node [
    id 56
    label "podw&#322;adny"
  ]
  node [
    id 57
    label "os&#322;abianie"
  ]
  node [
    id 58
    label "g&#322;owa"
  ]
  node [
    id 59
    label "figura"
  ]
  node [
    id 60
    label "portrecista"
  ]
  node [
    id 61
    label "dwun&#243;g"
  ]
  node [
    id 62
    label "profanum"
  ]
  node [
    id 63
    label "mikrokosmos"
  ]
  node [
    id 64
    label "nasada"
  ]
  node [
    id 65
    label "duch"
  ]
  node [
    id 66
    label "antropochoria"
  ]
  node [
    id 67
    label "osoba"
  ]
  node [
    id 68
    label "wz&#243;r"
  ]
  node [
    id 69
    label "senior"
  ]
  node [
    id 70
    label "oddzia&#322;ywanie"
  ]
  node [
    id 71
    label "Adam"
  ]
  node [
    id 72
    label "homo_sapiens"
  ]
  node [
    id 73
    label "polifag"
  ]
  node [
    id 74
    label "zabrudzi&#263;"
  ]
  node [
    id 75
    label "wydali&#263;"
  ]
  node [
    id 76
    label "zbruka&#263;"
  ]
  node [
    id 77
    label "zaszkodzi&#263;"
  ]
  node [
    id 78
    label "zeszmaci&#263;"
  ]
  node [
    id 79
    label "ujeba&#263;"
  ]
  node [
    id 80
    label "skali&#263;"
  ]
  node [
    id 81
    label "uwala&#263;"
  ]
  node [
    id 82
    label "take_down"
  ]
  node [
    id 83
    label "smear"
  ]
  node [
    id 84
    label "zanieczy&#347;ci&#263;"
  ]
  node [
    id 85
    label "upierdoli&#263;"
  ]
  node [
    id 86
    label "zrobi&#263;"
  ]
  node [
    id 87
    label "usun&#261;&#263;"
  ]
  node [
    id 88
    label "sack"
  ]
  node [
    id 89
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 90
    label "restore"
  ]
  node [
    id 91
    label "pojazd_drogowy"
  ]
  node [
    id 92
    label "spryskiwacz"
  ]
  node [
    id 93
    label "most"
  ]
  node [
    id 94
    label "baga&#380;nik"
  ]
  node [
    id 95
    label "silnik"
  ]
  node [
    id 96
    label "dachowanie"
  ]
  node [
    id 97
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 98
    label "pompa_wodna"
  ]
  node [
    id 99
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 100
    label "poduszka_powietrzna"
  ]
  node [
    id 101
    label "tempomat"
  ]
  node [
    id 102
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 103
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 104
    label "deska_rozdzielcza"
  ]
  node [
    id 105
    label "immobilizer"
  ]
  node [
    id 106
    label "t&#322;umik"
  ]
  node [
    id 107
    label "ABS"
  ]
  node [
    id 108
    label "kierownica"
  ]
  node [
    id 109
    label "bak"
  ]
  node [
    id 110
    label "dwu&#347;lad"
  ]
  node [
    id 111
    label "poci&#261;g_drogowy"
  ]
  node [
    id 112
    label "wycieraczka"
  ]
  node [
    id 113
    label "pojazd"
  ]
  node [
    id 114
    label "rekwizyt_muzyczny"
  ]
  node [
    id 115
    label "attenuator"
  ]
  node [
    id 116
    label "samoch&#243;d"
  ]
  node [
    id 117
    label "regulator"
  ]
  node [
    id 118
    label "bro&#324;_palna"
  ]
  node [
    id 119
    label "urz&#261;dzenie"
  ]
  node [
    id 120
    label "mata"
  ]
  node [
    id 121
    label "g&#243;ra_mi&#281;sa"
  ]
  node [
    id 122
    label "cycek"
  ]
  node [
    id 123
    label "biust"
  ]
  node [
    id 124
    label "hamowanie"
  ]
  node [
    id 125
    label "uk&#322;ad"
  ]
  node [
    id 126
    label "acrylonitrile-butadiene-styrene"
  ]
  node [
    id 127
    label "sze&#347;ciopak"
  ]
  node [
    id 128
    label "kulturysta"
  ]
  node [
    id 129
    label "mu&#322;y"
  ]
  node [
    id 130
    label "motor"
  ]
  node [
    id 131
    label "rower"
  ]
  node [
    id 132
    label "stolik_topograficzny"
  ]
  node [
    id 133
    label "przyrz&#261;d"
  ]
  node [
    id 134
    label "kontroler_gier"
  ]
  node [
    id 135
    label "biblioteka"
  ]
  node [
    id 136
    label "radiator"
  ]
  node [
    id 137
    label "wyci&#261;garka"
  ]
  node [
    id 138
    label "gondola_silnikowa"
  ]
  node [
    id 139
    label "aerosanie"
  ]
  node [
    id 140
    label "podgrzewacz"
  ]
  node [
    id 141
    label "motogodzina"
  ]
  node [
    id 142
    label "motoszybowiec"
  ]
  node [
    id 143
    label "program"
  ]
  node [
    id 144
    label "gniazdo_zaworowe"
  ]
  node [
    id 145
    label "mechanizm"
  ]
  node [
    id 146
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 147
    label "dociera&#263;"
  ]
  node [
    id 148
    label "dotarcie"
  ]
  node [
    id 149
    label "nap&#281;d"
  ]
  node [
    id 150
    label "motor&#243;wka"
  ]
  node [
    id 151
    label "rz&#281;zi&#263;"
  ]
  node [
    id 152
    label "perpetuum_mobile"
  ]
  node [
    id 153
    label "docieranie"
  ]
  node [
    id 154
    label "bombowiec"
  ]
  node [
    id 155
    label "dotrze&#263;"
  ]
  node [
    id 156
    label "rz&#281;&#380;enie"
  ]
  node [
    id 157
    label "ochrona"
  ]
  node [
    id 158
    label "rzuci&#263;"
  ]
  node [
    id 159
    label "prz&#281;s&#322;o"
  ]
  node [
    id 160
    label "m&#243;zg"
  ]
  node [
    id 161
    label "trasa"
  ]
  node [
    id 162
    label "jarzmo_mostowe"
  ]
  node [
    id 163
    label "pylon"
  ]
  node [
    id 164
    label "zam&#243;zgowie"
  ]
  node [
    id 165
    label "obiekt_mostowy"
  ]
  node [
    id 166
    label "szczelina_dylatacyjna"
  ]
  node [
    id 167
    label "rzucenie"
  ]
  node [
    id 168
    label "bridge"
  ]
  node [
    id 169
    label "rzuca&#263;"
  ]
  node [
    id 170
    label "suwnica"
  ]
  node [
    id 171
    label "porozumienie"
  ]
  node [
    id 172
    label "rzucanie"
  ]
  node [
    id 173
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 174
    label "sprinkler"
  ]
  node [
    id 175
    label "bakenbardy"
  ]
  node [
    id 176
    label "tank"
  ]
  node [
    id 177
    label "fordek"
  ]
  node [
    id 178
    label "zbiornik"
  ]
  node [
    id 179
    label "beard"
  ]
  node [
    id 180
    label "zarost"
  ]
  node [
    id 181
    label "przewracanie_si&#281;"
  ]
  node [
    id 182
    label "przewr&#243;cenie_si&#281;"
  ]
  node [
    id 183
    label "jechanie"
  ]
  node [
    id 184
    label "transportowiec"
  ]
  node [
    id 185
    label "statek_handlowy"
  ]
  node [
    id 186
    label "okr&#281;t_nawodny"
  ]
  node [
    id 187
    label "bran&#380;owiec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
]
