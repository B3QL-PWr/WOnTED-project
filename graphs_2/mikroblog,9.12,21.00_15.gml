graph [
  node [
    id 0
    label "dosta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "odpowied&#378;"
    origin "text"
  ]
  node [
    id 2
    label "prokuratura"
    origin "text"
  ]
  node [
    id 3
    label "odno&#347;nie"
    origin "text"
  ]
  node [
    id 4
    label "wpis"
    origin "text"
  ]
  node [
    id 5
    label "wykop"
    origin "text"
  ]
  node [
    id 6
    label "nawo&#322;uj&#261;cy"
    origin "text"
  ]
  node [
    id 7
    label "pod&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 8
    label "bomba"
    origin "text"
  ]
  node [
    id 9
    label "pod"
    origin "text"
  ]
  node [
    id 10
    label "m&#243;wnica"
    origin "text"
  ]
  node [
    id 11
    label "prezydent"
    origin "text"
  ]
  node [
    id 12
    label "listopad"
    origin "text"
  ]
  node [
    id 13
    label "wi&#281;c"
    origin "text"
  ]
  node [
    id 14
    label "zainteresowana"
    origin "text"
  ]
  node [
    id 15
    label "poprzedni"
    origin "text"
  ]
  node [
    id 16
    label "react"
  ]
  node [
    id 17
    label "replica"
  ]
  node [
    id 18
    label "rozmowa"
  ]
  node [
    id 19
    label "wyj&#347;cie"
  ]
  node [
    id 20
    label "respondent"
  ]
  node [
    id 21
    label "dokument"
  ]
  node [
    id 22
    label "reakcja"
  ]
  node [
    id 23
    label "zachowanie"
  ]
  node [
    id 24
    label "reaction"
  ]
  node [
    id 25
    label "organizm"
  ]
  node [
    id 26
    label "response"
  ]
  node [
    id 27
    label "rezultat"
  ]
  node [
    id 28
    label "zapis"
  ]
  node [
    id 29
    label "&#347;wiadectwo"
  ]
  node [
    id 30
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 31
    label "wytw&#243;r"
  ]
  node [
    id 32
    label "parafa"
  ]
  node [
    id 33
    label "plik"
  ]
  node [
    id 34
    label "raport&#243;wka"
  ]
  node [
    id 35
    label "utw&#243;r"
  ]
  node [
    id 36
    label "record"
  ]
  node [
    id 37
    label "fascyku&#322;"
  ]
  node [
    id 38
    label "dokumentacja"
  ]
  node [
    id 39
    label "registratura"
  ]
  node [
    id 40
    label "artyku&#322;"
  ]
  node [
    id 41
    label "writing"
  ]
  node [
    id 42
    label "sygnatariusz"
  ]
  node [
    id 43
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 44
    label "okazanie_si&#281;"
  ]
  node [
    id 45
    label "ograniczenie"
  ]
  node [
    id 46
    label "uzyskanie"
  ]
  node [
    id 47
    label "ruszenie"
  ]
  node [
    id 48
    label "podzianie_si&#281;"
  ]
  node [
    id 49
    label "spotkanie"
  ]
  node [
    id 50
    label "powychodzenie"
  ]
  node [
    id 51
    label "opuszczenie"
  ]
  node [
    id 52
    label "postrze&#380;enie"
  ]
  node [
    id 53
    label "transgression"
  ]
  node [
    id 54
    label "wyczerpanie_si&#281;"
  ]
  node [
    id 55
    label "wychodzenie"
  ]
  node [
    id 56
    label "uko&#324;czenie"
  ]
  node [
    id 57
    label "miejsce"
  ]
  node [
    id 58
    label "powiedzenie_si&#281;"
  ]
  node [
    id 59
    label "policzenie"
  ]
  node [
    id 60
    label "podziewanie_si&#281;"
  ]
  node [
    id 61
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 62
    label "exit"
  ]
  node [
    id 63
    label "vent"
  ]
  node [
    id 64
    label "uwolnienie_si&#281;"
  ]
  node [
    id 65
    label "deviation"
  ]
  node [
    id 66
    label "release"
  ]
  node [
    id 67
    label "wych&#243;d"
  ]
  node [
    id 68
    label "withdrawal"
  ]
  node [
    id 69
    label "wypadni&#281;cie"
  ]
  node [
    id 70
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 71
    label "kres"
  ]
  node [
    id 72
    label "odch&#243;d"
  ]
  node [
    id 73
    label "przebywanie"
  ]
  node [
    id 74
    label "przedstawienie"
  ]
  node [
    id 75
    label "wywiedzenie_si&#281;"
  ]
  node [
    id 76
    label "zagranie"
  ]
  node [
    id 77
    label "zako&#324;czenie"
  ]
  node [
    id 78
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 79
    label "emergence"
  ]
  node [
    id 80
    label "cisza"
  ]
  node [
    id 81
    label "rozhowor"
  ]
  node [
    id 82
    label "discussion"
  ]
  node [
    id 83
    label "czynno&#347;&#263;"
  ]
  node [
    id 84
    label "badany"
  ]
  node [
    id 85
    label "siedziba"
  ]
  node [
    id 86
    label "urz&#261;d"
  ]
  node [
    id 87
    label "organ"
  ]
  node [
    id 88
    label "&#321;ubianka"
  ]
  node [
    id 89
    label "miejsce_pracy"
  ]
  node [
    id 90
    label "dzia&#322;_personalny"
  ]
  node [
    id 91
    label "Kreml"
  ]
  node [
    id 92
    label "Bia&#322;y_Dom"
  ]
  node [
    id 93
    label "budynek"
  ]
  node [
    id 94
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 95
    label "sadowisko"
  ]
  node [
    id 96
    label "stanowisko"
  ]
  node [
    id 97
    label "position"
  ]
  node [
    id 98
    label "instytucja"
  ]
  node [
    id 99
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 100
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 101
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 102
    label "mianowaniec"
  ]
  node [
    id 103
    label "dzia&#322;"
  ]
  node [
    id 104
    label "okienko"
  ]
  node [
    id 105
    label "w&#322;adza"
  ]
  node [
    id 106
    label "tkanka"
  ]
  node [
    id 107
    label "jednostka_organizacyjna"
  ]
  node [
    id 108
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 109
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 110
    label "tw&#243;r"
  ]
  node [
    id 111
    label "organogeneza"
  ]
  node [
    id 112
    label "zesp&#243;&#322;"
  ]
  node [
    id 113
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 114
    label "struktura_anatomiczna"
  ]
  node [
    id 115
    label "uk&#322;ad"
  ]
  node [
    id 116
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 117
    label "dekortykacja"
  ]
  node [
    id 118
    label "Izba_Konsyliarska"
  ]
  node [
    id 119
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 120
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 121
    label "stomia"
  ]
  node [
    id 122
    label "budowa"
  ]
  node [
    id 123
    label "okolica"
  ]
  node [
    id 124
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 125
    label "Komitet_Region&#243;w"
  ]
  node [
    id 126
    label "inscription"
  ]
  node [
    id 127
    label "op&#322;ata"
  ]
  node [
    id 128
    label "akt"
  ]
  node [
    id 129
    label "tekst"
  ]
  node [
    id 130
    label "entrance"
  ]
  node [
    id 131
    label "ekscerpcja"
  ]
  node [
    id 132
    label "j&#281;zykowo"
  ]
  node [
    id 133
    label "wypowied&#378;"
  ]
  node [
    id 134
    label "redakcja"
  ]
  node [
    id 135
    label "pomini&#281;cie"
  ]
  node [
    id 136
    label "dzie&#322;o"
  ]
  node [
    id 137
    label "preparacja"
  ]
  node [
    id 138
    label "odmianka"
  ]
  node [
    id 139
    label "opu&#347;ci&#263;"
  ]
  node [
    id 140
    label "koniektura"
  ]
  node [
    id 141
    label "pisa&#263;"
  ]
  node [
    id 142
    label "obelga"
  ]
  node [
    id 143
    label "kwota"
  ]
  node [
    id 144
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 145
    label "activity"
  ]
  node [
    id 146
    label "bezproblemowy"
  ]
  node [
    id 147
    label "wydarzenie"
  ]
  node [
    id 148
    label "podnieci&#263;"
  ]
  node [
    id 149
    label "scena"
  ]
  node [
    id 150
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 151
    label "numer"
  ]
  node [
    id 152
    label "po&#380;ycie"
  ]
  node [
    id 153
    label "poj&#281;cie"
  ]
  node [
    id 154
    label "podniecenie"
  ]
  node [
    id 155
    label "nago&#347;&#263;"
  ]
  node [
    id 156
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 157
    label "seks"
  ]
  node [
    id 158
    label "podniecanie"
  ]
  node [
    id 159
    label "imisja"
  ]
  node [
    id 160
    label "zwyczaj"
  ]
  node [
    id 161
    label "rozmna&#380;anie"
  ]
  node [
    id 162
    label "ruch_frykcyjny"
  ]
  node [
    id 163
    label "ontologia"
  ]
  node [
    id 164
    label "na_pieska"
  ]
  node [
    id 165
    label "pozycja_misjonarska"
  ]
  node [
    id 166
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 167
    label "fragment"
  ]
  node [
    id 168
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 169
    label "z&#322;&#261;czenie"
  ]
  node [
    id 170
    label "gra_wst&#281;pna"
  ]
  node [
    id 171
    label "erotyka"
  ]
  node [
    id 172
    label "urzeczywistnienie"
  ]
  node [
    id 173
    label "baraszki"
  ]
  node [
    id 174
    label "certificate"
  ]
  node [
    id 175
    label "po&#380;&#261;danie"
  ]
  node [
    id 176
    label "wzw&#243;d"
  ]
  node [
    id 177
    label "funkcja"
  ]
  node [
    id 178
    label "act"
  ]
  node [
    id 179
    label "arystotelizm"
  ]
  node [
    id 180
    label "podnieca&#263;"
  ]
  node [
    id 181
    label "zrzutowy"
  ]
  node [
    id 182
    label "odk&#322;ad"
  ]
  node [
    id 183
    label "chody"
  ]
  node [
    id 184
    label "szaniec"
  ]
  node [
    id 185
    label "wyrobisko"
  ]
  node [
    id 186
    label "kopniak"
  ]
  node [
    id 187
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 188
    label "odwa&#322;"
  ]
  node [
    id 189
    label "grodzisko"
  ]
  node [
    id 190
    label "cios"
  ]
  node [
    id 191
    label "kick"
  ]
  node [
    id 192
    label "kopni&#281;cie"
  ]
  node [
    id 193
    label "&#347;rodkowiec"
  ]
  node [
    id 194
    label "podsadzka"
  ]
  node [
    id 195
    label "obudowa"
  ]
  node [
    id 196
    label "sp&#261;g"
  ]
  node [
    id 197
    label "strop"
  ]
  node [
    id 198
    label "rabowarka"
  ]
  node [
    id 199
    label "opinka"
  ]
  node [
    id 200
    label "stojak_cierny"
  ]
  node [
    id 201
    label "kopalnia"
  ]
  node [
    id 202
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 203
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 204
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 205
    label "immersion"
  ]
  node [
    id 206
    label "umieszczenie"
  ]
  node [
    id 207
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 208
    label "horodyszcze"
  ]
  node [
    id 209
    label "Wyszogr&#243;d"
  ]
  node [
    id 210
    label "las"
  ]
  node [
    id 211
    label "nora"
  ]
  node [
    id 212
    label "pies_my&#347;liwski"
  ]
  node [
    id 213
    label "trasa"
  ]
  node [
    id 214
    label "doj&#347;cie"
  ]
  node [
    id 215
    label "usypisko"
  ]
  node [
    id 216
    label "r&#243;w"
  ]
  node [
    id 217
    label "wa&#322;"
  ]
  node [
    id 218
    label "redoubt"
  ]
  node [
    id 219
    label "fortyfikacja"
  ]
  node [
    id 220
    label "mechanika"
  ]
  node [
    id 221
    label "struktura"
  ]
  node [
    id 222
    label "figura"
  ]
  node [
    id 223
    label "cecha"
  ]
  node [
    id 224
    label "kreacja"
  ]
  node [
    id 225
    label "zwierz&#281;"
  ]
  node [
    id 226
    label "posesja"
  ]
  node [
    id 227
    label "konstrukcja"
  ]
  node [
    id 228
    label "wjazd"
  ]
  node [
    id 229
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 230
    label "praca"
  ]
  node [
    id 231
    label "constitution"
  ]
  node [
    id 232
    label "gleba"
  ]
  node [
    id 233
    label "p&#281;d"
  ]
  node [
    id 234
    label "zbi&#243;r"
  ]
  node [
    id 235
    label "ablegier"
  ]
  node [
    id 236
    label "rozmna&#380;anie_bezp&#322;ciowe"
  ]
  node [
    id 237
    label "layer"
  ]
  node [
    id 238
    label "r&#243;j"
  ]
  node [
    id 239
    label "mrowisko"
  ]
  node [
    id 240
    label "set"
  ]
  node [
    id 241
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 242
    label "podwin&#261;&#263;"
  ]
  node [
    id 243
    label "doda&#263;"
  ]
  node [
    id 244
    label "zebra&#263;"
  ]
  node [
    id 245
    label "plant"
  ]
  node [
    id 246
    label "jell"
  ]
  node [
    id 247
    label "umie&#347;ci&#263;"
  ]
  node [
    id 248
    label "put"
  ]
  node [
    id 249
    label "uplasowa&#263;"
  ]
  node [
    id 250
    label "wpierniczy&#263;"
  ]
  node [
    id 251
    label "okre&#347;li&#263;"
  ]
  node [
    id 252
    label "zrobi&#263;"
  ]
  node [
    id 253
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 254
    label "zmieni&#263;"
  ]
  node [
    id 255
    label "umieszcza&#263;"
  ]
  node [
    id 256
    label "zgromadzi&#263;"
  ]
  node [
    id 257
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 258
    label "raise"
  ]
  node [
    id 259
    label "pozyska&#263;"
  ]
  node [
    id 260
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 261
    label "nat&#281;&#380;y&#263;"
  ]
  node [
    id 262
    label "sprz&#261;tn&#261;&#263;"
  ]
  node [
    id 263
    label "wzi&#261;&#263;"
  ]
  node [
    id 264
    label "przej&#261;&#263;"
  ]
  node [
    id 265
    label "spowodowa&#263;"
  ]
  node [
    id 266
    label "oszcz&#281;dzi&#263;"
  ]
  node [
    id 267
    label "plane"
  ]
  node [
    id 268
    label "przyci&#261;gn&#261;&#263;"
  ]
  node [
    id 269
    label "wezbra&#263;"
  ]
  node [
    id 270
    label "congregate"
  ]
  node [
    id 271
    label "dosta&#263;"
  ]
  node [
    id 272
    label "skupi&#263;"
  ]
  node [
    id 273
    label "skuli&#263;"
  ]
  node [
    id 274
    label "fold"
  ]
  node [
    id 275
    label "skr&#243;ci&#263;"
  ]
  node [
    id 276
    label "nada&#263;"
  ]
  node [
    id 277
    label "policzy&#263;"
  ]
  node [
    id 278
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 279
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 280
    label "complete"
  ]
  node [
    id 281
    label "oblec"
  ]
  node [
    id 282
    label "ubra&#263;"
  ]
  node [
    id 283
    label "przekaza&#263;"
  ]
  node [
    id 284
    label "oblec_si&#281;"
  ]
  node [
    id 285
    label "str&#243;j"
  ]
  node [
    id 286
    label "insert"
  ]
  node [
    id 287
    label "wpoi&#263;"
  ]
  node [
    id 288
    label "pour"
  ]
  node [
    id 289
    label "przyodzia&#263;"
  ]
  node [
    id 290
    label "natchn&#261;&#263;"
  ]
  node [
    id 291
    label "load"
  ]
  node [
    id 292
    label "deposit"
  ]
  node [
    id 293
    label "wzbudzi&#263;"
  ]
  node [
    id 294
    label "gem"
  ]
  node [
    id 295
    label "kompozycja"
  ]
  node [
    id 296
    label "runda"
  ]
  node [
    id 297
    label "muzyka"
  ]
  node [
    id 298
    label "zestaw"
  ]
  node [
    id 299
    label "czerep"
  ]
  node [
    id 300
    label "nab&#243;j"
  ]
  node [
    id 301
    label "pocisk"
  ]
  node [
    id 302
    label "novum"
  ]
  node [
    id 303
    label "niedostateczny"
  ]
  node [
    id 304
    label "bombowiec"
  ]
  node [
    id 305
    label "zapalnik"
  ]
  node [
    id 306
    label "strza&#322;"
  ]
  node [
    id 307
    label "materia&#322;_piroklastyczny"
  ]
  node [
    id 308
    label "pa&#322;a"
  ]
  node [
    id 309
    label "jedynka"
  ]
  node [
    id 310
    label "niezadowalaj&#261;cy"
  ]
  node [
    id 311
    label "dw&#243;jka"
  ]
  node [
    id 312
    label "trafny"
  ]
  node [
    id 313
    label "shot"
  ]
  node [
    id 314
    label "przykro&#347;&#263;"
  ]
  node [
    id 315
    label "huk"
  ]
  node [
    id 316
    label "bum-bum"
  ]
  node [
    id 317
    label "pi&#322;ka"
  ]
  node [
    id 318
    label "uderzenie"
  ]
  node [
    id 319
    label "eksplozja"
  ]
  node [
    id 320
    label "wyrzut"
  ]
  node [
    id 321
    label "usi&#322;owanie"
  ]
  node [
    id 322
    label "przypadek"
  ]
  node [
    id 323
    label "shooting"
  ]
  node [
    id 324
    label "odgadywanie"
  ]
  node [
    id 325
    label "proch_bezdymny"
  ]
  node [
    id 326
    label "amunicja"
  ]
  node [
    id 327
    label "&#322;uska"
  ]
  node [
    id 328
    label "o&#322;&#243;w"
  ]
  node [
    id 329
    label "kartuza"
  ]
  node [
    id 330
    label "prochownia"
  ]
  node [
    id 331
    label "musket_ball"
  ]
  node [
    id 332
    label "charge"
  ]
  node [
    id 333
    label "komora_nabojowa"
  ]
  node [
    id 334
    label "ta&#347;ma"
  ]
  node [
    id 335
    label "samoch&#243;d_pu&#322;apka"
  ]
  node [
    id 336
    label "patron"
  ]
  node [
    id 337
    label "knickknack"
  ]
  node [
    id 338
    label "przedmiot"
  ]
  node [
    id 339
    label "nowo&#347;&#263;"
  ]
  node [
    id 340
    label "g&#322;owica"
  ]
  node [
    id 341
    label "trafienie"
  ]
  node [
    id 342
    label "trafianie"
  ]
  node [
    id 343
    label "kulka"
  ]
  node [
    id 344
    label "rdze&#324;"
  ]
  node [
    id 345
    label "przeniesienie"
  ]
  node [
    id 346
    label "&#322;adunek_bojowy"
  ]
  node [
    id 347
    label "trafi&#263;"
  ]
  node [
    id 348
    label "przenoszenie"
  ]
  node [
    id 349
    label "przenie&#347;&#263;"
  ]
  node [
    id 350
    label "trafia&#263;"
  ]
  node [
    id 351
    label "przenosi&#263;"
  ]
  node [
    id 352
    label "bro&#324;"
  ]
  node [
    id 353
    label "mina"
  ]
  node [
    id 354
    label "op&#243;&#378;niacz"
  ]
  node [
    id 355
    label "mechanizm"
  ]
  node [
    id 356
    label "ogie&#324;"
  ]
  node [
    id 357
    label "szew_kostny"
  ]
  node [
    id 358
    label "wiedza"
  ]
  node [
    id 359
    label "kawa&#322;ek"
  ]
  node [
    id 360
    label "trzewioczaszka"
  ]
  node [
    id 361
    label "siode&#322;ko_tureckie"
  ]
  node [
    id 362
    label "wa&#322;_nadoczodo&#322;owy"
  ]
  node [
    id 363
    label "m&#243;zgoczaszka"
  ]
  node [
    id 364
    label "&#347;mie&#263;"
  ]
  node [
    id 365
    label "ko&#347;&#263;_czo&#322;owa"
  ]
  node [
    id 366
    label "dynia"
  ]
  node [
    id 367
    label "ko&#347;&#263;_podniebienna"
  ]
  node [
    id 368
    label "rozszczep_czaszki"
  ]
  node [
    id 369
    label "korpus"
  ]
  node [
    id 370
    label "szew_strza&#322;kowy"
  ]
  node [
    id 371
    label "mak&#243;wka"
  ]
  node [
    id 372
    label "skorupa"
  ]
  node [
    id 373
    label "&#322;uk_jarzmowy"
  ]
  node [
    id 374
    label "ko&#347;&#263;_klinowa"
  ]
  node [
    id 375
    label "noosfera"
  ]
  node [
    id 376
    label "g&#322;owa"
  ]
  node [
    id 377
    label "naczynie"
  ]
  node [
    id 378
    label "szkielet"
  ]
  node [
    id 379
    label "czaszka"
  ]
  node [
    id 380
    label "zatoka"
  ]
  node [
    id 381
    label "ma&#322;og&#322;owie"
  ]
  node [
    id 382
    label "potylica"
  ]
  node [
    id 383
    label "oczod&#243;&#322;"
  ]
  node [
    id 384
    label "czapa"
  ]
  node [
    id 385
    label "lemiesz"
  ]
  node [
    id 386
    label "&#380;uchwa"
  ]
  node [
    id 387
    label "szczelina_oczodo&#322;owa"
  ]
  node [
    id 388
    label "p&#243;&#322;kula"
  ]
  node [
    id 389
    label "po&#347;redniog&#322;owo&#347;&#263;"
  ]
  node [
    id 390
    label "diafanoskopia"
  ]
  node [
    id 391
    label "umys&#322;"
  ]
  node [
    id 392
    label "&#322;eb"
  ]
  node [
    id 393
    label "granat"
  ]
  node [
    id 394
    label "ciemi&#281;"
  ]
  node [
    id 395
    label "kabina"
  ]
  node [
    id 396
    label "eskadra_bombowa"
  ]
  node [
    id 397
    label "silnik"
  ]
  node [
    id 398
    label "&#347;mig&#322;o"
  ]
  node [
    id 399
    label "samolot_bojowy"
  ]
  node [
    id 400
    label "dywizjon_bombowy"
  ]
  node [
    id 401
    label "podwozie"
  ]
  node [
    id 402
    label "forum"
  ]
  node [
    id 403
    label "podwy&#380;szenie"
  ]
  node [
    id 404
    label "platform"
  ]
  node [
    id 405
    label "grupa_dyskusyjna"
  ]
  node [
    id 406
    label "s&#261;d"
  ]
  node [
    id 407
    label "plac"
  ]
  node [
    id 408
    label "bazylika"
  ]
  node [
    id 409
    label "przestrze&#324;"
  ]
  node [
    id 410
    label "portal"
  ]
  node [
    id 411
    label "konferencja"
  ]
  node [
    id 412
    label "agora"
  ]
  node [
    id 413
    label "grupa"
  ]
  node [
    id 414
    label "strona"
  ]
  node [
    id 415
    label "powi&#281;kszenie"
  ]
  node [
    id 416
    label "proces_ekonomiczny"
  ]
  node [
    id 417
    label "erecting"
  ]
  node [
    id 418
    label "gruba_ryba"
  ]
  node [
    id 419
    label "Gorbaczow"
  ]
  node [
    id 420
    label "zwierzchnik"
  ]
  node [
    id 421
    label "Putin"
  ]
  node [
    id 422
    label "Tito"
  ]
  node [
    id 423
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 424
    label "Naser"
  ]
  node [
    id 425
    label "de_Gaulle"
  ]
  node [
    id 426
    label "Nixon"
  ]
  node [
    id 427
    label "Kemal"
  ]
  node [
    id 428
    label "Clinton"
  ]
  node [
    id 429
    label "Bierut"
  ]
  node [
    id 430
    label "Roosevelt"
  ]
  node [
    id 431
    label "samorz&#261;dowiec"
  ]
  node [
    id 432
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 433
    label "Jelcyn"
  ]
  node [
    id 434
    label "dostojnik"
  ]
  node [
    id 435
    label "pryncypa&#322;"
  ]
  node [
    id 436
    label "kierowa&#263;"
  ]
  node [
    id 437
    label "kierownictwo"
  ]
  node [
    id 438
    label "cz&#322;owiek"
  ]
  node [
    id 439
    label "urz&#281;dnik"
  ]
  node [
    id 440
    label "notabl"
  ]
  node [
    id 441
    label "oficja&#322;"
  ]
  node [
    id 442
    label "samorz&#261;d"
  ]
  node [
    id 443
    label "polityk"
  ]
  node [
    id 444
    label "komuna"
  ]
  node [
    id 445
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 446
    label "miesi&#261;c"
  ]
  node [
    id 447
    label "tydzie&#324;"
  ]
  node [
    id 448
    label "miech"
  ]
  node [
    id 449
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 450
    label "czas"
  ]
  node [
    id 451
    label "rok"
  ]
  node [
    id 452
    label "kalendy"
  ]
  node [
    id 453
    label "przesz&#322;y"
  ]
  node [
    id 454
    label "wcze&#347;niejszy"
  ]
  node [
    id 455
    label "poprzednio"
  ]
  node [
    id 456
    label "miniony"
  ]
  node [
    id 457
    label "ostatni"
  ]
  node [
    id 458
    label "wcze&#347;niej"
  ]
  node [
    id 459
    label "RP"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 453
  ]
  edge [
    source 15
    target 454
  ]
  edge [
    source 15
    target 455
  ]
  edge [
    source 15
    target 456
  ]
  edge [
    source 15
    target 457
  ]
  edge [
    source 15
    target 458
  ]
]
