graph [
  node [
    id 0
    label "siemaneczko"
    origin "text"
  ]
  node [
    id 1
    label "mireczka"
    origin "text"
  ]
  node [
    id 2
    label "mirabelka"
    origin "text"
  ]
  node [
    id 3
    label "&#347;liwa_domowa"
  ]
  node [
    id 4
    label "&#347;liwka"
  ]
  node [
    id 5
    label "&#347;liwa"
  ]
  node [
    id 6
    label "pestkowiec"
  ]
  node [
    id 7
    label "fiolet"
  ]
  node [
    id 8
    label "owoc"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
]
