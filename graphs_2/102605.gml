graph [
  node [
    id 0
    label "g&#322;&#243;wnia"
    origin "text"
  ]
  node [
    id 1
    label "pe&#322;nomocnik"
    origin "text"
  ]
  node [
    id 2
    label "graniczny"
    origin "text"
  ]
  node [
    id 3
    label "zast&#281;pca"
    origin "text"
  ]
  node [
    id 4
    label "pomocnica"
    origin "text"
  ]
  node [
    id 5
    label "cela"
    origin "text"
  ]
  node [
    id 6
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "swoje"
    origin "text"
  ]
  node [
    id 8
    label "funkcja"
    origin "text"
  ]
  node [
    id 9
    label "otrzymywa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pisemny"
    origin "text"
  ]
  node [
    id 11
    label "pe&#322;nomocnictwo"
    origin "text"
  ]
  node [
    id 12
    label "sporz&#261;dzi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 14
    label "polski"
    origin "text"
  ]
  node [
    id 15
    label "litewski"
    origin "text"
  ]
  node [
    id 16
    label "substytuowa&#263;"
  ]
  node [
    id 17
    label "substytuowanie"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "ludzko&#347;&#263;"
  ]
  node [
    id 20
    label "asymilowanie"
  ]
  node [
    id 21
    label "wapniak"
  ]
  node [
    id 22
    label "asymilowa&#263;"
  ]
  node [
    id 23
    label "os&#322;abia&#263;"
  ]
  node [
    id 24
    label "posta&#263;"
  ]
  node [
    id 25
    label "hominid"
  ]
  node [
    id 26
    label "podw&#322;adny"
  ]
  node [
    id 27
    label "os&#322;abianie"
  ]
  node [
    id 28
    label "g&#322;owa"
  ]
  node [
    id 29
    label "figura"
  ]
  node [
    id 30
    label "portrecista"
  ]
  node [
    id 31
    label "dwun&#243;g"
  ]
  node [
    id 32
    label "profanum"
  ]
  node [
    id 33
    label "mikrokosmos"
  ]
  node [
    id 34
    label "nasada"
  ]
  node [
    id 35
    label "duch"
  ]
  node [
    id 36
    label "antropochoria"
  ]
  node [
    id 37
    label "osoba"
  ]
  node [
    id 38
    label "wz&#243;r"
  ]
  node [
    id 39
    label "senior"
  ]
  node [
    id 40
    label "oddzia&#322;ywanie"
  ]
  node [
    id 41
    label "Adam"
  ]
  node [
    id 42
    label "homo_sapiens"
  ]
  node [
    id 43
    label "polifag"
  ]
  node [
    id 44
    label "wskazywanie"
  ]
  node [
    id 45
    label "podstawienie"
  ]
  node [
    id 46
    label "wskazanie"
  ]
  node [
    id 47
    label "podstawianie"
  ]
  node [
    id 48
    label "wskaza&#263;"
  ]
  node [
    id 49
    label "podstawi&#263;"
  ]
  node [
    id 50
    label "wskazywa&#263;"
  ]
  node [
    id 51
    label "zast&#261;pi&#263;"
  ]
  node [
    id 52
    label "zast&#281;powa&#263;"
  ]
  node [
    id 53
    label "podstawia&#263;"
  ]
  node [
    id 54
    label "protezowa&#263;"
  ]
  node [
    id 55
    label "skrajny"
  ]
  node [
    id 56
    label "przyleg&#322;y"
  ]
  node [
    id 57
    label "granicznie"
  ]
  node [
    id 58
    label "wa&#380;ny"
  ]
  node [
    id 59
    label "ostateczny"
  ]
  node [
    id 60
    label "wynios&#322;y"
  ]
  node [
    id 61
    label "dono&#347;ny"
  ]
  node [
    id 62
    label "silny"
  ]
  node [
    id 63
    label "wa&#380;nie"
  ]
  node [
    id 64
    label "istotnie"
  ]
  node [
    id 65
    label "znaczny"
  ]
  node [
    id 66
    label "eksponowany"
  ]
  node [
    id 67
    label "dobry"
  ]
  node [
    id 68
    label "skrajnie"
  ]
  node [
    id 69
    label "okrajkowy"
  ]
  node [
    id 70
    label "konieczny"
  ]
  node [
    id 71
    label "ostatecznie"
  ]
  node [
    id 72
    label "zupe&#322;ny"
  ]
  node [
    id 73
    label "bliski"
  ]
  node [
    id 74
    label "przylegle"
  ]
  node [
    id 75
    label "przygraniczny"
  ]
  node [
    id 76
    label "bezpo&#347;rednio"
  ]
  node [
    id 77
    label "klasztor"
  ]
  node [
    id 78
    label "pomieszczenie"
  ]
  node [
    id 79
    label "amfilada"
  ]
  node [
    id 80
    label "front"
  ]
  node [
    id 81
    label "apartment"
  ]
  node [
    id 82
    label "pod&#322;oga"
  ]
  node [
    id 83
    label "udost&#281;pnienie"
  ]
  node [
    id 84
    label "miejsce"
  ]
  node [
    id 85
    label "sklepienie"
  ]
  node [
    id 86
    label "sufit"
  ]
  node [
    id 87
    label "umieszczenie"
  ]
  node [
    id 88
    label "zakamarek"
  ]
  node [
    id 89
    label "oratorium"
  ]
  node [
    id 90
    label "siedziba"
  ]
  node [
    id 91
    label "wirydarz"
  ]
  node [
    id 92
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 93
    label "zakon"
  ]
  node [
    id 94
    label "refektarz"
  ]
  node [
    id 95
    label "kapitularz"
  ]
  node [
    id 96
    label "kustodia"
  ]
  node [
    id 97
    label "&#321;agiewniki"
  ]
  node [
    id 98
    label "rola"
  ]
  node [
    id 99
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 100
    label "robi&#263;"
  ]
  node [
    id 101
    label "wytwarza&#263;"
  ]
  node [
    id 102
    label "work"
  ]
  node [
    id 103
    label "create"
  ]
  node [
    id 104
    label "muzyka"
  ]
  node [
    id 105
    label "praca"
  ]
  node [
    id 106
    label "organizowa&#263;"
  ]
  node [
    id 107
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 108
    label "czyni&#263;"
  ]
  node [
    id 109
    label "give"
  ]
  node [
    id 110
    label "stylizowa&#263;"
  ]
  node [
    id 111
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 112
    label "falowa&#263;"
  ]
  node [
    id 113
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 114
    label "peddle"
  ]
  node [
    id 115
    label "wydala&#263;"
  ]
  node [
    id 116
    label "tentegowa&#263;"
  ]
  node [
    id 117
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 118
    label "urz&#261;dza&#263;"
  ]
  node [
    id 119
    label "oszukiwa&#263;"
  ]
  node [
    id 120
    label "ukazywa&#263;"
  ]
  node [
    id 121
    label "przerabia&#263;"
  ]
  node [
    id 122
    label "act"
  ]
  node [
    id 123
    label "post&#281;powa&#263;"
  ]
  node [
    id 124
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 125
    label "najem"
  ]
  node [
    id 126
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 127
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 128
    label "zak&#322;ad"
  ]
  node [
    id 129
    label "stosunek_pracy"
  ]
  node [
    id 130
    label "benedykty&#324;ski"
  ]
  node [
    id 131
    label "poda&#380;_pracy"
  ]
  node [
    id 132
    label "pracowanie"
  ]
  node [
    id 133
    label "tyrka"
  ]
  node [
    id 134
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 135
    label "wytw&#243;r"
  ]
  node [
    id 136
    label "zaw&#243;d"
  ]
  node [
    id 137
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 138
    label "tynkarski"
  ]
  node [
    id 139
    label "pracowa&#263;"
  ]
  node [
    id 140
    label "czynno&#347;&#263;"
  ]
  node [
    id 141
    label "zmiana"
  ]
  node [
    id 142
    label "czynnik_produkcji"
  ]
  node [
    id 143
    label "zobowi&#261;zanie"
  ]
  node [
    id 144
    label "kierownictwo"
  ]
  node [
    id 145
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 146
    label "wokalistyka"
  ]
  node [
    id 147
    label "przedmiot"
  ]
  node [
    id 148
    label "wykonywanie"
  ]
  node [
    id 149
    label "muza"
  ]
  node [
    id 150
    label "zjawisko"
  ]
  node [
    id 151
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 152
    label "beatbox"
  ]
  node [
    id 153
    label "komponowa&#263;"
  ]
  node [
    id 154
    label "szko&#322;a"
  ]
  node [
    id 155
    label "komponowanie"
  ]
  node [
    id 156
    label "pasa&#380;"
  ]
  node [
    id 157
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 158
    label "notacja_muzyczna"
  ]
  node [
    id 159
    label "kontrapunkt"
  ]
  node [
    id 160
    label "nauka"
  ]
  node [
    id 161
    label "sztuka"
  ]
  node [
    id 162
    label "instrumentalistyka"
  ]
  node [
    id 163
    label "harmonia"
  ]
  node [
    id 164
    label "set"
  ]
  node [
    id 165
    label "wys&#322;uchanie"
  ]
  node [
    id 166
    label "kapela"
  ]
  node [
    id 167
    label "britpop"
  ]
  node [
    id 168
    label "uprawienie"
  ]
  node [
    id 169
    label "kszta&#322;t"
  ]
  node [
    id 170
    label "dialog"
  ]
  node [
    id 171
    label "p&#322;osa"
  ]
  node [
    id 172
    label "plik"
  ]
  node [
    id 173
    label "ziemia"
  ]
  node [
    id 174
    label "czyn"
  ]
  node [
    id 175
    label "ustawienie"
  ]
  node [
    id 176
    label "scenariusz"
  ]
  node [
    id 177
    label "pole"
  ]
  node [
    id 178
    label "gospodarstwo"
  ]
  node [
    id 179
    label "uprawi&#263;"
  ]
  node [
    id 180
    label "function"
  ]
  node [
    id 181
    label "zreinterpretowa&#263;"
  ]
  node [
    id 182
    label "zastosowanie"
  ]
  node [
    id 183
    label "reinterpretowa&#263;"
  ]
  node [
    id 184
    label "wrench"
  ]
  node [
    id 185
    label "irygowanie"
  ]
  node [
    id 186
    label "ustawi&#263;"
  ]
  node [
    id 187
    label "irygowa&#263;"
  ]
  node [
    id 188
    label "zreinterpretowanie"
  ]
  node [
    id 189
    label "cel"
  ]
  node [
    id 190
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 191
    label "gra&#263;"
  ]
  node [
    id 192
    label "aktorstwo"
  ]
  node [
    id 193
    label "kostium"
  ]
  node [
    id 194
    label "zagon"
  ]
  node [
    id 195
    label "znaczenie"
  ]
  node [
    id 196
    label "zagra&#263;"
  ]
  node [
    id 197
    label "reinterpretowanie"
  ]
  node [
    id 198
    label "sk&#322;ad"
  ]
  node [
    id 199
    label "tekst"
  ]
  node [
    id 200
    label "zagranie"
  ]
  node [
    id 201
    label "radlina"
  ]
  node [
    id 202
    label "granie"
  ]
  node [
    id 203
    label "supremum"
  ]
  node [
    id 204
    label "addytywno&#347;&#263;"
  ]
  node [
    id 205
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 206
    label "jednostka"
  ]
  node [
    id 207
    label "matematyka"
  ]
  node [
    id 208
    label "funkcjonowanie"
  ]
  node [
    id 209
    label "rzut"
  ]
  node [
    id 210
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 211
    label "powierzanie"
  ]
  node [
    id 212
    label "dziedzina"
  ]
  node [
    id 213
    label "przeciwdziedzina"
  ]
  node [
    id 214
    label "awansowa&#263;"
  ]
  node [
    id 215
    label "stawia&#263;"
  ]
  node [
    id 216
    label "wakowa&#263;"
  ]
  node [
    id 217
    label "postawi&#263;"
  ]
  node [
    id 218
    label "awansowanie"
  ]
  node [
    id 219
    label "infimum"
  ]
  node [
    id 220
    label "punkt"
  ]
  node [
    id 221
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 222
    label "rezultat"
  ]
  node [
    id 223
    label "thing"
  ]
  node [
    id 224
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 225
    label "rzecz"
  ]
  node [
    id 226
    label "odk&#322;adanie"
  ]
  node [
    id 227
    label "condition"
  ]
  node [
    id 228
    label "liczenie"
  ]
  node [
    id 229
    label "stawianie"
  ]
  node [
    id 230
    label "bycie"
  ]
  node [
    id 231
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 232
    label "assay"
  ]
  node [
    id 233
    label "wyraz"
  ]
  node [
    id 234
    label "gravity"
  ]
  node [
    id 235
    label "weight"
  ]
  node [
    id 236
    label "command"
  ]
  node [
    id 237
    label "odgrywanie_roli"
  ]
  node [
    id 238
    label "istota"
  ]
  node [
    id 239
    label "informacja"
  ]
  node [
    id 240
    label "cecha"
  ]
  node [
    id 241
    label "okre&#347;lanie"
  ]
  node [
    id 242
    label "kto&#347;"
  ]
  node [
    id 243
    label "wyra&#380;enie"
  ]
  node [
    id 244
    label "oddawanie"
  ]
  node [
    id 245
    label "stanowisko"
  ]
  node [
    id 246
    label "zlecanie"
  ]
  node [
    id 247
    label "ufanie"
  ]
  node [
    id 248
    label "wyznawanie"
  ]
  node [
    id 249
    label "zadanie"
  ]
  node [
    id 250
    label "przej&#347;cie"
  ]
  node [
    id 251
    label "przechodzenie"
  ]
  node [
    id 252
    label "przeniesienie"
  ]
  node [
    id 253
    label "status"
  ]
  node [
    id 254
    label "promowanie"
  ]
  node [
    id 255
    label "habilitowanie_si&#281;"
  ]
  node [
    id 256
    label "obj&#281;cie"
  ]
  node [
    id 257
    label "obejmowanie"
  ]
  node [
    id 258
    label "kariera"
  ]
  node [
    id 259
    label "przenoszenie"
  ]
  node [
    id 260
    label "pozyskiwanie"
  ]
  node [
    id 261
    label "pozyskanie"
  ]
  node [
    id 262
    label "pi&#281;cie_si&#281;"
  ]
  node [
    id 263
    label "raise"
  ]
  node [
    id 264
    label "pozyska&#263;"
  ]
  node [
    id 265
    label "obejmowa&#263;"
  ]
  node [
    id 266
    label "pozyskiwa&#263;"
  ]
  node [
    id 267
    label "dawa&#263;_awans"
  ]
  node [
    id 268
    label "obj&#261;&#263;"
  ]
  node [
    id 269
    label "przej&#347;&#263;"
  ]
  node [
    id 270
    label "pi&#261;&#263;_si&#281;"
  ]
  node [
    id 271
    label "da&#263;_awans"
  ]
  node [
    id 272
    label "przechodzi&#263;"
  ]
  node [
    id 273
    label "wolny"
  ]
  node [
    id 274
    label "by&#263;"
  ]
  node [
    id 275
    label "pozostawia&#263;"
  ]
  node [
    id 276
    label "wydawa&#263;"
  ]
  node [
    id 277
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 278
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 279
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 280
    label "przewidywa&#263;"
  ]
  node [
    id 281
    label "przyznawa&#263;"
  ]
  node [
    id 282
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 283
    label "go"
  ]
  node [
    id 284
    label "obstawia&#263;"
  ]
  node [
    id 285
    label "umieszcza&#263;"
  ]
  node [
    id 286
    label "ocenia&#263;"
  ]
  node [
    id 287
    label "zastawia&#263;"
  ]
  node [
    id 288
    label "znak"
  ]
  node [
    id 289
    label "introduce"
  ]
  node [
    id 290
    label "uruchamia&#263;"
  ]
  node [
    id 291
    label "fundowa&#263;"
  ]
  node [
    id 292
    label "zmienia&#263;"
  ]
  node [
    id 293
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 294
    label "deliver"
  ]
  node [
    id 295
    label "powodowa&#263;"
  ]
  node [
    id 296
    label "wyznacza&#263;"
  ]
  node [
    id 297
    label "przedstawia&#263;"
  ]
  node [
    id 298
    label "wydobywa&#263;"
  ]
  node [
    id 299
    label "zafundowa&#263;"
  ]
  node [
    id 300
    label "budowla"
  ]
  node [
    id 301
    label "wyda&#263;"
  ]
  node [
    id 302
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 303
    label "plant"
  ]
  node [
    id 304
    label "uruchomi&#263;"
  ]
  node [
    id 305
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 306
    label "pozostawi&#263;"
  ]
  node [
    id 307
    label "obra&#263;"
  ]
  node [
    id 308
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 309
    label "obstawi&#263;"
  ]
  node [
    id 310
    label "zmieni&#263;"
  ]
  node [
    id 311
    label "post"
  ]
  node [
    id 312
    label "wyznaczy&#263;"
  ]
  node [
    id 313
    label "oceni&#263;"
  ]
  node [
    id 314
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 315
    label "uczyni&#263;"
  ]
  node [
    id 316
    label "spowodowa&#263;"
  ]
  node [
    id 317
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 318
    label "wytworzy&#263;"
  ]
  node [
    id 319
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 320
    label "umie&#347;ci&#263;"
  ]
  node [
    id 321
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 322
    label "przyzna&#263;"
  ]
  node [
    id 323
    label "wydoby&#263;"
  ]
  node [
    id 324
    label "przedstawi&#263;"
  ]
  node [
    id 325
    label "establish"
  ]
  node [
    id 326
    label "stawi&#263;"
  ]
  node [
    id 327
    label "rozpoznawalno&#347;&#263;"
  ]
  node [
    id 328
    label "rachunek_operatorowy"
  ]
  node [
    id 329
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 330
    label "kryptologia"
  ]
  node [
    id 331
    label "logicyzm"
  ]
  node [
    id 332
    label "logika"
  ]
  node [
    id 333
    label "matematyka_czysta"
  ]
  node [
    id 334
    label "forsing"
  ]
  node [
    id 335
    label "modelowanie_matematyczne"
  ]
  node [
    id 336
    label "matma"
  ]
  node [
    id 337
    label "teoria_katastrof"
  ]
  node [
    id 338
    label "kierunek"
  ]
  node [
    id 339
    label "fizyka_matematyczna"
  ]
  node [
    id 340
    label "teoria_graf&#243;w"
  ]
  node [
    id 341
    label "rachunki"
  ]
  node [
    id 342
    label "topologia_algebraiczna"
  ]
  node [
    id 343
    label "matematyka_stosowana"
  ]
  node [
    id 344
    label "ograniczenie"
  ]
  node [
    id 345
    label "armia"
  ]
  node [
    id 346
    label "nawr&#243;t_choroby"
  ]
  node [
    id 347
    label "potomstwo"
  ]
  node [
    id 348
    label "odwzorowanie"
  ]
  node [
    id 349
    label "rysunek"
  ]
  node [
    id 350
    label "scene"
  ]
  node [
    id 351
    label "throw"
  ]
  node [
    id 352
    label "float"
  ]
  node [
    id 353
    label "projection"
  ]
  node [
    id 354
    label "injection"
  ]
  node [
    id 355
    label "blow"
  ]
  node [
    id 356
    label "pomys&#322;"
  ]
  node [
    id 357
    label "ruch"
  ]
  node [
    id 358
    label "k&#322;ad"
  ]
  node [
    id 359
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 360
    label "mold"
  ]
  node [
    id 361
    label "przyswoi&#263;"
  ]
  node [
    id 362
    label "one"
  ]
  node [
    id 363
    label "poj&#281;cie"
  ]
  node [
    id 364
    label "ewoluowanie"
  ]
  node [
    id 365
    label "skala"
  ]
  node [
    id 366
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 367
    label "przyswajanie"
  ]
  node [
    id 368
    label "wyewoluowanie"
  ]
  node [
    id 369
    label "reakcja"
  ]
  node [
    id 370
    label "przeliczy&#263;"
  ]
  node [
    id 371
    label "wyewoluowa&#263;"
  ]
  node [
    id 372
    label "ewoluowa&#263;"
  ]
  node [
    id 373
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 374
    label "liczba_naturalna"
  ]
  node [
    id 375
    label "czynnik_biotyczny"
  ]
  node [
    id 376
    label "individual"
  ]
  node [
    id 377
    label "obiekt"
  ]
  node [
    id 378
    label "przyswaja&#263;"
  ]
  node [
    id 379
    label "przyswojenie"
  ]
  node [
    id 380
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 381
    label "starzenie_si&#281;"
  ]
  node [
    id 382
    label "przeliczanie"
  ]
  node [
    id 383
    label "przelicza&#263;"
  ]
  node [
    id 384
    label "przeliczenie"
  ]
  node [
    id 385
    label "stosowanie"
  ]
  node [
    id 386
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 387
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 388
    label "use"
  ]
  node [
    id 389
    label "zrobienie"
  ]
  node [
    id 390
    label "podtrzymywanie"
  ]
  node [
    id 391
    label "w&#322;&#261;czanie"
  ]
  node [
    id 392
    label "w&#322;&#261;czenie"
  ]
  node [
    id 393
    label "nakr&#281;cenie"
  ]
  node [
    id 394
    label "uruchomienie"
  ]
  node [
    id 395
    label "nakr&#281;canie"
  ]
  node [
    id 396
    label "impact"
  ]
  node [
    id 397
    label "tr&#243;jstronny"
  ]
  node [
    id 398
    label "dzianie_si&#281;"
  ]
  node [
    id 399
    label "uruchamianie"
  ]
  node [
    id 400
    label "zatrzymanie"
  ]
  node [
    id 401
    label "zbi&#243;r"
  ]
  node [
    id 402
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 403
    label "sfera"
  ]
  node [
    id 404
    label "zakres"
  ]
  node [
    id 405
    label "bezdro&#380;e"
  ]
  node [
    id 406
    label "poddzia&#322;"
  ]
  node [
    id 407
    label "return"
  ]
  node [
    id 408
    label "dostawa&#263;"
  ]
  node [
    id 409
    label "take"
  ]
  node [
    id 410
    label "mie&#263;_miejsce"
  ]
  node [
    id 411
    label "nabywa&#263;"
  ]
  node [
    id 412
    label "uzyskiwa&#263;"
  ]
  node [
    id 413
    label "bra&#263;"
  ]
  node [
    id 414
    label "winnings"
  ]
  node [
    id 415
    label "opanowywa&#263;"
  ]
  node [
    id 416
    label "si&#281;ga&#263;"
  ]
  node [
    id 417
    label "range"
  ]
  node [
    id 418
    label "wystarcza&#263;"
  ]
  node [
    id 419
    label "kupowa&#263;"
  ]
  node [
    id 420
    label "obskakiwa&#263;"
  ]
  node [
    id 421
    label "pisemnie"
  ]
  node [
    id 422
    label "pi&#347;mienny"
  ]
  node [
    id 423
    label "wykszta&#322;cony"
  ]
  node [
    id 424
    label "za&#347;wiadczenie"
  ]
  node [
    id 425
    label "umocowa&#263;"
  ]
  node [
    id 426
    label "authority"
  ]
  node [
    id 427
    label "prawo"
  ]
  node [
    id 428
    label "power_of_attorney"
  ]
  node [
    id 429
    label "potwierdzenie"
  ]
  node [
    id 430
    label "certificate"
  ]
  node [
    id 431
    label "dokument"
  ]
  node [
    id 432
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 433
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 434
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 435
    label "procesualistyka"
  ]
  node [
    id 436
    label "regu&#322;a_Allena"
  ]
  node [
    id 437
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 438
    label "kryminalistyka"
  ]
  node [
    id 439
    label "struktura"
  ]
  node [
    id 440
    label "zasada_d'Alemberta"
  ]
  node [
    id 441
    label "obserwacja"
  ]
  node [
    id 442
    label "normatywizm"
  ]
  node [
    id 443
    label "jurisprudence"
  ]
  node [
    id 444
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 445
    label "kultura_duchowa"
  ]
  node [
    id 446
    label "przepis"
  ]
  node [
    id 447
    label "prawo_karne_procesowe"
  ]
  node [
    id 448
    label "criterion"
  ]
  node [
    id 449
    label "kazuistyka"
  ]
  node [
    id 450
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 451
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 452
    label "kryminologia"
  ]
  node [
    id 453
    label "opis"
  ]
  node [
    id 454
    label "regu&#322;a_Glogera"
  ]
  node [
    id 455
    label "prawo_Mendla"
  ]
  node [
    id 456
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 457
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 458
    label "prawo_karne"
  ]
  node [
    id 459
    label "legislacyjnie"
  ]
  node [
    id 460
    label "twierdzenie"
  ]
  node [
    id 461
    label "cywilistyka"
  ]
  node [
    id 462
    label "judykatura"
  ]
  node [
    id 463
    label "kanonistyka"
  ]
  node [
    id 464
    label "standard"
  ]
  node [
    id 465
    label "nauka_prawa"
  ]
  node [
    id 466
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 467
    label "podmiot"
  ]
  node [
    id 468
    label "law"
  ]
  node [
    id 469
    label "qualification"
  ]
  node [
    id 470
    label "dominion"
  ]
  node [
    id 471
    label "wykonawczy"
  ]
  node [
    id 472
    label "zasada"
  ]
  node [
    id 473
    label "normalizacja"
  ]
  node [
    id 474
    label "nada&#263;"
  ]
  node [
    id 475
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 476
    label "cook"
  ]
  node [
    id 477
    label "draw"
  ]
  node [
    id 478
    label "stworzy&#263;"
  ]
  node [
    id 479
    label "opracowa&#263;"
  ]
  node [
    id 480
    label "zrobi&#263;"
  ]
  node [
    id 481
    label "oprawi&#263;"
  ]
  node [
    id 482
    label "podzieli&#263;"
  ]
  node [
    id 483
    label "przygotowa&#263;"
  ]
  node [
    id 484
    label "specjalista_od_public_relations"
  ]
  node [
    id 485
    label "wizerunek"
  ]
  node [
    id 486
    label "invent"
  ]
  node [
    id 487
    label "post&#261;pi&#263;"
  ]
  node [
    id 488
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 489
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 490
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 491
    label "zorganizowa&#263;"
  ]
  node [
    id 492
    label "appoint"
  ]
  node [
    id 493
    label "wystylizowa&#263;"
  ]
  node [
    id 494
    label "cause"
  ]
  node [
    id 495
    label "przerobi&#263;"
  ]
  node [
    id 496
    label "nabra&#263;"
  ]
  node [
    id 497
    label "make"
  ]
  node [
    id 498
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 499
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 500
    label "wydali&#263;"
  ]
  node [
    id 501
    label "divide"
  ]
  node [
    id 502
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 503
    label "exchange"
  ]
  node [
    id 504
    label "change"
  ]
  node [
    id 505
    label "rozda&#263;"
  ]
  node [
    id 506
    label "policzy&#263;"
  ]
  node [
    id 507
    label "distribute"
  ]
  node [
    id 508
    label "wydzieli&#263;"
  ]
  node [
    id 509
    label "transgress"
  ]
  node [
    id 510
    label "pigeonhole"
  ]
  node [
    id 511
    label "sk&#322;&#243;ci&#263;"
  ]
  node [
    id 512
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 513
    label "impart"
  ]
  node [
    id 514
    label "uatrakcyjni&#263;"
  ]
  node [
    id 515
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 516
    label "obsadzi&#263;"
  ]
  node [
    id 517
    label "oblige"
  ]
  node [
    id 518
    label "frame"
  ]
  node [
    id 519
    label "manufacture"
  ]
  node [
    id 520
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 521
    label "wykona&#263;"
  ]
  node [
    id 522
    label "wyszkoli&#263;"
  ]
  node [
    id 523
    label "train"
  ]
  node [
    id 524
    label "arrange"
  ]
  node [
    id 525
    label "dress"
  ]
  node [
    id 526
    label "ukierunkowa&#263;"
  ]
  node [
    id 527
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 528
    label "artykulator"
  ]
  node [
    id 529
    label "kod"
  ]
  node [
    id 530
    label "kawa&#322;ek"
  ]
  node [
    id 531
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 532
    label "gramatyka"
  ]
  node [
    id 533
    label "stylik"
  ]
  node [
    id 534
    label "przet&#322;umaczenie"
  ]
  node [
    id 535
    label "formalizowanie"
  ]
  node [
    id 536
    label "ssa&#263;"
  ]
  node [
    id 537
    label "ssanie"
  ]
  node [
    id 538
    label "language"
  ]
  node [
    id 539
    label "liza&#263;"
  ]
  node [
    id 540
    label "napisa&#263;"
  ]
  node [
    id 541
    label "konsonantyzm"
  ]
  node [
    id 542
    label "wokalizm"
  ]
  node [
    id 543
    label "pisa&#263;"
  ]
  node [
    id 544
    label "fonetyka"
  ]
  node [
    id 545
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 546
    label "jeniec"
  ]
  node [
    id 547
    label "but"
  ]
  node [
    id 548
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 549
    label "po_koroniarsku"
  ]
  node [
    id 550
    label "t&#322;umaczenie"
  ]
  node [
    id 551
    label "m&#243;wienie"
  ]
  node [
    id 552
    label "pype&#263;"
  ]
  node [
    id 553
    label "lizanie"
  ]
  node [
    id 554
    label "pismo"
  ]
  node [
    id 555
    label "formalizowa&#263;"
  ]
  node [
    id 556
    label "rozumie&#263;"
  ]
  node [
    id 557
    label "organ"
  ]
  node [
    id 558
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 559
    label "rozumienie"
  ]
  node [
    id 560
    label "spos&#243;b"
  ]
  node [
    id 561
    label "makroglosja"
  ]
  node [
    id 562
    label "m&#243;wi&#263;"
  ]
  node [
    id 563
    label "jama_ustna"
  ]
  node [
    id 564
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 565
    label "formacja_geologiczna"
  ]
  node [
    id 566
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 567
    label "natural_language"
  ]
  node [
    id 568
    label "s&#322;ownictwo"
  ]
  node [
    id 569
    label "urz&#261;dzenie"
  ]
  node [
    id 570
    label "kawa&#322;"
  ]
  node [
    id 571
    label "plot"
  ]
  node [
    id 572
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 573
    label "utw&#243;r"
  ]
  node [
    id 574
    label "piece"
  ]
  node [
    id 575
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 576
    label "podp&#322;ywanie"
  ]
  node [
    id 577
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 578
    label "model"
  ]
  node [
    id 579
    label "narz&#281;dzie"
  ]
  node [
    id 580
    label "tryb"
  ]
  node [
    id 581
    label "nature"
  ]
  node [
    id 582
    label "code"
  ]
  node [
    id 583
    label "szyfrowanie"
  ]
  node [
    id 584
    label "ci&#261;g"
  ]
  node [
    id 585
    label "szablon"
  ]
  node [
    id 586
    label "&#380;o&#322;nierz"
  ]
  node [
    id 587
    label "internowanie"
  ]
  node [
    id 588
    label "ojczyc"
  ]
  node [
    id 589
    label "pojmaniec"
  ]
  node [
    id 590
    label "niewolnik"
  ]
  node [
    id 591
    label "internowa&#263;"
  ]
  node [
    id 592
    label "tkanka"
  ]
  node [
    id 593
    label "jednostka_organizacyjna"
  ]
  node [
    id 594
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 595
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 596
    label "tw&#243;r"
  ]
  node [
    id 597
    label "organogeneza"
  ]
  node [
    id 598
    label "zesp&#243;&#322;"
  ]
  node [
    id 599
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 600
    label "struktura_anatomiczna"
  ]
  node [
    id 601
    label "uk&#322;ad"
  ]
  node [
    id 602
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 603
    label "dekortykacja"
  ]
  node [
    id 604
    label "Izba_Konsyliarska"
  ]
  node [
    id 605
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 606
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 607
    label "stomia"
  ]
  node [
    id 608
    label "budowa"
  ]
  node [
    id 609
    label "okolica"
  ]
  node [
    id 610
    label "Komitet_Region&#243;w"
  ]
  node [
    id 611
    label "aparat_artykulacyjny"
  ]
  node [
    id 612
    label "zboczenie"
  ]
  node [
    id 613
    label "om&#243;wienie"
  ]
  node [
    id 614
    label "sponiewieranie"
  ]
  node [
    id 615
    label "discipline"
  ]
  node [
    id 616
    label "omawia&#263;"
  ]
  node [
    id 617
    label "kr&#261;&#380;enie"
  ]
  node [
    id 618
    label "tre&#347;&#263;"
  ]
  node [
    id 619
    label "robienie"
  ]
  node [
    id 620
    label "sponiewiera&#263;"
  ]
  node [
    id 621
    label "element"
  ]
  node [
    id 622
    label "entity"
  ]
  node [
    id 623
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 624
    label "tematyka"
  ]
  node [
    id 625
    label "w&#261;tek"
  ]
  node [
    id 626
    label "charakter"
  ]
  node [
    id 627
    label "zbaczanie"
  ]
  node [
    id 628
    label "program_nauczania"
  ]
  node [
    id 629
    label "om&#243;wi&#263;"
  ]
  node [
    id 630
    label "omawianie"
  ]
  node [
    id 631
    label "kultura"
  ]
  node [
    id 632
    label "zbacza&#263;"
  ]
  node [
    id 633
    label "zboczy&#263;"
  ]
  node [
    id 634
    label "zapi&#281;tek"
  ]
  node [
    id 635
    label "sznurowad&#322;o"
  ]
  node [
    id 636
    label "rozbijarka"
  ]
  node [
    id 637
    label "podeszwa"
  ]
  node [
    id 638
    label "obcas"
  ]
  node [
    id 639
    label "wzuwanie"
  ]
  node [
    id 640
    label "wzu&#263;"
  ]
  node [
    id 641
    label "przyszwa"
  ]
  node [
    id 642
    label "raki"
  ]
  node [
    id 643
    label "cholewa"
  ]
  node [
    id 644
    label "cholewka"
  ]
  node [
    id 645
    label "zel&#243;wka"
  ]
  node [
    id 646
    label "obuwie"
  ]
  node [
    id 647
    label "napi&#281;tek"
  ]
  node [
    id 648
    label "wzucie"
  ]
  node [
    id 649
    label "kom&#243;rka"
  ]
  node [
    id 650
    label "furnishing"
  ]
  node [
    id 651
    label "zabezpieczenie"
  ]
  node [
    id 652
    label "wyrz&#261;dzenie"
  ]
  node [
    id 653
    label "zagospodarowanie"
  ]
  node [
    id 654
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 655
    label "ig&#322;a"
  ]
  node [
    id 656
    label "wirnik"
  ]
  node [
    id 657
    label "aparatura"
  ]
  node [
    id 658
    label "system_energetyczny"
  ]
  node [
    id 659
    label "impulsator"
  ]
  node [
    id 660
    label "mechanizm"
  ]
  node [
    id 661
    label "sprz&#281;t"
  ]
  node [
    id 662
    label "blokowanie"
  ]
  node [
    id 663
    label "zablokowanie"
  ]
  node [
    id 664
    label "przygotowanie"
  ]
  node [
    id 665
    label "komora"
  ]
  node [
    id 666
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 667
    label "public_speaking"
  ]
  node [
    id 668
    label "powiadanie"
  ]
  node [
    id 669
    label "przepowiadanie"
  ]
  node [
    id 670
    label "wypowiadanie"
  ]
  node [
    id 671
    label "wydobywanie"
  ]
  node [
    id 672
    label "gaworzenie"
  ]
  node [
    id 673
    label "wyra&#380;anie"
  ]
  node [
    id 674
    label "formu&#322;owanie"
  ]
  node [
    id 675
    label "dowalenie"
  ]
  node [
    id 676
    label "przerywanie"
  ]
  node [
    id 677
    label "wydawanie"
  ]
  node [
    id 678
    label "dogadywanie_si&#281;"
  ]
  node [
    id 679
    label "dodawanie"
  ]
  node [
    id 680
    label "prawienie"
  ]
  node [
    id 681
    label "opowiadanie"
  ]
  node [
    id 682
    label "ozywanie_si&#281;"
  ]
  node [
    id 683
    label "zapeszanie"
  ]
  node [
    id 684
    label "zwracanie_si&#281;"
  ]
  node [
    id 685
    label "dysfonia"
  ]
  node [
    id 686
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 687
    label "speaking"
  ]
  node [
    id 688
    label "zauwa&#380;enie"
  ]
  node [
    id 689
    label "mawianie"
  ]
  node [
    id 690
    label "opowiedzenie"
  ]
  node [
    id 691
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 692
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 693
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 694
    label "informowanie"
  ]
  node [
    id 695
    label "dogadanie_si&#281;"
  ]
  node [
    id 696
    label "wygadanie"
  ]
  node [
    id 697
    label "terminology"
  ]
  node [
    id 698
    label "termin"
  ]
  node [
    id 699
    label "g&#322;osownia"
  ]
  node [
    id 700
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 701
    label "zasymilowa&#263;"
  ]
  node [
    id 702
    label "phonetics"
  ]
  node [
    id 703
    label "palatogram"
  ]
  node [
    id 704
    label "transkrypcja"
  ]
  node [
    id 705
    label "zasymilowanie"
  ]
  node [
    id 706
    label "psychotest"
  ]
  node [
    id 707
    label "wk&#322;ad"
  ]
  node [
    id 708
    label "handwriting"
  ]
  node [
    id 709
    label "przekaz"
  ]
  node [
    id 710
    label "dzie&#322;o"
  ]
  node [
    id 711
    label "paleograf"
  ]
  node [
    id 712
    label "interpunkcja"
  ]
  node [
    id 713
    label "dzia&#322;"
  ]
  node [
    id 714
    label "grafia"
  ]
  node [
    id 715
    label "egzemplarz"
  ]
  node [
    id 716
    label "communication"
  ]
  node [
    id 717
    label "script"
  ]
  node [
    id 718
    label "zajawka"
  ]
  node [
    id 719
    label "list"
  ]
  node [
    id 720
    label "adres"
  ]
  node [
    id 721
    label "Zwrotnica"
  ]
  node [
    id 722
    label "czasopismo"
  ]
  node [
    id 723
    label "ok&#322;adka"
  ]
  node [
    id 724
    label "ortografia"
  ]
  node [
    id 725
    label "letter"
  ]
  node [
    id 726
    label "komunikacja"
  ]
  node [
    id 727
    label "paleografia"
  ]
  node [
    id 728
    label "prasa"
  ]
  node [
    id 729
    label "fleksja"
  ]
  node [
    id 730
    label "sk&#322;adnia"
  ]
  node [
    id 731
    label "kategoria_gramatyczna"
  ]
  node [
    id 732
    label "morfologia"
  ]
  node [
    id 733
    label "styl"
  ]
  node [
    id 734
    label "read"
  ]
  node [
    id 735
    label "write"
  ]
  node [
    id 736
    label "donie&#347;&#263;"
  ]
  node [
    id 737
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 738
    label "formu&#322;owa&#263;"
  ]
  node [
    id 739
    label "ozdabia&#263;"
  ]
  node [
    id 740
    label "spell"
  ]
  node [
    id 741
    label "skryba"
  ]
  node [
    id 742
    label "donosi&#263;"
  ]
  node [
    id 743
    label "dysgrafia"
  ]
  node [
    id 744
    label "dysortografia"
  ]
  node [
    id 745
    label "tworzy&#263;"
  ]
  node [
    id 746
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 747
    label "rendition"
  ]
  node [
    id 748
    label "explanation"
  ]
  node [
    id 749
    label "bronienie"
  ]
  node [
    id 750
    label "remark"
  ]
  node [
    id 751
    label "przek&#322;adanie"
  ]
  node [
    id 752
    label "zrozumia&#322;y"
  ]
  node [
    id 753
    label "przekonywanie"
  ]
  node [
    id 754
    label "uzasadnianie"
  ]
  node [
    id 755
    label "rozwianie"
  ]
  node [
    id 756
    label "rozwiewanie"
  ]
  node [
    id 757
    label "gossip"
  ]
  node [
    id 758
    label "przedstawianie"
  ]
  node [
    id 759
    label "kr&#281;ty"
  ]
  node [
    id 760
    label "poja&#347;nia&#263;"
  ]
  node [
    id 761
    label "u&#322;atwia&#263;"
  ]
  node [
    id 762
    label "elaborate"
  ]
  node [
    id 763
    label "suplikowa&#263;"
  ]
  node [
    id 764
    label "przek&#322;ada&#263;"
  ]
  node [
    id 765
    label "przekonywa&#263;"
  ]
  node [
    id 766
    label "interpretowa&#263;"
  ]
  node [
    id 767
    label "broni&#263;"
  ]
  node [
    id 768
    label "explain"
  ]
  node [
    id 769
    label "sprawowa&#263;"
  ]
  node [
    id 770
    label "uzasadnia&#263;"
  ]
  node [
    id 771
    label "zinterpretowa&#263;"
  ]
  node [
    id 772
    label "put"
  ]
  node [
    id 773
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 774
    label "przekona&#263;"
  ]
  node [
    id 775
    label "wiedzie&#263;"
  ]
  node [
    id 776
    label "kuma&#263;"
  ]
  node [
    id 777
    label "czu&#263;"
  ]
  node [
    id 778
    label "dziama&#263;"
  ]
  node [
    id 779
    label "match"
  ]
  node [
    id 780
    label "empatia"
  ]
  node [
    id 781
    label "odbiera&#263;"
  ]
  node [
    id 782
    label "see"
  ]
  node [
    id 783
    label "zna&#263;"
  ]
  node [
    id 784
    label "gaworzy&#263;"
  ]
  node [
    id 785
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 786
    label "rozmawia&#263;"
  ]
  node [
    id 787
    label "wyra&#380;a&#263;"
  ]
  node [
    id 788
    label "umie&#263;"
  ]
  node [
    id 789
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 790
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 791
    label "express"
  ]
  node [
    id 792
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 793
    label "talk"
  ]
  node [
    id 794
    label "u&#380;ywa&#263;"
  ]
  node [
    id 795
    label "prawi&#263;"
  ]
  node [
    id 796
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 797
    label "powiada&#263;"
  ]
  node [
    id 798
    label "tell"
  ]
  node [
    id 799
    label "chew_the_fat"
  ]
  node [
    id 800
    label "say"
  ]
  node [
    id 801
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 802
    label "informowa&#263;"
  ]
  node [
    id 803
    label "okre&#347;la&#263;"
  ]
  node [
    id 804
    label "hermeneutyka"
  ]
  node [
    id 805
    label "kontekst"
  ]
  node [
    id 806
    label "apprehension"
  ]
  node [
    id 807
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 808
    label "interpretation"
  ]
  node [
    id 809
    label "obja&#347;nienie"
  ]
  node [
    id 810
    label "czucie"
  ]
  node [
    id 811
    label "realization"
  ]
  node [
    id 812
    label "kumanie"
  ]
  node [
    id 813
    label "wnioskowanie"
  ]
  node [
    id 814
    label "nadawanie"
  ]
  node [
    id 815
    label "precyzowanie"
  ]
  node [
    id 816
    label "formalny"
  ]
  node [
    id 817
    label "validate"
  ]
  node [
    id 818
    label "nadawa&#263;"
  ]
  node [
    id 819
    label "precyzowa&#263;"
  ]
  node [
    id 820
    label "salt_lick"
  ]
  node [
    id 821
    label "dotyka&#263;"
  ]
  node [
    id 822
    label "muska&#263;"
  ]
  node [
    id 823
    label "wada_wrodzona"
  ]
  node [
    id 824
    label "dotykanie"
  ]
  node [
    id 825
    label "przesuwanie"
  ]
  node [
    id 826
    label "zlizanie"
  ]
  node [
    id 827
    label "g&#322;askanie"
  ]
  node [
    id 828
    label "wylizywanie"
  ]
  node [
    id 829
    label "zlizywanie"
  ]
  node [
    id 830
    label "wylizanie"
  ]
  node [
    id 831
    label "usta"
  ]
  node [
    id 832
    label "&#347;lina"
  ]
  node [
    id 833
    label "pi&#263;"
  ]
  node [
    id 834
    label "sponge"
  ]
  node [
    id 835
    label "mleko"
  ]
  node [
    id 836
    label "rozpuszcza&#263;"
  ]
  node [
    id 837
    label "wci&#261;ga&#263;"
  ]
  node [
    id 838
    label "rusza&#263;"
  ]
  node [
    id 839
    label "sucking"
  ]
  node [
    id 840
    label "smoczek"
  ]
  node [
    id 841
    label "znami&#281;"
  ]
  node [
    id 842
    label "krosta"
  ]
  node [
    id 843
    label "spot"
  ]
  node [
    id 844
    label "schorzenie"
  ]
  node [
    id 845
    label "brodawka"
  ]
  node [
    id 846
    label "pip"
  ]
  node [
    id 847
    label "picie"
  ]
  node [
    id 848
    label "ruszanie"
  ]
  node [
    id 849
    label "consumption"
  ]
  node [
    id 850
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 851
    label "rozpuszczanie"
  ]
  node [
    id 852
    label "aspiration"
  ]
  node [
    id 853
    label "wci&#261;ganie"
  ]
  node [
    id 854
    label "odci&#261;ganie"
  ]
  node [
    id 855
    label "wessanie"
  ]
  node [
    id 856
    label "ga&#378;nik"
  ]
  node [
    id 857
    label "wysysanie"
  ]
  node [
    id 858
    label "wyssanie"
  ]
  node [
    id 859
    label "Polish"
  ]
  node [
    id 860
    label "goniony"
  ]
  node [
    id 861
    label "oberek"
  ]
  node [
    id 862
    label "ryba_po_grecku"
  ]
  node [
    id 863
    label "sztajer"
  ]
  node [
    id 864
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 865
    label "krakowiak"
  ]
  node [
    id 866
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 867
    label "pierogi_ruskie"
  ]
  node [
    id 868
    label "lacki"
  ]
  node [
    id 869
    label "polak"
  ]
  node [
    id 870
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 871
    label "chodzony"
  ]
  node [
    id 872
    label "po_polsku"
  ]
  node [
    id 873
    label "mazur"
  ]
  node [
    id 874
    label "polsko"
  ]
  node [
    id 875
    label "skoczny"
  ]
  node [
    id 876
    label "drabant"
  ]
  node [
    id 877
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 878
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 879
    label "wschodnioeuropejski"
  ]
  node [
    id 880
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 881
    label "poga&#324;ski"
  ]
  node [
    id 882
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 883
    label "topielec"
  ]
  node [
    id 884
    label "europejski"
  ]
  node [
    id 885
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 886
    label "langosz"
  ]
  node [
    id 887
    label "gwardzista"
  ]
  node [
    id 888
    label "melodia"
  ]
  node [
    id 889
    label "taniec"
  ]
  node [
    id 890
    label "taniec_ludowy"
  ]
  node [
    id 891
    label "&#347;redniowieczny"
  ]
  node [
    id 892
    label "europejsko"
  ]
  node [
    id 893
    label "specjalny"
  ]
  node [
    id 894
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 895
    label "weso&#322;y"
  ]
  node [
    id 896
    label "sprawny"
  ]
  node [
    id 897
    label "rytmiczny"
  ]
  node [
    id 898
    label "skocznie"
  ]
  node [
    id 899
    label "energiczny"
  ]
  node [
    id 900
    label "przytup"
  ]
  node [
    id 901
    label "ho&#322;ubiec"
  ]
  node [
    id 902
    label "wodzi&#263;"
  ]
  node [
    id 903
    label "lendler"
  ]
  node [
    id 904
    label "austriacki"
  ]
  node [
    id 905
    label "polka"
  ]
  node [
    id 906
    label "ludowy"
  ]
  node [
    id 907
    label "pie&#347;&#324;"
  ]
  node [
    id 908
    label "mieszkaniec"
  ]
  node [
    id 909
    label "centu&#347;"
  ]
  node [
    id 910
    label "lalka"
  ]
  node [
    id 911
    label "Ma&#322;opolanin"
  ]
  node [
    id 912
    label "krakauer"
  ]
  node [
    id 913
    label "po_litewsku"
  ]
  node [
    id 914
    label "szpekucha"
  ]
  node [
    id 915
    label "ser_jab&#322;kowy"
  ]
  node [
    id 916
    label "p&#243;&#322;nocnoeuropejski"
  ]
  node [
    id 917
    label "soczewiak"
  ]
  node [
    id 918
    label "j&#281;zyk_ba&#322;tycki"
  ]
  node [
    id 919
    label "Lithuanian"
  ]
  node [
    id 920
    label "blin_&#380;mudzki"
  ]
  node [
    id 921
    label "bu&#322;eczka"
  ]
  node [
    id 922
    label "pier&#243;g"
  ]
  node [
    id 923
    label "wielkanocny"
  ]
  node [
    id 924
    label "nordycki"
  ]
  node [
    id 925
    label "po_europejsku"
  ]
  node [
    id 926
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 927
    label "European"
  ]
  node [
    id 928
    label "typowy"
  ]
  node [
    id 929
    label "charakterystyczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 3
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 322
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 316
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 225
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 223
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 238
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 248
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 240
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 723
  ]
  edge [
    source 13
    target 724
  ]
  edge [
    source 13
    target 725
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 727
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 728
  ]
  edge [
    source 13
    target 729
  ]
  edge [
    source 13
    target 730
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 13
    target 733
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 217
  ]
  edge [
    source 13
    target 735
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 737
  ]
  edge [
    source 13
    target 738
  ]
  edge [
    source 13
    target 739
  ]
  edge [
    source 13
    target 215
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 13
    target 742
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 753
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 756
  ]
  edge [
    source 13
    target 757
  ]
  edge [
    source 13
    target 758
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 760
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 762
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 763
  ]
  edge [
    source 13
    target 764
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 297
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 298
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 230
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 859
  ]
  edge [
    source 14
    target 860
  ]
  edge [
    source 14
    target 861
  ]
  edge [
    source 14
    target 862
  ]
  edge [
    source 14
    target 863
  ]
  edge [
    source 14
    target 864
  ]
  edge [
    source 14
    target 865
  ]
  edge [
    source 14
    target 866
  ]
  edge [
    source 14
    target 867
  ]
  edge [
    source 14
    target 868
  ]
  edge [
    source 14
    target 869
  ]
  edge [
    source 14
    target 870
  ]
  edge [
    source 14
    target 871
  ]
  edge [
    source 14
    target 872
  ]
  edge [
    source 14
    target 873
  ]
  edge [
    source 14
    target 874
  ]
  edge [
    source 14
    target 875
  ]
  edge [
    source 14
    target 876
  ]
  edge [
    source 14
    target 877
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 14
    target 529
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 531
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 567
  ]
  edge [
    source 14
    target 568
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 878
  ]
  edge [
    source 14
    target 879
  ]
  edge [
    source 14
    target 880
  ]
  edge [
    source 14
    target 881
  ]
  edge [
    source 14
    target 882
  ]
  edge [
    source 14
    target 883
  ]
  edge [
    source 14
    target 884
  ]
  edge [
    source 14
    target 885
  ]
  edge [
    source 14
    target 886
  ]
  edge [
    source 14
    target 612
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 615
  ]
  edge [
    source 14
    target 225
  ]
  edge [
    source 14
    target 616
  ]
  edge [
    source 14
    target 617
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 619
  ]
  edge [
    source 14
    target 620
  ]
  edge [
    source 14
    target 621
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 623
  ]
  edge [
    source 14
    target 624
  ]
  edge [
    source 14
    target 625
  ]
  edge [
    source 14
    target 626
  ]
  edge [
    source 14
    target 627
  ]
  edge [
    source 14
    target 628
  ]
  edge [
    source 14
    target 629
  ]
  edge [
    source 14
    target 630
  ]
  edge [
    source 14
    target 223
  ]
  edge [
    source 14
    target 631
  ]
  edge [
    source 14
    target 238
  ]
  edge [
    source 14
    target 632
  ]
  edge [
    source 14
    target 633
  ]
  edge [
    source 14
    target 887
  ]
  edge [
    source 14
    target 888
  ]
  edge [
    source 14
    target 889
  ]
  edge [
    source 14
    target 890
  ]
  edge [
    source 14
    target 891
  ]
  edge [
    source 14
    target 892
  ]
  edge [
    source 14
    target 893
  ]
  edge [
    source 14
    target 894
  ]
  edge [
    source 14
    target 895
  ]
  edge [
    source 14
    target 896
  ]
  edge [
    source 14
    target 897
  ]
  edge [
    source 14
    target 898
  ]
  edge [
    source 14
    target 899
  ]
  edge [
    source 14
    target 900
  ]
  edge [
    source 14
    target 901
  ]
  edge [
    source 14
    target 902
  ]
  edge [
    source 14
    target 903
  ]
  edge [
    source 14
    target 904
  ]
  edge [
    source 14
    target 905
  ]
  edge [
    source 14
    target 906
  ]
  edge [
    source 14
    target 907
  ]
  edge [
    source 14
    target 908
  ]
  edge [
    source 14
    target 909
  ]
  edge [
    source 14
    target 910
  ]
  edge [
    source 14
    target 911
  ]
  edge [
    source 14
    target 912
  ]
  edge [
    source 15
    target 913
  ]
  edge [
    source 15
    target 914
  ]
  edge [
    source 15
    target 915
  ]
  edge [
    source 15
    target 916
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 917
  ]
  edge [
    source 15
    target 918
  ]
  edge [
    source 15
    target 919
  ]
  edge [
    source 15
    target 920
  ]
  edge [
    source 15
    target 921
  ]
  edge [
    source 15
    target 922
  ]
  edge [
    source 15
    target 923
  ]
  edge [
    source 15
    target 924
  ]
  edge [
    source 15
    target 925
  ]
  edge [
    source 15
    target 926
  ]
  edge [
    source 15
    target 927
  ]
  edge [
    source 15
    target 928
  ]
  edge [
    source 15
    target 929
  ]
  edge [
    source 15
    target 892
  ]
  edge [
    source 15
    target 527
  ]
  edge [
    source 15
    target 528
  ]
  edge [
    source 15
    target 529
  ]
  edge [
    source 15
    target 530
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 531
  ]
  edge [
    source 15
    target 532
  ]
  edge [
    source 15
    target 533
  ]
  edge [
    source 15
    target 534
  ]
  edge [
    source 15
    target 535
  ]
  edge [
    source 15
    target 536
  ]
  edge [
    source 15
    target 537
  ]
  edge [
    source 15
    target 538
  ]
  edge [
    source 15
    target 539
  ]
  edge [
    source 15
    target 540
  ]
  edge [
    source 15
    target 541
  ]
  edge [
    source 15
    target 542
  ]
  edge [
    source 15
    target 543
  ]
  edge [
    source 15
    target 544
  ]
  edge [
    source 15
    target 545
  ]
  edge [
    source 15
    target 546
  ]
  edge [
    source 15
    target 547
  ]
  edge [
    source 15
    target 548
  ]
  edge [
    source 15
    target 549
  ]
  edge [
    source 15
    target 445
  ]
  edge [
    source 15
    target 550
  ]
  edge [
    source 15
    target 551
  ]
  edge [
    source 15
    target 552
  ]
  edge [
    source 15
    target 553
  ]
  edge [
    source 15
    target 554
  ]
  edge [
    source 15
    target 555
  ]
  edge [
    source 15
    target 556
  ]
  edge [
    source 15
    target 557
  ]
  edge [
    source 15
    target 558
  ]
  edge [
    source 15
    target 559
  ]
  edge [
    source 15
    target 560
  ]
  edge [
    source 15
    target 561
  ]
  edge [
    source 15
    target 562
  ]
  edge [
    source 15
    target 563
  ]
  edge [
    source 15
    target 564
  ]
  edge [
    source 15
    target 565
  ]
  edge [
    source 15
    target 566
  ]
  edge [
    source 15
    target 567
  ]
  edge [
    source 15
    target 568
  ]
  edge [
    source 15
    target 569
  ]
]
