graph [
  node [
    id 0
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "niepodobna"
    origin "text"
  ]
  node [
    id 2
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 3
    label "przejecha&#263;"
    origin "text"
  ]
  node [
    id 4
    label "saffar"
    origin "text"
  ]
  node [
    id 5
    label "cofn&#261;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "nagle"
    origin "text"
  ]
  node [
    id 8
    label "brunon"
    origin "text"
  ]
  node [
    id 9
    label "nizib"
    origin "text"
  ]
  node [
    id 10
    label "odskoczy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "bok"
    origin "text"
  ]
  node [
    id 12
    label "ahmet"
    origin "text"
  ]
  node [
    id 13
    label "van"
    origin "text"
  ]
  node [
    id 14
    label "mitten"
    origin "text"
  ]
  node [
    id 15
    label "pochwyci&#263;"
    origin "text"
  ]
  node [
    id 16
    label "odci&#261;gn&#261;&#263;"
    origin "text"
  ]
  node [
    id 17
    label "kerabana"
    origin "text"
  ]
  node [
    id 18
    label "pocztylion"
    origin "text"
  ]
  node [
    id 19
    label "pop&#281;dzi&#263;"
    origin "text"
  ]
  node [
    id 20
    label "ko&#324;"
    origin "text"
  ]
  node [
    id 21
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 22
    label "baryer&#281;"
    origin "text"
  ]
  node [
    id 23
    label "poci&#261;g"
    origin "text"
  ]
  node [
    id 24
    label "po&#347;pieszny"
    origin "text"
  ]
  node [
    id 25
    label "ten&#380;e"
    origin "text"
  ]
  node [
    id 26
    label "chwila"
    origin "text"
  ]
  node [
    id 27
    label "przelecie&#263;"
    origin "text"
  ]
  node [
    id 28
    label "spiesznie"
    origin "text"
  ]
  node [
    id 29
    label "p&#281;d"
    origin "text"
  ]
  node [
    id 30
    label "zawadzi&#263;"
    origin "text"
  ]
  node [
    id 31
    label "tylny"
    origin "text"
  ]
  node [
    id 32
    label "ko&#322;"
    origin "text"
  ]
  node [
    id 33
    label "pow&#243;z"
    origin "text"
  ]
  node [
    id 34
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 35
    label "zd&#261;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 36
    label "dostatecznie"
    origin "text"
  ]
  node [
    id 37
    label "daleko"
    origin "text"
  ]
  node [
    id 38
    label "usun&#261;&#263;"
    origin "text"
  ]
  node [
    id 39
    label "droga"
    origin "text"
  ]
  node [
    id 40
    label "skutek"
    origin "text"
  ]
  node [
    id 41
    label "uderzenie"
    origin "text"
  ]
  node [
    id 42
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 43
    label "zgruchota&#263;"
    origin "text"
  ]
  node [
    id 44
    label "pasa&#380;er"
    origin "text"
  ]
  node [
    id 45
    label "uczu&#263;"
    origin "text"
  ]
  node [
    id 46
    label "nawet"
    origin "text"
  ]
  node [
    id 47
    label "wstrz&#261;&#347;nienie"
    origin "text"
  ]
  node [
    id 48
    label "ogl&#261;da&#263;"
  ]
  node [
    id 49
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 50
    label "notice"
  ]
  node [
    id 51
    label "perceive"
  ]
  node [
    id 52
    label "punkt_widzenia"
  ]
  node [
    id 53
    label "zmale&#263;"
  ]
  node [
    id 54
    label "male&#263;"
  ]
  node [
    id 55
    label "go_steady"
  ]
  node [
    id 56
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 57
    label "postrzega&#263;"
  ]
  node [
    id 58
    label "dostrzega&#263;"
  ]
  node [
    id 59
    label "aprobowa&#263;"
  ]
  node [
    id 60
    label "spowodowa&#263;"
  ]
  node [
    id 61
    label "os&#261;dza&#263;"
  ]
  node [
    id 62
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 63
    label "spotka&#263;"
  ]
  node [
    id 64
    label "reagowa&#263;"
  ]
  node [
    id 65
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 66
    label "wzrok"
  ]
  node [
    id 67
    label "react"
  ]
  node [
    id 68
    label "answer"
  ]
  node [
    id 69
    label "odpowiada&#263;"
  ]
  node [
    id 70
    label "uczestniczy&#263;"
  ]
  node [
    id 71
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 72
    label "dochodzi&#263;"
  ]
  node [
    id 73
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 74
    label "doj&#347;&#263;"
  ]
  node [
    id 75
    label "obacza&#263;"
  ]
  node [
    id 76
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 77
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 78
    label "styka&#263;_si&#281;"
  ]
  node [
    id 79
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 80
    label "visualize"
  ]
  node [
    id 81
    label "pozna&#263;"
  ]
  node [
    id 82
    label "insert"
  ]
  node [
    id 83
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 84
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 85
    label "befall"
  ]
  node [
    id 86
    label "znale&#378;&#263;"
  ]
  node [
    id 87
    label "orientowa&#263;_si&#281;"
  ]
  node [
    id 88
    label "uznawa&#263;"
  ]
  node [
    id 89
    label "approbate"
  ]
  node [
    id 90
    label "act"
  ]
  node [
    id 91
    label "robi&#263;"
  ]
  node [
    id 92
    label "hold"
  ]
  node [
    id 93
    label "strike"
  ]
  node [
    id 94
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 95
    label "s&#261;dzi&#263;"
  ]
  node [
    id 96
    label "powodowa&#263;"
  ]
  node [
    id 97
    label "znajdowa&#263;"
  ]
  node [
    id 98
    label "traktowa&#263;"
  ]
  node [
    id 99
    label "nagradza&#263;"
  ]
  node [
    id 100
    label "sign"
  ]
  node [
    id 101
    label "forytowa&#263;"
  ]
  node [
    id 102
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 103
    label "okulista"
  ]
  node [
    id 104
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 105
    label "zmys&#322;"
  ]
  node [
    id 106
    label "oko"
  ]
  node [
    id 107
    label "widzenie"
  ]
  node [
    id 108
    label "m&#281;tnienie"
  ]
  node [
    id 109
    label "m&#281;tnie&#263;"
  ]
  node [
    id 110
    label "expression"
  ]
  node [
    id 111
    label "kontakt"
  ]
  node [
    id 112
    label "worsen"
  ]
  node [
    id 113
    label "zmniejszy&#263;_si&#281;"
  ]
  node [
    id 114
    label "reduce"
  ]
  node [
    id 115
    label "sta&#263;_si&#281;"
  ]
  node [
    id 116
    label "relax"
  ]
  node [
    id 117
    label "zmniejsza&#263;_si&#281;"
  ]
  node [
    id 118
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 119
    label "slack"
  ]
  node [
    id 120
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 121
    label "uderzy&#263;"
  ]
  node [
    id 122
    label "zmieni&#263;"
  ]
  node [
    id 123
    label "skrzywdzi&#263;"
  ]
  node [
    id 124
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 125
    label "przegapi&#263;"
  ]
  node [
    id 126
    label "utorowa&#263;"
  ]
  node [
    id 127
    label "ride"
  ]
  node [
    id 128
    label "nacisn&#261;&#263;"
  ]
  node [
    id 129
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 130
    label "przesun&#261;&#263;"
  ]
  node [
    id 131
    label "najecha&#263;"
  ]
  node [
    id 132
    label "min&#261;&#263;"
  ]
  node [
    id 133
    label "przygotowa&#263;"
  ]
  node [
    id 134
    label "prze&#380;y&#263;"
  ]
  node [
    id 135
    label "drive"
  ]
  node [
    id 136
    label "objecha&#263;"
  ]
  node [
    id 137
    label "przeby&#263;"
  ]
  node [
    id 138
    label "pojecha&#263;"
  ]
  node [
    id 139
    label "skorzysta&#263;"
  ]
  node [
    id 140
    label "napa&#347;&#263;"
  ]
  node [
    id 141
    label "odjecha&#263;"
  ]
  node [
    id 142
    label "ruszy&#263;"
  ]
  node [
    id 143
    label "uda&#263;_si&#281;"
  ]
  node [
    id 144
    label "odby&#263;"
  ]
  node [
    id 145
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 146
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 147
    label "zrobi&#263;"
  ]
  node [
    id 148
    label "come_up"
  ]
  node [
    id 149
    label "sprawi&#263;"
  ]
  node [
    id 150
    label "zyska&#263;"
  ]
  node [
    id 151
    label "change"
  ]
  node [
    id 152
    label "straci&#263;"
  ]
  node [
    id 153
    label "zast&#261;pi&#263;"
  ]
  node [
    id 154
    label "przej&#347;&#263;"
  ]
  node [
    id 155
    label "overwhelm"
  ]
  node [
    id 156
    label "zaatakowa&#263;"
  ]
  node [
    id 157
    label "traversal"
  ]
  node [
    id 158
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 159
    label "skrytykowa&#263;"
  ]
  node [
    id 160
    label "spell"
  ]
  node [
    id 161
    label "naj&#347;&#263;"
  ]
  node [
    id 162
    label "powiedzie&#263;"
  ]
  node [
    id 163
    label "zaszkodzi&#263;"
  ]
  node [
    id 164
    label "niesprawiedliwy"
  ]
  node [
    id 165
    label "hurt"
  ]
  node [
    id 166
    label "wrong"
  ]
  node [
    id 167
    label "ukrzywdzi&#263;"
  ]
  node [
    id 168
    label "przenie&#347;&#263;"
  ]
  node [
    id 169
    label "shift"
  ]
  node [
    id 170
    label "motivate"
  ]
  node [
    id 171
    label "deepen"
  ]
  node [
    id 172
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 173
    label "transfer"
  ]
  node [
    id 174
    label "dostosowa&#263;"
  ]
  node [
    id 175
    label "nak&#322;oni&#263;"
  ]
  node [
    id 176
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 177
    label "tug"
  ]
  node [
    id 178
    label "cram"
  ]
  node [
    id 179
    label "nadusi&#263;"
  ]
  node [
    id 180
    label "zach&#281;ci&#263;"
  ]
  node [
    id 181
    label "poradzi&#263;_sobie"
  ]
  node [
    id 182
    label "see"
  ]
  node [
    id 183
    label "wytrzyma&#263;"
  ]
  node [
    id 184
    label "dozna&#263;"
  ]
  node [
    id 185
    label "gapiostwo"
  ]
  node [
    id 186
    label "overlook"
  ]
  node [
    id 187
    label "przeoczy&#263;"
  ]
  node [
    id 188
    label "die"
  ]
  node [
    id 189
    label "przesta&#263;"
  ]
  node [
    id 190
    label "run"
  ]
  node [
    id 191
    label "omin&#261;&#263;"
  ]
  node [
    id 192
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 193
    label "fall"
  ]
  node [
    id 194
    label "wystartowa&#263;"
  ]
  node [
    id 195
    label "dupn&#261;&#263;"
  ]
  node [
    id 196
    label "anoint"
  ]
  node [
    id 197
    label "jebn&#261;&#263;"
  ]
  node [
    id 198
    label "zada&#263;"
  ]
  node [
    id 199
    label "postara&#263;_si&#281;"
  ]
  node [
    id 200
    label "rap"
  ]
  node [
    id 201
    label "chop"
  ]
  node [
    id 202
    label "sztachn&#261;&#263;"
  ]
  node [
    id 203
    label "przywali&#263;"
  ]
  node [
    id 204
    label "nast&#261;pi&#263;"
  ]
  node [
    id 205
    label "dotkn&#261;&#263;"
  ]
  node [
    id 206
    label "urazi&#263;"
  ]
  node [
    id 207
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 208
    label "transgress"
  ]
  node [
    id 209
    label "wstrzeli&#263;_si&#281;"
  ]
  node [
    id 210
    label "lumber"
  ]
  node [
    id 211
    label "crush"
  ]
  node [
    id 212
    label "uderzy&#263;_do_panny"
  ]
  node [
    id 213
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 214
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 215
    label "hopn&#261;&#263;"
  ]
  node [
    id 216
    label "u&#322;atwi&#263;"
  ]
  node [
    id 217
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 218
    label "odblokowa&#263;"
  ]
  node [
    id 219
    label "wykona&#263;"
  ]
  node [
    id 220
    label "wytworzy&#263;"
  ]
  node [
    id 221
    label "arrange"
  ]
  node [
    id 222
    label "set"
  ]
  node [
    id 223
    label "wyszkoli&#263;"
  ]
  node [
    id 224
    label "dress"
  ]
  node [
    id 225
    label "cook"
  ]
  node [
    id 226
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 227
    label "train"
  ]
  node [
    id 228
    label "ukierunkowa&#263;"
  ]
  node [
    id 229
    label "talerz_perkusyjny"
  ]
  node [
    id 230
    label "poodwiedza&#263;"
  ]
  node [
    id 231
    label "nabra&#263;"
  ]
  node [
    id 232
    label "osaczy&#263;"
  ]
  node [
    id 233
    label "rozrusza&#263;"
  ]
  node [
    id 234
    label "wyprowadzi&#263;"
  ]
  node [
    id 235
    label "opieprzy&#263;"
  ]
  node [
    id 236
    label "zjecha&#263;"
  ]
  node [
    id 237
    label "tour"
  ]
  node [
    id 238
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 239
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 240
    label "wyprzedzi&#263;"
  ]
  node [
    id 241
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 242
    label "retract"
  ]
  node [
    id 243
    label "przyj&#261;&#263;"
  ]
  node [
    id 244
    label "wznowi&#263;"
  ]
  node [
    id 245
    label "przekierowa&#263;"
  ]
  node [
    id 246
    label "przestawi&#263;"
  ]
  node [
    id 247
    label "recall"
  ]
  node [
    id 248
    label "oddali&#263;"
  ]
  node [
    id 249
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 250
    label "retreat"
  ]
  node [
    id 251
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 252
    label "absorb"
  ]
  node [
    id 253
    label "swallow"
  ]
  node [
    id 254
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 255
    label "przybra&#263;"
  ]
  node [
    id 256
    label "odebra&#263;"
  ]
  node [
    id 257
    label "undertake"
  ]
  node [
    id 258
    label "umie&#347;ci&#263;"
  ]
  node [
    id 259
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 260
    label "draw"
  ]
  node [
    id 261
    label "obra&#263;"
  ]
  node [
    id 262
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 263
    label "dostarczy&#263;"
  ]
  node [
    id 264
    label "receive"
  ]
  node [
    id 265
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 266
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 267
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 268
    label "uzna&#263;"
  ]
  node [
    id 269
    label "przyj&#281;cie"
  ]
  node [
    id 270
    label "wzi&#261;&#263;"
  ]
  node [
    id 271
    label "oddalenie"
  ]
  node [
    id 272
    label "odw&#322;&#243;czy&#263;"
  ]
  node [
    id 273
    label "odprawi&#263;"
  ]
  node [
    id 274
    label "remove"
  ]
  node [
    id 275
    label "decelerate"
  ]
  node [
    id 276
    label "oddalanie"
  ]
  node [
    id 277
    label "oddala&#263;"
  ]
  node [
    id 278
    label "pokaza&#263;"
  ]
  node [
    id 279
    label "od&#322;o&#380;y&#263;"
  ]
  node [
    id 280
    label "odrzuci&#263;"
  ]
  node [
    id 281
    label "nakaza&#263;"
  ]
  node [
    id 282
    label "withdraw"
  ]
  node [
    id 283
    label "bow_out"
  ]
  node [
    id 284
    label "suspend"
  ]
  node [
    id 285
    label "sum_up"
  ]
  node [
    id 286
    label "postawi&#263;"
  ]
  node [
    id 287
    label "nastawi&#263;"
  ]
  node [
    id 288
    label "uk&#322;ad"
  ]
  node [
    id 289
    label "counterchange"
  ]
  node [
    id 290
    label "przebudowa&#263;"
  ]
  node [
    id 291
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 292
    label "skierowa&#263;"
  ]
  node [
    id 293
    label "doprowadzi&#263;"
  ]
  node [
    id 294
    label "go"
  ]
  node [
    id 295
    label "travel"
  ]
  node [
    id 296
    label "kill"
  ]
  node [
    id 297
    label "zerwa&#263;"
  ]
  node [
    id 298
    label "raptowny"
  ]
  node [
    id 299
    label "nieprzewidzianie"
  ]
  node [
    id 300
    label "szybko"
  ]
  node [
    id 301
    label "niespodziewanie"
  ]
  node [
    id 302
    label "nieoczekiwany"
  ]
  node [
    id 303
    label "sprawnie"
  ]
  node [
    id 304
    label "dynamicznie"
  ]
  node [
    id 305
    label "szybciochem"
  ]
  node [
    id 306
    label "szybciej"
  ]
  node [
    id 307
    label "bezpo&#347;rednio"
  ]
  node [
    id 308
    label "quicker"
  ]
  node [
    id 309
    label "quickest"
  ]
  node [
    id 310
    label "prosto"
  ]
  node [
    id 311
    label "promptly"
  ]
  node [
    id 312
    label "szybki"
  ]
  node [
    id 313
    label "raptownie"
  ]
  node [
    id 314
    label "zawrzenie"
  ]
  node [
    id 315
    label "gwa&#322;towny"
  ]
  node [
    id 316
    label "zawrze&#263;"
  ]
  node [
    id 317
    label "recoil"
  ]
  node [
    id 318
    label "otworzy&#263;_si&#281;"
  ]
  node [
    id 319
    label "skoczy&#263;"
  ]
  node [
    id 320
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 321
    label "parachute"
  ]
  node [
    id 322
    label "odcinek"
  ]
  node [
    id 323
    label "&#347;ciana"
  ]
  node [
    id 324
    label "strzelba"
  ]
  node [
    id 325
    label "wielok&#261;t"
  ]
  node [
    id 326
    label "tu&#322;&#243;w"
  ]
  node [
    id 327
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 328
    label "strona"
  ]
  node [
    id 329
    label "lufa"
  ]
  node [
    id 330
    label "kierunek"
  ]
  node [
    id 331
    label "wyrobisko"
  ]
  node [
    id 332
    label "trudno&#347;&#263;"
  ]
  node [
    id 333
    label "kres"
  ]
  node [
    id 334
    label "bariera"
  ]
  node [
    id 335
    label "przegroda"
  ]
  node [
    id 336
    label "p&#322;aszczyzna"
  ]
  node [
    id 337
    label "profil"
  ]
  node [
    id 338
    label "facebook"
  ]
  node [
    id 339
    label "zbocze"
  ]
  node [
    id 340
    label "miejsce"
  ]
  node [
    id 341
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 342
    label "obstruction"
  ]
  node [
    id 343
    label "kszta&#322;t"
  ]
  node [
    id 344
    label "pow&#322;oka"
  ]
  node [
    id 345
    label "wielo&#347;cian"
  ]
  node [
    id 346
    label "linia"
  ]
  node [
    id 347
    label "zorientowa&#263;"
  ]
  node [
    id 348
    label "orientowa&#263;"
  ]
  node [
    id 349
    label "fragment"
  ]
  node [
    id 350
    label "skr&#281;cenie"
  ]
  node [
    id 351
    label "skr&#281;ci&#263;"
  ]
  node [
    id 352
    label "internet"
  ]
  node [
    id 353
    label "obiekt"
  ]
  node [
    id 354
    label "g&#243;ra"
  ]
  node [
    id 355
    label "orientowanie"
  ]
  node [
    id 356
    label "zorientowanie"
  ]
  node [
    id 357
    label "forma"
  ]
  node [
    id 358
    label "podmiot"
  ]
  node [
    id 359
    label "ty&#322;"
  ]
  node [
    id 360
    label "logowanie"
  ]
  node [
    id 361
    label "voice"
  ]
  node [
    id 362
    label "kartka"
  ]
  node [
    id 363
    label "layout"
  ]
  node [
    id 364
    label "powierzchnia"
  ]
  node [
    id 365
    label "posta&#263;"
  ]
  node [
    id 366
    label "skr&#281;canie"
  ]
  node [
    id 367
    label "orientacja"
  ]
  node [
    id 368
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 369
    label "pagina"
  ]
  node [
    id 370
    label "uj&#281;cie"
  ]
  node [
    id 371
    label "serwis_internetowy"
  ]
  node [
    id 372
    label "adres_internetowy"
  ]
  node [
    id 373
    label "prz&#243;d"
  ]
  node [
    id 374
    label "s&#261;d"
  ]
  node [
    id 375
    label "skr&#281;ca&#263;"
  ]
  node [
    id 376
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 377
    label "plik"
  ]
  node [
    id 378
    label "pole"
  ]
  node [
    id 379
    label "part"
  ]
  node [
    id 380
    label "line"
  ]
  node [
    id 381
    label "kawa&#322;ek"
  ]
  node [
    id 382
    label "teren"
  ]
  node [
    id 383
    label "coupon"
  ]
  node [
    id 384
    label "epizod"
  ]
  node [
    id 385
    label "moneta"
  ]
  node [
    id 386
    label "pokwitowanie"
  ]
  node [
    id 387
    label "bro&#324;_palna"
  ]
  node [
    id 388
    label "kolba"
  ]
  node [
    id 389
    label "bro&#324;"
  ]
  node [
    id 390
    label "shotgun"
  ]
  node [
    id 391
    label "bro&#324;_strzelecka"
  ]
  node [
    id 392
    label "gun"
  ]
  node [
    id 393
    label "przebieg"
  ]
  node [
    id 394
    label "praktyka"
  ]
  node [
    id 395
    label "przeorientowanie"
  ]
  node [
    id 396
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 397
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 398
    label "przeorientowywanie"
  ]
  node [
    id 399
    label "ideologia"
  ]
  node [
    id 400
    label "metoda"
  ]
  node [
    id 401
    label "studia"
  ]
  node [
    id 402
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 403
    label "przeorientowa&#263;"
  ]
  node [
    id 404
    label "bearing"
  ]
  node [
    id 405
    label "spos&#243;b"
  ]
  node [
    id 406
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 407
    label "system"
  ]
  node [
    id 408
    label "przeorientowywa&#263;"
  ]
  node [
    id 409
    label "kieliszek"
  ]
  node [
    id 410
    label "komora_nabojowa"
  ]
  node [
    id 411
    label "ustnik"
  ]
  node [
    id 412
    label "urz&#261;dzenie_wylotowe"
  ]
  node [
    id 413
    label "w&#243;dka"
  ]
  node [
    id 414
    label "niedostateczny"
  ]
  node [
    id 415
    label "rurarnia"
  ]
  node [
    id 416
    label "rura"
  ]
  node [
    id 417
    label "shot"
  ]
  node [
    id 418
    label "nadlufka"
  ]
  node [
    id 419
    label "dulawka"
  ]
  node [
    id 420
    label "polygon"
  ]
  node [
    id 421
    label "figura_p&#322;aska"
  ]
  node [
    id 422
    label "k&#261;t"
  ]
  node [
    id 423
    label "krocze"
  ]
  node [
    id 424
    label "pupa"
  ]
  node [
    id 425
    label "biodro"
  ]
  node [
    id 426
    label "dekolt"
  ]
  node [
    id 427
    label "pacha"
  ]
  node [
    id 428
    label "struktura_anatomiczna"
  ]
  node [
    id 429
    label "pachwina"
  ]
  node [
    id 430
    label "zad"
  ]
  node [
    id 431
    label "plecy"
  ]
  node [
    id 432
    label "klatka_piersiowa"
  ]
  node [
    id 433
    label "stawon&#243;g"
  ]
  node [
    id 434
    label "body"
  ]
  node [
    id 435
    label "brzuch"
  ]
  node [
    id 436
    label "pier&#347;"
  ]
  node [
    id 437
    label "samoch&#243;d"
  ]
  node [
    id 438
    label "nadwozie"
  ]
  node [
    id 439
    label "reflektor"
  ]
  node [
    id 440
    label "karoseria"
  ]
  node [
    id 441
    label "obudowa"
  ]
  node [
    id 442
    label "pr&#243;g"
  ]
  node [
    id 443
    label "zderzak"
  ]
  node [
    id 444
    label "dach"
  ]
  node [
    id 445
    label "b&#322;otnik"
  ]
  node [
    id 446
    label "pojazd"
  ]
  node [
    id 447
    label "spoiler"
  ]
  node [
    id 448
    label "buda"
  ]
  node [
    id 449
    label "poduszka_powietrzna"
  ]
  node [
    id 450
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 451
    label "pompa_wodna"
  ]
  node [
    id 452
    label "bak"
  ]
  node [
    id 453
    label "deska_rozdzielcza"
  ]
  node [
    id 454
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 455
    label "spryskiwacz"
  ]
  node [
    id 456
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 457
    label "baga&#380;nik"
  ]
  node [
    id 458
    label "poci&#261;g_drogowy"
  ]
  node [
    id 459
    label "immobilizer"
  ]
  node [
    id 460
    label "kierownica"
  ]
  node [
    id 461
    label "ABS"
  ]
  node [
    id 462
    label "dwu&#347;lad"
  ]
  node [
    id 463
    label "tempomat"
  ]
  node [
    id 464
    label "pojazd_drogowy"
  ]
  node [
    id 465
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 466
    label "wycieraczka"
  ]
  node [
    id 467
    label "most"
  ]
  node [
    id 468
    label "silnik"
  ]
  node [
    id 469
    label "t&#322;umik"
  ]
  node [
    id 470
    label "dachowanie"
  ]
  node [
    id 471
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 472
    label "ensnare"
  ]
  node [
    id 473
    label "zacz&#261;&#263;"
  ]
  node [
    id 474
    label "do"
  ]
  node [
    id 475
    label "dorwa&#263;"
  ]
  node [
    id 476
    label "capture"
  ]
  node [
    id 477
    label "chwyci&#263;"
  ]
  node [
    id 478
    label "skuma&#263;"
  ]
  node [
    id 479
    label "przeszkodzi&#263;"
  ]
  node [
    id 480
    label "porwa&#263;"
  ]
  node [
    id 481
    label "ogarn&#261;&#263;"
  ]
  node [
    id 482
    label "attack"
  ]
  node [
    id 483
    label "zrozumie&#263;"
  ]
  node [
    id 484
    label "fascinate"
  ]
  node [
    id 485
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 486
    label "odj&#261;&#263;"
  ]
  node [
    id 487
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 488
    label "introduce"
  ]
  node [
    id 489
    label "post&#261;pi&#263;"
  ]
  node [
    id 490
    label "cause"
  ]
  node [
    id 491
    label "begin"
  ]
  node [
    id 492
    label "zaliczy&#263;"
  ]
  node [
    id 493
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 494
    label "wybuchn&#261;&#263;"
  ]
  node [
    id 495
    label "z&#322;apa&#263;"
  ]
  node [
    id 496
    label "spa&#347;&#263;"
  ]
  node [
    id 497
    label "ut"
  ]
  node [
    id 498
    label "C"
  ]
  node [
    id 499
    label "d&#378;wi&#281;k"
  ]
  node [
    id 500
    label "his"
  ]
  node [
    id 501
    label "powstrzyma&#263;"
  ]
  node [
    id 502
    label "perpetrate"
  ]
  node [
    id 503
    label "zniech&#281;ci&#263;"
  ]
  node [
    id 504
    label "op&#243;&#378;ni&#263;"
  ]
  node [
    id 505
    label "pull"
  ]
  node [
    id 506
    label "oddzieli&#263;"
  ]
  node [
    id 507
    label "naci&#261;gn&#261;&#263;"
  ]
  node [
    id 508
    label "zreflektowa&#263;"
  ]
  node [
    id 509
    label "zaczepi&#263;"
  ]
  node [
    id 510
    label "continue"
  ]
  node [
    id 511
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 512
    label "anticipate"
  ]
  node [
    id 513
    label "zwerbowa&#263;"
  ]
  node [
    id 514
    label "sk&#322;oni&#263;"
  ]
  node [
    id 515
    label "uszkodzi&#263;"
  ]
  node [
    id 516
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 517
    label "napr&#281;&#380;y&#263;"
  ]
  node [
    id 518
    label "string"
  ]
  node [
    id 519
    label "flex"
  ]
  node [
    id 520
    label "wy&#322;udzi&#263;"
  ]
  node [
    id 521
    label "unfold"
  ]
  node [
    id 522
    label "bend"
  ]
  node [
    id 523
    label "frighten"
  ]
  node [
    id 524
    label "wzbudzi&#263;"
  ]
  node [
    id 525
    label "undo"
  ]
  node [
    id 526
    label "wyrugowa&#263;"
  ]
  node [
    id 527
    label "zabi&#263;"
  ]
  node [
    id 528
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 529
    label "divide"
  ]
  node [
    id 530
    label "detach"
  ]
  node [
    id 531
    label "odseparowa&#263;"
  ]
  node [
    id 532
    label "podzieli&#263;"
  ]
  node [
    id 533
    label "furman"
  ]
  node [
    id 534
    label "listonosz"
  ]
  node [
    id 535
    label "pocztowiec"
  ]
  node [
    id 536
    label "dor&#281;czyciel"
  ]
  node [
    id 537
    label "kuczer"
  ]
  node [
    id 538
    label "powo&#378;nik"
  ]
  node [
    id 539
    label "przewo&#378;nik"
  ]
  node [
    id 540
    label "kozio&#322;"
  ]
  node [
    id 541
    label "znagli&#263;"
  ]
  node [
    id 542
    label "rush"
  ]
  node [
    id 543
    label "zmusi&#263;"
  ]
  node [
    id 544
    label "induce"
  ]
  node [
    id 545
    label "pop&#281;dzi&#263;_kota"
  ]
  node [
    id 546
    label "sandbag"
  ]
  node [
    id 547
    label "zada&#263;_gwa&#322;t"
  ]
  node [
    id 548
    label "force"
  ]
  node [
    id 549
    label "pospieszy&#263;"
  ]
  node [
    id 550
    label "lansada"
  ]
  node [
    id 551
    label "przegalopowanie"
  ]
  node [
    id 552
    label "zebroid"
  ]
  node [
    id 553
    label "galopowa&#263;"
  ]
  node [
    id 554
    label "pogalopowanie"
  ]
  node [
    id 555
    label "koniowate"
  ]
  node [
    id 556
    label "zar&#380;e&#263;"
  ]
  node [
    id 557
    label "karmiak"
  ]
  node [
    id 558
    label "pogalopowa&#263;"
  ]
  node [
    id 559
    label "osadzi&#263;_si&#281;"
  ]
  node [
    id 560
    label "czo&#322;dar"
  ]
  node [
    id 561
    label "zaci&#261;&#263;"
  ]
  node [
    id 562
    label "galopowanie"
  ]
  node [
    id 563
    label "zaci&#281;cie"
  ]
  node [
    id 564
    label "remuda"
  ]
  node [
    id 565
    label "k&#322;usowa&#263;"
  ]
  node [
    id 566
    label "podkuwa&#263;"
  ]
  node [
    id 567
    label "znarowienie"
  ]
  node [
    id 568
    label "osadzanie_si&#281;"
  ]
  node [
    id 569
    label "znarowi&#263;"
  ]
  node [
    id 570
    label "przegalopowa&#263;"
  ]
  node [
    id 571
    label "penis"
  ]
  node [
    id 572
    label "pok&#322;usowa&#263;"
  ]
  node [
    id 573
    label "osadzenie_si&#281;"
  ]
  node [
    id 574
    label "narowi&#263;"
  ]
  node [
    id 575
    label "r&#380;e&#263;"
  ]
  node [
    id 576
    label "zebrula"
  ]
  node [
    id 577
    label "os&#322;omu&#322;"
  ]
  node [
    id 578
    label "hipoterapeuta"
  ]
  node [
    id 579
    label "kawalerzysta"
  ]
  node [
    id 580
    label "dosiad"
  ]
  node [
    id 581
    label "zwierz&#281;_wierzchowe"
  ]
  node [
    id 582
    label "uje&#380;d&#380;anie"
  ]
  node [
    id 583
    label "figura"
  ]
  node [
    id 584
    label "podkuwanie"
  ]
  node [
    id 585
    label "hipoterapia"
  ]
  node [
    id 586
    label "osadza&#263;_si&#281;"
  ]
  node [
    id 587
    label "ko&#324;_dziki"
  ]
  node [
    id 588
    label "pok&#322;usowanie"
  ]
  node [
    id 589
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 590
    label "k&#322;usowanie"
  ]
  node [
    id 591
    label "narowienie"
  ]
  node [
    id 592
    label "r&#380;enie"
  ]
  node [
    id 593
    label "&#322;ykawo&#347;&#263;"
  ]
  node [
    id 594
    label "nar&#243;w"
  ]
  node [
    id 595
    label "stado"
  ]
  node [
    id 596
    label "obiekt_matematyczny"
  ]
  node [
    id 597
    label "gestaltyzm"
  ]
  node [
    id 598
    label "ornamentyka"
  ]
  node [
    id 599
    label "stylistyka"
  ]
  node [
    id 600
    label "podzbi&#243;r"
  ]
  node [
    id 601
    label "Osjan"
  ]
  node [
    id 602
    label "sztuka"
  ]
  node [
    id 603
    label "point"
  ]
  node [
    id 604
    label "kto&#347;"
  ]
  node [
    id 605
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 606
    label "styl"
  ]
  node [
    id 607
    label "antycypacja"
  ]
  node [
    id 608
    label "przedmiot"
  ]
  node [
    id 609
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 610
    label "wiersz"
  ]
  node [
    id 611
    label "cz&#322;owiek"
  ]
  node [
    id 612
    label "facet"
  ]
  node [
    id 613
    label "popis"
  ]
  node [
    id 614
    label "Aspazja"
  ]
  node [
    id 615
    label "przedstawienie"
  ]
  node [
    id 616
    label "cecha"
  ]
  node [
    id 617
    label "obraz"
  ]
  node [
    id 618
    label "informacja"
  ]
  node [
    id 619
    label "symetria"
  ]
  node [
    id 620
    label "budowa"
  ]
  node [
    id 621
    label "figure"
  ]
  node [
    id 622
    label "rzecz"
  ]
  node [
    id 623
    label "charakterystyka"
  ]
  node [
    id 624
    label "perspektywa"
  ]
  node [
    id 625
    label "lingwistyka_kognitywna"
  ]
  node [
    id 626
    label "character"
  ]
  node [
    id 627
    label "wygl&#261;d"
  ]
  node [
    id 628
    label "rze&#378;ba"
  ]
  node [
    id 629
    label "kompleksja"
  ]
  node [
    id 630
    label "shape"
  ]
  node [
    id 631
    label "wytw&#243;r"
  ]
  node [
    id 632
    label "bierka_szachowa"
  ]
  node [
    id 633
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 634
    label "karta"
  ]
  node [
    id 635
    label "koniokszta&#322;tne"
  ]
  node [
    id 636
    label "jog"
  ]
  node [
    id 637
    label "poje&#378;dzi&#263;"
  ]
  node [
    id 638
    label "pobiec"
  ]
  node [
    id 639
    label "ssak_nieparzystokopytny"
  ]
  node [
    id 640
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 641
    label "zebra"
  ]
  node [
    id 642
    label "hinny"
  ]
  node [
    id 643
    label "osio&#322;"
  ]
  node [
    id 644
    label "nawyk"
  ]
  node [
    id 645
    label "wada"
  ]
  node [
    id 646
    label "up&#243;r"
  ]
  node [
    id 647
    label "gallop"
  ]
  node [
    id 648
    label "biegni&#281;cie"
  ]
  node [
    id 649
    label "gnanie"
  ]
  node [
    id 650
    label "polowanie"
  ]
  node [
    id 651
    label "jechanie"
  ]
  node [
    id 652
    label "jecha&#263;_konno"
  ]
  node [
    id 653
    label "biec"
  ]
  node [
    id 654
    label "amble"
  ]
  node [
    id 655
    label "polowa&#263;"
  ]
  node [
    id 656
    label "p&#281;dzi&#263;"
  ]
  node [
    id 657
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 658
    label "&#347;mia&#263;_si&#281;"
  ]
  node [
    id 659
    label "snicker"
  ]
  node [
    id 660
    label "schorzenie"
  ]
  node [
    id 661
    label "fizjoterapeuta"
  ]
  node [
    id 662
    label "psychoterapeuta"
  ]
  node [
    id 663
    label "score"
  ]
  node [
    id 664
    label "przebiec"
  ]
  node [
    id 665
    label "przybijanie"
  ]
  node [
    id 666
    label "zabezpieczanie"
  ]
  node [
    id 667
    label "umacnianie"
  ]
  node [
    id 668
    label "pojechanie"
  ]
  node [
    id 669
    label "recommendation"
  ]
  node [
    id 670
    label "pognanie"
  ]
  node [
    id 671
    label "pobiegni&#281;cie"
  ]
  node [
    id 672
    label "po&#322;ykanie"
  ]
  node [
    id 673
    label "streszczanie_si&#281;"
  ]
  node [
    id 674
    label "nast&#281;powanie"
  ]
  node [
    id 675
    label "robienie"
  ]
  node [
    id 676
    label "pogorszenie"
  ]
  node [
    id 677
    label "spowodowanie"
  ]
  node [
    id 678
    label "samowolny"
  ]
  node [
    id 679
    label "narowisty"
  ]
  node [
    id 680
    label "przejechanie"
  ]
  node [
    id 681
    label "przebiegni&#281;cie"
  ]
  node [
    id 682
    label "powodowanie"
  ]
  node [
    id 683
    label "pogarszanie"
  ]
  node [
    id 684
    label "pogna&#263;"
  ]
  node [
    id 685
    label "ch&#322;o&#347;ni&#281;cie"
  ]
  node [
    id 686
    label "stanowczo"
  ]
  node [
    id 687
    label "ostruganie"
  ]
  node [
    id 688
    label "mina"
  ]
  node [
    id 689
    label "zranienie"
  ]
  node [
    id 690
    label "nieust&#281;pliwie"
  ]
  node [
    id 691
    label "uwa&#380;nie"
  ]
  node [
    id 692
    label "formacja_skalna"
  ]
  node [
    id 693
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 694
    label "dash"
  ]
  node [
    id 695
    label "poderwanie"
  ]
  node [
    id 696
    label "talent"
  ]
  node [
    id 697
    label "turn"
  ]
  node [
    id 698
    label "capability"
  ]
  node [
    id 699
    label "w&#281;dka"
  ]
  node [
    id 700
    label "powo&#380;enie"
  ]
  node [
    id 701
    label "lejce"
  ]
  node [
    id 702
    label "potkni&#281;cie"
  ]
  node [
    id 703
    label "w&#281;dkowanie"
  ]
  node [
    id 704
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 705
    label "bat"
  ]
  node [
    id 706
    label "zaci&#281;ty"
  ]
  node [
    id 707
    label "&#347;ci&#347;ni&#281;cie"
  ]
  node [
    id 708
    label "naci&#281;cie"
  ]
  node [
    id 709
    label "usta"
  ]
  node [
    id 710
    label "&#347;mianie_si&#281;"
  ]
  node [
    id 711
    label "wydawanie"
  ]
  node [
    id 712
    label "je&#378;dziectwo"
  ]
  node [
    id 713
    label "ubijanie"
  ]
  node [
    id 714
    label "przyzwyczajanie"
  ]
  node [
    id 715
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 716
    label "siod&#322;o_uje&#380;d&#380;eniowe"
  ]
  node [
    id 717
    label "pogorszy&#263;"
  ]
  node [
    id 718
    label "pogarsza&#263;"
  ]
  node [
    id 719
    label "nast&#281;powa&#263;"
  ]
  node [
    id 720
    label "canter"
  ]
  node [
    id 721
    label "spieszy&#263;_si&#281;"
  ]
  node [
    id 722
    label "&#380;o&#322;nierz"
  ]
  node [
    id 723
    label "jezdny"
  ]
  node [
    id 724
    label "jazda"
  ]
  node [
    id 725
    label "skok"
  ]
  node [
    id 726
    label "zooterapia"
  ]
  node [
    id 727
    label "umacnia&#263;"
  ]
  node [
    id 728
    label "zabezpiecza&#263;"
  ]
  node [
    id 729
    label "shoe"
  ]
  node [
    id 730
    label "worek"
  ]
  node [
    id 731
    label "pozycja"
  ]
  node [
    id 732
    label "sport_walki"
  ]
  node [
    id 733
    label "za&#347;mia&#263;_si&#281;"
  ]
  node [
    id 734
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 735
    label "neigh"
  ]
  node [
    id 736
    label "os&#322;ona"
  ]
  node [
    id 737
    label "przykrycie"
  ]
  node [
    id 738
    label "okrycie"
  ]
  node [
    id 739
    label "ch&#322;osn&#261;&#263;"
  ]
  node [
    id 740
    label "&#347;cisn&#261;&#263;"
  ]
  node [
    id 741
    label "naci&#261;&#263;"
  ]
  node [
    id 742
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 743
    label "zablokowa&#263;"
  ]
  node [
    id 744
    label "zepsu&#263;"
  ]
  node [
    id 745
    label "nadci&#261;&#263;"
  ]
  node [
    id 746
    label "poderwa&#263;"
  ]
  node [
    id 747
    label "zrani&#263;"
  ]
  node [
    id 748
    label "ostruga&#263;"
  ]
  node [
    id 749
    label "przerwa&#263;"
  ]
  node [
    id 750
    label "cut"
  ]
  node [
    id 751
    label "wprawi&#263;"
  ]
  node [
    id 752
    label "write_out"
  ]
  node [
    id 753
    label "ptaszek"
  ]
  node [
    id 754
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 755
    label "przyrodzenie"
  ]
  node [
    id 756
    label "fiut"
  ]
  node [
    id 757
    label "shaft"
  ]
  node [
    id 758
    label "kcie&#263;"
  ]
  node [
    id 759
    label "czu&#263;"
  ]
  node [
    id 760
    label "desire"
  ]
  node [
    id 761
    label "uczuwa&#263;"
  ]
  node [
    id 762
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 763
    label "smell"
  ]
  node [
    id 764
    label "doznawa&#263;"
  ]
  node [
    id 765
    label "przewidywa&#263;"
  ]
  node [
    id 766
    label "by&#263;"
  ]
  node [
    id 767
    label "spirit"
  ]
  node [
    id 768
    label "cug"
  ]
  node [
    id 769
    label "tender"
  ]
  node [
    id 770
    label "kolej"
  ]
  node [
    id 771
    label "lokomotywa"
  ]
  node [
    id 772
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 773
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 774
    label "wagon"
  ]
  node [
    id 775
    label "pojazd_kolejowy"
  ]
  node [
    id 776
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 777
    label "harmonijka"
  ]
  node [
    id 778
    label "klasa"
  ]
  node [
    id 779
    label "czo&#322;ownica"
  ]
  node [
    id 780
    label "tramwaj"
  ]
  node [
    id 781
    label "karton"
  ]
  node [
    id 782
    label "statek"
  ]
  node [
    id 783
    label "parow&#243;z_tendrowy"
  ]
  node [
    id 784
    label "okr&#281;t"
  ]
  node [
    id 785
    label "pojazd_trakcyjny"
  ]
  node [
    id 786
    label "ciuchcia"
  ]
  node [
    id 787
    label "&#347;l&#261;ski"
  ]
  node [
    id 788
    label "stan"
  ]
  node [
    id 789
    label "draft"
  ]
  node [
    id 790
    label "ci&#261;g"
  ]
  node [
    id 791
    label "para"
  ]
  node [
    id 792
    label "zaprz&#281;g"
  ]
  node [
    id 793
    label "pr&#261;d"
  ]
  node [
    id 794
    label "cedu&#322;a"
  ]
  node [
    id 795
    label "tor"
  ]
  node [
    id 796
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 797
    label "kolejno&#347;&#263;"
  ]
  node [
    id 798
    label "blokada"
  ]
  node [
    id 799
    label "pocz&#261;tek"
  ]
  node [
    id 800
    label "trakcja"
  ]
  node [
    id 801
    label "nast&#281;pstwo"
  ]
  node [
    id 802
    label "proces"
  ]
  node [
    id 803
    label "czas"
  ]
  node [
    id 804
    label "po&#347;piesznie"
  ]
  node [
    id 805
    label "bystrolotny"
  ]
  node [
    id 806
    label "dynamiczny"
  ]
  node [
    id 807
    label "prosty"
  ]
  node [
    id 808
    label "bezpo&#347;redni"
  ]
  node [
    id 809
    label "sprawny"
  ]
  node [
    id 810
    label "temperamentny"
  ]
  node [
    id 811
    label "intensywny"
  ]
  node [
    id 812
    label "energiczny"
  ]
  node [
    id 813
    label "kr&#243;tki"
  ]
  node [
    id 814
    label "headlong"
  ]
  node [
    id 815
    label "hurriedly"
  ]
  node [
    id 816
    label "pospieszno"
  ]
  node [
    id 817
    label "ten"
  ]
  node [
    id 818
    label "okre&#347;lony"
  ]
  node [
    id 819
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 820
    label "time"
  ]
  node [
    id 821
    label "chronometria"
  ]
  node [
    id 822
    label "odczyt"
  ]
  node [
    id 823
    label "laba"
  ]
  node [
    id 824
    label "czasoprzestrze&#324;"
  ]
  node [
    id 825
    label "time_period"
  ]
  node [
    id 826
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 827
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 828
    label "Zeitgeist"
  ]
  node [
    id 829
    label "pochodzenie"
  ]
  node [
    id 830
    label "przep&#322;ywanie"
  ]
  node [
    id 831
    label "schy&#322;ek"
  ]
  node [
    id 832
    label "czwarty_wymiar"
  ]
  node [
    id 833
    label "kategoria_gramatyczna"
  ]
  node [
    id 834
    label "poprzedzi&#263;"
  ]
  node [
    id 835
    label "pogoda"
  ]
  node [
    id 836
    label "czasokres"
  ]
  node [
    id 837
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 838
    label "poprzedzenie"
  ]
  node [
    id 839
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 840
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 841
    label "dzieje"
  ]
  node [
    id 842
    label "zegar"
  ]
  node [
    id 843
    label "koniugacja"
  ]
  node [
    id 844
    label "trawi&#263;"
  ]
  node [
    id 845
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 846
    label "poprzedza&#263;"
  ]
  node [
    id 847
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 848
    label "trawienie"
  ]
  node [
    id 849
    label "rachuba_czasu"
  ]
  node [
    id 850
    label "poprzedzanie"
  ]
  node [
    id 851
    label "okres_czasu"
  ]
  node [
    id 852
    label "period"
  ]
  node [
    id 853
    label "odwlekanie_si&#281;"
  ]
  node [
    id 854
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 855
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 856
    label "pochodzi&#263;"
  ]
  node [
    id 857
    label "mark"
  ]
  node [
    id 858
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 859
    label "drift"
  ]
  node [
    id 860
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 861
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 862
    label "pada&#263;"
  ]
  node [
    id 863
    label "popada&#263;"
  ]
  node [
    id 864
    label "proceed"
  ]
  node [
    id 865
    label "fly"
  ]
  node [
    id 866
    label "wydarzenie"
  ]
  node [
    id 867
    label "przemierzy&#263;"
  ]
  node [
    id 868
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 869
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 870
    label "oversight"
  ]
  node [
    id 871
    label "slide"
  ]
  node [
    id 872
    label "sport_motorowy"
  ]
  node [
    id 873
    label "przestawa&#263;"
  ]
  node [
    id 874
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 875
    label "przypada&#263;"
  ]
  node [
    id 876
    label "spada&#263;"
  ]
  node [
    id 877
    label "czu&#263;_si&#281;"
  ]
  node [
    id 878
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 879
    label "gin&#261;&#263;"
  ]
  node [
    id 880
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 881
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 882
    label "zdycha&#263;"
  ]
  node [
    id 883
    label "mie&#263;_miejsce"
  ]
  node [
    id 884
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 885
    label "spieszny"
  ]
  node [
    id 886
    label "ro&#347;lina"
  ]
  node [
    id 887
    label "d&#261;&#380;enie"
  ]
  node [
    id 888
    label "kormus"
  ]
  node [
    id 889
    label "organ_ro&#347;linny"
  ]
  node [
    id 890
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 891
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 892
    label "rozp&#281;d"
  ]
  node [
    id 893
    label "ruch"
  ]
  node [
    id 894
    label "wyci&#261;ganie"
  ]
  node [
    id 895
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 896
    label "zrzez"
  ]
  node [
    id 897
    label "k&#322;&#261;b"
  ]
  node [
    id 898
    label "ok&#243;&#322;ek"
  ]
  node [
    id 899
    label "move"
  ]
  node [
    id 900
    label "zmiana"
  ]
  node [
    id 901
    label "model"
  ]
  node [
    id 902
    label "aktywno&#347;&#263;"
  ]
  node [
    id 903
    label "utrzymywanie"
  ]
  node [
    id 904
    label "utrzymywa&#263;"
  ]
  node [
    id 905
    label "taktyka"
  ]
  node [
    id 906
    label "d&#322;ugi"
  ]
  node [
    id 907
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 908
    label "natural_process"
  ]
  node [
    id 909
    label "kanciasty"
  ]
  node [
    id 910
    label "utrzyma&#263;"
  ]
  node [
    id 911
    label "myk"
  ]
  node [
    id 912
    label "manewr"
  ]
  node [
    id 913
    label "utrzymanie"
  ]
  node [
    id 914
    label "tumult"
  ]
  node [
    id 915
    label "stopek"
  ]
  node [
    id 916
    label "movement"
  ]
  node [
    id 917
    label "strumie&#324;"
  ]
  node [
    id 918
    label "czynno&#347;&#263;"
  ]
  node [
    id 919
    label "komunikacja"
  ]
  node [
    id 920
    label "lokomocja"
  ]
  node [
    id 921
    label "commercial_enterprise"
  ]
  node [
    id 922
    label "zjawisko"
  ]
  node [
    id 923
    label "apraksja"
  ]
  node [
    id 924
    label "poruszenie"
  ]
  node [
    id 925
    label "mechanika"
  ]
  node [
    id 926
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 927
    label "dyssypacja_energii"
  ]
  node [
    id 928
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 929
    label "intencja"
  ]
  node [
    id 930
    label "campaign"
  ]
  node [
    id 931
    label "post&#281;powanie"
  ]
  node [
    id 932
    label "zabieranie"
  ]
  node [
    id 933
    label "zdobywanie_podst&#281;pem"
  ]
  node [
    id 934
    label "extraction"
  ]
  node [
    id 935
    label "sk&#322;anianie"
  ]
  node [
    id 936
    label "przypominanie"
  ]
  node [
    id 937
    label "hook"
  ]
  node [
    id 938
    label "dobywanie"
  ]
  node [
    id 939
    label "przemieszczanie"
  ]
  node [
    id 940
    label "rozci&#261;ganie"
  ]
  node [
    id 941
    label "dane"
  ]
  node [
    id 942
    label "ssanie"
  ]
  node [
    id 943
    label "wydostawanie"
  ]
  node [
    id 944
    label "prostowanie"
  ]
  node [
    id 945
    label "ratowanie"
  ]
  node [
    id 946
    label "zmuszanie"
  ]
  node [
    id 947
    label "zarabianie"
  ]
  node [
    id 948
    label "obrysowywanie"
  ]
  node [
    id 949
    label "wystawianie"
  ]
  node [
    id 950
    label "wy&#322;udzanie"
  ]
  node [
    id 951
    label "wch&#322;anianie"
  ]
  node [
    id 952
    label "osi&#261;ganie"
  ]
  node [
    id 953
    label "&#347;piewanie"
  ]
  node [
    id 954
    label "reach"
  ]
  node [
    id 955
    label "wypomnienie"
  ]
  node [
    id 956
    label "wy&#322;udzenie"
  ]
  node [
    id 957
    label "powyjmowanie"
  ]
  node [
    id 958
    label "zarobienie"
  ]
  node [
    id 959
    label "dobycie"
  ]
  node [
    id 960
    label "zmuszenie"
  ]
  node [
    id 961
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 962
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 963
    label "wyratowanie"
  ]
  node [
    id 964
    label "obrysowanie"
  ]
  node [
    id 965
    label "nak&#322;onienie"
  ]
  node [
    id 966
    label "nabranie"
  ]
  node [
    id 967
    label "przypomnienie"
  ]
  node [
    id 968
    label "rozprostowanie"
  ]
  node [
    id 969
    label "powyci&#261;ganie"
  ]
  node [
    id 970
    label "rozci&#261;gni&#281;cie"
  ]
  node [
    id 971
    label "wydostanie"
  ]
  node [
    id 972
    label "pozyskanie"
  ]
  node [
    id 973
    label "przemieszczenie"
  ]
  node [
    id 974
    label "zabranie"
  ]
  node [
    id 975
    label "mienie"
  ]
  node [
    id 976
    label "za&#347;piewanie"
  ]
  node [
    id 977
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 978
    label "za&#347;piewa&#263;"
  ]
  node [
    id 979
    label "zarobi&#263;"
  ]
  node [
    id 980
    label "obrysowa&#263;"
  ]
  node [
    id 981
    label "wydosta&#263;"
  ]
  node [
    id 982
    label "rozprostowa&#263;"
  ]
  node [
    id 983
    label "ocali&#263;"
  ]
  node [
    id 984
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 985
    label "wypomnie&#263;"
  ]
  node [
    id 986
    label "pozyska&#263;"
  ]
  node [
    id 987
    label "przypomnie&#263;"
  ]
  node [
    id 988
    label "zabra&#263;"
  ]
  node [
    id 989
    label "rozci&#261;gn&#261;&#263;"
  ]
  node [
    id 990
    label "drag"
  ]
  node [
    id 991
    label "describe"
  ]
  node [
    id 992
    label "ustawienie"
  ]
  node [
    id 993
    label "oberwanie_si&#281;"
  ]
  node [
    id 994
    label "grzbiet"
  ]
  node [
    id 995
    label "pl&#261;tanina"
  ]
  node [
    id 996
    label "ob&#322;ok_Oorta"
  ]
  node [
    id 997
    label "cloud"
  ]
  node [
    id 998
    label "r&#281;ka"
  ]
  node [
    id 999
    label "burza"
  ]
  node [
    id 1000
    label "skupienie"
  ]
  node [
    id 1001
    label "oberwa&#263;_si&#281;"
  ]
  node [
    id 1002
    label "chmura"
  ]
  node [
    id 1003
    label "powderpuff"
  ]
  node [
    id 1004
    label "sadzonka"
  ]
  node [
    id 1005
    label "cia&#322;o"
  ]
  node [
    id 1006
    label "kolonia"
  ]
  node [
    id 1007
    label "korze&#324;"
  ]
  node [
    id 1008
    label "wypotnik"
  ]
  node [
    id 1009
    label "pochewka"
  ]
  node [
    id 1010
    label "strzyc"
  ]
  node [
    id 1011
    label "wegetacja"
  ]
  node [
    id 1012
    label "zadziorek"
  ]
  node [
    id 1013
    label "flawonoid"
  ]
  node [
    id 1014
    label "fitotron"
  ]
  node [
    id 1015
    label "w&#322;&#243;kno"
  ]
  node [
    id 1016
    label "zawi&#261;zek"
  ]
  node [
    id 1017
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1018
    label "pora&#380;a&#263;"
  ]
  node [
    id 1019
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1020
    label "zbiorowisko"
  ]
  node [
    id 1021
    label "do&#322;owa&#263;"
  ]
  node [
    id 1022
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1023
    label "hodowla"
  ]
  node [
    id 1024
    label "wegetowa&#263;"
  ]
  node [
    id 1025
    label "bulwka"
  ]
  node [
    id 1026
    label "sok"
  ]
  node [
    id 1027
    label "epiderma"
  ]
  node [
    id 1028
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1029
    label "system_korzeniowy"
  ]
  node [
    id 1030
    label "g&#322;uszenie"
  ]
  node [
    id 1031
    label "owoc"
  ]
  node [
    id 1032
    label "strzy&#380;enie"
  ]
  node [
    id 1033
    label "wegetowanie"
  ]
  node [
    id 1034
    label "fotoautotrof"
  ]
  node [
    id 1035
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1036
    label "gumoza"
  ]
  node [
    id 1037
    label "wyro&#347;le"
  ]
  node [
    id 1038
    label "fitocenoza"
  ]
  node [
    id 1039
    label "ro&#347;liny"
  ]
  node [
    id 1040
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1041
    label "do&#322;owanie"
  ]
  node [
    id 1042
    label "nieuleczalnie_chory"
  ]
  node [
    id 1043
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1044
    label "nadmieni&#263;"
  ]
  node [
    id 1045
    label "wpa&#347;&#263;"
  ]
  node [
    id 1046
    label "limp"
  ]
  node [
    id 1047
    label "intervene"
  ]
  node [
    id 1048
    label "fall_upon"
  ]
  node [
    id 1049
    label "ogrom"
  ]
  node [
    id 1050
    label "zapach"
  ]
  node [
    id 1051
    label "odwiedzi&#263;"
  ]
  node [
    id 1052
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 1053
    label "collapse"
  ]
  node [
    id 1054
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 1055
    label "ulec"
  ]
  node [
    id 1056
    label "wymy&#347;li&#263;"
  ]
  node [
    id 1057
    label "decline"
  ]
  node [
    id 1058
    label "wpada&#263;"
  ]
  node [
    id 1059
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 1060
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 1061
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 1062
    label "ponie&#347;&#263;"
  ]
  node [
    id 1063
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 1064
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 1065
    label "emocja"
  ]
  node [
    id 1066
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1067
    label "namieni&#263;"
  ]
  node [
    id 1068
    label "allude"
  ]
  node [
    id 1069
    label "wspomnie&#263;"
  ]
  node [
    id 1070
    label "utrudni&#263;"
  ]
  node [
    id 1071
    label "pojazd_niemechaniczny"
  ]
  node [
    id 1072
    label "sko&#324;czy&#263;"
  ]
  node [
    id 1073
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 1074
    label "po&#347;pie&#263;"
  ]
  node [
    id 1075
    label "zako&#324;czy&#263;"
  ]
  node [
    id 1076
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 1077
    label "end"
  ]
  node [
    id 1078
    label "communicate"
  ]
  node [
    id 1079
    label "zdo&#322;a&#263;"
  ]
  node [
    id 1080
    label "op&#322;aci&#263;"
  ]
  node [
    id 1081
    label "zapewni&#263;"
  ]
  node [
    id 1082
    label "zachowa&#263;"
  ]
  node [
    id 1083
    label "feed"
  ]
  node [
    id 1084
    label "foster"
  ]
  node [
    id 1085
    label "preserve"
  ]
  node [
    id 1086
    label "byt"
  ]
  node [
    id 1087
    label "potrzyma&#263;"
  ]
  node [
    id 1088
    label "przetrzyma&#263;"
  ]
  node [
    id 1089
    label "podtrzyma&#263;"
  ]
  node [
    id 1090
    label "unie&#347;&#263;"
  ]
  node [
    id 1091
    label "obroni&#263;"
  ]
  node [
    id 1092
    label "nad&#261;&#380;y&#263;"
  ]
  node [
    id 1093
    label "wystarczaj&#261;co"
  ]
  node [
    id 1094
    label "sufficiently"
  ]
  node [
    id 1095
    label "dostateczny"
  ]
  node [
    id 1096
    label "zado&#347;&#263;"
  ]
  node [
    id 1097
    label "nie&#378;le"
  ]
  node [
    id 1098
    label "odpowiednio"
  ]
  node [
    id 1099
    label "wystarczaj&#261;cy"
  ]
  node [
    id 1100
    label "tr&#243;jka"
  ]
  node [
    id 1101
    label "g&#322;&#281;boko"
  ]
  node [
    id 1102
    label "du&#380;o"
  ]
  node [
    id 1103
    label "het"
  ]
  node [
    id 1104
    label "znacznie"
  ]
  node [
    id 1105
    label "wysoko"
  ]
  node [
    id 1106
    label "dawno"
  ]
  node [
    id 1107
    label "nieobecnie"
  ]
  node [
    id 1108
    label "daleki"
  ]
  node [
    id 1109
    label "nisko"
  ]
  node [
    id 1110
    label "przysz&#322;y"
  ]
  node [
    id 1111
    label "odlegle"
  ]
  node [
    id 1112
    label "nieobecny"
  ]
  node [
    id 1113
    label "zwi&#261;zany"
  ]
  node [
    id 1114
    label "odleg&#322;y"
  ]
  node [
    id 1115
    label "du&#380;y"
  ]
  node [
    id 1116
    label "dawny"
  ]
  node [
    id 1117
    label "ogl&#281;dny"
  ]
  node [
    id 1118
    label "obcy"
  ]
  node [
    id 1119
    label "oddalony"
  ]
  node [
    id 1120
    label "g&#322;&#281;boki"
  ]
  node [
    id 1121
    label "r&#243;&#380;ny"
  ]
  node [
    id 1122
    label "s&#322;aby"
  ]
  node [
    id 1123
    label "wysoki"
  ]
  node [
    id 1124
    label "chwalebnie"
  ]
  node [
    id 1125
    label "wznio&#347;le"
  ]
  node [
    id 1126
    label "g&#243;rno"
  ]
  node [
    id 1127
    label "szczytny"
  ]
  node [
    id 1128
    label "niepo&#347;lednio"
  ]
  node [
    id 1129
    label "ongi&#347;"
  ]
  node [
    id 1130
    label "d&#322;ugotrwale"
  ]
  node [
    id 1131
    label "dawnie"
  ]
  node [
    id 1132
    label "wcze&#347;niej"
  ]
  node [
    id 1133
    label "zamy&#347;lony"
  ]
  node [
    id 1134
    label "po&#347;lednio"
  ]
  node [
    id 1135
    label "ma&#322;y"
  ]
  node [
    id 1136
    label "uni&#380;enie"
  ]
  node [
    id 1137
    label "ma&#322;o"
  ]
  node [
    id 1138
    label "vilely"
  ]
  node [
    id 1139
    label "despicably"
  ]
  node [
    id 1140
    label "pospolicie"
  ]
  node [
    id 1141
    label "wstydliwie"
  ]
  node [
    id 1142
    label "blisko"
  ]
  node [
    id 1143
    label "niski"
  ]
  node [
    id 1144
    label "mocno"
  ]
  node [
    id 1145
    label "gruntownie"
  ]
  node [
    id 1146
    label "szczerze"
  ]
  node [
    id 1147
    label "intensywnie"
  ]
  node [
    id 1148
    label "silnie"
  ]
  node [
    id 1149
    label "cz&#281;sto"
  ]
  node [
    id 1150
    label "bardzo"
  ]
  node [
    id 1151
    label "wiela"
  ]
  node [
    id 1152
    label "znaczny"
  ]
  node [
    id 1153
    label "zauwa&#380;alnie"
  ]
  node [
    id 1154
    label "rozpowszechni&#263;"
  ]
  node [
    id 1155
    label "skopiowa&#263;"
  ]
  node [
    id 1156
    label "relocate"
  ]
  node [
    id 1157
    label "strzeli&#263;"
  ]
  node [
    id 1158
    label "pocisk"
  ]
  node [
    id 1159
    label "zastrzeli&#263;"
  ]
  node [
    id 1160
    label "zbi&#263;"
  ]
  node [
    id 1161
    label "skrzywi&#263;"
  ]
  node [
    id 1162
    label "break"
  ]
  node [
    id 1163
    label "zadzwoni&#263;"
  ]
  node [
    id 1164
    label "zapulsowa&#263;"
  ]
  node [
    id 1165
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 1166
    label "rozbroi&#263;"
  ]
  node [
    id 1167
    label "os&#322;oni&#263;"
  ]
  node [
    id 1168
    label "przybi&#263;"
  ]
  node [
    id 1169
    label "u&#347;mierci&#263;"
  ]
  node [
    id 1170
    label "skarci&#263;"
  ]
  node [
    id 1171
    label "zniszczy&#263;"
  ]
  node [
    id 1172
    label "po&#322;o&#380;y&#263;_na_kolana"
  ]
  node [
    id 1173
    label "zwalczy&#263;"
  ]
  node [
    id 1174
    label "zakry&#263;"
  ]
  node [
    id 1175
    label "dispatch"
  ]
  node [
    id 1176
    label "poder&#380;n&#261;&#263;"
  ]
  node [
    id 1177
    label "zmordowa&#263;"
  ]
  node [
    id 1178
    label "pomacha&#263;"
  ]
  node [
    id 1179
    label "chi&#324;ski"
  ]
  node [
    id 1180
    label "goban"
  ]
  node [
    id 1181
    label "gra_planszowa"
  ]
  node [
    id 1182
    label "sport_umys&#322;owy"
  ]
  node [
    id 1183
    label "ekwipunek"
  ]
  node [
    id 1184
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 1185
    label "podbieg"
  ]
  node [
    id 1186
    label "wyb&#243;j"
  ]
  node [
    id 1187
    label "journey"
  ]
  node [
    id 1188
    label "pobocze"
  ]
  node [
    id 1189
    label "ekskursja"
  ]
  node [
    id 1190
    label "drogowskaz"
  ]
  node [
    id 1191
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 1192
    label "budowla"
  ]
  node [
    id 1193
    label "rajza"
  ]
  node [
    id 1194
    label "passage"
  ]
  node [
    id 1195
    label "marszrutyzacja"
  ]
  node [
    id 1196
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 1197
    label "trasa"
  ]
  node [
    id 1198
    label "zbior&#243;wka"
  ]
  node [
    id 1199
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1200
    label "turystyka"
  ]
  node [
    id 1201
    label "wylot"
  ]
  node [
    id 1202
    label "bezsilnikowy"
  ]
  node [
    id 1203
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1204
    label "nawierzchnia"
  ]
  node [
    id 1205
    label "korona_drogi"
  ]
  node [
    id 1206
    label "infrastruktura"
  ]
  node [
    id 1207
    label "w&#281;ze&#322;"
  ]
  node [
    id 1208
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 1209
    label "stan_surowy"
  ]
  node [
    id 1210
    label "postanie"
  ]
  node [
    id 1211
    label "zbudowa&#263;"
  ]
  node [
    id 1212
    label "obudowywa&#263;"
  ]
  node [
    id 1213
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 1214
    label "obudowywanie"
  ]
  node [
    id 1215
    label "konstrukcja"
  ]
  node [
    id 1216
    label "Sukiennice"
  ]
  node [
    id 1217
    label "kolumnada"
  ]
  node [
    id 1218
    label "korpus"
  ]
  node [
    id 1219
    label "zbudowanie"
  ]
  node [
    id 1220
    label "fundament"
  ]
  node [
    id 1221
    label "obudowa&#263;"
  ]
  node [
    id 1222
    label "obudowanie"
  ]
  node [
    id 1223
    label "zbi&#243;r"
  ]
  node [
    id 1224
    label "narz&#281;dzie"
  ]
  node [
    id 1225
    label "nature"
  ]
  node [
    id 1226
    label "tryb"
  ]
  node [
    id 1227
    label "ton"
  ]
  node [
    id 1228
    label "ambitus"
  ]
  node [
    id 1229
    label "rozmiar"
  ]
  node [
    id 1230
    label "skala"
  ]
  node [
    id 1231
    label "r&#281;kaw"
  ]
  node [
    id 1232
    label "koniec"
  ]
  node [
    id 1233
    label "kontusz"
  ]
  node [
    id 1234
    label "otw&#243;r"
  ]
  node [
    id 1235
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 1236
    label "warstwa"
  ]
  node [
    id 1237
    label "pokrycie"
  ]
  node [
    id 1238
    label "tablica"
  ]
  node [
    id 1239
    label "fingerpost"
  ]
  node [
    id 1240
    label "przydro&#380;e"
  ]
  node [
    id 1241
    label "autostrada"
  ]
  node [
    id 1242
    label "bieg"
  ]
  node [
    id 1243
    label "operacja"
  ]
  node [
    id 1244
    label "podr&#243;&#380;"
  ]
  node [
    id 1245
    label "mieszanie_si&#281;"
  ]
  node [
    id 1246
    label "chodzenie"
  ]
  node [
    id 1247
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1248
    label "stray"
  ]
  node [
    id 1249
    label "pozostawa&#263;"
  ]
  node [
    id 1250
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1251
    label "digress"
  ]
  node [
    id 1252
    label "chodzi&#263;"
  ]
  node [
    id 1253
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 1254
    label "wyposa&#380;enie"
  ]
  node [
    id 1255
    label "nie&#347;miertelnik"
  ]
  node [
    id 1256
    label "moderunek"
  ]
  node [
    id 1257
    label "kocher"
  ]
  node [
    id 1258
    label "dormitorium"
  ]
  node [
    id 1259
    label "fotografia"
  ]
  node [
    id 1260
    label "spis"
  ]
  node [
    id 1261
    label "sk&#322;adanka"
  ]
  node [
    id 1262
    label "wyprawa"
  ]
  node [
    id 1263
    label "pomieszczenie"
  ]
  node [
    id 1264
    label "beznap&#281;dowy"
  ]
  node [
    id 1265
    label "erotyka"
  ]
  node [
    id 1266
    label "zajawka"
  ]
  node [
    id 1267
    label "love"
  ]
  node [
    id 1268
    label "podniecanie"
  ]
  node [
    id 1269
    label "po&#380;ycie"
  ]
  node [
    id 1270
    label "ukochanie"
  ]
  node [
    id 1271
    label "baraszki"
  ]
  node [
    id 1272
    label "numer"
  ]
  node [
    id 1273
    label "ruch_frykcyjny"
  ]
  node [
    id 1274
    label "tendency"
  ]
  node [
    id 1275
    label "wzw&#243;d"
  ]
  node [
    id 1276
    label "serce"
  ]
  node [
    id 1277
    label "wi&#281;&#378;"
  ]
  node [
    id 1278
    label "seks"
  ]
  node [
    id 1279
    label "pozycja_misjonarska"
  ]
  node [
    id 1280
    label "rozmna&#380;anie"
  ]
  node [
    id 1281
    label "feblik"
  ]
  node [
    id 1282
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1283
    label "imisja"
  ]
  node [
    id 1284
    label "podniecenie"
  ]
  node [
    id 1285
    label "podnieca&#263;"
  ]
  node [
    id 1286
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 1287
    label "zakochanie"
  ]
  node [
    id 1288
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 1289
    label "gra_wst&#281;pna"
  ]
  node [
    id 1290
    label "drogi"
  ]
  node [
    id 1291
    label "po&#380;&#261;danie"
  ]
  node [
    id 1292
    label "podnieci&#263;"
  ]
  node [
    id 1293
    label "afekt"
  ]
  node [
    id 1294
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 1295
    label "na_pieska"
  ]
  node [
    id 1296
    label "kochanka"
  ]
  node [
    id 1297
    label "psiapsi&#243;&#322;ka"
  ]
  node [
    id 1298
    label "kultura_fizyczna"
  ]
  node [
    id 1299
    label "turyzm"
  ]
  node [
    id 1300
    label "rezultat"
  ]
  node [
    id 1301
    label "przyczyna"
  ]
  node [
    id 1302
    label "typ"
  ]
  node [
    id 1303
    label "dzia&#322;anie"
  ]
  node [
    id 1304
    label "event"
  ]
  node [
    id 1305
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 1306
    label "odbicie"
  ]
  node [
    id 1307
    label "odbicie_si&#281;"
  ]
  node [
    id 1308
    label "dotkni&#281;cie"
  ]
  node [
    id 1309
    label "zadanie"
  ]
  node [
    id 1310
    label "pobicie"
  ]
  node [
    id 1311
    label "skrytykowanie"
  ]
  node [
    id 1312
    label "charge"
  ]
  node [
    id 1313
    label "instrumentalizacja"
  ]
  node [
    id 1314
    label "st&#322;uczenie"
  ]
  node [
    id 1315
    label "uderzanie"
  ]
  node [
    id 1316
    label "walka"
  ]
  node [
    id 1317
    label "stukni&#281;cie"
  ]
  node [
    id 1318
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1319
    label "dotyk"
  ]
  node [
    id 1320
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 1321
    label "trafienie"
  ]
  node [
    id 1322
    label "zagrywka"
  ]
  node [
    id 1323
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1324
    label "dostanie"
  ]
  node [
    id 1325
    label "poczucie"
  ]
  node [
    id 1326
    label "reakcja"
  ]
  node [
    id 1327
    label "cios"
  ]
  node [
    id 1328
    label "stroke"
  ]
  node [
    id 1329
    label "contact"
  ]
  node [
    id 1330
    label "nast&#261;pienie"
  ]
  node [
    id 1331
    label "flap"
  ]
  node [
    id 1332
    label "wdarcie_si&#281;"
  ]
  node [
    id 1333
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 1334
    label "zrobienie"
  ]
  node [
    id 1335
    label "dawka"
  ]
  node [
    id 1336
    label "coup"
  ]
  node [
    id 1337
    label "zas&#243;b"
  ]
  node [
    id 1338
    label "porcja"
  ]
  node [
    id 1339
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 1340
    label "narobienie"
  ]
  node [
    id 1341
    label "porobienie"
  ]
  node [
    id 1342
    label "creation"
  ]
  node [
    id 1343
    label "maneuver"
  ]
  node [
    id 1344
    label "posuni&#281;cie"
  ]
  node [
    id 1345
    label "indignation"
  ]
  node [
    id 1346
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 1347
    label "niedorobek"
  ]
  node [
    id 1348
    label "offense"
  ]
  node [
    id 1349
    label "spotkanie"
  ]
  node [
    id 1350
    label "tekst"
  ]
  node [
    id 1351
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 1352
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 1353
    label "ruszenie"
  ]
  node [
    id 1354
    label "wyzwisko"
  ]
  node [
    id 1355
    label "wrzuta"
  ]
  node [
    id 1356
    label "dotkni&#281;cie_si&#281;"
  ]
  node [
    id 1357
    label "touch"
  ]
  node [
    id 1358
    label "ubliga"
  ]
  node [
    id 1359
    label "przesuni&#281;cie"
  ]
  node [
    id 1360
    label "wzbudzenie"
  ]
  node [
    id 1361
    label "hit"
  ]
  node [
    id 1362
    label "krzywda"
  ]
  node [
    id 1363
    label "tkliwy"
  ]
  node [
    id 1364
    label "podotykanie"
  ]
  node [
    id 1365
    label "solmizacja"
  ]
  node [
    id 1366
    label "wydanie"
  ]
  node [
    id 1367
    label "transmiter"
  ]
  node [
    id 1368
    label "repetycja"
  ]
  node [
    id 1369
    label "akcent"
  ]
  node [
    id 1370
    label "nadlecenie"
  ]
  node [
    id 1371
    label "note"
  ]
  node [
    id 1372
    label "heksachord"
  ]
  node [
    id 1373
    label "wpadanie"
  ]
  node [
    id 1374
    label "phone"
  ]
  node [
    id 1375
    label "wydawa&#263;"
  ]
  node [
    id 1376
    label "seria"
  ]
  node [
    id 1377
    label "onomatopeja"
  ]
  node [
    id 1378
    label "brzmienie"
  ]
  node [
    id 1379
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1380
    label "dobiec"
  ]
  node [
    id 1381
    label "intonacja"
  ]
  node [
    id 1382
    label "wpadni&#281;cie"
  ]
  node [
    id 1383
    label "modalizm"
  ]
  node [
    id 1384
    label "wyda&#263;"
  ]
  node [
    id 1385
    label "sound"
  ]
  node [
    id 1386
    label "aggravation"
  ]
  node [
    id 1387
    label "gorszy"
  ]
  node [
    id 1388
    label "zmienienie"
  ]
  node [
    id 1389
    label "worsening"
  ]
  node [
    id 1390
    label "mecz"
  ]
  node [
    id 1391
    label "gra"
  ]
  node [
    id 1392
    label "rozgrywka"
  ]
  node [
    id 1393
    label "gambit"
  ]
  node [
    id 1394
    label "gra_w_karty"
  ]
  node [
    id 1395
    label "tactile_property"
  ]
  node [
    id 1396
    label "sztywnienie"
  ]
  node [
    id 1397
    label "sztywnie&#263;"
  ]
  node [
    id 1398
    label "zaopiniowanie"
  ]
  node [
    id 1399
    label "ocenienie"
  ]
  node [
    id 1400
    label "czepienie_si&#281;"
  ]
  node [
    id 1401
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1402
    label "nakarmienie"
  ]
  node [
    id 1403
    label "przepisanie"
  ]
  node [
    id 1404
    label "powierzanie"
  ]
  node [
    id 1405
    label "przepisa&#263;"
  ]
  node [
    id 1406
    label "zaszkodzenie"
  ]
  node [
    id 1407
    label "problem"
  ]
  node [
    id 1408
    label "zobowi&#261;zanie"
  ]
  node [
    id 1409
    label "zaj&#281;cie"
  ]
  node [
    id 1410
    label "yield"
  ]
  node [
    id 1411
    label "duty"
  ]
  node [
    id 1412
    label "work"
  ]
  node [
    id 1413
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 1414
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 1415
    label "blok"
  ]
  node [
    id 1416
    label "struktura_geologiczna"
  ]
  node [
    id 1417
    label "pr&#243;ba"
  ]
  node [
    id 1418
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 1419
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 1420
    label "siekacz"
  ]
  node [
    id 1421
    label "punkt"
  ]
  node [
    id 1422
    label "dostanie_si&#281;"
  ]
  node [
    id 1423
    label "gather"
  ]
  node [
    id 1424
    label "dolecenie"
  ]
  node [
    id 1425
    label "dotarcie"
  ]
  node [
    id 1426
    label "dopasowanie_si&#281;"
  ]
  node [
    id 1427
    label "znalezienie_si&#281;"
  ]
  node [
    id 1428
    label "sukces"
  ]
  node [
    id 1429
    label "znalezienie"
  ]
  node [
    id 1430
    label "zjawienie_si&#281;"
  ]
  node [
    id 1431
    label "causing"
  ]
  node [
    id 1432
    label "response"
  ]
  node [
    id 1433
    label "zachowanie"
  ]
  node [
    id 1434
    label "organizm"
  ]
  node [
    id 1435
    label "rozmowa"
  ]
  node [
    id 1436
    label "respondent"
  ]
  node [
    id 1437
    label "reaction"
  ]
  node [
    id 1438
    label "biczysko"
  ]
  node [
    id 1439
    label "zacinanie"
  ]
  node [
    id 1440
    label "narz&#281;dzie_wymierzania_kary"
  ]
  node [
    id 1441
    label "&#380;agl&#243;wka"
  ]
  node [
    id 1442
    label "zacina&#263;"
  ]
  node [
    id 1443
    label "idiofon"
  ]
  node [
    id 1444
    label "jednostka_obj&#281;to&#347;ci_p&#322;yn&#243;w"
  ]
  node [
    id 1445
    label "zaatakowanie"
  ]
  node [
    id 1446
    label "advent"
  ]
  node [
    id 1447
    label "naci&#347;ni&#281;cie"
  ]
  node [
    id 1448
    label "porobienie_si&#281;"
  ]
  node [
    id 1449
    label "czyn"
  ]
  node [
    id 1450
    label "action"
  ]
  node [
    id 1451
    label "obrona"
  ]
  node [
    id 1452
    label "military_action"
  ]
  node [
    id 1453
    label "contest"
  ]
  node [
    id 1454
    label "rywalizacja"
  ]
  node [
    id 1455
    label "sp&#243;r"
  ]
  node [
    id 1456
    label "konfrontacyjny"
  ]
  node [
    id 1457
    label "wrestle"
  ]
  node [
    id 1458
    label "sambo"
  ]
  node [
    id 1459
    label "granie"
  ]
  node [
    id 1460
    label "gilotyna"
  ]
  node [
    id 1461
    label "skr&#243;cenie"
  ]
  node [
    id 1462
    label "tenis"
  ]
  node [
    id 1463
    label "obci&#281;cie"
  ]
  node [
    id 1464
    label "k&#322;&#243;tnia"
  ]
  node [
    id 1465
    label "usuni&#281;cie"
  ]
  node [
    id 1466
    label "szafot"
  ]
  node [
    id 1467
    label "splay"
  ]
  node [
    id 1468
    label "poobcinanie"
  ]
  node [
    id 1469
    label "opitolenie"
  ]
  node [
    id 1470
    label "decapitation"
  ]
  node [
    id 1471
    label "st&#281;&#380;enie"
  ]
  node [
    id 1472
    label "ping-pong"
  ]
  node [
    id 1473
    label "kr&#243;j"
  ]
  node [
    id 1474
    label "zmro&#380;enie"
  ]
  node [
    id 1475
    label "zniszczenie"
  ]
  node [
    id 1476
    label "ukszta&#322;towanie"
  ]
  node [
    id 1477
    label "siatk&#243;wka"
  ]
  node [
    id 1478
    label "zabicie"
  ]
  node [
    id 1479
    label "w&#322;osy"
  ]
  node [
    id 1480
    label "g&#322;owa"
  ]
  node [
    id 1481
    label "ow&#322;osienie"
  ]
  node [
    id 1482
    label "oblanie"
  ]
  node [
    id 1483
    label "przeegzaminowanie"
  ]
  node [
    id 1484
    label "odci&#281;cie"
  ]
  node [
    id 1485
    label "kara_&#347;mierci"
  ]
  node [
    id 1486
    label "snub"
  ]
  node [
    id 1487
    label "uszkodzenie"
  ]
  node [
    id 1488
    label "zbutowanie"
  ]
  node [
    id 1489
    label "wypuszczenie"
  ]
  node [
    id 1490
    label "annihilation"
  ]
  node [
    id 1491
    label "battery"
  ]
  node [
    id 1492
    label "pot&#322;uczenie"
  ]
  node [
    id 1493
    label "wyt&#322;uczenie"
  ]
  node [
    id 1494
    label "kontuzja"
  ]
  node [
    id 1495
    label "nalanie"
  ]
  node [
    id 1496
    label "uraz"
  ]
  node [
    id 1497
    label "interruption"
  ]
  node [
    id 1498
    label "siniak"
  ]
  node [
    id 1499
    label "rozbicie_si&#281;"
  ]
  node [
    id 1500
    label "meteorology"
  ]
  node [
    id 1501
    label "warunki"
  ]
  node [
    id 1502
    label "weather"
  ]
  node [
    id 1503
    label "pok&#243;j"
  ]
  node [
    id 1504
    label "atak"
  ]
  node [
    id 1505
    label "prognoza_meteorologiczna"
  ]
  node [
    id 1506
    label "program"
  ]
  node [
    id 1507
    label "doczekanie"
  ]
  node [
    id 1508
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1509
    label "nabawianie_si&#281;"
  ]
  node [
    id 1510
    label "party"
  ]
  node [
    id 1511
    label "si&#281;gni&#281;cie"
  ]
  node [
    id 1512
    label "wzi&#281;cie"
  ]
  node [
    id 1513
    label "zwiastun"
  ]
  node [
    id 1514
    label "bycie_w_posiadaniu"
  ]
  node [
    id 1515
    label "zapanowanie"
  ]
  node [
    id 1516
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 1517
    label "dostawanie"
  ]
  node [
    id 1518
    label "wystanie"
  ]
  node [
    id 1519
    label "nabawienie_si&#281;"
  ]
  node [
    id 1520
    label "kupienie"
  ]
  node [
    id 1521
    label "knock"
  ]
  node [
    id 1522
    label "pukanie"
  ]
  node [
    id 1523
    label "ukaranie"
  ]
  node [
    id 1524
    label "walni&#281;cie"
  ]
  node [
    id 1525
    label "zabrzmienie"
  ]
  node [
    id 1526
    label "zaliczanie"
  ]
  node [
    id 1527
    label "zastrzelenie"
  ]
  node [
    id 1528
    label "wbicie"
  ]
  node [
    id 1529
    label "barrage"
  ]
  node [
    id 1530
    label "lanie"
  ]
  node [
    id 1531
    label "wygranie"
  ]
  node [
    id 1532
    label "wyswobodzenie"
  ]
  node [
    id 1533
    label "prototype"
  ]
  node [
    id 1534
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 1535
    label "pi&#322;ka"
  ]
  node [
    id 1536
    label "odskoczenie"
  ]
  node [
    id 1537
    label "wynagrodzenie"
  ]
  node [
    id 1538
    label "blask"
  ]
  node [
    id 1539
    label "reflection"
  ]
  node [
    id 1540
    label "zata&#324;czenie"
  ]
  node [
    id 1541
    label "odegranie_si&#281;"
  ]
  node [
    id 1542
    label "zwierciad&#322;o"
  ]
  node [
    id 1543
    label "picture"
  ]
  node [
    id 1544
    label "impression"
  ]
  node [
    id 1545
    label "ut&#322;uczenie"
  ]
  node [
    id 1546
    label "wymienienie"
  ]
  node [
    id 1547
    label "lustro"
  ]
  node [
    id 1548
    label "oddalenie_si&#281;"
  ]
  node [
    id 1549
    label "przelobowanie"
  ]
  node [
    id 1550
    label "stracenie_g&#322;owy"
  ]
  node [
    id 1551
    label "stamp"
  ]
  node [
    id 1552
    label "zostawienie"
  ]
  node [
    id 1553
    label "wybicie"
  ]
  node [
    id 1554
    label "skopiowanie"
  ]
  node [
    id 1555
    label "reproduction"
  ]
  node [
    id 1556
    label "zadawanie"
  ]
  node [
    id 1557
    label "grasowanie"
  ]
  node [
    id 1558
    label "walczenie"
  ]
  node [
    id 1559
    label "krytykowanie"
  ]
  node [
    id 1560
    label "zwracanie_si&#281;"
  ]
  node [
    id 1561
    label "zwracanie_uwagi"
  ]
  node [
    id 1562
    label "dzianie_si&#281;"
  ]
  node [
    id 1563
    label "wytrzepanie"
  ]
  node [
    id 1564
    label "staranowanie"
  ]
  node [
    id 1565
    label "stukanie"
  ]
  node [
    id 1566
    label "pouderzanie"
  ]
  node [
    id 1567
    label "rozkwaszenie"
  ]
  node [
    id 1568
    label "pobijanie"
  ]
  node [
    id 1569
    label "t&#322;uczenie"
  ]
  node [
    id 1570
    label "haratanie"
  ]
  node [
    id 1571
    label "torpedowanie"
  ]
  node [
    id 1572
    label "&#347;cinanie"
  ]
  node [
    id 1573
    label "napadanie"
  ]
  node [
    id 1574
    label "dotykanie"
  ]
  node [
    id 1575
    label "bicie"
  ]
  node [
    id 1576
    label "skontrowanie"
  ]
  node [
    id 1577
    label "friction"
  ]
  node [
    id 1578
    label "trzepanie"
  ]
  node [
    id 1579
    label "judgment"
  ]
  node [
    id 1580
    label "odbijanie"
  ]
  node [
    id 1581
    label "kontrowanie"
  ]
  node [
    id 1582
    label "zamachiwanie_si&#281;"
  ]
  node [
    id 1583
    label "taranowanie"
  ]
  node [
    id 1584
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 1585
    label "opanowanie"
  ]
  node [
    id 1586
    label "ekstraspekcja"
  ]
  node [
    id 1587
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 1588
    label "feeling"
  ]
  node [
    id 1589
    label "wiedza"
  ]
  node [
    id 1590
    label "intuition"
  ]
  node [
    id 1591
    label "doznanie"
  ]
  node [
    id 1592
    label "os&#322;upienie"
  ]
  node [
    id 1593
    label "zareagowanie"
  ]
  node [
    id 1594
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1595
    label "pozosta&#263;"
  ]
  node [
    id 1596
    label "osta&#263;_si&#281;"
  ]
  node [
    id 1597
    label "catch"
  ]
  node [
    id 1598
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 1599
    label "support"
  ]
  node [
    id 1600
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 1601
    label "zgnie&#347;&#263;"
  ]
  node [
    id 1602
    label "zdruzgota&#263;"
  ]
  node [
    id 1603
    label "consume"
  ]
  node [
    id 1604
    label "zdeformowa&#263;"
  ]
  node [
    id 1605
    label "rumple"
  ]
  node [
    id 1606
    label "wygra&#263;"
  ]
  node [
    id 1607
    label "za&#322;ama&#263;"
  ]
  node [
    id 1608
    label "rozwali&#263;"
  ]
  node [
    id 1609
    label "klient"
  ]
  node [
    id 1610
    label "podr&#243;&#380;ny"
  ]
  node [
    id 1611
    label "specjalny"
  ]
  node [
    id 1612
    label "przygodny"
  ]
  node [
    id 1613
    label "obywatel"
  ]
  node [
    id 1614
    label "us&#322;ugobiorca"
  ]
  node [
    id 1615
    label "szlachcic"
  ]
  node [
    id 1616
    label "agent_rozliczeniowy"
  ]
  node [
    id 1617
    label "bratek"
  ]
  node [
    id 1618
    label "klientela"
  ]
  node [
    id 1619
    label "Rzymianin"
  ]
  node [
    id 1620
    label "komputer_cyfrowy"
  ]
  node [
    id 1621
    label "feel"
  ]
  node [
    id 1622
    label "zagorze&#263;"
  ]
  node [
    id 1623
    label "zaczadzi&#263;"
  ]
  node [
    id 1624
    label "opali&#263;_si&#281;"
  ]
  node [
    id 1625
    label "zapali&#263;_si&#281;"
  ]
  node [
    id 1626
    label "za&#347;wieci&#263;"
  ]
  node [
    id 1627
    label "poczu&#263;"
  ]
  node [
    id 1628
    label "trz&#281;sienie"
  ]
  node [
    id 1629
    label "sk&#322;&#243;cenie"
  ]
  node [
    id 1630
    label "jolt"
  ]
  node [
    id 1631
    label "agitation"
  ]
  node [
    id 1632
    label "poruszanie"
  ]
  node [
    id 1633
    label "ruszanie"
  ]
  node [
    id 1634
    label "roztrzepywanie"
  ]
  node [
    id 1635
    label "spin"
  ]
  node [
    id 1636
    label "wytrz&#261;sanie"
  ]
  node [
    id 1637
    label "roztrz&#261;sanie"
  ]
  node [
    id 1638
    label "roztrz&#261;&#347;ni&#281;cie"
  ]
  node [
    id 1639
    label "rz&#261;dzenie"
  ]
  node [
    id 1640
    label "wytrz&#261;&#347;ni&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 471
  ]
  edge [
    source 15
    target 195
  ]
  edge [
    source 15
    target 472
  ]
  edge [
    source 15
    target 473
  ]
  edge [
    source 15
    target 474
  ]
  edge [
    source 15
    target 475
  ]
  edge [
    source 15
    target 476
  ]
  edge [
    source 15
    target 477
  ]
  edge [
    source 15
    target 478
  ]
  edge [
    source 15
    target 84
  ]
  edge [
    source 15
    target 50
  ]
  edge [
    source 15
    target 479
  ]
  edge [
    source 15
    target 86
  ]
  edge [
    source 15
    target 297
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 480
  ]
  edge [
    source 15
    target 481
  ]
  edge [
    source 15
    target 482
  ]
  edge [
    source 15
    target 483
  ]
  edge [
    source 15
    target 484
  ]
  edge [
    source 15
    target 270
  ]
  edge [
    source 15
    target 485
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 486
  ]
  edge [
    source 15
    target 487
  ]
  edge [
    source 15
    target 488
  ]
  edge [
    source 15
    target 489
  ]
  edge [
    source 15
    target 490
  ]
  edge [
    source 15
    target 491
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 492
  ]
  edge [
    source 15
    target 493
  ]
  edge [
    source 15
    target 494
  ]
  edge [
    source 15
    target 495
  ]
  edge [
    source 15
    target 496
  ]
  edge [
    source 15
    target 497
  ]
  edge [
    source 15
    target 498
  ]
  edge [
    source 15
    target 499
  ]
  edge [
    source 15
    target 500
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 501
  ]
  edge [
    source 16
    target 502
  ]
  edge [
    source 16
    target 260
  ]
  edge [
    source 16
    target 503
  ]
  edge [
    source 16
    target 504
  ]
  edge [
    source 16
    target 505
  ]
  edge [
    source 16
    target 248
  ]
  edge [
    source 16
    target 506
  ]
  edge [
    source 16
    target 507
  ]
  edge [
    source 16
    target 271
  ]
  edge [
    source 16
    target 272
  ]
  edge [
    source 16
    target 273
  ]
  edge [
    source 16
    target 274
  ]
  edge [
    source 16
    target 275
  ]
  edge [
    source 16
    target 276
  ]
  edge [
    source 16
    target 277
  ]
  edge [
    source 16
    target 278
  ]
  edge [
    source 16
    target 279
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 280
  ]
  edge [
    source 16
    target 60
  ]
  edge [
    source 16
    target 281
  ]
  edge [
    source 16
    target 282
  ]
  edge [
    source 16
    target 283
  ]
  edge [
    source 16
    target 284
  ]
  edge [
    source 16
    target 508
  ]
  edge [
    source 16
    target 509
  ]
  edge [
    source 16
    target 510
  ]
  edge [
    source 16
    target 511
  ]
  edge [
    source 16
    target 512
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 90
  ]
  edge [
    source 16
    target 513
  ]
  edge [
    source 16
    target 514
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 515
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 252
  ]
  edge [
    source 16
    target 516
  ]
  edge [
    source 16
    target 74
  ]
  edge [
    source 16
    target 517
  ]
  edge [
    source 16
    target 518
  ]
  edge [
    source 16
    target 519
  ]
  edge [
    source 16
    target 520
  ]
  edge [
    source 16
    target 521
  ]
  edge [
    source 16
    target 522
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 523
  ]
  edge [
    source 16
    target 524
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 294
  ]
  edge [
    source 16
    target 525
  ]
  edge [
    source 16
    target 526
  ]
  edge [
    source 16
    target 527
  ]
  edge [
    source 16
    target 528
  ]
  edge [
    source 16
    target 529
  ]
  edge [
    source 16
    target 530
  ]
  edge [
    source 16
    target 531
  ]
  edge [
    source 16
    target 532
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 533
  ]
  edge [
    source 18
    target 534
  ]
  edge [
    source 18
    target 535
  ]
  edge [
    source 18
    target 536
  ]
  edge [
    source 18
    target 537
  ]
  edge [
    source 18
    target 538
  ]
  edge [
    source 18
    target 539
  ]
  edge [
    source 18
    target 540
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 541
  ]
  edge [
    source 19
    target 542
  ]
  edge [
    source 19
    target 543
  ]
  edge [
    source 19
    target 544
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 19
    target 511
  ]
  edge [
    source 19
    target 545
  ]
  edge [
    source 19
    target 546
  ]
  edge [
    source 19
    target 60
  ]
  edge [
    source 19
    target 547
  ]
  edge [
    source 19
    target 548
  ]
  edge [
    source 19
    target 549
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 550
  ]
  edge [
    source 20
    target 551
  ]
  edge [
    source 20
    target 552
  ]
  edge [
    source 20
    target 553
  ]
  edge [
    source 20
    target 554
  ]
  edge [
    source 20
    target 555
  ]
  edge [
    source 20
    target 556
  ]
  edge [
    source 20
    target 557
  ]
  edge [
    source 20
    target 558
  ]
  edge [
    source 20
    target 559
  ]
  edge [
    source 20
    target 560
  ]
  edge [
    source 20
    target 561
  ]
  edge [
    source 20
    target 562
  ]
  edge [
    source 20
    target 563
  ]
  edge [
    source 20
    target 564
  ]
  edge [
    source 20
    target 565
  ]
  edge [
    source 20
    target 566
  ]
  edge [
    source 20
    target 567
  ]
  edge [
    source 20
    target 568
  ]
  edge [
    source 20
    target 569
  ]
  edge [
    source 20
    target 570
  ]
  edge [
    source 20
    target 571
  ]
  edge [
    source 20
    target 572
  ]
  edge [
    source 20
    target 573
  ]
  edge [
    source 20
    target 574
  ]
  edge [
    source 20
    target 575
  ]
  edge [
    source 20
    target 576
  ]
  edge [
    source 20
    target 577
  ]
  edge [
    source 20
    target 578
  ]
  edge [
    source 20
    target 579
  ]
  edge [
    source 20
    target 580
  ]
  edge [
    source 20
    target 581
  ]
  edge [
    source 20
    target 582
  ]
  edge [
    source 20
    target 583
  ]
  edge [
    source 20
    target 584
  ]
  edge [
    source 20
    target 585
  ]
  edge [
    source 20
    target 586
  ]
  edge [
    source 20
    target 587
  ]
  edge [
    source 20
    target 588
  ]
  edge [
    source 20
    target 589
  ]
  edge [
    source 20
    target 590
  ]
  edge [
    source 20
    target 591
  ]
  edge [
    source 20
    target 592
  ]
  edge [
    source 20
    target 593
  ]
  edge [
    source 20
    target 594
  ]
  edge [
    source 20
    target 595
  ]
  edge [
    source 20
    target 596
  ]
  edge [
    source 20
    target 597
  ]
  edge [
    source 20
    target 499
  ]
  edge [
    source 20
    target 598
  ]
  edge [
    source 20
    target 599
  ]
  edge [
    source 20
    target 600
  ]
  edge [
    source 20
    target 601
  ]
  edge [
    source 20
    target 602
  ]
  edge [
    source 20
    target 603
  ]
  edge [
    source 20
    target 604
  ]
  edge [
    source 20
    target 605
  ]
  edge [
    source 20
    target 606
  ]
  edge [
    source 20
    target 607
  ]
  edge [
    source 20
    target 608
  ]
  edge [
    source 20
    target 609
  ]
  edge [
    source 20
    target 610
  ]
  edge [
    source 20
    target 340
  ]
  edge [
    source 20
    target 611
  ]
  edge [
    source 20
    target 612
  ]
  edge [
    source 20
    target 613
  ]
  edge [
    source 20
    target 614
  ]
  edge [
    source 20
    target 615
  ]
  edge [
    source 20
    target 616
  ]
  edge [
    source 20
    target 617
  ]
  edge [
    source 20
    target 336
  ]
  edge [
    source 20
    target 618
  ]
  edge [
    source 20
    target 619
  ]
  edge [
    source 20
    target 620
  ]
  edge [
    source 20
    target 621
  ]
  edge [
    source 20
    target 622
  ]
  edge [
    source 20
    target 623
  ]
  edge [
    source 20
    target 624
  ]
  edge [
    source 20
    target 625
  ]
  edge [
    source 20
    target 626
  ]
  edge [
    source 20
    target 627
  ]
  edge [
    source 20
    target 628
  ]
  edge [
    source 20
    target 629
  ]
  edge [
    source 20
    target 630
  ]
  edge [
    source 20
    target 631
  ]
  edge [
    source 20
    target 632
  ]
  edge [
    source 20
    target 633
  ]
  edge [
    source 20
    target 634
  ]
  edge [
    source 20
    target 635
  ]
  edge [
    source 20
    target 636
  ]
  edge [
    source 20
    target 637
  ]
  edge [
    source 20
    target 638
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 639
  ]
  edge [
    source 20
    target 640
  ]
  edge [
    source 20
    target 641
  ]
  edge [
    source 20
    target 642
  ]
  edge [
    source 20
    target 643
  ]
  edge [
    source 20
    target 644
  ]
  edge [
    source 20
    target 645
  ]
  edge [
    source 20
    target 646
  ]
  edge [
    source 20
    target 647
  ]
  edge [
    source 20
    target 648
  ]
  edge [
    source 20
    target 649
  ]
  edge [
    source 20
    target 650
  ]
  edge [
    source 20
    target 651
  ]
  edge [
    source 20
    target 652
  ]
  edge [
    source 20
    target 653
  ]
  edge [
    source 20
    target 542
  ]
  edge [
    source 20
    target 654
  ]
  edge [
    source 20
    target 655
  ]
  edge [
    source 20
    target 656
  ]
  edge [
    source 20
    target 657
  ]
  edge [
    source 20
    target 658
  ]
  edge [
    source 20
    target 659
  ]
  edge [
    source 20
    target 660
  ]
  edge [
    source 20
    target 661
  ]
  edge [
    source 20
    target 662
  ]
  edge [
    source 20
    target 663
  ]
  edge [
    source 20
    target 664
  ]
  edge [
    source 20
    target 665
  ]
  edge [
    source 20
    target 666
  ]
  edge [
    source 20
    target 667
  ]
  edge [
    source 20
    target 668
  ]
  edge [
    source 20
    target 669
  ]
  edge [
    source 20
    target 670
  ]
  edge [
    source 20
    target 671
  ]
  edge [
    source 20
    target 672
  ]
  edge [
    source 20
    target 673
  ]
  edge [
    source 20
    target 674
  ]
  edge [
    source 20
    target 675
  ]
  edge [
    source 20
    target 676
  ]
  edge [
    source 20
    target 677
  ]
  edge [
    source 20
    target 678
  ]
  edge [
    source 20
    target 679
  ]
  edge [
    source 20
    target 680
  ]
  edge [
    source 20
    target 681
  ]
  edge [
    source 20
    target 682
  ]
  edge [
    source 20
    target 683
  ]
  edge [
    source 20
    target 544
  ]
  edge [
    source 20
    target 684
  ]
  edge [
    source 20
    target 685
  ]
  edge [
    source 20
    target 686
  ]
  edge [
    source 20
    target 687
  ]
  edge [
    source 20
    target 688
  ]
  edge [
    source 20
    target 689
  ]
  edge [
    source 20
    target 690
  ]
  edge [
    source 20
    target 691
  ]
  edge [
    source 20
    target 692
  ]
  edge [
    source 20
    target 693
  ]
  edge [
    source 20
    target 694
  ]
  edge [
    source 20
    target 695
  ]
  edge [
    source 20
    target 696
  ]
  edge [
    source 20
    target 697
  ]
  edge [
    source 20
    target 698
  ]
  edge [
    source 20
    target 699
  ]
  edge [
    source 20
    target 700
  ]
  edge [
    source 20
    target 701
  ]
  edge [
    source 20
    target 294
  ]
  edge [
    source 20
    target 702
  ]
  edge [
    source 20
    target 703
  ]
  edge [
    source 20
    target 704
  ]
  edge [
    source 20
    target 705
  ]
  edge [
    source 20
    target 706
  ]
  edge [
    source 20
    target 707
  ]
  edge [
    source 20
    target 708
  ]
  edge [
    source 20
    target 709
  ]
  edge [
    source 20
    target 341
  ]
  edge [
    source 20
    target 343
  ]
  edge [
    source 20
    target 710
  ]
  edge [
    source 20
    target 711
  ]
  edge [
    source 20
    target 712
  ]
  edge [
    source 20
    target 713
  ]
  edge [
    source 20
    target 714
  ]
  edge [
    source 20
    target 715
  ]
  edge [
    source 20
    target 716
  ]
  edge [
    source 20
    target 60
  ]
  edge [
    source 20
    target 717
  ]
  edge [
    source 20
    target 718
  ]
  edge [
    source 20
    target 96
  ]
  edge [
    source 20
    target 719
  ]
  edge [
    source 20
    target 720
  ]
  edge [
    source 20
    target 721
  ]
  edge [
    source 20
    target 722
  ]
  edge [
    source 20
    target 723
  ]
  edge [
    source 20
    target 724
  ]
  edge [
    source 20
    target 725
  ]
  edge [
    source 20
    target 726
  ]
  edge [
    source 20
    target 727
  ]
  edge [
    source 20
    target 728
  ]
  edge [
    source 20
    target 729
  ]
  edge [
    source 20
    target 730
  ]
  edge [
    source 20
    target 731
  ]
  edge [
    source 20
    target 732
  ]
  edge [
    source 20
    target 733
  ]
  edge [
    source 20
    target 734
  ]
  edge [
    source 20
    target 735
  ]
  edge [
    source 20
    target 736
  ]
  edge [
    source 20
    target 737
  ]
  edge [
    source 20
    target 738
  ]
  edge [
    source 20
    target 739
  ]
  edge [
    source 20
    target 740
  ]
  edge [
    source 20
    target 256
  ]
  edge [
    source 20
    target 741
  ]
  edge [
    source 20
    target 742
  ]
  edge [
    source 20
    target 743
  ]
  edge [
    source 20
    target 744
  ]
  edge [
    source 20
    target 745
  ]
  edge [
    source 20
    target 746
  ]
  edge [
    source 20
    target 747
  ]
  edge [
    source 20
    target 748
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 749
  ]
  edge [
    source 20
    target 750
  ]
  edge [
    source 20
    target 751
  ]
  edge [
    source 20
    target 752
  ]
  edge [
    source 20
    target 753
  ]
  edge [
    source 20
    target 754
  ]
  edge [
    source 20
    target 755
  ]
  edge [
    source 20
    target 756
  ]
  edge [
    source 20
    target 757
  ]
  edge [
    source 21
    target 758
  ]
  edge [
    source 21
    target 759
  ]
  edge [
    source 21
    target 760
  ]
  edge [
    source 21
    target 761
  ]
  edge [
    source 21
    target 762
  ]
  edge [
    source 21
    target 763
  ]
  edge [
    source 21
    target 764
  ]
  edge [
    source 21
    target 765
  ]
  edge [
    source 21
    target 512
  ]
  edge [
    source 21
    target 57
  ]
  edge [
    source 21
    target 766
  ]
  edge [
    source 21
    target 767
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 768
  ]
  edge [
    source 23
    target 769
  ]
  edge [
    source 23
    target 770
  ]
  edge [
    source 23
    target 771
  ]
  edge [
    source 23
    target 772
  ]
  edge [
    source 23
    target 773
  ]
  edge [
    source 23
    target 774
  ]
  edge [
    source 23
    target 775
  ]
  edge [
    source 23
    target 776
  ]
  edge [
    source 23
    target 777
  ]
  edge [
    source 23
    target 778
  ]
  edge [
    source 23
    target 779
  ]
  edge [
    source 23
    target 780
  ]
  edge [
    source 23
    target 781
  ]
  edge [
    source 23
    target 782
  ]
  edge [
    source 23
    target 783
  ]
  edge [
    source 23
    target 784
  ]
  edge [
    source 23
    target 785
  ]
  edge [
    source 23
    target 786
  ]
  edge [
    source 23
    target 787
  ]
  edge [
    source 23
    target 788
  ]
  edge [
    source 23
    target 789
  ]
  edge [
    source 23
    target 790
  ]
  edge [
    source 23
    target 791
  ]
  edge [
    source 23
    target 792
  ]
  edge [
    source 23
    target 793
  ]
  edge [
    source 23
    target 346
  ]
  edge [
    source 23
    target 794
  ]
  edge [
    source 23
    target 795
  ]
  edge [
    source 23
    target 796
  ]
  edge [
    source 23
    target 797
  ]
  edge [
    source 23
    target 798
  ]
  edge [
    source 23
    target 799
  ]
  edge [
    source 23
    target 800
  ]
  edge [
    source 23
    target 801
  ]
  edge [
    source 23
    target 802
  ]
  edge [
    source 23
    target 39
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 803
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 804
  ]
  edge [
    source 24
    target 312
  ]
  edge [
    source 24
    target 805
  ]
  edge [
    source 24
    target 806
  ]
  edge [
    source 24
    target 807
  ]
  edge [
    source 24
    target 808
  ]
  edge [
    source 24
    target 809
  ]
  edge [
    source 24
    target 810
  ]
  edge [
    source 24
    target 300
  ]
  edge [
    source 24
    target 811
  ]
  edge [
    source 24
    target 812
  ]
  edge [
    source 24
    target 813
  ]
  edge [
    source 24
    target 814
  ]
  edge [
    source 24
    target 815
  ]
  edge [
    source 24
    target 816
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 817
  ]
  edge [
    source 25
    target 818
  ]
  edge [
    source 25
    target 819
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 820
  ]
  edge [
    source 26
    target 803
  ]
  edge [
    source 26
    target 821
  ]
  edge [
    source 26
    target 822
  ]
  edge [
    source 26
    target 823
  ]
  edge [
    source 26
    target 824
  ]
  edge [
    source 26
    target 825
  ]
  edge [
    source 26
    target 826
  ]
  edge [
    source 26
    target 827
  ]
  edge [
    source 26
    target 828
  ]
  edge [
    source 26
    target 829
  ]
  edge [
    source 26
    target 830
  ]
  edge [
    source 26
    target 831
  ]
  edge [
    source 26
    target 832
  ]
  edge [
    source 26
    target 833
  ]
  edge [
    source 26
    target 834
  ]
  edge [
    source 26
    target 835
  ]
  edge [
    source 26
    target 836
  ]
  edge [
    source 26
    target 837
  ]
  edge [
    source 26
    target 838
  ]
  edge [
    source 26
    target 839
  ]
  edge [
    source 26
    target 840
  ]
  edge [
    source 26
    target 841
  ]
  edge [
    source 26
    target 842
  ]
  edge [
    source 26
    target 843
  ]
  edge [
    source 26
    target 844
  ]
  edge [
    source 26
    target 845
  ]
  edge [
    source 26
    target 846
  ]
  edge [
    source 26
    target 847
  ]
  edge [
    source 26
    target 848
  ]
  edge [
    source 26
    target 849
  ]
  edge [
    source 26
    target 850
  ]
  edge [
    source 26
    target 851
  ]
  edge [
    source 26
    target 852
  ]
  edge [
    source 26
    target 853
  ]
  edge [
    source 26
    target 854
  ]
  edge [
    source 26
    target 855
  ]
  edge [
    source 26
    target 856
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 857
  ]
  edge [
    source 27
    target 858
  ]
  edge [
    source 27
    target 124
  ]
  edge [
    source 27
    target 664
  ]
  edge [
    source 27
    target 137
  ]
  edge [
    source 27
    target 859
  ]
  edge [
    source 27
    target 860
  ]
  edge [
    source 27
    target 132
  ]
  edge [
    source 27
    target 663
  ]
  edge [
    source 27
    target 861
  ]
  edge [
    source 27
    target 862
  ]
  edge [
    source 27
    target 863
  ]
  edge [
    source 27
    target 188
  ]
  edge [
    source 27
    target 155
  ]
  edge [
    source 27
    target 189
  ]
  edge [
    source 27
    target 60
  ]
  edge [
    source 27
    target 190
  ]
  edge [
    source 27
    target 191
  ]
  edge [
    source 27
    target 192
  ]
  edge [
    source 27
    target 154
  ]
  edge [
    source 27
    target 145
  ]
  edge [
    source 27
    target 864
  ]
  edge [
    source 27
    target 865
  ]
  edge [
    source 27
    target 796
  ]
  edge [
    source 27
    target 129
  ]
  edge [
    source 27
    target 866
  ]
  edge [
    source 27
    target 130
  ]
  edge [
    source 27
    target 867
  ]
  edge [
    source 27
    target 868
  ]
  edge [
    source 27
    target 869
  ]
  edge [
    source 27
    target 84
  ]
  edge [
    source 27
    target 134
  ]
  edge [
    source 27
    target 144
  ]
  edge [
    source 27
    target 156
  ]
  edge [
    source 27
    target 157
  ]
  edge [
    source 27
    target 870
  ]
  edge [
    source 27
    target 871
  ]
  edge [
    source 27
    target 872
  ]
  edge [
    source 27
    target 724
  ]
  edge [
    source 27
    target 91
  ]
  edge [
    source 27
    target 873
  ]
  edge [
    source 27
    target 874
  ]
  edge [
    source 27
    target 875
  ]
  edge [
    source 27
    target 118
  ]
  edge [
    source 27
    target 876
  ]
  edge [
    source 27
    target 193
  ]
  edge [
    source 27
    target 877
  ]
  edge [
    source 27
    target 766
  ]
  edge [
    source 27
    target 878
  ]
  edge [
    source 27
    target 879
  ]
  edge [
    source 27
    target 880
  ]
  edge [
    source 27
    target 881
  ]
  edge [
    source 27
    target 882
  ]
  edge [
    source 27
    target 883
  ]
  edge [
    source 27
    target 884
  ]
  edge [
    source 27
    target 38
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 885
  ]
  edge [
    source 28
    target 300
  ]
  edge [
    source 28
    target 814
  ]
  edge [
    source 28
    target 815
  ]
  edge [
    source 28
    target 816
  ]
  edge [
    source 28
    target 303
  ]
  edge [
    source 28
    target 304
  ]
  edge [
    source 28
    target 305
  ]
  edge [
    source 28
    target 306
  ]
  edge [
    source 28
    target 307
  ]
  edge [
    source 28
    target 308
  ]
  edge [
    source 28
    target 309
  ]
  edge [
    source 28
    target 310
  ]
  edge [
    source 28
    target 311
  ]
  edge [
    source 28
    target 312
  ]
  edge [
    source 28
    target 804
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 886
  ]
  edge [
    source 29
    target 887
  ]
  edge [
    source 29
    target 888
  ]
  edge [
    source 29
    target 889
  ]
  edge [
    source 29
    target 890
  ]
  edge [
    source 29
    target 891
  ]
  edge [
    source 29
    target 892
  ]
  edge [
    source 29
    target 893
  ]
  edge [
    source 29
    target 894
  ]
  edge [
    source 29
    target 135
  ]
  edge [
    source 29
    target 895
  ]
  edge [
    source 29
    target 896
  ]
  edge [
    source 29
    target 897
  ]
  edge [
    source 29
    target 898
  ]
  edge [
    source 29
    target 899
  ]
  edge [
    source 29
    target 900
  ]
  edge [
    source 29
    target 901
  ]
  edge [
    source 29
    target 902
  ]
  edge [
    source 29
    target 903
  ]
  edge [
    source 29
    target 904
  ]
  edge [
    source 29
    target 905
  ]
  edge [
    source 29
    target 906
  ]
  edge [
    source 29
    target 907
  ]
  edge [
    source 29
    target 908
  ]
  edge [
    source 29
    target 909
  ]
  edge [
    source 29
    target 910
  ]
  edge [
    source 29
    target 911
  ]
  edge [
    source 29
    target 912
  ]
  edge [
    source 29
    target 913
  ]
  edge [
    source 29
    target 866
  ]
  edge [
    source 29
    target 914
  ]
  edge [
    source 29
    target 915
  ]
  edge [
    source 29
    target 916
  ]
  edge [
    source 29
    target 917
  ]
  edge [
    source 29
    target 918
  ]
  edge [
    source 29
    target 919
  ]
  edge [
    source 29
    target 920
  ]
  edge [
    source 29
    target 859
  ]
  edge [
    source 29
    target 921
  ]
  edge [
    source 29
    target 922
  ]
  edge [
    source 29
    target 923
  ]
  edge [
    source 29
    target 802
  ]
  edge [
    source 29
    target 924
  ]
  edge [
    source 29
    target 925
  ]
  edge [
    source 29
    target 295
  ]
  edge [
    source 29
    target 926
  ]
  edge [
    source 29
    target 927
  ]
  edge [
    source 29
    target 928
  ]
  edge [
    source 29
    target 813
  ]
  edge [
    source 29
    target 929
  ]
  edge [
    source 29
    target 930
  ]
  edge [
    source 29
    target 931
  ]
  edge [
    source 29
    target 932
  ]
  edge [
    source 29
    target 933
  ]
  edge [
    source 29
    target 934
  ]
  edge [
    source 29
    target 935
  ]
  edge [
    source 29
    target 936
  ]
  edge [
    source 29
    target 937
  ]
  edge [
    source 29
    target 938
  ]
  edge [
    source 29
    target 939
  ]
  edge [
    source 29
    target 940
  ]
  edge [
    source 29
    target 941
  ]
  edge [
    source 29
    target 942
  ]
  edge [
    source 29
    target 943
  ]
  edge [
    source 29
    target 944
  ]
  edge [
    source 29
    target 945
  ]
  edge [
    source 29
    target 946
  ]
  edge [
    source 29
    target 260
  ]
  edge [
    source 29
    target 947
  ]
  edge [
    source 29
    target 948
  ]
  edge [
    source 29
    target 949
  ]
  edge [
    source 29
    target 950
  ]
  edge [
    source 29
    target 951
  ]
  edge [
    source 29
    target 952
  ]
  edge [
    source 29
    target 953
  ]
  edge [
    source 29
    target 954
  ]
  edge [
    source 29
    target 955
  ]
  edge [
    source 29
    target 956
  ]
  edge [
    source 29
    target 957
  ]
  edge [
    source 29
    target 958
  ]
  edge [
    source 29
    target 959
  ]
  edge [
    source 29
    target 960
  ]
  edge [
    source 29
    target 961
  ]
  edge [
    source 29
    target 962
  ]
  edge [
    source 29
    target 963
  ]
  edge [
    source 29
    target 964
  ]
  edge [
    source 29
    target 965
  ]
  edge [
    source 29
    target 966
  ]
  edge [
    source 29
    target 967
  ]
  edge [
    source 29
    target 968
  ]
  edge [
    source 29
    target 969
  ]
  edge [
    source 29
    target 970
  ]
  edge [
    source 29
    target 971
  ]
  edge [
    source 29
    target 972
  ]
  edge [
    source 29
    target 973
  ]
  edge [
    source 29
    target 974
  ]
  edge [
    source 29
    target 975
  ]
  edge [
    source 29
    target 976
  ]
  edge [
    source 29
    target 977
  ]
  edge [
    source 29
    target 518
  ]
  edge [
    source 29
    target 978
  ]
  edge [
    source 29
    target 175
  ]
  edge [
    source 29
    target 979
  ]
  edge [
    source 29
    target 520
  ]
  edge [
    source 29
    target 231
  ]
  edge [
    source 29
    target 980
  ]
  edge [
    source 29
    target 981
  ]
  edge [
    source 29
    target 982
  ]
  edge [
    source 29
    target 983
  ]
  edge [
    source 29
    target 274
  ]
  edge [
    source 29
    target 543
  ]
  edge [
    source 29
    target 984
  ]
  edge [
    source 29
    target 985
  ]
  edge [
    source 29
    target 986
  ]
  edge [
    source 29
    target 987
  ]
  edge [
    source 29
    target 988
  ]
  edge [
    source 29
    target 989
  ]
  edge [
    source 29
    target 172
  ]
  edge [
    source 29
    target 502
  ]
  edge [
    source 29
    target 990
  ]
  edge [
    source 29
    target 991
  ]
  edge [
    source 29
    target 992
  ]
  edge [
    source 29
    target 993
  ]
  edge [
    source 29
    target 994
  ]
  edge [
    source 29
    target 995
  ]
  edge [
    source 29
    target 996
  ]
  edge [
    source 29
    target 997
  ]
  edge [
    source 29
    target 998
  ]
  edge [
    source 29
    target 999
  ]
  edge [
    source 29
    target 1000
  ]
  edge [
    source 29
    target 343
  ]
  edge [
    source 29
    target 1001
  ]
  edge [
    source 29
    target 1002
  ]
  edge [
    source 29
    target 1003
  ]
  edge [
    source 29
    target 1004
  ]
  edge [
    source 29
    target 1005
  ]
  edge [
    source 29
    target 1006
  ]
  edge [
    source 29
    target 1007
  ]
  edge [
    source 29
    target 1008
  ]
  edge [
    source 29
    target 1009
  ]
  edge [
    source 29
    target 1010
  ]
  edge [
    source 29
    target 1011
  ]
  edge [
    source 29
    target 1012
  ]
  edge [
    source 29
    target 1013
  ]
  edge [
    source 29
    target 1014
  ]
  edge [
    source 29
    target 1015
  ]
  edge [
    source 29
    target 1016
  ]
  edge [
    source 29
    target 1017
  ]
  edge [
    source 29
    target 1018
  ]
  edge [
    source 29
    target 1019
  ]
  edge [
    source 29
    target 1020
  ]
  edge [
    source 29
    target 1021
  ]
  edge [
    source 29
    target 1022
  ]
  edge [
    source 29
    target 1023
  ]
  edge [
    source 29
    target 1024
  ]
  edge [
    source 29
    target 1025
  ]
  edge [
    source 29
    target 1026
  ]
  edge [
    source 29
    target 1027
  ]
  edge [
    source 29
    target 1028
  ]
  edge [
    source 29
    target 1029
  ]
  edge [
    source 29
    target 1030
  ]
  edge [
    source 29
    target 1031
  ]
  edge [
    source 29
    target 1032
  ]
  edge [
    source 29
    target 1033
  ]
  edge [
    source 29
    target 1034
  ]
  edge [
    source 29
    target 1035
  ]
  edge [
    source 29
    target 1036
  ]
  edge [
    source 29
    target 1037
  ]
  edge [
    source 29
    target 1038
  ]
  edge [
    source 29
    target 1039
  ]
  edge [
    source 29
    target 1040
  ]
  edge [
    source 29
    target 1041
  ]
  edge [
    source 29
    target 1042
  ]
  edge [
    source 29
    target 1043
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1044
  ]
  edge [
    source 30
    target 1045
  ]
  edge [
    source 30
    target 479
  ]
  edge [
    source 30
    target 1046
  ]
  edge [
    source 30
    target 1047
  ]
  edge [
    source 30
    target 158
  ]
  edge [
    source 30
    target 499
  ]
  edge [
    source 30
    target 1048
  ]
  edge [
    source 30
    target 193
  ]
  edge [
    source 30
    target 1049
  ]
  edge [
    source 30
    target 1050
  ]
  edge [
    source 30
    target 1051
  ]
  edge [
    source 30
    target 63
  ]
  edge [
    source 30
    target 1052
  ]
  edge [
    source 30
    target 1053
  ]
  edge [
    source 30
    target 1054
  ]
  edge [
    source 30
    target 1055
  ]
  edge [
    source 30
    target 1056
  ]
  edge [
    source 30
    target 1057
  ]
  edge [
    source 30
    target 1058
  ]
  edge [
    source 30
    target 1059
  ]
  edge [
    source 30
    target 1060
  ]
  edge [
    source 30
    target 1061
  ]
  edge [
    source 30
    target 1062
  ]
  edge [
    source 30
    target 1063
  ]
  edge [
    source 30
    target 121
  ]
  edge [
    source 30
    target 622
  ]
  edge [
    source 30
    target 93
  ]
  edge [
    source 30
    target 1064
  ]
  edge [
    source 30
    target 1065
  ]
  edge [
    source 30
    target 1066
  ]
  edge [
    source 30
    target 1067
  ]
  edge [
    source 30
    target 1068
  ]
  edge [
    source 30
    target 1069
  ]
  edge [
    source 30
    target 1070
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 41
  ]
  edge [
    source 33
    target 42
  ]
  edge [
    source 33
    target 1071
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 910
  ]
  edge [
    source 35
    target 1072
  ]
  edge [
    source 35
    target 1073
  ]
  edge [
    source 35
    target 1074
  ]
  edge [
    source 35
    target 721
  ]
  edge [
    source 35
    target 1075
  ]
  edge [
    source 35
    target 147
  ]
  edge [
    source 35
    target 1076
  ]
  edge [
    source 35
    target 189
  ]
  edge [
    source 35
    target 1077
  ]
  edge [
    source 35
    target 1078
  ]
  edge [
    source 35
    target 1079
  ]
  edge [
    source 35
    target 912
  ]
  edge [
    source 35
    target 1080
  ]
  edge [
    source 35
    target 1081
  ]
  edge [
    source 35
    target 1082
  ]
  edge [
    source 35
    target 1083
  ]
  edge [
    source 35
    target 1084
  ]
  edge [
    source 35
    target 1085
  ]
  edge [
    source 35
    target 1086
  ]
  edge [
    source 35
    target 1087
  ]
  edge [
    source 35
    target 1088
  ]
  edge [
    source 35
    target 1089
  ]
  edge [
    source 35
    target 1090
  ]
  edge [
    source 35
    target 1091
  ]
  edge [
    source 35
    target 1092
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1093
  ]
  edge [
    source 36
    target 1094
  ]
  edge [
    source 36
    target 1095
  ]
  edge [
    source 36
    target 1096
  ]
  edge [
    source 36
    target 1097
  ]
  edge [
    source 36
    target 1098
  ]
  edge [
    source 36
    target 1099
  ]
  edge [
    source 36
    target 1100
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1101
  ]
  edge [
    source 37
    target 1102
  ]
  edge [
    source 37
    target 1103
  ]
  edge [
    source 37
    target 1104
  ]
  edge [
    source 37
    target 1105
  ]
  edge [
    source 37
    target 1106
  ]
  edge [
    source 37
    target 1107
  ]
  edge [
    source 37
    target 1108
  ]
  edge [
    source 37
    target 1109
  ]
  edge [
    source 37
    target 1110
  ]
  edge [
    source 37
    target 1111
  ]
  edge [
    source 37
    target 1112
  ]
  edge [
    source 37
    target 1113
  ]
  edge [
    source 37
    target 1114
  ]
  edge [
    source 37
    target 1115
  ]
  edge [
    source 37
    target 1116
  ]
  edge [
    source 37
    target 1117
  ]
  edge [
    source 37
    target 1118
  ]
  edge [
    source 37
    target 1119
  ]
  edge [
    source 37
    target 1120
  ]
  edge [
    source 37
    target 1121
  ]
  edge [
    source 37
    target 906
  ]
  edge [
    source 37
    target 1122
  ]
  edge [
    source 37
    target 1123
  ]
  edge [
    source 37
    target 1124
  ]
  edge [
    source 37
    target 1125
  ]
  edge [
    source 37
    target 1126
  ]
  edge [
    source 37
    target 1127
  ]
  edge [
    source 37
    target 1128
  ]
  edge [
    source 37
    target 1129
  ]
  edge [
    source 37
    target 1130
  ]
  edge [
    source 37
    target 1131
  ]
  edge [
    source 37
    target 1132
  ]
  edge [
    source 37
    target 1133
  ]
  edge [
    source 37
    target 1134
  ]
  edge [
    source 37
    target 1135
  ]
  edge [
    source 37
    target 1136
  ]
  edge [
    source 37
    target 1137
  ]
  edge [
    source 37
    target 1138
  ]
  edge [
    source 37
    target 1139
  ]
  edge [
    source 37
    target 1140
  ]
  edge [
    source 37
    target 1141
  ]
  edge [
    source 37
    target 1142
  ]
  edge [
    source 37
    target 1143
  ]
  edge [
    source 37
    target 1144
  ]
  edge [
    source 37
    target 1145
  ]
  edge [
    source 37
    target 1146
  ]
  edge [
    source 37
    target 1147
  ]
  edge [
    source 37
    target 1148
  ]
  edge [
    source 37
    target 1149
  ]
  edge [
    source 37
    target 1150
  ]
  edge [
    source 37
    target 1151
  ]
  edge [
    source 37
    target 1152
  ]
  edge [
    source 37
    target 1153
  ]
  edge [
    source 38
    target 168
  ]
  edge [
    source 38
    target 170
  ]
  edge [
    source 38
    target 130
  ]
  edge [
    source 38
    target 282
  ]
  edge [
    source 38
    target 294
  ]
  edge [
    source 38
    target 60
  ]
  edge [
    source 38
    target 525
  ]
  edge [
    source 38
    target 526
  ]
  edge [
    source 38
    target 527
  ]
  edge [
    source 38
    target 528
  ]
  edge [
    source 38
    target 122
  ]
  edge [
    source 38
    target 169
  ]
  edge [
    source 38
    target 171
  ]
  edge [
    source 38
    target 142
  ]
  edge [
    source 38
    target 172
  ]
  edge [
    source 38
    target 173
  ]
  edge [
    source 38
    target 174
  ]
  edge [
    source 38
    target 1154
  ]
  edge [
    source 38
    target 1155
  ]
  edge [
    source 38
    target 1156
  ]
  edge [
    source 38
    target 1157
  ]
  edge [
    source 38
    target 1158
  ]
  edge [
    source 38
    target 258
  ]
  edge [
    source 38
    target 1159
  ]
  edge [
    source 38
    target 1075
  ]
  edge [
    source 38
    target 1160
  ]
  edge [
    source 38
    target 1161
  ]
  edge [
    source 38
    target 1162
  ]
  edge [
    source 38
    target 1163
  ]
  edge [
    source 38
    target 1164
  ]
  edge [
    source 38
    target 1165
  ]
  edge [
    source 38
    target 1166
  ]
  edge [
    source 38
    target 1167
  ]
  edge [
    source 38
    target 1168
  ]
  edge [
    source 38
    target 296
  ]
  edge [
    source 38
    target 1169
  ]
  edge [
    source 38
    target 123
  ]
  edge [
    source 38
    target 1170
  ]
  edge [
    source 38
    target 1171
  ]
  edge [
    source 38
    target 1172
  ]
  edge [
    source 38
    target 1173
  ]
  edge [
    source 38
    target 1174
  ]
  edge [
    source 38
    target 121
  ]
  edge [
    source 38
    target 1175
  ]
  edge [
    source 38
    target 1176
  ]
  edge [
    source 38
    target 1177
  ]
  edge [
    source 38
    target 1178
  ]
  edge [
    source 38
    target 84
  ]
  edge [
    source 38
    target 90
  ]
  edge [
    source 38
    target 1179
  ]
  edge [
    source 38
    target 1180
  ]
  edge [
    source 38
    target 1181
  ]
  edge [
    source 38
    target 1182
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1183
  ]
  edge [
    source 39
    target 454
  ]
  edge [
    source 39
    target 1184
  ]
  edge [
    source 39
    target 1185
  ]
  edge [
    source 39
    target 1186
  ]
  edge [
    source 39
    target 1187
  ]
  edge [
    source 39
    target 1188
  ]
  edge [
    source 39
    target 1189
  ]
  edge [
    source 39
    target 1190
  ]
  edge [
    source 39
    target 1191
  ]
  edge [
    source 39
    target 1192
  ]
  edge [
    source 39
    target 1193
  ]
  edge [
    source 39
    target 1194
  ]
  edge [
    source 39
    target 1195
  ]
  edge [
    source 39
    target 1196
  ]
  edge [
    source 39
    target 1197
  ]
  edge [
    source 39
    target 1198
  ]
  edge [
    source 39
    target 1199
  ]
  edge [
    source 39
    target 405
  ]
  edge [
    source 39
    target 1200
  ]
  edge [
    source 39
    target 1201
  ]
  edge [
    source 39
    target 893
  ]
  edge [
    source 39
    target 1202
  ]
  edge [
    source 39
    target 1203
  ]
  edge [
    source 39
    target 1204
  ]
  edge [
    source 39
    target 1205
  ]
  edge [
    source 39
    target 393
  ]
  edge [
    source 39
    target 1206
  ]
  edge [
    source 39
    target 1207
  ]
  edge [
    source 39
    target 622
  ]
  edge [
    source 39
    target 1208
  ]
  edge [
    source 39
    target 1209
  ]
  edge [
    source 39
    target 1210
  ]
  edge [
    source 39
    target 1211
  ]
  edge [
    source 39
    target 1212
  ]
  edge [
    source 39
    target 1213
  ]
  edge [
    source 39
    target 1214
  ]
  edge [
    source 39
    target 1215
  ]
  edge [
    source 39
    target 1216
  ]
  edge [
    source 39
    target 1217
  ]
  edge [
    source 39
    target 1218
  ]
  edge [
    source 39
    target 1219
  ]
  edge [
    source 39
    target 1220
  ]
  edge [
    source 39
    target 1221
  ]
  edge [
    source 39
    target 1222
  ]
  edge [
    source 39
    target 1223
  ]
  edge [
    source 39
    target 901
  ]
  edge [
    source 39
    target 1224
  ]
  edge [
    source 39
    target 1225
  ]
  edge [
    source 39
    target 1226
  ]
  edge [
    source 39
    target 322
  ]
  edge [
    source 39
    target 1227
  ]
  edge [
    source 39
    target 1228
  ]
  edge [
    source 39
    target 1229
  ]
  edge [
    source 39
    target 1230
  ]
  edge [
    source 39
    target 803
  ]
  edge [
    source 39
    target 899
  ]
  edge [
    source 39
    target 900
  ]
  edge [
    source 39
    target 902
  ]
  edge [
    source 39
    target 903
  ]
  edge [
    source 39
    target 904
  ]
  edge [
    source 39
    target 905
  ]
  edge [
    source 39
    target 906
  ]
  edge [
    source 39
    target 907
  ]
  edge [
    source 39
    target 908
  ]
  edge [
    source 39
    target 909
  ]
  edge [
    source 39
    target 910
  ]
  edge [
    source 39
    target 911
  ]
  edge [
    source 39
    target 912
  ]
  edge [
    source 39
    target 913
  ]
  edge [
    source 39
    target 866
  ]
  edge [
    source 39
    target 914
  ]
  edge [
    source 39
    target 915
  ]
  edge [
    source 39
    target 916
  ]
  edge [
    source 39
    target 917
  ]
  edge [
    source 39
    target 918
  ]
  edge [
    source 39
    target 919
  ]
  edge [
    source 39
    target 920
  ]
  edge [
    source 39
    target 859
  ]
  edge [
    source 39
    target 921
  ]
  edge [
    source 39
    target 922
  ]
  edge [
    source 39
    target 923
  ]
  edge [
    source 39
    target 802
  ]
  edge [
    source 39
    target 924
  ]
  edge [
    source 39
    target 925
  ]
  edge [
    source 39
    target 295
  ]
  edge [
    source 39
    target 926
  ]
  edge [
    source 39
    target 927
  ]
  edge [
    source 39
    target 928
  ]
  edge [
    source 39
    target 813
  ]
  edge [
    source 39
    target 1231
  ]
  edge [
    source 39
    target 1232
  ]
  edge [
    source 39
    target 1233
  ]
  edge [
    source 39
    target 1234
  ]
  edge [
    source 39
    target 1235
  ]
  edge [
    source 39
    target 1236
  ]
  edge [
    source 39
    target 1237
  ]
  edge [
    source 39
    target 1238
  ]
  edge [
    source 39
    target 1239
  ]
  edge [
    source 39
    target 1240
  ]
  edge [
    source 39
    target 1241
  ]
  edge [
    source 39
    target 1242
  ]
  edge [
    source 39
    target 1243
  ]
  edge [
    source 39
    target 1244
  ]
  edge [
    source 39
    target 1245
  ]
  edge [
    source 39
    target 1246
  ]
  edge [
    source 39
    target 1247
  ]
  edge [
    source 39
    target 1248
  ]
  edge [
    source 39
    target 1249
  ]
  edge [
    source 39
    target 95
  ]
  edge [
    source 39
    target 1250
  ]
  edge [
    source 39
    target 1251
  ]
  edge [
    source 39
    target 1252
  ]
  edge [
    source 39
    target 1253
  ]
  edge [
    source 39
    target 1254
  ]
  edge [
    source 39
    target 1255
  ]
  edge [
    source 39
    target 1256
  ]
  edge [
    source 39
    target 1257
  ]
  edge [
    source 39
    target 1258
  ]
  edge [
    source 39
    target 1259
  ]
  edge [
    source 39
    target 1260
  ]
  edge [
    source 39
    target 1261
  ]
  edge [
    source 39
    target 650
  ]
  edge [
    source 39
    target 1262
  ]
  edge [
    source 39
    target 1263
  ]
  edge [
    source 39
    target 1264
  ]
  edge [
    source 39
    target 1265
  ]
  edge [
    source 39
    target 1266
  ]
  edge [
    source 39
    target 1267
  ]
  edge [
    source 39
    target 1268
  ]
  edge [
    source 39
    target 1269
  ]
  edge [
    source 39
    target 1270
  ]
  edge [
    source 39
    target 1271
  ]
  edge [
    source 39
    target 1272
  ]
  edge [
    source 39
    target 1273
  ]
  edge [
    source 39
    target 1274
  ]
  edge [
    source 39
    target 1275
  ]
  edge [
    source 39
    target 1276
  ]
  edge [
    source 39
    target 1277
  ]
  edge [
    source 39
    target 611
  ]
  edge [
    source 39
    target 1278
  ]
  edge [
    source 39
    target 1279
  ]
  edge [
    source 39
    target 1280
  ]
  edge [
    source 39
    target 1281
  ]
  edge [
    source 39
    target 1282
  ]
  edge [
    source 39
    target 1283
  ]
  edge [
    source 39
    target 1284
  ]
  edge [
    source 39
    target 1285
  ]
  edge [
    source 39
    target 1286
  ]
  edge [
    source 39
    target 1287
  ]
  edge [
    source 39
    target 1288
  ]
  edge [
    source 39
    target 704
  ]
  edge [
    source 39
    target 1289
  ]
  edge [
    source 39
    target 1290
  ]
  edge [
    source 39
    target 1291
  ]
  edge [
    source 39
    target 1292
  ]
  edge [
    source 39
    target 1065
  ]
  edge [
    source 39
    target 1293
  ]
  edge [
    source 39
    target 1294
  ]
  edge [
    source 39
    target 1295
  ]
  edge [
    source 39
    target 1296
  ]
  edge [
    source 39
    target 1297
  ]
  edge [
    source 39
    target 1298
  ]
  edge [
    source 39
    target 1299
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1300
  ]
  edge [
    source 40
    target 1301
  ]
  edge [
    source 40
    target 1302
  ]
  edge [
    source 40
    target 1303
  ]
  edge [
    source 40
    target 1304
  ]
  edge [
    source 41
    target 676
  ]
  edge [
    source 41
    target 820
  ]
  edge [
    source 41
    target 1305
  ]
  edge [
    source 41
    target 677
  ]
  edge [
    source 41
    target 499
  ]
  edge [
    source 41
    target 1306
  ]
  edge [
    source 41
    target 1307
  ]
  edge [
    source 41
    target 1308
  ]
  edge [
    source 41
    target 1309
  ]
  edge [
    source 41
    target 1310
  ]
  edge [
    source 41
    target 1311
  ]
  edge [
    source 41
    target 1312
  ]
  edge [
    source 41
    target 1313
  ]
  edge [
    source 41
    target 912
  ]
  edge [
    source 41
    target 1314
  ]
  edge [
    source 41
    target 542
  ]
  edge [
    source 41
    target 1315
  ]
  edge [
    source 41
    target 1316
  ]
  edge [
    source 41
    target 835
  ]
  edge [
    source 41
    target 1317
  ]
  edge [
    source 41
    target 1318
  ]
  edge [
    source 41
    target 1319
  ]
  edge [
    source 41
    target 1320
  ]
  edge [
    source 41
    target 1321
  ]
  edge [
    source 41
    target 1322
  ]
  edge [
    source 41
    target 1323
  ]
  edge [
    source 41
    target 1324
  ]
  edge [
    source 41
    target 1325
  ]
  edge [
    source 41
    target 1326
  ]
  edge [
    source 41
    target 1327
  ]
  edge [
    source 41
    target 1328
  ]
  edge [
    source 41
    target 1329
  ]
  edge [
    source 41
    target 1330
  ]
  edge [
    source 41
    target 705
  ]
  edge [
    source 41
    target 1331
  ]
  edge [
    source 41
    target 1332
  ]
  edge [
    source 41
    target 893
  ]
  edge [
    source 41
    target 1333
  ]
  edge [
    source 41
    target 1334
  ]
  edge [
    source 41
    target 1335
  ]
  edge [
    source 41
    target 1336
  ]
  edge [
    source 41
    target 1337
  ]
  edge [
    source 41
    target 1338
  ]
  edge [
    source 41
    target 1339
  ]
  edge [
    source 41
    target 1340
  ]
  edge [
    source 41
    target 1341
  ]
  edge [
    source 41
    target 1342
  ]
  edge [
    source 41
    target 918
  ]
  edge [
    source 41
    target 899
  ]
  edge [
    source 41
    target 1343
  ]
  edge [
    source 41
    target 910
  ]
  edge [
    source 41
    target 911
  ]
  edge [
    source 41
    target 913
  ]
  edge [
    source 41
    target 866
  ]
  edge [
    source 41
    target 903
  ]
  edge [
    source 41
    target 904
  ]
  edge [
    source 41
    target 905
  ]
  edge [
    source 41
    target 916
  ]
  edge [
    source 41
    target 1344
  ]
  edge [
    source 41
    target 1345
  ]
  edge [
    source 41
    target 1346
  ]
  edge [
    source 41
    target 1347
  ]
  edge [
    source 41
    target 1348
  ]
  edge [
    source 41
    target 1349
  ]
  edge [
    source 41
    target 1350
  ]
  edge [
    source 41
    target 1351
  ]
  edge [
    source 41
    target 1352
  ]
  edge [
    source 41
    target 1353
  ]
  edge [
    source 41
    target 1354
  ]
  edge [
    source 41
    target 1355
  ]
  edge [
    source 41
    target 1356
  ]
  edge [
    source 41
    target 1357
  ]
  edge [
    source 41
    target 1358
  ]
  edge [
    source 41
    target 1359
  ]
  edge [
    source 41
    target 1360
  ]
  edge [
    source 41
    target 1361
  ]
  edge [
    source 41
    target 1362
  ]
  edge [
    source 41
    target 1363
  ]
  edge [
    source 41
    target 1364
  ]
  edge [
    source 41
    target 111
  ]
  edge [
    source 41
    target 1365
  ]
  edge [
    source 41
    target 1366
  ]
  edge [
    source 41
    target 1367
  ]
  edge [
    source 41
    target 1368
  ]
  edge [
    source 41
    target 1045
  ]
  edge [
    source 41
    target 1369
  ]
  edge [
    source 41
    target 1370
  ]
  edge [
    source 41
    target 1371
  ]
  edge [
    source 41
    target 1372
  ]
  edge [
    source 41
    target 1373
  ]
  edge [
    source 41
    target 1374
  ]
  edge [
    source 41
    target 1375
  ]
  edge [
    source 41
    target 1376
  ]
  edge [
    source 41
    target 1377
  ]
  edge [
    source 41
    target 1378
  ]
  edge [
    source 41
    target 1058
  ]
  edge [
    source 41
    target 922
  ]
  edge [
    source 41
    target 1379
  ]
  edge [
    source 41
    target 1380
  ]
  edge [
    source 41
    target 1381
  ]
  edge [
    source 41
    target 1382
  ]
  edge [
    source 41
    target 1383
  ]
  edge [
    source 41
    target 1384
  ]
  edge [
    source 41
    target 1385
  ]
  edge [
    source 41
    target 900
  ]
  edge [
    source 41
    target 1386
  ]
  edge [
    source 41
    target 1387
  ]
  edge [
    source 41
    target 1388
  ]
  edge [
    source 41
    target 1389
  ]
  edge [
    source 41
    target 1390
  ]
  edge [
    source 41
    target 1391
  ]
  edge [
    source 41
    target 1392
  ]
  edge [
    source 41
    target 1393
  ]
  edge [
    source 41
    target 295
  ]
  edge [
    source 41
    target 1394
  ]
  edge [
    source 41
    target 1395
  ]
  edge [
    source 41
    target 105
  ]
  edge [
    source 41
    target 1303
  ]
  edge [
    source 41
    target 1396
  ]
  edge [
    source 41
    target 1397
  ]
  edge [
    source 41
    target 901
  ]
  edge [
    source 41
    target 902
  ]
  edge [
    source 41
    target 906
  ]
  edge [
    source 41
    target 907
  ]
  edge [
    source 41
    target 908
  ]
  edge [
    source 41
    target 909
  ]
  edge [
    source 41
    target 914
  ]
  edge [
    source 41
    target 915
  ]
  edge [
    source 41
    target 917
  ]
  edge [
    source 41
    target 919
  ]
  edge [
    source 41
    target 920
  ]
  edge [
    source 41
    target 859
  ]
  edge [
    source 41
    target 921
  ]
  edge [
    source 41
    target 923
  ]
  edge [
    source 41
    target 802
  ]
  edge [
    source 41
    target 924
  ]
  edge [
    source 41
    target 925
  ]
  edge [
    source 41
    target 926
  ]
  edge [
    source 41
    target 927
  ]
  edge [
    source 41
    target 928
  ]
  edge [
    source 41
    target 813
  ]
  edge [
    source 41
    target 1398
  ]
  edge [
    source 41
    target 1399
  ]
  edge [
    source 41
    target 1400
  ]
  edge [
    source 41
    target 1401
  ]
  edge [
    source 41
    target 1223
  ]
  edge [
    source 41
    target 1402
  ]
  edge [
    source 41
    target 1403
  ]
  edge [
    source 41
    target 1404
  ]
  edge [
    source 41
    target 1405
  ]
  edge [
    source 41
    target 1406
  ]
  edge [
    source 41
    target 1407
  ]
  edge [
    source 41
    target 1408
  ]
  edge [
    source 41
    target 1409
  ]
  edge [
    source 41
    target 1410
  ]
  edge [
    source 41
    target 1411
  ]
  edge [
    source 41
    target 1412
  ]
  edge [
    source 41
    target 1413
  ]
  edge [
    source 41
    target 1414
  ]
  edge [
    source 41
    target 1415
  ]
  edge [
    source 41
    target 1416
  ]
  edge [
    source 41
    target 1417
  ]
  edge [
    source 41
    target 1418
  ]
  edge [
    source 41
    target 1419
  ]
  edge [
    source 41
    target 417
  ]
  edge [
    source 41
    target 1420
  ]
  edge [
    source 41
    target 1421
  ]
  edge [
    source 41
    target 1422
  ]
  edge [
    source 41
    target 93
  ]
  edge [
    source 41
    target 1423
  ]
  edge [
    source 41
    target 1424
  ]
  edge [
    source 41
    target 1425
  ]
  edge [
    source 41
    target 1426
  ]
  edge [
    source 41
    target 1427
  ]
  edge [
    source 41
    target 1428
  ]
  edge [
    source 41
    target 1429
  ]
  edge [
    source 41
    target 1158
  ]
  edge [
    source 41
    target 1430
  ]
  edge [
    source 41
    target 930
  ]
  edge [
    source 41
    target 1431
  ]
  edge [
    source 41
    target 67
  ]
  edge [
    source 41
    target 1432
  ]
  edge [
    source 41
    target 1433
  ]
  edge [
    source 41
    target 1434
  ]
  edge [
    source 41
    target 1435
  ]
  edge [
    source 41
    target 1300
  ]
  edge [
    source 41
    target 1436
  ]
  edge [
    source 41
    target 1437
  ]
  edge [
    source 41
    target 1438
  ]
  edge [
    source 41
    target 1439
  ]
  edge [
    source 41
    target 1440
  ]
  edge [
    source 41
    target 699
  ]
  edge [
    source 41
    target 1441
  ]
  edge [
    source 41
    target 1442
  ]
  edge [
    source 41
    target 571
  ]
  edge [
    source 41
    target 1443
  ]
  edge [
    source 41
    target 561
  ]
  edge [
    source 41
    target 1444
  ]
  edge [
    source 41
    target 563
  ]
  edge [
    source 41
    target 1445
  ]
  edge [
    source 41
    target 1446
  ]
  edge [
    source 41
    target 1447
  ]
  edge [
    source 41
    target 1246
  ]
  edge [
    source 41
    target 1448
  ]
  edge [
    source 41
    target 1304
  ]
  edge [
    source 41
    target 1449
  ]
  edge [
    source 41
    target 332
  ]
  edge [
    source 41
    target 1450
  ]
  edge [
    source 41
    target 1451
  ]
  edge [
    source 41
    target 1452
  ]
  edge [
    source 41
    target 1453
  ]
  edge [
    source 41
    target 1454
  ]
  edge [
    source 41
    target 1455
  ]
  edge [
    source 41
    target 1456
  ]
  edge [
    source 41
    target 1457
  ]
  edge [
    source 41
    target 1458
  ]
  edge [
    source 41
    target 1459
  ]
  edge [
    source 41
    target 1460
  ]
  edge [
    source 41
    target 1461
  ]
  edge [
    source 41
    target 1462
  ]
  edge [
    source 41
    target 1463
  ]
  edge [
    source 41
    target 1464
  ]
  edge [
    source 41
    target 1465
  ]
  edge [
    source 41
    target 1466
  ]
  edge [
    source 41
    target 1467
  ]
  edge [
    source 41
    target 1468
  ]
  edge [
    source 41
    target 1469
  ]
  edge [
    source 41
    target 1470
  ]
  edge [
    source 41
    target 1471
  ]
  edge [
    source 41
    target 1472
  ]
  edge [
    source 41
    target 1473
  ]
  edge [
    source 41
    target 201
  ]
  edge [
    source 41
    target 1474
  ]
  edge [
    source 41
    target 1475
  ]
  edge [
    source 41
    target 1476
  ]
  edge [
    source 41
    target 1477
  ]
  edge [
    source 41
    target 1478
  ]
  edge [
    source 41
    target 1479
  ]
  edge [
    source 41
    target 1480
  ]
  edge [
    source 41
    target 1481
  ]
  edge [
    source 41
    target 1482
  ]
  edge [
    source 41
    target 1483
  ]
  edge [
    source 41
    target 750
  ]
  edge [
    source 41
    target 1484
  ]
  edge [
    source 41
    target 1485
  ]
  edge [
    source 41
    target 1486
  ]
  edge [
    source 41
    target 1487
  ]
  edge [
    source 41
    target 1488
  ]
  edge [
    source 41
    target 1489
  ]
  edge [
    source 41
    target 1490
  ]
  edge [
    source 41
    target 1491
  ]
  edge [
    source 41
    target 1492
  ]
  edge [
    source 41
    target 1493
  ]
  edge [
    source 41
    target 1494
  ]
  edge [
    source 41
    target 1495
  ]
  edge [
    source 41
    target 1496
  ]
  edge [
    source 41
    target 1497
  ]
  edge [
    source 41
    target 1498
  ]
  edge [
    source 41
    target 1499
  ]
  edge [
    source 41
    target 1500
  ]
  edge [
    source 41
    target 1501
  ]
  edge [
    source 41
    target 1502
  ]
  edge [
    source 41
    target 1503
  ]
  edge [
    source 41
    target 1504
  ]
  edge [
    source 41
    target 1505
  ]
  edge [
    source 41
    target 1087
  ]
  edge [
    source 41
    target 1506
  ]
  edge [
    source 41
    target 803
  ]
  edge [
    source 41
    target 1507
  ]
  edge [
    source 41
    target 1508
  ]
  edge [
    source 41
    target 1509
  ]
  edge [
    source 41
    target 1510
  ]
  edge [
    source 41
    target 1511
  ]
  edge [
    source 41
    target 1512
  ]
  edge [
    source 41
    target 1513
  ]
  edge [
    source 41
    target 1514
  ]
  edge [
    source 41
    target 1515
  ]
  edge [
    source 41
    target 1516
  ]
  edge [
    source 41
    target 1517
  ]
  edge [
    source 41
    target 660
  ]
  edge [
    source 41
    target 1518
  ]
  edge [
    source 41
    target 1519
  ]
  edge [
    source 41
    target 1520
  ]
  edge [
    source 41
    target 1521
  ]
  edge [
    source 41
    target 1522
  ]
  edge [
    source 41
    target 1523
  ]
  edge [
    source 41
    target 1524
  ]
  edge [
    source 41
    target 1525
  ]
  edge [
    source 41
    target 1526
  ]
  edge [
    source 41
    target 1527
  ]
  edge [
    source 41
    target 1528
  ]
  edge [
    source 41
    target 1529
  ]
  edge [
    source 41
    target 1530
  ]
  edge [
    source 41
    target 1531
  ]
  edge [
    source 41
    target 1532
  ]
  edge [
    source 41
    target 1533
  ]
  edge [
    source 41
    target 1534
  ]
  edge [
    source 41
    target 1535
  ]
  edge [
    source 41
    target 1536
  ]
  edge [
    source 41
    target 1537
  ]
  edge [
    source 41
    target 1538
  ]
  edge [
    source 41
    target 1539
  ]
  edge [
    source 41
    target 1540
  ]
  edge [
    source 41
    target 1541
  ]
  edge [
    source 41
    target 1542
  ]
  edge [
    source 41
    target 1543
  ]
  edge [
    source 41
    target 1544
  ]
  edge [
    source 41
    target 1545
  ]
  edge [
    source 41
    target 1546
  ]
  edge [
    source 41
    target 617
  ]
  edge [
    source 41
    target 1547
  ]
  edge [
    source 41
    target 1548
  ]
  edge [
    source 41
    target 974
  ]
  edge [
    source 41
    target 1549
  ]
  edge [
    source 41
    target 1550
  ]
  edge [
    source 41
    target 1551
  ]
  edge [
    source 41
    target 1552
  ]
  edge [
    source 41
    target 317
  ]
  edge [
    source 41
    target 1553
  ]
  edge [
    source 41
    target 1554
  ]
  edge [
    source 41
    target 855
  ]
  edge [
    source 41
    target 1555
  ]
  edge [
    source 41
    target 1556
  ]
  edge [
    source 41
    target 1557
  ]
  edge [
    source 41
    target 1558
  ]
  edge [
    source 41
    target 1559
  ]
  edge [
    source 41
    target 1560
  ]
  edge [
    source 41
    target 1561
  ]
  edge [
    source 41
    target 1562
  ]
  edge [
    source 41
    target 1563
  ]
  edge [
    source 41
    target 1564
  ]
  edge [
    source 41
    target 1565
  ]
  edge [
    source 41
    target 1566
  ]
  edge [
    source 41
    target 1567
  ]
  edge [
    source 41
    target 1568
  ]
  edge [
    source 41
    target 682
  ]
  edge [
    source 41
    target 1569
  ]
  edge [
    source 41
    target 1570
  ]
  edge [
    source 41
    target 1571
  ]
  edge [
    source 41
    target 1572
  ]
  edge [
    source 41
    target 1573
  ]
  edge [
    source 41
    target 674
  ]
  edge [
    source 41
    target 1574
  ]
  edge [
    source 41
    target 1575
  ]
  edge [
    source 41
    target 1576
  ]
  edge [
    source 41
    target 1577
  ]
  edge [
    source 41
    target 1578
  ]
  edge [
    source 41
    target 1579
  ]
  edge [
    source 41
    target 1580
  ]
  edge [
    source 41
    target 1581
  ]
  edge [
    source 41
    target 1582
  ]
  edge [
    source 41
    target 675
  ]
  edge [
    source 41
    target 1583
  ]
  edge [
    source 41
    target 1584
  ]
  edge [
    source 41
    target 1585
  ]
  edge [
    source 41
    target 1586
  ]
  edge [
    source 41
    target 1587
  ]
  edge [
    source 41
    target 763
  ]
  edge [
    source 41
    target 1588
  ]
  edge [
    source 41
    target 1589
  ]
  edge [
    source 41
    target 1590
  ]
  edge [
    source 41
    target 1591
  ]
  edge [
    source 41
    target 1592
  ]
  edge [
    source 41
    target 1593
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 864
  ]
  edge [
    source 42
    target 1594
  ]
  edge [
    source 42
    target 1595
  ]
  edge [
    source 42
    target 151
  ]
  edge [
    source 42
    target 1596
  ]
  edge [
    source 42
    target 1597
  ]
  edge [
    source 42
    target 84
  ]
  edge [
    source 42
    target 320
  ]
  edge [
    source 42
    target 1598
  ]
  edge [
    source 42
    target 1599
  ]
  edge [
    source 42
    target 134
  ]
  edge [
    source 42
    target 1600
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1601
  ]
  edge [
    source 43
    target 1602
  ]
  edge [
    source 43
    target 1603
  ]
  edge [
    source 43
    target 1604
  ]
  edge [
    source 43
    target 1605
  ]
  edge [
    source 43
    target 147
  ]
  edge [
    source 43
    target 211
  ]
  edge [
    source 43
    target 1171
  ]
  edge [
    source 43
    target 1606
  ]
  edge [
    source 43
    target 1607
  ]
  edge [
    source 43
    target 208
  ]
  edge [
    source 43
    target 1608
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1609
  ]
  edge [
    source 44
    target 1610
  ]
  edge [
    source 44
    target 611
  ]
  edge [
    source 44
    target 1611
  ]
  edge [
    source 44
    target 1612
  ]
  edge [
    source 44
    target 1613
  ]
  edge [
    source 44
    target 1614
  ]
  edge [
    source 44
    target 1615
  ]
  edge [
    source 44
    target 1616
  ]
  edge [
    source 44
    target 1506
  ]
  edge [
    source 44
    target 1617
  ]
  edge [
    source 44
    target 1618
  ]
  edge [
    source 44
    target 1619
  ]
  edge [
    source 44
    target 1620
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 84
  ]
  edge [
    source 45
    target 1621
  ]
  edge [
    source 45
    target 1622
  ]
  edge [
    source 45
    target 1623
  ]
  edge [
    source 45
    target 1624
  ]
  edge [
    source 45
    target 1625
  ]
  edge [
    source 45
    target 1626
  ]
  edge [
    source 45
    target 1627
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 1628
  ]
  edge [
    source 47
    target 1629
  ]
  edge [
    source 47
    target 1630
  ]
  edge [
    source 47
    target 1631
  ]
  edge [
    source 47
    target 1632
  ]
  edge [
    source 47
    target 1633
  ]
  edge [
    source 47
    target 1634
  ]
  edge [
    source 47
    target 1635
  ]
  edge [
    source 47
    target 1636
  ]
  edge [
    source 47
    target 715
  ]
  edge [
    source 47
    target 1637
  ]
  edge [
    source 47
    target 1638
  ]
  edge [
    source 47
    target 1639
  ]
  edge [
    source 47
    target 1640
  ]
]
