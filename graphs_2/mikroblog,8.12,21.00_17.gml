graph [
  node [
    id 0
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wszyscy"
    origin "text"
  ]
  node [
    id 2
    label "ostatni"
    origin "text"
  ]
  node [
    id 3
    label "wpis"
    origin "text"
  ]
  node [
    id 4
    label "temat"
    origin "text"
  ]
  node [
    id 5
    label "stan"
    origin "text"
  ]
  node [
    id 6
    label "zdrowie"
    origin "text"
  ]
  node [
    id 7
    label "moje"
    origin "text"
  ]
  node [
    id 8
    label "syn"
    origin "text"
  ]
  node [
    id 9
    label "wojtek"
    origin "text"
  ]
  node [
    id 10
    label "min&#261;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "ponad"
    origin "text"
  ]
  node [
    id 12
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 13
    label "greet"
  ]
  node [
    id 14
    label "welcome"
  ]
  node [
    id 15
    label "pozdrawia&#263;"
  ]
  node [
    id 16
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 17
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 18
    label "robi&#263;"
  ]
  node [
    id 19
    label "obchodzi&#263;"
  ]
  node [
    id 20
    label "bless"
  ]
  node [
    id 21
    label "kolejny"
  ]
  node [
    id 22
    label "cz&#322;owiek"
  ]
  node [
    id 23
    label "niedawno"
  ]
  node [
    id 24
    label "poprzedni"
  ]
  node [
    id 25
    label "pozosta&#322;y"
  ]
  node [
    id 26
    label "ostatnio"
  ]
  node [
    id 27
    label "sko&#324;czony"
  ]
  node [
    id 28
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 29
    label "aktualny"
  ]
  node [
    id 30
    label "najgorszy"
  ]
  node [
    id 31
    label "istota_&#380;ywa"
  ]
  node [
    id 32
    label "w&#261;tpliwy"
  ]
  node [
    id 33
    label "nast&#281;pnie"
  ]
  node [
    id 34
    label "inny"
  ]
  node [
    id 35
    label "nastopny"
  ]
  node [
    id 36
    label "kolejno"
  ]
  node [
    id 37
    label "kt&#243;ry&#347;"
  ]
  node [
    id 38
    label "przesz&#322;y"
  ]
  node [
    id 39
    label "wcze&#347;niejszy"
  ]
  node [
    id 40
    label "poprzednio"
  ]
  node [
    id 41
    label "w&#261;tpliwie"
  ]
  node [
    id 42
    label "pozorny"
  ]
  node [
    id 43
    label "&#380;ywy"
  ]
  node [
    id 44
    label "ostateczny"
  ]
  node [
    id 45
    label "wa&#380;ny"
  ]
  node [
    id 46
    label "ludzko&#347;&#263;"
  ]
  node [
    id 47
    label "asymilowanie"
  ]
  node [
    id 48
    label "wapniak"
  ]
  node [
    id 49
    label "asymilowa&#263;"
  ]
  node [
    id 50
    label "os&#322;abia&#263;"
  ]
  node [
    id 51
    label "posta&#263;"
  ]
  node [
    id 52
    label "hominid"
  ]
  node [
    id 53
    label "podw&#322;adny"
  ]
  node [
    id 54
    label "os&#322;abianie"
  ]
  node [
    id 55
    label "g&#322;owa"
  ]
  node [
    id 56
    label "figura"
  ]
  node [
    id 57
    label "portrecista"
  ]
  node [
    id 58
    label "dwun&#243;g"
  ]
  node [
    id 59
    label "profanum"
  ]
  node [
    id 60
    label "mikrokosmos"
  ]
  node [
    id 61
    label "nasada"
  ]
  node [
    id 62
    label "duch"
  ]
  node [
    id 63
    label "antropochoria"
  ]
  node [
    id 64
    label "osoba"
  ]
  node [
    id 65
    label "wz&#243;r"
  ]
  node [
    id 66
    label "senior"
  ]
  node [
    id 67
    label "oddzia&#322;ywanie"
  ]
  node [
    id 68
    label "Adam"
  ]
  node [
    id 69
    label "homo_sapiens"
  ]
  node [
    id 70
    label "polifag"
  ]
  node [
    id 71
    label "wykszta&#322;cony"
  ]
  node [
    id 72
    label "dyplomowany"
  ]
  node [
    id 73
    label "wykwalifikowany"
  ]
  node [
    id 74
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 75
    label "kompletny"
  ]
  node [
    id 76
    label "sko&#324;czenie"
  ]
  node [
    id 77
    label "okre&#347;lony"
  ]
  node [
    id 78
    label "wielki"
  ]
  node [
    id 79
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 80
    label "aktualnie"
  ]
  node [
    id 81
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 82
    label "aktualizowanie"
  ]
  node [
    id 83
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 84
    label "uaktualnienie"
  ]
  node [
    id 85
    label "inscription"
  ]
  node [
    id 86
    label "op&#322;ata"
  ]
  node [
    id 87
    label "akt"
  ]
  node [
    id 88
    label "tekst"
  ]
  node [
    id 89
    label "czynno&#347;&#263;"
  ]
  node [
    id 90
    label "entrance"
  ]
  node [
    id 91
    label "ekscerpcja"
  ]
  node [
    id 92
    label "j&#281;zykowo"
  ]
  node [
    id 93
    label "wypowied&#378;"
  ]
  node [
    id 94
    label "redakcja"
  ]
  node [
    id 95
    label "wytw&#243;r"
  ]
  node [
    id 96
    label "pomini&#281;cie"
  ]
  node [
    id 97
    label "dzie&#322;o"
  ]
  node [
    id 98
    label "preparacja"
  ]
  node [
    id 99
    label "odmianka"
  ]
  node [
    id 100
    label "opu&#347;ci&#263;"
  ]
  node [
    id 101
    label "koniektura"
  ]
  node [
    id 102
    label "pisa&#263;"
  ]
  node [
    id 103
    label "obelga"
  ]
  node [
    id 104
    label "kwota"
  ]
  node [
    id 105
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 106
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 107
    label "podnieci&#263;"
  ]
  node [
    id 108
    label "scena"
  ]
  node [
    id 109
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 110
    label "numer"
  ]
  node [
    id 111
    label "po&#380;ycie"
  ]
  node [
    id 112
    label "poj&#281;cie"
  ]
  node [
    id 113
    label "podniecenie"
  ]
  node [
    id 114
    label "nago&#347;&#263;"
  ]
  node [
    id 115
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 116
    label "fascyku&#322;"
  ]
  node [
    id 117
    label "seks"
  ]
  node [
    id 118
    label "podniecanie"
  ]
  node [
    id 119
    label "imisja"
  ]
  node [
    id 120
    label "zwyczaj"
  ]
  node [
    id 121
    label "rozmna&#380;anie"
  ]
  node [
    id 122
    label "ruch_frykcyjny"
  ]
  node [
    id 123
    label "ontologia"
  ]
  node [
    id 124
    label "wydarzenie"
  ]
  node [
    id 125
    label "na_pieska"
  ]
  node [
    id 126
    label "pozycja_misjonarska"
  ]
  node [
    id 127
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 128
    label "fragment"
  ]
  node [
    id 129
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 130
    label "z&#322;&#261;czenie"
  ]
  node [
    id 131
    label "gra_wst&#281;pna"
  ]
  node [
    id 132
    label "erotyka"
  ]
  node [
    id 133
    label "urzeczywistnienie"
  ]
  node [
    id 134
    label "baraszki"
  ]
  node [
    id 135
    label "certificate"
  ]
  node [
    id 136
    label "po&#380;&#261;danie"
  ]
  node [
    id 137
    label "wzw&#243;d"
  ]
  node [
    id 138
    label "funkcja"
  ]
  node [
    id 139
    label "act"
  ]
  node [
    id 140
    label "dokument"
  ]
  node [
    id 141
    label "arystotelizm"
  ]
  node [
    id 142
    label "podnieca&#263;"
  ]
  node [
    id 143
    label "activity"
  ]
  node [
    id 144
    label "bezproblemowy"
  ]
  node [
    id 145
    label "sprawa"
  ]
  node [
    id 146
    label "wyraz_pochodny"
  ]
  node [
    id 147
    label "zboczenie"
  ]
  node [
    id 148
    label "om&#243;wienie"
  ]
  node [
    id 149
    label "cecha"
  ]
  node [
    id 150
    label "rzecz"
  ]
  node [
    id 151
    label "omawia&#263;"
  ]
  node [
    id 152
    label "fraza"
  ]
  node [
    id 153
    label "tre&#347;&#263;"
  ]
  node [
    id 154
    label "entity"
  ]
  node [
    id 155
    label "forum"
  ]
  node [
    id 156
    label "topik"
  ]
  node [
    id 157
    label "tematyka"
  ]
  node [
    id 158
    label "w&#261;tek"
  ]
  node [
    id 159
    label "zbaczanie"
  ]
  node [
    id 160
    label "forma"
  ]
  node [
    id 161
    label "om&#243;wi&#263;"
  ]
  node [
    id 162
    label "omawianie"
  ]
  node [
    id 163
    label "melodia"
  ]
  node [
    id 164
    label "otoczka"
  ]
  node [
    id 165
    label "istota"
  ]
  node [
    id 166
    label "zbacza&#263;"
  ]
  node [
    id 167
    label "zboczy&#263;"
  ]
  node [
    id 168
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 169
    label "zbi&#243;r"
  ]
  node [
    id 170
    label "wypowiedzenie"
  ]
  node [
    id 171
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 172
    label "zdanie"
  ]
  node [
    id 173
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 174
    label "motyw"
  ]
  node [
    id 175
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 176
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 177
    label "informacja"
  ]
  node [
    id 178
    label "zawarto&#347;&#263;"
  ]
  node [
    id 179
    label "kszta&#322;t"
  ]
  node [
    id 180
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 181
    label "jednostka_systematyczna"
  ]
  node [
    id 182
    label "poznanie"
  ]
  node [
    id 183
    label "leksem"
  ]
  node [
    id 184
    label "blaszka"
  ]
  node [
    id 185
    label "kantyzm"
  ]
  node [
    id 186
    label "zdolno&#347;&#263;"
  ]
  node [
    id 187
    label "do&#322;ek"
  ]
  node [
    id 188
    label "gwiazda"
  ]
  node [
    id 189
    label "formality"
  ]
  node [
    id 190
    label "struktura"
  ]
  node [
    id 191
    label "wygl&#261;d"
  ]
  node [
    id 192
    label "mode"
  ]
  node [
    id 193
    label "morfem"
  ]
  node [
    id 194
    label "rdze&#324;"
  ]
  node [
    id 195
    label "kielich"
  ]
  node [
    id 196
    label "ornamentyka"
  ]
  node [
    id 197
    label "pasmo"
  ]
  node [
    id 198
    label "punkt_widzenia"
  ]
  node [
    id 199
    label "naczynie"
  ]
  node [
    id 200
    label "p&#322;at"
  ]
  node [
    id 201
    label "maszyna_drukarska"
  ]
  node [
    id 202
    label "obiekt"
  ]
  node [
    id 203
    label "style"
  ]
  node [
    id 204
    label "linearno&#347;&#263;"
  ]
  node [
    id 205
    label "wyra&#380;enie"
  ]
  node [
    id 206
    label "formacja"
  ]
  node [
    id 207
    label "spirala"
  ]
  node [
    id 208
    label "dyspozycja"
  ]
  node [
    id 209
    label "odmiana"
  ]
  node [
    id 210
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 211
    label "October"
  ]
  node [
    id 212
    label "creation"
  ]
  node [
    id 213
    label "p&#281;tla"
  ]
  node [
    id 214
    label "szablon"
  ]
  node [
    id 215
    label "miniatura"
  ]
  node [
    id 216
    label "zanucenie"
  ]
  node [
    id 217
    label "nuta"
  ]
  node [
    id 218
    label "zakosztowa&#263;"
  ]
  node [
    id 219
    label "zajawka"
  ]
  node [
    id 220
    label "zanuci&#263;"
  ]
  node [
    id 221
    label "emocja"
  ]
  node [
    id 222
    label "oskoma"
  ]
  node [
    id 223
    label "melika"
  ]
  node [
    id 224
    label "nucenie"
  ]
  node [
    id 225
    label "nuci&#263;"
  ]
  node [
    id 226
    label "brzmienie"
  ]
  node [
    id 227
    label "zjawisko"
  ]
  node [
    id 228
    label "taste"
  ]
  node [
    id 229
    label "muzyka"
  ]
  node [
    id 230
    label "inclination"
  ]
  node [
    id 231
    label "charakterystyka"
  ]
  node [
    id 232
    label "m&#322;ot"
  ]
  node [
    id 233
    label "znak"
  ]
  node [
    id 234
    label "drzewo"
  ]
  node [
    id 235
    label "pr&#243;ba"
  ]
  node [
    id 236
    label "attribute"
  ]
  node [
    id 237
    label "marka"
  ]
  node [
    id 238
    label "mentalno&#347;&#263;"
  ]
  node [
    id 239
    label "superego"
  ]
  node [
    id 240
    label "psychika"
  ]
  node [
    id 241
    label "znaczenie"
  ]
  node [
    id 242
    label "wn&#281;trze"
  ]
  node [
    id 243
    label "charakter"
  ]
  node [
    id 244
    label "matter"
  ]
  node [
    id 245
    label "splot"
  ]
  node [
    id 246
    label "ceg&#322;a"
  ]
  node [
    id 247
    label "socket"
  ]
  node [
    id 248
    label "rozmieszczenie"
  ]
  node [
    id 249
    label "fabu&#322;a"
  ]
  node [
    id 250
    label "okrywa"
  ]
  node [
    id 251
    label "kontekst"
  ]
  node [
    id 252
    label "object"
  ]
  node [
    id 253
    label "przedmiot"
  ]
  node [
    id 254
    label "wpadni&#281;cie"
  ]
  node [
    id 255
    label "mienie"
  ]
  node [
    id 256
    label "przyroda"
  ]
  node [
    id 257
    label "kultura"
  ]
  node [
    id 258
    label "wpa&#347;&#263;"
  ]
  node [
    id 259
    label "wpadanie"
  ]
  node [
    id 260
    label "wpada&#263;"
  ]
  node [
    id 261
    label "discussion"
  ]
  node [
    id 262
    label "rozpatrywanie"
  ]
  node [
    id 263
    label "dyskutowanie"
  ]
  node [
    id 264
    label "omowny"
  ]
  node [
    id 265
    label "figura_stylistyczna"
  ]
  node [
    id 266
    label "sformu&#322;owanie"
  ]
  node [
    id 267
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 268
    label "odchodzenie"
  ]
  node [
    id 269
    label "aberrance"
  ]
  node [
    id 270
    label "swerve"
  ]
  node [
    id 271
    label "kierunek"
  ]
  node [
    id 272
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 273
    label "distract"
  ]
  node [
    id 274
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 275
    label "odej&#347;&#263;"
  ]
  node [
    id 276
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 277
    label "twist"
  ]
  node [
    id 278
    label "zmieni&#263;"
  ]
  node [
    id 279
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 280
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 281
    label "przedyskutowa&#263;"
  ]
  node [
    id 282
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 283
    label "publicize"
  ]
  node [
    id 284
    label "digress"
  ]
  node [
    id 285
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 286
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 287
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 288
    label "odchodzi&#263;"
  ]
  node [
    id 289
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 290
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 291
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 292
    label "perversion"
  ]
  node [
    id 293
    label "death"
  ]
  node [
    id 294
    label "odej&#347;cie"
  ]
  node [
    id 295
    label "turn"
  ]
  node [
    id 296
    label "k&#261;t"
  ]
  node [
    id 297
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 298
    label "odchylenie_si&#281;"
  ]
  node [
    id 299
    label "deviation"
  ]
  node [
    id 300
    label "patologia"
  ]
  node [
    id 301
    label "dyskutowa&#263;"
  ]
  node [
    id 302
    label "formu&#322;owa&#263;"
  ]
  node [
    id 303
    label "discourse"
  ]
  node [
    id 304
    label "kognicja"
  ]
  node [
    id 305
    label "rozprawa"
  ]
  node [
    id 306
    label "szczeg&#243;&#322;"
  ]
  node [
    id 307
    label "proposition"
  ]
  node [
    id 308
    label "przes&#322;anka"
  ]
  node [
    id 309
    label "idea"
  ]
  node [
    id 310
    label "paj&#261;k"
  ]
  node [
    id 311
    label "przewodnik"
  ]
  node [
    id 312
    label "odcinek"
  ]
  node [
    id 313
    label "topikowate"
  ]
  node [
    id 314
    label "grupa_dyskusyjna"
  ]
  node [
    id 315
    label "s&#261;d"
  ]
  node [
    id 316
    label "plac"
  ]
  node [
    id 317
    label "bazylika"
  ]
  node [
    id 318
    label "przestrze&#324;"
  ]
  node [
    id 319
    label "miejsce"
  ]
  node [
    id 320
    label "portal"
  ]
  node [
    id 321
    label "konferencja"
  ]
  node [
    id 322
    label "agora"
  ]
  node [
    id 323
    label "grupa"
  ]
  node [
    id 324
    label "strona"
  ]
  node [
    id 325
    label "Ohio"
  ]
  node [
    id 326
    label "wci&#281;cie"
  ]
  node [
    id 327
    label "Nowy_York"
  ]
  node [
    id 328
    label "warstwa"
  ]
  node [
    id 329
    label "samopoczucie"
  ]
  node [
    id 330
    label "Illinois"
  ]
  node [
    id 331
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 332
    label "state"
  ]
  node [
    id 333
    label "Jukatan"
  ]
  node [
    id 334
    label "Kalifornia"
  ]
  node [
    id 335
    label "Wirginia"
  ]
  node [
    id 336
    label "wektor"
  ]
  node [
    id 337
    label "by&#263;"
  ]
  node [
    id 338
    label "Goa"
  ]
  node [
    id 339
    label "Teksas"
  ]
  node [
    id 340
    label "Waszyngton"
  ]
  node [
    id 341
    label "Massachusetts"
  ]
  node [
    id 342
    label "Alaska"
  ]
  node [
    id 343
    label "Arakan"
  ]
  node [
    id 344
    label "Hawaje"
  ]
  node [
    id 345
    label "Maryland"
  ]
  node [
    id 346
    label "punkt"
  ]
  node [
    id 347
    label "Michigan"
  ]
  node [
    id 348
    label "Arizona"
  ]
  node [
    id 349
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 350
    label "Georgia"
  ]
  node [
    id 351
    label "poziom"
  ]
  node [
    id 352
    label "Pensylwania"
  ]
  node [
    id 353
    label "shape"
  ]
  node [
    id 354
    label "Luizjana"
  ]
  node [
    id 355
    label "Nowy_Meksyk"
  ]
  node [
    id 356
    label "Alabama"
  ]
  node [
    id 357
    label "ilo&#347;&#263;"
  ]
  node [
    id 358
    label "Kansas"
  ]
  node [
    id 359
    label "Oregon"
  ]
  node [
    id 360
    label "Oklahoma"
  ]
  node [
    id 361
    label "Floryda"
  ]
  node [
    id 362
    label "jednostka_administracyjna"
  ]
  node [
    id 363
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 364
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 365
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 366
    label "indentation"
  ]
  node [
    id 367
    label "zjedzenie"
  ]
  node [
    id 368
    label "snub"
  ]
  node [
    id 369
    label "warunek_lokalowy"
  ]
  node [
    id 370
    label "location"
  ]
  node [
    id 371
    label "uwaga"
  ]
  node [
    id 372
    label "status"
  ]
  node [
    id 373
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 374
    label "chwila"
  ]
  node [
    id 375
    label "cia&#322;o"
  ]
  node [
    id 376
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 377
    label "praca"
  ]
  node [
    id 378
    label "rz&#261;d"
  ]
  node [
    id 379
    label "sk&#322;adnik"
  ]
  node [
    id 380
    label "warunki"
  ]
  node [
    id 381
    label "sytuacja"
  ]
  node [
    id 382
    label "p&#322;aszczyzna"
  ]
  node [
    id 383
    label "przek&#322;adaniec"
  ]
  node [
    id 384
    label "covering"
  ]
  node [
    id 385
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 386
    label "podwarstwa"
  ]
  node [
    id 387
    label "organizm"
  ]
  node [
    id 388
    label "obiekt_matematyczny"
  ]
  node [
    id 389
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 390
    label "zwrot_wektora"
  ]
  node [
    id 391
    label "vector"
  ]
  node [
    id 392
    label "parametryzacja"
  ]
  node [
    id 393
    label "kwas_deoksyrybonukleinowy"
  ]
  node [
    id 394
    label "po&#322;o&#380;enie"
  ]
  node [
    id 395
    label "ust&#281;p"
  ]
  node [
    id 396
    label "plan"
  ]
  node [
    id 397
    label "problemat"
  ]
  node [
    id 398
    label "plamka"
  ]
  node [
    id 399
    label "stopie&#324;_pisma"
  ]
  node [
    id 400
    label "jednostka"
  ]
  node [
    id 401
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 402
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 403
    label "mark"
  ]
  node [
    id 404
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 405
    label "prosta"
  ]
  node [
    id 406
    label "problematyka"
  ]
  node [
    id 407
    label "zapunktowa&#263;"
  ]
  node [
    id 408
    label "podpunkt"
  ]
  node [
    id 409
    label "wojsko"
  ]
  node [
    id 410
    label "kres"
  ]
  node [
    id 411
    label "point"
  ]
  node [
    id 412
    label "pozycja"
  ]
  node [
    id 413
    label "jako&#347;&#263;"
  ]
  node [
    id 414
    label "wyk&#322;adnik"
  ]
  node [
    id 415
    label "faza"
  ]
  node [
    id 416
    label "szczebel"
  ]
  node [
    id 417
    label "budynek"
  ]
  node [
    id 418
    label "wysoko&#347;&#263;"
  ]
  node [
    id 419
    label "ranga"
  ]
  node [
    id 420
    label "rozmiar"
  ]
  node [
    id 421
    label "part"
  ]
  node [
    id 422
    label "USA"
  ]
  node [
    id 423
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 424
    label "Polinezja"
  ]
  node [
    id 425
    label "Birma"
  ]
  node [
    id 426
    label "Indie_Portugalskie"
  ]
  node [
    id 427
    label "Belize"
  ]
  node [
    id 428
    label "Meksyk"
  ]
  node [
    id 429
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 430
    label "Aleuty"
  ]
  node [
    id 431
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 432
    label "mie&#263;_miejsce"
  ]
  node [
    id 433
    label "equal"
  ]
  node [
    id 434
    label "trwa&#263;"
  ]
  node [
    id 435
    label "chodzi&#263;"
  ]
  node [
    id 436
    label "si&#281;ga&#263;"
  ]
  node [
    id 437
    label "obecno&#347;&#263;"
  ]
  node [
    id 438
    label "stand"
  ]
  node [
    id 439
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 440
    label "uczestniczy&#263;"
  ]
  node [
    id 441
    label "kondycja"
  ]
  node [
    id 442
    label "os&#322;abienie"
  ]
  node [
    id 443
    label "zniszczenie"
  ]
  node [
    id 444
    label "zedrze&#263;"
  ]
  node [
    id 445
    label "niszczenie"
  ]
  node [
    id 446
    label "os&#322;abi&#263;"
  ]
  node [
    id 447
    label "soundness"
  ]
  node [
    id 448
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 449
    label "niszczy&#263;"
  ]
  node [
    id 450
    label "zdarcie"
  ]
  node [
    id 451
    label "firmness"
  ]
  node [
    id 452
    label "rozsypanie_si&#281;"
  ]
  node [
    id 453
    label "zniszczy&#263;"
  ]
  node [
    id 454
    label "situation"
  ]
  node [
    id 455
    label "rank"
  ]
  node [
    id 456
    label "doznanie"
  ]
  node [
    id 457
    label "fatigue_duty"
  ]
  node [
    id 458
    label "zmniejszenie"
  ]
  node [
    id 459
    label "kondycja_fizyczna"
  ]
  node [
    id 460
    label "spowodowanie"
  ]
  node [
    id 461
    label "infirmity"
  ]
  node [
    id 462
    label "s&#322;abszy"
  ]
  node [
    id 463
    label "pogorszenie"
  ]
  node [
    id 464
    label "suppress"
  ]
  node [
    id 465
    label "powodowa&#263;"
  ]
  node [
    id 466
    label "zmniejsza&#263;"
  ]
  node [
    id 467
    label "bate"
  ]
  node [
    id 468
    label "de-escalation"
  ]
  node [
    id 469
    label "powodowanie"
  ]
  node [
    id 470
    label "debilitation"
  ]
  node [
    id 471
    label "zmniejszanie"
  ]
  node [
    id 472
    label "pogarszanie"
  ]
  node [
    id 473
    label "wear"
  ]
  node [
    id 474
    label "destruction"
  ]
  node [
    id 475
    label "zu&#380;ycie"
  ]
  node [
    id 476
    label "attrition"
  ]
  node [
    id 477
    label "zaszkodzenie"
  ]
  node [
    id 478
    label "podpalenie"
  ]
  node [
    id 479
    label "strata"
  ]
  node [
    id 480
    label "spl&#261;drowanie"
  ]
  node [
    id 481
    label "poniszczenie"
  ]
  node [
    id 482
    label "ruin"
  ]
  node [
    id 483
    label "stanie_si&#281;"
  ]
  node [
    id 484
    label "rezultat"
  ]
  node [
    id 485
    label "poniszczenie_si&#281;"
  ]
  node [
    id 486
    label "reduce"
  ]
  node [
    id 487
    label "spowodowa&#263;"
  ]
  node [
    id 488
    label "zmniejszy&#263;"
  ]
  node [
    id 489
    label "cushion"
  ]
  node [
    id 490
    label "health"
  ]
  node [
    id 491
    label "wp&#322;yw"
  ]
  node [
    id 492
    label "destroy"
  ]
  node [
    id 493
    label "uszkadza&#263;"
  ]
  node [
    id 494
    label "szkodzi&#263;"
  ]
  node [
    id 495
    label "mar"
  ]
  node [
    id 496
    label "wygrywa&#263;"
  ]
  node [
    id 497
    label "pamper"
  ]
  node [
    id 498
    label "zaszkodzi&#263;"
  ]
  node [
    id 499
    label "zu&#380;y&#263;"
  ]
  node [
    id 500
    label "spoil"
  ]
  node [
    id 501
    label "consume"
  ]
  node [
    id 502
    label "wygra&#263;"
  ]
  node [
    id 503
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 504
    label "nadwyr&#281;&#380;enie"
  ]
  node [
    id 505
    label "gard&#322;o"
  ]
  node [
    id 506
    label "zranienie"
  ]
  node [
    id 507
    label "s&#322;ony"
  ]
  node [
    id 508
    label "wzi&#281;cie"
  ]
  node [
    id 509
    label "pl&#261;drowanie"
  ]
  node [
    id 510
    label "ravaging"
  ]
  node [
    id 511
    label "gnojenie"
  ]
  node [
    id 512
    label "szkodzenie"
  ]
  node [
    id 513
    label "pustoszenie"
  ]
  node [
    id 514
    label "decay"
  ]
  node [
    id 515
    label "poniewieranie_si&#281;"
  ]
  node [
    id 516
    label "devastation"
  ]
  node [
    id 517
    label "zu&#380;ywanie"
  ]
  node [
    id 518
    label "stawanie_si&#281;"
  ]
  node [
    id 519
    label "zarobi&#263;"
  ]
  node [
    id 520
    label "nadwyr&#281;&#380;y&#263;"
  ]
  node [
    id 521
    label "wzi&#261;&#263;"
  ]
  node [
    id 522
    label "zrani&#263;"
  ]
  node [
    id 523
    label "&#347;ci&#261;gn&#261;&#263;"
  ]
  node [
    id 524
    label "usynowienie"
  ]
  node [
    id 525
    label "dziecko"
  ]
  node [
    id 526
    label "usynawianie"
  ]
  node [
    id 527
    label "utulenie"
  ]
  node [
    id 528
    label "pediatra"
  ]
  node [
    id 529
    label "dzieciak"
  ]
  node [
    id 530
    label "utulanie"
  ]
  node [
    id 531
    label "dzieciarnia"
  ]
  node [
    id 532
    label "niepe&#322;noletni"
  ]
  node [
    id 533
    label "utula&#263;"
  ]
  node [
    id 534
    label "cz&#322;owieczek"
  ]
  node [
    id 535
    label "fledgling"
  ]
  node [
    id 536
    label "zwierz&#281;"
  ]
  node [
    id 537
    label "utuli&#263;"
  ]
  node [
    id 538
    label "m&#322;odzik"
  ]
  node [
    id 539
    label "pedofil"
  ]
  node [
    id 540
    label "m&#322;odziak"
  ]
  node [
    id 541
    label "potomek"
  ]
  node [
    id 542
    label "entliczek-pentliczek"
  ]
  node [
    id 543
    label "potomstwo"
  ]
  node [
    id 544
    label "sraluch"
  ]
  node [
    id 545
    label "przysposabianie"
  ]
  node [
    id 546
    label "przysposobienie"
  ]
  node [
    id 547
    label "adoption"
  ]
  node [
    id 548
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 549
    label "przesta&#263;"
  ]
  node [
    id 550
    label "run"
  ]
  node [
    id 551
    label "die"
  ]
  node [
    id 552
    label "przej&#347;&#263;"
  ]
  node [
    id 553
    label "overwhelm"
  ]
  node [
    id 554
    label "omin&#261;&#263;"
  ]
  node [
    id 555
    label "coating"
  ]
  node [
    id 556
    label "drop"
  ]
  node [
    id 557
    label "sko&#324;czy&#263;"
  ]
  node [
    id 558
    label "leave_office"
  ]
  node [
    id 559
    label "zrobi&#263;"
  ]
  node [
    id 560
    label "fail"
  ]
  node [
    id 561
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 562
    label "ustawa"
  ]
  node [
    id 563
    label "podlec"
  ]
  node [
    id 564
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 565
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 566
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 567
    label "zaliczy&#263;"
  ]
  node [
    id 568
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 569
    label "przeby&#263;"
  ]
  node [
    id 570
    label "dozna&#263;"
  ]
  node [
    id 571
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 572
    label "zacz&#261;&#263;"
  ]
  node [
    id 573
    label "happen"
  ]
  node [
    id 574
    label "pass"
  ]
  node [
    id 575
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 576
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 577
    label "beat"
  ]
  node [
    id 578
    label "absorb"
  ]
  node [
    id 579
    label "przerobi&#263;"
  ]
  node [
    id 580
    label "pique"
  ]
  node [
    id 581
    label "pomin&#261;&#263;"
  ]
  node [
    id 582
    label "wymin&#261;&#263;"
  ]
  node [
    id 583
    label "sidestep"
  ]
  node [
    id 584
    label "zlekcewa&#380;y&#263;"
  ]
  node [
    id 585
    label "unikn&#261;&#263;"
  ]
  node [
    id 586
    label "obej&#347;&#263;"
  ]
  node [
    id 587
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 588
    label "straci&#263;"
  ]
  node [
    id 589
    label "shed"
  ]
  node [
    id 590
    label "popyt"
  ]
  node [
    id 591
    label "tydzie&#324;"
  ]
  node [
    id 592
    label "miech"
  ]
  node [
    id 593
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 594
    label "czas"
  ]
  node [
    id 595
    label "rok"
  ]
  node [
    id 596
    label "kalendy"
  ]
  node [
    id 597
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 598
    label "satelita"
  ]
  node [
    id 599
    label "peryselenium"
  ]
  node [
    id 600
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 601
    label "&#347;wiat&#322;o"
  ]
  node [
    id 602
    label "aposelenium"
  ]
  node [
    id 603
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 604
    label "Tytan"
  ]
  node [
    id 605
    label "moon"
  ]
  node [
    id 606
    label "aparat_fotograficzny"
  ]
  node [
    id 607
    label "bag"
  ]
  node [
    id 608
    label "sakwa"
  ]
  node [
    id 609
    label "torba"
  ]
  node [
    id 610
    label "przyrz&#261;d"
  ]
  node [
    id 611
    label "w&#243;r"
  ]
  node [
    id 612
    label "poprzedzanie"
  ]
  node [
    id 613
    label "czasoprzestrze&#324;"
  ]
  node [
    id 614
    label "laba"
  ]
  node [
    id 615
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 616
    label "chronometria"
  ]
  node [
    id 617
    label "rachuba_czasu"
  ]
  node [
    id 618
    label "przep&#322;ywanie"
  ]
  node [
    id 619
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 620
    label "czasokres"
  ]
  node [
    id 621
    label "odczyt"
  ]
  node [
    id 622
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 623
    label "dzieje"
  ]
  node [
    id 624
    label "kategoria_gramatyczna"
  ]
  node [
    id 625
    label "poprzedzenie"
  ]
  node [
    id 626
    label "trawienie"
  ]
  node [
    id 627
    label "pochodzi&#263;"
  ]
  node [
    id 628
    label "period"
  ]
  node [
    id 629
    label "okres_czasu"
  ]
  node [
    id 630
    label "poprzedza&#263;"
  ]
  node [
    id 631
    label "schy&#322;ek"
  ]
  node [
    id 632
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 633
    label "odwlekanie_si&#281;"
  ]
  node [
    id 634
    label "zegar"
  ]
  node [
    id 635
    label "czwarty_wymiar"
  ]
  node [
    id 636
    label "pochodzenie"
  ]
  node [
    id 637
    label "koniugacja"
  ]
  node [
    id 638
    label "Zeitgeist"
  ]
  node [
    id 639
    label "trawi&#263;"
  ]
  node [
    id 640
    label "pogoda"
  ]
  node [
    id 641
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 642
    label "poprzedzi&#263;"
  ]
  node [
    id 643
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 644
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 645
    label "time_period"
  ]
  node [
    id 646
    label "doba"
  ]
  node [
    id 647
    label "weekend"
  ]
  node [
    id 648
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 649
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 650
    label "p&#243;&#322;rocze"
  ]
  node [
    id 651
    label "martwy_sezon"
  ]
  node [
    id 652
    label "kalendarz"
  ]
  node [
    id 653
    label "cykl_astronomiczny"
  ]
  node [
    id 654
    label "lata"
  ]
  node [
    id 655
    label "pora_roku"
  ]
  node [
    id 656
    label "stulecie"
  ]
  node [
    id 657
    label "kurs"
  ]
  node [
    id 658
    label "jubileusz"
  ]
  node [
    id 659
    label "kwarta&#322;"
  ]
  node [
    id 660
    label "intensywny"
  ]
  node [
    id 661
    label "terapia"
  ]
  node [
    id 662
    label "6"
  ]
  node [
    id 663
    label "brygada"
  ]
  node [
    id 664
    label "powietrznodesantowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 323
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 660
    target 661
  ]
  edge [
    source 662
    target 663
  ]
  edge [
    source 662
    target 664
  ]
  edge [
    source 663
    target 664
  ]
]
