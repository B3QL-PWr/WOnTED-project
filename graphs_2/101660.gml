graph [
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "czytanie"
    origin "text"
  ]
  node [
    id 2
    label "poselski"
    origin "text"
  ]
  node [
    id 3
    label "projekt"
    origin "text"
  ]
  node [
    id 4
    label "ustawa"
    origin "text"
  ]
  node [
    id 5
    label "zmiana"
    origin "text"
  ]
  node [
    id 6
    label "kodeks"
    origin "text"
  ]
  node [
    id 7
    label "sp&#243;&#322;ka"
    origin "text"
  ]
  node [
    id 8
    label "handlowy"
    origin "text"
  ]
  node [
    id 9
    label "koszt"
    origin "text"
  ]
  node [
    id 10
    label "s&#261;dowy"
    origin "text"
  ]
  node [
    id 11
    label "sprawa"
    origin "text"
  ]
  node [
    id 12
    label "cywilny"
    origin "text"
  ]
  node [
    id 13
    label "druk"
    origin "text"
  ]
  node [
    id 14
    label "godzina"
  ]
  node [
    id 15
    label "time"
  ]
  node [
    id 16
    label "kwadrans"
  ]
  node [
    id 17
    label "p&#243;&#322;godzina"
  ]
  node [
    id 18
    label "doba"
  ]
  node [
    id 19
    label "czas"
  ]
  node [
    id 20
    label "jednostka_czasu"
  ]
  node [
    id 21
    label "minuta"
  ]
  node [
    id 22
    label "poczytanie"
  ]
  node [
    id 23
    label "wyczytywanie"
  ]
  node [
    id 24
    label "wczytywanie_si&#281;"
  ]
  node [
    id 25
    label "pokazywanie"
  ]
  node [
    id 26
    label "doczytywanie"
  ]
  node [
    id 27
    label "bycie_w_stanie"
  ]
  node [
    id 28
    label "Biblia"
  ]
  node [
    id 29
    label "recitation"
  ]
  node [
    id 30
    label "reading"
  ]
  node [
    id 31
    label "dysleksja"
  ]
  node [
    id 32
    label "obrz&#261;dek"
  ]
  node [
    id 33
    label "zaczytanie_si&#281;"
  ]
  node [
    id 34
    label "czytywanie"
  ]
  node [
    id 35
    label "oczytywanie_si&#281;"
  ]
  node [
    id 36
    label "poznawanie"
  ]
  node [
    id 37
    label "wczytywanie"
  ]
  node [
    id 38
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 39
    label "przepowiadanie"
  ]
  node [
    id 40
    label "lektor"
  ]
  node [
    id 41
    label "powtarzanie"
  ]
  node [
    id 42
    label "prorokowanie"
  ]
  node [
    id 43
    label "przewidywanie"
  ]
  node [
    id 44
    label "divination"
  ]
  node [
    id 45
    label "auspicje"
  ]
  node [
    id 46
    label "profetyzm"
  ]
  node [
    id 47
    label "show"
  ]
  node [
    id 48
    label "appearance"
  ]
  node [
    id 49
    label "wyraz"
  ]
  node [
    id 50
    label "powodowanie"
  ]
  node [
    id 51
    label "kszta&#322;cenie"
  ]
  node [
    id 52
    label "command"
  ]
  node [
    id 53
    label "okazywanie"
  ]
  node [
    id 54
    label "robienie"
  ]
  node [
    id 55
    label "informowanie"
  ]
  node [
    id 56
    label "znaczenie"
  ]
  node [
    id 57
    label "podawanie"
  ]
  node [
    id 58
    label "obnoszenie"
  ]
  node [
    id 59
    label "&#347;wiadczenie"
  ]
  node [
    id 60
    label "warto&#347;&#263;"
  ]
  node [
    id 61
    label "przedstawianie"
  ]
  node [
    id 62
    label "czynno&#347;&#263;"
  ]
  node [
    id 63
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 64
    label "czucie"
  ]
  node [
    id 65
    label "inclusion"
  ]
  node [
    id 66
    label "zawieranie"
  ]
  node [
    id 67
    label "przep&#322;ywanie"
  ]
  node [
    id 68
    label "proces"
  ]
  node [
    id 69
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 70
    label "uczenie_si&#281;"
  ]
  node [
    id 71
    label "spotykanie"
  ]
  node [
    id 72
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 73
    label "zjawisko"
  ]
  node [
    id 74
    label "merging"
  ]
  node [
    id 75
    label "znajomy"
  ]
  node [
    id 76
    label "rozr&#243;&#380;nianie"
  ]
  node [
    id 77
    label "recognition"
  ]
  node [
    id 78
    label "zapoznawanie"
  ]
  node [
    id 79
    label "designation"
  ]
  node [
    id 80
    label "cognition"
  ]
  node [
    id 81
    label "umo&#380;liwianie"
  ]
  node [
    id 82
    label "mamotrept"
  ]
  node [
    id 83
    label "S&#322;owo_Bo&#380;e"
  ]
  node [
    id 84
    label "apokryf"
  ]
  node [
    id 85
    label "Biblia_Jakuba_Wujka"
  ]
  node [
    id 86
    label "paginator"
  ]
  node [
    id 87
    label "Nowy_Testament"
  ]
  node [
    id 88
    label "Stary_Testament"
  ]
  node [
    id 89
    label "Biblia_Tysi&#261;clecia"
  ]
  node [
    id 90
    label "posta&#263;_biblijna"
  ]
  node [
    id 91
    label "perykopa"
  ]
  node [
    id 92
    label "modlitwa"
  ]
  node [
    id 93
    label "function"
  ]
  node [
    id 94
    label "kult"
  ]
  node [
    id 95
    label "hodowla"
  ]
  node [
    id 96
    label "praca_rolnicza"
  ]
  node [
    id 97
    label "mod&#322;y"
  ]
  node [
    id 98
    label "liturgika"
  ]
  node [
    id 99
    label "porz&#261;dek"
  ]
  node [
    id 100
    label "ceremony"
  ]
  node [
    id 101
    label "seminarzysta"
  ]
  node [
    id 102
    label "nauczyciel"
  ]
  node [
    id 103
    label "prezenter"
  ]
  node [
    id 104
    label "ministrant"
  ]
  node [
    id 105
    label "dyslexia"
  ]
  node [
    id 106
    label "dysfunkcja"
  ]
  node [
    id 107
    label "czyta&#263;"
  ]
  node [
    id 108
    label "wprowadzanie"
  ]
  node [
    id 109
    label "ko&#324;czenie"
  ]
  node [
    id 110
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 111
    label "uznanie"
  ]
  node [
    id 112
    label "intencja"
  ]
  node [
    id 113
    label "dokumentacja"
  ]
  node [
    id 114
    label "plan"
  ]
  node [
    id 115
    label "agreement"
  ]
  node [
    id 116
    label "device"
  ]
  node [
    id 117
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 118
    label "dokument"
  ]
  node [
    id 119
    label "program_u&#380;ytkowy"
  ]
  node [
    id 120
    label "pomys&#322;"
  ]
  node [
    id 121
    label "thinking"
  ]
  node [
    id 122
    label "wytw&#243;r"
  ]
  node [
    id 123
    label "sygnatariusz"
  ]
  node [
    id 124
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 125
    label "writing"
  ]
  node [
    id 126
    label "&#347;wiadectwo"
  ]
  node [
    id 127
    label "zapis"
  ]
  node [
    id 128
    label "artyku&#322;"
  ]
  node [
    id 129
    label "utw&#243;r"
  ]
  node [
    id 130
    label "record"
  ]
  node [
    id 131
    label "raport&#243;wka"
  ]
  node [
    id 132
    label "registratura"
  ]
  node [
    id 133
    label "fascyku&#322;"
  ]
  node [
    id 134
    label "parafa"
  ]
  node [
    id 135
    label "plik"
  ]
  node [
    id 136
    label "reprezentacja"
  ]
  node [
    id 137
    label "przestrze&#324;"
  ]
  node [
    id 138
    label "punkt"
  ]
  node [
    id 139
    label "perspektywa"
  ]
  node [
    id 140
    label "model"
  ]
  node [
    id 141
    label "miejsce_pracy"
  ]
  node [
    id 142
    label "obraz"
  ]
  node [
    id 143
    label "rysunek"
  ]
  node [
    id 144
    label "dekoracja"
  ]
  node [
    id 145
    label "operat"
  ]
  node [
    id 146
    label "ekscerpcja"
  ]
  node [
    id 147
    label "kosztorys"
  ]
  node [
    id 148
    label "materia&#322;"
  ]
  node [
    id 149
    label "ukradzenie"
  ]
  node [
    id 150
    label "pocz&#261;tki"
  ]
  node [
    id 151
    label "ukra&#347;&#263;"
  ]
  node [
    id 152
    label "idea"
  ]
  node [
    id 153
    label "system"
  ]
  node [
    id 154
    label "marc&#243;wka"
  ]
  node [
    id 155
    label "akt"
  ]
  node [
    id 156
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 157
    label "charter"
  ]
  node [
    id 158
    label "Karta_Nauczyciela"
  ]
  node [
    id 159
    label "przej&#347;&#263;"
  ]
  node [
    id 160
    label "przej&#347;cie"
  ]
  node [
    id 161
    label "poj&#281;cie"
  ]
  node [
    id 162
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 163
    label "erotyka"
  ]
  node [
    id 164
    label "fragment"
  ]
  node [
    id 165
    label "podniecanie"
  ]
  node [
    id 166
    label "po&#380;ycie"
  ]
  node [
    id 167
    label "baraszki"
  ]
  node [
    id 168
    label "numer"
  ]
  node [
    id 169
    label "certificate"
  ]
  node [
    id 170
    label "ruch_frykcyjny"
  ]
  node [
    id 171
    label "wydarzenie"
  ]
  node [
    id 172
    label "ontologia"
  ]
  node [
    id 173
    label "wzw&#243;d"
  ]
  node [
    id 174
    label "scena"
  ]
  node [
    id 175
    label "seks"
  ]
  node [
    id 176
    label "pozycja_misjonarska"
  ]
  node [
    id 177
    label "rozmna&#380;anie"
  ]
  node [
    id 178
    label "arystotelizm"
  ]
  node [
    id 179
    label "zwyczaj"
  ]
  node [
    id 180
    label "urzeczywistnienie"
  ]
  node [
    id 181
    label "z&#322;&#261;czenie"
  ]
  node [
    id 182
    label "funkcja"
  ]
  node [
    id 183
    label "act"
  ]
  node [
    id 184
    label "imisja"
  ]
  node [
    id 185
    label "podniecenie"
  ]
  node [
    id 186
    label "podnieca&#263;"
  ]
  node [
    id 187
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 188
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 189
    label "nago&#347;&#263;"
  ]
  node [
    id 190
    label "gra_wst&#281;pna"
  ]
  node [
    id 191
    label "po&#380;&#261;danie"
  ]
  node [
    id 192
    label "podnieci&#263;"
  ]
  node [
    id 193
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 194
    label "na_pieska"
  ]
  node [
    id 195
    label "rozwi&#261;zanie"
  ]
  node [
    id 196
    label "zabory"
  ]
  node [
    id 197
    label "ci&#281;&#380;arna"
  ]
  node [
    id 198
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 199
    label "przewy&#380;szenie"
  ]
  node [
    id 200
    label "experience"
  ]
  node [
    id 201
    label "przemokni&#281;cie"
  ]
  node [
    id 202
    label "prze&#380;ycie"
  ]
  node [
    id 203
    label "wydeptywanie"
  ]
  node [
    id 204
    label "offense"
  ]
  node [
    id 205
    label "traversal"
  ]
  node [
    id 206
    label "trwanie"
  ]
  node [
    id 207
    label "przepojenie"
  ]
  node [
    id 208
    label "przedostanie_si&#281;"
  ]
  node [
    id 209
    label "mini&#281;cie"
  ]
  node [
    id 210
    label "przestanie"
  ]
  node [
    id 211
    label "stanie_si&#281;"
  ]
  node [
    id 212
    label "miejsce"
  ]
  node [
    id 213
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 214
    label "nas&#261;czenie"
  ]
  node [
    id 215
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 216
    label "przebycie"
  ]
  node [
    id 217
    label "wymienienie"
  ]
  node [
    id 218
    label "nasycenie_si&#281;"
  ]
  node [
    id 219
    label "strain"
  ]
  node [
    id 220
    label "wytyczenie"
  ]
  node [
    id 221
    label "przerobienie"
  ]
  node [
    id 222
    label "zdarzenie_si&#281;"
  ]
  node [
    id 223
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 224
    label "przepuszczenie"
  ]
  node [
    id 225
    label "dostanie_si&#281;"
  ]
  node [
    id 226
    label "nale&#380;enie"
  ]
  node [
    id 227
    label "odmienienie"
  ]
  node [
    id 228
    label "wydeptanie"
  ]
  node [
    id 229
    label "mienie"
  ]
  node [
    id 230
    label "doznanie"
  ]
  node [
    id 231
    label "zaliczenie"
  ]
  node [
    id 232
    label "wstawka"
  ]
  node [
    id 233
    label "faza"
  ]
  node [
    id 234
    label "crack"
  ]
  node [
    id 235
    label "zacz&#281;cie"
  ]
  node [
    id 236
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 237
    label "zmieni&#263;"
  ]
  node [
    id 238
    label "absorb"
  ]
  node [
    id 239
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 240
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 241
    label "przesta&#263;"
  ]
  node [
    id 242
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 243
    label "podlec"
  ]
  node [
    id 244
    label "die"
  ]
  node [
    id 245
    label "pique"
  ]
  node [
    id 246
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 247
    label "zacz&#261;&#263;"
  ]
  node [
    id 248
    label "przeby&#263;"
  ]
  node [
    id 249
    label "happen"
  ]
  node [
    id 250
    label "zaliczy&#263;"
  ]
  node [
    id 251
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 252
    label "pass"
  ]
  node [
    id 253
    label "dozna&#263;"
  ]
  node [
    id 254
    label "przerobi&#263;"
  ]
  node [
    id 255
    label "min&#261;&#263;"
  ]
  node [
    id 256
    label "beat"
  ]
  node [
    id 257
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 258
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 259
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 260
    label "odnaj&#281;cie"
  ]
  node [
    id 261
    label "naj&#281;cie"
  ]
  node [
    id 262
    label "oznaka"
  ]
  node [
    id 263
    label "odmienianie"
  ]
  node [
    id 264
    label "zmianka"
  ]
  node [
    id 265
    label "amendment"
  ]
  node [
    id 266
    label "passage"
  ]
  node [
    id 267
    label "praca"
  ]
  node [
    id 268
    label "rewizja"
  ]
  node [
    id 269
    label "komplet"
  ]
  node [
    id 270
    label "tura"
  ]
  node [
    id 271
    label "change"
  ]
  node [
    id 272
    label "ferment"
  ]
  node [
    id 273
    label "anatomopatolog"
  ]
  node [
    id 274
    label "charakter"
  ]
  node [
    id 275
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 276
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 277
    label "przywidzenie"
  ]
  node [
    id 278
    label "boski"
  ]
  node [
    id 279
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 280
    label "krajobraz"
  ]
  node [
    id 281
    label "presence"
  ]
  node [
    id 282
    label "lekcja"
  ]
  node [
    id 283
    label "ensemble"
  ]
  node [
    id 284
    label "grupa"
  ]
  node [
    id 285
    label "klasa"
  ]
  node [
    id 286
    label "zestaw"
  ]
  node [
    id 287
    label "chronometria"
  ]
  node [
    id 288
    label "odczyt"
  ]
  node [
    id 289
    label "laba"
  ]
  node [
    id 290
    label "czasoprzestrze&#324;"
  ]
  node [
    id 291
    label "time_period"
  ]
  node [
    id 292
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 293
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 294
    label "Zeitgeist"
  ]
  node [
    id 295
    label "pochodzenie"
  ]
  node [
    id 296
    label "schy&#322;ek"
  ]
  node [
    id 297
    label "czwarty_wymiar"
  ]
  node [
    id 298
    label "kategoria_gramatyczna"
  ]
  node [
    id 299
    label "poprzedzi&#263;"
  ]
  node [
    id 300
    label "pogoda"
  ]
  node [
    id 301
    label "czasokres"
  ]
  node [
    id 302
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 303
    label "poprzedzenie"
  ]
  node [
    id 304
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 305
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 306
    label "dzieje"
  ]
  node [
    id 307
    label "zegar"
  ]
  node [
    id 308
    label "koniugacja"
  ]
  node [
    id 309
    label "trawi&#263;"
  ]
  node [
    id 310
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 311
    label "poprzedza&#263;"
  ]
  node [
    id 312
    label "trawienie"
  ]
  node [
    id 313
    label "chwila"
  ]
  node [
    id 314
    label "rachuba_czasu"
  ]
  node [
    id 315
    label "poprzedzanie"
  ]
  node [
    id 316
    label "okres_czasu"
  ]
  node [
    id 317
    label "period"
  ]
  node [
    id 318
    label "odwlekanie_si&#281;"
  ]
  node [
    id 319
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 320
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 321
    label "pochodzi&#263;"
  ]
  node [
    id 322
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 323
    label "signal"
  ]
  node [
    id 324
    label "implikowa&#263;"
  ]
  node [
    id 325
    label "fakt"
  ]
  node [
    id 326
    label "symbol"
  ]
  node [
    id 327
    label "proces_my&#347;lowy"
  ]
  node [
    id 328
    label "odwo&#322;anie"
  ]
  node [
    id 329
    label "checkup"
  ]
  node [
    id 330
    label "krytyka"
  ]
  node [
    id 331
    label "correction"
  ]
  node [
    id 332
    label "kipisz"
  ]
  node [
    id 333
    label "przegl&#261;d"
  ]
  node [
    id 334
    label "korekta"
  ]
  node [
    id 335
    label "dow&#243;d"
  ]
  node [
    id 336
    label "kontrola"
  ]
  node [
    id 337
    label "rekurs"
  ]
  node [
    id 338
    label "biokatalizator"
  ]
  node [
    id 339
    label "bia&#322;ko"
  ]
  node [
    id 340
    label "zymaza"
  ]
  node [
    id 341
    label "poruszenie"
  ]
  node [
    id 342
    label "immobilizowa&#263;"
  ]
  node [
    id 343
    label "immobilizacja"
  ]
  node [
    id 344
    label "apoenzym"
  ]
  node [
    id 345
    label "immobilizowanie"
  ]
  node [
    id 346
    label "enzyme"
  ]
  node [
    id 347
    label "zaw&#243;d"
  ]
  node [
    id 348
    label "pracowanie"
  ]
  node [
    id 349
    label "pracowa&#263;"
  ]
  node [
    id 350
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 351
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 352
    label "czynnik_produkcji"
  ]
  node [
    id 353
    label "stosunek_pracy"
  ]
  node [
    id 354
    label "kierownictwo"
  ]
  node [
    id 355
    label "najem"
  ]
  node [
    id 356
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 357
    label "siedziba"
  ]
  node [
    id 358
    label "zak&#322;ad"
  ]
  node [
    id 359
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 360
    label "tynkarski"
  ]
  node [
    id 361
    label "tyrka"
  ]
  node [
    id 362
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 363
    label "benedykty&#324;ski"
  ]
  node [
    id 364
    label "poda&#380;_pracy"
  ]
  node [
    id 365
    label "zobowi&#261;zanie"
  ]
  node [
    id 366
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 367
    label "patolog"
  ]
  node [
    id 368
    label "anatom"
  ]
  node [
    id 369
    label "parafrazowanie"
  ]
  node [
    id 370
    label "sparafrazowanie"
  ]
  node [
    id 371
    label "Transfiguration"
  ]
  node [
    id 372
    label "zmienianie"
  ]
  node [
    id 373
    label "wymienianie"
  ]
  node [
    id 374
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 375
    label "zamiana"
  ]
  node [
    id 376
    label "r&#281;kopis"
  ]
  node [
    id 377
    label "zbi&#243;r"
  ]
  node [
    id 378
    label "przepis"
  ]
  node [
    id 379
    label "kodeks_morski"
  ]
  node [
    id 380
    label "kodeks_wykrocze&#324;"
  ]
  node [
    id 381
    label "kodeks_karny"
  ]
  node [
    id 382
    label "kodeks_drogowy"
  ]
  node [
    id 383
    label "Justynian"
  ]
  node [
    id 384
    label "obwiniony"
  ]
  node [
    id 385
    label "kodeks_pracy"
  ]
  node [
    id 386
    label "kodeks_cywilny"
  ]
  node [
    id 387
    label "kodeks_dyplomatyczny"
  ]
  node [
    id 388
    label "zasada"
  ]
  node [
    id 389
    label "code"
  ]
  node [
    id 390
    label "kodeks_rodzinny"
  ]
  node [
    id 391
    label "pakiet_klimatyczny"
  ]
  node [
    id 392
    label "uprawianie"
  ]
  node [
    id 393
    label "collection"
  ]
  node [
    id 394
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 395
    label "gathering"
  ]
  node [
    id 396
    label "album"
  ]
  node [
    id 397
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 398
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 399
    label "sum"
  ]
  node [
    id 400
    label "egzemplarz"
  ]
  node [
    id 401
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 402
    label "series"
  ]
  node [
    id 403
    label "dane"
  ]
  node [
    id 404
    label "cymelium"
  ]
  node [
    id 405
    label "prawid&#322;o"
  ]
  node [
    id 406
    label "zasada_d'Alemberta"
  ]
  node [
    id 407
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 408
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 409
    label "opis"
  ]
  node [
    id 410
    label "base"
  ]
  node [
    id 411
    label "moralno&#347;&#263;"
  ]
  node [
    id 412
    label "regu&#322;a_Allena"
  ]
  node [
    id 413
    label "prawo_Mendla"
  ]
  node [
    id 414
    label "criterion"
  ]
  node [
    id 415
    label "standard"
  ]
  node [
    id 416
    label "obserwacja"
  ]
  node [
    id 417
    label "podstawa"
  ]
  node [
    id 418
    label "regu&#322;a_Glogera"
  ]
  node [
    id 419
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 420
    label "spos&#243;b"
  ]
  node [
    id 421
    label "qualification"
  ]
  node [
    id 422
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 423
    label "umowa"
  ]
  node [
    id 424
    label "normalizacja"
  ]
  node [
    id 425
    label "dominion"
  ]
  node [
    id 426
    label "twierdzenie"
  ]
  node [
    id 427
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 428
    label "prawo"
  ]
  node [
    id 429
    label "substancja"
  ]
  node [
    id 430
    label "occupation"
  ]
  node [
    id 431
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 432
    label "regulation"
  ]
  node [
    id 433
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 434
    label "norma_prawna"
  ]
  node [
    id 435
    label "przedawnianie_si&#281;"
  ]
  node [
    id 436
    label "recepta"
  ]
  node [
    id 437
    label "przedawnienie_si&#281;"
  ]
  node [
    id 438
    label "porada"
  ]
  node [
    id 439
    label "cz&#322;owiek"
  ]
  node [
    id 440
    label "wsp&#243;lnictwo"
  ]
  node [
    id 441
    label "organizacja"
  ]
  node [
    id 442
    label "zesp&#243;&#322;"
  ]
  node [
    id 443
    label "podmiot_gospodarczy"
  ]
  node [
    id 444
    label "przybud&#243;wka"
  ]
  node [
    id 445
    label "struktura"
  ]
  node [
    id 446
    label "organization"
  ]
  node [
    id 447
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 448
    label "od&#322;am"
  ]
  node [
    id 449
    label "TOPR"
  ]
  node [
    id 450
    label "komitet_koordynacyjny"
  ]
  node [
    id 451
    label "przedstawicielstwo"
  ]
  node [
    id 452
    label "ZMP"
  ]
  node [
    id 453
    label "Cepelia"
  ]
  node [
    id 454
    label "GOPR"
  ]
  node [
    id 455
    label "endecki"
  ]
  node [
    id 456
    label "ZBoWiD"
  ]
  node [
    id 457
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 458
    label "podmiot"
  ]
  node [
    id 459
    label "boj&#243;wka"
  ]
  node [
    id 460
    label "ZOMO"
  ]
  node [
    id 461
    label "jednostka_organizacyjna"
  ]
  node [
    id 462
    label "centrala"
  ]
  node [
    id 463
    label "group"
  ]
  node [
    id 464
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 465
    label "The_Beatles"
  ]
  node [
    id 466
    label "odm&#322;odzenie"
  ]
  node [
    id 467
    label "ro&#347;lina"
  ]
  node [
    id 468
    label "odm&#322;adzanie"
  ]
  node [
    id 469
    label "Depeche_Mode"
  ]
  node [
    id 470
    label "odm&#322;adza&#263;"
  ]
  node [
    id 471
    label "&#346;wietliki"
  ]
  node [
    id 472
    label "zespolik"
  ]
  node [
    id 473
    label "whole"
  ]
  node [
    id 474
    label "Mazowsze"
  ]
  node [
    id 475
    label "schorzenie"
  ]
  node [
    id 476
    label "skupienie"
  ]
  node [
    id 477
    label "batch"
  ]
  node [
    id 478
    label "zabudowania"
  ]
  node [
    id 479
    label "uczestnictwo"
  ]
  node [
    id 480
    label "handlowo"
  ]
  node [
    id 481
    label "nak&#322;ad"
  ]
  node [
    id 482
    label "sumpt"
  ]
  node [
    id 483
    label "wydatek"
  ]
  node [
    id 484
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 485
    label "ilo&#347;&#263;"
  ]
  node [
    id 486
    label "liczba"
  ]
  node [
    id 487
    label "kwota"
  ]
  node [
    id 488
    label "wych&#243;d"
  ]
  node [
    id 489
    label "urz&#281;dowy"
  ]
  node [
    id 490
    label "s&#261;downie"
  ]
  node [
    id 491
    label "formalny"
  ]
  node [
    id 492
    label "urz&#281;dowo"
  ]
  node [
    id 493
    label "oficjalny"
  ]
  node [
    id 494
    label "judicially"
  ]
  node [
    id 495
    label "s&#261;downy"
  ]
  node [
    id 496
    label "rzecz"
  ]
  node [
    id 497
    label "rozprawa"
  ]
  node [
    id 498
    label "kognicja"
  ]
  node [
    id 499
    label "proposition"
  ]
  node [
    id 500
    label "object"
  ]
  node [
    id 501
    label "przes&#322;anka"
  ]
  node [
    id 502
    label "szczeg&#243;&#322;"
  ]
  node [
    id 503
    label "temat"
  ]
  node [
    id 504
    label "przebiegni&#281;cie"
  ]
  node [
    id 505
    label "przebiec"
  ]
  node [
    id 506
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 507
    label "motyw"
  ]
  node [
    id 508
    label "fabu&#322;a"
  ]
  node [
    id 509
    label "istota"
  ]
  node [
    id 510
    label "Kant"
  ]
  node [
    id 511
    label "ideacja"
  ]
  node [
    id 512
    label "byt"
  ]
  node [
    id 513
    label "p&#322;&#243;d"
  ]
  node [
    id 514
    label "cel"
  ]
  node [
    id 515
    label "intelekt"
  ]
  node [
    id 516
    label "ideologia"
  ]
  node [
    id 517
    label "przedmiot"
  ]
  node [
    id 518
    label "wpada&#263;"
  ]
  node [
    id 519
    label "przyroda"
  ]
  node [
    id 520
    label "wpa&#347;&#263;"
  ]
  node [
    id 521
    label "kultura"
  ]
  node [
    id 522
    label "obiekt"
  ]
  node [
    id 523
    label "wpadni&#281;cie"
  ]
  node [
    id 524
    label "wpadanie"
  ]
  node [
    id 525
    label "tekst"
  ]
  node [
    id 526
    label "cytat"
  ]
  node [
    id 527
    label "s&#261;d"
  ]
  node [
    id 528
    label "opracowanie"
  ]
  node [
    id 529
    label "obja&#347;nienie"
  ]
  node [
    id 530
    label "s&#261;dzenie"
  ]
  node [
    id 531
    label "obrady"
  ]
  node [
    id 532
    label "rozumowanie"
  ]
  node [
    id 533
    label "sk&#322;adnik"
  ]
  node [
    id 534
    label "zniuansowa&#263;"
  ]
  node [
    id 535
    label "niuansowa&#263;"
  ]
  node [
    id 536
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 537
    label "element"
  ]
  node [
    id 538
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 539
    label "przyczyna"
  ]
  node [
    id 540
    label "wnioskowanie"
  ]
  node [
    id 541
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 542
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 543
    label "zboczy&#263;"
  ]
  node [
    id 544
    label "w&#261;tek"
  ]
  node [
    id 545
    label "fraza"
  ]
  node [
    id 546
    label "entity"
  ]
  node [
    id 547
    label "otoczka"
  ]
  node [
    id 548
    label "forma"
  ]
  node [
    id 549
    label "zboczenie"
  ]
  node [
    id 550
    label "forum"
  ]
  node [
    id 551
    label "om&#243;wi&#263;"
  ]
  node [
    id 552
    label "tre&#347;&#263;"
  ]
  node [
    id 553
    label "topik"
  ]
  node [
    id 554
    label "melodia"
  ]
  node [
    id 555
    label "cecha"
  ]
  node [
    id 556
    label "wyraz_pochodny"
  ]
  node [
    id 557
    label "zbacza&#263;"
  ]
  node [
    id 558
    label "om&#243;wienie"
  ]
  node [
    id 559
    label "tematyka"
  ]
  node [
    id 560
    label "omawianie"
  ]
  node [
    id 561
    label "omawia&#263;"
  ]
  node [
    id 562
    label "zbaczanie"
  ]
  node [
    id 563
    label "cywilnie"
  ]
  node [
    id 564
    label "nieoficjalny"
  ]
  node [
    id 565
    label "nieformalny"
  ]
  node [
    id 566
    label "nieoficjalnie"
  ]
  node [
    id 567
    label "formatowa&#263;"
  ]
  node [
    id 568
    label "tkanina"
  ]
  node [
    id 569
    label "glif"
  ]
  node [
    id 570
    label "printing"
  ]
  node [
    id 571
    label "zdobnik"
  ]
  node [
    id 572
    label "character"
  ]
  node [
    id 573
    label "zaproszenie"
  ]
  node [
    id 574
    label "publikacja"
  ]
  node [
    id 575
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 576
    label "formatowanie"
  ]
  node [
    id 577
    label "impression"
  ]
  node [
    id 578
    label "technika"
  ]
  node [
    id 579
    label "prohibita"
  ]
  node [
    id 580
    label "dese&#324;"
  ]
  node [
    id 581
    label "pismo"
  ]
  node [
    id 582
    label "mechanika_precyzyjna"
  ]
  node [
    id 583
    label "technologia"
  ]
  node [
    id 584
    label "fotowoltaika"
  ]
  node [
    id 585
    label "cywilizacja"
  ]
  node [
    id 586
    label "telekomunikacja"
  ]
  node [
    id 587
    label "teletechnika"
  ]
  node [
    id 588
    label "wiedza"
  ]
  node [
    id 589
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 590
    label "engineering"
  ]
  node [
    id 591
    label "sprawno&#347;&#263;"
  ]
  node [
    id 592
    label "rezultat"
  ]
  node [
    id 593
    label "work"
  ]
  node [
    id 594
    label "ortografia"
  ]
  node [
    id 595
    label "dzia&#322;"
  ]
  node [
    id 596
    label "zajawka"
  ]
  node [
    id 597
    label "paleografia"
  ]
  node [
    id 598
    label "dzie&#322;o"
  ]
  node [
    id 599
    label "wk&#322;ad"
  ]
  node [
    id 600
    label "ok&#322;adka"
  ]
  node [
    id 601
    label "letter"
  ]
  node [
    id 602
    label "prasa"
  ]
  node [
    id 603
    label "psychotest"
  ]
  node [
    id 604
    label "communication"
  ]
  node [
    id 605
    label "Zwrotnica"
  ]
  node [
    id 606
    label "script"
  ]
  node [
    id 607
    label "czasopismo"
  ]
  node [
    id 608
    label "j&#281;zyk"
  ]
  node [
    id 609
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 610
    label "komunikacja"
  ]
  node [
    id 611
    label "adres"
  ]
  node [
    id 612
    label "przekaz"
  ]
  node [
    id 613
    label "grafia"
  ]
  node [
    id 614
    label "interpunkcja"
  ]
  node [
    id 615
    label "handwriting"
  ]
  node [
    id 616
    label "paleograf"
  ]
  node [
    id 617
    label "list"
  ]
  node [
    id 618
    label "wz&#243;r"
  ]
  node [
    id 619
    label "design"
  ]
  node [
    id 620
    label "produkcja"
  ]
  node [
    id 621
    label "notification"
  ]
  node [
    id 622
    label "apretura"
  ]
  node [
    id 623
    label "rozpru&#263;_si&#281;"
  ]
  node [
    id 624
    label "karbonizacja"
  ]
  node [
    id 625
    label "rozprucie_si&#281;"
  ]
  node [
    id 626
    label "splot"
  ]
  node [
    id 627
    label "towar"
  ]
  node [
    id 628
    label "prucie_si&#281;"
  ]
  node [
    id 629
    label "karbonizowa&#263;"
  ]
  node [
    id 630
    label "maglownia"
  ]
  node [
    id 631
    label "opalarnia"
  ]
  node [
    id 632
    label "pru&#263;_si&#281;"
  ]
  node [
    id 633
    label "pisa&#263;"
  ]
  node [
    id 634
    label "redakcja"
  ]
  node [
    id 635
    label "j&#281;zykowo"
  ]
  node [
    id 636
    label "preparacja"
  ]
  node [
    id 637
    label "wypowied&#378;"
  ]
  node [
    id 638
    label "obelga"
  ]
  node [
    id 639
    label "odmianka"
  ]
  node [
    id 640
    label "opu&#347;ci&#263;"
  ]
  node [
    id 641
    label "pomini&#281;cie"
  ]
  node [
    id 642
    label "koniektura"
  ]
  node [
    id 643
    label "czcionka"
  ]
  node [
    id 644
    label "kr&#243;j"
  ]
  node [
    id 645
    label "splay"
  ]
  node [
    id 646
    label "propozycja"
  ]
  node [
    id 647
    label "karteczka"
  ]
  node [
    id 648
    label "invitation"
  ]
  node [
    id 649
    label "pro&#347;ba"
  ]
  node [
    id 650
    label "zaproponowanie"
  ]
  node [
    id 651
    label "podw&#243;jno&#347;&#263;"
  ]
  node [
    id 652
    label "wzajemno&#347;&#263;"
  ]
  node [
    id 653
    label "dostosowywa&#263;"
  ]
  node [
    id 654
    label "przygotowywa&#263;"
  ]
  node [
    id 655
    label "edytowa&#263;"
  ]
  node [
    id 656
    label "format"
  ]
  node [
    id 657
    label "sk&#322;ada&#263;"
  ]
  node [
    id 658
    label "przygotowywanie"
  ]
  node [
    id 659
    label "dostosowywanie"
  ]
  node [
    id 660
    label "sk&#322;adanie"
  ]
  node [
    id 661
    label "edytowanie"
  ]
  node [
    id 662
    label "rarytas"
  ]
  node [
    id 663
    label "ojciec"
  ]
  node [
    id 664
    label "wyspa"
  ]
  node [
    id 665
    label "ministerstwo"
  ]
  node [
    id 666
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 667
    label "&#321;ukasz"
  ]
  node [
    id 668
    label "R&#281;dziniaka"
  ]
  node [
    id 669
    label "R&#281;dziniak"
  ]
  node [
    id 670
    label "gospodarka"
  ]
  node [
    id 671
    label "pakiet"
  ]
  node [
    id 672
    label "na"
  ]
  node [
    id 673
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 4
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 663
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 496
    target 671
  ]
  edge [
    source 496
    target 672
  ]
  edge [
    source 496
    target 673
  ]
  edge [
    source 663
    target 664
  ]
  edge [
    source 665
    target 666
  ]
  edge [
    source 665
    target 670
  ]
  edge [
    source 667
    target 668
  ]
  edge [
    source 667
    target 669
  ]
  edge [
    source 671
    target 672
  ]
  edge [
    source 671
    target 673
  ]
  edge [
    source 672
    target 673
  ]
]
