graph [
  node [
    id 0
    label "dwa"
    origin "text"
  ]
  node [
    id 1
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "syn"
    origin "text"
  ]
  node [
    id 4
    label "nauczy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "gra&#263;"
    origin "text"
  ]
  node [
    id 6
    label "magic"
    origin "text"
  ]
  node [
    id 7
    label "the"
    origin "text"
  ]
  node [
    id 8
    label "gathering"
    origin "text"
  ]
  node [
    id 9
    label "strategiczny"
    origin "text"
  ]
  node [
    id 10
    label "gra"
    origin "text"
  ]
  node [
    id 11
    label "karciany"
    origin "text"
  ]
  node [
    id 12
    label "by&#263;"
    origin "text"
  ]
  node [
    id 13
    label "niezmiernie"
    origin "text"
  ]
  node [
    id 14
    label "wci&#261;ga&#263;"
    origin "text"
  ]
  node [
    id 15
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 16
    label "czym"
    origin "text"
  ]
  node [
    id 17
    label "szybki"
    origin "text"
  ]
  node [
    id 18
    label "czas"
    origin "text"
  ]
  node [
    id 19
    label "zapami&#281;tywa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "si&#281;"
    origin "text"
  ]
  node [
    id 21
    label "mn&#243;stwo"
    origin "text"
  ]
  node [
    id 22
    label "informacja"
    origin "text"
  ]
  node [
    id 23
    label "typ"
    origin "text"
  ]
  node [
    id 24
    label "zimny"
    origin "text"
  ]
  node [
    id 25
    label "oddech"
    origin "text"
  ]
  node [
    id 26
    label "blokowa&#263;"
    origin "text"
  ]
  node [
    id 27
    label "moc"
    origin "text"
  ]
  node [
    id 28
    label "ogie&#324;"
    origin "text"
  ]
  node [
    id 29
    label "doba"
  ]
  node [
    id 30
    label "weekend"
  ]
  node [
    id 31
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 32
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 33
    label "miesi&#261;c"
  ]
  node [
    id 34
    label "poprzedzanie"
  ]
  node [
    id 35
    label "czasoprzestrze&#324;"
  ]
  node [
    id 36
    label "laba"
  ]
  node [
    id 37
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 38
    label "chronometria"
  ]
  node [
    id 39
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 40
    label "rachuba_czasu"
  ]
  node [
    id 41
    label "przep&#322;ywanie"
  ]
  node [
    id 42
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 43
    label "czasokres"
  ]
  node [
    id 44
    label "odczyt"
  ]
  node [
    id 45
    label "chwila"
  ]
  node [
    id 46
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 47
    label "dzieje"
  ]
  node [
    id 48
    label "kategoria_gramatyczna"
  ]
  node [
    id 49
    label "poprzedzenie"
  ]
  node [
    id 50
    label "trawienie"
  ]
  node [
    id 51
    label "pochodzi&#263;"
  ]
  node [
    id 52
    label "period"
  ]
  node [
    id 53
    label "okres_czasu"
  ]
  node [
    id 54
    label "poprzedza&#263;"
  ]
  node [
    id 55
    label "schy&#322;ek"
  ]
  node [
    id 56
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 57
    label "odwlekanie_si&#281;"
  ]
  node [
    id 58
    label "zegar"
  ]
  node [
    id 59
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 60
    label "czwarty_wymiar"
  ]
  node [
    id 61
    label "pochodzenie"
  ]
  node [
    id 62
    label "koniugacja"
  ]
  node [
    id 63
    label "Zeitgeist"
  ]
  node [
    id 64
    label "trawi&#263;"
  ]
  node [
    id 65
    label "pogoda"
  ]
  node [
    id 66
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 67
    label "poprzedzi&#263;"
  ]
  node [
    id 68
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 69
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 70
    label "time_period"
  ]
  node [
    id 71
    label "noc"
  ]
  node [
    id 72
    label "dzie&#324;"
  ]
  node [
    id 73
    label "godzina"
  ]
  node [
    id 74
    label "long_time"
  ]
  node [
    id 75
    label "jednostka_geologiczna"
  ]
  node [
    id 76
    label "niedziela"
  ]
  node [
    id 77
    label "sobota"
  ]
  node [
    id 78
    label "miech"
  ]
  node [
    id 79
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 80
    label "rok"
  ]
  node [
    id 81
    label "kalendy"
  ]
  node [
    id 82
    label "usynowienie"
  ]
  node [
    id 83
    label "dziecko"
  ]
  node [
    id 84
    label "usynawianie"
  ]
  node [
    id 85
    label "utulenie"
  ]
  node [
    id 86
    label "pediatra"
  ]
  node [
    id 87
    label "dzieciak"
  ]
  node [
    id 88
    label "utulanie"
  ]
  node [
    id 89
    label "dzieciarnia"
  ]
  node [
    id 90
    label "cz&#322;owiek"
  ]
  node [
    id 91
    label "niepe&#322;noletni"
  ]
  node [
    id 92
    label "organizm"
  ]
  node [
    id 93
    label "utula&#263;"
  ]
  node [
    id 94
    label "cz&#322;owieczek"
  ]
  node [
    id 95
    label "fledgling"
  ]
  node [
    id 96
    label "zwierz&#281;"
  ]
  node [
    id 97
    label "utuli&#263;"
  ]
  node [
    id 98
    label "m&#322;odzik"
  ]
  node [
    id 99
    label "pedofil"
  ]
  node [
    id 100
    label "m&#322;odziak"
  ]
  node [
    id 101
    label "potomek"
  ]
  node [
    id 102
    label "entliczek-pentliczek"
  ]
  node [
    id 103
    label "potomstwo"
  ]
  node [
    id 104
    label "sraluch"
  ]
  node [
    id 105
    label "przysposobienie"
  ]
  node [
    id 106
    label "adoption"
  ]
  node [
    id 107
    label "przysposabianie"
  ]
  node [
    id 108
    label "instruct"
  ]
  node [
    id 109
    label "wyszkoli&#263;"
  ]
  node [
    id 110
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 111
    label "pom&#243;c"
  ]
  node [
    id 112
    label "train"
  ]
  node [
    id 113
    label "o&#347;wieci&#263;"
  ]
  node [
    id 114
    label "work"
  ]
  node [
    id 115
    label "chemia"
  ]
  node [
    id 116
    label "spowodowa&#263;"
  ]
  node [
    id 117
    label "reakcja_chemiczna"
  ]
  node [
    id 118
    label "act"
  ]
  node [
    id 119
    label "&#347;wieci&#263;"
  ]
  node [
    id 120
    label "play"
  ]
  node [
    id 121
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 122
    label "muzykowa&#263;"
  ]
  node [
    id 123
    label "robi&#263;"
  ]
  node [
    id 124
    label "majaczy&#263;"
  ]
  node [
    id 125
    label "szczeka&#263;"
  ]
  node [
    id 126
    label "wykonywa&#263;"
  ]
  node [
    id 127
    label "napierdziela&#263;"
  ]
  node [
    id 128
    label "dzia&#322;a&#263;"
  ]
  node [
    id 129
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 130
    label "instrument_muzyczny"
  ]
  node [
    id 131
    label "pasowa&#263;"
  ]
  node [
    id 132
    label "sound"
  ]
  node [
    id 133
    label "dally"
  ]
  node [
    id 134
    label "i&#347;&#263;"
  ]
  node [
    id 135
    label "stara&#263;_si&#281;"
  ]
  node [
    id 136
    label "tokowa&#263;"
  ]
  node [
    id 137
    label "wida&#263;"
  ]
  node [
    id 138
    label "prezentowa&#263;"
  ]
  node [
    id 139
    label "rozgrywa&#263;"
  ]
  node [
    id 140
    label "do"
  ]
  node [
    id 141
    label "brzmie&#263;"
  ]
  node [
    id 142
    label "wykorzystywa&#263;"
  ]
  node [
    id 143
    label "cope"
  ]
  node [
    id 144
    label "otwarcie"
  ]
  node [
    id 145
    label "typify"
  ]
  node [
    id 146
    label "przedstawia&#263;"
  ]
  node [
    id 147
    label "rola"
  ]
  node [
    id 148
    label "kopiowa&#263;"
  ]
  node [
    id 149
    label "czerpa&#263;"
  ]
  node [
    id 150
    label "post&#281;powa&#263;"
  ]
  node [
    id 151
    label "mock"
  ]
  node [
    id 152
    label "lecie&#263;"
  ]
  node [
    id 153
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 154
    label "mie&#263;_miejsce"
  ]
  node [
    id 155
    label "bangla&#263;"
  ]
  node [
    id 156
    label "trace"
  ]
  node [
    id 157
    label "impart"
  ]
  node [
    id 158
    label "proceed"
  ]
  node [
    id 159
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 160
    label "try"
  ]
  node [
    id 161
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 162
    label "boost"
  ]
  node [
    id 163
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 164
    label "dziama&#263;"
  ]
  node [
    id 165
    label "blend"
  ]
  node [
    id 166
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 167
    label "draw"
  ]
  node [
    id 168
    label "wyrusza&#263;"
  ]
  node [
    id 169
    label "bie&#380;e&#263;"
  ]
  node [
    id 170
    label "dzia&#263;_si&#281;"
  ]
  node [
    id 171
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 172
    label "tryb"
  ]
  node [
    id 173
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 174
    label "atakowa&#263;"
  ]
  node [
    id 175
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 176
    label "describe"
  ]
  node [
    id 177
    label "continue"
  ]
  node [
    id 178
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 179
    label "bark"
  ]
  node [
    id 180
    label "m&#243;wi&#263;"
  ]
  node [
    id 181
    label "hum"
  ]
  node [
    id 182
    label "obgadywa&#263;"
  ]
  node [
    id 183
    label "pies"
  ]
  node [
    id 184
    label "kozio&#322;"
  ]
  node [
    id 185
    label "szcz&#281;ka&#263;"
  ]
  node [
    id 186
    label "karabin"
  ]
  node [
    id 187
    label "wymy&#347;la&#263;"
  ]
  node [
    id 188
    label "istnie&#263;"
  ]
  node [
    id 189
    label "function"
  ]
  node [
    id 190
    label "determine"
  ]
  node [
    id 191
    label "powodowa&#263;"
  ]
  node [
    id 192
    label "commit"
  ]
  node [
    id 193
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 194
    label "organizowa&#263;"
  ]
  node [
    id 195
    label "czyni&#263;"
  ]
  node [
    id 196
    label "give"
  ]
  node [
    id 197
    label "stylizowa&#263;"
  ]
  node [
    id 198
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 199
    label "falowa&#263;"
  ]
  node [
    id 200
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 201
    label "peddle"
  ]
  node [
    id 202
    label "praca"
  ]
  node [
    id 203
    label "wydala&#263;"
  ]
  node [
    id 204
    label "tentegowa&#263;"
  ]
  node [
    id 205
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 206
    label "urz&#261;dza&#263;"
  ]
  node [
    id 207
    label "oszukiwa&#263;"
  ]
  node [
    id 208
    label "ukazywa&#263;"
  ]
  node [
    id 209
    label "przerabia&#263;"
  ]
  node [
    id 210
    label "teatr"
  ]
  node [
    id 211
    label "exhibit"
  ]
  node [
    id 212
    label "podawa&#263;"
  ]
  node [
    id 213
    label "display"
  ]
  node [
    id 214
    label "pokazywa&#263;"
  ]
  node [
    id 215
    label "demonstrowa&#263;"
  ]
  node [
    id 216
    label "przedstawienie"
  ]
  node [
    id 217
    label "zapoznawa&#263;"
  ]
  node [
    id 218
    label "opisywa&#263;"
  ]
  node [
    id 219
    label "represent"
  ]
  node [
    id 220
    label "zg&#322;asza&#263;"
  ]
  node [
    id 221
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 222
    label "attest"
  ]
  node [
    id 223
    label "stanowi&#263;"
  ]
  node [
    id 224
    label "gaworzy&#263;"
  ]
  node [
    id 225
    label "ptactwo"
  ]
  node [
    id 226
    label "wypowiada&#263;"
  ]
  node [
    id 227
    label "wytwarza&#263;"
  ]
  node [
    id 228
    label "create"
  ]
  node [
    id 229
    label "muzyka"
  ]
  node [
    id 230
    label "echo"
  ]
  node [
    id 231
    label "wydawa&#263;"
  ]
  node [
    id 232
    label "wyra&#380;a&#263;"
  ]
  node [
    id 233
    label "s&#322;ycha&#263;"
  ]
  node [
    id 234
    label "korzysta&#263;"
  ]
  node [
    id 235
    label "liga&#263;"
  ]
  node [
    id 236
    label "distribute"
  ]
  node [
    id 237
    label "u&#380;ywa&#263;"
  ]
  node [
    id 238
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 239
    label "use"
  ]
  node [
    id 240
    label "krzywdzi&#263;"
  ]
  node [
    id 241
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 242
    label "przywo&#322;a&#263;"
  ]
  node [
    id 243
    label "tone"
  ]
  node [
    id 244
    label "render"
  ]
  node [
    id 245
    label "harmonia"
  ]
  node [
    id 246
    label "equate"
  ]
  node [
    id 247
    label "odpowiada&#263;"
  ]
  node [
    id 248
    label "poddawa&#263;"
  ]
  node [
    id 249
    label "equip"
  ]
  node [
    id 250
    label "mianowa&#263;"
  ]
  node [
    id 251
    label "przeprowadza&#263;"
  ]
  node [
    id 252
    label "migota&#263;"
  ]
  node [
    id 253
    label "ple&#347;&#263;"
  ]
  node [
    id 254
    label "ramble_on"
  ]
  node [
    id 255
    label "&#347;wita&#263;"
  ]
  node [
    id 256
    label "widzie&#263;"
  ]
  node [
    id 257
    label "zarysowywa&#263;_si&#281;"
  ]
  node [
    id 258
    label "rave"
  ]
  node [
    id 259
    label "przypomina&#263;_si&#281;"
  ]
  node [
    id 260
    label "ut"
  ]
  node [
    id 261
    label "d&#378;wi&#281;k"
  ]
  node [
    id 262
    label "C"
  ]
  node [
    id 263
    label "his"
  ]
  node [
    id 264
    label "jawno"
  ]
  node [
    id 265
    label "rozpocz&#281;cie"
  ]
  node [
    id 266
    label "udost&#281;pnienie"
  ]
  node [
    id 267
    label "publicznie"
  ]
  node [
    id 268
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 269
    label "ewidentnie"
  ]
  node [
    id 270
    label "jawnie"
  ]
  node [
    id 271
    label "opening"
  ]
  node [
    id 272
    label "jawny"
  ]
  node [
    id 273
    label "otwarty"
  ]
  node [
    id 274
    label "czynno&#347;&#263;"
  ]
  node [
    id 275
    label "bezpo&#347;rednio"
  ]
  node [
    id 276
    label "zdecydowanie"
  ]
  node [
    id 277
    label "granie"
  ]
  node [
    id 278
    label "uprawienie"
  ]
  node [
    id 279
    label "kszta&#322;t"
  ]
  node [
    id 280
    label "dialog"
  ]
  node [
    id 281
    label "p&#322;osa"
  ]
  node [
    id 282
    label "wykonywanie"
  ]
  node [
    id 283
    label "plik"
  ]
  node [
    id 284
    label "ziemia"
  ]
  node [
    id 285
    label "czyn"
  ]
  node [
    id 286
    label "ustawienie"
  ]
  node [
    id 287
    label "scenariusz"
  ]
  node [
    id 288
    label "pole"
  ]
  node [
    id 289
    label "gospodarstwo"
  ]
  node [
    id 290
    label "uprawi&#263;"
  ]
  node [
    id 291
    label "posta&#263;"
  ]
  node [
    id 292
    label "zreinterpretowa&#263;"
  ]
  node [
    id 293
    label "zastosowanie"
  ]
  node [
    id 294
    label "reinterpretowa&#263;"
  ]
  node [
    id 295
    label "wrench"
  ]
  node [
    id 296
    label "irygowanie"
  ]
  node [
    id 297
    label "ustawi&#263;"
  ]
  node [
    id 298
    label "irygowa&#263;"
  ]
  node [
    id 299
    label "zreinterpretowanie"
  ]
  node [
    id 300
    label "cel"
  ]
  node [
    id 301
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 302
    label "aktorstwo"
  ]
  node [
    id 303
    label "kostium"
  ]
  node [
    id 304
    label "zagon"
  ]
  node [
    id 305
    label "znaczenie"
  ]
  node [
    id 306
    label "zagra&#263;"
  ]
  node [
    id 307
    label "reinterpretowanie"
  ]
  node [
    id 308
    label "sk&#322;ad"
  ]
  node [
    id 309
    label "tekst"
  ]
  node [
    id 310
    label "zagranie"
  ]
  node [
    id 311
    label "radlina"
  ]
  node [
    id 312
    label "gorze&#263;"
  ]
  node [
    id 313
    label "o&#347;wietla&#263;"
  ]
  node [
    id 314
    label "kierowa&#263;"
  ]
  node [
    id 315
    label "kolor"
  ]
  node [
    id 316
    label "flash"
  ]
  node [
    id 317
    label "czuwa&#263;"
  ]
  node [
    id 318
    label "&#347;wiat&#322;o"
  ]
  node [
    id 319
    label "radiance"
  ]
  node [
    id 320
    label "wyr&#243;&#380;nia&#263;_si&#281;"
  ]
  node [
    id 321
    label "tryska&#263;"
  ]
  node [
    id 322
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 323
    label "smoulder"
  ]
  node [
    id 324
    label "emanowa&#263;"
  ]
  node [
    id 325
    label "ridicule"
  ]
  node [
    id 326
    label "tli&#263;_si&#281;"
  ]
  node [
    id 327
    label "bi&#263;_po_oczach"
  ]
  node [
    id 328
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 329
    label "uprzedza&#263;"
  ]
  node [
    id 330
    label "present"
  ]
  node [
    id 331
    label "program"
  ]
  node [
    id 332
    label "bole&#263;"
  ]
  node [
    id 333
    label "naciska&#263;"
  ]
  node [
    id 334
    label "strzela&#263;"
  ]
  node [
    id 335
    label "popyla&#263;"
  ]
  node [
    id 336
    label "gada&#263;"
  ]
  node [
    id 337
    label "harowa&#263;"
  ]
  node [
    id 338
    label "bi&#263;"
  ]
  node [
    id 339
    label "uderza&#263;"
  ]
  node [
    id 340
    label "psu&#263;_si&#281;"
  ]
  node [
    id 341
    label "taktycznie"
  ]
  node [
    id 342
    label "wojskowy"
  ]
  node [
    id 343
    label "strategicznie"
  ]
  node [
    id 344
    label "ostro&#380;ny"
  ]
  node [
    id 345
    label "perspektywiczny"
  ]
  node [
    id 346
    label "taktyczny"
  ]
  node [
    id 347
    label "perspektywicznie"
  ]
  node [
    id 348
    label "ostro&#380;nie"
  ]
  node [
    id 349
    label "obiecuj&#261;co"
  ]
  node [
    id 350
    label "rozwa&#380;ny"
  ]
  node [
    id 351
    label "dobry"
  ]
  node [
    id 352
    label "so&#322;dat"
  ]
  node [
    id 353
    label "rota"
  ]
  node [
    id 354
    label "militarnie"
  ]
  node [
    id 355
    label "elew"
  ]
  node [
    id 356
    label "wojskowo"
  ]
  node [
    id 357
    label "Gurkha"
  ]
  node [
    id 358
    label "zdemobilizowanie"
  ]
  node [
    id 359
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 360
    label "walcz&#261;cy"
  ]
  node [
    id 361
    label "&#380;o&#322;dowy"
  ]
  node [
    id 362
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 363
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 364
    label "antybalistyczny"
  ]
  node [
    id 365
    label "demobilizowa&#263;"
  ]
  node [
    id 366
    label "podleg&#322;y"
  ]
  node [
    id 367
    label "specjalny"
  ]
  node [
    id 368
    label "harcap"
  ]
  node [
    id 369
    label "wojsko"
  ]
  node [
    id 370
    label "demobilizowanie"
  ]
  node [
    id 371
    label "typowy"
  ]
  node [
    id 372
    label "mundurowy"
  ]
  node [
    id 373
    label "zdemobilizowa&#263;"
  ]
  node [
    id 374
    label "zmienno&#347;&#263;"
  ]
  node [
    id 375
    label "rozgrywka"
  ]
  node [
    id 376
    label "apparent_motion"
  ]
  node [
    id 377
    label "wydarzenie"
  ]
  node [
    id 378
    label "contest"
  ]
  node [
    id 379
    label "akcja"
  ]
  node [
    id 380
    label "komplet"
  ]
  node [
    id 381
    label "zabawa"
  ]
  node [
    id 382
    label "zasada"
  ]
  node [
    id 383
    label "rywalizacja"
  ]
  node [
    id 384
    label "zbijany"
  ]
  node [
    id 385
    label "post&#281;powanie"
  ]
  node [
    id 386
    label "game"
  ]
  node [
    id 387
    label "odg&#322;os"
  ]
  node [
    id 388
    label "Pok&#233;mon"
  ]
  node [
    id 389
    label "synteza"
  ]
  node [
    id 390
    label "odtworzenie"
  ]
  node [
    id 391
    label "rekwizyt_do_gry"
  ]
  node [
    id 392
    label "resonance"
  ]
  node [
    id 393
    label "wydanie"
  ]
  node [
    id 394
    label "wpadni&#281;cie"
  ]
  node [
    id 395
    label "wpadanie"
  ]
  node [
    id 396
    label "brzmienie"
  ]
  node [
    id 397
    label "zjawisko"
  ]
  node [
    id 398
    label "wyda&#263;"
  ]
  node [
    id 399
    label "wpa&#347;&#263;"
  ]
  node [
    id 400
    label "note"
  ]
  node [
    id 401
    label "onomatopeja"
  ]
  node [
    id 402
    label "wpada&#263;"
  ]
  node [
    id 403
    label "cecha"
  ]
  node [
    id 404
    label "s&#261;d"
  ]
  node [
    id 405
    label "kognicja"
  ]
  node [
    id 406
    label "campaign"
  ]
  node [
    id 407
    label "rozprawa"
  ]
  node [
    id 408
    label "zachowanie"
  ]
  node [
    id 409
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 410
    label "fashion"
  ]
  node [
    id 411
    label "robienie"
  ]
  node [
    id 412
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 413
    label "zmierzanie"
  ]
  node [
    id 414
    label "przes&#322;anka"
  ]
  node [
    id 415
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 416
    label "kazanie"
  ]
  node [
    id 417
    label "trafienie"
  ]
  node [
    id 418
    label "rewan&#380;owy"
  ]
  node [
    id 419
    label "zagrywka"
  ]
  node [
    id 420
    label "faza"
  ]
  node [
    id 421
    label "euroliga"
  ]
  node [
    id 422
    label "interliga"
  ]
  node [
    id 423
    label "runda"
  ]
  node [
    id 424
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 425
    label "rozrywka"
  ]
  node [
    id 426
    label "impreza"
  ]
  node [
    id 427
    label "igraszka"
  ]
  node [
    id 428
    label "taniec"
  ]
  node [
    id 429
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 430
    label "gambling"
  ]
  node [
    id 431
    label "chwyt"
  ]
  node [
    id 432
    label "igra"
  ]
  node [
    id 433
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 434
    label "nabawienie_si&#281;"
  ]
  node [
    id 435
    label "ubaw"
  ]
  node [
    id 436
    label "wodzirej"
  ]
  node [
    id 437
    label "activity"
  ]
  node [
    id 438
    label "bezproblemowy"
  ]
  node [
    id 439
    label "przebiec"
  ]
  node [
    id 440
    label "charakter"
  ]
  node [
    id 441
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 442
    label "motyw"
  ]
  node [
    id 443
    label "przebiegni&#281;cie"
  ]
  node [
    id 444
    label "fabu&#322;a"
  ]
  node [
    id 445
    label "proces_technologiczny"
  ]
  node [
    id 446
    label "mieszanina"
  ]
  node [
    id 447
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 448
    label "fusion"
  ]
  node [
    id 449
    label "poj&#281;cie"
  ]
  node [
    id 450
    label "zestawienie"
  ]
  node [
    id 451
    label "uog&#243;lnienie"
  ]
  node [
    id 452
    label "puszczenie"
  ]
  node [
    id 453
    label "ustalenie"
  ]
  node [
    id 454
    label "wyst&#281;p"
  ]
  node [
    id 455
    label "reproduction"
  ]
  node [
    id 456
    label "przywr&#243;cenie"
  ]
  node [
    id 457
    label "w&#322;&#261;czenie"
  ]
  node [
    id 458
    label "zregenerowanie_si&#281;"
  ]
  node [
    id 459
    label "restoration"
  ]
  node [
    id 460
    label "odbudowanie"
  ]
  node [
    id 461
    label "lekcja"
  ]
  node [
    id 462
    label "ensemble"
  ]
  node [
    id 463
    label "grupa"
  ]
  node [
    id 464
    label "klasa"
  ]
  node [
    id 465
    label "zestaw"
  ]
  node [
    id 466
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 467
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 468
    label "regu&#322;a_Allena"
  ]
  node [
    id 469
    label "base"
  ]
  node [
    id 470
    label "umowa"
  ]
  node [
    id 471
    label "obserwacja"
  ]
  node [
    id 472
    label "zasada_d'Alemberta"
  ]
  node [
    id 473
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 474
    label "normalizacja"
  ]
  node [
    id 475
    label "moralno&#347;&#263;"
  ]
  node [
    id 476
    label "criterion"
  ]
  node [
    id 477
    label "opis"
  ]
  node [
    id 478
    label "regu&#322;a_Glogera"
  ]
  node [
    id 479
    label "prawo_Mendla"
  ]
  node [
    id 480
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 481
    label "twierdzenie"
  ]
  node [
    id 482
    label "prawo"
  ]
  node [
    id 483
    label "standard"
  ]
  node [
    id 484
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 485
    label "spos&#243;b"
  ]
  node [
    id 486
    label "dominion"
  ]
  node [
    id 487
    label "qualification"
  ]
  node [
    id 488
    label "occupation"
  ]
  node [
    id 489
    label "podstawa"
  ]
  node [
    id 490
    label "substancja"
  ]
  node [
    id 491
    label "prawid&#322;o"
  ]
  node [
    id 492
    label "dywidenda"
  ]
  node [
    id 493
    label "przebieg"
  ]
  node [
    id 494
    label "operacja"
  ]
  node [
    id 495
    label "udzia&#322;"
  ]
  node [
    id 496
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 497
    label "commotion"
  ]
  node [
    id 498
    label "jazda"
  ]
  node [
    id 499
    label "stock"
  ]
  node [
    id 500
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 501
    label "w&#281;ze&#322;"
  ]
  node [
    id 502
    label "wysoko&#347;&#263;"
  ]
  node [
    id 503
    label "instrument_strunowy"
  ]
  node [
    id 504
    label "pi&#322;ka"
  ]
  node [
    id 505
    label "prawny"
  ]
  node [
    id 506
    label "konstytucyjnoprawny"
  ]
  node [
    id 507
    label "prawniczo"
  ]
  node [
    id 508
    label "prawnie"
  ]
  node [
    id 509
    label "legalny"
  ]
  node [
    id 510
    label "jurydyczny"
  ]
  node [
    id 511
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 512
    label "equal"
  ]
  node [
    id 513
    label "trwa&#263;"
  ]
  node [
    id 514
    label "chodzi&#263;"
  ]
  node [
    id 515
    label "si&#281;ga&#263;"
  ]
  node [
    id 516
    label "stan"
  ]
  node [
    id 517
    label "obecno&#347;&#263;"
  ]
  node [
    id 518
    label "stand"
  ]
  node [
    id 519
    label "uczestniczy&#263;"
  ]
  node [
    id 520
    label "participate"
  ]
  node [
    id 521
    label "pozostawa&#263;"
  ]
  node [
    id 522
    label "zostawa&#263;"
  ]
  node [
    id 523
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 524
    label "adhere"
  ]
  node [
    id 525
    label "compass"
  ]
  node [
    id 526
    label "appreciation"
  ]
  node [
    id 527
    label "osi&#261;ga&#263;"
  ]
  node [
    id 528
    label "dociera&#263;"
  ]
  node [
    id 529
    label "get"
  ]
  node [
    id 530
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 531
    label "mierzy&#263;"
  ]
  node [
    id 532
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 533
    label "exsert"
  ]
  node [
    id 534
    label "being"
  ]
  node [
    id 535
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 536
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 537
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 538
    label "p&#322;ywa&#263;"
  ]
  node [
    id 539
    label "run"
  ]
  node [
    id 540
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 541
    label "przebiega&#263;"
  ]
  node [
    id 542
    label "wk&#322;ada&#263;"
  ]
  node [
    id 543
    label "carry"
  ]
  node [
    id 544
    label "bywa&#263;"
  ]
  node [
    id 545
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 546
    label "para"
  ]
  node [
    id 547
    label "str&#243;j"
  ]
  node [
    id 548
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 549
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 550
    label "krok"
  ]
  node [
    id 551
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 552
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 553
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 554
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 555
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 556
    label "Ohio"
  ]
  node [
    id 557
    label "wci&#281;cie"
  ]
  node [
    id 558
    label "Nowy_York"
  ]
  node [
    id 559
    label "warstwa"
  ]
  node [
    id 560
    label "samopoczucie"
  ]
  node [
    id 561
    label "Illinois"
  ]
  node [
    id 562
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 563
    label "state"
  ]
  node [
    id 564
    label "Jukatan"
  ]
  node [
    id 565
    label "Kalifornia"
  ]
  node [
    id 566
    label "Wirginia"
  ]
  node [
    id 567
    label "wektor"
  ]
  node [
    id 568
    label "Goa"
  ]
  node [
    id 569
    label "Teksas"
  ]
  node [
    id 570
    label "Waszyngton"
  ]
  node [
    id 571
    label "miejsce"
  ]
  node [
    id 572
    label "Massachusetts"
  ]
  node [
    id 573
    label "Alaska"
  ]
  node [
    id 574
    label "Arakan"
  ]
  node [
    id 575
    label "Hawaje"
  ]
  node [
    id 576
    label "Maryland"
  ]
  node [
    id 577
    label "punkt"
  ]
  node [
    id 578
    label "Michigan"
  ]
  node [
    id 579
    label "Arizona"
  ]
  node [
    id 580
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 581
    label "Georgia"
  ]
  node [
    id 582
    label "poziom"
  ]
  node [
    id 583
    label "Pensylwania"
  ]
  node [
    id 584
    label "shape"
  ]
  node [
    id 585
    label "Luizjana"
  ]
  node [
    id 586
    label "Nowy_Meksyk"
  ]
  node [
    id 587
    label "Alabama"
  ]
  node [
    id 588
    label "ilo&#347;&#263;"
  ]
  node [
    id 589
    label "Kansas"
  ]
  node [
    id 590
    label "Oregon"
  ]
  node [
    id 591
    label "Oklahoma"
  ]
  node [
    id 592
    label "Floryda"
  ]
  node [
    id 593
    label "jednostka_administracyjna"
  ]
  node [
    id 594
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 595
    label "niezmierny"
  ]
  node [
    id 596
    label "szczytnie"
  ]
  node [
    id 597
    label "ogromnie"
  ]
  node [
    id 598
    label "ogromny"
  ]
  node [
    id 599
    label "olbrzymi"
  ]
  node [
    id 600
    label "dono&#347;nie"
  ]
  node [
    id 601
    label "bardzo"
  ]
  node [
    id 602
    label "intensywnie"
  ]
  node [
    id 603
    label "wznio&#347;le"
  ]
  node [
    id 604
    label "chwalebnie"
  ]
  node [
    id 605
    label "szczytny"
  ]
  node [
    id 606
    label "przestrze&#324;"
  ]
  node [
    id 607
    label "rozleg&#322;y"
  ]
  node [
    id 608
    label "nieograniczenie"
  ]
  node [
    id 609
    label "pull"
  ]
  node [
    id 610
    label "wprowadza&#263;"
  ]
  node [
    id 611
    label "rynek"
  ]
  node [
    id 612
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 613
    label "wprawia&#263;"
  ]
  node [
    id 614
    label "zaczyna&#263;"
  ]
  node [
    id 615
    label "wpisywa&#263;"
  ]
  node [
    id 616
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 617
    label "wchodzi&#263;"
  ]
  node [
    id 618
    label "take"
  ]
  node [
    id 619
    label "inflict"
  ]
  node [
    id 620
    label "umieszcza&#263;"
  ]
  node [
    id 621
    label "schodzi&#263;"
  ]
  node [
    id 622
    label "induct"
  ]
  node [
    id 623
    label "begin"
  ]
  node [
    id 624
    label "doprowadza&#263;"
  ]
  node [
    id 625
    label "nastawia&#263;"
  ]
  node [
    id 626
    label "get_in_touch"
  ]
  node [
    id 627
    label "odkr&#281;ca&#263;_kurek"
  ]
  node [
    id 628
    label "dokoptowywa&#263;"
  ]
  node [
    id 629
    label "ogl&#261;da&#263;"
  ]
  node [
    id 630
    label "uruchamia&#263;"
  ]
  node [
    id 631
    label "involve"
  ]
  node [
    id 632
    label "connect"
  ]
  node [
    id 633
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 634
    label "motywowa&#263;"
  ]
  node [
    id 635
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 636
    label "odwadnia&#263;"
  ]
  node [
    id 637
    label "wi&#261;zanie"
  ]
  node [
    id 638
    label "odwodni&#263;"
  ]
  node [
    id 639
    label "bratnia_dusza"
  ]
  node [
    id 640
    label "powi&#261;zanie"
  ]
  node [
    id 641
    label "zwi&#261;zanie"
  ]
  node [
    id 642
    label "konstytucja"
  ]
  node [
    id 643
    label "organizacja"
  ]
  node [
    id 644
    label "marriage"
  ]
  node [
    id 645
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 646
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 647
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 648
    label "zwi&#261;za&#263;"
  ]
  node [
    id 649
    label "odwadnianie"
  ]
  node [
    id 650
    label "odwodnienie"
  ]
  node [
    id 651
    label "marketing_afiliacyjny"
  ]
  node [
    id 652
    label "substancja_chemiczna"
  ]
  node [
    id 653
    label "koligacja"
  ]
  node [
    id 654
    label "bearing"
  ]
  node [
    id 655
    label "lokant"
  ]
  node [
    id 656
    label "azeotrop"
  ]
  node [
    id 657
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 658
    label "dehydration"
  ]
  node [
    id 659
    label "oznaka"
  ]
  node [
    id 660
    label "osuszenie"
  ]
  node [
    id 661
    label "spowodowanie"
  ]
  node [
    id 662
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 663
    label "cia&#322;o"
  ]
  node [
    id 664
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 665
    label "odprowadzenie"
  ]
  node [
    id 666
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 667
    label "odsuni&#281;cie"
  ]
  node [
    id 668
    label "odsun&#261;&#263;"
  ]
  node [
    id 669
    label "drain"
  ]
  node [
    id 670
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 671
    label "odprowadzi&#263;"
  ]
  node [
    id 672
    label "osuszy&#263;"
  ]
  node [
    id 673
    label "numeracja"
  ]
  node [
    id 674
    label "odprowadza&#263;"
  ]
  node [
    id 675
    label "osusza&#263;"
  ]
  node [
    id 676
    label "odci&#261;ga&#263;"
  ]
  node [
    id 677
    label "odsuwa&#263;"
  ]
  node [
    id 678
    label "struktura"
  ]
  node [
    id 679
    label "zbi&#243;r"
  ]
  node [
    id 680
    label "akt"
  ]
  node [
    id 681
    label "cezar"
  ]
  node [
    id 682
    label "dokument"
  ]
  node [
    id 683
    label "budowa"
  ]
  node [
    id 684
    label "uchwa&#322;a"
  ]
  node [
    id 685
    label "odprowadzanie"
  ]
  node [
    id 686
    label "powodowanie"
  ]
  node [
    id 687
    label "odci&#261;ganie"
  ]
  node [
    id 688
    label "dehydratacja"
  ]
  node [
    id 689
    label "osuszanie"
  ]
  node [
    id 690
    label "proces_chemiczny"
  ]
  node [
    id 691
    label "odsuwanie"
  ]
  node [
    id 692
    label "ograniczenie"
  ]
  node [
    id 693
    label "po&#322;&#261;czenie"
  ]
  node [
    id 694
    label "do&#322;&#261;czenie"
  ]
  node [
    id 695
    label "opakowanie"
  ]
  node [
    id 696
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 697
    label "attachment"
  ]
  node [
    id 698
    label "obezw&#322;adnienie"
  ]
  node [
    id 699
    label "zawi&#261;zanie"
  ]
  node [
    id 700
    label "wi&#281;&#378;"
  ]
  node [
    id 701
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 702
    label "tying"
  ]
  node [
    id 703
    label "st&#281;&#380;enie"
  ]
  node [
    id 704
    label "affiliation"
  ]
  node [
    id 705
    label "fastening"
  ]
  node [
    id 706
    label "zaprawa"
  ]
  node [
    id 707
    label "nawi&#261;zanie_si&#281;"
  ]
  node [
    id 708
    label "z&#322;&#261;czenie"
  ]
  node [
    id 709
    label "zobowi&#261;zanie"
  ]
  node [
    id 710
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 711
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 712
    label "consort"
  ]
  node [
    id 713
    label "cement"
  ]
  node [
    id 714
    label "opakowa&#263;"
  ]
  node [
    id 715
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 716
    label "relate"
  ]
  node [
    id 717
    label "form"
  ]
  node [
    id 718
    label "tobo&#322;ek"
  ]
  node [
    id 719
    label "unify"
  ]
  node [
    id 720
    label "incorporate"
  ]
  node [
    id 721
    label "bind"
  ]
  node [
    id 722
    label "zawi&#261;za&#263;"
  ]
  node [
    id 723
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 724
    label "powi&#261;za&#263;"
  ]
  node [
    id 725
    label "scali&#263;"
  ]
  node [
    id 726
    label "zatrzyma&#263;"
  ]
  node [
    id 727
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 728
    label "narta"
  ]
  node [
    id 729
    label "przedmiot"
  ]
  node [
    id 730
    label "podwi&#261;zywanie"
  ]
  node [
    id 731
    label "dressing"
  ]
  node [
    id 732
    label "socket"
  ]
  node [
    id 733
    label "szermierka"
  ]
  node [
    id 734
    label "przywi&#261;zywanie"
  ]
  node [
    id 735
    label "pakowanie"
  ]
  node [
    id 736
    label "my&#347;lenie"
  ]
  node [
    id 737
    label "do&#322;&#261;czanie"
  ]
  node [
    id 738
    label "communication"
  ]
  node [
    id 739
    label "wytwarzanie"
  ]
  node [
    id 740
    label "ceg&#322;a"
  ]
  node [
    id 741
    label "combination"
  ]
  node [
    id 742
    label "zobowi&#261;zywanie"
  ]
  node [
    id 743
    label "szcz&#281;ka"
  ]
  node [
    id 744
    label "anga&#380;owanie"
  ]
  node [
    id 745
    label "wi&#261;za&#263;"
  ]
  node [
    id 746
    label "twardnienie"
  ]
  node [
    id 747
    label "podwi&#261;zanie"
  ]
  node [
    id 748
    label "przywi&#261;zanie"
  ]
  node [
    id 749
    label "przymocowywanie"
  ]
  node [
    id 750
    label "scalanie"
  ]
  node [
    id 751
    label "mezomeria"
  ]
  node [
    id 752
    label "kojarzenie_si&#281;"
  ]
  node [
    id 753
    label "&#322;&#261;czenie"
  ]
  node [
    id 754
    label "uchwyt"
  ]
  node [
    id 755
    label "warto&#347;ciowo&#347;&#263;"
  ]
  node [
    id 756
    label "rozmieszczenie"
  ]
  node [
    id 757
    label "zmiana"
  ]
  node [
    id 758
    label "element_konstrukcyjny"
  ]
  node [
    id 759
    label "obezw&#322;adnianie"
  ]
  node [
    id 760
    label "manewr"
  ]
  node [
    id 761
    label "miecz"
  ]
  node [
    id 762
    label "oddzia&#322;ywanie"
  ]
  node [
    id 763
    label "obwi&#261;zanie"
  ]
  node [
    id 764
    label "zawi&#261;zek"
  ]
  node [
    id 765
    label "obwi&#261;zywanie"
  ]
  node [
    id 766
    label "roztw&#243;r"
  ]
  node [
    id 767
    label "podmiot"
  ]
  node [
    id 768
    label "jednostka_organizacyjna"
  ]
  node [
    id 769
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 770
    label "TOPR"
  ]
  node [
    id 771
    label "endecki"
  ]
  node [
    id 772
    label "zesp&#243;&#322;"
  ]
  node [
    id 773
    label "przedstawicielstwo"
  ]
  node [
    id 774
    label "od&#322;am"
  ]
  node [
    id 775
    label "Cepelia"
  ]
  node [
    id 776
    label "ZBoWiD"
  ]
  node [
    id 777
    label "organization"
  ]
  node [
    id 778
    label "centrala"
  ]
  node [
    id 779
    label "GOPR"
  ]
  node [
    id 780
    label "ZOMO"
  ]
  node [
    id 781
    label "ZMP"
  ]
  node [
    id 782
    label "komitet_koordynacyjny"
  ]
  node [
    id 783
    label "przybud&#243;wka"
  ]
  node [
    id 784
    label "boj&#243;wka"
  ]
  node [
    id 785
    label "zrelatywizowa&#263;"
  ]
  node [
    id 786
    label "zrelatywizowanie"
  ]
  node [
    id 787
    label "mention"
  ]
  node [
    id 788
    label "pomy&#347;lenie"
  ]
  node [
    id 789
    label "relatywizowa&#263;"
  ]
  node [
    id 790
    label "relatywizowanie"
  ]
  node [
    id 791
    label "kontakt"
  ]
  node [
    id 792
    label "intensywny"
  ]
  node [
    id 793
    label "prosty"
  ]
  node [
    id 794
    label "kr&#243;tki"
  ]
  node [
    id 795
    label "temperamentny"
  ]
  node [
    id 796
    label "bystrolotny"
  ]
  node [
    id 797
    label "dynamiczny"
  ]
  node [
    id 798
    label "szybko"
  ]
  node [
    id 799
    label "sprawny"
  ]
  node [
    id 800
    label "bezpo&#347;redni"
  ]
  node [
    id 801
    label "energiczny"
  ]
  node [
    id 802
    label "aktywny"
  ]
  node [
    id 803
    label "mocny"
  ]
  node [
    id 804
    label "energicznie"
  ]
  node [
    id 805
    label "dynamizowanie"
  ]
  node [
    id 806
    label "zmienny"
  ]
  node [
    id 807
    label "&#380;ywy"
  ]
  node [
    id 808
    label "zdynamizowanie"
  ]
  node [
    id 809
    label "ostry"
  ]
  node [
    id 810
    label "dynamicznie"
  ]
  node [
    id 811
    label "Tuesday"
  ]
  node [
    id 812
    label "emocjonalny"
  ]
  node [
    id 813
    label "temperamentnie"
  ]
  node [
    id 814
    label "wyrazisty"
  ]
  node [
    id 815
    label "jary"
  ]
  node [
    id 816
    label "letki"
  ]
  node [
    id 817
    label "umiej&#281;tny"
  ]
  node [
    id 818
    label "zdrowy"
  ]
  node [
    id 819
    label "dzia&#322;alny"
  ]
  node [
    id 820
    label "sprawnie"
  ]
  node [
    id 821
    label "jednowyrazowy"
  ]
  node [
    id 822
    label "bliski"
  ]
  node [
    id 823
    label "s&#322;aby"
  ]
  node [
    id 824
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 825
    label "kr&#243;tko"
  ]
  node [
    id 826
    label "drobny"
  ]
  node [
    id 827
    label "ruch"
  ]
  node [
    id 828
    label "brak"
  ]
  node [
    id 829
    label "z&#322;y"
  ]
  node [
    id 830
    label "szczery"
  ]
  node [
    id 831
    label "skromny"
  ]
  node [
    id 832
    label "po_prostu"
  ]
  node [
    id 833
    label "naturalny"
  ]
  node [
    id 834
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 835
    label "rozprostowanie"
  ]
  node [
    id 836
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 837
    label "prosto"
  ]
  node [
    id 838
    label "prostowanie_si&#281;"
  ]
  node [
    id 839
    label "niepozorny"
  ]
  node [
    id 840
    label "cios"
  ]
  node [
    id 841
    label "prostoduszny"
  ]
  node [
    id 842
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 843
    label "naiwny"
  ]
  node [
    id 844
    label "&#322;atwy"
  ]
  node [
    id 845
    label "prostowanie"
  ]
  node [
    id 846
    label "zwyk&#322;y"
  ]
  node [
    id 847
    label "quickest"
  ]
  node [
    id 848
    label "szybciochem"
  ]
  node [
    id 849
    label "quicker"
  ]
  node [
    id 850
    label "szybciej"
  ]
  node [
    id 851
    label "promptly"
  ]
  node [
    id 852
    label "znacz&#261;cy"
  ]
  node [
    id 853
    label "zwarty"
  ]
  node [
    id 854
    label "efektywny"
  ]
  node [
    id 855
    label "ogrodnictwo"
  ]
  node [
    id 856
    label "pe&#322;ny"
  ]
  node [
    id 857
    label "nieproporcjonalny"
  ]
  node [
    id 858
    label "bystry"
  ]
  node [
    id 859
    label "lotny"
  ]
  node [
    id 860
    label "time"
  ]
  node [
    id 861
    label "blok"
  ]
  node [
    id 862
    label "handout"
  ]
  node [
    id 863
    label "pomiar"
  ]
  node [
    id 864
    label "lecture"
  ]
  node [
    id 865
    label "reading"
  ]
  node [
    id 866
    label "podawanie"
  ]
  node [
    id 867
    label "wyk&#322;ad"
  ]
  node [
    id 868
    label "potrzyma&#263;"
  ]
  node [
    id 869
    label "warunki"
  ]
  node [
    id 870
    label "pok&#243;j"
  ]
  node [
    id 871
    label "atak"
  ]
  node [
    id 872
    label "meteorology"
  ]
  node [
    id 873
    label "weather"
  ]
  node [
    id 874
    label "prognoza_meteorologiczna"
  ]
  node [
    id 875
    label "czas_wolny"
  ]
  node [
    id 876
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 877
    label "metrologia"
  ]
  node [
    id 878
    label "godzinnik"
  ]
  node [
    id 879
    label "bicie"
  ]
  node [
    id 880
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 881
    label "wahad&#322;o"
  ]
  node [
    id 882
    label "kurant"
  ]
  node [
    id 883
    label "cyferblat"
  ]
  node [
    id 884
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 885
    label "nabicie"
  ]
  node [
    id 886
    label "werk"
  ]
  node [
    id 887
    label "czasomierz"
  ]
  node [
    id 888
    label "tyka&#263;"
  ]
  node [
    id 889
    label "tykn&#261;&#263;"
  ]
  node [
    id 890
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 891
    label "urz&#261;dzenie"
  ]
  node [
    id 892
    label "kotwica"
  ]
  node [
    id 893
    label "fleksja"
  ]
  node [
    id 894
    label "liczba"
  ]
  node [
    id 895
    label "coupling"
  ]
  node [
    id 896
    label "osoba"
  ]
  node [
    id 897
    label "czasownik"
  ]
  node [
    id 898
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 899
    label "orz&#281;sek"
  ]
  node [
    id 900
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 901
    label "zaczynanie_si&#281;"
  ]
  node [
    id 902
    label "wynikanie"
  ]
  node [
    id 903
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 904
    label "origin"
  ]
  node [
    id 905
    label "background"
  ]
  node [
    id 906
    label "geneza"
  ]
  node [
    id 907
    label "beginning"
  ]
  node [
    id 908
    label "digestion"
  ]
  node [
    id 909
    label "unicestwianie"
  ]
  node [
    id 910
    label "sp&#281;dzanie"
  ]
  node [
    id 911
    label "contemplation"
  ]
  node [
    id 912
    label "rozk&#322;adanie"
  ]
  node [
    id 913
    label "marnowanie"
  ]
  node [
    id 914
    label "proces_fizjologiczny"
  ]
  node [
    id 915
    label "przetrawianie"
  ]
  node [
    id 916
    label "perystaltyka"
  ]
  node [
    id 917
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 918
    label "przebywa&#263;"
  ]
  node [
    id 919
    label "pour"
  ]
  node [
    id 920
    label "sail"
  ]
  node [
    id 921
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 922
    label "go&#347;ci&#263;"
  ]
  node [
    id 923
    label "mija&#263;"
  ]
  node [
    id 924
    label "odej&#347;cie"
  ]
  node [
    id 925
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 926
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 927
    label "zanikni&#281;cie"
  ]
  node [
    id 928
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 929
    label "ciecz"
  ]
  node [
    id 930
    label "opuszczenie"
  ]
  node [
    id 931
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 932
    label "departure"
  ]
  node [
    id 933
    label "oddalenie_si&#281;"
  ]
  node [
    id 934
    label "przeby&#263;"
  ]
  node [
    id 935
    label "min&#261;&#263;"
  ]
  node [
    id 936
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 937
    label "swimming"
  ]
  node [
    id 938
    label "zago&#347;ci&#263;"
  ]
  node [
    id 939
    label "cross"
  ]
  node [
    id 940
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 941
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 942
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 943
    label "zrobi&#263;"
  ]
  node [
    id 944
    label "opatrzy&#263;"
  ]
  node [
    id 945
    label "overwhelm"
  ]
  node [
    id 946
    label "opatrywa&#263;"
  ]
  node [
    id 947
    label "date"
  ]
  node [
    id 948
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 949
    label "wynika&#263;"
  ]
  node [
    id 950
    label "fall"
  ]
  node [
    id 951
    label "poby&#263;"
  ]
  node [
    id 952
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 953
    label "bolt"
  ]
  node [
    id 954
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 955
    label "uda&#263;_si&#281;"
  ]
  node [
    id 956
    label "opatrzenie"
  ]
  node [
    id 957
    label "zdarzenie_si&#281;"
  ]
  node [
    id 958
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 959
    label "progress"
  ]
  node [
    id 960
    label "opatrywanie"
  ]
  node [
    id 961
    label "mini&#281;cie"
  ]
  node [
    id 962
    label "doznanie"
  ]
  node [
    id 963
    label "zaistnienie"
  ]
  node [
    id 964
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 965
    label "przebycie"
  ]
  node [
    id 966
    label "cruise"
  ]
  node [
    id 967
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 968
    label "usuwa&#263;"
  ]
  node [
    id 969
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 970
    label "lutowa&#263;"
  ]
  node [
    id 971
    label "marnowa&#263;"
  ]
  node [
    id 972
    label "przetrawia&#263;"
  ]
  node [
    id 973
    label "poch&#322;ania&#263;"
  ]
  node [
    id 974
    label "digest"
  ]
  node [
    id 975
    label "metal"
  ]
  node [
    id 976
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 977
    label "sp&#281;dza&#263;"
  ]
  node [
    id 978
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 979
    label "zjawianie_si&#281;"
  ]
  node [
    id 980
    label "przebywanie"
  ]
  node [
    id 981
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 982
    label "mijanie"
  ]
  node [
    id 983
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 984
    label "zaznawanie"
  ]
  node [
    id 985
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 986
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 987
    label "flux"
  ]
  node [
    id 988
    label "epoka"
  ]
  node [
    id 989
    label "flow"
  ]
  node [
    id 990
    label "choroba_przyrodzona"
  ]
  node [
    id 991
    label "ciota"
  ]
  node [
    id 992
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 993
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 994
    label "kres"
  ]
  node [
    id 995
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 996
    label "enormousness"
  ]
  node [
    id 997
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 998
    label "rozmiar"
  ]
  node [
    id 999
    label "part"
  ]
  node [
    id 1000
    label "publikacja"
  ]
  node [
    id 1001
    label "wiedza"
  ]
  node [
    id 1002
    label "obiega&#263;"
  ]
  node [
    id 1003
    label "powzi&#281;cie"
  ]
  node [
    id 1004
    label "dane"
  ]
  node [
    id 1005
    label "obiegni&#281;cie"
  ]
  node [
    id 1006
    label "sygna&#322;"
  ]
  node [
    id 1007
    label "obieganie"
  ]
  node [
    id 1008
    label "powzi&#261;&#263;"
  ]
  node [
    id 1009
    label "obiec"
  ]
  node [
    id 1010
    label "doj&#347;cie"
  ]
  node [
    id 1011
    label "doj&#347;&#263;"
  ]
  node [
    id 1012
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1013
    label "sprawa"
  ]
  node [
    id 1014
    label "ust&#281;p"
  ]
  node [
    id 1015
    label "plan"
  ]
  node [
    id 1016
    label "obiekt_matematyczny"
  ]
  node [
    id 1017
    label "problemat"
  ]
  node [
    id 1018
    label "plamka"
  ]
  node [
    id 1019
    label "stopie&#324;_pisma"
  ]
  node [
    id 1020
    label "jednostka"
  ]
  node [
    id 1021
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1022
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1023
    label "mark"
  ]
  node [
    id 1024
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1025
    label "prosta"
  ]
  node [
    id 1026
    label "problematyka"
  ]
  node [
    id 1027
    label "obiekt"
  ]
  node [
    id 1028
    label "zapunktowa&#263;"
  ]
  node [
    id 1029
    label "podpunkt"
  ]
  node [
    id 1030
    label "point"
  ]
  node [
    id 1031
    label "pozycja"
  ]
  node [
    id 1032
    label "cognition"
  ]
  node [
    id 1033
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 1034
    label "intelekt"
  ]
  node [
    id 1035
    label "pozwolenie"
  ]
  node [
    id 1036
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1037
    label "zaawansowanie"
  ]
  node [
    id 1038
    label "wykszta&#322;cenie"
  ]
  node [
    id 1039
    label "przekazywa&#263;"
  ]
  node [
    id 1040
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 1041
    label "pulsation"
  ]
  node [
    id 1042
    label "przekazywanie"
  ]
  node [
    id 1043
    label "przewodzenie"
  ]
  node [
    id 1044
    label "fala"
  ]
  node [
    id 1045
    label "przekazanie"
  ]
  node [
    id 1046
    label "przewodzi&#263;"
  ]
  node [
    id 1047
    label "znak"
  ]
  node [
    id 1048
    label "zapowied&#378;"
  ]
  node [
    id 1049
    label "medium_transmisyjne"
  ]
  node [
    id 1050
    label "demodulacja"
  ]
  node [
    id 1051
    label "przekaza&#263;"
  ]
  node [
    id 1052
    label "czynnik"
  ]
  node [
    id 1053
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 1054
    label "aliasing"
  ]
  node [
    id 1055
    label "wizja"
  ]
  node [
    id 1056
    label "modulacja"
  ]
  node [
    id 1057
    label "drift"
  ]
  node [
    id 1058
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 1059
    label "druk"
  ]
  node [
    id 1060
    label "produkcja"
  ]
  node [
    id 1061
    label "notification"
  ]
  node [
    id 1062
    label "edytowa&#263;"
  ]
  node [
    id 1063
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 1064
    label "spakowanie"
  ]
  node [
    id 1065
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1066
    label "pakowa&#263;"
  ]
  node [
    id 1067
    label "rekord"
  ]
  node [
    id 1068
    label "korelator"
  ]
  node [
    id 1069
    label "wyci&#261;ganie"
  ]
  node [
    id 1070
    label "sekwencjonowa&#263;"
  ]
  node [
    id 1071
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 1072
    label "jednostka_informacji"
  ]
  node [
    id 1073
    label "evidence"
  ]
  node [
    id 1074
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 1075
    label "rozpakowywanie"
  ]
  node [
    id 1076
    label "rozpakowanie"
  ]
  node [
    id 1077
    label "rozpakowywa&#263;"
  ]
  node [
    id 1078
    label "konwersja"
  ]
  node [
    id 1079
    label "nap&#322;ywanie"
  ]
  node [
    id 1080
    label "rozpakowa&#263;"
  ]
  node [
    id 1081
    label "spakowa&#263;"
  ]
  node [
    id 1082
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 1083
    label "edytowanie"
  ]
  node [
    id 1084
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 1085
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 1086
    label "sekwencjonowanie"
  ]
  node [
    id 1087
    label "dochodzenie"
  ]
  node [
    id 1088
    label "uzyskanie"
  ]
  node [
    id 1089
    label "skill"
  ]
  node [
    id 1090
    label "dochrapanie_si&#281;"
  ]
  node [
    id 1091
    label "znajomo&#347;ci"
  ]
  node [
    id 1092
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 1093
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 1094
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1095
    label "entrance"
  ]
  node [
    id 1096
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1097
    label "dor&#281;czenie"
  ]
  node [
    id 1098
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 1099
    label "bodziec"
  ]
  node [
    id 1100
    label "dost&#281;p"
  ]
  node [
    id 1101
    label "przesy&#322;ka"
  ]
  node [
    id 1102
    label "gotowy"
  ]
  node [
    id 1103
    label "avenue"
  ]
  node [
    id 1104
    label "postrzeganie"
  ]
  node [
    id 1105
    label "dodatek"
  ]
  node [
    id 1106
    label "dojrza&#322;y"
  ]
  node [
    id 1107
    label "dojechanie"
  ]
  node [
    id 1108
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 1109
    label "ingress"
  ]
  node [
    id 1110
    label "strzelenie"
  ]
  node [
    id 1111
    label "orzekni&#281;cie"
  ]
  node [
    id 1112
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1113
    label "orgazm"
  ]
  node [
    id 1114
    label "dolecenie"
  ]
  node [
    id 1115
    label "rozpowszechnienie"
  ]
  node [
    id 1116
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1117
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 1118
    label "stanie_si&#281;"
  ]
  node [
    id 1119
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 1120
    label "dop&#322;ata"
  ]
  node [
    id 1121
    label "zrobienie"
  ]
  node [
    id 1122
    label "odwiedzi&#263;"
  ]
  node [
    id 1123
    label "rozpowszechni&#263;_si&#281;"
  ]
  node [
    id 1124
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 1125
    label "orb"
  ]
  node [
    id 1126
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1127
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 1128
    label "supervene"
  ]
  node [
    id 1129
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 1130
    label "zaj&#347;&#263;"
  ]
  node [
    id 1131
    label "catch"
  ]
  node [
    id 1132
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 1133
    label "heed"
  ]
  node [
    id 1134
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 1135
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 1136
    label "dozna&#263;"
  ]
  node [
    id 1137
    label "dokoptowa&#263;"
  ]
  node [
    id 1138
    label "postrzega&#263;"
  ]
  node [
    id 1139
    label "dolecie&#263;"
  ]
  node [
    id 1140
    label "drive"
  ]
  node [
    id 1141
    label "dotrze&#263;"
  ]
  node [
    id 1142
    label "uzyska&#263;"
  ]
  node [
    id 1143
    label "become"
  ]
  node [
    id 1144
    label "podj&#261;&#263;"
  ]
  node [
    id 1145
    label "zacz&#261;&#263;"
  ]
  node [
    id 1146
    label "otrzyma&#263;"
  ]
  node [
    id 1147
    label "odwiedza&#263;"
  ]
  node [
    id 1148
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 1149
    label "rotate"
  ]
  node [
    id 1150
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 1151
    label "authorize"
  ]
  node [
    id 1152
    label "odwiedzanie"
  ]
  node [
    id 1153
    label "biegni&#281;cie"
  ]
  node [
    id 1154
    label "zakre&#347;lanie"
  ]
  node [
    id 1155
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 1156
    label "okr&#261;&#380;anie"
  ]
  node [
    id 1157
    label "rozpowszechnienie_si&#281;"
  ]
  node [
    id 1158
    label "zakre&#347;lenie"
  ]
  node [
    id 1159
    label "odwiedzenie"
  ]
  node [
    id 1160
    label "okr&#261;&#380;enie"
  ]
  node [
    id 1161
    label "podj&#281;cie"
  ]
  node [
    id 1162
    label "otrzymanie"
  ]
  node [
    id 1163
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1164
    label "facet"
  ]
  node [
    id 1165
    label "jednostka_systematyczna"
  ]
  node [
    id 1166
    label "kr&#243;lestwo"
  ]
  node [
    id 1167
    label "autorament"
  ]
  node [
    id 1168
    label "variety"
  ]
  node [
    id 1169
    label "antycypacja"
  ]
  node [
    id 1170
    label "przypuszczenie"
  ]
  node [
    id 1171
    label "cynk"
  ]
  node [
    id 1172
    label "obstawia&#263;"
  ]
  node [
    id 1173
    label "gromada"
  ]
  node [
    id 1174
    label "sztuka"
  ]
  node [
    id 1175
    label "rezultat"
  ]
  node [
    id 1176
    label "design"
  ]
  node [
    id 1177
    label "pob&#243;r"
  ]
  node [
    id 1178
    label "type"
  ]
  node [
    id 1179
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1180
    label "wygl&#261;d"
  ]
  node [
    id 1181
    label "pogl&#261;d"
  ]
  node [
    id 1182
    label "proces"
  ]
  node [
    id 1183
    label "wytw&#243;r"
  ]
  node [
    id 1184
    label "upodobnienie"
  ]
  node [
    id 1185
    label "narracja"
  ]
  node [
    id 1186
    label "prediction"
  ]
  node [
    id 1187
    label "pr&#243;bowanie"
  ]
  node [
    id 1188
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 1189
    label "realizacja"
  ]
  node [
    id 1190
    label "scena"
  ]
  node [
    id 1191
    label "didaskalia"
  ]
  node [
    id 1192
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 1193
    label "environment"
  ]
  node [
    id 1194
    label "head"
  ]
  node [
    id 1195
    label "egzemplarz"
  ]
  node [
    id 1196
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1197
    label "utw&#243;r"
  ]
  node [
    id 1198
    label "kultura_duchowa"
  ]
  node [
    id 1199
    label "fortel"
  ]
  node [
    id 1200
    label "theatrical_performance"
  ]
  node [
    id 1201
    label "ambala&#380;"
  ]
  node [
    id 1202
    label "sprawno&#347;&#263;"
  ]
  node [
    id 1203
    label "kobieta"
  ]
  node [
    id 1204
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1205
    label "Faust"
  ]
  node [
    id 1206
    label "scenografia"
  ]
  node [
    id 1207
    label "ods&#322;ona"
  ]
  node [
    id 1208
    label "turn"
  ]
  node [
    id 1209
    label "pokaz"
  ]
  node [
    id 1210
    label "przedstawi&#263;"
  ]
  node [
    id 1211
    label "Apollo"
  ]
  node [
    id 1212
    label "kultura"
  ]
  node [
    id 1213
    label "przedstawianie"
  ]
  node [
    id 1214
    label "towar"
  ]
  node [
    id 1215
    label "bratek"
  ]
  node [
    id 1216
    label "datum"
  ]
  node [
    id 1217
    label "poszlaka"
  ]
  node [
    id 1218
    label "dopuszczenie"
  ]
  node [
    id 1219
    label "teoria"
  ]
  node [
    id 1220
    label "conjecture"
  ]
  node [
    id 1221
    label "koniektura"
  ]
  node [
    id 1222
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1223
    label "asymilowanie"
  ]
  node [
    id 1224
    label "wapniak"
  ]
  node [
    id 1225
    label "asymilowa&#263;"
  ]
  node [
    id 1226
    label "os&#322;abia&#263;"
  ]
  node [
    id 1227
    label "hominid"
  ]
  node [
    id 1228
    label "podw&#322;adny"
  ]
  node [
    id 1229
    label "os&#322;abianie"
  ]
  node [
    id 1230
    label "g&#322;owa"
  ]
  node [
    id 1231
    label "figura"
  ]
  node [
    id 1232
    label "portrecista"
  ]
  node [
    id 1233
    label "dwun&#243;g"
  ]
  node [
    id 1234
    label "profanum"
  ]
  node [
    id 1235
    label "mikrokosmos"
  ]
  node [
    id 1236
    label "nasada"
  ]
  node [
    id 1237
    label "duch"
  ]
  node [
    id 1238
    label "antropochoria"
  ]
  node [
    id 1239
    label "wz&#243;r"
  ]
  node [
    id 1240
    label "senior"
  ]
  node [
    id 1241
    label "Adam"
  ]
  node [
    id 1242
    label "homo_sapiens"
  ]
  node [
    id 1243
    label "polifag"
  ]
  node [
    id 1244
    label "tip-off"
  ]
  node [
    id 1245
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 1246
    label "tip"
  ]
  node [
    id 1247
    label "metal_kolorowy"
  ]
  node [
    id 1248
    label "mikroelement"
  ]
  node [
    id 1249
    label "cynkowiec"
  ]
  node [
    id 1250
    label "ubezpiecza&#263;"
  ]
  node [
    id 1251
    label "venture"
  ]
  node [
    id 1252
    label "przewidywa&#263;"
  ]
  node [
    id 1253
    label "zapewnia&#263;"
  ]
  node [
    id 1254
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 1255
    label "typowa&#263;"
  ]
  node [
    id 1256
    label "ochrona"
  ]
  node [
    id 1257
    label "zastawia&#263;"
  ]
  node [
    id 1258
    label "budowa&#263;"
  ]
  node [
    id 1259
    label "zajmowa&#263;"
  ]
  node [
    id 1260
    label "obejmowa&#263;"
  ]
  node [
    id 1261
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 1262
    label "os&#322;ania&#263;"
  ]
  node [
    id 1263
    label "otacza&#263;"
  ]
  node [
    id 1264
    label "broni&#263;"
  ]
  node [
    id 1265
    label "powierza&#263;"
  ]
  node [
    id 1266
    label "bramka"
  ]
  node [
    id 1267
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 1268
    label "frame"
  ]
  node [
    id 1269
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1270
    label "dzia&#322;anie"
  ]
  node [
    id 1271
    label "event"
  ]
  node [
    id 1272
    label "przyczyna"
  ]
  node [
    id 1273
    label "zoologia"
  ]
  node [
    id 1274
    label "skupienie"
  ]
  node [
    id 1275
    label "stage_set"
  ]
  node [
    id 1276
    label "tribe"
  ]
  node [
    id 1277
    label "hurma"
  ]
  node [
    id 1278
    label "botanika"
  ]
  node [
    id 1279
    label "ro&#347;liny"
  ]
  node [
    id 1280
    label "grzyby"
  ]
  node [
    id 1281
    label "Arktogea"
  ]
  node [
    id 1282
    label "prokarioty"
  ]
  node [
    id 1283
    label "zwierz&#281;ta"
  ]
  node [
    id 1284
    label "domena"
  ]
  node [
    id 1285
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 1286
    label "protisty"
  ]
  node [
    id 1287
    label "pa&#324;stwo"
  ]
  node [
    id 1288
    label "terytorium"
  ]
  node [
    id 1289
    label "kategoria_systematyczna"
  ]
  node [
    id 1290
    label "niemi&#322;y"
  ]
  node [
    id 1291
    label "martwo"
  ]
  node [
    id 1292
    label "obumarcie"
  ]
  node [
    id 1293
    label "zimno"
  ]
  node [
    id 1294
    label "ozi&#281;bienie"
  ]
  node [
    id 1295
    label "opanowany"
  ]
  node [
    id 1296
    label "niezadowalaj&#261;cy"
  ]
  node [
    id 1297
    label "znieczulanie"
  ]
  node [
    id 1298
    label "umieranie"
  ]
  node [
    id 1299
    label "umarcie"
  ]
  node [
    id 1300
    label "wiszenie"
  ]
  node [
    id 1301
    label "rozs&#261;dny"
  ]
  node [
    id 1302
    label "ch&#322;odno"
  ]
  node [
    id 1303
    label "nieczu&#322;y"
  ]
  node [
    id 1304
    label "znieczulenie"
  ]
  node [
    id 1305
    label "obumieranie"
  ]
  node [
    id 1306
    label "niech&#281;tny"
  ]
  node [
    id 1307
    label "niezgodny"
  ]
  node [
    id 1308
    label "niezadowalaj&#261;co"
  ]
  node [
    id 1309
    label "pieski"
  ]
  node [
    id 1310
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 1311
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 1312
    label "niekorzystny"
  ]
  node [
    id 1313
    label "z&#322;oszczenie"
  ]
  node [
    id 1314
    label "sierdzisty"
  ]
  node [
    id 1315
    label "niegrzeczny"
  ]
  node [
    id 1316
    label "zez&#322;oszczenie"
  ]
  node [
    id 1317
    label "zdenerwowany"
  ]
  node [
    id 1318
    label "negatywny"
  ]
  node [
    id 1319
    label "rozgniewanie"
  ]
  node [
    id 1320
    label "gniewanie"
  ]
  node [
    id 1321
    label "niemoralny"
  ]
  node [
    id 1322
    label "&#378;le"
  ]
  node [
    id 1323
    label "niepomy&#347;lny"
  ]
  node [
    id 1324
    label "syf"
  ]
  node [
    id 1325
    label "nieczule"
  ]
  node [
    id 1326
    label "niepodatny"
  ]
  node [
    id 1327
    label "oboj&#281;tny"
  ]
  node [
    id 1328
    label "nieprzyjemnie"
  ]
  node [
    id 1329
    label "rozd&#281;cie_si&#281;"
  ]
  node [
    id 1330
    label "niemile"
  ]
  node [
    id 1331
    label "nie&#380;yczliwie"
  ]
  node [
    id 1332
    label "wstr&#281;tliwy"
  ]
  node [
    id 1333
    label "przemy&#347;lany"
  ]
  node [
    id 1334
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1335
    label "rozs&#261;dnie"
  ]
  node [
    id 1336
    label "rozumny"
  ]
  node [
    id 1337
    label "m&#261;dry"
  ]
  node [
    id 1338
    label "spokojny"
  ]
  node [
    id 1339
    label "anesthesia"
  ]
  node [
    id 1340
    label "zoboj&#281;tnienie"
  ]
  node [
    id 1341
    label "anesthetic"
  ]
  node [
    id 1342
    label "st&#322;umienie"
  ]
  node [
    id 1343
    label "anestezjolog"
  ]
  node [
    id 1344
    label "z&#322;agodzenie"
  ]
  node [
    id 1345
    label "lekarstwo"
  ]
  node [
    id 1346
    label "znieczulacz"
  ]
  node [
    id 1347
    label "hamowanie"
  ]
  node [
    id 1348
    label "oboj&#281;tnienie"
  ]
  node [
    id 1349
    label "&#322;agodzenie"
  ]
  node [
    id 1350
    label "opryszczkowe_zapalenie_opon_m&#243;zgowych_i_m&#243;zgu"
  ]
  node [
    id 1351
    label "kriofil"
  ]
  node [
    id 1352
    label "p&#281;cherz"
  ]
  node [
    id 1353
    label "wirus_opryszczki_pospolitej"
  ]
  node [
    id 1354
    label "choroba_wirusowa"
  ]
  node [
    id 1355
    label "coldness"
  ]
  node [
    id 1356
    label "temperatura"
  ]
  node [
    id 1357
    label "spokojnie"
  ]
  node [
    id 1358
    label "ch&#322;odny"
  ]
  node [
    id 1359
    label "przesycenie"
  ]
  node [
    id 1360
    label "och&#322;odzenie"
  ]
  node [
    id 1361
    label "pogorszenie"
  ]
  node [
    id 1362
    label "refrigeration"
  ]
  node [
    id 1363
    label "ch&#322;odnie"
  ]
  node [
    id 1364
    label "niesympatycznie"
  ]
  node [
    id 1365
    label "martwy"
  ]
  node [
    id 1366
    label "necrobiosis"
  ]
  node [
    id 1367
    label "stawanie_si&#281;"
  ]
  node [
    id 1368
    label "sag"
  ]
  node [
    id 1369
    label "powieszenie"
  ]
  node [
    id 1370
    label "trwanie"
  ]
  node [
    id 1371
    label "majtanie_si&#281;"
  ]
  node [
    id 1372
    label "unoszenie_si&#281;"
  ]
  node [
    id 1373
    label "dyndanie"
  ]
  node [
    id 1374
    label "odumarcie"
  ]
  node [
    id 1375
    label "przestanie"
  ]
  node [
    id 1376
    label "dysponowanie_si&#281;"
  ]
  node [
    id 1377
    label "&#380;ycie"
  ]
  node [
    id 1378
    label "pomarcie"
  ]
  node [
    id 1379
    label "die"
  ]
  node [
    id 1380
    label "sko&#324;czenie"
  ]
  node [
    id 1381
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 1382
    label "zdechni&#281;cie"
  ]
  node [
    id 1383
    label "zabicie"
  ]
  node [
    id 1384
    label "pu&#347;ciute&#324;ko"
  ]
  node [
    id 1385
    label "nieaktualnie"
  ]
  node [
    id 1386
    label "niesprawnie"
  ]
  node [
    id 1387
    label "oboj&#281;tnie"
  ]
  node [
    id 1388
    label "niezmiennie"
  ]
  node [
    id 1389
    label "nieruchomo"
  ]
  node [
    id 1390
    label "pusto"
  ]
  node [
    id 1391
    label "przygn&#281;biaj&#261;co"
  ]
  node [
    id 1392
    label "cicho"
  ]
  node [
    id 1393
    label "apatycznie"
  ]
  node [
    id 1394
    label "korkowanie"
  ]
  node [
    id 1395
    label "death"
  ]
  node [
    id 1396
    label "&#347;mier&#263;"
  ]
  node [
    id 1397
    label "zabijanie"
  ]
  node [
    id 1398
    label "przestawanie"
  ]
  node [
    id 1399
    label "odumieranie"
  ]
  node [
    id 1400
    label "zdychanie"
  ]
  node [
    id 1401
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 1402
    label "zanikanie"
  ]
  node [
    id 1403
    label "ko&#324;czenie"
  ]
  node [
    id 1404
    label "nieuleczalnie_chory"
  ]
  node [
    id 1405
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 1406
    label "necrosis"
  ]
  node [
    id 1407
    label "posusz"
  ]
  node [
    id 1408
    label "wydech"
  ]
  node [
    id 1409
    label "zatka&#263;"
  ]
  node [
    id 1410
    label "&#347;wista&#263;"
  ]
  node [
    id 1411
    label "zatyka&#263;"
  ]
  node [
    id 1412
    label "oddychanie"
  ]
  node [
    id 1413
    label "zaparcie_oddechu"
  ]
  node [
    id 1414
    label "rebirthing"
  ]
  node [
    id 1415
    label "zatkanie"
  ]
  node [
    id 1416
    label "zapieranie_oddechu"
  ]
  node [
    id 1417
    label "ud&#322;awienie_si&#281;"
  ]
  node [
    id 1418
    label "zapiera&#263;_oddech"
  ]
  node [
    id 1419
    label "ud&#322;awi&#263;_si&#281;"
  ]
  node [
    id 1420
    label "zaprze&#263;_oddech"
  ]
  node [
    id 1421
    label "zatykanie"
  ]
  node [
    id 1422
    label "wdech"
  ]
  node [
    id 1423
    label "hipowentylacja"
  ]
  node [
    id 1424
    label "wentylacja_p&#322;uc"
  ]
  node [
    id 1425
    label "chuch"
  ]
  node [
    id 1426
    label "piek&#322;o"
  ]
  node [
    id 1427
    label "human_body"
  ]
  node [
    id 1428
    label "ofiarowywanie"
  ]
  node [
    id 1429
    label "sfera_afektywna"
  ]
  node [
    id 1430
    label "nekromancja"
  ]
  node [
    id 1431
    label "Po&#347;wist"
  ]
  node [
    id 1432
    label "podekscytowanie"
  ]
  node [
    id 1433
    label "deformowanie"
  ]
  node [
    id 1434
    label "sumienie"
  ]
  node [
    id 1435
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1436
    label "deformowa&#263;"
  ]
  node [
    id 1437
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1438
    label "psychika"
  ]
  node [
    id 1439
    label "zjawa"
  ]
  node [
    id 1440
    label "zmar&#322;y"
  ]
  node [
    id 1441
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1442
    label "power"
  ]
  node [
    id 1443
    label "entity"
  ]
  node [
    id 1444
    label "ofiarowywa&#263;"
  ]
  node [
    id 1445
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1446
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1447
    label "byt"
  ]
  node [
    id 1448
    label "si&#322;a"
  ]
  node [
    id 1449
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1450
    label "ego"
  ]
  node [
    id 1451
    label "ofiarowanie"
  ]
  node [
    id 1452
    label "kompleksja"
  ]
  node [
    id 1453
    label "fizjonomia"
  ]
  node [
    id 1454
    label "kompleks"
  ]
  node [
    id 1455
    label "zapalno&#347;&#263;"
  ]
  node [
    id 1456
    label "T&#281;sknica"
  ]
  node [
    id 1457
    label "ofiarowa&#263;"
  ]
  node [
    id 1458
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1459
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1460
    label "passion"
  ]
  node [
    id 1461
    label "hipokapnia"
  ]
  node [
    id 1462
    label "odpr&#281;&#380;anie"
  ]
  node [
    id 1463
    label "odpoczywanie"
  ]
  node [
    id 1464
    label "hiperkapnia"
  ]
  node [
    id 1465
    label "expansion"
  ]
  node [
    id 1466
    label "proces_biologiczny"
  ]
  node [
    id 1467
    label "respiration"
  ]
  node [
    id 1468
    label "breathing"
  ]
  node [
    id 1469
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1470
    label "zablokowa&#263;"
  ]
  node [
    id 1471
    label "close"
  ]
  node [
    id 1472
    label "zamkn&#261;&#263;"
  ]
  node [
    id 1473
    label "zamurowa&#263;"
  ]
  node [
    id 1474
    label "zgasi&#263;"
  ]
  node [
    id 1475
    label "wia&#263;"
  ]
  node [
    id 1476
    label "piszcze&#263;"
  ]
  node [
    id 1477
    label "pipe"
  ]
  node [
    id 1478
    label "whistle"
  ]
  node [
    id 1479
    label "swish"
  ]
  node [
    id 1480
    label "macha&#263;"
  ]
  node [
    id 1481
    label "paramedycyna"
  ]
  node [
    id 1482
    label "przymocowywa&#263;"
  ]
  node [
    id 1483
    label "gasi&#263;"
  ]
  node [
    id 1484
    label "wsuwa&#263;"
  ]
  node [
    id 1485
    label "zamyka&#263;"
  ]
  node [
    id 1486
    label "hesitate"
  ]
  node [
    id 1487
    label "niewydolno&#347;&#263;"
  ]
  node [
    id 1488
    label "p&#322;uca"
  ]
  node [
    id 1489
    label "umieszczanie"
  ]
  node [
    id 1490
    label "zamykanie"
  ]
  node [
    id 1491
    label "blockage"
  ]
  node [
    id 1492
    label "chokehold"
  ]
  node [
    id 1493
    label "tamowanie"
  ]
  node [
    id 1494
    label "blokowanie"
  ]
  node [
    id 1495
    label "zatamowanie"
  ]
  node [
    id 1496
    label "zablokowanie"
  ]
  node [
    id 1497
    label "zgaszenie"
  ]
  node [
    id 1498
    label "zamkni&#281;cie"
  ]
  node [
    id 1499
    label "przeszkadza&#263;"
  ]
  node [
    id 1500
    label "wstrzymywa&#263;"
  ]
  node [
    id 1501
    label "throng"
  ]
  node [
    id 1502
    label "unieruchamia&#263;"
  ]
  node [
    id 1503
    label "kiblowa&#263;"
  ]
  node [
    id 1504
    label "sk&#322;ada&#263;"
  ]
  node [
    id 1505
    label "walczy&#263;"
  ]
  node [
    id 1506
    label "zablokowywa&#263;"
  ]
  node [
    id 1507
    label "zatrzymywa&#263;"
  ]
  node [
    id 1508
    label "interlock"
  ]
  node [
    id 1509
    label "parry"
  ]
  node [
    id 1510
    label "dostarcza&#263;"
  ]
  node [
    id 1511
    label "schorzenie"
  ]
  node [
    id 1512
    label "komornik"
  ]
  node [
    id 1513
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 1514
    label "return"
  ]
  node [
    id 1515
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 1516
    label "bra&#263;"
  ]
  node [
    id 1517
    label "rozciekawia&#263;"
  ]
  node [
    id 1518
    label "klasyfikacja"
  ]
  node [
    id 1519
    label "zadawa&#263;"
  ]
  node [
    id 1520
    label "fill"
  ]
  node [
    id 1521
    label "zabiera&#263;"
  ]
  node [
    id 1522
    label "topographic_point"
  ]
  node [
    id 1523
    label "pali&#263;_si&#281;"
  ]
  node [
    id 1524
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 1525
    label "aim"
  ]
  node [
    id 1526
    label "anektowa&#263;"
  ]
  node [
    id 1527
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 1528
    label "prosecute"
  ]
  node [
    id 1529
    label "sake"
  ]
  node [
    id 1530
    label "suspend"
  ]
  node [
    id 1531
    label "zgarnia&#263;"
  ]
  node [
    id 1532
    label "przerywa&#263;"
  ]
  node [
    id 1533
    label "przechowywa&#263;"
  ]
  node [
    id 1534
    label "&#322;apa&#263;"
  ]
  node [
    id 1535
    label "przetrzymywa&#263;"
  ]
  node [
    id 1536
    label "allude"
  ]
  node [
    id 1537
    label "zaczepia&#263;"
  ]
  node [
    id 1538
    label "hold"
  ]
  node [
    id 1539
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 1540
    label "zmaga&#263;_si&#281;"
  ]
  node [
    id 1541
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 1542
    label "opowiada&#263;_si&#281;"
  ]
  node [
    id 1543
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 1544
    label "fight"
  ]
  node [
    id 1545
    label "wrestle"
  ]
  node [
    id 1546
    label "zawody"
  ]
  node [
    id 1547
    label "contend"
  ]
  node [
    id 1548
    label "argue"
  ]
  node [
    id 1549
    label "zbiera&#263;"
  ]
  node [
    id 1550
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 1551
    label "przywraca&#263;"
  ]
  node [
    id 1552
    label "dawa&#263;"
  ]
  node [
    id 1553
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 1554
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 1555
    label "convey"
  ]
  node [
    id 1556
    label "publicize"
  ]
  node [
    id 1557
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 1558
    label "uk&#322;ada&#263;"
  ]
  node [
    id 1559
    label "opracowywa&#263;"
  ]
  node [
    id 1560
    label "set"
  ]
  node [
    id 1561
    label "oddawa&#263;"
  ]
  node [
    id 1562
    label "zmienia&#263;"
  ]
  node [
    id 1563
    label "dzieli&#263;"
  ]
  node [
    id 1564
    label "scala&#263;"
  ]
  node [
    id 1565
    label "handicap"
  ]
  node [
    id 1566
    label "przestawa&#263;"
  ]
  node [
    id 1567
    label "transgress"
  ]
  node [
    id 1568
    label "wadzi&#263;"
  ]
  node [
    id 1569
    label "utrudnia&#263;"
  ]
  node [
    id 1570
    label "kwitn&#261;&#263;"
  ]
  node [
    id 1571
    label "siedzie&#263;"
  ]
  node [
    id 1572
    label "repetowa&#263;"
  ]
  node [
    id 1573
    label "parametr"
  ]
  node [
    id 1574
    label "wa&#380;no&#347;&#263;"
  ]
  node [
    id 1575
    label "wuchta"
  ]
  node [
    id 1576
    label "wytrzyma&#322;o&#347;&#263;"
  ]
  node [
    id 1577
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1578
    label "nasycenie"
  ]
  node [
    id 1579
    label "izotonia"
  ]
  node [
    id 1580
    label "potencja"
  ]
  node [
    id 1581
    label "immunity"
  ]
  node [
    id 1582
    label "nefelometria"
  ]
  node [
    id 1583
    label "posiada&#263;"
  ]
  node [
    id 1584
    label "potencja&#322;"
  ]
  node [
    id 1585
    label "zapomina&#263;"
  ]
  node [
    id 1586
    label "zapomnienie"
  ]
  node [
    id 1587
    label "zapominanie"
  ]
  node [
    id 1588
    label "ability"
  ]
  node [
    id 1589
    label "obliczeniowo"
  ]
  node [
    id 1590
    label "zapomnie&#263;"
  ]
  node [
    id 1591
    label "wymiar"
  ]
  node [
    id 1592
    label "zmienna"
  ]
  node [
    id 1593
    label "charakterystyka"
  ]
  node [
    id 1594
    label "wielko&#347;&#263;"
  ]
  node [
    id 1595
    label "u&#380;yteczno&#347;&#263;_ca&#322;kowita"
  ]
  node [
    id 1596
    label "zaimpregnowanie"
  ]
  node [
    id 1597
    label "zadowolenie"
  ]
  node [
    id 1598
    label "satysfakcja"
  ]
  node [
    id 1599
    label "saturation"
  ]
  node [
    id 1600
    label "wprowadzenie"
  ]
  node [
    id 1601
    label "syty"
  ]
  node [
    id 1602
    label "fertilization"
  ]
  node [
    id 1603
    label "przenikni&#281;cie"
  ]
  node [
    id 1604
    label "napojenie_si&#281;"
  ]
  node [
    id 1605
    label "satisfaction"
  ]
  node [
    id 1606
    label "impregnation"
  ]
  node [
    id 1607
    label "zaspokojenie"
  ]
  node [
    id 1608
    label "porz&#261;dno&#347;&#263;"
  ]
  node [
    id 1609
    label "kondycja"
  ]
  node [
    id 1610
    label "rewalidacja"
  ]
  node [
    id 1611
    label "odpowiednio&#347;&#263;"
  ]
  node [
    id 1612
    label "graveness"
  ]
  node [
    id 1613
    label "trwa&#322;o&#347;&#263;"
  ]
  node [
    id 1614
    label "pos&#322;uchanie"
  ]
  node [
    id 1615
    label "skumanie"
  ]
  node [
    id 1616
    label "orientacja"
  ]
  node [
    id 1617
    label "zorientowanie"
  ]
  node [
    id 1618
    label "clasp"
  ]
  node [
    id 1619
    label "forma"
  ]
  node [
    id 1620
    label "przem&#243;wienie"
  ]
  node [
    id 1621
    label "potency"
  ]
  node [
    id 1622
    label "tomizm"
  ]
  node [
    id 1623
    label "wydolno&#347;&#263;"
  ]
  node [
    id 1624
    label "arystotelizm"
  ]
  node [
    id 1625
    label "gotowo&#347;&#263;"
  ]
  node [
    id 1626
    label "r&#243;wno&#347;&#263;"
  ]
  node [
    id 1627
    label "elektrolit"
  ]
  node [
    id 1628
    label "analiza_chemiczna"
  ]
  node [
    id 1629
    label "zawiesina"
  ]
  node [
    id 1630
    label "roztw&#243;r_koloidowy"
  ]
  node [
    id 1631
    label "energia"
  ]
  node [
    id 1632
    label "rumieniec"
  ]
  node [
    id 1633
    label "nowotw&#243;r_niez&#322;o&#347;liwy"
  ]
  node [
    id 1634
    label "sk&#243;ra"
  ]
  node [
    id 1635
    label "hell"
  ]
  node [
    id 1636
    label "rozpalenie"
  ]
  node [
    id 1637
    label "co&#347;"
  ]
  node [
    id 1638
    label "iskra"
  ]
  node [
    id 1639
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 1640
    label "light"
  ]
  node [
    id 1641
    label "rozpalanie"
  ]
  node [
    id 1642
    label "deszcz"
  ]
  node [
    id 1643
    label "akcesorium"
  ]
  node [
    id 1644
    label "zapalenie"
  ]
  node [
    id 1645
    label "zarzewie"
  ]
  node [
    id 1646
    label "ciep&#322;o"
  ]
  node [
    id 1647
    label "znami&#281;"
  ]
  node [
    id 1648
    label "war"
  ]
  node [
    id 1649
    label "przyp&#322;yw"
  ]
  node [
    id 1650
    label "p&#322;omie&#324;"
  ]
  node [
    id 1651
    label "palenie_si&#281;"
  ]
  node [
    id 1652
    label "&#380;ywio&#322;"
  ]
  node [
    id 1653
    label "incandescence"
  ]
  node [
    id 1654
    label "palenie"
  ]
  node [
    id 1655
    label "fire"
  ]
  node [
    id 1656
    label "ardor"
  ]
  node [
    id 1657
    label "thing"
  ]
  node [
    id 1658
    label "cosik"
  ]
  node [
    id 1659
    label "przy&#263;miewa&#263;"
  ]
  node [
    id 1660
    label "odst&#281;p"
  ]
  node [
    id 1661
    label "interpretacja"
  ]
  node [
    id 1662
    label "fotokataliza"
  ]
  node [
    id 1663
    label "promieniowanie_elektromagnetyczne"
  ]
  node [
    id 1664
    label "rzuca&#263;"
  ]
  node [
    id 1665
    label "obsadnik"
  ]
  node [
    id 1666
    label "promieniowanie_optyczne"
  ]
  node [
    id 1667
    label "lampa"
  ]
  node [
    id 1668
    label "ja&#347;nia"
  ]
  node [
    id 1669
    label "przeb&#322;yskiwa&#263;"
  ]
  node [
    id 1670
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 1671
    label "rzuci&#263;"
  ]
  node [
    id 1672
    label "o&#347;wietlenie"
  ]
  node [
    id 1673
    label "punkt_widzenia"
  ]
  node [
    id 1674
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 1675
    label "przy&#263;mienie"
  ]
  node [
    id 1676
    label "instalacja"
  ]
  node [
    id 1677
    label "&#347;wiecenie"
  ]
  node [
    id 1678
    label "przeb&#322;yskiwanie"
  ]
  node [
    id 1679
    label "przy&#263;mi&#263;"
  ]
  node [
    id 1680
    label "b&#322;ysk"
  ]
  node [
    id 1681
    label "&#347;wiat&#322;y"
  ]
  node [
    id 1682
    label "promie&#324;"
  ]
  node [
    id 1683
    label "m&#261;drze"
  ]
  node [
    id 1684
    label "rozja&#347;nia&#263;"
  ]
  node [
    id 1685
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 1686
    label "lighting"
  ]
  node [
    id 1687
    label "lighter"
  ]
  node [
    id 1688
    label "rzucenie"
  ]
  node [
    id 1689
    label "plama"
  ]
  node [
    id 1690
    label "&#347;rednica"
  ]
  node [
    id 1691
    label "przy&#263;miewanie"
  ]
  node [
    id 1692
    label "rzucanie"
  ]
  node [
    id 1693
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1694
    label "egzergia"
  ]
  node [
    id 1695
    label "emitowa&#263;"
  ]
  node [
    id 1696
    label "kwant_energii"
  ]
  node [
    id 1697
    label "szwung"
  ]
  node [
    id 1698
    label "emitowanie"
  ]
  node [
    id 1699
    label "energy"
  ]
  node [
    id 1700
    label "emocja"
  ]
  node [
    id 1701
    label "geotermia"
  ]
  node [
    id 1702
    label "przyjemnie"
  ]
  node [
    id 1703
    label "emisyjno&#347;&#263;"
  ]
  node [
    id 1704
    label "mi&#322;o"
  ]
  node [
    id 1705
    label "ciep&#322;y"
  ]
  node [
    id 1706
    label "heat"
  ]
  node [
    id 1707
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 1708
    label "walka"
  ]
  node [
    id 1709
    label "liga"
  ]
  node [
    id 1710
    label "przemoc"
  ]
  node [
    id 1711
    label "krytyka"
  ]
  node [
    id 1712
    label "bat"
  ]
  node [
    id 1713
    label "kaszel"
  ]
  node [
    id 1714
    label "fit"
  ]
  node [
    id 1715
    label "spasm"
  ]
  node [
    id 1716
    label "wypowied&#378;"
  ]
  node [
    id 1717
    label "&#380;&#261;danie"
  ]
  node [
    id 1718
    label "ofensywa"
  ]
  node [
    id 1719
    label "stroke"
  ]
  node [
    id 1720
    label "knock"
  ]
  node [
    id 1721
    label "atrakcyjno&#347;&#263;"
  ]
  node [
    id 1722
    label "wstyd"
  ]
  node [
    id 1723
    label "przebarwienie"
  ]
  node [
    id 1724
    label "hot_flash"
  ]
  node [
    id 1725
    label "reakcja"
  ]
  node [
    id 1726
    label "stamp"
  ]
  node [
    id 1727
    label "s&#322;upek"
  ]
  node [
    id 1728
    label "okrytonasienne"
  ]
  node [
    id 1729
    label "py&#322;ek"
  ]
  node [
    id 1730
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1731
    label "materia"
  ]
  node [
    id 1732
    label "zajawka"
  ]
  node [
    id 1733
    label "class"
  ]
  node [
    id 1734
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 1735
    label "feblik"
  ]
  node [
    id 1736
    label "wedrze&#263;_si&#281;"
  ]
  node [
    id 1737
    label "wdarcie_si&#281;"
  ]
  node [
    id 1738
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1739
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1740
    label "wdziera&#263;_si&#281;"
  ]
  node [
    id 1741
    label "tendency"
  ]
  node [
    id 1742
    label "huczek"
  ]
  node [
    id 1743
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1744
    label "subject"
  ]
  node [
    id 1745
    label "kamena"
  ]
  node [
    id 1746
    label "&#347;wiadectwo"
  ]
  node [
    id 1747
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1748
    label "ciek_wodny"
  ]
  node [
    id 1749
    label "matuszka"
  ]
  node [
    id 1750
    label "pocz&#261;tek"
  ]
  node [
    id 1751
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 1752
    label "bra&#263;_si&#281;"
  ]
  node [
    id 1753
    label "poci&#261;ganie"
  ]
  node [
    id 1754
    label "p&#322;yw"
  ]
  node [
    id 1755
    label "wzrost"
  ]
  node [
    id 1756
    label "akcesoria"
  ]
  node [
    id 1757
    label "accessory"
  ]
  node [
    id 1758
    label "przyrz&#261;d"
  ]
  node [
    id 1759
    label "liczba_kwantowa"
  ]
  node [
    id 1760
    label "poker"
  ]
  node [
    id 1761
    label "ubarwienie"
  ]
  node [
    id 1762
    label "blakn&#261;&#263;"
  ]
  node [
    id 1763
    label "prze&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 1764
    label "zblakni&#281;cie"
  ]
  node [
    id 1765
    label "prze&#322;amywa&#263;"
  ]
  node [
    id 1766
    label "prze&#322;amywanie_si&#281;"
  ]
  node [
    id 1767
    label "prze&#322;ama&#263;_si&#281;"
  ]
  node [
    id 1768
    label "prze&#322;amanie"
  ]
  node [
    id 1769
    label "prze&#322;amywanie"
  ]
  node [
    id 1770
    label "prze&#322;ama&#263;"
  ]
  node [
    id 1771
    label "zblakn&#261;&#263;"
  ]
  node [
    id 1772
    label "symbol"
  ]
  node [
    id 1773
    label "blakni&#281;cie"
  ]
  node [
    id 1774
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 1775
    label "rain"
  ]
  node [
    id 1776
    label "opad"
  ]
  node [
    id 1777
    label "burza"
  ]
  node [
    id 1778
    label "zajaranie"
  ]
  node [
    id 1779
    label "roz&#380;arzenie"
  ]
  node [
    id 1780
    label "fajka"
  ]
  node [
    id 1781
    label "pozapalanie"
  ]
  node [
    id 1782
    label "zaburzenie"
  ]
  node [
    id 1783
    label "papieros"
  ]
  node [
    id 1784
    label "rozja&#347;nienie"
  ]
  node [
    id 1785
    label "ignition"
  ]
  node [
    id 1786
    label "rozdra&#380;nianie"
  ]
  node [
    id 1787
    label "rozdra&#380;ni&#263;"
  ]
  node [
    id 1788
    label "rozdra&#380;nia&#263;"
  ]
  node [
    id 1789
    label "odpalenie"
  ]
  node [
    id 1790
    label "rozdra&#380;nienie"
  ]
  node [
    id 1791
    label "szczupak"
  ]
  node [
    id 1792
    label "coating"
  ]
  node [
    id 1793
    label "krupon"
  ]
  node [
    id 1794
    label "harleyowiec"
  ]
  node [
    id 1795
    label "tkanka_podsk&#243;rna"
  ]
  node [
    id 1796
    label "kurtka"
  ]
  node [
    id 1797
    label "p&#322;aszcz"
  ]
  node [
    id 1798
    label "&#322;upa"
  ]
  node [
    id 1799
    label "wyprze&#263;"
  ]
  node [
    id 1800
    label "okrywa"
  ]
  node [
    id 1801
    label "gruczo&#322;_kuprowy"
  ]
  node [
    id 1802
    label "gruczo&#322;_potowy"
  ]
  node [
    id 1803
    label "lico"
  ]
  node [
    id 1804
    label "wi&#243;rkownik"
  ]
  node [
    id 1805
    label "mizdra"
  ]
  node [
    id 1806
    label "dupa"
  ]
  node [
    id 1807
    label "rockers"
  ]
  node [
    id 1808
    label "&#322;uskowato&#347;&#263;"
  ]
  node [
    id 1809
    label "surowiec"
  ]
  node [
    id 1810
    label "czerniak_z&#322;o&#347;liwy"
  ]
  node [
    id 1811
    label "gruczo&#322;_&#322;ojowy"
  ]
  node [
    id 1812
    label "organ"
  ]
  node [
    id 1813
    label "pow&#322;oka"
  ]
  node [
    id 1814
    label "zdrowie"
  ]
  node [
    id 1815
    label "wyprawa"
  ]
  node [
    id 1816
    label "sk&#243;ra_w&#322;a&#347;ciwa"
  ]
  node [
    id 1817
    label "hardrockowiec"
  ]
  node [
    id 1818
    label "nask&#243;rek"
  ]
  node [
    id 1819
    label "gestapowiec"
  ]
  node [
    id 1820
    label "funkcja"
  ]
  node [
    id 1821
    label "shell"
  ]
  node [
    id 1822
    label "wrz&#261;tek"
  ]
  node [
    id 1823
    label "gor&#261;co"
  ]
  node [
    id 1824
    label "wyraz"
  ]
  node [
    id 1825
    label "ostentation"
  ]
  node [
    id 1826
    label "freshness"
  ]
  node [
    id 1827
    label "flicker"
  ]
  node [
    id 1828
    label "&#380;agiew"
  ]
  node [
    id 1829
    label "odrobina"
  ]
  node [
    id 1830
    label "odblask"
  ]
  node [
    id 1831
    label "glint"
  ]
  node [
    id 1832
    label "blask"
  ]
  node [
    id 1833
    label "discharge"
  ]
  node [
    id 1834
    label "dopalenie"
  ]
  node [
    id 1835
    label "burning"
  ]
  node [
    id 1836
    label "na&#322;&#243;g"
  ]
  node [
    id 1837
    label "emergency"
  ]
  node [
    id 1838
    label "burn"
  ]
  node [
    id 1839
    label "dokuczanie"
  ]
  node [
    id 1840
    label "cygaro"
  ]
  node [
    id 1841
    label "napalenie"
  ]
  node [
    id 1842
    label "podpalanie"
  ]
  node [
    id 1843
    label "pykni&#281;cie"
  ]
  node [
    id 1844
    label "zu&#380;ywanie"
  ]
  node [
    id 1845
    label "wypalanie"
  ]
  node [
    id 1846
    label "podtrzymywanie"
  ]
  node [
    id 1847
    label "strzelanie"
  ]
  node [
    id 1848
    label "dra&#380;nienie"
  ]
  node [
    id 1849
    label "kadzenie"
  ]
  node [
    id 1850
    label "wypalenie"
  ]
  node [
    id 1851
    label "przygotowywanie"
  ]
  node [
    id 1852
    label "dowcip"
  ]
  node [
    id 1853
    label "popalenie"
  ]
  node [
    id 1854
    label "niszczenie"
  ]
  node [
    id 1855
    label "grzanie"
  ]
  node [
    id 1856
    label "paliwo"
  ]
  node [
    id 1857
    label "bolenie"
  ]
  node [
    id 1858
    label "incineration"
  ]
  node [
    id 1859
    label "psucie"
  ]
  node [
    id 1860
    label "jaranie"
  ]
  node [
    id 1861
    label "o&#347;wietlanie"
  ]
  node [
    id 1862
    label "distraction"
  ]
  node [
    id 1863
    label "rozja&#347;nianie_si&#281;"
  ]
  node [
    id 1864
    label "roz&#380;arzanie_si&#281;"
  ]
  node [
    id 1865
    label "pobudzanie"
  ]
  node [
    id 1866
    label "rozp&#322;omienianie_si&#281;"
  ]
  node [
    id 1867
    label "wzbudzanie"
  ]
  node [
    id 1868
    label "rozja&#347;nienie_si&#281;"
  ]
  node [
    id 1869
    label "rozp&#322;omienienie_si&#281;"
  ]
  node [
    id 1870
    label "wzbudzenie"
  ]
  node [
    id 1871
    label "podpalenie"
  ]
  node [
    id 1872
    label "pobudzenie"
  ]
  node [
    id 1873
    label "roz&#380;arzenie_si&#281;"
  ]
  node [
    id 1874
    label "zagrzanie"
  ]
  node [
    id 1875
    label "waste"
  ]
  node [
    id 1876
    label "inflammation"
  ]
  node [
    id 1877
    label "brand"
  ]
  node [
    id 1878
    label "podpa&#322;ka"
  ]
  node [
    id 1879
    label "Magic"
  ]
  node [
    id 1880
    label "Gathering"
  ]
  node [
    id 1881
    label "Anshul"
  ]
  node [
    id 1882
    label "samara"
  ]
  node [
    id 1883
    label "World"
  ]
  node [
    id 1884
    label "of"
  ]
  node [
    id 1885
    label "Warcraft"
  ]
  node [
    id 1886
    label "ko&#322;o"
  ]
  node [
    id 1887
    label "fortuna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 1879
  ]
  edge [
    source 7
    target 1880
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 273
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 609
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 322
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 611
  ]
  edge [
    source 14
    target 612
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 615
  ]
  edge [
    source 14
    target 616
  ]
  edge [
    source 14
    target 617
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 217
  ]
  edge [
    source 14
    target 619
  ]
  edge [
    source 14
    target 620
  ]
  edge [
    source 14
    target 621
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 623
  ]
  edge [
    source 14
    target 624
  ]
  edge [
    source 14
    target 625
  ]
  edge [
    source 14
    target 626
  ]
  edge [
    source 14
    target 627
  ]
  edge [
    source 14
    target 628
  ]
  edge [
    source 14
    target 629
  ]
  edge [
    source 14
    target 630
  ]
  edge [
    source 14
    target 631
  ]
  edge [
    source 14
    target 632
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 193
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 208
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 633
  ]
  edge [
    source 14
    target 634
  ]
  edge [
    source 14
    target 635
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 659
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 15
    target 663
  ]
  edge [
    source 15
    target 664
  ]
  edge [
    source 15
    target 665
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 15
    target 669
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 670
  ]
  edge [
    source 15
    target 671
  ]
  edge [
    source 15
    target 672
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 674
  ]
  edge [
    source 15
    target 191
  ]
  edge [
    source 15
    target 675
  ]
  edge [
    source 15
    target 676
  ]
  edge [
    source 15
    target 677
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 679
  ]
  edge [
    source 15
    target 680
  ]
  edge [
    source 15
    target 681
  ]
  edge [
    source 15
    target 682
  ]
  edge [
    source 15
    target 683
  ]
  edge [
    source 15
    target 684
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 686
  ]
  edge [
    source 15
    target 687
  ]
  edge [
    source 15
    target 688
  ]
  edge [
    source 15
    target 689
  ]
  edge [
    source 15
    target 690
  ]
  edge [
    source 15
    target 691
  ]
  edge [
    source 15
    target 692
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 694
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 696
  ]
  edge [
    source 15
    target 697
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 501
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 448
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 412
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 351
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 275
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 602
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 367
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 34
  ]
  edge [
    source 18
    target 35
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 37
  ]
  edge [
    source 18
    target 38
  ]
  edge [
    source 18
    target 39
  ]
  edge [
    source 18
    target 40
  ]
  edge [
    source 18
    target 41
  ]
  edge [
    source 18
    target 42
  ]
  edge [
    source 18
    target 43
  ]
  edge [
    source 18
    target 44
  ]
  edge [
    source 18
    target 45
  ]
  edge [
    source 18
    target 46
  ]
  edge [
    source 18
    target 47
  ]
  edge [
    source 18
    target 48
  ]
  edge [
    source 18
    target 49
  ]
  edge [
    source 18
    target 50
  ]
  edge [
    source 18
    target 51
  ]
  edge [
    source 18
    target 52
  ]
  edge [
    source 18
    target 53
  ]
  edge [
    source 18
    target 54
  ]
  edge [
    source 18
    target 55
  ]
  edge [
    source 18
    target 56
  ]
  edge [
    source 18
    target 57
  ]
  edge [
    source 18
    target 58
  ]
  edge [
    source 18
    target 59
  ]
  edge [
    source 18
    target 60
  ]
  edge [
    source 18
    target 61
  ]
  edge [
    source 18
    target 62
  ]
  edge [
    source 18
    target 63
  ]
  edge [
    source 18
    target 64
  ]
  edge [
    source 18
    target 65
  ]
  edge [
    source 18
    target 66
  ]
  edge [
    source 18
    target 67
  ]
  edge [
    source 18
    target 68
  ]
  edge [
    source 18
    target 69
  ]
  edge [
    source 18
    target 70
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 331
  ]
  edge [
    source 18
    target 397
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 547
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 543
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 540
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 440
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 606
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 588
  ]
  edge [
    source 21
    target 996
  ]
  edge [
    source 21
    target 997
  ]
  edge [
    source 21
    target 998
  ]
  edge [
    source 21
    target 999
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 577
  ]
  edge [
    source 22
    target 1000
  ]
  edge [
    source 22
    target 1001
  ]
  edge [
    source 22
    target 1002
  ]
  edge [
    source 22
    target 1003
  ]
  edge [
    source 22
    target 1004
  ]
  edge [
    source 22
    target 1005
  ]
  edge [
    source 22
    target 1006
  ]
  edge [
    source 22
    target 1007
  ]
  edge [
    source 22
    target 1008
  ]
  edge [
    source 22
    target 1009
  ]
  edge [
    source 22
    target 1010
  ]
  edge [
    source 22
    target 1011
  ]
  edge [
    source 22
    target 1012
  ]
  edge [
    source 22
    target 1013
  ]
  edge [
    source 22
    target 1014
  ]
  edge [
    source 22
    target 1015
  ]
  edge [
    source 22
    target 1016
  ]
  edge [
    source 22
    target 1017
  ]
  edge [
    source 22
    target 1018
  ]
  edge [
    source 22
    target 1019
  ]
  edge [
    source 22
    target 1020
  ]
  edge [
    source 22
    target 1021
  ]
  edge [
    source 22
    target 571
  ]
  edge [
    source 22
    target 1022
  ]
  edge [
    source 22
    target 1023
  ]
  edge [
    source 22
    target 45
  ]
  edge [
    source 22
    target 1024
  ]
  edge [
    source 22
    target 1025
  ]
  edge [
    source 22
    target 1026
  ]
  edge [
    source 22
    target 1027
  ]
  edge [
    source 22
    target 1028
  ]
  edge [
    source 22
    target 1029
  ]
  edge [
    source 22
    target 369
  ]
  edge [
    source 22
    target 994
  ]
  edge [
    source 22
    target 606
  ]
  edge [
    source 22
    target 1030
  ]
  edge [
    source 22
    target 1031
  ]
  edge [
    source 22
    target 1032
  ]
  edge [
    source 22
    target 1033
  ]
  edge [
    source 22
    target 1034
  ]
  edge [
    source 22
    target 1035
  ]
  edge [
    source 22
    target 1036
  ]
  edge [
    source 22
    target 1037
  ]
  edge [
    source 22
    target 1038
  ]
  edge [
    source 22
    target 997
  ]
  edge [
    source 22
    target 1039
  ]
  edge [
    source 22
    target 1040
  ]
  edge [
    source 22
    target 1041
  ]
  edge [
    source 22
    target 1042
  ]
  edge [
    source 22
    target 1043
  ]
  edge [
    source 22
    target 261
  ]
  edge [
    source 22
    target 693
  ]
  edge [
    source 22
    target 1044
  ]
  edge [
    source 22
    target 1045
  ]
  edge [
    source 22
    target 1046
  ]
  edge [
    source 22
    target 1047
  ]
  edge [
    source 22
    target 1048
  ]
  edge [
    source 22
    target 1049
  ]
  edge [
    source 22
    target 1050
  ]
  edge [
    source 22
    target 1051
  ]
  edge [
    source 22
    target 1052
  ]
  edge [
    source 22
    target 1053
  ]
  edge [
    source 22
    target 1054
  ]
  edge [
    source 22
    target 1055
  ]
  edge [
    source 22
    target 1056
  ]
  edge [
    source 22
    target 1057
  ]
  edge [
    source 22
    target 1058
  ]
  edge [
    source 22
    target 309
  ]
  edge [
    source 22
    target 1059
  ]
  edge [
    source 22
    target 1060
  ]
  edge [
    source 22
    target 1061
  ]
  edge [
    source 22
    target 1062
  ]
  edge [
    source 22
    target 1063
  ]
  edge [
    source 22
    target 1064
  ]
  edge [
    source 22
    target 1065
  ]
  edge [
    source 22
    target 1066
  ]
  edge [
    source 22
    target 1067
  ]
  edge [
    source 22
    target 1068
  ]
  edge [
    source 22
    target 1069
  ]
  edge [
    source 22
    target 735
  ]
  edge [
    source 22
    target 1070
  ]
  edge [
    source 22
    target 1071
  ]
  edge [
    source 22
    target 1072
  ]
  edge [
    source 22
    target 679
  ]
  edge [
    source 22
    target 1073
  ]
  edge [
    source 22
    target 1074
  ]
  edge [
    source 22
    target 1075
  ]
  edge [
    source 22
    target 530
  ]
  edge [
    source 22
    target 1076
  ]
  edge [
    source 22
    target 1077
  ]
  edge [
    source 22
    target 1078
  ]
  edge [
    source 22
    target 1079
  ]
  edge [
    source 22
    target 1080
  ]
  edge [
    source 22
    target 1081
  ]
  edge [
    source 22
    target 1082
  ]
  edge [
    source 22
    target 1083
  ]
  edge [
    source 22
    target 1084
  ]
  edge [
    source 22
    target 1085
  ]
  edge [
    source 22
    target 1086
  ]
  edge [
    source 22
    target 1087
  ]
  edge [
    source 22
    target 1088
  ]
  edge [
    source 22
    target 1089
  ]
  edge [
    source 22
    target 1090
  ]
  edge [
    source 22
    target 1091
  ]
  edge [
    source 22
    target 1092
  ]
  edge [
    source 22
    target 37
  ]
  edge [
    source 22
    target 1093
  ]
  edge [
    source 22
    target 1094
  ]
  edge [
    source 22
    target 640
  ]
  edge [
    source 22
    target 1095
  ]
  edge [
    source 22
    target 704
  ]
  edge [
    source 22
    target 1096
  ]
  edge [
    source 22
    target 1097
  ]
  edge [
    source 22
    target 1098
  ]
  edge [
    source 22
    target 661
  ]
  edge [
    source 22
    target 1099
  ]
  edge [
    source 22
    target 1100
  ]
  edge [
    source 22
    target 1101
  ]
  edge [
    source 22
    target 1102
  ]
  edge [
    source 22
    target 1103
  ]
  edge [
    source 22
    target 1104
  ]
  edge [
    source 22
    target 1105
  ]
  edge [
    source 22
    target 962
  ]
  edge [
    source 22
    target 1106
  ]
  edge [
    source 22
    target 1107
  ]
  edge [
    source 22
    target 1108
  ]
  edge [
    source 22
    target 1109
  ]
  edge [
    source 22
    target 274
  ]
  edge [
    source 22
    target 1110
  ]
  edge [
    source 22
    target 1111
  ]
  edge [
    source 22
    target 1112
  ]
  edge [
    source 22
    target 1113
  ]
  edge [
    source 22
    target 1114
  ]
  edge [
    source 22
    target 1115
  ]
  edge [
    source 22
    target 1116
  ]
  edge [
    source 22
    target 1117
  ]
  edge [
    source 22
    target 1118
  ]
  edge [
    source 22
    target 1119
  ]
  edge [
    source 22
    target 1120
  ]
  edge [
    source 22
    target 1121
  ]
  edge [
    source 22
    target 1122
  ]
  edge [
    source 22
    target 1123
  ]
  edge [
    source 22
    target 1124
  ]
  edge [
    source 22
    target 1125
  ]
  edge [
    source 22
    target 1126
  ]
  edge [
    source 22
    target 1127
  ]
  edge [
    source 22
    target 1128
  ]
  edge [
    source 22
    target 1129
  ]
  edge [
    source 22
    target 1130
  ]
  edge [
    source 22
    target 1131
  ]
  edge [
    source 22
    target 529
  ]
  edge [
    source 22
    target 645
  ]
  edge [
    source 22
    target 948
  ]
  edge [
    source 22
    target 1132
  ]
  edge [
    source 22
    target 1133
  ]
  edge [
    source 22
    target 1134
  ]
  edge [
    source 22
    target 1135
  ]
  edge [
    source 22
    target 116
  ]
  edge [
    source 22
    target 59
  ]
  edge [
    source 22
    target 1136
  ]
  edge [
    source 22
    target 1137
  ]
  edge [
    source 22
    target 1138
  ]
  edge [
    source 22
    target 941
  ]
  edge [
    source 22
    target 1139
  ]
  edge [
    source 22
    target 1140
  ]
  edge [
    source 22
    target 1141
  ]
  edge [
    source 22
    target 1142
  ]
  edge [
    source 22
    target 1143
  ]
  edge [
    source 22
    target 1144
  ]
  edge [
    source 22
    target 1145
  ]
  edge [
    source 22
    target 1146
  ]
  edge [
    source 22
    target 989
  ]
  edge [
    source 22
    target 1147
  ]
  edge [
    source 22
    target 1148
  ]
  edge [
    source 22
    target 1149
  ]
  edge [
    source 22
    target 1150
  ]
  edge [
    source 22
    target 1151
  ]
  edge [
    source 22
    target 1152
  ]
  edge [
    source 22
    target 1153
  ]
  edge [
    source 22
    target 1154
  ]
  edge [
    source 22
    target 1155
  ]
  edge [
    source 22
    target 1156
  ]
  edge [
    source 22
    target 1157
  ]
  edge [
    source 22
    target 1158
  ]
  edge [
    source 22
    target 1159
  ]
  edge [
    source 22
    target 1160
  ]
  edge [
    source 22
    target 1161
  ]
  edge [
    source 22
    target 1162
  ]
  edge [
    source 22
    target 1163
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 90
  ]
  edge [
    source 23
    target 1164
  ]
  edge [
    source 23
    target 1165
  ]
  edge [
    source 23
    target 1166
  ]
  edge [
    source 23
    target 1167
  ]
  edge [
    source 23
    target 1168
  ]
  edge [
    source 23
    target 1169
  ]
  edge [
    source 23
    target 1170
  ]
  edge [
    source 23
    target 1171
  ]
  edge [
    source 23
    target 1172
  ]
  edge [
    source 23
    target 1173
  ]
  edge [
    source 23
    target 1174
  ]
  edge [
    source 23
    target 1175
  ]
  edge [
    source 23
    target 1176
  ]
  edge [
    source 23
    target 1177
  ]
  edge [
    source 23
    target 369
  ]
  edge [
    source 23
    target 1178
  ]
  edge [
    source 23
    target 1179
  ]
  edge [
    source 23
    target 1180
  ]
  edge [
    source 23
    target 1181
  ]
  edge [
    source 23
    target 1182
  ]
  edge [
    source 23
    target 1183
  ]
  edge [
    source 23
    target 1048
  ]
  edge [
    source 23
    target 1184
  ]
  edge [
    source 23
    target 397
  ]
  edge [
    source 23
    target 1185
  ]
  edge [
    source 23
    target 1186
  ]
  edge [
    source 23
    target 1187
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 729
  ]
  edge [
    source 23
    target 1188
  ]
  edge [
    source 23
    target 1189
  ]
  edge [
    source 23
    target 1190
  ]
  edge [
    source 23
    target 1191
  ]
  edge [
    source 23
    target 285
  ]
  edge [
    source 23
    target 1192
  ]
  edge [
    source 23
    target 1193
  ]
  edge [
    source 23
    target 1194
  ]
  edge [
    source 23
    target 287
  ]
  edge [
    source 23
    target 1195
  ]
  edge [
    source 23
    target 1020
  ]
  edge [
    source 23
    target 1196
  ]
  edge [
    source 23
    target 1197
  ]
  edge [
    source 23
    target 1198
  ]
  edge [
    source 23
    target 1199
  ]
  edge [
    source 23
    target 1200
  ]
  edge [
    source 23
    target 1201
  ]
  edge [
    source 23
    target 1202
  ]
  edge [
    source 23
    target 1203
  ]
  edge [
    source 23
    target 1204
  ]
  edge [
    source 23
    target 1205
  ]
  edge [
    source 23
    target 1206
  ]
  edge [
    source 23
    target 1207
  ]
  edge [
    source 23
    target 1208
  ]
  edge [
    source 23
    target 1209
  ]
  edge [
    source 23
    target 588
  ]
  edge [
    source 23
    target 216
  ]
  edge [
    source 23
    target 1210
  ]
  edge [
    source 23
    target 1211
  ]
  edge [
    source 23
    target 1212
  ]
  edge [
    source 23
    target 1213
  ]
  edge [
    source 23
    target 146
  ]
  edge [
    source 23
    target 1214
  ]
  edge [
    source 23
    target 1215
  ]
  edge [
    source 23
    target 1216
  ]
  edge [
    source 23
    target 1217
  ]
  edge [
    source 23
    target 1218
  ]
  edge [
    source 23
    target 1219
  ]
  edge [
    source 23
    target 1220
  ]
  edge [
    source 23
    target 1221
  ]
  edge [
    source 23
    target 1222
  ]
  edge [
    source 23
    target 1223
  ]
  edge [
    source 23
    target 1224
  ]
  edge [
    source 23
    target 1225
  ]
  edge [
    source 23
    target 1226
  ]
  edge [
    source 23
    target 291
  ]
  edge [
    source 23
    target 1227
  ]
  edge [
    source 23
    target 1228
  ]
  edge [
    source 23
    target 1229
  ]
  edge [
    source 23
    target 1230
  ]
  edge [
    source 23
    target 1231
  ]
  edge [
    source 23
    target 1232
  ]
  edge [
    source 23
    target 1233
  ]
  edge [
    source 23
    target 1234
  ]
  edge [
    source 23
    target 1235
  ]
  edge [
    source 23
    target 1236
  ]
  edge [
    source 23
    target 1237
  ]
  edge [
    source 23
    target 1238
  ]
  edge [
    source 23
    target 896
  ]
  edge [
    source 23
    target 1239
  ]
  edge [
    source 23
    target 1240
  ]
  edge [
    source 23
    target 762
  ]
  edge [
    source 23
    target 1241
  ]
  edge [
    source 23
    target 1242
  ]
  edge [
    source 23
    target 1243
  ]
  edge [
    source 23
    target 1244
  ]
  edge [
    source 23
    target 1245
  ]
  edge [
    source 23
    target 1246
  ]
  edge [
    source 23
    target 1006
  ]
  edge [
    source 23
    target 1247
  ]
  edge [
    source 23
    target 1248
  ]
  edge [
    source 23
    target 1249
  ]
  edge [
    source 23
    target 1250
  ]
  edge [
    source 23
    target 1251
  ]
  edge [
    source 23
    target 1252
  ]
  edge [
    source 23
    target 1253
  ]
  edge [
    source 23
    target 1254
  ]
  edge [
    source 23
    target 1255
  ]
  edge [
    source 23
    target 1256
  ]
  edge [
    source 23
    target 1257
  ]
  edge [
    source 23
    target 1258
  ]
  edge [
    source 23
    target 1259
  ]
  edge [
    source 23
    target 1260
  ]
  edge [
    source 23
    target 1261
  ]
  edge [
    source 23
    target 1262
  ]
  edge [
    source 23
    target 1263
  ]
  edge [
    source 23
    target 1264
  ]
  edge [
    source 23
    target 1265
  ]
  edge [
    source 23
    target 1266
  ]
  edge [
    source 23
    target 1267
  ]
  edge [
    source 23
    target 1268
  ]
  edge [
    source 23
    target 1269
  ]
  edge [
    source 23
    target 1270
  ]
  edge [
    source 23
    target 1271
  ]
  edge [
    source 23
    target 1272
  ]
  edge [
    source 23
    target 593
  ]
  edge [
    source 23
    target 1273
  ]
  edge [
    source 23
    target 1274
  ]
  edge [
    source 23
    target 1275
  ]
  edge [
    source 23
    target 1276
  ]
  edge [
    source 23
    target 1277
  ]
  edge [
    source 23
    target 463
  ]
  edge [
    source 23
    target 1278
  ]
  edge [
    source 23
    target 1279
  ]
  edge [
    source 23
    target 1280
  ]
  edge [
    source 23
    target 1281
  ]
  edge [
    source 23
    target 1282
  ]
  edge [
    source 23
    target 1283
  ]
  edge [
    source 23
    target 1284
  ]
  edge [
    source 23
    target 1285
  ]
  edge [
    source 23
    target 1286
  ]
  edge [
    source 23
    target 1287
  ]
  edge [
    source 23
    target 1288
  ]
  edge [
    source 23
    target 1289
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1290
  ]
  edge [
    source 24
    target 1291
  ]
  edge [
    source 24
    target 1292
  ]
  edge [
    source 24
    target 1293
  ]
  edge [
    source 24
    target 829
  ]
  edge [
    source 24
    target 1294
  ]
  edge [
    source 24
    target 1295
  ]
  edge [
    source 24
    target 1296
  ]
  edge [
    source 24
    target 1297
  ]
  edge [
    source 24
    target 1298
  ]
  edge [
    source 24
    target 1299
  ]
  edge [
    source 24
    target 1300
  ]
  edge [
    source 24
    target 1301
  ]
  edge [
    source 24
    target 1302
  ]
  edge [
    source 24
    target 1303
  ]
  edge [
    source 24
    target 1304
  ]
  edge [
    source 24
    target 1305
  ]
  edge [
    source 24
    target 1306
  ]
  edge [
    source 24
    target 1307
  ]
  edge [
    source 24
    target 1308
  ]
  edge [
    source 24
    target 1309
  ]
  edge [
    source 24
    target 1310
  ]
  edge [
    source 24
    target 1311
  ]
  edge [
    source 24
    target 1312
  ]
  edge [
    source 24
    target 1313
  ]
  edge [
    source 24
    target 1314
  ]
  edge [
    source 24
    target 1315
  ]
  edge [
    source 24
    target 1316
  ]
  edge [
    source 24
    target 1317
  ]
  edge [
    source 24
    target 1318
  ]
  edge [
    source 24
    target 1319
  ]
  edge [
    source 24
    target 1320
  ]
  edge [
    source 24
    target 1321
  ]
  edge [
    source 24
    target 1322
  ]
  edge [
    source 24
    target 1323
  ]
  edge [
    source 24
    target 1324
  ]
  edge [
    source 24
    target 1325
  ]
  edge [
    source 24
    target 1326
  ]
  edge [
    source 24
    target 1327
  ]
  edge [
    source 24
    target 1328
  ]
  edge [
    source 24
    target 1329
  ]
  edge [
    source 24
    target 1330
  ]
  edge [
    source 24
    target 1331
  ]
  edge [
    source 24
    target 1332
  ]
  edge [
    source 24
    target 1333
  ]
  edge [
    source 24
    target 1334
  ]
  edge [
    source 24
    target 1335
  ]
  edge [
    source 24
    target 1336
  ]
  edge [
    source 24
    target 1337
  ]
  edge [
    source 24
    target 1338
  ]
  edge [
    source 24
    target 1339
  ]
  edge [
    source 24
    target 1340
  ]
  edge [
    source 24
    target 1341
  ]
  edge [
    source 24
    target 1342
  ]
  edge [
    source 24
    target 1343
  ]
  edge [
    source 24
    target 1344
  ]
  edge [
    source 24
    target 1345
  ]
  edge [
    source 24
    target 1346
  ]
  edge [
    source 24
    target 1347
  ]
  edge [
    source 24
    target 1348
  ]
  edge [
    source 24
    target 1349
  ]
  edge [
    source 24
    target 1350
  ]
  edge [
    source 24
    target 1351
  ]
  edge [
    source 24
    target 1352
  ]
  edge [
    source 24
    target 1353
  ]
  edge [
    source 24
    target 1354
  ]
  edge [
    source 24
    target 1355
  ]
  edge [
    source 24
    target 1356
  ]
  edge [
    source 24
    target 1357
  ]
  edge [
    source 24
    target 1358
  ]
  edge [
    source 24
    target 1359
  ]
  edge [
    source 24
    target 1360
  ]
  edge [
    source 24
    target 1361
  ]
  edge [
    source 24
    target 1362
  ]
  edge [
    source 24
    target 1363
  ]
  edge [
    source 24
    target 1364
  ]
  edge [
    source 24
    target 1365
  ]
  edge [
    source 24
    target 1366
  ]
  edge [
    source 24
    target 409
  ]
  edge [
    source 24
    target 1367
  ]
  edge [
    source 24
    target 1368
  ]
  edge [
    source 24
    target 1369
  ]
  edge [
    source 24
    target 1370
  ]
  edge [
    source 24
    target 1371
  ]
  edge [
    source 24
    target 1372
  ]
  edge [
    source 24
    target 1373
  ]
  edge [
    source 24
    target 1374
  ]
  edge [
    source 24
    target 1375
  ]
  edge [
    source 24
    target 1376
  ]
  edge [
    source 24
    target 1377
  ]
  edge [
    source 24
    target 1378
  ]
  edge [
    source 24
    target 1379
  ]
  edge [
    source 24
    target 1380
  ]
  edge [
    source 24
    target 1381
  ]
  edge [
    source 24
    target 1382
  ]
  edge [
    source 24
    target 1383
  ]
  edge [
    source 24
    target 1384
  ]
  edge [
    source 24
    target 1385
  ]
  edge [
    source 24
    target 1386
  ]
  edge [
    source 24
    target 1387
  ]
  edge [
    source 24
    target 1388
  ]
  edge [
    source 24
    target 1389
  ]
  edge [
    source 24
    target 1390
  ]
  edge [
    source 24
    target 1391
  ]
  edge [
    source 24
    target 1392
  ]
  edge [
    source 24
    target 1393
  ]
  edge [
    source 24
    target 1394
  ]
  edge [
    source 24
    target 1395
  ]
  edge [
    source 24
    target 1396
  ]
  edge [
    source 24
    target 1397
  ]
  edge [
    source 24
    target 1398
  ]
  edge [
    source 24
    target 1399
  ]
  edge [
    source 24
    target 1400
  ]
  edge [
    source 24
    target 516
  ]
  edge [
    source 24
    target 1401
  ]
  edge [
    source 24
    target 1402
  ]
  edge [
    source 24
    target 1403
  ]
  edge [
    source 24
    target 1404
  ]
  edge [
    source 24
    target 1405
  ]
  edge [
    source 24
    target 1406
  ]
  edge [
    source 24
    target 1118
  ]
  edge [
    source 24
    target 1407
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1408
  ]
  edge [
    source 25
    target 1409
  ]
  edge [
    source 25
    target 1410
  ]
  edge [
    source 25
    target 1411
  ]
  edge [
    source 25
    target 1412
  ]
  edge [
    source 25
    target 1237
  ]
  edge [
    source 25
    target 1413
  ]
  edge [
    source 25
    target 1414
  ]
  edge [
    source 25
    target 1415
  ]
  edge [
    source 25
    target 1416
  ]
  edge [
    source 25
    target 1417
  ]
  edge [
    source 25
    target 1418
  ]
  edge [
    source 25
    target 1419
  ]
  edge [
    source 25
    target 1420
  ]
  edge [
    source 25
    target 274
  ]
  edge [
    source 25
    target 1421
  ]
  edge [
    source 25
    target 1422
  ]
  edge [
    source 25
    target 1423
  ]
  edge [
    source 25
    target 437
  ]
  edge [
    source 25
    target 438
  ]
  edge [
    source 25
    target 377
  ]
  edge [
    source 25
    target 1424
  ]
  edge [
    source 25
    target 1425
  ]
  edge [
    source 25
    target 1426
  ]
  edge [
    source 25
    target 90
  ]
  edge [
    source 25
    target 1427
  ]
  edge [
    source 25
    target 1428
  ]
  edge [
    source 25
    target 1429
  ]
  edge [
    source 25
    target 1430
  ]
  edge [
    source 25
    target 1431
  ]
  edge [
    source 25
    target 403
  ]
  edge [
    source 25
    target 1432
  ]
  edge [
    source 25
    target 1433
  ]
  edge [
    source 25
    target 1434
  ]
  edge [
    source 25
    target 1435
  ]
  edge [
    source 25
    target 1436
  ]
  edge [
    source 25
    target 1437
  ]
  edge [
    source 25
    target 1438
  ]
  edge [
    source 25
    target 1439
  ]
  edge [
    source 25
    target 1440
  ]
  edge [
    source 25
    target 1441
  ]
  edge [
    source 25
    target 1442
  ]
  edge [
    source 25
    target 1443
  ]
  edge [
    source 25
    target 1444
  ]
  edge [
    source 25
    target 1445
  ]
  edge [
    source 25
    target 1446
  ]
  edge [
    source 25
    target 1447
  ]
  edge [
    source 25
    target 1448
  ]
  edge [
    source 25
    target 1449
  ]
  edge [
    source 25
    target 1450
  ]
  edge [
    source 25
    target 1451
  ]
  edge [
    source 25
    target 1452
  ]
  edge [
    source 25
    target 440
  ]
  edge [
    source 25
    target 1453
  ]
  edge [
    source 25
    target 1454
  ]
  edge [
    source 25
    target 584
  ]
  edge [
    source 25
    target 1455
  ]
  edge [
    source 25
    target 1235
  ]
  edge [
    source 25
    target 1456
  ]
  edge [
    source 25
    target 1457
  ]
  edge [
    source 25
    target 1458
  ]
  edge [
    source 25
    target 896
  ]
  edge [
    source 25
    target 1459
  ]
  edge [
    source 25
    target 1460
  ]
  edge [
    source 25
    target 1461
  ]
  edge [
    source 25
    target 1462
  ]
  edge [
    source 25
    target 1463
  ]
  edge [
    source 25
    target 1464
  ]
  edge [
    source 25
    target 1465
  ]
  edge [
    source 25
    target 411
  ]
  edge [
    source 25
    target 1466
  ]
  edge [
    source 25
    target 1467
  ]
  edge [
    source 25
    target 1468
  ]
  edge [
    source 25
    target 1469
  ]
  edge [
    source 25
    target 1470
  ]
  edge [
    source 25
    target 1471
  ]
  edge [
    source 25
    target 1472
  ]
  edge [
    source 25
    target 1473
  ]
  edge [
    source 25
    target 1474
  ]
  edge [
    source 25
    target 1475
  ]
  edge [
    source 25
    target 178
  ]
  edge [
    source 25
    target 1476
  ]
  edge [
    source 25
    target 161
  ]
  edge [
    source 25
    target 1477
  ]
  edge [
    source 25
    target 1478
  ]
  edge [
    source 25
    target 1479
  ]
  edge [
    source 25
    target 1480
  ]
  edge [
    source 25
    target 1481
  ]
  edge [
    source 25
    target 1482
  ]
  edge [
    source 25
    target 1483
  ]
  edge [
    source 25
    target 1484
  ]
  edge [
    source 25
    target 1485
  ]
  edge [
    source 25
    target 1486
  ]
  edge [
    source 25
    target 1487
  ]
  edge [
    source 25
    target 1488
  ]
  edge [
    source 25
    target 1489
  ]
  edge [
    source 25
    target 1490
  ]
  edge [
    source 25
    target 1491
  ]
  edge [
    source 25
    target 1492
  ]
  edge [
    source 25
    target 749
  ]
  edge [
    source 25
    target 1493
  ]
  edge [
    source 25
    target 1494
  ]
  edge [
    source 25
    target 1495
  ]
  edge [
    source 25
    target 1496
  ]
  edge [
    source 25
    target 1497
  ]
  edge [
    source 25
    target 1498
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1499
  ]
  edge [
    source 26
    target 1259
  ]
  edge [
    source 26
    target 1500
  ]
  edge [
    source 26
    target 1501
  ]
  edge [
    source 26
    target 1502
  ]
  edge [
    source 26
    target 1503
  ]
  edge [
    source 26
    target 1504
  ]
  edge [
    source 26
    target 1505
  ]
  edge [
    source 26
    target 1506
  ]
  edge [
    source 26
    target 1507
  ]
  edge [
    source 26
    target 1508
  ]
  edge [
    source 26
    target 1509
  ]
  edge [
    source 26
    target 191
  ]
  edge [
    source 26
    target 1510
  ]
  edge [
    source 26
    target 123
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 1511
  ]
  edge [
    source 26
    target 1512
  ]
  edge [
    source 26
    target 1513
  ]
  edge [
    source 26
    target 1514
  ]
  edge [
    source 26
    target 1515
  ]
  edge [
    source 26
    target 513
  ]
  edge [
    source 26
    target 1516
  ]
  edge [
    source 26
    target 1517
  ]
  edge [
    source 26
    target 1518
  ]
  edge [
    source 26
    target 1519
  ]
  edge [
    source 26
    target 1520
  ]
  edge [
    source 26
    target 1521
  ]
  edge [
    source 26
    target 1522
  ]
  edge [
    source 26
    target 1260
  ]
  edge [
    source 26
    target 1523
  ]
  edge [
    source 26
    target 1524
  ]
  edge [
    source 26
    target 1525
  ]
  edge [
    source 26
    target 1526
  ]
  edge [
    source 26
    target 1527
  ]
  edge [
    source 26
    target 1528
  ]
  edge [
    source 26
    target 1529
  ]
  edge [
    source 26
    target 140
  ]
  edge [
    source 26
    target 1530
  ]
  edge [
    source 26
    target 1531
  ]
  edge [
    source 26
    target 1532
  ]
  edge [
    source 26
    target 1533
  ]
  edge [
    source 26
    target 1534
  ]
  edge [
    source 26
    target 1535
  ]
  edge [
    source 26
    target 1536
  ]
  edge [
    source 26
    target 1537
  ]
  edge [
    source 26
    target 1538
  ]
  edge [
    source 26
    target 1485
  ]
  edge [
    source 26
    target 1539
  ]
  edge [
    source 26
    target 135
  ]
  edge [
    source 26
    target 1540
  ]
  edge [
    source 26
    target 1541
  ]
  edge [
    source 26
    target 1542
  ]
  edge [
    source 26
    target 1543
  ]
  edge [
    source 26
    target 128
  ]
  edge [
    source 26
    target 1544
  ]
  edge [
    source 26
    target 1545
  ]
  edge [
    source 26
    target 1546
  ]
  edge [
    source 26
    target 143
  ]
  edge [
    source 26
    target 1547
  ]
  edge [
    source 26
    target 1548
  ]
  edge [
    source 26
    target 736
  ]
  edge [
    source 26
    target 1039
  ]
  edge [
    source 26
    target 1549
  ]
  edge [
    source 26
    target 1550
  ]
  edge [
    source 26
    target 1551
  ]
  edge [
    source 26
    target 1552
  ]
  edge [
    source 26
    target 1553
  ]
  edge [
    source 26
    target 1554
  ]
  edge [
    source 26
    target 1555
  ]
  edge [
    source 26
    target 1556
  ]
  edge [
    source 26
    target 1557
  ]
  edge [
    source 26
    target 244
  ]
  edge [
    source 26
    target 1558
  ]
  edge [
    source 26
    target 1559
  ]
  edge [
    source 26
    target 1560
  ]
  edge [
    source 26
    target 1561
  ]
  edge [
    source 26
    target 112
  ]
  edge [
    source 26
    target 1562
  ]
  edge [
    source 26
    target 1563
  ]
  edge [
    source 26
    target 1564
  ]
  edge [
    source 26
    target 465
  ]
  edge [
    source 26
    target 1565
  ]
  edge [
    source 26
    target 1566
  ]
  edge [
    source 26
    target 1567
  ]
  edge [
    source 26
    target 1568
  ]
  edge [
    source 26
    target 1569
  ]
  edge [
    source 26
    target 1570
  ]
  edge [
    source 26
    target 1571
  ]
  edge [
    source 26
    target 1572
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1573
  ]
  edge [
    source 27
    target 1574
  ]
  edge [
    source 27
    target 1575
  ]
  edge [
    source 27
    target 1576
  ]
  edge [
    source 27
    target 1577
  ]
  edge [
    source 27
    target 1578
  ]
  edge [
    source 27
    target 1579
  ]
  edge [
    source 27
    target 449
  ]
  edge [
    source 27
    target 1469
  ]
  edge [
    source 27
    target 1580
  ]
  edge [
    source 27
    target 1581
  ]
  edge [
    source 27
    target 1582
  ]
  edge [
    source 27
    target 1583
  ]
  edge [
    source 27
    target 403
  ]
  edge [
    source 27
    target 1584
  ]
  edge [
    source 27
    target 1585
  ]
  edge [
    source 27
    target 1586
  ]
  edge [
    source 27
    target 1587
  ]
  edge [
    source 27
    target 1588
  ]
  edge [
    source 27
    target 1589
  ]
  edge [
    source 27
    target 1590
  ]
  edge [
    source 27
    target 1591
  ]
  edge [
    source 27
    target 1592
  ]
  edge [
    source 27
    target 1593
  ]
  edge [
    source 27
    target 1594
  ]
  edge [
    source 27
    target 588
  ]
  edge [
    source 27
    target 996
  ]
  edge [
    source 27
    target 1595
  ]
  edge [
    source 27
    target 962
  ]
  edge [
    source 27
    target 1596
  ]
  edge [
    source 27
    target 1597
  ]
  edge [
    source 27
    target 1598
  ]
  edge [
    source 27
    target 1599
  ]
  edge [
    source 27
    target 1600
  ]
  edge [
    source 27
    target 1601
  ]
  edge [
    source 27
    target 1602
  ]
  edge [
    source 27
    target 516
  ]
  edge [
    source 27
    target 1603
  ]
  edge [
    source 27
    target 1604
  ]
  edge [
    source 27
    target 1605
  ]
  edge [
    source 27
    target 1606
  ]
  edge [
    source 27
    target 1607
  ]
  edge [
    source 27
    target 1435
  ]
  edge [
    source 27
    target 1608
  ]
  edge [
    source 27
    target 1609
  ]
  edge [
    source 27
    target 1610
  ]
  edge [
    source 27
    target 1611
  ]
  edge [
    source 27
    target 1612
  ]
  edge [
    source 27
    target 305
  ]
  edge [
    source 27
    target 1613
  ]
  edge [
    source 27
    target 1614
  ]
  edge [
    source 27
    target 1615
  ]
  edge [
    source 27
    target 1616
  ]
  edge [
    source 27
    target 1183
  ]
  edge [
    source 27
    target 1617
  ]
  edge [
    source 27
    target 1219
  ]
  edge [
    source 27
    target 1163
  ]
  edge [
    source 27
    target 1618
  ]
  edge [
    source 27
    target 1619
  ]
  edge [
    source 27
    target 1620
  ]
  edge [
    source 27
    target 369
  ]
  edge [
    source 27
    target 1621
  ]
  edge [
    source 27
    target 1447
  ]
  edge [
    source 27
    target 1448
  ]
  edge [
    source 27
    target 1622
  ]
  edge [
    source 27
    target 1623
  ]
  edge [
    source 27
    target 1624
  ]
  edge [
    source 27
    target 1625
  ]
  edge [
    source 27
    target 703
  ]
  edge [
    source 27
    target 1626
  ]
  edge [
    source 27
    target 1627
  ]
  edge [
    source 27
    target 1628
  ]
  edge [
    source 27
    target 1629
  ]
  edge [
    source 27
    target 1630
  ]
  edge [
    source 28
    target 1631
  ]
  edge [
    source 28
    target 1632
  ]
  edge [
    source 28
    target 1633
  ]
  edge [
    source 28
    target 1634
  ]
  edge [
    source 28
    target 1635
  ]
  edge [
    source 28
    target 1636
  ]
  edge [
    source 28
    target 1637
  ]
  edge [
    source 28
    target 1638
  ]
  edge [
    source 28
    target 1639
  ]
  edge [
    source 28
    target 1640
  ]
  edge [
    source 28
    target 1641
  ]
  edge [
    source 28
    target 1642
  ]
  edge [
    source 28
    target 1643
  ]
  edge [
    source 28
    target 1644
  ]
  edge [
    source 28
    target 1523
  ]
  edge [
    source 28
    target 318
  ]
  edge [
    source 28
    target 1645
  ]
  edge [
    source 28
    target 1646
  ]
  edge [
    source 28
    target 1647
  ]
  edge [
    source 28
    target 1648
  ]
  edge [
    source 28
    target 315
  ]
  edge [
    source 28
    target 1649
  ]
  edge [
    source 28
    target 1650
  ]
  edge [
    source 28
    target 1651
  ]
  edge [
    source 28
    target 1652
  ]
  edge [
    source 28
    target 1653
  ]
  edge [
    source 28
    target 871
  ]
  edge [
    source 28
    target 1654
  ]
  edge [
    source 28
    target 1655
  ]
  edge [
    source 28
    target 1656
  ]
  edge [
    source 28
    target 1657
  ]
  edge [
    source 28
    target 1658
  ]
  edge [
    source 28
    target 1659
  ]
  edge [
    source 28
    target 119
  ]
  edge [
    source 28
    target 1660
  ]
  edge [
    source 28
    target 394
  ]
  edge [
    source 28
    target 1661
  ]
  edge [
    source 28
    target 397
  ]
  edge [
    source 28
    target 403
  ]
  edge [
    source 28
    target 1662
  ]
  edge [
    source 28
    target 1663
  ]
  edge [
    source 28
    target 399
  ]
  edge [
    source 28
    target 1664
  ]
  edge [
    source 28
    target 1665
  ]
  edge [
    source 28
    target 1666
  ]
  edge [
    source 28
    target 1667
  ]
  edge [
    source 28
    target 1668
  ]
  edge [
    source 28
    target 1669
  ]
  edge [
    source 28
    target 1670
  ]
  edge [
    source 28
    target 402
  ]
  edge [
    source 28
    target 1671
  ]
  edge [
    source 28
    target 1672
  ]
  edge [
    source 28
    target 1673
  ]
  edge [
    source 28
    target 1674
  ]
  edge [
    source 28
    target 1675
  ]
  edge [
    source 28
    target 1676
  ]
  edge [
    source 28
    target 1677
  ]
  edge [
    source 28
    target 319
  ]
  edge [
    source 28
    target 1678
  ]
  edge [
    source 28
    target 1679
  ]
  edge [
    source 28
    target 1680
  ]
  edge [
    source 28
    target 1681
  ]
  edge [
    source 28
    target 1682
  ]
  edge [
    source 28
    target 1683
  ]
  edge [
    source 28
    target 1684
  ]
  edge [
    source 28
    target 1685
  ]
  edge [
    source 28
    target 1686
  ]
  edge [
    source 28
    target 1687
  ]
  edge [
    source 28
    target 1688
  ]
  edge [
    source 28
    target 1689
  ]
  edge [
    source 28
    target 1690
  ]
  edge [
    source 28
    target 395
  ]
  edge [
    source 28
    target 1691
  ]
  edge [
    source 28
    target 1692
  ]
  edge [
    source 28
    target 1693
  ]
  edge [
    source 28
    target 1694
  ]
  edge [
    source 28
    target 1695
  ]
  edge [
    source 28
    target 1696
  ]
  edge [
    source 28
    target 1697
  ]
  edge [
    source 28
    target 1577
  ]
  edge [
    source 28
    target 1442
  ]
  edge [
    source 28
    target 1698
  ]
  edge [
    source 28
    target 1699
  ]
  edge [
    source 28
    target 1700
  ]
  edge [
    source 28
    target 1701
  ]
  edge [
    source 28
    target 1702
  ]
  edge [
    source 28
    target 65
  ]
  edge [
    source 28
    target 1703
  ]
  edge [
    source 28
    target 1356
  ]
  edge [
    source 28
    target 1704
  ]
  edge [
    source 28
    target 1705
  ]
  edge [
    source 28
    target 1706
  ]
  edge [
    source 28
    target 1707
  ]
  edge [
    source 28
    target 1708
  ]
  edge [
    source 28
    target 1709
  ]
  edge [
    source 28
    target 659
  ]
  edge [
    source 28
    target 1361
  ]
  edge [
    source 28
    target 1710
  ]
  edge [
    source 28
    target 1711
  ]
  edge [
    source 28
    target 1712
  ]
  edge [
    source 28
    target 1713
  ]
  edge [
    source 28
    target 1714
  ]
  edge [
    source 28
    target 1715
  ]
  edge [
    source 28
    target 419
  ]
  edge [
    source 28
    target 1716
  ]
  edge [
    source 28
    target 1717
  ]
  edge [
    source 28
    target 760
  ]
  edge [
    source 28
    target 1718
  ]
  edge [
    source 28
    target 1719
  ]
  edge [
    source 28
    target 1031
  ]
  edge [
    source 28
    target 1720
  ]
  edge [
    source 28
    target 1721
  ]
  edge [
    source 28
    target 1722
  ]
  edge [
    source 28
    target 1723
  ]
  edge [
    source 28
    target 1724
  ]
  edge [
    source 28
    target 1725
  ]
  edge [
    source 28
    target 1726
  ]
  edge [
    source 28
    target 1180
  ]
  edge [
    source 28
    target 1727
  ]
  edge [
    source 28
    target 1728
  ]
  edge [
    source 28
    target 1729
  ]
  edge [
    source 28
    target 757
  ]
  edge [
    source 28
    target 1730
  ]
  edge [
    source 28
    target 1731
  ]
  edge [
    source 28
    target 1732
  ]
  edge [
    source 28
    target 1733
  ]
  edge [
    source 28
    target 1734
  ]
  edge [
    source 28
    target 1735
  ]
  edge [
    source 28
    target 1736
  ]
  edge [
    source 28
    target 1737
  ]
  edge [
    source 28
    target 1738
  ]
  edge [
    source 28
    target 1739
  ]
  edge [
    source 28
    target 1740
  ]
  edge [
    source 28
    target 1741
  ]
  edge [
    source 28
    target 463
  ]
  edge [
    source 28
    target 1742
  ]
  edge [
    source 28
    target 1743
  ]
  edge [
    source 28
    target 1744
  ]
  edge [
    source 28
    target 1745
  ]
  edge [
    source 28
    target 1052
  ]
  edge [
    source 28
    target 1746
  ]
  edge [
    source 28
    target 1747
  ]
  edge [
    source 28
    target 1748
  ]
  edge [
    source 28
    target 1749
  ]
  edge [
    source 28
    target 1750
  ]
  edge [
    source 28
    target 906
  ]
  edge [
    source 28
    target 1175
  ]
  edge [
    source 28
    target 1751
  ]
  edge [
    source 28
    target 1752
  ]
  edge [
    source 28
    target 1272
  ]
  edge [
    source 28
    target 1753
  ]
  edge [
    source 28
    target 989
  ]
  edge [
    source 28
    target 1754
  ]
  edge [
    source 28
    target 1755
  ]
  edge [
    source 28
    target 412
  ]
  edge [
    source 28
    target 827
  ]
  edge [
    source 28
    target 1756
  ]
  edge [
    source 28
    target 1183
  ]
  edge [
    source 28
    target 1757
  ]
  edge [
    source 28
    target 1758
  ]
  edge [
    source 28
    target 1759
  ]
  edge [
    source 28
    target 1760
  ]
  edge [
    source 28
    target 1761
  ]
  edge [
    source 28
    target 1762
  ]
  edge [
    source 28
    target 678
  ]
  edge [
    source 28
    target 1763
  ]
  edge [
    source 28
    target 1764
  ]
  edge [
    source 28
    target 1765
  ]
  edge [
    source 28
    target 1766
  ]
  edge [
    source 28
    target 1767
  ]
  edge [
    source 28
    target 1768
  ]
  edge [
    source 28
    target 1769
  ]
  edge [
    source 28
    target 1770
  ]
  edge [
    source 28
    target 1771
  ]
  edge [
    source 28
    target 1772
  ]
  edge [
    source 28
    target 1773
  ]
  edge [
    source 28
    target 1774
  ]
  edge [
    source 28
    target 1775
  ]
  edge [
    source 28
    target 1776
  ]
  edge [
    source 28
    target 1777
  ]
  edge [
    source 28
    target 1778
  ]
  edge [
    source 28
    target 1779
  ]
  edge [
    source 28
    target 1780
  ]
  edge [
    source 28
    target 1781
  ]
  edge [
    source 28
    target 1782
  ]
  edge [
    source 28
    target 1783
  ]
  edge [
    source 28
    target 1784
  ]
  edge [
    source 28
    target 1785
  ]
  edge [
    source 28
    target 1786
  ]
  edge [
    source 28
    target 1787
  ]
  edge [
    source 28
    target 1788
  ]
  edge [
    source 28
    target 1789
  ]
  edge [
    source 28
    target 1790
  ]
  edge [
    source 28
    target 457
  ]
  edge [
    source 28
    target 1121
  ]
  edge [
    source 28
    target 1791
  ]
  edge [
    source 28
    target 1792
  ]
  edge [
    source 28
    target 1793
  ]
  edge [
    source 28
    target 1794
  ]
  edge [
    source 28
    target 1795
  ]
  edge [
    source 28
    target 1796
  ]
  edge [
    source 28
    target 975
  ]
  edge [
    source 28
    target 1797
  ]
  edge [
    source 28
    target 1798
  ]
  edge [
    source 28
    target 1799
  ]
  edge [
    source 28
    target 1800
  ]
  edge [
    source 28
    target 1801
  ]
  edge [
    source 28
    target 1377
  ]
  edge [
    source 28
    target 1802
  ]
  edge [
    source 28
    target 1803
  ]
  edge [
    source 28
    target 1804
  ]
  edge [
    source 28
    target 1805
  ]
  edge [
    source 28
    target 1806
  ]
  edge [
    source 28
    target 1807
  ]
  edge [
    source 28
    target 1808
  ]
  edge [
    source 28
    target 1809
  ]
  edge [
    source 28
    target 1810
  ]
  edge [
    source 28
    target 1811
  ]
  edge [
    source 28
    target 1812
  ]
  edge [
    source 28
    target 1813
  ]
  edge [
    source 28
    target 1814
  ]
  edge [
    source 28
    target 1815
  ]
  edge [
    source 28
    target 1816
  ]
  edge [
    source 28
    target 1817
  ]
  edge [
    source 28
    target 1818
  ]
  edge [
    source 28
    target 1819
  ]
  edge [
    source 28
    target 1820
  ]
  edge [
    source 28
    target 663
  ]
  edge [
    source 28
    target 1821
  ]
  edge [
    source 28
    target 1822
  ]
  edge [
    source 28
    target 1823
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 28
    target 1824
  ]
  edge [
    source 28
    target 1825
  ]
  edge [
    source 28
    target 1826
  ]
  edge [
    source 28
    target 90
  ]
  edge [
    source 28
    target 1827
  ]
  edge [
    source 28
    target 1828
  ]
  edge [
    source 28
    target 1829
  ]
  edge [
    source 28
    target 1830
  ]
  edge [
    source 28
    target 1831
  ]
  edge [
    source 28
    target 1832
  ]
  edge [
    source 28
    target 1833
  ]
  edge [
    source 28
    target 1834
  ]
  edge [
    source 28
    target 686
  ]
  edge [
    source 28
    target 1835
  ]
  edge [
    source 28
    target 1836
  ]
  edge [
    source 28
    target 1837
  ]
  edge [
    source 28
    target 1838
  ]
  edge [
    source 28
    target 1839
  ]
  edge [
    source 28
    target 1840
  ]
  edge [
    source 28
    target 1841
  ]
  edge [
    source 28
    target 411
  ]
  edge [
    source 28
    target 1842
  ]
  edge [
    source 28
    target 1843
  ]
  edge [
    source 28
    target 1844
  ]
  edge [
    source 28
    target 1845
  ]
  edge [
    source 28
    target 1846
  ]
  edge [
    source 28
    target 1847
  ]
  edge [
    source 28
    target 1848
  ]
  edge [
    source 28
    target 1849
  ]
  edge [
    source 28
    target 1850
  ]
  edge [
    source 28
    target 274
  ]
  edge [
    source 28
    target 1851
  ]
  edge [
    source 28
    target 1852
  ]
  edge [
    source 28
    target 1853
  ]
  edge [
    source 28
    target 1854
  ]
  edge [
    source 28
    target 1855
  ]
  edge [
    source 28
    target 1856
  ]
  edge [
    source 28
    target 1857
  ]
  edge [
    source 28
    target 1858
  ]
  edge [
    source 28
    target 1859
  ]
  edge [
    source 28
    target 1860
  ]
  edge [
    source 28
    target 1861
  ]
  edge [
    source 28
    target 1862
  ]
  edge [
    source 28
    target 1863
  ]
  edge [
    source 28
    target 1864
  ]
  edge [
    source 28
    target 1865
  ]
  edge [
    source 28
    target 1866
  ]
  edge [
    source 28
    target 1867
  ]
  edge [
    source 28
    target 1868
  ]
  edge [
    source 28
    target 1869
  ]
  edge [
    source 28
    target 1870
  ]
  edge [
    source 28
    target 1871
  ]
  edge [
    source 28
    target 1872
  ]
  edge [
    source 28
    target 1873
  ]
  edge [
    source 28
    target 661
  ]
  edge [
    source 28
    target 1874
  ]
  edge [
    source 28
    target 1875
  ]
  edge [
    source 28
    target 1876
  ]
  edge [
    source 28
    target 1877
  ]
  edge [
    source 28
    target 1878
  ]
  edge [
    source 1879
    target 1880
  ]
  edge [
    source 1881
    target 1882
  ]
  edge [
    source 1883
    target 1884
  ]
  edge [
    source 1883
    target 1885
  ]
  edge [
    source 1884
    target 1885
  ]
  edge [
    source 1886
    target 1887
  ]
]
