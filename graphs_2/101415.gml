graph [
  node [
    id 0
    label "stratosphere"
    origin "text"
  ]
  node [
    id 1
    label "las"
    origin "text"
  ]
  node [
    id 2
    label "vegas"
    origin "text"
  ]
  node [
    id 3
    label "podszyt"
  ]
  node [
    id 4
    label "dno_lasu"
  ]
  node [
    id 5
    label "nadle&#347;nictwo"
  ]
  node [
    id 6
    label "teren_le&#347;ny"
  ]
  node [
    id 7
    label "zalesienie"
  ]
  node [
    id 8
    label "karczowa&#263;"
  ]
  node [
    id 9
    label "mn&#243;stwo"
  ]
  node [
    id 10
    label "wykarczowa&#263;"
  ]
  node [
    id 11
    label "rewir"
  ]
  node [
    id 12
    label "karczowanie"
  ]
  node [
    id 13
    label "obr&#281;b"
  ]
  node [
    id 14
    label "chody"
  ]
  node [
    id 15
    label "wykarczowanie"
  ]
  node [
    id 16
    label "wiatro&#322;om"
  ]
  node [
    id 17
    label "teren"
  ]
  node [
    id 18
    label "podrost"
  ]
  node [
    id 19
    label "&#347;ci&#243;&#322;ka"
  ]
  node [
    id 20
    label "driada"
  ]
  node [
    id 21
    label "le&#347;nictwo"
  ]
  node [
    id 22
    label "formacja_ro&#347;linna"
  ]
  node [
    id 23
    label "runo"
  ]
  node [
    id 24
    label "ilo&#347;&#263;"
  ]
  node [
    id 25
    label "enormousness"
  ]
  node [
    id 26
    label "wymiar"
  ]
  node [
    id 27
    label "zakres"
  ]
  node [
    id 28
    label "kontekst"
  ]
  node [
    id 29
    label "miejsce_pracy"
  ]
  node [
    id 30
    label "nation"
  ]
  node [
    id 31
    label "krajobraz"
  ]
  node [
    id 32
    label "obszar"
  ]
  node [
    id 33
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 34
    label "przyroda"
  ]
  node [
    id 35
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 36
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 37
    label "w&#322;adza"
  ]
  node [
    id 38
    label "samosiejka"
  ]
  node [
    id 39
    label "s&#322;oma"
  ]
  node [
    id 40
    label "pi&#281;tro"
  ]
  node [
    id 41
    label "dar&#324;"
  ]
  node [
    id 42
    label "warstwa"
  ]
  node [
    id 43
    label "sier&#347;&#263;"
  ]
  node [
    id 44
    label "afforestation"
  ]
  node [
    id 45
    label "zadrzewienie"
  ]
  node [
    id 46
    label "nora"
  ]
  node [
    id 47
    label "pies_my&#347;liwski"
  ]
  node [
    id 48
    label "miejsce"
  ]
  node [
    id 49
    label "trasa"
  ]
  node [
    id 50
    label "doj&#347;cie"
  ]
  node [
    id 51
    label "jednostka_administracyjna"
  ]
  node [
    id 52
    label "urz&#261;d"
  ]
  node [
    id 53
    label "p&#243;&#322;noc"
  ]
  node [
    id 54
    label "Kosowo"
  ]
  node [
    id 55
    label "&#347;cieg"
  ]
  node [
    id 56
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 57
    label "Zab&#322;ocie"
  ]
  node [
    id 58
    label "sector"
  ]
  node [
    id 59
    label "zach&#243;d"
  ]
  node [
    id 60
    label "po&#322;udnie"
  ]
  node [
    id 61
    label "Pow&#261;zki"
  ]
  node [
    id 62
    label "circle"
  ]
  node [
    id 63
    label "Piotrowo"
  ]
  node [
    id 64
    label "Olszanica"
  ]
  node [
    id 65
    label "Ruda_Pabianicka"
  ]
  node [
    id 66
    label "holarktyka"
  ]
  node [
    id 67
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 68
    label "Ludwin&#243;w"
  ]
  node [
    id 69
    label "Arktyka"
  ]
  node [
    id 70
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 71
    label "Zabu&#380;e"
  ]
  node [
    id 72
    label "antroposfera"
  ]
  node [
    id 73
    label "Neogea"
  ]
  node [
    id 74
    label "Syberia_Zachodnia"
  ]
  node [
    id 75
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 76
    label "pas_planetoid"
  ]
  node [
    id 77
    label "Syberia_Wschodnia"
  ]
  node [
    id 78
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 79
    label "Antarktyka"
  ]
  node [
    id 80
    label "Rakowice"
  ]
  node [
    id 81
    label "akrecja"
  ]
  node [
    id 82
    label "&#321;&#281;g"
  ]
  node [
    id 83
    label "Kresy_Zachodnie"
  ]
  node [
    id 84
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 85
    label "przestrze&#324;"
  ]
  node [
    id 86
    label "wyko&#324;czenie"
  ]
  node [
    id 87
    label "wsch&#243;d"
  ]
  node [
    id 88
    label "Notogea"
  ]
  node [
    id 89
    label "krzew"
  ]
  node [
    id 90
    label "usun&#261;&#263;"
  ]
  node [
    id 91
    label "drzewo"
  ]
  node [
    id 92
    label "usuwa&#263;"
  ]
  node [
    id 93
    label "authorize"
  ]
  node [
    id 94
    label "nimfa"
  ]
  node [
    id 95
    label "rejon"
  ]
  node [
    id 96
    label "okr&#281;g"
  ]
  node [
    id 97
    label "komisariat"
  ]
  node [
    id 98
    label "szpital"
  ]
  node [
    id 99
    label "biologia"
  ]
  node [
    id 100
    label "szk&#243;&#322;karstwo"
  ]
  node [
    id 101
    label "nauka_le&#347;na"
  ]
  node [
    id 102
    label "usuwanie"
  ]
  node [
    id 103
    label "ablation"
  ]
  node [
    id 104
    label "usuni&#281;cie"
  ]
  node [
    id 105
    label "Stratosphere"
  ]
  node [
    id 106
    label "lasa"
  ]
  node [
    id 107
    label "Vegas"
  ]
  node [
    id 108
    label "CN"
  ]
  node [
    id 109
    label "Tower"
  ]
  node [
    id 110
    label "Newa"
  ]
  node [
    id 111
    label "york"
  ]
  node [
    id 112
    label "Plaza"
  ]
  node [
    id 113
    label "Freemont"
  ]
  node [
    id 114
    label "street"
  ]
  node [
    id 115
    label "biga"
  ]
  node [
    id 116
    label "Shot"
  ]
  node [
    id 117
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 118
    label "Scream"
  ]
  node [
    id 119
    label "High"
  ]
  node [
    id 120
    label "Roller"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 107
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 112
  ]
  edge [
    source 107
    target 112
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 110
  ]
  edge [
    source 111
    target 111
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 119
    target 120
  ]
]
