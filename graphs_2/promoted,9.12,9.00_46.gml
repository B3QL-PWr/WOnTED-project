graph [
  node [
    id 0
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 1
    label "Stary_&#346;wiat"
  ]
  node [
    id 2
    label "asymilowanie_si&#281;"
  ]
  node [
    id 3
    label "p&#243;&#322;noc"
  ]
  node [
    id 4
    label "przedmiot"
  ]
  node [
    id 5
    label "Wsch&#243;d"
  ]
  node [
    id 6
    label "class"
  ]
  node [
    id 7
    label "geosfera"
  ]
  node [
    id 8
    label "obiekt_naturalny"
  ]
  node [
    id 9
    label "przejmowanie"
  ]
  node [
    id 10
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 11
    label "przyroda"
  ]
  node [
    id 12
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 13
    label "po&#322;udnie"
  ]
  node [
    id 14
    label "zjawisko"
  ]
  node [
    id 15
    label "rzecz"
  ]
  node [
    id 16
    label "makrokosmos"
  ]
  node [
    id 17
    label "huczek"
  ]
  node [
    id 18
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 19
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 20
    label "environment"
  ]
  node [
    id 21
    label "morze"
  ]
  node [
    id 22
    label "rze&#378;ba"
  ]
  node [
    id 23
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 24
    label "przejmowa&#263;"
  ]
  node [
    id 25
    label "hydrosfera"
  ]
  node [
    id 26
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 27
    label "ciemna_materia"
  ]
  node [
    id 28
    label "ekosystem"
  ]
  node [
    id 29
    label "biota"
  ]
  node [
    id 30
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 31
    label "ekosfera"
  ]
  node [
    id 32
    label "geotermia"
  ]
  node [
    id 33
    label "planeta"
  ]
  node [
    id 34
    label "ozonosfera"
  ]
  node [
    id 35
    label "wszechstworzenie"
  ]
  node [
    id 36
    label "grupa"
  ]
  node [
    id 37
    label "woda"
  ]
  node [
    id 38
    label "kuchnia"
  ]
  node [
    id 39
    label "biosfera"
  ]
  node [
    id 40
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 41
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 42
    label "populace"
  ]
  node [
    id 43
    label "magnetosfera"
  ]
  node [
    id 44
    label "Nowy_&#346;wiat"
  ]
  node [
    id 45
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 46
    label "universe"
  ]
  node [
    id 47
    label "biegun"
  ]
  node [
    id 48
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 49
    label "litosfera"
  ]
  node [
    id 50
    label "teren"
  ]
  node [
    id 51
    label "mikrokosmos"
  ]
  node [
    id 52
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 53
    label "przestrze&#324;"
  ]
  node [
    id 54
    label "stw&#243;r"
  ]
  node [
    id 55
    label "p&#243;&#322;kula"
  ]
  node [
    id 56
    label "przej&#281;cie"
  ]
  node [
    id 57
    label "barysfera"
  ]
  node [
    id 58
    label "obszar"
  ]
  node [
    id 59
    label "czarna_dziura"
  ]
  node [
    id 60
    label "atmosfera"
  ]
  node [
    id 61
    label "przej&#261;&#263;"
  ]
  node [
    id 62
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 63
    label "Ziemia"
  ]
  node [
    id 64
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 65
    label "geoida"
  ]
  node [
    id 66
    label "zagranica"
  ]
  node [
    id 67
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 68
    label "fauna"
  ]
  node [
    id 69
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 70
    label "odm&#322;adzanie"
  ]
  node [
    id 71
    label "liga"
  ]
  node [
    id 72
    label "jednostka_systematyczna"
  ]
  node [
    id 73
    label "asymilowanie"
  ]
  node [
    id 74
    label "gromada"
  ]
  node [
    id 75
    label "asymilowa&#263;"
  ]
  node [
    id 76
    label "egzemplarz"
  ]
  node [
    id 77
    label "Entuzjastki"
  ]
  node [
    id 78
    label "zbi&#243;r"
  ]
  node [
    id 79
    label "kompozycja"
  ]
  node [
    id 80
    label "Terranie"
  ]
  node [
    id 81
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 82
    label "category"
  ]
  node [
    id 83
    label "pakiet_klimatyczny"
  ]
  node [
    id 84
    label "oddzia&#322;"
  ]
  node [
    id 85
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 86
    label "cz&#261;steczka"
  ]
  node [
    id 87
    label "stage_set"
  ]
  node [
    id 88
    label "type"
  ]
  node [
    id 89
    label "specgrupa"
  ]
  node [
    id 90
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 91
    label "&#346;wietliki"
  ]
  node [
    id 92
    label "odm&#322;odzenie"
  ]
  node [
    id 93
    label "Eurogrupa"
  ]
  node [
    id 94
    label "odm&#322;adza&#263;"
  ]
  node [
    id 95
    label "formacja_geologiczna"
  ]
  node [
    id 96
    label "harcerze_starsi"
  ]
  node [
    id 97
    label "Kosowo"
  ]
  node [
    id 98
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 99
    label "Zab&#322;ocie"
  ]
  node [
    id 100
    label "zach&#243;d"
  ]
  node [
    id 101
    label "Pow&#261;zki"
  ]
  node [
    id 102
    label "Piotrowo"
  ]
  node [
    id 103
    label "Olszanica"
  ]
  node [
    id 104
    label "Ruda_Pabianicka"
  ]
  node [
    id 105
    label "holarktyka"
  ]
  node [
    id 106
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 107
    label "Ludwin&#243;w"
  ]
  node [
    id 108
    label "Arktyka"
  ]
  node [
    id 109
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 110
    label "Zabu&#380;e"
  ]
  node [
    id 111
    label "miejsce"
  ]
  node [
    id 112
    label "antroposfera"
  ]
  node [
    id 113
    label "Neogea"
  ]
  node [
    id 114
    label "terytorium"
  ]
  node [
    id 115
    label "Syberia_Zachodnia"
  ]
  node [
    id 116
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 117
    label "zakres"
  ]
  node [
    id 118
    label "pas_planetoid"
  ]
  node [
    id 119
    label "Syberia_Wschodnia"
  ]
  node [
    id 120
    label "Antarktyka"
  ]
  node [
    id 121
    label "Rakowice"
  ]
  node [
    id 122
    label "akrecja"
  ]
  node [
    id 123
    label "wymiar"
  ]
  node [
    id 124
    label "&#321;&#281;g"
  ]
  node [
    id 125
    label "Kresy_Zachodnie"
  ]
  node [
    id 126
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 127
    label "wsch&#243;d"
  ]
  node [
    id 128
    label "Notogea"
  ]
  node [
    id 129
    label "integer"
  ]
  node [
    id 130
    label "liczba"
  ]
  node [
    id 131
    label "zlewanie_si&#281;"
  ]
  node [
    id 132
    label "ilo&#347;&#263;"
  ]
  node [
    id 133
    label "uk&#322;ad"
  ]
  node [
    id 134
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 135
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 136
    label "pe&#322;ny"
  ]
  node [
    id 137
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 138
    label "proces"
  ]
  node [
    id 139
    label "boski"
  ]
  node [
    id 140
    label "krajobraz"
  ]
  node [
    id 141
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 142
    label "przywidzenie"
  ]
  node [
    id 143
    label "presence"
  ]
  node [
    id 144
    label "charakter"
  ]
  node [
    id 145
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 146
    label "rozdzielanie"
  ]
  node [
    id 147
    label "bezbrze&#380;e"
  ]
  node [
    id 148
    label "punkt"
  ]
  node [
    id 149
    label "czasoprzestrze&#324;"
  ]
  node [
    id 150
    label "niezmierzony"
  ]
  node [
    id 151
    label "przedzielenie"
  ]
  node [
    id 152
    label "nielito&#347;ciwy"
  ]
  node [
    id 153
    label "rozdziela&#263;"
  ]
  node [
    id 154
    label "oktant"
  ]
  node [
    id 155
    label "przedzieli&#263;"
  ]
  node [
    id 156
    label "przestw&#243;r"
  ]
  node [
    id 157
    label "&#347;rodowisko"
  ]
  node [
    id 158
    label "rura"
  ]
  node [
    id 159
    label "grzebiuszka"
  ]
  node [
    id 160
    label "cz&#322;owiek"
  ]
  node [
    id 161
    label "odbicie"
  ]
  node [
    id 162
    label "atom"
  ]
  node [
    id 163
    label "kosmos"
  ]
  node [
    id 164
    label "miniatura"
  ]
  node [
    id 165
    label "smok_wawelski"
  ]
  node [
    id 166
    label "niecz&#322;owiek"
  ]
  node [
    id 167
    label "monster"
  ]
  node [
    id 168
    label "istota_&#380;ywa"
  ]
  node [
    id 169
    label "potw&#243;r"
  ]
  node [
    id 170
    label "istota_fantastyczna"
  ]
  node [
    id 171
    label "kultura"
  ]
  node [
    id 172
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 173
    label "ciep&#322;o"
  ]
  node [
    id 174
    label "energia_termiczna"
  ]
  node [
    id 175
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 176
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 177
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 178
    label "aspekt"
  ]
  node [
    id 179
    label "troposfera"
  ]
  node [
    id 180
    label "klimat"
  ]
  node [
    id 181
    label "metasfera"
  ]
  node [
    id 182
    label "atmosferyki"
  ]
  node [
    id 183
    label "homosfera"
  ]
  node [
    id 184
    label "cecha"
  ]
  node [
    id 185
    label "powietrznia"
  ]
  node [
    id 186
    label "jonosfera"
  ]
  node [
    id 187
    label "termosfera"
  ]
  node [
    id 188
    label "egzosfera"
  ]
  node [
    id 189
    label "heterosfera"
  ]
  node [
    id 190
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 191
    label "tropopauza"
  ]
  node [
    id 192
    label "kwas"
  ]
  node [
    id 193
    label "powietrze"
  ]
  node [
    id 194
    label "stratosfera"
  ]
  node [
    id 195
    label "pow&#322;oka"
  ]
  node [
    id 196
    label "mezosfera"
  ]
  node [
    id 197
    label "mezopauza"
  ]
  node [
    id 198
    label "atmosphere"
  ]
  node [
    id 199
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 200
    label "sferoida"
  ]
  node [
    id 201
    label "object"
  ]
  node [
    id 202
    label "temat"
  ]
  node [
    id 203
    label "wpadni&#281;cie"
  ]
  node [
    id 204
    label "mienie"
  ]
  node [
    id 205
    label "istota"
  ]
  node [
    id 206
    label "obiekt"
  ]
  node [
    id 207
    label "wpa&#347;&#263;"
  ]
  node [
    id 208
    label "wpadanie"
  ]
  node [
    id 209
    label "wpada&#263;"
  ]
  node [
    id 210
    label "treat"
  ]
  node [
    id 211
    label "czerpa&#263;"
  ]
  node [
    id 212
    label "bra&#263;"
  ]
  node [
    id 213
    label "go"
  ]
  node [
    id 214
    label "handle"
  ]
  node [
    id 215
    label "wzbudza&#263;"
  ]
  node [
    id 216
    label "ogarnia&#263;"
  ]
  node [
    id 217
    label "bang"
  ]
  node [
    id 218
    label "wzi&#261;&#263;"
  ]
  node [
    id 219
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 220
    label "stimulate"
  ]
  node [
    id 221
    label "ogarn&#261;&#263;"
  ]
  node [
    id 222
    label "wzbudzi&#263;"
  ]
  node [
    id 223
    label "thrill"
  ]
  node [
    id 224
    label "czerpanie"
  ]
  node [
    id 225
    label "acquisition"
  ]
  node [
    id 226
    label "branie"
  ]
  node [
    id 227
    label "caparison"
  ]
  node [
    id 228
    label "movement"
  ]
  node [
    id 229
    label "wzbudzanie"
  ]
  node [
    id 230
    label "czynno&#347;&#263;"
  ]
  node [
    id 231
    label "ogarnianie"
  ]
  node [
    id 232
    label "wra&#380;enie"
  ]
  node [
    id 233
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 234
    label "interception"
  ]
  node [
    id 235
    label "wzbudzenie"
  ]
  node [
    id 236
    label "emotion"
  ]
  node [
    id 237
    label "zaczerpni&#281;cie"
  ]
  node [
    id 238
    label "wzi&#281;cie"
  ]
  node [
    id 239
    label "zboczenie"
  ]
  node [
    id 240
    label "om&#243;wienie"
  ]
  node [
    id 241
    label "sponiewieranie"
  ]
  node [
    id 242
    label "discipline"
  ]
  node [
    id 243
    label "omawia&#263;"
  ]
  node [
    id 244
    label "kr&#261;&#380;enie"
  ]
  node [
    id 245
    label "tre&#347;&#263;"
  ]
  node [
    id 246
    label "robienie"
  ]
  node [
    id 247
    label "sponiewiera&#263;"
  ]
  node [
    id 248
    label "element"
  ]
  node [
    id 249
    label "entity"
  ]
  node [
    id 250
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 251
    label "tematyka"
  ]
  node [
    id 252
    label "w&#261;tek"
  ]
  node [
    id 253
    label "zbaczanie"
  ]
  node [
    id 254
    label "program_nauczania"
  ]
  node [
    id 255
    label "om&#243;wi&#263;"
  ]
  node [
    id 256
    label "omawianie"
  ]
  node [
    id 257
    label "thing"
  ]
  node [
    id 258
    label "zbacza&#263;"
  ]
  node [
    id 259
    label "zboczy&#263;"
  ]
  node [
    id 260
    label "performance"
  ]
  node [
    id 261
    label "sztuka"
  ]
  node [
    id 262
    label "Boreasz"
  ]
  node [
    id 263
    label "noc"
  ]
  node [
    id 264
    label "p&#243;&#322;nocek"
  ]
  node [
    id 265
    label "strona_&#347;wiata"
  ]
  node [
    id 266
    label "godzina"
  ]
  node [
    id 267
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 268
    label "granica_pa&#324;stwa"
  ]
  node [
    id 269
    label "warstwa"
  ]
  node [
    id 270
    label "kriosfera"
  ]
  node [
    id 271
    label "lej_polarny"
  ]
  node [
    id 272
    label "sfera"
  ]
  node [
    id 273
    label "brzeg"
  ]
  node [
    id 274
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 275
    label "p&#322;oza"
  ]
  node [
    id 276
    label "zawiasy"
  ]
  node [
    id 277
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 278
    label "organ"
  ]
  node [
    id 279
    label "element_anatomiczny"
  ]
  node [
    id 280
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 281
    label "reda"
  ]
  node [
    id 282
    label "zbiornik_wodny"
  ]
  node [
    id 283
    label "przymorze"
  ]
  node [
    id 284
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 285
    label "bezmiar"
  ]
  node [
    id 286
    label "pe&#322;ne_morze"
  ]
  node [
    id 287
    label "latarnia_morska"
  ]
  node [
    id 288
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 289
    label "nereida"
  ]
  node [
    id 290
    label "okeanida"
  ]
  node [
    id 291
    label "marina"
  ]
  node [
    id 292
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 293
    label "Morze_Czerwone"
  ]
  node [
    id 294
    label "talasoterapia"
  ]
  node [
    id 295
    label "Morze_Bia&#322;e"
  ]
  node [
    id 296
    label "paliszcze"
  ]
  node [
    id 297
    label "Neptun"
  ]
  node [
    id 298
    label "Morze_Czarne"
  ]
  node [
    id 299
    label "laguna"
  ]
  node [
    id 300
    label "Morze_Egejskie"
  ]
  node [
    id 301
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 302
    label "Morze_Adriatyckie"
  ]
  node [
    id 303
    label "rze&#378;biarstwo"
  ]
  node [
    id 304
    label "planacja"
  ]
  node [
    id 305
    label "relief"
  ]
  node [
    id 306
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 307
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 308
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 309
    label "bozzetto"
  ]
  node [
    id 310
    label "plastyka"
  ]
  node [
    id 311
    label "j&#261;dro"
  ]
  node [
    id 312
    label "&#347;rodek"
  ]
  node [
    id 313
    label "dzie&#324;"
  ]
  node [
    id 314
    label "dwunasta"
  ]
  node [
    id 315
    label "pora"
  ]
  node [
    id 316
    label "ozon"
  ]
  node [
    id 317
    label "gleba"
  ]
  node [
    id 318
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 319
    label "sialma"
  ]
  node [
    id 320
    label "skorupa_ziemska"
  ]
  node [
    id 321
    label "warstwa_perydotytowa"
  ]
  node [
    id 322
    label "warstwa_granitowa"
  ]
  node [
    id 323
    label "kula"
  ]
  node [
    id 324
    label "kresom&#243;zgowie"
  ]
  node [
    id 325
    label "przyra"
  ]
  node [
    id 326
    label "biom"
  ]
  node [
    id 327
    label "awifauna"
  ]
  node [
    id 328
    label "ichtiofauna"
  ]
  node [
    id 329
    label "geosystem"
  ]
  node [
    id 330
    label "dotleni&#263;"
  ]
  node [
    id 331
    label "spi&#281;trza&#263;"
  ]
  node [
    id 332
    label "spi&#281;trzenie"
  ]
  node [
    id 333
    label "utylizator"
  ]
  node [
    id 334
    label "p&#322;ycizna"
  ]
  node [
    id 335
    label "nabranie"
  ]
  node [
    id 336
    label "Waruna"
  ]
  node [
    id 337
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 338
    label "przybieranie"
  ]
  node [
    id 339
    label "uci&#261;g"
  ]
  node [
    id 340
    label "bombast"
  ]
  node [
    id 341
    label "fala"
  ]
  node [
    id 342
    label "kryptodepresja"
  ]
  node [
    id 343
    label "water"
  ]
  node [
    id 344
    label "wysi&#281;k"
  ]
  node [
    id 345
    label "pustka"
  ]
  node [
    id 346
    label "ciecz"
  ]
  node [
    id 347
    label "przybrze&#380;e"
  ]
  node [
    id 348
    label "nap&#243;j"
  ]
  node [
    id 349
    label "spi&#281;trzanie"
  ]
  node [
    id 350
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 351
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 352
    label "bicie"
  ]
  node [
    id 353
    label "klarownik"
  ]
  node [
    id 354
    label "chlastanie"
  ]
  node [
    id 355
    label "woda_s&#322;odka"
  ]
  node [
    id 356
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 357
    label "nabra&#263;"
  ]
  node [
    id 358
    label "chlasta&#263;"
  ]
  node [
    id 359
    label "uj&#281;cie_wody"
  ]
  node [
    id 360
    label "zrzut"
  ]
  node [
    id 361
    label "wypowied&#378;"
  ]
  node [
    id 362
    label "wodnik"
  ]
  node [
    id 363
    label "pojazd"
  ]
  node [
    id 364
    label "l&#243;d"
  ]
  node [
    id 365
    label "wybrze&#380;e"
  ]
  node [
    id 366
    label "deklamacja"
  ]
  node [
    id 367
    label "tlenek"
  ]
  node [
    id 368
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 369
    label "biotop"
  ]
  node [
    id 370
    label "biocenoza"
  ]
  node [
    id 371
    label "kontekst"
  ]
  node [
    id 372
    label "miejsce_pracy"
  ]
  node [
    id 373
    label "nation"
  ]
  node [
    id 374
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 375
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 376
    label "w&#322;adza"
  ]
  node [
    id 377
    label "szata_ro&#347;linna"
  ]
  node [
    id 378
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 379
    label "formacja_ro&#347;linna"
  ]
  node [
    id 380
    label "zielono&#347;&#263;"
  ]
  node [
    id 381
    label "pi&#281;tro"
  ]
  node [
    id 382
    label "plant"
  ]
  node [
    id 383
    label "ro&#347;lina"
  ]
  node [
    id 384
    label "iglak"
  ]
  node [
    id 385
    label "cyprysowate"
  ]
  node [
    id 386
    label "zaj&#281;cie"
  ]
  node [
    id 387
    label "instytucja"
  ]
  node [
    id 388
    label "tajniki"
  ]
  node [
    id 389
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 390
    label "jedzenie"
  ]
  node [
    id 391
    label "zaplecze"
  ]
  node [
    id 392
    label "pomieszczenie"
  ]
  node [
    id 393
    label "zlewozmywak"
  ]
  node [
    id 394
    label "gotowa&#263;"
  ]
  node [
    id 395
    label "strefa"
  ]
  node [
    id 396
    label "Jowisz"
  ]
  node [
    id 397
    label "syzygia"
  ]
  node [
    id 398
    label "Saturn"
  ]
  node [
    id 399
    label "Uran"
  ]
  node [
    id 400
    label "message"
  ]
  node [
    id 401
    label "dar"
  ]
  node [
    id 402
    label "real"
  ]
  node [
    id 403
    label "Ukraina"
  ]
  node [
    id 404
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 405
    label "blok_wschodni"
  ]
  node [
    id 406
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 407
    label "Europa_Wschodnia"
  ]
  node [
    id 408
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 409
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 410
    label "Arabia"
  ]
  node [
    id 411
    label "saudyjski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 0
    target 401
  ]
  edge [
    source 0
    target 402
  ]
  edge [
    source 0
    target 403
  ]
  edge [
    source 0
    target 404
  ]
  edge [
    source 0
    target 405
  ]
  edge [
    source 0
    target 406
  ]
  edge [
    source 0
    target 407
  ]
  edge [
    source 0
    target 408
  ]
  edge [
    source 0
    target 409
  ]
  edge [
    source 410
    target 411
  ]
]
