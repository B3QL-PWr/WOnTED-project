graph [
  node [
    id 0
    label "podstawa"
    origin "text"
  ]
  node [
    id 1
    label "art"
    origin "text"
  ]
  node [
    id 2
    label "usta"
    origin "text"
  ]
  node [
    id 3
    label "ustawa"
    origin "text"
  ]
  node [
    id 4
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "maj"
    origin "text"
  ]
  node [
    id 6
    label "rocznik"
    origin "text"
  ]
  node [
    id 7
    label "&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 8
    label "pieni&#281;&#380;ny"
    origin "text"
  ]
  node [
    id 9
    label "przys&#322;ugiwa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "osoba"
    origin "text"
  ]
  node [
    id 11
    label "deportowana"
    origin "text"
  ]
  node [
    id 12
    label "praca"
    origin "text"
  ]
  node [
    id 13
    label "przymusowy"
    origin "text"
  ]
  node [
    id 14
    label "osadzona"
    origin "text"
  ]
  node [
    id 15
    label "ob&#243;z"
    origin "text"
  ]
  node [
    id 16
    label "przez"
    origin "text"
  ]
  node [
    id 17
    label "iii"
    origin "text"
  ]
  node [
    id 18
    label "rzesza"
    origin "text"
  ]
  node [
    id 19
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 20
    label "socjalistyczny"
    origin "text"
  ]
  node [
    id 21
    label "republika"
    origin "text"
  ]
  node [
    id 22
    label "radziecki"
    origin "text"
  ]
  node [
    id 23
    label "dziennik"
    origin "text"
  ]
  node [
    id 24
    label "poz"
    origin "text"
  ]
  node [
    id 25
    label "zarz&#261;dza&#263;"
    origin "text"
  ]
  node [
    id 26
    label "si&#281;"
    origin "text"
  ]
  node [
    id 27
    label "nast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 28
    label "post&#281;powanie"
    origin "text"
  ]
  node [
    id 29
    label "przyznanie"
    origin "text"
  ]
  node [
    id 30
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 31
    label "mowa"
    origin "text"
  ]
  node [
    id 32
    label "zwa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "daleko"
    origin "text"
  ]
  node [
    id 34
    label "wszczyna&#263;"
    origin "text"
  ]
  node [
    id 35
    label "pisemny"
    origin "text"
  ]
  node [
    id 36
    label "wniosek"
    origin "text"
  ]
  node [
    id 37
    label "zainteresowana"
    origin "text"
  ]
  node [
    id 38
    label "zaopiniowa&#263;"
    origin "text"
  ]
  node [
    id 39
    label "w&#322;a&#347;ciwy"
    origin "text"
  ]
  node [
    id 40
    label "stowarzyszenie"
    origin "text"
  ]
  node [
    id 41
    label "poszkodowana"
    origin "text"
  ]
  node [
    id 42
    label "pot&#281;ga"
  ]
  node [
    id 43
    label "documentation"
  ]
  node [
    id 44
    label "przedmiot"
  ]
  node [
    id 45
    label "column"
  ]
  node [
    id 46
    label "zasadzi&#263;"
  ]
  node [
    id 47
    label "za&#322;o&#380;enie"
  ]
  node [
    id 48
    label "punkt_odniesienia"
  ]
  node [
    id 49
    label "zasadzenie"
  ]
  node [
    id 50
    label "bok"
  ]
  node [
    id 51
    label "d&#243;&#322;"
  ]
  node [
    id 52
    label "dzieci&#281;ctwo"
  ]
  node [
    id 53
    label "background"
  ]
  node [
    id 54
    label "podstawowy"
  ]
  node [
    id 55
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 56
    label "strategia"
  ]
  node [
    id 57
    label "pomys&#322;"
  ]
  node [
    id 58
    label "&#347;ciana"
  ]
  node [
    id 59
    label "podwini&#281;cie"
  ]
  node [
    id 60
    label "zap&#322;acenie"
  ]
  node [
    id 61
    label "przyodzianie"
  ]
  node [
    id 62
    label "budowla"
  ]
  node [
    id 63
    label "pokrycie"
  ]
  node [
    id 64
    label "rozebranie"
  ]
  node [
    id 65
    label "zak&#322;adka"
  ]
  node [
    id 66
    label "struktura"
  ]
  node [
    id 67
    label "poubieranie"
  ]
  node [
    id 68
    label "infliction"
  ]
  node [
    id 69
    label "spowodowanie"
  ]
  node [
    id 70
    label "pozak&#322;adanie"
  ]
  node [
    id 71
    label "program"
  ]
  node [
    id 72
    label "przebranie"
  ]
  node [
    id 73
    label "przywdzianie"
  ]
  node [
    id 74
    label "obleczenie_si&#281;"
  ]
  node [
    id 75
    label "utworzenie"
  ]
  node [
    id 76
    label "str&#243;j"
  ]
  node [
    id 77
    label "twierdzenie"
  ]
  node [
    id 78
    label "obleczenie"
  ]
  node [
    id 79
    label "umieszczenie"
  ]
  node [
    id 80
    label "czynno&#347;&#263;"
  ]
  node [
    id 81
    label "przygotowywanie"
  ]
  node [
    id 82
    label "przymierzenie"
  ]
  node [
    id 83
    label "wyko&#324;czenie"
  ]
  node [
    id 84
    label "point"
  ]
  node [
    id 85
    label "przygotowanie"
  ]
  node [
    id 86
    label "proposition"
  ]
  node [
    id 87
    label "przewidzenie"
  ]
  node [
    id 88
    label "zrobienie"
  ]
  node [
    id 89
    label "profil"
  ]
  node [
    id 90
    label "zbocze"
  ]
  node [
    id 91
    label "kszta&#322;t"
  ]
  node [
    id 92
    label "p&#322;aszczyzna"
  ]
  node [
    id 93
    label "przegroda"
  ]
  node [
    id 94
    label "bariera"
  ]
  node [
    id 95
    label "kres"
  ]
  node [
    id 96
    label "facebook"
  ]
  node [
    id 97
    label "wielo&#347;cian"
  ]
  node [
    id 98
    label "obstruction"
  ]
  node [
    id 99
    label "pow&#322;oka"
  ]
  node [
    id 100
    label "wyrobisko"
  ]
  node [
    id 101
    label "miejsce"
  ]
  node [
    id 102
    label "trudno&#347;&#263;"
  ]
  node [
    id 103
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 104
    label "tu&#322;&#243;w"
  ]
  node [
    id 105
    label "kierunek"
  ]
  node [
    id 106
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 107
    label "wielok&#261;t"
  ]
  node [
    id 108
    label "odcinek"
  ]
  node [
    id 109
    label "strzelba"
  ]
  node [
    id 110
    label "lufa"
  ]
  node [
    id 111
    label "strona"
  ]
  node [
    id 112
    label "zboczenie"
  ]
  node [
    id 113
    label "om&#243;wienie"
  ]
  node [
    id 114
    label "sponiewieranie"
  ]
  node [
    id 115
    label "discipline"
  ]
  node [
    id 116
    label "rzecz"
  ]
  node [
    id 117
    label "omawia&#263;"
  ]
  node [
    id 118
    label "kr&#261;&#380;enie"
  ]
  node [
    id 119
    label "tre&#347;&#263;"
  ]
  node [
    id 120
    label "robienie"
  ]
  node [
    id 121
    label "sponiewiera&#263;"
  ]
  node [
    id 122
    label "element"
  ]
  node [
    id 123
    label "entity"
  ]
  node [
    id 124
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 125
    label "tematyka"
  ]
  node [
    id 126
    label "w&#261;tek"
  ]
  node [
    id 127
    label "charakter"
  ]
  node [
    id 128
    label "zbaczanie"
  ]
  node [
    id 129
    label "program_nauczania"
  ]
  node [
    id 130
    label "om&#243;wi&#263;"
  ]
  node [
    id 131
    label "omawianie"
  ]
  node [
    id 132
    label "thing"
  ]
  node [
    id 133
    label "kultura"
  ]
  node [
    id 134
    label "istota"
  ]
  node [
    id 135
    label "zbacza&#263;"
  ]
  node [
    id 136
    label "zboczy&#263;"
  ]
  node [
    id 137
    label "wykopywa&#263;"
  ]
  node [
    id 138
    label "wykopanie"
  ]
  node [
    id 139
    label "&#347;piew"
  ]
  node [
    id 140
    label "wykopywanie"
  ]
  node [
    id 141
    label "hole"
  ]
  node [
    id 142
    label "low"
  ]
  node [
    id 143
    label "niski"
  ]
  node [
    id 144
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 145
    label "depressive_disorder"
  ]
  node [
    id 146
    label "d&#378;wi&#281;k"
  ]
  node [
    id 147
    label "wykopa&#263;"
  ]
  node [
    id 148
    label "za&#322;amanie"
  ]
  node [
    id 149
    label "niezaawansowany"
  ]
  node [
    id 150
    label "najwa&#380;niejszy"
  ]
  node [
    id 151
    label "pocz&#261;tkowy"
  ]
  node [
    id 152
    label "podstawowo"
  ]
  node [
    id 153
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 154
    label "establish"
  ]
  node [
    id 155
    label "plant"
  ]
  node [
    id 156
    label "osnowa&#263;"
  ]
  node [
    id 157
    label "przymocowa&#263;"
  ]
  node [
    id 158
    label "umie&#347;ci&#263;"
  ]
  node [
    id 159
    label "wetkn&#261;&#263;"
  ]
  node [
    id 160
    label "wetkni&#281;cie"
  ]
  node [
    id 161
    label "przetkanie"
  ]
  node [
    id 162
    label "anchor"
  ]
  node [
    id 163
    label "przymocowanie"
  ]
  node [
    id 164
    label "zaczerpni&#281;cie"
  ]
  node [
    id 165
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 166
    label "interposition"
  ]
  node [
    id 167
    label "odm&#322;odzenie"
  ]
  node [
    id 168
    label "dzieci&#324;stwo"
  ]
  node [
    id 169
    label "pocz&#261;tki"
  ]
  node [
    id 170
    label "pochodzenie"
  ]
  node [
    id 171
    label "kontekst"
  ]
  node [
    id 172
    label "idea"
  ]
  node [
    id 173
    label "wytw&#243;r"
  ]
  node [
    id 174
    label "ukradzenie"
  ]
  node [
    id 175
    label "ukra&#347;&#263;"
  ]
  node [
    id 176
    label "system"
  ]
  node [
    id 177
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 178
    label "plan"
  ]
  node [
    id 179
    label "operacja"
  ]
  node [
    id 180
    label "metoda"
  ]
  node [
    id 181
    label "gra"
  ]
  node [
    id 182
    label "wzorzec_projektowy"
  ]
  node [
    id 183
    label "dziedzina"
  ]
  node [
    id 184
    label "doktryna"
  ]
  node [
    id 185
    label "wrinkle"
  ]
  node [
    id 186
    label "dokument"
  ]
  node [
    id 187
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 188
    label "wojsko"
  ]
  node [
    id 189
    label "wyk&#322;adnik_pot&#281;gi"
  ]
  node [
    id 190
    label "organizacja"
  ]
  node [
    id 191
    label "violence"
  ]
  node [
    id 192
    label "podstawa_pot&#281;gi"
  ]
  node [
    id 193
    label "zdolno&#347;&#263;"
  ]
  node [
    id 194
    label "potencja"
  ]
  node [
    id 195
    label "iloczyn"
  ]
  node [
    id 196
    label "dzi&#243;b"
  ]
  node [
    id 197
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 198
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 199
    label "zacinanie"
  ]
  node [
    id 200
    label "ssa&#263;"
  ]
  node [
    id 201
    label "organ"
  ]
  node [
    id 202
    label "zacina&#263;"
  ]
  node [
    id 203
    label "zaci&#261;&#263;"
  ]
  node [
    id 204
    label "ssanie"
  ]
  node [
    id 205
    label "jama_ustna"
  ]
  node [
    id 206
    label "jadaczka"
  ]
  node [
    id 207
    label "zaci&#281;cie"
  ]
  node [
    id 208
    label "warga_dolna"
  ]
  node [
    id 209
    label "twarz"
  ]
  node [
    id 210
    label "warga_g&#243;rna"
  ]
  node [
    id 211
    label "ryjek"
  ]
  node [
    id 212
    label "tkanka"
  ]
  node [
    id 213
    label "jednostka_organizacyjna"
  ]
  node [
    id 214
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 215
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 216
    label "tw&#243;r"
  ]
  node [
    id 217
    label "organogeneza"
  ]
  node [
    id 218
    label "zesp&#243;&#322;"
  ]
  node [
    id 219
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 220
    label "struktura_anatomiczna"
  ]
  node [
    id 221
    label "uk&#322;ad"
  ]
  node [
    id 222
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 223
    label "dekortykacja"
  ]
  node [
    id 224
    label "Izba_Konsyliarska"
  ]
  node [
    id 225
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 226
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 227
    label "stomia"
  ]
  node [
    id 228
    label "budowa"
  ]
  node [
    id 229
    label "okolica"
  ]
  node [
    id 230
    label "Komitet_Region&#243;w"
  ]
  node [
    id 231
    label "ptak"
  ]
  node [
    id 232
    label "grzebie&#324;"
  ]
  node [
    id 233
    label "bow"
  ]
  node [
    id 234
    label "statek"
  ]
  node [
    id 235
    label "ustnik"
  ]
  node [
    id 236
    label "sto&#380;ek_dziobowy"
  ]
  node [
    id 237
    label "samolot"
  ]
  node [
    id 238
    label "zako&#324;czenie"
  ]
  node [
    id 239
    label "ostry"
  ]
  node [
    id 240
    label "blizna"
  ]
  node [
    id 241
    label "dziob&#243;wka"
  ]
  node [
    id 242
    label "cz&#322;owiek"
  ]
  node [
    id 243
    label "cera"
  ]
  node [
    id 244
    label "wielko&#347;&#263;"
  ]
  node [
    id 245
    label "rys"
  ]
  node [
    id 246
    label "przedstawiciel"
  ]
  node [
    id 247
    label "p&#322;e&#263;"
  ]
  node [
    id 248
    label "posta&#263;"
  ]
  node [
    id 249
    label "zas&#322;ona"
  ]
  node [
    id 250
    label "p&#243;&#322;profil"
  ]
  node [
    id 251
    label "policzek"
  ]
  node [
    id 252
    label "brew"
  ]
  node [
    id 253
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 254
    label "uj&#281;cie"
  ]
  node [
    id 255
    label "micha"
  ]
  node [
    id 256
    label "reputacja"
  ]
  node [
    id 257
    label "wyraz_twarzy"
  ]
  node [
    id 258
    label "powieka"
  ]
  node [
    id 259
    label "czo&#322;o"
  ]
  node [
    id 260
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 261
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 262
    label "twarzyczka"
  ]
  node [
    id 263
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 264
    label "ucho"
  ]
  node [
    id 265
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 266
    label "prz&#243;d"
  ]
  node [
    id 267
    label "oko"
  ]
  node [
    id 268
    label "nos"
  ]
  node [
    id 269
    label "podbr&#243;dek"
  ]
  node [
    id 270
    label "liczko"
  ]
  node [
    id 271
    label "pysk"
  ]
  node [
    id 272
    label "maskowato&#347;&#263;"
  ]
  node [
    id 273
    label "poderwa&#263;"
  ]
  node [
    id 274
    label "ko&#324;"
  ]
  node [
    id 275
    label "&#347;cisn&#261;&#263;"
  ]
  node [
    id 276
    label "ostruga&#263;"
  ]
  node [
    id 277
    label "nadci&#261;&#263;"
  ]
  node [
    id 278
    label "bat"
  ]
  node [
    id 279
    label "uderzy&#263;"
  ]
  node [
    id 280
    label "zrani&#263;"
  ]
  node [
    id 281
    label "w&#281;dka"
  ]
  node [
    id 282
    label "lejce"
  ]
  node [
    id 283
    label "wprawi&#263;"
  ]
  node [
    id 284
    label "cut"
  ]
  node [
    id 285
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 286
    label "przerwa&#263;"
  ]
  node [
    id 287
    label "ch&#322;osn&#261;&#263;"
  ]
  node [
    id 288
    label "zepsu&#263;"
  ]
  node [
    id 289
    label "naci&#261;&#263;"
  ]
  node [
    id 290
    label "odebra&#263;"
  ]
  node [
    id 291
    label "zablokowa&#263;"
  ]
  node [
    id 292
    label "write_out"
  ]
  node [
    id 293
    label "padanie"
  ]
  node [
    id 294
    label "mina"
  ]
  node [
    id 295
    label "w&#281;dkowanie"
  ]
  node [
    id 296
    label "kaleczenie"
  ]
  node [
    id 297
    label "nacinanie"
  ]
  node [
    id 298
    label "podrywanie"
  ]
  node [
    id 299
    label "powo&#380;enie"
  ]
  node [
    id 300
    label "zaciskanie"
  ]
  node [
    id 301
    label "struganie"
  ]
  node [
    id 302
    label "ch&#322;ostanie"
  ]
  node [
    id 303
    label "&#347;cina&#263;"
  ]
  node [
    id 304
    label "robi&#263;"
  ]
  node [
    id 305
    label "zaciera&#263;"
  ]
  node [
    id 306
    label "przerywa&#263;"
  ]
  node [
    id 307
    label "psu&#263;"
  ]
  node [
    id 308
    label "zaciska&#263;"
  ]
  node [
    id 309
    label "odbiera&#263;"
  ]
  node [
    id 310
    label "zakrawa&#263;"
  ]
  node [
    id 311
    label "wprawia&#263;"
  ]
  node [
    id 312
    label "podrywa&#263;"
  ]
  node [
    id 313
    label "hack"
  ]
  node [
    id 314
    label "reduce"
  ]
  node [
    id 315
    label "przestawa&#263;"
  ]
  node [
    id 316
    label "blokowa&#263;"
  ]
  node [
    id 317
    label "nacina&#263;"
  ]
  node [
    id 318
    label "pocina&#263;"
  ]
  node [
    id 319
    label "uderza&#263;"
  ]
  node [
    id 320
    label "ch&#322;osta&#263;"
  ]
  node [
    id 321
    label "kaleczy&#263;"
  ]
  node [
    id 322
    label "struga&#263;"
  ]
  node [
    id 323
    label "ch&#322;o&#347;ni&#281;cie"
  ]
  node [
    id 324
    label "&#347;ci&#347;ni&#281;cie"
  ]
  node [
    id 325
    label "naci&#281;cie"
  ]
  node [
    id 326
    label "zaci&#281;ty"
  ]
  node [
    id 327
    label "poderwanie"
  ]
  node [
    id 328
    label "talent"
  ]
  node [
    id 329
    label "go"
  ]
  node [
    id 330
    label "capability"
  ]
  node [
    id 331
    label "stanowczo"
  ]
  node [
    id 332
    label "ostruganie"
  ]
  node [
    id 333
    label "formacja_skalna"
  ]
  node [
    id 334
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 335
    label "potkni&#281;cie"
  ]
  node [
    id 336
    label "zranienie"
  ]
  node [
    id 337
    label "turn"
  ]
  node [
    id 338
    label "dash"
  ]
  node [
    id 339
    label "uwa&#380;nie"
  ]
  node [
    id 340
    label "nieust&#281;pliwie"
  ]
  node [
    id 341
    label "&#347;lina"
  ]
  node [
    id 342
    label "pi&#263;"
  ]
  node [
    id 343
    label "sponge"
  ]
  node [
    id 344
    label "mleko"
  ]
  node [
    id 345
    label "rozpuszcza&#263;"
  ]
  node [
    id 346
    label "wci&#261;ga&#263;"
  ]
  node [
    id 347
    label "j&#281;zyk"
  ]
  node [
    id 348
    label "rusza&#263;"
  ]
  node [
    id 349
    label "sucking"
  ]
  node [
    id 350
    label "smoczek"
  ]
  node [
    id 351
    label "narz&#261;d_g&#281;bowy"
  ]
  node [
    id 352
    label "picie"
  ]
  node [
    id 353
    label "ruszanie"
  ]
  node [
    id 354
    label "consumption"
  ]
  node [
    id 355
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 356
    label "rozpuszczanie"
  ]
  node [
    id 357
    label "aspiration"
  ]
  node [
    id 358
    label "wci&#261;ganie"
  ]
  node [
    id 359
    label "odci&#261;ganie"
  ]
  node [
    id 360
    label "wessanie"
  ]
  node [
    id 361
    label "ga&#378;nik"
  ]
  node [
    id 362
    label "mechanizm"
  ]
  node [
    id 363
    label "wysysanie"
  ]
  node [
    id 364
    label "wyssanie"
  ]
  node [
    id 365
    label "Karta_Nauczyciela"
  ]
  node [
    id 366
    label "przej&#347;cie"
  ]
  node [
    id 367
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 368
    label "akt"
  ]
  node [
    id 369
    label "przej&#347;&#263;"
  ]
  node [
    id 370
    label "charter"
  ]
  node [
    id 371
    label "marc&#243;wka"
  ]
  node [
    id 372
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 373
    label "podnieci&#263;"
  ]
  node [
    id 374
    label "scena"
  ]
  node [
    id 375
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 376
    label "numer"
  ]
  node [
    id 377
    label "po&#380;ycie"
  ]
  node [
    id 378
    label "poj&#281;cie"
  ]
  node [
    id 379
    label "podniecenie"
  ]
  node [
    id 380
    label "nago&#347;&#263;"
  ]
  node [
    id 381
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 382
    label "fascyku&#322;"
  ]
  node [
    id 383
    label "seks"
  ]
  node [
    id 384
    label "podniecanie"
  ]
  node [
    id 385
    label "imisja"
  ]
  node [
    id 386
    label "zwyczaj"
  ]
  node [
    id 387
    label "rozmna&#380;anie"
  ]
  node [
    id 388
    label "ruch_frykcyjny"
  ]
  node [
    id 389
    label "ontologia"
  ]
  node [
    id 390
    label "wydarzenie"
  ]
  node [
    id 391
    label "na_pieska"
  ]
  node [
    id 392
    label "pozycja_misjonarska"
  ]
  node [
    id 393
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 394
    label "fragment"
  ]
  node [
    id 395
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 396
    label "z&#322;&#261;czenie"
  ]
  node [
    id 397
    label "gra_wst&#281;pna"
  ]
  node [
    id 398
    label "erotyka"
  ]
  node [
    id 399
    label "urzeczywistnienie"
  ]
  node [
    id 400
    label "baraszki"
  ]
  node [
    id 401
    label "certificate"
  ]
  node [
    id 402
    label "po&#380;&#261;danie"
  ]
  node [
    id 403
    label "wzw&#243;d"
  ]
  node [
    id 404
    label "funkcja"
  ]
  node [
    id 405
    label "act"
  ]
  node [
    id 406
    label "arystotelizm"
  ]
  node [
    id 407
    label "podnieca&#263;"
  ]
  node [
    id 408
    label "zabory"
  ]
  node [
    id 409
    label "ci&#281;&#380;arna"
  ]
  node [
    id 410
    label "rozwi&#261;zanie"
  ]
  node [
    id 411
    label "mini&#281;cie"
  ]
  node [
    id 412
    label "wymienienie"
  ]
  node [
    id 413
    label "zaliczenie"
  ]
  node [
    id 414
    label "traversal"
  ]
  node [
    id 415
    label "zdarzenie_si&#281;"
  ]
  node [
    id 416
    label "przewy&#380;szenie"
  ]
  node [
    id 417
    label "experience"
  ]
  node [
    id 418
    label "przepuszczenie"
  ]
  node [
    id 419
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 420
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 421
    label "strain"
  ]
  node [
    id 422
    label "faza"
  ]
  node [
    id 423
    label "przerobienie"
  ]
  node [
    id 424
    label "wydeptywanie"
  ]
  node [
    id 425
    label "crack"
  ]
  node [
    id 426
    label "wydeptanie"
  ]
  node [
    id 427
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 428
    label "wstawka"
  ]
  node [
    id 429
    label "prze&#380;ycie"
  ]
  node [
    id 430
    label "uznanie"
  ]
  node [
    id 431
    label "doznanie"
  ]
  node [
    id 432
    label "dostanie_si&#281;"
  ]
  node [
    id 433
    label "trwanie"
  ]
  node [
    id 434
    label "przebycie"
  ]
  node [
    id 435
    label "wytyczenie"
  ]
  node [
    id 436
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 437
    label "przepojenie"
  ]
  node [
    id 438
    label "nas&#261;czenie"
  ]
  node [
    id 439
    label "nale&#380;enie"
  ]
  node [
    id 440
    label "mienie"
  ]
  node [
    id 441
    label "odmienienie"
  ]
  node [
    id 442
    label "przedostanie_si&#281;"
  ]
  node [
    id 443
    label "przemokni&#281;cie"
  ]
  node [
    id 444
    label "nasycenie_si&#281;"
  ]
  node [
    id 445
    label "zacz&#281;cie"
  ]
  node [
    id 446
    label "stanie_si&#281;"
  ]
  node [
    id 447
    label "offense"
  ]
  node [
    id 448
    label "przestanie"
  ]
  node [
    id 449
    label "podlec"
  ]
  node [
    id 450
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 451
    label "min&#261;&#263;"
  ]
  node [
    id 452
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 453
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 454
    label "zaliczy&#263;"
  ]
  node [
    id 455
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 456
    label "zmieni&#263;"
  ]
  node [
    id 457
    label "przeby&#263;"
  ]
  node [
    id 458
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 459
    label "die"
  ]
  node [
    id 460
    label "dozna&#263;"
  ]
  node [
    id 461
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 462
    label "zacz&#261;&#263;"
  ]
  node [
    id 463
    label "happen"
  ]
  node [
    id 464
    label "pass"
  ]
  node [
    id 465
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 466
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 467
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 468
    label "beat"
  ]
  node [
    id 469
    label "absorb"
  ]
  node [
    id 470
    label "przerobi&#263;"
  ]
  node [
    id 471
    label "pique"
  ]
  node [
    id 472
    label "przesta&#263;"
  ]
  node [
    id 473
    label "odnaj&#281;cie"
  ]
  node [
    id 474
    label "naj&#281;cie"
  ]
  node [
    id 475
    label "ranek"
  ]
  node [
    id 476
    label "doba"
  ]
  node [
    id 477
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 478
    label "noc"
  ]
  node [
    id 479
    label "podwiecz&#243;r"
  ]
  node [
    id 480
    label "po&#322;udnie"
  ]
  node [
    id 481
    label "godzina"
  ]
  node [
    id 482
    label "przedpo&#322;udnie"
  ]
  node [
    id 483
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 484
    label "long_time"
  ]
  node [
    id 485
    label "wiecz&#243;r"
  ]
  node [
    id 486
    label "t&#322;usty_czwartek"
  ]
  node [
    id 487
    label "popo&#322;udnie"
  ]
  node [
    id 488
    label "walentynki"
  ]
  node [
    id 489
    label "czynienie_si&#281;"
  ]
  node [
    id 490
    label "s&#322;o&#324;ce"
  ]
  node [
    id 491
    label "rano"
  ]
  node [
    id 492
    label "tydzie&#324;"
  ]
  node [
    id 493
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 494
    label "wzej&#347;cie"
  ]
  node [
    id 495
    label "czas"
  ]
  node [
    id 496
    label "wsta&#263;"
  ]
  node [
    id 497
    label "day"
  ]
  node [
    id 498
    label "termin"
  ]
  node [
    id 499
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 500
    label "wstanie"
  ]
  node [
    id 501
    label "przedwiecz&#243;r"
  ]
  node [
    id 502
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 503
    label "Sylwester"
  ]
  node [
    id 504
    label "poprzedzanie"
  ]
  node [
    id 505
    label "czasoprzestrze&#324;"
  ]
  node [
    id 506
    label "laba"
  ]
  node [
    id 507
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 508
    label "chronometria"
  ]
  node [
    id 509
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 510
    label "rachuba_czasu"
  ]
  node [
    id 511
    label "przep&#322;ywanie"
  ]
  node [
    id 512
    label "czasokres"
  ]
  node [
    id 513
    label "odczyt"
  ]
  node [
    id 514
    label "chwila"
  ]
  node [
    id 515
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 516
    label "dzieje"
  ]
  node [
    id 517
    label "kategoria_gramatyczna"
  ]
  node [
    id 518
    label "poprzedzenie"
  ]
  node [
    id 519
    label "trawienie"
  ]
  node [
    id 520
    label "pochodzi&#263;"
  ]
  node [
    id 521
    label "period"
  ]
  node [
    id 522
    label "okres_czasu"
  ]
  node [
    id 523
    label "poprzedza&#263;"
  ]
  node [
    id 524
    label "schy&#322;ek"
  ]
  node [
    id 525
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 526
    label "odwlekanie_si&#281;"
  ]
  node [
    id 527
    label "zegar"
  ]
  node [
    id 528
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 529
    label "czwarty_wymiar"
  ]
  node [
    id 530
    label "koniugacja"
  ]
  node [
    id 531
    label "Zeitgeist"
  ]
  node [
    id 532
    label "trawi&#263;"
  ]
  node [
    id 533
    label "pogoda"
  ]
  node [
    id 534
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 535
    label "poprzedzi&#263;"
  ]
  node [
    id 536
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 537
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 538
    label "time_period"
  ]
  node [
    id 539
    label "nazewnictwo"
  ]
  node [
    id 540
    label "term"
  ]
  node [
    id 541
    label "przypadni&#281;cie"
  ]
  node [
    id 542
    label "ekspiracja"
  ]
  node [
    id 543
    label "przypa&#347;&#263;"
  ]
  node [
    id 544
    label "chronogram"
  ]
  node [
    id 545
    label "praktyka"
  ]
  node [
    id 546
    label "nazwa"
  ]
  node [
    id 547
    label "odwieczerz"
  ]
  node [
    id 548
    label "pora"
  ]
  node [
    id 549
    label "przyj&#281;cie"
  ]
  node [
    id 550
    label "spotkanie"
  ]
  node [
    id 551
    label "night"
  ]
  node [
    id 552
    label "zach&#243;d"
  ]
  node [
    id 553
    label "vesper"
  ]
  node [
    id 554
    label "aurora"
  ]
  node [
    id 555
    label "wsch&#243;d"
  ]
  node [
    id 556
    label "zjawisko"
  ]
  node [
    id 557
    label "&#347;rodek"
  ]
  node [
    id 558
    label "obszar"
  ]
  node [
    id 559
    label "Ziemia"
  ]
  node [
    id 560
    label "dwunasta"
  ]
  node [
    id 561
    label "strona_&#347;wiata"
  ]
  node [
    id 562
    label "dopo&#322;udnie"
  ]
  node [
    id 563
    label "blady_&#347;wit"
  ]
  node [
    id 564
    label "podkurek"
  ]
  node [
    id 565
    label "time"
  ]
  node [
    id 566
    label "p&#243;&#322;godzina"
  ]
  node [
    id 567
    label "jednostka_czasu"
  ]
  node [
    id 568
    label "minuta"
  ]
  node [
    id 569
    label "kwadrans"
  ]
  node [
    id 570
    label "p&#243;&#322;noc"
  ]
  node [
    id 571
    label "nokturn"
  ]
  node [
    id 572
    label "jednostka_geologiczna"
  ]
  node [
    id 573
    label "weekend"
  ]
  node [
    id 574
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 575
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 576
    label "miesi&#261;c"
  ]
  node [
    id 577
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 578
    label "mount"
  ]
  node [
    id 579
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 580
    label "wzej&#347;&#263;"
  ]
  node [
    id 581
    label "ascend"
  ]
  node [
    id 582
    label "kuca&#263;"
  ]
  node [
    id 583
    label "wyzdrowie&#263;"
  ]
  node [
    id 584
    label "opu&#347;ci&#263;"
  ]
  node [
    id 585
    label "rise"
  ]
  node [
    id 586
    label "arise"
  ]
  node [
    id 587
    label "stan&#261;&#263;"
  ]
  node [
    id 588
    label "wyzdrowienie"
  ]
  node [
    id 589
    label "le&#380;enie"
  ]
  node [
    id 590
    label "kl&#281;czenie"
  ]
  node [
    id 591
    label "opuszczenie"
  ]
  node [
    id 592
    label "uniesienie_si&#281;"
  ]
  node [
    id 593
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 594
    label "siedzenie"
  ]
  node [
    id 595
    label "beginning"
  ]
  node [
    id 596
    label "S&#322;o&#324;ce"
  ]
  node [
    id 597
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 598
    label "&#347;wiat&#322;o"
  ]
  node [
    id 599
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 600
    label "kochanie"
  ]
  node [
    id 601
    label "sunlight"
  ]
  node [
    id 602
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 603
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 604
    label "grudzie&#324;"
  ]
  node [
    id 605
    label "luty"
  ]
  node [
    id 606
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 607
    label "miech"
  ]
  node [
    id 608
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 609
    label "rok"
  ]
  node [
    id 610
    label "kalendy"
  ]
  node [
    id 611
    label "formacja"
  ]
  node [
    id 612
    label "yearbook"
  ]
  node [
    id 613
    label "czasopismo"
  ]
  node [
    id 614
    label "kronika"
  ]
  node [
    id 615
    label "Bund"
  ]
  node [
    id 616
    label "Mazowsze"
  ]
  node [
    id 617
    label "PPR"
  ]
  node [
    id 618
    label "Jakobici"
  ]
  node [
    id 619
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 620
    label "leksem"
  ]
  node [
    id 621
    label "SLD"
  ]
  node [
    id 622
    label "zespolik"
  ]
  node [
    id 623
    label "Razem"
  ]
  node [
    id 624
    label "PiS"
  ]
  node [
    id 625
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 626
    label "partia"
  ]
  node [
    id 627
    label "Kuomintang"
  ]
  node [
    id 628
    label "ZSL"
  ]
  node [
    id 629
    label "szko&#322;a"
  ]
  node [
    id 630
    label "jednostka"
  ]
  node [
    id 631
    label "proces"
  ]
  node [
    id 632
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 633
    label "rugby"
  ]
  node [
    id 634
    label "AWS"
  ]
  node [
    id 635
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 636
    label "blok"
  ]
  node [
    id 637
    label "PO"
  ]
  node [
    id 638
    label "si&#322;a"
  ]
  node [
    id 639
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 640
    label "Federali&#347;ci"
  ]
  node [
    id 641
    label "PSL"
  ]
  node [
    id 642
    label "Wigowie"
  ]
  node [
    id 643
    label "ZChN"
  ]
  node [
    id 644
    label "egzekutywa"
  ]
  node [
    id 645
    label "The_Beatles"
  ]
  node [
    id 646
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 647
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 648
    label "unit"
  ]
  node [
    id 649
    label "Depeche_Mode"
  ]
  node [
    id 650
    label "forma"
  ]
  node [
    id 651
    label "zapis"
  ]
  node [
    id 652
    label "chronograf"
  ]
  node [
    id 653
    label "latopis"
  ]
  node [
    id 654
    label "ksi&#281;ga"
  ]
  node [
    id 655
    label "egzemplarz"
  ]
  node [
    id 656
    label "psychotest"
  ]
  node [
    id 657
    label "pismo"
  ]
  node [
    id 658
    label "communication"
  ]
  node [
    id 659
    label "wk&#322;ad"
  ]
  node [
    id 660
    label "zajawka"
  ]
  node [
    id 661
    label "ok&#322;adka"
  ]
  node [
    id 662
    label "Zwrotnica"
  ]
  node [
    id 663
    label "dzia&#322;"
  ]
  node [
    id 664
    label "prasa"
  ]
  node [
    id 665
    label "czynienie_dobra"
  ]
  node [
    id 666
    label "zobowi&#261;zanie"
  ]
  node [
    id 667
    label "p&#322;acenie"
  ]
  node [
    id 668
    label "wyraz"
  ]
  node [
    id 669
    label "koszt_rodzajowy"
  ]
  node [
    id 670
    label "znaczenie"
  ]
  node [
    id 671
    label "service"
  ]
  node [
    id 672
    label "us&#322;uga"
  ]
  node [
    id 673
    label "przekonywanie"
  ]
  node [
    id 674
    label "sk&#322;adanie"
  ]
  node [
    id 675
    label "informowanie"
  ]
  node [
    id 676
    label "command"
  ]
  node [
    id 677
    label "performance"
  ]
  node [
    id 678
    label "pracowanie"
  ]
  node [
    id 679
    label "opowiadanie"
  ]
  node [
    id 680
    label "dawanie"
  ]
  node [
    id 681
    label "przyk&#322;adanie"
  ]
  node [
    id 682
    label "collection"
  ]
  node [
    id 683
    label "gromadzenie"
  ]
  node [
    id 684
    label "zestawianie"
  ]
  node [
    id 685
    label "opracowywanie"
  ]
  node [
    id 686
    label "m&#243;wienie"
  ]
  node [
    id 687
    label "gi&#281;cie"
  ]
  node [
    id 688
    label "follow-up"
  ]
  node [
    id 689
    label "wypowied&#378;"
  ]
  node [
    id 690
    label "report"
  ]
  node [
    id 691
    label "spalenie"
  ]
  node [
    id 692
    label "rozpowiedzenie"
  ]
  node [
    id 693
    label "podbarwianie"
  ]
  node [
    id 694
    label "przedstawianie"
  ]
  node [
    id 695
    label "story"
  ]
  node [
    id 696
    label "rozpowiadanie"
  ]
  node [
    id 697
    label "proza"
  ]
  node [
    id 698
    label "prawienie"
  ]
  node [
    id 699
    label "utw&#243;r_epicki"
  ]
  node [
    id 700
    label "fabu&#322;a"
  ]
  node [
    id 701
    label "wydawanie"
  ]
  node [
    id 702
    label "wage"
  ]
  node [
    id 703
    label "pay"
  ]
  node [
    id 704
    label "wykupywanie"
  ]
  node [
    id 705
    label "osi&#261;ganie"
  ]
  node [
    id 706
    label "przepracowanie_si&#281;"
  ]
  node [
    id 707
    label "zarz&#261;dzanie"
  ]
  node [
    id 708
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 709
    label "podlizanie_si&#281;"
  ]
  node [
    id 710
    label "dopracowanie"
  ]
  node [
    id 711
    label "podlizywanie_si&#281;"
  ]
  node [
    id 712
    label "uruchamianie"
  ]
  node [
    id 713
    label "dzia&#322;anie"
  ]
  node [
    id 714
    label "d&#261;&#380;enie"
  ]
  node [
    id 715
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 716
    label "uruchomienie"
  ]
  node [
    id 717
    label "nakr&#281;canie"
  ]
  node [
    id 718
    label "funkcjonowanie"
  ]
  node [
    id 719
    label "tr&#243;jstronny"
  ]
  node [
    id 720
    label "postaranie_si&#281;"
  ]
  node [
    id 721
    label "odpocz&#281;cie"
  ]
  node [
    id 722
    label "nakr&#281;cenie"
  ]
  node [
    id 723
    label "zatrzymanie"
  ]
  node [
    id 724
    label "spracowanie_si&#281;"
  ]
  node [
    id 725
    label "skakanie"
  ]
  node [
    id 726
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 727
    label "podtrzymywanie"
  ]
  node [
    id 728
    label "w&#322;&#261;czanie"
  ]
  node [
    id 729
    label "zaprz&#281;ganie"
  ]
  node [
    id 730
    label "podejmowanie"
  ]
  node [
    id 731
    label "maszyna"
  ]
  node [
    id 732
    label "wyrabianie"
  ]
  node [
    id 733
    label "dzianie_si&#281;"
  ]
  node [
    id 734
    label "use"
  ]
  node [
    id 735
    label "przepracowanie"
  ]
  node [
    id 736
    label "poruszanie_si&#281;"
  ]
  node [
    id 737
    label "impact"
  ]
  node [
    id 738
    label "przepracowywanie"
  ]
  node [
    id 739
    label "awansowanie"
  ]
  node [
    id 740
    label "courtship"
  ]
  node [
    id 741
    label "zapracowanie"
  ]
  node [
    id 742
    label "wyrobienie"
  ]
  node [
    id 743
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 744
    label "w&#322;&#261;czenie"
  ]
  node [
    id 745
    label "powiadanie"
  ]
  node [
    id 746
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 747
    label "komunikowanie"
  ]
  node [
    id 748
    label "orientowanie"
  ]
  node [
    id 749
    label "zorientowanie"
  ]
  node [
    id 750
    label "odk&#322;adanie"
  ]
  node [
    id 751
    label "condition"
  ]
  node [
    id 752
    label "liczenie"
  ]
  node [
    id 753
    label "stawianie"
  ]
  node [
    id 754
    label "bycie"
  ]
  node [
    id 755
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 756
    label "assay"
  ]
  node [
    id 757
    label "wskazywanie"
  ]
  node [
    id 758
    label "gravity"
  ]
  node [
    id 759
    label "weight"
  ]
  node [
    id 760
    label "odgrywanie_roli"
  ]
  node [
    id 761
    label "informacja"
  ]
  node [
    id 762
    label "cecha"
  ]
  node [
    id 763
    label "okre&#347;lanie"
  ]
  node [
    id 764
    label "kto&#347;"
  ]
  node [
    id 765
    label "wyra&#380;enie"
  ]
  node [
    id 766
    label "stosunek_prawny"
  ]
  node [
    id 767
    label "oblig"
  ]
  node [
    id 768
    label "uregulowa&#263;"
  ]
  node [
    id 769
    label "oddzia&#322;anie"
  ]
  node [
    id 770
    label "occupation"
  ]
  node [
    id 771
    label "duty"
  ]
  node [
    id 772
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 773
    label "zapowied&#378;"
  ]
  node [
    id 774
    label "obowi&#261;zek"
  ]
  node [
    id 775
    label "statement"
  ]
  node [
    id 776
    label "zapewnienie"
  ]
  node [
    id 777
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 778
    label "przedstawienie"
  ]
  node [
    id 779
    label "zachowanie"
  ]
  node [
    id 780
    label "produkt_gotowy"
  ]
  node [
    id 781
    label "asortyment"
  ]
  node [
    id 782
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 783
    label "oddzia&#322;ywanie"
  ]
  node [
    id 784
    label "sk&#322;anianie"
  ]
  node [
    id 785
    label "przekonywanie_si&#281;"
  ]
  node [
    id 786
    label "persuasion"
  ]
  node [
    id 787
    label "oznaka"
  ]
  node [
    id 788
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 789
    label "monetarny"
  ]
  node [
    id 790
    label "przypada&#263;"
  ]
  node [
    id 791
    label "by&#263;"
  ]
  node [
    id 792
    label "rzuca&#263;_si&#281;"
  ]
  node [
    id 793
    label "przywiera&#263;"
  ]
  node [
    id 794
    label "trafia&#263;_si&#281;"
  ]
  node [
    id 795
    label "fall"
  ]
  node [
    id 796
    label "podoba&#263;_si&#281;"
  ]
  node [
    id 797
    label "dociera&#263;"
  ]
  node [
    id 798
    label "wypada&#263;"
  ]
  node [
    id 799
    label "pada&#263;"
  ]
  node [
    id 800
    label "Chocho&#322;"
  ]
  node [
    id 801
    label "Herkules_Poirot"
  ]
  node [
    id 802
    label "Edyp"
  ]
  node [
    id 803
    label "ludzko&#347;&#263;"
  ]
  node [
    id 804
    label "parali&#380;owa&#263;"
  ]
  node [
    id 805
    label "Harry_Potter"
  ]
  node [
    id 806
    label "Casanova"
  ]
  node [
    id 807
    label "Zgredek"
  ]
  node [
    id 808
    label "Gargantua"
  ]
  node [
    id 809
    label "Winnetou"
  ]
  node [
    id 810
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 811
    label "Dulcynea"
  ]
  node [
    id 812
    label "g&#322;owa"
  ]
  node [
    id 813
    label "figura"
  ]
  node [
    id 814
    label "portrecista"
  ]
  node [
    id 815
    label "person"
  ]
  node [
    id 816
    label "Plastu&#347;"
  ]
  node [
    id 817
    label "Quasimodo"
  ]
  node [
    id 818
    label "Sherlock_Holmes"
  ]
  node [
    id 819
    label "Faust"
  ]
  node [
    id 820
    label "Wallenrod"
  ]
  node [
    id 821
    label "Dwukwiat"
  ]
  node [
    id 822
    label "Don_Juan"
  ]
  node [
    id 823
    label "profanum"
  ]
  node [
    id 824
    label "Don_Kiszot"
  ]
  node [
    id 825
    label "mikrokosmos"
  ]
  node [
    id 826
    label "duch"
  ]
  node [
    id 827
    label "antropochoria"
  ]
  node [
    id 828
    label "Hamlet"
  ]
  node [
    id 829
    label "Werter"
  ]
  node [
    id 830
    label "Szwejk"
  ]
  node [
    id 831
    label "homo_sapiens"
  ]
  node [
    id 832
    label "mentalno&#347;&#263;"
  ]
  node [
    id 833
    label "superego"
  ]
  node [
    id 834
    label "psychika"
  ]
  node [
    id 835
    label "wn&#281;trze"
  ]
  node [
    id 836
    label "charakterystyka"
  ]
  node [
    id 837
    label "zaistnie&#263;"
  ]
  node [
    id 838
    label "Osjan"
  ]
  node [
    id 839
    label "wygl&#261;d"
  ]
  node [
    id 840
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 841
    label "osobowo&#347;&#263;"
  ]
  node [
    id 842
    label "trim"
  ]
  node [
    id 843
    label "poby&#263;"
  ]
  node [
    id 844
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 845
    label "Aspazja"
  ]
  node [
    id 846
    label "punkt_widzenia"
  ]
  node [
    id 847
    label "kompleksja"
  ]
  node [
    id 848
    label "wytrzyma&#263;"
  ]
  node [
    id 849
    label "pozosta&#263;"
  ]
  node [
    id 850
    label "go&#347;&#263;"
  ]
  node [
    id 851
    label "hamper"
  ]
  node [
    id 852
    label "spasm"
  ]
  node [
    id 853
    label "mrozi&#263;"
  ]
  node [
    id 854
    label "pora&#380;a&#263;"
  ]
  node [
    id 855
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 856
    label "fleksja"
  ]
  node [
    id 857
    label "liczba"
  ]
  node [
    id 858
    label "coupling"
  ]
  node [
    id 859
    label "tryb"
  ]
  node [
    id 860
    label "czasownik"
  ]
  node [
    id 861
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 862
    label "orz&#281;sek"
  ]
  node [
    id 863
    label "fotograf"
  ]
  node [
    id 864
    label "malarz"
  ]
  node [
    id 865
    label "artysta"
  ]
  node [
    id 866
    label "powodowanie"
  ]
  node [
    id 867
    label "hipnotyzowanie"
  ]
  node [
    id 868
    label "&#347;lad"
  ]
  node [
    id 869
    label "docieranie"
  ]
  node [
    id 870
    label "natural_process"
  ]
  node [
    id 871
    label "reakcja_chemiczna"
  ]
  node [
    id 872
    label "wdzieranie_si&#281;"
  ]
  node [
    id 873
    label "rezultat"
  ]
  node [
    id 874
    label "lobbysta"
  ]
  node [
    id 875
    label "pryncypa&#322;"
  ]
  node [
    id 876
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 877
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 878
    label "wiedza"
  ]
  node [
    id 879
    label "kierowa&#263;"
  ]
  node [
    id 880
    label "alkohol"
  ]
  node [
    id 881
    label "&#380;ycie"
  ]
  node [
    id 882
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 883
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 884
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 885
    label "sztuka"
  ]
  node [
    id 886
    label "dekiel"
  ]
  node [
    id 887
    label "ro&#347;lina"
  ]
  node [
    id 888
    label "&#347;ci&#281;cie"
  ]
  node [
    id 889
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 890
    label "&#347;ci&#281;gno"
  ]
  node [
    id 891
    label "noosfera"
  ]
  node [
    id 892
    label "byd&#322;o"
  ]
  node [
    id 893
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 894
    label "makrocefalia"
  ]
  node [
    id 895
    label "obiekt"
  ]
  node [
    id 896
    label "g&#243;ra"
  ]
  node [
    id 897
    label "m&#243;zg"
  ]
  node [
    id 898
    label "kierownictwo"
  ]
  node [
    id 899
    label "fryzura"
  ]
  node [
    id 900
    label "umys&#322;"
  ]
  node [
    id 901
    label "cia&#322;o"
  ]
  node [
    id 902
    label "cz&#322;onek"
  ]
  node [
    id 903
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 904
    label "czaszka"
  ]
  node [
    id 905
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 906
    label "allochoria"
  ]
  node [
    id 907
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 908
    label "bierka_szachowa"
  ]
  node [
    id 909
    label "obiekt_matematyczny"
  ]
  node [
    id 910
    label "gestaltyzm"
  ]
  node [
    id 911
    label "styl"
  ]
  node [
    id 912
    label "obraz"
  ]
  node [
    id 913
    label "character"
  ]
  node [
    id 914
    label "rze&#378;ba"
  ]
  node [
    id 915
    label "stylistyka"
  ]
  node [
    id 916
    label "figure"
  ]
  node [
    id 917
    label "antycypacja"
  ]
  node [
    id 918
    label "ornamentyka"
  ]
  node [
    id 919
    label "facet"
  ]
  node [
    id 920
    label "popis"
  ]
  node [
    id 921
    label "wiersz"
  ]
  node [
    id 922
    label "symetria"
  ]
  node [
    id 923
    label "lingwistyka_kognitywna"
  ]
  node [
    id 924
    label "karta"
  ]
  node [
    id 925
    label "shape"
  ]
  node [
    id 926
    label "podzbi&#243;r"
  ]
  node [
    id 927
    label "perspektywa"
  ]
  node [
    id 928
    label "Szekspir"
  ]
  node [
    id 929
    label "Mickiewicz"
  ]
  node [
    id 930
    label "cierpienie"
  ]
  node [
    id 931
    label "piek&#322;o"
  ]
  node [
    id 932
    label "human_body"
  ]
  node [
    id 933
    label "ofiarowywanie"
  ]
  node [
    id 934
    label "sfera_afektywna"
  ]
  node [
    id 935
    label "nekromancja"
  ]
  node [
    id 936
    label "Po&#347;wist"
  ]
  node [
    id 937
    label "podekscytowanie"
  ]
  node [
    id 938
    label "deformowanie"
  ]
  node [
    id 939
    label "sumienie"
  ]
  node [
    id 940
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 941
    label "deformowa&#263;"
  ]
  node [
    id 942
    label "zjawa"
  ]
  node [
    id 943
    label "zmar&#322;y"
  ]
  node [
    id 944
    label "istota_nadprzyrodzona"
  ]
  node [
    id 945
    label "power"
  ]
  node [
    id 946
    label "ofiarowywa&#263;"
  ]
  node [
    id 947
    label "oddech"
  ]
  node [
    id 948
    label "seksualno&#347;&#263;"
  ]
  node [
    id 949
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 950
    label "byt"
  ]
  node [
    id 951
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 952
    label "ego"
  ]
  node [
    id 953
    label "ofiarowanie"
  ]
  node [
    id 954
    label "fizjonomia"
  ]
  node [
    id 955
    label "kompleks"
  ]
  node [
    id 956
    label "zapalno&#347;&#263;"
  ]
  node [
    id 957
    label "T&#281;sknica"
  ]
  node [
    id 958
    label "ofiarowa&#263;"
  ]
  node [
    id 959
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 960
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 961
    label "passion"
  ]
  node [
    id 962
    label "atom"
  ]
  node [
    id 963
    label "odbicie"
  ]
  node [
    id 964
    label "przyroda"
  ]
  node [
    id 965
    label "kosmos"
  ]
  node [
    id 966
    label "miniatura"
  ]
  node [
    id 967
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 968
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 969
    label "najem"
  ]
  node [
    id 970
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 971
    label "zak&#322;ad"
  ]
  node [
    id 972
    label "stosunek_pracy"
  ]
  node [
    id 973
    label "benedykty&#324;ski"
  ]
  node [
    id 974
    label "poda&#380;_pracy"
  ]
  node [
    id 975
    label "tyrka"
  ]
  node [
    id 976
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 977
    label "zaw&#243;d"
  ]
  node [
    id 978
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 979
    label "tynkarski"
  ]
  node [
    id 980
    label "pracowa&#263;"
  ]
  node [
    id 981
    label "zmiana"
  ]
  node [
    id 982
    label "czynnik_produkcji"
  ]
  node [
    id 983
    label "siedziba"
  ]
  node [
    id 984
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 985
    label "p&#322;&#243;d"
  ]
  node [
    id 986
    label "work"
  ]
  node [
    id 987
    label "activity"
  ]
  node [
    id 988
    label "bezproblemowy"
  ]
  node [
    id 989
    label "warunek_lokalowy"
  ]
  node [
    id 990
    label "plac"
  ]
  node [
    id 991
    label "location"
  ]
  node [
    id 992
    label "uwaga"
  ]
  node [
    id 993
    label "przestrze&#324;"
  ]
  node [
    id 994
    label "status"
  ]
  node [
    id 995
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 996
    label "rz&#261;d"
  ]
  node [
    id 997
    label "miejsce_pracy"
  ]
  node [
    id 998
    label "&#321;ubianka"
  ]
  node [
    id 999
    label "dzia&#322;_personalny"
  ]
  node [
    id 1000
    label "Kreml"
  ]
  node [
    id 1001
    label "Bia&#322;y_Dom"
  ]
  node [
    id 1002
    label "budynek"
  ]
  node [
    id 1003
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 1004
    label "sadowisko"
  ]
  node [
    id 1005
    label "instytucja"
  ]
  node [
    id 1006
    label "firma"
  ]
  node [
    id 1007
    label "czyn"
  ]
  node [
    id 1008
    label "company"
  ]
  node [
    id 1009
    label "instytut"
  ]
  node [
    id 1010
    label "umowa"
  ]
  node [
    id 1011
    label "cierpliwy"
  ]
  node [
    id 1012
    label "mozolny"
  ]
  node [
    id 1013
    label "wytrwa&#322;y"
  ]
  node [
    id 1014
    label "benedykty&#324;sko"
  ]
  node [
    id 1015
    label "typowy"
  ]
  node [
    id 1016
    label "po_benedykty&#324;sku"
  ]
  node [
    id 1017
    label "rewizja"
  ]
  node [
    id 1018
    label "passage"
  ]
  node [
    id 1019
    label "change"
  ]
  node [
    id 1020
    label "ferment"
  ]
  node [
    id 1021
    label "komplet"
  ]
  node [
    id 1022
    label "anatomopatolog"
  ]
  node [
    id 1023
    label "zmianka"
  ]
  node [
    id 1024
    label "amendment"
  ]
  node [
    id 1025
    label "odmienianie"
  ]
  node [
    id 1026
    label "tura"
  ]
  node [
    id 1027
    label "zawodoznawstwo"
  ]
  node [
    id 1028
    label "emocja"
  ]
  node [
    id 1029
    label "office"
  ]
  node [
    id 1030
    label "kwalifikacje"
  ]
  node [
    id 1031
    label "craft"
  ]
  node [
    id 1032
    label "transakcja"
  ]
  node [
    id 1033
    label "endeavor"
  ]
  node [
    id 1034
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1035
    label "mie&#263;_miejsce"
  ]
  node [
    id 1036
    label "podejmowa&#263;"
  ]
  node [
    id 1037
    label "dziama&#263;"
  ]
  node [
    id 1038
    label "do"
  ]
  node [
    id 1039
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 1040
    label "bangla&#263;"
  ]
  node [
    id 1041
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 1042
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1043
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1044
    label "funkcjonowa&#263;"
  ]
  node [
    id 1045
    label "biuro"
  ]
  node [
    id 1046
    label "lead"
  ]
  node [
    id 1047
    label "w&#322;adza"
  ]
  node [
    id 1048
    label "konieczny"
  ]
  node [
    id 1049
    label "poniewolny"
  ]
  node [
    id 1050
    label "przymusowo"
  ]
  node [
    id 1051
    label "niezb&#281;dnie"
  ]
  node [
    id 1052
    label "mimowolny"
  ]
  node [
    id 1053
    label "bezwolny"
  ]
  node [
    id 1054
    label "bezwiedny"
  ]
  node [
    id 1055
    label "Paneuropa"
  ]
  node [
    id 1056
    label "confederation"
  ]
  node [
    id 1057
    label "podob&#243;z"
  ]
  node [
    id 1058
    label "namiot"
  ]
  node [
    id 1059
    label "grupa"
  ]
  node [
    id 1060
    label "obozowisko"
  ]
  node [
    id 1061
    label "schronienie"
  ]
  node [
    id 1062
    label "odpoczynek"
  ]
  node [
    id 1063
    label "ONZ"
  ]
  node [
    id 1064
    label "NATO"
  ]
  node [
    id 1065
    label "alianci"
  ]
  node [
    id 1066
    label "miejsce_odosobnienia"
  ]
  node [
    id 1067
    label "osada"
  ]
  node [
    id 1068
    label "rozrywka"
  ]
  node [
    id 1069
    label "stan"
  ]
  node [
    id 1070
    label "wyraj"
  ]
  node [
    id 1071
    label "wczas"
  ]
  node [
    id 1072
    label "diversion"
  ]
  node [
    id 1073
    label "bajt"
  ]
  node [
    id 1074
    label "bloking"
  ]
  node [
    id 1075
    label "j&#261;kanie"
  ]
  node [
    id 1076
    label "przeszkoda"
  ]
  node [
    id 1077
    label "blokada"
  ]
  node [
    id 1078
    label "bry&#322;a"
  ]
  node [
    id 1079
    label "kontynent"
  ]
  node [
    id 1080
    label "nastawnia"
  ]
  node [
    id 1081
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1082
    label "blockage"
  ]
  node [
    id 1083
    label "zbi&#243;r"
  ]
  node [
    id 1084
    label "block"
  ]
  node [
    id 1085
    label "start"
  ]
  node [
    id 1086
    label "skorupa_ziemska"
  ]
  node [
    id 1087
    label "zeszyt"
  ]
  node [
    id 1088
    label "blokowisko"
  ]
  node [
    id 1089
    label "artyku&#322;"
  ]
  node [
    id 1090
    label "barak"
  ]
  node [
    id 1091
    label "stok_kontynentalny"
  ]
  node [
    id 1092
    label "whole"
  ]
  node [
    id 1093
    label "square"
  ]
  node [
    id 1094
    label "siatk&#243;wka"
  ]
  node [
    id 1095
    label "kr&#261;g"
  ]
  node [
    id 1096
    label "ram&#243;wka"
  ]
  node [
    id 1097
    label "zamek"
  ]
  node [
    id 1098
    label "obrona"
  ]
  node [
    id 1099
    label "bie&#380;nia"
  ]
  node [
    id 1100
    label "referat"
  ]
  node [
    id 1101
    label "dom_wielorodzinny"
  ]
  node [
    id 1102
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 1103
    label "odwadnia&#263;"
  ]
  node [
    id 1104
    label "wi&#261;zanie"
  ]
  node [
    id 1105
    label "odwodni&#263;"
  ]
  node [
    id 1106
    label "bratnia_dusza"
  ]
  node [
    id 1107
    label "powi&#261;zanie"
  ]
  node [
    id 1108
    label "zwi&#261;zanie"
  ]
  node [
    id 1109
    label "konstytucja"
  ]
  node [
    id 1110
    label "marriage"
  ]
  node [
    id 1111
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1112
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1113
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1114
    label "zwi&#261;za&#263;"
  ]
  node [
    id 1115
    label "odwadnianie"
  ]
  node [
    id 1116
    label "odwodnienie"
  ]
  node [
    id 1117
    label "marketing_afiliacyjny"
  ]
  node [
    id 1118
    label "substancja_chemiczna"
  ]
  node [
    id 1119
    label "koligacja"
  ]
  node [
    id 1120
    label "bearing"
  ]
  node [
    id 1121
    label "lokant"
  ]
  node [
    id 1122
    label "azeotrop"
  ]
  node [
    id 1123
    label "bezpieczny"
  ]
  node [
    id 1124
    label "ukryty"
  ]
  node [
    id 1125
    label "cover"
  ]
  node [
    id 1126
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 1127
    label "ukrycie"
  ]
  node [
    id 1128
    label "odm&#322;adzanie"
  ]
  node [
    id 1129
    label "liga"
  ]
  node [
    id 1130
    label "jednostka_systematyczna"
  ]
  node [
    id 1131
    label "asymilowanie"
  ]
  node [
    id 1132
    label "gromada"
  ]
  node [
    id 1133
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 1134
    label "asymilowa&#263;"
  ]
  node [
    id 1135
    label "Entuzjastki"
  ]
  node [
    id 1136
    label "kompozycja"
  ]
  node [
    id 1137
    label "Terranie"
  ]
  node [
    id 1138
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1139
    label "category"
  ]
  node [
    id 1140
    label "pakiet_klimatyczny"
  ]
  node [
    id 1141
    label "oddzia&#322;"
  ]
  node [
    id 1142
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1143
    label "cz&#261;steczka"
  ]
  node [
    id 1144
    label "stage_set"
  ]
  node [
    id 1145
    label "type"
  ]
  node [
    id 1146
    label "specgrupa"
  ]
  node [
    id 1147
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1148
    label "&#346;wietliki"
  ]
  node [
    id 1149
    label "Eurogrupa"
  ]
  node [
    id 1150
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1151
    label "formacja_geologiczna"
  ]
  node [
    id 1152
    label "harcerze_starsi"
  ]
  node [
    id 1153
    label "North_Atlantic_Treaty_Organization"
  ]
  node [
    id 1154
    label "b&#322;&#281;kitne_he&#322;my"
  ]
  node [
    id 1155
    label "misja_weryfikacyjna"
  ]
  node [
    id 1156
    label "WIPO"
  ]
  node [
    id 1157
    label "United_Nations"
  ]
  node [
    id 1158
    label "demofobia"
  ]
  node [
    id 1159
    label "najazd"
  ]
  node [
    id 1160
    label "t&#322;um"
  ]
  node [
    id 1161
    label "fobia"
  ]
  node [
    id 1162
    label "inpouring"
  ]
  node [
    id 1163
    label "przybycie"
  ]
  node [
    id 1164
    label "rapt"
  ]
  node [
    id 1165
    label "narciarstwo"
  ]
  node [
    id 1166
    label "skocznia"
  ]
  node [
    id 1167
    label "nieoczekiwany"
  ]
  node [
    id 1168
    label "potop_szwedzki"
  ]
  node [
    id 1169
    label "napad"
  ]
  node [
    id 1170
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 1171
    label "dehydration"
  ]
  node [
    id 1172
    label "osuszenie"
  ]
  node [
    id 1173
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 1174
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 1175
    label "odprowadzenie"
  ]
  node [
    id 1176
    label "odsuni&#281;cie"
  ]
  node [
    id 1177
    label "odsun&#261;&#263;"
  ]
  node [
    id 1178
    label "drain"
  ]
  node [
    id 1179
    label "spowodowa&#263;"
  ]
  node [
    id 1180
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 1181
    label "odprowadzi&#263;"
  ]
  node [
    id 1182
    label "osuszy&#263;"
  ]
  node [
    id 1183
    label "numeracja"
  ]
  node [
    id 1184
    label "odprowadza&#263;"
  ]
  node [
    id 1185
    label "powodowa&#263;"
  ]
  node [
    id 1186
    label "osusza&#263;"
  ]
  node [
    id 1187
    label "odci&#261;ga&#263;"
  ]
  node [
    id 1188
    label "odsuwa&#263;"
  ]
  node [
    id 1189
    label "cezar"
  ]
  node [
    id 1190
    label "uchwa&#322;a"
  ]
  node [
    id 1191
    label "odprowadzanie"
  ]
  node [
    id 1192
    label "dehydratacja"
  ]
  node [
    id 1193
    label "osuszanie"
  ]
  node [
    id 1194
    label "proces_chemiczny"
  ]
  node [
    id 1195
    label "odsuwanie"
  ]
  node [
    id 1196
    label "ograniczenie"
  ]
  node [
    id 1197
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1198
    label "do&#322;&#261;czenie"
  ]
  node [
    id 1199
    label "opakowanie"
  ]
  node [
    id 1200
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 1201
    label "attachment"
  ]
  node [
    id 1202
    label "obezw&#322;adnienie"
  ]
  node [
    id 1203
    label "zawi&#261;zanie"
  ]
  node [
    id 1204
    label "wi&#281;&#378;"
  ]
  node [
    id 1205
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 1206
    label "tying"
  ]
  node [
    id 1207
    label "st&#281;&#380;enie"
  ]
  node [
    id 1208
    label "affiliation"
  ]
  node [
    id 1209
    label "fastening"
  ]
  node [
    id 1210
    label "zaprawa"
  ]
  node [
    id 1211
    label "nawi&#261;zanie_si&#281;"
  ]
  node [
    id 1212
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 1213
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 1214
    label "w&#281;ze&#322;"
  ]
  node [
    id 1215
    label "consort"
  ]
  node [
    id 1216
    label "cement"
  ]
  node [
    id 1217
    label "opakowa&#263;"
  ]
  node [
    id 1218
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 1219
    label "relate"
  ]
  node [
    id 1220
    label "form"
  ]
  node [
    id 1221
    label "tobo&#322;ek"
  ]
  node [
    id 1222
    label "unify"
  ]
  node [
    id 1223
    label "incorporate"
  ]
  node [
    id 1224
    label "bind"
  ]
  node [
    id 1225
    label "zawi&#261;za&#263;"
  ]
  node [
    id 1226
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 1227
    label "powi&#261;za&#263;"
  ]
  node [
    id 1228
    label "scali&#263;"
  ]
  node [
    id 1229
    label "zatrzyma&#263;"
  ]
  node [
    id 1230
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1231
    label "narta"
  ]
  node [
    id 1232
    label "podwi&#261;zywanie"
  ]
  node [
    id 1233
    label "dressing"
  ]
  node [
    id 1234
    label "socket"
  ]
  node [
    id 1235
    label "szermierka"
  ]
  node [
    id 1236
    label "przywi&#261;zywanie"
  ]
  node [
    id 1237
    label "pakowanie"
  ]
  node [
    id 1238
    label "my&#347;lenie"
  ]
  node [
    id 1239
    label "do&#322;&#261;czanie"
  ]
  node [
    id 1240
    label "wytwarzanie"
  ]
  node [
    id 1241
    label "ceg&#322;a"
  ]
  node [
    id 1242
    label "combination"
  ]
  node [
    id 1243
    label "zobowi&#261;zywanie"
  ]
  node [
    id 1244
    label "szcz&#281;ka"
  ]
  node [
    id 1245
    label "anga&#380;owanie"
  ]
  node [
    id 1246
    label "wi&#261;za&#263;"
  ]
  node [
    id 1247
    label "twardnienie"
  ]
  node [
    id 1248
    label "podwi&#261;zanie"
  ]
  node [
    id 1249
    label "przywi&#261;zanie"
  ]
  node [
    id 1250
    label "przymocowywanie"
  ]
  node [
    id 1251
    label "scalanie"
  ]
  node [
    id 1252
    label "mezomeria"
  ]
  node [
    id 1253
    label "fusion"
  ]
  node [
    id 1254
    label "kojarzenie_si&#281;"
  ]
  node [
    id 1255
    label "&#322;&#261;czenie"
  ]
  node [
    id 1256
    label "uchwyt"
  ]
  node [
    id 1257
    label "rozmieszczenie"
  ]
  node [
    id 1258
    label "warto&#347;ciowo&#347;&#263;"
  ]
  node [
    id 1259
    label "element_konstrukcyjny"
  ]
  node [
    id 1260
    label "obezw&#322;adnianie"
  ]
  node [
    id 1261
    label "manewr"
  ]
  node [
    id 1262
    label "miecz"
  ]
  node [
    id 1263
    label "obwi&#261;zanie"
  ]
  node [
    id 1264
    label "zawi&#261;zek"
  ]
  node [
    id 1265
    label "obwi&#261;zywanie"
  ]
  node [
    id 1266
    label "roztw&#243;r"
  ]
  node [
    id 1267
    label "podmiot"
  ]
  node [
    id 1268
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1269
    label "TOPR"
  ]
  node [
    id 1270
    label "endecki"
  ]
  node [
    id 1271
    label "przedstawicielstwo"
  ]
  node [
    id 1272
    label "od&#322;am"
  ]
  node [
    id 1273
    label "Cepelia"
  ]
  node [
    id 1274
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1275
    label "ZBoWiD"
  ]
  node [
    id 1276
    label "organization"
  ]
  node [
    id 1277
    label "centrala"
  ]
  node [
    id 1278
    label "GOPR"
  ]
  node [
    id 1279
    label "ZOMO"
  ]
  node [
    id 1280
    label "ZMP"
  ]
  node [
    id 1281
    label "komitet_koordynacyjny"
  ]
  node [
    id 1282
    label "przybud&#243;wka"
  ]
  node [
    id 1283
    label "boj&#243;wka"
  ]
  node [
    id 1284
    label "zrelatywizowa&#263;"
  ]
  node [
    id 1285
    label "zrelatywizowanie"
  ]
  node [
    id 1286
    label "mention"
  ]
  node [
    id 1287
    label "pomy&#347;lenie"
  ]
  node [
    id 1288
    label "relatywizowa&#263;"
  ]
  node [
    id 1289
    label "relatywizowanie"
  ]
  node [
    id 1290
    label "kontakt"
  ]
  node [
    id 1291
    label "lewicowy"
  ]
  node [
    id 1292
    label "lewoskr&#281;tny"
  ]
  node [
    id 1293
    label "polityczny"
  ]
  node [
    id 1294
    label "lewicowo"
  ]
  node [
    id 1295
    label "lewy"
  ]
  node [
    id 1296
    label "Karelia"
  ]
  node [
    id 1297
    label "Ka&#322;mucja"
  ]
  node [
    id 1298
    label "Mari_El"
  ]
  node [
    id 1299
    label "Inguszetia"
  ]
  node [
    id 1300
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1301
    label "Udmurcja"
  ]
  node [
    id 1302
    label "Singapur"
  ]
  node [
    id 1303
    label "Ad&#380;aria"
  ]
  node [
    id 1304
    label "Karaka&#322;pacja"
  ]
  node [
    id 1305
    label "Czeczenia"
  ]
  node [
    id 1306
    label "Abchazja"
  ]
  node [
    id 1307
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 1308
    label "Tatarstan"
  ]
  node [
    id 1309
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1310
    label "pa&#324;stwo"
  ]
  node [
    id 1311
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 1312
    label "Baszkiria"
  ]
  node [
    id 1313
    label "Jakucja"
  ]
  node [
    id 1314
    label "Dagestan"
  ]
  node [
    id 1315
    label "Buriacja"
  ]
  node [
    id 1316
    label "Tuwa"
  ]
  node [
    id 1317
    label "ustr&#243;j"
  ]
  node [
    id 1318
    label "Komi"
  ]
  node [
    id 1319
    label "Czuwaszja"
  ]
  node [
    id 1320
    label "Chakasja"
  ]
  node [
    id 1321
    label "Nachiczewan"
  ]
  node [
    id 1322
    label "Mordowia"
  ]
  node [
    id 1323
    label "porz&#261;dek"
  ]
  node [
    id 1324
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1325
    label "Katar"
  ]
  node [
    id 1326
    label "Libia"
  ]
  node [
    id 1327
    label "Gwatemala"
  ]
  node [
    id 1328
    label "Ekwador"
  ]
  node [
    id 1329
    label "Afganistan"
  ]
  node [
    id 1330
    label "Tad&#380;ykistan"
  ]
  node [
    id 1331
    label "Bhutan"
  ]
  node [
    id 1332
    label "Argentyna"
  ]
  node [
    id 1333
    label "D&#380;ibuti"
  ]
  node [
    id 1334
    label "Wenezuela"
  ]
  node [
    id 1335
    label "Gabon"
  ]
  node [
    id 1336
    label "Ukraina"
  ]
  node [
    id 1337
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1338
    label "Rwanda"
  ]
  node [
    id 1339
    label "Liechtenstein"
  ]
  node [
    id 1340
    label "Sri_Lanka"
  ]
  node [
    id 1341
    label "Madagaskar"
  ]
  node [
    id 1342
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1343
    label "Kongo"
  ]
  node [
    id 1344
    label "Tonga"
  ]
  node [
    id 1345
    label "Bangladesz"
  ]
  node [
    id 1346
    label "Kanada"
  ]
  node [
    id 1347
    label "Wehrlen"
  ]
  node [
    id 1348
    label "Algieria"
  ]
  node [
    id 1349
    label "Uganda"
  ]
  node [
    id 1350
    label "Surinam"
  ]
  node [
    id 1351
    label "Sahara_Zachodnia"
  ]
  node [
    id 1352
    label "Chile"
  ]
  node [
    id 1353
    label "W&#281;gry"
  ]
  node [
    id 1354
    label "Birma"
  ]
  node [
    id 1355
    label "Kazachstan"
  ]
  node [
    id 1356
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1357
    label "Armenia"
  ]
  node [
    id 1358
    label "Tuwalu"
  ]
  node [
    id 1359
    label "Timor_Wschodni"
  ]
  node [
    id 1360
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1361
    label "Izrael"
  ]
  node [
    id 1362
    label "Estonia"
  ]
  node [
    id 1363
    label "Komory"
  ]
  node [
    id 1364
    label "Kamerun"
  ]
  node [
    id 1365
    label "Haiti"
  ]
  node [
    id 1366
    label "Belize"
  ]
  node [
    id 1367
    label "Sierra_Leone"
  ]
  node [
    id 1368
    label "Luksemburg"
  ]
  node [
    id 1369
    label "USA"
  ]
  node [
    id 1370
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1371
    label "Barbados"
  ]
  node [
    id 1372
    label "San_Marino"
  ]
  node [
    id 1373
    label "Bu&#322;garia"
  ]
  node [
    id 1374
    label "Indonezja"
  ]
  node [
    id 1375
    label "Wietnam"
  ]
  node [
    id 1376
    label "Malawi"
  ]
  node [
    id 1377
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1378
    label "Francja"
  ]
  node [
    id 1379
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1380
    label "Zambia"
  ]
  node [
    id 1381
    label "Angola"
  ]
  node [
    id 1382
    label "Grenada"
  ]
  node [
    id 1383
    label "Nepal"
  ]
  node [
    id 1384
    label "Panama"
  ]
  node [
    id 1385
    label "Rumunia"
  ]
  node [
    id 1386
    label "Czarnog&#243;ra"
  ]
  node [
    id 1387
    label "Malediwy"
  ]
  node [
    id 1388
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1389
    label "S&#322;owacja"
  ]
  node [
    id 1390
    label "para"
  ]
  node [
    id 1391
    label "Egipt"
  ]
  node [
    id 1392
    label "zwrot"
  ]
  node [
    id 1393
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1394
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1395
    label "Mozambik"
  ]
  node [
    id 1396
    label "Kolumbia"
  ]
  node [
    id 1397
    label "Laos"
  ]
  node [
    id 1398
    label "Burundi"
  ]
  node [
    id 1399
    label "Suazi"
  ]
  node [
    id 1400
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1401
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1402
    label "Czechy"
  ]
  node [
    id 1403
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1404
    label "Wyspy_Marshalla"
  ]
  node [
    id 1405
    label "Dominika"
  ]
  node [
    id 1406
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1407
    label "Syria"
  ]
  node [
    id 1408
    label "Palau"
  ]
  node [
    id 1409
    label "Gwinea_Bissau"
  ]
  node [
    id 1410
    label "Liberia"
  ]
  node [
    id 1411
    label "Jamajka"
  ]
  node [
    id 1412
    label "Zimbabwe"
  ]
  node [
    id 1413
    label "Polska"
  ]
  node [
    id 1414
    label "Dominikana"
  ]
  node [
    id 1415
    label "Senegal"
  ]
  node [
    id 1416
    label "Togo"
  ]
  node [
    id 1417
    label "Gujana"
  ]
  node [
    id 1418
    label "Gruzja"
  ]
  node [
    id 1419
    label "Albania"
  ]
  node [
    id 1420
    label "Zair"
  ]
  node [
    id 1421
    label "Meksyk"
  ]
  node [
    id 1422
    label "Macedonia"
  ]
  node [
    id 1423
    label "Chorwacja"
  ]
  node [
    id 1424
    label "Kambod&#380;a"
  ]
  node [
    id 1425
    label "Monako"
  ]
  node [
    id 1426
    label "Mauritius"
  ]
  node [
    id 1427
    label "Gwinea"
  ]
  node [
    id 1428
    label "Mali"
  ]
  node [
    id 1429
    label "Nigeria"
  ]
  node [
    id 1430
    label "Kostaryka"
  ]
  node [
    id 1431
    label "Hanower"
  ]
  node [
    id 1432
    label "Paragwaj"
  ]
  node [
    id 1433
    label "W&#322;ochy"
  ]
  node [
    id 1434
    label "Seszele"
  ]
  node [
    id 1435
    label "Wyspy_Salomona"
  ]
  node [
    id 1436
    label "Hiszpania"
  ]
  node [
    id 1437
    label "Boliwia"
  ]
  node [
    id 1438
    label "Kirgistan"
  ]
  node [
    id 1439
    label "Irlandia"
  ]
  node [
    id 1440
    label "Czad"
  ]
  node [
    id 1441
    label "Irak"
  ]
  node [
    id 1442
    label "Lesoto"
  ]
  node [
    id 1443
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1444
    label "Malta"
  ]
  node [
    id 1445
    label "Andora"
  ]
  node [
    id 1446
    label "Chiny"
  ]
  node [
    id 1447
    label "Filipiny"
  ]
  node [
    id 1448
    label "Antarktis"
  ]
  node [
    id 1449
    label "Niemcy"
  ]
  node [
    id 1450
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1451
    label "Pakistan"
  ]
  node [
    id 1452
    label "terytorium"
  ]
  node [
    id 1453
    label "Nikaragua"
  ]
  node [
    id 1454
    label "Brazylia"
  ]
  node [
    id 1455
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1456
    label "Maroko"
  ]
  node [
    id 1457
    label "Portugalia"
  ]
  node [
    id 1458
    label "Niger"
  ]
  node [
    id 1459
    label "Kenia"
  ]
  node [
    id 1460
    label "Botswana"
  ]
  node [
    id 1461
    label "Fid&#380;i"
  ]
  node [
    id 1462
    label "Tunezja"
  ]
  node [
    id 1463
    label "Australia"
  ]
  node [
    id 1464
    label "Tajlandia"
  ]
  node [
    id 1465
    label "Burkina_Faso"
  ]
  node [
    id 1466
    label "interior"
  ]
  node [
    id 1467
    label "Tanzania"
  ]
  node [
    id 1468
    label "Benin"
  ]
  node [
    id 1469
    label "Indie"
  ]
  node [
    id 1470
    label "&#321;otwa"
  ]
  node [
    id 1471
    label "Kiribati"
  ]
  node [
    id 1472
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1473
    label "Rodezja"
  ]
  node [
    id 1474
    label "Cypr"
  ]
  node [
    id 1475
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1476
    label "Peru"
  ]
  node [
    id 1477
    label "Austria"
  ]
  node [
    id 1478
    label "Urugwaj"
  ]
  node [
    id 1479
    label "Jordania"
  ]
  node [
    id 1480
    label "Grecja"
  ]
  node [
    id 1481
    label "Azerbejd&#380;an"
  ]
  node [
    id 1482
    label "Turcja"
  ]
  node [
    id 1483
    label "Samoa"
  ]
  node [
    id 1484
    label "Sudan"
  ]
  node [
    id 1485
    label "Oman"
  ]
  node [
    id 1486
    label "ziemia"
  ]
  node [
    id 1487
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1488
    label "Uzbekistan"
  ]
  node [
    id 1489
    label "Portoryko"
  ]
  node [
    id 1490
    label "Honduras"
  ]
  node [
    id 1491
    label "Mongolia"
  ]
  node [
    id 1492
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1493
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1494
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1495
    label "Serbia"
  ]
  node [
    id 1496
    label "Tajwan"
  ]
  node [
    id 1497
    label "Wielka_Brytania"
  ]
  node [
    id 1498
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1499
    label "Liban"
  ]
  node [
    id 1500
    label "Japonia"
  ]
  node [
    id 1501
    label "Ghana"
  ]
  node [
    id 1502
    label "Belgia"
  ]
  node [
    id 1503
    label "Bahrajn"
  ]
  node [
    id 1504
    label "Mikronezja"
  ]
  node [
    id 1505
    label "Etiopia"
  ]
  node [
    id 1506
    label "Kuwejt"
  ]
  node [
    id 1507
    label "Bahamy"
  ]
  node [
    id 1508
    label "Rosja"
  ]
  node [
    id 1509
    label "Mo&#322;dawia"
  ]
  node [
    id 1510
    label "Litwa"
  ]
  node [
    id 1511
    label "S&#322;owenia"
  ]
  node [
    id 1512
    label "Szwajcaria"
  ]
  node [
    id 1513
    label "Erytrea"
  ]
  node [
    id 1514
    label "Arabia_Saudyjska"
  ]
  node [
    id 1515
    label "Kuba"
  ]
  node [
    id 1516
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1517
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1518
    label "Malezja"
  ]
  node [
    id 1519
    label "Korea"
  ]
  node [
    id 1520
    label "Jemen"
  ]
  node [
    id 1521
    label "Nowa_Zelandia"
  ]
  node [
    id 1522
    label "Namibia"
  ]
  node [
    id 1523
    label "Nauru"
  ]
  node [
    id 1524
    label "holoarktyka"
  ]
  node [
    id 1525
    label "Brunei"
  ]
  node [
    id 1526
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1527
    label "Khitai"
  ]
  node [
    id 1528
    label "Mauretania"
  ]
  node [
    id 1529
    label "Iran"
  ]
  node [
    id 1530
    label "Gambia"
  ]
  node [
    id 1531
    label "Somalia"
  ]
  node [
    id 1532
    label "Holandia"
  ]
  node [
    id 1533
    label "Turkmenistan"
  ]
  node [
    id 1534
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1535
    label "Salwador"
  ]
  node [
    id 1536
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1537
    label "dolar_singapurski"
  ]
  node [
    id 1538
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1539
    label "Kaukaz"
  ]
  node [
    id 1540
    label "Federacja_Rosyjska"
  ]
  node [
    id 1541
    label "Wepska_Gmina_Narodowa"
  ]
  node [
    id 1542
    label "Zyrianka"
  ]
  node [
    id 1543
    label "Syberia_Wschodnia"
  ]
  node [
    id 1544
    label "Zabajkale"
  ]
  node [
    id 1545
    label "po_sowiecku"
  ]
  node [
    id 1546
    label "pozosta&#322;y"
  ]
  node [
    id 1547
    label "rosyjski"
  ]
  node [
    id 1548
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 1549
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 1550
    label "kacapski"
  ]
  node [
    id 1551
    label "po_rosyjsku"
  ]
  node [
    id 1552
    label "wielkoruski"
  ]
  node [
    id 1553
    label "Russian"
  ]
  node [
    id 1554
    label "rusek"
  ]
  node [
    id 1555
    label "inny"
  ]
  node [
    id 1556
    label "&#380;ywy"
  ]
  node [
    id 1557
    label "program_informacyjny"
  ]
  node [
    id 1558
    label "journal"
  ]
  node [
    id 1559
    label "diariusz"
  ]
  node [
    id 1560
    label "spis"
  ]
  node [
    id 1561
    label "sheet"
  ]
  node [
    id 1562
    label "pami&#281;tnik"
  ]
  node [
    id 1563
    label "gazeta"
  ]
  node [
    id 1564
    label "tytu&#322;"
  ]
  node [
    id 1565
    label "redakcja"
  ]
  node [
    id 1566
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 1567
    label "rozdzia&#322;"
  ]
  node [
    id 1568
    label "Ewangelia"
  ]
  node [
    id 1569
    label "book"
  ]
  node [
    id 1570
    label "tome"
  ]
  node [
    id 1571
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 1572
    label "pami&#261;tka"
  ]
  node [
    id 1573
    label "notes"
  ]
  node [
    id 1574
    label "zapiski"
  ]
  node [
    id 1575
    label "raptularz"
  ]
  node [
    id 1576
    label "album"
  ]
  node [
    id 1577
    label "catalog"
  ]
  node [
    id 1578
    label "pozycja"
  ]
  node [
    id 1579
    label "tekst"
  ]
  node [
    id 1580
    label "sumariusz"
  ]
  node [
    id 1581
    label "stock"
  ]
  node [
    id 1582
    label "figurowa&#263;"
  ]
  node [
    id 1583
    label "wyliczanka"
  ]
  node [
    id 1584
    label "poleca&#263;"
  ]
  node [
    id 1585
    label "control"
  ]
  node [
    id 1586
    label "decydowa&#263;"
  ]
  node [
    id 1587
    label "sprawowa&#263;"
  ]
  node [
    id 1588
    label "ordynowa&#263;"
  ]
  node [
    id 1589
    label "doradza&#263;"
  ]
  node [
    id 1590
    label "wydawa&#263;"
  ]
  node [
    id 1591
    label "m&#243;wi&#263;"
  ]
  node [
    id 1592
    label "charge"
  ]
  node [
    id 1593
    label "placard"
  ]
  node [
    id 1594
    label "powierza&#263;"
  ]
  node [
    id 1595
    label "zadawa&#263;"
  ]
  node [
    id 1596
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1597
    label "decide"
  ]
  node [
    id 1598
    label "klasyfikator"
  ]
  node [
    id 1599
    label "mean"
  ]
  node [
    id 1600
    label "prosecute"
  ]
  node [
    id 1601
    label "klawisz"
  ]
  node [
    id 1602
    label "naciska&#263;"
  ]
  node [
    id 1603
    label "atakowa&#263;"
  ]
  node [
    id 1604
    label "alternate"
  ]
  node [
    id 1605
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 1606
    label "chance"
  ]
  node [
    id 1607
    label "force"
  ]
  node [
    id 1608
    label "rush"
  ]
  node [
    id 1609
    label "crowd"
  ]
  node [
    id 1610
    label "napierdziela&#263;"
  ]
  node [
    id 1611
    label "przekonywa&#263;"
  ]
  node [
    id 1612
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1613
    label "strike"
  ]
  node [
    id 1614
    label "schorzenie"
  ]
  node [
    id 1615
    label "ofensywny"
  ]
  node [
    id 1616
    label "przewaga"
  ]
  node [
    id 1617
    label "sport"
  ]
  node [
    id 1618
    label "epidemia"
  ]
  node [
    id 1619
    label "attack"
  ]
  node [
    id 1620
    label "rozgrywa&#263;"
  ]
  node [
    id 1621
    label "krytykowa&#263;"
  ]
  node [
    id 1622
    label "walczy&#263;"
  ]
  node [
    id 1623
    label "aim"
  ]
  node [
    id 1624
    label "trouble_oneself"
  ]
  node [
    id 1625
    label "napada&#263;"
  ]
  node [
    id 1626
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1627
    label "usi&#322;owa&#263;"
  ]
  node [
    id 1628
    label "s&#261;d"
  ]
  node [
    id 1629
    label "kognicja"
  ]
  node [
    id 1630
    label "campaign"
  ]
  node [
    id 1631
    label "rozprawa"
  ]
  node [
    id 1632
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1633
    label "fashion"
  ]
  node [
    id 1634
    label "zmierzanie"
  ]
  node [
    id 1635
    label "przes&#322;anka"
  ]
  node [
    id 1636
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 1637
    label "kazanie"
  ]
  node [
    id 1638
    label "przebiec"
  ]
  node [
    id 1639
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1640
    label "motyw"
  ]
  node [
    id 1641
    label "przebiegni&#281;cie"
  ]
  node [
    id 1642
    label "dochodzenie"
  ]
  node [
    id 1643
    label "przychodzenie"
  ]
  node [
    id 1644
    label "wyprzedzenie"
  ]
  node [
    id 1645
    label "podchodzenie"
  ]
  node [
    id 1646
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 1647
    label "przyj&#347;cie"
  ]
  node [
    id 1648
    label "przemierzanie"
  ]
  node [
    id 1649
    label "udawanie_si&#281;"
  ]
  node [
    id 1650
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 1651
    label "odchodzenie"
  ]
  node [
    id 1652
    label "wyprzedzanie"
  ]
  node [
    id 1653
    label "spotykanie"
  ]
  node [
    id 1654
    label "podej&#347;cie"
  ]
  node [
    id 1655
    label "przemierzenie"
  ]
  node [
    id 1656
    label "wodzenie"
  ]
  node [
    id 1657
    label "fabrication"
  ]
  node [
    id 1658
    label "porobienie"
  ]
  node [
    id 1659
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 1660
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 1661
    label "creation"
  ]
  node [
    id 1662
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 1663
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 1664
    label "tentegowanie"
  ]
  node [
    id 1665
    label "reakcja"
  ]
  node [
    id 1666
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 1667
    label "tajemnica"
  ]
  node [
    id 1668
    label "spos&#243;b"
  ]
  node [
    id 1669
    label "pochowanie"
  ]
  node [
    id 1670
    label "zdyscyplinowanie"
  ]
  node [
    id 1671
    label "post&#261;pienie"
  ]
  node [
    id 1672
    label "post"
  ]
  node [
    id 1673
    label "zwierz&#281;"
  ]
  node [
    id 1674
    label "behawior"
  ]
  node [
    id 1675
    label "observation"
  ]
  node [
    id 1676
    label "dieta"
  ]
  node [
    id 1677
    label "podtrzymanie"
  ]
  node [
    id 1678
    label "etolog"
  ]
  node [
    id 1679
    label "przechowanie"
  ]
  node [
    id 1680
    label "rozumowanie"
  ]
  node [
    id 1681
    label "opracowanie"
  ]
  node [
    id 1682
    label "obrady"
  ]
  node [
    id 1683
    label "cytat"
  ]
  node [
    id 1684
    label "obja&#347;nienie"
  ]
  node [
    id 1685
    label "s&#261;dzenie"
  ]
  node [
    id 1686
    label "fakt"
  ]
  node [
    id 1687
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1688
    label "przyczyna"
  ]
  node [
    id 1689
    label "wnioskowanie"
  ]
  node [
    id 1690
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 1691
    label "podejrzany"
  ]
  node [
    id 1692
    label "s&#261;downictwo"
  ]
  node [
    id 1693
    label "court"
  ]
  node [
    id 1694
    label "forum"
  ]
  node [
    id 1695
    label "bronienie"
  ]
  node [
    id 1696
    label "urz&#261;d"
  ]
  node [
    id 1697
    label "oskar&#380;yciel"
  ]
  node [
    id 1698
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1699
    label "skazany"
  ]
  node [
    id 1700
    label "broni&#263;"
  ]
  node [
    id 1701
    label "my&#347;l"
  ]
  node [
    id 1702
    label "pods&#261;dny"
  ]
  node [
    id 1703
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1704
    label "antylogizm"
  ]
  node [
    id 1705
    label "konektyw"
  ]
  node [
    id 1706
    label "&#347;wiadek"
  ]
  node [
    id 1707
    label "procesowicz"
  ]
  node [
    id 1708
    label "nakazywanie"
  ]
  node [
    id 1709
    label "krytyka"
  ]
  node [
    id 1710
    label "wyg&#322;oszenie"
  ]
  node [
    id 1711
    label "zmuszanie"
  ]
  node [
    id 1712
    label "wyg&#322;aszanie"
  ]
  node [
    id 1713
    label "nakazanie"
  ]
  node [
    id 1714
    label "wymaganie"
  ]
  node [
    id 1715
    label "msza"
  ]
  node [
    id 1716
    label "zmuszenie"
  ]
  node [
    id 1717
    label "nauka"
  ]
  node [
    id 1718
    label "sermon"
  ]
  node [
    id 1719
    label "order"
  ]
  node [
    id 1720
    label "danie"
  ]
  node [
    id 1721
    label "confession"
  ]
  node [
    id 1722
    label "stwierdzenie"
  ]
  node [
    id 1723
    label "recognition"
  ]
  node [
    id 1724
    label "oznajmienie"
  ]
  node [
    id 1725
    label "obiecanie"
  ]
  node [
    id 1726
    label "cios"
  ]
  node [
    id 1727
    label "give"
  ]
  node [
    id 1728
    label "udost&#281;pnienie"
  ]
  node [
    id 1729
    label "rendition"
  ]
  node [
    id 1730
    label "wymienienie_si&#281;"
  ]
  node [
    id 1731
    label "eating"
  ]
  node [
    id 1732
    label "coup"
  ]
  node [
    id 1733
    label "hand"
  ]
  node [
    id 1734
    label "uprawianie_seksu"
  ]
  node [
    id 1735
    label "allow"
  ]
  node [
    id 1736
    label "dostarczenie"
  ]
  node [
    id 1737
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 1738
    label "uderzenie"
  ]
  node [
    id 1739
    label "zadanie"
  ]
  node [
    id 1740
    label "powierzenie"
  ]
  node [
    id 1741
    label "przeznaczenie"
  ]
  node [
    id 1742
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 1743
    label "przekazanie"
  ]
  node [
    id 1744
    label "odst&#261;pienie"
  ]
  node [
    id 1745
    label "dodanie"
  ]
  node [
    id 1746
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 1747
    label "wyposa&#380;enie"
  ]
  node [
    id 1748
    label "dostanie"
  ]
  node [
    id 1749
    label "potrawa"
  ]
  node [
    id 1750
    label "menu"
  ]
  node [
    id 1751
    label "uderzanie"
  ]
  node [
    id 1752
    label "wyst&#261;pienie"
  ]
  node [
    id 1753
    label "jedzenie"
  ]
  node [
    id 1754
    label "wyposa&#380;anie"
  ]
  node [
    id 1755
    label "pobicie"
  ]
  node [
    id 1756
    label "posi&#322;ek"
  ]
  node [
    id 1757
    label "urz&#261;dzenie"
  ]
  node [
    id 1758
    label "ustalenie"
  ]
  node [
    id 1759
    label "claim"
  ]
  node [
    id 1760
    label "wypowiedzenie"
  ]
  node [
    id 1761
    label "manifesto"
  ]
  node [
    id 1762
    label "zwiastowanie"
  ]
  node [
    id 1763
    label "announcement"
  ]
  node [
    id 1764
    label "apel"
  ]
  node [
    id 1765
    label "Manifest_lipcowy"
  ]
  node [
    id 1766
    label "poinformowanie"
  ]
  node [
    id 1767
    label "gramatyka"
  ]
  node [
    id 1768
    label "kod"
  ]
  node [
    id 1769
    label "przet&#322;umaczenie"
  ]
  node [
    id 1770
    label "konsonantyzm"
  ]
  node [
    id 1771
    label "wokalizm"
  ]
  node [
    id 1772
    label "fonetyka"
  ]
  node [
    id 1773
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1774
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1775
    label "po_koroniarsku"
  ]
  node [
    id 1776
    label "t&#322;umaczenie"
  ]
  node [
    id 1777
    label "rozumie&#263;"
  ]
  node [
    id 1778
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1779
    label "rozumienie"
  ]
  node [
    id 1780
    label "address"
  ]
  node [
    id 1781
    label "komunikacja"
  ]
  node [
    id 1782
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1783
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1784
    label "s&#322;ownictwo"
  ]
  node [
    id 1785
    label "tongue"
  ]
  node [
    id 1786
    label "pos&#322;uchanie"
  ]
  node [
    id 1787
    label "sparafrazowanie"
  ]
  node [
    id 1788
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 1789
    label "strawestowa&#263;"
  ]
  node [
    id 1790
    label "sparafrazowa&#263;"
  ]
  node [
    id 1791
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 1792
    label "trawestowa&#263;"
  ]
  node [
    id 1793
    label "sformu&#322;owanie"
  ]
  node [
    id 1794
    label "parafrazowanie"
  ]
  node [
    id 1795
    label "ozdobnik"
  ]
  node [
    id 1796
    label "delimitacja"
  ]
  node [
    id 1797
    label "parafrazowa&#263;"
  ]
  node [
    id 1798
    label "stylizacja"
  ]
  node [
    id 1799
    label "komunikat"
  ]
  node [
    id 1800
    label "trawestowanie"
  ]
  node [
    id 1801
    label "strawestowanie"
  ]
  node [
    id 1802
    label "transportation_system"
  ]
  node [
    id 1803
    label "explicite"
  ]
  node [
    id 1804
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 1805
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 1806
    label "implicite"
  ]
  node [
    id 1807
    label "ekspedytor"
  ]
  node [
    id 1808
    label "posiada&#263;"
  ]
  node [
    id 1809
    label "potencja&#322;"
  ]
  node [
    id 1810
    label "zapomina&#263;"
  ]
  node [
    id 1811
    label "zapomnienie"
  ]
  node [
    id 1812
    label "zapominanie"
  ]
  node [
    id 1813
    label "ability"
  ]
  node [
    id 1814
    label "obliczeniowo"
  ]
  node [
    id 1815
    label "zapomnie&#263;"
  ]
  node [
    id 1816
    label "language"
  ]
  node [
    id 1817
    label "code"
  ]
  node [
    id 1818
    label "szyfrowanie"
  ]
  node [
    id 1819
    label "ci&#261;g"
  ]
  node [
    id 1820
    label "szablon"
  ]
  node [
    id 1821
    label "public_speaking"
  ]
  node [
    id 1822
    label "przepowiadanie"
  ]
  node [
    id 1823
    label "wyznawanie"
  ]
  node [
    id 1824
    label "wypowiadanie"
  ]
  node [
    id 1825
    label "wydobywanie"
  ]
  node [
    id 1826
    label "gaworzenie"
  ]
  node [
    id 1827
    label "stosowanie"
  ]
  node [
    id 1828
    label "wyra&#380;anie"
  ]
  node [
    id 1829
    label "formu&#322;owanie"
  ]
  node [
    id 1830
    label "dowalenie"
  ]
  node [
    id 1831
    label "przerywanie"
  ]
  node [
    id 1832
    label "dogadywanie_si&#281;"
  ]
  node [
    id 1833
    label "dodawanie"
  ]
  node [
    id 1834
    label "ozywanie_si&#281;"
  ]
  node [
    id 1835
    label "zapeszanie"
  ]
  node [
    id 1836
    label "zwracanie_si&#281;"
  ]
  node [
    id 1837
    label "dysfonia"
  ]
  node [
    id 1838
    label "speaking"
  ]
  node [
    id 1839
    label "zauwa&#380;enie"
  ]
  node [
    id 1840
    label "mawianie"
  ]
  node [
    id 1841
    label "opowiedzenie"
  ]
  node [
    id 1842
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 1843
    label "dogadanie_si&#281;"
  ]
  node [
    id 1844
    label "wygadanie"
  ]
  node [
    id 1845
    label "terminology"
  ]
  node [
    id 1846
    label "g&#322;osownia"
  ]
  node [
    id 1847
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 1848
    label "zasymilowa&#263;"
  ]
  node [
    id 1849
    label "phonetics"
  ]
  node [
    id 1850
    label "palatogram"
  ]
  node [
    id 1851
    label "transkrypcja"
  ]
  node [
    id 1852
    label "zasymilowanie"
  ]
  node [
    id 1853
    label "handwriting"
  ]
  node [
    id 1854
    label "przekaz"
  ]
  node [
    id 1855
    label "dzie&#322;o"
  ]
  node [
    id 1856
    label "paleograf"
  ]
  node [
    id 1857
    label "interpunkcja"
  ]
  node [
    id 1858
    label "grafia"
  ]
  node [
    id 1859
    label "script"
  ]
  node [
    id 1860
    label "list"
  ]
  node [
    id 1861
    label "adres"
  ]
  node [
    id 1862
    label "ortografia"
  ]
  node [
    id 1863
    label "letter"
  ]
  node [
    id 1864
    label "paleografia"
  ]
  node [
    id 1865
    label "sk&#322;adnia"
  ]
  node [
    id 1866
    label "morfologia"
  ]
  node [
    id 1867
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 1868
    label "explanation"
  ]
  node [
    id 1869
    label "remark"
  ]
  node [
    id 1870
    label "przek&#322;adanie"
  ]
  node [
    id 1871
    label "zrozumia&#322;y"
  ]
  node [
    id 1872
    label "uzasadnianie"
  ]
  node [
    id 1873
    label "rozwianie"
  ]
  node [
    id 1874
    label "rozwiewanie"
  ]
  node [
    id 1875
    label "gossip"
  ]
  node [
    id 1876
    label "kr&#281;ty"
  ]
  node [
    id 1877
    label "poja&#347;nia&#263;"
  ]
  node [
    id 1878
    label "u&#322;atwia&#263;"
  ]
  node [
    id 1879
    label "elaborate"
  ]
  node [
    id 1880
    label "suplikowa&#263;"
  ]
  node [
    id 1881
    label "przek&#322;ada&#263;"
  ]
  node [
    id 1882
    label "interpretowa&#263;"
  ]
  node [
    id 1883
    label "explain"
  ]
  node [
    id 1884
    label "przedstawia&#263;"
  ]
  node [
    id 1885
    label "uzasadnia&#263;"
  ]
  node [
    id 1886
    label "zinterpretowa&#263;"
  ]
  node [
    id 1887
    label "put"
  ]
  node [
    id 1888
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 1889
    label "zrobi&#263;"
  ]
  node [
    id 1890
    label "przekona&#263;"
  ]
  node [
    id 1891
    label "frame"
  ]
  node [
    id 1892
    label "wiedzie&#263;"
  ]
  node [
    id 1893
    label "kuma&#263;"
  ]
  node [
    id 1894
    label "czu&#263;"
  ]
  node [
    id 1895
    label "match"
  ]
  node [
    id 1896
    label "empatia"
  ]
  node [
    id 1897
    label "see"
  ]
  node [
    id 1898
    label "zna&#263;"
  ]
  node [
    id 1899
    label "gaworzy&#263;"
  ]
  node [
    id 1900
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1901
    label "rozmawia&#263;"
  ]
  node [
    id 1902
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1903
    label "umie&#263;"
  ]
  node [
    id 1904
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 1905
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 1906
    label "formu&#322;owa&#263;"
  ]
  node [
    id 1907
    label "express"
  ]
  node [
    id 1908
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1909
    label "talk"
  ]
  node [
    id 1910
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1911
    label "prawi&#263;"
  ]
  node [
    id 1912
    label "powiada&#263;"
  ]
  node [
    id 1913
    label "tell"
  ]
  node [
    id 1914
    label "chew_the_fat"
  ]
  node [
    id 1915
    label "say"
  ]
  node [
    id 1916
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 1917
    label "informowa&#263;"
  ]
  node [
    id 1918
    label "wydobywa&#263;"
  ]
  node [
    id 1919
    label "okre&#347;la&#263;"
  ]
  node [
    id 1920
    label "hermeneutyka"
  ]
  node [
    id 1921
    label "apprehension"
  ]
  node [
    id 1922
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 1923
    label "interpretation"
  ]
  node [
    id 1924
    label "czucie"
  ]
  node [
    id 1925
    label "realization"
  ]
  node [
    id 1926
    label "kumanie"
  ]
  node [
    id 1927
    label "nazywa&#263;"
  ]
  node [
    id 1928
    label "mieni&#263;"
  ]
  node [
    id 1929
    label "nadawa&#263;"
  ]
  node [
    id 1930
    label "nisko"
  ]
  node [
    id 1931
    label "znacznie"
  ]
  node [
    id 1932
    label "het"
  ]
  node [
    id 1933
    label "dawno"
  ]
  node [
    id 1934
    label "daleki"
  ]
  node [
    id 1935
    label "g&#322;&#281;boko"
  ]
  node [
    id 1936
    label "nieobecnie"
  ]
  node [
    id 1937
    label "wysoko"
  ]
  node [
    id 1938
    label "du&#380;o"
  ]
  node [
    id 1939
    label "dawny"
  ]
  node [
    id 1940
    label "ogl&#281;dny"
  ]
  node [
    id 1941
    label "d&#322;ugi"
  ]
  node [
    id 1942
    label "du&#380;y"
  ]
  node [
    id 1943
    label "odleg&#322;y"
  ]
  node [
    id 1944
    label "zwi&#261;zany"
  ]
  node [
    id 1945
    label "r&#243;&#380;ny"
  ]
  node [
    id 1946
    label "s&#322;aby"
  ]
  node [
    id 1947
    label "odlegle"
  ]
  node [
    id 1948
    label "oddalony"
  ]
  node [
    id 1949
    label "g&#322;&#281;boki"
  ]
  node [
    id 1950
    label "obcy"
  ]
  node [
    id 1951
    label "nieobecny"
  ]
  node [
    id 1952
    label "przysz&#322;y"
  ]
  node [
    id 1953
    label "niepo&#347;lednio"
  ]
  node [
    id 1954
    label "wysoki"
  ]
  node [
    id 1955
    label "g&#243;rno"
  ]
  node [
    id 1956
    label "chwalebnie"
  ]
  node [
    id 1957
    label "wznio&#347;le"
  ]
  node [
    id 1958
    label "szczytny"
  ]
  node [
    id 1959
    label "d&#322;ugotrwale"
  ]
  node [
    id 1960
    label "wcze&#347;niej"
  ]
  node [
    id 1961
    label "ongi&#347;"
  ]
  node [
    id 1962
    label "dawnie"
  ]
  node [
    id 1963
    label "zamy&#347;lony"
  ]
  node [
    id 1964
    label "uni&#380;enie"
  ]
  node [
    id 1965
    label "pospolicie"
  ]
  node [
    id 1966
    label "blisko"
  ]
  node [
    id 1967
    label "wstydliwie"
  ]
  node [
    id 1968
    label "ma&#322;o"
  ]
  node [
    id 1969
    label "vilely"
  ]
  node [
    id 1970
    label "despicably"
  ]
  node [
    id 1971
    label "po&#347;lednio"
  ]
  node [
    id 1972
    label "ma&#322;y"
  ]
  node [
    id 1973
    label "mocno"
  ]
  node [
    id 1974
    label "gruntownie"
  ]
  node [
    id 1975
    label "szczerze"
  ]
  node [
    id 1976
    label "silnie"
  ]
  node [
    id 1977
    label "intensywnie"
  ]
  node [
    id 1978
    label "wiela"
  ]
  node [
    id 1979
    label "bardzo"
  ]
  node [
    id 1980
    label "cz&#281;sto"
  ]
  node [
    id 1981
    label "zauwa&#380;alnie"
  ]
  node [
    id 1982
    label "znaczny"
  ]
  node [
    id 1983
    label "begin"
  ]
  node [
    id 1984
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 1985
    label "pisemnie"
  ]
  node [
    id 1986
    label "pi&#347;mienny"
  ]
  node [
    id 1987
    label "wykszta&#322;cony"
  ]
  node [
    id 1988
    label "prayer"
  ]
  node [
    id 1989
    label "propozycja"
  ]
  node [
    id 1990
    label "motion"
  ]
  node [
    id 1991
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 1992
    label "alternatywa_Fredholma"
  ]
  node [
    id 1993
    label "oznajmianie"
  ]
  node [
    id 1994
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 1995
    label "teoria"
  ]
  node [
    id 1996
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 1997
    label "paradoks_Leontiefa"
  ]
  node [
    id 1998
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 1999
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 2000
    label "teza"
  ]
  node [
    id 2001
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 2002
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 2003
    label "twierdzenie_Pettisa"
  ]
  node [
    id 2004
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 2005
    label "twierdzenie_Maya"
  ]
  node [
    id 2006
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 2007
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 2008
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 2009
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 2010
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 2011
    label "zapewnianie"
  ]
  node [
    id 2012
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 2013
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 2014
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 2015
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 2016
    label "twierdzenie_Stokesa"
  ]
  node [
    id 2017
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 2018
    label "twierdzenie_Cevy"
  ]
  node [
    id 2019
    label "twierdzenie_Pascala"
  ]
  node [
    id 2020
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 2021
    label "zasada"
  ]
  node [
    id 2022
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 2023
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 2024
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 2025
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 2026
    label "thinking"
  ]
  node [
    id 2027
    label "political_orientation"
  ]
  node [
    id 2028
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 2029
    label "fantomatyka"
  ]
  node [
    id 2030
    label "proposal"
  ]
  node [
    id 2031
    label "proszenie"
  ]
  node [
    id 2032
    label "proces_my&#347;lowy"
  ]
  node [
    id 2033
    label "konkluzja"
  ]
  node [
    id 2034
    label "stwierdzi&#263;"
  ]
  node [
    id 2035
    label "testify"
  ]
  node [
    id 2036
    label "powiedzie&#263;"
  ]
  node [
    id 2037
    label "uzna&#263;"
  ]
  node [
    id 2038
    label "oznajmi&#263;"
  ]
  node [
    id 2039
    label "declare"
  ]
  node [
    id 2040
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 2041
    label "nale&#380;ny"
  ]
  node [
    id 2042
    label "nale&#380;yty"
  ]
  node [
    id 2043
    label "uprawniony"
  ]
  node [
    id 2044
    label "zasadniczy"
  ]
  node [
    id 2045
    label "stosownie"
  ]
  node [
    id 2046
    label "taki"
  ]
  node [
    id 2047
    label "charakterystyczny"
  ]
  node [
    id 2048
    label "prawdziwy"
  ]
  node [
    id 2049
    label "ten"
  ]
  node [
    id 2050
    label "dobry"
  ]
  node [
    id 2051
    label "powinny"
  ]
  node [
    id 2052
    label "nale&#380;nie"
  ]
  node [
    id 2053
    label "godny"
  ]
  node [
    id 2054
    label "przynale&#380;ny"
  ]
  node [
    id 2055
    label "zwyczajny"
  ]
  node [
    id 2056
    label "typowo"
  ]
  node [
    id 2057
    label "cz&#281;sty"
  ]
  node [
    id 2058
    label "zwyk&#322;y"
  ]
  node [
    id 2059
    label "charakterystycznie"
  ]
  node [
    id 2060
    label "szczeg&#243;lny"
  ]
  node [
    id 2061
    label "wyj&#261;tkowy"
  ]
  node [
    id 2062
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 2063
    label "podobny"
  ]
  node [
    id 2064
    label "&#380;ywny"
  ]
  node [
    id 2065
    label "szczery"
  ]
  node [
    id 2066
    label "naturalny"
  ]
  node [
    id 2067
    label "naprawd&#281;"
  ]
  node [
    id 2068
    label "realnie"
  ]
  node [
    id 2069
    label "zgodny"
  ]
  node [
    id 2070
    label "m&#261;dry"
  ]
  node [
    id 2071
    label "prawdziwie"
  ]
  node [
    id 2072
    label "w&#322;ady"
  ]
  node [
    id 2073
    label "g&#322;&#243;wny"
  ]
  node [
    id 2074
    label "og&#243;lny"
  ]
  node [
    id 2075
    label "zasadniczo"
  ]
  node [
    id 2076
    label "surowy"
  ]
  node [
    id 2077
    label "zadowalaj&#261;cy"
  ]
  node [
    id 2078
    label "nale&#380;ycie"
  ]
  node [
    id 2079
    label "przystojny"
  ]
  node [
    id 2080
    label "okre&#347;lony"
  ]
  node [
    id 2081
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 2082
    label "dobroczynny"
  ]
  node [
    id 2083
    label "czw&#243;rka"
  ]
  node [
    id 2084
    label "spokojny"
  ]
  node [
    id 2085
    label "skuteczny"
  ]
  node [
    id 2086
    label "&#347;mieszny"
  ]
  node [
    id 2087
    label "mi&#322;y"
  ]
  node [
    id 2088
    label "grzeczny"
  ]
  node [
    id 2089
    label "powitanie"
  ]
  node [
    id 2090
    label "dobrze"
  ]
  node [
    id 2091
    label "ca&#322;y"
  ]
  node [
    id 2092
    label "pomy&#347;lny"
  ]
  node [
    id 2093
    label "moralny"
  ]
  node [
    id 2094
    label "drogi"
  ]
  node [
    id 2095
    label "pozytywny"
  ]
  node [
    id 2096
    label "odpowiedni"
  ]
  node [
    id 2097
    label "korzystny"
  ]
  node [
    id 2098
    label "pos&#322;uszny"
  ]
  node [
    id 2099
    label "jaki&#347;"
  ]
  node [
    id 2100
    label "stosowny"
  ]
  node [
    id 2101
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 2102
    label "Chewra_Kadisza"
  ]
  node [
    id 2103
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 2104
    label "Rotary_International"
  ]
  node [
    id 2105
    label "fabianie"
  ]
  node [
    id 2106
    label "Eleusis"
  ]
  node [
    id 2107
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 2108
    label "Monar"
  ]
  node [
    id 2109
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 2110
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 2111
    label "G&#322;osk&#243;w"
  ]
  node [
    id 2112
    label "reedukator"
  ]
  node [
    id 2113
    label "harcerstwo"
  ]
  node [
    id 2114
    label "zwi&#261;zka"
  ]
  node [
    id 2115
    label "sprawi&#263;"
  ]
  node [
    id 2116
    label "kombatant"
  ]
  node [
    id 2117
    label "i"
  ]
  node [
    id 2118
    label "represjonowa&#263;"
  ]
  node [
    id 2119
    label "mi&#281;dzynarodowy"
  ]
  node [
    id 2120
    label "poszukiwanie"
  ]
  node [
    id 2121
    label "bad"
  ]
  node [
    id 2122
    label "Arolsen"
  ]
  node [
    id 2123
    label "czerwony"
  ]
  node [
    id 2124
    label "krzy&#380;"
  ]
  node [
    id 2125
    label "pami&#281;&#263;"
  ]
  node [
    id 2126
    label "narodowy"
  ]
  node [
    id 2127
    label "komisja"
  ]
  node [
    id 2128
    label "&#347;ciga&#263;"
  ]
  node [
    id 2129
    label "zbrodzie&#324;"
  ]
  node [
    id 2130
    label "przeciwko"
  ]
  node [
    id 2131
    label "nar&#243;d"
  ]
  node [
    id 2132
    label "polskie"
  ]
  node [
    id 2133
    label "centralny"
  ]
  node [
    id 2134
    label "archiwum"
  ]
  node [
    id 2135
    label "ministerstwo"
  ]
  node [
    id 2136
    label "wewn&#281;trzny"
  ]
  node [
    id 2137
    label "administracja"
  ]
  node [
    id 2138
    label "wschodni"
  ]
  node [
    id 2139
    label "fundacja"
  ]
  node [
    id 2140
    label "o&#347;rodek"
  ]
  node [
    id 2141
    label "Hoover"
  ]
  node [
    id 2142
    label "uniwersytet"
  ]
  node [
    id 2143
    label "wyspa"
  ]
  node [
    id 2144
    label "Stanford"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 606
  ]
  edge [
    source 5
    target 576
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 607
  ]
  edge [
    source 5
    target 608
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 609
  ]
  edge [
    source 5
    target 610
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 7
    target 702
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 704
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 706
  ]
  edge [
    source 7
    target 707
  ]
  edge [
    source 7
    target 708
  ]
  edge [
    source 7
    target 709
  ]
  edge [
    source 7
    target 710
  ]
  edge [
    source 7
    target 711
  ]
  edge [
    source 7
    target 712
  ]
  edge [
    source 7
    target 713
  ]
  edge [
    source 7
    target 714
  ]
  edge [
    source 7
    target 715
  ]
  edge [
    source 7
    target 716
  ]
  edge [
    source 7
    target 717
  ]
  edge [
    source 7
    target 718
  ]
  edge [
    source 7
    target 719
  ]
  edge [
    source 7
    target 720
  ]
  edge [
    source 7
    target 721
  ]
  edge [
    source 7
    target 722
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 723
  ]
  edge [
    source 7
    target 724
  ]
  edge [
    source 7
    target 725
  ]
  edge [
    source 7
    target 726
  ]
  edge [
    source 7
    target 727
  ]
  edge [
    source 7
    target 728
  ]
  edge [
    source 7
    target 729
  ]
  edge [
    source 7
    target 730
  ]
  edge [
    source 7
    target 731
  ]
  edge [
    source 7
    target 732
  ]
  edge [
    source 7
    target 733
  ]
  edge [
    source 7
    target 734
  ]
  edge [
    source 7
    target 735
  ]
  edge [
    source 7
    target 736
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 737
  ]
  edge [
    source 7
    target 738
  ]
  edge [
    source 7
    target 739
  ]
  edge [
    source 7
    target 740
  ]
  edge [
    source 7
    target 741
  ]
  edge [
    source 7
    target 742
  ]
  edge [
    source 7
    target 743
  ]
  edge [
    source 7
    target 744
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 745
  ]
  edge [
    source 7
    target 746
  ]
  edge [
    source 7
    target 747
  ]
  edge [
    source 7
    target 748
  ]
  edge [
    source 7
    target 749
  ]
  edge [
    source 7
    target 750
  ]
  edge [
    source 7
    target 751
  ]
  edge [
    source 7
    target 752
  ]
  edge [
    source 7
    target 753
  ]
  edge [
    source 7
    target 754
  ]
  edge [
    source 7
    target 755
  ]
  edge [
    source 7
    target 756
  ]
  edge [
    source 7
    target 757
  ]
  edge [
    source 7
    target 758
  ]
  edge [
    source 7
    target 759
  ]
  edge [
    source 7
    target 760
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 761
  ]
  edge [
    source 7
    target 762
  ]
  edge [
    source 7
    target 763
  ]
  edge [
    source 7
    target 764
  ]
  edge [
    source 7
    target 765
  ]
  edge [
    source 7
    target 766
  ]
  edge [
    source 7
    target 767
  ]
  edge [
    source 7
    target 768
  ]
  edge [
    source 7
    target 769
  ]
  edge [
    source 7
    target 770
  ]
  edge [
    source 7
    target 771
  ]
  edge [
    source 7
    target 772
  ]
  edge [
    source 7
    target 773
  ]
  edge [
    source 7
    target 774
  ]
  edge [
    source 7
    target 775
  ]
  edge [
    source 7
    target 776
  ]
  edge [
    source 7
    target 777
  ]
  edge [
    source 7
    target 778
  ]
  edge [
    source 7
    target 779
  ]
  edge [
    source 7
    target 780
  ]
  edge [
    source 7
    target 781
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 782
  ]
  edge [
    source 7
    target 783
  ]
  edge [
    source 7
    target 784
  ]
  edge [
    source 7
    target 785
  ]
  edge [
    source 7
    target 786
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 787
  ]
  edge [
    source 7
    target 788
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 789
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 801
  ]
  edge [
    source 10
    target 802
  ]
  edge [
    source 10
    target 803
  ]
  edge [
    source 10
    target 804
  ]
  edge [
    source 10
    target 805
  ]
  edge [
    source 10
    target 806
  ]
  edge [
    source 10
    target 807
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 823
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 824
  ]
  edge [
    source 10
    target 825
  ]
  edge [
    source 10
    target 826
  ]
  edge [
    source 10
    target 827
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 828
  ]
  edge [
    source 10
    target 829
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 830
  ]
  edge [
    source 10
    target 831
  ]
  edge [
    source 10
    target 832
  ]
  edge [
    source 10
    target 833
  ]
  edge [
    source 10
    target 834
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 835
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 836
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 839
  ]
  edge [
    source 10
    target 840
  ]
  edge [
    source 10
    target 841
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 842
  ]
  edge [
    source 10
    target 843
  ]
  edge [
    source 10
    target 844
  ]
  edge [
    source 10
    target 845
  ]
  edge [
    source 10
    target 846
  ]
  edge [
    source 10
    target 847
  ]
  edge [
    source 10
    target 848
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 849
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 850
  ]
  edge [
    source 10
    target 851
  ]
  edge [
    source 10
    target 852
  ]
  edge [
    source 10
    target 853
  ]
  edge [
    source 10
    target 854
  ]
  edge [
    source 10
    target 855
  ]
  edge [
    source 10
    target 856
  ]
  edge [
    source 10
    target 857
  ]
  edge [
    source 10
    target 858
  ]
  edge [
    source 10
    target 859
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 860
  ]
  edge [
    source 10
    target 861
  ]
  edge [
    source 10
    target 862
  ]
  edge [
    source 10
    target 863
  ]
  edge [
    source 10
    target 864
  ]
  edge [
    source 10
    target 865
  ]
  edge [
    source 10
    target 866
  ]
  edge [
    source 10
    target 867
  ]
  edge [
    source 10
    target 868
  ]
  edge [
    source 10
    target 869
  ]
  edge [
    source 10
    target 870
  ]
  edge [
    source 10
    target 871
  ]
  edge [
    source 10
    target 872
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 873
  ]
  edge [
    source 10
    target 874
  ]
  edge [
    source 10
    target 875
  ]
  edge [
    source 10
    target 876
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 877
  ]
  edge [
    source 10
    target 878
  ]
  edge [
    source 10
    target 879
  ]
  edge [
    source 10
    target 880
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 881
  ]
  edge [
    source 10
    target 882
  ]
  edge [
    source 10
    target 883
  ]
  edge [
    source 10
    target 884
  ]
  edge [
    source 10
    target 885
  ]
  edge [
    source 10
    target 886
  ]
  edge [
    source 10
    target 887
  ]
  edge [
    source 10
    target 888
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 889
  ]
  edge [
    source 10
    target 890
  ]
  edge [
    source 10
    target 891
  ]
  edge [
    source 10
    target 892
  ]
  edge [
    source 10
    target 893
  ]
  edge [
    source 10
    target 894
  ]
  edge [
    source 10
    target 895
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 896
  ]
  edge [
    source 10
    target 897
  ]
  edge [
    source 10
    target 898
  ]
  edge [
    source 10
    target 899
  ]
  edge [
    source 10
    target 900
  ]
  edge [
    source 10
    target 901
  ]
  edge [
    source 10
    target 902
  ]
  edge [
    source 10
    target 903
  ]
  edge [
    source 10
    target 904
  ]
  edge [
    source 10
    target 905
  ]
  edge [
    source 10
    target 906
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 10
    target 907
  ]
  edge [
    source 10
    target 908
  ]
  edge [
    source 10
    target 909
  ]
  edge [
    source 10
    target 910
  ]
  edge [
    source 10
    target 911
  ]
  edge [
    source 10
    target 912
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 913
  ]
  edge [
    source 10
    target 914
  ]
  edge [
    source 10
    target 915
  ]
  edge [
    source 10
    target 916
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 917
  ]
  edge [
    source 10
    target 918
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 919
  ]
  edge [
    source 10
    target 920
  ]
  edge [
    source 10
    target 921
  ]
  edge [
    source 10
    target 922
  ]
  edge [
    source 10
    target 923
  ]
  edge [
    source 10
    target 924
  ]
  edge [
    source 10
    target 925
  ]
  edge [
    source 10
    target 926
  ]
  edge [
    source 10
    target 927
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 928
  ]
  edge [
    source 10
    target 929
  ]
  edge [
    source 10
    target 930
  ]
  edge [
    source 10
    target 931
  ]
  edge [
    source 10
    target 932
  ]
  edge [
    source 10
    target 933
  ]
  edge [
    source 10
    target 934
  ]
  edge [
    source 10
    target 935
  ]
  edge [
    source 10
    target 936
  ]
  edge [
    source 10
    target 937
  ]
  edge [
    source 10
    target 938
  ]
  edge [
    source 10
    target 939
  ]
  edge [
    source 10
    target 940
  ]
  edge [
    source 10
    target 941
  ]
  edge [
    source 10
    target 942
  ]
  edge [
    source 10
    target 943
  ]
  edge [
    source 10
    target 944
  ]
  edge [
    source 10
    target 945
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 946
  ]
  edge [
    source 10
    target 947
  ]
  edge [
    source 10
    target 948
  ]
  edge [
    source 10
    target 949
  ]
  edge [
    source 10
    target 950
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 951
  ]
  edge [
    source 10
    target 952
  ]
  edge [
    source 10
    target 953
  ]
  edge [
    source 10
    target 954
  ]
  edge [
    source 10
    target 955
  ]
  edge [
    source 10
    target 956
  ]
  edge [
    source 10
    target 957
  ]
  edge [
    source 10
    target 958
  ]
  edge [
    source 10
    target 959
  ]
  edge [
    source 10
    target 960
  ]
  edge [
    source 10
    target 961
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 962
  ]
  edge [
    source 10
    target 963
  ]
  edge [
    source 10
    target 964
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 965
  ]
  edge [
    source 10
    target 966
  ]
  edge [
    source 10
    target 10
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 10
    target 1696
  ]
  edge [
    source 10
    target 1038
  ]
  edge [
    source 10
    target 2115
  ]
  edge [
    source 10
    target 2116
  ]
  edge [
    source 10
    target 2117
  ]
  edge [
    source 10
    target 2118
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 11
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 967
  ]
  edge [
    source 12
    target 968
  ]
  edge [
    source 12
    target 969
  ]
  edge [
    source 12
    target 970
  ]
  edge [
    source 12
    target 971
  ]
  edge [
    source 12
    target 972
  ]
  edge [
    source 12
    target 973
  ]
  edge [
    source 12
    target 974
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 975
  ]
  edge [
    source 12
    target 976
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 977
  ]
  edge [
    source 12
    target 978
  ]
  edge [
    source 12
    target 979
  ]
  edge [
    source 12
    target 980
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 981
  ]
  edge [
    source 12
    target 982
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 983
  ]
  edge [
    source 12
    target 984
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 12
    target 985
  ]
  edge [
    source 12
    target 986
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 987
  ]
  edge [
    source 12
    target 988
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 989
  ]
  edge [
    source 12
    target 990
  ]
  edge [
    source 12
    target 991
  ]
  edge [
    source 12
    target 992
  ]
  edge [
    source 12
    target 993
  ]
  edge [
    source 12
    target 994
  ]
  edge [
    source 12
    target 995
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 996
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 997
  ]
  edge [
    source 12
    target 998
  ]
  edge [
    source 12
    target 999
  ]
  edge [
    source 12
    target 1000
  ]
  edge [
    source 12
    target 1001
  ]
  edge [
    source 12
    target 1002
  ]
  edge [
    source 12
    target 1003
  ]
  edge [
    source 12
    target 1004
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 213
  ]
  edge [
    source 12
    target 1005
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 1006
  ]
  edge [
    source 12
    target 1007
  ]
  edge [
    source 12
    target 1008
  ]
  edge [
    source 12
    target 1009
  ]
  edge [
    source 12
    target 1010
  ]
  edge [
    source 12
    target 1011
  ]
  edge [
    source 12
    target 1012
  ]
  edge [
    source 12
    target 1013
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 12
    target 1014
  ]
  edge [
    source 12
    target 1015
  ]
  edge [
    source 12
    target 1016
  ]
  edge [
    source 12
    target 1017
  ]
  edge [
    source 12
    target 1018
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 1019
  ]
  edge [
    source 12
    target 1020
  ]
  edge [
    source 12
    target 1021
  ]
  edge [
    source 12
    target 1022
  ]
  edge [
    source 12
    target 1023
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 1024
  ]
  edge [
    source 12
    target 1025
  ]
  edge [
    source 12
    target 1026
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 404
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 1027
  ]
  edge [
    source 12
    target 1028
  ]
  edge [
    source 12
    target 1029
  ]
  edge [
    source 12
    target 1030
  ]
  edge [
    source 12
    target 1031
  ]
  edge [
    source 12
    target 1032
  ]
  edge [
    source 12
    target 1033
  ]
  edge [
    source 12
    target 1034
  ]
  edge [
    source 12
    target 1035
  ]
  edge [
    source 12
    target 1036
  ]
  edge [
    source 12
    target 1037
  ]
  edge [
    source 12
    target 1038
  ]
  edge [
    source 12
    target 1039
  ]
  edge [
    source 12
    target 1040
  ]
  edge [
    source 12
    target 1041
  ]
  edge [
    source 12
    target 1042
  ]
  edge [
    source 12
    target 1043
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 1044
  ]
  edge [
    source 12
    target 1045
  ]
  edge [
    source 12
    target 1046
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 1047
  ]
  edge [
    source 12
    target 22
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 35
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1048
  ]
  edge [
    source 13
    target 1049
  ]
  edge [
    source 13
    target 1050
  ]
  edge [
    source 13
    target 1051
  ]
  edge [
    source 13
    target 1052
  ]
  edge [
    source 13
    target 1053
  ]
  edge [
    source 13
    target 1054
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 1055
  ]
  edge [
    source 15
    target 1056
  ]
  edge [
    source 15
    target 1057
  ]
  edge [
    source 15
    target 1058
  ]
  edge [
    source 15
    target 1059
  ]
  edge [
    source 15
    target 1060
  ]
  edge [
    source 15
    target 1061
  ]
  edge [
    source 15
    target 1062
  ]
  edge [
    source 15
    target 1063
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 1064
  ]
  edge [
    source 15
    target 1065
  ]
  edge [
    source 15
    target 1066
  ]
  edge [
    source 15
    target 1067
  ]
  edge [
    source 15
    target 1068
  ]
  edge [
    source 15
    target 1069
  ]
  edge [
    source 15
    target 1070
  ]
  edge [
    source 15
    target 1071
  ]
  edge [
    source 15
    target 1072
  ]
  edge [
    source 15
    target 1073
  ]
  edge [
    source 15
    target 1074
  ]
  edge [
    source 15
    target 1075
  ]
  edge [
    source 15
    target 1076
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 1077
  ]
  edge [
    source 15
    target 1078
  ]
  edge [
    source 15
    target 663
  ]
  edge [
    source 15
    target 1079
  ]
  edge [
    source 15
    target 1080
  ]
  edge [
    source 15
    target 1081
  ]
  edge [
    source 15
    target 1082
  ]
  edge [
    source 15
    target 1083
  ]
  edge [
    source 15
    target 1084
  ]
  edge [
    source 15
    target 190
  ]
  edge [
    source 15
    target 1002
  ]
  edge [
    source 15
    target 1085
  ]
  edge [
    source 15
    target 1086
  ]
  edge [
    source 15
    target 71
  ]
  edge [
    source 15
    target 1087
  ]
  edge [
    source 15
    target 1088
  ]
  edge [
    source 15
    target 1089
  ]
  edge [
    source 15
    target 1090
  ]
  edge [
    source 15
    target 1091
  ]
  edge [
    source 15
    target 1092
  ]
  edge [
    source 15
    target 1093
  ]
  edge [
    source 15
    target 1094
  ]
  edge [
    source 15
    target 1095
  ]
  edge [
    source 15
    target 1096
  ]
  edge [
    source 15
    target 1097
  ]
  edge [
    source 15
    target 1098
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 1099
  ]
  edge [
    source 15
    target 1100
  ]
  edge [
    source 15
    target 1101
  ]
  edge [
    source 15
    target 1102
  ]
  edge [
    source 15
    target 1103
  ]
  edge [
    source 15
    target 1104
  ]
  edge [
    source 15
    target 1105
  ]
  edge [
    source 15
    target 1106
  ]
  edge [
    source 15
    target 1107
  ]
  edge [
    source 15
    target 1108
  ]
  edge [
    source 15
    target 1109
  ]
  edge [
    source 15
    target 1110
  ]
  edge [
    source 15
    target 1111
  ]
  edge [
    source 15
    target 1112
  ]
  edge [
    source 15
    target 1113
  ]
  edge [
    source 15
    target 1114
  ]
  edge [
    source 15
    target 1115
  ]
  edge [
    source 15
    target 1116
  ]
  edge [
    source 15
    target 1117
  ]
  edge [
    source 15
    target 1118
  ]
  edge [
    source 15
    target 1119
  ]
  edge [
    source 15
    target 1120
  ]
  edge [
    source 15
    target 1121
  ]
  edge [
    source 15
    target 1122
  ]
  edge [
    source 15
    target 1123
  ]
  edge [
    source 15
    target 1124
  ]
  edge [
    source 15
    target 1125
  ]
  edge [
    source 15
    target 1126
  ]
  edge [
    source 15
    target 1127
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 1128
  ]
  edge [
    source 15
    target 1129
  ]
  edge [
    source 15
    target 1130
  ]
  edge [
    source 15
    target 1131
  ]
  edge [
    source 15
    target 1132
  ]
  edge [
    source 15
    target 1133
  ]
  edge [
    source 15
    target 1134
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 1135
  ]
  edge [
    source 15
    target 1136
  ]
  edge [
    source 15
    target 1137
  ]
  edge [
    source 15
    target 1138
  ]
  edge [
    source 15
    target 1139
  ]
  edge [
    source 15
    target 1140
  ]
  edge [
    source 15
    target 1141
  ]
  edge [
    source 15
    target 1142
  ]
  edge [
    source 15
    target 1143
  ]
  edge [
    source 15
    target 1144
  ]
  edge [
    source 15
    target 1145
  ]
  edge [
    source 15
    target 1146
  ]
  edge [
    source 15
    target 1147
  ]
  edge [
    source 15
    target 1148
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 1149
  ]
  edge [
    source 15
    target 1150
  ]
  edge [
    source 15
    target 1151
  ]
  edge [
    source 15
    target 1152
  ]
  edge [
    source 15
    target 1153
  ]
  edge [
    source 15
    target 221
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 1154
  ]
  edge [
    source 15
    target 1155
  ]
  edge [
    source 15
    target 1156
  ]
  edge [
    source 15
    target 1157
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 39
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1059
  ]
  edge [
    source 18
    target 1158
  ]
  edge [
    source 18
    target 1159
  ]
  edge [
    source 18
    target 1128
  ]
  edge [
    source 18
    target 1129
  ]
  edge [
    source 18
    target 1130
  ]
  edge [
    source 18
    target 1131
  ]
  edge [
    source 18
    target 1132
  ]
  edge [
    source 18
    target 1133
  ]
  edge [
    source 18
    target 1134
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 1135
  ]
  edge [
    source 18
    target 1083
  ]
  edge [
    source 18
    target 1136
  ]
  edge [
    source 18
    target 1137
  ]
  edge [
    source 18
    target 1138
  ]
  edge [
    source 18
    target 1139
  ]
  edge [
    source 18
    target 1140
  ]
  edge [
    source 18
    target 1141
  ]
  edge [
    source 18
    target 1142
  ]
  edge [
    source 18
    target 1143
  ]
  edge [
    source 18
    target 1144
  ]
  edge [
    source 18
    target 1145
  ]
  edge [
    source 18
    target 1146
  ]
  edge [
    source 18
    target 1147
  ]
  edge [
    source 18
    target 1148
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 1149
  ]
  edge [
    source 18
    target 1150
  ]
  edge [
    source 18
    target 1151
  ]
  edge [
    source 18
    target 1152
  ]
  edge [
    source 18
    target 1160
  ]
  edge [
    source 18
    target 1161
  ]
  edge [
    source 18
    target 1162
  ]
  edge [
    source 18
    target 1163
  ]
  edge [
    source 18
    target 1164
  ]
  edge [
    source 18
    target 1165
  ]
  edge [
    source 18
    target 1166
  ]
  edge [
    source 18
    target 1167
  ]
  edge [
    source 18
    target 1168
  ]
  edge [
    source 18
    target 1169
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1103
  ]
  edge [
    source 19
    target 1104
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 1106
  ]
  edge [
    source 19
    target 1107
  ]
  edge [
    source 19
    target 1108
  ]
  edge [
    source 19
    target 1109
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 1110
  ]
  edge [
    source 19
    target 1111
  ]
  edge [
    source 19
    target 1112
  ]
  edge [
    source 19
    target 1113
  ]
  edge [
    source 19
    target 1114
  ]
  edge [
    source 19
    target 1115
  ]
  edge [
    source 19
    target 1116
  ]
  edge [
    source 19
    target 1117
  ]
  edge [
    source 19
    target 1118
  ]
  edge [
    source 19
    target 1119
  ]
  edge [
    source 19
    target 1120
  ]
  edge [
    source 19
    target 1121
  ]
  edge [
    source 19
    target 1122
  ]
  edge [
    source 19
    target 1170
  ]
  edge [
    source 19
    target 1171
  ]
  edge [
    source 19
    target 787
  ]
  edge [
    source 19
    target 1172
  ]
  edge [
    source 19
    target 69
  ]
  edge [
    source 19
    target 1173
  ]
  edge [
    source 19
    target 901
  ]
  edge [
    source 19
    target 1174
  ]
  edge [
    source 19
    target 1175
  ]
  edge [
    source 19
    target 355
  ]
  edge [
    source 19
    target 1176
  ]
  edge [
    source 19
    target 1177
  ]
  edge [
    source 19
    target 1178
  ]
  edge [
    source 19
    target 1179
  ]
  edge [
    source 19
    target 1180
  ]
  edge [
    source 19
    target 1181
  ]
  edge [
    source 19
    target 1182
  ]
  edge [
    source 19
    target 1183
  ]
  edge [
    source 19
    target 1184
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 1186
  ]
  edge [
    source 19
    target 1187
  ]
  edge [
    source 19
    target 1188
  ]
  edge [
    source 19
    target 66
  ]
  edge [
    source 19
    target 1083
  ]
  edge [
    source 19
    target 368
  ]
  edge [
    source 19
    target 1189
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 1190
  ]
  edge [
    source 19
    target 1191
  ]
  edge [
    source 19
    target 866
  ]
  edge [
    source 19
    target 359
  ]
  edge [
    source 19
    target 1192
  ]
  edge [
    source 19
    target 1193
  ]
  edge [
    source 19
    target 1194
  ]
  edge [
    source 19
    target 1195
  ]
  edge [
    source 19
    target 1196
  ]
  edge [
    source 19
    target 1197
  ]
  edge [
    source 19
    target 1198
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 1200
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 1202
  ]
  edge [
    source 19
    target 1203
  ]
  edge [
    source 19
    target 1204
  ]
  edge [
    source 19
    target 1205
  ]
  edge [
    source 19
    target 1206
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1208
  ]
  edge [
    source 19
    target 1209
  ]
  edge [
    source 19
    target 1210
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 396
  ]
  edge [
    source 19
    target 666
  ]
  edge [
    source 19
    target 1212
  ]
  edge [
    source 19
    target 1213
  ]
  edge [
    source 19
    target 1214
  ]
  edge [
    source 19
    target 1215
  ]
  edge [
    source 19
    target 1216
  ]
  edge [
    source 19
    target 1217
  ]
  edge [
    source 19
    target 1218
  ]
  edge [
    source 19
    target 1219
  ]
  edge [
    source 19
    target 1220
  ]
  edge [
    source 19
    target 1221
  ]
  edge [
    source 19
    target 1222
  ]
  edge [
    source 19
    target 1223
  ]
  edge [
    source 19
    target 1224
  ]
  edge [
    source 19
    target 1225
  ]
  edge [
    source 19
    target 1226
  ]
  edge [
    source 19
    target 1227
  ]
  edge [
    source 19
    target 1228
  ]
  edge [
    source 19
    target 1229
  ]
  edge [
    source 19
    target 1230
  ]
  edge [
    source 19
    target 1231
  ]
  edge [
    source 19
    target 44
  ]
  edge [
    source 19
    target 1232
  ]
  edge [
    source 19
    target 1233
  ]
  edge [
    source 19
    target 1234
  ]
  edge [
    source 19
    target 1235
  ]
  edge [
    source 19
    target 1236
  ]
  edge [
    source 19
    target 1237
  ]
  edge [
    source 19
    target 1238
  ]
  edge [
    source 19
    target 1239
  ]
  edge [
    source 19
    target 658
  ]
  edge [
    source 19
    target 1240
  ]
  edge [
    source 19
    target 1241
  ]
  edge [
    source 19
    target 1242
  ]
  edge [
    source 19
    target 1243
  ]
  edge [
    source 19
    target 1244
  ]
  edge [
    source 19
    target 1245
  ]
  edge [
    source 19
    target 1246
  ]
  edge [
    source 19
    target 1247
  ]
  edge [
    source 19
    target 1248
  ]
  edge [
    source 19
    target 1249
  ]
  edge [
    source 19
    target 1250
  ]
  edge [
    source 19
    target 1251
  ]
  edge [
    source 19
    target 1252
  ]
  edge [
    source 19
    target 1253
  ]
  edge [
    source 19
    target 1254
  ]
  edge [
    source 19
    target 1255
  ]
  edge [
    source 19
    target 1256
  ]
  edge [
    source 19
    target 1257
  ]
  edge [
    source 19
    target 1258
  ]
  edge [
    source 19
    target 981
  ]
  edge [
    source 19
    target 1259
  ]
  edge [
    source 19
    target 1260
  ]
  edge [
    source 19
    target 1261
  ]
  edge [
    source 19
    target 1262
  ]
  edge [
    source 19
    target 783
  ]
  edge [
    source 19
    target 1263
  ]
  edge [
    source 19
    target 1264
  ]
  edge [
    source 19
    target 1265
  ]
  edge [
    source 19
    target 1266
  ]
  edge [
    source 19
    target 1267
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 1268
  ]
  edge [
    source 19
    target 1269
  ]
  edge [
    source 19
    target 1270
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 1271
  ]
  edge [
    source 19
    target 1272
  ]
  edge [
    source 19
    target 1273
  ]
  edge [
    source 19
    target 1274
  ]
  edge [
    source 19
    target 1275
  ]
  edge [
    source 19
    target 1276
  ]
  edge [
    source 19
    target 1277
  ]
  edge [
    source 19
    target 1278
  ]
  edge [
    source 19
    target 1279
  ]
  edge [
    source 19
    target 1280
  ]
  edge [
    source 19
    target 1281
  ]
  edge [
    source 19
    target 1282
  ]
  edge [
    source 19
    target 1283
  ]
  edge [
    source 19
    target 1284
  ]
  edge [
    source 19
    target 1285
  ]
  edge [
    source 19
    target 1286
  ]
  edge [
    source 19
    target 1287
  ]
  edge [
    source 19
    target 1288
  ]
  edge [
    source 19
    target 1289
  ]
  edge [
    source 19
    target 1290
  ]
  edge [
    source 19
    target 34
  ]
  edge [
    source 19
    target 30
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1291
  ]
  edge [
    source 20
    target 1292
  ]
  edge [
    source 20
    target 1293
  ]
  edge [
    source 20
    target 1294
  ]
  edge [
    source 20
    target 1295
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 35
  ]
  edge [
    source 20
    target 2114
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1296
  ]
  edge [
    source 21
    target 1297
  ]
  edge [
    source 21
    target 1298
  ]
  edge [
    source 21
    target 1299
  ]
  edge [
    source 21
    target 1300
  ]
  edge [
    source 21
    target 1301
  ]
  edge [
    source 21
    target 1302
  ]
  edge [
    source 21
    target 1303
  ]
  edge [
    source 21
    target 1304
  ]
  edge [
    source 21
    target 1305
  ]
  edge [
    source 21
    target 1306
  ]
  edge [
    source 21
    target 1307
  ]
  edge [
    source 21
    target 1308
  ]
  edge [
    source 21
    target 1309
  ]
  edge [
    source 21
    target 1310
  ]
  edge [
    source 21
    target 1311
  ]
  edge [
    source 21
    target 1312
  ]
  edge [
    source 21
    target 1313
  ]
  edge [
    source 21
    target 1314
  ]
  edge [
    source 21
    target 1315
  ]
  edge [
    source 21
    target 1316
  ]
  edge [
    source 21
    target 1317
  ]
  edge [
    source 21
    target 1318
  ]
  edge [
    source 21
    target 1319
  ]
  edge [
    source 21
    target 1320
  ]
  edge [
    source 21
    target 1321
  ]
  edge [
    source 21
    target 1322
  ]
  edge [
    source 21
    target 1323
  ]
  edge [
    source 21
    target 901
  ]
  edge [
    source 21
    target 1324
  ]
  edge [
    source 21
    target 1325
  ]
  edge [
    source 21
    target 1326
  ]
  edge [
    source 21
    target 1327
  ]
  edge [
    source 21
    target 1328
  ]
  edge [
    source 21
    target 1329
  ]
  edge [
    source 21
    target 1330
  ]
  edge [
    source 21
    target 1331
  ]
  edge [
    source 21
    target 1332
  ]
  edge [
    source 21
    target 1333
  ]
  edge [
    source 21
    target 1334
  ]
  edge [
    source 21
    target 1335
  ]
  edge [
    source 21
    target 1336
  ]
  edge [
    source 21
    target 1337
  ]
  edge [
    source 21
    target 1338
  ]
  edge [
    source 21
    target 1339
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 1340
  ]
  edge [
    source 21
    target 1341
  ]
  edge [
    source 21
    target 1342
  ]
  edge [
    source 21
    target 1343
  ]
  edge [
    source 21
    target 1344
  ]
  edge [
    source 21
    target 1345
  ]
  edge [
    source 21
    target 1346
  ]
  edge [
    source 21
    target 1347
  ]
  edge [
    source 21
    target 1348
  ]
  edge [
    source 21
    target 1349
  ]
  edge [
    source 21
    target 1350
  ]
  edge [
    source 21
    target 1351
  ]
  edge [
    source 21
    target 1352
  ]
  edge [
    source 21
    target 1353
  ]
  edge [
    source 21
    target 1354
  ]
  edge [
    source 21
    target 1355
  ]
  edge [
    source 21
    target 1356
  ]
  edge [
    source 21
    target 1357
  ]
  edge [
    source 21
    target 1358
  ]
  edge [
    source 21
    target 1359
  ]
  edge [
    source 21
    target 1360
  ]
  edge [
    source 21
    target 1361
  ]
  edge [
    source 21
    target 1362
  ]
  edge [
    source 21
    target 1363
  ]
  edge [
    source 21
    target 1364
  ]
  edge [
    source 21
    target 1365
  ]
  edge [
    source 21
    target 1366
  ]
  edge [
    source 21
    target 1367
  ]
  edge [
    source 21
    target 1368
  ]
  edge [
    source 21
    target 1369
  ]
  edge [
    source 21
    target 1370
  ]
  edge [
    source 21
    target 1371
  ]
  edge [
    source 21
    target 1372
  ]
  edge [
    source 21
    target 1373
  ]
  edge [
    source 21
    target 1374
  ]
  edge [
    source 21
    target 1375
  ]
  edge [
    source 21
    target 1376
  ]
  edge [
    source 21
    target 1377
  ]
  edge [
    source 21
    target 1378
  ]
  edge [
    source 21
    target 1379
  ]
  edge [
    source 21
    target 626
  ]
  edge [
    source 21
    target 1380
  ]
  edge [
    source 21
    target 1381
  ]
  edge [
    source 21
    target 1382
  ]
  edge [
    source 21
    target 1383
  ]
  edge [
    source 21
    target 1384
  ]
  edge [
    source 21
    target 1385
  ]
  edge [
    source 21
    target 1386
  ]
  edge [
    source 21
    target 1387
  ]
  edge [
    source 21
    target 1388
  ]
  edge [
    source 21
    target 1389
  ]
  edge [
    source 21
    target 1390
  ]
  edge [
    source 21
    target 1391
  ]
  edge [
    source 21
    target 1392
  ]
  edge [
    source 21
    target 1393
  ]
  edge [
    source 21
    target 1394
  ]
  edge [
    source 21
    target 1395
  ]
  edge [
    source 21
    target 1396
  ]
  edge [
    source 21
    target 1397
  ]
  edge [
    source 21
    target 1398
  ]
  edge [
    source 21
    target 1399
  ]
  edge [
    source 21
    target 1400
  ]
  edge [
    source 21
    target 1401
  ]
  edge [
    source 21
    target 1402
  ]
  edge [
    source 21
    target 1403
  ]
  edge [
    source 21
    target 1404
  ]
  edge [
    source 21
    target 1405
  ]
  edge [
    source 21
    target 1406
  ]
  edge [
    source 21
    target 1407
  ]
  edge [
    source 21
    target 1408
  ]
  edge [
    source 21
    target 1409
  ]
  edge [
    source 21
    target 1410
  ]
  edge [
    source 21
    target 1411
  ]
  edge [
    source 21
    target 1412
  ]
  edge [
    source 21
    target 1413
  ]
  edge [
    source 21
    target 1414
  ]
  edge [
    source 21
    target 1415
  ]
  edge [
    source 21
    target 1416
  ]
  edge [
    source 21
    target 1417
  ]
  edge [
    source 21
    target 1418
  ]
  edge [
    source 21
    target 1419
  ]
  edge [
    source 21
    target 1420
  ]
  edge [
    source 21
    target 1421
  ]
  edge [
    source 21
    target 1422
  ]
  edge [
    source 21
    target 1423
  ]
  edge [
    source 21
    target 1424
  ]
  edge [
    source 21
    target 1425
  ]
  edge [
    source 21
    target 1426
  ]
  edge [
    source 21
    target 1427
  ]
  edge [
    source 21
    target 1428
  ]
  edge [
    source 21
    target 1429
  ]
  edge [
    source 21
    target 1430
  ]
  edge [
    source 21
    target 1431
  ]
  edge [
    source 21
    target 1432
  ]
  edge [
    source 21
    target 1433
  ]
  edge [
    source 21
    target 1434
  ]
  edge [
    source 21
    target 1435
  ]
  edge [
    source 21
    target 1436
  ]
  edge [
    source 21
    target 1437
  ]
  edge [
    source 21
    target 1438
  ]
  edge [
    source 21
    target 1439
  ]
  edge [
    source 21
    target 1440
  ]
  edge [
    source 21
    target 1441
  ]
  edge [
    source 21
    target 1442
  ]
  edge [
    source 21
    target 1443
  ]
  edge [
    source 21
    target 1444
  ]
  edge [
    source 21
    target 1445
  ]
  edge [
    source 21
    target 1446
  ]
  edge [
    source 21
    target 1447
  ]
  edge [
    source 21
    target 1448
  ]
  edge [
    source 21
    target 1449
  ]
  edge [
    source 21
    target 1450
  ]
  edge [
    source 21
    target 1451
  ]
  edge [
    source 21
    target 1452
  ]
  edge [
    source 21
    target 1453
  ]
  edge [
    source 21
    target 1454
  ]
  edge [
    source 21
    target 1455
  ]
  edge [
    source 21
    target 1456
  ]
  edge [
    source 21
    target 1457
  ]
  edge [
    source 21
    target 1458
  ]
  edge [
    source 21
    target 1459
  ]
  edge [
    source 21
    target 1460
  ]
  edge [
    source 21
    target 1461
  ]
  edge [
    source 21
    target 1462
  ]
  edge [
    source 21
    target 1463
  ]
  edge [
    source 21
    target 1464
  ]
  edge [
    source 21
    target 1465
  ]
  edge [
    source 21
    target 1466
  ]
  edge [
    source 21
    target 1467
  ]
  edge [
    source 21
    target 1468
  ]
  edge [
    source 21
    target 1469
  ]
  edge [
    source 21
    target 1470
  ]
  edge [
    source 21
    target 1471
  ]
  edge [
    source 21
    target 1472
  ]
  edge [
    source 21
    target 1473
  ]
  edge [
    source 21
    target 1474
  ]
  edge [
    source 21
    target 1475
  ]
  edge [
    source 21
    target 1476
  ]
  edge [
    source 21
    target 1477
  ]
  edge [
    source 21
    target 1478
  ]
  edge [
    source 21
    target 1479
  ]
  edge [
    source 21
    target 1480
  ]
  edge [
    source 21
    target 1481
  ]
  edge [
    source 21
    target 1482
  ]
  edge [
    source 21
    target 1483
  ]
  edge [
    source 21
    target 1484
  ]
  edge [
    source 21
    target 1485
  ]
  edge [
    source 21
    target 1486
  ]
  edge [
    source 21
    target 1487
  ]
  edge [
    source 21
    target 1488
  ]
  edge [
    source 21
    target 1489
  ]
  edge [
    source 21
    target 1490
  ]
  edge [
    source 21
    target 1491
  ]
  edge [
    source 21
    target 1492
  ]
  edge [
    source 21
    target 1493
  ]
  edge [
    source 21
    target 1494
  ]
  edge [
    source 21
    target 1495
  ]
  edge [
    source 21
    target 1496
  ]
  edge [
    source 21
    target 1497
  ]
  edge [
    source 21
    target 1498
  ]
  edge [
    source 21
    target 1499
  ]
  edge [
    source 21
    target 1500
  ]
  edge [
    source 21
    target 1501
  ]
  edge [
    source 21
    target 1502
  ]
  edge [
    source 21
    target 1503
  ]
  edge [
    source 21
    target 1504
  ]
  edge [
    source 21
    target 1505
  ]
  edge [
    source 21
    target 1506
  ]
  edge [
    source 21
    target 1059
  ]
  edge [
    source 21
    target 1507
  ]
  edge [
    source 21
    target 1508
  ]
  edge [
    source 21
    target 1509
  ]
  edge [
    source 21
    target 1510
  ]
  edge [
    source 21
    target 1511
  ]
  edge [
    source 21
    target 1512
  ]
  edge [
    source 21
    target 1513
  ]
  edge [
    source 21
    target 1514
  ]
  edge [
    source 21
    target 1515
  ]
  edge [
    source 21
    target 1516
  ]
  edge [
    source 21
    target 1517
  ]
  edge [
    source 21
    target 1518
  ]
  edge [
    source 21
    target 1519
  ]
  edge [
    source 21
    target 1520
  ]
  edge [
    source 21
    target 1521
  ]
  edge [
    source 21
    target 1522
  ]
  edge [
    source 21
    target 1523
  ]
  edge [
    source 21
    target 1524
  ]
  edge [
    source 21
    target 1525
  ]
  edge [
    source 21
    target 1526
  ]
  edge [
    source 21
    target 1527
  ]
  edge [
    source 21
    target 1528
  ]
  edge [
    source 21
    target 1529
  ]
  edge [
    source 21
    target 1530
  ]
  edge [
    source 21
    target 1531
  ]
  edge [
    source 21
    target 1532
  ]
  edge [
    source 21
    target 1533
  ]
  edge [
    source 21
    target 1534
  ]
  edge [
    source 21
    target 1535
  ]
  edge [
    source 21
    target 1536
  ]
  edge [
    source 21
    target 1537
  ]
  edge [
    source 21
    target 1538
  ]
  edge [
    source 21
    target 1539
  ]
  edge [
    source 21
    target 1540
  ]
  edge [
    source 21
    target 1541
  ]
  edge [
    source 21
    target 1542
  ]
  edge [
    source 21
    target 1543
  ]
  edge [
    source 21
    target 1544
  ]
  edge [
    source 21
    target 36
  ]
  edge [
    source 21
    target 2114
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1545
  ]
  edge [
    source 22
    target 1546
  ]
  edge [
    source 22
    target 1547
  ]
  edge [
    source 22
    target 1548
  ]
  edge [
    source 22
    target 1549
  ]
  edge [
    source 22
    target 1550
  ]
  edge [
    source 22
    target 1551
  ]
  edge [
    source 22
    target 1552
  ]
  edge [
    source 22
    target 1553
  ]
  edge [
    source 22
    target 347
  ]
  edge [
    source 22
    target 1554
  ]
  edge [
    source 22
    target 1291
  ]
  edge [
    source 22
    target 1555
  ]
  edge [
    source 22
    target 1556
  ]
  edge [
    source 22
    target 2114
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1557
  ]
  edge [
    source 23
    target 1558
  ]
  edge [
    source 23
    target 1559
  ]
  edge [
    source 23
    target 1560
  ]
  edge [
    source 23
    target 654
  ]
  edge [
    source 23
    target 1561
  ]
  edge [
    source 23
    target 1562
  ]
  edge [
    source 23
    target 1563
  ]
  edge [
    source 23
    target 1564
  ]
  edge [
    source 23
    target 1565
  ]
  edge [
    source 23
    target 1566
  ]
  edge [
    source 23
    target 613
  ]
  edge [
    source 23
    target 664
  ]
  edge [
    source 23
    target 1567
  ]
  edge [
    source 23
    target 657
  ]
  edge [
    source 23
    target 1568
  ]
  edge [
    source 23
    target 1569
  ]
  edge [
    source 23
    target 186
  ]
  edge [
    source 23
    target 1570
  ]
  edge [
    source 23
    target 1571
  ]
  edge [
    source 23
    target 1572
  ]
  edge [
    source 23
    target 1573
  ]
  edge [
    source 23
    target 1574
  ]
  edge [
    source 23
    target 1575
  ]
  edge [
    source 23
    target 1576
  ]
  edge [
    source 23
    target 699
  ]
  edge [
    source 23
    target 1083
  ]
  edge [
    source 23
    target 1577
  ]
  edge [
    source 23
    target 1578
  ]
  edge [
    source 23
    target 368
  ]
  edge [
    source 23
    target 1579
  ]
  edge [
    source 23
    target 1580
  ]
  edge [
    source 23
    target 1581
  ]
  edge [
    source 23
    target 1582
  ]
  edge [
    source 23
    target 80
  ]
  edge [
    source 23
    target 1583
  ]
  edge [
    source 23
    target 37
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1034
  ]
  edge [
    source 25
    target 1584
  ]
  edge [
    source 25
    target 1585
  ]
  edge [
    source 25
    target 1586
  ]
  edge [
    source 25
    target 1587
  ]
  edge [
    source 25
    target 1588
  ]
  edge [
    source 25
    target 1589
  ]
  edge [
    source 25
    target 1590
  ]
  edge [
    source 25
    target 1591
  ]
  edge [
    source 25
    target 1592
  ]
  edge [
    source 25
    target 1593
  ]
  edge [
    source 25
    target 1594
  ]
  edge [
    source 25
    target 1595
  ]
  edge [
    source 25
    target 1596
  ]
  edge [
    source 25
    target 1597
  ]
  edge [
    source 25
    target 1598
  ]
  edge [
    source 25
    target 1599
  ]
  edge [
    source 25
    target 1600
  ]
  edge [
    source 25
    target 791
  ]
  edge [
    source 25
    target 1601
  ]
  edge [
    source 25
    target 39
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1602
  ]
  edge [
    source 27
    target 1035
  ]
  edge [
    source 27
    target 1603
  ]
  edge [
    source 27
    target 1604
  ]
  edge [
    source 27
    target 1605
  ]
  edge [
    source 27
    target 1606
  ]
  edge [
    source 27
    target 1607
  ]
  edge [
    source 27
    target 1608
  ]
  edge [
    source 27
    target 1609
  ]
  edge [
    source 27
    target 1610
  ]
  edge [
    source 27
    target 1611
  ]
  edge [
    source 27
    target 1596
  ]
  edge [
    source 27
    target 1612
  ]
  edge [
    source 27
    target 1613
  ]
  edge [
    source 27
    target 304
  ]
  edge [
    source 27
    target 1614
  ]
  edge [
    source 27
    target 1042
  ]
  edge [
    source 27
    target 1615
  ]
  edge [
    source 27
    target 1616
  ]
  edge [
    source 27
    target 1617
  ]
  edge [
    source 27
    target 1618
  ]
  edge [
    source 27
    target 1619
  ]
  edge [
    source 27
    target 1620
  ]
  edge [
    source 27
    target 1621
  ]
  edge [
    source 27
    target 1622
  ]
  edge [
    source 27
    target 1623
  ]
  edge [
    source 27
    target 1624
  ]
  edge [
    source 27
    target 1625
  ]
  edge [
    source 27
    target 1626
  ]
  edge [
    source 27
    target 1591
  ]
  edge [
    source 27
    target 1627
  ]
  edge [
    source 27
    target 40
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1628
  ]
  edge [
    source 28
    target 1629
  ]
  edge [
    source 28
    target 1630
  ]
  edge [
    source 28
    target 1631
  ]
  edge [
    source 28
    target 779
  ]
  edge [
    source 28
    target 390
  ]
  edge [
    source 28
    target 1632
  ]
  edge [
    source 28
    target 1633
  ]
  edge [
    source 28
    target 120
  ]
  edge [
    source 28
    target 1274
  ]
  edge [
    source 28
    target 1634
  ]
  edge [
    source 28
    target 1635
  ]
  edge [
    source 28
    target 1636
  ]
  edge [
    source 28
    target 1637
  ]
  edge [
    source 28
    target 80
  ]
  edge [
    source 28
    target 1638
  ]
  edge [
    source 28
    target 127
  ]
  edge [
    source 28
    target 1639
  ]
  edge [
    source 28
    target 1640
  ]
  edge [
    source 28
    target 1641
  ]
  edge [
    source 28
    target 700
  ]
  edge [
    source 28
    target 1642
  ]
  edge [
    source 28
    target 1643
  ]
  edge [
    source 28
    target 366
  ]
  edge [
    source 28
    target 1644
  ]
  edge [
    source 28
    target 1645
  ]
  edge [
    source 28
    target 1646
  ]
  edge [
    source 28
    target 1647
  ]
  edge [
    source 28
    target 1648
  ]
  edge [
    source 28
    target 1649
  ]
  edge [
    source 28
    target 1650
  ]
  edge [
    source 28
    target 1651
  ]
  edge [
    source 28
    target 1652
  ]
  edge [
    source 28
    target 1653
  ]
  edge [
    source 28
    target 1654
  ]
  edge [
    source 28
    target 1655
  ]
  edge [
    source 28
    target 1656
  ]
  edge [
    source 28
    target 1657
  ]
  edge [
    source 28
    target 1658
  ]
  edge [
    source 28
    target 44
  ]
  edge [
    source 28
    target 754
  ]
  edge [
    source 28
    target 1659
  ]
  edge [
    source 28
    target 1660
  ]
  edge [
    source 28
    target 1661
  ]
  edge [
    source 28
    target 1662
  ]
  edge [
    source 28
    target 405
  ]
  edge [
    source 28
    target 1663
  ]
  edge [
    source 28
    target 1664
  ]
  edge [
    source 28
    target 987
  ]
  edge [
    source 28
    target 988
  ]
  edge [
    source 28
    target 1665
  ]
  edge [
    source 28
    target 1666
  ]
  edge [
    source 28
    target 1667
  ]
  edge [
    source 28
    target 66
  ]
  edge [
    source 28
    target 1668
  ]
  edge [
    source 28
    target 1669
  ]
  edge [
    source 28
    target 1670
  ]
  edge [
    source 28
    target 1671
  ]
  edge [
    source 28
    target 1672
  ]
  edge [
    source 28
    target 1120
  ]
  edge [
    source 28
    target 1673
  ]
  edge [
    source 28
    target 1674
  ]
  edge [
    source 28
    target 1675
  ]
  edge [
    source 28
    target 1676
  ]
  edge [
    source 28
    target 1677
  ]
  edge [
    source 28
    target 1678
  ]
  edge [
    source 28
    target 1679
  ]
  edge [
    source 28
    target 88
  ]
  edge [
    source 28
    target 1680
  ]
  edge [
    source 28
    target 1681
  ]
  edge [
    source 28
    target 631
  ]
  edge [
    source 28
    target 1682
  ]
  edge [
    source 28
    target 1683
  ]
  edge [
    source 28
    target 1579
  ]
  edge [
    source 28
    target 1684
  ]
  edge [
    source 28
    target 1685
  ]
  edge [
    source 28
    target 1686
  ]
  edge [
    source 28
    target 1687
  ]
  edge [
    source 28
    target 1688
  ]
  edge [
    source 28
    target 1689
  ]
  edge [
    source 28
    target 1690
  ]
  edge [
    source 28
    target 218
  ]
  edge [
    source 28
    target 1691
  ]
  edge [
    source 28
    target 1692
  ]
  edge [
    source 28
    target 176
  ]
  edge [
    source 28
    target 1045
  ]
  edge [
    source 28
    target 173
  ]
  edge [
    source 28
    target 1693
  ]
  edge [
    source 28
    target 1694
  ]
  edge [
    source 28
    target 1695
  ]
  edge [
    source 28
    target 1696
  ]
  edge [
    source 28
    target 1697
  ]
  edge [
    source 28
    target 1698
  ]
  edge [
    source 28
    target 1699
  ]
  edge [
    source 28
    target 1700
  ]
  edge [
    source 28
    target 1701
  ]
  edge [
    source 28
    target 1702
  ]
  edge [
    source 28
    target 1703
  ]
  edge [
    source 28
    target 1098
  ]
  edge [
    source 28
    target 689
  ]
  edge [
    source 28
    target 1005
  ]
  edge [
    source 28
    target 1704
  ]
  edge [
    source 28
    target 1705
  ]
  edge [
    source 28
    target 1706
  ]
  edge [
    source 28
    target 1707
  ]
  edge [
    source 28
    target 111
  ]
  edge [
    source 28
    target 1708
  ]
  edge [
    source 28
    target 1709
  ]
  edge [
    source 28
    target 1710
  ]
  edge [
    source 28
    target 1711
  ]
  edge [
    source 28
    target 1712
  ]
  edge [
    source 28
    target 1713
  ]
  edge [
    source 28
    target 1714
  ]
  edge [
    source 28
    target 1715
  ]
  edge [
    source 28
    target 1716
  ]
  edge [
    source 28
    target 1717
  ]
  edge [
    source 28
    target 1718
  ]
  edge [
    source 28
    target 676
  ]
  edge [
    source 28
    target 1719
  ]
  edge [
    source 29
    target 1720
  ]
  edge [
    source 29
    target 1721
  ]
  edge [
    source 29
    target 1722
  ]
  edge [
    source 29
    target 1723
  ]
  edge [
    source 29
    target 1724
  ]
  edge [
    source 29
    target 1725
  ]
  edge [
    source 29
    target 60
  ]
  edge [
    source 29
    target 1726
  ]
  edge [
    source 29
    target 1727
  ]
  edge [
    source 29
    target 1728
  ]
  edge [
    source 29
    target 1729
  ]
  edge [
    source 29
    target 1730
  ]
  edge [
    source 29
    target 1731
  ]
  edge [
    source 29
    target 1732
  ]
  edge [
    source 29
    target 1733
  ]
  edge [
    source 29
    target 1734
  ]
  edge [
    source 29
    target 1735
  ]
  edge [
    source 29
    target 1736
  ]
  edge [
    source 29
    target 1737
  ]
  edge [
    source 29
    target 1738
  ]
  edge [
    source 29
    target 1739
  ]
  edge [
    source 29
    target 1740
  ]
  edge [
    source 29
    target 1741
  ]
  edge [
    source 29
    target 1742
  ]
  edge [
    source 29
    target 1743
  ]
  edge [
    source 29
    target 1744
  ]
  edge [
    source 29
    target 1745
  ]
  edge [
    source 29
    target 1746
  ]
  edge [
    source 29
    target 1747
  ]
  edge [
    source 29
    target 80
  ]
  edge [
    source 29
    target 1748
  ]
  edge [
    source 29
    target 924
  ]
  edge [
    source 29
    target 1749
  ]
  edge [
    source 29
    target 464
  ]
  edge [
    source 29
    target 1750
  ]
  edge [
    source 29
    target 1751
  ]
  edge [
    source 29
    target 1752
  ]
  edge [
    source 29
    target 1753
  ]
  edge [
    source 29
    target 1754
  ]
  edge [
    source 29
    target 1755
  ]
  edge [
    source 29
    target 1756
  ]
  edge [
    source 29
    target 1757
  ]
  edge [
    source 29
    target 88
  ]
  edge [
    source 29
    target 689
  ]
  edge [
    source 29
    target 1758
  ]
  edge [
    source 29
    target 1759
  ]
  edge [
    source 29
    target 775
  ]
  edge [
    source 29
    target 1760
  ]
  edge [
    source 29
    target 1761
  ]
  edge [
    source 29
    target 1762
  ]
  edge [
    source 29
    target 1763
  ]
  edge [
    source 29
    target 1764
  ]
  edge [
    source 29
    target 1765
  ]
  edge [
    source 29
    target 1766
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 1767
  ]
  edge [
    source 31
    target 1768
  ]
  edge [
    source 31
    target 1769
  ]
  edge [
    source 31
    target 1770
  ]
  edge [
    source 31
    target 193
  ]
  edge [
    source 31
    target 1771
  ]
  edge [
    source 31
    target 1772
  ]
  edge [
    source 31
    target 1773
  ]
  edge [
    source 31
    target 1774
  ]
  edge [
    source 31
    target 1775
  ]
  edge [
    source 31
    target 1776
  ]
  edge [
    source 31
    target 686
  ]
  edge [
    source 31
    target 657
  ]
  edge [
    source 31
    target 1777
  ]
  edge [
    source 31
    target 1778
  ]
  edge [
    source 31
    target 80
  ]
  edge [
    source 31
    target 1779
  ]
  edge [
    source 31
    target 689
  ]
  edge [
    source 31
    target 1780
  ]
  edge [
    source 31
    target 1591
  ]
  edge [
    source 31
    target 1781
  ]
  edge [
    source 31
    target 1782
  ]
  edge [
    source 31
    target 1783
  ]
  edge [
    source 31
    target 1784
  ]
  edge [
    source 31
    target 1785
  ]
  edge [
    source 31
    target 1786
  ]
  edge [
    source 31
    target 1628
  ]
  edge [
    source 31
    target 1787
  ]
  edge [
    source 31
    target 1788
  ]
  edge [
    source 31
    target 1789
  ]
  edge [
    source 31
    target 1790
  ]
  edge [
    source 31
    target 1791
  ]
  edge [
    source 31
    target 1792
  ]
  edge [
    source 31
    target 1793
  ]
  edge [
    source 31
    target 1794
  ]
  edge [
    source 31
    target 1795
  ]
  edge [
    source 31
    target 1796
  ]
  edge [
    source 31
    target 1797
  ]
  edge [
    source 31
    target 1798
  ]
  edge [
    source 31
    target 1799
  ]
  edge [
    source 31
    target 1800
  ]
  edge [
    source 31
    target 1801
  ]
  edge [
    source 31
    target 873
  ]
  edge [
    source 31
    target 1802
  ]
  edge [
    source 31
    target 1803
  ]
  edge [
    source 31
    target 1804
  ]
  edge [
    source 31
    target 1274
  ]
  edge [
    source 31
    target 424
  ]
  edge [
    source 31
    target 101
  ]
  edge [
    source 31
    target 426
  ]
  edge [
    source 31
    target 1805
  ]
  edge [
    source 31
    target 1806
  ]
  edge [
    source 31
    target 1807
  ]
  edge [
    source 31
    target 1808
  ]
  edge [
    source 31
    target 762
  ]
  edge [
    source 31
    target 1809
  ]
  edge [
    source 31
    target 1810
  ]
  edge [
    source 31
    target 1811
  ]
  edge [
    source 31
    target 1812
  ]
  edge [
    source 31
    target 1813
  ]
  edge [
    source 31
    target 1814
  ]
  edge [
    source 31
    target 1815
  ]
  edge [
    source 31
    target 987
  ]
  edge [
    source 31
    target 988
  ]
  edge [
    source 31
    target 390
  ]
  edge [
    source 31
    target 66
  ]
  edge [
    source 31
    target 1816
  ]
  edge [
    source 31
    target 1817
  ]
  edge [
    source 31
    target 1818
  ]
  edge [
    source 31
    target 1819
  ]
  edge [
    source 31
    target 1820
  ]
  edge [
    source 31
    target 1125
  ]
  edge [
    source 31
    target 347
  ]
  edge [
    source 31
    target 1821
  ]
  edge [
    source 31
    target 745
  ]
  edge [
    source 31
    target 1822
  ]
  edge [
    source 31
    target 1823
  ]
  edge [
    source 31
    target 1824
  ]
  edge [
    source 31
    target 1825
  ]
  edge [
    source 31
    target 1826
  ]
  edge [
    source 31
    target 1827
  ]
  edge [
    source 31
    target 1828
  ]
  edge [
    source 31
    target 1829
  ]
  edge [
    source 31
    target 1830
  ]
  edge [
    source 31
    target 1831
  ]
  edge [
    source 31
    target 701
  ]
  edge [
    source 31
    target 1832
  ]
  edge [
    source 31
    target 1833
  ]
  edge [
    source 31
    target 698
  ]
  edge [
    source 31
    target 679
  ]
  edge [
    source 31
    target 1834
  ]
  edge [
    source 31
    target 1835
  ]
  edge [
    source 31
    target 1836
  ]
  edge [
    source 31
    target 1837
  ]
  edge [
    source 31
    target 1660
  ]
  edge [
    source 31
    target 1838
  ]
  edge [
    source 31
    target 1839
  ]
  edge [
    source 31
    target 1840
  ]
  edge [
    source 31
    target 1841
  ]
  edge [
    source 31
    target 1842
  ]
  edge [
    source 31
    target 1659
  ]
  edge [
    source 31
    target 675
  ]
  edge [
    source 31
    target 1843
  ]
  edge [
    source 31
    target 1844
  ]
  edge [
    source 31
    target 1845
  ]
  edge [
    source 31
    target 498
  ]
  edge [
    source 31
    target 1134
  ]
  edge [
    source 31
    target 1846
  ]
  edge [
    source 31
    target 1847
  ]
  edge [
    source 31
    target 1848
  ]
  edge [
    source 31
    target 1849
  ]
  edge [
    source 31
    target 1131
  ]
  edge [
    source 31
    target 1850
  ]
  edge [
    source 31
    target 1851
  ]
  edge [
    source 31
    target 1852
  ]
  edge [
    source 31
    target 656
  ]
  edge [
    source 31
    target 659
  ]
  edge [
    source 31
    target 1853
  ]
  edge [
    source 31
    target 1854
  ]
  edge [
    source 31
    target 1855
  ]
  edge [
    source 31
    target 1856
  ]
  edge [
    source 31
    target 1857
  ]
  edge [
    source 31
    target 663
  ]
  edge [
    source 31
    target 1858
  ]
  edge [
    source 31
    target 655
  ]
  edge [
    source 31
    target 658
  ]
  edge [
    source 31
    target 1859
  ]
  edge [
    source 31
    target 660
  ]
  edge [
    source 31
    target 215
  ]
  edge [
    source 31
    target 1860
  ]
  edge [
    source 31
    target 1861
  ]
  edge [
    source 31
    target 662
  ]
  edge [
    source 31
    target 613
  ]
  edge [
    source 31
    target 661
  ]
  edge [
    source 31
    target 1862
  ]
  edge [
    source 31
    target 1863
  ]
  edge [
    source 31
    target 1864
  ]
  edge [
    source 31
    target 186
  ]
  edge [
    source 31
    target 664
  ]
  edge [
    source 31
    target 856
  ]
  edge [
    source 31
    target 1865
  ]
  edge [
    source 31
    target 517
  ]
  edge [
    source 31
    target 1866
  ]
  edge [
    source 31
    target 1867
  ]
  edge [
    source 31
    target 1729
  ]
  edge [
    source 31
    target 88
  ]
  edge [
    source 31
    target 1868
  ]
  edge [
    source 31
    target 1695
  ]
  edge [
    source 31
    target 1869
  ]
  edge [
    source 31
    target 1870
  ]
  edge [
    source 31
    target 1871
  ]
  edge [
    source 31
    target 120
  ]
  edge [
    source 31
    target 673
  ]
  edge [
    source 31
    target 1872
  ]
  edge [
    source 31
    target 1873
  ]
  edge [
    source 31
    target 1874
  ]
  edge [
    source 31
    target 1579
  ]
  edge [
    source 31
    target 1875
  ]
  edge [
    source 31
    target 694
  ]
  edge [
    source 31
    target 1876
  ]
  edge [
    source 31
    target 1877
  ]
  edge [
    source 31
    target 304
  ]
  edge [
    source 31
    target 1878
  ]
  edge [
    source 31
    target 1879
  ]
  edge [
    source 31
    target 1727
  ]
  edge [
    source 31
    target 1880
  ]
  edge [
    source 31
    target 1881
  ]
  edge [
    source 31
    target 1611
  ]
  edge [
    source 31
    target 1882
  ]
  edge [
    source 31
    target 1700
  ]
  edge [
    source 31
    target 1883
  ]
  edge [
    source 31
    target 1884
  ]
  edge [
    source 31
    target 1587
  ]
  edge [
    source 31
    target 1885
  ]
  edge [
    source 31
    target 1886
  ]
  edge [
    source 31
    target 1887
  ]
  edge [
    source 31
    target 1888
  ]
  edge [
    source 31
    target 1889
  ]
  edge [
    source 31
    target 1890
  ]
  edge [
    source 31
    target 1891
  ]
  edge [
    source 31
    target 1892
  ]
  edge [
    source 31
    target 1893
  ]
  edge [
    source 31
    target 1894
  ]
  edge [
    source 31
    target 1037
  ]
  edge [
    source 31
    target 1895
  ]
  edge [
    source 31
    target 1896
  ]
  edge [
    source 31
    target 309
  ]
  edge [
    source 31
    target 1897
  ]
  edge [
    source 31
    target 1898
  ]
  edge [
    source 31
    target 1899
  ]
  edge [
    source 31
    target 1900
  ]
  edge [
    source 31
    target 1901
  ]
  edge [
    source 31
    target 1902
  ]
  edge [
    source 31
    target 1903
  ]
  edge [
    source 31
    target 1904
  ]
  edge [
    source 31
    target 1905
  ]
  edge [
    source 31
    target 1906
  ]
  edge [
    source 31
    target 1907
  ]
  edge [
    source 31
    target 1908
  ]
  edge [
    source 31
    target 1909
  ]
  edge [
    source 31
    target 1910
  ]
  edge [
    source 31
    target 1911
  ]
  edge [
    source 31
    target 1596
  ]
  edge [
    source 31
    target 1912
  ]
  edge [
    source 31
    target 1913
  ]
  edge [
    source 31
    target 1914
  ]
  edge [
    source 31
    target 1915
  ]
  edge [
    source 31
    target 1916
  ]
  edge [
    source 31
    target 1917
  ]
  edge [
    source 31
    target 1918
  ]
  edge [
    source 31
    target 1919
  ]
  edge [
    source 31
    target 1920
  ]
  edge [
    source 31
    target 754
  ]
  edge [
    source 31
    target 171
  ]
  edge [
    source 31
    target 1921
  ]
  edge [
    source 31
    target 173
  ]
  edge [
    source 31
    target 1922
  ]
  edge [
    source 31
    target 1923
  ]
  edge [
    source 31
    target 1684
  ]
  edge [
    source 31
    target 1924
  ]
  edge [
    source 31
    target 1925
  ]
  edge [
    source 31
    target 1926
  ]
  edge [
    source 31
    target 1689
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1927
  ]
  edge [
    source 32
    target 1928
  ]
  edge [
    source 32
    target 1727
  ]
  edge [
    source 32
    target 1929
  ]
  edge [
    source 32
    target 1919
  ]
  edge [
    source 33
    target 1930
  ]
  edge [
    source 33
    target 1931
  ]
  edge [
    source 33
    target 1932
  ]
  edge [
    source 33
    target 1933
  ]
  edge [
    source 33
    target 1934
  ]
  edge [
    source 33
    target 1935
  ]
  edge [
    source 33
    target 1936
  ]
  edge [
    source 33
    target 1937
  ]
  edge [
    source 33
    target 1938
  ]
  edge [
    source 33
    target 1939
  ]
  edge [
    source 33
    target 1940
  ]
  edge [
    source 33
    target 1941
  ]
  edge [
    source 33
    target 1942
  ]
  edge [
    source 33
    target 1943
  ]
  edge [
    source 33
    target 1944
  ]
  edge [
    source 33
    target 1945
  ]
  edge [
    source 33
    target 1946
  ]
  edge [
    source 33
    target 1947
  ]
  edge [
    source 33
    target 1948
  ]
  edge [
    source 33
    target 1949
  ]
  edge [
    source 33
    target 1950
  ]
  edge [
    source 33
    target 1951
  ]
  edge [
    source 33
    target 1952
  ]
  edge [
    source 33
    target 1953
  ]
  edge [
    source 33
    target 1954
  ]
  edge [
    source 33
    target 1955
  ]
  edge [
    source 33
    target 1956
  ]
  edge [
    source 33
    target 1957
  ]
  edge [
    source 33
    target 1958
  ]
  edge [
    source 33
    target 1959
  ]
  edge [
    source 33
    target 1960
  ]
  edge [
    source 33
    target 1961
  ]
  edge [
    source 33
    target 1962
  ]
  edge [
    source 33
    target 1963
  ]
  edge [
    source 33
    target 1964
  ]
  edge [
    source 33
    target 1965
  ]
  edge [
    source 33
    target 1966
  ]
  edge [
    source 33
    target 1967
  ]
  edge [
    source 33
    target 1968
  ]
  edge [
    source 33
    target 1969
  ]
  edge [
    source 33
    target 1970
  ]
  edge [
    source 33
    target 143
  ]
  edge [
    source 33
    target 1971
  ]
  edge [
    source 33
    target 1972
  ]
  edge [
    source 33
    target 1973
  ]
  edge [
    source 33
    target 1974
  ]
  edge [
    source 33
    target 1975
  ]
  edge [
    source 33
    target 1976
  ]
  edge [
    source 33
    target 1977
  ]
  edge [
    source 33
    target 1978
  ]
  edge [
    source 33
    target 1979
  ]
  edge [
    source 33
    target 1980
  ]
  edge [
    source 33
    target 1981
  ]
  edge [
    source 33
    target 1982
  ]
  edge [
    source 34
    target 1983
  ]
  edge [
    source 34
    target 1984
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1985
  ]
  edge [
    source 35
    target 1986
  ]
  edge [
    source 35
    target 1987
  ]
  edge [
    source 36
    target 657
  ]
  edge [
    source 36
    target 1988
  ]
  edge [
    source 36
    target 77
  ]
  edge [
    source 36
    target 1989
  ]
  edge [
    source 36
    target 1701
  ]
  edge [
    source 36
    target 1990
  ]
  edge [
    source 36
    target 1689
  ]
  edge [
    source 36
    target 1991
  ]
  edge [
    source 36
    target 1992
  ]
  edge [
    source 36
    target 1993
  ]
  edge [
    source 36
    target 1994
  ]
  edge [
    source 36
    target 1995
  ]
  edge [
    source 36
    target 1996
  ]
  edge [
    source 36
    target 1997
  ]
  edge [
    source 36
    target 1628
  ]
  edge [
    source 36
    target 1998
  ]
  edge [
    source 36
    target 1999
  ]
  edge [
    source 36
    target 2000
  ]
  edge [
    source 36
    target 2001
  ]
  edge [
    source 36
    target 2002
  ]
  edge [
    source 36
    target 2003
  ]
  edge [
    source 36
    target 2004
  ]
  edge [
    source 36
    target 2005
  ]
  edge [
    source 36
    target 2006
  ]
  edge [
    source 36
    target 2007
  ]
  edge [
    source 36
    target 2008
  ]
  edge [
    source 36
    target 2009
  ]
  edge [
    source 36
    target 2010
  ]
  edge [
    source 36
    target 2011
  ]
  edge [
    source 36
    target 2012
  ]
  edge [
    source 36
    target 2013
  ]
  edge [
    source 36
    target 2014
  ]
  edge [
    source 36
    target 2015
  ]
  edge [
    source 36
    target 2016
  ]
  edge [
    source 36
    target 2017
  ]
  edge [
    source 36
    target 2018
  ]
  edge [
    source 36
    target 2019
  ]
  edge [
    source 36
    target 86
  ]
  edge [
    source 36
    target 2020
  ]
  edge [
    source 36
    target 747
  ]
  edge [
    source 36
    target 2021
  ]
  edge [
    source 36
    target 2022
  ]
  edge [
    source 36
    target 2023
  ]
  edge [
    source 36
    target 2024
  ]
  edge [
    source 36
    target 2025
  ]
  edge [
    source 36
    target 629
  ]
  edge [
    source 36
    target 173
  ]
  edge [
    source 36
    target 985
  ]
  edge [
    source 36
    target 2026
  ]
  edge [
    source 36
    target 900
  ]
  edge [
    source 36
    target 2027
  ]
  edge [
    source 36
    target 134
  ]
  edge [
    source 36
    target 57
  ]
  edge [
    source 36
    target 2028
  ]
  edge [
    source 36
    target 172
  ]
  edge [
    source 36
    target 176
  ]
  edge [
    source 36
    target 2029
  ]
  edge [
    source 36
    target 656
  ]
  edge [
    source 36
    target 659
  ]
  edge [
    source 36
    target 1853
  ]
  edge [
    source 36
    target 1854
  ]
  edge [
    source 36
    target 1855
  ]
  edge [
    source 36
    target 1856
  ]
  edge [
    source 36
    target 1857
  ]
  edge [
    source 36
    target 762
  ]
  edge [
    source 36
    target 663
  ]
  edge [
    source 36
    target 1858
  ]
  edge [
    source 36
    target 655
  ]
  edge [
    source 36
    target 658
  ]
  edge [
    source 36
    target 1859
  ]
  edge [
    source 36
    target 660
  ]
  edge [
    source 36
    target 215
  ]
  edge [
    source 36
    target 1860
  ]
  edge [
    source 36
    target 1861
  ]
  edge [
    source 36
    target 662
  ]
  edge [
    source 36
    target 613
  ]
  edge [
    source 36
    target 661
  ]
  edge [
    source 36
    target 1862
  ]
  edge [
    source 36
    target 1863
  ]
  edge [
    source 36
    target 1781
  ]
  edge [
    source 36
    target 1864
  ]
  edge [
    source 36
    target 347
  ]
  edge [
    source 36
    target 186
  ]
  edge [
    source 36
    target 664
  ]
  edge [
    source 36
    target 2030
  ]
  edge [
    source 36
    target 2031
  ]
  edge [
    source 36
    target 1642
  ]
  edge [
    source 36
    target 2032
  ]
  edge [
    source 36
    target 1046
  ]
  edge [
    source 36
    target 2033
  ]
  edge [
    source 36
    target 674
  ]
  edge [
    source 36
    target 1635
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 2034
  ]
  edge [
    source 38
    target 2035
  ]
  edge [
    source 38
    target 2036
  ]
  edge [
    source 38
    target 2037
  ]
  edge [
    source 38
    target 2038
  ]
  edge [
    source 38
    target 2039
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 2040
  ]
  edge [
    source 39
    target 2041
  ]
  edge [
    source 39
    target 2042
  ]
  edge [
    source 39
    target 1015
  ]
  edge [
    source 39
    target 2043
  ]
  edge [
    source 39
    target 2044
  ]
  edge [
    source 39
    target 2045
  ]
  edge [
    source 39
    target 2046
  ]
  edge [
    source 39
    target 2047
  ]
  edge [
    source 39
    target 2048
  ]
  edge [
    source 39
    target 2049
  ]
  edge [
    source 39
    target 2050
  ]
  edge [
    source 39
    target 2051
  ]
  edge [
    source 39
    target 2052
  ]
  edge [
    source 39
    target 2053
  ]
  edge [
    source 39
    target 2054
  ]
  edge [
    source 39
    target 2055
  ]
  edge [
    source 39
    target 2056
  ]
  edge [
    source 39
    target 2057
  ]
  edge [
    source 39
    target 2058
  ]
  edge [
    source 39
    target 2059
  ]
  edge [
    source 39
    target 2060
  ]
  edge [
    source 39
    target 2061
  ]
  edge [
    source 39
    target 2062
  ]
  edge [
    source 39
    target 2063
  ]
  edge [
    source 39
    target 2064
  ]
  edge [
    source 39
    target 2065
  ]
  edge [
    source 39
    target 2066
  ]
  edge [
    source 39
    target 2067
  ]
  edge [
    source 39
    target 2068
  ]
  edge [
    source 39
    target 2069
  ]
  edge [
    source 39
    target 2070
  ]
  edge [
    source 39
    target 2071
  ]
  edge [
    source 39
    target 2072
  ]
  edge [
    source 39
    target 2073
  ]
  edge [
    source 39
    target 2074
  ]
  edge [
    source 39
    target 2075
  ]
  edge [
    source 39
    target 2076
  ]
  edge [
    source 39
    target 2077
  ]
  edge [
    source 39
    target 2078
  ]
  edge [
    source 39
    target 2079
  ]
  edge [
    source 39
    target 2080
  ]
  edge [
    source 39
    target 2081
  ]
  edge [
    source 39
    target 2082
  ]
  edge [
    source 39
    target 2083
  ]
  edge [
    source 39
    target 2084
  ]
  edge [
    source 39
    target 2085
  ]
  edge [
    source 39
    target 2086
  ]
  edge [
    source 39
    target 2087
  ]
  edge [
    source 39
    target 2088
  ]
  edge [
    source 39
    target 2089
  ]
  edge [
    source 39
    target 2090
  ]
  edge [
    source 39
    target 2091
  ]
  edge [
    source 39
    target 1392
  ]
  edge [
    source 39
    target 2092
  ]
  edge [
    source 39
    target 2093
  ]
  edge [
    source 39
    target 2094
  ]
  edge [
    source 39
    target 2095
  ]
  edge [
    source 39
    target 2096
  ]
  edge [
    source 39
    target 2097
  ]
  edge [
    source 39
    target 2098
  ]
  edge [
    source 39
    target 2099
  ]
  edge [
    source 39
    target 2100
  ]
  edge [
    source 40
    target 2101
  ]
  edge [
    source 40
    target 1268
  ]
  edge [
    source 40
    target 2102
  ]
  edge [
    source 40
    target 190
  ]
  edge [
    source 40
    target 2103
  ]
  edge [
    source 40
    target 2104
  ]
  edge [
    source 40
    target 2105
  ]
  edge [
    source 40
    target 2106
  ]
  edge [
    source 40
    target 2107
  ]
  edge [
    source 40
    target 2108
  ]
  edge [
    source 40
    target 2109
  ]
  edge [
    source 40
    target 1059
  ]
  edge [
    source 40
    target 2110
  ]
  edge [
    source 40
    target 1267
  ]
  edge [
    source 40
    target 213
  ]
  edge [
    source 40
    target 66
  ]
  edge [
    source 40
    target 1269
  ]
  edge [
    source 40
    target 1270
  ]
  edge [
    source 40
    target 218
  ]
  edge [
    source 40
    target 1272
  ]
  edge [
    source 40
    target 1271
  ]
  edge [
    source 40
    target 1273
  ]
  edge [
    source 40
    target 1274
  ]
  edge [
    source 40
    target 1275
  ]
  edge [
    source 40
    target 1276
  ]
  edge [
    source 40
    target 1277
  ]
  edge [
    source 40
    target 1278
  ]
  edge [
    source 40
    target 1279
  ]
  edge [
    source 40
    target 1280
  ]
  edge [
    source 40
    target 1281
  ]
  edge [
    source 40
    target 1282
  ]
  edge [
    source 40
    target 1283
  ]
  edge [
    source 40
    target 1128
  ]
  edge [
    source 40
    target 1129
  ]
  edge [
    source 40
    target 1130
  ]
  edge [
    source 40
    target 1131
  ]
  edge [
    source 40
    target 1132
  ]
  edge [
    source 40
    target 1133
  ]
  edge [
    source 40
    target 1134
  ]
  edge [
    source 40
    target 655
  ]
  edge [
    source 40
    target 1135
  ]
  edge [
    source 40
    target 1083
  ]
  edge [
    source 40
    target 1136
  ]
  edge [
    source 40
    target 1137
  ]
  edge [
    source 40
    target 1138
  ]
  edge [
    source 40
    target 1139
  ]
  edge [
    source 40
    target 1140
  ]
  edge [
    source 40
    target 1141
  ]
  edge [
    source 40
    target 1142
  ]
  edge [
    source 40
    target 1143
  ]
  edge [
    source 40
    target 1144
  ]
  edge [
    source 40
    target 1145
  ]
  edge [
    source 40
    target 1146
  ]
  edge [
    source 40
    target 1147
  ]
  edge [
    source 40
    target 1148
  ]
  edge [
    source 40
    target 167
  ]
  edge [
    source 40
    target 1149
  ]
  edge [
    source 40
    target 1150
  ]
  edge [
    source 40
    target 1151
  ]
  edge [
    source 40
    target 1152
  ]
  edge [
    source 40
    target 2111
  ]
  edge [
    source 40
    target 2112
  ]
  edge [
    source 40
    target 2113
  ]
  edge [
    source 924
    target 2139
  ]
  edge [
    source 924
    target 2140
  ]
  edge [
    source 1009
    target 2125
  ]
  edge [
    source 1009
    target 2126
  ]
  edge [
    source 1009
    target 2141
  ]
  edge [
    source 1038
    target 1696
  ]
  edge [
    source 1038
    target 2115
  ]
  edge [
    source 1038
    target 2116
  ]
  edge [
    source 1038
    target 2117
  ]
  edge [
    source 1038
    target 2118
  ]
  edge [
    source 1045
    target 2119
  ]
  edge [
    source 1045
    target 2120
  ]
  edge [
    source 1696
    target 2115
  ]
  edge [
    source 1696
    target 2116
  ]
  edge [
    source 1696
    target 2117
  ]
  edge [
    source 1696
    target 2118
  ]
  edge [
    source 2115
    target 2116
  ]
  edge [
    source 2115
    target 2117
  ]
  edge [
    source 2115
    target 2118
  ]
  edge [
    source 2115
    target 2133
  ]
  edge [
    source 2115
    target 2134
  ]
  edge [
    source 2115
    target 2135
  ]
  edge [
    source 2115
    target 2136
  ]
  edge [
    source 2115
    target 2137
  ]
  edge [
    source 2116
    target 2117
  ]
  edge [
    source 2116
    target 2118
  ]
  edge [
    source 2117
    target 2118
  ]
  edge [
    source 2117
    target 2133
  ]
  edge [
    source 2117
    target 2134
  ]
  edge [
    source 2117
    target 2135
  ]
  edge [
    source 2117
    target 2136
  ]
  edge [
    source 2117
    target 2137
  ]
  edge [
    source 2119
    target 2120
  ]
  edge [
    source 2119
    target 2123
  ]
  edge [
    source 2119
    target 2124
  ]
  edge [
    source 2121
    target 2122
  ]
  edge [
    source 2123
    target 2124
  ]
  edge [
    source 2125
    target 2126
  ]
  edge [
    source 2127
    target 2128
  ]
  edge [
    source 2127
    target 2129
  ]
  edge [
    source 2127
    target 2130
  ]
  edge [
    source 2127
    target 2131
  ]
  edge [
    source 2127
    target 2132
  ]
  edge [
    source 2128
    target 2129
  ]
  edge [
    source 2128
    target 2130
  ]
  edge [
    source 2128
    target 2131
  ]
  edge [
    source 2128
    target 2132
  ]
  edge [
    source 2129
    target 2130
  ]
  edge [
    source 2129
    target 2131
  ]
  edge [
    source 2129
    target 2132
  ]
  edge [
    source 2130
    target 2131
  ]
  edge [
    source 2130
    target 2132
  ]
  edge [
    source 2131
    target 2132
  ]
  edge [
    source 2133
    target 2134
  ]
  edge [
    source 2133
    target 2135
  ]
  edge [
    source 2133
    target 2136
  ]
  edge [
    source 2133
    target 2137
  ]
  edge [
    source 2134
    target 2135
  ]
  edge [
    source 2134
    target 2136
  ]
  edge [
    source 2134
    target 2137
  ]
  edge [
    source 2134
    target 2138
  ]
  edge [
    source 2135
    target 2136
  ]
  edge [
    source 2135
    target 2137
  ]
  edge [
    source 2136
    target 2137
  ]
  edge [
    source 2139
    target 2140
  ]
  edge [
    source 2142
    target 2143
  ]
  edge [
    source 2142
    target 2144
  ]
  edge [
    source 2143
    target 2144
  ]
]
