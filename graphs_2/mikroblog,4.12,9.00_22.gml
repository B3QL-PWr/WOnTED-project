graph [
  node [
    id 0
    label "contentnadzis"
    origin "text"
  ]
  node [
    id 1
    label "codziennaafrica"
    origin "text"
  ]
  node [
    id 2
    label "totoafrica"
    origin "text"
  ]
  node [
    id 3
    label "dziendobry"
    origin "text"
  ]
  node [
    id 4
    label "cytatnadzis"
    origin "text"
  ]
  node [
    id 5
    label "ciekawostkanadzis"
    origin "text"
  ]
  node [
    id 6
    label "pomyslnaobiad"
    origin "text"
  ]
  node [
    id 7
    label "pogodanadzis"
    origin "text"
  ]
  node [
    id 8
    label "druga"
  ]
  node [
    id 9
    label "wojna"
  ]
  node [
    id 10
    label "&#347;wiatowy"
  ]
  node [
    id 11
    label "sos"
  ]
  node [
    id 12
    label "z"
  ]
  node [
    id 13
    label "gorgonzola"
  ]
  node [
    id 14
    label "sk&#322;adnik"
  ]
  node [
    id 15
    label "filet"
  ]
  node [
    id 16
    label "drobiowy"
  ]
  node [
    id 17
    label "w"
  ]
  node [
    id 18
    label "wiatr"
  ]
  node [
    id 19
    label "P&#322;d"
  ]
  node [
    id 20
    label "P&#322;n"
  ]
  node [
    id 21
    label "Zach"
  ]
  node [
    id 22
    label "15"
  ]
  node [
    id 23
    label "11"
  ]
  node [
    id 24
    label "km"
  ]
  node [
    id 25
    label "27"
  ]
  node [
    id 26
    label "12"
  ]
  node [
    id 27
    label "28"
  ]
  node [
    id 28
    label "h"
  ]
  node [
    id 29
    label "&#2039;"
  ]
  node [
    id 30
    label "Bydgoszcz"
  ]
  node [
    id 31
    label "29"
  ]
  node [
    id 32
    label "26"
  ]
  node [
    id 33
    label "23"
  ]
  node [
    id 34
    label "33"
  ]
  node [
    id 35
    label "Gda&#324;sk"
  ]
  node [
    id 36
    label "37"
  ]
  node [
    id 37
    label "43"
  ]
  node [
    id 38
    label "24"
  ]
  node [
    id 39
    label "Gorz&#243;w"
  ]
  node [
    id 40
    label "Wlkp"
  ]
  node [
    id 41
    label "22"
  ]
  node [
    id 42
    label "25"
  ]
  node [
    id 43
    label "Katowice"
  ]
  node [
    id 44
    label "20"
  ]
  node [
    id 45
    label "Krak"
  ]
  node [
    id 46
    label "18"
  ]
  node [
    id 47
    label "17"
  ]
  node [
    id 48
    label "14"
  ]
  node [
    id 49
    label "21"
  ]
  node [
    id 50
    label "19"
  ]
  node [
    id 51
    label "&#322;&#243;d&#378;"
  ]
  node [
    id 52
    label "13"
  ]
  node [
    id 53
    label "Olsztyn"
  ]
  node [
    id 54
    label "opole"
  ]
  node [
    id 55
    label "30"
  ]
  node [
    id 56
    label "Rzesz&#243;w"
  ]
  node [
    id 57
    label "10"
  ]
  node [
    id 58
    label "16"
  ]
  node [
    id 59
    label "warszawa"
  ]
  node [
    id 60
    label "Wroc&#322;aw"
  ]
  node [
    id 61
    label "zielony"
  ]
  node [
    id 62
    label "G&#243;ra"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 18
    target 25
  ]
  edge [
    source 18
    target 26
  ]
  edge [
    source 18
    target 27
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 29
  ]
  edge [
    source 18
    target 30
  ]
  edge [
    source 18
    target 31
  ]
  edge [
    source 18
    target 32
  ]
  edge [
    source 18
    target 33
  ]
  edge [
    source 18
    target 34
  ]
  edge [
    source 18
    target 35
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 37
  ]
  edge [
    source 18
    target 38
  ]
  edge [
    source 18
    target 39
  ]
  edge [
    source 18
    target 40
  ]
  edge [
    source 18
    target 41
  ]
  edge [
    source 18
    target 42
  ]
  edge [
    source 18
    target 43
  ]
  edge [
    source 18
    target 44
  ]
  edge [
    source 18
    target 45
  ]
  edge [
    source 18
    target 46
  ]
  edge [
    source 18
    target 47
  ]
  edge [
    source 18
    target 48
  ]
  edge [
    source 18
    target 49
  ]
  edge [
    source 18
    target 50
  ]
  edge [
    source 18
    target 51
  ]
  edge [
    source 18
    target 52
  ]
  edge [
    source 18
    target 53
  ]
  edge [
    source 18
    target 54
  ]
  edge [
    source 18
    target 55
  ]
  edge [
    source 18
    target 56
  ]
  edge [
    source 18
    target 57
  ]
  edge [
    source 18
    target 58
  ]
  edge [
    source 18
    target 59
  ]
  edge [
    source 18
    target 60
  ]
  edge [
    source 18
    target 61
  ]
  edge [
    source 18
    target 62
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 50
  ]
  edge [
    source 19
    target 41
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 19
    target 58
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 20
    target 33
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 20
    target 27
  ]
  edge [
    source 20
    target 44
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 20
    target 29
  ]
  edge [
    source 20
    target 45
  ]
  edge [
    source 20
    target 46
  ]
  edge [
    source 20
    target 48
  ]
  edge [
    source 20
    target 51
  ]
  edge [
    source 20
    target 31
  ]
  edge [
    source 20
    target 42
  ]
  edge [
    source 20
    target 53
  ]
  edge [
    source 20
    target 50
  ]
  edge [
    source 20
    target 49
  ]
  edge [
    source 20
    target 55
  ]
  edge [
    source 20
    target 41
  ]
  edge [
    source 20
    target 61
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 21
    target 31
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 21
    target 36
  ]
  edge [
    source 21
    target 37
  ]
  edge [
    source 21
    target 38
  ]
  edge [
    source 21
    target 39
  ]
  edge [
    source 21
    target 40
  ]
  edge [
    source 21
    target 41
  ]
  edge [
    source 21
    target 42
  ]
  edge [
    source 21
    target 43
  ]
  edge [
    source 21
    target 44
  ]
  edge [
    source 21
    target 45
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 46
  ]
  edge [
    source 21
    target 47
  ]
  edge [
    source 21
    target 48
  ]
  edge [
    source 21
    target 49
  ]
  edge [
    source 21
    target 50
  ]
  edge [
    source 21
    target 51
  ]
  edge [
    source 21
    target 52
  ]
  edge [
    source 21
    target 53
  ]
  edge [
    source 21
    target 54
  ]
  edge [
    source 21
    target 55
  ]
  edge [
    source 21
    target 56
  ]
  edge [
    source 21
    target 57
  ]
  edge [
    source 21
    target 59
  ]
  edge [
    source 21
    target 60
  ]
  edge [
    source 21
    target 61
  ]
  edge [
    source 21
    target 62
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 24
    target 37
  ]
  edge [
    source 24
    target 39
  ]
  edge [
    source 24
    target 40
  ]
  edge [
    source 24
    target 41
  ]
  edge [
    source 24
    target 42
  ]
  edge [
    source 24
    target 43
  ]
  edge [
    source 24
    target 44
  ]
  edge [
    source 24
    target 45
  ]
  edge [
    source 24
    target 46
  ]
  edge [
    source 24
    target 47
  ]
  edge [
    source 24
    target 48
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 51
  ]
  edge [
    source 24
    target 53
  ]
  edge [
    source 24
    target 54
  ]
  edge [
    source 24
    target 55
  ]
  edge [
    source 24
    target 38
  ]
  edge [
    source 24
    target 56
  ]
  edge [
    source 24
    target 59
  ]
  edge [
    source 24
    target 60
  ]
  edge [
    source 24
    target 61
  ]
  edge [
    source 24
    target 62
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 51
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 28
    target 37
  ]
  edge [
    source 28
    target 39
  ]
  edge [
    source 28
    target 40
  ]
  edge [
    source 28
    target 42
  ]
  edge [
    source 28
    target 43
  ]
  edge [
    source 28
    target 44
  ]
  edge [
    source 28
    target 45
  ]
  edge [
    source 28
    target 51
  ]
  edge [
    source 28
    target 53
  ]
  edge [
    source 28
    target 54
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 28
    target 56
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 59
  ]
  edge [
    source 28
    target 60
  ]
  edge [
    source 28
    target 61
  ]
  edge [
    source 28
    target 62
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 29
    target 37
  ]
  edge [
    source 29
    target 39
  ]
  edge [
    source 29
    target 40
  ]
  edge [
    source 29
    target 42
  ]
  edge [
    source 29
    target 43
  ]
  edge [
    source 29
    target 44
  ]
  edge [
    source 29
    target 45
  ]
  edge [
    source 29
    target 51
  ]
  edge [
    source 29
    target 53
  ]
  edge [
    source 29
    target 54
  ]
  edge [
    source 29
    target 38
  ]
  edge [
    source 29
    target 56
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 59
  ]
  edge [
    source 29
    target 60
  ]
  edge [
    source 29
    target 61
  ]
  edge [
    source 29
    target 62
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 54
  ]
  edge [
    source 32
    target 59
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 37
    target 40
  ]
  edge [
    source 38
    target 56
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 53
  ]
  edge [
    source 42
    target 60
  ]
  edge [
    source 42
    target 61
  ]
  edge [
    source 42
    target 62
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 61
    target 62
  ]
]
