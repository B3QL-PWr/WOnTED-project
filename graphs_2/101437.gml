graph [
  node [
    id 0
    label "the"
    origin "text"
  ]
  node [
    id 1
    label "best"
    origin "text"
  ]
  node [
    id 2
    label "deep"
    origin "text"
  ]
  node [
    id 3
    label "purple"
    origin "text"
  ]
  node [
    id 4
    label "live"
    origin "text"
  ]
  node [
    id 5
    label "europe"
    origin "text"
  ]
  node [
    id 6
    label "The"
  ]
  node [
    id 7
    label "Best"
  ]
  node [
    id 8
    label "of"
  ]
  node [
    id 9
    label "Deep"
  ]
  node [
    id 10
    label "Purple"
  ]
  node [
    id 11
    label "Live"
  ]
  node [
    id 12
    label "inny"
  ]
  node [
    id 13
    label "Europe"
  ]
  node [
    id 14
    label "Ian"
  ]
  node [
    id 15
    label "Gillan"
  ]
  node [
    id 16
    label "Ritchie"
  ]
  node [
    id 17
    label "Blackmore"
  ]
  node [
    id 18
    label "jon"
  ]
  node [
    id 19
    label "lord"
  ]
  node [
    id 20
    label "Roger"
  ]
  node [
    id 21
    label "Glover"
  ]
  node [
    id 22
    label "Paice"
  ]
  node [
    id 23
    label "David"
  ]
  node [
    id 24
    label "Coverdale"
  ]
  node [
    id 25
    label "Glenn"
  ]
  node [
    id 26
    label "Hughes"
  ]
  node [
    id 27
    label "Steve"
  ]
  node [
    id 28
    label "Morse"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
]
