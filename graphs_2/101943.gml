graph [
  node [
    id 0
    label "art"
    origin "text"
  ]
  node [
    id 1
    label "otrzymywa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "brzmienie"
    origin "text"
  ]
  node [
    id 3
    label "return"
  ]
  node [
    id 4
    label "dostawa&#263;"
  ]
  node [
    id 5
    label "take"
  ]
  node [
    id 6
    label "wytwarza&#263;"
  ]
  node [
    id 7
    label "mie&#263;_miejsce"
  ]
  node [
    id 8
    label "by&#263;"
  ]
  node [
    id 9
    label "nabywa&#263;"
  ]
  node [
    id 10
    label "uzyskiwa&#263;"
  ]
  node [
    id 11
    label "bra&#263;"
  ]
  node [
    id 12
    label "winnings"
  ]
  node [
    id 13
    label "opanowywa&#263;"
  ]
  node [
    id 14
    label "si&#281;ga&#263;"
  ]
  node [
    id 15
    label "range"
  ]
  node [
    id 16
    label "wystarcza&#263;"
  ]
  node [
    id 17
    label "kupowa&#263;"
  ]
  node [
    id 18
    label "obskakiwa&#263;"
  ]
  node [
    id 19
    label "create"
  ]
  node [
    id 20
    label "give"
  ]
  node [
    id 21
    label "robi&#263;"
  ]
  node [
    id 22
    label "wyra&#380;anie"
  ]
  node [
    id 23
    label "sound"
  ]
  node [
    id 24
    label "cecha"
  ]
  node [
    id 25
    label "tone"
  ]
  node [
    id 26
    label "wydawanie"
  ]
  node [
    id 27
    label "spirit"
  ]
  node [
    id 28
    label "rejestr"
  ]
  node [
    id 29
    label "kolorystyka"
  ]
  node [
    id 30
    label "puszczenie"
  ]
  node [
    id 31
    label "dawanie"
  ]
  node [
    id 32
    label "zwracanie_si&#281;"
  ]
  node [
    id 33
    label "ukazywanie_si&#281;"
  ]
  node [
    id 34
    label "denuncjowanie"
  ]
  node [
    id 35
    label "urz&#261;dzanie"
  ]
  node [
    id 36
    label "dzianie_si&#281;"
  ]
  node [
    id 37
    label "wytwarzanie"
  ]
  node [
    id 38
    label "robienie"
  ]
  node [
    id 39
    label "emission"
  ]
  node [
    id 40
    label "emergence"
  ]
  node [
    id 41
    label "ujawnianie"
  ]
  node [
    id 42
    label "czynno&#347;&#263;"
  ]
  node [
    id 43
    label "podawanie"
  ]
  node [
    id 44
    label "wprowadzanie"
  ]
  node [
    id 45
    label "issue"
  ]
  node [
    id 46
    label "charakterystyka"
  ]
  node [
    id 47
    label "m&#322;ot"
  ]
  node [
    id 48
    label "znak"
  ]
  node [
    id 49
    label "drzewo"
  ]
  node [
    id 50
    label "pr&#243;ba"
  ]
  node [
    id 51
    label "attribute"
  ]
  node [
    id 52
    label "marka"
  ]
  node [
    id 53
    label "formu&#322;owanie"
  ]
  node [
    id 54
    label "rozwlekanie"
  ]
  node [
    id 55
    label "conceptualization"
  ]
  node [
    id 56
    label "externalization"
  ]
  node [
    id 57
    label "zauwa&#380;anie"
  ]
  node [
    id 58
    label "znaczenie"
  ]
  node [
    id 59
    label "komunikowanie"
  ]
  node [
    id 60
    label "oznaczanie"
  ]
  node [
    id 61
    label "formu&#322;owanie_si&#281;"
  ]
  node [
    id 62
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 63
    label "rzucanie"
  ]
  node [
    id 64
    label "zbi&#243;r"
  ]
  node [
    id 65
    label "catalog"
  ]
  node [
    id 66
    label "uk&#322;ad_elektroniczny"
  ]
  node [
    id 67
    label "przycisk"
  ]
  node [
    id 68
    label "pozycja"
  ]
  node [
    id 69
    label "stock"
  ]
  node [
    id 70
    label "tekst"
  ]
  node [
    id 71
    label "regestr"
  ]
  node [
    id 72
    label "sumariusz"
  ]
  node [
    id 73
    label "procesor"
  ]
  node [
    id 74
    label "figurowa&#263;"
  ]
  node [
    id 75
    label "skala"
  ]
  node [
    id 76
    label "book"
  ]
  node [
    id 77
    label "wyliczanka"
  ]
  node [
    id 78
    label "urz&#261;dzenie"
  ]
  node [
    id 79
    label "organy"
  ]
  node [
    id 80
    label "zestawienie"
  ]
  node [
    id 81
    label "generalny"
  ]
  node [
    id 82
    label "inspektor"
  ]
  node [
    id 83
    label "ochrona"
  ]
  node [
    id 84
    label "da&#263;"
  ]
  node [
    id 85
    label "osobowy"
  ]
  node [
    id 86
    label "europejski"
  ]
  node [
    id 87
    label "parlament"
  ]
  node [
    id 88
    label "system"
  ]
  node [
    id 89
    label "informacyjny"
  ]
  node [
    id 90
    label "Schengen"
  ]
  node [
    id 91
    label "wizowy"
  ]
  node [
    id 92
    label "systema"
  ]
  node [
    id 93
    label "krajowy"
  ]
  node [
    id 94
    label "informatyczny"
  ]
  node [
    id 95
    label "rzeczpospolita"
  ]
  node [
    id 96
    label "polski"
  ]
  node [
    id 97
    label "urz&#261;d"
  ]
  node [
    id 98
    label "policja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 83
  ]
  edge [
    source 81
    target 84
  ]
  edge [
    source 81
    target 85
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 84
  ]
  edge [
    source 82
    target 85
  ]
  edge [
    source 82
    target 86
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 85
  ]
  edge [
    source 83
    target 86
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 86
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 97
  ]
  edge [
    source 86
    target 98
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 90
  ]
  edge [
    source 88
    target 93
  ]
  edge [
    source 88
    target 94
  ]
  edge [
    source 88
    target 91
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 91
  ]
  edge [
    source 89
    target 92
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 94
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 97
    target 98
  ]
]
