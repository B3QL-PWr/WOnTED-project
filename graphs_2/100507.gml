graph [
  node [
    id 0
    label "wie&#380;a"
    origin "text"
  ]
  node [
    id 1
    label "szybowy"
    origin "text"
  ]
  node [
    id 2
    label "g&#243;rnictwo"
    origin "text"
  ]
  node [
    id 3
    label "budynek"
  ]
  node [
    id 4
    label "tuner"
  ]
  node [
    id 5
    label "wzmacniacz"
  ]
  node [
    id 6
    label "strzelec"
  ]
  node [
    id 7
    label "odtwarzacz"
  ]
  node [
    id 8
    label "hejnalica"
  ]
  node [
    id 9
    label "sprz&#281;t_audio"
  ]
  node [
    id 10
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 11
    label "korektor"
  ]
  node [
    id 12
    label "zestaw"
  ]
  node [
    id 13
    label "struktura"
  ]
  node [
    id 14
    label "zbi&#243;r"
  ]
  node [
    id 15
    label "stage_set"
  ]
  node [
    id 16
    label "sk&#322;ada&#263;"
  ]
  node [
    id 17
    label "sygna&#322;"
  ]
  node [
    id 18
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 19
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 20
    label "&#380;o&#322;nierz"
  ]
  node [
    id 21
    label "figura"
  ]
  node [
    id 22
    label "cz&#322;owiek"
  ]
  node [
    id 23
    label "futbolista"
  ]
  node [
    id 24
    label "sportowiec"
  ]
  node [
    id 25
    label "Renata_Mauer"
  ]
  node [
    id 26
    label "balkon"
  ]
  node [
    id 27
    label "budowla"
  ]
  node [
    id 28
    label "pod&#322;oga"
  ]
  node [
    id 29
    label "kondygnacja"
  ]
  node [
    id 30
    label "skrzyd&#322;o"
  ]
  node [
    id 31
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 32
    label "dach"
  ]
  node [
    id 33
    label "strop"
  ]
  node [
    id 34
    label "klatka_schodowa"
  ]
  node [
    id 35
    label "przedpro&#380;e"
  ]
  node [
    id 36
    label "Pentagon"
  ]
  node [
    id 37
    label "alkierz"
  ]
  node [
    id 38
    label "front"
  ]
  node [
    id 39
    label "amplifikator"
  ]
  node [
    id 40
    label "radiator"
  ]
  node [
    id 41
    label "urz&#261;dzenie"
  ]
  node [
    id 42
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 43
    label "redaktor"
  ]
  node [
    id 44
    label "przybory_do_pisania"
  ]
  node [
    id 45
    label "counterweight"
  ]
  node [
    id 46
    label "kosmetyk_kolorowy"
  ]
  node [
    id 47
    label "artyku&#322;"
  ]
  node [
    id 48
    label "g&#243;rnik_do&#322;owy"
  ]
  node [
    id 49
    label "szklany"
  ]
  node [
    id 50
    label "pusty"
  ]
  node [
    id 51
    label "przezroczysty"
  ]
  node [
    id 52
    label "chorobliwy"
  ]
  node [
    id 53
    label "szkli&#347;cie"
  ]
  node [
    id 54
    label "oboj&#281;tny"
  ]
  node [
    id 55
    label "d&#378;wi&#281;czny"
  ]
  node [
    id 56
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 57
    label "wydobywa&#263;"
  ]
  node [
    id 58
    label "odstrzeliwa&#263;"
  ]
  node [
    id 59
    label "rozpierak"
  ]
  node [
    id 60
    label "krzeska"
  ]
  node [
    id 61
    label "wydoby&#263;"
  ]
  node [
    id 62
    label "przemys&#322;_ci&#281;&#380;ki"
  ]
  node [
    id 63
    label "obrywak"
  ]
  node [
    id 64
    label "wydobycie"
  ]
  node [
    id 65
    label "wydobywanie"
  ]
  node [
    id 66
    label "&#322;adownik"
  ]
  node [
    id 67
    label "zgarniacz"
  ]
  node [
    id 68
    label "nauka"
  ]
  node [
    id 69
    label "wcinka"
  ]
  node [
    id 70
    label "solnictwo"
  ]
  node [
    id 71
    label "odstrzeliwanie"
  ]
  node [
    id 72
    label "aerologia_g&#243;rnicza"
  ]
  node [
    id 73
    label "wiertnictwo"
  ]
  node [
    id 74
    label "przesyp"
  ]
  node [
    id 75
    label "wiedza"
  ]
  node [
    id 76
    label "miasteczko_rowerowe"
  ]
  node [
    id 77
    label "porada"
  ]
  node [
    id 78
    label "fotowoltaika"
  ]
  node [
    id 79
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 80
    label "przem&#243;wienie"
  ]
  node [
    id 81
    label "nauki_o_poznaniu"
  ]
  node [
    id 82
    label "nomotetyczny"
  ]
  node [
    id 83
    label "systematyka"
  ]
  node [
    id 84
    label "proces"
  ]
  node [
    id 85
    label "typologia"
  ]
  node [
    id 86
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 87
    label "kultura_duchowa"
  ]
  node [
    id 88
    label "&#322;awa_szkolna"
  ]
  node [
    id 89
    label "nauki_penalne"
  ]
  node [
    id 90
    label "dziedzina"
  ]
  node [
    id 91
    label "imagineskopia"
  ]
  node [
    id 92
    label "teoria_naukowa"
  ]
  node [
    id 93
    label "inwentyka"
  ]
  node [
    id 94
    label "metodologia"
  ]
  node [
    id 95
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 96
    label "nauki_o_Ziemi"
  ]
  node [
    id 97
    label "od&#322;upywanie"
  ]
  node [
    id 98
    label "urywanie"
  ]
  node [
    id 99
    label "zabijanie"
  ]
  node [
    id 100
    label "uwydatnia&#263;"
  ]
  node [
    id 101
    label "eksploatowa&#263;"
  ]
  node [
    id 102
    label "uzyskiwa&#263;"
  ]
  node [
    id 103
    label "wydostawa&#263;"
  ]
  node [
    id 104
    label "wyjmowa&#263;"
  ]
  node [
    id 105
    label "train"
  ]
  node [
    id 106
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 107
    label "wydawa&#263;"
  ]
  node [
    id 108
    label "dobywa&#263;"
  ]
  node [
    id 109
    label "ocala&#263;"
  ]
  node [
    id 110
    label "excavate"
  ]
  node [
    id 111
    label "raise"
  ]
  node [
    id 112
    label "draw"
  ]
  node [
    id 113
    label "doby&#263;"
  ]
  node [
    id 114
    label "wyeksploatowa&#263;"
  ]
  node [
    id 115
    label "extract"
  ]
  node [
    id 116
    label "obtain"
  ]
  node [
    id 117
    label "wyj&#261;&#263;"
  ]
  node [
    id 118
    label "ocali&#263;"
  ]
  node [
    id 119
    label "uzyska&#263;"
  ]
  node [
    id 120
    label "wyda&#263;"
  ]
  node [
    id 121
    label "wydosta&#263;"
  ]
  node [
    id 122
    label "uwydatni&#263;"
  ]
  node [
    id 123
    label "distill"
  ]
  node [
    id 124
    label "dobywanie"
  ]
  node [
    id 125
    label "powodowanie"
  ]
  node [
    id 126
    label "u&#380;ytkowanie"
  ]
  node [
    id 127
    label "eksploatowanie"
  ]
  node [
    id 128
    label "wydostawanie"
  ]
  node [
    id 129
    label "wyjmowanie"
  ]
  node [
    id 130
    label "ratowanie"
  ]
  node [
    id 131
    label "robienie"
  ]
  node [
    id 132
    label "uzyskiwanie"
  ]
  node [
    id 133
    label "evocation"
  ]
  node [
    id 134
    label "czynno&#347;&#263;"
  ]
  node [
    id 135
    label "uwydatnianie"
  ]
  node [
    id 136
    label "extraction"
  ]
  node [
    id 137
    label "wyeksploatowanie"
  ]
  node [
    id 138
    label "uwydatnienie"
  ]
  node [
    id 139
    label "uzyskanie"
  ]
  node [
    id 140
    label "fusillade"
  ]
  node [
    id 141
    label "spowodowanie"
  ]
  node [
    id 142
    label "wyratowanie"
  ]
  node [
    id 143
    label "wyj&#281;cie"
  ]
  node [
    id 144
    label "powyci&#261;ganie"
  ]
  node [
    id 145
    label "wydostanie"
  ]
  node [
    id 146
    label "dobycie"
  ]
  node [
    id 147
    label "explosion"
  ]
  node [
    id 148
    label "produkcja"
  ]
  node [
    id 149
    label "zrobienie"
  ]
  node [
    id 150
    label "zabija&#263;"
  ]
  node [
    id 151
    label "od&#322;upywa&#263;"
  ]
  node [
    id 152
    label "urywa&#263;"
  ]
  node [
    id 153
    label "fire"
  ]
  node [
    id 154
    label "wa&#322;"
  ]
  node [
    id 155
    label "ilo&#347;&#263;"
  ]
  node [
    id 156
    label "miejsce"
  ]
  node [
    id 157
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 158
    label "toporek"
  ]
  node [
    id 159
    label "przerywnik"
  ]
  node [
    id 160
    label "przy&#322;&#261;cze"
  ]
  node [
    id 161
    label "zagrywka"
  ]
  node [
    id 162
    label "wn&#281;ka"
  ]
  node [
    id 163
    label "wci&#281;cie"
  ]
  node [
    id 164
    label "film"
  ]
  node [
    id 165
    label "sztuczka"
  ]
  node [
    id 166
    label "fortel"
  ]
  node [
    id 167
    label "podci&#261;gnik"
  ]
  node [
    id 168
    label "budownictwo"
  ]
  node [
    id 169
    label "&#322;adowarka"
  ]
  node [
    id 170
    label "robotnik"
  ]
  node [
    id 171
    label "zasobnik"
  ]
  node [
    id 172
    label "mocowanie"
  ]
  node [
    id 173
    label "bro&#324;_maszynowa"
  ]
  node [
    id 174
    label "&#322;om"
  ]
  node [
    id 175
    label "rolnictwo"
  ]
  node [
    id 176
    label "scraper"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
]
