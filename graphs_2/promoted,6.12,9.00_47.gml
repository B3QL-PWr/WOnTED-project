graph [
  node [
    id 0
    label "odrzutowiec"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "fan"
    origin "text"
  ]
  node [
    id 3
    label "paliwo&#380;erny"
    origin "text"
  ]
  node [
    id 4
    label "hummer"
    origin "text"
  ]
  node [
    id 5
    label "samolot"
  ]
  node [
    id 6
    label "spalin&#243;wka"
  ]
  node [
    id 7
    label "katapulta"
  ]
  node [
    id 8
    label "pilot_automatyczny"
  ]
  node [
    id 9
    label "kad&#322;ub"
  ]
  node [
    id 10
    label "wiatrochron"
  ]
  node [
    id 11
    label "kabina"
  ]
  node [
    id 12
    label "wylatywanie"
  ]
  node [
    id 13
    label "kapotowanie"
  ]
  node [
    id 14
    label "kapotowa&#263;"
  ]
  node [
    id 15
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 16
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 17
    label "skrzyd&#322;o"
  ]
  node [
    id 18
    label "pok&#322;ad"
  ]
  node [
    id 19
    label "kapota&#380;"
  ]
  node [
    id 20
    label "sta&#322;op&#322;at"
  ]
  node [
    id 21
    label "sterownica"
  ]
  node [
    id 22
    label "p&#322;atowiec"
  ]
  node [
    id 23
    label "wylecenie"
  ]
  node [
    id 24
    label "wylecie&#263;"
  ]
  node [
    id 25
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 26
    label "wylatywa&#263;"
  ]
  node [
    id 27
    label "gondola"
  ]
  node [
    id 28
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 29
    label "dzi&#243;b"
  ]
  node [
    id 30
    label "inhalator_tlenowy"
  ]
  node [
    id 31
    label "kapot"
  ]
  node [
    id 32
    label "kabinka"
  ]
  node [
    id 33
    label "&#380;yroskop"
  ]
  node [
    id 34
    label "czarna_skrzynka"
  ]
  node [
    id 35
    label "lecenie"
  ]
  node [
    id 36
    label "fotel_lotniczy"
  ]
  node [
    id 37
    label "wy&#347;lizg"
  ]
  node [
    id 38
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 39
    label "mie&#263;_miejsce"
  ]
  node [
    id 40
    label "equal"
  ]
  node [
    id 41
    label "trwa&#263;"
  ]
  node [
    id 42
    label "chodzi&#263;"
  ]
  node [
    id 43
    label "si&#281;ga&#263;"
  ]
  node [
    id 44
    label "stan"
  ]
  node [
    id 45
    label "obecno&#347;&#263;"
  ]
  node [
    id 46
    label "stand"
  ]
  node [
    id 47
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 48
    label "uczestniczy&#263;"
  ]
  node [
    id 49
    label "participate"
  ]
  node [
    id 50
    label "robi&#263;"
  ]
  node [
    id 51
    label "istnie&#263;"
  ]
  node [
    id 52
    label "pozostawa&#263;"
  ]
  node [
    id 53
    label "zostawa&#263;"
  ]
  node [
    id 54
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 55
    label "adhere"
  ]
  node [
    id 56
    label "compass"
  ]
  node [
    id 57
    label "korzysta&#263;"
  ]
  node [
    id 58
    label "appreciation"
  ]
  node [
    id 59
    label "osi&#261;ga&#263;"
  ]
  node [
    id 60
    label "dociera&#263;"
  ]
  node [
    id 61
    label "get"
  ]
  node [
    id 62
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 63
    label "mierzy&#263;"
  ]
  node [
    id 64
    label "u&#380;ywa&#263;"
  ]
  node [
    id 65
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 66
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 67
    label "exsert"
  ]
  node [
    id 68
    label "being"
  ]
  node [
    id 69
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 70
    label "cecha"
  ]
  node [
    id 71
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 72
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 73
    label "p&#322;ywa&#263;"
  ]
  node [
    id 74
    label "run"
  ]
  node [
    id 75
    label "bangla&#263;"
  ]
  node [
    id 76
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 77
    label "przebiega&#263;"
  ]
  node [
    id 78
    label "wk&#322;ada&#263;"
  ]
  node [
    id 79
    label "proceed"
  ]
  node [
    id 80
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 81
    label "carry"
  ]
  node [
    id 82
    label "bywa&#263;"
  ]
  node [
    id 83
    label "dziama&#263;"
  ]
  node [
    id 84
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 85
    label "stara&#263;_si&#281;"
  ]
  node [
    id 86
    label "para"
  ]
  node [
    id 87
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 88
    label "str&#243;j"
  ]
  node [
    id 89
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 90
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 91
    label "krok"
  ]
  node [
    id 92
    label "tryb"
  ]
  node [
    id 93
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 94
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 95
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 96
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 97
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 98
    label "continue"
  ]
  node [
    id 99
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 100
    label "Ohio"
  ]
  node [
    id 101
    label "wci&#281;cie"
  ]
  node [
    id 102
    label "Nowy_York"
  ]
  node [
    id 103
    label "warstwa"
  ]
  node [
    id 104
    label "samopoczucie"
  ]
  node [
    id 105
    label "Illinois"
  ]
  node [
    id 106
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 107
    label "state"
  ]
  node [
    id 108
    label "Jukatan"
  ]
  node [
    id 109
    label "Kalifornia"
  ]
  node [
    id 110
    label "Wirginia"
  ]
  node [
    id 111
    label "wektor"
  ]
  node [
    id 112
    label "Teksas"
  ]
  node [
    id 113
    label "Goa"
  ]
  node [
    id 114
    label "Waszyngton"
  ]
  node [
    id 115
    label "miejsce"
  ]
  node [
    id 116
    label "Massachusetts"
  ]
  node [
    id 117
    label "Alaska"
  ]
  node [
    id 118
    label "Arakan"
  ]
  node [
    id 119
    label "Hawaje"
  ]
  node [
    id 120
    label "Maryland"
  ]
  node [
    id 121
    label "punkt"
  ]
  node [
    id 122
    label "Michigan"
  ]
  node [
    id 123
    label "Arizona"
  ]
  node [
    id 124
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 125
    label "Georgia"
  ]
  node [
    id 126
    label "poziom"
  ]
  node [
    id 127
    label "Pensylwania"
  ]
  node [
    id 128
    label "shape"
  ]
  node [
    id 129
    label "Luizjana"
  ]
  node [
    id 130
    label "Nowy_Meksyk"
  ]
  node [
    id 131
    label "Alabama"
  ]
  node [
    id 132
    label "ilo&#347;&#263;"
  ]
  node [
    id 133
    label "Kansas"
  ]
  node [
    id 134
    label "Oregon"
  ]
  node [
    id 135
    label "Floryda"
  ]
  node [
    id 136
    label "Oklahoma"
  ]
  node [
    id 137
    label "jednostka_administracyjna"
  ]
  node [
    id 138
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 139
    label "fan_club"
  ]
  node [
    id 140
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 141
    label "fandom"
  ]
  node [
    id 142
    label "sympatyk"
  ]
  node [
    id 143
    label "entuzjasta"
  ]
  node [
    id 144
    label "grupa"
  ]
  node [
    id 145
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 146
    label "nieoszcz&#281;dny"
  ]
  node [
    id 147
    label "paliwo&#380;ernie"
  ]
  node [
    id 148
    label "niezapobiegliwy"
  ]
  node [
    id 149
    label "nieoszcz&#281;dnie"
  ]
  node [
    id 150
    label "nierozwa&#380;ny"
  ]
  node [
    id 151
    label "niegospodarny"
  ]
  node [
    id 152
    label "z&#322;y"
  ]
  node [
    id 153
    label "nieop&#322;acalnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
]
