graph [
  node [
    id 0
    label "okazja"
    origin "text"
  ]
  node [
    id 1
    label "imieniny"
    origin "text"
  ]
  node [
    id 2
    label "andrzej"
    origin "text"
  ]
  node [
    id 3
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "do&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "glupiewykopowezabawy"
    origin "text"
  ]
  node [
    id 6
    label "nasi"
    origin "text"
  ]
  node [
    id 7
    label "ulubiony"
    origin "text"
  ]
  node [
    id 8
    label "podw&#243;zka"
  ]
  node [
    id 9
    label "wydarzenie"
  ]
  node [
    id 10
    label "okazka"
  ]
  node [
    id 11
    label "oferta"
  ]
  node [
    id 12
    label "autostop"
  ]
  node [
    id 13
    label "atrakcyjny"
  ]
  node [
    id 14
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 15
    label "sytuacja"
  ]
  node [
    id 16
    label "adeptness"
  ]
  node [
    id 17
    label "posiada&#263;"
  ]
  node [
    id 18
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 19
    label "egzekutywa"
  ]
  node [
    id 20
    label "potencja&#322;"
  ]
  node [
    id 21
    label "wyb&#243;r"
  ]
  node [
    id 22
    label "prospect"
  ]
  node [
    id 23
    label "ability"
  ]
  node [
    id 24
    label "obliczeniowo"
  ]
  node [
    id 25
    label "alternatywa"
  ]
  node [
    id 26
    label "cecha"
  ]
  node [
    id 27
    label "operator_modalny"
  ]
  node [
    id 28
    label "podwoda"
  ]
  node [
    id 29
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 30
    label "transport"
  ]
  node [
    id 31
    label "offer"
  ]
  node [
    id 32
    label "propozycja"
  ]
  node [
    id 33
    label "przebiec"
  ]
  node [
    id 34
    label "charakter"
  ]
  node [
    id 35
    label "czynno&#347;&#263;"
  ]
  node [
    id 36
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 37
    label "motyw"
  ]
  node [
    id 38
    label "przebiegni&#281;cie"
  ]
  node [
    id 39
    label "fabu&#322;a"
  ]
  node [
    id 40
    label "warunki"
  ]
  node [
    id 41
    label "szczeg&#243;&#322;"
  ]
  node [
    id 42
    label "state"
  ]
  node [
    id 43
    label "realia"
  ]
  node [
    id 44
    label "stop"
  ]
  node [
    id 45
    label "podr&#243;&#380;"
  ]
  node [
    id 46
    label "g&#322;adki"
  ]
  node [
    id 47
    label "uatrakcyjnianie"
  ]
  node [
    id 48
    label "atrakcyjnie"
  ]
  node [
    id 49
    label "poci&#261;gaj&#261;co"
  ]
  node [
    id 50
    label "interesuj&#261;cy"
  ]
  node [
    id 51
    label "po&#380;&#261;dany"
  ]
  node [
    id 52
    label "dobry"
  ]
  node [
    id 53
    label "uatrakcyjnienie"
  ]
  node [
    id 54
    label "dzie&#324;"
  ]
  node [
    id 55
    label "impreza"
  ]
  node [
    id 56
    label "ranek"
  ]
  node [
    id 57
    label "doba"
  ]
  node [
    id 58
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 59
    label "noc"
  ]
  node [
    id 60
    label "podwiecz&#243;r"
  ]
  node [
    id 61
    label "po&#322;udnie"
  ]
  node [
    id 62
    label "godzina"
  ]
  node [
    id 63
    label "przedpo&#322;udnie"
  ]
  node [
    id 64
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 65
    label "long_time"
  ]
  node [
    id 66
    label "wiecz&#243;r"
  ]
  node [
    id 67
    label "t&#322;usty_czwartek"
  ]
  node [
    id 68
    label "popo&#322;udnie"
  ]
  node [
    id 69
    label "walentynki"
  ]
  node [
    id 70
    label "czynienie_si&#281;"
  ]
  node [
    id 71
    label "s&#322;o&#324;ce"
  ]
  node [
    id 72
    label "rano"
  ]
  node [
    id 73
    label "tydzie&#324;"
  ]
  node [
    id 74
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 75
    label "wzej&#347;cie"
  ]
  node [
    id 76
    label "czas"
  ]
  node [
    id 77
    label "wsta&#263;"
  ]
  node [
    id 78
    label "day"
  ]
  node [
    id 79
    label "termin"
  ]
  node [
    id 80
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 81
    label "wstanie"
  ]
  node [
    id 82
    label "przedwiecz&#243;r"
  ]
  node [
    id 83
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 84
    label "Sylwester"
  ]
  node [
    id 85
    label "impra"
  ]
  node [
    id 86
    label "rozrywka"
  ]
  node [
    id 87
    label "przyj&#281;cie"
  ]
  node [
    id 88
    label "party"
  ]
  node [
    id 89
    label "czu&#263;"
  ]
  node [
    id 90
    label "desire"
  ]
  node [
    id 91
    label "kcie&#263;"
  ]
  node [
    id 92
    label "postrzega&#263;"
  ]
  node [
    id 93
    label "przewidywa&#263;"
  ]
  node [
    id 94
    label "by&#263;"
  ]
  node [
    id 95
    label "smell"
  ]
  node [
    id 96
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 97
    label "uczuwa&#263;"
  ]
  node [
    id 98
    label "spirit"
  ]
  node [
    id 99
    label "doznawa&#263;"
  ]
  node [
    id 100
    label "anticipate"
  ]
  node [
    id 101
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 102
    label "catch"
  ]
  node [
    id 103
    label "zrobi&#263;"
  ]
  node [
    id 104
    label "spowodowa&#263;"
  ]
  node [
    id 105
    label "dokoptowa&#263;"
  ]
  node [
    id 106
    label "articulation"
  ]
  node [
    id 107
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 108
    label "act"
  ]
  node [
    id 109
    label "post&#261;pi&#263;"
  ]
  node [
    id 110
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 111
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 112
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 113
    label "zorganizowa&#263;"
  ]
  node [
    id 114
    label "appoint"
  ]
  node [
    id 115
    label "wystylizowa&#263;"
  ]
  node [
    id 116
    label "cause"
  ]
  node [
    id 117
    label "przerobi&#263;"
  ]
  node [
    id 118
    label "nabra&#263;"
  ]
  node [
    id 119
    label "make"
  ]
  node [
    id 120
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 121
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 122
    label "wydali&#263;"
  ]
  node [
    id 123
    label "dokooptowa&#263;"
  ]
  node [
    id 124
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 125
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 126
    label "wyj&#261;tkowy"
  ]
  node [
    id 127
    label "faworytny"
  ]
  node [
    id 128
    label "wyj&#261;tkowo"
  ]
  node [
    id 129
    label "inny"
  ]
  node [
    id 130
    label "ukochany"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
]
