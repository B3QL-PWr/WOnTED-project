graph [
  node [
    id 0
    label "letni"
    origin "text"
  ]
  node [
    id 1
    label "ch&#322;opiec"
    origin "text"
  ]
  node [
    id 2
    label "wys&#322;a&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kartka"
    origin "text"
  ]
  node [
    id 4
    label "urodzinowy"
    origin "text"
  ]
  node [
    id 5
    label "swoje"
    origin "text"
  ]
  node [
    id 6
    label "zmar&#322;y"
    origin "text"
  ]
  node [
    id 7
    label "tata"
    origin "text"
  ]
  node [
    id 8
    label "latowy"
  ]
  node [
    id 9
    label "typowy"
  ]
  node [
    id 10
    label "weso&#322;y"
  ]
  node [
    id 11
    label "s&#322;oneczny"
  ]
  node [
    id 12
    label "sezonowy"
  ]
  node [
    id 13
    label "ciep&#322;y"
  ]
  node [
    id 14
    label "letnio"
  ]
  node [
    id 15
    label "oboj&#281;tny"
  ]
  node [
    id 16
    label "nijaki"
  ]
  node [
    id 17
    label "nijak"
  ]
  node [
    id 18
    label "niezabawny"
  ]
  node [
    id 19
    label "&#380;aden"
  ]
  node [
    id 20
    label "zwyczajny"
  ]
  node [
    id 21
    label "poszarzenie"
  ]
  node [
    id 22
    label "neutralny"
  ]
  node [
    id 23
    label "szarzenie"
  ]
  node [
    id 24
    label "bezbarwnie"
  ]
  node [
    id 25
    label "nieciekawy"
  ]
  node [
    id 26
    label "czasowy"
  ]
  node [
    id 27
    label "sezonowo"
  ]
  node [
    id 28
    label "zoboj&#281;tnienie"
  ]
  node [
    id 29
    label "nieszkodliwy"
  ]
  node [
    id 30
    label "&#347;ni&#281;ty"
  ]
  node [
    id 31
    label "oboj&#281;tnie"
  ]
  node [
    id 32
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 33
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 34
    label "niewa&#380;ny"
  ]
  node [
    id 35
    label "neutralizowanie"
  ]
  node [
    id 36
    label "bierny"
  ]
  node [
    id 37
    label "zneutralizowanie"
  ]
  node [
    id 38
    label "pijany"
  ]
  node [
    id 39
    label "weso&#322;o"
  ]
  node [
    id 40
    label "pozytywny"
  ]
  node [
    id 41
    label "beztroski"
  ]
  node [
    id 42
    label "dobry"
  ]
  node [
    id 43
    label "mi&#322;y"
  ]
  node [
    id 44
    label "ocieplanie_si&#281;"
  ]
  node [
    id 45
    label "ocieplanie"
  ]
  node [
    id 46
    label "grzanie"
  ]
  node [
    id 47
    label "ocieplenie_si&#281;"
  ]
  node [
    id 48
    label "zagrzanie"
  ]
  node [
    id 49
    label "ocieplenie"
  ]
  node [
    id 50
    label "korzystny"
  ]
  node [
    id 51
    label "przyjemny"
  ]
  node [
    id 52
    label "ciep&#322;o"
  ]
  node [
    id 53
    label "s&#322;onecznie"
  ]
  node [
    id 54
    label "bezdeszczowy"
  ]
  node [
    id 55
    label "bezchmurny"
  ]
  node [
    id 56
    label "pogodny"
  ]
  node [
    id 57
    label "fotowoltaiczny"
  ]
  node [
    id 58
    label "jasny"
  ]
  node [
    id 59
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 60
    label "typowo"
  ]
  node [
    id 61
    label "cz&#281;sty"
  ]
  node [
    id 62
    label "zwyk&#322;y"
  ]
  node [
    id 63
    label "g&#243;wniarz"
  ]
  node [
    id 64
    label "synek"
  ]
  node [
    id 65
    label "cz&#322;owiek"
  ]
  node [
    id 66
    label "boyfriend"
  ]
  node [
    id 67
    label "okrzos"
  ]
  node [
    id 68
    label "dziecko"
  ]
  node [
    id 69
    label "sympatia"
  ]
  node [
    id 70
    label "usynowienie"
  ]
  node [
    id 71
    label "pomocnik"
  ]
  node [
    id 72
    label "kawaler"
  ]
  node [
    id 73
    label "&#347;l&#261;ski"
  ]
  node [
    id 74
    label "m&#322;odzieniec"
  ]
  node [
    id 75
    label "kajtek"
  ]
  node [
    id 76
    label "pederasta"
  ]
  node [
    id 77
    label "usynawianie"
  ]
  node [
    id 78
    label "utulenie"
  ]
  node [
    id 79
    label "pediatra"
  ]
  node [
    id 80
    label "dzieciak"
  ]
  node [
    id 81
    label "utulanie"
  ]
  node [
    id 82
    label "dzieciarnia"
  ]
  node [
    id 83
    label "niepe&#322;noletni"
  ]
  node [
    id 84
    label "organizm"
  ]
  node [
    id 85
    label "utula&#263;"
  ]
  node [
    id 86
    label "cz&#322;owieczek"
  ]
  node [
    id 87
    label "fledgling"
  ]
  node [
    id 88
    label "zwierz&#281;"
  ]
  node [
    id 89
    label "utuli&#263;"
  ]
  node [
    id 90
    label "m&#322;odzik"
  ]
  node [
    id 91
    label "pedofil"
  ]
  node [
    id 92
    label "m&#322;odziak"
  ]
  node [
    id 93
    label "potomek"
  ]
  node [
    id 94
    label "entliczek-pentliczek"
  ]
  node [
    id 95
    label "potomstwo"
  ]
  node [
    id 96
    label "sraluch"
  ]
  node [
    id 97
    label "ludzko&#347;&#263;"
  ]
  node [
    id 98
    label "asymilowanie"
  ]
  node [
    id 99
    label "wapniak"
  ]
  node [
    id 100
    label "asymilowa&#263;"
  ]
  node [
    id 101
    label "os&#322;abia&#263;"
  ]
  node [
    id 102
    label "posta&#263;"
  ]
  node [
    id 103
    label "hominid"
  ]
  node [
    id 104
    label "podw&#322;adny"
  ]
  node [
    id 105
    label "os&#322;abianie"
  ]
  node [
    id 106
    label "g&#322;owa"
  ]
  node [
    id 107
    label "figura"
  ]
  node [
    id 108
    label "portrecista"
  ]
  node [
    id 109
    label "dwun&#243;g"
  ]
  node [
    id 110
    label "profanum"
  ]
  node [
    id 111
    label "mikrokosmos"
  ]
  node [
    id 112
    label "nasada"
  ]
  node [
    id 113
    label "duch"
  ]
  node [
    id 114
    label "antropochoria"
  ]
  node [
    id 115
    label "osoba"
  ]
  node [
    id 116
    label "wz&#243;r"
  ]
  node [
    id 117
    label "senior"
  ]
  node [
    id 118
    label "oddzia&#322;ywanie"
  ]
  node [
    id 119
    label "Adam"
  ]
  node [
    id 120
    label "homo_sapiens"
  ]
  node [
    id 121
    label "polifag"
  ]
  node [
    id 122
    label "emocja"
  ]
  node [
    id 123
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 124
    label "partner"
  ]
  node [
    id 125
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 126
    label "love"
  ]
  node [
    id 127
    label "kredens"
  ]
  node [
    id 128
    label "zawodnik"
  ]
  node [
    id 129
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 130
    label "bylina"
  ]
  node [
    id 131
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 132
    label "gracz"
  ]
  node [
    id 133
    label "r&#281;ka"
  ]
  node [
    id 134
    label "pomoc"
  ]
  node [
    id 135
    label "wrzosowate"
  ]
  node [
    id 136
    label "pomagacz"
  ]
  node [
    id 137
    label "junior"
  ]
  node [
    id 138
    label "junak"
  ]
  node [
    id 139
    label "m&#322;odzie&#380;"
  ]
  node [
    id 140
    label "mo&#322;ojec"
  ]
  node [
    id 141
    label "kawa&#322;ek"
  ]
  node [
    id 142
    label "m&#322;okos"
  ]
  node [
    id 143
    label "smarkateria"
  ]
  node [
    id 144
    label "ch&#322;opak"
  ]
  node [
    id 145
    label "cug"
  ]
  node [
    id 146
    label "krepel"
  ]
  node [
    id 147
    label "mietlorz"
  ]
  node [
    id 148
    label "francuz"
  ]
  node [
    id 149
    label "etnolekt"
  ]
  node [
    id 150
    label "sza&#322;ot"
  ]
  node [
    id 151
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 152
    label "polski"
  ]
  node [
    id 153
    label "regionalny"
  ]
  node [
    id 154
    label "halba"
  ]
  node [
    id 155
    label "buchta"
  ]
  node [
    id 156
    label "czarne_kluski"
  ]
  node [
    id 157
    label "szpajza"
  ]
  node [
    id 158
    label "szl&#261;ski"
  ]
  node [
    id 159
    label "&#347;lonski"
  ]
  node [
    id 160
    label "po_&#347;l&#261;sku"
  ]
  node [
    id 161
    label "waloszek"
  ]
  node [
    id 162
    label "gej"
  ]
  node [
    id 163
    label "tytu&#322;"
  ]
  node [
    id 164
    label "order"
  ]
  node [
    id 165
    label "zalotnik"
  ]
  node [
    id 166
    label "kawalerka"
  ]
  node [
    id 167
    label "rycerz"
  ]
  node [
    id 168
    label "odznaczenie"
  ]
  node [
    id 169
    label "nie&#380;onaty"
  ]
  node [
    id 170
    label "zakon_rycerski"
  ]
  node [
    id 171
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 172
    label "zakonnik"
  ]
  node [
    id 173
    label "syn"
  ]
  node [
    id 174
    label "przysposabianie"
  ]
  node [
    id 175
    label "przysposobienie"
  ]
  node [
    id 176
    label "adoption"
  ]
  node [
    id 177
    label "nakaza&#263;"
  ]
  node [
    id 178
    label "przekaza&#263;"
  ]
  node [
    id 179
    label "ship"
  ]
  node [
    id 180
    label "post"
  ]
  node [
    id 181
    label "line"
  ]
  node [
    id 182
    label "wytworzy&#263;"
  ]
  node [
    id 183
    label "convey"
  ]
  node [
    id 184
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 185
    label "poleci&#263;"
  ]
  node [
    id 186
    label "zapakowa&#263;"
  ]
  node [
    id 187
    label "cause"
  ]
  node [
    id 188
    label "manufacture"
  ]
  node [
    id 189
    label "zrobi&#263;"
  ]
  node [
    id 190
    label "sheathe"
  ]
  node [
    id 191
    label "pieni&#261;dze"
  ]
  node [
    id 192
    label "translate"
  ]
  node [
    id 193
    label "give"
  ]
  node [
    id 194
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 195
    label "wyj&#261;&#263;"
  ]
  node [
    id 196
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 197
    label "range"
  ]
  node [
    id 198
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 199
    label "propagate"
  ]
  node [
    id 200
    label "wp&#322;aci&#263;"
  ]
  node [
    id 201
    label "transfer"
  ]
  node [
    id 202
    label "poda&#263;"
  ]
  node [
    id 203
    label "sygna&#322;"
  ]
  node [
    id 204
    label "impart"
  ]
  node [
    id 205
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 206
    label "zachowanie"
  ]
  node [
    id 207
    label "zachowywanie"
  ]
  node [
    id 208
    label "rok_ko&#347;cielny"
  ]
  node [
    id 209
    label "tekst"
  ]
  node [
    id 210
    label "czas"
  ]
  node [
    id 211
    label "praktyka"
  ]
  node [
    id 212
    label "zachowa&#263;"
  ]
  node [
    id 213
    label "zachowywa&#263;"
  ]
  node [
    id 214
    label "faul"
  ]
  node [
    id 215
    label "wk&#322;ad"
  ]
  node [
    id 216
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 217
    label "s&#281;dzia"
  ]
  node [
    id 218
    label "bon"
  ]
  node [
    id 219
    label "ticket"
  ]
  node [
    id 220
    label "arkusz"
  ]
  node [
    id 221
    label "kartonik"
  ]
  node [
    id 222
    label "kara"
  ]
  node [
    id 223
    label "strona"
  ]
  node [
    id 224
    label "kwota"
  ]
  node [
    id 225
    label "nemezis"
  ]
  node [
    id 226
    label "konsekwencja"
  ]
  node [
    id 227
    label "punishment"
  ]
  node [
    id 228
    label "klacz"
  ]
  node [
    id 229
    label "forfeit"
  ]
  node [
    id 230
    label "roboty_przymusowe"
  ]
  node [
    id 231
    label "dokument"
  ]
  node [
    id 232
    label "voucher"
  ]
  node [
    id 233
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 234
    label "p&#322;at"
  ]
  node [
    id 235
    label "p&#243;&#322;arkusz"
  ]
  node [
    id 236
    label "zagrywka"
  ]
  node [
    id 237
    label "wykroczenie"
  ]
  node [
    id 238
    label "kontuzjowa&#263;"
  ]
  node [
    id 239
    label "foul"
  ]
  node [
    id 240
    label "kontuzjowanie"
  ]
  node [
    id 241
    label "pracownik"
  ]
  node [
    id 242
    label "s&#261;d"
  ]
  node [
    id 243
    label "opiniodawca"
  ]
  node [
    id 244
    label "prawnik"
  ]
  node [
    id 245
    label "orzekanie"
  ]
  node [
    id 246
    label "sport"
  ]
  node [
    id 247
    label "os&#261;dziciel"
  ]
  node [
    id 248
    label "orzeka&#263;"
  ]
  node [
    id 249
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 250
    label "logowanie"
  ]
  node [
    id 251
    label "plik"
  ]
  node [
    id 252
    label "adres_internetowy"
  ]
  node [
    id 253
    label "linia"
  ]
  node [
    id 254
    label "serwis_internetowy"
  ]
  node [
    id 255
    label "bok"
  ]
  node [
    id 256
    label "skr&#281;canie"
  ]
  node [
    id 257
    label "skr&#281;ca&#263;"
  ]
  node [
    id 258
    label "orientowanie"
  ]
  node [
    id 259
    label "skr&#281;ci&#263;"
  ]
  node [
    id 260
    label "uj&#281;cie"
  ]
  node [
    id 261
    label "zorientowanie"
  ]
  node [
    id 262
    label "ty&#322;"
  ]
  node [
    id 263
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 264
    label "fragment"
  ]
  node [
    id 265
    label "layout"
  ]
  node [
    id 266
    label "obiekt"
  ]
  node [
    id 267
    label "zorientowa&#263;"
  ]
  node [
    id 268
    label "pagina"
  ]
  node [
    id 269
    label "podmiot"
  ]
  node [
    id 270
    label "g&#243;ra"
  ]
  node [
    id 271
    label "orientowa&#263;"
  ]
  node [
    id 272
    label "voice"
  ]
  node [
    id 273
    label "orientacja"
  ]
  node [
    id 274
    label "prz&#243;d"
  ]
  node [
    id 275
    label "internet"
  ]
  node [
    id 276
    label "powierzchnia"
  ]
  node [
    id 277
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 278
    label "forma"
  ]
  node [
    id 279
    label "skr&#281;cenie"
  ]
  node [
    id 280
    label "uczestnictwo"
  ]
  node [
    id 281
    label "ok&#322;adka"
  ]
  node [
    id 282
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 283
    label "element"
  ]
  node [
    id 284
    label "input"
  ]
  node [
    id 285
    label "czasopismo"
  ]
  node [
    id 286
    label "lokata"
  ]
  node [
    id 287
    label "zeszyt"
  ]
  node [
    id 288
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 289
    label "martwy"
  ]
  node [
    id 290
    label "umarlak"
  ]
  node [
    id 291
    label "nieumar&#322;y"
  ]
  node [
    id 292
    label "chowanie"
  ]
  node [
    id 293
    label "zw&#322;oki"
  ]
  node [
    id 294
    label "martwo"
  ]
  node [
    id 295
    label "bezmy&#347;lny"
  ]
  node [
    id 296
    label "umieranie"
  ]
  node [
    id 297
    label "nieaktualny"
  ]
  node [
    id 298
    label "wiszenie"
  ]
  node [
    id 299
    label "niesprawny"
  ]
  node [
    id 300
    label "umarcie"
  ]
  node [
    id 301
    label "obumarcie"
  ]
  node [
    id 302
    label "obumieranie"
  ]
  node [
    id 303
    label "trwa&#322;y"
  ]
  node [
    id 304
    label "ekshumowanie"
  ]
  node [
    id 305
    label "pochowanie"
  ]
  node [
    id 306
    label "zabalsamowanie"
  ]
  node [
    id 307
    label "kremacja"
  ]
  node [
    id 308
    label "zm&#281;czony"
  ]
  node [
    id 309
    label "pochowa&#263;"
  ]
  node [
    id 310
    label "balsamowa&#263;"
  ]
  node [
    id 311
    label "tanatoplastyka"
  ]
  node [
    id 312
    label "ekshumowa&#263;"
  ]
  node [
    id 313
    label "cia&#322;o"
  ]
  node [
    id 314
    label "tanatoplastyk"
  ]
  node [
    id 315
    label "sekcja"
  ]
  node [
    id 316
    label "balsamowanie"
  ]
  node [
    id 317
    label "zabalsamowa&#263;"
  ]
  node [
    id 318
    label "pogrzeb"
  ]
  node [
    id 319
    label "nadgni&#322;y"
  ]
  node [
    id 320
    label "o&#380;ywieniec"
  ]
  node [
    id 321
    label "umieszczanie"
  ]
  node [
    id 322
    label "potrzymanie"
  ]
  node [
    id 323
    label "dochowanie_si&#281;"
  ]
  node [
    id 324
    label "burial"
  ]
  node [
    id 325
    label "gr&#243;b"
  ]
  node [
    id 326
    label "wk&#322;adanie"
  ]
  node [
    id 327
    label "concealment"
  ]
  node [
    id 328
    label "ukrywanie"
  ]
  node [
    id 329
    label "sk&#322;adanie"
  ]
  node [
    id 330
    label "opiekowanie_si&#281;"
  ]
  node [
    id 331
    label "education"
  ]
  node [
    id 332
    label "czucie"
  ]
  node [
    id 333
    label "clasp"
  ]
  node [
    id 334
    label "wychowywanie_si&#281;"
  ]
  node [
    id 335
    label "przetrzymywanie"
  ]
  node [
    id 336
    label "boarding"
  ]
  node [
    id 337
    label "niewidoczny"
  ]
  node [
    id 338
    label "hodowanie"
  ]
  node [
    id 339
    label "nekromancja"
  ]
  node [
    id 340
    label "istota_fantastyczna"
  ]
  node [
    id 341
    label "piek&#322;o"
  ]
  node [
    id 342
    label "human_body"
  ]
  node [
    id 343
    label "ofiarowywanie"
  ]
  node [
    id 344
    label "sfera_afektywna"
  ]
  node [
    id 345
    label "Po&#347;wist"
  ]
  node [
    id 346
    label "cecha"
  ]
  node [
    id 347
    label "podekscytowanie"
  ]
  node [
    id 348
    label "deformowanie"
  ]
  node [
    id 349
    label "sumienie"
  ]
  node [
    id 350
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 351
    label "deformowa&#263;"
  ]
  node [
    id 352
    label "osobowo&#347;&#263;"
  ]
  node [
    id 353
    label "psychika"
  ]
  node [
    id 354
    label "zjawa"
  ]
  node [
    id 355
    label "istota_nadprzyrodzona"
  ]
  node [
    id 356
    label "power"
  ]
  node [
    id 357
    label "entity"
  ]
  node [
    id 358
    label "ofiarowywa&#263;"
  ]
  node [
    id 359
    label "oddech"
  ]
  node [
    id 360
    label "seksualno&#347;&#263;"
  ]
  node [
    id 361
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 362
    label "byt"
  ]
  node [
    id 363
    label "si&#322;a"
  ]
  node [
    id 364
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 365
    label "ego"
  ]
  node [
    id 366
    label "ofiarowanie"
  ]
  node [
    id 367
    label "kompleksja"
  ]
  node [
    id 368
    label "charakter"
  ]
  node [
    id 369
    label "fizjonomia"
  ]
  node [
    id 370
    label "kompleks"
  ]
  node [
    id 371
    label "shape"
  ]
  node [
    id 372
    label "zapalno&#347;&#263;"
  ]
  node [
    id 373
    label "T&#281;sknica"
  ]
  node [
    id 374
    label "ofiarowa&#263;"
  ]
  node [
    id 375
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 376
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 377
    label "passion"
  ]
  node [
    id 378
    label "kuwada"
  ]
  node [
    id 379
    label "ojciec"
  ]
  node [
    id 380
    label "rodzice"
  ]
  node [
    id 381
    label "ojczym"
  ]
  node [
    id 382
    label "rodzic"
  ]
  node [
    id 383
    label "przodek"
  ]
  node [
    id 384
    label "papa"
  ]
  node [
    id 385
    label "stary"
  ]
  node [
    id 386
    label "starzy"
  ]
  node [
    id 387
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 388
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 389
    label "pokolenie"
  ]
  node [
    id 390
    label "wapniaki"
  ]
  node [
    id 391
    label "opiekun"
  ]
  node [
    id 392
    label "rodzic_chrzestny"
  ]
  node [
    id 393
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 394
    label "ojcowie"
  ]
  node [
    id 395
    label "linea&#380;"
  ]
  node [
    id 396
    label "krewny"
  ]
  node [
    id 397
    label "chodnik"
  ]
  node [
    id 398
    label "w&#243;z"
  ]
  node [
    id 399
    label "p&#322;ug"
  ]
  node [
    id 400
    label "wyrobisko"
  ]
  node [
    id 401
    label "dziad"
  ]
  node [
    id 402
    label "antecesor"
  ]
  node [
    id 403
    label "post&#281;p"
  ]
  node [
    id 404
    label "kszta&#322;ciciel"
  ]
  node [
    id 405
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 406
    label "tworzyciel"
  ]
  node [
    id 407
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 408
    label "&#347;w"
  ]
  node [
    id 409
    label "pomys&#322;odawca"
  ]
  node [
    id 410
    label "wykonawca"
  ]
  node [
    id 411
    label "samiec"
  ]
  node [
    id 412
    label "materia&#322;_budowlany"
  ]
  node [
    id 413
    label "twarz"
  ]
  node [
    id 414
    label "gun_muzzle"
  ]
  node [
    id 415
    label "izolacja"
  ]
  node [
    id 416
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 417
    label "nienowoczesny"
  ]
  node [
    id 418
    label "gruba_ryba"
  ]
  node [
    id 419
    label "zestarzenie_si&#281;"
  ]
  node [
    id 420
    label "poprzedni"
  ]
  node [
    id 421
    label "dawno"
  ]
  node [
    id 422
    label "staro"
  ]
  node [
    id 423
    label "m&#261;&#380;"
  ]
  node [
    id 424
    label "dotychczasowy"
  ]
  node [
    id 425
    label "p&#243;&#378;ny"
  ]
  node [
    id 426
    label "d&#322;ugoletni"
  ]
  node [
    id 427
    label "charakterystyczny"
  ]
  node [
    id 428
    label "brat"
  ]
  node [
    id 429
    label "po_staro&#347;wiecku"
  ]
  node [
    id 430
    label "zwierzchnik"
  ]
  node [
    id 431
    label "znajomy"
  ]
  node [
    id 432
    label "odleg&#322;y"
  ]
  node [
    id 433
    label "starzenie_si&#281;"
  ]
  node [
    id 434
    label "starczo"
  ]
  node [
    id 435
    label "dawniej"
  ]
  node [
    id 436
    label "niegdysiejszy"
  ]
  node [
    id 437
    label "dojrza&#322;y"
  ]
  node [
    id 438
    label "syndrom_kuwady"
  ]
  node [
    id 439
    label "na&#347;ladownictwo"
  ]
  node [
    id 440
    label "zwyczaj"
  ]
  node [
    id 441
    label "ci&#261;&#380;a"
  ]
  node [
    id 442
    label "&#380;onaty"
  ]
  node [
    id 443
    label "Royal"
  ]
  node [
    id 444
    label "mail"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 443
    target 444
  ]
]
