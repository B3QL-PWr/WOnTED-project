graph [
  node [
    id 0
    label "koniec"
    origin "text"
  ]
  node [
    id 1
    label "wojna"
    origin "text"
  ]
  node [
    id 2
    label "przez"
    origin "text"
  ]
  node [
    id 3
    label "prawie"
    origin "text"
  ]
  node [
    id 4
    label "siedemdziesi&#261;t"
    origin "text"
  ]
  node [
    id 5
    label "lato"
    origin "text"
  ]
  node [
    id 6
    label "kolejny"
    origin "text"
  ]
  node [
    id 7
    label "lokator"
    origin "text"
  ]
  node [
    id 8
    label "prywatny"
    origin "text"
  ]
  node [
    id 9
    label "w&#322;a&#347;ciciel"
    origin "text"
  ]
  node [
    id 10
    label "niszczy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "per&#322;a"
    origin "text"
  ]
  node [
    id 12
    label "&#347;l&#261;ski"
    origin "text"
  ]
  node [
    id 13
    label "renesans"
    origin "text"
  ]
  node [
    id 14
    label "zamienia&#263;"
    origin "text"
  ]
  node [
    id 15
    label "pa&#322;ac"
    origin "text"
  ]
  node [
    id 16
    label "gorzan&#243;w"
    origin "text"
  ]
  node [
    id 17
    label "tragiczny"
    origin "text"
  ]
  node [
    id 18
    label "ruina"
    origin "text"
  ]
  node [
    id 19
    label "ostatnie_podrygi"
  ]
  node [
    id 20
    label "visitation"
  ]
  node [
    id 21
    label "agonia"
  ]
  node [
    id 22
    label "defenestracja"
  ]
  node [
    id 23
    label "punkt"
  ]
  node [
    id 24
    label "dzia&#322;anie"
  ]
  node [
    id 25
    label "kres"
  ]
  node [
    id 26
    label "wydarzenie"
  ]
  node [
    id 27
    label "mogi&#322;a"
  ]
  node [
    id 28
    label "kres_&#380;ycia"
  ]
  node [
    id 29
    label "szereg"
  ]
  node [
    id 30
    label "szeol"
  ]
  node [
    id 31
    label "pogrzebanie"
  ]
  node [
    id 32
    label "miejsce"
  ]
  node [
    id 33
    label "chwila"
  ]
  node [
    id 34
    label "&#380;a&#322;oba"
  ]
  node [
    id 35
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 36
    label "zabicie"
  ]
  node [
    id 37
    label "przebiec"
  ]
  node [
    id 38
    label "charakter"
  ]
  node [
    id 39
    label "czynno&#347;&#263;"
  ]
  node [
    id 40
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 41
    label "motyw"
  ]
  node [
    id 42
    label "przebiegni&#281;cie"
  ]
  node [
    id 43
    label "fabu&#322;a"
  ]
  node [
    id 44
    label "Rzym_Zachodni"
  ]
  node [
    id 45
    label "whole"
  ]
  node [
    id 46
    label "ilo&#347;&#263;"
  ]
  node [
    id 47
    label "element"
  ]
  node [
    id 48
    label "Rzym_Wschodni"
  ]
  node [
    id 49
    label "urz&#261;dzenie"
  ]
  node [
    id 50
    label "warunek_lokalowy"
  ]
  node [
    id 51
    label "plac"
  ]
  node [
    id 52
    label "location"
  ]
  node [
    id 53
    label "uwaga"
  ]
  node [
    id 54
    label "przestrze&#324;"
  ]
  node [
    id 55
    label "status"
  ]
  node [
    id 56
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 57
    label "cia&#322;o"
  ]
  node [
    id 58
    label "cecha"
  ]
  node [
    id 59
    label "praca"
  ]
  node [
    id 60
    label "rz&#261;d"
  ]
  node [
    id 61
    label "time"
  ]
  node [
    id 62
    label "czas"
  ]
  node [
    id 63
    label "&#347;mier&#263;"
  ]
  node [
    id 64
    label "death"
  ]
  node [
    id 65
    label "upadek"
  ]
  node [
    id 66
    label "zmierzch"
  ]
  node [
    id 67
    label "stan"
  ]
  node [
    id 68
    label "nieuleczalnie_chory"
  ]
  node [
    id 69
    label "spocz&#261;&#263;"
  ]
  node [
    id 70
    label "spocz&#281;cie"
  ]
  node [
    id 71
    label "pochowanie"
  ]
  node [
    id 72
    label "spoczywa&#263;"
  ]
  node [
    id 73
    label "chowanie"
  ]
  node [
    id 74
    label "park_sztywnych"
  ]
  node [
    id 75
    label "pomnik"
  ]
  node [
    id 76
    label "nagrobek"
  ]
  node [
    id 77
    label "prochowisko"
  ]
  node [
    id 78
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 79
    label "spoczywanie"
  ]
  node [
    id 80
    label "za&#347;wiaty"
  ]
  node [
    id 81
    label "piek&#322;o"
  ]
  node [
    id 82
    label "judaizm"
  ]
  node [
    id 83
    label "wyrzucenie"
  ]
  node [
    id 84
    label "defenestration"
  ]
  node [
    id 85
    label "zaj&#347;cie"
  ]
  node [
    id 86
    label "&#380;al"
  ]
  node [
    id 87
    label "paznokie&#263;"
  ]
  node [
    id 88
    label "symbol"
  ]
  node [
    id 89
    label "kir"
  ]
  node [
    id 90
    label "brud"
  ]
  node [
    id 91
    label "burying"
  ]
  node [
    id 92
    label "zasypanie"
  ]
  node [
    id 93
    label "zw&#322;oki"
  ]
  node [
    id 94
    label "burial"
  ]
  node [
    id 95
    label "w&#322;o&#380;enie"
  ]
  node [
    id 96
    label "porobienie"
  ]
  node [
    id 97
    label "gr&#243;b"
  ]
  node [
    id 98
    label "uniemo&#380;liwienie"
  ]
  node [
    id 99
    label "destruction"
  ]
  node [
    id 100
    label "zabrzmienie"
  ]
  node [
    id 101
    label "skrzywdzenie"
  ]
  node [
    id 102
    label "pozabijanie"
  ]
  node [
    id 103
    label "zniszczenie"
  ]
  node [
    id 104
    label "zaszkodzenie"
  ]
  node [
    id 105
    label "usuni&#281;cie"
  ]
  node [
    id 106
    label "spowodowanie"
  ]
  node [
    id 107
    label "killing"
  ]
  node [
    id 108
    label "zdarzenie_si&#281;"
  ]
  node [
    id 109
    label "czyn"
  ]
  node [
    id 110
    label "umarcie"
  ]
  node [
    id 111
    label "granie"
  ]
  node [
    id 112
    label "zamkni&#281;cie"
  ]
  node [
    id 113
    label "compaction"
  ]
  node [
    id 114
    label "po&#322;o&#380;enie"
  ]
  node [
    id 115
    label "sprawa"
  ]
  node [
    id 116
    label "ust&#281;p"
  ]
  node [
    id 117
    label "plan"
  ]
  node [
    id 118
    label "obiekt_matematyczny"
  ]
  node [
    id 119
    label "problemat"
  ]
  node [
    id 120
    label "plamka"
  ]
  node [
    id 121
    label "stopie&#324;_pisma"
  ]
  node [
    id 122
    label "jednostka"
  ]
  node [
    id 123
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 124
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 125
    label "mark"
  ]
  node [
    id 126
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 127
    label "prosta"
  ]
  node [
    id 128
    label "problematyka"
  ]
  node [
    id 129
    label "obiekt"
  ]
  node [
    id 130
    label "zapunktowa&#263;"
  ]
  node [
    id 131
    label "podpunkt"
  ]
  node [
    id 132
    label "wojsko"
  ]
  node [
    id 133
    label "point"
  ]
  node [
    id 134
    label "pozycja"
  ]
  node [
    id 135
    label "szpaler"
  ]
  node [
    id 136
    label "zbi&#243;r"
  ]
  node [
    id 137
    label "column"
  ]
  node [
    id 138
    label "uporz&#261;dkowanie"
  ]
  node [
    id 139
    label "mn&#243;stwo"
  ]
  node [
    id 140
    label "unit"
  ]
  node [
    id 141
    label "rozmieszczenie"
  ]
  node [
    id 142
    label "tract"
  ]
  node [
    id 143
    label "wyra&#380;enie"
  ]
  node [
    id 144
    label "infimum"
  ]
  node [
    id 145
    label "powodowanie"
  ]
  node [
    id 146
    label "liczenie"
  ]
  node [
    id 147
    label "cz&#322;owiek"
  ]
  node [
    id 148
    label "skutek"
  ]
  node [
    id 149
    label "podzia&#322;anie"
  ]
  node [
    id 150
    label "supremum"
  ]
  node [
    id 151
    label "kampania"
  ]
  node [
    id 152
    label "uruchamianie"
  ]
  node [
    id 153
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 154
    label "operacja"
  ]
  node [
    id 155
    label "hipnotyzowanie"
  ]
  node [
    id 156
    label "robienie"
  ]
  node [
    id 157
    label "uruchomienie"
  ]
  node [
    id 158
    label "nakr&#281;canie"
  ]
  node [
    id 159
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 160
    label "matematyka"
  ]
  node [
    id 161
    label "reakcja_chemiczna"
  ]
  node [
    id 162
    label "tr&#243;jstronny"
  ]
  node [
    id 163
    label "natural_process"
  ]
  node [
    id 164
    label "nakr&#281;cenie"
  ]
  node [
    id 165
    label "zatrzymanie"
  ]
  node [
    id 166
    label "wp&#322;yw"
  ]
  node [
    id 167
    label "rzut"
  ]
  node [
    id 168
    label "podtrzymywanie"
  ]
  node [
    id 169
    label "w&#322;&#261;czanie"
  ]
  node [
    id 170
    label "liczy&#263;"
  ]
  node [
    id 171
    label "operation"
  ]
  node [
    id 172
    label "rezultat"
  ]
  node [
    id 173
    label "dzianie_si&#281;"
  ]
  node [
    id 174
    label "zadzia&#322;anie"
  ]
  node [
    id 175
    label "priorytet"
  ]
  node [
    id 176
    label "bycie"
  ]
  node [
    id 177
    label "rozpocz&#281;cie"
  ]
  node [
    id 178
    label "docieranie"
  ]
  node [
    id 179
    label "funkcja"
  ]
  node [
    id 180
    label "czynny"
  ]
  node [
    id 181
    label "impact"
  ]
  node [
    id 182
    label "oferta"
  ]
  node [
    id 183
    label "zako&#324;czenie"
  ]
  node [
    id 184
    label "act"
  ]
  node [
    id 185
    label "wdzieranie_si&#281;"
  ]
  node [
    id 186
    label "w&#322;&#261;czenie"
  ]
  node [
    id 187
    label "war"
  ]
  node [
    id 188
    label "walka"
  ]
  node [
    id 189
    label "angaria"
  ]
  node [
    id 190
    label "zimna_wojna"
  ]
  node [
    id 191
    label "zbrodnia_przeciw_pokojowi"
  ]
  node [
    id 192
    label "konflikt"
  ]
  node [
    id 193
    label "sp&#243;r"
  ]
  node [
    id 194
    label "wojna_stuletnia"
  ]
  node [
    id 195
    label "wr&#243;g"
  ]
  node [
    id 196
    label "zbrodnia_przeciw_ludzko&#347;ci"
  ]
  node [
    id 197
    label "gra_w_karty"
  ]
  node [
    id 198
    label "burza"
  ]
  node [
    id 199
    label "zbrodnia_wojenna"
  ]
  node [
    id 200
    label "clash"
  ]
  node [
    id 201
    label "wsp&#243;r"
  ]
  node [
    id 202
    label "obrona"
  ]
  node [
    id 203
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 204
    label "zaatakowanie"
  ]
  node [
    id 205
    label "konfrontacyjny"
  ]
  node [
    id 206
    label "contest"
  ]
  node [
    id 207
    label "action"
  ]
  node [
    id 208
    label "sambo"
  ]
  node [
    id 209
    label "rywalizacja"
  ]
  node [
    id 210
    label "trudno&#347;&#263;"
  ]
  node [
    id 211
    label "wrestle"
  ]
  node [
    id 212
    label "military_action"
  ]
  node [
    id 213
    label "przeciwnik"
  ]
  node [
    id 214
    label "czynnik"
  ]
  node [
    id 215
    label "posta&#263;"
  ]
  node [
    id 216
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 217
    label "prawo"
  ]
  node [
    id 218
    label "grzmienie"
  ]
  node [
    id 219
    label "pogrzmot"
  ]
  node [
    id 220
    label "zjawisko"
  ]
  node [
    id 221
    label "nieporz&#261;dek"
  ]
  node [
    id 222
    label "rioting"
  ]
  node [
    id 223
    label "scene"
  ]
  node [
    id 224
    label "nawa&#322;no&#347;&#263;"
  ]
  node [
    id 225
    label "zagrzmie&#263;"
  ]
  node [
    id 226
    label "grzmie&#263;"
  ]
  node [
    id 227
    label "burza_piaskowa"
  ]
  node [
    id 228
    label "deszcz"
  ]
  node [
    id 229
    label "piorun"
  ]
  node [
    id 230
    label "chmura"
  ]
  node [
    id 231
    label "nawa&#322;"
  ]
  node [
    id 232
    label "zagrzmienie"
  ]
  node [
    id 233
    label "fire"
  ]
  node [
    id 234
    label "wrz&#261;tek"
  ]
  node [
    id 235
    label "ciep&#322;o"
  ]
  node [
    id 236
    label "gor&#261;co"
  ]
  node [
    id 237
    label "pora_roku"
  ]
  node [
    id 238
    label "nast&#281;pnie"
  ]
  node [
    id 239
    label "inny"
  ]
  node [
    id 240
    label "nastopny"
  ]
  node [
    id 241
    label "kolejno"
  ]
  node [
    id 242
    label "kt&#243;ry&#347;"
  ]
  node [
    id 243
    label "osobno"
  ]
  node [
    id 244
    label "r&#243;&#380;ny"
  ]
  node [
    id 245
    label "inszy"
  ]
  node [
    id 246
    label "inaczej"
  ]
  node [
    id 247
    label "najemca"
  ]
  node [
    id 248
    label "urz&#261;dzenie_nawigacyjne"
  ]
  node [
    id 249
    label "mieszkaniec"
  ]
  node [
    id 250
    label "za&#322;o&#380;yciel"
  ]
  node [
    id 251
    label "u&#380;ytkownik"
  ]
  node [
    id 252
    label "ludno&#347;&#263;"
  ]
  node [
    id 253
    label "zwierz&#281;"
  ]
  node [
    id 254
    label "inicjator"
  ]
  node [
    id 255
    label "autor"
  ]
  node [
    id 256
    label "podmiot"
  ]
  node [
    id 257
    label "wykupienie"
  ]
  node [
    id 258
    label "bycie_w_posiadaniu"
  ]
  node [
    id 259
    label "wykupywanie"
  ]
  node [
    id 260
    label "przedmiot"
  ]
  node [
    id 261
    label "kom&#243;rka"
  ]
  node [
    id 262
    label "furnishing"
  ]
  node [
    id 263
    label "zabezpieczenie"
  ]
  node [
    id 264
    label "zrobienie"
  ]
  node [
    id 265
    label "wyrz&#261;dzenie"
  ]
  node [
    id 266
    label "zagospodarowanie"
  ]
  node [
    id 267
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 268
    label "ig&#322;a"
  ]
  node [
    id 269
    label "narz&#281;dzie"
  ]
  node [
    id 270
    label "wirnik"
  ]
  node [
    id 271
    label "aparatura"
  ]
  node [
    id 272
    label "system_energetyczny"
  ]
  node [
    id 273
    label "impulsator"
  ]
  node [
    id 274
    label "mechanizm"
  ]
  node [
    id 275
    label "sprz&#281;t"
  ]
  node [
    id 276
    label "blokowanie"
  ]
  node [
    id 277
    label "set"
  ]
  node [
    id 278
    label "zablokowanie"
  ]
  node [
    id 279
    label "przygotowanie"
  ]
  node [
    id 280
    label "komora"
  ]
  node [
    id 281
    label "j&#281;zyk"
  ]
  node [
    id 282
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 283
    label "niepubliczny"
  ]
  node [
    id 284
    label "czyj&#347;"
  ]
  node [
    id 285
    label "personalny"
  ]
  node [
    id 286
    label "prywatnie"
  ]
  node [
    id 287
    label "nieformalny"
  ]
  node [
    id 288
    label "w&#322;asny"
  ]
  node [
    id 289
    label "samodzielny"
  ]
  node [
    id 290
    label "zwi&#261;zany"
  ]
  node [
    id 291
    label "swoisty"
  ]
  node [
    id 292
    label "osobny"
  ]
  node [
    id 293
    label "nieoficjalny"
  ]
  node [
    id 294
    label "nieformalnie"
  ]
  node [
    id 295
    label "personalnie"
  ]
  node [
    id 296
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 297
    label "byt"
  ]
  node [
    id 298
    label "osobowo&#347;&#263;"
  ]
  node [
    id 299
    label "organizacja"
  ]
  node [
    id 300
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 301
    label "nauka_prawa"
  ]
  node [
    id 302
    label "wyczerpanie"
  ]
  node [
    id 303
    label "odkupienie"
  ]
  node [
    id 304
    label "wyswobodzenie"
  ]
  node [
    id 305
    label "kupienie"
  ]
  node [
    id 306
    label "ransom"
  ]
  node [
    id 307
    label "kupowanie"
  ]
  node [
    id 308
    label "odkupywanie"
  ]
  node [
    id 309
    label "wyczerpywanie"
  ]
  node [
    id 310
    label "wyswobadzanie"
  ]
  node [
    id 311
    label "destroy"
  ]
  node [
    id 312
    label "uszkadza&#263;"
  ]
  node [
    id 313
    label "os&#322;abia&#263;"
  ]
  node [
    id 314
    label "szkodzi&#263;"
  ]
  node [
    id 315
    label "zdrowie"
  ]
  node [
    id 316
    label "mar"
  ]
  node [
    id 317
    label "wygrywa&#263;"
  ]
  node [
    id 318
    label "powodowa&#263;"
  ]
  node [
    id 319
    label "pamper"
  ]
  node [
    id 320
    label "strike"
  ]
  node [
    id 321
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 322
    label "robi&#263;"
  ]
  node [
    id 323
    label "muzykowa&#263;"
  ]
  node [
    id 324
    label "mie&#263;_miejsce"
  ]
  node [
    id 325
    label "play"
  ]
  node [
    id 326
    label "znosi&#263;"
  ]
  node [
    id 327
    label "zagwarantowywa&#263;"
  ]
  node [
    id 328
    label "osi&#261;ga&#263;"
  ]
  node [
    id 329
    label "gra&#263;"
  ]
  node [
    id 330
    label "net_income"
  ]
  node [
    id 331
    label "instrument_muzyczny"
  ]
  node [
    id 332
    label "suppress"
  ]
  node [
    id 333
    label "os&#322;abianie"
  ]
  node [
    id 334
    label "os&#322;abienie"
  ]
  node [
    id 335
    label "kondycja_fizyczna"
  ]
  node [
    id 336
    label "os&#322;abi&#263;"
  ]
  node [
    id 337
    label "zmniejsza&#263;"
  ]
  node [
    id 338
    label "bate"
  ]
  node [
    id 339
    label "dzia&#322;a&#263;"
  ]
  node [
    id 340
    label "wrong"
  ]
  node [
    id 341
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 342
    label "motywowa&#263;"
  ]
  node [
    id 343
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 344
    label "narusza&#263;"
  ]
  node [
    id 345
    label "kondycja"
  ]
  node [
    id 346
    label "zedrze&#263;"
  ]
  node [
    id 347
    label "niszczenie"
  ]
  node [
    id 348
    label "soundness"
  ]
  node [
    id 349
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 350
    label "zdarcie"
  ]
  node [
    id 351
    label "firmness"
  ]
  node [
    id 352
    label "rozsypanie_si&#281;"
  ]
  node [
    id 353
    label "zniszczy&#263;"
  ]
  node [
    id 354
    label "gem"
  ]
  node [
    id 355
    label "talent"
  ]
  node [
    id 356
    label "obiekt_naturalny"
  ]
  node [
    id 357
    label "tworzywo"
  ]
  node [
    id 358
    label "mineraloid"
  ]
  node [
    id 359
    label "mieszanina"
  ]
  node [
    id 360
    label "substancja"
  ]
  node [
    id 361
    label "ludzko&#347;&#263;"
  ]
  node [
    id 362
    label "asymilowanie"
  ]
  node [
    id 363
    label "wapniak"
  ]
  node [
    id 364
    label "asymilowa&#263;"
  ]
  node [
    id 365
    label "hominid"
  ]
  node [
    id 366
    label "podw&#322;adny"
  ]
  node [
    id 367
    label "g&#322;owa"
  ]
  node [
    id 368
    label "figura"
  ]
  node [
    id 369
    label "portrecista"
  ]
  node [
    id 370
    label "dwun&#243;g"
  ]
  node [
    id 371
    label "profanum"
  ]
  node [
    id 372
    label "mikrokosmos"
  ]
  node [
    id 373
    label "nasada"
  ]
  node [
    id 374
    label "duch"
  ]
  node [
    id 375
    label "antropochoria"
  ]
  node [
    id 376
    label "osoba"
  ]
  node [
    id 377
    label "wz&#243;r"
  ]
  node [
    id 378
    label "senior"
  ]
  node [
    id 379
    label "oddzia&#322;ywanie"
  ]
  node [
    id 380
    label "Adam"
  ]
  node [
    id 381
    label "homo_sapiens"
  ]
  node [
    id 382
    label "polifag"
  ]
  node [
    id 383
    label "tennis"
  ]
  node [
    id 384
    label "brylant"
  ]
  node [
    id 385
    label "dyspozycja"
  ]
  node [
    id 386
    label "gigant"
  ]
  node [
    id 387
    label "faculty"
  ]
  node [
    id 388
    label "stygmat"
  ]
  node [
    id 389
    label "moneta"
  ]
  node [
    id 390
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 391
    label "cug"
  ]
  node [
    id 392
    label "krepel"
  ]
  node [
    id 393
    label "francuz"
  ]
  node [
    id 394
    label "mietlorz"
  ]
  node [
    id 395
    label "etnolekt"
  ]
  node [
    id 396
    label "sza&#322;ot"
  ]
  node [
    id 397
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 398
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 399
    label "regionalny"
  ]
  node [
    id 400
    label "polski"
  ]
  node [
    id 401
    label "halba"
  ]
  node [
    id 402
    label "ch&#322;opiec"
  ]
  node [
    id 403
    label "buchta"
  ]
  node [
    id 404
    label "czarne_kluski"
  ]
  node [
    id 405
    label "szpajza"
  ]
  node [
    id 406
    label "szl&#261;ski"
  ]
  node [
    id 407
    label "&#347;lonski"
  ]
  node [
    id 408
    label "po_&#347;l&#261;sku"
  ]
  node [
    id 409
    label "waloszek"
  ]
  node [
    id 410
    label "j&#281;zyk_naturalny"
  ]
  node [
    id 411
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 412
    label "nale&#380;ny"
  ]
  node [
    id 413
    label "nale&#380;yty"
  ]
  node [
    id 414
    label "typowy"
  ]
  node [
    id 415
    label "uprawniony"
  ]
  node [
    id 416
    label "zasadniczy"
  ]
  node [
    id 417
    label "stosownie"
  ]
  node [
    id 418
    label "taki"
  ]
  node [
    id 419
    label "charakterystyczny"
  ]
  node [
    id 420
    label "prawdziwy"
  ]
  node [
    id 421
    label "ten"
  ]
  node [
    id 422
    label "dobry"
  ]
  node [
    id 423
    label "tradycyjny"
  ]
  node [
    id 424
    label "regionalnie"
  ]
  node [
    id 425
    label "lokalny"
  ]
  node [
    id 426
    label "Polish"
  ]
  node [
    id 427
    label "goniony"
  ]
  node [
    id 428
    label "oberek"
  ]
  node [
    id 429
    label "ryba_po_grecku"
  ]
  node [
    id 430
    label "sztajer"
  ]
  node [
    id 431
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 432
    label "krakowiak"
  ]
  node [
    id 433
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 434
    label "pierogi_ruskie"
  ]
  node [
    id 435
    label "lacki"
  ]
  node [
    id 436
    label "polak"
  ]
  node [
    id 437
    label "chodzony"
  ]
  node [
    id 438
    label "po_polsku"
  ]
  node [
    id 439
    label "mazur"
  ]
  node [
    id 440
    label "polsko"
  ]
  node [
    id 441
    label "skoczny"
  ]
  node [
    id 442
    label "drabant"
  ]
  node [
    id 443
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 444
    label "g&#243;rno&#347;l&#261;ski"
  ]
  node [
    id 445
    label "po_&#347;lonsku"
  ]
  node [
    id 446
    label "po_szl&#261;sku"
  ]
  node [
    id 447
    label "taniec_ludowy"
  ]
  node [
    id 448
    label "melodia"
  ]
  node [
    id 449
    label "taniec"
  ]
  node [
    id 450
    label "teren"
  ]
  node [
    id 451
    label "dro&#380;d&#380;owy"
  ]
  node [
    id 452
    label "nadzienie"
  ]
  node [
    id 453
    label "pampuch"
  ]
  node [
    id 454
    label "zatoka"
  ]
  node [
    id 455
    label "pomieszczenie"
  ]
  node [
    id 456
    label "dro&#380;d&#380;&#243;wka"
  ]
  node [
    id 457
    label "dzik"
  ]
  node [
    id 458
    label "zw&#243;j"
  ]
  node [
    id 459
    label "p&#261;czek"
  ]
  node [
    id 460
    label "bu&#322;ka_paryska"
  ]
  node [
    id 461
    label "klucz_nastawny"
  ]
  node [
    id 462
    label "warkocz"
  ]
  node [
    id 463
    label "francuski"
  ]
  node [
    id 464
    label "kufel"
  ]
  node [
    id 465
    label "skwarek"
  ]
  node [
    id 466
    label "sa&#322;atka"
  ]
  node [
    id 467
    label "musztarda"
  ]
  node [
    id 468
    label "g&#243;wniarz"
  ]
  node [
    id 469
    label "synek"
  ]
  node [
    id 470
    label "boyfriend"
  ]
  node [
    id 471
    label "okrzos"
  ]
  node [
    id 472
    label "dziecko"
  ]
  node [
    id 473
    label "sympatia"
  ]
  node [
    id 474
    label "usynowienie"
  ]
  node [
    id 475
    label "pomocnik"
  ]
  node [
    id 476
    label "kawaler"
  ]
  node [
    id 477
    label "pederasta"
  ]
  node [
    id 478
    label "m&#322;odzieniec"
  ]
  node [
    id 479
    label "kajtek"
  ]
  node [
    id 480
    label "usynawianie"
  ]
  node [
    id 481
    label "deser"
  ]
  node [
    id 482
    label "para"
  ]
  node [
    id 483
    label "pr&#261;d"
  ]
  node [
    id 484
    label "poci&#261;g"
  ]
  node [
    id 485
    label "draft"
  ]
  node [
    id 486
    label "ci&#261;g"
  ]
  node [
    id 487
    label "zaprz&#281;g"
  ]
  node [
    id 488
    label "Rafael"
  ]
  node [
    id 489
    label "Machiavelli"
  ]
  node [
    id 490
    label "cinquecento"
  ]
  node [
    id 491
    label "nowo&#380;ytno&#347;&#263;"
  ]
  node [
    id 492
    label "quattrocento"
  ]
  node [
    id 493
    label "rozkwit"
  ]
  node [
    id 494
    label "humanizm"
  ]
  node [
    id 495
    label "manieryzm"
  ]
  node [
    id 496
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 497
    label "Cinquecento"
  ]
  node [
    id 498
    label "samoch&#243;d"
  ]
  node [
    id 499
    label "fiat"
  ]
  node [
    id 500
    label "epoka"
  ]
  node [
    id 501
    label "o&#347;wiecenie"
  ]
  node [
    id 502
    label "barok"
  ]
  node [
    id 503
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 504
    label "postawa"
  ]
  node [
    id 505
    label "ruch"
  ]
  node [
    id 506
    label "humanism"
  ]
  node [
    id 507
    label "szko&#322;a"
  ]
  node [
    id 508
    label "styl"
  ]
  node [
    id 509
    label "fashion"
  ]
  node [
    id 510
    label "rozw&#243;j"
  ]
  node [
    id 511
    label "blooming"
  ]
  node [
    id 512
    label "wegetacja"
  ]
  node [
    id 513
    label "zmienia&#263;"
  ]
  node [
    id 514
    label "zakomunikowa&#263;"
  ]
  node [
    id 515
    label "zast&#281;powa&#263;"
  ]
  node [
    id 516
    label "mienia&#263;"
  ]
  node [
    id 517
    label "decydowa&#263;"
  ]
  node [
    id 518
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 519
    label "spowodowa&#263;"
  ]
  node [
    id 520
    label "traci&#263;"
  ]
  node [
    id 521
    label "alternate"
  ]
  node [
    id 522
    label "change"
  ]
  node [
    id 523
    label "reengineering"
  ]
  node [
    id 524
    label "sprawia&#263;"
  ]
  node [
    id 525
    label "zyskiwa&#263;"
  ]
  node [
    id 526
    label "przechodzi&#263;"
  ]
  node [
    id 527
    label "handlowa&#263;"
  ]
  node [
    id 528
    label "myli&#263;"
  ]
  node [
    id 529
    label "budynek"
  ]
  node [
    id 530
    label "Wersal"
  ]
  node [
    id 531
    label "Belweder"
  ]
  node [
    id 532
    label "rezydencja"
  ]
  node [
    id 533
    label "w&#322;adza"
  ]
  node [
    id 534
    label "struktura"
  ]
  node [
    id 535
    label "rz&#261;dzenie"
  ]
  node [
    id 536
    label "panowanie"
  ]
  node [
    id 537
    label "Kreml"
  ]
  node [
    id 538
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 539
    label "wydolno&#347;&#263;"
  ]
  node [
    id 540
    label "grupa"
  ]
  node [
    id 541
    label "siedziba"
  ]
  node [
    id 542
    label "dom"
  ]
  node [
    id 543
    label "balkon"
  ]
  node [
    id 544
    label "budowla"
  ]
  node [
    id 545
    label "pod&#322;oga"
  ]
  node [
    id 546
    label "kondygnacja"
  ]
  node [
    id 547
    label "skrzyd&#322;o"
  ]
  node [
    id 548
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 549
    label "dach"
  ]
  node [
    id 550
    label "strop"
  ]
  node [
    id 551
    label "klatka_schodowa"
  ]
  node [
    id 552
    label "przedpro&#380;e"
  ]
  node [
    id 553
    label "Pentagon"
  ]
  node [
    id 554
    label "alkierz"
  ]
  node [
    id 555
    label "front"
  ]
  node [
    id 556
    label "straszny"
  ]
  node [
    id 557
    label "dramatyczny"
  ]
  node [
    id 558
    label "nacechowany"
  ]
  node [
    id 559
    label "tragicznie"
  ]
  node [
    id 560
    label "&#347;miertelny"
  ]
  node [
    id 561
    label "feralny"
  ]
  node [
    id 562
    label "pechowy"
  ]
  node [
    id 563
    label "traiczny"
  ]
  node [
    id 564
    label "koszmarny"
  ]
  node [
    id 565
    label "nieszcz&#281;sny"
  ]
  node [
    id 566
    label "wstrz&#261;saj&#261;cy"
  ]
  node [
    id 567
    label "nieudany"
  ]
  node [
    id 568
    label "niepomy&#347;lny"
  ]
  node [
    id 569
    label "nieszcz&#281;&#347;liwie"
  ]
  node [
    id 570
    label "nieszcz&#281;&#347;liwy"
  ]
  node [
    id 571
    label "feralnie"
  ]
  node [
    id 572
    label "emocjonuj&#261;cy"
  ]
  node [
    id 573
    label "przej&#281;ty"
  ]
  node [
    id 574
    label "powa&#380;ny"
  ]
  node [
    id 575
    label "dynamiczny"
  ]
  node [
    id 576
    label "dramatycznie"
  ]
  node [
    id 577
    label "niegrzeczny"
  ]
  node [
    id 578
    label "olbrzymi"
  ]
  node [
    id 579
    label "niemoralny"
  ]
  node [
    id 580
    label "kurewski"
  ]
  node [
    id 581
    label "strasznie"
  ]
  node [
    id 582
    label "pora&#380;aj&#261;co"
  ]
  node [
    id 583
    label "przejmuj&#261;cy"
  ]
  node [
    id 584
    label "zwyczajny"
  ]
  node [
    id 585
    label "typowo"
  ]
  node [
    id 586
    label "cz&#281;sty"
  ]
  node [
    id 587
    label "zwyk&#322;y"
  ]
  node [
    id 588
    label "koszmarnie"
  ]
  node [
    id 589
    label "straszliwy"
  ]
  node [
    id 590
    label "makabrycznie"
  ]
  node [
    id 591
    label "okropny"
  ]
  node [
    id 592
    label "senny"
  ]
  node [
    id 593
    label "masakryczny"
  ]
  node [
    id 594
    label "&#347;miertelnie"
  ]
  node [
    id 595
    label "niebezpieczny"
  ]
  node [
    id 596
    label "przed&#347;miertelny"
  ]
  node [
    id 597
    label "zaciek&#322;y"
  ]
  node [
    id 598
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 599
    label "ko&#324;cowy"
  ]
  node [
    id 600
    label "za&#380;arty"
  ]
  node [
    id 601
    label "wielki"
  ]
  node [
    id 602
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 603
    label "descent"
  ]
  node [
    id 604
    label "reszta"
  ]
  node [
    id 605
    label "trace"
  ]
  node [
    id 606
    label "&#347;wiadectwo"
  ]
  node [
    id 607
    label "Ohio"
  ]
  node [
    id 608
    label "wci&#281;cie"
  ]
  node [
    id 609
    label "Nowy_York"
  ]
  node [
    id 610
    label "warstwa"
  ]
  node [
    id 611
    label "samopoczucie"
  ]
  node [
    id 612
    label "Illinois"
  ]
  node [
    id 613
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 614
    label "state"
  ]
  node [
    id 615
    label "Jukatan"
  ]
  node [
    id 616
    label "Kalifornia"
  ]
  node [
    id 617
    label "Wirginia"
  ]
  node [
    id 618
    label "wektor"
  ]
  node [
    id 619
    label "by&#263;"
  ]
  node [
    id 620
    label "Teksas"
  ]
  node [
    id 621
    label "Goa"
  ]
  node [
    id 622
    label "Waszyngton"
  ]
  node [
    id 623
    label "Massachusetts"
  ]
  node [
    id 624
    label "Alaska"
  ]
  node [
    id 625
    label "Arakan"
  ]
  node [
    id 626
    label "Hawaje"
  ]
  node [
    id 627
    label "Maryland"
  ]
  node [
    id 628
    label "Michigan"
  ]
  node [
    id 629
    label "Arizona"
  ]
  node [
    id 630
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 631
    label "Georgia"
  ]
  node [
    id 632
    label "poziom"
  ]
  node [
    id 633
    label "Pensylwania"
  ]
  node [
    id 634
    label "shape"
  ]
  node [
    id 635
    label "Luizjana"
  ]
  node [
    id 636
    label "Nowy_Meksyk"
  ]
  node [
    id 637
    label "Alabama"
  ]
  node [
    id 638
    label "Kansas"
  ]
  node [
    id 639
    label "Oregon"
  ]
  node [
    id 640
    label "Floryda"
  ]
  node [
    id 641
    label "Oklahoma"
  ]
  node [
    id 642
    label "jednostka_administracyjna"
  ]
  node [
    id 643
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 644
    label "Gorzan&#243;w"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 404
  ]
  edge [
    source 12
    target 405
  ]
  edge [
    source 12
    target 406
  ]
  edge [
    source 12
    target 407
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 12
    target 413
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 418
  ]
  edge [
    source 12
    target 419
  ]
  edge [
    source 12
    target 420
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 260
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 281
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 56
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 513
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 522
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 529
  ]
  edge [
    source 15
    target 530
  ]
  edge [
    source 15
    target 531
  ]
  edge [
    source 15
    target 532
  ]
  edge [
    source 15
    target 533
  ]
  edge [
    source 15
    target 534
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 535
  ]
  edge [
    source 15
    target 536
  ]
  edge [
    source 15
    target 537
  ]
  edge [
    source 15
    target 538
  ]
  edge [
    source 15
    target 539
  ]
  edge [
    source 15
    target 540
  ]
  edge [
    source 15
    target 60
  ]
  edge [
    source 15
    target 541
  ]
  edge [
    source 15
    target 542
  ]
  edge [
    source 15
    target 543
  ]
  edge [
    source 15
    target 544
  ]
  edge [
    source 15
    target 545
  ]
  edge [
    source 15
    target 546
  ]
  edge [
    source 15
    target 547
  ]
  edge [
    source 15
    target 548
  ]
  edge [
    source 15
    target 549
  ]
  edge [
    source 15
    target 550
  ]
  edge [
    source 15
    target 551
  ]
  edge [
    source 15
    target 552
  ]
  edge [
    source 15
    target 553
  ]
  edge [
    source 15
    target 554
  ]
  edge [
    source 15
    target 555
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 556
  ]
  edge [
    source 17
    target 557
  ]
  edge [
    source 17
    target 558
  ]
  edge [
    source 17
    target 559
  ]
  edge [
    source 17
    target 560
  ]
  edge [
    source 17
    target 561
  ]
  edge [
    source 17
    target 562
  ]
  edge [
    source 17
    target 414
  ]
  edge [
    source 17
    target 563
  ]
  edge [
    source 17
    target 564
  ]
  edge [
    source 17
    target 565
  ]
  edge [
    source 17
    target 566
  ]
  edge [
    source 17
    target 567
  ]
  edge [
    source 17
    target 568
  ]
  edge [
    source 17
    target 569
  ]
  edge [
    source 17
    target 570
  ]
  edge [
    source 17
    target 571
  ]
  edge [
    source 17
    target 572
  ]
  edge [
    source 17
    target 397
  ]
  edge [
    source 17
    target 573
  ]
  edge [
    source 17
    target 574
  ]
  edge [
    source 17
    target 575
  ]
  edge [
    source 17
    target 576
  ]
  edge [
    source 17
    target 577
  ]
  edge [
    source 17
    target 578
  ]
  edge [
    source 17
    target 579
  ]
  edge [
    source 17
    target 580
  ]
  edge [
    source 17
    target 581
  ]
  edge [
    source 17
    target 582
  ]
  edge [
    source 17
    target 583
  ]
  edge [
    source 17
    target 584
  ]
  edge [
    source 17
    target 585
  ]
  edge [
    source 17
    target 586
  ]
  edge [
    source 17
    target 587
  ]
  edge [
    source 17
    target 588
  ]
  edge [
    source 17
    target 589
  ]
  edge [
    source 17
    target 590
  ]
  edge [
    source 17
    target 591
  ]
  edge [
    source 17
    target 592
  ]
  edge [
    source 17
    target 593
  ]
  edge [
    source 17
    target 594
  ]
  edge [
    source 17
    target 595
  ]
  edge [
    source 17
    target 596
  ]
  edge [
    source 17
    target 597
  ]
  edge [
    source 17
    target 598
  ]
  edge [
    source 17
    target 599
  ]
  edge [
    source 17
    target 600
  ]
  edge [
    source 17
    target 601
  ]
  edge [
    source 18
    target 67
  ]
  edge [
    source 18
    target 602
  ]
  edge [
    source 18
    target 603
  ]
  edge [
    source 18
    target 604
  ]
  edge [
    source 18
    target 605
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 606
  ]
  edge [
    source 18
    target 607
  ]
  edge [
    source 18
    target 608
  ]
  edge [
    source 18
    target 609
  ]
  edge [
    source 18
    target 610
  ]
  edge [
    source 18
    target 611
  ]
  edge [
    source 18
    target 612
  ]
  edge [
    source 18
    target 613
  ]
  edge [
    source 18
    target 614
  ]
  edge [
    source 18
    target 615
  ]
  edge [
    source 18
    target 616
  ]
  edge [
    source 18
    target 617
  ]
  edge [
    source 18
    target 618
  ]
  edge [
    source 18
    target 619
  ]
  edge [
    source 18
    target 620
  ]
  edge [
    source 18
    target 621
  ]
  edge [
    source 18
    target 622
  ]
  edge [
    source 18
    target 32
  ]
  edge [
    source 18
    target 623
  ]
  edge [
    source 18
    target 624
  ]
  edge [
    source 18
    target 625
  ]
  edge [
    source 18
    target 626
  ]
  edge [
    source 18
    target 627
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 628
  ]
  edge [
    source 18
    target 629
  ]
  edge [
    source 18
    target 630
  ]
  edge [
    source 18
    target 631
  ]
  edge [
    source 18
    target 632
  ]
  edge [
    source 18
    target 633
  ]
  edge [
    source 18
    target 634
  ]
  edge [
    source 18
    target 635
  ]
  edge [
    source 18
    target 636
  ]
  edge [
    source 18
    target 637
  ]
  edge [
    source 18
    target 46
  ]
  edge [
    source 18
    target 638
  ]
  edge [
    source 18
    target 639
  ]
  edge [
    source 18
    target 640
  ]
  edge [
    source 18
    target 641
  ]
  edge [
    source 18
    target 642
  ]
  edge [
    source 18
    target 643
  ]
]
