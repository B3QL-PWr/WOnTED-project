graph [
  node [
    id 0
    label "henrykowo"
    origin "text"
  ]
  node [
    id 1
    label "powiat"
    origin "text"
  ]
  node [
    id 2
    label "lidzbarski"
    origin "text"
  ]
  node [
    id 3
    label "wojew&#243;dztwo"
  ]
  node [
    id 4
    label "gmina"
  ]
  node [
    id 5
    label "jednostka_administracyjna"
  ]
  node [
    id 6
    label "urz&#261;d"
  ]
  node [
    id 7
    label "Karlsbad"
  ]
  node [
    id 8
    label "Dobro&#324;"
  ]
  node [
    id 9
    label "rada_gminy"
  ]
  node [
    id 10
    label "Wielka_Wie&#347;"
  ]
  node [
    id 11
    label "radny"
  ]
  node [
    id 12
    label "organizacja_religijna"
  ]
  node [
    id 13
    label "Biskupice"
  ]
  node [
    id 14
    label "mikroregion"
  ]
  node [
    id 15
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 16
    label "pa&#324;stwo"
  ]
  node [
    id 17
    label "makroregion"
  ]
  node [
    id 18
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 19
    label "warmi&#324;sko"
  ]
  node [
    id 20
    label "mazurski"
  ]
  node [
    id 21
    label "drogi"
  ]
  node [
    id 22
    label "wojew&#243;dzki"
  ]
  node [
    id 23
    label "nr"
  ]
  node [
    id 24
    label "507"
  ]
  node [
    id 25
    label "Henryka"
  ]
  node [
    id 26
    label "Laberyka"
  ]
  node [
    id 27
    label "kapitu&#322;a"
  ]
  node [
    id 28
    label "warmi&#324;ski"
  ]
  node [
    id 29
    label "wojna"
  ]
  node [
    id 30
    label "g&#322;odowy"
  ]
  node [
    id 31
    label "trzynastoletni"
  ]
  node [
    id 32
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 33
    label "PW"
  ]
  node [
    id 34
    label "&#347;wi&#281;ty"
  ]
  node [
    id 35
    label "Katarzyna"
  ]
  node [
    id 36
    label "Christian"
  ]
  node [
    id 37
    label "beniamin"
  ]
  node [
    id 38
    label "Szultza"
  ]
  node [
    id 39
    label "Andrzej"
  ]
  node [
    id 40
    label "von"
  ]
  node [
    id 41
    label "Hattena"
  ]
  node [
    id 42
    label "J"
  ]
  node [
    id 43
    label "Pipera"
  ]
  node [
    id 44
    label "Maria"
  ]
  node [
    id 45
    label "Magdalena"
  ]
  node [
    id 46
    label "J&#243;zefa"
  ]
  node [
    id 47
    label "koronacja"
  ]
  node [
    id 48
    label "zeszyt"
  ]
  node [
    id 49
    label "dzieci&#261;tko"
  ]
  node [
    id 50
    label "zdj&#281;cie"
  ]
  node [
    id 51
    label "krzy&#380;"
  ]
  node [
    id 52
    label "Antoni"
  ]
  node [
    id 53
    label "uzdrawia&#263;"
  ]
  node [
    id 54
    label "chory"
  ]
  node [
    id 55
    label "Piotr"
  ]
  node [
    id 56
    label "Kolberg"
  ]
  node [
    id 57
    label "Bernhard"
  ]
  node [
    id 58
    label "Poschmann"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 44
  ]
  edge [
    source 34
    target 45
  ]
  edge [
    source 34
    target 46
  ]
  edge [
    source 34
    target 47
  ]
  edge [
    source 34
    target 48
  ]
  edge [
    source 34
    target 49
  ]
  edge [
    source 34
    target 52
  ]
  edge [
    source 34
    target 53
  ]
  edge [
    source 34
    target 54
  ]
  edge [
    source 35
    target 47
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 51
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 57
    target 58
  ]
]
