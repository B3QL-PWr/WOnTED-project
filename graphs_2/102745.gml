graph [
  node [
    id 0
    label "zakres"
    origin "text"
  ]
  node [
    id 1
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 2
    label "gabinet"
    origin "text"
  ]
  node [
    id 3
    label "prezydent"
    origin "text"
  ]
  node [
    id 4
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 5
    label "szczeg&#243;lno&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "sfera"
  ]
  node [
    id 7
    label "granica"
  ]
  node [
    id 8
    label "zbi&#243;r"
  ]
  node [
    id 9
    label "wielko&#347;&#263;"
  ]
  node [
    id 10
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 11
    label "podzakres"
  ]
  node [
    id 12
    label "dziedzina"
  ]
  node [
    id 13
    label "desygnat"
  ]
  node [
    id 14
    label "circle"
  ]
  node [
    id 15
    label "rozmiar"
  ]
  node [
    id 16
    label "izochronizm"
  ]
  node [
    id 17
    label "zasi&#261;g"
  ]
  node [
    id 18
    label "bridge"
  ]
  node [
    id 19
    label "distribution"
  ]
  node [
    id 20
    label "warunek_lokalowy"
  ]
  node [
    id 21
    label "liczba"
  ]
  node [
    id 22
    label "cecha"
  ]
  node [
    id 23
    label "rzadko&#347;&#263;"
  ]
  node [
    id 24
    label "zaleta"
  ]
  node [
    id 25
    label "ilo&#347;&#263;"
  ]
  node [
    id 26
    label "measure"
  ]
  node [
    id 27
    label "znaczenie"
  ]
  node [
    id 28
    label "opinia"
  ]
  node [
    id 29
    label "dymensja"
  ]
  node [
    id 30
    label "poj&#281;cie"
  ]
  node [
    id 31
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 32
    label "zdolno&#347;&#263;"
  ]
  node [
    id 33
    label "potencja"
  ]
  node [
    id 34
    label "property"
  ]
  node [
    id 35
    label "egzemplarz"
  ]
  node [
    id 36
    label "series"
  ]
  node [
    id 37
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 38
    label "uprawianie"
  ]
  node [
    id 39
    label "praca_rolnicza"
  ]
  node [
    id 40
    label "collection"
  ]
  node [
    id 41
    label "dane"
  ]
  node [
    id 42
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 43
    label "pakiet_klimatyczny"
  ]
  node [
    id 44
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 45
    label "sum"
  ]
  node [
    id 46
    label "gathering"
  ]
  node [
    id 47
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 48
    label "album"
  ]
  node [
    id 49
    label "przej&#347;cie"
  ]
  node [
    id 50
    label "kres"
  ]
  node [
    id 51
    label "granica_pa&#324;stwa"
  ]
  node [
    id 52
    label "Ural"
  ]
  node [
    id 53
    label "miara"
  ]
  node [
    id 54
    label "end"
  ]
  node [
    id 55
    label "pu&#322;ap"
  ]
  node [
    id 56
    label "koniec"
  ]
  node [
    id 57
    label "granice"
  ]
  node [
    id 58
    label "frontier"
  ]
  node [
    id 59
    label "wymiar"
  ]
  node [
    id 60
    label "strefa"
  ]
  node [
    id 61
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 62
    label "kula"
  ]
  node [
    id 63
    label "class"
  ]
  node [
    id 64
    label "sector"
  ]
  node [
    id 65
    label "przestrze&#324;"
  ]
  node [
    id 66
    label "p&#243;&#322;kula"
  ]
  node [
    id 67
    label "huczek"
  ]
  node [
    id 68
    label "p&#243;&#322;sfera"
  ]
  node [
    id 69
    label "powierzchnia"
  ]
  node [
    id 70
    label "kolur"
  ]
  node [
    id 71
    label "grupa"
  ]
  node [
    id 72
    label "funkcja"
  ]
  node [
    id 73
    label "bezdro&#380;e"
  ]
  node [
    id 74
    label "poddzia&#322;"
  ]
  node [
    id 75
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 76
    label "odpowiednik"
  ]
  node [
    id 77
    label "designatum"
  ]
  node [
    id 78
    label "nazwa_rzetelna"
  ]
  node [
    id 79
    label "nazwa_pozorna"
  ]
  node [
    id 80
    label "denotacja"
  ]
  node [
    id 81
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 82
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 83
    label "strategia"
  ]
  node [
    id 84
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 85
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 86
    label "plan"
  ]
  node [
    id 87
    label "operacja"
  ]
  node [
    id 88
    label "metoda"
  ]
  node [
    id 89
    label "gra"
  ]
  node [
    id 90
    label "pocz&#261;tki"
  ]
  node [
    id 91
    label "wzorzec_projektowy"
  ]
  node [
    id 92
    label "program"
  ]
  node [
    id 93
    label "doktryna"
  ]
  node [
    id 94
    label "wrinkle"
  ]
  node [
    id 95
    label "dokument"
  ]
  node [
    id 96
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 97
    label "absolutorium"
  ]
  node [
    id 98
    label "dzia&#322;anie"
  ]
  node [
    id 99
    label "activity"
  ]
  node [
    id 100
    label "biurko"
  ]
  node [
    id 101
    label "boks"
  ]
  node [
    id 102
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 103
    label "szko&#322;a"
  ]
  node [
    id 104
    label "s&#261;d"
  ]
  node [
    id 105
    label "egzekutywa"
  ]
  node [
    id 106
    label "instytucja"
  ]
  node [
    id 107
    label "premier"
  ]
  node [
    id 108
    label "Londyn"
  ]
  node [
    id 109
    label "palestra"
  ]
  node [
    id 110
    label "pok&#243;j"
  ]
  node [
    id 111
    label "pracownia"
  ]
  node [
    id 112
    label "gabinet_cieni"
  ]
  node [
    id 113
    label "pomieszczenie"
  ]
  node [
    id 114
    label "Konsulat"
  ]
  node [
    id 115
    label "organ"
  ]
  node [
    id 116
    label "obrady"
  ]
  node [
    id 117
    label "executive"
  ]
  node [
    id 118
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 119
    label "partia"
  ]
  node [
    id 120
    label "federacja"
  ]
  node [
    id 121
    label "w&#322;adza"
  ]
  node [
    id 122
    label "osoba_prawna"
  ]
  node [
    id 123
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 124
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 125
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 126
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 127
    label "biuro"
  ]
  node [
    id 128
    label "organizacja"
  ]
  node [
    id 129
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 130
    label "Fundusze_Unijne"
  ]
  node [
    id 131
    label "zamyka&#263;"
  ]
  node [
    id 132
    label "establishment"
  ]
  node [
    id 133
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 134
    label "urz&#261;d"
  ]
  node [
    id 135
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 136
    label "afiliowa&#263;"
  ]
  node [
    id 137
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 138
    label "standard"
  ]
  node [
    id 139
    label "zamykanie"
  ]
  node [
    id 140
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 141
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 142
    label "warsztat"
  ]
  node [
    id 143
    label "mir"
  ]
  node [
    id 144
    label "uk&#322;ad"
  ]
  node [
    id 145
    label "pacyfista"
  ]
  node [
    id 146
    label "preliminarium_pokojowe"
  ]
  node [
    id 147
    label "spok&#243;j"
  ]
  node [
    id 148
    label "amfilada"
  ]
  node [
    id 149
    label "front"
  ]
  node [
    id 150
    label "apartment"
  ]
  node [
    id 151
    label "pod&#322;oga"
  ]
  node [
    id 152
    label "udost&#281;pnienie"
  ]
  node [
    id 153
    label "miejsce"
  ]
  node [
    id 154
    label "sklepienie"
  ]
  node [
    id 155
    label "sufit"
  ]
  node [
    id 156
    label "umieszczenie"
  ]
  node [
    id 157
    label "zakamarek"
  ]
  node [
    id 158
    label "rz&#261;d"
  ]
  node [
    id 159
    label "Bismarck"
  ]
  node [
    id 160
    label "Sto&#322;ypin"
  ]
  node [
    id 161
    label "zwierzchnik"
  ]
  node [
    id 162
    label "Miko&#322;ajczyk"
  ]
  node [
    id 163
    label "Chruszczow"
  ]
  node [
    id 164
    label "Jelcyn"
  ]
  node [
    id 165
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 166
    label "dostojnik"
  ]
  node [
    id 167
    label "blat"
  ]
  node [
    id 168
    label "szuflada"
  ]
  node [
    id 169
    label "mebel"
  ]
  node [
    id 170
    label "hak"
  ]
  node [
    id 171
    label "sk&#243;ra"
  ]
  node [
    id 172
    label "sekundant"
  ]
  node [
    id 173
    label "cholewka"
  ]
  node [
    id 174
    label "sport_walki"
  ]
  node [
    id 175
    label "stajnia"
  ]
  node [
    id 176
    label "Wimbledon"
  ]
  node [
    id 177
    label "Westminster"
  ]
  node [
    id 178
    label "Londek"
  ]
  node [
    id 179
    label "do&#347;wiadczenie"
  ]
  node [
    id 180
    label "teren_szko&#322;y"
  ]
  node [
    id 181
    label "wiedza"
  ]
  node [
    id 182
    label "Mickiewicz"
  ]
  node [
    id 183
    label "kwalifikacje"
  ]
  node [
    id 184
    label "podr&#281;cznik"
  ]
  node [
    id 185
    label "absolwent"
  ]
  node [
    id 186
    label "praktyka"
  ]
  node [
    id 187
    label "school"
  ]
  node [
    id 188
    label "system"
  ]
  node [
    id 189
    label "zda&#263;"
  ]
  node [
    id 190
    label "urszulanki"
  ]
  node [
    id 191
    label "sztuba"
  ]
  node [
    id 192
    label "&#322;awa_szkolna"
  ]
  node [
    id 193
    label "nauka"
  ]
  node [
    id 194
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 195
    label "przepisa&#263;"
  ]
  node [
    id 196
    label "muzyka"
  ]
  node [
    id 197
    label "form"
  ]
  node [
    id 198
    label "klasa"
  ]
  node [
    id 199
    label "lekcja"
  ]
  node [
    id 200
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 201
    label "przepisanie"
  ]
  node [
    id 202
    label "czas"
  ]
  node [
    id 203
    label "skolaryzacja"
  ]
  node [
    id 204
    label "zdanie"
  ]
  node [
    id 205
    label "stopek"
  ]
  node [
    id 206
    label "sekretariat"
  ]
  node [
    id 207
    label "ideologia"
  ]
  node [
    id 208
    label "lesson"
  ]
  node [
    id 209
    label "niepokalanki"
  ]
  node [
    id 210
    label "siedziba"
  ]
  node [
    id 211
    label "szkolenie"
  ]
  node [
    id 212
    label "kara"
  ]
  node [
    id 213
    label "tablica"
  ]
  node [
    id 214
    label "zesp&#243;&#322;"
  ]
  node [
    id 215
    label "podejrzany"
  ]
  node [
    id 216
    label "s&#261;downictwo"
  ]
  node [
    id 217
    label "wytw&#243;r"
  ]
  node [
    id 218
    label "court"
  ]
  node [
    id 219
    label "forum"
  ]
  node [
    id 220
    label "bronienie"
  ]
  node [
    id 221
    label "wydarzenie"
  ]
  node [
    id 222
    label "oskar&#380;yciel"
  ]
  node [
    id 223
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 224
    label "skazany"
  ]
  node [
    id 225
    label "post&#281;powanie"
  ]
  node [
    id 226
    label "broni&#263;"
  ]
  node [
    id 227
    label "my&#347;l"
  ]
  node [
    id 228
    label "pods&#261;dny"
  ]
  node [
    id 229
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 230
    label "obrona"
  ]
  node [
    id 231
    label "wypowied&#378;"
  ]
  node [
    id 232
    label "antylogizm"
  ]
  node [
    id 233
    label "konektyw"
  ]
  node [
    id 234
    label "&#347;wiadek"
  ]
  node [
    id 235
    label "procesowicz"
  ]
  node [
    id 236
    label "strona"
  ]
  node [
    id 237
    label "Grecja"
  ]
  node [
    id 238
    label "prawnicy"
  ]
  node [
    id 239
    label "regent"
  ]
  node [
    id 240
    label "legal_profession"
  ]
  node [
    id 241
    label "budynek"
  ]
  node [
    id 242
    label "chancellery"
  ]
  node [
    id 243
    label "gruba_ryba"
  ]
  node [
    id 244
    label "Gorbaczow"
  ]
  node [
    id 245
    label "Putin"
  ]
  node [
    id 246
    label "Tito"
  ]
  node [
    id 247
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 248
    label "Naser"
  ]
  node [
    id 249
    label "de_Gaulle"
  ]
  node [
    id 250
    label "Nixon"
  ]
  node [
    id 251
    label "Kemal"
  ]
  node [
    id 252
    label "Clinton"
  ]
  node [
    id 253
    label "Bierut"
  ]
  node [
    id 254
    label "Roosevelt"
  ]
  node [
    id 255
    label "samorz&#261;dowiec"
  ]
  node [
    id 256
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 257
    label "pryncypa&#322;"
  ]
  node [
    id 258
    label "kierowa&#263;"
  ]
  node [
    id 259
    label "kierownictwo"
  ]
  node [
    id 260
    label "cz&#322;owiek"
  ]
  node [
    id 261
    label "urz&#281;dnik"
  ]
  node [
    id 262
    label "notabl"
  ]
  node [
    id 263
    label "oficja&#322;"
  ]
  node [
    id 264
    label "samorz&#261;d"
  ]
  node [
    id 265
    label "polityk"
  ]
  node [
    id 266
    label "komuna"
  ]
  node [
    id 267
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 268
    label "para"
  ]
  node [
    id 269
    label "necessity"
  ]
  node [
    id 270
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 271
    label "trza"
  ]
  node [
    id 272
    label "uczestniczy&#263;"
  ]
  node [
    id 273
    label "participate"
  ]
  node [
    id 274
    label "robi&#263;"
  ]
  node [
    id 275
    label "by&#263;"
  ]
  node [
    id 276
    label "trzeba"
  ]
  node [
    id 277
    label "pair"
  ]
  node [
    id 278
    label "odparowywanie"
  ]
  node [
    id 279
    label "gaz_cieplarniany"
  ]
  node [
    id 280
    label "chodzi&#263;"
  ]
  node [
    id 281
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 282
    label "poker"
  ]
  node [
    id 283
    label "moneta"
  ]
  node [
    id 284
    label "parowanie"
  ]
  node [
    id 285
    label "damp"
  ]
  node [
    id 286
    label "sztuka"
  ]
  node [
    id 287
    label "odparowanie"
  ]
  node [
    id 288
    label "odparowa&#263;"
  ]
  node [
    id 289
    label "dodatek"
  ]
  node [
    id 290
    label "jednostka_monetarna"
  ]
  node [
    id 291
    label "smoke"
  ]
  node [
    id 292
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 293
    label "odparowywa&#263;"
  ]
  node [
    id 294
    label "Albania"
  ]
  node [
    id 295
    label "gaz"
  ]
  node [
    id 296
    label "wyparowanie"
  ]
  node [
    id 297
    label "wyj&#261;tkowo&#347;&#263;"
  ]
  node [
    id 298
    label "inno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
]
