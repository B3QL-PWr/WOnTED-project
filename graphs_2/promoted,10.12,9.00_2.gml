graph [
  node [
    id 0
    label "patostreamer"
    origin "text"
  ]
  node [
    id 1
    label "magical'"
    origin "text"
  ]
  node [
    id 2
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 4
    label "polski"
    origin "text"
  ]
  node [
    id 5
    label "policja"
    origin "text"
  ]
  node [
    id 6
    label "zatrzyma&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pijany"
    origin "text"
  ]
  node [
    id 8
    label "kierowca"
    origin "text"
  ]
  node [
    id 9
    label "ranek"
  ]
  node [
    id 10
    label "doba"
  ]
  node [
    id 11
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 12
    label "noc"
  ]
  node [
    id 13
    label "podwiecz&#243;r"
  ]
  node [
    id 14
    label "po&#322;udnie"
  ]
  node [
    id 15
    label "godzina"
  ]
  node [
    id 16
    label "przedpo&#322;udnie"
  ]
  node [
    id 17
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 18
    label "long_time"
  ]
  node [
    id 19
    label "wiecz&#243;r"
  ]
  node [
    id 20
    label "t&#322;usty_czwartek"
  ]
  node [
    id 21
    label "popo&#322;udnie"
  ]
  node [
    id 22
    label "walentynki"
  ]
  node [
    id 23
    label "czynienie_si&#281;"
  ]
  node [
    id 24
    label "s&#322;o&#324;ce"
  ]
  node [
    id 25
    label "rano"
  ]
  node [
    id 26
    label "tydzie&#324;"
  ]
  node [
    id 27
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 28
    label "wzej&#347;cie"
  ]
  node [
    id 29
    label "czas"
  ]
  node [
    id 30
    label "wsta&#263;"
  ]
  node [
    id 31
    label "day"
  ]
  node [
    id 32
    label "termin"
  ]
  node [
    id 33
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 34
    label "wstanie"
  ]
  node [
    id 35
    label "przedwiecz&#243;r"
  ]
  node [
    id 36
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 37
    label "Sylwester"
  ]
  node [
    id 38
    label "poprzedzanie"
  ]
  node [
    id 39
    label "czasoprzestrze&#324;"
  ]
  node [
    id 40
    label "laba"
  ]
  node [
    id 41
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 42
    label "chronometria"
  ]
  node [
    id 43
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 44
    label "rachuba_czasu"
  ]
  node [
    id 45
    label "przep&#322;ywanie"
  ]
  node [
    id 46
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 47
    label "czasokres"
  ]
  node [
    id 48
    label "odczyt"
  ]
  node [
    id 49
    label "chwila"
  ]
  node [
    id 50
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 51
    label "dzieje"
  ]
  node [
    id 52
    label "kategoria_gramatyczna"
  ]
  node [
    id 53
    label "poprzedzenie"
  ]
  node [
    id 54
    label "trawienie"
  ]
  node [
    id 55
    label "pochodzi&#263;"
  ]
  node [
    id 56
    label "period"
  ]
  node [
    id 57
    label "okres_czasu"
  ]
  node [
    id 58
    label "poprzedza&#263;"
  ]
  node [
    id 59
    label "schy&#322;ek"
  ]
  node [
    id 60
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 61
    label "odwlekanie_si&#281;"
  ]
  node [
    id 62
    label "zegar"
  ]
  node [
    id 63
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 64
    label "czwarty_wymiar"
  ]
  node [
    id 65
    label "pochodzenie"
  ]
  node [
    id 66
    label "koniugacja"
  ]
  node [
    id 67
    label "Zeitgeist"
  ]
  node [
    id 68
    label "trawi&#263;"
  ]
  node [
    id 69
    label "pogoda"
  ]
  node [
    id 70
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 71
    label "poprzedzi&#263;"
  ]
  node [
    id 72
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 73
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 74
    label "time_period"
  ]
  node [
    id 75
    label "nazewnictwo"
  ]
  node [
    id 76
    label "term"
  ]
  node [
    id 77
    label "przypadni&#281;cie"
  ]
  node [
    id 78
    label "ekspiracja"
  ]
  node [
    id 79
    label "przypa&#347;&#263;"
  ]
  node [
    id 80
    label "chronogram"
  ]
  node [
    id 81
    label "praktyka"
  ]
  node [
    id 82
    label "nazwa"
  ]
  node [
    id 83
    label "przyj&#281;cie"
  ]
  node [
    id 84
    label "spotkanie"
  ]
  node [
    id 85
    label "night"
  ]
  node [
    id 86
    label "zach&#243;d"
  ]
  node [
    id 87
    label "vesper"
  ]
  node [
    id 88
    label "pora"
  ]
  node [
    id 89
    label "odwieczerz"
  ]
  node [
    id 90
    label "blady_&#347;wit"
  ]
  node [
    id 91
    label "podkurek"
  ]
  node [
    id 92
    label "aurora"
  ]
  node [
    id 93
    label "wsch&#243;d"
  ]
  node [
    id 94
    label "zjawisko"
  ]
  node [
    id 95
    label "&#347;rodek"
  ]
  node [
    id 96
    label "obszar"
  ]
  node [
    id 97
    label "Ziemia"
  ]
  node [
    id 98
    label "dwunasta"
  ]
  node [
    id 99
    label "strona_&#347;wiata"
  ]
  node [
    id 100
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 101
    label "dopo&#322;udnie"
  ]
  node [
    id 102
    label "p&#243;&#322;noc"
  ]
  node [
    id 103
    label "nokturn"
  ]
  node [
    id 104
    label "time"
  ]
  node [
    id 105
    label "p&#243;&#322;godzina"
  ]
  node [
    id 106
    label "jednostka_czasu"
  ]
  node [
    id 107
    label "minuta"
  ]
  node [
    id 108
    label "kwadrans"
  ]
  node [
    id 109
    label "jednostka_geologiczna"
  ]
  node [
    id 110
    label "weekend"
  ]
  node [
    id 111
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 112
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 113
    label "miesi&#261;c"
  ]
  node [
    id 114
    label "S&#322;o&#324;ce"
  ]
  node [
    id 115
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 116
    label "&#347;wiat&#322;o"
  ]
  node [
    id 117
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 118
    label "kochanie"
  ]
  node [
    id 119
    label "sunlight"
  ]
  node [
    id 120
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 121
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 122
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 123
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 124
    label "mount"
  ]
  node [
    id 125
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 126
    label "wzej&#347;&#263;"
  ]
  node [
    id 127
    label "ascend"
  ]
  node [
    id 128
    label "kuca&#263;"
  ]
  node [
    id 129
    label "wyzdrowie&#263;"
  ]
  node [
    id 130
    label "opu&#347;ci&#263;"
  ]
  node [
    id 131
    label "rise"
  ]
  node [
    id 132
    label "arise"
  ]
  node [
    id 133
    label "stan&#261;&#263;"
  ]
  node [
    id 134
    label "przesta&#263;"
  ]
  node [
    id 135
    label "wyzdrowienie"
  ]
  node [
    id 136
    label "le&#380;enie"
  ]
  node [
    id 137
    label "kl&#281;czenie"
  ]
  node [
    id 138
    label "opuszczenie"
  ]
  node [
    id 139
    label "uniesienie_si&#281;"
  ]
  node [
    id 140
    label "siedzenie"
  ]
  node [
    id 141
    label "beginning"
  ]
  node [
    id 142
    label "przestanie"
  ]
  node [
    id 143
    label "grudzie&#324;"
  ]
  node [
    id 144
    label "luty"
  ]
  node [
    id 145
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 146
    label "aid"
  ]
  node [
    id 147
    label "concur"
  ]
  node [
    id 148
    label "help"
  ]
  node [
    id 149
    label "u&#322;atwi&#263;"
  ]
  node [
    id 150
    label "zrobi&#263;"
  ]
  node [
    id 151
    label "zaskutkowa&#263;"
  ]
  node [
    id 152
    label "post&#261;pi&#263;"
  ]
  node [
    id 153
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 154
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 155
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 156
    label "zorganizowa&#263;"
  ]
  node [
    id 157
    label "appoint"
  ]
  node [
    id 158
    label "wystylizowa&#263;"
  ]
  node [
    id 159
    label "cause"
  ]
  node [
    id 160
    label "przerobi&#263;"
  ]
  node [
    id 161
    label "nabra&#263;"
  ]
  node [
    id 162
    label "make"
  ]
  node [
    id 163
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 164
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 165
    label "wydali&#263;"
  ]
  node [
    id 166
    label "sprawdzi&#263;_si&#281;"
  ]
  node [
    id 167
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 168
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 169
    label "przynie&#347;&#263;"
  ]
  node [
    id 170
    label "przedmiot"
  ]
  node [
    id 171
    label "Polish"
  ]
  node [
    id 172
    label "goniony"
  ]
  node [
    id 173
    label "oberek"
  ]
  node [
    id 174
    label "ryba_po_grecku"
  ]
  node [
    id 175
    label "sztajer"
  ]
  node [
    id 176
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 177
    label "krakowiak"
  ]
  node [
    id 178
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 179
    label "pierogi_ruskie"
  ]
  node [
    id 180
    label "lacki"
  ]
  node [
    id 181
    label "polak"
  ]
  node [
    id 182
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 183
    label "chodzony"
  ]
  node [
    id 184
    label "po_polsku"
  ]
  node [
    id 185
    label "mazur"
  ]
  node [
    id 186
    label "polsko"
  ]
  node [
    id 187
    label "skoczny"
  ]
  node [
    id 188
    label "drabant"
  ]
  node [
    id 189
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 190
    label "j&#281;zyk"
  ]
  node [
    id 191
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 192
    label "artykulator"
  ]
  node [
    id 193
    label "kod"
  ]
  node [
    id 194
    label "kawa&#322;ek"
  ]
  node [
    id 195
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 196
    label "gramatyka"
  ]
  node [
    id 197
    label "stylik"
  ]
  node [
    id 198
    label "przet&#322;umaczenie"
  ]
  node [
    id 199
    label "formalizowanie"
  ]
  node [
    id 200
    label "ssanie"
  ]
  node [
    id 201
    label "ssa&#263;"
  ]
  node [
    id 202
    label "language"
  ]
  node [
    id 203
    label "liza&#263;"
  ]
  node [
    id 204
    label "napisa&#263;"
  ]
  node [
    id 205
    label "konsonantyzm"
  ]
  node [
    id 206
    label "wokalizm"
  ]
  node [
    id 207
    label "pisa&#263;"
  ]
  node [
    id 208
    label "fonetyka"
  ]
  node [
    id 209
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 210
    label "jeniec"
  ]
  node [
    id 211
    label "but"
  ]
  node [
    id 212
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 213
    label "po_koroniarsku"
  ]
  node [
    id 214
    label "kultura_duchowa"
  ]
  node [
    id 215
    label "t&#322;umaczenie"
  ]
  node [
    id 216
    label "m&#243;wienie"
  ]
  node [
    id 217
    label "pype&#263;"
  ]
  node [
    id 218
    label "lizanie"
  ]
  node [
    id 219
    label "pismo"
  ]
  node [
    id 220
    label "formalizowa&#263;"
  ]
  node [
    id 221
    label "rozumie&#263;"
  ]
  node [
    id 222
    label "organ"
  ]
  node [
    id 223
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 224
    label "rozumienie"
  ]
  node [
    id 225
    label "spos&#243;b"
  ]
  node [
    id 226
    label "makroglosja"
  ]
  node [
    id 227
    label "m&#243;wi&#263;"
  ]
  node [
    id 228
    label "jama_ustna"
  ]
  node [
    id 229
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 230
    label "formacja_geologiczna"
  ]
  node [
    id 231
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 232
    label "natural_language"
  ]
  node [
    id 233
    label "s&#322;ownictwo"
  ]
  node [
    id 234
    label "urz&#261;dzenie"
  ]
  node [
    id 235
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 236
    label "wschodnioeuropejski"
  ]
  node [
    id 237
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 238
    label "poga&#324;ski"
  ]
  node [
    id 239
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 240
    label "topielec"
  ]
  node [
    id 241
    label "europejski"
  ]
  node [
    id 242
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 243
    label "langosz"
  ]
  node [
    id 244
    label "zboczenie"
  ]
  node [
    id 245
    label "om&#243;wienie"
  ]
  node [
    id 246
    label "sponiewieranie"
  ]
  node [
    id 247
    label "discipline"
  ]
  node [
    id 248
    label "rzecz"
  ]
  node [
    id 249
    label "omawia&#263;"
  ]
  node [
    id 250
    label "kr&#261;&#380;enie"
  ]
  node [
    id 251
    label "tre&#347;&#263;"
  ]
  node [
    id 252
    label "robienie"
  ]
  node [
    id 253
    label "sponiewiera&#263;"
  ]
  node [
    id 254
    label "element"
  ]
  node [
    id 255
    label "entity"
  ]
  node [
    id 256
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 257
    label "tematyka"
  ]
  node [
    id 258
    label "w&#261;tek"
  ]
  node [
    id 259
    label "charakter"
  ]
  node [
    id 260
    label "zbaczanie"
  ]
  node [
    id 261
    label "program_nauczania"
  ]
  node [
    id 262
    label "om&#243;wi&#263;"
  ]
  node [
    id 263
    label "omawianie"
  ]
  node [
    id 264
    label "thing"
  ]
  node [
    id 265
    label "kultura"
  ]
  node [
    id 266
    label "istota"
  ]
  node [
    id 267
    label "zbacza&#263;"
  ]
  node [
    id 268
    label "zboczy&#263;"
  ]
  node [
    id 269
    label "gwardzista"
  ]
  node [
    id 270
    label "melodia"
  ]
  node [
    id 271
    label "taniec"
  ]
  node [
    id 272
    label "taniec_ludowy"
  ]
  node [
    id 273
    label "&#347;redniowieczny"
  ]
  node [
    id 274
    label "specjalny"
  ]
  node [
    id 275
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 276
    label "weso&#322;y"
  ]
  node [
    id 277
    label "sprawny"
  ]
  node [
    id 278
    label "rytmiczny"
  ]
  node [
    id 279
    label "skocznie"
  ]
  node [
    id 280
    label "energiczny"
  ]
  node [
    id 281
    label "lendler"
  ]
  node [
    id 282
    label "austriacki"
  ]
  node [
    id 283
    label "polka"
  ]
  node [
    id 284
    label "europejsko"
  ]
  node [
    id 285
    label "przytup"
  ]
  node [
    id 286
    label "ho&#322;ubiec"
  ]
  node [
    id 287
    label "wodzi&#263;"
  ]
  node [
    id 288
    label "ludowy"
  ]
  node [
    id 289
    label "pie&#347;&#324;"
  ]
  node [
    id 290
    label "mieszkaniec"
  ]
  node [
    id 291
    label "centu&#347;"
  ]
  node [
    id 292
    label "lalka"
  ]
  node [
    id 293
    label "Ma&#322;opolanin"
  ]
  node [
    id 294
    label "krakauer"
  ]
  node [
    id 295
    label "grupa"
  ]
  node [
    id 296
    label "komisariat"
  ]
  node [
    id 297
    label "s&#322;u&#380;ba"
  ]
  node [
    id 298
    label "posterunek"
  ]
  node [
    id 299
    label "psiarnia"
  ]
  node [
    id 300
    label "awansowa&#263;"
  ]
  node [
    id 301
    label "stawia&#263;"
  ]
  node [
    id 302
    label "wakowa&#263;"
  ]
  node [
    id 303
    label "powierzanie"
  ]
  node [
    id 304
    label "postawi&#263;"
  ]
  node [
    id 305
    label "pozycja"
  ]
  node [
    id 306
    label "agencja"
  ]
  node [
    id 307
    label "awansowanie"
  ]
  node [
    id 308
    label "warta"
  ]
  node [
    id 309
    label "praca"
  ]
  node [
    id 310
    label "tkanka"
  ]
  node [
    id 311
    label "jednostka_organizacyjna"
  ]
  node [
    id 312
    label "budowa"
  ]
  node [
    id 313
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 314
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 315
    label "tw&#243;r"
  ]
  node [
    id 316
    label "organogeneza"
  ]
  node [
    id 317
    label "zesp&#243;&#322;"
  ]
  node [
    id 318
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 319
    label "struktura_anatomiczna"
  ]
  node [
    id 320
    label "uk&#322;ad"
  ]
  node [
    id 321
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 322
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 323
    label "Izba_Konsyliarska"
  ]
  node [
    id 324
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 325
    label "stomia"
  ]
  node [
    id 326
    label "dekortykacja"
  ]
  node [
    id 327
    label "okolica"
  ]
  node [
    id 328
    label "Komitet_Region&#243;w"
  ]
  node [
    id 329
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 330
    label "instytucja"
  ]
  node [
    id 331
    label "wys&#322;uga"
  ]
  node [
    id 332
    label "service"
  ]
  node [
    id 333
    label "czworak"
  ]
  node [
    id 334
    label "ZOMO"
  ]
  node [
    id 335
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 336
    label "odm&#322;adzanie"
  ]
  node [
    id 337
    label "liga"
  ]
  node [
    id 338
    label "jednostka_systematyczna"
  ]
  node [
    id 339
    label "asymilowanie"
  ]
  node [
    id 340
    label "gromada"
  ]
  node [
    id 341
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 342
    label "asymilowa&#263;"
  ]
  node [
    id 343
    label "egzemplarz"
  ]
  node [
    id 344
    label "Entuzjastki"
  ]
  node [
    id 345
    label "zbi&#243;r"
  ]
  node [
    id 346
    label "kompozycja"
  ]
  node [
    id 347
    label "Terranie"
  ]
  node [
    id 348
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 349
    label "category"
  ]
  node [
    id 350
    label "pakiet_klimatyczny"
  ]
  node [
    id 351
    label "oddzia&#322;"
  ]
  node [
    id 352
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 353
    label "cz&#261;steczka"
  ]
  node [
    id 354
    label "stage_set"
  ]
  node [
    id 355
    label "type"
  ]
  node [
    id 356
    label "specgrupa"
  ]
  node [
    id 357
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 358
    label "&#346;wietliki"
  ]
  node [
    id 359
    label "odm&#322;odzenie"
  ]
  node [
    id 360
    label "Eurogrupa"
  ]
  node [
    id 361
    label "odm&#322;adza&#263;"
  ]
  node [
    id 362
    label "harcerze_starsi"
  ]
  node [
    id 363
    label "urz&#261;d"
  ]
  node [
    id 364
    label "jednostka"
  ]
  node [
    id 365
    label "czasowy"
  ]
  node [
    id 366
    label "commissariat"
  ]
  node [
    id 367
    label "rewir"
  ]
  node [
    id 368
    label "komornik"
  ]
  node [
    id 369
    label "suspend"
  ]
  node [
    id 370
    label "zaczepi&#263;"
  ]
  node [
    id 371
    label "bury"
  ]
  node [
    id 372
    label "bankrupt"
  ]
  node [
    id 373
    label "zabra&#263;"
  ]
  node [
    id 374
    label "continue"
  ]
  node [
    id 375
    label "give"
  ]
  node [
    id 376
    label "spowodowa&#263;"
  ]
  node [
    id 377
    label "zamkn&#261;&#263;"
  ]
  node [
    id 378
    label "przechowa&#263;"
  ]
  node [
    id 379
    label "zaaresztowa&#263;"
  ]
  node [
    id 380
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 381
    label "przerwa&#263;"
  ]
  node [
    id 382
    label "unieruchomi&#263;"
  ]
  node [
    id 383
    label "anticipate"
  ]
  node [
    id 384
    label "work"
  ]
  node [
    id 385
    label "chemia"
  ]
  node [
    id 386
    label "reakcja_chemiczna"
  ]
  node [
    id 387
    label "act"
  ]
  node [
    id 388
    label "withdraw"
  ]
  node [
    id 389
    label "doprowadzi&#263;"
  ]
  node [
    id 390
    label "z&#322;apa&#263;"
  ]
  node [
    id 391
    label "wzi&#261;&#263;"
  ]
  node [
    id 392
    label "zaj&#261;&#263;"
  ]
  node [
    id 393
    label "consume"
  ]
  node [
    id 394
    label "deprive"
  ]
  node [
    id 395
    label "przenie&#347;&#263;"
  ]
  node [
    id 396
    label "abstract"
  ]
  node [
    id 397
    label "uda&#263;_si&#281;"
  ]
  node [
    id 398
    label "przesun&#261;&#263;"
  ]
  node [
    id 399
    label "zgarn&#261;&#263;"
  ]
  node [
    id 400
    label "close"
  ]
  node [
    id 401
    label "przeszkodzi&#263;"
  ]
  node [
    id 402
    label "calve"
  ]
  node [
    id 403
    label "wstrzyma&#263;"
  ]
  node [
    id 404
    label "rozerwa&#263;"
  ]
  node [
    id 405
    label "przedziurawi&#263;"
  ]
  node [
    id 406
    label "urwa&#263;"
  ]
  node [
    id 407
    label "przerzedzi&#263;"
  ]
  node [
    id 408
    label "przerywa&#263;"
  ]
  node [
    id 409
    label "przerwanie"
  ]
  node [
    id 410
    label "kultywar"
  ]
  node [
    id 411
    label "break"
  ]
  node [
    id 412
    label "przerywanie"
  ]
  node [
    id 413
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 414
    label "forbid"
  ]
  node [
    id 415
    label "throng"
  ]
  node [
    id 416
    label "lock"
  ]
  node [
    id 417
    label "pami&#281;&#263;"
  ]
  node [
    id 418
    label "uchroni&#263;"
  ]
  node [
    id 419
    label "ukry&#263;"
  ]
  node [
    id 420
    label "preserve"
  ]
  node [
    id 421
    label "zachowa&#263;"
  ]
  node [
    id 422
    label "podtrzyma&#263;"
  ]
  node [
    id 423
    label "cover"
  ]
  node [
    id 424
    label "brunatny"
  ]
  node [
    id 425
    label "przygn&#281;biaj&#261;cy"
  ]
  node [
    id 426
    label "ciemnoszary"
  ]
  node [
    id 427
    label "brudnoszary"
  ]
  node [
    id 428
    label "buro"
  ]
  node [
    id 429
    label "urz&#281;dnik"
  ]
  node [
    id 430
    label "podkomorzy"
  ]
  node [
    id 431
    label "ch&#322;op"
  ]
  node [
    id 432
    label "urz&#281;dnik_s&#261;dowy"
  ]
  node [
    id 433
    label "zajmowa&#263;"
  ]
  node [
    id 434
    label "bezrolny"
  ]
  node [
    id 435
    label "lokator"
  ]
  node [
    id 436
    label "sekutnik"
  ]
  node [
    id 437
    label "zagadn&#261;&#263;"
  ]
  node [
    id 438
    label "odwiedzi&#263;"
  ]
  node [
    id 439
    label "napa&#347;&#263;"
  ]
  node [
    id 440
    label "absorb"
  ]
  node [
    id 441
    label "limp"
  ]
  node [
    id 442
    label "zaatakowa&#263;"
  ]
  node [
    id 443
    label "wpa&#347;&#263;"
  ]
  node [
    id 444
    label "prosecute"
  ]
  node [
    id 445
    label "przymocowa&#263;"
  ]
  node [
    id 446
    label "engage"
  ]
  node [
    id 447
    label "zako&#324;czy&#263;"
  ]
  node [
    id 448
    label "put"
  ]
  node [
    id 449
    label "insert"
  ]
  node [
    id 450
    label "zawrze&#263;"
  ]
  node [
    id 451
    label "zablokowa&#263;"
  ]
  node [
    id 452
    label "sko&#324;czy&#263;"
  ]
  node [
    id 453
    label "uj&#261;&#263;"
  ]
  node [
    id 454
    label "rozwi&#261;za&#263;"
  ]
  node [
    id 455
    label "kill"
  ]
  node [
    id 456
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 457
    label "umie&#347;ci&#263;"
  ]
  node [
    id 458
    label "upijanie_si&#281;"
  ]
  node [
    id 459
    label "szalony"
  ]
  node [
    id 460
    label "nieprzytomny"
  ]
  node [
    id 461
    label "cz&#322;owiek"
  ]
  node [
    id 462
    label "d&#281;tka"
  ]
  node [
    id 463
    label "pij&#261;cy"
  ]
  node [
    id 464
    label "napi&#322;y"
  ]
  node [
    id 465
    label "upicie_si&#281;"
  ]
  node [
    id 466
    label "s&#322;abeusz"
  ]
  node [
    id 467
    label "kicha"
  ]
  node [
    id 468
    label "nieudany"
  ]
  node [
    id 469
    label "pi&#322;ka"
  ]
  node [
    id 470
    label "ogumienie"
  ]
  node [
    id 471
    label "wentyl"
  ]
  node [
    id 472
    label "zm&#281;czony"
  ]
  node [
    id 473
    label "mato&#322;"
  ]
  node [
    id 474
    label "baloney"
  ]
  node [
    id 475
    label "sytuacja"
  ]
  node [
    id 476
    label "&#322;amaga"
  ]
  node [
    id 477
    label "szybki"
  ]
  node [
    id 478
    label "podupcony"
  ]
  node [
    id 479
    label "nietuzinkowy"
  ]
  node [
    id 480
    label "niezr&#243;wnowa&#380;ony"
  ]
  node [
    id 481
    label "niepokoj&#261;cy"
  ]
  node [
    id 482
    label "stukni&#281;ty"
  ]
  node [
    id 483
    label "nieprzewidywalny"
  ]
  node [
    id 484
    label "nienormalny"
  ]
  node [
    id 485
    label "zwariowanie"
  ]
  node [
    id 486
    label "oryginalny"
  ]
  node [
    id 487
    label "jebni&#281;ty"
  ]
  node [
    id 488
    label "dziwny"
  ]
  node [
    id 489
    label "szale&#324;czo"
  ]
  node [
    id 490
    label "g&#261;bczasta_encefalopatia_byd&#322;a"
  ]
  node [
    id 491
    label "chory"
  ]
  node [
    id 492
    label "wielki"
  ]
  node [
    id 493
    label "szalenie"
  ]
  node [
    id 494
    label "nierozs&#261;dny"
  ]
  node [
    id 495
    label "konsument"
  ]
  node [
    id 496
    label "trunkowy"
  ]
  node [
    id 497
    label "ludzko&#347;&#263;"
  ]
  node [
    id 498
    label "wapniak"
  ]
  node [
    id 499
    label "os&#322;abia&#263;"
  ]
  node [
    id 500
    label "posta&#263;"
  ]
  node [
    id 501
    label "hominid"
  ]
  node [
    id 502
    label "podw&#322;adny"
  ]
  node [
    id 503
    label "os&#322;abianie"
  ]
  node [
    id 504
    label "g&#322;owa"
  ]
  node [
    id 505
    label "figura"
  ]
  node [
    id 506
    label "portrecista"
  ]
  node [
    id 507
    label "dwun&#243;g"
  ]
  node [
    id 508
    label "profanum"
  ]
  node [
    id 509
    label "mikrokosmos"
  ]
  node [
    id 510
    label "nasada"
  ]
  node [
    id 511
    label "duch"
  ]
  node [
    id 512
    label "antropochoria"
  ]
  node [
    id 513
    label "osoba"
  ]
  node [
    id 514
    label "wz&#243;r"
  ]
  node [
    id 515
    label "senior"
  ]
  node [
    id 516
    label "oddzia&#322;ywanie"
  ]
  node [
    id 517
    label "Adam"
  ]
  node [
    id 518
    label "homo_sapiens"
  ]
  node [
    id 519
    label "polifag"
  ]
  node [
    id 520
    label "niesw&#243;j"
  ]
  node [
    id 521
    label "kosmiczny"
  ]
  node [
    id 522
    label "nieprzytomnie"
  ]
  node [
    id 523
    label "transportowiec"
  ]
  node [
    id 524
    label "pracownik"
  ]
  node [
    id 525
    label "statek_handlowy"
  ]
  node [
    id 526
    label "okr&#281;t_nawodny"
  ]
  node [
    id 527
    label "bran&#380;owiec"
  ]
  node [
    id 528
    label "Daniel"
  ]
  node [
    id 529
    label "Magical"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 528
    target 529
  ]
]
