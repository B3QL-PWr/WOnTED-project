graph [
  node [
    id 0
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 1
    label "guang"
    origin "text"
  ]
  node [
    id 2
    label "ujawni&#263;"
    origin "text"
  ]
  node [
    id 3
    label "chiny"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 6
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "narkomania"
    origin "text"
  ]
  node [
    id 9
    label "pacjent"
    origin "text"
  ]
  node [
    id 10
    label "hiv"
    origin "text"
  ]
  node [
    id 11
    label "problem"
    origin "text"
  ]
  node [
    id 12
    label "&#347;rodowiskowy"
    origin "text"
  ]
  node [
    id 13
    label "discover"
  ]
  node [
    id 14
    label "objawi&#263;"
  ]
  node [
    id 15
    label "poinformowa&#263;"
  ]
  node [
    id 16
    label "dostrzec"
  ]
  node [
    id 17
    label "denounce"
  ]
  node [
    id 18
    label "inform"
  ]
  node [
    id 19
    label "zakomunikowa&#263;"
  ]
  node [
    id 20
    label "cognizance"
  ]
  node [
    id 21
    label "zobaczy&#263;"
  ]
  node [
    id 22
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 23
    label "zorientowa&#263;_si&#281;"
  ]
  node [
    id 24
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 25
    label "testify"
  ]
  node [
    id 26
    label "przybli&#380;enie"
  ]
  node [
    id 27
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 28
    label "kategoria"
  ]
  node [
    id 29
    label "szpaler"
  ]
  node [
    id 30
    label "lon&#380;a"
  ]
  node [
    id 31
    label "uporz&#261;dkowanie"
  ]
  node [
    id 32
    label "egzekutywa"
  ]
  node [
    id 33
    label "jednostka_systematyczna"
  ]
  node [
    id 34
    label "instytucja"
  ]
  node [
    id 35
    label "premier"
  ]
  node [
    id 36
    label "Londyn"
  ]
  node [
    id 37
    label "gabinet_cieni"
  ]
  node [
    id 38
    label "gromada"
  ]
  node [
    id 39
    label "number"
  ]
  node [
    id 40
    label "Konsulat"
  ]
  node [
    id 41
    label "tract"
  ]
  node [
    id 42
    label "klasa"
  ]
  node [
    id 43
    label "w&#322;adza"
  ]
  node [
    id 44
    label "struktura"
  ]
  node [
    id 45
    label "ustalenie"
  ]
  node [
    id 46
    label "spowodowanie"
  ]
  node [
    id 47
    label "structure"
  ]
  node [
    id 48
    label "czynno&#347;&#263;"
  ]
  node [
    id 49
    label "sequence"
  ]
  node [
    id 50
    label "succession"
  ]
  node [
    id 51
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 52
    label "zapoznanie"
  ]
  node [
    id 53
    label "podanie"
  ]
  node [
    id 54
    label "bliski"
  ]
  node [
    id 55
    label "wyja&#347;nienie"
  ]
  node [
    id 56
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 57
    label "przemieszczenie"
  ]
  node [
    id 58
    label "approach"
  ]
  node [
    id 59
    label "pickup"
  ]
  node [
    id 60
    label "estimate"
  ]
  node [
    id 61
    label "po&#322;&#261;czenie"
  ]
  node [
    id 62
    label "ocena"
  ]
  node [
    id 63
    label "zbi&#243;r"
  ]
  node [
    id 64
    label "wytw&#243;r"
  ]
  node [
    id 65
    label "type"
  ]
  node [
    id 66
    label "poj&#281;cie"
  ]
  node [
    id 67
    label "teoria"
  ]
  node [
    id 68
    label "forma"
  ]
  node [
    id 69
    label "organ"
  ]
  node [
    id 70
    label "obrady"
  ]
  node [
    id 71
    label "executive"
  ]
  node [
    id 72
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 73
    label "partia"
  ]
  node [
    id 74
    label "federacja"
  ]
  node [
    id 75
    label "osoba_prawna"
  ]
  node [
    id 76
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 77
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 78
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 79
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 80
    label "biuro"
  ]
  node [
    id 81
    label "organizacja"
  ]
  node [
    id 82
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 83
    label "Fundusze_Unijne"
  ]
  node [
    id 84
    label "zamyka&#263;"
  ]
  node [
    id 85
    label "establishment"
  ]
  node [
    id 86
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 87
    label "urz&#261;d"
  ]
  node [
    id 88
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 89
    label "afiliowa&#263;"
  ]
  node [
    id 90
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 91
    label "standard"
  ]
  node [
    id 92
    label "zamykanie"
  ]
  node [
    id 93
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 94
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 95
    label "przej&#347;cie"
  ]
  node [
    id 96
    label "espalier"
  ]
  node [
    id 97
    label "aleja"
  ]
  node [
    id 98
    label "szyk"
  ]
  node [
    id 99
    label "wagon"
  ]
  node [
    id 100
    label "mecz_mistrzowski"
  ]
  node [
    id 101
    label "przedmiot"
  ]
  node [
    id 102
    label "arrangement"
  ]
  node [
    id 103
    label "class"
  ]
  node [
    id 104
    label "&#322;awka"
  ]
  node [
    id 105
    label "wykrzyknik"
  ]
  node [
    id 106
    label "zaleta"
  ]
  node [
    id 107
    label "programowanie_obiektowe"
  ]
  node [
    id 108
    label "tablica"
  ]
  node [
    id 109
    label "warstwa"
  ]
  node [
    id 110
    label "rezerwa"
  ]
  node [
    id 111
    label "Ekwici"
  ]
  node [
    id 112
    label "&#347;rodowisko"
  ]
  node [
    id 113
    label "szko&#322;a"
  ]
  node [
    id 114
    label "sala"
  ]
  node [
    id 115
    label "pomoc"
  ]
  node [
    id 116
    label "form"
  ]
  node [
    id 117
    label "grupa"
  ]
  node [
    id 118
    label "przepisa&#263;"
  ]
  node [
    id 119
    label "jako&#347;&#263;"
  ]
  node [
    id 120
    label "znak_jako&#347;ci"
  ]
  node [
    id 121
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 122
    label "poziom"
  ]
  node [
    id 123
    label "promocja"
  ]
  node [
    id 124
    label "przepisanie"
  ]
  node [
    id 125
    label "kurs"
  ]
  node [
    id 126
    label "obiekt"
  ]
  node [
    id 127
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 128
    label "dziennik_lekcyjny"
  ]
  node [
    id 129
    label "typ"
  ]
  node [
    id 130
    label "fakcja"
  ]
  node [
    id 131
    label "obrona"
  ]
  node [
    id 132
    label "atak"
  ]
  node [
    id 133
    label "botanika"
  ]
  node [
    id 134
    label "jednostka_administracyjna"
  ]
  node [
    id 135
    label "zoologia"
  ]
  node [
    id 136
    label "skupienie"
  ]
  node [
    id 137
    label "kr&#243;lestwo"
  ]
  node [
    id 138
    label "stage_set"
  ]
  node [
    id 139
    label "tribe"
  ]
  node [
    id 140
    label "hurma"
  ]
  node [
    id 141
    label "lina"
  ]
  node [
    id 142
    label "sprz&#281;t_wspinaczkowy"
  ]
  node [
    id 143
    label "Bismarck"
  ]
  node [
    id 144
    label "zwierzchnik"
  ]
  node [
    id 145
    label "Sto&#322;ypin"
  ]
  node [
    id 146
    label "Miko&#322;ajczyk"
  ]
  node [
    id 147
    label "Chruszczow"
  ]
  node [
    id 148
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 149
    label "Jelcyn"
  ]
  node [
    id 150
    label "dostojnik"
  ]
  node [
    id 151
    label "prawo"
  ]
  node [
    id 152
    label "cz&#322;owiek"
  ]
  node [
    id 153
    label "rz&#261;dzenie"
  ]
  node [
    id 154
    label "panowanie"
  ]
  node [
    id 155
    label "Kreml"
  ]
  node [
    id 156
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 157
    label "wydolno&#347;&#263;"
  ]
  node [
    id 158
    label "Wimbledon"
  ]
  node [
    id 159
    label "Westminster"
  ]
  node [
    id 160
    label "Londek"
  ]
  node [
    id 161
    label "czu&#263;"
  ]
  node [
    id 162
    label "desire"
  ]
  node [
    id 163
    label "kcie&#263;"
  ]
  node [
    id 164
    label "postrzega&#263;"
  ]
  node [
    id 165
    label "przewidywa&#263;"
  ]
  node [
    id 166
    label "by&#263;"
  ]
  node [
    id 167
    label "smell"
  ]
  node [
    id 168
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 169
    label "uczuwa&#263;"
  ]
  node [
    id 170
    label "spirit"
  ]
  node [
    id 171
    label "doznawa&#263;"
  ]
  node [
    id 172
    label "anticipate"
  ]
  node [
    id 173
    label "gaworzy&#263;"
  ]
  node [
    id 174
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 175
    label "remark"
  ]
  node [
    id 176
    label "rozmawia&#263;"
  ]
  node [
    id 177
    label "wyra&#380;a&#263;"
  ]
  node [
    id 178
    label "umie&#263;"
  ]
  node [
    id 179
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 180
    label "dziama&#263;"
  ]
  node [
    id 181
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 182
    label "formu&#322;owa&#263;"
  ]
  node [
    id 183
    label "dysfonia"
  ]
  node [
    id 184
    label "express"
  ]
  node [
    id 185
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 186
    label "talk"
  ]
  node [
    id 187
    label "u&#380;ywa&#263;"
  ]
  node [
    id 188
    label "prawi&#263;"
  ]
  node [
    id 189
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 190
    label "powiada&#263;"
  ]
  node [
    id 191
    label "tell"
  ]
  node [
    id 192
    label "chew_the_fat"
  ]
  node [
    id 193
    label "say"
  ]
  node [
    id 194
    label "j&#281;zyk"
  ]
  node [
    id 195
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 196
    label "informowa&#263;"
  ]
  node [
    id 197
    label "wydobywa&#263;"
  ]
  node [
    id 198
    label "okre&#347;la&#263;"
  ]
  node [
    id 199
    label "korzysta&#263;"
  ]
  node [
    id 200
    label "distribute"
  ]
  node [
    id 201
    label "give"
  ]
  node [
    id 202
    label "bash"
  ]
  node [
    id 203
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 204
    label "decydowa&#263;"
  ]
  node [
    id 205
    label "signify"
  ]
  node [
    id 206
    label "style"
  ]
  node [
    id 207
    label "powodowa&#263;"
  ]
  node [
    id 208
    label "komunikowa&#263;"
  ]
  node [
    id 209
    label "znaczy&#263;"
  ]
  node [
    id 210
    label "give_voice"
  ]
  node [
    id 211
    label "oznacza&#263;"
  ]
  node [
    id 212
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 213
    label "represent"
  ]
  node [
    id 214
    label "convey"
  ]
  node [
    id 215
    label "arouse"
  ]
  node [
    id 216
    label "robi&#263;"
  ]
  node [
    id 217
    label "determine"
  ]
  node [
    id 218
    label "work"
  ]
  node [
    id 219
    label "reakcja_chemiczna"
  ]
  node [
    id 220
    label "uwydatnia&#263;"
  ]
  node [
    id 221
    label "eksploatowa&#263;"
  ]
  node [
    id 222
    label "uzyskiwa&#263;"
  ]
  node [
    id 223
    label "wydostawa&#263;"
  ]
  node [
    id 224
    label "wyjmowa&#263;"
  ]
  node [
    id 225
    label "train"
  ]
  node [
    id 226
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 227
    label "wydawa&#263;"
  ]
  node [
    id 228
    label "dobywa&#263;"
  ]
  node [
    id 229
    label "ocala&#263;"
  ]
  node [
    id 230
    label "excavate"
  ]
  node [
    id 231
    label "g&#243;rnictwo"
  ]
  node [
    id 232
    label "raise"
  ]
  node [
    id 233
    label "wiedzie&#263;"
  ]
  node [
    id 234
    label "can"
  ]
  node [
    id 235
    label "m&#243;c"
  ]
  node [
    id 236
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 237
    label "rozumie&#263;"
  ]
  node [
    id 238
    label "szczeka&#263;"
  ]
  node [
    id 239
    label "funkcjonowa&#263;"
  ]
  node [
    id 240
    label "mawia&#263;"
  ]
  node [
    id 241
    label "opowiada&#263;"
  ]
  node [
    id 242
    label "chatter"
  ]
  node [
    id 243
    label "niemowl&#281;"
  ]
  node [
    id 244
    label "kosmetyk"
  ]
  node [
    id 245
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 246
    label "stanowisko_archeologiczne"
  ]
  node [
    id 247
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 248
    label "artykulator"
  ]
  node [
    id 249
    label "kod"
  ]
  node [
    id 250
    label "kawa&#322;ek"
  ]
  node [
    id 251
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 252
    label "gramatyka"
  ]
  node [
    id 253
    label "stylik"
  ]
  node [
    id 254
    label "przet&#322;umaczenie"
  ]
  node [
    id 255
    label "formalizowanie"
  ]
  node [
    id 256
    label "ssa&#263;"
  ]
  node [
    id 257
    label "ssanie"
  ]
  node [
    id 258
    label "language"
  ]
  node [
    id 259
    label "liza&#263;"
  ]
  node [
    id 260
    label "napisa&#263;"
  ]
  node [
    id 261
    label "konsonantyzm"
  ]
  node [
    id 262
    label "wokalizm"
  ]
  node [
    id 263
    label "pisa&#263;"
  ]
  node [
    id 264
    label "fonetyka"
  ]
  node [
    id 265
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 266
    label "jeniec"
  ]
  node [
    id 267
    label "but"
  ]
  node [
    id 268
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 269
    label "po_koroniarsku"
  ]
  node [
    id 270
    label "kultura_duchowa"
  ]
  node [
    id 271
    label "t&#322;umaczenie"
  ]
  node [
    id 272
    label "m&#243;wienie"
  ]
  node [
    id 273
    label "pype&#263;"
  ]
  node [
    id 274
    label "lizanie"
  ]
  node [
    id 275
    label "pismo"
  ]
  node [
    id 276
    label "formalizowa&#263;"
  ]
  node [
    id 277
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 278
    label "rozumienie"
  ]
  node [
    id 279
    label "spos&#243;b"
  ]
  node [
    id 280
    label "makroglosja"
  ]
  node [
    id 281
    label "jama_ustna"
  ]
  node [
    id 282
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 283
    label "formacja_geologiczna"
  ]
  node [
    id 284
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 285
    label "natural_language"
  ]
  node [
    id 286
    label "s&#322;ownictwo"
  ]
  node [
    id 287
    label "urz&#261;dzenie"
  ]
  node [
    id 288
    label "dysphonia"
  ]
  node [
    id 289
    label "dysleksja"
  ]
  node [
    id 290
    label "patologia"
  ]
  node [
    id 291
    label "drug_addiction"
  ]
  node [
    id 292
    label "flashback"
  ]
  node [
    id 293
    label "na&#322;&#243;g"
  ]
  node [
    id 294
    label "g&#322;&#243;d_nikotynowy"
  ]
  node [
    id 295
    label "g&#322;&#243;d_narkotyczny"
  ]
  node [
    id 296
    label "dysfunkcja"
  ]
  node [
    id 297
    label "nawyk"
  ]
  node [
    id 298
    label "g&#322;&#243;d_alkoholowy"
  ]
  node [
    id 299
    label "addiction"
  ]
  node [
    id 300
    label "ognisko"
  ]
  node [
    id 301
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 302
    label "powalenie"
  ]
  node [
    id 303
    label "odezwanie_si&#281;"
  ]
  node [
    id 304
    label "atakowanie"
  ]
  node [
    id 305
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 306
    label "patomorfologia"
  ]
  node [
    id 307
    label "grupa_ryzyka"
  ]
  node [
    id 308
    label "przypadek"
  ]
  node [
    id 309
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 310
    label "patolnia"
  ]
  node [
    id 311
    label "nabawienie_si&#281;"
  ]
  node [
    id 312
    label "przemoc"
  ]
  node [
    id 313
    label "inkubacja"
  ]
  node [
    id 314
    label "medycyna"
  ]
  node [
    id 315
    label "szambo"
  ]
  node [
    id 316
    label "gangsterski"
  ]
  node [
    id 317
    label "fizjologia_patologiczna"
  ]
  node [
    id 318
    label "kryzys"
  ]
  node [
    id 319
    label "powali&#263;"
  ]
  node [
    id 320
    label "remisja"
  ]
  node [
    id 321
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 322
    label "zajmowa&#263;"
  ]
  node [
    id 323
    label "zaburzenie"
  ]
  node [
    id 324
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 325
    label "neuropatologia"
  ]
  node [
    id 326
    label "aspo&#322;eczny"
  ]
  node [
    id 327
    label "badanie_histopatologiczne"
  ]
  node [
    id 328
    label "abnormality"
  ]
  node [
    id 329
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 330
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 331
    label "patogeneza"
  ]
  node [
    id 332
    label "psychopatologia"
  ]
  node [
    id 333
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 334
    label "paleopatologia"
  ]
  node [
    id 335
    label "logopatologia"
  ]
  node [
    id 336
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 337
    label "immunopatologia"
  ]
  node [
    id 338
    label "osteopatologia"
  ]
  node [
    id 339
    label "odzywanie_si&#281;"
  ]
  node [
    id 340
    label "diagnoza"
  ]
  node [
    id 341
    label "atakowa&#263;"
  ]
  node [
    id 342
    label "histopatologia"
  ]
  node [
    id 343
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 344
    label "nabawianie_si&#281;"
  ]
  node [
    id 345
    label "underworld"
  ]
  node [
    id 346
    label "meteoropatologia"
  ]
  node [
    id 347
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 348
    label "zajmowanie"
  ]
  node [
    id 349
    label "wspomnienie"
  ]
  node [
    id 350
    label "refleksja"
  ]
  node [
    id 351
    label "przywidzenie"
  ]
  node [
    id 352
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 353
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 354
    label "klient"
  ]
  node [
    id 355
    label "piel&#281;gniarz"
  ]
  node [
    id 356
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 357
    label "od&#322;&#261;czanie"
  ]
  node [
    id 358
    label "od&#322;&#261;czenie"
  ]
  node [
    id 359
    label "chory"
  ]
  node [
    id 360
    label "szpitalnik"
  ]
  node [
    id 361
    label "agent_rozliczeniowy"
  ]
  node [
    id 362
    label "komputer_cyfrowy"
  ]
  node [
    id 363
    label "us&#322;ugobiorca"
  ]
  node [
    id 364
    label "Rzymianin"
  ]
  node [
    id 365
    label "szlachcic"
  ]
  node [
    id 366
    label "obywatel"
  ]
  node [
    id 367
    label "klientela"
  ]
  node [
    id 368
    label "bratek"
  ]
  node [
    id 369
    label "program"
  ]
  node [
    id 370
    label "ludzko&#347;&#263;"
  ]
  node [
    id 371
    label "asymilowanie"
  ]
  node [
    id 372
    label "wapniak"
  ]
  node [
    id 373
    label "asymilowa&#263;"
  ]
  node [
    id 374
    label "os&#322;abia&#263;"
  ]
  node [
    id 375
    label "posta&#263;"
  ]
  node [
    id 376
    label "hominid"
  ]
  node [
    id 377
    label "podw&#322;adny"
  ]
  node [
    id 378
    label "os&#322;abianie"
  ]
  node [
    id 379
    label "g&#322;owa"
  ]
  node [
    id 380
    label "figura"
  ]
  node [
    id 381
    label "portrecista"
  ]
  node [
    id 382
    label "dwun&#243;g"
  ]
  node [
    id 383
    label "profanum"
  ]
  node [
    id 384
    label "mikrokosmos"
  ]
  node [
    id 385
    label "nasada"
  ]
  node [
    id 386
    label "duch"
  ]
  node [
    id 387
    label "antropochoria"
  ]
  node [
    id 388
    label "osoba"
  ]
  node [
    id 389
    label "wz&#243;r"
  ]
  node [
    id 390
    label "senior"
  ]
  node [
    id 391
    label "oddzia&#322;ywanie"
  ]
  node [
    id 392
    label "Adam"
  ]
  node [
    id 393
    label "homo_sapiens"
  ]
  node [
    id 394
    label "polifag"
  ]
  node [
    id 395
    label "krzy&#380;owiec"
  ]
  node [
    id 396
    label "tytu&#322;"
  ]
  node [
    id 397
    label "szpitalnicy"
  ]
  node [
    id 398
    label "kawaler"
  ]
  node [
    id 399
    label "zakonnik"
  ]
  node [
    id 400
    label "publish"
  ]
  node [
    id 401
    label "challenge"
  ]
  node [
    id 402
    label "separate"
  ]
  node [
    id 403
    label "oddzieli&#263;"
  ]
  node [
    id 404
    label "release"
  ]
  node [
    id 405
    label "odci&#261;&#263;"
  ]
  node [
    id 406
    label "amputate"
  ]
  node [
    id 407
    label "przerwa&#263;"
  ]
  node [
    id 408
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 409
    label "cutoff"
  ]
  node [
    id 410
    label "od&#322;&#261;czony"
  ]
  node [
    id 411
    label "odbicie"
  ]
  node [
    id 412
    label "przerwanie"
  ]
  node [
    id 413
    label "odci&#281;cie"
  ]
  node [
    id 414
    label "ablation"
  ]
  node [
    id 415
    label "oddzielenie"
  ]
  node [
    id 416
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 417
    label "choro"
  ]
  node [
    id 418
    label "nieprzytomny"
  ]
  node [
    id 419
    label "skandaliczny"
  ]
  node [
    id 420
    label "chor&#243;bka"
  ]
  node [
    id 421
    label "nienormalny"
  ]
  node [
    id 422
    label "niezdrowy"
  ]
  node [
    id 423
    label "w&#347;ciek&#322;y"
  ]
  node [
    id 424
    label "niezrozumia&#322;y"
  ]
  node [
    id 425
    label "chorowanie"
  ]
  node [
    id 426
    label "le&#380;alnia"
  ]
  node [
    id 427
    label "psychiczny"
  ]
  node [
    id 428
    label "zachorowanie"
  ]
  node [
    id 429
    label "rozchorowywanie_si&#281;"
  ]
  node [
    id 430
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 431
    label "take"
  ]
  node [
    id 432
    label "przerywa&#263;"
  ]
  node [
    id 433
    label "odcina&#263;"
  ]
  node [
    id 434
    label "abstract"
  ]
  node [
    id 435
    label "oddziela&#263;"
  ]
  node [
    id 436
    label "wydarzenie"
  ]
  node [
    id 437
    label "happening"
  ]
  node [
    id 438
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 439
    label "schorzenie"
  ]
  node [
    id 440
    label "przyk&#322;ad"
  ]
  node [
    id 441
    label "kategoria_gramatyczna"
  ]
  node [
    id 442
    label "przeznaczenie"
  ]
  node [
    id 443
    label "severance"
  ]
  node [
    id 444
    label "odcinanie"
  ]
  node [
    id 445
    label "oddzielanie"
  ]
  node [
    id 446
    label "odbijanie"
  ]
  node [
    id 447
    label "przerywanie"
  ]
  node [
    id 448
    label "rupture"
  ]
  node [
    id 449
    label "dissociation"
  ]
  node [
    id 450
    label "sprawa"
  ]
  node [
    id 451
    label "subiekcja"
  ]
  node [
    id 452
    label "problemat"
  ]
  node [
    id 453
    label "jajko_Kolumba"
  ]
  node [
    id 454
    label "obstruction"
  ]
  node [
    id 455
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 456
    label "problematyka"
  ]
  node [
    id 457
    label "trudno&#347;&#263;"
  ]
  node [
    id 458
    label "pierepa&#322;ka"
  ]
  node [
    id 459
    label "ambaras"
  ]
  node [
    id 460
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 461
    label "napotka&#263;"
  ]
  node [
    id 462
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 463
    label "k&#322;opotliwy"
  ]
  node [
    id 464
    label "napotkanie"
  ]
  node [
    id 465
    label "difficulty"
  ]
  node [
    id 466
    label "obstacle"
  ]
  node [
    id 467
    label "cecha"
  ]
  node [
    id 468
    label "sytuacja"
  ]
  node [
    id 469
    label "kognicja"
  ]
  node [
    id 470
    label "object"
  ]
  node [
    id 471
    label "rozprawa"
  ]
  node [
    id 472
    label "temat"
  ]
  node [
    id 473
    label "szczeg&#243;&#322;"
  ]
  node [
    id 474
    label "proposition"
  ]
  node [
    id 475
    label "przes&#322;anka"
  ]
  node [
    id 476
    label "rzecz"
  ]
  node [
    id 477
    label "idea"
  ]
  node [
    id 478
    label "k&#322;opot"
  ]
  node [
    id 479
    label "zdj&#281;cie"
  ]
  node [
    id 480
    label "lu"
  ]
  node [
    id 481
    label "Guang"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 479
    target 480
  ]
  edge [
    source 479
    target 481
  ]
  edge [
    source 480
    target 481
  ]
]
