graph [
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "wysoki"
    origin "text"
  ]
  node [
    id 3
    label "izba"
    origin "text"
  ]
  node [
    id 4
    label "minister"
    origin "text"
  ]
  node [
    id 5
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "tak"
    origin "text"
  ]
  node [
    id 7
    label "jak"
    origin "text"
  ]
  node [
    id 8
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "moi"
    origin "text"
  ]
  node [
    id 10
    label "przedm&#243;wca"
    origin "text"
  ]
  node [
    id 11
    label "wielki"
    origin "text"
  ]
  node [
    id 12
    label "podzi&#281;kowanie"
    origin "text"
  ]
  node [
    id 13
    label "dla"
    origin "text"
  ]
  node [
    id 14
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 15
    label "ten"
    origin "text"
  ]
  node [
    id 16
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 17
    label "inicjatywa"
    origin "text"
  ]
  node [
    id 18
    label "szczeg&#243;lnie"
    origin "text"
  ]
  node [
    id 19
    label "jakub"
    origin "text"
  ]
  node [
    id 20
    label "p&#322;a&#380;y&#324;skiego"
    origin "text"
  ]
  node [
    id 21
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 22
    label "dobrze"
    origin "text"
  ]
  node [
    id 23
    label "by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "polska"
    origin "text"
  ]
  node [
    id 25
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 26
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 27
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 28
    label "osoba"
    origin "text"
  ]
  node [
    id 29
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 30
    label "ponad"
    origin "text"
  ]
  node [
    id 31
    label "lata"
    origin "text"
  ]
  node [
    id 32
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 33
    label "wype&#322;nienie"
    origin "text"
  ]
  node [
    id 34
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 35
    label "dziejowy"
    origin "text"
  ]
  node [
    id 36
    label "mo&#380;liwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 37
    label "powr&#243;t"
    origin "text"
  ]
  node [
    id 38
    label "ojczyzna"
    origin "text"
  ]
  node [
    id 39
    label "wiele"
    origin "text"
  ]
  node [
    id 40
    label "inny"
    origin "text"
  ]
  node [
    id 41
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 42
    label "narodowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 43
    label "dawno"
    origin "text"
  ]
  node [
    id 44
    label "wr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 45
    label "pocz&#261;tek"
    origin "text"
  ]
  node [
    id 46
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 47
    label "nasa"
    origin "text"
  ]
  node [
    id 48
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 49
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 50
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 51
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 52
    label "czas"
    origin "text"
  ]
  node [
    id 53
    label "realizowa&#263;"
    origin "text"
  ]
  node [
    id 54
    label "albo"
    origin "text"
  ]
  node [
    id 55
    label "po&#347;rednictwo"
    origin "text"
  ]
  node [
    id 56
    label "samorz&#261;d"
    origin "text"
  ]
  node [
    id 57
    label "murza"
  ]
  node [
    id 58
    label "belfer"
  ]
  node [
    id 59
    label "szkolnik"
  ]
  node [
    id 60
    label "pupil"
  ]
  node [
    id 61
    label "ojciec"
  ]
  node [
    id 62
    label "kszta&#322;ciciel"
  ]
  node [
    id 63
    label "Midas"
  ]
  node [
    id 64
    label "przyw&#243;dca"
  ]
  node [
    id 65
    label "opiekun"
  ]
  node [
    id 66
    label "Mieszko_I"
  ]
  node [
    id 67
    label "doros&#322;y"
  ]
  node [
    id 68
    label "pracodawca"
  ]
  node [
    id 69
    label "profesor"
  ]
  node [
    id 70
    label "m&#261;&#380;"
  ]
  node [
    id 71
    label "rz&#261;dzenie"
  ]
  node [
    id 72
    label "bogaty"
  ]
  node [
    id 73
    label "pa&#324;stwo"
  ]
  node [
    id 74
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 75
    label "w&#322;odarz"
  ]
  node [
    id 76
    label "nabab"
  ]
  node [
    id 77
    label "samiec"
  ]
  node [
    id 78
    label "preceptor"
  ]
  node [
    id 79
    label "pedagog"
  ]
  node [
    id 80
    label "efendi"
  ]
  node [
    id 81
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 82
    label "popularyzator"
  ]
  node [
    id 83
    label "gra_w_karty"
  ]
  node [
    id 84
    label "zwrot"
  ]
  node [
    id 85
    label "jegomo&#347;&#263;"
  ]
  node [
    id 86
    label "androlog"
  ]
  node [
    id 87
    label "bratek"
  ]
  node [
    id 88
    label "andropauza"
  ]
  node [
    id 89
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 90
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 91
    label "ch&#322;opina"
  ]
  node [
    id 92
    label "w&#322;adza"
  ]
  node [
    id 93
    label "Sabataj_Cwi"
  ]
  node [
    id 94
    label "lider"
  ]
  node [
    id 95
    label "Mao"
  ]
  node [
    id 96
    label "Anders"
  ]
  node [
    id 97
    label "Fidel_Castro"
  ]
  node [
    id 98
    label "Miko&#322;ajczyk"
  ]
  node [
    id 99
    label "Tito"
  ]
  node [
    id 100
    label "Ko&#347;ciuszko"
  ]
  node [
    id 101
    label "p&#322;atnik"
  ]
  node [
    id 102
    label "zwierzchnik"
  ]
  node [
    id 103
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 104
    label "nadzorca"
  ]
  node [
    id 105
    label "funkcjonariusz"
  ]
  node [
    id 106
    label "podmiot"
  ]
  node [
    id 107
    label "wykupywanie"
  ]
  node [
    id 108
    label "wykupienie"
  ]
  node [
    id 109
    label "bycie_w_posiadaniu"
  ]
  node [
    id 110
    label "rozszerzyciel"
  ]
  node [
    id 111
    label "asymilowa&#263;"
  ]
  node [
    id 112
    label "nasada"
  ]
  node [
    id 113
    label "profanum"
  ]
  node [
    id 114
    label "wz&#243;r"
  ]
  node [
    id 115
    label "senior"
  ]
  node [
    id 116
    label "asymilowanie"
  ]
  node [
    id 117
    label "os&#322;abia&#263;"
  ]
  node [
    id 118
    label "homo_sapiens"
  ]
  node [
    id 119
    label "ludzko&#347;&#263;"
  ]
  node [
    id 120
    label "Adam"
  ]
  node [
    id 121
    label "hominid"
  ]
  node [
    id 122
    label "posta&#263;"
  ]
  node [
    id 123
    label "portrecista"
  ]
  node [
    id 124
    label "polifag"
  ]
  node [
    id 125
    label "podw&#322;adny"
  ]
  node [
    id 126
    label "dwun&#243;g"
  ]
  node [
    id 127
    label "wapniak"
  ]
  node [
    id 128
    label "duch"
  ]
  node [
    id 129
    label "os&#322;abianie"
  ]
  node [
    id 130
    label "antropochoria"
  ]
  node [
    id 131
    label "figura"
  ]
  node [
    id 132
    label "g&#322;owa"
  ]
  node [
    id 133
    label "mikrokosmos"
  ]
  node [
    id 134
    label "oddzia&#322;ywanie"
  ]
  node [
    id 135
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 136
    label "du&#380;y"
  ]
  node [
    id 137
    label "dojrza&#322;y"
  ]
  node [
    id 138
    label "dojrzale"
  ]
  node [
    id 139
    label "wydoro&#347;lenie"
  ]
  node [
    id 140
    label "doro&#347;lenie"
  ]
  node [
    id 141
    label "m&#261;dry"
  ]
  node [
    id 142
    label "&#378;ra&#322;y"
  ]
  node [
    id 143
    label "doletni"
  ]
  node [
    id 144
    label "doro&#347;le"
  ]
  node [
    id 145
    label "punkt"
  ]
  node [
    id 146
    label "zmiana"
  ]
  node [
    id 147
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 148
    label "turn"
  ]
  node [
    id 149
    label "wyra&#380;enie"
  ]
  node [
    id 150
    label "fraza_czasownikowa"
  ]
  node [
    id 151
    label "turning"
  ]
  node [
    id 152
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 153
    label "skr&#281;t"
  ]
  node [
    id 154
    label "jednostka_leksykalna"
  ]
  node [
    id 155
    label "obr&#243;t"
  ]
  node [
    id 156
    label "starosta"
  ]
  node [
    id 157
    label "w&#322;adca"
  ]
  node [
    id 158
    label "zarz&#261;dca"
  ]
  node [
    id 159
    label "nauczyciel_akademicki"
  ]
  node [
    id 160
    label "tytu&#322;"
  ]
  node [
    id 161
    label "stopie&#324;_naukowy"
  ]
  node [
    id 162
    label "konsulent"
  ]
  node [
    id 163
    label "profesura"
  ]
  node [
    id 164
    label "nauczyciel"
  ]
  node [
    id 165
    label "wirtuoz"
  ]
  node [
    id 166
    label "autor"
  ]
  node [
    id 167
    label "szko&#322;a"
  ]
  node [
    id 168
    label "tarcza"
  ]
  node [
    id 169
    label "klasa"
  ]
  node [
    id 170
    label "elew"
  ]
  node [
    id 171
    label "wyprawka"
  ]
  node [
    id 172
    label "mundurek"
  ]
  node [
    id 173
    label "absolwent"
  ]
  node [
    id 174
    label "ochotnik"
  ]
  node [
    id 175
    label "nauczyciel_muzyki"
  ]
  node [
    id 176
    label "pomocnik"
  ]
  node [
    id 177
    label "zakonnik"
  ]
  node [
    id 178
    label "student"
  ]
  node [
    id 179
    label "ekspert"
  ]
  node [
    id 180
    label "bogacz"
  ]
  node [
    id 181
    label "dostojnik"
  ]
  node [
    id 182
    label "urz&#281;dnik"
  ]
  node [
    id 183
    label "&#347;w"
  ]
  node [
    id 184
    label "rodzic"
  ]
  node [
    id 185
    label "pomys&#322;odawca"
  ]
  node [
    id 186
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 187
    label "rodzice"
  ]
  node [
    id 188
    label "wykonawca"
  ]
  node [
    id 189
    label "stary"
  ]
  node [
    id 190
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 191
    label "kuwada"
  ]
  node [
    id 192
    label "ojczym"
  ]
  node [
    id 193
    label "papa"
  ]
  node [
    id 194
    label "przodek"
  ]
  node [
    id 195
    label "tworzyciel"
  ]
  node [
    id 196
    label "facet"
  ]
  node [
    id 197
    label "kochanek"
  ]
  node [
    id 198
    label "fio&#322;ek"
  ]
  node [
    id 199
    label "brat"
  ]
  node [
    id 200
    label "zwierz&#281;"
  ]
  node [
    id 201
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 202
    label "pan_i_w&#322;adca"
  ]
  node [
    id 203
    label "pan_m&#322;ody"
  ]
  node [
    id 204
    label "ch&#322;op"
  ]
  node [
    id 205
    label "&#347;lubny"
  ]
  node [
    id 206
    label "m&#243;j"
  ]
  node [
    id 207
    label "pan_domu"
  ]
  node [
    id 208
    label "ma&#322;&#380;onek"
  ]
  node [
    id 209
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 210
    label "mo&#347;&#263;"
  ]
  node [
    id 211
    label "Frygia"
  ]
  node [
    id 212
    label "dominowanie"
  ]
  node [
    id 213
    label "reign"
  ]
  node [
    id 214
    label "sprawowanie"
  ]
  node [
    id 215
    label "dominion"
  ]
  node [
    id 216
    label "rule"
  ]
  node [
    id 217
    label "zwierz&#281;_domowe"
  ]
  node [
    id 218
    label "John_Dewey"
  ]
  node [
    id 219
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 220
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 221
    label "J&#281;drzejewicz"
  ]
  node [
    id 222
    label "specjalista"
  ]
  node [
    id 223
    label "&#380;ycie"
  ]
  node [
    id 224
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 225
    label "Turek"
  ]
  node [
    id 226
    label "effendi"
  ]
  node [
    id 227
    label "och&#281;do&#380;ny"
  ]
  node [
    id 228
    label "zapa&#347;ny"
  ]
  node [
    id 229
    label "sytuowany"
  ]
  node [
    id 230
    label "obfituj&#261;cy"
  ]
  node [
    id 231
    label "forsiasty"
  ]
  node [
    id 232
    label "spania&#322;y"
  ]
  node [
    id 233
    label "obficie"
  ]
  node [
    id 234
    label "r&#243;&#380;norodny"
  ]
  node [
    id 235
    label "bogato"
  ]
  node [
    id 236
    label "Japonia"
  ]
  node [
    id 237
    label "Zair"
  ]
  node [
    id 238
    label "Belize"
  ]
  node [
    id 239
    label "San_Marino"
  ]
  node [
    id 240
    label "Tanzania"
  ]
  node [
    id 241
    label "Antigua_i_Barbuda"
  ]
  node [
    id 242
    label "granica_pa&#324;stwa"
  ]
  node [
    id 243
    label "Senegal"
  ]
  node [
    id 244
    label "Indie"
  ]
  node [
    id 245
    label "Seszele"
  ]
  node [
    id 246
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 247
    label "Zimbabwe"
  ]
  node [
    id 248
    label "Filipiny"
  ]
  node [
    id 249
    label "Mauretania"
  ]
  node [
    id 250
    label "Malezja"
  ]
  node [
    id 251
    label "Rumunia"
  ]
  node [
    id 252
    label "Surinam"
  ]
  node [
    id 253
    label "Ukraina"
  ]
  node [
    id 254
    label "Syria"
  ]
  node [
    id 255
    label "Wyspy_Marshalla"
  ]
  node [
    id 256
    label "Burkina_Faso"
  ]
  node [
    id 257
    label "Grecja"
  ]
  node [
    id 258
    label "Polska"
  ]
  node [
    id 259
    label "Wenezuela"
  ]
  node [
    id 260
    label "Nepal"
  ]
  node [
    id 261
    label "Suazi"
  ]
  node [
    id 262
    label "S&#322;owacja"
  ]
  node [
    id 263
    label "Algieria"
  ]
  node [
    id 264
    label "Chiny"
  ]
  node [
    id 265
    label "Grenada"
  ]
  node [
    id 266
    label "Barbados"
  ]
  node [
    id 267
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 268
    label "Pakistan"
  ]
  node [
    id 269
    label "Niemcy"
  ]
  node [
    id 270
    label "Bahrajn"
  ]
  node [
    id 271
    label "Komory"
  ]
  node [
    id 272
    label "Australia"
  ]
  node [
    id 273
    label "Rodezja"
  ]
  node [
    id 274
    label "Malawi"
  ]
  node [
    id 275
    label "Gwinea"
  ]
  node [
    id 276
    label "Wehrlen"
  ]
  node [
    id 277
    label "Meksyk"
  ]
  node [
    id 278
    label "Liechtenstein"
  ]
  node [
    id 279
    label "Czarnog&#243;ra"
  ]
  node [
    id 280
    label "Wielka_Brytania"
  ]
  node [
    id 281
    label "Kuwejt"
  ]
  node [
    id 282
    label "Angola"
  ]
  node [
    id 283
    label "Monako"
  ]
  node [
    id 284
    label "Jemen"
  ]
  node [
    id 285
    label "Etiopia"
  ]
  node [
    id 286
    label "Madagaskar"
  ]
  node [
    id 287
    label "terytorium"
  ]
  node [
    id 288
    label "Kolumbia"
  ]
  node [
    id 289
    label "Portoryko"
  ]
  node [
    id 290
    label "Mauritius"
  ]
  node [
    id 291
    label "Kostaryka"
  ]
  node [
    id 292
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 293
    label "Tajlandia"
  ]
  node [
    id 294
    label "Argentyna"
  ]
  node [
    id 295
    label "Zambia"
  ]
  node [
    id 296
    label "Sri_Lanka"
  ]
  node [
    id 297
    label "Gwatemala"
  ]
  node [
    id 298
    label "Kirgistan"
  ]
  node [
    id 299
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 300
    label "Hiszpania"
  ]
  node [
    id 301
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 302
    label "Salwador"
  ]
  node [
    id 303
    label "Korea"
  ]
  node [
    id 304
    label "Macedonia"
  ]
  node [
    id 305
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 306
    label "Brunei"
  ]
  node [
    id 307
    label "Mozambik"
  ]
  node [
    id 308
    label "Turcja"
  ]
  node [
    id 309
    label "Kambod&#380;a"
  ]
  node [
    id 310
    label "Benin"
  ]
  node [
    id 311
    label "Bhutan"
  ]
  node [
    id 312
    label "Tunezja"
  ]
  node [
    id 313
    label "Austria"
  ]
  node [
    id 314
    label "Izrael"
  ]
  node [
    id 315
    label "Sierra_Leone"
  ]
  node [
    id 316
    label "Jamajka"
  ]
  node [
    id 317
    label "Rosja"
  ]
  node [
    id 318
    label "Rwanda"
  ]
  node [
    id 319
    label "holoarktyka"
  ]
  node [
    id 320
    label "Nigeria"
  ]
  node [
    id 321
    label "USA"
  ]
  node [
    id 322
    label "Oman"
  ]
  node [
    id 323
    label "Luksemburg"
  ]
  node [
    id 324
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 325
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 326
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 327
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 328
    label "Dominikana"
  ]
  node [
    id 329
    label "Irlandia"
  ]
  node [
    id 330
    label "Liban"
  ]
  node [
    id 331
    label "Hanower"
  ]
  node [
    id 332
    label "Estonia"
  ]
  node [
    id 333
    label "Samoa"
  ]
  node [
    id 334
    label "Nowa_Zelandia"
  ]
  node [
    id 335
    label "Gabon"
  ]
  node [
    id 336
    label "Iran"
  ]
  node [
    id 337
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 338
    label "S&#322;owenia"
  ]
  node [
    id 339
    label "Egipt"
  ]
  node [
    id 340
    label "Kiribati"
  ]
  node [
    id 341
    label "Togo"
  ]
  node [
    id 342
    label "Mongolia"
  ]
  node [
    id 343
    label "Sudan"
  ]
  node [
    id 344
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 345
    label "Bahamy"
  ]
  node [
    id 346
    label "Bangladesz"
  ]
  node [
    id 347
    label "partia"
  ]
  node [
    id 348
    label "Serbia"
  ]
  node [
    id 349
    label "Czechy"
  ]
  node [
    id 350
    label "Holandia"
  ]
  node [
    id 351
    label "Birma"
  ]
  node [
    id 352
    label "Albania"
  ]
  node [
    id 353
    label "Mikronezja"
  ]
  node [
    id 354
    label "Gambia"
  ]
  node [
    id 355
    label "Kazachstan"
  ]
  node [
    id 356
    label "interior"
  ]
  node [
    id 357
    label "Uzbekistan"
  ]
  node [
    id 358
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 359
    label "Malta"
  ]
  node [
    id 360
    label "Lesoto"
  ]
  node [
    id 361
    label "para"
  ]
  node [
    id 362
    label "Antarktis"
  ]
  node [
    id 363
    label "Andora"
  ]
  node [
    id 364
    label "Nauru"
  ]
  node [
    id 365
    label "Kuba"
  ]
  node [
    id 366
    label "Wietnam"
  ]
  node [
    id 367
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 368
    label "ziemia"
  ]
  node [
    id 369
    label "Chorwacja"
  ]
  node [
    id 370
    label "Kamerun"
  ]
  node [
    id 371
    label "Urugwaj"
  ]
  node [
    id 372
    label "Niger"
  ]
  node [
    id 373
    label "Turkmenistan"
  ]
  node [
    id 374
    label "Szwajcaria"
  ]
  node [
    id 375
    label "organizacja"
  ]
  node [
    id 376
    label "grupa"
  ]
  node [
    id 377
    label "Litwa"
  ]
  node [
    id 378
    label "Palau"
  ]
  node [
    id 379
    label "Gruzja"
  ]
  node [
    id 380
    label "Kongo"
  ]
  node [
    id 381
    label "Tajwan"
  ]
  node [
    id 382
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 383
    label "Honduras"
  ]
  node [
    id 384
    label "Boliwia"
  ]
  node [
    id 385
    label "Uganda"
  ]
  node [
    id 386
    label "Namibia"
  ]
  node [
    id 387
    label "Erytrea"
  ]
  node [
    id 388
    label "Azerbejd&#380;an"
  ]
  node [
    id 389
    label "Panama"
  ]
  node [
    id 390
    label "Gujana"
  ]
  node [
    id 391
    label "Somalia"
  ]
  node [
    id 392
    label "Burundi"
  ]
  node [
    id 393
    label "Tuwalu"
  ]
  node [
    id 394
    label "Libia"
  ]
  node [
    id 395
    label "Katar"
  ]
  node [
    id 396
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 397
    label "Trynidad_i_Tobago"
  ]
  node [
    id 398
    label "Sahara_Zachodnia"
  ]
  node [
    id 399
    label "Gwinea_Bissau"
  ]
  node [
    id 400
    label "Bu&#322;garia"
  ]
  node [
    id 401
    label "Tonga"
  ]
  node [
    id 402
    label "Nikaragua"
  ]
  node [
    id 403
    label "Fid&#380;i"
  ]
  node [
    id 404
    label "Timor_Wschodni"
  ]
  node [
    id 405
    label "Laos"
  ]
  node [
    id 406
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 407
    label "Ghana"
  ]
  node [
    id 408
    label "Brazylia"
  ]
  node [
    id 409
    label "Belgia"
  ]
  node [
    id 410
    label "Irak"
  ]
  node [
    id 411
    label "Peru"
  ]
  node [
    id 412
    label "Arabia_Saudyjska"
  ]
  node [
    id 413
    label "Indonezja"
  ]
  node [
    id 414
    label "Malediwy"
  ]
  node [
    id 415
    label "Afganistan"
  ]
  node [
    id 416
    label "Jordania"
  ]
  node [
    id 417
    label "Kenia"
  ]
  node [
    id 418
    label "Czad"
  ]
  node [
    id 419
    label "Liberia"
  ]
  node [
    id 420
    label "Mali"
  ]
  node [
    id 421
    label "Armenia"
  ]
  node [
    id 422
    label "W&#281;gry"
  ]
  node [
    id 423
    label "Chile"
  ]
  node [
    id 424
    label "Kanada"
  ]
  node [
    id 425
    label "Cypr"
  ]
  node [
    id 426
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 427
    label "Ekwador"
  ]
  node [
    id 428
    label "Mo&#322;dawia"
  ]
  node [
    id 429
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 430
    label "W&#322;ochy"
  ]
  node [
    id 431
    label "Wyspy_Salomona"
  ]
  node [
    id 432
    label "&#321;otwa"
  ]
  node [
    id 433
    label "D&#380;ibuti"
  ]
  node [
    id 434
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 435
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 436
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 437
    label "Portugalia"
  ]
  node [
    id 438
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 439
    label "Maroko"
  ]
  node [
    id 440
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 441
    label "Francja"
  ]
  node [
    id 442
    label "Botswana"
  ]
  node [
    id 443
    label "Dominika"
  ]
  node [
    id 444
    label "Paragwaj"
  ]
  node [
    id 445
    label "Tad&#380;ykistan"
  ]
  node [
    id 446
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 447
    label "Haiti"
  ]
  node [
    id 448
    label "Khitai"
  ]
  node [
    id 449
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 450
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 451
    label "parlamentarzysta"
  ]
  node [
    id 452
    label "Pi&#322;sudski"
  ]
  node [
    id 453
    label "oficer"
  ]
  node [
    id 454
    label "podoficer"
  ]
  node [
    id 455
    label "mundurowy"
  ]
  node [
    id 456
    label "podchor&#261;&#380;y"
  ]
  node [
    id 457
    label "parlament"
  ]
  node [
    id 458
    label "mandatariusz"
  ]
  node [
    id 459
    label "polityk"
  ]
  node [
    id 460
    label "grupa_bilateralna"
  ]
  node [
    id 461
    label "oficja&#322;"
  ]
  node [
    id 462
    label "notabl"
  ]
  node [
    id 463
    label "kasztanka"
  ]
  node [
    id 464
    label "Komendant"
  ]
  node [
    id 465
    label "znaczny"
  ]
  node [
    id 466
    label "niepo&#347;ledni"
  ]
  node [
    id 467
    label "szczytnie"
  ]
  node [
    id 468
    label "wysoko"
  ]
  node [
    id 469
    label "warto&#347;ciowy"
  ]
  node [
    id 470
    label "wysoce"
  ]
  node [
    id 471
    label "uprzywilejowany"
  ]
  node [
    id 472
    label "wznios&#322;y"
  ]
  node [
    id 473
    label "chwalebny"
  ]
  node [
    id 474
    label "z_wysoka"
  ]
  node [
    id 475
    label "daleki"
  ]
  node [
    id 476
    label "wyrafinowany"
  ]
  node [
    id 477
    label "du&#380;o"
  ]
  node [
    id 478
    label "niema&#322;o"
  ]
  node [
    id 479
    label "prawdziwy"
  ]
  node [
    id 480
    label "rozwini&#281;ty"
  ]
  node [
    id 481
    label "dorodny"
  ]
  node [
    id 482
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 483
    label "szczeg&#243;lny"
  ]
  node [
    id 484
    label "lekki"
  ]
  node [
    id 485
    label "zauwa&#380;alny"
  ]
  node [
    id 486
    label "znacznie"
  ]
  node [
    id 487
    label "niez&#322;y"
  ]
  node [
    id 488
    label "wyj&#261;tkowy"
  ]
  node [
    id 489
    label "niepo&#347;lednio"
  ]
  node [
    id 490
    label "pochwalny"
  ]
  node [
    id 491
    label "powa&#380;ny"
  ]
  node [
    id 492
    label "chwalebnie"
  ]
  node [
    id 493
    label "szlachetny"
  ]
  node [
    id 494
    label "wspania&#322;y"
  ]
  node [
    id 495
    label "podnios&#322;y"
  ]
  node [
    id 496
    label "wznio&#347;le"
  ]
  node [
    id 497
    label "oderwany"
  ]
  node [
    id 498
    label "pi&#281;kny"
  ]
  node [
    id 499
    label "warto&#347;ciowo"
  ]
  node [
    id 500
    label "rewaluowanie"
  ]
  node [
    id 501
    label "drogi"
  ]
  node [
    id 502
    label "dobry"
  ]
  node [
    id 503
    label "u&#380;yteczny"
  ]
  node [
    id 504
    label "zrewaluowanie"
  ]
  node [
    id 505
    label "wyrafinowanie"
  ]
  node [
    id 506
    label "obyty"
  ]
  node [
    id 507
    label "wykwintny"
  ]
  node [
    id 508
    label "wymy&#347;lny"
  ]
  node [
    id 509
    label "przysz&#322;y"
  ]
  node [
    id 510
    label "odlegle"
  ]
  node [
    id 511
    label "nieobecny"
  ]
  node [
    id 512
    label "zwi&#261;zany"
  ]
  node [
    id 513
    label "odleg&#322;y"
  ]
  node [
    id 514
    label "dawny"
  ]
  node [
    id 515
    label "ogl&#281;dny"
  ]
  node [
    id 516
    label "obcy"
  ]
  node [
    id 517
    label "oddalony"
  ]
  node [
    id 518
    label "daleko"
  ]
  node [
    id 519
    label "g&#322;&#281;boki"
  ]
  node [
    id 520
    label "r&#243;&#380;ny"
  ]
  node [
    id 521
    label "d&#322;ugi"
  ]
  node [
    id 522
    label "s&#322;aby"
  ]
  node [
    id 523
    label "g&#243;rno"
  ]
  node [
    id 524
    label "szczytny"
  ]
  node [
    id 525
    label "intensywnie"
  ]
  node [
    id 526
    label "niezmiernie"
  ]
  node [
    id 527
    label "urz&#261;d"
  ]
  node [
    id 528
    label "NIK"
  ]
  node [
    id 529
    label "zwi&#261;zek"
  ]
  node [
    id 530
    label "pok&#243;j"
  ]
  node [
    id 531
    label "pomieszczenie"
  ]
  node [
    id 532
    label "organ"
  ]
  node [
    id 533
    label "uk&#322;ad"
  ]
  node [
    id 534
    label "preliminarium_pokojowe"
  ]
  node [
    id 535
    label "spok&#243;j"
  ]
  node [
    id 536
    label "pacyfista"
  ]
  node [
    id 537
    label "mir"
  ]
  node [
    id 538
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 539
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 540
    label "Komitet_Region&#243;w"
  ]
  node [
    id 541
    label "struktura_anatomiczna"
  ]
  node [
    id 542
    label "organogeneza"
  ]
  node [
    id 543
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 544
    label "tw&#243;r"
  ]
  node [
    id 545
    label "tkanka"
  ]
  node [
    id 546
    label "stomia"
  ]
  node [
    id 547
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 548
    label "budowa"
  ]
  node [
    id 549
    label "dekortykacja"
  ]
  node [
    id 550
    label "okolica"
  ]
  node [
    id 551
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 552
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 553
    label "Izba_Konsyliarska"
  ]
  node [
    id 554
    label "zesp&#243;&#322;"
  ]
  node [
    id 555
    label "jednostka_organizacyjna"
  ]
  node [
    id 556
    label "zwi&#261;zanie"
  ]
  node [
    id 557
    label "odwadnianie"
  ]
  node [
    id 558
    label "azeotrop"
  ]
  node [
    id 559
    label "odwodni&#263;"
  ]
  node [
    id 560
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 561
    label "lokant"
  ]
  node [
    id 562
    label "marriage"
  ]
  node [
    id 563
    label "bratnia_dusza"
  ]
  node [
    id 564
    label "zwi&#261;za&#263;"
  ]
  node [
    id 565
    label "koligacja"
  ]
  node [
    id 566
    label "odwodnienie"
  ]
  node [
    id 567
    label "marketing_afiliacyjny"
  ]
  node [
    id 568
    label "substancja_chemiczna"
  ]
  node [
    id 569
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 570
    label "wi&#261;zanie"
  ]
  node [
    id 571
    label "powi&#261;zanie"
  ]
  node [
    id 572
    label "odwadnia&#263;"
  ]
  node [
    id 573
    label "bearing"
  ]
  node [
    id 574
    label "konstytucja"
  ]
  node [
    id 575
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 576
    label "siedziba"
  ]
  node [
    id 577
    label "dzia&#322;"
  ]
  node [
    id 578
    label "mianowaniec"
  ]
  node [
    id 579
    label "okienko"
  ]
  node [
    id 580
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 581
    label "position"
  ]
  node [
    id 582
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 583
    label "stanowisko"
  ]
  node [
    id 584
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 585
    label "instytucja"
  ]
  node [
    id 586
    label "zakamarek"
  ]
  node [
    id 587
    label "amfilada"
  ]
  node [
    id 588
    label "sklepienie"
  ]
  node [
    id 589
    label "apartment"
  ]
  node [
    id 590
    label "udost&#281;pnienie"
  ]
  node [
    id 591
    label "front"
  ]
  node [
    id 592
    label "umieszczenie"
  ]
  node [
    id 593
    label "miejsce"
  ]
  node [
    id 594
    label "sufit"
  ]
  node [
    id 595
    label "pod&#322;oga"
  ]
  node [
    id 596
    label "plankton_polityczny"
  ]
  node [
    id 597
    label "europarlament"
  ]
  node [
    id 598
    label "ustawodawca"
  ]
  node [
    id 599
    label "rz&#261;d"
  ]
  node [
    id 600
    label "Sto&#322;ypin"
  ]
  node [
    id 601
    label "Goebbels"
  ]
  node [
    id 602
    label "szpaler"
  ]
  node [
    id 603
    label "number"
  ]
  node [
    id 604
    label "Londyn"
  ]
  node [
    id 605
    label "przybli&#380;enie"
  ]
  node [
    id 606
    label "premier"
  ]
  node [
    id 607
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 608
    label "tract"
  ]
  node [
    id 609
    label "uporz&#261;dkowanie"
  ]
  node [
    id 610
    label "egzekutywa"
  ]
  node [
    id 611
    label "Konsulat"
  ]
  node [
    id 612
    label "gabinet_cieni"
  ]
  node [
    id 613
    label "lon&#380;a"
  ]
  node [
    id 614
    label "gromada"
  ]
  node [
    id 615
    label "jednostka_systematyczna"
  ]
  node [
    id 616
    label "kategoria"
  ]
  node [
    id 617
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 618
    label "zrobi&#263;"
  ]
  node [
    id 619
    label "odj&#261;&#263;"
  ]
  node [
    id 620
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 621
    label "introduce"
  ]
  node [
    id 622
    label "do"
  ]
  node [
    id 623
    label "post&#261;pi&#263;"
  ]
  node [
    id 624
    label "cause"
  ]
  node [
    id 625
    label "begin"
  ]
  node [
    id 626
    label "zorganizowa&#263;"
  ]
  node [
    id 627
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 628
    label "wydali&#263;"
  ]
  node [
    id 629
    label "make"
  ]
  node [
    id 630
    label "wystylizowa&#263;"
  ]
  node [
    id 631
    label "appoint"
  ]
  node [
    id 632
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 633
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 634
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 635
    label "przerobi&#263;"
  ]
  node [
    id 636
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 637
    label "nabra&#263;"
  ]
  node [
    id 638
    label "policzy&#263;"
  ]
  node [
    id 639
    label "zabra&#263;"
  ]
  node [
    id 640
    label "withdraw"
  ]
  node [
    id 641
    label "reduce"
  ]
  node [
    id 642
    label "separate"
  ]
  node [
    id 643
    label "oddali&#263;"
  ]
  node [
    id 644
    label "oddzieli&#263;"
  ]
  node [
    id 645
    label "see"
  ]
  node [
    id 646
    label "act"
  ]
  node [
    id 647
    label "advance"
  ]
  node [
    id 648
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 649
    label "ut"
  ]
  node [
    id 650
    label "C"
  ]
  node [
    id 651
    label "d&#378;wi&#281;k"
  ]
  node [
    id 652
    label "his"
  ]
  node [
    id 653
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 654
    label "zobo"
  ]
  node [
    id 655
    label "byd&#322;o"
  ]
  node [
    id 656
    label "dzo"
  ]
  node [
    id 657
    label "yakalo"
  ]
  node [
    id 658
    label "zbi&#243;r"
  ]
  node [
    id 659
    label "kr&#281;torogie"
  ]
  node [
    id 660
    label "livestock"
  ]
  node [
    id 661
    label "posp&#243;lstwo"
  ]
  node [
    id 662
    label "kraal"
  ]
  node [
    id 663
    label "czochrad&#322;o"
  ]
  node [
    id 664
    label "prze&#380;uwacz"
  ]
  node [
    id 665
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 666
    label "bizon"
  ]
  node [
    id 667
    label "zebu"
  ]
  node [
    id 668
    label "byd&#322;o_domowe"
  ]
  node [
    id 669
    label "Rzym_Zachodni"
  ]
  node [
    id 670
    label "Rzym_Wschodni"
  ]
  node [
    id 671
    label "element"
  ]
  node [
    id 672
    label "ilo&#347;&#263;"
  ]
  node [
    id 673
    label "whole"
  ]
  node [
    id 674
    label "urz&#261;dzenie"
  ]
  node [
    id 675
    label "poj&#281;cie"
  ]
  node [
    id 676
    label "przedmiot"
  ]
  node [
    id 677
    label "materia"
  ]
  node [
    id 678
    label "&#347;rodowisko"
  ]
  node [
    id 679
    label "szkodnik"
  ]
  node [
    id 680
    label "gangsterski"
  ]
  node [
    id 681
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 682
    label "underworld"
  ]
  node [
    id 683
    label "szambo"
  ]
  node [
    id 684
    label "component"
  ]
  node [
    id 685
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 686
    label "r&#243;&#380;niczka"
  ]
  node [
    id 687
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 688
    label "aspo&#322;eczny"
  ]
  node [
    id 689
    label "rozmiar"
  ]
  node [
    id 690
    label "part"
  ]
  node [
    id 691
    label "komora"
  ]
  node [
    id 692
    label "wyrz&#261;dzenie"
  ]
  node [
    id 693
    label "kom&#243;rka"
  ]
  node [
    id 694
    label "impulsator"
  ]
  node [
    id 695
    label "przygotowanie"
  ]
  node [
    id 696
    label "furnishing"
  ]
  node [
    id 697
    label "zabezpieczenie"
  ]
  node [
    id 698
    label "sprz&#281;t"
  ]
  node [
    id 699
    label "aparatura"
  ]
  node [
    id 700
    label "ig&#322;a"
  ]
  node [
    id 701
    label "wirnik"
  ]
  node [
    id 702
    label "zablokowanie"
  ]
  node [
    id 703
    label "blokowanie"
  ]
  node [
    id 704
    label "j&#281;zyk"
  ]
  node [
    id 705
    label "czynno&#347;&#263;"
  ]
  node [
    id 706
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 707
    label "system_energetyczny"
  ]
  node [
    id 708
    label "narz&#281;dzie"
  ]
  node [
    id 709
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 710
    label "set"
  ]
  node [
    id 711
    label "zrobienie"
  ]
  node [
    id 712
    label "zagospodarowanie"
  ]
  node [
    id 713
    label "mechanizm"
  ]
  node [
    id 714
    label "m&#243;wca"
  ]
  node [
    id 715
    label "poprzednik"
  ]
  node [
    id 716
    label "rozm&#243;wca"
  ]
  node [
    id 717
    label "rzecz"
  ]
  node [
    id 718
    label "zdanie"
  ]
  node [
    id 719
    label "okres"
  ]
  node [
    id 720
    label "argument"
  ]
  node [
    id 721
    label "implikacja"
  ]
  node [
    id 722
    label "partner"
  ]
  node [
    id 723
    label "Katon"
  ]
  node [
    id 724
    label "dupny"
  ]
  node [
    id 725
    label "wybitny"
  ]
  node [
    id 726
    label "nieprzeci&#281;tny"
  ]
  node [
    id 727
    label "&#347;wietny"
  ]
  node [
    id 728
    label "celny"
  ]
  node [
    id 729
    label "niespotykany"
  ]
  node [
    id 730
    label "ponadprzeci&#281;tny"
  ]
  node [
    id 731
    label "imponuj&#261;cy"
  ]
  node [
    id 732
    label "wybitnie"
  ]
  node [
    id 733
    label "wydatny"
  ]
  node [
    id 734
    label "nieprzeci&#281;tnie"
  ]
  node [
    id 735
    label "wyj&#261;tkowo"
  ]
  node [
    id 736
    label "zgodny"
  ]
  node [
    id 737
    label "prawdziwie"
  ]
  node [
    id 738
    label "podobny"
  ]
  node [
    id 739
    label "szczery"
  ]
  node [
    id 740
    label "naprawd&#281;"
  ]
  node [
    id 741
    label "naturalny"
  ]
  node [
    id 742
    label "&#380;ywny"
  ]
  node [
    id 743
    label "realnie"
  ]
  node [
    id 744
    label "silny"
  ]
  node [
    id 745
    label "wa&#380;nie"
  ]
  node [
    id 746
    label "eksponowany"
  ]
  node [
    id 747
    label "wynios&#322;y"
  ]
  node [
    id 748
    label "istotnie"
  ]
  node [
    id 749
    label "dono&#347;ny"
  ]
  node [
    id 750
    label "z&#322;y"
  ]
  node [
    id 751
    label "do_dupy"
  ]
  node [
    id 752
    label "notyfikowanie"
  ]
  node [
    id 753
    label "notyfikowa&#263;"
  ]
  node [
    id 754
    label "denial"
  ]
  node [
    id 755
    label "wypowied&#378;"
  ]
  node [
    id 756
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 757
    label "z&#322;o&#380;enie"
  ]
  node [
    id 758
    label "fold"
  ]
  node [
    id 759
    label "leksem"
  ]
  node [
    id 760
    label "przekazanie"
  ]
  node [
    id 761
    label "blend"
  ]
  node [
    id 762
    label "lodging"
  ]
  node [
    id 763
    label "derywat_z&#322;o&#380;ony"
  ]
  node [
    id 764
    label "zestawienie"
  ]
  node [
    id 765
    label "zgi&#281;cie"
  ]
  node [
    id 766
    label "pay"
  ]
  node [
    id 767
    label "powiedzenie"
  ]
  node [
    id 768
    label "removal"
  ]
  node [
    id 769
    label "opracowanie"
  ]
  node [
    id 770
    label "stage_set"
  ]
  node [
    id 771
    label "zgromadzenie"
  ]
  node [
    id 772
    label "danie"
  ]
  node [
    id 773
    label "przy&#322;o&#380;enie"
  ]
  node [
    id 774
    label "posk&#322;adanie"
  ]
  node [
    id 775
    label "parafrazowanie"
  ]
  node [
    id 776
    label "komunikat"
  ]
  node [
    id 777
    label "stylizacja"
  ]
  node [
    id 778
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 779
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 780
    label "strawestowanie"
  ]
  node [
    id 781
    label "sparafrazowanie"
  ]
  node [
    id 782
    label "sformu&#322;owanie"
  ]
  node [
    id 783
    label "pos&#322;uchanie"
  ]
  node [
    id 784
    label "strawestowa&#263;"
  ]
  node [
    id 785
    label "parafrazowa&#263;"
  ]
  node [
    id 786
    label "delimitacja"
  ]
  node [
    id 787
    label "rezultat"
  ]
  node [
    id 788
    label "ozdobnik"
  ]
  node [
    id 789
    label "trawestowa&#263;"
  ]
  node [
    id 790
    label "s&#261;d"
  ]
  node [
    id 791
    label "sparafrazowa&#263;"
  ]
  node [
    id 792
    label "trawestowanie"
  ]
  node [
    id 793
    label "odm&#243;wienie"
  ]
  node [
    id 794
    label "donoszenie"
  ]
  node [
    id 795
    label "poinformowanie"
  ]
  node [
    id 796
    label "dyplomacja"
  ]
  node [
    id 797
    label "doniesienie"
  ]
  node [
    id 798
    label "informowanie"
  ]
  node [
    id 799
    label "czek"
  ]
  node [
    id 800
    label "donie&#347;&#263;"
  ]
  node [
    id 801
    label "informowa&#263;"
  ]
  node [
    id 802
    label "poinformowa&#263;"
  ]
  node [
    id 803
    label "donosi&#263;"
  ]
  node [
    id 804
    label "advise"
  ]
  node [
    id 805
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 806
    label "cz&#322;onek"
  ]
  node [
    id 807
    label "substytuowanie"
  ]
  node [
    id 808
    label "zast&#281;pca"
  ]
  node [
    id 809
    label "substytuowa&#263;"
  ]
  node [
    id 810
    label "przyk&#322;ad"
  ]
  node [
    id 811
    label "wej&#347;cie"
  ]
  node [
    id 812
    label "cia&#322;o"
  ]
  node [
    id 813
    label "shaft"
  ]
  node [
    id 814
    label "ptaszek"
  ]
  node [
    id 815
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 816
    label "przyrodzenie"
  ]
  node [
    id 817
    label "fiut"
  ]
  node [
    id 818
    label "element_anatomiczny"
  ]
  node [
    id 819
    label "wchodzenie"
  ]
  node [
    id 820
    label "podstawienie"
  ]
  node [
    id 821
    label "wskazanie"
  ]
  node [
    id 822
    label "podstawianie"
  ]
  node [
    id 823
    label "wskazywanie"
  ]
  node [
    id 824
    label "pe&#322;nomocnik"
  ]
  node [
    id 825
    label "protezowa&#263;"
  ]
  node [
    id 826
    label "podstawi&#263;"
  ]
  node [
    id 827
    label "podstawia&#263;"
  ]
  node [
    id 828
    label "zast&#281;powa&#263;"
  ]
  node [
    id 829
    label "wskaza&#263;"
  ]
  node [
    id 830
    label "zast&#261;pi&#263;"
  ]
  node [
    id 831
    label "wskazywa&#263;"
  ]
  node [
    id 832
    label "czyn"
  ]
  node [
    id 833
    label "ilustracja"
  ]
  node [
    id 834
    label "fakt"
  ]
  node [
    id 835
    label "okre&#347;lony"
  ]
  node [
    id 836
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 837
    label "wiadomy"
  ]
  node [
    id 838
    label "skuteczny"
  ]
  node [
    id 839
    label "ca&#322;y"
  ]
  node [
    id 840
    label "czw&#243;rka"
  ]
  node [
    id 841
    label "spokojny"
  ]
  node [
    id 842
    label "pos&#322;uszny"
  ]
  node [
    id 843
    label "korzystny"
  ]
  node [
    id 844
    label "pozytywny"
  ]
  node [
    id 845
    label "moralny"
  ]
  node [
    id 846
    label "pomy&#347;lny"
  ]
  node [
    id 847
    label "powitanie"
  ]
  node [
    id 848
    label "grzeczny"
  ]
  node [
    id 849
    label "&#347;mieszny"
  ]
  node [
    id 850
    label "odpowiedni"
  ]
  node [
    id 851
    label "dobroczynny"
  ]
  node [
    id 852
    label "mi&#322;y"
  ]
  node [
    id 853
    label "niedost&#281;pny"
  ]
  node [
    id 854
    label "pot&#281;&#380;ny"
  ]
  node [
    id 855
    label "wynio&#347;le"
  ]
  node [
    id 856
    label "dumny"
  ]
  node [
    id 857
    label "zajebisty"
  ]
  node [
    id 858
    label "wytrzyma&#322;y"
  ]
  node [
    id 859
    label "mocno"
  ]
  node [
    id 860
    label "niepodwa&#380;alny"
  ]
  node [
    id 861
    label "&#380;ywotny"
  ]
  node [
    id 862
    label "konkretny"
  ]
  node [
    id 863
    label "zdrowy"
  ]
  node [
    id 864
    label "meflochina"
  ]
  node [
    id 865
    label "intensywny"
  ]
  node [
    id 866
    label "krzepienie"
  ]
  node [
    id 867
    label "mocny"
  ]
  node [
    id 868
    label "przekonuj&#261;cy"
  ]
  node [
    id 869
    label "zdecydowany"
  ]
  node [
    id 870
    label "pokrzepienie"
  ]
  node [
    id 871
    label "silnie"
  ]
  node [
    id 872
    label "importantly"
  ]
  node [
    id 873
    label "istotny"
  ]
  node [
    id 874
    label "gromowy"
  ]
  node [
    id 875
    label "g&#322;o&#347;ny"
  ]
  node [
    id 876
    label "dono&#347;nie"
  ]
  node [
    id 877
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 878
    label "propozycja"
  ]
  node [
    id 879
    label "plan"
  ]
  node [
    id 880
    label "relacja"
  ]
  node [
    id 881
    label "cecha"
  ]
  node [
    id 882
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 883
    label "independence"
  ]
  node [
    id 884
    label "reprezentacja"
  ]
  node [
    id 885
    label "przestrze&#324;"
  ]
  node [
    id 886
    label "intencja"
  ]
  node [
    id 887
    label "perspektywa"
  ]
  node [
    id 888
    label "model"
  ]
  node [
    id 889
    label "miejsce_pracy"
  ]
  node [
    id 890
    label "device"
  ]
  node [
    id 891
    label "obraz"
  ]
  node [
    id 892
    label "rysunek"
  ]
  node [
    id 893
    label "agreement"
  ]
  node [
    id 894
    label "dekoracja"
  ]
  node [
    id 895
    label "wytw&#243;r"
  ]
  node [
    id 896
    label "pomys&#322;"
  ]
  node [
    id 897
    label "proposal"
  ]
  node [
    id 898
    label "osobnie"
  ]
  node [
    id 899
    label "specially"
  ]
  node [
    id 900
    label "niezwykle"
  ]
  node [
    id 901
    label "niestandardowo"
  ]
  node [
    id 902
    label "osobno"
  ]
  node [
    id 903
    label "r&#243;&#380;nie"
  ]
  node [
    id 904
    label "ma&#347;lak_pstry"
  ]
  node [
    id 905
    label "robi&#263;"
  ]
  node [
    id 906
    label "rozpatrywa&#263;"
  ]
  node [
    id 907
    label "argue"
  ]
  node [
    id 908
    label "take_care"
  ]
  node [
    id 909
    label "zamierza&#263;"
  ]
  node [
    id 910
    label "deliver"
  ]
  node [
    id 911
    label "os&#261;dza&#263;"
  ]
  node [
    id 912
    label "troska&#263;_si&#281;"
  ]
  node [
    id 913
    label "hold"
  ]
  node [
    id 914
    label "strike"
  ]
  node [
    id 915
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 916
    label "s&#261;dzi&#263;"
  ]
  node [
    id 917
    label "powodowa&#263;"
  ]
  node [
    id 918
    label "znajdowa&#263;"
  ]
  node [
    id 919
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 920
    label "przeprowadza&#263;"
  ]
  node [
    id 921
    label "consider"
  ]
  node [
    id 922
    label "volunteer"
  ]
  node [
    id 923
    label "oszukiwa&#263;"
  ]
  node [
    id 924
    label "tentegowa&#263;"
  ]
  node [
    id 925
    label "urz&#261;dza&#263;"
  ]
  node [
    id 926
    label "praca"
  ]
  node [
    id 927
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 928
    label "czyni&#263;"
  ]
  node [
    id 929
    label "work"
  ]
  node [
    id 930
    label "przerabia&#263;"
  ]
  node [
    id 931
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 932
    label "give"
  ]
  node [
    id 933
    label "post&#281;powa&#263;"
  ]
  node [
    id 934
    label "peddle"
  ]
  node [
    id 935
    label "organizowa&#263;"
  ]
  node [
    id 936
    label "falowa&#263;"
  ]
  node [
    id 937
    label "stylizowa&#263;"
  ]
  node [
    id 938
    label "wydala&#263;"
  ]
  node [
    id 939
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 940
    label "ukazywa&#263;"
  ]
  node [
    id 941
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 942
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 943
    label "pozytywnie"
  ]
  node [
    id 944
    label "korzystnie"
  ]
  node [
    id 945
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 946
    label "pomy&#347;lnie"
  ]
  node [
    id 947
    label "lepiej"
  ]
  node [
    id 948
    label "moralnie"
  ]
  node [
    id 949
    label "odpowiednio"
  ]
  node [
    id 950
    label "skutecznie"
  ]
  node [
    id 951
    label "dobroczynnie"
  ]
  node [
    id 952
    label "stosowny"
  ]
  node [
    id 953
    label "nale&#380;nie"
  ]
  node [
    id 954
    label "nale&#380;ycie"
  ]
  node [
    id 955
    label "charakterystycznie"
  ]
  node [
    id 956
    label "auspiciously"
  ]
  node [
    id 957
    label "etyczny"
  ]
  node [
    id 958
    label "wiela"
  ]
  node [
    id 959
    label "beneficially"
  ]
  node [
    id 960
    label "utylitarnie"
  ]
  node [
    id 961
    label "ontologicznie"
  ]
  node [
    id 962
    label "dodatni"
  ]
  node [
    id 963
    label "przyjemnie"
  ]
  node [
    id 964
    label "wiersz"
  ]
  node [
    id 965
    label "philanthropically"
  ]
  node [
    id 966
    label "spo&#322;ecznie"
  ]
  node [
    id 967
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 968
    label "stan"
  ]
  node [
    id 969
    label "stand"
  ]
  node [
    id 970
    label "trwa&#263;"
  ]
  node [
    id 971
    label "equal"
  ]
  node [
    id 972
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 973
    label "chodzi&#263;"
  ]
  node [
    id 974
    label "uczestniczy&#263;"
  ]
  node [
    id 975
    label "obecno&#347;&#263;"
  ]
  node [
    id 976
    label "si&#281;ga&#263;"
  ]
  node [
    id 977
    label "mie&#263;_miejsce"
  ]
  node [
    id 978
    label "participate"
  ]
  node [
    id 979
    label "adhere"
  ]
  node [
    id 980
    label "pozostawa&#263;"
  ]
  node [
    id 981
    label "zostawa&#263;"
  ]
  node [
    id 982
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 983
    label "istnie&#263;"
  ]
  node [
    id 984
    label "compass"
  ]
  node [
    id 985
    label "exsert"
  ]
  node [
    id 986
    label "get"
  ]
  node [
    id 987
    label "u&#380;ywa&#263;"
  ]
  node [
    id 988
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 989
    label "osi&#261;ga&#263;"
  ]
  node [
    id 990
    label "korzysta&#263;"
  ]
  node [
    id 991
    label "appreciation"
  ]
  node [
    id 992
    label "dociera&#263;"
  ]
  node [
    id 993
    label "mierzy&#263;"
  ]
  node [
    id 994
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 995
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 996
    label "being"
  ]
  node [
    id 997
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 998
    label "proceed"
  ]
  node [
    id 999
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1000
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1001
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1002
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1003
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1004
    label "str&#243;j"
  ]
  node [
    id 1005
    label "krok"
  ]
  node [
    id 1006
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1007
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1008
    label "przebiega&#263;"
  ]
  node [
    id 1009
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1010
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1011
    label "continue"
  ]
  node [
    id 1012
    label "carry"
  ]
  node [
    id 1013
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1014
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1015
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1016
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1017
    label "bangla&#263;"
  ]
  node [
    id 1018
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1019
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1020
    label "bywa&#263;"
  ]
  node [
    id 1021
    label "tryb"
  ]
  node [
    id 1022
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1023
    label "dziama&#263;"
  ]
  node [
    id 1024
    label "run"
  ]
  node [
    id 1025
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1026
    label "Arakan"
  ]
  node [
    id 1027
    label "Teksas"
  ]
  node [
    id 1028
    label "Georgia"
  ]
  node [
    id 1029
    label "Maryland"
  ]
  node [
    id 1030
    label "warstwa"
  ]
  node [
    id 1031
    label "Luizjana"
  ]
  node [
    id 1032
    label "Massachusetts"
  ]
  node [
    id 1033
    label "Michigan"
  ]
  node [
    id 1034
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1035
    label "samopoczucie"
  ]
  node [
    id 1036
    label "Floryda"
  ]
  node [
    id 1037
    label "Ohio"
  ]
  node [
    id 1038
    label "Alaska"
  ]
  node [
    id 1039
    label "Nowy_Meksyk"
  ]
  node [
    id 1040
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1041
    label "wci&#281;cie"
  ]
  node [
    id 1042
    label "Kansas"
  ]
  node [
    id 1043
    label "Alabama"
  ]
  node [
    id 1044
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1045
    label "Kalifornia"
  ]
  node [
    id 1046
    label "Wirginia"
  ]
  node [
    id 1047
    label "Nowy_York"
  ]
  node [
    id 1048
    label "Waszyngton"
  ]
  node [
    id 1049
    label "Pensylwania"
  ]
  node [
    id 1050
    label "wektor"
  ]
  node [
    id 1051
    label "Hawaje"
  ]
  node [
    id 1052
    label "state"
  ]
  node [
    id 1053
    label "poziom"
  ]
  node [
    id 1054
    label "jednostka_administracyjna"
  ]
  node [
    id 1055
    label "Illinois"
  ]
  node [
    id 1056
    label "Oklahoma"
  ]
  node [
    id 1057
    label "Jukatan"
  ]
  node [
    id 1058
    label "Arizona"
  ]
  node [
    id 1059
    label "Oregon"
  ]
  node [
    id 1060
    label "shape"
  ]
  node [
    id 1061
    label "Goa"
  ]
  node [
    id 1062
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 1063
    label "cz&#322;owiekowate"
  ]
  node [
    id 1064
    label "konsument"
  ]
  node [
    id 1065
    label "istota_&#380;ywa"
  ]
  node [
    id 1066
    label "pracownik"
  ]
  node [
    id 1067
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 1068
    label "Gargantua"
  ]
  node [
    id 1069
    label "Chocho&#322;"
  ]
  node [
    id 1070
    label "Hamlet"
  ]
  node [
    id 1071
    label "Wallenrod"
  ]
  node [
    id 1072
    label "Quasimodo"
  ]
  node [
    id 1073
    label "parali&#380;owa&#263;"
  ]
  node [
    id 1074
    label "Plastu&#347;"
  ]
  node [
    id 1075
    label "kategoria_gramatyczna"
  ]
  node [
    id 1076
    label "istota"
  ]
  node [
    id 1077
    label "Casanova"
  ]
  node [
    id 1078
    label "Szwejk"
  ]
  node [
    id 1079
    label "Don_Juan"
  ]
  node [
    id 1080
    label "Edyp"
  ]
  node [
    id 1081
    label "koniugacja"
  ]
  node [
    id 1082
    label "Werter"
  ]
  node [
    id 1083
    label "person"
  ]
  node [
    id 1084
    label "Harry_Potter"
  ]
  node [
    id 1085
    label "Sherlock_Holmes"
  ]
  node [
    id 1086
    label "Dwukwiat"
  ]
  node [
    id 1087
    label "Winnetou"
  ]
  node [
    id 1088
    label "Don_Kiszot"
  ]
  node [
    id 1089
    label "Herkules_Poirot"
  ]
  node [
    id 1090
    label "Faust"
  ]
  node [
    id 1091
    label "Zgredek"
  ]
  node [
    id 1092
    label "Dulcynea"
  ]
  node [
    id 1093
    label "jajko"
  ]
  node [
    id 1094
    label "wapniaki"
  ]
  node [
    id 1095
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 1096
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 1097
    label "zawodnik"
  ]
  node [
    id 1098
    label "starzec"
  ]
  node [
    id 1099
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 1100
    label "komendancja"
  ]
  node [
    id 1101
    label "feuda&#322;"
  ]
  node [
    id 1102
    label "debilitation"
  ]
  node [
    id 1103
    label "powodowanie"
  ]
  node [
    id 1104
    label "kondycja_fizyczna"
  ]
  node [
    id 1105
    label "s&#322;abszy"
  ]
  node [
    id 1106
    label "de-escalation"
  ]
  node [
    id 1107
    label "zmniejszanie"
  ]
  node [
    id 1108
    label "os&#322;abi&#263;"
  ]
  node [
    id 1109
    label "os&#322;abienie"
  ]
  node [
    id 1110
    label "zdrowie"
  ]
  node [
    id 1111
    label "pogarszanie"
  ]
  node [
    id 1112
    label "bate"
  ]
  node [
    id 1113
    label "suppress"
  ]
  node [
    id 1114
    label "zmniejsza&#263;"
  ]
  node [
    id 1115
    label "g&#322;oska"
  ]
  node [
    id 1116
    label "acquisition"
  ]
  node [
    id 1117
    label "assimilation"
  ]
  node [
    id 1118
    label "czerpanie"
  ]
  node [
    id 1119
    label "upodabnianie"
  ]
  node [
    id 1120
    label "organizm"
  ]
  node [
    id 1121
    label "fonetyka"
  ]
  node [
    id 1122
    label "pobieranie"
  ]
  node [
    id 1123
    label "kultura"
  ]
  node [
    id 1124
    label "asymilowanie_si&#281;"
  ]
  node [
    id 1125
    label "zmienianie"
  ]
  node [
    id 1126
    label "absorption"
  ]
  node [
    id 1127
    label "dostosowywa&#263;"
  ]
  node [
    id 1128
    label "przej&#261;&#263;"
  ]
  node [
    id 1129
    label "pobra&#263;"
  ]
  node [
    id 1130
    label "assimilate"
  ]
  node [
    id 1131
    label "przejmowa&#263;"
  ]
  node [
    id 1132
    label "upodobni&#263;"
  ]
  node [
    id 1133
    label "pobiera&#263;"
  ]
  node [
    id 1134
    label "upodabnia&#263;"
  ]
  node [
    id 1135
    label "dostosowa&#263;"
  ]
  node [
    id 1136
    label "wytrzyma&#263;"
  ]
  node [
    id 1137
    label "trim"
  ]
  node [
    id 1138
    label "Osjan"
  ]
  node [
    id 1139
    label "formacja"
  ]
  node [
    id 1140
    label "point"
  ]
  node [
    id 1141
    label "kto&#347;"
  ]
  node [
    id 1142
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1143
    label "pozosta&#263;"
  ]
  node [
    id 1144
    label "poby&#263;"
  ]
  node [
    id 1145
    label "przedstawienie"
  ]
  node [
    id 1146
    label "Aspazja"
  ]
  node [
    id 1147
    label "go&#347;&#263;"
  ]
  node [
    id 1148
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1149
    label "charakterystyka"
  ]
  node [
    id 1150
    label "kompleksja"
  ]
  node [
    id 1151
    label "wygl&#261;d"
  ]
  node [
    id 1152
    label "punkt_widzenia"
  ]
  node [
    id 1153
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1154
    label "zaistnie&#263;"
  ]
  node [
    id 1155
    label "spos&#243;b"
  ]
  node [
    id 1156
    label "projekt"
  ]
  node [
    id 1157
    label "mildew"
  ]
  node [
    id 1158
    label "ideal"
  ]
  node [
    id 1159
    label "zapis"
  ]
  node [
    id 1160
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 1161
    label "typ"
  ]
  node [
    id 1162
    label "ruch"
  ]
  node [
    id 1163
    label "dekal"
  ]
  node [
    id 1164
    label "figure"
  ]
  node [
    id 1165
    label "motyw"
  ]
  node [
    id 1166
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 1167
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1168
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 1169
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 1170
    label "umys&#322;"
  ]
  node [
    id 1171
    label "kierowa&#263;"
  ]
  node [
    id 1172
    label "obiekt"
  ]
  node [
    id 1173
    label "sztuka"
  ]
  node [
    id 1174
    label "czaszka"
  ]
  node [
    id 1175
    label "g&#243;ra"
  ]
  node [
    id 1176
    label "wiedza"
  ]
  node [
    id 1177
    label "fryzura"
  ]
  node [
    id 1178
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 1179
    label "pryncypa&#322;"
  ]
  node [
    id 1180
    label "ro&#347;lina"
  ]
  node [
    id 1181
    label "ucho"
  ]
  node [
    id 1182
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 1183
    label "alkohol"
  ]
  node [
    id 1184
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 1185
    label "kierownictwo"
  ]
  node [
    id 1186
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1187
    label "makrocefalia"
  ]
  node [
    id 1188
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 1189
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1190
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 1191
    label "dekiel"
  ]
  node [
    id 1192
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 1193
    label "m&#243;zg"
  ]
  node [
    id 1194
    label "&#347;ci&#281;gno"
  ]
  node [
    id 1195
    label "kszta&#322;t"
  ]
  node [
    id 1196
    label "noosfera"
  ]
  node [
    id 1197
    label "dziedzina"
  ]
  node [
    id 1198
    label "hipnotyzowanie"
  ]
  node [
    id 1199
    label "zjawisko"
  ]
  node [
    id 1200
    label "&#347;lad"
  ]
  node [
    id 1201
    label "reakcja_chemiczna"
  ]
  node [
    id 1202
    label "docieranie"
  ]
  node [
    id 1203
    label "lobbysta"
  ]
  node [
    id 1204
    label "natural_process"
  ]
  node [
    id 1205
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1206
    label "allochoria"
  ]
  node [
    id 1207
    label "malarz"
  ]
  node [
    id 1208
    label "artysta"
  ]
  node [
    id 1209
    label "fotograf"
  ]
  node [
    id 1210
    label "obiekt_matematyczny"
  ]
  node [
    id 1211
    label "gestaltyzm"
  ]
  node [
    id 1212
    label "ornamentyka"
  ]
  node [
    id 1213
    label "stylistyka"
  ]
  node [
    id 1214
    label "podzbi&#243;r"
  ]
  node [
    id 1215
    label "styl"
  ]
  node [
    id 1216
    label "antycypacja"
  ]
  node [
    id 1217
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1218
    label "popis"
  ]
  node [
    id 1219
    label "p&#322;aszczyzna"
  ]
  node [
    id 1220
    label "informacja"
  ]
  node [
    id 1221
    label "symetria"
  ]
  node [
    id 1222
    label "lingwistyka_kognitywna"
  ]
  node [
    id 1223
    label "character"
  ]
  node [
    id 1224
    label "rze&#378;ba"
  ]
  node [
    id 1225
    label "bierka_szachowa"
  ]
  node [
    id 1226
    label "karta"
  ]
  node [
    id 1227
    label "nak&#322;adka"
  ]
  node [
    id 1228
    label "jama_gard&#322;owa"
  ]
  node [
    id 1229
    label "podstawa"
  ]
  node [
    id 1230
    label "base"
  ]
  node [
    id 1231
    label "li&#347;&#263;"
  ]
  node [
    id 1232
    label "rezonator"
  ]
  node [
    id 1233
    label "deformowa&#263;"
  ]
  node [
    id 1234
    label "deformowanie"
  ]
  node [
    id 1235
    label "sfera_afektywna"
  ]
  node [
    id 1236
    label "sumienie"
  ]
  node [
    id 1237
    label "entity"
  ]
  node [
    id 1238
    label "psychika"
  ]
  node [
    id 1239
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1240
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1241
    label "charakter"
  ]
  node [
    id 1242
    label "fizjonomia"
  ]
  node [
    id 1243
    label "power"
  ]
  node [
    id 1244
    label "byt"
  ]
  node [
    id 1245
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1246
    label "human_body"
  ]
  node [
    id 1247
    label "podekscytowanie"
  ]
  node [
    id 1248
    label "kompleks"
  ]
  node [
    id 1249
    label "piek&#322;o"
  ]
  node [
    id 1250
    label "oddech"
  ]
  node [
    id 1251
    label "ofiarowywa&#263;"
  ]
  node [
    id 1252
    label "nekromancja"
  ]
  node [
    id 1253
    label "si&#322;a"
  ]
  node [
    id 1254
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1255
    label "zjawa"
  ]
  node [
    id 1256
    label "zapalno&#347;&#263;"
  ]
  node [
    id 1257
    label "ego"
  ]
  node [
    id 1258
    label "ofiarowa&#263;"
  ]
  node [
    id 1259
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1260
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1261
    label "Po&#347;wist"
  ]
  node [
    id 1262
    label "passion"
  ]
  node [
    id 1263
    label "zmar&#322;y"
  ]
  node [
    id 1264
    label "ofiarowanie"
  ]
  node [
    id 1265
    label "ofiarowywanie"
  ]
  node [
    id 1266
    label "T&#281;sknica"
  ]
  node [
    id 1267
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1268
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1269
    label "miniatura"
  ]
  node [
    id 1270
    label "przyroda"
  ]
  node [
    id 1271
    label "odbicie"
  ]
  node [
    id 1272
    label "atom"
  ]
  node [
    id 1273
    label "kosmos"
  ]
  node [
    id 1274
    label "Ziemia"
  ]
  node [
    id 1275
    label "think"
  ]
  node [
    id 1276
    label "zachowywa&#263;"
  ]
  node [
    id 1277
    label "pilnowa&#263;"
  ]
  node [
    id 1278
    label "chowa&#263;"
  ]
  node [
    id 1279
    label "zna&#263;"
  ]
  node [
    id 1280
    label "recall"
  ]
  node [
    id 1281
    label "echo"
  ]
  node [
    id 1282
    label "ukrywa&#263;"
  ]
  node [
    id 1283
    label "hodowa&#263;"
  ]
  node [
    id 1284
    label "report"
  ]
  node [
    id 1285
    label "hide"
  ]
  node [
    id 1286
    label "meliniarz"
  ]
  node [
    id 1287
    label "umieszcza&#263;"
  ]
  node [
    id 1288
    label "przetrzymywa&#263;"
  ]
  node [
    id 1289
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 1290
    label "czu&#263;"
  ]
  node [
    id 1291
    label "train"
  ]
  node [
    id 1292
    label "znosi&#263;"
  ]
  node [
    id 1293
    label "przechowywa&#263;"
  ]
  node [
    id 1294
    label "dieta"
  ]
  node [
    id 1295
    label "zdyscyplinowanie"
  ]
  node [
    id 1296
    label "behave"
  ]
  node [
    id 1297
    label "podtrzymywa&#263;"
  ]
  node [
    id 1298
    label "control"
  ]
  node [
    id 1299
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 1300
    label "tajemnica"
  ]
  node [
    id 1301
    label "post"
  ]
  node [
    id 1302
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 1303
    label "wiedzie&#263;"
  ]
  node [
    id 1304
    label "cognizance"
  ]
  node [
    id 1305
    label "resonance"
  ]
  node [
    id 1306
    label "superego"
  ]
  node [
    id 1307
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1308
    label "znaczenie"
  ]
  node [
    id 1309
    label "wn&#281;trze"
  ]
  node [
    id 1310
    label "hamper"
  ]
  node [
    id 1311
    label "pora&#380;a&#263;"
  ]
  node [
    id 1312
    label "mrozi&#263;"
  ]
  node [
    id 1313
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 1314
    label "spasm"
  ]
  node [
    id 1315
    label "liczba"
  ]
  node [
    id 1316
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 1317
    label "czasownik"
  ]
  node [
    id 1318
    label "coupling"
  ]
  node [
    id 1319
    label "fleksja"
  ]
  node [
    id 1320
    label "orz&#281;sek"
  ]
  node [
    id 1321
    label "Szekspir"
  ]
  node [
    id 1322
    label "Mickiewicz"
  ]
  node [
    id 1323
    label "cierpienie"
  ]
  node [
    id 1324
    label "summer"
  ]
  node [
    id 1325
    label "chronometria"
  ]
  node [
    id 1326
    label "odczyt"
  ]
  node [
    id 1327
    label "laba"
  ]
  node [
    id 1328
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1329
    label "time_period"
  ]
  node [
    id 1330
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1331
    label "Zeitgeist"
  ]
  node [
    id 1332
    label "pochodzenie"
  ]
  node [
    id 1333
    label "przep&#322;ywanie"
  ]
  node [
    id 1334
    label "schy&#322;ek"
  ]
  node [
    id 1335
    label "czwarty_wymiar"
  ]
  node [
    id 1336
    label "poprzedzi&#263;"
  ]
  node [
    id 1337
    label "pogoda"
  ]
  node [
    id 1338
    label "czasokres"
  ]
  node [
    id 1339
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1340
    label "poprzedzenie"
  ]
  node [
    id 1341
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1342
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1343
    label "dzieje"
  ]
  node [
    id 1344
    label "zegar"
  ]
  node [
    id 1345
    label "trawi&#263;"
  ]
  node [
    id 1346
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1347
    label "poprzedza&#263;"
  ]
  node [
    id 1348
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1349
    label "trawienie"
  ]
  node [
    id 1350
    label "chwila"
  ]
  node [
    id 1351
    label "rachuba_czasu"
  ]
  node [
    id 1352
    label "poprzedzanie"
  ]
  node [
    id 1353
    label "okres_czasu"
  ]
  node [
    id 1354
    label "period"
  ]
  node [
    id 1355
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1356
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1357
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1358
    label "pochodzi&#263;"
  ]
  node [
    id 1359
    label "look"
  ]
  node [
    id 1360
    label "decydowa&#263;"
  ]
  node [
    id 1361
    label "anticipate"
  ]
  node [
    id 1362
    label "pauzowa&#263;"
  ]
  node [
    id 1363
    label "oczekiwa&#263;"
  ]
  node [
    id 1364
    label "sp&#281;dza&#263;"
  ]
  node [
    id 1365
    label "hesitate"
  ]
  node [
    id 1366
    label "odpoczywa&#263;"
  ]
  node [
    id 1367
    label "mean"
  ]
  node [
    id 1368
    label "decide"
  ]
  node [
    id 1369
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1370
    label "klasyfikator"
  ]
  node [
    id 1371
    label "base_on_balls"
  ]
  node [
    id 1372
    label "doprowadza&#263;"
  ]
  node [
    id 1373
    label "usuwa&#263;"
  ]
  node [
    id 1374
    label "authorize"
  ]
  node [
    id 1375
    label "przep&#281;dza&#263;"
  ]
  node [
    id 1376
    label "przykrzy&#263;"
  ]
  node [
    id 1377
    label "p&#281;dzi&#263;"
  ]
  node [
    id 1378
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1379
    label "chcie&#263;"
  ]
  node [
    id 1380
    label "performance"
  ]
  node [
    id 1381
    label "spowodowanie"
  ]
  node [
    id 1382
    label "uzupe&#322;nienie"
  ]
  node [
    id 1383
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1384
    label "pe&#322;ny"
  ]
  node [
    id 1385
    label "activity"
  ]
  node [
    id 1386
    label "nasilenie_si&#281;"
  ]
  node [
    id 1387
    label "ziszczenie_si&#281;"
  ]
  node [
    id 1388
    label "poczucie"
  ]
  node [
    id 1389
    label "znalezienie_si&#281;"
  ]
  node [
    id 1390
    label "bash"
  ]
  node [
    id 1391
    label "rubryka"
  ]
  node [
    id 1392
    label "woof"
  ]
  node [
    id 1393
    label "completion"
  ]
  node [
    id 1394
    label "rozprawa"
  ]
  node [
    id 1395
    label "attachment"
  ]
  node [
    id 1396
    label "dodanie"
  ]
  node [
    id 1397
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1398
    label "doj&#347;&#263;"
  ]
  node [
    id 1399
    label "doj&#347;cie"
  ]
  node [
    id 1400
    label "summation"
  ]
  node [
    id 1401
    label "dochodzenie"
  ]
  node [
    id 1402
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 1403
    label "narobienie"
  ]
  node [
    id 1404
    label "porobienie"
  ]
  node [
    id 1405
    label "creation"
  ]
  node [
    id 1406
    label "campaign"
  ]
  node [
    id 1407
    label "causing"
  ]
  node [
    id 1408
    label "layout"
  ]
  node [
    id 1409
    label "poumieszczanie"
  ]
  node [
    id 1410
    label "ulokowanie_si&#281;"
  ]
  node [
    id 1411
    label "siedzenie"
  ]
  node [
    id 1412
    label "uplasowanie"
  ]
  node [
    id 1413
    label "zakrycie"
  ]
  node [
    id 1414
    label "ustalenie"
  ]
  node [
    id 1415
    label "prze&#322;adowanie"
  ]
  node [
    id 1416
    label "zachowanie"
  ]
  node [
    id 1417
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 1418
    label "klawisz"
  ]
  node [
    id 1419
    label "wype&#322;nianie"
  ]
  node [
    id 1420
    label "pozycja"
  ]
  node [
    id 1421
    label "artyku&#322;"
  ]
  node [
    id 1422
    label "tabela"
  ]
  node [
    id 1423
    label "heading"
  ]
  node [
    id 1424
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 1425
    label "opanowanie"
  ]
  node [
    id 1426
    label "ekstraspekcja"
  ]
  node [
    id 1427
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 1428
    label "smell"
  ]
  node [
    id 1429
    label "feeling"
  ]
  node [
    id 1430
    label "intuition"
  ]
  node [
    id 1431
    label "doznanie"
  ]
  node [
    id 1432
    label "os&#322;upienie"
  ]
  node [
    id 1433
    label "zareagowanie"
  ]
  node [
    id 1434
    label "kompletny"
  ]
  node [
    id 1435
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 1436
    label "nieograniczony"
  ]
  node [
    id 1437
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 1438
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 1439
    label "zupe&#322;ny"
  ]
  node [
    id 1440
    label "bezwzgl&#281;dny"
  ]
  node [
    id 1441
    label "satysfakcja"
  ]
  node [
    id 1442
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 1443
    label "r&#243;wny"
  ]
  node [
    id 1444
    label "pe&#322;no"
  ]
  node [
    id 1445
    label "otwarty"
  ]
  node [
    id 1446
    label "konsekwencja"
  ]
  node [
    id 1447
    label "righteousness"
  ]
  node [
    id 1448
    label "roboty_przymusowe"
  ]
  node [
    id 1449
    label "punishment"
  ]
  node [
    id 1450
    label "nemezis"
  ]
  node [
    id 1451
    label "odczuwa&#263;"
  ]
  node [
    id 1452
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 1453
    label "skrupienie_si&#281;"
  ]
  node [
    id 1454
    label "odczu&#263;"
  ]
  node [
    id 1455
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 1456
    label "odczucie"
  ]
  node [
    id 1457
    label "skrupianie_si&#281;"
  ]
  node [
    id 1458
    label "event"
  ]
  node [
    id 1459
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 1460
    label "koszula_Dejaniry"
  ]
  node [
    id 1461
    label "odczuwanie"
  ]
  node [
    id 1462
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 1463
    label "m&#322;ot"
  ]
  node [
    id 1464
    label "marka"
  ]
  node [
    id 1465
    label "pr&#243;ba"
  ]
  node [
    id 1466
    label "attribute"
  ]
  node [
    id 1467
    label "drzewo"
  ]
  node [
    id 1468
    label "znak"
  ]
  node [
    id 1469
    label "kara"
  ]
  node [
    id 1470
    label "dziejowo"
  ]
  node [
    id 1471
    label "epokowy"
  ]
  node [
    id 1472
    label "wiekopomny"
  ]
  node [
    id 1473
    label "historyczny"
  ]
  node [
    id 1474
    label "operator_modalny"
  ]
  node [
    id 1475
    label "alternatywa"
  ]
  node [
    id 1476
    label "wydarzenie"
  ]
  node [
    id 1477
    label "wyb&#243;r"
  ]
  node [
    id 1478
    label "potencja&#322;"
  ]
  node [
    id 1479
    label "obliczeniowo"
  ]
  node [
    id 1480
    label "ability"
  ]
  node [
    id 1481
    label "posiada&#263;"
  ]
  node [
    id 1482
    label "prospect"
  ]
  node [
    id 1483
    label "wielko&#347;&#263;"
  ]
  node [
    id 1484
    label "sytuacja"
  ]
  node [
    id 1485
    label "sk&#322;adnik"
  ]
  node [
    id 1486
    label "warunki"
  ]
  node [
    id 1487
    label "przebiegni&#281;cie"
  ]
  node [
    id 1488
    label "przebiec"
  ]
  node [
    id 1489
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1490
    label "fabu&#322;a"
  ]
  node [
    id 1491
    label "moc_obliczeniowa"
  ]
  node [
    id 1492
    label "support"
  ]
  node [
    id 1493
    label "mie&#263;"
  ]
  node [
    id 1494
    label "zawiera&#263;"
  ]
  node [
    id 1495
    label "keep_open"
  ]
  node [
    id 1496
    label "rozwi&#261;zanie"
  ]
  node [
    id 1497
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 1498
    label "problem"
  ]
  node [
    id 1499
    label "zdecydowanie_si&#281;"
  ]
  node [
    id 1500
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 1501
    label "pick"
  ]
  node [
    id 1502
    label "decyzja"
  ]
  node [
    id 1503
    label "federacja"
  ]
  node [
    id 1504
    label "executive"
  ]
  node [
    id 1505
    label "obrady"
  ]
  node [
    id 1506
    label "return"
  ]
  node [
    id 1507
    label "odyseja"
  ]
  node [
    id 1508
    label "rektyfikacja"
  ]
  node [
    id 1509
    label "poker"
  ]
  node [
    id 1510
    label "nale&#380;e&#263;"
  ]
  node [
    id 1511
    label "odparowanie"
  ]
  node [
    id 1512
    label "smoke"
  ]
  node [
    id 1513
    label "odparowa&#263;"
  ]
  node [
    id 1514
    label "parowanie"
  ]
  node [
    id 1515
    label "pair"
  ]
  node [
    id 1516
    label "odparowywa&#263;"
  ]
  node [
    id 1517
    label "dodatek"
  ]
  node [
    id 1518
    label "odparowywanie"
  ]
  node [
    id 1519
    label "jednostka_monetarna"
  ]
  node [
    id 1520
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 1521
    label "moneta"
  ]
  node [
    id 1522
    label "damp"
  ]
  node [
    id 1523
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 1524
    label "wyparowanie"
  ]
  node [
    id 1525
    label "gaz_cieplarniany"
  ]
  node [
    id 1526
    label "gaz"
  ]
  node [
    id 1527
    label "destylacja"
  ]
  node [
    id 1528
    label "odyssey"
  ]
  node [
    id 1529
    label "podr&#243;&#380;"
  ]
  node [
    id 1530
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 1531
    label "patriota"
  ]
  node [
    id 1532
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 1533
    label "matuszka"
  ]
  node [
    id 1534
    label "czynnik"
  ]
  node [
    id 1535
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1536
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1537
    label "&#347;wiadectwo"
  ]
  node [
    id 1538
    label "przyczyna"
  ]
  node [
    id 1539
    label "kamena"
  ]
  node [
    id 1540
    label "poci&#261;ganie"
  ]
  node [
    id 1541
    label "geneza"
  ]
  node [
    id 1542
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 1543
    label "bra&#263;_si&#281;"
  ]
  node [
    id 1544
    label "ciek_wodny"
  ]
  node [
    id 1545
    label "subject"
  ]
  node [
    id 1546
    label "popadia"
  ]
  node [
    id 1547
    label "zwolennik"
  ]
  node [
    id 1548
    label "obywatel"
  ]
  node [
    id 1549
    label "inszy"
  ]
  node [
    id 1550
    label "inaczej"
  ]
  node [
    id 1551
    label "kolejny"
  ]
  node [
    id 1552
    label "odr&#281;bny"
  ]
  node [
    id 1553
    label "nast&#281;pnie"
  ]
  node [
    id 1554
    label "kolejno"
  ]
  node [
    id 1555
    label "kt&#243;ry&#347;"
  ]
  node [
    id 1556
    label "nastopny"
  ]
  node [
    id 1557
    label "osobny"
  ]
  node [
    id 1558
    label "odr&#281;bnie"
  ]
  node [
    id 1559
    label "udzielnie"
  ]
  node [
    id 1560
    label "individually"
  ]
  node [
    id 1561
    label "Samojedzi"
  ]
  node [
    id 1562
    label "Syngalezi"
  ]
  node [
    id 1563
    label "nacja"
  ]
  node [
    id 1564
    label "Baszkirzy"
  ]
  node [
    id 1565
    label "Aztekowie"
  ]
  node [
    id 1566
    label "lud"
  ]
  node [
    id 1567
    label "t&#322;um"
  ]
  node [
    id 1568
    label "Mohikanie"
  ]
  node [
    id 1569
    label "Wotiacy"
  ]
  node [
    id 1570
    label "Komancze"
  ]
  node [
    id 1571
    label "Apacze"
  ]
  node [
    id 1572
    label "Czejenowie"
  ]
  node [
    id 1573
    label "ludno&#347;&#263;"
  ]
  node [
    id 1574
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 1575
    label "Irokezi"
  ]
  node [
    id 1576
    label "Buriaci"
  ]
  node [
    id 1577
    label "Siuksowie"
  ]
  node [
    id 1578
    label "ch&#322;opstwo"
  ]
  node [
    id 1579
    label "innowierstwo"
  ]
  node [
    id 1580
    label "najazd"
  ]
  node [
    id 1581
    label "demofobia"
  ]
  node [
    id 1582
    label "Wizygoci"
  ]
  node [
    id 1583
    label "Maroni"
  ]
  node [
    id 1584
    label "Wenedowie"
  ]
  node [
    id 1585
    label "Tocharowie"
  ]
  node [
    id 1586
    label "chamstwo"
  ]
  node [
    id 1587
    label "Nawahowie"
  ]
  node [
    id 1588
    label "Ladynowie"
  ]
  node [
    id 1589
    label "Po&#322;owcy"
  ]
  node [
    id 1590
    label "Kozacy"
  ]
  node [
    id 1591
    label "Ugrowie"
  ]
  node [
    id 1592
    label "Do&#322;ganie"
  ]
  node [
    id 1593
    label "Dogonowie"
  ]
  node [
    id 1594
    label "gmin"
  ]
  node [
    id 1595
    label "Negryci"
  ]
  node [
    id 1596
    label "Nogajowie"
  ]
  node [
    id 1597
    label "Paleoazjaci"
  ]
  node [
    id 1598
    label "Majowie"
  ]
  node [
    id 1599
    label "Tagalowie"
  ]
  node [
    id 1600
    label "Indoariowie"
  ]
  node [
    id 1601
    label "Frygijczycy"
  ]
  node [
    id 1602
    label "Kumbrowie"
  ]
  node [
    id 1603
    label "Indoira&#324;czycy"
  ]
  node [
    id 1604
    label "Kipczacy"
  ]
  node [
    id 1605
    label "Retowie"
  ]
  node [
    id 1606
    label "etnogeneza"
  ]
  node [
    id 1607
    label "nationality"
  ]
  node [
    id 1608
    label "ongi&#347;"
  ]
  node [
    id 1609
    label "d&#322;ugotrwale"
  ]
  node [
    id 1610
    label "dawnie"
  ]
  node [
    id 1611
    label "wcze&#347;niej"
  ]
  node [
    id 1612
    label "kiedy&#347;"
  ]
  node [
    id 1613
    label "niegdysiejszy"
  ]
  node [
    id 1614
    label "drzewiej"
  ]
  node [
    id 1615
    label "d&#322;ugo"
  ]
  node [
    id 1616
    label "d&#322;ugotrwa&#322;y"
  ]
  node [
    id 1617
    label "wcze&#347;niejszy"
  ]
  node [
    id 1618
    label "od_dawna"
  ]
  node [
    id 1619
    label "anachroniczny"
  ]
  node [
    id 1620
    label "dawniej"
  ]
  node [
    id 1621
    label "przesz&#322;y"
  ]
  node [
    id 1622
    label "d&#322;ugoletni"
  ]
  node [
    id 1623
    label "poprzedni"
  ]
  node [
    id 1624
    label "przestarza&#322;y"
  ]
  node [
    id 1625
    label "kombatant"
  ]
  node [
    id 1626
    label "recur"
  ]
  node [
    id 1627
    label "pojedna&#263;_si&#281;"
  ]
  node [
    id 1628
    label "podj&#261;&#263;"
  ]
  node [
    id 1629
    label "nawi&#261;za&#263;"
  ]
  node [
    id 1630
    label "spowodowa&#263;"
  ]
  node [
    id 1631
    label "revive"
  ]
  node [
    id 1632
    label "render"
  ]
  node [
    id 1633
    label "zosta&#263;"
  ]
  node [
    id 1634
    label "przyj&#347;&#263;"
  ]
  node [
    id 1635
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 1636
    label "przyby&#263;"
  ]
  node [
    id 1637
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1638
    label "zyska&#263;"
  ]
  node [
    id 1639
    label "dotrze&#263;"
  ]
  node [
    id 1640
    label "become"
  ]
  node [
    id 1641
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1642
    label "line_up"
  ]
  node [
    id 1643
    label "tie"
  ]
  node [
    id 1644
    label "bind"
  ]
  node [
    id 1645
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1646
    label "przyczepi&#263;"
  ]
  node [
    id 1647
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1648
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1649
    label "change"
  ]
  node [
    id 1650
    label "osta&#263;_si&#281;"
  ]
  node [
    id 1651
    label "catch"
  ]
  node [
    id 1652
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1653
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 1654
    label "zmieni&#263;"
  ]
  node [
    id 1655
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 1656
    label "zareagowa&#263;"
  ]
  node [
    id 1657
    label "allude"
  ]
  node [
    id 1658
    label "raise"
  ]
  node [
    id 1659
    label "draw"
  ]
  node [
    id 1660
    label "upgrade"
  ]
  node [
    id 1661
    label "pierworodztwo"
  ]
  node [
    id 1662
    label "faza"
  ]
  node [
    id 1663
    label "nast&#281;pstwo"
  ]
  node [
    id 1664
    label "uwaga"
  ]
  node [
    id 1665
    label "plac"
  ]
  node [
    id 1666
    label "location"
  ]
  node [
    id 1667
    label "warunek_lokalowy"
  ]
  node [
    id 1668
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1669
    label "status"
  ]
  node [
    id 1670
    label "komutowanie"
  ]
  node [
    id 1671
    label "dw&#243;jnik"
  ]
  node [
    id 1672
    label "przerywacz"
  ]
  node [
    id 1673
    label "przew&#243;d"
  ]
  node [
    id 1674
    label "obsesja"
  ]
  node [
    id 1675
    label "nastr&#243;j"
  ]
  node [
    id 1676
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1677
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 1678
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 1679
    label "cykl_astronomiczny"
  ]
  node [
    id 1680
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1681
    label "coil"
  ]
  node [
    id 1682
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 1683
    label "stan_skupienia"
  ]
  node [
    id 1684
    label "komutowa&#263;"
  ]
  node [
    id 1685
    label "degree"
  ]
  node [
    id 1686
    label "obw&#243;d"
  ]
  node [
    id 1687
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 1688
    label "fotoelement"
  ]
  node [
    id 1689
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 1690
    label "kraw&#281;d&#378;"
  ]
  node [
    id 1691
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 1692
    label "pierwor&#243;dztwo"
  ]
  node [
    id 1693
    label "kolejno&#347;&#263;"
  ]
  node [
    id 1694
    label "proces"
  ]
  node [
    id 1695
    label "wydziedziczenie"
  ]
  node [
    id 1696
    label "wydziedziczy&#263;"
  ]
  node [
    id 1697
    label "prawo"
  ]
  node [
    id 1698
    label "ulepszenie"
  ]
  node [
    id 1699
    label "dysfonia"
  ]
  node [
    id 1700
    label "prawi&#263;"
  ]
  node [
    id 1701
    label "remark"
  ]
  node [
    id 1702
    label "express"
  ]
  node [
    id 1703
    label "chew_the_fat"
  ]
  node [
    id 1704
    label "talk"
  ]
  node [
    id 1705
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 1706
    label "say"
  ]
  node [
    id 1707
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1708
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 1709
    label "tell"
  ]
  node [
    id 1710
    label "rozmawia&#263;"
  ]
  node [
    id 1711
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1712
    label "powiada&#263;"
  ]
  node [
    id 1713
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 1714
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1715
    label "okre&#347;la&#263;"
  ]
  node [
    id 1716
    label "gaworzy&#263;"
  ]
  node [
    id 1717
    label "formu&#322;owa&#263;"
  ]
  node [
    id 1718
    label "umie&#263;"
  ]
  node [
    id 1719
    label "wydobywa&#263;"
  ]
  node [
    id 1720
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 1721
    label "doznawa&#263;"
  ]
  node [
    id 1722
    label "distribute"
  ]
  node [
    id 1723
    label "style"
  ]
  node [
    id 1724
    label "signify"
  ]
  node [
    id 1725
    label "komunikowa&#263;"
  ]
  node [
    id 1726
    label "inform"
  ]
  node [
    id 1727
    label "oznacza&#263;"
  ]
  node [
    id 1728
    label "znaczy&#263;"
  ]
  node [
    id 1729
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 1730
    label "convey"
  ]
  node [
    id 1731
    label "arouse"
  ]
  node [
    id 1732
    label "represent"
  ]
  node [
    id 1733
    label "give_voice"
  ]
  node [
    id 1734
    label "determine"
  ]
  node [
    id 1735
    label "wyjmowa&#263;"
  ]
  node [
    id 1736
    label "wydostawa&#263;"
  ]
  node [
    id 1737
    label "wydawa&#263;"
  ]
  node [
    id 1738
    label "g&#243;rnictwo"
  ]
  node [
    id 1739
    label "dobywa&#263;"
  ]
  node [
    id 1740
    label "uwydatnia&#263;"
  ]
  node [
    id 1741
    label "eksploatowa&#263;"
  ]
  node [
    id 1742
    label "excavate"
  ]
  node [
    id 1743
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 1744
    label "ocala&#263;"
  ]
  node [
    id 1745
    label "uzyskiwa&#263;"
  ]
  node [
    id 1746
    label "m&#243;c"
  ]
  node [
    id 1747
    label "can"
  ]
  node [
    id 1748
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 1749
    label "szczeka&#263;"
  ]
  node [
    id 1750
    label "rozumie&#263;"
  ]
  node [
    id 1751
    label "funkcjonowa&#263;"
  ]
  node [
    id 1752
    label "mawia&#263;"
  ]
  node [
    id 1753
    label "opowiada&#263;"
  ]
  node [
    id 1754
    label "chatter"
  ]
  node [
    id 1755
    label "niemowl&#281;"
  ]
  node [
    id 1756
    label "kosmetyk"
  ]
  node [
    id 1757
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 1758
    label "stanowisko_archeologiczne"
  ]
  node [
    id 1759
    label "pisa&#263;"
  ]
  node [
    id 1760
    label "kod"
  ]
  node [
    id 1761
    label "pype&#263;"
  ]
  node [
    id 1762
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1763
    label "gramatyka"
  ]
  node [
    id 1764
    label "language"
  ]
  node [
    id 1765
    label "t&#322;umaczenie"
  ]
  node [
    id 1766
    label "artykulator"
  ]
  node [
    id 1767
    label "rozumienie"
  ]
  node [
    id 1768
    label "jama_ustna"
  ]
  node [
    id 1769
    label "ssanie"
  ]
  node [
    id 1770
    label "lizanie"
  ]
  node [
    id 1771
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 1772
    label "liza&#263;"
  ]
  node [
    id 1773
    label "makroglosja"
  ]
  node [
    id 1774
    label "natural_language"
  ]
  node [
    id 1775
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1776
    label "napisa&#263;"
  ]
  node [
    id 1777
    label "m&#243;wienie"
  ]
  node [
    id 1778
    label "s&#322;ownictwo"
  ]
  node [
    id 1779
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1780
    label "konsonantyzm"
  ]
  node [
    id 1781
    label "ssa&#263;"
  ]
  node [
    id 1782
    label "wokalizm"
  ]
  node [
    id 1783
    label "kultura_duchowa"
  ]
  node [
    id 1784
    label "formalizowanie"
  ]
  node [
    id 1785
    label "jeniec"
  ]
  node [
    id 1786
    label "kawa&#322;ek"
  ]
  node [
    id 1787
    label "po_koroniarsku"
  ]
  node [
    id 1788
    label "stylik"
  ]
  node [
    id 1789
    label "przet&#322;umaczenie"
  ]
  node [
    id 1790
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1791
    label "formacja_geologiczna"
  ]
  node [
    id 1792
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1793
    label "but"
  ]
  node [
    id 1794
    label "pismo"
  ]
  node [
    id 1795
    label "formalizowa&#263;"
  ]
  node [
    id 1796
    label "dysleksja"
  ]
  node [
    id 1797
    label "dysphonia"
  ]
  node [
    id 1798
    label "undertaking"
  ]
  node [
    id 1799
    label "wystarcza&#263;"
  ]
  node [
    id 1800
    label "kosztowa&#263;"
  ]
  node [
    id 1801
    label "sprawowa&#263;"
  ]
  node [
    id 1802
    label "przebywa&#263;"
  ]
  node [
    id 1803
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 1804
    label "wystarczy&#263;"
  ]
  node [
    id 1805
    label "wystawa&#263;"
  ]
  node [
    id 1806
    label "digest"
  ]
  node [
    id 1807
    label "mieszka&#263;"
  ]
  node [
    id 1808
    label "dobywa&#263;_si&#281;"
  ]
  node [
    id 1809
    label "panowa&#263;"
  ]
  node [
    id 1810
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 1811
    label "function"
  ]
  node [
    id 1812
    label "zjednywa&#263;"
  ]
  node [
    id 1813
    label "pause"
  ]
  node [
    id 1814
    label "przestawa&#263;"
  ]
  node [
    id 1815
    label "tkwi&#263;"
  ]
  node [
    id 1816
    label "savor"
  ]
  node [
    id 1817
    label "try"
  ]
  node [
    id 1818
    label "essay"
  ]
  node [
    id 1819
    label "cena"
  ]
  node [
    id 1820
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 1821
    label "suffice"
  ]
  node [
    id 1822
    label "dostawa&#263;"
  ]
  node [
    id 1823
    label "zaspokaja&#263;"
  ]
  node [
    id 1824
    label "stawa&#263;"
  ]
  node [
    id 1825
    label "stan&#261;&#263;"
  ]
  node [
    id 1826
    label "zaspokoi&#263;"
  ]
  node [
    id 1827
    label "dosta&#263;"
  ]
  node [
    id 1828
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 1829
    label "stop"
  ]
  node [
    id 1830
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 1831
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 1832
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 1833
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 1834
    label "prosecute"
  ]
  node [
    id 1835
    label "fall"
  ]
  node [
    id 1836
    label "zajmowa&#263;"
  ]
  node [
    id 1837
    label "room"
  ]
  node [
    id 1838
    label "free"
  ]
  node [
    id 1839
    label "jako&#347;"
  ]
  node [
    id 1840
    label "charakterystyczny"
  ]
  node [
    id 1841
    label "jako_tako"
  ]
  node [
    id 1842
    label "ciekawy"
  ]
  node [
    id 1843
    label "dziwny"
  ]
  node [
    id 1844
    label "przyzwoity"
  ]
  node [
    id 1845
    label "nieszpetny"
  ]
  node [
    id 1846
    label "udolny"
  ]
  node [
    id 1847
    label "spory"
  ]
  node [
    id 1848
    label "nie&#378;le"
  ]
  node [
    id 1849
    label "niczegowaty"
  ]
  node [
    id 1850
    label "kulturalny"
  ]
  node [
    id 1851
    label "wystarczaj&#261;cy"
  ]
  node [
    id 1852
    label "przyzwoicie"
  ]
  node [
    id 1853
    label "skromny"
  ]
  node [
    id 1854
    label "przystojny"
  ]
  node [
    id 1855
    label "nale&#380;yty"
  ]
  node [
    id 1856
    label "indagator"
  ]
  node [
    id 1857
    label "swoisty"
  ]
  node [
    id 1858
    label "interesuj&#261;cy"
  ]
  node [
    id 1859
    label "nietuzinkowy"
  ]
  node [
    id 1860
    label "ciekawie"
  ]
  node [
    id 1861
    label "interesowanie"
  ]
  node [
    id 1862
    label "intryguj&#261;cy"
  ]
  node [
    id 1863
    label "ch&#281;tny"
  ]
  node [
    id 1864
    label "typowy"
  ]
  node [
    id 1865
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 1866
    label "dziwy"
  ]
  node [
    id 1867
    label "dziwnie"
  ]
  node [
    id 1868
    label "jako_taki"
  ]
  node [
    id 1869
    label "w_miar&#281;"
  ]
  node [
    id 1870
    label "time"
  ]
  node [
    id 1871
    label "blok"
  ]
  node [
    id 1872
    label "reading"
  ]
  node [
    id 1873
    label "handout"
  ]
  node [
    id 1874
    label "podawanie"
  ]
  node [
    id 1875
    label "wyk&#322;ad"
  ]
  node [
    id 1876
    label "lecture"
  ]
  node [
    id 1877
    label "pomiar"
  ]
  node [
    id 1878
    label "meteorology"
  ]
  node [
    id 1879
    label "weather"
  ]
  node [
    id 1880
    label "atak"
  ]
  node [
    id 1881
    label "prognoza_meteorologiczna"
  ]
  node [
    id 1882
    label "potrzyma&#263;"
  ]
  node [
    id 1883
    label "program"
  ]
  node [
    id 1884
    label "czas_wolny"
  ]
  node [
    id 1885
    label "metrologia"
  ]
  node [
    id 1886
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 1887
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 1888
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 1889
    label "czasomierz"
  ]
  node [
    id 1890
    label "tyka&#263;"
  ]
  node [
    id 1891
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 1892
    label "tykn&#261;&#263;"
  ]
  node [
    id 1893
    label "nabicie"
  ]
  node [
    id 1894
    label "bicie"
  ]
  node [
    id 1895
    label "kotwica"
  ]
  node [
    id 1896
    label "godzinnik"
  ]
  node [
    id 1897
    label "werk"
  ]
  node [
    id 1898
    label "wahad&#322;o"
  ]
  node [
    id 1899
    label "kurant"
  ]
  node [
    id 1900
    label "cyferblat"
  ]
  node [
    id 1901
    label "lutowa&#263;"
  ]
  node [
    id 1902
    label "metal"
  ]
  node [
    id 1903
    label "przetrawia&#263;"
  ]
  node [
    id 1904
    label "poch&#322;ania&#263;"
  ]
  node [
    id 1905
    label "marnowa&#263;"
  ]
  node [
    id 1906
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 1907
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1908
    label "marnowanie"
  ]
  node [
    id 1909
    label "unicestwianie"
  ]
  node [
    id 1910
    label "sp&#281;dzanie"
  ]
  node [
    id 1911
    label "digestion"
  ]
  node [
    id 1912
    label "perystaltyka"
  ]
  node [
    id 1913
    label "proces_fizjologiczny"
  ]
  node [
    id 1914
    label "rozk&#322;adanie"
  ]
  node [
    id 1915
    label "przetrawianie"
  ]
  node [
    id 1916
    label "contemplation"
  ]
  node [
    id 1917
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 1918
    label "background"
  ]
  node [
    id 1919
    label "wynikanie"
  ]
  node [
    id 1920
    label "origin"
  ]
  node [
    id 1921
    label "zaczynanie_si&#281;"
  ]
  node [
    id 1922
    label "beginning"
  ]
  node [
    id 1923
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 1924
    label "cross"
  ]
  node [
    id 1925
    label "swimming"
  ]
  node [
    id 1926
    label "min&#261;&#263;"
  ]
  node [
    id 1927
    label "przeby&#263;"
  ]
  node [
    id 1928
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 1929
    label "zago&#347;ci&#263;"
  ]
  node [
    id 1930
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 1931
    label "pour"
  ]
  node [
    id 1932
    label "mija&#263;"
  ]
  node [
    id 1933
    label "sail"
  ]
  node [
    id 1934
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 1935
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 1936
    label "go&#347;ci&#263;"
  ]
  node [
    id 1937
    label "przebycie"
  ]
  node [
    id 1938
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 1939
    label "mini&#281;cie"
  ]
  node [
    id 1940
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 1941
    label "zaistnienie"
  ]
  node [
    id 1942
    label "cruise"
  ]
  node [
    id 1943
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 1944
    label "zjawianie_si&#281;"
  ]
  node [
    id 1945
    label "mijanie"
  ]
  node [
    id 1946
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 1947
    label "przebywanie"
  ]
  node [
    id 1948
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 1949
    label "flux"
  ]
  node [
    id 1950
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1951
    label "zaznawanie"
  ]
  node [
    id 1952
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1953
    label "overwhelm"
  ]
  node [
    id 1954
    label "opatrzy&#263;"
  ]
  node [
    id 1955
    label "opatrywanie"
  ]
  node [
    id 1956
    label "zanikni&#281;cie"
  ]
  node [
    id 1957
    label "departure"
  ]
  node [
    id 1958
    label "odej&#347;cie"
  ]
  node [
    id 1959
    label "opuszczenie"
  ]
  node [
    id 1960
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 1961
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 1962
    label "ciecz"
  ]
  node [
    id 1963
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1964
    label "oddalenie_si&#281;"
  ]
  node [
    id 1965
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 1966
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 1967
    label "bolt"
  ]
  node [
    id 1968
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1969
    label "date"
  ]
  node [
    id 1970
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 1971
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 1972
    label "wynika&#263;"
  ]
  node [
    id 1973
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1974
    label "progress"
  ]
  node [
    id 1975
    label "opatrzenie"
  ]
  node [
    id 1976
    label "opatrywa&#263;"
  ]
  node [
    id 1977
    label "epoka"
  ]
  node [
    id 1978
    label "ciota"
  ]
  node [
    id 1979
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 1980
    label "flow"
  ]
  node [
    id 1981
    label "choroba_przyrodzona"
  ]
  node [
    id 1982
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 1983
    label "kres"
  ]
  node [
    id 1984
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1985
    label "tworzy&#263;"
  ]
  node [
    id 1986
    label "prawdzi&#263;"
  ]
  node [
    id 1987
    label "create"
  ]
  node [
    id 1988
    label "wykorzystywa&#263;"
  ]
  node [
    id 1989
    label "spieni&#281;&#380;a&#263;"
  ]
  node [
    id 1990
    label "transact"
  ]
  node [
    id 1991
    label "string"
  ]
  node [
    id 1992
    label "pomaga&#263;"
  ]
  node [
    id 1993
    label "wykonywa&#263;"
  ]
  node [
    id 1994
    label "stanowi&#263;"
  ]
  node [
    id 1995
    label "wytwarza&#263;"
  ]
  node [
    id 1996
    label "consist"
  ]
  node [
    id 1997
    label "pope&#322;nia&#263;"
  ]
  node [
    id 1998
    label "wymienia&#263;"
  ]
  node [
    id 1999
    label "deal"
  ]
  node [
    id 2000
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 2001
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 2002
    label "krzywdzi&#263;"
  ]
  node [
    id 2003
    label "use"
  ]
  node [
    id 2004
    label "liga&#263;"
  ]
  node [
    id 2005
    label "urzeczywistnia&#263;"
  ]
  node [
    id 2006
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 2007
    label "metier"
  ]
  node [
    id 2008
    label "autonomy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 0
    target 401
  ]
  edge [
    source 0
    target 402
  ]
  edge [
    source 0
    target 403
  ]
  edge [
    source 0
    target 404
  ]
  edge [
    source 0
    target 405
  ]
  edge [
    source 0
    target 406
  ]
  edge [
    source 0
    target 407
  ]
  edge [
    source 0
    target 408
  ]
  edge [
    source 0
    target 409
  ]
  edge [
    source 0
    target 410
  ]
  edge [
    source 0
    target 411
  ]
  edge [
    source 0
    target 412
  ]
  edge [
    source 0
    target 413
  ]
  edge [
    source 0
    target 414
  ]
  edge [
    source 0
    target 415
  ]
  edge [
    source 0
    target 416
  ]
  edge [
    source 0
    target 417
  ]
  edge [
    source 0
    target 418
  ]
  edge [
    source 0
    target 419
  ]
  edge [
    source 0
    target 420
  ]
  edge [
    source 0
    target 421
  ]
  edge [
    source 0
    target 422
  ]
  edge [
    source 0
    target 423
  ]
  edge [
    source 0
    target 424
  ]
  edge [
    source 0
    target 425
  ]
  edge [
    source 0
    target 426
  ]
  edge [
    source 0
    target 427
  ]
  edge [
    source 0
    target 428
  ]
  edge [
    source 0
    target 429
  ]
  edge [
    source 0
    target 430
  ]
  edge [
    source 0
    target 431
  ]
  edge [
    source 0
    target 432
  ]
  edge [
    source 0
    target 433
  ]
  edge [
    source 0
    target 434
  ]
  edge [
    source 0
    target 435
  ]
  edge [
    source 0
    target 436
  ]
  edge [
    source 0
    target 437
  ]
  edge [
    source 0
    target 438
  ]
  edge [
    source 0
    target 439
  ]
  edge [
    source 0
    target 440
  ]
  edge [
    source 0
    target 441
  ]
  edge [
    source 0
    target 442
  ]
  edge [
    source 0
    target 443
  ]
  edge [
    source 0
    target 444
  ]
  edge [
    source 0
    target 445
  ]
  edge [
    source 0
    target 446
  ]
  edge [
    source 0
    target 447
  ]
  edge [
    source 0
    target 448
  ]
  edge [
    source 0
    target 449
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 450
  ]
  edge [
    source 1
    target 451
  ]
  edge [
    source 1
    target 452
  ]
  edge [
    source 1
    target 453
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 454
  ]
  edge [
    source 1
    target 455
  ]
  edge [
    source 1
    target 456
  ]
  edge [
    source 1
    target 457
  ]
  edge [
    source 1
    target 458
  ]
  edge [
    source 1
    target 459
  ]
  edge [
    source 1
    target 460
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 461
  ]
  edge [
    source 1
    target 462
  ]
  edge [
    source 1
    target 463
  ]
  edge [
    source 1
    target 464
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 2
    target 520
  ]
  edge [
    source 2
    target 521
  ]
  edge [
    source 2
    target 522
  ]
  edge [
    source 2
    target 523
  ]
  edge [
    source 2
    target 524
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 525
  ]
  edge [
    source 2
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 3
    target 581
  ]
  edge [
    source 3
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 3
    target 586
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 588
  ]
  edge [
    source 3
    target 589
  ]
  edge [
    source 3
    target 590
  ]
  edge [
    source 3
    target 591
  ]
  edge [
    source 3
    target 592
  ]
  edge [
    source 3
    target 593
  ]
  edge [
    source 3
    target 594
  ]
  edge [
    source 3
    target 595
  ]
  edge [
    source 3
    target 596
  ]
  edge [
    source 3
    target 597
  ]
  edge [
    source 3
    target 598
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 617
  ]
  edge [
    source 5
    target 618
  ]
  edge [
    source 5
    target 619
  ]
  edge [
    source 5
    target 620
  ]
  edge [
    source 5
    target 621
  ]
  edge [
    source 5
    target 622
  ]
  edge [
    source 5
    target 623
  ]
  edge [
    source 5
    target 624
  ]
  edge [
    source 5
    target 625
  ]
  edge [
    source 5
    target 626
  ]
  edge [
    source 5
    target 627
  ]
  edge [
    source 5
    target 628
  ]
  edge [
    source 5
    target 629
  ]
  edge [
    source 5
    target 630
  ]
  edge [
    source 5
    target 631
  ]
  edge [
    source 5
    target 632
  ]
  edge [
    source 5
    target 633
  ]
  edge [
    source 5
    target 634
  ]
  edge [
    source 5
    target 635
  ]
  edge [
    source 5
    target 636
  ]
  edge [
    source 5
    target 637
  ]
  edge [
    source 5
    target 638
  ]
  edge [
    source 5
    target 639
  ]
  edge [
    source 5
    target 640
  ]
  edge [
    source 5
    target 641
  ]
  edge [
    source 5
    target 642
  ]
  edge [
    source 5
    target 643
  ]
  edge [
    source 5
    target 644
  ]
  edge [
    source 5
    target 645
  ]
  edge [
    source 5
    target 646
  ]
  edge [
    source 5
    target 647
  ]
  edge [
    source 5
    target 648
  ]
  edge [
    source 5
    target 649
  ]
  edge [
    source 5
    target 650
  ]
  edge [
    source 5
    target 651
  ]
  edge [
    source 5
    target 652
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 26
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 805
  ]
  edge [
    source 14
    target 25
  ]
  edge [
    source 14
    target 806
  ]
  edge [
    source 14
    target 807
  ]
  edge [
    source 14
    target 808
  ]
  edge [
    source 14
    target 809
  ]
  edge [
    source 14
    target 810
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 811
  ]
  edge [
    source 14
    target 812
  ]
  edge [
    source 14
    target 813
  ]
  edge [
    source 14
    target 814
  ]
  edge [
    source 14
    target 815
  ]
  edge [
    source 14
    target 816
  ]
  edge [
    source 14
    target 817
  ]
  edge [
    source 14
    target 818
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 375
  ]
  edge [
    source 14
    target 376
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 819
  ]
  edge [
    source 14
    target 820
  ]
  edge [
    source 14
    target 821
  ]
  edge [
    source 14
    target 822
  ]
  edge [
    source 14
    target 823
  ]
  edge [
    source 14
    target 824
  ]
  edge [
    source 14
    target 825
  ]
  edge [
    source 14
    target 826
  ]
  edge [
    source 14
    target 827
  ]
  edge [
    source 14
    target 828
  ]
  edge [
    source 14
    target 829
  ]
  edge [
    source 14
    target 830
  ]
  edge [
    source 14
    target 831
  ]
  edge [
    source 14
    target 832
  ]
  edge [
    source 14
    target 833
  ]
  edge [
    source 14
    target 834
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 836
  ]
  edge [
    source 15
    target 837
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 465
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 502
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 501
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 16
    target 858
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 16
    target 860
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 861
  ]
  edge [
    source 16
    target 862
  ]
  edge [
    source 16
    target 863
  ]
  edge [
    source 16
    target 864
  ]
  edge [
    source 16
    target 865
  ]
  edge [
    source 16
    target 866
  ]
  edge [
    source 16
    target 867
  ]
  edge [
    source 16
    target 868
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 871
  ]
  edge [
    source 16
    target 485
  ]
  edge [
    source 16
    target 486
  ]
  edge [
    source 16
    target 482
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 16
    target 873
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 874
  ]
  edge [
    source 16
    target 875
  ]
  edge [
    source 16
    target 876
  ]
  edge [
    source 16
    target 39
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 894
  ]
  edge [
    source 17
    target 895
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 17
    target 897
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 735
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 488
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 904
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 905
  ]
  edge [
    source 21
    target 906
  ]
  edge [
    source 21
    target 907
  ]
  edge [
    source 21
    target 908
  ]
  edge [
    source 21
    target 909
  ]
  edge [
    source 21
    target 910
  ]
  edge [
    source 21
    target 911
  ]
  edge [
    source 21
    target 912
  ]
  edge [
    source 21
    target 913
  ]
  edge [
    source 21
    target 914
  ]
  edge [
    source 21
    target 915
  ]
  edge [
    source 21
    target 916
  ]
  edge [
    source 21
    target 917
  ]
  edge [
    source 21
    target 918
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 921
  ]
  edge [
    source 21
    target 922
  ]
  edge [
    source 21
    target 923
  ]
  edge [
    source 21
    target 924
  ]
  edge [
    source 21
    target 925
  ]
  edge [
    source 21
    target 926
  ]
  edge [
    source 21
    target 927
  ]
  edge [
    source 21
    target 928
  ]
  edge [
    source 21
    target 929
  ]
  edge [
    source 21
    target 930
  ]
  edge [
    source 21
    target 646
  ]
  edge [
    source 21
    target 931
  ]
  edge [
    source 21
    target 932
  ]
  edge [
    source 21
    target 933
  ]
  edge [
    source 21
    target 934
  ]
  edge [
    source 21
    target 935
  ]
  edge [
    source 21
    target 936
  ]
  edge [
    source 21
    target 937
  ]
  edge [
    source 21
    target 938
  ]
  edge [
    source 21
    target 939
  ]
  edge [
    source 21
    target 940
  ]
  edge [
    source 21
    target 941
  ]
  edge [
    source 21
    target 942
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 943
  ]
  edge [
    source 22
    target 944
  ]
  edge [
    source 22
    target 39
  ]
  edge [
    source 22
    target 945
  ]
  edge [
    source 22
    target 946
  ]
  edge [
    source 22
    target 947
  ]
  edge [
    source 22
    target 948
  ]
  edge [
    source 22
    target 949
  ]
  edge [
    source 22
    target 502
  ]
  edge [
    source 22
    target 950
  ]
  edge [
    source 22
    target 951
  ]
  edge [
    source 22
    target 135
  ]
  edge [
    source 22
    target 952
  ]
  edge [
    source 22
    target 737
  ]
  edge [
    source 22
    target 953
  ]
  edge [
    source 22
    target 954
  ]
  edge [
    source 22
    target 955
  ]
  edge [
    source 22
    target 846
  ]
  edge [
    source 22
    target 956
  ]
  edge [
    source 22
    target 957
  ]
  edge [
    source 22
    target 845
  ]
  edge [
    source 22
    target 838
  ]
  edge [
    source 22
    target 136
  ]
  edge [
    source 22
    target 958
  ]
  edge [
    source 22
    target 843
  ]
  edge [
    source 22
    target 959
  ]
  edge [
    source 22
    target 960
  ]
  edge [
    source 22
    target 961
  ]
  edge [
    source 22
    target 844
  ]
  edge [
    source 22
    target 962
  ]
  edge [
    source 22
    target 963
  ]
  edge [
    source 22
    target 850
  ]
  edge [
    source 22
    target 964
  ]
  edge [
    source 22
    target 839
  ]
  edge [
    source 22
    target 840
  ]
  edge [
    source 22
    target 841
  ]
  edge [
    source 22
    target 842
  ]
  edge [
    source 22
    target 501
  ]
  edge [
    source 22
    target 847
  ]
  edge [
    source 22
    target 848
  ]
  edge [
    source 22
    target 849
  ]
  edge [
    source 22
    target 84
  ]
  edge [
    source 22
    target 851
  ]
  edge [
    source 22
    target 852
  ]
  edge [
    source 22
    target 965
  ]
  edge [
    source 22
    target 966
  ]
  edge [
    source 22
    target 51
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 52
  ]
  edge [
    source 23
    target 53
  ]
  edge [
    source 23
    target 967
  ]
  edge [
    source 23
    target 968
  ]
  edge [
    source 23
    target 969
  ]
  edge [
    source 23
    target 970
  ]
  edge [
    source 23
    target 971
  ]
  edge [
    source 23
    target 972
  ]
  edge [
    source 23
    target 973
  ]
  edge [
    source 23
    target 974
  ]
  edge [
    source 23
    target 975
  ]
  edge [
    source 23
    target 976
  ]
  edge [
    source 23
    target 977
  ]
  edge [
    source 23
    target 905
  ]
  edge [
    source 23
    target 978
  ]
  edge [
    source 23
    target 979
  ]
  edge [
    source 23
    target 980
  ]
  edge [
    source 23
    target 981
  ]
  edge [
    source 23
    target 982
  ]
  edge [
    source 23
    target 983
  ]
  edge [
    source 23
    target 984
  ]
  edge [
    source 23
    target 985
  ]
  edge [
    source 23
    target 986
  ]
  edge [
    source 23
    target 987
  ]
  edge [
    source 23
    target 988
  ]
  edge [
    source 23
    target 989
  ]
  edge [
    source 23
    target 990
  ]
  edge [
    source 23
    target 991
  ]
  edge [
    source 23
    target 992
  ]
  edge [
    source 23
    target 993
  ]
  edge [
    source 23
    target 994
  ]
  edge [
    source 23
    target 995
  ]
  edge [
    source 23
    target 996
  ]
  edge [
    source 23
    target 881
  ]
  edge [
    source 23
    target 997
  ]
  edge [
    source 23
    target 998
  ]
  edge [
    source 23
    target 999
  ]
  edge [
    source 23
    target 1000
  ]
  edge [
    source 23
    target 1001
  ]
  edge [
    source 23
    target 1002
  ]
  edge [
    source 23
    target 1003
  ]
  edge [
    source 23
    target 1004
  ]
  edge [
    source 23
    target 361
  ]
  edge [
    source 23
    target 1005
  ]
  edge [
    source 23
    target 1006
  ]
  edge [
    source 23
    target 1007
  ]
  edge [
    source 23
    target 1008
  ]
  edge [
    source 23
    target 1009
  ]
  edge [
    source 23
    target 1010
  ]
  edge [
    source 23
    target 1011
  ]
  edge [
    source 23
    target 1012
  ]
  edge [
    source 23
    target 1013
  ]
  edge [
    source 23
    target 1014
  ]
  edge [
    source 23
    target 1015
  ]
  edge [
    source 23
    target 1016
  ]
  edge [
    source 23
    target 1017
  ]
  edge [
    source 23
    target 1018
  ]
  edge [
    source 23
    target 1019
  ]
  edge [
    source 23
    target 1020
  ]
  edge [
    source 23
    target 1021
  ]
  edge [
    source 23
    target 1022
  ]
  edge [
    source 23
    target 1023
  ]
  edge [
    source 23
    target 1024
  ]
  edge [
    source 23
    target 1025
  ]
  edge [
    source 23
    target 1026
  ]
  edge [
    source 23
    target 1027
  ]
  edge [
    source 23
    target 1028
  ]
  edge [
    source 23
    target 1029
  ]
  edge [
    source 23
    target 1030
  ]
  edge [
    source 23
    target 1031
  ]
  edge [
    source 23
    target 1032
  ]
  edge [
    source 23
    target 1033
  ]
  edge [
    source 23
    target 1034
  ]
  edge [
    source 23
    target 1035
  ]
  edge [
    source 23
    target 1036
  ]
  edge [
    source 23
    target 1037
  ]
  edge [
    source 23
    target 1038
  ]
  edge [
    source 23
    target 1039
  ]
  edge [
    source 23
    target 1040
  ]
  edge [
    source 23
    target 1041
  ]
  edge [
    source 23
    target 1042
  ]
  edge [
    source 23
    target 1043
  ]
  edge [
    source 23
    target 593
  ]
  edge [
    source 23
    target 1044
  ]
  edge [
    source 23
    target 1045
  ]
  edge [
    source 23
    target 1046
  ]
  edge [
    source 23
    target 145
  ]
  edge [
    source 23
    target 1047
  ]
  edge [
    source 23
    target 1048
  ]
  edge [
    source 23
    target 1049
  ]
  edge [
    source 23
    target 1050
  ]
  edge [
    source 23
    target 1051
  ]
  edge [
    source 23
    target 1052
  ]
  edge [
    source 23
    target 1053
  ]
  edge [
    source 23
    target 1054
  ]
  edge [
    source 23
    target 1055
  ]
  edge [
    source 23
    target 1056
  ]
  edge [
    source 23
    target 1057
  ]
  edge [
    source 23
    target 1058
  ]
  edge [
    source 23
    target 672
  ]
  edge [
    source 23
    target 1059
  ]
  edge [
    source 23
    target 1060
  ]
  edge [
    source 23
    target 1061
  ]
  edge [
    source 23
    target 32
  ]
  edge [
    source 23
    target 48
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 111
  ]
  edge [
    source 25
    target 112
  ]
  edge [
    source 25
    target 113
  ]
  edge [
    source 25
    target 114
  ]
  edge [
    source 25
    target 115
  ]
  edge [
    source 25
    target 116
  ]
  edge [
    source 25
    target 117
  ]
  edge [
    source 25
    target 118
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 119
  ]
  edge [
    source 25
    target 120
  ]
  edge [
    source 25
    target 121
  ]
  edge [
    source 25
    target 122
  ]
  edge [
    source 25
    target 123
  ]
  edge [
    source 25
    target 124
  ]
  edge [
    source 25
    target 125
  ]
  edge [
    source 25
    target 126
  ]
  edge [
    source 25
    target 127
  ]
  edge [
    source 25
    target 128
  ]
  edge [
    source 25
    target 129
  ]
  edge [
    source 25
    target 130
  ]
  edge [
    source 25
    target 131
  ]
  edge [
    source 25
    target 132
  ]
  edge [
    source 25
    target 133
  ]
  edge [
    source 25
    target 134
  ]
  edge [
    source 25
    target 1062
  ]
  edge [
    source 25
    target 1063
  ]
  edge [
    source 25
    target 1064
  ]
  edge [
    source 25
    target 1065
  ]
  edge [
    source 25
    target 1066
  ]
  edge [
    source 25
    target 1067
  ]
  edge [
    source 25
    target 1068
  ]
  edge [
    source 25
    target 1069
  ]
  edge [
    source 25
    target 1070
  ]
  edge [
    source 25
    target 1071
  ]
  edge [
    source 25
    target 1072
  ]
  edge [
    source 25
    target 1073
  ]
  edge [
    source 25
    target 1074
  ]
  edge [
    source 25
    target 1075
  ]
  edge [
    source 25
    target 1076
  ]
  edge [
    source 25
    target 1077
  ]
  edge [
    source 25
    target 1078
  ]
  edge [
    source 25
    target 1079
  ]
  edge [
    source 25
    target 1080
  ]
  edge [
    source 25
    target 1081
  ]
  edge [
    source 25
    target 1082
  ]
  edge [
    source 25
    target 1083
  ]
  edge [
    source 25
    target 1084
  ]
  edge [
    source 25
    target 1085
  ]
  edge [
    source 25
    target 1086
  ]
  edge [
    source 25
    target 1087
  ]
  edge [
    source 25
    target 1088
  ]
  edge [
    source 25
    target 1089
  ]
  edge [
    source 25
    target 1090
  ]
  edge [
    source 25
    target 1091
  ]
  edge [
    source 25
    target 1092
  ]
  edge [
    source 25
    target 184
  ]
  edge [
    source 25
    target 1093
  ]
  edge [
    source 25
    target 1094
  ]
  edge [
    source 25
    target 67
  ]
  edge [
    source 25
    target 1095
  ]
  edge [
    source 25
    target 1096
  ]
  edge [
    source 25
    target 1097
  ]
  edge [
    source 25
    target 1098
  ]
  edge [
    source 25
    target 102
  ]
  edge [
    source 25
    target 1099
  ]
  edge [
    source 25
    target 1100
  ]
  edge [
    source 25
    target 1101
  ]
  edge [
    source 25
    target 1102
  ]
  edge [
    source 25
    target 1103
  ]
  edge [
    source 25
    target 1104
  ]
  edge [
    source 25
    target 1105
  ]
  edge [
    source 25
    target 1106
  ]
  edge [
    source 25
    target 1107
  ]
  edge [
    source 25
    target 1108
  ]
  edge [
    source 25
    target 1109
  ]
  edge [
    source 25
    target 1110
  ]
  edge [
    source 25
    target 1111
  ]
  edge [
    source 25
    target 905
  ]
  edge [
    source 25
    target 1112
  ]
  edge [
    source 25
    target 1113
  ]
  edge [
    source 25
    target 917
  ]
  edge [
    source 25
    target 1114
  ]
  edge [
    source 25
    target 1115
  ]
  edge [
    source 25
    target 1116
  ]
  edge [
    source 25
    target 1117
  ]
  edge [
    source 25
    target 1118
  ]
  edge [
    source 25
    target 1119
  ]
  edge [
    source 25
    target 1120
  ]
  edge [
    source 25
    target 1121
  ]
  edge [
    source 25
    target 1122
  ]
  edge [
    source 25
    target 1123
  ]
  edge [
    source 25
    target 738
  ]
  edge [
    source 25
    target 1124
  ]
  edge [
    source 25
    target 1125
  ]
  edge [
    source 25
    target 376
  ]
  edge [
    source 25
    target 1126
  ]
  edge [
    source 25
    target 1127
  ]
  edge [
    source 25
    target 1128
  ]
  edge [
    source 25
    target 1129
  ]
  edge [
    source 25
    target 1130
  ]
  edge [
    source 25
    target 1131
  ]
  edge [
    source 25
    target 1132
  ]
  edge [
    source 25
    target 1133
  ]
  edge [
    source 25
    target 1134
  ]
  edge [
    source 25
    target 1135
  ]
  edge [
    source 25
    target 1136
  ]
  edge [
    source 25
    target 1137
  ]
  edge [
    source 25
    target 1138
  ]
  edge [
    source 25
    target 1139
  ]
  edge [
    source 25
    target 1140
  ]
  edge [
    source 25
    target 1141
  ]
  edge [
    source 25
    target 1142
  ]
  edge [
    source 25
    target 1143
  ]
  edge [
    source 25
    target 1144
  ]
  edge [
    source 25
    target 1145
  ]
  edge [
    source 25
    target 1146
  ]
  edge [
    source 25
    target 881
  ]
  edge [
    source 25
    target 1147
  ]
  edge [
    source 25
    target 548
  ]
  edge [
    source 25
    target 1148
  ]
  edge [
    source 25
    target 1149
  ]
  edge [
    source 25
    target 1150
  ]
  edge [
    source 25
    target 1151
  ]
  edge [
    source 25
    target 895
  ]
  edge [
    source 25
    target 1152
  ]
  edge [
    source 25
    target 1153
  ]
  edge [
    source 25
    target 1154
  ]
  edge [
    source 25
    target 1155
  ]
  edge [
    source 25
    target 1156
  ]
  edge [
    source 25
    target 1157
  ]
  edge [
    source 25
    target 1158
  ]
  edge [
    source 25
    target 1159
  ]
  edge [
    source 25
    target 1160
  ]
  edge [
    source 25
    target 1161
  ]
  edge [
    source 25
    target 1162
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 1163
  ]
  edge [
    source 25
    target 1164
  ]
  edge [
    source 25
    target 1165
  ]
  edge [
    source 25
    target 1166
  ]
  edge [
    source 25
    target 1167
  ]
  edge [
    source 25
    target 1168
  ]
  edge [
    source 25
    target 1169
  ]
  edge [
    source 25
    target 1170
  ]
  edge [
    source 25
    target 1171
  ]
  edge [
    source 25
    target 1172
  ]
  edge [
    source 25
    target 1173
  ]
  edge [
    source 25
    target 1174
  ]
  edge [
    source 25
    target 1175
  ]
  edge [
    source 25
    target 1176
  ]
  edge [
    source 25
    target 1177
  ]
  edge [
    source 25
    target 1178
  ]
  edge [
    source 25
    target 1179
  ]
  edge [
    source 25
    target 1180
  ]
  edge [
    source 25
    target 1181
  ]
  edge [
    source 25
    target 655
  ]
  edge [
    source 25
    target 1182
  ]
  edge [
    source 25
    target 1183
  ]
  edge [
    source 25
    target 1184
  ]
  edge [
    source 25
    target 1185
  ]
  edge [
    source 25
    target 1186
  ]
  edge [
    source 25
    target 806
  ]
  edge [
    source 25
    target 1187
  ]
  edge [
    source 25
    target 1188
  ]
  edge [
    source 25
    target 1189
  ]
  edge [
    source 25
    target 1190
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 25
    target 1191
  ]
  edge [
    source 25
    target 1192
  ]
  edge [
    source 25
    target 1193
  ]
  edge [
    source 25
    target 1194
  ]
  edge [
    source 25
    target 812
  ]
  edge [
    source 25
    target 1195
  ]
  edge [
    source 25
    target 1196
  ]
  edge [
    source 25
    target 1197
  ]
  edge [
    source 25
    target 1198
  ]
  edge [
    source 25
    target 646
  ]
  edge [
    source 25
    target 1199
  ]
  edge [
    source 25
    target 1200
  ]
  edge [
    source 25
    target 787
  ]
  edge [
    source 25
    target 1201
  ]
  edge [
    source 25
    target 1202
  ]
  edge [
    source 25
    target 1203
  ]
  edge [
    source 25
    target 1204
  ]
  edge [
    source 25
    target 1205
  ]
  edge [
    source 25
    target 1206
  ]
  edge [
    source 25
    target 1207
  ]
  edge [
    source 25
    target 1208
  ]
  edge [
    source 25
    target 1209
  ]
  edge [
    source 25
    target 1210
  ]
  edge [
    source 25
    target 1211
  ]
  edge [
    source 25
    target 651
  ]
  edge [
    source 25
    target 1212
  ]
  edge [
    source 25
    target 1213
  ]
  edge [
    source 25
    target 1214
  ]
  edge [
    source 25
    target 1215
  ]
  edge [
    source 25
    target 1216
  ]
  edge [
    source 25
    target 676
  ]
  edge [
    source 25
    target 1217
  ]
  edge [
    source 25
    target 964
  ]
  edge [
    source 25
    target 593
  ]
  edge [
    source 25
    target 196
  ]
  edge [
    source 25
    target 1218
  ]
  edge [
    source 25
    target 891
  ]
  edge [
    source 25
    target 1219
  ]
  edge [
    source 25
    target 1220
  ]
  edge [
    source 25
    target 1221
  ]
  edge [
    source 25
    target 717
  ]
  edge [
    source 25
    target 887
  ]
  edge [
    source 25
    target 1222
  ]
  edge [
    source 25
    target 1223
  ]
  edge [
    source 25
    target 1224
  ]
  edge [
    source 25
    target 1060
  ]
  edge [
    source 25
    target 1225
  ]
  edge [
    source 25
    target 1226
  ]
  edge [
    source 25
    target 1227
  ]
  edge [
    source 25
    target 1228
  ]
  edge [
    source 25
    target 1229
  ]
  edge [
    source 25
    target 1230
  ]
  edge [
    source 25
    target 1231
  ]
  edge [
    source 25
    target 1232
  ]
  edge [
    source 25
    target 1233
  ]
  edge [
    source 25
    target 1234
  ]
  edge [
    source 25
    target 1235
  ]
  edge [
    source 25
    target 1236
  ]
  edge [
    source 25
    target 1237
  ]
  edge [
    source 25
    target 1238
  ]
  edge [
    source 25
    target 1239
  ]
  edge [
    source 25
    target 1240
  ]
  edge [
    source 25
    target 1241
  ]
  edge [
    source 25
    target 1242
  ]
  edge [
    source 25
    target 1243
  ]
  edge [
    source 25
    target 1244
  ]
  edge [
    source 25
    target 1245
  ]
  edge [
    source 25
    target 1246
  ]
  edge [
    source 25
    target 1247
  ]
  edge [
    source 25
    target 1248
  ]
  edge [
    source 25
    target 1249
  ]
  edge [
    source 25
    target 1250
  ]
  edge [
    source 25
    target 1251
  ]
  edge [
    source 25
    target 1252
  ]
  edge [
    source 25
    target 1253
  ]
  edge [
    source 25
    target 1254
  ]
  edge [
    source 25
    target 1255
  ]
  edge [
    source 25
    target 1256
  ]
  edge [
    source 25
    target 1257
  ]
  edge [
    source 25
    target 1258
  ]
  edge [
    source 25
    target 1259
  ]
  edge [
    source 25
    target 1260
  ]
  edge [
    source 25
    target 1261
  ]
  edge [
    source 25
    target 1262
  ]
  edge [
    source 25
    target 1263
  ]
  edge [
    source 25
    target 1264
  ]
  edge [
    source 25
    target 1265
  ]
  edge [
    source 25
    target 1266
  ]
  edge [
    source 25
    target 1267
  ]
  edge [
    source 25
    target 1268
  ]
  edge [
    source 25
    target 1269
  ]
  edge [
    source 25
    target 1270
  ]
  edge [
    source 25
    target 1271
  ]
  edge [
    source 25
    target 1272
  ]
  edge [
    source 25
    target 1273
  ]
  edge [
    source 25
    target 1274
  ]
  edge [
    source 25
    target 51
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 905
  ]
  edge [
    source 27
    target 1275
  ]
  edge [
    source 27
    target 1276
  ]
  edge [
    source 27
    target 912
  ]
  edge [
    source 27
    target 1277
  ]
  edge [
    source 27
    target 908
  ]
  edge [
    source 27
    target 1278
  ]
  edge [
    source 27
    target 1279
  ]
  edge [
    source 27
    target 1280
  ]
  edge [
    source 27
    target 976
  ]
  edge [
    source 27
    target 1281
  ]
  edge [
    source 27
    target 1282
  ]
  edge [
    source 27
    target 1014
  ]
  edge [
    source 27
    target 1283
  ]
  edge [
    source 27
    target 1284
  ]
  edge [
    source 27
    target 1011
  ]
  edge [
    source 27
    target 1285
  ]
  edge [
    source 27
    target 1286
  ]
  edge [
    source 27
    target 1287
  ]
  edge [
    source 27
    target 1288
  ]
  edge [
    source 27
    target 1289
  ]
  edge [
    source 27
    target 1290
  ]
  edge [
    source 27
    target 1291
  ]
  edge [
    source 27
    target 1292
  ]
  edge [
    source 27
    target 1293
  ]
  edge [
    source 27
    target 913
  ]
  edge [
    source 27
    target 1294
  ]
  edge [
    source 27
    target 1295
  ]
  edge [
    source 27
    target 1296
  ]
  edge [
    source 27
    target 933
  ]
  edge [
    source 27
    target 1297
  ]
  edge [
    source 27
    target 1298
  ]
  edge [
    source 27
    target 1299
  ]
  edge [
    source 27
    target 1300
  ]
  edge [
    source 27
    target 1301
  ]
  edge [
    source 27
    target 984
  ]
  edge [
    source 27
    target 985
  ]
  edge [
    source 27
    target 986
  ]
  edge [
    source 27
    target 987
  ]
  edge [
    source 27
    target 988
  ]
  edge [
    source 27
    target 989
  ]
  edge [
    source 27
    target 990
  ]
  edge [
    source 27
    target 991
  ]
  edge [
    source 27
    target 992
  ]
  edge [
    source 27
    target 993
  ]
  edge [
    source 27
    target 994
  ]
  edge [
    source 27
    target 995
  ]
  edge [
    source 27
    target 923
  ]
  edge [
    source 27
    target 924
  ]
  edge [
    source 27
    target 925
  ]
  edge [
    source 27
    target 926
  ]
  edge [
    source 27
    target 927
  ]
  edge [
    source 27
    target 928
  ]
  edge [
    source 27
    target 929
  ]
  edge [
    source 27
    target 930
  ]
  edge [
    source 27
    target 646
  ]
  edge [
    source 27
    target 931
  ]
  edge [
    source 27
    target 932
  ]
  edge [
    source 27
    target 934
  ]
  edge [
    source 27
    target 935
  ]
  edge [
    source 27
    target 936
  ]
  edge [
    source 27
    target 937
  ]
  edge [
    source 27
    target 938
  ]
  edge [
    source 27
    target 939
  ]
  edge [
    source 27
    target 940
  ]
  edge [
    source 27
    target 941
  ]
  edge [
    source 27
    target 942
  ]
  edge [
    source 27
    target 1302
  ]
  edge [
    source 27
    target 1303
  ]
  edge [
    source 27
    target 1304
  ]
  edge [
    source 27
    target 1305
  ]
  edge [
    source 27
    target 1199
  ]
  edge [
    source 28
    target 1067
  ]
  edge [
    source 28
    target 1068
  ]
  edge [
    source 28
    target 1069
  ]
  edge [
    source 28
    target 1070
  ]
  edge [
    source 28
    target 113
  ]
  edge [
    source 28
    target 1071
  ]
  edge [
    source 28
    target 1072
  ]
  edge [
    source 28
    target 118
  ]
  edge [
    source 28
    target 1073
  ]
  edge [
    source 28
    target 1074
  ]
  edge [
    source 28
    target 119
  ]
  edge [
    source 28
    target 1075
  ]
  edge [
    source 28
    target 122
  ]
  edge [
    source 28
    target 123
  ]
  edge [
    source 28
    target 1076
  ]
  edge [
    source 28
    target 1077
  ]
  edge [
    source 28
    target 1078
  ]
  edge [
    source 28
    target 1079
  ]
  edge [
    source 28
    target 1080
  ]
  edge [
    source 28
    target 1081
  ]
  edge [
    source 28
    target 1082
  ]
  edge [
    source 28
    target 128
  ]
  edge [
    source 28
    target 1083
  ]
  edge [
    source 28
    target 1084
  ]
  edge [
    source 28
    target 1085
  ]
  edge [
    source 28
    target 130
  ]
  edge [
    source 28
    target 131
  ]
  edge [
    source 28
    target 1086
  ]
  edge [
    source 28
    target 132
  ]
  edge [
    source 28
    target 133
  ]
  edge [
    source 28
    target 1087
  ]
  edge [
    source 28
    target 134
  ]
  edge [
    source 28
    target 1088
  ]
  edge [
    source 28
    target 1089
  ]
  edge [
    source 28
    target 1090
  ]
  edge [
    source 28
    target 1091
  ]
  edge [
    source 28
    target 1092
  ]
  edge [
    source 28
    target 1306
  ]
  edge [
    source 28
    target 1307
  ]
  edge [
    source 28
    target 1241
  ]
  edge [
    source 28
    target 881
  ]
  edge [
    source 28
    target 1308
  ]
  edge [
    source 28
    target 1309
  ]
  edge [
    source 28
    target 1238
  ]
  edge [
    source 28
    target 1136
  ]
  edge [
    source 28
    target 1137
  ]
  edge [
    source 28
    target 1138
  ]
  edge [
    source 28
    target 1139
  ]
  edge [
    source 28
    target 1140
  ]
  edge [
    source 28
    target 1141
  ]
  edge [
    source 28
    target 1142
  ]
  edge [
    source 28
    target 1143
  ]
  edge [
    source 28
    target 1144
  ]
  edge [
    source 28
    target 1145
  ]
  edge [
    source 28
    target 1146
  ]
  edge [
    source 28
    target 1147
  ]
  edge [
    source 28
    target 548
  ]
  edge [
    source 28
    target 1148
  ]
  edge [
    source 28
    target 1149
  ]
  edge [
    source 28
    target 1150
  ]
  edge [
    source 28
    target 1151
  ]
  edge [
    source 28
    target 895
  ]
  edge [
    source 28
    target 1152
  ]
  edge [
    source 28
    target 1153
  ]
  edge [
    source 28
    target 1154
  ]
  edge [
    source 28
    target 1310
  ]
  edge [
    source 28
    target 1311
  ]
  edge [
    source 28
    target 1312
  ]
  edge [
    source 28
    target 1313
  ]
  edge [
    source 28
    target 1314
  ]
  edge [
    source 28
    target 1315
  ]
  edge [
    source 28
    target 1316
  ]
  edge [
    source 28
    target 1317
  ]
  edge [
    source 28
    target 1021
  ]
  edge [
    source 28
    target 1318
  ]
  edge [
    source 28
    target 1319
  ]
  edge [
    source 28
    target 52
  ]
  edge [
    source 28
    target 1320
  ]
  edge [
    source 28
    target 1207
  ]
  edge [
    source 28
    target 1208
  ]
  edge [
    source 28
    target 1209
  ]
  edge [
    source 28
    target 1198
  ]
  edge [
    source 28
    target 1103
  ]
  edge [
    source 28
    target 646
  ]
  edge [
    source 28
    target 1199
  ]
  edge [
    source 28
    target 1200
  ]
  edge [
    source 28
    target 787
  ]
  edge [
    source 28
    target 1201
  ]
  edge [
    source 28
    target 1202
  ]
  edge [
    source 28
    target 1203
  ]
  edge [
    source 28
    target 1204
  ]
  edge [
    source 28
    target 1205
  ]
  edge [
    source 28
    target 1166
  ]
  edge [
    source 28
    target 1167
  ]
  edge [
    source 28
    target 1168
  ]
  edge [
    source 28
    target 1169
  ]
  edge [
    source 28
    target 1170
  ]
  edge [
    source 28
    target 1171
  ]
  edge [
    source 28
    target 1172
  ]
  edge [
    source 28
    target 1173
  ]
  edge [
    source 28
    target 1174
  ]
  edge [
    source 28
    target 1175
  ]
  edge [
    source 28
    target 1176
  ]
  edge [
    source 28
    target 1177
  ]
  edge [
    source 28
    target 1178
  ]
  edge [
    source 28
    target 1179
  ]
  edge [
    source 28
    target 1180
  ]
  edge [
    source 28
    target 1181
  ]
  edge [
    source 28
    target 655
  ]
  edge [
    source 28
    target 1182
  ]
  edge [
    source 28
    target 1183
  ]
  edge [
    source 28
    target 1184
  ]
  edge [
    source 28
    target 1185
  ]
  edge [
    source 28
    target 1186
  ]
  edge [
    source 28
    target 806
  ]
  edge [
    source 28
    target 1187
  ]
  edge [
    source 28
    target 1188
  ]
  edge [
    source 28
    target 1189
  ]
  edge [
    source 28
    target 1190
  ]
  edge [
    source 28
    target 223
  ]
  edge [
    source 28
    target 1191
  ]
  edge [
    source 28
    target 1192
  ]
  edge [
    source 28
    target 1193
  ]
  edge [
    source 28
    target 1194
  ]
  edge [
    source 28
    target 812
  ]
  edge [
    source 28
    target 1195
  ]
  edge [
    source 28
    target 1196
  ]
  edge [
    source 28
    target 1206
  ]
  edge [
    source 28
    target 1210
  ]
  edge [
    source 28
    target 1211
  ]
  edge [
    source 28
    target 651
  ]
  edge [
    source 28
    target 1212
  ]
  edge [
    source 28
    target 1213
  ]
  edge [
    source 28
    target 1214
  ]
  edge [
    source 28
    target 1215
  ]
  edge [
    source 28
    target 1216
  ]
  edge [
    source 28
    target 676
  ]
  edge [
    source 28
    target 1217
  ]
  edge [
    source 28
    target 964
  ]
  edge [
    source 28
    target 593
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 1218
  ]
  edge [
    source 28
    target 891
  ]
  edge [
    source 28
    target 1219
  ]
  edge [
    source 28
    target 1220
  ]
  edge [
    source 28
    target 1221
  ]
  edge [
    source 28
    target 1164
  ]
  edge [
    source 28
    target 717
  ]
  edge [
    source 28
    target 887
  ]
  edge [
    source 28
    target 1222
  ]
  edge [
    source 28
    target 1223
  ]
  edge [
    source 28
    target 1224
  ]
  edge [
    source 28
    target 1060
  ]
  edge [
    source 28
    target 1225
  ]
  edge [
    source 28
    target 1226
  ]
  edge [
    source 28
    target 1197
  ]
  edge [
    source 28
    target 1321
  ]
  edge [
    source 28
    target 1322
  ]
  edge [
    source 28
    target 1323
  ]
  edge [
    source 28
    target 1233
  ]
  edge [
    source 28
    target 1234
  ]
  edge [
    source 28
    target 1235
  ]
  edge [
    source 28
    target 1236
  ]
  edge [
    source 28
    target 1237
  ]
  edge [
    source 28
    target 1239
  ]
  edge [
    source 28
    target 1240
  ]
  edge [
    source 28
    target 1242
  ]
  edge [
    source 28
    target 1243
  ]
  edge [
    source 28
    target 1244
  ]
  edge [
    source 28
    target 1245
  ]
  edge [
    source 28
    target 1246
  ]
  edge [
    source 28
    target 1247
  ]
  edge [
    source 28
    target 1248
  ]
  edge [
    source 28
    target 1249
  ]
  edge [
    source 28
    target 1250
  ]
  edge [
    source 28
    target 1251
  ]
  edge [
    source 28
    target 1252
  ]
  edge [
    source 28
    target 1253
  ]
  edge [
    source 28
    target 1254
  ]
  edge [
    source 28
    target 1255
  ]
  edge [
    source 28
    target 1256
  ]
  edge [
    source 28
    target 1257
  ]
  edge [
    source 28
    target 1258
  ]
  edge [
    source 28
    target 1259
  ]
  edge [
    source 28
    target 1260
  ]
  edge [
    source 28
    target 1261
  ]
  edge [
    source 28
    target 1262
  ]
  edge [
    source 28
    target 1263
  ]
  edge [
    source 28
    target 1264
  ]
  edge [
    source 28
    target 1265
  ]
  edge [
    source 28
    target 1266
  ]
  edge [
    source 28
    target 1267
  ]
  edge [
    source 28
    target 1268
  ]
  edge [
    source 28
    target 1269
  ]
  edge [
    source 28
    target 1270
  ]
  edge [
    source 28
    target 1271
  ]
  edge [
    source 28
    target 1272
  ]
  edge [
    source 28
    target 1273
  ]
  edge [
    source 28
    target 1274
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 42
  ]
  edge [
    source 29
    target 43
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 45
  ]
  edge [
    source 31
    target 46
  ]
  edge [
    source 31
    target 52
  ]
  edge [
    source 31
    target 1324
  ]
  edge [
    source 31
    target 1325
  ]
  edge [
    source 31
    target 1326
  ]
  edge [
    source 31
    target 1327
  ]
  edge [
    source 31
    target 1328
  ]
  edge [
    source 31
    target 1329
  ]
  edge [
    source 31
    target 995
  ]
  edge [
    source 31
    target 1330
  ]
  edge [
    source 31
    target 1331
  ]
  edge [
    source 31
    target 1332
  ]
  edge [
    source 31
    target 1333
  ]
  edge [
    source 31
    target 1334
  ]
  edge [
    source 31
    target 1335
  ]
  edge [
    source 31
    target 1075
  ]
  edge [
    source 31
    target 1336
  ]
  edge [
    source 31
    target 1337
  ]
  edge [
    source 31
    target 1338
  ]
  edge [
    source 31
    target 1339
  ]
  edge [
    source 31
    target 1340
  ]
  edge [
    source 31
    target 1341
  ]
  edge [
    source 31
    target 1342
  ]
  edge [
    source 31
    target 1343
  ]
  edge [
    source 31
    target 1344
  ]
  edge [
    source 31
    target 1081
  ]
  edge [
    source 31
    target 1345
  ]
  edge [
    source 31
    target 1346
  ]
  edge [
    source 31
    target 1347
  ]
  edge [
    source 31
    target 1348
  ]
  edge [
    source 31
    target 1349
  ]
  edge [
    source 31
    target 1350
  ]
  edge [
    source 31
    target 1351
  ]
  edge [
    source 31
    target 1352
  ]
  edge [
    source 31
    target 1353
  ]
  edge [
    source 31
    target 1354
  ]
  edge [
    source 31
    target 1355
  ]
  edge [
    source 31
    target 1356
  ]
  edge [
    source 31
    target 1357
  ]
  edge [
    source 31
    target 1358
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 913
  ]
  edge [
    source 32
    target 1359
  ]
  edge [
    source 32
    target 1360
  ]
  edge [
    source 32
    target 1361
  ]
  edge [
    source 32
    target 1362
  ]
  edge [
    source 32
    target 1363
  ]
  edge [
    source 32
    target 1364
  ]
  edge [
    source 32
    target 777
  ]
  edge [
    source 32
    target 1151
  ]
  edge [
    source 32
    target 967
  ]
  edge [
    source 32
    target 968
  ]
  edge [
    source 32
    target 969
  ]
  edge [
    source 32
    target 970
  ]
  edge [
    source 32
    target 971
  ]
  edge [
    source 32
    target 972
  ]
  edge [
    source 32
    target 973
  ]
  edge [
    source 32
    target 974
  ]
  edge [
    source 32
    target 975
  ]
  edge [
    source 32
    target 976
  ]
  edge [
    source 32
    target 977
  ]
  edge [
    source 32
    target 1365
  ]
  edge [
    source 32
    target 1366
  ]
  edge [
    source 32
    target 1367
  ]
  edge [
    source 32
    target 1368
  ]
  edge [
    source 32
    target 1369
  ]
  edge [
    source 32
    target 1370
  ]
  edge [
    source 32
    target 905
  ]
  edge [
    source 32
    target 1371
  ]
  edge [
    source 32
    target 1372
  ]
  edge [
    source 32
    target 1373
  ]
  edge [
    source 32
    target 1374
  ]
  edge [
    source 32
    target 1375
  ]
  edge [
    source 32
    target 1376
  ]
  edge [
    source 32
    target 1377
  ]
  edge [
    source 32
    target 1378
  ]
  edge [
    source 32
    target 1379
  ]
  edge [
    source 32
    target 48
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1380
  ]
  edge [
    source 33
    target 1381
  ]
  edge [
    source 33
    target 1382
  ]
  edge [
    source 33
    target 1383
  ]
  edge [
    source 33
    target 1384
  ]
  edge [
    source 33
    target 1385
  ]
  edge [
    source 33
    target 671
  ]
  edge [
    source 33
    target 1386
  ]
  edge [
    source 33
    target 1387
  ]
  edge [
    source 33
    target 1388
  ]
  edge [
    source 33
    target 1389
  ]
  edge [
    source 33
    target 1298
  ]
  edge [
    source 33
    target 592
  ]
  edge [
    source 33
    target 1390
  ]
  edge [
    source 33
    target 711
  ]
  edge [
    source 33
    target 1391
  ]
  edge [
    source 33
    target 1392
  ]
  edge [
    source 33
    target 1393
  ]
  edge [
    source 33
    target 717
  ]
  edge [
    source 33
    target 1394
  ]
  edge [
    source 33
    target 1395
  ]
  edge [
    source 33
    target 1396
  ]
  edge [
    source 33
    target 1397
  ]
  edge [
    source 33
    target 1398
  ]
  edge [
    source 33
    target 1399
  ]
  edge [
    source 33
    target 1400
  ]
  edge [
    source 33
    target 1401
  ]
  edge [
    source 33
    target 1402
  ]
  edge [
    source 33
    target 1403
  ]
  edge [
    source 33
    target 1404
  ]
  edge [
    source 33
    target 1405
  ]
  edge [
    source 33
    target 705
  ]
  edge [
    source 33
    target 1406
  ]
  edge [
    source 33
    target 1407
  ]
  edge [
    source 33
    target 1408
  ]
  edge [
    source 33
    target 1409
  ]
  edge [
    source 33
    target 1410
  ]
  edge [
    source 33
    target 1411
  ]
  edge [
    source 33
    target 1412
  ]
  edge [
    source 33
    target 1413
  ]
  edge [
    source 33
    target 1414
  ]
  edge [
    source 33
    target 531
  ]
  edge [
    source 33
    target 1415
  ]
  edge [
    source 33
    target 675
  ]
  edge [
    source 33
    target 676
  ]
  edge [
    source 33
    target 677
  ]
  edge [
    source 33
    target 678
  ]
  edge [
    source 33
    target 679
  ]
  edge [
    source 33
    target 680
  ]
  edge [
    source 33
    target 681
  ]
  edge [
    source 33
    target 682
  ]
  edge [
    source 33
    target 683
  ]
  edge [
    source 33
    target 684
  ]
  edge [
    source 33
    target 685
  ]
  edge [
    source 33
    target 686
  ]
  edge [
    source 33
    target 687
  ]
  edge [
    source 33
    target 688
  ]
  edge [
    source 33
    target 1416
  ]
  edge [
    source 33
    target 1417
  ]
  edge [
    source 33
    target 1145
  ]
  edge [
    source 33
    target 1418
  ]
  edge [
    source 33
    target 1419
  ]
  edge [
    source 33
    target 577
  ]
  edge [
    source 33
    target 1420
  ]
  edge [
    source 33
    target 1421
  ]
  edge [
    source 33
    target 1422
  ]
  edge [
    source 33
    target 1423
  ]
  edge [
    source 33
    target 1424
  ]
  edge [
    source 33
    target 1425
  ]
  edge [
    source 33
    target 1426
  ]
  edge [
    source 33
    target 1427
  ]
  edge [
    source 33
    target 1428
  ]
  edge [
    source 33
    target 1429
  ]
  edge [
    source 33
    target 1176
  ]
  edge [
    source 33
    target 1430
  ]
  edge [
    source 33
    target 1431
  ]
  edge [
    source 33
    target 1432
  ]
  edge [
    source 33
    target 1433
  ]
  edge [
    source 33
    target 1434
  ]
  edge [
    source 33
    target 1435
  ]
  edge [
    source 33
    target 1436
  ]
  edge [
    source 33
    target 1437
  ]
  edge [
    source 33
    target 839
  ]
  edge [
    source 33
    target 1438
  ]
  edge [
    source 33
    target 1439
  ]
  edge [
    source 33
    target 1440
  ]
  edge [
    source 33
    target 1441
  ]
  edge [
    source 33
    target 1442
  ]
  edge [
    source 33
    target 1443
  ]
  edge [
    source 33
    target 1444
  ]
  edge [
    source 33
    target 1445
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1446
  ]
  edge [
    source 34
    target 881
  ]
  edge [
    source 34
    target 1447
  ]
  edge [
    source 34
    target 1448
  ]
  edge [
    source 34
    target 1449
  ]
  edge [
    source 34
    target 1450
  ]
  edge [
    source 34
    target 1451
  ]
  edge [
    source 34
    target 1452
  ]
  edge [
    source 34
    target 1453
  ]
  edge [
    source 34
    target 1454
  ]
  edge [
    source 34
    target 1455
  ]
  edge [
    source 34
    target 787
  ]
  edge [
    source 34
    target 1456
  ]
  edge [
    source 34
    target 1457
  ]
  edge [
    source 34
    target 1458
  ]
  edge [
    source 34
    target 1459
  ]
  edge [
    source 34
    target 1460
  ]
  edge [
    source 34
    target 1461
  ]
  edge [
    source 34
    target 1462
  ]
  edge [
    source 34
    target 1149
  ]
  edge [
    source 34
    target 1463
  ]
  edge [
    source 34
    target 1464
  ]
  edge [
    source 34
    target 1465
  ]
  edge [
    source 34
    target 1466
  ]
  edge [
    source 34
    target 1467
  ]
  edge [
    source 34
    target 1468
  ]
  edge [
    source 34
    target 1469
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1470
  ]
  edge [
    source 35
    target 1471
  ]
  edge [
    source 35
    target 1472
  ]
  edge [
    source 35
    target 1473
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1474
  ]
  edge [
    source 36
    target 1475
  ]
  edge [
    source 36
    target 881
  ]
  edge [
    source 36
    target 1476
  ]
  edge [
    source 36
    target 1477
  ]
  edge [
    source 36
    target 610
  ]
  edge [
    source 36
    target 1478
  ]
  edge [
    source 36
    target 1479
  ]
  edge [
    source 36
    target 1480
  ]
  edge [
    source 36
    target 1481
  ]
  edge [
    source 36
    target 1044
  ]
  edge [
    source 36
    target 1482
  ]
  edge [
    source 36
    target 1483
  ]
  edge [
    source 36
    target 1149
  ]
  edge [
    source 36
    target 1463
  ]
  edge [
    source 36
    target 1464
  ]
  edge [
    source 36
    target 1465
  ]
  edge [
    source 36
    target 1466
  ]
  edge [
    source 36
    target 1467
  ]
  edge [
    source 36
    target 1468
  ]
  edge [
    source 36
    target 1484
  ]
  edge [
    source 36
    target 1485
  ]
  edge [
    source 36
    target 1486
  ]
  edge [
    source 36
    target 1241
  ]
  edge [
    source 36
    target 1487
  ]
  edge [
    source 36
    target 1488
  ]
  edge [
    source 36
    target 1489
  ]
  edge [
    source 36
    target 1165
  ]
  edge [
    source 36
    target 1490
  ]
  edge [
    source 36
    target 705
  ]
  edge [
    source 36
    target 1491
  ]
  edge [
    source 36
    target 1167
  ]
  edge [
    source 36
    target 1492
  ]
  edge [
    source 36
    target 1493
  ]
  edge [
    source 36
    target 1494
  ]
  edge [
    source 36
    target 972
  ]
  edge [
    source 36
    target 1495
  ]
  edge [
    source 36
    target 1303
  ]
  edge [
    source 36
    target 1496
  ]
  edge [
    source 36
    target 1497
  ]
  edge [
    source 36
    target 1498
  ]
  edge [
    source 36
    target 1499
  ]
  edge [
    source 36
    target 1162
  ]
  edge [
    source 36
    target 790
  ]
  edge [
    source 36
    target 1500
  ]
  edge [
    source 36
    target 1501
  ]
  edge [
    source 36
    target 1502
  ]
  edge [
    source 36
    target 687
  ]
  edge [
    source 36
    target 1503
  ]
  edge [
    source 36
    target 347
  ]
  edge [
    source 36
    target 92
  ]
  edge [
    source 36
    target 1504
  ]
  edge [
    source 36
    target 1505
  ]
  edge [
    source 36
    target 532
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 361
  ]
  edge [
    source 37
    target 1476
  ]
  edge [
    source 37
    target 1506
  ]
  edge [
    source 37
    target 1507
  ]
  edge [
    source 37
    target 1508
  ]
  edge [
    source 37
    target 1509
  ]
  edge [
    source 37
    target 1510
  ]
  edge [
    source 37
    target 1511
  ]
  edge [
    source 37
    target 1173
  ]
  edge [
    source 37
    target 1512
  ]
  edge [
    source 37
    target 352
  ]
  edge [
    source 37
    target 1513
  ]
  edge [
    source 37
    target 1514
  ]
  edge [
    source 37
    target 973
  ]
  edge [
    source 37
    target 1515
  ]
  edge [
    source 37
    target 658
  ]
  edge [
    source 37
    target 533
  ]
  edge [
    source 37
    target 1516
  ]
  edge [
    source 37
    target 1517
  ]
  edge [
    source 37
    target 1518
  ]
  edge [
    source 37
    target 1519
  ]
  edge [
    source 37
    target 1520
  ]
  edge [
    source 37
    target 1521
  ]
  edge [
    source 37
    target 1522
  ]
  edge [
    source 37
    target 1523
  ]
  edge [
    source 37
    target 1524
  ]
  edge [
    source 37
    target 376
  ]
  edge [
    source 37
    target 1525
  ]
  edge [
    source 37
    target 1526
  ]
  edge [
    source 37
    target 554
  ]
  edge [
    source 37
    target 1241
  ]
  edge [
    source 37
    target 1487
  ]
  edge [
    source 37
    target 1488
  ]
  edge [
    source 37
    target 1489
  ]
  edge [
    source 37
    target 1165
  ]
  edge [
    source 37
    target 1490
  ]
  edge [
    source 37
    target 705
  ]
  edge [
    source 37
    target 1527
  ]
  edge [
    source 37
    target 1528
  ]
  edge [
    source 37
    target 1529
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 73
  ]
  edge [
    source 38
    target 1530
  ]
  edge [
    source 38
    target 1531
  ]
  edge [
    source 38
    target 1532
  ]
  edge [
    source 38
    target 1533
  ]
  edge [
    source 38
    target 1534
  ]
  edge [
    source 38
    target 1535
  ]
  edge [
    source 38
    target 1536
  ]
  edge [
    source 38
    target 1537
  ]
  edge [
    source 38
    target 1538
  ]
  edge [
    source 38
    target 1539
  ]
  edge [
    source 38
    target 45
  ]
  edge [
    source 38
    target 787
  ]
  edge [
    source 38
    target 1540
  ]
  edge [
    source 38
    target 1541
  ]
  edge [
    source 38
    target 1542
  ]
  edge [
    source 38
    target 1543
  ]
  edge [
    source 38
    target 1544
  ]
  edge [
    source 38
    target 1545
  ]
  edge [
    source 38
    target 236
  ]
  edge [
    source 38
    target 237
  ]
  edge [
    source 38
    target 238
  ]
  edge [
    source 38
    target 239
  ]
  edge [
    source 38
    target 240
  ]
  edge [
    source 38
    target 241
  ]
  edge [
    source 38
    target 242
  ]
  edge [
    source 38
    target 243
  ]
  edge [
    source 38
    target 245
  ]
  edge [
    source 38
    target 249
  ]
  edge [
    source 38
    target 244
  ]
  edge [
    source 38
    target 248
  ]
  edge [
    source 38
    target 246
  ]
  edge [
    source 38
    target 247
  ]
  edge [
    source 38
    target 250
  ]
  edge [
    source 38
    target 251
  ]
  edge [
    source 38
    target 252
  ]
  edge [
    source 38
    target 253
  ]
  edge [
    source 38
    target 254
  ]
  edge [
    source 38
    target 255
  ]
  edge [
    source 38
    target 256
  ]
  edge [
    source 38
    target 257
  ]
  edge [
    source 38
    target 258
  ]
  edge [
    source 38
    target 259
  ]
  edge [
    source 38
    target 261
  ]
  edge [
    source 38
    target 260
  ]
  edge [
    source 38
    target 262
  ]
  edge [
    source 38
    target 263
  ]
  edge [
    source 38
    target 264
  ]
  edge [
    source 38
    target 265
  ]
  edge [
    source 38
    target 266
  ]
  edge [
    source 38
    target 267
  ]
  edge [
    source 38
    target 268
  ]
  edge [
    source 38
    target 269
  ]
  edge [
    source 38
    target 270
  ]
  edge [
    source 38
    target 271
  ]
  edge [
    source 38
    target 272
  ]
  edge [
    source 38
    target 273
  ]
  edge [
    source 38
    target 274
  ]
  edge [
    source 38
    target 275
  ]
  edge [
    source 38
    target 276
  ]
  edge [
    source 38
    target 277
  ]
  edge [
    source 38
    target 278
  ]
  edge [
    source 38
    target 279
  ]
  edge [
    source 38
    target 280
  ]
  edge [
    source 38
    target 281
  ]
  edge [
    source 38
    target 283
  ]
  edge [
    source 38
    target 282
  ]
  edge [
    source 38
    target 284
  ]
  edge [
    source 38
    target 285
  ]
  edge [
    source 38
    target 286
  ]
  edge [
    source 38
    target 287
  ]
  edge [
    source 38
    target 288
  ]
  edge [
    source 38
    target 289
  ]
  edge [
    source 38
    target 290
  ]
  edge [
    source 38
    target 291
  ]
  edge [
    source 38
    target 292
  ]
  edge [
    source 38
    target 293
  ]
  edge [
    source 38
    target 294
  ]
  edge [
    source 38
    target 295
  ]
  edge [
    source 38
    target 296
  ]
  edge [
    source 38
    target 297
  ]
  edge [
    source 38
    target 298
  ]
  edge [
    source 38
    target 299
  ]
  edge [
    source 38
    target 300
  ]
  edge [
    source 38
    target 301
  ]
  edge [
    source 38
    target 302
  ]
  edge [
    source 38
    target 303
  ]
  edge [
    source 38
    target 304
  ]
  edge [
    source 38
    target 305
  ]
  edge [
    source 38
    target 306
  ]
  edge [
    source 38
    target 307
  ]
  edge [
    source 38
    target 308
  ]
  edge [
    source 38
    target 309
  ]
  edge [
    source 38
    target 310
  ]
  edge [
    source 38
    target 311
  ]
  edge [
    source 38
    target 312
  ]
  edge [
    source 38
    target 313
  ]
  edge [
    source 38
    target 314
  ]
  edge [
    source 38
    target 315
  ]
  edge [
    source 38
    target 316
  ]
  edge [
    source 38
    target 317
  ]
  edge [
    source 38
    target 318
  ]
  edge [
    source 38
    target 319
  ]
  edge [
    source 38
    target 320
  ]
  edge [
    source 38
    target 321
  ]
  edge [
    source 38
    target 322
  ]
  edge [
    source 38
    target 323
  ]
  edge [
    source 38
    target 324
  ]
  edge [
    source 38
    target 325
  ]
  edge [
    source 38
    target 326
  ]
  edge [
    source 38
    target 327
  ]
  edge [
    source 38
    target 328
  ]
  edge [
    source 38
    target 329
  ]
  edge [
    source 38
    target 330
  ]
  edge [
    source 38
    target 331
  ]
  edge [
    source 38
    target 332
  ]
  edge [
    source 38
    target 336
  ]
  edge [
    source 38
    target 334
  ]
  edge [
    source 38
    target 335
  ]
  edge [
    source 38
    target 333
  ]
  edge [
    source 38
    target 337
  ]
  edge [
    source 38
    target 338
  ]
  edge [
    source 38
    target 340
  ]
  edge [
    source 38
    target 339
  ]
  edge [
    source 38
    target 341
  ]
  edge [
    source 38
    target 342
  ]
  edge [
    source 38
    target 343
  ]
  edge [
    source 38
    target 344
  ]
  edge [
    source 38
    target 345
  ]
  edge [
    source 38
    target 346
  ]
  edge [
    source 38
    target 347
  ]
  edge [
    source 38
    target 348
  ]
  edge [
    source 38
    target 349
  ]
  edge [
    source 38
    target 350
  ]
  edge [
    source 38
    target 351
  ]
  edge [
    source 38
    target 352
  ]
  edge [
    source 38
    target 353
  ]
  edge [
    source 38
    target 354
  ]
  edge [
    source 38
    target 355
  ]
  edge [
    source 38
    target 356
  ]
  edge [
    source 38
    target 357
  ]
  edge [
    source 38
    target 358
  ]
  edge [
    source 38
    target 359
  ]
  edge [
    source 38
    target 360
  ]
  edge [
    source 38
    target 361
  ]
  edge [
    source 38
    target 362
  ]
  edge [
    source 38
    target 363
  ]
  edge [
    source 38
    target 364
  ]
  edge [
    source 38
    target 365
  ]
  edge [
    source 38
    target 366
  ]
  edge [
    source 38
    target 367
  ]
  edge [
    source 38
    target 368
  ]
  edge [
    source 38
    target 370
  ]
  edge [
    source 38
    target 369
  ]
  edge [
    source 38
    target 371
  ]
  edge [
    source 38
    target 372
  ]
  edge [
    source 38
    target 373
  ]
  edge [
    source 38
    target 374
  ]
  edge [
    source 38
    target 84
  ]
  edge [
    source 38
    target 375
  ]
  edge [
    source 38
    target 376
  ]
  edge [
    source 38
    target 378
  ]
  edge [
    source 38
    target 377
  ]
  edge [
    source 38
    target 379
  ]
  edge [
    source 38
    target 381
  ]
  edge [
    source 38
    target 380
  ]
  edge [
    source 38
    target 382
  ]
  edge [
    source 38
    target 383
  ]
  edge [
    source 38
    target 384
  ]
  edge [
    source 38
    target 385
  ]
  edge [
    source 38
    target 386
  ]
  edge [
    source 38
    target 388
  ]
  edge [
    source 38
    target 387
  ]
  edge [
    source 38
    target 390
  ]
  edge [
    source 38
    target 389
  ]
  edge [
    source 38
    target 391
  ]
  edge [
    source 38
    target 392
  ]
  edge [
    source 38
    target 393
  ]
  edge [
    source 38
    target 394
  ]
  edge [
    source 38
    target 395
  ]
  edge [
    source 38
    target 396
  ]
  edge [
    source 38
    target 398
  ]
  edge [
    source 38
    target 397
  ]
  edge [
    source 38
    target 399
  ]
  edge [
    source 38
    target 400
  ]
  edge [
    source 38
    target 403
  ]
  edge [
    source 38
    target 402
  ]
  edge [
    source 38
    target 401
  ]
  edge [
    source 38
    target 404
  ]
  edge [
    source 38
    target 405
  ]
  edge [
    source 38
    target 406
  ]
  edge [
    source 38
    target 407
  ]
  edge [
    source 38
    target 408
  ]
  edge [
    source 38
    target 409
  ]
  edge [
    source 38
    target 410
  ]
  edge [
    source 38
    target 411
  ]
  edge [
    source 38
    target 412
  ]
  edge [
    source 38
    target 413
  ]
  edge [
    source 38
    target 414
  ]
  edge [
    source 38
    target 415
  ]
  edge [
    source 38
    target 416
  ]
  edge [
    source 38
    target 417
  ]
  edge [
    source 38
    target 418
  ]
  edge [
    source 38
    target 419
  ]
  edge [
    source 38
    target 422
  ]
  edge [
    source 38
    target 423
  ]
  edge [
    source 38
    target 420
  ]
  edge [
    source 38
    target 421
  ]
  edge [
    source 38
    target 424
  ]
  edge [
    source 38
    target 425
  ]
  edge [
    source 38
    target 426
  ]
  edge [
    source 38
    target 427
  ]
  edge [
    source 38
    target 428
  ]
  edge [
    source 38
    target 429
  ]
  edge [
    source 38
    target 430
  ]
  edge [
    source 38
    target 431
  ]
  edge [
    source 38
    target 432
  ]
  edge [
    source 38
    target 433
  ]
  edge [
    source 38
    target 434
  ]
  edge [
    source 38
    target 435
  ]
  edge [
    source 38
    target 436
  ]
  edge [
    source 38
    target 437
  ]
  edge [
    source 38
    target 442
  ]
  edge [
    source 38
    target 439
  ]
  edge [
    source 38
    target 440
  ]
  edge [
    source 38
    target 441
  ]
  edge [
    source 38
    target 438
  ]
  edge [
    source 38
    target 443
  ]
  edge [
    source 38
    target 444
  ]
  edge [
    source 38
    target 445
  ]
  edge [
    source 38
    target 446
  ]
  edge [
    source 38
    target 447
  ]
  edge [
    source 38
    target 448
  ]
  edge [
    source 38
    target 449
  ]
  edge [
    source 38
    target 1546
  ]
  edge [
    source 38
    target 1547
  ]
  edge [
    source 38
    target 1548
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 136
  ]
  edge [
    source 39
    target 958
  ]
  edge [
    source 39
    target 477
  ]
  edge [
    source 39
    target 465
  ]
  edge [
    source 39
    target 478
  ]
  edge [
    source 39
    target 479
  ]
  edge [
    source 39
    target 480
  ]
  edge [
    source 39
    target 67
  ]
  edge [
    source 39
    target 481
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 1549
  ]
  edge [
    source 40
    target 1550
  ]
  edge [
    source 40
    target 902
  ]
  edge [
    source 40
    target 520
  ]
  edge [
    source 40
    target 1551
  ]
  edge [
    source 40
    target 1552
  ]
  edge [
    source 40
    target 1553
  ]
  edge [
    source 40
    target 1554
  ]
  edge [
    source 40
    target 1555
  ]
  edge [
    source 40
    target 1556
  ]
  edge [
    source 40
    target 51
  ]
  edge [
    source 40
    target 903
  ]
  edge [
    source 40
    target 901
  ]
  edge [
    source 40
    target 1557
  ]
  edge [
    source 40
    target 898
  ]
  edge [
    source 40
    target 1558
  ]
  edge [
    source 40
    target 1559
  ]
  edge [
    source 40
    target 1560
  ]
  edge [
    source 41
    target 1561
  ]
  edge [
    source 41
    target 1562
  ]
  edge [
    source 41
    target 1563
  ]
  edge [
    source 41
    target 1564
  ]
  edge [
    source 41
    target 1565
  ]
  edge [
    source 41
    target 1566
  ]
  edge [
    source 41
    target 1567
  ]
  edge [
    source 41
    target 1568
  ]
  edge [
    source 41
    target 1569
  ]
  edge [
    source 41
    target 1570
  ]
  edge [
    source 41
    target 1571
  ]
  edge [
    source 41
    target 1572
  ]
  edge [
    source 41
    target 1573
  ]
  edge [
    source 41
    target 1574
  ]
  edge [
    source 41
    target 1575
  ]
  edge [
    source 41
    target 1576
  ]
  edge [
    source 41
    target 1577
  ]
  edge [
    source 41
    target 1578
  ]
  edge [
    source 41
    target 1579
  ]
  edge [
    source 41
    target 1268
  ]
  edge [
    source 41
    target 1580
  ]
  edge [
    source 41
    target 1581
  ]
  edge [
    source 41
    target 376
  ]
  edge [
    source 41
    target 1582
  ]
  edge [
    source 41
    target 1583
  ]
  edge [
    source 41
    target 1584
  ]
  edge [
    source 41
    target 1585
  ]
  edge [
    source 41
    target 1586
  ]
  edge [
    source 41
    target 1587
  ]
  edge [
    source 41
    target 1588
  ]
  edge [
    source 41
    target 1589
  ]
  edge [
    source 41
    target 1590
  ]
  edge [
    source 41
    target 1591
  ]
  edge [
    source 41
    target 1592
  ]
  edge [
    source 41
    target 1593
  ]
  edge [
    source 41
    target 1594
  ]
  edge [
    source 41
    target 1595
  ]
  edge [
    source 41
    target 1596
  ]
  edge [
    source 41
    target 1597
  ]
  edge [
    source 41
    target 1598
  ]
  edge [
    source 41
    target 1599
  ]
  edge [
    source 41
    target 1600
  ]
  edge [
    source 41
    target 1601
  ]
  edge [
    source 41
    target 1602
  ]
  edge [
    source 41
    target 1603
  ]
  edge [
    source 41
    target 1604
  ]
  edge [
    source 41
    target 1605
  ]
  edge [
    source 41
    target 1606
  ]
  edge [
    source 41
    target 1607
  ]
  edge [
    source 41
    target 296
  ]
  edge [
    source 41
    target 342
  ]
  edge [
    source 42
    target 1606
  ]
  edge [
    source 42
    target 1563
  ]
  edge [
    source 42
    target 1607
  ]
  edge [
    source 42
    target 1332
  ]
  edge [
    source 42
    target 1561
  ]
  edge [
    source 42
    target 1562
  ]
  edge [
    source 42
    target 1564
  ]
  edge [
    source 42
    target 1565
  ]
  edge [
    source 42
    target 1566
  ]
  edge [
    source 42
    target 1568
  ]
  edge [
    source 42
    target 1575
  ]
  edge [
    source 42
    target 1570
  ]
  edge [
    source 42
    target 1569
  ]
  edge [
    source 42
    target 1571
  ]
  edge [
    source 42
    target 1572
  ]
  edge [
    source 42
    target 1574
  ]
  edge [
    source 42
    target 1576
  ]
  edge [
    source 42
    target 1577
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1608
  ]
  edge [
    source 43
    target 1609
  ]
  edge [
    source 43
    target 1610
  ]
  edge [
    source 43
    target 1611
  ]
  edge [
    source 43
    target 514
  ]
  edge [
    source 43
    target 1612
  ]
  edge [
    source 43
    target 1613
  ]
  edge [
    source 43
    target 1614
  ]
  edge [
    source 43
    target 1615
  ]
  edge [
    source 43
    target 1616
  ]
  edge [
    source 43
    target 1617
  ]
  edge [
    source 43
    target 1618
  ]
  edge [
    source 43
    target 1619
  ]
  edge [
    source 43
    target 1620
  ]
  edge [
    source 43
    target 513
  ]
  edge [
    source 43
    target 1621
  ]
  edge [
    source 43
    target 1622
  ]
  edge [
    source 43
    target 1623
  ]
  edge [
    source 43
    target 1624
  ]
  edge [
    source 43
    target 1625
  ]
  edge [
    source 43
    target 189
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1626
  ]
  edge [
    source 44
    target 1627
  ]
  edge [
    source 44
    target 1628
  ]
  edge [
    source 44
    target 1506
  ]
  edge [
    source 44
    target 1629
  ]
  edge [
    source 44
    target 1630
  ]
  edge [
    source 44
    target 1631
  ]
  edge [
    source 44
    target 1632
  ]
  edge [
    source 44
    target 1633
  ]
  edge [
    source 44
    target 1634
  ]
  edge [
    source 44
    target 1635
  ]
  edge [
    source 44
    target 1636
  ]
  edge [
    source 44
    target 1637
  ]
  edge [
    source 44
    target 986
  ]
  edge [
    source 44
    target 1638
  ]
  edge [
    source 44
    target 1639
  ]
  edge [
    source 44
    target 1640
  ]
  edge [
    source 44
    target 1398
  ]
  edge [
    source 44
    target 1641
  ]
  edge [
    source 44
    target 52
  ]
  edge [
    source 44
    target 1642
  ]
  edge [
    source 44
    target 1154
  ]
  edge [
    source 44
    target 1643
  ]
  edge [
    source 44
    target 1644
  ]
  edge [
    source 44
    target 1645
  ]
  edge [
    source 44
    target 1646
  ]
  edge [
    source 44
    target 1647
  ]
  edge [
    source 44
    target 646
  ]
  edge [
    source 44
    target 998
  ]
  edge [
    source 44
    target 1648
  ]
  edge [
    source 44
    target 1143
  ]
  edge [
    source 44
    target 1649
  ]
  edge [
    source 44
    target 1650
  ]
  edge [
    source 44
    target 1651
  ]
  edge [
    source 44
    target 1652
  ]
  edge [
    source 44
    target 1653
  ]
  edge [
    source 44
    target 1654
  ]
  edge [
    source 44
    target 618
  ]
  edge [
    source 44
    target 1655
  ]
  edge [
    source 44
    target 1656
  ]
  edge [
    source 44
    target 1657
  ]
  edge [
    source 44
    target 1658
  ]
  edge [
    source 44
    target 1659
  ]
  edge [
    source 45
    target 593
  ]
  edge [
    source 45
    target 1660
  ]
  edge [
    source 45
    target 1661
  ]
  edge [
    source 45
    target 1662
  ]
  edge [
    source 45
    target 1663
  ]
  edge [
    source 45
    target 885
  ]
  edge [
    source 45
    target 599
  ]
  edge [
    source 45
    target 1664
  ]
  edge [
    source 45
    target 881
  ]
  edge [
    source 45
    target 926
  ]
  edge [
    source 45
    target 1665
  ]
  edge [
    source 45
    target 1666
  ]
  edge [
    source 45
    target 1667
  ]
  edge [
    source 45
    target 1668
  ]
  edge [
    source 45
    target 812
  ]
  edge [
    source 45
    target 1669
  ]
  edge [
    source 45
    target 1350
  ]
  edge [
    source 45
    target 669
  ]
  edge [
    source 45
    target 670
  ]
  edge [
    source 45
    target 671
  ]
  edge [
    source 45
    target 672
  ]
  edge [
    source 45
    target 673
  ]
  edge [
    source 45
    target 674
  ]
  edge [
    source 45
    target 1670
  ]
  edge [
    source 45
    target 1671
  ]
  edge [
    source 45
    target 1672
  ]
  edge [
    source 45
    target 1673
  ]
  edge [
    source 45
    target 1674
  ]
  edge [
    source 45
    target 1675
  ]
  edge [
    source 45
    target 1676
  ]
  edge [
    source 45
    target 1677
  ]
  edge [
    source 45
    target 1678
  ]
  edge [
    source 45
    target 1679
  ]
  edge [
    source 45
    target 1680
  ]
  edge [
    source 45
    target 1681
  ]
  edge [
    source 45
    target 1682
  ]
  edge [
    source 45
    target 1683
  ]
  edge [
    source 45
    target 1684
  ]
  edge [
    source 45
    target 1199
  ]
  edge [
    source 45
    target 1685
  ]
  edge [
    source 45
    target 1686
  ]
  edge [
    source 45
    target 1687
  ]
  edge [
    source 45
    target 1688
  ]
  edge [
    source 45
    target 719
  ]
  edge [
    source 45
    target 1689
  ]
  edge [
    source 45
    target 1690
  ]
  edge [
    source 45
    target 1691
  ]
  edge [
    source 45
    target 52
  ]
  edge [
    source 45
    target 1692
  ]
  edge [
    source 45
    target 1452
  ]
  edge [
    source 45
    target 1451
  ]
  edge [
    source 45
    target 1693
  ]
  edge [
    source 45
    target 1453
  ]
  edge [
    source 45
    target 1454
  ]
  edge [
    source 45
    target 1694
  ]
  edge [
    source 45
    target 1455
  ]
  edge [
    source 45
    target 1461
  ]
  edge [
    source 45
    target 1695
  ]
  edge [
    source 45
    target 787
  ]
  edge [
    source 45
    target 1456
  ]
  edge [
    source 45
    target 1457
  ]
  edge [
    source 45
    target 1459
  ]
  edge [
    source 45
    target 1460
  ]
  edge [
    source 45
    target 1696
  ]
  edge [
    source 45
    target 1697
  ]
  edge [
    source 45
    target 1458
  ]
  edge [
    source 45
    target 1698
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1369
  ]
  edge [
    source 46
    target 1699
  ]
  edge [
    source 46
    target 1700
  ]
  edge [
    source 46
    target 1701
  ]
  edge [
    source 46
    target 1702
  ]
  edge [
    source 46
    target 1703
  ]
  edge [
    source 46
    target 1704
  ]
  edge [
    source 46
    target 1705
  ]
  edge [
    source 46
    target 1706
  ]
  edge [
    source 46
    target 1707
  ]
  edge [
    source 46
    target 1708
  ]
  edge [
    source 46
    target 704
  ]
  edge [
    source 46
    target 1709
  ]
  edge [
    source 46
    target 801
  ]
  edge [
    source 46
    target 1710
  ]
  edge [
    source 46
    target 1711
  ]
  edge [
    source 46
    target 1712
  ]
  edge [
    source 46
    target 1713
  ]
  edge [
    source 46
    target 1714
  ]
  edge [
    source 46
    target 1715
  ]
  edge [
    source 46
    target 987
  ]
  edge [
    source 46
    target 1716
  ]
  edge [
    source 46
    target 1717
  ]
  edge [
    source 46
    target 1023
  ]
  edge [
    source 46
    target 1718
  ]
  edge [
    source 46
    target 1719
  ]
  edge [
    source 46
    target 1720
  ]
  edge [
    source 46
    target 990
  ]
  edge [
    source 46
    target 1721
  ]
  edge [
    source 46
    target 1722
  ]
  edge [
    source 46
    target 932
  ]
  edge [
    source 46
    target 1390
  ]
  edge [
    source 46
    target 1723
  ]
  edge [
    source 46
    target 1360
  ]
  edge [
    source 46
    target 917
  ]
  edge [
    source 46
    target 1724
  ]
  edge [
    source 46
    target 1725
  ]
  edge [
    source 46
    target 1726
  ]
  edge [
    source 46
    target 1727
  ]
  edge [
    source 46
    target 1728
  ]
  edge [
    source 46
    target 1729
  ]
  edge [
    source 46
    target 1730
  ]
  edge [
    source 46
    target 1731
  ]
  edge [
    source 46
    target 1732
  ]
  edge [
    source 46
    target 1733
  ]
  edge [
    source 46
    target 905
  ]
  edge [
    source 46
    target 1201
  ]
  edge [
    source 46
    target 1734
  ]
  edge [
    source 46
    target 929
  ]
  edge [
    source 46
    target 1735
  ]
  edge [
    source 46
    target 1736
  ]
  edge [
    source 46
    target 1737
  ]
  edge [
    source 46
    target 1738
  ]
  edge [
    source 46
    target 1739
  ]
  edge [
    source 46
    target 1740
  ]
  edge [
    source 46
    target 1741
  ]
  edge [
    source 46
    target 1742
  ]
  edge [
    source 46
    target 1658
  ]
  edge [
    source 46
    target 1743
  ]
  edge [
    source 46
    target 1291
  ]
  edge [
    source 46
    target 1744
  ]
  edge [
    source 46
    target 1745
  ]
  edge [
    source 46
    target 1746
  ]
  edge [
    source 46
    target 1747
  ]
  edge [
    source 46
    target 1303
  ]
  edge [
    source 46
    target 1748
  ]
  edge [
    source 46
    target 1749
  ]
  edge [
    source 46
    target 1750
  ]
  edge [
    source 46
    target 1751
  ]
  edge [
    source 46
    target 1752
  ]
  edge [
    source 46
    target 1753
  ]
  edge [
    source 46
    target 1754
  ]
  edge [
    source 46
    target 1755
  ]
  edge [
    source 46
    target 1756
  ]
  edge [
    source 46
    target 1757
  ]
  edge [
    source 46
    target 1758
  ]
  edge [
    source 46
    target 1759
  ]
  edge [
    source 46
    target 1760
  ]
  edge [
    source 46
    target 1761
  ]
  edge [
    source 46
    target 1762
  ]
  edge [
    source 46
    target 1763
  ]
  edge [
    source 46
    target 1764
  ]
  edge [
    source 46
    target 1121
  ]
  edge [
    source 46
    target 1765
  ]
  edge [
    source 46
    target 1766
  ]
  edge [
    source 46
    target 1767
  ]
  edge [
    source 46
    target 1768
  ]
  edge [
    source 46
    target 674
  ]
  edge [
    source 46
    target 532
  ]
  edge [
    source 46
    target 1769
  ]
  edge [
    source 46
    target 1770
  ]
  edge [
    source 46
    target 1771
  ]
  edge [
    source 46
    target 676
  ]
  edge [
    source 46
    target 1772
  ]
  edge [
    source 46
    target 1773
  ]
  edge [
    source 46
    target 1774
  ]
  edge [
    source 46
    target 1217
  ]
  edge [
    source 46
    target 1775
  ]
  edge [
    source 46
    target 1776
  ]
  edge [
    source 46
    target 1777
  ]
  edge [
    source 46
    target 1778
  ]
  edge [
    source 46
    target 1779
  ]
  edge [
    source 46
    target 1780
  ]
  edge [
    source 46
    target 1781
  ]
  edge [
    source 46
    target 1782
  ]
  edge [
    source 46
    target 1783
  ]
  edge [
    source 46
    target 1784
  ]
  edge [
    source 46
    target 1785
  ]
  edge [
    source 46
    target 1786
  ]
  edge [
    source 46
    target 1787
  ]
  edge [
    source 46
    target 1788
  ]
  edge [
    source 46
    target 1789
  ]
  edge [
    source 46
    target 1790
  ]
  edge [
    source 46
    target 1791
  ]
  edge [
    source 46
    target 1792
  ]
  edge [
    source 46
    target 1155
  ]
  edge [
    source 46
    target 1793
  ]
  edge [
    source 46
    target 1794
  ]
  edge [
    source 46
    target 1795
  ]
  edge [
    source 46
    target 1796
  ]
  edge [
    source 46
    target 1797
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 1798
  ]
  edge [
    source 48
    target 1799
  ]
  edge [
    source 48
    target 1800
  ]
  edge [
    source 48
    target 980
  ]
  edge [
    source 48
    target 1801
  ]
  edge [
    source 48
    target 1016
  ]
  edge [
    source 48
    target 970
  ]
  edge [
    source 48
    target 1802
  ]
  edge [
    source 48
    target 1803
  ]
  edge [
    source 48
    target 1804
  ]
  edge [
    source 48
    target 1805
  ]
  edge [
    source 48
    target 1230
  ]
  edge [
    source 48
    target 1806
  ]
  edge [
    source 48
    target 1807
  ]
  edge [
    source 48
    target 969
  ]
  edge [
    source 48
    target 997
  ]
  edge [
    source 48
    target 1808
  ]
  edge [
    source 48
    target 979
  ]
  edge [
    source 48
    target 981
  ]
  edge [
    source 48
    target 982
  ]
  edge [
    source 48
    target 983
  ]
  edge [
    source 48
    target 1809
  ]
  edge [
    source 48
    target 1810
  ]
  edge [
    source 48
    target 1811
  ]
  edge [
    source 48
    target 1644
  ]
  edge [
    source 48
    target 1812
  ]
  edge [
    source 48
    target 1365
  ]
  edge [
    source 48
    target 1813
  ]
  edge [
    source 48
    target 1814
  ]
  edge [
    source 48
    target 1007
  ]
  edge [
    source 48
    target 1815
  ]
  edge [
    source 48
    target 967
  ]
  edge [
    source 48
    target 968
  ]
  edge [
    source 48
    target 971
  ]
  edge [
    source 48
    target 972
  ]
  edge [
    source 48
    target 973
  ]
  edge [
    source 48
    target 974
  ]
  edge [
    source 48
    target 975
  ]
  edge [
    source 48
    target 976
  ]
  edge [
    source 48
    target 977
  ]
  edge [
    source 48
    target 1816
  ]
  edge [
    source 48
    target 1817
  ]
  edge [
    source 48
    target 1818
  ]
  edge [
    source 48
    target 1721
  ]
  edge [
    source 48
    target 1819
  ]
  edge [
    source 48
    target 1820
  ]
  edge [
    source 48
    target 1821
  ]
  edge [
    source 48
    target 1822
  ]
  edge [
    source 48
    target 1823
  ]
  edge [
    source 48
    target 1824
  ]
  edge [
    source 48
    target 1630
  ]
  edge [
    source 48
    target 1825
  ]
  edge [
    source 48
    target 1826
  ]
  edge [
    source 48
    target 1827
  ]
  edge [
    source 48
    target 913
  ]
  edge [
    source 48
    target 1359
  ]
  edge [
    source 48
    target 1360
  ]
  edge [
    source 48
    target 1361
  ]
  edge [
    source 48
    target 1362
  ]
  edge [
    source 48
    target 1363
  ]
  edge [
    source 48
    target 1364
  ]
  edge [
    source 48
    target 761
  ]
  edge [
    source 48
    target 1828
  ]
  edge [
    source 48
    target 1492
  ]
  edge [
    source 48
    target 1829
  ]
  edge [
    source 48
    target 1830
  ]
  edge [
    source 48
    target 1831
  ]
  edge [
    source 48
    target 1832
  ]
  edge [
    source 48
    target 1833
  ]
  edge [
    source 48
    target 1834
  ]
  edge [
    source 48
    target 1835
  ]
  edge [
    source 48
    target 1836
  ]
  edge [
    source 48
    target 1837
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 1838
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 1839
  ]
  edge [
    source 51
    target 487
  ]
  edge [
    source 51
    target 1840
  ]
  edge [
    source 51
    target 1841
  ]
  edge [
    source 51
    target 1842
  ]
  edge [
    source 51
    target 1843
  ]
  edge [
    source 51
    target 1844
  ]
  edge [
    source 51
    target 1845
  ]
  edge [
    source 51
    target 838
  ]
  edge [
    source 51
    target 1846
  ]
  edge [
    source 51
    target 1847
  ]
  edge [
    source 51
    target 843
  ]
  edge [
    source 51
    target 844
  ]
  edge [
    source 51
    target 1848
  ]
  edge [
    source 51
    target 865
  ]
  edge [
    source 51
    target 849
  ]
  edge [
    source 51
    target 1849
  ]
  edge [
    source 51
    target 1850
  ]
  edge [
    source 51
    target 952
  ]
  edge [
    source 51
    target 1851
  ]
  edge [
    source 51
    target 1852
  ]
  edge [
    source 51
    target 1853
  ]
  edge [
    source 51
    target 845
  ]
  edge [
    source 51
    target 1854
  ]
  edge [
    source 51
    target 1855
  ]
  edge [
    source 51
    target 848
  ]
  edge [
    source 51
    target 1856
  ]
  edge [
    source 51
    target 1857
  ]
  edge [
    source 51
    target 1858
  ]
  edge [
    source 51
    target 1859
  ]
  edge [
    source 51
    target 1860
  ]
  edge [
    source 51
    target 1861
  ]
  edge [
    source 51
    target 1862
  ]
  edge [
    source 51
    target 1863
  ]
  edge [
    source 51
    target 488
  ]
  edge [
    source 51
    target 738
  ]
  edge [
    source 51
    target 1864
  ]
  edge [
    source 51
    target 955
  ]
  edge [
    source 51
    target 1865
  ]
  edge [
    source 51
    target 483
  ]
  edge [
    source 51
    target 1866
  ]
  edge [
    source 51
    target 1867
  ]
  edge [
    source 51
    target 1868
  ]
  edge [
    source 51
    target 1869
  ]
  edge [
    source 52
    target 1325
  ]
  edge [
    source 52
    target 1326
  ]
  edge [
    source 52
    target 1327
  ]
  edge [
    source 52
    target 1328
  ]
  edge [
    source 52
    target 1329
  ]
  edge [
    source 52
    target 995
  ]
  edge [
    source 52
    target 1330
  ]
  edge [
    source 52
    target 1331
  ]
  edge [
    source 52
    target 1332
  ]
  edge [
    source 52
    target 1333
  ]
  edge [
    source 52
    target 1334
  ]
  edge [
    source 52
    target 1335
  ]
  edge [
    source 52
    target 1075
  ]
  edge [
    source 52
    target 1336
  ]
  edge [
    source 52
    target 1337
  ]
  edge [
    source 52
    target 1338
  ]
  edge [
    source 52
    target 1339
  ]
  edge [
    source 52
    target 1340
  ]
  edge [
    source 52
    target 1341
  ]
  edge [
    source 52
    target 1342
  ]
  edge [
    source 52
    target 1343
  ]
  edge [
    source 52
    target 1344
  ]
  edge [
    source 52
    target 1081
  ]
  edge [
    source 52
    target 1345
  ]
  edge [
    source 52
    target 1346
  ]
  edge [
    source 52
    target 1347
  ]
  edge [
    source 52
    target 1348
  ]
  edge [
    source 52
    target 1349
  ]
  edge [
    source 52
    target 1350
  ]
  edge [
    source 52
    target 1351
  ]
  edge [
    source 52
    target 1352
  ]
  edge [
    source 52
    target 1353
  ]
  edge [
    source 52
    target 1354
  ]
  edge [
    source 52
    target 1355
  ]
  edge [
    source 52
    target 1356
  ]
  edge [
    source 52
    target 1357
  ]
  edge [
    source 52
    target 1358
  ]
  edge [
    source 52
    target 1870
  ]
  edge [
    source 52
    target 1871
  ]
  edge [
    source 52
    target 1872
  ]
  edge [
    source 52
    target 1873
  ]
  edge [
    source 52
    target 1874
  ]
  edge [
    source 52
    target 1875
  ]
  edge [
    source 52
    target 1876
  ]
  edge [
    source 52
    target 1877
  ]
  edge [
    source 52
    target 1878
  ]
  edge [
    source 52
    target 1486
  ]
  edge [
    source 52
    target 1879
  ]
  edge [
    source 52
    target 1199
  ]
  edge [
    source 52
    target 530
  ]
  edge [
    source 52
    target 1880
  ]
  edge [
    source 52
    target 1881
  ]
  edge [
    source 52
    target 1882
  ]
  edge [
    source 52
    target 1883
  ]
  edge [
    source 52
    target 1884
  ]
  edge [
    source 52
    target 1885
  ]
  edge [
    source 52
    target 1886
  ]
  edge [
    source 52
    target 1887
  ]
  edge [
    source 52
    target 1888
  ]
  edge [
    source 52
    target 1889
  ]
  edge [
    source 52
    target 1890
  ]
  edge [
    source 52
    target 1891
  ]
  edge [
    source 52
    target 1892
  ]
  edge [
    source 52
    target 1893
  ]
  edge [
    source 52
    target 1894
  ]
  edge [
    source 52
    target 1895
  ]
  edge [
    source 52
    target 1896
  ]
  edge [
    source 52
    target 1897
  ]
  edge [
    source 52
    target 674
  ]
  edge [
    source 52
    target 1898
  ]
  edge [
    source 52
    target 1899
  ]
  edge [
    source 52
    target 1900
  ]
  edge [
    source 52
    target 1315
  ]
  edge [
    source 52
    target 1316
  ]
  edge [
    source 52
    target 1317
  ]
  edge [
    source 52
    target 1021
  ]
  edge [
    source 52
    target 1318
  ]
  edge [
    source 52
    target 1319
  ]
  edge [
    source 52
    target 1320
  ]
  edge [
    source 52
    target 1901
  ]
  edge [
    source 52
    target 1902
  ]
  edge [
    source 52
    target 1903
  ]
  edge [
    source 52
    target 1904
  ]
  edge [
    source 52
    target 1905
  ]
  edge [
    source 52
    target 1806
  ]
  edge [
    source 52
    target 1373
  ]
  edge [
    source 52
    target 1906
  ]
  edge [
    source 52
    target 1364
  ]
  edge [
    source 52
    target 1907
  ]
  edge [
    source 52
    target 1908
  ]
  edge [
    source 52
    target 1909
  ]
  edge [
    source 52
    target 1910
  ]
  edge [
    source 52
    target 1911
  ]
  edge [
    source 52
    target 1912
  ]
  edge [
    source 52
    target 1913
  ]
  edge [
    source 52
    target 1914
  ]
  edge [
    source 52
    target 1915
  ]
  edge [
    source 52
    target 1916
  ]
  edge [
    source 52
    target 1917
  ]
  edge [
    source 52
    target 1918
  ]
  edge [
    source 52
    target 1004
  ]
  edge [
    source 52
    target 1919
  ]
  edge [
    source 52
    target 1920
  ]
  edge [
    source 52
    target 1921
  ]
  edge [
    source 52
    target 1922
  ]
  edge [
    source 52
    target 1923
  ]
  edge [
    source 52
    target 1541
  ]
  edge [
    source 52
    target 1924
  ]
  edge [
    source 52
    target 1925
  ]
  edge [
    source 52
    target 1926
  ]
  edge [
    source 52
    target 1927
  ]
  edge [
    source 52
    target 1928
  ]
  edge [
    source 52
    target 1929
  ]
  edge [
    source 52
    target 1930
  ]
  edge [
    source 52
    target 998
  ]
  edge [
    source 52
    target 1931
  ]
  edge [
    source 52
    target 1932
  ]
  edge [
    source 52
    target 1933
  ]
  edge [
    source 52
    target 1802
  ]
  edge [
    source 52
    target 1007
  ]
  edge [
    source 52
    target 1934
  ]
  edge [
    source 52
    target 1935
  ]
  edge [
    source 52
    target 1012
  ]
  edge [
    source 52
    target 1936
  ]
  edge [
    source 52
    target 1937
  ]
  edge [
    source 52
    target 1938
  ]
  edge [
    source 52
    target 1939
  ]
  edge [
    source 52
    target 1940
  ]
  edge [
    source 52
    target 1941
  ]
  edge [
    source 52
    target 1431
  ]
  edge [
    source 52
    target 1942
  ]
  edge [
    source 52
    target 1943
  ]
  edge [
    source 52
    target 1944
  ]
  edge [
    source 52
    target 1945
  ]
  edge [
    source 52
    target 1946
  ]
  edge [
    source 52
    target 1947
  ]
  edge [
    source 52
    target 1948
  ]
  edge [
    source 52
    target 1949
  ]
  edge [
    source 52
    target 1950
  ]
  edge [
    source 52
    target 1951
  ]
  edge [
    source 52
    target 1952
  ]
  edge [
    source 52
    target 1953
  ]
  edge [
    source 52
    target 618
  ]
  edge [
    source 52
    target 1954
  ]
  edge [
    source 52
    target 1647
  ]
  edge [
    source 52
    target 1637
  ]
  edge [
    source 52
    target 1955
  ]
  edge [
    source 52
    target 1956
  ]
  edge [
    source 52
    target 1957
  ]
  edge [
    source 52
    target 1958
  ]
  edge [
    source 52
    target 1959
  ]
  edge [
    source 52
    target 1960
  ]
  edge [
    source 52
    target 1961
  ]
  edge [
    source 52
    target 1962
  ]
  edge [
    source 52
    target 1963
  ]
  edge [
    source 52
    target 1964
  ]
  edge [
    source 52
    target 1965
  ]
  edge [
    source 52
    target 1144
  ]
  edge [
    source 52
    target 1966
  ]
  edge [
    source 52
    target 999
  ]
  edge [
    source 52
    target 1967
  ]
  edge [
    source 52
    target 1968
  ]
  edge [
    source 52
    target 1969
  ]
  edge [
    source 52
    target 1835
  ]
  edge [
    source 52
    target 1970
  ]
  edge [
    source 52
    target 1630
  ]
  edge [
    source 52
    target 1971
  ]
  edge [
    source 52
    target 1972
  ]
  edge [
    source 52
    target 1383
  ]
  edge [
    source 52
    target 1973
  ]
  edge [
    source 52
    target 1974
  ]
  edge [
    source 52
    target 1975
  ]
  edge [
    source 52
    target 1976
  ]
  edge [
    source 52
    target 1241
  ]
  edge [
    source 52
    target 1977
  ]
  edge [
    source 52
    target 1978
  ]
  edge [
    source 52
    target 1979
  ]
  edge [
    source 52
    target 1980
  ]
  edge [
    source 52
    target 1981
  ]
  edge [
    source 52
    target 1982
  ]
  edge [
    source 52
    target 1983
  ]
  edge [
    source 52
    target 885
  ]
  edge [
    source 52
    target 1984
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 920
  ]
  edge [
    source 53
    target 1985
  ]
  edge [
    source 53
    target 1986
  ]
  edge [
    source 53
    target 1987
  ]
  edge [
    source 53
    target 1988
  ]
  edge [
    source 53
    target 1989
  ]
  edge [
    source 53
    target 1834
  ]
  edge [
    source 53
    target 905
  ]
  edge [
    source 53
    target 1990
  ]
  edge [
    source 53
    target 989
  ]
  edge [
    source 53
    target 917
  ]
  edge [
    source 53
    target 1991
  ]
  edge [
    source 53
    target 1992
  ]
  edge [
    source 53
    target 1993
  ]
  edge [
    source 53
    target 986
  ]
  edge [
    source 53
    target 1994
  ]
  edge [
    source 53
    target 1995
  ]
  edge [
    source 53
    target 1996
  ]
  edge [
    source 53
    target 1658
  ]
  edge [
    source 53
    target 1997
  ]
  edge [
    source 53
    target 1907
  ]
  edge [
    source 53
    target 1998
  ]
  edge [
    source 53
    target 1999
  ]
  edge [
    source 53
    target 987
  ]
  edge [
    source 53
    target 990
  ]
  edge [
    source 53
    target 2000
  ]
  edge [
    source 53
    target 1722
  ]
  edge [
    source 53
    target 932
  ]
  edge [
    source 53
    target 2001
  ]
  edge [
    source 53
    target 2002
  ]
  edge [
    source 53
    target 2003
  ]
  edge [
    source 53
    target 2004
  ]
  edge [
    source 53
    target 2005
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 2006
  ]
  edge [
    source 55
    target 2007
  ]
  edge [
    source 56
    target 2008
  ]
  edge [
    source 56
    target 532
  ]
  edge [
    source 56
    target 538
  ]
  edge [
    source 56
    target 533
  ]
  edge [
    source 56
    target 539
  ]
  edge [
    source 56
    target 540
  ]
  edge [
    source 56
    target 541
  ]
  edge [
    source 56
    target 542
  ]
  edge [
    source 56
    target 543
  ]
  edge [
    source 56
    target 544
  ]
  edge [
    source 56
    target 545
  ]
  edge [
    source 56
    target 546
  ]
  edge [
    source 56
    target 547
  ]
  edge [
    source 56
    target 548
  ]
  edge [
    source 56
    target 552
  ]
  edge [
    source 56
    target 550
  ]
  edge [
    source 56
    target 551
  ]
  edge [
    source 56
    target 549
  ]
  edge [
    source 56
    target 553
  ]
  edge [
    source 56
    target 554
  ]
  edge [
    source 56
    target 555
  ]
]
