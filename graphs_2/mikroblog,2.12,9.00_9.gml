graph [
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "siebie"
    origin "text"
  ]
  node [
    id 2
    label "my&#347;le"
    origin "text"
  ]
  node [
    id 3
    label "jaca"
    origin "text"
  ]
  node [
    id 4
    label "taki"
    origin "text"
  ]
  node [
    id 5
    label "chewbacca"
    origin "text"
  ]
  node [
    id 6
    label "uniwersum"
    origin "text"
  ]
  node [
    id 7
    label "urz&#281;dniczy"
    origin "text"
  ]
  node [
    id 8
    label "siedzie&#263;"
    origin "text"
  ]
  node [
    id 9
    label "co&#347;"
    origin "text"
  ]
  node [
    id 10
    label "skrzecze&#263;"
    origin "text"
  ]
  node [
    id 11
    label "rycze&#263;"
    origin "text"
  ]
  node [
    id 12
    label "tam"
    origin "text"
  ]
  node [
    id 13
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 15
    label "odg&#322;os"
    origin "text"
  ]
  node [
    id 16
    label "wiadomo"
    origin "text"
  ]
  node [
    id 17
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 18
    label "trudno"
    origin "text"
  ]
  node [
    id 19
    label "zrozumie&#263;"
    origin "text"
  ]
  node [
    id 20
    label "wszyscy"
    origin "text"
  ]
  node [
    id 21
    label "bohater"
    origin "text"
  ]
  node [
    id 22
    label "rozumie&#263;"
    origin "text"
  ]
  node [
    id 23
    label "jeszcze"
    origin "text"
  ]
  node [
    id 24
    label "odpowiada&#263;"
    origin "text"
  ]
  node [
    id 25
    label "xdd"
    origin "text"
  ]
  node [
    id 26
    label "okre&#347;lony"
  ]
  node [
    id 27
    label "przyzwoity"
  ]
  node [
    id 28
    label "ciekawy"
  ]
  node [
    id 29
    label "jako&#347;"
  ]
  node [
    id 30
    label "jako_tako"
  ]
  node [
    id 31
    label "niez&#322;y"
  ]
  node [
    id 32
    label "dziwny"
  ]
  node [
    id 33
    label "charakterystyczny"
  ]
  node [
    id 34
    label "wiadomy"
  ]
  node [
    id 35
    label "ciemna_materia"
  ]
  node [
    id 36
    label "planeta"
  ]
  node [
    id 37
    label "mikrokosmos"
  ]
  node [
    id 38
    label "ekosfera"
  ]
  node [
    id 39
    label "przestrze&#324;"
  ]
  node [
    id 40
    label "czarna_dziura"
  ]
  node [
    id 41
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 42
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 43
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 44
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 45
    label "poj&#281;cie"
  ]
  node [
    id 46
    label "makrokosmos"
  ]
  node [
    id 47
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 48
    label "rozdzielanie"
  ]
  node [
    id 49
    label "bezbrze&#380;e"
  ]
  node [
    id 50
    label "punkt"
  ]
  node [
    id 51
    label "czasoprzestrze&#324;"
  ]
  node [
    id 52
    label "zbi&#243;r"
  ]
  node [
    id 53
    label "niezmierzony"
  ]
  node [
    id 54
    label "przedzielenie"
  ]
  node [
    id 55
    label "nielito&#347;ciwy"
  ]
  node [
    id 56
    label "rozdziela&#263;"
  ]
  node [
    id 57
    label "oktant"
  ]
  node [
    id 58
    label "miejsce"
  ]
  node [
    id 59
    label "przedzieli&#263;"
  ]
  node [
    id 60
    label "przestw&#243;r"
  ]
  node [
    id 61
    label "pos&#322;uchanie"
  ]
  node [
    id 62
    label "skumanie"
  ]
  node [
    id 63
    label "orientacja"
  ]
  node [
    id 64
    label "wytw&#243;r"
  ]
  node [
    id 65
    label "teoria"
  ]
  node [
    id 66
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 67
    label "clasp"
  ]
  node [
    id 68
    label "przem&#243;wienie"
  ]
  node [
    id 69
    label "forma"
  ]
  node [
    id 70
    label "zorientowanie"
  ]
  node [
    id 71
    label "Jowisz"
  ]
  node [
    id 72
    label "syzygia"
  ]
  node [
    id 73
    label "atmosfera"
  ]
  node [
    id 74
    label "kosmos"
  ]
  node [
    id 75
    label "Saturn"
  ]
  node [
    id 76
    label "Uran"
  ]
  node [
    id 77
    label "aspekt"
  ]
  node [
    id 78
    label "strefa"
  ]
  node [
    id 79
    label "cz&#322;owiek"
  ]
  node [
    id 80
    label "odbicie"
  ]
  node [
    id 81
    label "atom"
  ]
  node [
    id 82
    label "przyroda"
  ]
  node [
    id 83
    label "Ziemia"
  ]
  node [
    id 84
    label "miniatura"
  ]
  node [
    id 85
    label "kultura"
  ]
  node [
    id 86
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 87
    label "kontekst"
  ]
  node [
    id 88
    label "message"
  ]
  node [
    id 89
    label "&#347;wiat"
  ]
  node [
    id 90
    label "dar"
  ]
  node [
    id 91
    label "zjawisko"
  ]
  node [
    id 92
    label "cecha"
  ]
  node [
    id 93
    label "real"
  ]
  node [
    id 94
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 95
    label "urz&#281;dniczo"
  ]
  node [
    id 96
    label "urz&#281;dowy"
  ]
  node [
    id 97
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 98
    label "urz&#281;dowo"
  ]
  node [
    id 99
    label "oficjalny"
  ]
  node [
    id 100
    label "formalny"
  ]
  node [
    id 101
    label "nale&#380;ny"
  ]
  node [
    id 102
    label "nale&#380;yty"
  ]
  node [
    id 103
    label "typowy"
  ]
  node [
    id 104
    label "uprawniony"
  ]
  node [
    id 105
    label "zasadniczy"
  ]
  node [
    id 106
    label "stosownie"
  ]
  node [
    id 107
    label "prawdziwy"
  ]
  node [
    id 108
    label "ten"
  ]
  node [
    id 109
    label "dobry"
  ]
  node [
    id 110
    label "sit"
  ]
  node [
    id 111
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 112
    label "tkwi&#263;"
  ]
  node [
    id 113
    label "ptak"
  ]
  node [
    id 114
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 115
    label "spoczywa&#263;"
  ]
  node [
    id 116
    label "trwa&#263;"
  ]
  node [
    id 117
    label "przebywa&#263;"
  ]
  node [
    id 118
    label "brood"
  ]
  node [
    id 119
    label "zwierz&#281;"
  ]
  node [
    id 120
    label "pause"
  ]
  node [
    id 121
    label "garowa&#263;"
  ]
  node [
    id 122
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 123
    label "doprowadza&#263;"
  ]
  node [
    id 124
    label "mieszka&#263;"
  ]
  node [
    id 125
    label "istnie&#263;"
  ]
  node [
    id 126
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 127
    label "przestawa&#263;"
  ]
  node [
    id 128
    label "hesitate"
  ]
  node [
    id 129
    label "by&#263;"
  ]
  node [
    id 130
    label "lie"
  ]
  node [
    id 131
    label "odpoczywa&#263;"
  ]
  node [
    id 132
    label "nadzieja"
  ]
  node [
    id 133
    label "gr&#243;b"
  ]
  node [
    id 134
    label "pozostawa&#263;"
  ]
  node [
    id 135
    label "zostawa&#263;"
  ]
  node [
    id 136
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 137
    label "stand"
  ]
  node [
    id 138
    label "adhere"
  ]
  node [
    id 139
    label "sta&#263;"
  ]
  node [
    id 140
    label "zajmowa&#263;"
  ]
  node [
    id 141
    label "room"
  ]
  node [
    id 142
    label "fall"
  ]
  node [
    id 143
    label "panowa&#263;"
  ]
  node [
    id 144
    label "fix"
  ]
  node [
    id 145
    label "polega&#263;"
  ]
  node [
    id 146
    label "rig"
  ]
  node [
    id 147
    label "wykonywa&#263;"
  ]
  node [
    id 148
    label "prowadzi&#263;"
  ]
  node [
    id 149
    label "deliver"
  ]
  node [
    id 150
    label "powodowa&#263;"
  ]
  node [
    id 151
    label "wzbudza&#263;"
  ]
  node [
    id 152
    label "moderate"
  ]
  node [
    id 153
    label "wprowadza&#263;"
  ]
  node [
    id 154
    label "rosn&#261;&#263;"
  ]
  node [
    id 155
    label "harowa&#263;"
  ]
  node [
    id 156
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 157
    label "le&#380;akowa&#263;"
  ]
  node [
    id 158
    label "&#347;l&#281;cze&#263;"
  ]
  node [
    id 159
    label "tankowa&#263;"
  ]
  node [
    id 160
    label "degenerat"
  ]
  node [
    id 161
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 162
    label "zwyrol"
  ]
  node [
    id 163
    label "czerniak"
  ]
  node [
    id 164
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 165
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 166
    label "paszcza"
  ]
  node [
    id 167
    label "popapraniec"
  ]
  node [
    id 168
    label "skuba&#263;"
  ]
  node [
    id 169
    label "skubanie"
  ]
  node [
    id 170
    label "agresja"
  ]
  node [
    id 171
    label "skubni&#281;cie"
  ]
  node [
    id 172
    label "zwierz&#281;ta"
  ]
  node [
    id 173
    label "fukni&#281;cie"
  ]
  node [
    id 174
    label "farba"
  ]
  node [
    id 175
    label "fukanie"
  ]
  node [
    id 176
    label "istota_&#380;ywa"
  ]
  node [
    id 177
    label "gad"
  ]
  node [
    id 178
    label "tresowa&#263;"
  ]
  node [
    id 179
    label "oswaja&#263;"
  ]
  node [
    id 180
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 181
    label "poligamia"
  ]
  node [
    id 182
    label "oz&#243;r"
  ]
  node [
    id 183
    label "skubn&#261;&#263;"
  ]
  node [
    id 184
    label "wios&#322;owa&#263;"
  ]
  node [
    id 185
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 186
    label "le&#380;enie"
  ]
  node [
    id 187
    label "niecz&#322;owiek"
  ]
  node [
    id 188
    label "wios&#322;owanie"
  ]
  node [
    id 189
    label "napasienie_si&#281;"
  ]
  node [
    id 190
    label "wiwarium"
  ]
  node [
    id 191
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 192
    label "animalista"
  ]
  node [
    id 193
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 194
    label "budowa"
  ]
  node [
    id 195
    label "hodowla"
  ]
  node [
    id 196
    label "pasienie_si&#281;"
  ]
  node [
    id 197
    label "sodomita"
  ]
  node [
    id 198
    label "monogamia"
  ]
  node [
    id 199
    label "przyssawka"
  ]
  node [
    id 200
    label "zachowanie"
  ]
  node [
    id 201
    label "budowa_cia&#322;a"
  ]
  node [
    id 202
    label "okrutnik"
  ]
  node [
    id 203
    label "grzbiet"
  ]
  node [
    id 204
    label "weterynarz"
  ]
  node [
    id 205
    label "&#322;eb"
  ]
  node [
    id 206
    label "wylinka"
  ]
  node [
    id 207
    label "bestia"
  ]
  node [
    id 208
    label "poskramia&#263;"
  ]
  node [
    id 209
    label "fauna"
  ]
  node [
    id 210
    label "treser"
  ]
  node [
    id 211
    label "siedzenie"
  ]
  node [
    id 212
    label "le&#380;e&#263;"
  ]
  node [
    id 213
    label "dziobni&#281;cie"
  ]
  node [
    id 214
    label "wysiadywa&#263;"
  ]
  node [
    id 215
    label "ko&#347;&#263;_potyliczna"
  ]
  node [
    id 216
    label "dzioba&#263;"
  ]
  node [
    id 217
    label "grzebie&#324;"
  ]
  node [
    id 218
    label "pi&#243;ro"
  ]
  node [
    id 219
    label "ptaki"
  ]
  node [
    id 220
    label "kuper"
  ]
  node [
    id 221
    label "hukni&#281;cie"
  ]
  node [
    id 222
    label "dziobn&#261;&#263;"
  ]
  node [
    id 223
    label "ptasz&#281;"
  ]
  node [
    id 224
    label "skrzyd&#322;o"
  ]
  node [
    id 225
    label "kloaka"
  ]
  node [
    id 226
    label "dzi&#243;bn&#261;&#263;"
  ]
  node [
    id 227
    label "wysiedzie&#263;"
  ]
  node [
    id 228
    label "upierzenie"
  ]
  node [
    id 229
    label "bird"
  ]
  node [
    id 230
    label "dziobanie"
  ]
  node [
    id 231
    label "pogwizdywanie"
  ]
  node [
    id 232
    label "dzi&#243;b"
  ]
  node [
    id 233
    label "ptactwo"
  ]
  node [
    id 234
    label "roz&#263;wierkanie_si&#281;"
  ]
  node [
    id 235
    label "zaklekotanie"
  ]
  node [
    id 236
    label "skok"
  ]
  node [
    id 237
    label "owodniowiec"
  ]
  node [
    id 238
    label "dzi&#243;bni&#281;cie"
  ]
  node [
    id 239
    label "sitowate"
  ]
  node [
    id 240
    label "ro&#347;lina_zielna"
  ]
  node [
    id 241
    label "cover"
  ]
  node [
    id 242
    label "thing"
  ]
  node [
    id 243
    label "cosik"
  ]
  node [
    id 244
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 245
    label "squawk"
  ]
  node [
    id 246
    label "&#380;aba"
  ]
  node [
    id 247
    label "zarechota&#263;"
  ]
  node [
    id 248
    label "kumkanie"
  ]
  node [
    id 249
    label "zaskrzecze&#263;"
  ]
  node [
    id 250
    label "p&#322;azy_bezogonowe"
  ]
  node [
    id 251
    label "p&#322;az"
  ]
  node [
    id 252
    label "&#380;abowate"
  ]
  node [
    id 253
    label "kumka&#263;"
  ]
  node [
    id 254
    label "zarechotanie"
  ]
  node [
    id 255
    label "rechotanie"
  ]
  node [
    id 256
    label "rechota&#263;"
  ]
  node [
    id 257
    label "zakumka&#263;"
  ]
  node [
    id 258
    label "zia&#263;"
  ]
  node [
    id 259
    label "hula&#263;"
  ]
  node [
    id 260
    label "wrzeszcze&#263;"
  ]
  node [
    id 261
    label "hucze&#263;"
  ]
  node [
    id 262
    label "mika&#263;"
  ]
  node [
    id 263
    label "p&#322;aka&#263;"
  ]
  node [
    id 264
    label "bawl"
  ]
  node [
    id 265
    label "&#347;mia&#263;_si&#281;"
  ]
  node [
    id 266
    label "krzycze&#263;"
  ]
  node [
    id 267
    label "w&#322;&#243;czy&#263;_si&#281;"
  ]
  node [
    id 268
    label "chodzi&#263;"
  ]
  node [
    id 269
    label "biega&#263;"
  ]
  node [
    id 270
    label "funkcjonowa&#263;"
  ]
  node [
    id 271
    label "lampartowa&#263;_si&#281;"
  ]
  node [
    id 272
    label "bomblowa&#263;"
  ]
  node [
    id 273
    label "rant"
  ]
  node [
    id 274
    label "rozrabia&#263;"
  ]
  node [
    id 275
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 276
    label "wia&#263;"
  ]
  node [
    id 277
    label "carouse"
  ]
  node [
    id 278
    label "storm"
  ]
  node [
    id 279
    label "biec"
  ]
  node [
    id 280
    label "bawi&#263;_si&#281;"
  ]
  node [
    id 281
    label "brzmie&#263;"
  ]
  node [
    id 282
    label "panoszy&#263;_si&#281;"
  ]
  node [
    id 283
    label "folgowa&#263;"
  ]
  node [
    id 284
    label "czyha&#263;"
  ]
  node [
    id 285
    label "szale&#263;"
  ]
  node [
    id 286
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 287
    label "lumpowa&#263;"
  ]
  node [
    id 288
    label "lumpowa&#263;_si&#281;"
  ]
  node [
    id 289
    label "bucha&#263;"
  ]
  node [
    id 290
    label "breathe"
  ]
  node [
    id 291
    label "gape"
  ]
  node [
    id 292
    label "oddycha&#263;"
  ]
  node [
    id 293
    label "czu&#263;"
  ]
  node [
    id 294
    label "blow"
  ]
  node [
    id 295
    label "wydziela&#263;"
  ]
  node [
    id 296
    label "emit"
  ]
  node [
    id 297
    label "peal"
  ]
  node [
    id 298
    label "sting"
  ]
  node [
    id 299
    label "robi&#263;"
  ]
  node [
    id 300
    label "wy&#263;"
  ]
  node [
    id 301
    label "cudowa&#263;"
  ]
  node [
    id 302
    label "szkoda"
  ]
  node [
    id 303
    label "backfire"
  ]
  node [
    id 304
    label "sorrow"
  ]
  node [
    id 305
    label "narzeka&#263;"
  ]
  node [
    id 306
    label "snivel"
  ]
  node [
    id 307
    label "reagowa&#263;"
  ]
  node [
    id 308
    label "pokrywa&#263;_si&#281;"
  ]
  node [
    id 309
    label "tu"
  ]
  node [
    id 310
    label "mie&#263;_miejsce"
  ]
  node [
    id 311
    label "plon"
  ]
  node [
    id 312
    label "give"
  ]
  node [
    id 313
    label "surrender"
  ]
  node [
    id 314
    label "kojarzy&#263;"
  ]
  node [
    id 315
    label "d&#378;wi&#281;k"
  ]
  node [
    id 316
    label "impart"
  ]
  node [
    id 317
    label "dawa&#263;"
  ]
  node [
    id 318
    label "reszta"
  ]
  node [
    id 319
    label "zapach"
  ]
  node [
    id 320
    label "wydawnictwo"
  ]
  node [
    id 321
    label "wiano"
  ]
  node [
    id 322
    label "produkcja"
  ]
  node [
    id 323
    label "podawa&#263;"
  ]
  node [
    id 324
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 325
    label "ujawnia&#263;"
  ]
  node [
    id 326
    label "placard"
  ]
  node [
    id 327
    label "powierza&#263;"
  ]
  node [
    id 328
    label "denuncjowa&#263;"
  ]
  node [
    id 329
    label "tajemnica"
  ]
  node [
    id 330
    label "panna_na_wydaniu"
  ]
  node [
    id 331
    label "wytwarza&#263;"
  ]
  node [
    id 332
    label "train"
  ]
  node [
    id 333
    label "przekazywa&#263;"
  ]
  node [
    id 334
    label "dostarcza&#263;"
  ]
  node [
    id 335
    label "&#322;adowa&#263;"
  ]
  node [
    id 336
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 337
    label "przeznacza&#263;"
  ]
  node [
    id 338
    label "traktowa&#263;"
  ]
  node [
    id 339
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 340
    label "obiecywa&#263;"
  ]
  node [
    id 341
    label "odst&#281;powa&#263;"
  ]
  node [
    id 342
    label "tender"
  ]
  node [
    id 343
    label "rap"
  ]
  node [
    id 344
    label "umieszcza&#263;"
  ]
  node [
    id 345
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 346
    label "t&#322;uc"
  ]
  node [
    id 347
    label "render"
  ]
  node [
    id 348
    label "wpiernicza&#263;"
  ]
  node [
    id 349
    label "exsert"
  ]
  node [
    id 350
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 351
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 352
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 353
    label "p&#322;aci&#263;"
  ]
  node [
    id 354
    label "hold_out"
  ]
  node [
    id 355
    label "nalewa&#263;"
  ]
  node [
    id 356
    label "zezwala&#263;"
  ]
  node [
    id 357
    label "hold"
  ]
  node [
    id 358
    label "organizowa&#263;"
  ]
  node [
    id 359
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 360
    label "czyni&#263;"
  ]
  node [
    id 361
    label "stylizowa&#263;"
  ]
  node [
    id 362
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 363
    label "falowa&#263;"
  ]
  node [
    id 364
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 365
    label "peddle"
  ]
  node [
    id 366
    label "praca"
  ]
  node [
    id 367
    label "wydala&#263;"
  ]
  node [
    id 368
    label "tentegowa&#263;"
  ]
  node [
    id 369
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 370
    label "urz&#261;dza&#263;"
  ]
  node [
    id 371
    label "oszukiwa&#263;"
  ]
  node [
    id 372
    label "work"
  ]
  node [
    id 373
    label "ukazywa&#263;"
  ]
  node [
    id 374
    label "przerabia&#263;"
  ]
  node [
    id 375
    label "act"
  ]
  node [
    id 376
    label "post&#281;powa&#263;"
  ]
  node [
    id 377
    label "rynek"
  ]
  node [
    id 378
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 379
    label "wprawia&#263;"
  ]
  node [
    id 380
    label "zaczyna&#263;"
  ]
  node [
    id 381
    label "wpisywa&#263;"
  ]
  node [
    id 382
    label "wchodzi&#263;"
  ]
  node [
    id 383
    label "take"
  ]
  node [
    id 384
    label "zapoznawa&#263;"
  ]
  node [
    id 385
    label "inflict"
  ]
  node [
    id 386
    label "schodzi&#263;"
  ]
  node [
    id 387
    label "induct"
  ]
  node [
    id 388
    label "begin"
  ]
  node [
    id 389
    label "create"
  ]
  node [
    id 390
    label "donosi&#263;"
  ]
  node [
    id 391
    label "inform"
  ]
  node [
    id 392
    label "demaskator"
  ]
  node [
    id 393
    label "dostrzega&#263;"
  ]
  node [
    id 394
    label "objawia&#263;"
  ]
  node [
    id 395
    label "unwrap"
  ]
  node [
    id 396
    label "informowa&#263;"
  ]
  node [
    id 397
    label "indicate"
  ]
  node [
    id 398
    label "zaskakiwa&#263;"
  ]
  node [
    id 399
    label "swat"
  ]
  node [
    id 400
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 401
    label "relate"
  ]
  node [
    id 402
    label "wyznawa&#263;"
  ]
  node [
    id 403
    label "oddawa&#263;"
  ]
  node [
    id 404
    label "confide"
  ]
  node [
    id 405
    label "zleca&#263;"
  ]
  node [
    id 406
    label "ufa&#263;"
  ]
  node [
    id 407
    label "command"
  ]
  node [
    id 408
    label "grant"
  ]
  node [
    id 409
    label "tenis"
  ]
  node [
    id 410
    label "deal"
  ]
  node [
    id 411
    label "stawia&#263;"
  ]
  node [
    id 412
    label "rozgrywa&#263;"
  ]
  node [
    id 413
    label "kelner"
  ]
  node [
    id 414
    label "siatk&#243;wka"
  ]
  node [
    id 415
    label "jedzenie"
  ]
  node [
    id 416
    label "faszerowa&#263;"
  ]
  node [
    id 417
    label "introduce"
  ]
  node [
    id 418
    label "serwowa&#263;"
  ]
  node [
    id 419
    label "kwota"
  ]
  node [
    id 420
    label "wydanie"
  ]
  node [
    id 421
    label "remainder"
  ]
  node [
    id 422
    label "pozosta&#322;y"
  ]
  node [
    id 423
    label "wyda&#263;"
  ]
  node [
    id 424
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 425
    label "impreza"
  ]
  node [
    id 426
    label "realizacja"
  ]
  node [
    id 427
    label "tingel-tangel"
  ]
  node [
    id 428
    label "numer"
  ]
  node [
    id 429
    label "monta&#380;"
  ]
  node [
    id 430
    label "postprodukcja"
  ]
  node [
    id 431
    label "performance"
  ]
  node [
    id 432
    label "fabrication"
  ]
  node [
    id 433
    label "product"
  ]
  node [
    id 434
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 435
    label "uzysk"
  ]
  node [
    id 436
    label "rozw&#243;j"
  ]
  node [
    id 437
    label "odtworzenie"
  ]
  node [
    id 438
    label "dorobek"
  ]
  node [
    id 439
    label "kreacja"
  ]
  node [
    id 440
    label "trema"
  ]
  node [
    id 441
    label "creation"
  ]
  node [
    id 442
    label "kooperowa&#263;"
  ]
  node [
    id 443
    label "return"
  ]
  node [
    id 444
    label "metr"
  ]
  node [
    id 445
    label "rezultat"
  ]
  node [
    id 446
    label "naturalia"
  ]
  node [
    id 447
    label "wypaplanie"
  ]
  node [
    id 448
    label "enigmat"
  ]
  node [
    id 449
    label "wiedza"
  ]
  node [
    id 450
    label "spos&#243;b"
  ]
  node [
    id 451
    label "zachowywanie"
  ]
  node [
    id 452
    label "secret"
  ]
  node [
    id 453
    label "obowi&#261;zek"
  ]
  node [
    id 454
    label "dyskrecja"
  ]
  node [
    id 455
    label "informacja"
  ]
  node [
    id 456
    label "rzecz"
  ]
  node [
    id 457
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 458
    label "taj&#324;"
  ]
  node [
    id 459
    label "zachowa&#263;"
  ]
  node [
    id 460
    label "zachowywa&#263;"
  ]
  node [
    id 461
    label "posa&#380;ek"
  ]
  node [
    id 462
    label "mienie"
  ]
  node [
    id 463
    label "wydawa&#263;_za_m&#261;&#380;"
  ]
  node [
    id 464
    label "wyda&#263;_za_m&#261;&#380;"
  ]
  node [
    id 465
    label "debit"
  ]
  node [
    id 466
    label "redaktor"
  ]
  node [
    id 467
    label "druk"
  ]
  node [
    id 468
    label "publikacja"
  ]
  node [
    id 469
    label "redakcja"
  ]
  node [
    id 470
    label "szata_graficzna"
  ]
  node [
    id 471
    label "firma"
  ]
  node [
    id 472
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 473
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 474
    label "poster"
  ]
  node [
    id 475
    label "phone"
  ]
  node [
    id 476
    label "wpadni&#281;cie"
  ]
  node [
    id 477
    label "intonacja"
  ]
  node [
    id 478
    label "wpa&#347;&#263;"
  ]
  node [
    id 479
    label "note"
  ]
  node [
    id 480
    label "onomatopeja"
  ]
  node [
    id 481
    label "modalizm"
  ]
  node [
    id 482
    label "nadlecenie"
  ]
  node [
    id 483
    label "sound"
  ]
  node [
    id 484
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 485
    label "wpada&#263;"
  ]
  node [
    id 486
    label "solmizacja"
  ]
  node [
    id 487
    label "seria"
  ]
  node [
    id 488
    label "dobiec"
  ]
  node [
    id 489
    label "transmiter"
  ]
  node [
    id 490
    label "heksachord"
  ]
  node [
    id 491
    label "akcent"
  ]
  node [
    id 492
    label "repetycja"
  ]
  node [
    id 493
    label "brzmienie"
  ]
  node [
    id 494
    label "wpadanie"
  ]
  node [
    id 495
    label "liczba_kwantowa"
  ]
  node [
    id 496
    label "kosmetyk"
  ]
  node [
    id 497
    label "ciasto"
  ]
  node [
    id 498
    label "aromat"
  ]
  node [
    id 499
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 500
    label "puff"
  ]
  node [
    id 501
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 502
    label "przyprawa"
  ]
  node [
    id 503
    label "upojno&#347;&#263;"
  ]
  node [
    id 504
    label "owiewanie"
  ]
  node [
    id 505
    label "smak"
  ]
  node [
    id 506
    label "intensywny"
  ]
  node [
    id 507
    label "udolny"
  ]
  node [
    id 508
    label "skuteczny"
  ]
  node [
    id 509
    label "&#347;mieszny"
  ]
  node [
    id 510
    label "niczegowaty"
  ]
  node [
    id 511
    label "dobrze"
  ]
  node [
    id 512
    label "nieszpetny"
  ]
  node [
    id 513
    label "spory"
  ]
  node [
    id 514
    label "pozytywny"
  ]
  node [
    id 515
    label "korzystny"
  ]
  node [
    id 516
    label "nie&#378;le"
  ]
  node [
    id 517
    label "kulturalny"
  ]
  node [
    id 518
    label "skromny"
  ]
  node [
    id 519
    label "grzeczny"
  ]
  node [
    id 520
    label "stosowny"
  ]
  node [
    id 521
    label "przystojny"
  ]
  node [
    id 522
    label "moralny"
  ]
  node [
    id 523
    label "przyzwoicie"
  ]
  node [
    id 524
    label "wystarczaj&#261;cy"
  ]
  node [
    id 525
    label "nietuzinkowy"
  ]
  node [
    id 526
    label "intryguj&#261;cy"
  ]
  node [
    id 527
    label "ch&#281;tny"
  ]
  node [
    id 528
    label "swoisty"
  ]
  node [
    id 529
    label "interesowanie"
  ]
  node [
    id 530
    label "interesuj&#261;cy"
  ]
  node [
    id 531
    label "ciekawie"
  ]
  node [
    id 532
    label "indagator"
  ]
  node [
    id 533
    label "charakterystycznie"
  ]
  node [
    id 534
    label "szczeg&#243;lny"
  ]
  node [
    id 535
    label "wyj&#261;tkowy"
  ]
  node [
    id 536
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 537
    label "podobny"
  ]
  node [
    id 538
    label "dziwnie"
  ]
  node [
    id 539
    label "dziwy"
  ]
  node [
    id 540
    label "inny"
  ]
  node [
    id 541
    label "w_miar&#281;"
  ]
  node [
    id 542
    label "jako_taki"
  ]
  node [
    id 543
    label "resonance"
  ]
  node [
    id 544
    label "proces"
  ]
  node [
    id 545
    label "boski"
  ]
  node [
    id 546
    label "krajobraz"
  ]
  node [
    id 547
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 548
    label "przywidzenie"
  ]
  node [
    id 549
    label "presence"
  ]
  node [
    id 550
    label "charakter"
  ]
  node [
    id 551
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 552
    label "wyra&#380;anie"
  ]
  node [
    id 553
    label "tone"
  ]
  node [
    id 554
    label "wydawanie"
  ]
  node [
    id 555
    label "spirit"
  ]
  node [
    id 556
    label "rejestr"
  ]
  node [
    id 557
    label "kolorystyka"
  ]
  node [
    id 558
    label "instrumentacja_g&#322;oskowa"
  ]
  node [
    id 559
    label "leksem"
  ]
  node [
    id 560
    label "figura_s&#322;&#243;w"
  ]
  node [
    id 561
    label "wymy&#347;lenie"
  ]
  node [
    id 562
    label "spotkanie"
  ]
  node [
    id 563
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 564
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 565
    label "ulegni&#281;cie"
  ]
  node [
    id 566
    label "collapse"
  ]
  node [
    id 567
    label "poniesienie"
  ]
  node [
    id 568
    label "ciecz"
  ]
  node [
    id 569
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 570
    label "odwiedzenie"
  ]
  node [
    id 571
    label "uderzenie"
  ]
  node [
    id 572
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 573
    label "rzeka"
  ]
  node [
    id 574
    label "postrzeganie"
  ]
  node [
    id 575
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 576
    label "&#347;wiat&#322;o"
  ]
  node [
    id 577
    label "dostanie_si&#281;"
  ]
  node [
    id 578
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 579
    label "release"
  ]
  node [
    id 580
    label "rozbicie_si&#281;"
  ]
  node [
    id 581
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 582
    label "uleganie"
  ]
  node [
    id 583
    label "dostawanie_si&#281;"
  ]
  node [
    id 584
    label "odwiedzanie"
  ]
  node [
    id 585
    label "spotykanie"
  ]
  node [
    id 586
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 587
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 588
    label "wymy&#347;lanie"
  ]
  node [
    id 589
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 590
    label "ingress"
  ]
  node [
    id 591
    label "dzianie_si&#281;"
  ]
  node [
    id 592
    label "wp&#322;ywanie"
  ]
  node [
    id 593
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 594
    label "overlap"
  ]
  node [
    id 595
    label "wkl&#281;sanie"
  ]
  node [
    id 596
    label "powierzy&#263;"
  ]
  node [
    id 597
    label "pieni&#261;dze"
  ]
  node [
    id 598
    label "skojarzy&#263;"
  ]
  node [
    id 599
    label "zadenuncjowa&#263;"
  ]
  node [
    id 600
    label "da&#263;"
  ]
  node [
    id 601
    label "zrobi&#263;"
  ]
  node [
    id 602
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 603
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 604
    label "translate"
  ]
  node [
    id 605
    label "picture"
  ]
  node [
    id 606
    label "poda&#263;"
  ]
  node [
    id 607
    label "wprowadzi&#263;"
  ]
  node [
    id 608
    label "wytworzy&#263;"
  ]
  node [
    id 609
    label "dress"
  ]
  node [
    id 610
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 611
    label "supply"
  ]
  node [
    id 612
    label "ujawni&#263;"
  ]
  node [
    id 613
    label "delivery"
  ]
  node [
    id 614
    label "zdarzenie_si&#281;"
  ]
  node [
    id 615
    label "rendition"
  ]
  node [
    id 616
    label "egzemplarz"
  ]
  node [
    id 617
    label "impression"
  ]
  node [
    id 618
    label "zadenuncjowanie"
  ]
  node [
    id 619
    label "wytworzenie"
  ]
  node [
    id 620
    label "issue"
  ]
  node [
    id 621
    label "danie"
  ]
  node [
    id 622
    label "czasopismo"
  ]
  node [
    id 623
    label "podanie"
  ]
  node [
    id 624
    label "wprowadzenie"
  ]
  node [
    id 625
    label "odmiana"
  ]
  node [
    id 626
    label "ujawnienie"
  ]
  node [
    id 627
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 628
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 629
    label "urz&#261;dzenie"
  ]
  node [
    id 630
    label "zrobienie"
  ]
  node [
    id 631
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 632
    label "strike"
  ]
  node [
    id 633
    label "zaziera&#263;"
  ]
  node [
    id 634
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 635
    label "spotyka&#263;"
  ]
  node [
    id 636
    label "drop"
  ]
  node [
    id 637
    label "pogo"
  ]
  node [
    id 638
    label "ogrom"
  ]
  node [
    id 639
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 640
    label "popada&#263;"
  ]
  node [
    id 641
    label "odwiedza&#263;"
  ]
  node [
    id 642
    label "wymy&#347;la&#263;"
  ]
  node [
    id 643
    label "przypomina&#263;"
  ]
  node [
    id 644
    label "ujmowa&#263;"
  ]
  node [
    id 645
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 646
    label "chowa&#263;"
  ]
  node [
    id 647
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 648
    label "demaskowa&#263;"
  ]
  node [
    id 649
    label "ulega&#263;"
  ]
  node [
    id 650
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 651
    label "emocja"
  ]
  node [
    id 652
    label "flatten"
  ]
  node [
    id 653
    label "ulec"
  ]
  node [
    id 654
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 655
    label "fall_upon"
  ]
  node [
    id 656
    label "ponie&#347;&#263;"
  ]
  node [
    id 657
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 658
    label "uderzy&#263;"
  ]
  node [
    id 659
    label "wymy&#347;li&#263;"
  ]
  node [
    id 660
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 661
    label "decline"
  ]
  node [
    id 662
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 663
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 664
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 665
    label "spotka&#263;"
  ]
  node [
    id 666
    label "odwiedzi&#263;"
  ]
  node [
    id 667
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 668
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 669
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 670
    label "equal"
  ]
  node [
    id 671
    label "si&#281;ga&#263;"
  ]
  node [
    id 672
    label "stan"
  ]
  node [
    id 673
    label "obecno&#347;&#263;"
  ]
  node [
    id 674
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 675
    label "uczestniczy&#263;"
  ]
  node [
    id 676
    label "gaworzy&#263;"
  ]
  node [
    id 677
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 678
    label "remark"
  ]
  node [
    id 679
    label "rozmawia&#263;"
  ]
  node [
    id 680
    label "wyra&#380;a&#263;"
  ]
  node [
    id 681
    label "umie&#263;"
  ]
  node [
    id 682
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 683
    label "dziama&#263;"
  ]
  node [
    id 684
    label "formu&#322;owa&#263;"
  ]
  node [
    id 685
    label "dysfonia"
  ]
  node [
    id 686
    label "express"
  ]
  node [
    id 687
    label "talk"
  ]
  node [
    id 688
    label "u&#380;ywa&#263;"
  ]
  node [
    id 689
    label "prawi&#263;"
  ]
  node [
    id 690
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 691
    label "powiada&#263;"
  ]
  node [
    id 692
    label "tell"
  ]
  node [
    id 693
    label "chew_the_fat"
  ]
  node [
    id 694
    label "say"
  ]
  node [
    id 695
    label "j&#281;zyk"
  ]
  node [
    id 696
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 697
    label "wydobywa&#263;"
  ]
  node [
    id 698
    label "okre&#347;la&#263;"
  ]
  node [
    id 699
    label "korzysta&#263;"
  ]
  node [
    id 700
    label "distribute"
  ]
  node [
    id 701
    label "bash"
  ]
  node [
    id 702
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 703
    label "doznawa&#263;"
  ]
  node [
    id 704
    label "decydowa&#263;"
  ]
  node [
    id 705
    label "signify"
  ]
  node [
    id 706
    label "style"
  ]
  node [
    id 707
    label "komunikowa&#263;"
  ]
  node [
    id 708
    label "znaczy&#263;"
  ]
  node [
    id 709
    label "give_voice"
  ]
  node [
    id 710
    label "oznacza&#263;"
  ]
  node [
    id 711
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 712
    label "represent"
  ]
  node [
    id 713
    label "convey"
  ]
  node [
    id 714
    label "arouse"
  ]
  node [
    id 715
    label "determine"
  ]
  node [
    id 716
    label "reakcja_chemiczna"
  ]
  node [
    id 717
    label "uwydatnia&#263;"
  ]
  node [
    id 718
    label "eksploatowa&#263;"
  ]
  node [
    id 719
    label "uzyskiwa&#263;"
  ]
  node [
    id 720
    label "wydostawa&#263;"
  ]
  node [
    id 721
    label "wyjmowa&#263;"
  ]
  node [
    id 722
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 723
    label "dobywa&#263;"
  ]
  node [
    id 724
    label "ocala&#263;"
  ]
  node [
    id 725
    label "excavate"
  ]
  node [
    id 726
    label "g&#243;rnictwo"
  ]
  node [
    id 727
    label "raise"
  ]
  node [
    id 728
    label "wiedzie&#263;"
  ]
  node [
    id 729
    label "can"
  ]
  node [
    id 730
    label "m&#243;c"
  ]
  node [
    id 731
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 732
    label "szczeka&#263;"
  ]
  node [
    id 733
    label "mawia&#263;"
  ]
  node [
    id 734
    label "opowiada&#263;"
  ]
  node [
    id 735
    label "chatter"
  ]
  node [
    id 736
    label "niemowl&#281;"
  ]
  node [
    id 737
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 738
    label "stanowisko_archeologiczne"
  ]
  node [
    id 739
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 740
    label "artykulator"
  ]
  node [
    id 741
    label "kod"
  ]
  node [
    id 742
    label "kawa&#322;ek"
  ]
  node [
    id 743
    label "przedmiot"
  ]
  node [
    id 744
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 745
    label "gramatyka"
  ]
  node [
    id 746
    label "stylik"
  ]
  node [
    id 747
    label "przet&#322;umaczenie"
  ]
  node [
    id 748
    label "formalizowanie"
  ]
  node [
    id 749
    label "ssanie"
  ]
  node [
    id 750
    label "ssa&#263;"
  ]
  node [
    id 751
    label "language"
  ]
  node [
    id 752
    label "liza&#263;"
  ]
  node [
    id 753
    label "napisa&#263;"
  ]
  node [
    id 754
    label "konsonantyzm"
  ]
  node [
    id 755
    label "wokalizm"
  ]
  node [
    id 756
    label "pisa&#263;"
  ]
  node [
    id 757
    label "fonetyka"
  ]
  node [
    id 758
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 759
    label "jeniec"
  ]
  node [
    id 760
    label "but"
  ]
  node [
    id 761
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 762
    label "po_koroniarsku"
  ]
  node [
    id 763
    label "kultura_duchowa"
  ]
  node [
    id 764
    label "t&#322;umaczenie"
  ]
  node [
    id 765
    label "m&#243;wienie"
  ]
  node [
    id 766
    label "pype&#263;"
  ]
  node [
    id 767
    label "lizanie"
  ]
  node [
    id 768
    label "pismo"
  ]
  node [
    id 769
    label "formalizowa&#263;"
  ]
  node [
    id 770
    label "organ"
  ]
  node [
    id 771
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 772
    label "rozumienie"
  ]
  node [
    id 773
    label "makroglosja"
  ]
  node [
    id 774
    label "jama_ustna"
  ]
  node [
    id 775
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 776
    label "formacja_geologiczna"
  ]
  node [
    id 777
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 778
    label "natural_language"
  ]
  node [
    id 779
    label "s&#322;ownictwo"
  ]
  node [
    id 780
    label "dysphonia"
  ]
  node [
    id 781
    label "dysleksja"
  ]
  node [
    id 782
    label "trudny"
  ]
  node [
    id 783
    label "hard"
  ]
  node [
    id 784
    label "k&#322;opotliwy"
  ]
  node [
    id 785
    label "skomplikowany"
  ]
  node [
    id 786
    label "ci&#281;&#380;ko"
  ]
  node [
    id 787
    label "wymagaj&#261;cy"
  ]
  node [
    id 788
    label "oceni&#263;"
  ]
  node [
    id 789
    label "skuma&#263;"
  ]
  node [
    id 790
    label "poczu&#263;"
  ]
  node [
    id 791
    label "do"
  ]
  node [
    id 792
    label "zacz&#261;&#263;"
  ]
  node [
    id 793
    label "think"
  ]
  node [
    id 794
    label "visualize"
  ]
  node [
    id 795
    label "okre&#347;li&#263;"
  ]
  node [
    id 796
    label "wystawi&#263;"
  ]
  node [
    id 797
    label "evaluate"
  ]
  node [
    id 798
    label "znale&#378;&#263;"
  ]
  node [
    id 799
    label "pomy&#347;le&#263;"
  ]
  node [
    id 800
    label "post&#261;pi&#263;"
  ]
  node [
    id 801
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 802
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 803
    label "odj&#261;&#263;"
  ]
  node [
    id 804
    label "cause"
  ]
  node [
    id 805
    label "feel"
  ]
  node [
    id 806
    label "zagorze&#263;"
  ]
  node [
    id 807
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 808
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 809
    label "his"
  ]
  node [
    id 810
    label "ut"
  ]
  node [
    id 811
    label "C"
  ]
  node [
    id 812
    label "Messi"
  ]
  node [
    id 813
    label "Herkules_Poirot"
  ]
  node [
    id 814
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 815
    label "Achilles"
  ]
  node [
    id 816
    label "Harry_Potter"
  ]
  node [
    id 817
    label "Casanova"
  ]
  node [
    id 818
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 819
    label "Zgredek"
  ]
  node [
    id 820
    label "Asterix"
  ]
  node [
    id 821
    label "Winnetou"
  ]
  node [
    id 822
    label "uczestnik"
  ]
  node [
    id 823
    label "posta&#263;"
  ]
  node [
    id 824
    label "&#347;mia&#322;ek"
  ]
  node [
    id 825
    label "Mario"
  ]
  node [
    id 826
    label "Borewicz"
  ]
  node [
    id 827
    label "Quasimodo"
  ]
  node [
    id 828
    label "Sherlock_Holmes"
  ]
  node [
    id 829
    label "Wallenrod"
  ]
  node [
    id 830
    label "Herkules"
  ]
  node [
    id 831
    label "bohaterski"
  ]
  node [
    id 832
    label "Don_Juan"
  ]
  node [
    id 833
    label "podmiot"
  ]
  node [
    id 834
    label "Don_Kiszot"
  ]
  node [
    id 835
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 836
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 837
    label "Hamlet"
  ]
  node [
    id 838
    label "Werter"
  ]
  node [
    id 839
    label "Szwejk"
  ]
  node [
    id 840
    label "charakterystyka"
  ]
  node [
    id 841
    label "zaistnie&#263;"
  ]
  node [
    id 842
    label "Osjan"
  ]
  node [
    id 843
    label "kto&#347;"
  ]
  node [
    id 844
    label "wygl&#261;d"
  ]
  node [
    id 845
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 846
    label "osobowo&#347;&#263;"
  ]
  node [
    id 847
    label "trim"
  ]
  node [
    id 848
    label "poby&#263;"
  ]
  node [
    id 849
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 850
    label "Aspazja"
  ]
  node [
    id 851
    label "punkt_widzenia"
  ]
  node [
    id 852
    label "kompleksja"
  ]
  node [
    id 853
    label "wytrzyma&#263;"
  ]
  node [
    id 854
    label "formacja"
  ]
  node [
    id 855
    label "pozosta&#263;"
  ]
  node [
    id 856
    label "point"
  ]
  node [
    id 857
    label "przedstawienie"
  ]
  node [
    id 858
    label "go&#347;&#263;"
  ]
  node [
    id 859
    label "ludzko&#347;&#263;"
  ]
  node [
    id 860
    label "asymilowanie"
  ]
  node [
    id 861
    label "wapniak"
  ]
  node [
    id 862
    label "asymilowa&#263;"
  ]
  node [
    id 863
    label "os&#322;abia&#263;"
  ]
  node [
    id 864
    label "hominid"
  ]
  node [
    id 865
    label "podw&#322;adny"
  ]
  node [
    id 866
    label "os&#322;abianie"
  ]
  node [
    id 867
    label "g&#322;owa"
  ]
  node [
    id 868
    label "figura"
  ]
  node [
    id 869
    label "portrecista"
  ]
  node [
    id 870
    label "dwun&#243;g"
  ]
  node [
    id 871
    label "profanum"
  ]
  node [
    id 872
    label "nasada"
  ]
  node [
    id 873
    label "duch"
  ]
  node [
    id 874
    label "antropochoria"
  ]
  node [
    id 875
    label "osoba"
  ]
  node [
    id 876
    label "wz&#243;r"
  ]
  node [
    id 877
    label "senior"
  ]
  node [
    id 878
    label "oddzia&#322;ywanie"
  ]
  node [
    id 879
    label "Adam"
  ]
  node [
    id 880
    label "homo_sapiens"
  ]
  node [
    id 881
    label "polifag"
  ]
  node [
    id 882
    label "zuch"
  ]
  node [
    id 883
    label "rycerzyk"
  ]
  node [
    id 884
    label "odwa&#380;ny"
  ]
  node [
    id 885
    label "ryzykant"
  ]
  node [
    id 886
    label "morowiec"
  ]
  node [
    id 887
    label "trawa"
  ]
  node [
    id 888
    label "ro&#347;lina"
  ]
  node [
    id 889
    label "twardziel"
  ]
  node [
    id 890
    label "owsowe"
  ]
  node [
    id 891
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 892
    label "byt"
  ]
  node [
    id 893
    label "organizacja"
  ]
  node [
    id 894
    label "prawo"
  ]
  node [
    id 895
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 896
    label "nauka_prawa"
  ]
  node [
    id 897
    label "Szekspir"
  ]
  node [
    id 898
    label "Mickiewicz"
  ]
  node [
    id 899
    label "cierpienie"
  ]
  node [
    id 900
    label "Atomowa_Pch&#322;a"
  ]
  node [
    id 901
    label "bohatersko"
  ]
  node [
    id 902
    label "dzielny"
  ]
  node [
    id 903
    label "silny"
  ]
  node [
    id 904
    label "waleczny"
  ]
  node [
    id 905
    label "kuma&#263;"
  ]
  node [
    id 906
    label "match"
  ]
  node [
    id 907
    label "empatia"
  ]
  node [
    id 908
    label "odbiera&#263;"
  ]
  node [
    id 909
    label "see"
  ]
  node [
    id 910
    label "zna&#263;"
  ]
  node [
    id 911
    label "cognizance"
  ]
  node [
    id 912
    label "postrzega&#263;"
  ]
  node [
    id 913
    label "przewidywa&#263;"
  ]
  node [
    id 914
    label "smell"
  ]
  node [
    id 915
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 916
    label "uczuwa&#263;"
  ]
  node [
    id 917
    label "anticipate"
  ]
  node [
    id 918
    label "zabiera&#263;"
  ]
  node [
    id 919
    label "zlecenie"
  ]
  node [
    id 920
    label "odzyskiwa&#263;"
  ]
  node [
    id 921
    label "radio"
  ]
  node [
    id 922
    label "przyjmowa&#263;"
  ]
  node [
    id 923
    label "bra&#263;"
  ]
  node [
    id 924
    label "antena"
  ]
  node [
    id 925
    label "liszy&#263;"
  ]
  node [
    id 926
    label "pozbawia&#263;"
  ]
  node [
    id 927
    label "telewizor"
  ]
  node [
    id 928
    label "konfiskowa&#263;"
  ]
  node [
    id 929
    label "deprive"
  ]
  node [
    id 930
    label "accept"
  ]
  node [
    id 931
    label "zrozumienie"
  ]
  node [
    id 932
    label "empathy"
  ]
  node [
    id 933
    label "lito&#347;&#263;"
  ]
  node [
    id 934
    label "wczuwa&#263;_si&#281;"
  ]
  node [
    id 935
    label "ci&#261;gle"
  ]
  node [
    id 936
    label "stale"
  ]
  node [
    id 937
    label "ci&#261;g&#322;y"
  ]
  node [
    id 938
    label "nieprzerwanie"
  ]
  node [
    id 939
    label "react"
  ]
  node [
    id 940
    label "ponosi&#263;"
  ]
  node [
    id 941
    label "report"
  ]
  node [
    id 942
    label "pytanie"
  ]
  node [
    id 943
    label "equate"
  ]
  node [
    id 944
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 945
    label "answer"
  ]
  node [
    id 946
    label "contend"
  ]
  node [
    id 947
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 948
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 949
    label "motywowa&#263;"
  ]
  node [
    id 950
    label "wst&#281;powa&#263;"
  ]
  node [
    id 951
    label "str&#243;j"
  ]
  node [
    id 952
    label "hurt"
  ]
  node [
    id 953
    label "digest"
  ]
  node [
    id 954
    label "make"
  ]
  node [
    id 955
    label "bolt"
  ]
  node [
    id 956
    label "odci&#261;ga&#263;"
  ]
  node [
    id 957
    label "umowa"
  ]
  node [
    id 958
    label "sprawa"
  ]
  node [
    id 959
    label "wypytanie"
  ]
  node [
    id 960
    label "egzaminowanie"
  ]
  node [
    id 961
    label "zwracanie_si&#281;"
  ]
  node [
    id 962
    label "wywo&#322;ywanie"
  ]
  node [
    id 963
    label "rozpytywanie"
  ]
  node [
    id 964
    label "wypowiedzenie"
  ]
  node [
    id 965
    label "wypowied&#378;"
  ]
  node [
    id 966
    label "problemat"
  ]
  node [
    id 967
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 968
    label "problematyka"
  ]
  node [
    id 969
    label "sprawdzian"
  ]
  node [
    id 970
    label "zadanie"
  ]
  node [
    id 971
    label "przes&#322;uchiwanie"
  ]
  node [
    id 972
    label "question"
  ]
  node [
    id 973
    label "sprawdzanie"
  ]
  node [
    id 974
    label "odpowiadanie"
  ]
  node [
    id 975
    label "survey"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 260
  ]
  edge [
    source 11
    target 261
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 285
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 11
    target 287
  ]
  edge [
    source 11
    target 288
  ]
  edge [
    source 11
    target 289
  ]
  edge [
    source 11
    target 290
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 291
  ]
  edge [
    source 11
    target 292
  ]
  edge [
    source 11
    target 293
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 295
  ]
  edge [
    source 11
    target 296
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 297
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 300
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 309
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 299
  ]
  edge [
    source 13
    target 310
  ]
  edge [
    source 13
    target 311
  ]
  edge [
    source 13
    target 312
  ]
  edge [
    source 13
    target 313
  ]
  edge [
    source 13
    target 314
  ]
  edge [
    source 13
    target 315
  ]
  edge [
    source 13
    target 316
  ]
  edge [
    source 13
    target 317
  ]
  edge [
    source 13
    target 318
  ]
  edge [
    source 13
    target 319
  ]
  edge [
    source 13
    target 320
  ]
  edge [
    source 13
    target 321
  ]
  edge [
    source 13
    target 322
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 325
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 327
  ]
  edge [
    source 13
    target 328
  ]
  edge [
    source 13
    target 329
  ]
  edge [
    source 13
    target 330
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 334
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 336
  ]
  edge [
    source 13
    target 337
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 339
  ]
  edge [
    source 13
    target 340
  ]
  edge [
    source 13
    target 341
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 13
    target 343
  ]
  edge [
    source 13
    target 344
  ]
  edge [
    source 13
    target 345
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 352
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 355
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 241
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 406
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 52
  ]
  edge [
    source 13
    target 433
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 29
  ]
  edge [
    source 14
    target 30
  ]
  edge [
    source 14
    target 31
  ]
  edge [
    source 14
    target 32
  ]
  edge [
    source 14
    target 33
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 14
    target 507
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 509
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 511
  ]
  edge [
    source 14
    target 512
  ]
  edge [
    source 14
    target 513
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 522
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 14
    target 529
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 531
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 543
  ]
  edge [
    source 15
    target 420
  ]
  edge [
    source 15
    target 476
  ]
  edge [
    source 15
    target 315
  ]
  edge [
    source 15
    target 494
  ]
  edge [
    source 15
    target 483
  ]
  edge [
    source 15
    target 493
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 423
  ]
  edge [
    source 15
    target 478
  ]
  edge [
    source 15
    target 479
  ]
  edge [
    source 15
    target 480
  ]
  edge [
    source 15
    target 485
  ]
  edge [
    source 15
    target 475
  ]
  edge [
    source 15
    target 477
  ]
  edge [
    source 15
    target 481
  ]
  edge [
    source 15
    target 482
  ]
  edge [
    source 15
    target 484
  ]
  edge [
    source 15
    target 486
  ]
  edge [
    source 15
    target 487
  ]
  edge [
    source 15
    target 488
  ]
  edge [
    source 15
    target 489
  ]
  edge [
    source 15
    target 490
  ]
  edge [
    source 15
    target 491
  ]
  edge [
    source 15
    target 492
  ]
  edge [
    source 15
    target 544
  ]
  edge [
    source 15
    target 545
  ]
  edge [
    source 15
    target 546
  ]
  edge [
    source 15
    target 42
  ]
  edge [
    source 15
    target 547
  ]
  edge [
    source 15
    target 548
  ]
  edge [
    source 15
    target 549
  ]
  edge [
    source 15
    target 550
  ]
  edge [
    source 15
    target 551
  ]
  edge [
    source 15
    target 552
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 553
  ]
  edge [
    source 15
    target 554
  ]
  edge [
    source 15
    target 555
  ]
  edge [
    source 15
    target 556
  ]
  edge [
    source 15
    target 557
  ]
  edge [
    source 15
    target 558
  ]
  edge [
    source 15
    target 559
  ]
  edge [
    source 15
    target 560
  ]
  edge [
    source 15
    target 561
  ]
  edge [
    source 15
    target 562
  ]
  edge [
    source 15
    target 563
  ]
  edge [
    source 15
    target 564
  ]
  edge [
    source 15
    target 565
  ]
  edge [
    source 15
    target 566
  ]
  edge [
    source 15
    target 66
  ]
  edge [
    source 15
    target 456
  ]
  edge [
    source 15
    target 567
  ]
  edge [
    source 15
    target 319
  ]
  edge [
    source 15
    target 568
  ]
  edge [
    source 15
    target 569
  ]
  edge [
    source 15
    target 570
  ]
  edge [
    source 15
    target 571
  ]
  edge [
    source 15
    target 572
  ]
  edge [
    source 15
    target 573
  ]
  edge [
    source 15
    target 574
  ]
  edge [
    source 15
    target 575
  ]
  edge [
    source 15
    target 576
  ]
  edge [
    source 15
    target 577
  ]
  edge [
    source 15
    target 578
  ]
  edge [
    source 15
    target 579
  ]
  edge [
    source 15
    target 580
  ]
  edge [
    source 15
    target 581
  ]
  edge [
    source 15
    target 582
  ]
  edge [
    source 15
    target 583
  ]
  edge [
    source 15
    target 584
  ]
  edge [
    source 15
    target 585
  ]
  edge [
    source 15
    target 586
  ]
  edge [
    source 15
    target 587
  ]
  edge [
    source 15
    target 588
  ]
  edge [
    source 15
    target 589
  ]
  edge [
    source 15
    target 590
  ]
  edge [
    source 15
    target 591
  ]
  edge [
    source 15
    target 592
  ]
  edge [
    source 15
    target 593
  ]
  edge [
    source 15
    target 594
  ]
  edge [
    source 15
    target 595
  ]
  edge [
    source 15
    target 596
  ]
  edge [
    source 15
    target 597
  ]
  edge [
    source 15
    target 311
  ]
  edge [
    source 15
    target 312
  ]
  edge [
    source 15
    target 598
  ]
  edge [
    source 15
    target 599
  ]
  edge [
    source 15
    target 316
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 318
  ]
  edge [
    source 15
    target 320
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 602
  ]
  edge [
    source 15
    target 603
  ]
  edge [
    source 15
    target 321
  ]
  edge [
    source 15
    target 322
  ]
  edge [
    source 15
    target 604
  ]
  edge [
    source 15
    target 605
  ]
  edge [
    source 15
    target 606
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 608
  ]
  edge [
    source 15
    target 609
  ]
  edge [
    source 15
    target 610
  ]
  edge [
    source 15
    target 329
  ]
  edge [
    source 15
    target 330
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 15
    target 612
  ]
  edge [
    source 15
    target 613
  ]
  edge [
    source 15
    target 614
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 616
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 468
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 299
  ]
  edge [
    source 15
    target 310
  ]
  edge [
    source 15
    target 313
  ]
  edge [
    source 15
    target 314
  ]
  edge [
    source 15
    target 317
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 323
  ]
  edge [
    source 15
    target 324
  ]
  edge [
    source 15
    target 325
  ]
  edge [
    source 15
    target 326
  ]
  edge [
    source 15
    target 327
  ]
  edge [
    source 15
    target 328
  ]
  edge [
    source 15
    target 331
  ]
  edge [
    source 15
    target 332
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 293
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 659
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 15
    target 663
  ]
  edge [
    source 15
    target 664
  ]
  edge [
    source 15
    target 665
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 310
  ]
  edge [
    source 16
    target 670
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 268
  ]
  edge [
    source 16
    target 671
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 673
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 676
  ]
  edge [
    source 17
    target 677
  ]
  edge [
    source 17
    target 678
  ]
  edge [
    source 17
    target 679
  ]
  edge [
    source 17
    target 680
  ]
  edge [
    source 17
    target 681
  ]
  edge [
    source 17
    target 682
  ]
  edge [
    source 17
    target 683
  ]
  edge [
    source 17
    target 244
  ]
  edge [
    source 17
    target 684
  ]
  edge [
    source 17
    target 685
  ]
  edge [
    source 17
    target 686
  ]
  edge [
    source 17
    target 324
  ]
  edge [
    source 17
    target 687
  ]
  edge [
    source 17
    target 688
  ]
  edge [
    source 17
    target 689
  ]
  edge [
    source 17
    target 690
  ]
  edge [
    source 17
    target 691
  ]
  edge [
    source 17
    target 692
  ]
  edge [
    source 17
    target 693
  ]
  edge [
    source 17
    target 694
  ]
  edge [
    source 17
    target 695
  ]
  edge [
    source 17
    target 696
  ]
  edge [
    source 17
    target 396
  ]
  edge [
    source 17
    target 697
  ]
  edge [
    source 17
    target 698
  ]
  edge [
    source 17
    target 699
  ]
  edge [
    source 17
    target 700
  ]
  edge [
    source 17
    target 312
  ]
  edge [
    source 17
    target 701
  ]
  edge [
    source 17
    target 702
  ]
  edge [
    source 17
    target 703
  ]
  edge [
    source 17
    target 704
  ]
  edge [
    source 17
    target 705
  ]
  edge [
    source 17
    target 706
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 707
  ]
  edge [
    source 17
    target 391
  ]
  edge [
    source 17
    target 708
  ]
  edge [
    source 17
    target 709
  ]
  edge [
    source 17
    target 710
  ]
  edge [
    source 17
    target 711
  ]
  edge [
    source 17
    target 712
  ]
  edge [
    source 17
    target 713
  ]
  edge [
    source 17
    target 714
  ]
  edge [
    source 17
    target 299
  ]
  edge [
    source 17
    target 715
  ]
  edge [
    source 17
    target 372
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 717
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 719
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 332
  ]
  edge [
    source 17
    target 722
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 724
  ]
  edge [
    source 17
    target 725
  ]
  edge [
    source 17
    target 726
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 728
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 270
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 496
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 766
  ]
  edge [
    source 17
    target 767
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 770
  ]
  edge [
    source 17
    target 771
  ]
  edge [
    source 17
    target 772
  ]
  edge [
    source 17
    target 450
  ]
  edge [
    source 17
    target 773
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 776
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 629
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 782
  ]
  edge [
    source 18
    target 783
  ]
  edge [
    source 18
    target 784
  ]
  edge [
    source 18
    target 785
  ]
  edge [
    source 18
    target 786
  ]
  edge [
    source 18
    target 787
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 788
  ]
  edge [
    source 19
    target 789
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 19
    target 791
  ]
  edge [
    source 19
    target 792
  ]
  edge [
    source 19
    target 793
  ]
  edge [
    source 19
    target 794
  ]
  edge [
    source 19
    target 795
  ]
  edge [
    source 19
    target 601
  ]
  edge [
    source 19
    target 796
  ]
  edge [
    source 19
    target 797
  ]
  edge [
    source 19
    target 798
  ]
  edge [
    source 19
    target 799
  ]
  edge [
    source 19
    target 800
  ]
  edge [
    source 19
    target 801
  ]
  edge [
    source 19
    target 802
  ]
  edge [
    source 19
    target 803
  ]
  edge [
    source 19
    target 804
  ]
  edge [
    source 19
    target 417
  ]
  edge [
    source 19
    target 388
  ]
  edge [
    source 19
    target 610
  ]
  edge [
    source 19
    target 805
  ]
  edge [
    source 19
    target 806
  ]
  edge [
    source 19
    target 807
  ]
  edge [
    source 19
    target 808
  ]
  edge [
    source 19
    target 809
  ]
  edge [
    source 19
    target 315
  ]
  edge [
    source 19
    target 810
  ]
  edge [
    source 19
    target 811
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 812
  ]
  edge [
    source 21
    target 813
  ]
  edge [
    source 21
    target 79
  ]
  edge [
    source 21
    target 814
  ]
  edge [
    source 21
    target 815
  ]
  edge [
    source 21
    target 816
  ]
  edge [
    source 21
    target 817
  ]
  edge [
    source 21
    target 818
  ]
  edge [
    source 21
    target 819
  ]
  edge [
    source 21
    target 820
  ]
  edge [
    source 21
    target 821
  ]
  edge [
    source 21
    target 822
  ]
  edge [
    source 21
    target 823
  ]
  edge [
    source 21
    target 824
  ]
  edge [
    source 21
    target 825
  ]
  edge [
    source 21
    target 826
  ]
  edge [
    source 21
    target 827
  ]
  edge [
    source 21
    target 828
  ]
  edge [
    source 21
    target 829
  ]
  edge [
    source 21
    target 830
  ]
  edge [
    source 21
    target 831
  ]
  edge [
    source 21
    target 832
  ]
  edge [
    source 21
    target 833
  ]
  edge [
    source 21
    target 834
  ]
  edge [
    source 21
    target 835
  ]
  edge [
    source 21
    target 836
  ]
  edge [
    source 21
    target 837
  ]
  edge [
    source 21
    target 838
  ]
  edge [
    source 21
    target 839
  ]
  edge [
    source 21
    target 840
  ]
  edge [
    source 21
    target 841
  ]
  edge [
    source 21
    target 92
  ]
  edge [
    source 21
    target 842
  ]
  edge [
    source 21
    target 843
  ]
  edge [
    source 21
    target 844
  ]
  edge [
    source 21
    target 845
  ]
  edge [
    source 21
    target 846
  ]
  edge [
    source 21
    target 64
  ]
  edge [
    source 21
    target 847
  ]
  edge [
    source 21
    target 848
  ]
  edge [
    source 21
    target 849
  ]
  edge [
    source 21
    target 850
  ]
  edge [
    source 21
    target 851
  ]
  edge [
    source 21
    target 852
  ]
  edge [
    source 21
    target 853
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 854
  ]
  edge [
    source 21
    target 855
  ]
  edge [
    source 21
    target 856
  ]
  edge [
    source 21
    target 857
  ]
  edge [
    source 21
    target 858
  ]
  edge [
    source 21
    target 859
  ]
  edge [
    source 21
    target 860
  ]
  edge [
    source 21
    target 861
  ]
  edge [
    source 21
    target 862
  ]
  edge [
    source 21
    target 863
  ]
  edge [
    source 21
    target 864
  ]
  edge [
    source 21
    target 865
  ]
  edge [
    source 21
    target 866
  ]
  edge [
    source 21
    target 867
  ]
  edge [
    source 21
    target 868
  ]
  edge [
    source 21
    target 869
  ]
  edge [
    source 21
    target 870
  ]
  edge [
    source 21
    target 871
  ]
  edge [
    source 21
    target 37
  ]
  edge [
    source 21
    target 872
  ]
  edge [
    source 21
    target 873
  ]
  edge [
    source 21
    target 874
  ]
  edge [
    source 21
    target 875
  ]
  edge [
    source 21
    target 876
  ]
  edge [
    source 21
    target 877
  ]
  edge [
    source 21
    target 878
  ]
  edge [
    source 21
    target 879
  ]
  edge [
    source 21
    target 880
  ]
  edge [
    source 21
    target 881
  ]
  edge [
    source 21
    target 882
  ]
  edge [
    source 21
    target 883
  ]
  edge [
    source 21
    target 884
  ]
  edge [
    source 21
    target 885
  ]
  edge [
    source 21
    target 886
  ]
  edge [
    source 21
    target 887
  ]
  edge [
    source 21
    target 888
  ]
  edge [
    source 21
    target 889
  ]
  edge [
    source 21
    target 890
  ]
  edge [
    source 21
    target 891
  ]
  edge [
    source 21
    target 892
  ]
  edge [
    source 21
    target 893
  ]
  edge [
    source 21
    target 894
  ]
  edge [
    source 21
    target 895
  ]
  edge [
    source 21
    target 896
  ]
  edge [
    source 21
    target 897
  ]
  edge [
    source 21
    target 898
  ]
  edge [
    source 21
    target 899
  ]
  edge [
    source 21
    target 900
  ]
  edge [
    source 21
    target 901
  ]
  edge [
    source 21
    target 902
  ]
  edge [
    source 21
    target 903
  ]
  edge [
    source 21
    target 904
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 728
  ]
  edge [
    source 22
    target 905
  ]
  edge [
    source 22
    target 293
  ]
  edge [
    source 22
    target 312
  ]
  edge [
    source 22
    target 683
  ]
  edge [
    source 22
    target 906
  ]
  edge [
    source 22
    target 907
  ]
  edge [
    source 22
    target 695
  ]
  edge [
    source 22
    target 908
  ]
  edge [
    source 22
    target 909
  ]
  edge [
    source 22
    target 910
  ]
  edge [
    source 22
    target 911
  ]
  edge [
    source 22
    target 912
  ]
  edge [
    source 22
    target 913
  ]
  edge [
    source 22
    target 129
  ]
  edge [
    source 22
    target 914
  ]
  edge [
    source 22
    target 915
  ]
  edge [
    source 22
    target 916
  ]
  edge [
    source 22
    target 555
  ]
  edge [
    source 22
    target 703
  ]
  edge [
    source 22
    target 917
  ]
  edge [
    source 22
    target 918
  ]
  edge [
    source 22
    target 919
  ]
  edge [
    source 22
    target 920
  ]
  edge [
    source 22
    target 921
  ]
  edge [
    source 22
    target 922
  ]
  edge [
    source 22
    target 923
  ]
  edge [
    source 22
    target 924
  ]
  edge [
    source 22
    target 142
  ]
  edge [
    source 22
    target 925
  ]
  edge [
    source 22
    target 926
  ]
  edge [
    source 22
    target 927
  ]
  edge [
    source 22
    target 928
  ]
  edge [
    source 22
    target 929
  ]
  edge [
    source 22
    target 930
  ]
  edge [
    source 22
    target 739
  ]
  edge [
    source 22
    target 740
  ]
  edge [
    source 22
    target 741
  ]
  edge [
    source 22
    target 742
  ]
  edge [
    source 22
    target 743
  ]
  edge [
    source 22
    target 744
  ]
  edge [
    source 22
    target 745
  ]
  edge [
    source 22
    target 746
  ]
  edge [
    source 22
    target 747
  ]
  edge [
    source 22
    target 748
  ]
  edge [
    source 22
    target 749
  ]
  edge [
    source 22
    target 750
  ]
  edge [
    source 22
    target 751
  ]
  edge [
    source 22
    target 752
  ]
  edge [
    source 22
    target 753
  ]
  edge [
    source 22
    target 754
  ]
  edge [
    source 22
    target 755
  ]
  edge [
    source 22
    target 756
  ]
  edge [
    source 22
    target 757
  ]
  edge [
    source 22
    target 758
  ]
  edge [
    source 22
    target 759
  ]
  edge [
    source 22
    target 760
  ]
  edge [
    source 22
    target 761
  ]
  edge [
    source 22
    target 762
  ]
  edge [
    source 22
    target 763
  ]
  edge [
    source 22
    target 764
  ]
  edge [
    source 22
    target 765
  ]
  edge [
    source 22
    target 766
  ]
  edge [
    source 22
    target 767
  ]
  edge [
    source 22
    target 768
  ]
  edge [
    source 22
    target 769
  ]
  edge [
    source 22
    target 770
  ]
  edge [
    source 22
    target 771
  ]
  edge [
    source 22
    target 772
  ]
  edge [
    source 22
    target 450
  ]
  edge [
    source 22
    target 773
  ]
  edge [
    source 22
    target 774
  ]
  edge [
    source 22
    target 775
  ]
  edge [
    source 22
    target 776
  ]
  edge [
    source 22
    target 777
  ]
  edge [
    source 22
    target 778
  ]
  edge [
    source 22
    target 779
  ]
  edge [
    source 22
    target 629
  ]
  edge [
    source 22
    target 931
  ]
  edge [
    source 22
    target 932
  ]
  edge [
    source 22
    target 933
  ]
  edge [
    source 22
    target 934
  ]
  edge [
    source 22
    target 731
  ]
  edge [
    source 22
    target 732
  ]
  edge [
    source 22
    target 679
  ]
  edge [
    source 22
    target 270
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 935
  ]
  edge [
    source 23
    target 936
  ]
  edge [
    source 23
    target 937
  ]
  edge [
    source 23
    target 938
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 939
  ]
  edge [
    source 24
    target 317
  ]
  edge [
    source 24
    target 129
  ]
  edge [
    source 24
    target 940
  ]
  edge [
    source 24
    target 941
  ]
  edge [
    source 24
    target 942
  ]
  edge [
    source 24
    target 943
  ]
  edge [
    source 24
    target 944
  ]
  edge [
    source 24
    target 945
  ]
  edge [
    source 24
    target 150
  ]
  edge [
    source 24
    target 553
  ]
  edge [
    source 24
    target 946
  ]
  edge [
    source 24
    target 307
  ]
  edge [
    source 24
    target 316
  ]
  edge [
    source 24
    target 947
  ]
  edge [
    source 24
    target 675
  ]
  edge [
    source 24
    target 310
  ]
  edge [
    source 24
    target 948
  ]
  edge [
    source 24
    target 949
  ]
  edge [
    source 24
    target 375
  ]
  edge [
    source 24
    target 690
  ]
  edge [
    source 24
    target 669
  ]
  edge [
    source 24
    target 670
  ]
  edge [
    source 24
    target 116
  ]
  edge [
    source 24
    target 268
  ]
  edge [
    source 24
    target 671
  ]
  edge [
    source 24
    target 672
  ]
  edge [
    source 24
    target 673
  ]
  edge [
    source 24
    target 137
  ]
  edge [
    source 24
    target 674
  ]
  edge [
    source 24
    target 333
  ]
  edge [
    source 24
    target 334
  ]
  edge [
    source 24
    target 299
  ]
  edge [
    source 24
    target 335
  ]
  edge [
    source 24
    target 336
  ]
  edge [
    source 24
    target 312
  ]
  edge [
    source 24
    target 337
  ]
  edge [
    source 24
    target 313
  ]
  edge [
    source 24
    target 338
  ]
  edge [
    source 24
    target 339
  ]
  edge [
    source 24
    target 340
  ]
  edge [
    source 24
    target 341
  ]
  edge [
    source 24
    target 342
  ]
  edge [
    source 24
    target 343
  ]
  edge [
    source 24
    target 344
  ]
  edge [
    source 24
    target 345
  ]
  edge [
    source 24
    target 346
  ]
  edge [
    source 24
    target 327
  ]
  edge [
    source 24
    target 347
  ]
  edge [
    source 24
    target 348
  ]
  edge [
    source 24
    target 349
  ]
  edge [
    source 24
    target 350
  ]
  edge [
    source 24
    target 332
  ]
  edge [
    source 24
    target 351
  ]
  edge [
    source 24
    target 352
  ]
  edge [
    source 24
    target 353
  ]
  edge [
    source 24
    target 354
  ]
  edge [
    source 24
    target 355
  ]
  edge [
    source 24
    target 356
  ]
  edge [
    source 24
    target 357
  ]
  edge [
    source 24
    target 950
  ]
  edge [
    source 24
    target 951
  ]
  edge [
    source 24
    target 952
  ]
  edge [
    source 24
    target 953
  ]
  edge [
    source 24
    target 954
  ]
  edge [
    source 24
    target 955
  ]
  edge [
    source 24
    target 956
  ]
  edge [
    source 24
    target 703
  ]
  edge [
    source 24
    target 957
  ]
  edge [
    source 24
    target 241
  ]
  edge [
    source 24
    target 958
  ]
  edge [
    source 24
    target 959
  ]
  edge [
    source 24
    target 960
  ]
  edge [
    source 24
    target 961
  ]
  edge [
    source 24
    target 962
  ]
  edge [
    source 24
    target 963
  ]
  edge [
    source 24
    target 964
  ]
  edge [
    source 24
    target 965
  ]
  edge [
    source 24
    target 966
  ]
  edge [
    source 24
    target 967
  ]
  edge [
    source 24
    target 968
  ]
  edge [
    source 24
    target 969
  ]
  edge [
    source 24
    target 970
  ]
  edge [
    source 24
    target 971
  ]
  edge [
    source 24
    target 972
  ]
  edge [
    source 24
    target 973
  ]
  edge [
    source 24
    target 974
  ]
  edge [
    source 24
    target 975
  ]
]
