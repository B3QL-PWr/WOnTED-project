graph [
  node [
    id 0
    label "tadeusz"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;odek"
    origin "text"
  ]
  node [
    id 2
    label "Tadeusz"
  ]
  node [
    id 3
    label "W&#322;odek"
  ]
  node [
    id 4
    label "Jakoba"
  ]
  node [
    id 5
    label "Sieversa"
  ]
  node [
    id 6
    label "Osip"
  ]
  node [
    id 7
    label "Igelstr&#246;ma"
  ]
  node [
    id 8
    label "Stanis&#322;awa"
  ]
  node [
    id 9
    label "august"
  ]
  node [
    id 10
    label "poniatowski"
  ]
  node [
    id 11
    label "rada"
  ]
  node [
    id 12
    label "ustawa&#263;"
  ]
  node [
    id 13
    label "gazeta"
  ]
  node [
    id 14
    label "krajowy"
  ]
  node [
    id 15
    label "powsta&#263;"
  ]
  node [
    id 16
    label "ko&#347;ciuszkowski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
]
