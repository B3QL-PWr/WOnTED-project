graph [
  node [
    id 0
    label "babcia"
    origin "text"
  ]
  node [
    id 1
    label "letni"
    origin "text"
  ]
  node [
    id 2
    label "wnuczek"
    origin "text"
  ]
  node [
    id 3
    label "pewno"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "spodziewa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przodkini"
  ]
  node [
    id 7
    label "baba"
  ]
  node [
    id 8
    label "babulinka"
  ]
  node [
    id 9
    label "kobieta"
  ]
  node [
    id 10
    label "dziadkowie"
  ]
  node [
    id 11
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 12
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 13
    label "pokolenie"
  ]
  node [
    id 14
    label "krewna"
  ]
  node [
    id 15
    label "doros&#322;y"
  ]
  node [
    id 16
    label "&#380;ona"
  ]
  node [
    id 17
    label "cz&#322;owiek"
  ]
  node [
    id 18
    label "samica"
  ]
  node [
    id 19
    label "uleganie"
  ]
  node [
    id 20
    label "ulec"
  ]
  node [
    id 21
    label "m&#281;&#380;yna"
  ]
  node [
    id 22
    label "partnerka"
  ]
  node [
    id 23
    label "ulegni&#281;cie"
  ]
  node [
    id 24
    label "pa&#324;stwo"
  ]
  node [
    id 25
    label "&#322;ono"
  ]
  node [
    id 26
    label "menopauza"
  ]
  node [
    id 27
    label "przekwitanie"
  ]
  node [
    id 28
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 29
    label "babka"
  ]
  node [
    id 30
    label "ulega&#263;"
  ]
  node [
    id 31
    label "zniewie&#347;cialec"
  ]
  node [
    id 32
    label "bag"
  ]
  node [
    id 33
    label "ciasto"
  ]
  node [
    id 34
    label "oferma"
  ]
  node [
    id 35
    label "figura"
  ]
  node [
    id 36
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 37
    label "staruszka"
  ]
  node [
    id 38
    label "istota_&#380;ywa"
  ]
  node [
    id 39
    label "kafar"
  ]
  node [
    id 40
    label "mazgaj"
  ]
  node [
    id 41
    label "latowy"
  ]
  node [
    id 42
    label "typowy"
  ]
  node [
    id 43
    label "weso&#322;y"
  ]
  node [
    id 44
    label "s&#322;oneczny"
  ]
  node [
    id 45
    label "sezonowy"
  ]
  node [
    id 46
    label "ciep&#322;y"
  ]
  node [
    id 47
    label "letnio"
  ]
  node [
    id 48
    label "oboj&#281;tny"
  ]
  node [
    id 49
    label "nijaki"
  ]
  node [
    id 50
    label "nijak"
  ]
  node [
    id 51
    label "niezabawny"
  ]
  node [
    id 52
    label "&#380;aden"
  ]
  node [
    id 53
    label "zwyczajny"
  ]
  node [
    id 54
    label "poszarzenie"
  ]
  node [
    id 55
    label "neutralny"
  ]
  node [
    id 56
    label "szarzenie"
  ]
  node [
    id 57
    label "bezbarwnie"
  ]
  node [
    id 58
    label "nieciekawy"
  ]
  node [
    id 59
    label "czasowy"
  ]
  node [
    id 60
    label "sezonowo"
  ]
  node [
    id 61
    label "zoboj&#281;tnienie"
  ]
  node [
    id 62
    label "nieszkodliwy"
  ]
  node [
    id 63
    label "&#347;ni&#281;ty"
  ]
  node [
    id 64
    label "oboj&#281;tnie"
  ]
  node [
    id 65
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 66
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 67
    label "niewa&#380;ny"
  ]
  node [
    id 68
    label "neutralizowanie"
  ]
  node [
    id 69
    label "bierny"
  ]
  node [
    id 70
    label "zneutralizowanie"
  ]
  node [
    id 71
    label "pijany"
  ]
  node [
    id 72
    label "weso&#322;o"
  ]
  node [
    id 73
    label "pozytywny"
  ]
  node [
    id 74
    label "beztroski"
  ]
  node [
    id 75
    label "dobry"
  ]
  node [
    id 76
    label "mi&#322;y"
  ]
  node [
    id 77
    label "ocieplanie_si&#281;"
  ]
  node [
    id 78
    label "ocieplanie"
  ]
  node [
    id 79
    label "grzanie"
  ]
  node [
    id 80
    label "ocieplenie_si&#281;"
  ]
  node [
    id 81
    label "zagrzanie"
  ]
  node [
    id 82
    label "ocieplenie"
  ]
  node [
    id 83
    label "korzystny"
  ]
  node [
    id 84
    label "przyjemny"
  ]
  node [
    id 85
    label "ciep&#322;o"
  ]
  node [
    id 86
    label "s&#322;onecznie"
  ]
  node [
    id 87
    label "bezdeszczowy"
  ]
  node [
    id 88
    label "bezchmurny"
  ]
  node [
    id 89
    label "pogodny"
  ]
  node [
    id 90
    label "fotowoltaiczny"
  ]
  node [
    id 91
    label "jasny"
  ]
  node [
    id 92
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 93
    label "typowo"
  ]
  node [
    id 94
    label "cz&#281;sty"
  ]
  node [
    id 95
    label "zwyk&#322;y"
  ]
  node [
    id 96
    label "potomek"
  ]
  node [
    id 97
    label "wnucz&#281;"
  ]
  node [
    id 98
    label "wnucz&#281;ta"
  ]
  node [
    id 99
    label "krewny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
]
