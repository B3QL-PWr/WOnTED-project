graph [
  node [
    id 0
    label "prowizorium"
    origin "text"
  ]
  node [
    id 1
    label "bud&#380;etowy"
    origin "text"
  ]
  node [
    id 2
    label "tymczasowo&#347;&#263;"
  ]
  node [
    id 3
    label "cecha"
  ]
  node [
    id 4
    label "bud&#380;etowo"
  ]
  node [
    id 5
    label "budgetary"
  ]
  node [
    id 6
    label "etatowy"
  ]
  node [
    id 7
    label "zatrudniony"
  ]
  node [
    id 8
    label "oficjalny"
  ]
  node [
    id 9
    label "etatowo"
  ]
  node [
    id 10
    label "sta&#322;y"
  ]
  node [
    id 11
    label "rada"
  ]
  node [
    id 12
    label "minister"
  ]
  node [
    id 13
    label "konstytucja"
  ]
  node [
    id 14
    label "marcowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
]
