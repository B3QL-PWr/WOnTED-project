graph [
  node [
    id 0
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 1
    label "media"
    origin "text"
  ]
  node [
    id 2
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 3
    label "nawo&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "eksterminacja"
    origin "text"
  ]
  node [
    id 5
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 6
    label "palesty&#324;ski"
    origin "text"
  ]
  node [
    id 7
    label "negowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "istnienie"
    origin "text"
  ]
  node [
    id 9
    label "j&#281;zyk_angielski"
  ]
  node [
    id 10
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 11
    label "fajny"
  ]
  node [
    id 12
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 13
    label "po_ameryka&#324;sku"
  ]
  node [
    id 14
    label "typowy"
  ]
  node [
    id 15
    label "anglosaski"
  ]
  node [
    id 16
    label "boston"
  ]
  node [
    id 17
    label "pepperoni"
  ]
  node [
    id 18
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 19
    label "nowoczesny"
  ]
  node [
    id 20
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 21
    label "zachodni"
  ]
  node [
    id 22
    label "Princeton"
  ]
  node [
    id 23
    label "charakterystyczny"
  ]
  node [
    id 24
    label "cake-walk"
  ]
  node [
    id 25
    label "po_anglosasku"
  ]
  node [
    id 26
    label "anglosasko"
  ]
  node [
    id 27
    label "zachodny"
  ]
  node [
    id 28
    label "nowy"
  ]
  node [
    id 29
    label "nowo&#380;ytny"
  ]
  node [
    id 30
    label "otwarty"
  ]
  node [
    id 31
    label "nowocze&#347;nie"
  ]
  node [
    id 32
    label "byczy"
  ]
  node [
    id 33
    label "fajnie"
  ]
  node [
    id 34
    label "klawy"
  ]
  node [
    id 35
    label "dobry"
  ]
  node [
    id 36
    label "charakterystycznie"
  ]
  node [
    id 37
    label "szczeg&#243;lny"
  ]
  node [
    id 38
    label "wyj&#261;tkowy"
  ]
  node [
    id 39
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 40
    label "podobny"
  ]
  node [
    id 41
    label "zwyczajny"
  ]
  node [
    id 42
    label "typowo"
  ]
  node [
    id 43
    label "cz&#281;sty"
  ]
  node [
    id 44
    label "zwyk&#322;y"
  ]
  node [
    id 45
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 46
    label "nale&#380;ny"
  ]
  node [
    id 47
    label "nale&#380;yty"
  ]
  node [
    id 48
    label "uprawniony"
  ]
  node [
    id 49
    label "zasadniczy"
  ]
  node [
    id 50
    label "stosownie"
  ]
  node [
    id 51
    label "taki"
  ]
  node [
    id 52
    label "prawdziwy"
  ]
  node [
    id 53
    label "ten"
  ]
  node [
    id 54
    label "taniec_towarzyski"
  ]
  node [
    id 55
    label "melodia"
  ]
  node [
    id 56
    label "taniec"
  ]
  node [
    id 57
    label "tkanina_we&#322;niana"
  ]
  node [
    id 58
    label "walc"
  ]
  node [
    id 59
    label "ubrani&#243;wka"
  ]
  node [
    id 60
    label "salami"
  ]
  node [
    id 61
    label "mass-media"
  ]
  node [
    id 62
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 63
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 64
    label "przekazior"
  ]
  node [
    id 65
    label "uzbrajanie"
  ]
  node [
    id 66
    label "medium"
  ]
  node [
    id 67
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 68
    label "&#347;rodek"
  ]
  node [
    id 69
    label "jasnowidz"
  ]
  node [
    id 70
    label "hipnoza"
  ]
  node [
    id 71
    label "cz&#322;owiek"
  ]
  node [
    id 72
    label "spirytysta"
  ]
  node [
    id 73
    label "otoczenie"
  ]
  node [
    id 74
    label "publikator"
  ]
  node [
    id 75
    label "warunki"
  ]
  node [
    id 76
    label "strona"
  ]
  node [
    id 77
    label "przeka&#378;nik"
  ]
  node [
    id 78
    label "&#347;rodek_przekazu"
  ]
  node [
    id 79
    label "armament"
  ]
  node [
    id 80
    label "arming"
  ]
  node [
    id 81
    label "instalacja"
  ]
  node [
    id 82
    label "wyposa&#380;anie"
  ]
  node [
    id 83
    label "dozbrajanie"
  ]
  node [
    id 84
    label "dozbrojenie"
  ]
  node [
    id 85
    label "montowanie"
  ]
  node [
    id 86
    label "free"
  ]
  node [
    id 87
    label "urge"
  ]
  node [
    id 88
    label "address"
  ]
  node [
    id 89
    label "prosi&#263;"
  ]
  node [
    id 90
    label "wzywa&#263;"
  ]
  node [
    id 91
    label "invite"
  ]
  node [
    id 92
    label "cry"
  ]
  node [
    id 93
    label "nakazywa&#263;"
  ]
  node [
    id 94
    label "donosi&#263;"
  ]
  node [
    id 95
    label "pobudza&#263;"
  ]
  node [
    id 96
    label "order"
  ]
  node [
    id 97
    label "poleca&#263;"
  ]
  node [
    id 98
    label "trwa&#263;"
  ]
  node [
    id 99
    label "zaprasza&#263;"
  ]
  node [
    id 100
    label "zach&#281;ca&#263;"
  ]
  node [
    id 101
    label "suffice"
  ]
  node [
    id 102
    label "preach"
  ]
  node [
    id 103
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 104
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 105
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 106
    label "pies"
  ]
  node [
    id 107
    label "zezwala&#263;"
  ]
  node [
    id 108
    label "ask"
  ]
  node [
    id 109
    label "cover"
  ]
  node [
    id 110
    label "mord"
  ]
  node [
    id 111
    label "genocide"
  ]
  node [
    id 112
    label "przest&#281;pstwo"
  ]
  node [
    id 113
    label "zbrodnia"
  ]
  node [
    id 114
    label "zag&#322;ada"
  ]
  node [
    id 115
    label "zabicie"
  ]
  node [
    id 116
    label "Irokezi"
  ]
  node [
    id 117
    label "ludno&#347;&#263;"
  ]
  node [
    id 118
    label "Apacze"
  ]
  node [
    id 119
    label "Syngalezi"
  ]
  node [
    id 120
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 121
    label "t&#322;um"
  ]
  node [
    id 122
    label "Mohikanie"
  ]
  node [
    id 123
    label "Komancze"
  ]
  node [
    id 124
    label "lud"
  ]
  node [
    id 125
    label "Siuksowie"
  ]
  node [
    id 126
    label "Buriaci"
  ]
  node [
    id 127
    label "Samojedzi"
  ]
  node [
    id 128
    label "Baszkirzy"
  ]
  node [
    id 129
    label "Wotiacy"
  ]
  node [
    id 130
    label "Aztekowie"
  ]
  node [
    id 131
    label "nacja"
  ]
  node [
    id 132
    label "Czejenowie"
  ]
  node [
    id 133
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 134
    label "innowierstwo"
  ]
  node [
    id 135
    label "ch&#322;opstwo"
  ]
  node [
    id 136
    label "grupa"
  ]
  node [
    id 137
    label "najazd"
  ]
  node [
    id 138
    label "demofobia"
  ]
  node [
    id 139
    label "Tagalowie"
  ]
  node [
    id 140
    label "Ugrowie"
  ]
  node [
    id 141
    label "Retowie"
  ]
  node [
    id 142
    label "Negryci"
  ]
  node [
    id 143
    label "Ladynowie"
  ]
  node [
    id 144
    label "Wizygoci"
  ]
  node [
    id 145
    label "Dogonowie"
  ]
  node [
    id 146
    label "chamstwo"
  ]
  node [
    id 147
    label "Do&#322;ganie"
  ]
  node [
    id 148
    label "Indoira&#324;czycy"
  ]
  node [
    id 149
    label "gmin"
  ]
  node [
    id 150
    label "Kozacy"
  ]
  node [
    id 151
    label "Indoariowie"
  ]
  node [
    id 152
    label "Maroni"
  ]
  node [
    id 153
    label "Kumbrowie"
  ]
  node [
    id 154
    label "Po&#322;owcy"
  ]
  node [
    id 155
    label "Nogajowie"
  ]
  node [
    id 156
    label "Nawahowie"
  ]
  node [
    id 157
    label "Wenedowie"
  ]
  node [
    id 158
    label "Majowie"
  ]
  node [
    id 159
    label "Kipczacy"
  ]
  node [
    id 160
    label "Frygijczycy"
  ]
  node [
    id 161
    label "Paleoazjaci"
  ]
  node [
    id 162
    label "Tocharowie"
  ]
  node [
    id 163
    label "etnogeneza"
  ]
  node [
    id 164
    label "nationality"
  ]
  node [
    id 165
    label "Mongolia"
  ]
  node [
    id 166
    label "Sri_Lanka"
  ]
  node [
    id 167
    label "zapiera&#263;"
  ]
  node [
    id 168
    label "repudiate"
  ]
  node [
    id 169
    label "kwestionowa&#263;"
  ]
  node [
    id 170
    label "sabotage"
  ]
  node [
    id 171
    label "stara&#263;_si&#281;"
  ]
  node [
    id 172
    label "przepiera&#263;"
  ]
  node [
    id 173
    label "wstrzymywa&#263;"
  ]
  node [
    id 174
    label "pcha&#263;"
  ]
  node [
    id 175
    label "zatyka&#263;"
  ]
  node [
    id 176
    label "zaprzecza&#263;"
  ]
  node [
    id 177
    label "blokowa&#263;"
  ]
  node [
    id 178
    label "opiera&#263;"
  ]
  node [
    id 179
    label "prze&#263;"
  ]
  node [
    id 180
    label "odbiera&#263;"
  ]
  node [
    id 181
    label "urzeczywistnianie"
  ]
  node [
    id 182
    label "utrzymywanie"
  ]
  node [
    id 183
    label "bycie"
  ]
  node [
    id 184
    label "byt"
  ]
  node [
    id 185
    label "produkowanie"
  ]
  node [
    id 186
    label "znikni&#281;cie"
  ]
  node [
    id 187
    label "being"
  ]
  node [
    id 188
    label "robienie"
  ]
  node [
    id 189
    label "utrzyma&#263;"
  ]
  node [
    id 190
    label "egzystencja"
  ]
  node [
    id 191
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 192
    label "utrzymanie"
  ]
  node [
    id 193
    label "wyprodukowanie"
  ]
  node [
    id 194
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 195
    label "entity"
  ]
  node [
    id 196
    label "utrzymywa&#263;"
  ]
  node [
    id 197
    label "integer"
  ]
  node [
    id 198
    label "liczba"
  ]
  node [
    id 199
    label "zlewanie_si&#281;"
  ]
  node [
    id 200
    label "ilo&#347;&#263;"
  ]
  node [
    id 201
    label "uk&#322;ad"
  ]
  node [
    id 202
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 203
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 204
    label "pe&#322;ny"
  ]
  node [
    id 205
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 206
    label "obroni&#263;"
  ]
  node [
    id 207
    label "potrzyma&#263;"
  ]
  node [
    id 208
    label "op&#322;aci&#263;"
  ]
  node [
    id 209
    label "manewr"
  ]
  node [
    id 210
    label "zdo&#322;a&#263;"
  ]
  node [
    id 211
    label "podtrzyma&#263;"
  ]
  node [
    id 212
    label "feed"
  ]
  node [
    id 213
    label "zrobi&#263;"
  ]
  node [
    id 214
    label "przetrzyma&#263;"
  ]
  node [
    id 215
    label "foster"
  ]
  node [
    id 216
    label "preserve"
  ]
  node [
    id 217
    label "zapewni&#263;"
  ]
  node [
    id 218
    label "zachowa&#263;"
  ]
  node [
    id 219
    label "unie&#347;&#263;"
  ]
  node [
    id 220
    label "obronienie"
  ]
  node [
    id 221
    label "zap&#322;acenie"
  ]
  node [
    id 222
    label "zachowanie"
  ]
  node [
    id 223
    label "potrzymanie"
  ]
  node [
    id 224
    label "przetrzymanie"
  ]
  node [
    id 225
    label "preservation"
  ]
  node [
    id 226
    label "bearing"
  ]
  node [
    id 227
    label "zdo&#322;anie"
  ]
  node [
    id 228
    label "subsystencja"
  ]
  node [
    id 229
    label "uniesienie"
  ]
  node [
    id 230
    label "wy&#380;ywienie"
  ]
  node [
    id 231
    label "zapewnienie"
  ]
  node [
    id 232
    label "podtrzymanie"
  ]
  node [
    id 233
    label "wychowanie"
  ]
  node [
    id 234
    label "zrobienie"
  ]
  node [
    id 235
    label "bronienie"
  ]
  node [
    id 236
    label "trzymanie"
  ]
  node [
    id 237
    label "podtrzymywanie"
  ]
  node [
    id 238
    label "wychowywanie"
  ]
  node [
    id 239
    label "panowanie"
  ]
  node [
    id 240
    label "zachowywanie"
  ]
  node [
    id 241
    label "twierdzenie"
  ]
  node [
    id 242
    label "chowanie"
  ]
  node [
    id 243
    label "retention"
  ]
  node [
    id 244
    label "op&#322;acanie"
  ]
  node [
    id 245
    label "s&#261;dzenie"
  ]
  node [
    id 246
    label "zapewnianie"
  ]
  node [
    id 247
    label "obejrzenie"
  ]
  node [
    id 248
    label "widzenie"
  ]
  node [
    id 249
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 250
    label "przeszkodzenie"
  ]
  node [
    id 251
    label "przeszkadzanie"
  ]
  node [
    id 252
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 253
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 254
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 255
    label "wiek_matuzalemowy"
  ]
  node [
    id 256
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 257
    label "argue"
  ]
  node [
    id 258
    label "podtrzymywa&#263;"
  ]
  node [
    id 259
    label "s&#261;dzi&#263;"
  ]
  node [
    id 260
    label "twierdzi&#263;"
  ]
  node [
    id 261
    label "zapewnia&#263;"
  ]
  node [
    id 262
    label "corroborate"
  ]
  node [
    id 263
    label "trzyma&#263;"
  ]
  node [
    id 264
    label "panowa&#263;"
  ]
  node [
    id 265
    label "defy"
  ]
  node [
    id 266
    label "cope"
  ]
  node [
    id 267
    label "broni&#263;"
  ]
  node [
    id 268
    label "sprawowa&#263;"
  ]
  node [
    id 269
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 270
    label "zachowywa&#263;"
  ]
  node [
    id 271
    label "ontologicznie"
  ]
  node [
    id 272
    label "potencja"
  ]
  node [
    id 273
    label "establishment"
  ]
  node [
    id 274
    label "fabrication"
  ]
  node [
    id 275
    label "koszt_ca&#322;kowity"
  ]
  node [
    id 276
    label "pojawianie_si&#281;"
  ]
  node [
    id 277
    label "tworzenie"
  ]
  node [
    id 278
    label "gospodarka"
  ]
  node [
    id 279
    label "ubycie"
  ]
  node [
    id 280
    label "disappearance"
  ]
  node [
    id 281
    label "usuni&#281;cie"
  ]
  node [
    id 282
    label "poznikanie"
  ]
  node [
    id 283
    label "evanescence"
  ]
  node [
    id 284
    label "wyj&#347;cie"
  ]
  node [
    id 285
    label "die"
  ]
  node [
    id 286
    label "przepadni&#281;cie"
  ]
  node [
    id 287
    label "ukradzenie"
  ]
  node [
    id 288
    label "niewidoczny"
  ]
  node [
    id 289
    label "stanie_si&#281;"
  ]
  node [
    id 290
    label "zgini&#281;cie"
  ]
  node [
    id 291
    label "stworzenie"
  ]
  node [
    id 292
    label "devising"
  ]
  node [
    id 293
    label "pojawienie_si&#281;"
  ]
  node [
    id 294
    label "shuffle"
  ]
  node [
    id 295
    label "spe&#322;nianie"
  ]
  node [
    id 296
    label "fulfillment"
  ]
  node [
    id 297
    label "powodowanie"
  ]
  node [
    id 298
    label "porobienie"
  ]
  node [
    id 299
    label "przedmiot"
  ]
  node [
    id 300
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 301
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 302
    label "creation"
  ]
  node [
    id 303
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 304
    label "act"
  ]
  node [
    id 305
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 306
    label "czynno&#347;&#263;"
  ]
  node [
    id 307
    label "tentegowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
]
