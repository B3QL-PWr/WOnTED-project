graph [
  node [
    id 0
    label "rap"
    origin "text"
  ]
  node [
    id 1
    label "czarnuszyrap"
    origin "text"
  ]
  node [
    id 2
    label "heheszki"
    origin "text"
  ]
  node [
    id 3
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 4
    label "muzyka_rozrywkowa"
  ]
  node [
    id 5
    label "karpiowate"
  ]
  node [
    id 6
    label "ryba"
  ]
  node [
    id 7
    label "czarna_muzyka"
  ]
  node [
    id 8
    label "drapie&#380;nik"
  ]
  node [
    id 9
    label "asp"
  ]
  node [
    id 10
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 11
    label "kr&#281;gowiec"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "systemik"
  ]
  node [
    id 14
    label "doniczkowiec"
  ]
  node [
    id 15
    label "mi&#281;so"
  ]
  node [
    id 16
    label "system"
  ]
  node [
    id 17
    label "patroszy&#263;"
  ]
  node [
    id 18
    label "rakowato&#347;&#263;"
  ]
  node [
    id 19
    label "w&#281;dkarstwo"
  ]
  node [
    id 20
    label "ryby"
  ]
  node [
    id 21
    label "fish"
  ]
  node [
    id 22
    label "linia_boczna"
  ]
  node [
    id 23
    label "tar&#322;o"
  ]
  node [
    id 24
    label "wyrostek_filtracyjny"
  ]
  node [
    id 25
    label "m&#281;tnooki"
  ]
  node [
    id 26
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 27
    label "pokrywa_skrzelowa"
  ]
  node [
    id 28
    label "ikra"
  ]
  node [
    id 29
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 30
    label "szczelina_skrzelowa"
  ]
  node [
    id 31
    label "mi&#281;so&#380;erca"
  ]
  node [
    id 32
    label "karpiokszta&#322;tne"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
