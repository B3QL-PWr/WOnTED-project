graph [
  node [
    id 0
    label "koniec"
    origin "text"
  ]
  node [
    id 1
    label "napis"
    origin "text"
  ]
  node [
    id 2
    label "ostatnie_podrygi"
  ]
  node [
    id 3
    label "visitation"
  ]
  node [
    id 4
    label "agonia"
  ]
  node [
    id 5
    label "defenestracja"
  ]
  node [
    id 6
    label "punkt"
  ]
  node [
    id 7
    label "dzia&#322;anie"
  ]
  node [
    id 8
    label "kres"
  ]
  node [
    id 9
    label "wydarzenie"
  ]
  node [
    id 10
    label "mogi&#322;a"
  ]
  node [
    id 11
    label "kres_&#380;ycia"
  ]
  node [
    id 12
    label "szereg"
  ]
  node [
    id 13
    label "szeol"
  ]
  node [
    id 14
    label "pogrzebanie"
  ]
  node [
    id 15
    label "miejsce"
  ]
  node [
    id 16
    label "chwila"
  ]
  node [
    id 17
    label "&#380;a&#322;oba"
  ]
  node [
    id 18
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 19
    label "zabicie"
  ]
  node [
    id 20
    label "przebiec"
  ]
  node [
    id 21
    label "charakter"
  ]
  node [
    id 22
    label "czynno&#347;&#263;"
  ]
  node [
    id 23
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 24
    label "motyw"
  ]
  node [
    id 25
    label "przebiegni&#281;cie"
  ]
  node [
    id 26
    label "fabu&#322;a"
  ]
  node [
    id 27
    label "Rzym_Zachodni"
  ]
  node [
    id 28
    label "whole"
  ]
  node [
    id 29
    label "ilo&#347;&#263;"
  ]
  node [
    id 30
    label "element"
  ]
  node [
    id 31
    label "Rzym_Wschodni"
  ]
  node [
    id 32
    label "urz&#261;dzenie"
  ]
  node [
    id 33
    label "warunek_lokalowy"
  ]
  node [
    id 34
    label "plac"
  ]
  node [
    id 35
    label "location"
  ]
  node [
    id 36
    label "uwaga"
  ]
  node [
    id 37
    label "przestrze&#324;"
  ]
  node [
    id 38
    label "status"
  ]
  node [
    id 39
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 40
    label "cia&#322;o"
  ]
  node [
    id 41
    label "cecha"
  ]
  node [
    id 42
    label "praca"
  ]
  node [
    id 43
    label "rz&#261;d"
  ]
  node [
    id 44
    label "time"
  ]
  node [
    id 45
    label "czas"
  ]
  node [
    id 46
    label "&#347;mier&#263;"
  ]
  node [
    id 47
    label "death"
  ]
  node [
    id 48
    label "upadek"
  ]
  node [
    id 49
    label "zmierzch"
  ]
  node [
    id 50
    label "stan"
  ]
  node [
    id 51
    label "nieuleczalnie_chory"
  ]
  node [
    id 52
    label "spocz&#261;&#263;"
  ]
  node [
    id 53
    label "spocz&#281;cie"
  ]
  node [
    id 54
    label "pochowanie"
  ]
  node [
    id 55
    label "spoczywa&#263;"
  ]
  node [
    id 56
    label "chowanie"
  ]
  node [
    id 57
    label "park_sztywnych"
  ]
  node [
    id 58
    label "pomnik"
  ]
  node [
    id 59
    label "nagrobek"
  ]
  node [
    id 60
    label "prochowisko"
  ]
  node [
    id 61
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 62
    label "spoczywanie"
  ]
  node [
    id 63
    label "za&#347;wiaty"
  ]
  node [
    id 64
    label "piek&#322;o"
  ]
  node [
    id 65
    label "judaizm"
  ]
  node [
    id 66
    label "destruction"
  ]
  node [
    id 67
    label "zabrzmienie"
  ]
  node [
    id 68
    label "skrzywdzenie"
  ]
  node [
    id 69
    label "pozabijanie"
  ]
  node [
    id 70
    label "zniszczenie"
  ]
  node [
    id 71
    label "zaszkodzenie"
  ]
  node [
    id 72
    label "usuni&#281;cie"
  ]
  node [
    id 73
    label "spowodowanie"
  ]
  node [
    id 74
    label "killing"
  ]
  node [
    id 75
    label "zdarzenie_si&#281;"
  ]
  node [
    id 76
    label "czyn"
  ]
  node [
    id 77
    label "umarcie"
  ]
  node [
    id 78
    label "granie"
  ]
  node [
    id 79
    label "zamkni&#281;cie"
  ]
  node [
    id 80
    label "compaction"
  ]
  node [
    id 81
    label "&#380;al"
  ]
  node [
    id 82
    label "paznokie&#263;"
  ]
  node [
    id 83
    label "symbol"
  ]
  node [
    id 84
    label "kir"
  ]
  node [
    id 85
    label "brud"
  ]
  node [
    id 86
    label "wyrzucenie"
  ]
  node [
    id 87
    label "defenestration"
  ]
  node [
    id 88
    label "zaj&#347;cie"
  ]
  node [
    id 89
    label "burying"
  ]
  node [
    id 90
    label "zasypanie"
  ]
  node [
    id 91
    label "zw&#322;oki"
  ]
  node [
    id 92
    label "burial"
  ]
  node [
    id 93
    label "w&#322;o&#380;enie"
  ]
  node [
    id 94
    label "porobienie"
  ]
  node [
    id 95
    label "gr&#243;b"
  ]
  node [
    id 96
    label "uniemo&#380;liwienie"
  ]
  node [
    id 97
    label "po&#322;o&#380;enie"
  ]
  node [
    id 98
    label "sprawa"
  ]
  node [
    id 99
    label "ust&#281;p"
  ]
  node [
    id 100
    label "plan"
  ]
  node [
    id 101
    label "obiekt_matematyczny"
  ]
  node [
    id 102
    label "problemat"
  ]
  node [
    id 103
    label "plamka"
  ]
  node [
    id 104
    label "stopie&#324;_pisma"
  ]
  node [
    id 105
    label "jednostka"
  ]
  node [
    id 106
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 107
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 108
    label "mark"
  ]
  node [
    id 109
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 110
    label "prosta"
  ]
  node [
    id 111
    label "problematyka"
  ]
  node [
    id 112
    label "obiekt"
  ]
  node [
    id 113
    label "zapunktowa&#263;"
  ]
  node [
    id 114
    label "podpunkt"
  ]
  node [
    id 115
    label "wojsko"
  ]
  node [
    id 116
    label "point"
  ]
  node [
    id 117
    label "pozycja"
  ]
  node [
    id 118
    label "szpaler"
  ]
  node [
    id 119
    label "zbi&#243;r"
  ]
  node [
    id 120
    label "column"
  ]
  node [
    id 121
    label "uporz&#261;dkowanie"
  ]
  node [
    id 122
    label "mn&#243;stwo"
  ]
  node [
    id 123
    label "unit"
  ]
  node [
    id 124
    label "rozmieszczenie"
  ]
  node [
    id 125
    label "tract"
  ]
  node [
    id 126
    label "wyra&#380;enie"
  ]
  node [
    id 127
    label "infimum"
  ]
  node [
    id 128
    label "powodowanie"
  ]
  node [
    id 129
    label "liczenie"
  ]
  node [
    id 130
    label "cz&#322;owiek"
  ]
  node [
    id 131
    label "skutek"
  ]
  node [
    id 132
    label "podzia&#322;anie"
  ]
  node [
    id 133
    label "supremum"
  ]
  node [
    id 134
    label "kampania"
  ]
  node [
    id 135
    label "uruchamianie"
  ]
  node [
    id 136
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 137
    label "operacja"
  ]
  node [
    id 138
    label "hipnotyzowanie"
  ]
  node [
    id 139
    label "robienie"
  ]
  node [
    id 140
    label "uruchomienie"
  ]
  node [
    id 141
    label "nakr&#281;canie"
  ]
  node [
    id 142
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 143
    label "matematyka"
  ]
  node [
    id 144
    label "reakcja_chemiczna"
  ]
  node [
    id 145
    label "tr&#243;jstronny"
  ]
  node [
    id 146
    label "natural_process"
  ]
  node [
    id 147
    label "nakr&#281;cenie"
  ]
  node [
    id 148
    label "zatrzymanie"
  ]
  node [
    id 149
    label "wp&#322;yw"
  ]
  node [
    id 150
    label "rzut"
  ]
  node [
    id 151
    label "podtrzymywanie"
  ]
  node [
    id 152
    label "w&#322;&#261;czanie"
  ]
  node [
    id 153
    label "liczy&#263;"
  ]
  node [
    id 154
    label "operation"
  ]
  node [
    id 155
    label "rezultat"
  ]
  node [
    id 156
    label "dzianie_si&#281;"
  ]
  node [
    id 157
    label "zadzia&#322;anie"
  ]
  node [
    id 158
    label "priorytet"
  ]
  node [
    id 159
    label "bycie"
  ]
  node [
    id 160
    label "rozpocz&#281;cie"
  ]
  node [
    id 161
    label "docieranie"
  ]
  node [
    id 162
    label "funkcja"
  ]
  node [
    id 163
    label "czynny"
  ]
  node [
    id 164
    label "impact"
  ]
  node [
    id 165
    label "oferta"
  ]
  node [
    id 166
    label "zako&#324;czenie"
  ]
  node [
    id 167
    label "act"
  ]
  node [
    id 168
    label "wdzieranie_si&#281;"
  ]
  node [
    id 169
    label "w&#322;&#261;czenie"
  ]
  node [
    id 170
    label "autografia"
  ]
  node [
    id 171
    label "tekst"
  ]
  node [
    id 172
    label "expressive_style"
  ]
  node [
    id 173
    label "informacja"
  ]
  node [
    id 174
    label "ekscerpcja"
  ]
  node [
    id 175
    label "j&#281;zykowo"
  ]
  node [
    id 176
    label "wypowied&#378;"
  ]
  node [
    id 177
    label "redakcja"
  ]
  node [
    id 178
    label "wytw&#243;r"
  ]
  node [
    id 179
    label "pomini&#281;cie"
  ]
  node [
    id 180
    label "dzie&#322;o"
  ]
  node [
    id 181
    label "preparacja"
  ]
  node [
    id 182
    label "odmianka"
  ]
  node [
    id 183
    label "opu&#347;ci&#263;"
  ]
  node [
    id 184
    label "koniektura"
  ]
  node [
    id 185
    label "pisa&#263;"
  ]
  node [
    id 186
    label "obelga"
  ]
  node [
    id 187
    label "publikacja"
  ]
  node [
    id 188
    label "wiedza"
  ]
  node [
    id 189
    label "obiega&#263;"
  ]
  node [
    id 190
    label "powzi&#281;cie"
  ]
  node [
    id 191
    label "dane"
  ]
  node [
    id 192
    label "obiegni&#281;cie"
  ]
  node [
    id 193
    label "sygna&#322;"
  ]
  node [
    id 194
    label "obieganie"
  ]
  node [
    id 195
    label "powzi&#261;&#263;"
  ]
  node [
    id 196
    label "obiec"
  ]
  node [
    id 197
    label "doj&#347;cie"
  ]
  node [
    id 198
    label "doj&#347;&#263;"
  ]
  node [
    id 199
    label "technika_litograficzna"
  ]
  node [
    id 200
    label "reprodukcja"
  ]
  node [
    id 201
    label "przenoszenie"
  ]
  node [
    id 202
    label "kamie&#324;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
]
