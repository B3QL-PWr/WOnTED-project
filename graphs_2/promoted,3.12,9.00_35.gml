graph [
  node [
    id 0
    label "strach"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "ba&#263;"
    origin "text"
  ]
  node [
    id 3
    label "spirit"
  ]
  node [
    id 4
    label "emocja"
  ]
  node [
    id 5
    label "zjawa"
  ]
  node [
    id 6
    label "straszyd&#322;o"
  ]
  node [
    id 7
    label "zastraszanie"
  ]
  node [
    id 8
    label "phobia"
  ]
  node [
    id 9
    label "zastraszenie"
  ]
  node [
    id 10
    label "akatyzja"
  ]
  node [
    id 11
    label "ba&#263;_si&#281;"
  ]
  node [
    id 12
    label "zesp&#243;&#322;_chorobowy"
  ]
  node [
    id 13
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 14
    label "ogrom"
  ]
  node [
    id 15
    label "iskrzy&#263;"
  ]
  node [
    id 16
    label "d&#322;awi&#263;"
  ]
  node [
    id 17
    label "ostygn&#261;&#263;"
  ]
  node [
    id 18
    label "stygn&#261;&#263;"
  ]
  node [
    id 19
    label "stan"
  ]
  node [
    id 20
    label "temperatura"
  ]
  node [
    id 21
    label "wpa&#347;&#263;"
  ]
  node [
    id 22
    label "afekt"
  ]
  node [
    id 23
    label "wpada&#263;"
  ]
  node [
    id 24
    label "istota_fantastyczna"
  ]
  node [
    id 25
    label "refleksja"
  ]
  node [
    id 26
    label "widziad&#322;o"
  ]
  node [
    id 27
    label "stw&#243;r"
  ]
  node [
    id 28
    label "szkarada"
  ]
  node [
    id 29
    label "l&#281;k"
  ]
  node [
    id 30
    label "bullying"
  ]
  node [
    id 31
    label "oddzia&#322;ywanie"
  ]
  node [
    id 32
    label "oddzia&#322;anie"
  ]
  node [
    id 33
    label "presja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
]
