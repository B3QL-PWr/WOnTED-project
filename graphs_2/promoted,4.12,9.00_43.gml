graph [
  node [
    id 0
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 1
    label "przera&#380;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "alarm"
  ]
  node [
    id 3
    label "wzbudza&#263;"
  ]
  node [
    id 4
    label "go"
  ]
  node [
    id 5
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 6
    label "system_alarmowy"
  ]
  node [
    id 7
    label "pop&#322;och"
  ]
  node [
    id 8
    label "sygna&#322;"
  ]
  node [
    id 9
    label "czas"
  ]
  node [
    id 10
    label "sygnalizator"
  ]
  node [
    id 11
    label "czujnik"
  ]
  node [
    id 12
    label "urz&#261;dzenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
]
