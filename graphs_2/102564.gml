graph [
  node [
    id 0
    label "wymaga&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dokument"
    origin "text"
  ]
  node [
    id 2
    label "ustalenie"
    origin "text"
  ]
  node [
    id 3
    label "prawa"
    origin "text"
  ]
  node [
    id 4
    label "zasi&#322;ek"
    origin "text"
  ]
  node [
    id 5
    label "rodzinny"
    origin "text"
  ]
  node [
    id 6
    label "oraz"
    origin "text"
  ]
  node [
    id 7
    label "wniosek"
    origin "text"
  ]
  node [
    id 8
    label "dodatek"
    origin "text"
  ]
  node [
    id 9
    label "o&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 10
    label "cz&#322;onek"
    origin "text"
  ]
  node [
    id 11
    label "rodzina"
    origin "text"
  ]
  node [
    id 12
    label "wysoko&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "doch&#243;d"
    origin "text"
  ]
  node [
    id 14
    label "podlega&#263;"
    origin "text"
  ]
  node [
    id 15
    label "opodatkowanie"
    origin "text"
  ]
  node [
    id 16
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 17
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 18
    label "uzyska&#263;"
    origin "text"
  ]
  node [
    id 19
    label "rok"
    origin "text"
  ]
  node [
    id 20
    label "kalendarzowy"
    origin "text"
  ]
  node [
    id 21
    label "poprzedza&#263;"
    origin "text"
  ]
  node [
    id 22
    label "okres"
    origin "text"
  ]
  node [
    id 23
    label "zasi&#322;kowy"
    origin "text"
  ]
  node [
    id 24
    label "niezb&#281;dny"
    origin "text"
  ]
  node [
    id 25
    label "dla"
    origin "text"
  ]
  node [
    id 26
    label "prawid&#322;owy"
    origin "text"
  ]
  node [
    id 27
    label "nadanie"
    origin "text"
  ]
  node [
    id 28
    label "uprawnienie"
    origin "text"
  ]
  node [
    id 29
    label "by&#263;"
  ]
  node [
    id 30
    label "claim"
  ]
  node [
    id 31
    label "&#380;&#261;da&#263;"
  ]
  node [
    id 32
    label "zmusza&#263;"
  ]
  node [
    id 33
    label "take"
  ]
  node [
    id 34
    label "force"
  ]
  node [
    id 35
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 36
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 37
    label "sandbag"
  ]
  node [
    id 38
    label "powodowa&#263;"
  ]
  node [
    id 39
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 40
    label "mie&#263;_miejsce"
  ]
  node [
    id 41
    label "equal"
  ]
  node [
    id 42
    label "trwa&#263;"
  ]
  node [
    id 43
    label "chodzi&#263;"
  ]
  node [
    id 44
    label "si&#281;ga&#263;"
  ]
  node [
    id 45
    label "stan"
  ]
  node [
    id 46
    label "obecno&#347;&#263;"
  ]
  node [
    id 47
    label "stand"
  ]
  node [
    id 48
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 49
    label "uczestniczy&#263;"
  ]
  node [
    id 50
    label "domaga&#263;_si&#281;"
  ]
  node [
    id 51
    label "woo"
  ]
  node [
    id 52
    label "chcie&#263;"
  ]
  node [
    id 53
    label "zapis"
  ]
  node [
    id 54
    label "&#347;wiadectwo"
  ]
  node [
    id 55
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 56
    label "wytw&#243;r"
  ]
  node [
    id 57
    label "parafa"
  ]
  node [
    id 58
    label "plik"
  ]
  node [
    id 59
    label "raport&#243;wka"
  ]
  node [
    id 60
    label "utw&#243;r"
  ]
  node [
    id 61
    label "record"
  ]
  node [
    id 62
    label "fascyku&#322;"
  ]
  node [
    id 63
    label "dokumentacja"
  ]
  node [
    id 64
    label "registratura"
  ]
  node [
    id 65
    label "artyku&#322;"
  ]
  node [
    id 66
    label "writing"
  ]
  node [
    id 67
    label "sygnatariusz"
  ]
  node [
    id 68
    label "dow&#243;d"
  ]
  node [
    id 69
    label "za&#347;wiadczenie"
  ]
  node [
    id 70
    label "certificate"
  ]
  node [
    id 71
    label "promocja"
  ]
  node [
    id 72
    label "spos&#243;b"
  ]
  node [
    id 73
    label "entrance"
  ]
  node [
    id 74
    label "czynno&#347;&#263;"
  ]
  node [
    id 75
    label "wpis"
  ]
  node [
    id 76
    label "normalizacja"
  ]
  node [
    id 77
    label "obrazowanie"
  ]
  node [
    id 78
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 79
    label "organ"
  ]
  node [
    id 80
    label "tre&#347;&#263;"
  ]
  node [
    id 81
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 82
    label "part"
  ]
  node [
    id 83
    label "element_anatomiczny"
  ]
  node [
    id 84
    label "tekst"
  ]
  node [
    id 85
    label "komunikat"
  ]
  node [
    id 86
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 87
    label "przedmiot"
  ]
  node [
    id 88
    label "p&#322;&#243;d"
  ]
  node [
    id 89
    label "work"
  ]
  node [
    id 90
    label "rezultat"
  ]
  node [
    id 91
    label "podkatalog"
  ]
  node [
    id 92
    label "nadpisa&#263;"
  ]
  node [
    id 93
    label "nadpisanie"
  ]
  node [
    id 94
    label "bundle"
  ]
  node [
    id 95
    label "folder"
  ]
  node [
    id 96
    label "nadpisywanie"
  ]
  node [
    id 97
    label "paczka"
  ]
  node [
    id 98
    label "nadpisywa&#263;"
  ]
  node [
    id 99
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 100
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 101
    label "przedstawiciel"
  ]
  node [
    id 102
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 103
    label "wydanie"
  ]
  node [
    id 104
    label "zbi&#243;r"
  ]
  node [
    id 105
    label "torba"
  ]
  node [
    id 106
    label "ekscerpcja"
  ]
  node [
    id 107
    label "materia&#322;"
  ]
  node [
    id 108
    label "operat"
  ]
  node [
    id 109
    label "kosztorys"
  ]
  node [
    id 110
    label "biuro"
  ]
  node [
    id 111
    label "register"
  ]
  node [
    id 112
    label "blok"
  ]
  node [
    id 113
    label "prawda"
  ]
  node [
    id 114
    label "znak_j&#281;zykowy"
  ]
  node [
    id 115
    label "nag&#322;&#243;wek"
  ]
  node [
    id 116
    label "szkic"
  ]
  node [
    id 117
    label "line"
  ]
  node [
    id 118
    label "fragment"
  ]
  node [
    id 119
    label "wyr&#243;b"
  ]
  node [
    id 120
    label "rodzajnik"
  ]
  node [
    id 121
    label "towar"
  ]
  node [
    id 122
    label "paragraf"
  ]
  node [
    id 123
    label "paraph"
  ]
  node [
    id 124
    label "podpis"
  ]
  node [
    id 125
    label "decyzja"
  ]
  node [
    id 126
    label "umocnienie"
  ]
  node [
    id 127
    label "appointment"
  ]
  node [
    id 128
    label "spowodowanie"
  ]
  node [
    id 129
    label "localization"
  ]
  node [
    id 130
    label "informacja"
  ]
  node [
    id 131
    label "zdecydowanie"
  ]
  node [
    id 132
    label "zrobienie"
  ]
  node [
    id 133
    label "punkt"
  ]
  node [
    id 134
    label "publikacja"
  ]
  node [
    id 135
    label "wiedza"
  ]
  node [
    id 136
    label "doj&#347;cie"
  ]
  node [
    id 137
    label "obiega&#263;"
  ]
  node [
    id 138
    label "powzi&#281;cie"
  ]
  node [
    id 139
    label "dane"
  ]
  node [
    id 140
    label "obiegni&#281;cie"
  ]
  node [
    id 141
    label "sygna&#322;"
  ]
  node [
    id 142
    label "obieganie"
  ]
  node [
    id 143
    label "powzi&#261;&#263;"
  ]
  node [
    id 144
    label "obiec"
  ]
  node [
    id 145
    label "doj&#347;&#263;"
  ]
  node [
    id 146
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 147
    label "pewnie"
  ]
  node [
    id 148
    label "zdecydowany"
  ]
  node [
    id 149
    label "zauwa&#380;alnie"
  ]
  node [
    id 150
    label "oddzia&#322;anie"
  ]
  node [
    id 151
    label "podj&#281;cie"
  ]
  node [
    id 152
    label "cecha"
  ]
  node [
    id 153
    label "resoluteness"
  ]
  node [
    id 154
    label "judgment"
  ]
  node [
    id 155
    label "campaign"
  ]
  node [
    id 156
    label "causing"
  ]
  node [
    id 157
    label "activity"
  ]
  node [
    id 158
    label "bezproblemowy"
  ]
  node [
    id 159
    label "wydarzenie"
  ]
  node [
    id 160
    label "narobienie"
  ]
  node [
    id 161
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 162
    label "creation"
  ]
  node [
    id 163
    label "porobienie"
  ]
  node [
    id 164
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 165
    label "management"
  ]
  node [
    id 166
    label "resolution"
  ]
  node [
    id 167
    label "szaniec"
  ]
  node [
    id 168
    label "kurtyna"
  ]
  node [
    id 169
    label "barykada"
  ]
  node [
    id 170
    label "umacnia&#263;"
  ]
  node [
    id 171
    label "zabezpieczenie"
  ]
  node [
    id 172
    label "transzeja"
  ]
  node [
    id 173
    label "umacnianie"
  ]
  node [
    id 174
    label "trwa&#322;y"
  ]
  node [
    id 175
    label "fosa"
  ]
  node [
    id 176
    label "kazamata"
  ]
  node [
    id 177
    label "palisada"
  ]
  node [
    id 178
    label "exploitation"
  ]
  node [
    id 179
    label "ochrona"
  ]
  node [
    id 180
    label "&#347;r&#243;dszaniec"
  ]
  node [
    id 181
    label "fort"
  ]
  node [
    id 182
    label "confirmation"
  ]
  node [
    id 183
    label "przedbramie"
  ]
  node [
    id 184
    label "zamek"
  ]
  node [
    id 185
    label "okop"
  ]
  node [
    id 186
    label "machiku&#322;"
  ]
  node [
    id 187
    label "bastion"
  ]
  node [
    id 188
    label "umocni&#263;"
  ]
  node [
    id 189
    label "baszta"
  ]
  node [
    id 190
    label "utrwalenie"
  ]
  node [
    id 191
    label "zapomoga"
  ]
  node [
    id 192
    label "darowizna"
  ]
  node [
    id 193
    label "rodzinnie"
  ]
  node [
    id 194
    label "zwi&#261;zany"
  ]
  node [
    id 195
    label "przyjemny"
  ]
  node [
    id 196
    label "towarzyski"
  ]
  node [
    id 197
    label "wsp&#243;lny"
  ]
  node [
    id 198
    label "ciep&#322;y"
  ]
  node [
    id 199
    label "familijnie"
  ]
  node [
    id 200
    label "charakterystyczny"
  ]
  node [
    id 201
    label "swobodny"
  ]
  node [
    id 202
    label "przyjemnie"
  ]
  node [
    id 203
    label "prywatnie"
  ]
  node [
    id 204
    label "swobodnie"
  ]
  node [
    id 205
    label "familijny"
  ]
  node [
    id 206
    label "pleasantly"
  ]
  node [
    id 207
    label "razem"
  ]
  node [
    id 208
    label "mi&#322;y"
  ]
  node [
    id 209
    label "ocieplanie_si&#281;"
  ]
  node [
    id 210
    label "ocieplanie"
  ]
  node [
    id 211
    label "grzanie"
  ]
  node [
    id 212
    label "ocieplenie_si&#281;"
  ]
  node [
    id 213
    label "zagrzanie"
  ]
  node [
    id 214
    label "ocieplenie"
  ]
  node [
    id 215
    label "korzystny"
  ]
  node [
    id 216
    label "ciep&#322;o"
  ]
  node [
    id 217
    label "dobry"
  ]
  node [
    id 218
    label "towarzysko"
  ]
  node [
    id 219
    label "nieformalny"
  ]
  node [
    id 220
    label "otwarty"
  ]
  node [
    id 221
    label "zro&#347;ni&#281;cie_si&#281;"
  ]
  node [
    id 222
    label "po&#322;&#261;czenie"
  ]
  node [
    id 223
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 224
    label "charakterystycznie"
  ]
  node [
    id 225
    label "szczeg&#243;lny"
  ]
  node [
    id 226
    label "wyj&#261;tkowy"
  ]
  node [
    id 227
    label "typowy"
  ]
  node [
    id 228
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 229
    label "podobny"
  ]
  node [
    id 230
    label "spolny"
  ]
  node [
    id 231
    label "wsp&#243;lnie"
  ]
  node [
    id 232
    label "sp&#243;lny"
  ]
  node [
    id 233
    label "jeden"
  ]
  node [
    id 234
    label "uwsp&#243;lnienie"
  ]
  node [
    id 235
    label "uwsp&#243;lnianie"
  ]
  node [
    id 236
    label "naturalny"
  ]
  node [
    id 237
    label "bezpruderyjny"
  ]
  node [
    id 238
    label "dowolny"
  ]
  node [
    id 239
    label "wygodny"
  ]
  node [
    id 240
    label "niezale&#380;ny"
  ]
  node [
    id 241
    label "wolnie"
  ]
  node [
    id 242
    label "pismo"
  ]
  node [
    id 243
    label "prayer"
  ]
  node [
    id 244
    label "twierdzenie"
  ]
  node [
    id 245
    label "propozycja"
  ]
  node [
    id 246
    label "my&#347;l"
  ]
  node [
    id 247
    label "motion"
  ]
  node [
    id 248
    label "wnioskowanie"
  ]
  node [
    id 249
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 250
    label "alternatywa_Fredholma"
  ]
  node [
    id 251
    label "oznajmianie"
  ]
  node [
    id 252
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 253
    label "teoria"
  ]
  node [
    id 254
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 255
    label "paradoks_Leontiefa"
  ]
  node [
    id 256
    label "s&#261;d"
  ]
  node [
    id 257
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 258
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 259
    label "teza"
  ]
  node [
    id 260
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 261
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 262
    label "twierdzenie_Pettisa"
  ]
  node [
    id 263
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 264
    label "twierdzenie_Maya"
  ]
  node [
    id 265
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 266
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 267
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 268
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 269
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 270
    label "zapewnianie"
  ]
  node [
    id 271
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 272
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 273
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 274
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 275
    label "twierdzenie_Stokesa"
  ]
  node [
    id 276
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 277
    label "twierdzenie_Cevy"
  ]
  node [
    id 278
    label "twierdzenie_Pascala"
  ]
  node [
    id 279
    label "proposition"
  ]
  node [
    id 280
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 281
    label "komunikowanie"
  ]
  node [
    id 282
    label "zasada"
  ]
  node [
    id 283
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 284
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 285
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 286
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 287
    label "szko&#322;a"
  ]
  node [
    id 288
    label "thinking"
  ]
  node [
    id 289
    label "umys&#322;"
  ]
  node [
    id 290
    label "political_orientation"
  ]
  node [
    id 291
    label "istota"
  ]
  node [
    id 292
    label "pomys&#322;"
  ]
  node [
    id 293
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 294
    label "idea"
  ]
  node [
    id 295
    label "system"
  ]
  node [
    id 296
    label "fantomatyka"
  ]
  node [
    id 297
    label "psychotest"
  ]
  node [
    id 298
    label "wk&#322;ad"
  ]
  node [
    id 299
    label "handwriting"
  ]
  node [
    id 300
    label "przekaz"
  ]
  node [
    id 301
    label "dzie&#322;o"
  ]
  node [
    id 302
    label "paleograf"
  ]
  node [
    id 303
    label "interpunkcja"
  ]
  node [
    id 304
    label "dzia&#322;"
  ]
  node [
    id 305
    label "grafia"
  ]
  node [
    id 306
    label "egzemplarz"
  ]
  node [
    id 307
    label "communication"
  ]
  node [
    id 308
    label "script"
  ]
  node [
    id 309
    label "zajawka"
  ]
  node [
    id 310
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 311
    label "list"
  ]
  node [
    id 312
    label "adres"
  ]
  node [
    id 313
    label "Zwrotnica"
  ]
  node [
    id 314
    label "czasopismo"
  ]
  node [
    id 315
    label "ok&#322;adka"
  ]
  node [
    id 316
    label "ortografia"
  ]
  node [
    id 317
    label "letter"
  ]
  node [
    id 318
    label "komunikacja"
  ]
  node [
    id 319
    label "paleografia"
  ]
  node [
    id 320
    label "j&#281;zyk"
  ]
  node [
    id 321
    label "prasa"
  ]
  node [
    id 322
    label "proposal"
  ]
  node [
    id 323
    label "proszenie"
  ]
  node [
    id 324
    label "dochodzenie"
  ]
  node [
    id 325
    label "proces_my&#347;lowy"
  ]
  node [
    id 326
    label "lead"
  ]
  node [
    id 327
    label "konkluzja"
  ]
  node [
    id 328
    label "sk&#322;adanie"
  ]
  node [
    id 329
    label "przes&#322;anka"
  ]
  node [
    id 330
    label "dziennik"
  ]
  node [
    id 331
    label "element"
  ]
  node [
    id 332
    label "rzecz"
  ]
  node [
    id 333
    label "galanteria"
  ]
  node [
    id 334
    label "aneks"
  ]
  node [
    id 335
    label "zboczenie"
  ]
  node [
    id 336
    label "om&#243;wienie"
  ]
  node [
    id 337
    label "sponiewieranie"
  ]
  node [
    id 338
    label "discipline"
  ]
  node [
    id 339
    label "omawia&#263;"
  ]
  node [
    id 340
    label "kr&#261;&#380;enie"
  ]
  node [
    id 341
    label "robienie"
  ]
  node [
    id 342
    label "sponiewiera&#263;"
  ]
  node [
    id 343
    label "entity"
  ]
  node [
    id 344
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 345
    label "tematyka"
  ]
  node [
    id 346
    label "w&#261;tek"
  ]
  node [
    id 347
    label "charakter"
  ]
  node [
    id 348
    label "zbaczanie"
  ]
  node [
    id 349
    label "program_nauczania"
  ]
  node [
    id 350
    label "om&#243;wi&#263;"
  ]
  node [
    id 351
    label "omawianie"
  ]
  node [
    id 352
    label "thing"
  ]
  node [
    id 353
    label "kultura"
  ]
  node [
    id 354
    label "zbacza&#263;"
  ]
  node [
    id 355
    label "zboczy&#263;"
  ]
  node [
    id 356
    label "r&#243;&#380;niczka"
  ]
  node [
    id 357
    label "&#347;rodowisko"
  ]
  node [
    id 358
    label "materia"
  ]
  node [
    id 359
    label "szambo"
  ]
  node [
    id 360
    label "aspo&#322;eczny"
  ]
  node [
    id 361
    label "component"
  ]
  node [
    id 362
    label "szkodnik"
  ]
  node [
    id 363
    label "gangsterski"
  ]
  node [
    id 364
    label "poj&#281;cie"
  ]
  node [
    id 365
    label "underworld"
  ]
  node [
    id 366
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 367
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 368
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 369
    label "income"
  ]
  node [
    id 370
    label "stopa_procentowa"
  ]
  node [
    id 371
    label "krzywa_Engla"
  ]
  node [
    id 372
    label "korzy&#347;&#263;"
  ]
  node [
    id 373
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 374
    label "wp&#322;yw"
  ]
  node [
    id 375
    label "pomieszczenie"
  ]
  node [
    id 376
    label "budynek"
  ]
  node [
    id 377
    label "object"
  ]
  node [
    id 378
    label "temat"
  ]
  node [
    id 379
    label "wpadni&#281;cie"
  ]
  node [
    id 380
    label "mienie"
  ]
  node [
    id 381
    label "przyroda"
  ]
  node [
    id 382
    label "obiekt"
  ]
  node [
    id 383
    label "wpa&#347;&#263;"
  ]
  node [
    id 384
    label "wpadanie"
  ]
  node [
    id 385
    label "wpada&#263;"
  ]
  node [
    id 386
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 387
    label "favor"
  ]
  node [
    id 388
    label "konfekcja"
  ]
  node [
    id 389
    label "haberdashery"
  ]
  node [
    id 390
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 391
    label "program_informacyjny"
  ]
  node [
    id 392
    label "journal"
  ]
  node [
    id 393
    label "diariusz"
  ]
  node [
    id 394
    label "spis"
  ]
  node [
    id 395
    label "ksi&#281;ga"
  ]
  node [
    id 396
    label "sheet"
  ]
  node [
    id 397
    label "pami&#281;tnik"
  ]
  node [
    id 398
    label "gazeta"
  ]
  node [
    id 399
    label "przy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 400
    label "inquest"
  ]
  node [
    id 401
    label "powodowanie"
  ]
  node [
    id 402
    label "maturation"
  ]
  node [
    id 403
    label "dop&#322;ywanie"
  ]
  node [
    id 404
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 405
    label "inquisition"
  ]
  node [
    id 406
    label "dor&#281;czanie"
  ]
  node [
    id 407
    label "stawanie_si&#281;"
  ]
  node [
    id 408
    label "trial"
  ]
  node [
    id 409
    label "dosi&#281;ganie"
  ]
  node [
    id 410
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 411
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 412
    label "przesy&#322;ka"
  ]
  node [
    id 413
    label "rozpowszechnianie"
  ]
  node [
    id 414
    label "postrzeganie"
  ]
  node [
    id 415
    label "assay"
  ]
  node [
    id 416
    label "doje&#380;d&#380;anie"
  ]
  node [
    id 417
    label "dochrapywanie_si&#281;"
  ]
  node [
    id 418
    label "ro&#347;ni&#281;cie"
  ]
  node [
    id 419
    label "Inquisition"
  ]
  node [
    id 420
    label "roszczenie"
  ]
  node [
    id 421
    label "dolatywanie"
  ]
  node [
    id 422
    label "zaznawanie"
  ]
  node [
    id 423
    label "strzelenie"
  ]
  node [
    id 424
    label "orgazm"
  ]
  node [
    id 425
    label "detektyw"
  ]
  node [
    id 426
    label "&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 427
    label "doczekanie"
  ]
  node [
    id 428
    label "rozwijanie_si&#281;"
  ]
  node [
    id 429
    label "uzyskiwanie"
  ]
  node [
    id 430
    label "docieranie"
  ]
  node [
    id 431
    label "dojrza&#322;y"
  ]
  node [
    id 432
    label "osi&#261;ganie"
  ]
  node [
    id 433
    label "dop&#322;ata"
  ]
  node [
    id 434
    label "examination"
  ]
  node [
    id 435
    label "sta&#263;_si&#281;"
  ]
  node [
    id 436
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 437
    label "supervene"
  ]
  node [
    id 438
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 439
    label "zaj&#347;&#263;"
  ]
  node [
    id 440
    label "catch"
  ]
  node [
    id 441
    label "get"
  ]
  node [
    id 442
    label "bodziec"
  ]
  node [
    id 443
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 444
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 445
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 446
    label "heed"
  ]
  node [
    id 447
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 448
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 449
    label "spowodowa&#263;"
  ]
  node [
    id 450
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 451
    label "dozna&#263;"
  ]
  node [
    id 452
    label "dokoptowa&#263;"
  ]
  node [
    id 453
    label "postrzega&#263;"
  ]
  node [
    id 454
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 455
    label "dolecie&#263;"
  ]
  node [
    id 456
    label "drive"
  ]
  node [
    id 457
    label "dotrze&#263;"
  ]
  node [
    id 458
    label "become"
  ]
  node [
    id 459
    label "uzyskanie"
  ]
  node [
    id 460
    label "skill"
  ]
  node [
    id 461
    label "dochrapanie_si&#281;"
  ]
  node [
    id 462
    label "znajomo&#347;ci"
  ]
  node [
    id 463
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 464
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 465
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 466
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 467
    label "powi&#261;zanie"
  ]
  node [
    id 468
    label "affiliation"
  ]
  node [
    id 469
    label "dor&#281;czenie"
  ]
  node [
    id 470
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 471
    label "dost&#281;p"
  ]
  node [
    id 472
    label "gotowy"
  ]
  node [
    id 473
    label "avenue"
  ]
  node [
    id 474
    label "doznanie"
  ]
  node [
    id 475
    label "dojechanie"
  ]
  node [
    id 476
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 477
    label "ingress"
  ]
  node [
    id 478
    label "orzekni&#281;cie"
  ]
  node [
    id 479
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 480
    label "dolecenie"
  ]
  node [
    id 481
    label "rozpowszechnienie"
  ]
  node [
    id 482
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 483
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 484
    label "stanie_si&#281;"
  ]
  node [
    id 485
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 486
    label "wypowied&#378;"
  ]
  node [
    id 487
    label "zwiastowanie"
  ]
  node [
    id 488
    label "statement"
  ]
  node [
    id 489
    label "announcement"
  ]
  node [
    id 490
    label "poinformowanie"
  ]
  node [
    id 491
    label "pos&#322;uchanie"
  ]
  node [
    id 492
    label "sparafrazowanie"
  ]
  node [
    id 493
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 494
    label "strawestowa&#263;"
  ]
  node [
    id 495
    label "sparafrazowa&#263;"
  ]
  node [
    id 496
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 497
    label "trawestowa&#263;"
  ]
  node [
    id 498
    label "sformu&#322;owanie"
  ]
  node [
    id 499
    label "parafrazowanie"
  ]
  node [
    id 500
    label "ozdobnik"
  ]
  node [
    id 501
    label "delimitacja"
  ]
  node [
    id 502
    label "parafrazowa&#263;"
  ]
  node [
    id 503
    label "stylizacja"
  ]
  node [
    id 504
    label "trawestowanie"
  ]
  node [
    id 505
    label "strawestowanie"
  ]
  node [
    id 506
    label "kreacjonista"
  ]
  node [
    id 507
    label "roi&#263;_si&#281;"
  ]
  node [
    id 508
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 509
    label "telling"
  ]
  node [
    id 510
    label "oznajmienie"
  ]
  node [
    id 511
    label "&#347;wiadczenie"
  ]
  node [
    id 512
    label "podmiot"
  ]
  node [
    id 513
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 514
    label "cz&#322;owiek"
  ]
  node [
    id 515
    label "ptaszek"
  ]
  node [
    id 516
    label "organizacja"
  ]
  node [
    id 517
    label "cia&#322;o"
  ]
  node [
    id 518
    label "przyrodzenie"
  ]
  node [
    id 519
    label "fiut"
  ]
  node [
    id 520
    label "shaft"
  ]
  node [
    id 521
    label "wchodzenie"
  ]
  node [
    id 522
    label "grupa"
  ]
  node [
    id 523
    label "wej&#347;cie"
  ]
  node [
    id 524
    label "tkanka"
  ]
  node [
    id 525
    label "jednostka_organizacyjna"
  ]
  node [
    id 526
    label "budowa"
  ]
  node [
    id 527
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 528
    label "tw&#243;r"
  ]
  node [
    id 529
    label "organogeneza"
  ]
  node [
    id 530
    label "zesp&#243;&#322;"
  ]
  node [
    id 531
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 532
    label "struktura_anatomiczna"
  ]
  node [
    id 533
    label "uk&#322;ad"
  ]
  node [
    id 534
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 535
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 536
    label "Izba_Konsyliarska"
  ]
  node [
    id 537
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 538
    label "stomia"
  ]
  node [
    id 539
    label "dekortykacja"
  ]
  node [
    id 540
    label "okolica"
  ]
  node [
    id 541
    label "Komitet_Region&#243;w"
  ]
  node [
    id 542
    label "ludzko&#347;&#263;"
  ]
  node [
    id 543
    label "asymilowanie"
  ]
  node [
    id 544
    label "wapniak"
  ]
  node [
    id 545
    label "asymilowa&#263;"
  ]
  node [
    id 546
    label "os&#322;abia&#263;"
  ]
  node [
    id 547
    label "posta&#263;"
  ]
  node [
    id 548
    label "hominid"
  ]
  node [
    id 549
    label "podw&#322;adny"
  ]
  node [
    id 550
    label "os&#322;abianie"
  ]
  node [
    id 551
    label "g&#322;owa"
  ]
  node [
    id 552
    label "figura"
  ]
  node [
    id 553
    label "portrecista"
  ]
  node [
    id 554
    label "dwun&#243;g"
  ]
  node [
    id 555
    label "profanum"
  ]
  node [
    id 556
    label "mikrokosmos"
  ]
  node [
    id 557
    label "nasada"
  ]
  node [
    id 558
    label "duch"
  ]
  node [
    id 559
    label "antropochoria"
  ]
  node [
    id 560
    label "osoba"
  ]
  node [
    id 561
    label "wz&#243;r"
  ]
  node [
    id 562
    label "senior"
  ]
  node [
    id 563
    label "oddzia&#322;ywanie"
  ]
  node [
    id 564
    label "Adam"
  ]
  node [
    id 565
    label "homo_sapiens"
  ]
  node [
    id 566
    label "polifag"
  ]
  node [
    id 567
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 568
    label "przyk&#322;ad"
  ]
  node [
    id 569
    label "substytuowa&#263;"
  ]
  node [
    id 570
    label "substytuowanie"
  ]
  node [
    id 571
    label "zast&#281;pca"
  ]
  node [
    id 572
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 573
    label "byt"
  ]
  node [
    id 574
    label "osobowo&#347;&#263;"
  ]
  node [
    id 575
    label "prawo"
  ]
  node [
    id 576
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 577
    label "nauka_prawa"
  ]
  node [
    id 578
    label "penis"
  ]
  node [
    id 579
    label "agent"
  ]
  node [
    id 580
    label "tick"
  ]
  node [
    id 581
    label "znaczek"
  ]
  node [
    id 582
    label "nicpo&#324;"
  ]
  node [
    id 583
    label "ciul"
  ]
  node [
    id 584
    label "wyzwisko"
  ]
  node [
    id 585
    label "skurwysyn"
  ]
  node [
    id 586
    label "dupek"
  ]
  node [
    id 587
    label "genitalia"
  ]
  node [
    id 588
    label "moszna"
  ]
  node [
    id 589
    label "ekshumowanie"
  ]
  node [
    id 590
    label "p&#322;aszczyzna"
  ]
  node [
    id 591
    label "odwadnia&#263;"
  ]
  node [
    id 592
    label "zabalsamowanie"
  ]
  node [
    id 593
    label "odwodni&#263;"
  ]
  node [
    id 594
    label "sk&#243;ra"
  ]
  node [
    id 595
    label "staw"
  ]
  node [
    id 596
    label "ow&#322;osienie"
  ]
  node [
    id 597
    label "mi&#281;so"
  ]
  node [
    id 598
    label "zabalsamowa&#263;"
  ]
  node [
    id 599
    label "unerwienie"
  ]
  node [
    id 600
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 601
    label "kremacja"
  ]
  node [
    id 602
    label "miejsce"
  ]
  node [
    id 603
    label "biorytm"
  ]
  node [
    id 604
    label "sekcja"
  ]
  node [
    id 605
    label "istota_&#380;ywa"
  ]
  node [
    id 606
    label "otworzy&#263;"
  ]
  node [
    id 607
    label "otwiera&#263;"
  ]
  node [
    id 608
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 609
    label "otworzenie"
  ]
  node [
    id 610
    label "pochowanie"
  ]
  node [
    id 611
    label "otwieranie"
  ]
  node [
    id 612
    label "szkielet"
  ]
  node [
    id 613
    label "ty&#322;"
  ]
  node [
    id 614
    label "tanatoplastyk"
  ]
  node [
    id 615
    label "odwadnianie"
  ]
  node [
    id 616
    label "odwodnienie"
  ]
  node [
    id 617
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 618
    label "pochowa&#263;"
  ]
  node [
    id 619
    label "tanatoplastyka"
  ]
  node [
    id 620
    label "balsamowa&#263;"
  ]
  node [
    id 621
    label "nieumar&#322;y"
  ]
  node [
    id 622
    label "temperatura"
  ]
  node [
    id 623
    label "balsamowanie"
  ]
  node [
    id 624
    label "ekshumowa&#263;"
  ]
  node [
    id 625
    label "l&#281;d&#378;wie"
  ]
  node [
    id 626
    label "prz&#243;d"
  ]
  node [
    id 627
    label "pogrzeb"
  ]
  node [
    id 628
    label "odm&#322;adzanie"
  ]
  node [
    id 629
    label "liga"
  ]
  node [
    id 630
    label "jednostka_systematyczna"
  ]
  node [
    id 631
    label "gromada"
  ]
  node [
    id 632
    label "Entuzjastki"
  ]
  node [
    id 633
    label "kompozycja"
  ]
  node [
    id 634
    label "Terranie"
  ]
  node [
    id 635
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 636
    label "category"
  ]
  node [
    id 637
    label "pakiet_klimatyczny"
  ]
  node [
    id 638
    label "oddzia&#322;"
  ]
  node [
    id 639
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 640
    label "cz&#261;steczka"
  ]
  node [
    id 641
    label "stage_set"
  ]
  node [
    id 642
    label "type"
  ]
  node [
    id 643
    label "specgrupa"
  ]
  node [
    id 644
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 645
    label "&#346;wietliki"
  ]
  node [
    id 646
    label "odm&#322;odzenie"
  ]
  node [
    id 647
    label "Eurogrupa"
  ]
  node [
    id 648
    label "odm&#322;adza&#263;"
  ]
  node [
    id 649
    label "formacja_geologiczna"
  ]
  node [
    id 650
    label "harcerze_starsi"
  ]
  node [
    id 651
    label "struktura"
  ]
  node [
    id 652
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 653
    label "TOPR"
  ]
  node [
    id 654
    label "endecki"
  ]
  node [
    id 655
    label "przedstawicielstwo"
  ]
  node [
    id 656
    label "od&#322;am"
  ]
  node [
    id 657
    label "Cepelia"
  ]
  node [
    id 658
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 659
    label "ZBoWiD"
  ]
  node [
    id 660
    label "organization"
  ]
  node [
    id 661
    label "centrala"
  ]
  node [
    id 662
    label "GOPR"
  ]
  node [
    id 663
    label "ZOMO"
  ]
  node [
    id 664
    label "ZMP"
  ]
  node [
    id 665
    label "komitet_koordynacyjny"
  ]
  node [
    id 666
    label "przybud&#243;wka"
  ]
  node [
    id 667
    label "boj&#243;wka"
  ]
  node [
    id 668
    label "wnikni&#281;cie"
  ]
  node [
    id 669
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 670
    label "spotkanie"
  ]
  node [
    id 671
    label "poznanie"
  ]
  node [
    id 672
    label "pojawienie_si&#281;"
  ]
  node [
    id 673
    label "zdarzenie_si&#281;"
  ]
  node [
    id 674
    label "przenikni&#281;cie"
  ]
  node [
    id 675
    label "wpuszczenie"
  ]
  node [
    id 676
    label "zaatakowanie"
  ]
  node [
    id 677
    label "trespass"
  ]
  node [
    id 678
    label "przekroczenie"
  ]
  node [
    id 679
    label "otw&#243;r"
  ]
  node [
    id 680
    label "wzi&#281;cie"
  ]
  node [
    id 681
    label "vent"
  ]
  node [
    id 682
    label "stimulation"
  ]
  node [
    id 683
    label "dostanie_si&#281;"
  ]
  node [
    id 684
    label "pocz&#261;tek"
  ]
  node [
    id 685
    label "approach"
  ]
  node [
    id 686
    label "release"
  ]
  node [
    id 687
    label "wnij&#347;cie"
  ]
  node [
    id 688
    label "bramka"
  ]
  node [
    id 689
    label "wzniesienie_si&#281;"
  ]
  node [
    id 690
    label "podw&#243;rze"
  ]
  node [
    id 691
    label "dom"
  ]
  node [
    id 692
    label "wch&#243;d"
  ]
  node [
    id 693
    label "nast&#261;pienie"
  ]
  node [
    id 694
    label "zacz&#281;cie"
  ]
  node [
    id 695
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 696
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 697
    label "urz&#261;dzenie"
  ]
  node [
    id 698
    label "atakowanie"
  ]
  node [
    id 699
    label "w&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 700
    label "wpuszczanie"
  ]
  node [
    id 701
    label "zaliczanie_si&#281;"
  ]
  node [
    id 702
    label "pchanie_si&#281;"
  ]
  node [
    id 703
    label "poznawanie"
  ]
  node [
    id 704
    label "dostawanie_si&#281;"
  ]
  node [
    id 705
    label "&#322;a&#380;enie"
  ]
  node [
    id 706
    label "wnikanie"
  ]
  node [
    id 707
    label "zaczynanie"
  ]
  node [
    id 708
    label "zag&#322;&#281;bianie_si&#281;"
  ]
  node [
    id 709
    label "spotykanie"
  ]
  node [
    id 710
    label "nadeptanie"
  ]
  node [
    id 711
    label "pojawianie_si&#281;"
  ]
  node [
    id 712
    label "wznoszenie_si&#281;"
  ]
  node [
    id 713
    label "przenikanie"
  ]
  node [
    id 714
    label "climb"
  ]
  node [
    id 715
    label "nast&#281;powanie"
  ]
  node [
    id 716
    label "przekraczanie"
  ]
  node [
    id 717
    label "powinowaci"
  ]
  node [
    id 718
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 719
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 720
    label "rodze&#324;stwo"
  ]
  node [
    id 721
    label "krewni"
  ]
  node [
    id 722
    label "Ossoli&#324;scy"
  ]
  node [
    id 723
    label "potomstwo"
  ]
  node [
    id 724
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 725
    label "theater"
  ]
  node [
    id 726
    label "Soplicowie"
  ]
  node [
    id 727
    label "kin"
  ]
  node [
    id 728
    label "family"
  ]
  node [
    id 729
    label "rodzice"
  ]
  node [
    id 730
    label "ordynacja"
  ]
  node [
    id 731
    label "dom_rodzinny"
  ]
  node [
    id 732
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 733
    label "Ostrogscy"
  ]
  node [
    id 734
    label "bliscy"
  ]
  node [
    id 735
    label "przyjaciel_domu"
  ]
  node [
    id 736
    label "rz&#261;d"
  ]
  node [
    id 737
    label "Firlejowie"
  ]
  node [
    id 738
    label "Kossakowie"
  ]
  node [
    id 739
    label "Czartoryscy"
  ]
  node [
    id 740
    label "Sapiehowie"
  ]
  node [
    id 741
    label "series"
  ]
  node [
    id 742
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 743
    label "uprawianie"
  ]
  node [
    id 744
    label "praca_rolnicza"
  ]
  node [
    id 745
    label "collection"
  ]
  node [
    id 746
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 747
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 748
    label "sum"
  ]
  node [
    id 749
    label "gathering"
  ]
  node [
    id 750
    label "album"
  ]
  node [
    id 751
    label "grono"
  ]
  node [
    id 752
    label "kuzynostwo"
  ]
  node [
    id 753
    label "stan_cywilny"
  ]
  node [
    id 754
    label "para"
  ]
  node [
    id 755
    label "matrymonialny"
  ]
  node [
    id 756
    label "lewirat"
  ]
  node [
    id 757
    label "sakrament"
  ]
  node [
    id 758
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 759
    label "zwi&#261;zek"
  ]
  node [
    id 760
    label "partia"
  ]
  node [
    id 761
    label "czeladka"
  ]
  node [
    id 762
    label "dzietno&#347;&#263;"
  ]
  node [
    id 763
    label "bawienie_si&#281;"
  ]
  node [
    id 764
    label "pomiot"
  ]
  node [
    id 765
    label "starzy"
  ]
  node [
    id 766
    label "pokolenie"
  ]
  node [
    id 767
    label "wapniaki"
  ]
  node [
    id 768
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 769
    label "substancja_mieszkaniowa"
  ]
  node [
    id 770
    label "instytucja"
  ]
  node [
    id 771
    label "siedziba"
  ]
  node [
    id 772
    label "stead"
  ]
  node [
    id 773
    label "garderoba"
  ]
  node [
    id 774
    label "wiecha"
  ]
  node [
    id 775
    label "fratria"
  ]
  node [
    id 776
    label "maj&#261;tek_ziemski"
  ]
  node [
    id 777
    label "obrz&#281;d"
  ]
  node [
    id 778
    label "przybli&#380;enie"
  ]
  node [
    id 779
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 780
    label "kategoria"
  ]
  node [
    id 781
    label "szpaler"
  ]
  node [
    id 782
    label "lon&#380;a"
  ]
  node [
    id 783
    label "uporz&#261;dkowanie"
  ]
  node [
    id 784
    label "egzekutywa"
  ]
  node [
    id 785
    label "premier"
  ]
  node [
    id 786
    label "Londyn"
  ]
  node [
    id 787
    label "gabinet_cieni"
  ]
  node [
    id 788
    label "number"
  ]
  node [
    id 789
    label "Konsulat"
  ]
  node [
    id 790
    label "tract"
  ]
  node [
    id 791
    label "klasa"
  ]
  node [
    id 792
    label "w&#322;adza"
  ]
  node [
    id 793
    label "folk_music"
  ]
  node [
    id 794
    label "tallness"
  ]
  node [
    id 795
    label "altitude"
  ]
  node [
    id 796
    label "rozmiar"
  ]
  node [
    id 797
    label "degree"
  ]
  node [
    id 798
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 799
    label "odcinek"
  ]
  node [
    id 800
    label "k&#261;t"
  ]
  node [
    id 801
    label "wielko&#347;&#263;"
  ]
  node [
    id 802
    label "brzmienie"
  ]
  node [
    id 803
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 804
    label "ton"
  ]
  node [
    id 805
    label "ambitus"
  ]
  node [
    id 806
    label "czas"
  ]
  node [
    id 807
    label "skala"
  ]
  node [
    id 808
    label "teren"
  ]
  node [
    id 809
    label "pole"
  ]
  node [
    id 810
    label "kawa&#322;ek"
  ]
  node [
    id 811
    label "coupon"
  ]
  node [
    id 812
    label "pokwitowanie"
  ]
  node [
    id 813
    label "moneta"
  ]
  node [
    id 814
    label "epizod"
  ]
  node [
    id 815
    label "warunek_lokalowy"
  ]
  node [
    id 816
    label "liczba"
  ]
  node [
    id 817
    label "circumference"
  ]
  node [
    id 818
    label "odzie&#380;"
  ]
  node [
    id 819
    label "ilo&#347;&#263;"
  ]
  node [
    id 820
    label "znaczenie"
  ]
  node [
    id 821
    label "dymensja"
  ]
  node [
    id 822
    label "wyra&#380;anie"
  ]
  node [
    id 823
    label "sound"
  ]
  node [
    id 824
    label "tone"
  ]
  node [
    id 825
    label "wydawanie"
  ]
  node [
    id 826
    label "spirit"
  ]
  node [
    id 827
    label "rejestr"
  ]
  node [
    id 828
    label "kolorystyka"
  ]
  node [
    id 829
    label "obiekt_matematyczny"
  ]
  node [
    id 830
    label "ubocze"
  ]
  node [
    id 831
    label "rami&#281;_k&#261;ta"
  ]
  node [
    id 832
    label "rzadko&#347;&#263;"
  ]
  node [
    id 833
    label "zaleta"
  ]
  node [
    id 834
    label "measure"
  ]
  node [
    id 835
    label "opinia"
  ]
  node [
    id 836
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 837
    label "zdolno&#347;&#263;"
  ]
  node [
    id 838
    label "potencja"
  ]
  node [
    id 839
    label "property"
  ]
  node [
    id 840
    label "jednostka_monetarna"
  ]
  node [
    id 841
    label "catfish"
  ]
  node [
    id 842
    label "ryba"
  ]
  node [
    id 843
    label "sumowate"
  ]
  node [
    id 844
    label "Uzbekistan"
  ]
  node [
    id 845
    label "d&#322;ugo&#347;&#263;"
  ]
  node [
    id 846
    label "radiokomunikacja"
  ]
  node [
    id 847
    label "fala_elektromagnetyczna"
  ]
  node [
    id 848
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 849
    label "cyclicity"
  ]
  node [
    id 850
    label "dobro"
  ]
  node [
    id 851
    label "kwota"
  ]
  node [
    id 852
    label "&#347;lad"
  ]
  node [
    id 853
    label "zjawisko"
  ]
  node [
    id 854
    label "lobbysta"
  ]
  node [
    id 855
    label "doch&#243;d_narodowy"
  ]
  node [
    id 856
    label "doznawa&#263;"
  ]
  node [
    id 857
    label "czu&#263;"
  ]
  node [
    id 858
    label "zale&#380;e&#263;"
  ]
  node [
    id 859
    label "lie"
  ]
  node [
    id 860
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 861
    label "potrzebowa&#263;"
  ]
  node [
    id 862
    label "przewidywa&#263;"
  ]
  node [
    id 863
    label "smell"
  ]
  node [
    id 864
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 865
    label "uczuwa&#263;"
  ]
  node [
    id 866
    label "anticipate"
  ]
  node [
    id 867
    label "hurt"
  ]
  node [
    id 868
    label "obci&#261;&#380;enie"
  ]
  node [
    id 869
    label "op&#322;ata"
  ]
  node [
    id 870
    label "danina"
  ]
  node [
    id 871
    label "trybut"
  ]
  node [
    id 872
    label "tax"
  ]
  node [
    id 873
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 874
    label "psucie_si&#281;"
  ]
  node [
    id 875
    label "oskar&#380;enie"
  ]
  node [
    id 876
    label "zaszkodzenie"
  ]
  node [
    id 877
    label "baga&#380;"
  ]
  node [
    id 878
    label "loading"
  ]
  node [
    id 879
    label "charge"
  ]
  node [
    id 880
    label "hindrance"
  ]
  node [
    id 881
    label "na&#322;o&#380;enie"
  ]
  node [
    id 882
    label "zawada"
  ]
  node [
    id 883
    label "encumbrance"
  ]
  node [
    id 884
    label "zobowi&#261;zanie"
  ]
  node [
    id 885
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 886
    label "podatek"
  ]
  node [
    id 887
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 888
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 889
    label "osta&#263;_si&#281;"
  ]
  node [
    id 890
    label "change"
  ]
  node [
    id 891
    label "pozosta&#263;"
  ]
  node [
    id 892
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 893
    label "proceed"
  ]
  node [
    id 894
    label "support"
  ]
  node [
    id 895
    label "prze&#380;y&#263;"
  ]
  node [
    id 896
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 897
    label "realize"
  ]
  node [
    id 898
    label "zrobi&#263;"
  ]
  node [
    id 899
    label "make"
  ]
  node [
    id 900
    label "wytworzy&#263;"
  ]
  node [
    id 901
    label "give_birth"
  ]
  node [
    id 902
    label "post&#261;pi&#263;"
  ]
  node [
    id 903
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 904
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 905
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 906
    label "zorganizowa&#263;"
  ]
  node [
    id 907
    label "appoint"
  ]
  node [
    id 908
    label "wystylizowa&#263;"
  ]
  node [
    id 909
    label "cause"
  ]
  node [
    id 910
    label "przerobi&#263;"
  ]
  node [
    id 911
    label "nabra&#263;"
  ]
  node [
    id 912
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 913
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 914
    label "wydali&#263;"
  ]
  node [
    id 915
    label "manufacture"
  ]
  node [
    id 916
    label "damka"
  ]
  node [
    id 917
    label "warcaby"
  ]
  node [
    id 918
    label "promotion"
  ]
  node [
    id 919
    label "impreza"
  ]
  node [
    id 920
    label "sprzeda&#380;"
  ]
  node [
    id 921
    label "zamiana"
  ]
  node [
    id 922
    label "udzieli&#263;"
  ]
  node [
    id 923
    label "brief"
  ]
  node [
    id 924
    label "akcja"
  ]
  node [
    id 925
    label "bran&#380;a"
  ]
  node [
    id 926
    label "commencement"
  ]
  node [
    id 927
    label "okazja"
  ]
  node [
    id 928
    label "promowa&#263;"
  ]
  node [
    id 929
    label "graduacja"
  ]
  node [
    id 930
    label "nominacja"
  ]
  node [
    id 931
    label "szachy"
  ]
  node [
    id 932
    label "popularyzacja"
  ]
  node [
    id 933
    label "wypromowa&#263;"
  ]
  node [
    id 934
    label "gradation"
  ]
  node [
    id 935
    label "p&#243;&#322;rocze"
  ]
  node [
    id 936
    label "martwy_sezon"
  ]
  node [
    id 937
    label "kalendarz"
  ]
  node [
    id 938
    label "cykl_astronomiczny"
  ]
  node [
    id 939
    label "lata"
  ]
  node [
    id 940
    label "pora_roku"
  ]
  node [
    id 941
    label "stulecie"
  ]
  node [
    id 942
    label "kurs"
  ]
  node [
    id 943
    label "jubileusz"
  ]
  node [
    id 944
    label "kwarta&#322;"
  ]
  node [
    id 945
    label "miesi&#261;c"
  ]
  node [
    id 946
    label "summer"
  ]
  node [
    id 947
    label "poprzedzanie"
  ]
  node [
    id 948
    label "czasoprzestrze&#324;"
  ]
  node [
    id 949
    label "laba"
  ]
  node [
    id 950
    label "chronometria"
  ]
  node [
    id 951
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 952
    label "rachuba_czasu"
  ]
  node [
    id 953
    label "przep&#322;ywanie"
  ]
  node [
    id 954
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 955
    label "czasokres"
  ]
  node [
    id 956
    label "odczyt"
  ]
  node [
    id 957
    label "chwila"
  ]
  node [
    id 958
    label "dzieje"
  ]
  node [
    id 959
    label "kategoria_gramatyczna"
  ]
  node [
    id 960
    label "poprzedzenie"
  ]
  node [
    id 961
    label "trawienie"
  ]
  node [
    id 962
    label "pochodzi&#263;"
  ]
  node [
    id 963
    label "period"
  ]
  node [
    id 964
    label "okres_czasu"
  ]
  node [
    id 965
    label "schy&#322;ek"
  ]
  node [
    id 966
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 967
    label "odwlekanie_si&#281;"
  ]
  node [
    id 968
    label "zegar"
  ]
  node [
    id 969
    label "czwarty_wymiar"
  ]
  node [
    id 970
    label "pochodzenie"
  ]
  node [
    id 971
    label "koniugacja"
  ]
  node [
    id 972
    label "Zeitgeist"
  ]
  node [
    id 973
    label "trawi&#263;"
  ]
  node [
    id 974
    label "pogoda"
  ]
  node [
    id 975
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 976
    label "poprzedzi&#263;"
  ]
  node [
    id 977
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 978
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 979
    label "time_period"
  ]
  node [
    id 980
    label "term"
  ]
  node [
    id 981
    label "rok_akademicki"
  ]
  node [
    id 982
    label "rok_szkolny"
  ]
  node [
    id 983
    label "semester"
  ]
  node [
    id 984
    label "anniwersarz"
  ]
  node [
    id 985
    label "rocznica"
  ]
  node [
    id 986
    label "obszar"
  ]
  node [
    id 987
    label "tydzie&#324;"
  ]
  node [
    id 988
    label "miech"
  ]
  node [
    id 989
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 990
    label "kalendy"
  ]
  node [
    id 991
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 992
    label "long_time"
  ]
  node [
    id 993
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 994
    label "almanac"
  ]
  node [
    id 995
    label "rozk&#322;ad"
  ]
  node [
    id 996
    label "wydawnictwo"
  ]
  node [
    id 997
    label "Juliusz_Cezar"
  ]
  node [
    id 998
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 999
    label "zwy&#380;kowanie"
  ]
  node [
    id 1000
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 1001
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1002
    label "zaj&#281;cia"
  ]
  node [
    id 1003
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1004
    label "trasa"
  ]
  node [
    id 1005
    label "przeorientowywanie"
  ]
  node [
    id 1006
    label "przejazd"
  ]
  node [
    id 1007
    label "kierunek"
  ]
  node [
    id 1008
    label "przeorientowywa&#263;"
  ]
  node [
    id 1009
    label "nauka"
  ]
  node [
    id 1010
    label "przeorientowanie"
  ]
  node [
    id 1011
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 1012
    label "przeorientowa&#263;"
  ]
  node [
    id 1013
    label "manner"
  ]
  node [
    id 1014
    label "course"
  ]
  node [
    id 1015
    label "passage"
  ]
  node [
    id 1016
    label "zni&#380;kowanie"
  ]
  node [
    id 1017
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1018
    label "seria"
  ]
  node [
    id 1019
    label "stawka"
  ]
  node [
    id 1020
    label "way"
  ]
  node [
    id 1021
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 1022
    label "deprecjacja"
  ]
  node [
    id 1023
    label "cedu&#322;a"
  ]
  node [
    id 1024
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 1025
    label "bearing"
  ]
  node [
    id 1026
    label "Lira"
  ]
  node [
    id 1027
    label "opatrywa&#263;"
  ]
  node [
    id 1028
    label "oznacza&#263;"
  ]
  node [
    id 1029
    label "give"
  ]
  node [
    id 1030
    label "bandage"
  ]
  node [
    id 1031
    label "dopowiada&#263;"
  ]
  node [
    id 1032
    label "revise"
  ]
  node [
    id 1033
    label "zabezpiecza&#263;"
  ]
  node [
    id 1034
    label "przywraca&#263;"
  ]
  node [
    id 1035
    label "dress"
  ]
  node [
    id 1036
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 1037
    label "okres_amazo&#324;ski"
  ]
  node [
    id 1038
    label "stater"
  ]
  node [
    id 1039
    label "flow"
  ]
  node [
    id 1040
    label "choroba_przyrodzona"
  ]
  node [
    id 1041
    label "postglacja&#322;"
  ]
  node [
    id 1042
    label "sylur"
  ]
  node [
    id 1043
    label "kreda"
  ]
  node [
    id 1044
    label "ordowik"
  ]
  node [
    id 1045
    label "okres_hesperyjski"
  ]
  node [
    id 1046
    label "paleogen"
  ]
  node [
    id 1047
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 1048
    label "okres_halsztacki"
  ]
  node [
    id 1049
    label "riak"
  ]
  node [
    id 1050
    label "czwartorz&#281;d"
  ]
  node [
    id 1051
    label "podokres"
  ]
  node [
    id 1052
    label "trzeciorz&#281;d"
  ]
  node [
    id 1053
    label "kalim"
  ]
  node [
    id 1054
    label "fala"
  ]
  node [
    id 1055
    label "perm"
  ]
  node [
    id 1056
    label "retoryka"
  ]
  node [
    id 1057
    label "prekambr"
  ]
  node [
    id 1058
    label "faza"
  ]
  node [
    id 1059
    label "neogen"
  ]
  node [
    id 1060
    label "pulsacja"
  ]
  node [
    id 1061
    label "proces_fizjologiczny"
  ]
  node [
    id 1062
    label "kambr"
  ]
  node [
    id 1063
    label "kriogen"
  ]
  node [
    id 1064
    label "jednostka_geologiczna"
  ]
  node [
    id 1065
    label "orosir"
  ]
  node [
    id 1066
    label "poprzednik"
  ]
  node [
    id 1067
    label "spell"
  ]
  node [
    id 1068
    label "interstadia&#322;"
  ]
  node [
    id 1069
    label "ektas"
  ]
  node [
    id 1070
    label "sider"
  ]
  node [
    id 1071
    label "epoka"
  ]
  node [
    id 1072
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 1073
    label "cykl"
  ]
  node [
    id 1074
    label "ciota"
  ]
  node [
    id 1075
    label "pierwszorz&#281;d"
  ]
  node [
    id 1076
    label "okres_noachijski"
  ]
  node [
    id 1077
    label "ediakar"
  ]
  node [
    id 1078
    label "zdanie"
  ]
  node [
    id 1079
    label "nast&#281;pnik"
  ]
  node [
    id 1080
    label "condition"
  ]
  node [
    id 1081
    label "jura"
  ]
  node [
    id 1082
    label "glacja&#322;"
  ]
  node [
    id 1083
    label "sten"
  ]
  node [
    id 1084
    label "era"
  ]
  node [
    id 1085
    label "trias"
  ]
  node [
    id 1086
    label "p&#243;&#322;okres"
  ]
  node [
    id 1087
    label "dewon"
  ]
  node [
    id 1088
    label "karbon"
  ]
  node [
    id 1089
    label "izochronizm"
  ]
  node [
    id 1090
    label "preglacja&#322;"
  ]
  node [
    id 1091
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 1092
    label "drugorz&#281;d"
  ]
  node [
    id 1093
    label "zniewie&#347;cialec"
  ]
  node [
    id 1094
    label "oferma"
  ]
  node [
    id 1095
    label "miesi&#261;czka"
  ]
  node [
    id 1096
    label "gej"
  ]
  node [
    id 1097
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 1098
    label "pedalstwo"
  ]
  node [
    id 1099
    label "mazgaj"
  ]
  node [
    id 1100
    label "fraza"
  ]
  node [
    id 1101
    label "przekazanie"
  ]
  node [
    id 1102
    label "stanowisko"
  ]
  node [
    id 1103
    label "wypowiedzenie"
  ]
  node [
    id 1104
    label "prison_term"
  ]
  node [
    id 1105
    label "przedstawienie"
  ]
  node [
    id 1106
    label "wyra&#380;enie"
  ]
  node [
    id 1107
    label "zaliczenie"
  ]
  node [
    id 1108
    label "antylogizm"
  ]
  node [
    id 1109
    label "zmuszenie"
  ]
  node [
    id 1110
    label "konektyw"
  ]
  node [
    id 1111
    label "attitude"
  ]
  node [
    id 1112
    label "powierzenie"
  ]
  node [
    id 1113
    label "adjudication"
  ]
  node [
    id 1114
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 1115
    label "pass"
  ]
  node [
    id 1116
    label "kres"
  ]
  node [
    id 1117
    label "aalen"
  ]
  node [
    id 1118
    label "jura_wczesna"
  ]
  node [
    id 1119
    label "holocen"
  ]
  node [
    id 1120
    label "pliocen"
  ]
  node [
    id 1121
    label "plejstocen"
  ]
  node [
    id 1122
    label "paleocen"
  ]
  node [
    id 1123
    label "bajos"
  ]
  node [
    id 1124
    label "kelowej"
  ]
  node [
    id 1125
    label "eocen"
  ]
  node [
    id 1126
    label "miocen"
  ]
  node [
    id 1127
    label "&#347;rodkowy_trias"
  ]
  node [
    id 1128
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 1129
    label "wczesny_trias"
  ]
  node [
    id 1130
    label "jura_&#347;rodkowa"
  ]
  node [
    id 1131
    label "oligocen"
  ]
  node [
    id 1132
    label "implikacja"
  ]
  node [
    id 1133
    label "argument"
  ]
  node [
    id 1134
    label "kszta&#322;t"
  ]
  node [
    id 1135
    label "pasemko"
  ]
  node [
    id 1136
    label "znak_diakrytyczny"
  ]
  node [
    id 1137
    label "zafalowanie"
  ]
  node [
    id 1138
    label "kot"
  ]
  node [
    id 1139
    label "przemoc"
  ]
  node [
    id 1140
    label "reakcja"
  ]
  node [
    id 1141
    label "strumie&#324;"
  ]
  node [
    id 1142
    label "karb"
  ]
  node [
    id 1143
    label "mn&#243;stwo"
  ]
  node [
    id 1144
    label "fit"
  ]
  node [
    id 1145
    label "grzywa_fali"
  ]
  node [
    id 1146
    label "woda"
  ]
  node [
    id 1147
    label "efekt_Dopplera"
  ]
  node [
    id 1148
    label "obcinka"
  ]
  node [
    id 1149
    label "t&#322;um"
  ]
  node [
    id 1150
    label "stream"
  ]
  node [
    id 1151
    label "zafalowa&#263;"
  ]
  node [
    id 1152
    label "rozbicie_si&#281;"
  ]
  node [
    id 1153
    label "wojsko"
  ]
  node [
    id 1154
    label "clutter"
  ]
  node [
    id 1155
    label "rozbijanie_si&#281;"
  ]
  node [
    id 1156
    label "czo&#322;o_fali"
  ]
  node [
    id 1157
    label "coil"
  ]
  node [
    id 1158
    label "fotoelement"
  ]
  node [
    id 1159
    label "komutowanie"
  ]
  node [
    id 1160
    label "stan_skupienia"
  ]
  node [
    id 1161
    label "nastr&#243;j"
  ]
  node [
    id 1162
    label "przerywacz"
  ]
  node [
    id 1163
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 1164
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 1165
    label "kraw&#281;d&#378;"
  ]
  node [
    id 1166
    label "obsesja"
  ]
  node [
    id 1167
    label "dw&#243;jnik"
  ]
  node [
    id 1168
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 1169
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 1170
    label "przew&#243;d"
  ]
  node [
    id 1171
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 1172
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 1173
    label "obw&#243;d"
  ]
  node [
    id 1174
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1175
    label "komutowa&#263;"
  ]
  node [
    id 1176
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 1177
    label "serce"
  ]
  node [
    id 1178
    label "ripple"
  ]
  node [
    id 1179
    label "pracowanie"
  ]
  node [
    id 1180
    label "zabicie"
  ]
  node [
    id 1181
    label "set"
  ]
  node [
    id 1182
    label "przebieg"
  ]
  node [
    id 1183
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 1184
    label "owulacja"
  ]
  node [
    id 1185
    label "sekwencja"
  ]
  node [
    id 1186
    label "edycja"
  ]
  node [
    id 1187
    label "cycle"
  ]
  node [
    id 1188
    label "nauka_humanistyczna"
  ]
  node [
    id 1189
    label "erystyka"
  ]
  node [
    id 1190
    label "chironomia"
  ]
  node [
    id 1191
    label "elokwencja"
  ]
  node [
    id 1192
    label "sztuka"
  ]
  node [
    id 1193
    label "elokucja"
  ]
  node [
    id 1194
    label "tropika"
  ]
  node [
    id 1195
    label "era_paleozoiczna"
  ]
  node [
    id 1196
    label "ludlow"
  ]
  node [
    id 1197
    label "paleoproterozoik"
  ]
  node [
    id 1198
    label "zlodowacenie"
  ]
  node [
    id 1199
    label "asteroksylon"
  ]
  node [
    id 1200
    label "pluwia&#322;"
  ]
  node [
    id 1201
    label "mezoproterozoik"
  ]
  node [
    id 1202
    label "era_kenozoiczna"
  ]
  node [
    id 1203
    label "p&#243;&#378;ny_trias"
  ]
  node [
    id 1204
    label "ret"
  ]
  node [
    id 1205
    label "era_mezozoiczna"
  ]
  node [
    id 1206
    label "konodont"
  ]
  node [
    id 1207
    label "kajper"
  ]
  node [
    id 1208
    label "neoproterozoik"
  ]
  node [
    id 1209
    label "chalk"
  ]
  node [
    id 1210
    label "narz&#281;dzie"
  ]
  node [
    id 1211
    label "santon"
  ]
  node [
    id 1212
    label "cenoman"
  ]
  node [
    id 1213
    label "neokom"
  ]
  node [
    id 1214
    label "apt"
  ]
  node [
    id 1215
    label "pobia&#322;ka"
  ]
  node [
    id 1216
    label "ska&#322;a_organogeniczna"
  ]
  node [
    id 1217
    label "alb"
  ]
  node [
    id 1218
    label "pastel"
  ]
  node [
    id 1219
    label "turon"
  ]
  node [
    id 1220
    label "pteranodon"
  ]
  node [
    id 1221
    label "wieloton"
  ]
  node [
    id 1222
    label "tu&#324;czyk"
  ]
  node [
    id 1223
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1224
    label "zabarwienie"
  ]
  node [
    id 1225
    label "interwa&#322;"
  ]
  node [
    id 1226
    label "modalizm"
  ]
  node [
    id 1227
    label "ubarwienie"
  ]
  node [
    id 1228
    label "note"
  ]
  node [
    id 1229
    label "formality"
  ]
  node [
    id 1230
    label "glinka"
  ]
  node [
    id 1231
    label "jednostka"
  ]
  node [
    id 1232
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1233
    label "zwyczaj"
  ]
  node [
    id 1234
    label "solmizacja"
  ]
  node [
    id 1235
    label "r&#243;&#380;nica"
  ]
  node [
    id 1236
    label "akcent"
  ]
  node [
    id 1237
    label "repetycja"
  ]
  node [
    id 1238
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1239
    label "heksachord"
  ]
  node [
    id 1240
    label "pistolet_maszynowy"
  ]
  node [
    id 1241
    label "jednostka_si&#322;y"
  ]
  node [
    id 1242
    label "pozauk&#322;adowa_jednostka_miary"
  ]
  node [
    id 1243
    label "wapie&#324;_pi&#324;czowski"
  ]
  node [
    id 1244
    label "pensylwan"
  ]
  node [
    id 1245
    label "tworzywo"
  ]
  node [
    id 1246
    label "mezozaur"
  ]
  node [
    id 1247
    label "pikaia"
  ]
  node [
    id 1248
    label "era_eozoiczna"
  ]
  node [
    id 1249
    label "era_archaiczna"
  ]
  node [
    id 1250
    label "rand"
  ]
  node [
    id 1251
    label "huron"
  ]
  node [
    id 1252
    label "Permian"
  ]
  node [
    id 1253
    label "blokada"
  ]
  node [
    id 1254
    label "cechsztyn"
  ]
  node [
    id 1255
    label "dogger"
  ]
  node [
    id 1256
    label "plezjozaur"
  ]
  node [
    id 1257
    label "euoplocefal"
  ]
  node [
    id 1258
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1259
    label "eon"
  ]
  node [
    id 1260
    label "niezb&#281;dnie"
  ]
  node [
    id 1261
    label "konieczny"
  ]
  node [
    id 1262
    label "necessarily"
  ]
  node [
    id 1263
    label "s&#322;uszny"
  ]
  node [
    id 1264
    label "symetryczny"
  ]
  node [
    id 1265
    label "prawid&#322;owo"
  ]
  node [
    id 1266
    label "po&#380;&#261;dany"
  ]
  node [
    id 1267
    label "s&#322;usznie"
  ]
  node [
    id 1268
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1269
    label "zasadny"
  ]
  node [
    id 1270
    label "nale&#380;yty"
  ]
  node [
    id 1271
    label "prawdziwy"
  ]
  node [
    id 1272
    label "solidny"
  ]
  node [
    id 1273
    label "dobroczynny"
  ]
  node [
    id 1274
    label "czw&#243;rka"
  ]
  node [
    id 1275
    label "spokojny"
  ]
  node [
    id 1276
    label "skuteczny"
  ]
  node [
    id 1277
    label "&#347;mieszny"
  ]
  node [
    id 1278
    label "grzeczny"
  ]
  node [
    id 1279
    label "powitanie"
  ]
  node [
    id 1280
    label "dobrze"
  ]
  node [
    id 1281
    label "ca&#322;y"
  ]
  node [
    id 1282
    label "zwrot"
  ]
  node [
    id 1283
    label "pomy&#347;lny"
  ]
  node [
    id 1284
    label "moralny"
  ]
  node [
    id 1285
    label "drogi"
  ]
  node [
    id 1286
    label "pozytywny"
  ]
  node [
    id 1287
    label "odpowiedni"
  ]
  node [
    id 1288
    label "pos&#322;uszny"
  ]
  node [
    id 1289
    label "symetrycznie"
  ]
  node [
    id 1290
    label "broadcast"
  ]
  node [
    id 1291
    label "nazwanie"
  ]
  node [
    id 1292
    label "przes&#322;anie"
  ]
  node [
    id 1293
    label "akt"
  ]
  node [
    id 1294
    label "przyznanie"
  ]
  node [
    id 1295
    label "denomination"
  ]
  node [
    id 1296
    label "danie"
  ]
  node [
    id 1297
    label "confession"
  ]
  node [
    id 1298
    label "stwierdzenie"
  ]
  node [
    id 1299
    label "recognition"
  ]
  node [
    id 1300
    label "forward"
  ]
  node [
    id 1301
    label "message"
  ]
  node [
    id 1302
    label "p&#243;j&#347;cie"
  ]
  node [
    id 1303
    label "bed"
  ]
  node [
    id 1304
    label "podnieci&#263;"
  ]
  node [
    id 1305
    label "scena"
  ]
  node [
    id 1306
    label "numer"
  ]
  node [
    id 1307
    label "po&#380;ycie"
  ]
  node [
    id 1308
    label "podniecenie"
  ]
  node [
    id 1309
    label "nago&#347;&#263;"
  ]
  node [
    id 1310
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 1311
    label "seks"
  ]
  node [
    id 1312
    label "podniecanie"
  ]
  node [
    id 1313
    label "imisja"
  ]
  node [
    id 1314
    label "rozmna&#380;anie"
  ]
  node [
    id 1315
    label "ruch_frykcyjny"
  ]
  node [
    id 1316
    label "ontologia"
  ]
  node [
    id 1317
    label "na_pieska"
  ]
  node [
    id 1318
    label "pozycja_misjonarska"
  ]
  node [
    id 1319
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1320
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 1321
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1322
    label "gra_wst&#281;pna"
  ]
  node [
    id 1323
    label "erotyka"
  ]
  node [
    id 1324
    label "urzeczywistnienie"
  ]
  node [
    id 1325
    label "baraszki"
  ]
  node [
    id 1326
    label "po&#380;&#261;danie"
  ]
  node [
    id 1327
    label "wzw&#243;d"
  ]
  node [
    id 1328
    label "funkcja"
  ]
  node [
    id 1329
    label "act"
  ]
  node [
    id 1330
    label "arystotelizm"
  ]
  node [
    id 1331
    label "podnieca&#263;"
  ]
  node [
    id 1332
    label "leksem"
  ]
  node [
    id 1333
    label "wezwanie"
  ]
  node [
    id 1334
    label "patron"
  ]
  node [
    id 1335
    label "law"
  ]
  node [
    id 1336
    label "authorization"
  ]
  node [
    id 1337
    label "title"
  ]
  node [
    id 1338
    label "posiada&#263;"
  ]
  node [
    id 1339
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1340
    label "potencja&#322;"
  ]
  node [
    id 1341
    label "wyb&#243;r"
  ]
  node [
    id 1342
    label "prospect"
  ]
  node [
    id 1343
    label "ability"
  ]
  node [
    id 1344
    label "obliczeniowo"
  ]
  node [
    id 1345
    label "alternatywa"
  ]
  node [
    id 1346
    label "operator_modalny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 5
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 772
  ]
  edge [
    source 11
    target 773
  ]
  edge [
    source 11
    target 774
  ]
  edge [
    source 11
    target 775
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 777
  ]
  edge [
    source 11
    target 778
  ]
  edge [
    source 11
    target 779
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 781
  ]
  edge [
    source 11
    target 782
  ]
  edge [
    source 11
    target 783
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 27
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 368
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 364
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 856
  ]
  edge [
    source 14
    target 857
  ]
  edge [
    source 14
    target 858
  ]
  edge [
    source 14
    target 859
  ]
  edge [
    source 14
    target 860
  ]
  edge [
    source 14
    target 861
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 862
  ]
  edge [
    source 14
    target 29
  ]
  edge [
    source 14
    target 863
  ]
  edge [
    source 14
    target 864
  ]
  edge [
    source 14
    target 865
  ]
  edge [
    source 14
    target 826
  ]
  edge [
    source 14
    target 866
  ]
  edge [
    source 14
    target 867
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 868
  ]
  edge [
    source 15
    target 869
  ]
  edge [
    source 15
    target 870
  ]
  edge [
    source 15
    target 871
  ]
  edge [
    source 15
    target 872
  ]
  edge [
    source 15
    target 873
  ]
  edge [
    source 15
    target 874
  ]
  edge [
    source 15
    target 875
  ]
  edge [
    source 15
    target 876
  ]
  edge [
    source 15
    target 877
  ]
  edge [
    source 15
    target 878
  ]
  edge [
    source 15
    target 879
  ]
  edge [
    source 15
    target 880
  ]
  edge [
    source 15
    target 881
  ]
  edge [
    source 15
    target 882
  ]
  edge [
    source 15
    target 883
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 851
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 511
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 454
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 440
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 894
  ]
  edge [
    source 17
    target 895
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 454
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 71
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 54
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 791
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 935
  ]
  edge [
    source 19
    target 936
  ]
  edge [
    source 19
    target 937
  ]
  edge [
    source 19
    target 938
  ]
  edge [
    source 19
    target 939
  ]
  edge [
    source 19
    target 940
  ]
  edge [
    source 19
    target 941
  ]
  edge [
    source 19
    target 942
  ]
  edge [
    source 19
    target 806
  ]
  edge [
    source 19
    target 943
  ]
  edge [
    source 19
    target 522
  ]
  edge [
    source 19
    target 944
  ]
  edge [
    source 19
    target 945
  ]
  edge [
    source 19
    target 946
  ]
  edge [
    source 19
    target 628
  ]
  edge [
    source 19
    target 629
  ]
  edge [
    source 19
    target 630
  ]
  edge [
    source 19
    target 543
  ]
  edge [
    source 19
    target 631
  ]
  edge [
    source 19
    target 99
  ]
  edge [
    source 19
    target 545
  ]
  edge [
    source 19
    target 306
  ]
  edge [
    source 19
    target 632
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 19
    target 633
  ]
  edge [
    source 19
    target 634
  ]
  edge [
    source 19
    target 635
  ]
  edge [
    source 19
    target 636
  ]
  edge [
    source 19
    target 637
  ]
  edge [
    source 19
    target 638
  ]
  edge [
    source 19
    target 639
  ]
  edge [
    source 19
    target 640
  ]
  edge [
    source 19
    target 641
  ]
  edge [
    source 19
    target 642
  ]
  edge [
    source 19
    target 643
  ]
  edge [
    source 19
    target 644
  ]
  edge [
    source 19
    target 645
  ]
  edge [
    source 19
    target 646
  ]
  edge [
    source 19
    target 647
  ]
  edge [
    source 19
    target 648
  ]
  edge [
    source 19
    target 649
  ]
  edge [
    source 19
    target 650
  ]
  edge [
    source 19
    target 947
  ]
  edge [
    source 19
    target 948
  ]
  edge [
    source 19
    target 949
  ]
  edge [
    source 19
    target 464
  ]
  edge [
    source 19
    target 950
  ]
  edge [
    source 19
    target 951
  ]
  edge [
    source 19
    target 952
  ]
  edge [
    source 19
    target 953
  ]
  edge [
    source 19
    target 954
  ]
  edge [
    source 19
    target 955
  ]
  edge [
    source 19
    target 956
  ]
  edge [
    source 19
    target 957
  ]
  edge [
    source 19
    target 411
  ]
  edge [
    source 19
    target 958
  ]
  edge [
    source 19
    target 959
  ]
  edge [
    source 19
    target 960
  ]
  edge [
    source 19
    target 961
  ]
  edge [
    source 19
    target 962
  ]
  edge [
    source 19
    target 963
  ]
  edge [
    source 19
    target 964
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 965
  ]
  edge [
    source 19
    target 966
  ]
  edge [
    source 19
    target 967
  ]
  edge [
    source 19
    target 968
  ]
  edge [
    source 19
    target 450
  ]
  edge [
    source 19
    target 969
  ]
  edge [
    source 19
    target 970
  ]
  edge [
    source 19
    target 971
  ]
  edge [
    source 19
    target 972
  ]
  edge [
    source 19
    target 973
  ]
  edge [
    source 19
    target 974
  ]
  edge [
    source 19
    target 975
  ]
  edge [
    source 19
    target 976
  ]
  edge [
    source 19
    target 977
  ]
  edge [
    source 19
    target 978
  ]
  edge [
    source 19
    target 979
  ]
  edge [
    source 19
    target 980
  ]
  edge [
    source 19
    target 981
  ]
  edge [
    source 19
    target 982
  ]
  edge [
    source 19
    target 983
  ]
  edge [
    source 19
    target 984
  ]
  edge [
    source 19
    target 985
  ]
  edge [
    source 19
    target 986
  ]
  edge [
    source 19
    target 987
  ]
  edge [
    source 19
    target 988
  ]
  edge [
    source 19
    target 989
  ]
  edge [
    source 19
    target 990
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 791
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 72
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 456
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 19
    target 1026
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1027
  ]
  edge [
    source 21
    target 806
  ]
  edge [
    source 21
    target 947
  ]
  edge [
    source 21
    target 948
  ]
  edge [
    source 21
    target 949
  ]
  edge [
    source 21
    target 464
  ]
  edge [
    source 21
    target 950
  ]
  edge [
    source 21
    target 951
  ]
  edge [
    source 21
    target 952
  ]
  edge [
    source 21
    target 953
  ]
  edge [
    source 21
    target 954
  ]
  edge [
    source 21
    target 955
  ]
  edge [
    source 21
    target 956
  ]
  edge [
    source 21
    target 957
  ]
  edge [
    source 21
    target 411
  ]
  edge [
    source 21
    target 958
  ]
  edge [
    source 21
    target 959
  ]
  edge [
    source 21
    target 960
  ]
  edge [
    source 21
    target 961
  ]
  edge [
    source 21
    target 962
  ]
  edge [
    source 21
    target 963
  ]
  edge [
    source 21
    target 964
  ]
  edge [
    source 21
    target 965
  ]
  edge [
    source 21
    target 966
  ]
  edge [
    source 21
    target 967
  ]
  edge [
    source 21
    target 968
  ]
  edge [
    source 21
    target 450
  ]
  edge [
    source 21
    target 969
  ]
  edge [
    source 21
    target 970
  ]
  edge [
    source 21
    target 971
  ]
  edge [
    source 21
    target 972
  ]
  edge [
    source 21
    target 973
  ]
  edge [
    source 21
    target 974
  ]
  edge [
    source 21
    target 975
  ]
  edge [
    source 21
    target 976
  ]
  edge [
    source 21
    target 977
  ]
  edge [
    source 21
    target 978
  ]
  edge [
    source 21
    target 979
  ]
  edge [
    source 21
    target 1028
  ]
  edge [
    source 21
    target 1029
  ]
  edge [
    source 21
    target 1030
  ]
  edge [
    source 21
    target 1031
  ]
  edge [
    source 21
    target 1032
  ]
  edge [
    source 21
    target 1033
  ]
  edge [
    source 21
    target 1034
  ]
  edge [
    source 21
    target 1035
  ]
  edge [
    source 21
    target 1036
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1037
  ]
  edge [
    source 22
    target 1038
  ]
  edge [
    source 22
    target 1039
  ]
  edge [
    source 22
    target 1040
  ]
  edge [
    source 22
    target 1041
  ]
  edge [
    source 22
    target 1042
  ]
  edge [
    source 22
    target 1043
  ]
  edge [
    source 22
    target 1044
  ]
  edge [
    source 22
    target 1045
  ]
  edge [
    source 22
    target 1046
  ]
  edge [
    source 22
    target 1047
  ]
  edge [
    source 22
    target 1048
  ]
  edge [
    source 22
    target 1049
  ]
  edge [
    source 22
    target 1050
  ]
  edge [
    source 22
    target 1051
  ]
  edge [
    source 22
    target 1052
  ]
  edge [
    source 22
    target 1053
  ]
  edge [
    source 22
    target 1054
  ]
  edge [
    source 22
    target 1055
  ]
  edge [
    source 22
    target 1056
  ]
  edge [
    source 22
    target 1057
  ]
  edge [
    source 22
    target 1058
  ]
  edge [
    source 22
    target 1059
  ]
  edge [
    source 22
    target 1060
  ]
  edge [
    source 22
    target 1061
  ]
  edge [
    source 22
    target 1062
  ]
  edge [
    source 22
    target 958
  ]
  edge [
    source 22
    target 1063
  ]
  edge [
    source 22
    target 1064
  ]
  edge [
    source 22
    target 979
  ]
  edge [
    source 22
    target 963
  ]
  edge [
    source 22
    target 804
  ]
  edge [
    source 22
    target 1065
  ]
  edge [
    source 22
    target 964
  ]
  edge [
    source 22
    target 1066
  ]
  edge [
    source 22
    target 1067
  ]
  edge [
    source 22
    target 1068
  ]
  edge [
    source 22
    target 1069
  ]
  edge [
    source 22
    target 1070
  ]
  edge [
    source 22
    target 1071
  ]
  edge [
    source 22
    target 981
  ]
  edge [
    source 22
    target 1072
  ]
  edge [
    source 22
    target 965
  ]
  edge [
    source 22
    target 1073
  ]
  edge [
    source 22
    target 1074
  ]
  edge [
    source 22
    target 1075
  ]
  edge [
    source 22
    target 1076
  ]
  edge [
    source 22
    target 806
  ]
  edge [
    source 22
    target 1077
  ]
  edge [
    source 22
    target 1078
  ]
  edge [
    source 22
    target 1079
  ]
  edge [
    source 22
    target 1080
  ]
  edge [
    source 22
    target 1081
  ]
  edge [
    source 22
    target 1082
  ]
  edge [
    source 22
    target 1083
  ]
  edge [
    source 22
    target 972
  ]
  edge [
    source 22
    target 1084
  ]
  edge [
    source 22
    target 1085
  ]
  edge [
    source 22
    target 1086
  ]
  edge [
    source 22
    target 982
  ]
  edge [
    source 22
    target 1087
  ]
  edge [
    source 22
    target 1088
  ]
  edge [
    source 22
    target 1089
  ]
  edge [
    source 22
    target 1090
  ]
  edge [
    source 22
    target 978
  ]
  edge [
    source 22
    target 1091
  ]
  edge [
    source 22
    target 1092
  ]
  edge [
    source 22
    target 983
  ]
  edge [
    source 22
    target 1093
  ]
  edge [
    source 22
    target 1094
  ]
  edge [
    source 22
    target 1095
  ]
  edge [
    source 22
    target 1096
  ]
  edge [
    source 22
    target 1097
  ]
  edge [
    source 22
    target 1098
  ]
  edge [
    source 22
    target 1099
  ]
  edge [
    source 22
    target 947
  ]
  edge [
    source 22
    target 948
  ]
  edge [
    source 22
    target 949
  ]
  edge [
    source 22
    target 464
  ]
  edge [
    source 22
    target 950
  ]
  edge [
    source 22
    target 951
  ]
  edge [
    source 22
    target 952
  ]
  edge [
    source 22
    target 953
  ]
  edge [
    source 22
    target 954
  ]
  edge [
    source 22
    target 955
  ]
  edge [
    source 22
    target 956
  ]
  edge [
    source 22
    target 957
  ]
  edge [
    source 22
    target 411
  ]
  edge [
    source 22
    target 959
  ]
  edge [
    source 22
    target 960
  ]
  edge [
    source 22
    target 961
  ]
  edge [
    source 22
    target 962
  ]
  edge [
    source 22
    target 966
  ]
  edge [
    source 22
    target 967
  ]
  edge [
    source 22
    target 968
  ]
  edge [
    source 22
    target 450
  ]
  edge [
    source 22
    target 969
  ]
  edge [
    source 22
    target 970
  ]
  edge [
    source 22
    target 971
  ]
  edge [
    source 22
    target 973
  ]
  edge [
    source 22
    target 974
  ]
  edge [
    source 22
    target 975
  ]
  edge [
    source 22
    target 976
  ]
  edge [
    source 22
    target 977
  ]
  edge [
    source 22
    target 287
  ]
  edge [
    source 22
    target 1100
  ]
  edge [
    source 22
    target 1101
  ]
  edge [
    source 22
    target 1102
  ]
  edge [
    source 22
    target 1103
  ]
  edge [
    source 22
    target 1104
  ]
  edge [
    source 22
    target 295
  ]
  edge [
    source 22
    target 1105
  ]
  edge [
    source 22
    target 1106
  ]
  edge [
    source 22
    target 1107
  ]
  edge [
    source 22
    target 1108
  ]
  edge [
    source 22
    target 1109
  ]
  edge [
    source 22
    target 1110
  ]
  edge [
    source 22
    target 1111
  ]
  edge [
    source 22
    target 1112
  ]
  edge [
    source 22
    target 1113
  ]
  edge [
    source 22
    target 1114
  ]
  edge [
    source 22
    target 1115
  ]
  edge [
    source 22
    target 1116
  ]
  edge [
    source 22
    target 1117
  ]
  edge [
    source 22
    target 1118
  ]
  edge [
    source 22
    target 1119
  ]
  edge [
    source 22
    target 1120
  ]
  edge [
    source 22
    target 1121
  ]
  edge [
    source 22
    target 1122
  ]
  edge [
    source 22
    target 1123
  ]
  edge [
    source 22
    target 1124
  ]
  edge [
    source 22
    target 1125
  ]
  edge [
    source 22
    target 1126
  ]
  edge [
    source 22
    target 1127
  ]
  edge [
    source 22
    target 980
  ]
  edge [
    source 22
    target 1128
  ]
  edge [
    source 22
    target 1129
  ]
  edge [
    source 22
    target 1130
  ]
  edge [
    source 22
    target 1131
  ]
  edge [
    source 22
    target 1132
  ]
  edge [
    source 22
    target 332
  ]
  edge [
    source 22
    target 514
  ]
  edge [
    source 22
    target 1133
  ]
  edge [
    source 22
    target 1134
  ]
  edge [
    source 22
    target 1135
  ]
  edge [
    source 22
    target 1136
  ]
  edge [
    source 22
    target 853
  ]
  edge [
    source 22
    target 1137
  ]
  edge [
    source 22
    target 1138
  ]
  edge [
    source 22
    target 1139
  ]
  edge [
    source 22
    target 1140
  ]
  edge [
    source 22
    target 1141
  ]
  edge [
    source 22
    target 1142
  ]
  edge [
    source 22
    target 1143
  ]
  edge [
    source 22
    target 1144
  ]
  edge [
    source 22
    target 1145
  ]
  edge [
    source 22
    target 1146
  ]
  edge [
    source 22
    target 1147
  ]
  edge [
    source 22
    target 1148
  ]
  edge [
    source 22
    target 1149
  ]
  edge [
    source 22
    target 1150
  ]
  edge [
    source 22
    target 1151
  ]
  edge [
    source 22
    target 1152
  ]
  edge [
    source 22
    target 1153
  ]
  edge [
    source 22
    target 1154
  ]
  edge [
    source 22
    target 1155
  ]
  edge [
    source 22
    target 1156
  ]
  edge [
    source 22
    target 938
  ]
  edge [
    source 22
    target 1157
  ]
  edge [
    source 22
    target 1158
  ]
  edge [
    source 22
    target 1159
  ]
  edge [
    source 22
    target 1160
  ]
  edge [
    source 22
    target 1161
  ]
  edge [
    source 22
    target 1162
  ]
  edge [
    source 22
    target 1163
  ]
  edge [
    source 22
    target 1164
  ]
  edge [
    source 22
    target 1165
  ]
  edge [
    source 22
    target 1166
  ]
  edge [
    source 22
    target 1167
  ]
  edge [
    source 22
    target 1168
  ]
  edge [
    source 22
    target 1169
  ]
  edge [
    source 22
    target 848
  ]
  edge [
    source 22
    target 1170
  ]
  edge [
    source 22
    target 1171
  ]
  edge [
    source 22
    target 1172
  ]
  edge [
    source 22
    target 1173
  ]
  edge [
    source 22
    target 1174
  ]
  edge [
    source 22
    target 797
  ]
  edge [
    source 22
    target 1175
  ]
  edge [
    source 22
    target 1176
  ]
  edge [
    source 22
    target 152
  ]
  edge [
    source 22
    target 1177
  ]
  edge [
    source 22
    target 1178
  ]
  edge [
    source 22
    target 1179
  ]
  edge [
    source 22
    target 1180
  ]
  edge [
    source 22
    target 1181
  ]
  edge [
    source 22
    target 1182
  ]
  edge [
    source 22
    target 1183
  ]
  edge [
    source 22
    target 1184
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 99
  ]
  edge [
    source 22
    target 1186
  ]
  edge [
    source 22
    target 1187
  ]
  edge [
    source 22
    target 347
  ]
  edge [
    source 22
    target 1188
  ]
  edge [
    source 22
    target 1189
  ]
  edge [
    source 22
    target 1190
  ]
  edge [
    source 22
    target 1191
  ]
  edge [
    source 22
    target 1192
  ]
  edge [
    source 22
    target 1193
  ]
  edge [
    source 22
    target 1194
  ]
  edge [
    source 22
    target 649
  ]
  edge [
    source 22
    target 1195
  ]
  edge [
    source 22
    target 1196
  ]
  edge [
    source 22
    target 813
  ]
  edge [
    source 22
    target 1197
  ]
  edge [
    source 22
    target 1198
  ]
  edge [
    source 22
    target 1199
  ]
  edge [
    source 22
    target 1200
  ]
  edge [
    source 22
    target 1201
  ]
  edge [
    source 22
    target 1202
  ]
  edge [
    source 22
    target 1203
  ]
  edge [
    source 22
    target 1204
  ]
  edge [
    source 22
    target 1205
  ]
  edge [
    source 22
    target 1206
  ]
  edge [
    source 22
    target 1207
  ]
  edge [
    source 22
    target 1208
  ]
  edge [
    source 22
    target 1209
  ]
  edge [
    source 22
    target 1210
  ]
  edge [
    source 22
    target 1211
  ]
  edge [
    source 22
    target 1212
  ]
  edge [
    source 22
    target 1213
  ]
  edge [
    source 22
    target 1214
  ]
  edge [
    source 22
    target 1215
  ]
  edge [
    source 22
    target 1216
  ]
  edge [
    source 22
    target 1217
  ]
  edge [
    source 22
    target 1218
  ]
  edge [
    source 22
    target 1219
  ]
  edge [
    source 22
    target 1220
  ]
  edge [
    source 22
    target 1221
  ]
  edge [
    source 22
    target 1222
  ]
  edge [
    source 22
    target 1223
  ]
  edge [
    source 22
    target 1224
  ]
  edge [
    source 22
    target 1225
  ]
  edge [
    source 22
    target 1226
  ]
  edge [
    source 22
    target 1227
  ]
  edge [
    source 22
    target 1228
  ]
  edge [
    source 22
    target 1229
  ]
  edge [
    source 22
    target 1230
  ]
  edge [
    source 22
    target 1231
  ]
  edge [
    source 22
    target 823
  ]
  edge [
    source 22
    target 1232
  ]
  edge [
    source 22
    target 1233
  ]
  edge [
    source 22
    target 1234
  ]
  edge [
    source 22
    target 1018
  ]
  edge [
    source 22
    target 824
  ]
  edge [
    source 22
    target 828
  ]
  edge [
    source 22
    target 1235
  ]
  edge [
    source 22
    target 1236
  ]
  edge [
    source 22
    target 1237
  ]
  edge [
    source 22
    target 1238
  ]
  edge [
    source 22
    target 1239
  ]
  edge [
    source 22
    target 827
  ]
  edge [
    source 22
    target 1240
  ]
  edge [
    source 22
    target 1241
  ]
  edge [
    source 22
    target 1242
  ]
  edge [
    source 22
    target 1243
  ]
  edge [
    source 22
    target 1244
  ]
  edge [
    source 22
    target 1245
  ]
  edge [
    source 22
    target 1246
  ]
  edge [
    source 22
    target 1247
  ]
  edge [
    source 22
    target 1248
  ]
  edge [
    source 22
    target 1249
  ]
  edge [
    source 22
    target 1250
  ]
  edge [
    source 22
    target 1251
  ]
  edge [
    source 22
    target 1252
  ]
  edge [
    source 22
    target 1253
  ]
  edge [
    source 22
    target 1254
  ]
  edge [
    source 22
    target 1255
  ]
  edge [
    source 22
    target 1256
  ]
  edge [
    source 22
    target 1257
  ]
  edge [
    source 22
    target 1258
  ]
  edge [
    source 22
    target 1259
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1260
  ]
  edge [
    source 24
    target 1261
  ]
  edge [
    source 24
    target 1262
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1263
  ]
  edge [
    source 26
    target 1264
  ]
  edge [
    source 26
    target 1265
  ]
  edge [
    source 26
    target 1266
  ]
  edge [
    source 26
    target 217
  ]
  edge [
    source 26
    target 1267
  ]
  edge [
    source 26
    target 1268
  ]
  edge [
    source 26
    target 1269
  ]
  edge [
    source 26
    target 1270
  ]
  edge [
    source 26
    target 1271
  ]
  edge [
    source 26
    target 1272
  ]
  edge [
    source 26
    target 1273
  ]
  edge [
    source 26
    target 1274
  ]
  edge [
    source 26
    target 1275
  ]
  edge [
    source 26
    target 1276
  ]
  edge [
    source 26
    target 1277
  ]
  edge [
    source 26
    target 208
  ]
  edge [
    source 26
    target 1278
  ]
  edge [
    source 26
    target 1279
  ]
  edge [
    source 26
    target 1280
  ]
  edge [
    source 26
    target 1281
  ]
  edge [
    source 26
    target 1282
  ]
  edge [
    source 26
    target 1283
  ]
  edge [
    source 26
    target 1284
  ]
  edge [
    source 26
    target 1285
  ]
  edge [
    source 26
    target 1286
  ]
  edge [
    source 26
    target 1287
  ]
  edge [
    source 26
    target 215
  ]
  edge [
    source 26
    target 1288
  ]
  edge [
    source 26
    target 1289
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1290
  ]
  edge [
    source 27
    target 128
  ]
  edge [
    source 27
    target 673
  ]
  edge [
    source 27
    target 1291
  ]
  edge [
    source 27
    target 1292
  ]
  edge [
    source 27
    target 1293
  ]
  edge [
    source 27
    target 1294
  ]
  edge [
    source 27
    target 1295
  ]
  edge [
    source 27
    target 74
  ]
  edge [
    source 27
    target 1296
  ]
  edge [
    source 27
    target 1297
  ]
  edge [
    source 27
    target 1298
  ]
  edge [
    source 27
    target 1299
  ]
  edge [
    source 27
    target 510
  ]
  edge [
    source 27
    target 155
  ]
  edge [
    source 27
    target 156
  ]
  edge [
    source 27
    target 157
  ]
  edge [
    source 27
    target 158
  ]
  edge [
    source 27
    target 159
  ]
  edge [
    source 27
    target 242
  ]
  edge [
    source 27
    target 1101
  ]
  edge [
    source 27
    target 1300
  ]
  edge [
    source 27
    target 1301
  ]
  edge [
    source 27
    target 820
  ]
  edge [
    source 27
    target 1302
  ]
  edge [
    source 27
    target 1303
  ]
  edge [
    source 27
    target 294
  ]
  edge [
    source 27
    target 55
  ]
  edge [
    source 27
    target 1304
  ]
  edge [
    source 27
    target 1305
  ]
  edge [
    source 27
    target 404
  ]
  edge [
    source 27
    target 1306
  ]
  edge [
    source 27
    target 1307
  ]
  edge [
    source 27
    target 364
  ]
  edge [
    source 27
    target 1308
  ]
  edge [
    source 27
    target 1309
  ]
  edge [
    source 27
    target 1310
  ]
  edge [
    source 27
    target 62
  ]
  edge [
    source 27
    target 1311
  ]
  edge [
    source 27
    target 1312
  ]
  edge [
    source 27
    target 1313
  ]
  edge [
    source 27
    target 1233
  ]
  edge [
    source 27
    target 1314
  ]
  edge [
    source 27
    target 1315
  ]
  edge [
    source 27
    target 1316
  ]
  edge [
    source 27
    target 1317
  ]
  edge [
    source 27
    target 1318
  ]
  edge [
    source 27
    target 1319
  ]
  edge [
    source 27
    target 118
  ]
  edge [
    source 27
    target 1320
  ]
  edge [
    source 27
    target 1321
  ]
  edge [
    source 27
    target 1322
  ]
  edge [
    source 27
    target 1323
  ]
  edge [
    source 27
    target 1324
  ]
  edge [
    source 27
    target 1325
  ]
  edge [
    source 27
    target 70
  ]
  edge [
    source 27
    target 1326
  ]
  edge [
    source 27
    target 1327
  ]
  edge [
    source 27
    target 1328
  ]
  edge [
    source 27
    target 1329
  ]
  edge [
    source 27
    target 1330
  ]
  edge [
    source 27
    target 1331
  ]
  edge [
    source 27
    target 980
  ]
  edge [
    source 27
    target 1332
  ]
  edge [
    source 27
    target 1333
  ]
  edge [
    source 27
    target 1334
  ]
  edge [
    source 28
    target 1335
  ]
  edge [
    source 28
    target 1336
  ]
  edge [
    source 28
    target 128
  ]
  edge [
    source 28
    target 1337
  ]
  edge [
    source 28
    target 466
  ]
  edge [
    source 28
    target 74
  ]
  edge [
    source 28
    target 157
  ]
  edge [
    source 28
    target 158
  ]
  edge [
    source 28
    target 159
  ]
  edge [
    source 28
    target 155
  ]
  edge [
    source 28
    target 156
  ]
  edge [
    source 28
    target 1338
  ]
  edge [
    source 28
    target 1339
  ]
  edge [
    source 28
    target 784
  ]
  edge [
    source 28
    target 1340
  ]
  edge [
    source 28
    target 1341
  ]
  edge [
    source 28
    target 1342
  ]
  edge [
    source 28
    target 1343
  ]
  edge [
    source 28
    target 1344
  ]
  edge [
    source 28
    target 1345
  ]
  edge [
    source 28
    target 152
  ]
  edge [
    source 28
    target 1346
  ]
  edge [
    source 28
    target 53
  ]
  edge [
    source 28
    target 54
  ]
  edge [
    source 28
    target 55
  ]
  edge [
    source 28
    target 56
  ]
  edge [
    source 28
    target 57
  ]
  edge [
    source 28
    target 58
  ]
  edge [
    source 28
    target 59
  ]
  edge [
    source 28
    target 60
  ]
  edge [
    source 28
    target 61
  ]
  edge [
    source 28
    target 64
  ]
  edge [
    source 28
    target 63
  ]
  edge [
    source 28
    target 62
  ]
  edge [
    source 28
    target 65
  ]
  edge [
    source 28
    target 66
  ]
  edge [
    source 28
    target 67
  ]
]
