graph [
  node [
    id 0
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 1
    label "jebanyn"
    origin "text"
  ]
  node [
    id 2
    label "razem"
    origin "text"
  ]
  node [
    id 3
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 4
    label "lekarstwo"
    origin "text"
  ]
  node [
    id 5
    label "heheszki"
    origin "text"
  ]
  node [
    id 6
    label "jaki&#347;"
  ]
  node [
    id 7
    label "przyzwoity"
  ]
  node [
    id 8
    label "ciekawy"
  ]
  node [
    id 9
    label "jako&#347;"
  ]
  node [
    id 10
    label "jako_tako"
  ]
  node [
    id 11
    label "niez&#322;y"
  ]
  node [
    id 12
    label "dziwny"
  ]
  node [
    id 13
    label "charakterystyczny"
  ]
  node [
    id 14
    label "&#322;&#261;cznie"
  ]
  node [
    id 15
    label "&#322;&#261;czny"
  ]
  node [
    id 16
    label "zbiorczo"
  ]
  node [
    id 17
    label "apteczka"
  ]
  node [
    id 18
    label "tonizowa&#263;"
  ]
  node [
    id 19
    label "szprycowa&#263;"
  ]
  node [
    id 20
    label "naszprycowanie"
  ]
  node [
    id 21
    label "szprycowanie"
  ]
  node [
    id 22
    label "przepisanie"
  ]
  node [
    id 23
    label "tonizowanie"
  ]
  node [
    id 24
    label "medicine"
  ]
  node [
    id 25
    label "naszprycowa&#263;"
  ]
  node [
    id 26
    label "przepisa&#263;"
  ]
  node [
    id 27
    label "substancja"
  ]
  node [
    id 28
    label "szafka"
  ]
  node [
    id 29
    label "torba"
  ]
  node [
    id 30
    label "banda&#380;"
  ]
  node [
    id 31
    label "prestoplast"
  ]
  node [
    id 32
    label "zestaw"
  ]
  node [
    id 33
    label "przenikanie"
  ]
  node [
    id 34
    label "byt"
  ]
  node [
    id 35
    label "materia"
  ]
  node [
    id 36
    label "cz&#261;steczka"
  ]
  node [
    id 37
    label "temperatura_krytyczna"
  ]
  node [
    id 38
    label "przenika&#263;"
  ]
  node [
    id 39
    label "smolisty"
  ]
  node [
    id 40
    label "nafaszerowa&#263;"
  ]
  node [
    id 41
    label "narkotyk"
  ]
  node [
    id 42
    label "szko&#322;a"
  ]
  node [
    id 43
    label "przekazanie"
  ]
  node [
    id 44
    label "skopiowanie"
  ]
  node [
    id 45
    label "arrangement"
  ]
  node [
    id 46
    label "przeniesienie"
  ]
  node [
    id 47
    label "testament"
  ]
  node [
    id 48
    label "zadanie"
  ]
  node [
    id 49
    label "answer"
  ]
  node [
    id 50
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 51
    label "transcription"
  ]
  node [
    id 52
    label "klasa"
  ]
  node [
    id 53
    label "zalecenie"
  ]
  node [
    id 54
    label "wzmacnia&#263;"
  ]
  node [
    id 55
    label "wzmacnianie"
  ]
  node [
    id 56
    label "syringe"
  ]
  node [
    id 57
    label "faszerowa&#263;"
  ]
  node [
    id 58
    label "faszerowanie"
  ]
  node [
    id 59
    label "przekaza&#263;"
  ]
  node [
    id 60
    label "supply"
  ]
  node [
    id 61
    label "zaleci&#263;"
  ]
  node [
    id 62
    label "rewrite"
  ]
  node [
    id 63
    label "zrzec_si&#281;"
  ]
  node [
    id 64
    label "skopiowa&#263;"
  ]
  node [
    id 65
    label "przenie&#347;&#263;"
  ]
  node [
    id 66
    label "nafaszerowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
]
