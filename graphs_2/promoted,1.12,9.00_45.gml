graph [
  node [
    id 0
    label "wiek"
    origin "text"
  ]
  node [
    id 1
    label "lato"
    origin "text"
  ]
  node [
    id 2
    label "long_time"
  ]
  node [
    id 3
    label "choroba_wieku"
  ]
  node [
    id 4
    label "rok"
  ]
  node [
    id 5
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 6
    label "cecha"
  ]
  node [
    id 7
    label "chron"
  ]
  node [
    id 8
    label "period"
  ]
  node [
    id 9
    label "czas"
  ]
  node [
    id 10
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 11
    label "jednostka_geologiczna"
  ]
  node [
    id 12
    label "chronometria"
  ]
  node [
    id 13
    label "odczyt"
  ]
  node [
    id 14
    label "laba"
  ]
  node [
    id 15
    label "czasoprzestrze&#324;"
  ]
  node [
    id 16
    label "time_period"
  ]
  node [
    id 17
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 18
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 19
    label "Zeitgeist"
  ]
  node [
    id 20
    label "pochodzenie"
  ]
  node [
    id 21
    label "przep&#322;ywanie"
  ]
  node [
    id 22
    label "schy&#322;ek"
  ]
  node [
    id 23
    label "czwarty_wymiar"
  ]
  node [
    id 24
    label "kategoria_gramatyczna"
  ]
  node [
    id 25
    label "poprzedzi&#263;"
  ]
  node [
    id 26
    label "pogoda"
  ]
  node [
    id 27
    label "czasokres"
  ]
  node [
    id 28
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 29
    label "poprzedzenie"
  ]
  node [
    id 30
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 31
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 32
    label "dzieje"
  ]
  node [
    id 33
    label "zegar"
  ]
  node [
    id 34
    label "koniugacja"
  ]
  node [
    id 35
    label "trawi&#263;"
  ]
  node [
    id 36
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 37
    label "poprzedza&#263;"
  ]
  node [
    id 38
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 39
    label "trawienie"
  ]
  node [
    id 40
    label "chwila"
  ]
  node [
    id 41
    label "rachuba_czasu"
  ]
  node [
    id 42
    label "poprzedzanie"
  ]
  node [
    id 43
    label "okres_czasu"
  ]
  node [
    id 44
    label "odwlekanie_si&#281;"
  ]
  node [
    id 45
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 46
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 47
    label "pochodzi&#263;"
  ]
  node [
    id 48
    label "charakterystyka"
  ]
  node [
    id 49
    label "m&#322;ot"
  ]
  node [
    id 50
    label "marka"
  ]
  node [
    id 51
    label "pr&#243;ba"
  ]
  node [
    id 52
    label "attribute"
  ]
  node [
    id 53
    label "drzewo"
  ]
  node [
    id 54
    label "znak"
  ]
  node [
    id 55
    label "pora_roku"
  ]
  node [
    id 56
    label "kwarta&#322;"
  ]
  node [
    id 57
    label "jubileusz"
  ]
  node [
    id 58
    label "miesi&#261;c"
  ]
  node [
    id 59
    label "martwy_sezon"
  ]
  node [
    id 60
    label "kurs"
  ]
  node [
    id 61
    label "stulecie"
  ]
  node [
    id 62
    label "cykl_astronomiczny"
  ]
  node [
    id 63
    label "lata"
  ]
  node [
    id 64
    label "grupa"
  ]
  node [
    id 65
    label "p&#243;&#322;rocze"
  ]
  node [
    id 66
    label "kalendarz"
  ]
  node [
    id 67
    label "moment"
  ]
  node [
    id 68
    label "ciota"
  ]
  node [
    id 69
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 70
    label "flow"
  ]
  node [
    id 71
    label "proces_fizjologiczny"
  ]
  node [
    id 72
    label "choroba_przyrodzona"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 1
    target 55
  ]
]
