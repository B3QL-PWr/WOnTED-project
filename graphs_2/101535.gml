graph [
  node [
    id 0
    label "media"
    origin "text"
  ]
  node [
    id 1
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 2
    label "nowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "uzbrajanie"
  ]
  node [
    id 4
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 5
    label "mass-media"
  ]
  node [
    id 6
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 7
    label "medium"
  ]
  node [
    id 8
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 9
    label "przekazior"
  ]
  node [
    id 10
    label "cz&#322;owiek"
  ]
  node [
    id 11
    label "hipnoza"
  ]
  node [
    id 12
    label "&#347;rodek"
  ]
  node [
    id 13
    label "warunki"
  ]
  node [
    id 14
    label "publikator"
  ]
  node [
    id 15
    label "spirytysta"
  ]
  node [
    id 16
    label "strona"
  ]
  node [
    id 17
    label "otoczenie"
  ]
  node [
    id 18
    label "jasnowidz"
  ]
  node [
    id 19
    label "&#347;rodek_przekazu"
  ]
  node [
    id 20
    label "przeka&#378;nik"
  ]
  node [
    id 21
    label "arming"
  ]
  node [
    id 22
    label "wyposa&#380;anie"
  ]
  node [
    id 23
    label "dozbrojenie"
  ]
  node [
    id 24
    label "armament"
  ]
  node [
    id 25
    label "dozbrajanie"
  ]
  node [
    id 26
    label "instalacja"
  ]
  node [
    id 27
    label "montowanie"
  ]
  node [
    id 28
    label "publiczny"
  ]
  node [
    id 29
    label "niepubliczny"
  ]
  node [
    id 30
    label "spo&#322;ecznie"
  ]
  node [
    id 31
    label "publicznie"
  ]
  node [
    id 32
    label "upublicznienie"
  ]
  node [
    id 33
    label "upublicznianie"
  ]
  node [
    id 34
    label "jawny"
  ]
  node [
    id 35
    label "wiek"
  ]
  node [
    id 36
    label "urozmaicenie"
  ]
  node [
    id 37
    label "newness"
  ]
  node [
    id 38
    label "co&#347;"
  ]
  node [
    id 39
    label "differentiation"
  ]
  node [
    id 40
    label "podzielenie"
  ]
  node [
    id 41
    label "nadanie"
  ]
  node [
    id 42
    label "long_time"
  ]
  node [
    id 43
    label "choroba_wieku"
  ]
  node [
    id 44
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 45
    label "rok"
  ]
  node [
    id 46
    label "cecha"
  ]
  node [
    id 47
    label "chron"
  ]
  node [
    id 48
    label "period"
  ]
  node [
    id 49
    label "czas"
  ]
  node [
    id 50
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 51
    label "jednostka_geologiczna"
  ]
  node [
    id 52
    label "Scott"
  ]
  node [
    id 53
    label "Berkun"
  ]
  node [
    id 54
    label "Calling"
  ]
  node [
    id 55
    label "Bullshit"
  ]
  node [
    id 56
    label "on"
  ]
  node [
    id 57
    label "Social"
  ]
  node [
    id 58
    label "ojciec"
  ]
  node [
    id 59
    label "&#8217;"
  ]
  node [
    id 60
    label "Reilly"
  ]
  node [
    id 61
    label "radar"
  ]
  node [
    id 62
    label "Joshua"
  ]
  node [
    id 63
    label "Michele"
  ]
  node [
    id 64
    label "Ross"
  ]
  node [
    id 65
    label "Andrew"
  ]
  node [
    id 66
    label "Keena"
  ]
  node [
    id 67
    label "weba"
  ]
  node [
    id 68
    label "2"
  ]
  node [
    id 69
    label "0"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 58
    target 61
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 69
  ]
  edge [
    source 68
    target 69
  ]
]
