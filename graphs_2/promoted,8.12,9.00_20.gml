graph [
  node [
    id 0
    label "wymiana"
    origin "text"
  ]
  node [
    id 1
    label "ogie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "wzajemny"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "zast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 6
    label "palec"
    origin "text"
  ]
  node [
    id 7
    label "implicite"
  ]
  node [
    id 8
    label "deal"
  ]
  node [
    id 9
    label "zjawisko_fonetyczne"
  ]
  node [
    id 10
    label "exchange"
  ]
  node [
    id 11
    label "wydarzenie"
  ]
  node [
    id 12
    label "explicite"
  ]
  node [
    id 13
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 14
    label "zamiana"
  ]
  node [
    id 15
    label "szachy"
  ]
  node [
    id 16
    label "ruch"
  ]
  node [
    id 17
    label "handel"
  ]
  node [
    id 18
    label "business"
  ]
  node [
    id 19
    label "dzia&#322;alno&#347;&#263;_gospodarcza"
  ]
  node [
    id 20
    label "komercja"
  ]
  node [
    id 21
    label "popyt"
  ]
  node [
    id 22
    label "przebiec"
  ]
  node [
    id 23
    label "charakter"
  ]
  node [
    id 24
    label "czynno&#347;&#263;"
  ]
  node [
    id 25
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 26
    label "motyw"
  ]
  node [
    id 27
    label "przebiegni&#281;cie"
  ]
  node [
    id 28
    label "fabu&#322;a"
  ]
  node [
    id 29
    label "zmianka"
  ]
  node [
    id 30
    label "odmienianie"
  ]
  node [
    id 31
    label "zmiana"
  ]
  node [
    id 32
    label "mechanika"
  ]
  node [
    id 33
    label "utrzymywanie"
  ]
  node [
    id 34
    label "move"
  ]
  node [
    id 35
    label "poruszenie"
  ]
  node [
    id 36
    label "movement"
  ]
  node [
    id 37
    label "myk"
  ]
  node [
    id 38
    label "utrzyma&#263;"
  ]
  node [
    id 39
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 40
    label "zjawisko"
  ]
  node [
    id 41
    label "utrzymanie"
  ]
  node [
    id 42
    label "travel"
  ]
  node [
    id 43
    label "kanciasty"
  ]
  node [
    id 44
    label "commercial_enterprise"
  ]
  node [
    id 45
    label "model"
  ]
  node [
    id 46
    label "strumie&#324;"
  ]
  node [
    id 47
    label "proces"
  ]
  node [
    id 48
    label "aktywno&#347;&#263;"
  ]
  node [
    id 49
    label "kr&#243;tki"
  ]
  node [
    id 50
    label "taktyka"
  ]
  node [
    id 51
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 52
    label "apraksja"
  ]
  node [
    id 53
    label "natural_process"
  ]
  node [
    id 54
    label "utrzymywa&#263;"
  ]
  node [
    id 55
    label "d&#322;ugi"
  ]
  node [
    id 56
    label "dyssypacja_energii"
  ]
  node [
    id 57
    label "tumult"
  ]
  node [
    id 58
    label "stopek"
  ]
  node [
    id 59
    label "manewr"
  ]
  node [
    id 60
    label "lokomocja"
  ]
  node [
    id 61
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 62
    label "komunikacja"
  ]
  node [
    id 63
    label "drift"
  ]
  node [
    id 64
    label "cover"
  ]
  node [
    id 65
    label "bezpo&#347;rednio"
  ]
  node [
    id 66
    label "przekaz"
  ]
  node [
    id 67
    label "po&#347;rednio"
  ]
  node [
    id 68
    label "szachownica"
  ]
  node [
    id 69
    label "przes&#322;ona"
  ]
  node [
    id 70
    label "p&#243;&#322;ruch"
  ]
  node [
    id 71
    label "linia_przemiany"
  ]
  node [
    id 72
    label "szach"
  ]
  node [
    id 73
    label "niedoczas"
  ]
  node [
    id 74
    label "zegar_szachowy"
  ]
  node [
    id 75
    label "sport_umys&#322;owy"
  ]
  node [
    id 76
    label "roszada"
  ]
  node [
    id 77
    label "promocja"
  ]
  node [
    id 78
    label "tempo"
  ]
  node [
    id 79
    label "bicie_w_przelocie"
  ]
  node [
    id 80
    label "gra_planszowa"
  ]
  node [
    id 81
    label "dual"
  ]
  node [
    id 82
    label "mat"
  ]
  node [
    id 83
    label "energia"
  ]
  node [
    id 84
    label "rumieniec"
  ]
  node [
    id 85
    label "nowotw&#243;r_niez&#322;o&#347;liwy"
  ]
  node [
    id 86
    label "sk&#243;ra"
  ]
  node [
    id 87
    label "hell"
  ]
  node [
    id 88
    label "rozpalenie"
  ]
  node [
    id 89
    label "co&#347;"
  ]
  node [
    id 90
    label "iskra"
  ]
  node [
    id 91
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 92
    label "light"
  ]
  node [
    id 93
    label "rozpalanie"
  ]
  node [
    id 94
    label "deszcz"
  ]
  node [
    id 95
    label "akcesorium"
  ]
  node [
    id 96
    label "zapalenie"
  ]
  node [
    id 97
    label "pali&#263;_si&#281;"
  ]
  node [
    id 98
    label "&#347;wiat&#322;o"
  ]
  node [
    id 99
    label "zarzewie"
  ]
  node [
    id 100
    label "ciep&#322;o"
  ]
  node [
    id 101
    label "znami&#281;"
  ]
  node [
    id 102
    label "war"
  ]
  node [
    id 103
    label "kolor"
  ]
  node [
    id 104
    label "przyp&#322;yw"
  ]
  node [
    id 105
    label "p&#322;omie&#324;"
  ]
  node [
    id 106
    label "palenie_si&#281;"
  ]
  node [
    id 107
    label "&#380;ywio&#322;"
  ]
  node [
    id 108
    label "incandescence"
  ]
  node [
    id 109
    label "atak"
  ]
  node [
    id 110
    label "palenie"
  ]
  node [
    id 111
    label "fire"
  ]
  node [
    id 112
    label "ardor"
  ]
  node [
    id 113
    label "thing"
  ]
  node [
    id 114
    label "cosik"
  ]
  node [
    id 115
    label "przy&#263;miewa&#263;"
  ]
  node [
    id 116
    label "&#347;wieci&#263;"
  ]
  node [
    id 117
    label "odst&#281;p"
  ]
  node [
    id 118
    label "wpadni&#281;cie"
  ]
  node [
    id 119
    label "interpretacja"
  ]
  node [
    id 120
    label "cecha"
  ]
  node [
    id 121
    label "fotokataliza"
  ]
  node [
    id 122
    label "promieniowanie_elektromagnetyczne"
  ]
  node [
    id 123
    label "wpa&#347;&#263;"
  ]
  node [
    id 124
    label "rzuca&#263;"
  ]
  node [
    id 125
    label "obsadnik"
  ]
  node [
    id 126
    label "promieniowanie_optyczne"
  ]
  node [
    id 127
    label "lampa"
  ]
  node [
    id 128
    label "ja&#347;nia"
  ]
  node [
    id 129
    label "przeb&#322;yskiwa&#263;"
  ]
  node [
    id 130
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 131
    label "wpada&#263;"
  ]
  node [
    id 132
    label "rzuci&#263;"
  ]
  node [
    id 133
    label "o&#347;wietlenie"
  ]
  node [
    id 134
    label "punkt_widzenia"
  ]
  node [
    id 135
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 136
    label "przy&#263;mienie"
  ]
  node [
    id 137
    label "instalacja"
  ]
  node [
    id 138
    label "&#347;wiecenie"
  ]
  node [
    id 139
    label "radiance"
  ]
  node [
    id 140
    label "przeb&#322;yskiwanie"
  ]
  node [
    id 141
    label "przy&#263;mi&#263;"
  ]
  node [
    id 142
    label "b&#322;ysk"
  ]
  node [
    id 143
    label "&#347;wiat&#322;y"
  ]
  node [
    id 144
    label "promie&#324;"
  ]
  node [
    id 145
    label "m&#261;drze"
  ]
  node [
    id 146
    label "rozja&#347;nia&#263;"
  ]
  node [
    id 147
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 148
    label "lighting"
  ]
  node [
    id 149
    label "lighter"
  ]
  node [
    id 150
    label "rzucenie"
  ]
  node [
    id 151
    label "plama"
  ]
  node [
    id 152
    label "&#347;rednica"
  ]
  node [
    id 153
    label "wpadanie"
  ]
  node [
    id 154
    label "przy&#263;miewanie"
  ]
  node [
    id 155
    label "rzucanie"
  ]
  node [
    id 156
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 157
    label "emitowa&#263;"
  ]
  node [
    id 158
    label "egzergia"
  ]
  node [
    id 159
    label "kwant_energii"
  ]
  node [
    id 160
    label "szwung"
  ]
  node [
    id 161
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 162
    label "power"
  ]
  node [
    id 163
    label "emitowanie"
  ]
  node [
    id 164
    label "energy"
  ]
  node [
    id 165
    label "emocja"
  ]
  node [
    id 166
    label "geotermia"
  ]
  node [
    id 167
    label "przyjemnie"
  ]
  node [
    id 168
    label "pogoda"
  ]
  node [
    id 169
    label "emisyjno&#347;&#263;"
  ]
  node [
    id 170
    label "temperatura"
  ]
  node [
    id 171
    label "mi&#322;o"
  ]
  node [
    id 172
    label "ciep&#322;y"
  ]
  node [
    id 173
    label "heat"
  ]
  node [
    id 174
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 175
    label "walka"
  ]
  node [
    id 176
    label "liga"
  ]
  node [
    id 177
    label "oznaka"
  ]
  node [
    id 178
    label "pogorszenie"
  ]
  node [
    id 179
    label "przemoc"
  ]
  node [
    id 180
    label "krytyka"
  ]
  node [
    id 181
    label "bat"
  ]
  node [
    id 182
    label "kaszel"
  ]
  node [
    id 183
    label "fit"
  ]
  node [
    id 184
    label "spasm"
  ]
  node [
    id 185
    label "zagrywka"
  ]
  node [
    id 186
    label "wypowied&#378;"
  ]
  node [
    id 187
    label "&#380;&#261;danie"
  ]
  node [
    id 188
    label "ofensywa"
  ]
  node [
    id 189
    label "stroke"
  ]
  node [
    id 190
    label "pozycja"
  ]
  node [
    id 191
    label "knock"
  ]
  node [
    id 192
    label "atrakcyjno&#347;&#263;"
  ]
  node [
    id 193
    label "wstyd"
  ]
  node [
    id 194
    label "przebarwienie"
  ]
  node [
    id 195
    label "hot_flash"
  ]
  node [
    id 196
    label "reakcja"
  ]
  node [
    id 197
    label "stamp"
  ]
  node [
    id 198
    label "wygl&#261;d"
  ]
  node [
    id 199
    label "s&#322;upek"
  ]
  node [
    id 200
    label "okrytonasienne"
  ]
  node [
    id 201
    label "py&#322;ek"
  ]
  node [
    id 202
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 203
    label "subject"
  ]
  node [
    id 204
    label "kamena"
  ]
  node [
    id 205
    label "czynnik"
  ]
  node [
    id 206
    label "&#347;wiadectwo"
  ]
  node [
    id 207
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 208
    label "ciek_wodny"
  ]
  node [
    id 209
    label "matuszka"
  ]
  node [
    id 210
    label "pocz&#261;tek"
  ]
  node [
    id 211
    label "geneza"
  ]
  node [
    id 212
    label "rezultat"
  ]
  node [
    id 213
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 214
    label "bra&#263;_si&#281;"
  ]
  node [
    id 215
    label "przyczyna"
  ]
  node [
    id 216
    label "poci&#261;ganie"
  ]
  node [
    id 217
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 218
    label "materia"
  ]
  node [
    id 219
    label "zajawka"
  ]
  node [
    id 220
    label "class"
  ]
  node [
    id 221
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 222
    label "feblik"
  ]
  node [
    id 223
    label "wedrze&#263;_si&#281;"
  ]
  node [
    id 224
    label "wdarcie_si&#281;"
  ]
  node [
    id 225
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 226
    label "wdzieranie_si&#281;"
  ]
  node [
    id 227
    label "wdziera&#263;_si&#281;"
  ]
  node [
    id 228
    label "tendency"
  ]
  node [
    id 229
    label "grupa"
  ]
  node [
    id 230
    label "huczek"
  ]
  node [
    id 231
    label "flow"
  ]
  node [
    id 232
    label "p&#322;yw"
  ]
  node [
    id 233
    label "wzrost"
  ]
  node [
    id 234
    label "akcesoria"
  ]
  node [
    id 235
    label "wytw&#243;r"
  ]
  node [
    id 236
    label "accessory"
  ]
  node [
    id 237
    label "przyrz&#261;d"
  ]
  node [
    id 238
    label "liczba_kwantowa"
  ]
  node [
    id 239
    label "poker"
  ]
  node [
    id 240
    label "ubarwienie"
  ]
  node [
    id 241
    label "blakn&#261;&#263;"
  ]
  node [
    id 242
    label "struktura"
  ]
  node [
    id 243
    label "prze&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 244
    label "zblakni&#281;cie"
  ]
  node [
    id 245
    label "prze&#322;amywa&#263;"
  ]
  node [
    id 246
    label "prze&#322;amywanie_si&#281;"
  ]
  node [
    id 247
    label "prze&#322;ama&#263;_si&#281;"
  ]
  node [
    id 248
    label "prze&#322;amanie"
  ]
  node [
    id 249
    label "prze&#322;amywanie"
  ]
  node [
    id 250
    label "prze&#322;ama&#263;"
  ]
  node [
    id 251
    label "zblakn&#261;&#263;"
  ]
  node [
    id 252
    label "symbol"
  ]
  node [
    id 253
    label "blakni&#281;cie"
  ]
  node [
    id 254
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 255
    label "rain"
  ]
  node [
    id 256
    label "opad"
  ]
  node [
    id 257
    label "mn&#243;stwo"
  ]
  node [
    id 258
    label "burza"
  ]
  node [
    id 259
    label "szczupak"
  ]
  node [
    id 260
    label "coating"
  ]
  node [
    id 261
    label "krupon"
  ]
  node [
    id 262
    label "harleyowiec"
  ]
  node [
    id 263
    label "tkanka_podsk&#243;rna"
  ]
  node [
    id 264
    label "kurtka"
  ]
  node [
    id 265
    label "metal"
  ]
  node [
    id 266
    label "p&#322;aszcz"
  ]
  node [
    id 267
    label "&#322;upa"
  ]
  node [
    id 268
    label "wyprze&#263;"
  ]
  node [
    id 269
    label "okrywa"
  ]
  node [
    id 270
    label "gruczo&#322;_kuprowy"
  ]
  node [
    id 271
    label "&#380;ycie"
  ]
  node [
    id 272
    label "gruczo&#322;_potowy"
  ]
  node [
    id 273
    label "lico"
  ]
  node [
    id 274
    label "wi&#243;rkownik"
  ]
  node [
    id 275
    label "mizdra"
  ]
  node [
    id 276
    label "dupa"
  ]
  node [
    id 277
    label "rockers"
  ]
  node [
    id 278
    label "&#322;uskowato&#347;&#263;"
  ]
  node [
    id 279
    label "surowiec"
  ]
  node [
    id 280
    label "czerniak_z&#322;o&#347;liwy"
  ]
  node [
    id 281
    label "gruczo&#322;_&#322;ojowy"
  ]
  node [
    id 282
    label "organ"
  ]
  node [
    id 283
    label "pow&#322;oka"
  ]
  node [
    id 284
    label "zdrowie"
  ]
  node [
    id 285
    label "wyprawa"
  ]
  node [
    id 286
    label "sk&#243;ra_w&#322;a&#347;ciwa"
  ]
  node [
    id 287
    label "hardrockowiec"
  ]
  node [
    id 288
    label "nask&#243;rek"
  ]
  node [
    id 289
    label "gestapowiec"
  ]
  node [
    id 290
    label "funkcja"
  ]
  node [
    id 291
    label "cia&#322;o"
  ]
  node [
    id 292
    label "shell"
  ]
  node [
    id 293
    label "zajaranie"
  ]
  node [
    id 294
    label "roz&#380;arzenie"
  ]
  node [
    id 295
    label "fajka"
  ]
  node [
    id 296
    label "pozapalanie"
  ]
  node [
    id 297
    label "zaburzenie"
  ]
  node [
    id 298
    label "papieros"
  ]
  node [
    id 299
    label "rozja&#347;nienie"
  ]
  node [
    id 300
    label "ignition"
  ]
  node [
    id 301
    label "rozdra&#380;nianie"
  ]
  node [
    id 302
    label "rozdra&#380;ni&#263;"
  ]
  node [
    id 303
    label "rozdra&#380;nia&#263;"
  ]
  node [
    id 304
    label "odpalenie"
  ]
  node [
    id 305
    label "rozdra&#380;nienie"
  ]
  node [
    id 306
    label "w&#322;&#261;czenie"
  ]
  node [
    id 307
    label "zrobienie"
  ]
  node [
    id 308
    label "wrz&#261;tek"
  ]
  node [
    id 309
    label "gor&#261;co"
  ]
  node [
    id 310
    label "kszta&#322;t"
  ]
  node [
    id 311
    label "wyraz"
  ]
  node [
    id 312
    label "ostentation"
  ]
  node [
    id 313
    label "freshness"
  ]
  node [
    id 314
    label "cz&#322;owiek"
  ]
  node [
    id 315
    label "flicker"
  ]
  node [
    id 316
    label "&#380;agiew"
  ]
  node [
    id 317
    label "odrobina"
  ]
  node [
    id 318
    label "odblask"
  ]
  node [
    id 319
    label "glint"
  ]
  node [
    id 320
    label "blask"
  ]
  node [
    id 321
    label "discharge"
  ]
  node [
    id 322
    label "dopalenie"
  ]
  node [
    id 323
    label "powodowanie"
  ]
  node [
    id 324
    label "burning"
  ]
  node [
    id 325
    label "na&#322;&#243;g"
  ]
  node [
    id 326
    label "emergency"
  ]
  node [
    id 327
    label "burn"
  ]
  node [
    id 328
    label "dokuczanie"
  ]
  node [
    id 329
    label "cygaro"
  ]
  node [
    id 330
    label "napalenie"
  ]
  node [
    id 331
    label "robienie"
  ]
  node [
    id 332
    label "podpalanie"
  ]
  node [
    id 333
    label "pykni&#281;cie"
  ]
  node [
    id 334
    label "zu&#380;ywanie"
  ]
  node [
    id 335
    label "wypalanie"
  ]
  node [
    id 336
    label "podtrzymywanie"
  ]
  node [
    id 337
    label "strzelanie"
  ]
  node [
    id 338
    label "dra&#380;nienie"
  ]
  node [
    id 339
    label "kadzenie"
  ]
  node [
    id 340
    label "wypalenie"
  ]
  node [
    id 341
    label "przygotowywanie"
  ]
  node [
    id 342
    label "dowcip"
  ]
  node [
    id 343
    label "popalenie"
  ]
  node [
    id 344
    label "niszczenie"
  ]
  node [
    id 345
    label "grzanie"
  ]
  node [
    id 346
    label "paliwo"
  ]
  node [
    id 347
    label "bolenie"
  ]
  node [
    id 348
    label "incineration"
  ]
  node [
    id 349
    label "psucie"
  ]
  node [
    id 350
    label "jaranie"
  ]
  node [
    id 351
    label "rozja&#347;nienie_si&#281;"
  ]
  node [
    id 352
    label "rozp&#322;omienienie_si&#281;"
  ]
  node [
    id 353
    label "wzbudzenie"
  ]
  node [
    id 354
    label "podpalenie"
  ]
  node [
    id 355
    label "pobudzenie"
  ]
  node [
    id 356
    label "roz&#380;arzenie_si&#281;"
  ]
  node [
    id 357
    label "spowodowanie"
  ]
  node [
    id 358
    label "zagrzanie"
  ]
  node [
    id 359
    label "waste"
  ]
  node [
    id 360
    label "inflammation"
  ]
  node [
    id 361
    label "o&#347;wietlanie"
  ]
  node [
    id 362
    label "distraction"
  ]
  node [
    id 363
    label "rozja&#347;nianie_si&#281;"
  ]
  node [
    id 364
    label "roz&#380;arzanie_si&#281;"
  ]
  node [
    id 365
    label "pobudzanie"
  ]
  node [
    id 366
    label "rozp&#322;omienianie_si&#281;"
  ]
  node [
    id 367
    label "wzbudzanie"
  ]
  node [
    id 368
    label "brand"
  ]
  node [
    id 369
    label "podpa&#322;ka"
  ]
  node [
    id 370
    label "wzajemnie"
  ]
  node [
    id 371
    label "zobop&#243;lny"
  ]
  node [
    id 372
    label "wsp&#243;lny"
  ]
  node [
    id 373
    label "zajemny"
  ]
  node [
    id 374
    label "spolny"
  ]
  node [
    id 375
    label "wsp&#243;lnie"
  ]
  node [
    id 376
    label "sp&#243;lny"
  ]
  node [
    id 377
    label "jeden"
  ]
  node [
    id 378
    label "uwsp&#243;lnienie"
  ]
  node [
    id 379
    label "uwsp&#243;lnianie"
  ]
  node [
    id 380
    label "decydowa&#263;"
  ]
  node [
    id 381
    label "zmienia&#263;"
  ]
  node [
    id 382
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 383
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 384
    label "decide"
  ]
  node [
    id 385
    label "klasyfikator"
  ]
  node [
    id 386
    label "mean"
  ]
  node [
    id 387
    label "traci&#263;"
  ]
  node [
    id 388
    label "alternate"
  ]
  node [
    id 389
    label "change"
  ]
  node [
    id 390
    label "reengineering"
  ]
  node [
    id 391
    label "sprawia&#263;"
  ]
  node [
    id 392
    label "zyskiwa&#263;"
  ]
  node [
    id 393
    label "przechodzi&#263;"
  ]
  node [
    id 394
    label "dziewczynka"
  ]
  node [
    id 395
    label "dziewczyna"
  ]
  node [
    id 396
    label "prostytutka"
  ]
  node [
    id 397
    label "dziecko"
  ]
  node [
    id 398
    label "potomkini"
  ]
  node [
    id 399
    label "dziewka"
  ]
  node [
    id 400
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 401
    label "sikorka"
  ]
  node [
    id 402
    label "kora"
  ]
  node [
    id 403
    label "dziewcz&#281;"
  ]
  node [
    id 404
    label "dziecina"
  ]
  node [
    id 405
    label "m&#322;&#243;dka"
  ]
  node [
    id 406
    label "sympatia"
  ]
  node [
    id 407
    label "dziunia"
  ]
  node [
    id 408
    label "dziewczynina"
  ]
  node [
    id 409
    label "partnerka"
  ]
  node [
    id 410
    label "siksa"
  ]
  node [
    id 411
    label "dziewoja"
  ]
  node [
    id 412
    label "polidaktylia"
  ]
  node [
    id 413
    label "dzia&#322;anie"
  ]
  node [
    id 414
    label "koniuszek_palca"
  ]
  node [
    id 415
    label "paznokie&#263;"
  ]
  node [
    id 416
    label "d&#322;o&#324;"
  ]
  node [
    id 417
    label "pazur"
  ]
  node [
    id 418
    label "element_anatomiczny"
  ]
  node [
    id 419
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 420
    label "poduszka"
  ]
  node [
    id 421
    label "zap&#322;on"
  ]
  node [
    id 422
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 423
    label "knykie&#263;"
  ]
  node [
    id 424
    label "palpacja"
  ]
  node [
    id 425
    label "infimum"
  ]
  node [
    id 426
    label "liczenie"
  ]
  node [
    id 427
    label "skutek"
  ]
  node [
    id 428
    label "podzia&#322;anie"
  ]
  node [
    id 429
    label "supremum"
  ]
  node [
    id 430
    label "kampania"
  ]
  node [
    id 431
    label "uruchamianie"
  ]
  node [
    id 432
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 433
    label "operacja"
  ]
  node [
    id 434
    label "jednostka"
  ]
  node [
    id 435
    label "hipnotyzowanie"
  ]
  node [
    id 436
    label "uruchomienie"
  ]
  node [
    id 437
    label "nakr&#281;canie"
  ]
  node [
    id 438
    label "matematyka"
  ]
  node [
    id 439
    label "reakcja_chemiczna"
  ]
  node [
    id 440
    label "tr&#243;jstronny"
  ]
  node [
    id 441
    label "nakr&#281;cenie"
  ]
  node [
    id 442
    label "zatrzymanie"
  ]
  node [
    id 443
    label "wp&#322;yw"
  ]
  node [
    id 444
    label "rzut"
  ]
  node [
    id 445
    label "w&#322;&#261;czanie"
  ]
  node [
    id 446
    label "liczy&#263;"
  ]
  node [
    id 447
    label "operation"
  ]
  node [
    id 448
    label "dzianie_si&#281;"
  ]
  node [
    id 449
    label "zadzia&#322;anie"
  ]
  node [
    id 450
    label "priorytet"
  ]
  node [
    id 451
    label "bycie"
  ]
  node [
    id 452
    label "kres"
  ]
  node [
    id 453
    label "rozpocz&#281;cie"
  ]
  node [
    id 454
    label "docieranie"
  ]
  node [
    id 455
    label "czynny"
  ]
  node [
    id 456
    label "impact"
  ]
  node [
    id 457
    label "oferta"
  ]
  node [
    id 458
    label "zako&#324;czenie"
  ]
  node [
    id 459
    label "act"
  ]
  node [
    id 460
    label "Rzym_Zachodni"
  ]
  node [
    id 461
    label "whole"
  ]
  node [
    id 462
    label "ilo&#347;&#263;"
  ]
  node [
    id 463
    label "element"
  ]
  node [
    id 464
    label "Rzym_Wschodni"
  ]
  node [
    id 465
    label "urz&#261;dzenie"
  ]
  node [
    id 466
    label "&#380;a&#322;oba"
  ]
  node [
    id 467
    label "ob&#322;&#261;czek"
  ]
  node [
    id 468
    label "linie_Beau"
  ]
  node [
    id 469
    label "piecz&#261;tka"
  ]
  node [
    id 470
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 471
    label "m&#243;zg"
  ]
  node [
    id 472
    label "przedmiot"
  ]
  node [
    id 473
    label "po&#347;ciel"
  ]
  node [
    id 474
    label "podpora"
  ]
  node [
    id 475
    label "wyko&#324;czenie"
  ]
  node [
    id 476
    label "wype&#322;niacz"
  ]
  node [
    id 477
    label "fotel"
  ]
  node [
    id 478
    label "&#322;apa"
  ]
  node [
    id 479
    label "kanapa"
  ]
  node [
    id 480
    label "z&#322;&#261;czenie"
  ]
  node [
    id 481
    label "charakterno&#347;&#263;"
  ]
  node [
    id 482
    label "p&#322;omyk"
  ]
  node [
    id 483
    label "krzy&#380;"
  ]
  node [
    id 484
    label "linia_mi&#322;o&#347;ci"
  ]
  node [
    id 485
    label "wyklepanie"
  ]
  node [
    id 486
    label "chiromancja"
  ]
  node [
    id 487
    label "klepanie"
  ]
  node [
    id 488
    label "wyklepa&#263;"
  ]
  node [
    id 489
    label "nadgarstek"
  ]
  node [
    id 490
    label "szeroko&#347;&#263;_d&#322;oni"
  ]
  node [
    id 491
    label "dotykanie"
  ]
  node [
    id 492
    label "graba"
  ]
  node [
    id 493
    label "klepa&#263;"
  ]
  node [
    id 494
    label "r&#261;czyna"
  ]
  node [
    id 495
    label "cmoknonsens"
  ]
  node [
    id 496
    label "chwyta&#263;"
  ]
  node [
    id 497
    label "r&#281;ka"
  ]
  node [
    id 498
    label "chwytanie"
  ]
  node [
    id 499
    label "linia_&#380;ycia"
  ]
  node [
    id 500
    label "hasta"
  ]
  node [
    id 501
    label "linia_rozumu"
  ]
  node [
    id 502
    label "dotyka&#263;"
  ]
  node [
    id 503
    label "po&#380;ar"
  ]
  node [
    id 504
    label "kopu&#322;ka"
  ]
  node [
    id 505
    label "silnik_spalinowy"
  ]
  node [
    id 506
    label "palpation"
  ]
  node [
    id 507
    label "badanie"
  ]
  node [
    id 508
    label "zesp&#243;&#322;_Greiga"
  ]
  node [
    id 509
    label "zesp&#243;&#322;_ustno-twarzowo-palcowy_typu_II"
  ]
  node [
    id 510
    label "wada_wrodzona"
  ]
  node [
    id 511
    label "Przemys&#322;aw"
  ]
  node [
    id 512
    label "W&#243;jtowicz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 511
    target 512
  ]
]
