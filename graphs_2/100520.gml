graph [
  node [
    id 0
    label "marian"
    origin "text"
  ]
  node [
    id 1
    label "subocz"
    origin "text"
  ]
  node [
    id 2
    label "Marian"
  ]
  node [
    id 3
    label "Subocz"
  ]
  node [
    id 4
    label "diecezja"
  ]
  node [
    id 5
    label "koszali&#324;sko"
  ]
  node [
    id 6
    label "ko&#322;obrzeski"
  ]
  node [
    id 7
    label "Caritas"
  ]
  node [
    id 8
    label "polski"
  ]
  node [
    id 9
    label "Ignacy"
  ]
  node [
    id 10
    label "je&#380;"
  ]
  node [
    id 11
    label "parafia"
  ]
  node [
    id 12
    label "mariacki"
  ]
  node [
    id 13
    label "papieski"
  ]
  node [
    id 14
    label "uniwersytet"
  ]
  node [
    id 15
    label "salezja&#324;ski"
  ]
  node [
    id 16
    label "wysoki"
  ]
  node [
    id 17
    label "seminarium"
  ]
  node [
    id 18
    label "duchowny"
  ]
  node [
    id 19
    label "IV"
  ]
  node [
    id 20
    label "pielgrzymka"
  ]
  node [
    id 21
    label "do"
  ]
  node [
    id 22
    label "ojczyzna"
  ]
  node [
    id 23
    label "Jan"
  ]
  node [
    id 24
    label "pawe&#322;"
  ]
  node [
    id 25
    label "ii"
  ]
  node [
    id 26
    label "komisja"
  ]
  node [
    id 27
    label "charytatywny"
  ]
  node [
    id 28
    label "konferencja"
  ]
  node [
    id 29
    label "episkopat"
  ]
  node [
    id 30
    label "Czes&#322;awa"
  ]
  node [
    id 31
    label "domino"
  ]
  node [
    id 32
    label "wigilijny"
  ]
  node [
    id 33
    label "dzie&#322;o"
  ]
  node [
    id 34
    label "pomoc"
  ]
  node [
    id 35
    label "dziecko"
  ]
  node [
    id 36
    label "b&#243;g"
  ]
  node [
    id 37
    label "narodzi&#263;"
  ]
  node [
    id 38
    label "akademia"
  ]
  node [
    id 39
    label "teologia"
  ]
  node [
    id 40
    label "katolicki"
  ]
  node [
    id 41
    label "Tadeusz"
  ]
  node [
    id 42
    label "Pieronek"
  ]
  node [
    id 43
    label "VI"
  ]
  node [
    id 44
    label "ojciec"
  ]
  node [
    id 45
    label "&#347;wi&#281;ty"
  ]
  node [
    id 46
    label "unia"
  ]
  node [
    id 47
    label "europejski"
  ]
  node [
    id 48
    label "wsp&#243;lnota"
  ]
  node [
    id 49
    label "Go&#322;&#281;biewski"
  ]
  node [
    id 50
    label "Maksymilian"
  ]
  node [
    id 51
    label "Maria"
  ]
  node [
    id 52
    label "Kolbe"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 43
  ]
  edge [
    source 20
    target 44
  ]
  edge [
    source 20
    target 45
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 43
  ]
  edge [
    source 21
    target 44
  ]
  edge [
    source 21
    target 45
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 43
  ]
  edge [
    source 23
    target 44
  ]
  edge [
    source 23
    target 45
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 43
  ]
  edge [
    source 24
    target 44
  ]
  edge [
    source 24
    target 45
  ]
  edge [
    source 25
    target 43
  ]
  edge [
    source 25
    target 44
  ]
  edge [
    source 25
    target 45
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 51
    target 52
  ]
]
