graph [
  node [
    id 0
    label "letni"
    origin "text"
  ]
  node [
    id 1
    label "ch&#322;opiec"
    origin "text"
  ]
  node [
    id 2
    label "powiesi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "gdy"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "gn&#281;bi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "przez"
    origin "text"
  ]
  node [
    id 8
    label "azjatycki"
    origin "text"
  ]
  node [
    id 9
    label "gang"
    origin "text"
  ]
  node [
    id 10
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 11
    label "latowy"
  ]
  node [
    id 12
    label "typowy"
  ]
  node [
    id 13
    label "weso&#322;y"
  ]
  node [
    id 14
    label "s&#322;oneczny"
  ]
  node [
    id 15
    label "sezonowy"
  ]
  node [
    id 16
    label "ciep&#322;y"
  ]
  node [
    id 17
    label "letnio"
  ]
  node [
    id 18
    label "oboj&#281;tny"
  ]
  node [
    id 19
    label "nijaki"
  ]
  node [
    id 20
    label "nijak"
  ]
  node [
    id 21
    label "niezabawny"
  ]
  node [
    id 22
    label "&#380;aden"
  ]
  node [
    id 23
    label "zwyczajny"
  ]
  node [
    id 24
    label "poszarzenie"
  ]
  node [
    id 25
    label "neutralny"
  ]
  node [
    id 26
    label "szarzenie"
  ]
  node [
    id 27
    label "bezbarwnie"
  ]
  node [
    id 28
    label "nieciekawy"
  ]
  node [
    id 29
    label "czasowy"
  ]
  node [
    id 30
    label "sezonowo"
  ]
  node [
    id 31
    label "zoboj&#281;tnienie"
  ]
  node [
    id 32
    label "nieszkodliwy"
  ]
  node [
    id 33
    label "&#347;ni&#281;ty"
  ]
  node [
    id 34
    label "oboj&#281;tnie"
  ]
  node [
    id 35
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 36
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 37
    label "niewa&#380;ny"
  ]
  node [
    id 38
    label "neutralizowanie"
  ]
  node [
    id 39
    label "bierny"
  ]
  node [
    id 40
    label "zneutralizowanie"
  ]
  node [
    id 41
    label "pijany"
  ]
  node [
    id 42
    label "weso&#322;o"
  ]
  node [
    id 43
    label "pozytywny"
  ]
  node [
    id 44
    label "beztroski"
  ]
  node [
    id 45
    label "dobry"
  ]
  node [
    id 46
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 47
    label "typowo"
  ]
  node [
    id 48
    label "cz&#281;sty"
  ]
  node [
    id 49
    label "zwyk&#322;y"
  ]
  node [
    id 50
    label "mi&#322;y"
  ]
  node [
    id 51
    label "ocieplanie_si&#281;"
  ]
  node [
    id 52
    label "ocieplanie"
  ]
  node [
    id 53
    label "grzanie"
  ]
  node [
    id 54
    label "ocieplenie_si&#281;"
  ]
  node [
    id 55
    label "zagrzanie"
  ]
  node [
    id 56
    label "ocieplenie"
  ]
  node [
    id 57
    label "korzystny"
  ]
  node [
    id 58
    label "przyjemny"
  ]
  node [
    id 59
    label "ciep&#322;o"
  ]
  node [
    id 60
    label "s&#322;onecznie"
  ]
  node [
    id 61
    label "bezdeszczowy"
  ]
  node [
    id 62
    label "bezchmurny"
  ]
  node [
    id 63
    label "pogodny"
  ]
  node [
    id 64
    label "fotowoltaiczny"
  ]
  node [
    id 65
    label "jasny"
  ]
  node [
    id 66
    label "g&#243;wniarz"
  ]
  node [
    id 67
    label "synek"
  ]
  node [
    id 68
    label "cz&#322;owiek"
  ]
  node [
    id 69
    label "boyfriend"
  ]
  node [
    id 70
    label "okrzos"
  ]
  node [
    id 71
    label "dziecko"
  ]
  node [
    id 72
    label "sympatia"
  ]
  node [
    id 73
    label "usynowienie"
  ]
  node [
    id 74
    label "pomocnik"
  ]
  node [
    id 75
    label "kawaler"
  ]
  node [
    id 76
    label "&#347;l&#261;ski"
  ]
  node [
    id 77
    label "m&#322;odzieniec"
  ]
  node [
    id 78
    label "kajtek"
  ]
  node [
    id 79
    label "pederasta"
  ]
  node [
    id 80
    label "usynawianie"
  ]
  node [
    id 81
    label "utulenie"
  ]
  node [
    id 82
    label "pediatra"
  ]
  node [
    id 83
    label "dzieciak"
  ]
  node [
    id 84
    label "utulanie"
  ]
  node [
    id 85
    label "dzieciarnia"
  ]
  node [
    id 86
    label "niepe&#322;noletni"
  ]
  node [
    id 87
    label "organizm"
  ]
  node [
    id 88
    label "utula&#263;"
  ]
  node [
    id 89
    label "cz&#322;owieczek"
  ]
  node [
    id 90
    label "fledgling"
  ]
  node [
    id 91
    label "zwierz&#281;"
  ]
  node [
    id 92
    label "utuli&#263;"
  ]
  node [
    id 93
    label "m&#322;odzik"
  ]
  node [
    id 94
    label "pedofil"
  ]
  node [
    id 95
    label "m&#322;odziak"
  ]
  node [
    id 96
    label "potomek"
  ]
  node [
    id 97
    label "entliczek-pentliczek"
  ]
  node [
    id 98
    label "potomstwo"
  ]
  node [
    id 99
    label "sraluch"
  ]
  node [
    id 100
    label "ludzko&#347;&#263;"
  ]
  node [
    id 101
    label "asymilowanie"
  ]
  node [
    id 102
    label "wapniak"
  ]
  node [
    id 103
    label "asymilowa&#263;"
  ]
  node [
    id 104
    label "os&#322;abia&#263;"
  ]
  node [
    id 105
    label "posta&#263;"
  ]
  node [
    id 106
    label "hominid"
  ]
  node [
    id 107
    label "podw&#322;adny"
  ]
  node [
    id 108
    label "os&#322;abianie"
  ]
  node [
    id 109
    label "g&#322;owa"
  ]
  node [
    id 110
    label "figura"
  ]
  node [
    id 111
    label "portrecista"
  ]
  node [
    id 112
    label "dwun&#243;g"
  ]
  node [
    id 113
    label "profanum"
  ]
  node [
    id 114
    label "mikrokosmos"
  ]
  node [
    id 115
    label "nasada"
  ]
  node [
    id 116
    label "duch"
  ]
  node [
    id 117
    label "antropochoria"
  ]
  node [
    id 118
    label "osoba"
  ]
  node [
    id 119
    label "wz&#243;r"
  ]
  node [
    id 120
    label "senior"
  ]
  node [
    id 121
    label "oddzia&#322;ywanie"
  ]
  node [
    id 122
    label "Adam"
  ]
  node [
    id 123
    label "homo_sapiens"
  ]
  node [
    id 124
    label "polifag"
  ]
  node [
    id 125
    label "emocja"
  ]
  node [
    id 126
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 127
    label "partner"
  ]
  node [
    id 128
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 129
    label "love"
  ]
  node [
    id 130
    label "kredens"
  ]
  node [
    id 131
    label "zawodnik"
  ]
  node [
    id 132
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 133
    label "bylina"
  ]
  node [
    id 134
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 135
    label "gracz"
  ]
  node [
    id 136
    label "r&#281;ka"
  ]
  node [
    id 137
    label "pomoc"
  ]
  node [
    id 138
    label "wrzosowate"
  ]
  node [
    id 139
    label "pomagacz"
  ]
  node [
    id 140
    label "junior"
  ]
  node [
    id 141
    label "junak"
  ]
  node [
    id 142
    label "m&#322;odzie&#380;"
  ]
  node [
    id 143
    label "mo&#322;ojec"
  ]
  node [
    id 144
    label "kawa&#322;ek"
  ]
  node [
    id 145
    label "m&#322;okos"
  ]
  node [
    id 146
    label "smarkateria"
  ]
  node [
    id 147
    label "ch&#322;opak"
  ]
  node [
    id 148
    label "cug"
  ]
  node [
    id 149
    label "krepel"
  ]
  node [
    id 150
    label "mietlorz"
  ]
  node [
    id 151
    label "francuz"
  ]
  node [
    id 152
    label "etnolekt"
  ]
  node [
    id 153
    label "sza&#322;ot"
  ]
  node [
    id 154
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 155
    label "polski"
  ]
  node [
    id 156
    label "regionalny"
  ]
  node [
    id 157
    label "halba"
  ]
  node [
    id 158
    label "buchta"
  ]
  node [
    id 159
    label "czarne_kluski"
  ]
  node [
    id 160
    label "szpajza"
  ]
  node [
    id 161
    label "szl&#261;ski"
  ]
  node [
    id 162
    label "&#347;lonski"
  ]
  node [
    id 163
    label "po_&#347;l&#261;sku"
  ]
  node [
    id 164
    label "waloszek"
  ]
  node [
    id 165
    label "gej"
  ]
  node [
    id 166
    label "tytu&#322;"
  ]
  node [
    id 167
    label "order"
  ]
  node [
    id 168
    label "zalotnik"
  ]
  node [
    id 169
    label "kawalerka"
  ]
  node [
    id 170
    label "rycerz"
  ]
  node [
    id 171
    label "odznaczenie"
  ]
  node [
    id 172
    label "nie&#380;onaty"
  ]
  node [
    id 173
    label "zakon_rycerski"
  ]
  node [
    id 174
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 175
    label "zakonnik"
  ]
  node [
    id 176
    label "syn"
  ]
  node [
    id 177
    label "przysposabianie"
  ]
  node [
    id 178
    label "przysposobienie"
  ]
  node [
    id 179
    label "adoption"
  ]
  node [
    id 180
    label "suspend"
  ]
  node [
    id 181
    label "zabi&#263;"
  ]
  node [
    id 182
    label "bent"
  ]
  node [
    id 183
    label "szubienica"
  ]
  node [
    id 184
    label "umie&#347;ci&#263;"
  ]
  node [
    id 185
    label "stryczek"
  ]
  node [
    id 186
    label "set"
  ]
  node [
    id 187
    label "put"
  ]
  node [
    id 188
    label "uplasowa&#263;"
  ]
  node [
    id 189
    label "wpierniczy&#263;"
  ]
  node [
    id 190
    label "okre&#347;li&#263;"
  ]
  node [
    id 191
    label "zrobi&#263;"
  ]
  node [
    id 192
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 193
    label "zmieni&#263;"
  ]
  node [
    id 194
    label "umieszcza&#263;"
  ]
  node [
    id 195
    label "zadzwoni&#263;"
  ]
  node [
    id 196
    label "poder&#380;n&#261;&#263;"
  ]
  node [
    id 197
    label "skarci&#263;"
  ]
  node [
    id 198
    label "skrzywdzi&#263;"
  ]
  node [
    id 199
    label "os&#322;oni&#263;"
  ]
  node [
    id 200
    label "przybi&#263;"
  ]
  node [
    id 201
    label "rozbroi&#263;"
  ]
  node [
    id 202
    label "uderzy&#263;"
  ]
  node [
    id 203
    label "po&#322;o&#380;y&#263;_na_kolana"
  ]
  node [
    id 204
    label "skrzywi&#263;"
  ]
  node [
    id 205
    label "dispatch"
  ]
  node [
    id 206
    label "zmordowa&#263;"
  ]
  node [
    id 207
    label "zakry&#263;"
  ]
  node [
    id 208
    label "zbi&#263;"
  ]
  node [
    id 209
    label "zapulsowa&#263;"
  ]
  node [
    id 210
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 211
    label "break"
  ]
  node [
    id 212
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 213
    label "zastrzeli&#263;"
  ]
  node [
    id 214
    label "u&#347;mierci&#263;"
  ]
  node [
    id 215
    label "zwalczy&#263;"
  ]
  node [
    id 216
    label "pomacha&#263;"
  ]
  node [
    id 217
    label "kill"
  ]
  node [
    id 218
    label "zako&#324;czy&#263;"
  ]
  node [
    id 219
    label "zniszczy&#263;"
  ]
  node [
    id 220
    label "powieszenie"
  ]
  node [
    id 221
    label "pohybel"
  ]
  node [
    id 222
    label "wieszanie"
  ]
  node [
    id 223
    label "wiesza&#263;"
  ]
  node [
    id 224
    label "narz&#281;dzie_&#347;mierci"
  ]
  node [
    id 225
    label "hangman's_rope"
  ]
  node [
    id 226
    label "kara_&#347;mierci"
  ]
  node [
    id 227
    label "suspension"
  ]
  node [
    id 228
    label "sznur"
  ]
  node [
    id 229
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 230
    label "mie&#263;_miejsce"
  ]
  node [
    id 231
    label "equal"
  ]
  node [
    id 232
    label "trwa&#263;"
  ]
  node [
    id 233
    label "chodzi&#263;"
  ]
  node [
    id 234
    label "si&#281;ga&#263;"
  ]
  node [
    id 235
    label "stan"
  ]
  node [
    id 236
    label "obecno&#347;&#263;"
  ]
  node [
    id 237
    label "stand"
  ]
  node [
    id 238
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 239
    label "uczestniczy&#263;"
  ]
  node [
    id 240
    label "participate"
  ]
  node [
    id 241
    label "robi&#263;"
  ]
  node [
    id 242
    label "istnie&#263;"
  ]
  node [
    id 243
    label "pozostawa&#263;"
  ]
  node [
    id 244
    label "zostawa&#263;"
  ]
  node [
    id 245
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 246
    label "adhere"
  ]
  node [
    id 247
    label "compass"
  ]
  node [
    id 248
    label "korzysta&#263;"
  ]
  node [
    id 249
    label "appreciation"
  ]
  node [
    id 250
    label "osi&#261;ga&#263;"
  ]
  node [
    id 251
    label "dociera&#263;"
  ]
  node [
    id 252
    label "get"
  ]
  node [
    id 253
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 254
    label "mierzy&#263;"
  ]
  node [
    id 255
    label "u&#380;ywa&#263;"
  ]
  node [
    id 256
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 257
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 258
    label "exsert"
  ]
  node [
    id 259
    label "being"
  ]
  node [
    id 260
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 261
    label "cecha"
  ]
  node [
    id 262
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 263
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 264
    label "p&#322;ywa&#263;"
  ]
  node [
    id 265
    label "run"
  ]
  node [
    id 266
    label "bangla&#263;"
  ]
  node [
    id 267
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 268
    label "przebiega&#263;"
  ]
  node [
    id 269
    label "wk&#322;ada&#263;"
  ]
  node [
    id 270
    label "proceed"
  ]
  node [
    id 271
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 272
    label "carry"
  ]
  node [
    id 273
    label "bywa&#263;"
  ]
  node [
    id 274
    label "dziama&#263;"
  ]
  node [
    id 275
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 276
    label "stara&#263;_si&#281;"
  ]
  node [
    id 277
    label "para"
  ]
  node [
    id 278
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 279
    label "str&#243;j"
  ]
  node [
    id 280
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 281
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 282
    label "krok"
  ]
  node [
    id 283
    label "tryb"
  ]
  node [
    id 284
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 285
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 286
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 287
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 288
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 289
    label "continue"
  ]
  node [
    id 290
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 291
    label "Ohio"
  ]
  node [
    id 292
    label "wci&#281;cie"
  ]
  node [
    id 293
    label "Nowy_York"
  ]
  node [
    id 294
    label "warstwa"
  ]
  node [
    id 295
    label "samopoczucie"
  ]
  node [
    id 296
    label "Illinois"
  ]
  node [
    id 297
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 298
    label "state"
  ]
  node [
    id 299
    label "Jukatan"
  ]
  node [
    id 300
    label "Kalifornia"
  ]
  node [
    id 301
    label "Wirginia"
  ]
  node [
    id 302
    label "wektor"
  ]
  node [
    id 303
    label "Teksas"
  ]
  node [
    id 304
    label "Goa"
  ]
  node [
    id 305
    label "Waszyngton"
  ]
  node [
    id 306
    label "miejsce"
  ]
  node [
    id 307
    label "Massachusetts"
  ]
  node [
    id 308
    label "Alaska"
  ]
  node [
    id 309
    label "Arakan"
  ]
  node [
    id 310
    label "Hawaje"
  ]
  node [
    id 311
    label "Maryland"
  ]
  node [
    id 312
    label "punkt"
  ]
  node [
    id 313
    label "Michigan"
  ]
  node [
    id 314
    label "Arizona"
  ]
  node [
    id 315
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 316
    label "Georgia"
  ]
  node [
    id 317
    label "poziom"
  ]
  node [
    id 318
    label "Pensylwania"
  ]
  node [
    id 319
    label "shape"
  ]
  node [
    id 320
    label "Luizjana"
  ]
  node [
    id 321
    label "Nowy_Meksyk"
  ]
  node [
    id 322
    label "Alabama"
  ]
  node [
    id 323
    label "ilo&#347;&#263;"
  ]
  node [
    id 324
    label "Kansas"
  ]
  node [
    id 325
    label "Oregon"
  ]
  node [
    id 326
    label "Floryda"
  ]
  node [
    id 327
    label "Oklahoma"
  ]
  node [
    id 328
    label "jednostka_administracyjna"
  ]
  node [
    id 329
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 330
    label "lower"
  ]
  node [
    id 331
    label "niepokoi&#263;"
  ]
  node [
    id 332
    label "dr&#281;czy&#263;"
  ]
  node [
    id 333
    label "mortify"
  ]
  node [
    id 334
    label "zam&#281;cza&#263;"
  ]
  node [
    id 335
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 336
    label "doskwiera&#263;"
  ]
  node [
    id 337
    label "trespass"
  ]
  node [
    id 338
    label "turbowa&#263;"
  ]
  node [
    id 339
    label "wzbudza&#263;"
  ]
  node [
    id 340
    label "kampong"
  ]
  node [
    id 341
    label "ghaty"
  ]
  node [
    id 342
    label "charakterystyczny"
  ]
  node [
    id 343
    label "balut"
  ]
  node [
    id 344
    label "ka&#322;mucki"
  ]
  node [
    id 345
    label "azjatycko"
  ]
  node [
    id 346
    label "po_ka&#322;mucku"
  ]
  node [
    id 347
    label "j&#281;zyk_a&#322;tajski"
  ]
  node [
    id 348
    label "rosyjski"
  ]
  node [
    id 349
    label "wie&#347;"
  ]
  node [
    id 350
    label "jajko"
  ]
  node [
    id 351
    label "potrawa"
  ]
  node [
    id 352
    label "schody"
  ]
  node [
    id 353
    label "kamienny"
  ]
  node [
    id 354
    label "rzeka"
  ]
  node [
    id 355
    label "charakterystycznie"
  ]
  node [
    id 356
    label "szczeg&#243;lny"
  ]
  node [
    id 357
    label "wyj&#261;tkowy"
  ]
  node [
    id 358
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 359
    label "podobny"
  ]
  node [
    id 360
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 361
    label "nale&#380;ny"
  ]
  node [
    id 362
    label "nale&#380;yty"
  ]
  node [
    id 363
    label "uprawniony"
  ]
  node [
    id 364
    label "zasadniczy"
  ]
  node [
    id 365
    label "stosownie"
  ]
  node [
    id 366
    label "taki"
  ]
  node [
    id 367
    label "prawdziwy"
  ]
  node [
    id 368
    label "ten"
  ]
  node [
    id 369
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 370
    label "gangster"
  ]
  node [
    id 371
    label "organizacja"
  ]
  node [
    id 372
    label "banda"
  ]
  node [
    id 373
    label "podmiot"
  ]
  node [
    id 374
    label "jednostka_organizacyjna"
  ]
  node [
    id 375
    label "struktura"
  ]
  node [
    id 376
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 377
    label "TOPR"
  ]
  node [
    id 378
    label "endecki"
  ]
  node [
    id 379
    label "zesp&#243;&#322;"
  ]
  node [
    id 380
    label "od&#322;am"
  ]
  node [
    id 381
    label "przedstawicielstwo"
  ]
  node [
    id 382
    label "Cepelia"
  ]
  node [
    id 383
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 384
    label "ZBoWiD"
  ]
  node [
    id 385
    label "organization"
  ]
  node [
    id 386
    label "centrala"
  ]
  node [
    id 387
    label "GOPR"
  ]
  node [
    id 388
    label "ZOMO"
  ]
  node [
    id 389
    label "ZMP"
  ]
  node [
    id 390
    label "komitet_koordynacyjny"
  ]
  node [
    id 391
    label "przybud&#243;wka"
  ]
  node [
    id 392
    label "boj&#243;wka"
  ]
  node [
    id 393
    label "przest&#281;pca"
  ]
  node [
    id 394
    label "przest&#281;pczo&#347;&#263;_zorganizowana"
  ]
  node [
    id 395
    label "towarzystwo"
  ]
  node [
    id 396
    label "crew"
  ]
  node [
    id 397
    label "granda"
  ]
  node [
    id 398
    label "ogrodzenie"
  ]
  node [
    id 399
    label "gromada"
  ]
  node [
    id 400
    label "band"
  ]
  node [
    id 401
    label "package"
  ]
  node [
    id 402
    label "st&#243;&#322;_bilardowy"
  ]
  node [
    id 403
    label "kraw&#281;d&#378;"
  ]
  node [
    id 404
    label "flight"
  ]
  node [
    id 405
    label "criminalism"
  ]
  node [
    id 406
    label "przest&#281;pstwo"
  ]
  node [
    id 407
    label "bezprawie"
  ]
  node [
    id 408
    label "problem_spo&#322;eczny"
  ]
  node [
    id 409
    label "patologia"
  ]
  node [
    id 410
    label "do&#347;wiadczenie"
  ]
  node [
    id 411
    label "teren_szko&#322;y"
  ]
  node [
    id 412
    label "wiedza"
  ]
  node [
    id 413
    label "Mickiewicz"
  ]
  node [
    id 414
    label "kwalifikacje"
  ]
  node [
    id 415
    label "podr&#281;cznik"
  ]
  node [
    id 416
    label "absolwent"
  ]
  node [
    id 417
    label "praktyka"
  ]
  node [
    id 418
    label "school"
  ]
  node [
    id 419
    label "system"
  ]
  node [
    id 420
    label "zda&#263;"
  ]
  node [
    id 421
    label "gabinet"
  ]
  node [
    id 422
    label "urszulanki"
  ]
  node [
    id 423
    label "sztuba"
  ]
  node [
    id 424
    label "&#322;awa_szkolna"
  ]
  node [
    id 425
    label "nauka"
  ]
  node [
    id 426
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 427
    label "przepisa&#263;"
  ]
  node [
    id 428
    label "muzyka"
  ]
  node [
    id 429
    label "grupa"
  ]
  node [
    id 430
    label "form"
  ]
  node [
    id 431
    label "klasa"
  ]
  node [
    id 432
    label "lekcja"
  ]
  node [
    id 433
    label "metoda"
  ]
  node [
    id 434
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 435
    label "przepisanie"
  ]
  node [
    id 436
    label "czas"
  ]
  node [
    id 437
    label "skolaryzacja"
  ]
  node [
    id 438
    label "zdanie"
  ]
  node [
    id 439
    label "stopek"
  ]
  node [
    id 440
    label "sekretariat"
  ]
  node [
    id 441
    label "ideologia"
  ]
  node [
    id 442
    label "lesson"
  ]
  node [
    id 443
    label "instytucja"
  ]
  node [
    id 444
    label "niepokalanki"
  ]
  node [
    id 445
    label "siedziba"
  ]
  node [
    id 446
    label "szkolenie"
  ]
  node [
    id 447
    label "kara"
  ]
  node [
    id 448
    label "tablica"
  ]
  node [
    id 449
    label "wyprawka"
  ]
  node [
    id 450
    label "pomoc_naukowa"
  ]
  node [
    id 451
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 452
    label "odm&#322;adzanie"
  ]
  node [
    id 453
    label "liga"
  ]
  node [
    id 454
    label "jednostka_systematyczna"
  ]
  node [
    id 455
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 456
    label "egzemplarz"
  ]
  node [
    id 457
    label "Entuzjastki"
  ]
  node [
    id 458
    label "zbi&#243;r"
  ]
  node [
    id 459
    label "kompozycja"
  ]
  node [
    id 460
    label "Terranie"
  ]
  node [
    id 461
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 462
    label "category"
  ]
  node [
    id 463
    label "pakiet_klimatyczny"
  ]
  node [
    id 464
    label "oddzia&#322;"
  ]
  node [
    id 465
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 466
    label "cz&#261;steczka"
  ]
  node [
    id 467
    label "stage_set"
  ]
  node [
    id 468
    label "type"
  ]
  node [
    id 469
    label "specgrupa"
  ]
  node [
    id 470
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 471
    label "&#346;wietliki"
  ]
  node [
    id 472
    label "odm&#322;odzenie"
  ]
  node [
    id 473
    label "Eurogrupa"
  ]
  node [
    id 474
    label "odm&#322;adza&#263;"
  ]
  node [
    id 475
    label "formacja_geologiczna"
  ]
  node [
    id 476
    label "harcerze_starsi"
  ]
  node [
    id 477
    label "course"
  ]
  node [
    id 478
    label "pomaganie"
  ]
  node [
    id 479
    label "training"
  ]
  node [
    id 480
    label "zapoznawanie"
  ]
  node [
    id 481
    label "seria"
  ]
  node [
    id 482
    label "zaj&#281;cia"
  ]
  node [
    id 483
    label "pouczenie"
  ]
  node [
    id 484
    label "o&#347;wiecanie"
  ]
  node [
    id 485
    label "Lira"
  ]
  node [
    id 486
    label "kliker"
  ]
  node [
    id 487
    label "miasteczko_rowerowe"
  ]
  node [
    id 488
    label "porada"
  ]
  node [
    id 489
    label "fotowoltaika"
  ]
  node [
    id 490
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 491
    label "przem&#243;wienie"
  ]
  node [
    id 492
    label "nauki_o_poznaniu"
  ]
  node [
    id 493
    label "nomotetyczny"
  ]
  node [
    id 494
    label "systematyka"
  ]
  node [
    id 495
    label "proces"
  ]
  node [
    id 496
    label "typologia"
  ]
  node [
    id 497
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 498
    label "kultura_duchowa"
  ]
  node [
    id 499
    label "nauki_penalne"
  ]
  node [
    id 500
    label "dziedzina"
  ]
  node [
    id 501
    label "imagineskopia"
  ]
  node [
    id 502
    label "teoria_naukowa"
  ]
  node [
    id 503
    label "inwentyka"
  ]
  node [
    id 504
    label "metodologia"
  ]
  node [
    id 505
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 506
    label "nauki_o_Ziemi"
  ]
  node [
    id 507
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 508
    label "eliminacje"
  ]
  node [
    id 509
    label "osoba_prawna"
  ]
  node [
    id 510
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 511
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 512
    label "poj&#281;cie"
  ]
  node [
    id 513
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 514
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 515
    label "biuro"
  ]
  node [
    id 516
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 517
    label "Fundusze_Unijne"
  ]
  node [
    id 518
    label "zamyka&#263;"
  ]
  node [
    id 519
    label "establishment"
  ]
  node [
    id 520
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 521
    label "urz&#261;d"
  ]
  node [
    id 522
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 523
    label "afiliowa&#263;"
  ]
  node [
    id 524
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 525
    label "standard"
  ]
  node [
    id 526
    label "zamykanie"
  ]
  node [
    id 527
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 528
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 529
    label "materia&#322;"
  ]
  node [
    id 530
    label "spos&#243;b"
  ]
  node [
    id 531
    label "obrz&#261;dek"
  ]
  node [
    id 532
    label "Biblia"
  ]
  node [
    id 533
    label "tekst"
  ]
  node [
    id 534
    label "lektor"
  ]
  node [
    id 535
    label "kwota"
  ]
  node [
    id 536
    label "nemezis"
  ]
  node [
    id 537
    label "konsekwencja"
  ]
  node [
    id 538
    label "punishment"
  ]
  node [
    id 539
    label "klacz"
  ]
  node [
    id 540
    label "forfeit"
  ]
  node [
    id 541
    label "roboty_przymusowe"
  ]
  node [
    id 542
    label "poprzedzanie"
  ]
  node [
    id 543
    label "czasoprzestrze&#324;"
  ]
  node [
    id 544
    label "laba"
  ]
  node [
    id 545
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 546
    label "chronometria"
  ]
  node [
    id 547
    label "rachuba_czasu"
  ]
  node [
    id 548
    label "przep&#322;ywanie"
  ]
  node [
    id 549
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 550
    label "czasokres"
  ]
  node [
    id 551
    label "odczyt"
  ]
  node [
    id 552
    label "chwila"
  ]
  node [
    id 553
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 554
    label "dzieje"
  ]
  node [
    id 555
    label "kategoria_gramatyczna"
  ]
  node [
    id 556
    label "poprzedzenie"
  ]
  node [
    id 557
    label "trawienie"
  ]
  node [
    id 558
    label "pochodzi&#263;"
  ]
  node [
    id 559
    label "period"
  ]
  node [
    id 560
    label "okres_czasu"
  ]
  node [
    id 561
    label "poprzedza&#263;"
  ]
  node [
    id 562
    label "schy&#322;ek"
  ]
  node [
    id 563
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 564
    label "odwlekanie_si&#281;"
  ]
  node [
    id 565
    label "zegar"
  ]
  node [
    id 566
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 567
    label "czwarty_wymiar"
  ]
  node [
    id 568
    label "pochodzenie"
  ]
  node [
    id 569
    label "koniugacja"
  ]
  node [
    id 570
    label "Zeitgeist"
  ]
  node [
    id 571
    label "trawi&#263;"
  ]
  node [
    id 572
    label "pogoda"
  ]
  node [
    id 573
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 574
    label "poprzedzi&#263;"
  ]
  node [
    id 575
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 576
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 577
    label "time_period"
  ]
  node [
    id 578
    label "j&#261;dro"
  ]
  node [
    id 579
    label "systemik"
  ]
  node [
    id 580
    label "rozprz&#261;c"
  ]
  node [
    id 581
    label "oprogramowanie"
  ]
  node [
    id 582
    label "systemat"
  ]
  node [
    id 583
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 584
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 585
    label "model"
  ]
  node [
    id 586
    label "usenet"
  ]
  node [
    id 587
    label "s&#261;d"
  ]
  node [
    id 588
    label "porz&#261;dek"
  ]
  node [
    id 589
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 590
    label "przyn&#281;ta"
  ]
  node [
    id 591
    label "p&#322;&#243;d"
  ]
  node [
    id 592
    label "net"
  ]
  node [
    id 593
    label "w&#281;dkarstwo"
  ]
  node [
    id 594
    label "eratem"
  ]
  node [
    id 595
    label "doktryna"
  ]
  node [
    id 596
    label "pulpit"
  ]
  node [
    id 597
    label "konstelacja"
  ]
  node [
    id 598
    label "jednostka_geologiczna"
  ]
  node [
    id 599
    label "o&#347;"
  ]
  node [
    id 600
    label "podsystem"
  ]
  node [
    id 601
    label "ryba"
  ]
  node [
    id 602
    label "Leopard"
  ]
  node [
    id 603
    label "Android"
  ]
  node [
    id 604
    label "zachowanie"
  ]
  node [
    id 605
    label "cybernetyk"
  ]
  node [
    id 606
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 607
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 608
    label "method"
  ]
  node [
    id 609
    label "sk&#322;ad"
  ]
  node [
    id 610
    label "podstawa"
  ]
  node [
    id 611
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 612
    label "practice"
  ]
  node [
    id 613
    label "znawstwo"
  ]
  node [
    id 614
    label "skill"
  ]
  node [
    id 615
    label "czyn"
  ]
  node [
    id 616
    label "zwyczaj"
  ]
  node [
    id 617
    label "eksperiencja"
  ]
  node [
    id 618
    label "praca"
  ]
  node [
    id 619
    label "&#321;ubianka"
  ]
  node [
    id 620
    label "miejsce_pracy"
  ]
  node [
    id 621
    label "dzia&#322;_personalny"
  ]
  node [
    id 622
    label "Kreml"
  ]
  node [
    id 623
    label "Bia&#322;y_Dom"
  ]
  node [
    id 624
    label "budynek"
  ]
  node [
    id 625
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 626
    label "sadowisko"
  ]
  node [
    id 627
    label "wokalistyka"
  ]
  node [
    id 628
    label "przedmiot"
  ]
  node [
    id 629
    label "wykonywanie"
  ]
  node [
    id 630
    label "muza"
  ]
  node [
    id 631
    label "wykonywa&#263;"
  ]
  node [
    id 632
    label "zjawisko"
  ]
  node [
    id 633
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 634
    label "beatbox"
  ]
  node [
    id 635
    label "komponowa&#263;"
  ]
  node [
    id 636
    label "komponowanie"
  ]
  node [
    id 637
    label "wytw&#243;r"
  ]
  node [
    id 638
    label "pasa&#380;"
  ]
  node [
    id 639
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 640
    label "notacja_muzyczna"
  ]
  node [
    id 641
    label "kontrapunkt"
  ]
  node [
    id 642
    label "sztuka"
  ]
  node [
    id 643
    label "instrumentalistyka"
  ]
  node [
    id 644
    label "harmonia"
  ]
  node [
    id 645
    label "wys&#322;uchanie"
  ]
  node [
    id 646
    label "kapela"
  ]
  node [
    id 647
    label "britpop"
  ]
  node [
    id 648
    label "badanie"
  ]
  node [
    id 649
    label "obserwowanie"
  ]
  node [
    id 650
    label "wy&#347;wiadczenie"
  ]
  node [
    id 651
    label "wydarzenie"
  ]
  node [
    id 652
    label "assay"
  ]
  node [
    id 653
    label "checkup"
  ]
  node [
    id 654
    label "spotkanie"
  ]
  node [
    id 655
    label "do&#347;wiadczanie"
  ]
  node [
    id 656
    label "zbadanie"
  ]
  node [
    id 657
    label "potraktowanie"
  ]
  node [
    id 658
    label "poczucie"
  ]
  node [
    id 659
    label "proporcja"
  ]
  node [
    id 660
    label "wykszta&#322;cenie"
  ]
  node [
    id 661
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 662
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 663
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 664
    label "urszulanki_szare"
  ]
  node [
    id 665
    label "cognition"
  ]
  node [
    id 666
    label "intelekt"
  ]
  node [
    id 667
    label "pozwolenie"
  ]
  node [
    id 668
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 669
    label "zaawansowanie"
  ]
  node [
    id 670
    label "przekaza&#263;"
  ]
  node [
    id 671
    label "supply"
  ]
  node [
    id 672
    label "zaleci&#263;"
  ]
  node [
    id 673
    label "rewrite"
  ]
  node [
    id 674
    label "zrzec_si&#281;"
  ]
  node [
    id 675
    label "testament"
  ]
  node [
    id 676
    label "skopiowa&#263;"
  ]
  node [
    id 677
    label "zadanie"
  ]
  node [
    id 678
    label "lekarstwo"
  ]
  node [
    id 679
    label "przenie&#347;&#263;"
  ]
  node [
    id 680
    label "ucze&#324;"
  ]
  node [
    id 681
    label "student"
  ]
  node [
    id 682
    label "zaliczy&#263;"
  ]
  node [
    id 683
    label "powierzy&#263;"
  ]
  node [
    id 684
    label "zmusi&#263;"
  ]
  node [
    id 685
    label "translate"
  ]
  node [
    id 686
    label "give"
  ]
  node [
    id 687
    label "picture"
  ]
  node [
    id 688
    label "przedstawi&#263;"
  ]
  node [
    id 689
    label "z&#322;o&#380;y&#263;_egzamin"
  ]
  node [
    id 690
    label "convey"
  ]
  node [
    id 691
    label "przekazanie"
  ]
  node [
    id 692
    label "skopiowanie"
  ]
  node [
    id 693
    label "arrangement"
  ]
  node [
    id 694
    label "przeniesienie"
  ]
  node [
    id 695
    label "answer"
  ]
  node [
    id 696
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 697
    label "transcription"
  ]
  node [
    id 698
    label "zalecenie"
  ]
  node [
    id 699
    label "fraza"
  ]
  node [
    id 700
    label "stanowisko"
  ]
  node [
    id 701
    label "wypowiedzenie"
  ]
  node [
    id 702
    label "prison_term"
  ]
  node [
    id 703
    label "okres"
  ]
  node [
    id 704
    label "przedstawienie"
  ]
  node [
    id 705
    label "wyra&#380;enie"
  ]
  node [
    id 706
    label "zaliczenie"
  ]
  node [
    id 707
    label "antylogizm"
  ]
  node [
    id 708
    label "zmuszenie"
  ]
  node [
    id 709
    label "konektyw"
  ]
  node [
    id 710
    label "attitude"
  ]
  node [
    id 711
    label "powierzenie"
  ]
  node [
    id 712
    label "adjudication"
  ]
  node [
    id 713
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 714
    label "pass"
  ]
  node [
    id 715
    label "political_orientation"
  ]
  node [
    id 716
    label "idea"
  ]
  node [
    id 717
    label "stra&#380;nik"
  ]
  node [
    id 718
    label "przedszkole"
  ]
  node [
    id 719
    label "opiekun"
  ]
  node [
    id 720
    label "ruch"
  ]
  node [
    id 721
    label "rozmiar&#243;wka"
  ]
  node [
    id 722
    label "p&#322;aszczyzna"
  ]
  node [
    id 723
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 724
    label "tarcza"
  ]
  node [
    id 725
    label "kosz"
  ]
  node [
    id 726
    label "transparent"
  ]
  node [
    id 727
    label "uk&#322;ad"
  ]
  node [
    id 728
    label "rubryka"
  ]
  node [
    id 729
    label "kontener"
  ]
  node [
    id 730
    label "spis"
  ]
  node [
    id 731
    label "plate"
  ]
  node [
    id 732
    label "konstrukcja"
  ]
  node [
    id 733
    label "szachownica_Punnetta"
  ]
  node [
    id 734
    label "chart"
  ]
  node [
    id 735
    label "izba"
  ]
  node [
    id 736
    label "biurko"
  ]
  node [
    id 737
    label "boks"
  ]
  node [
    id 738
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 739
    label "egzekutywa"
  ]
  node [
    id 740
    label "premier"
  ]
  node [
    id 741
    label "Londyn"
  ]
  node [
    id 742
    label "palestra"
  ]
  node [
    id 743
    label "pok&#243;j"
  ]
  node [
    id 744
    label "pracownia"
  ]
  node [
    id 745
    label "gabinet_cieni"
  ]
  node [
    id 746
    label "pomieszczenie"
  ]
  node [
    id 747
    label "Konsulat"
  ]
  node [
    id 748
    label "wagon"
  ]
  node [
    id 749
    label "mecz_mistrzowski"
  ]
  node [
    id 750
    label "class"
  ]
  node [
    id 751
    label "&#322;awka"
  ]
  node [
    id 752
    label "wykrzyknik"
  ]
  node [
    id 753
    label "zaleta"
  ]
  node [
    id 754
    label "programowanie_obiektowe"
  ]
  node [
    id 755
    label "rezerwa"
  ]
  node [
    id 756
    label "Ekwici"
  ]
  node [
    id 757
    label "&#347;rodowisko"
  ]
  node [
    id 758
    label "sala"
  ]
  node [
    id 759
    label "jako&#347;&#263;"
  ]
  node [
    id 760
    label "znak_jako&#347;ci"
  ]
  node [
    id 761
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 762
    label "promocja"
  ]
  node [
    id 763
    label "kurs"
  ]
  node [
    id 764
    label "obiekt"
  ]
  node [
    id 765
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 766
    label "dziennik_lekcyjny"
  ]
  node [
    id 767
    label "typ"
  ]
  node [
    id 768
    label "fakcja"
  ]
  node [
    id 769
    label "obrona"
  ]
  node [
    id 770
    label "atak"
  ]
  node [
    id 771
    label "botanika"
  ]
  node [
    id 772
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 773
    label "Wallenrod"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
]
