graph [
  node [
    id 0
    label "kobieta"
    origin "text"
  ]
  node [
    id 1
    label "wyznaczy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nagroda"
    origin "text"
  ]
  node [
    id 3
    label "wskazanie"
    origin "text"
  ]
  node [
    id 4
    label "sprawca"
    origin "text"
  ]
  node [
    id 5
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 6
    label "zaatakowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "amstaff"
    origin "text"
  ]
  node [
    id 8
    label "spowodowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "fejsbukowy"
    origin "text"
  ]
  node [
    id 10
    label "lincz"
    origin "text"
  ]
  node [
    id 11
    label "doros&#322;y"
  ]
  node [
    id 12
    label "&#380;ona"
  ]
  node [
    id 13
    label "cz&#322;owiek"
  ]
  node [
    id 14
    label "samica"
  ]
  node [
    id 15
    label "uleganie"
  ]
  node [
    id 16
    label "ulec"
  ]
  node [
    id 17
    label "m&#281;&#380;yna"
  ]
  node [
    id 18
    label "partnerka"
  ]
  node [
    id 19
    label "ulegni&#281;cie"
  ]
  node [
    id 20
    label "pa&#324;stwo"
  ]
  node [
    id 21
    label "&#322;ono"
  ]
  node [
    id 22
    label "menopauza"
  ]
  node [
    id 23
    label "przekwitanie"
  ]
  node [
    id 24
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 25
    label "babka"
  ]
  node [
    id 26
    label "ulega&#263;"
  ]
  node [
    id 27
    label "wydoro&#347;lenie"
  ]
  node [
    id 28
    label "du&#380;y"
  ]
  node [
    id 29
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 30
    label "doro&#347;lenie"
  ]
  node [
    id 31
    label "&#378;ra&#322;y"
  ]
  node [
    id 32
    label "doro&#347;le"
  ]
  node [
    id 33
    label "senior"
  ]
  node [
    id 34
    label "dojrzale"
  ]
  node [
    id 35
    label "wapniak"
  ]
  node [
    id 36
    label "dojrza&#322;y"
  ]
  node [
    id 37
    label "m&#261;dry"
  ]
  node [
    id 38
    label "doletni"
  ]
  node [
    id 39
    label "ludzko&#347;&#263;"
  ]
  node [
    id 40
    label "asymilowanie"
  ]
  node [
    id 41
    label "asymilowa&#263;"
  ]
  node [
    id 42
    label "os&#322;abia&#263;"
  ]
  node [
    id 43
    label "posta&#263;"
  ]
  node [
    id 44
    label "hominid"
  ]
  node [
    id 45
    label "podw&#322;adny"
  ]
  node [
    id 46
    label "os&#322;abianie"
  ]
  node [
    id 47
    label "g&#322;owa"
  ]
  node [
    id 48
    label "figura"
  ]
  node [
    id 49
    label "portrecista"
  ]
  node [
    id 50
    label "dwun&#243;g"
  ]
  node [
    id 51
    label "profanum"
  ]
  node [
    id 52
    label "mikrokosmos"
  ]
  node [
    id 53
    label "nasada"
  ]
  node [
    id 54
    label "duch"
  ]
  node [
    id 55
    label "antropochoria"
  ]
  node [
    id 56
    label "osoba"
  ]
  node [
    id 57
    label "wz&#243;r"
  ]
  node [
    id 58
    label "oddzia&#322;ywanie"
  ]
  node [
    id 59
    label "Adam"
  ]
  node [
    id 60
    label "homo_sapiens"
  ]
  node [
    id 61
    label "polifag"
  ]
  node [
    id 62
    label "ma&#322;&#380;onek"
  ]
  node [
    id 63
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 64
    label "&#347;lubna"
  ]
  node [
    id 65
    label "kobita"
  ]
  node [
    id 66
    label "panna_m&#322;oda"
  ]
  node [
    id 67
    label "aktorka"
  ]
  node [
    id 68
    label "partner"
  ]
  node [
    id 69
    label "poddanie_si&#281;"
  ]
  node [
    id 70
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 71
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 72
    label "return"
  ]
  node [
    id 73
    label "poddanie"
  ]
  node [
    id 74
    label "nagi&#281;cie_si&#281;"
  ]
  node [
    id 75
    label "pozwolenie"
  ]
  node [
    id 76
    label "subjugation"
  ]
  node [
    id 77
    label "stanie_si&#281;"
  ]
  node [
    id 78
    label "&#380;ycie"
  ]
  node [
    id 79
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 80
    label "przywo&#322;a&#263;"
  ]
  node [
    id 81
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 82
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 83
    label "poddawa&#263;"
  ]
  node [
    id 84
    label "postpone"
  ]
  node [
    id 85
    label "render"
  ]
  node [
    id 86
    label "zezwala&#263;"
  ]
  node [
    id 87
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 88
    label "subject"
  ]
  node [
    id 89
    label "kwitnienie"
  ]
  node [
    id 90
    label "przemijanie"
  ]
  node [
    id 91
    label "przestawanie"
  ]
  node [
    id 92
    label "starzenie_si&#281;"
  ]
  node [
    id 93
    label "menopause"
  ]
  node [
    id 94
    label "obumieranie"
  ]
  node [
    id 95
    label "dojrzewanie"
  ]
  node [
    id 96
    label "zezwalanie"
  ]
  node [
    id 97
    label "zaliczanie"
  ]
  node [
    id 98
    label "podporz&#261;dkowywanie_si&#281;"
  ]
  node [
    id 99
    label "poddawanie"
  ]
  node [
    id 100
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 101
    label "burst"
  ]
  node [
    id 102
    label "przywo&#322;anie"
  ]
  node [
    id 103
    label "naginanie_si&#281;"
  ]
  node [
    id 104
    label "poddawanie_si&#281;"
  ]
  node [
    id 105
    label "stawanie_si&#281;"
  ]
  node [
    id 106
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 107
    label "sta&#263;_si&#281;"
  ]
  node [
    id 108
    label "fall"
  ]
  node [
    id 109
    label "give"
  ]
  node [
    id 110
    label "pozwoli&#263;"
  ]
  node [
    id 111
    label "podda&#263;"
  ]
  node [
    id 112
    label "put_in"
  ]
  node [
    id 113
    label "podda&#263;_si&#281;"
  ]
  node [
    id 114
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 115
    label "Katar"
  ]
  node [
    id 116
    label "Libia"
  ]
  node [
    id 117
    label "Gwatemala"
  ]
  node [
    id 118
    label "Ekwador"
  ]
  node [
    id 119
    label "Afganistan"
  ]
  node [
    id 120
    label "Tad&#380;ykistan"
  ]
  node [
    id 121
    label "Bhutan"
  ]
  node [
    id 122
    label "Argentyna"
  ]
  node [
    id 123
    label "D&#380;ibuti"
  ]
  node [
    id 124
    label "Wenezuela"
  ]
  node [
    id 125
    label "Gabon"
  ]
  node [
    id 126
    label "Ukraina"
  ]
  node [
    id 127
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 128
    label "Rwanda"
  ]
  node [
    id 129
    label "Liechtenstein"
  ]
  node [
    id 130
    label "organizacja"
  ]
  node [
    id 131
    label "Sri_Lanka"
  ]
  node [
    id 132
    label "Madagaskar"
  ]
  node [
    id 133
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 134
    label "Kongo"
  ]
  node [
    id 135
    label "Tonga"
  ]
  node [
    id 136
    label "Bangladesz"
  ]
  node [
    id 137
    label "Kanada"
  ]
  node [
    id 138
    label "Wehrlen"
  ]
  node [
    id 139
    label "Algieria"
  ]
  node [
    id 140
    label "Uganda"
  ]
  node [
    id 141
    label "Surinam"
  ]
  node [
    id 142
    label "Sahara_Zachodnia"
  ]
  node [
    id 143
    label "Chile"
  ]
  node [
    id 144
    label "W&#281;gry"
  ]
  node [
    id 145
    label "Birma"
  ]
  node [
    id 146
    label "Kazachstan"
  ]
  node [
    id 147
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 148
    label "Armenia"
  ]
  node [
    id 149
    label "Tuwalu"
  ]
  node [
    id 150
    label "Timor_Wschodni"
  ]
  node [
    id 151
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 152
    label "Izrael"
  ]
  node [
    id 153
    label "Estonia"
  ]
  node [
    id 154
    label "Komory"
  ]
  node [
    id 155
    label "Kamerun"
  ]
  node [
    id 156
    label "Haiti"
  ]
  node [
    id 157
    label "Belize"
  ]
  node [
    id 158
    label "Sierra_Leone"
  ]
  node [
    id 159
    label "Luksemburg"
  ]
  node [
    id 160
    label "USA"
  ]
  node [
    id 161
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 162
    label "Barbados"
  ]
  node [
    id 163
    label "San_Marino"
  ]
  node [
    id 164
    label "Bu&#322;garia"
  ]
  node [
    id 165
    label "Indonezja"
  ]
  node [
    id 166
    label "Wietnam"
  ]
  node [
    id 167
    label "Malawi"
  ]
  node [
    id 168
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 169
    label "Francja"
  ]
  node [
    id 170
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 171
    label "partia"
  ]
  node [
    id 172
    label "Zambia"
  ]
  node [
    id 173
    label "Angola"
  ]
  node [
    id 174
    label "Grenada"
  ]
  node [
    id 175
    label "Nepal"
  ]
  node [
    id 176
    label "Panama"
  ]
  node [
    id 177
    label "Rumunia"
  ]
  node [
    id 178
    label "Czarnog&#243;ra"
  ]
  node [
    id 179
    label "Malediwy"
  ]
  node [
    id 180
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 181
    label "S&#322;owacja"
  ]
  node [
    id 182
    label "para"
  ]
  node [
    id 183
    label "Egipt"
  ]
  node [
    id 184
    label "zwrot"
  ]
  node [
    id 185
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 186
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 187
    label "Mozambik"
  ]
  node [
    id 188
    label "Kolumbia"
  ]
  node [
    id 189
    label "Laos"
  ]
  node [
    id 190
    label "Burundi"
  ]
  node [
    id 191
    label "Suazi"
  ]
  node [
    id 192
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 193
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 194
    label "Czechy"
  ]
  node [
    id 195
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 196
    label "Wyspy_Marshalla"
  ]
  node [
    id 197
    label "Dominika"
  ]
  node [
    id 198
    label "Trynidad_i_Tobago"
  ]
  node [
    id 199
    label "Syria"
  ]
  node [
    id 200
    label "Palau"
  ]
  node [
    id 201
    label "Gwinea_Bissau"
  ]
  node [
    id 202
    label "Liberia"
  ]
  node [
    id 203
    label "Jamajka"
  ]
  node [
    id 204
    label "Zimbabwe"
  ]
  node [
    id 205
    label "Polska"
  ]
  node [
    id 206
    label "Dominikana"
  ]
  node [
    id 207
    label "Senegal"
  ]
  node [
    id 208
    label "Togo"
  ]
  node [
    id 209
    label "Gujana"
  ]
  node [
    id 210
    label "Gruzja"
  ]
  node [
    id 211
    label "Albania"
  ]
  node [
    id 212
    label "Zair"
  ]
  node [
    id 213
    label "Meksyk"
  ]
  node [
    id 214
    label "Macedonia"
  ]
  node [
    id 215
    label "Chorwacja"
  ]
  node [
    id 216
    label "Kambod&#380;a"
  ]
  node [
    id 217
    label "Monako"
  ]
  node [
    id 218
    label "Mauritius"
  ]
  node [
    id 219
    label "Gwinea"
  ]
  node [
    id 220
    label "Mali"
  ]
  node [
    id 221
    label "Nigeria"
  ]
  node [
    id 222
    label "Kostaryka"
  ]
  node [
    id 223
    label "Hanower"
  ]
  node [
    id 224
    label "Paragwaj"
  ]
  node [
    id 225
    label "W&#322;ochy"
  ]
  node [
    id 226
    label "Seszele"
  ]
  node [
    id 227
    label "Wyspy_Salomona"
  ]
  node [
    id 228
    label "Hiszpania"
  ]
  node [
    id 229
    label "Boliwia"
  ]
  node [
    id 230
    label "Kirgistan"
  ]
  node [
    id 231
    label "Irlandia"
  ]
  node [
    id 232
    label "Czad"
  ]
  node [
    id 233
    label "Irak"
  ]
  node [
    id 234
    label "Lesoto"
  ]
  node [
    id 235
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 236
    label "Malta"
  ]
  node [
    id 237
    label "Andora"
  ]
  node [
    id 238
    label "Chiny"
  ]
  node [
    id 239
    label "Filipiny"
  ]
  node [
    id 240
    label "Antarktis"
  ]
  node [
    id 241
    label "Niemcy"
  ]
  node [
    id 242
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 243
    label "Pakistan"
  ]
  node [
    id 244
    label "terytorium"
  ]
  node [
    id 245
    label "Nikaragua"
  ]
  node [
    id 246
    label "Brazylia"
  ]
  node [
    id 247
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 248
    label "Maroko"
  ]
  node [
    id 249
    label "Portugalia"
  ]
  node [
    id 250
    label "Niger"
  ]
  node [
    id 251
    label "Kenia"
  ]
  node [
    id 252
    label "Botswana"
  ]
  node [
    id 253
    label "Fid&#380;i"
  ]
  node [
    id 254
    label "Tunezja"
  ]
  node [
    id 255
    label "Australia"
  ]
  node [
    id 256
    label "Tajlandia"
  ]
  node [
    id 257
    label "Burkina_Faso"
  ]
  node [
    id 258
    label "interior"
  ]
  node [
    id 259
    label "Tanzania"
  ]
  node [
    id 260
    label "Benin"
  ]
  node [
    id 261
    label "Indie"
  ]
  node [
    id 262
    label "&#321;otwa"
  ]
  node [
    id 263
    label "Kiribati"
  ]
  node [
    id 264
    label "Antigua_i_Barbuda"
  ]
  node [
    id 265
    label "Rodezja"
  ]
  node [
    id 266
    label "Cypr"
  ]
  node [
    id 267
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 268
    label "Peru"
  ]
  node [
    id 269
    label "Austria"
  ]
  node [
    id 270
    label "Urugwaj"
  ]
  node [
    id 271
    label "Jordania"
  ]
  node [
    id 272
    label "Grecja"
  ]
  node [
    id 273
    label "Azerbejd&#380;an"
  ]
  node [
    id 274
    label "Turcja"
  ]
  node [
    id 275
    label "Samoa"
  ]
  node [
    id 276
    label "Sudan"
  ]
  node [
    id 277
    label "Oman"
  ]
  node [
    id 278
    label "ziemia"
  ]
  node [
    id 279
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 280
    label "Uzbekistan"
  ]
  node [
    id 281
    label "Portoryko"
  ]
  node [
    id 282
    label "Honduras"
  ]
  node [
    id 283
    label "Mongolia"
  ]
  node [
    id 284
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 285
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 286
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 287
    label "Serbia"
  ]
  node [
    id 288
    label "Tajwan"
  ]
  node [
    id 289
    label "Wielka_Brytania"
  ]
  node [
    id 290
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 291
    label "Liban"
  ]
  node [
    id 292
    label "Japonia"
  ]
  node [
    id 293
    label "Ghana"
  ]
  node [
    id 294
    label "Belgia"
  ]
  node [
    id 295
    label "Bahrajn"
  ]
  node [
    id 296
    label "Mikronezja"
  ]
  node [
    id 297
    label "Etiopia"
  ]
  node [
    id 298
    label "Kuwejt"
  ]
  node [
    id 299
    label "grupa"
  ]
  node [
    id 300
    label "Bahamy"
  ]
  node [
    id 301
    label "Rosja"
  ]
  node [
    id 302
    label "Mo&#322;dawia"
  ]
  node [
    id 303
    label "Litwa"
  ]
  node [
    id 304
    label "S&#322;owenia"
  ]
  node [
    id 305
    label "Szwajcaria"
  ]
  node [
    id 306
    label "Erytrea"
  ]
  node [
    id 307
    label "Arabia_Saudyjska"
  ]
  node [
    id 308
    label "Kuba"
  ]
  node [
    id 309
    label "granica_pa&#324;stwa"
  ]
  node [
    id 310
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 311
    label "Malezja"
  ]
  node [
    id 312
    label "Korea"
  ]
  node [
    id 313
    label "Jemen"
  ]
  node [
    id 314
    label "Nowa_Zelandia"
  ]
  node [
    id 315
    label "Namibia"
  ]
  node [
    id 316
    label "Nauru"
  ]
  node [
    id 317
    label "holoarktyka"
  ]
  node [
    id 318
    label "Brunei"
  ]
  node [
    id 319
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 320
    label "Khitai"
  ]
  node [
    id 321
    label "Mauretania"
  ]
  node [
    id 322
    label "Iran"
  ]
  node [
    id 323
    label "Gambia"
  ]
  node [
    id 324
    label "Somalia"
  ]
  node [
    id 325
    label "Holandia"
  ]
  node [
    id 326
    label "Turkmenistan"
  ]
  node [
    id 327
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 328
    label "Salwador"
  ]
  node [
    id 329
    label "klatka_piersiowa"
  ]
  node [
    id 330
    label "penis"
  ]
  node [
    id 331
    label "wzg&#243;rek_&#322;onowy"
  ]
  node [
    id 332
    label "brzuch"
  ]
  node [
    id 333
    label "ow&#322;osienie_&#322;onowe"
  ]
  node [
    id 334
    label "podbrzusze"
  ]
  node [
    id 335
    label "przyroda"
  ]
  node [
    id 336
    label "l&#281;d&#378;wie"
  ]
  node [
    id 337
    label "wn&#281;trze"
  ]
  node [
    id 338
    label "cia&#322;o"
  ]
  node [
    id 339
    label "dziedzina"
  ]
  node [
    id 340
    label "powierzchnia"
  ]
  node [
    id 341
    label "macica"
  ]
  node [
    id 342
    label "pochwa"
  ]
  node [
    id 343
    label "samka"
  ]
  node [
    id 344
    label "p&#322;e&#263;_pi&#281;kna"
  ]
  node [
    id 345
    label "drogi_rodne"
  ]
  node [
    id 346
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 347
    label "zwierz&#281;"
  ]
  node [
    id 348
    label "female"
  ]
  node [
    id 349
    label "przodkini"
  ]
  node [
    id 350
    label "baba"
  ]
  node [
    id 351
    label "babulinka"
  ]
  node [
    id 352
    label "ciasto"
  ]
  node [
    id 353
    label "ro&#347;lina_zielna"
  ]
  node [
    id 354
    label "babkowate"
  ]
  node [
    id 355
    label "po&#322;o&#380;na"
  ]
  node [
    id 356
    label "dziadkowie"
  ]
  node [
    id 357
    label "ryba"
  ]
  node [
    id 358
    label "ko&#378;larz_babka"
  ]
  node [
    id 359
    label "moneta"
  ]
  node [
    id 360
    label "plantain"
  ]
  node [
    id 361
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 362
    label "set"
  ]
  node [
    id 363
    label "position"
  ]
  node [
    id 364
    label "okre&#347;li&#263;"
  ]
  node [
    id 365
    label "aim"
  ]
  node [
    id 366
    label "zaznaczy&#263;"
  ]
  node [
    id 367
    label "sign"
  ]
  node [
    id 368
    label "ustali&#263;"
  ]
  node [
    id 369
    label "wybra&#263;"
  ]
  node [
    id 370
    label "powo&#322;a&#263;"
  ]
  node [
    id 371
    label "sie&#263;_rybacka"
  ]
  node [
    id 372
    label "zu&#380;y&#263;"
  ]
  node [
    id 373
    label "wyj&#261;&#263;"
  ]
  node [
    id 374
    label "distill"
  ]
  node [
    id 375
    label "pick"
  ]
  node [
    id 376
    label "kotwica"
  ]
  node [
    id 377
    label "wskaza&#263;"
  ]
  node [
    id 378
    label "appoint"
  ]
  node [
    id 379
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 380
    label "flag"
  ]
  node [
    id 381
    label "uwydatni&#263;"
  ]
  node [
    id 382
    label "podkre&#347;li&#263;"
  ]
  node [
    id 383
    label "zdecydowa&#263;"
  ]
  node [
    id 384
    label "zrobi&#263;"
  ]
  node [
    id 385
    label "situate"
  ]
  node [
    id 386
    label "nominate"
  ]
  node [
    id 387
    label "put"
  ]
  node [
    id 388
    label "bind"
  ]
  node [
    id 389
    label "umocni&#263;"
  ]
  node [
    id 390
    label "unwrap"
  ]
  node [
    id 391
    label "gem"
  ]
  node [
    id 392
    label "kompozycja"
  ]
  node [
    id 393
    label "runda"
  ]
  node [
    id 394
    label "muzyka"
  ]
  node [
    id 395
    label "zestaw"
  ]
  node [
    id 396
    label "oskar"
  ]
  node [
    id 397
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 398
    label "konsekwencja"
  ]
  node [
    id 399
    label "prize"
  ]
  node [
    id 400
    label "trophy"
  ]
  node [
    id 401
    label "oznaczenie"
  ]
  node [
    id 402
    label "potraktowanie"
  ]
  node [
    id 403
    label "nagrodzenie"
  ]
  node [
    id 404
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 405
    label "zrobienie"
  ]
  node [
    id 406
    label "odczuwa&#263;"
  ]
  node [
    id 407
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 408
    label "skrupienie_si&#281;"
  ]
  node [
    id 409
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 410
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 411
    label "odczucie"
  ]
  node [
    id 412
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 413
    label "koszula_Dejaniry"
  ]
  node [
    id 414
    label "odczuwanie"
  ]
  node [
    id 415
    label "event"
  ]
  node [
    id 416
    label "rezultat"
  ]
  node [
    id 417
    label "skrupianie_si&#281;"
  ]
  node [
    id 418
    label "odczu&#263;"
  ]
  node [
    id 419
    label "podanie"
  ]
  node [
    id 420
    label "pokazanie"
  ]
  node [
    id 421
    label "wskaz&#243;wka"
  ]
  node [
    id 422
    label "wyja&#347;nienie"
  ]
  node [
    id 423
    label "appointment"
  ]
  node [
    id 424
    label "meaning"
  ]
  node [
    id 425
    label "education"
  ]
  node [
    id 426
    label "wybranie"
  ]
  node [
    id 427
    label "podkre&#347;lenie"
  ]
  node [
    id 428
    label "przyczyna"
  ]
  node [
    id 429
    label "wynik"
  ]
  node [
    id 430
    label "zaokr&#261;glenie"
  ]
  node [
    id 431
    label "dzia&#322;anie"
  ]
  node [
    id 432
    label "typ"
  ]
  node [
    id 433
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 434
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 435
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 436
    label "czynnik"
  ]
  node [
    id 437
    label "matuszka"
  ]
  node [
    id 438
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 439
    label "geneza"
  ]
  node [
    id 440
    label "poci&#261;ganie"
  ]
  node [
    id 441
    label "ustawienie"
  ]
  node [
    id 442
    label "danie"
  ]
  node [
    id 443
    label "narrative"
  ]
  node [
    id 444
    label "pismo"
  ]
  node [
    id 445
    label "nafaszerowanie"
  ]
  node [
    id 446
    label "tenis"
  ]
  node [
    id 447
    label "prayer"
  ]
  node [
    id 448
    label "siatk&#243;wka"
  ]
  node [
    id 449
    label "pi&#322;ka"
  ]
  node [
    id 450
    label "myth"
  ]
  node [
    id 451
    label "service"
  ]
  node [
    id 452
    label "jedzenie"
  ]
  node [
    id 453
    label "zagranie"
  ]
  node [
    id 454
    label "poinformowanie"
  ]
  node [
    id 455
    label "zaserwowanie"
  ]
  node [
    id 456
    label "opowie&#347;&#263;"
  ]
  node [
    id 457
    label "pass"
  ]
  node [
    id 458
    label "udowodnienie"
  ]
  node [
    id 459
    label "exhibit"
  ]
  node [
    id 460
    label "obniesienie"
  ]
  node [
    id 461
    label "przedstawienie"
  ]
  node [
    id 462
    label "spowodowanie"
  ]
  node [
    id 463
    label "nauczenie"
  ]
  node [
    id 464
    label "wczytanie"
  ]
  node [
    id 465
    label "wyra&#380;enie"
  ]
  node [
    id 466
    label "zu&#380;ycie"
  ]
  node [
    id 467
    label "powo&#322;anie"
  ]
  node [
    id 468
    label "powybieranie"
  ]
  node [
    id 469
    label "ustalenie"
  ]
  node [
    id 470
    label "wyj&#281;cie"
  ]
  node [
    id 471
    label "optowanie"
  ]
  node [
    id 472
    label "linia"
  ]
  node [
    id 473
    label "kreska"
  ]
  node [
    id 474
    label "stress"
  ]
  node [
    id 475
    label "accent"
  ]
  node [
    id 476
    label "narysowanie"
  ]
  node [
    id 477
    label "uzasadnienie"
  ]
  node [
    id 478
    label "fakt"
  ]
  node [
    id 479
    label "tarcza"
  ]
  node [
    id 480
    label "zegar"
  ]
  node [
    id 481
    label "solucja"
  ]
  node [
    id 482
    label "informacja"
  ]
  node [
    id 483
    label "implikowa&#263;"
  ]
  node [
    id 484
    label "explanation"
  ]
  node [
    id 485
    label "report"
  ]
  node [
    id 486
    label "zrozumia&#322;y"
  ]
  node [
    id 487
    label "apologetyk"
  ]
  node [
    id 488
    label "justyfikacja"
  ]
  node [
    id 489
    label "gossip"
  ]
  node [
    id 490
    label "sprawiciel"
  ]
  node [
    id 491
    label "nast&#261;pi&#263;"
  ]
  node [
    id 492
    label "attack"
  ]
  node [
    id 493
    label "przeby&#263;"
  ]
  node [
    id 494
    label "spell"
  ]
  node [
    id 495
    label "postara&#263;_si&#281;"
  ]
  node [
    id 496
    label "rozegra&#263;"
  ]
  node [
    id 497
    label "powiedzie&#263;"
  ]
  node [
    id 498
    label "anoint"
  ]
  node [
    id 499
    label "sport"
  ]
  node [
    id 500
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 501
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 502
    label "skrytykowa&#263;"
  ]
  node [
    id 503
    label "post&#261;pi&#263;"
  ]
  node [
    id 504
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 505
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 506
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 507
    label "zorganizowa&#263;"
  ]
  node [
    id 508
    label "wystylizowa&#263;"
  ]
  node [
    id 509
    label "cause"
  ]
  node [
    id 510
    label "przerobi&#263;"
  ]
  node [
    id 511
    label "nabra&#263;"
  ]
  node [
    id 512
    label "make"
  ]
  node [
    id 513
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 514
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 515
    label "wydali&#263;"
  ]
  node [
    id 516
    label "work"
  ]
  node [
    id 517
    label "chemia"
  ]
  node [
    id 518
    label "reakcja_chemiczna"
  ]
  node [
    id 519
    label "act"
  ]
  node [
    id 520
    label "discover"
  ]
  node [
    id 521
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 522
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 523
    label "wydoby&#263;"
  ]
  node [
    id 524
    label "poda&#263;"
  ]
  node [
    id 525
    label "express"
  ]
  node [
    id 526
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 527
    label "wyrazi&#263;"
  ]
  node [
    id 528
    label "rzekn&#261;&#263;"
  ]
  node [
    id 529
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 530
    label "convey"
  ]
  node [
    id 531
    label "zaopiniowa&#263;"
  ]
  node [
    id 532
    label "review"
  ]
  node [
    id 533
    label "oceni&#263;"
  ]
  node [
    id 534
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 535
    label "play"
  ]
  node [
    id 536
    label "przeprowadzi&#263;"
  ]
  node [
    id 537
    label "spo&#380;y&#263;"
  ]
  node [
    id 538
    label "zakosztowa&#263;"
  ]
  node [
    id 539
    label "sprawdzi&#263;"
  ]
  node [
    id 540
    label "podj&#261;&#263;"
  ]
  node [
    id 541
    label "try"
  ]
  node [
    id 542
    label "pos&#322;u&#380;y&#263;_si&#281;"
  ]
  node [
    id 543
    label "supervene"
  ]
  node [
    id 544
    label "nacisn&#261;&#263;"
  ]
  node [
    id 545
    label "gamble"
  ]
  node [
    id 546
    label "odby&#263;"
  ]
  node [
    id 547
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 548
    label "traversal"
  ]
  node [
    id 549
    label "overwhelm"
  ]
  node [
    id 550
    label "prze&#380;y&#263;"
  ]
  node [
    id 551
    label "zgrupowanie"
  ]
  node [
    id 552
    label "kultura_fizyczna"
  ]
  node [
    id 553
    label "usportowienie"
  ]
  node [
    id 554
    label "atakowa&#263;"
  ]
  node [
    id 555
    label "zaatakowanie"
  ]
  node [
    id 556
    label "atakowanie"
  ]
  node [
    id 557
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 558
    label "usportowi&#263;"
  ]
  node [
    id 559
    label "sokolstwo"
  ]
  node [
    id 560
    label "pies_rodzinny"
  ]
  node [
    id 561
    label "ast"
  ]
  node [
    id 562
    label "terier_w_typie_bull"
  ]
  node [
    id 563
    label "Bachus"
  ]
  node [
    id 564
    label "ameryka&#324;ski_staffordshire_terier"
  ]
  node [
    id 565
    label "Logan"
  ]
  node [
    id 566
    label "winoro&#347;l"
  ]
  node [
    id 567
    label "Dionizos"
  ]
  node [
    id 568
    label "samos&#261;d"
  ]
  node [
    id 569
    label "s&#261;d"
  ]
  node [
    id 570
    label "lynching"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 1
    target 393
  ]
  edge [
    source 1
    target 394
  ]
  edge [
    source 1
    target 395
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
]
