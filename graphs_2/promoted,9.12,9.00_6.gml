graph [
  node [
    id 0
    label "kolejny"
    origin "text"
  ]
  node [
    id 1
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 2
    label "zapowiada&#263;"
    origin "text"
  ]
  node [
    id 3
    label "cykl"
    origin "text"
  ]
  node [
    id 4
    label "anw"
    origin "text"
  ]
  node [
    id 5
    label "weekend"
    origin "text"
  ]
  node [
    id 6
    label "nast&#281;pnie"
  ]
  node [
    id 7
    label "inny"
  ]
  node [
    id 8
    label "nastopny"
  ]
  node [
    id 9
    label "kolejno"
  ]
  node [
    id 10
    label "kt&#243;ry&#347;"
  ]
  node [
    id 11
    label "osobno"
  ]
  node [
    id 12
    label "r&#243;&#380;ny"
  ]
  node [
    id 13
    label "inszy"
  ]
  node [
    id 14
    label "inaczej"
  ]
  node [
    id 15
    label "blok"
  ]
  node [
    id 16
    label "prawda"
  ]
  node [
    id 17
    label "znak_j&#281;zykowy"
  ]
  node [
    id 18
    label "nag&#322;&#243;wek"
  ]
  node [
    id 19
    label "szkic"
  ]
  node [
    id 20
    label "line"
  ]
  node [
    id 21
    label "fragment"
  ]
  node [
    id 22
    label "tekst"
  ]
  node [
    id 23
    label "wyr&#243;b"
  ]
  node [
    id 24
    label "rodzajnik"
  ]
  node [
    id 25
    label "dokument"
  ]
  node [
    id 26
    label "towar"
  ]
  node [
    id 27
    label "paragraf"
  ]
  node [
    id 28
    label "ekscerpcja"
  ]
  node [
    id 29
    label "j&#281;zykowo"
  ]
  node [
    id 30
    label "wypowied&#378;"
  ]
  node [
    id 31
    label "redakcja"
  ]
  node [
    id 32
    label "wytw&#243;r"
  ]
  node [
    id 33
    label "pomini&#281;cie"
  ]
  node [
    id 34
    label "dzie&#322;o"
  ]
  node [
    id 35
    label "preparacja"
  ]
  node [
    id 36
    label "odmianka"
  ]
  node [
    id 37
    label "opu&#347;ci&#263;"
  ]
  node [
    id 38
    label "koniektura"
  ]
  node [
    id 39
    label "pisa&#263;"
  ]
  node [
    id 40
    label "obelga"
  ]
  node [
    id 41
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 42
    label "utw&#243;r"
  ]
  node [
    id 43
    label "s&#261;d"
  ]
  node [
    id 44
    label "za&#322;o&#380;enie"
  ]
  node [
    id 45
    label "nieprawdziwy"
  ]
  node [
    id 46
    label "prawdziwy"
  ]
  node [
    id 47
    label "truth"
  ]
  node [
    id 48
    label "realia"
  ]
  node [
    id 49
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 50
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 51
    label "produkt"
  ]
  node [
    id 52
    label "creation"
  ]
  node [
    id 53
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 54
    label "p&#322;uczkarnia"
  ]
  node [
    id 55
    label "znakowarka"
  ]
  node [
    id 56
    label "produkcja"
  ]
  node [
    id 57
    label "tytu&#322;"
  ]
  node [
    id 58
    label "head"
  ]
  node [
    id 59
    label "znak_pisarski"
  ]
  node [
    id 60
    label "przepis"
  ]
  node [
    id 61
    label "bajt"
  ]
  node [
    id 62
    label "bloking"
  ]
  node [
    id 63
    label "j&#261;kanie"
  ]
  node [
    id 64
    label "przeszkoda"
  ]
  node [
    id 65
    label "zesp&#243;&#322;"
  ]
  node [
    id 66
    label "blokada"
  ]
  node [
    id 67
    label "bry&#322;a"
  ]
  node [
    id 68
    label "dzia&#322;"
  ]
  node [
    id 69
    label "kontynent"
  ]
  node [
    id 70
    label "nastawnia"
  ]
  node [
    id 71
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 72
    label "blockage"
  ]
  node [
    id 73
    label "zbi&#243;r"
  ]
  node [
    id 74
    label "block"
  ]
  node [
    id 75
    label "organizacja"
  ]
  node [
    id 76
    label "budynek"
  ]
  node [
    id 77
    label "start"
  ]
  node [
    id 78
    label "skorupa_ziemska"
  ]
  node [
    id 79
    label "program"
  ]
  node [
    id 80
    label "zeszyt"
  ]
  node [
    id 81
    label "grupa"
  ]
  node [
    id 82
    label "blokowisko"
  ]
  node [
    id 83
    label "barak"
  ]
  node [
    id 84
    label "stok_kontynentalny"
  ]
  node [
    id 85
    label "whole"
  ]
  node [
    id 86
    label "square"
  ]
  node [
    id 87
    label "siatk&#243;wka"
  ]
  node [
    id 88
    label "kr&#261;g"
  ]
  node [
    id 89
    label "ram&#243;wka"
  ]
  node [
    id 90
    label "zamek"
  ]
  node [
    id 91
    label "obrona"
  ]
  node [
    id 92
    label "ok&#322;adka"
  ]
  node [
    id 93
    label "bie&#380;nia"
  ]
  node [
    id 94
    label "referat"
  ]
  node [
    id 95
    label "dom_wielorodzinny"
  ]
  node [
    id 96
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 97
    label "zapis"
  ]
  node [
    id 98
    label "&#347;wiadectwo"
  ]
  node [
    id 99
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 100
    label "parafa"
  ]
  node [
    id 101
    label "plik"
  ]
  node [
    id 102
    label "raport&#243;wka"
  ]
  node [
    id 103
    label "record"
  ]
  node [
    id 104
    label "registratura"
  ]
  node [
    id 105
    label "dokumentacja"
  ]
  node [
    id 106
    label "fascyku&#322;"
  ]
  node [
    id 107
    label "writing"
  ]
  node [
    id 108
    label "sygnatariusz"
  ]
  node [
    id 109
    label "rysunek"
  ]
  node [
    id 110
    label "szkicownik"
  ]
  node [
    id 111
    label "opracowanie"
  ]
  node [
    id 112
    label "sketch"
  ]
  node [
    id 113
    label "plot"
  ]
  node [
    id 114
    label "pomys&#322;"
  ]
  node [
    id 115
    label "opowiadanie"
  ]
  node [
    id 116
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 117
    label "metka"
  ]
  node [
    id 118
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 119
    label "cz&#322;owiek"
  ]
  node [
    id 120
    label "szprycowa&#263;"
  ]
  node [
    id 121
    label "naszprycowa&#263;"
  ]
  node [
    id 122
    label "rzuca&#263;"
  ]
  node [
    id 123
    label "tandeta"
  ]
  node [
    id 124
    label "obr&#243;t_handlowy"
  ]
  node [
    id 125
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 126
    label "rzuci&#263;"
  ]
  node [
    id 127
    label "naszprycowanie"
  ]
  node [
    id 128
    label "tkanina"
  ]
  node [
    id 129
    label "szprycowanie"
  ]
  node [
    id 130
    label "za&#322;adownia"
  ]
  node [
    id 131
    label "asortyment"
  ]
  node [
    id 132
    label "&#322;&#243;dzki"
  ]
  node [
    id 133
    label "narkobiznes"
  ]
  node [
    id 134
    label "rzucenie"
  ]
  node [
    id 135
    label "rzucanie"
  ]
  node [
    id 136
    label "ostrzega&#263;"
  ]
  node [
    id 137
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 138
    label "harbinger"
  ]
  node [
    id 139
    label "og&#322;asza&#263;"
  ]
  node [
    id 140
    label "bode"
  ]
  node [
    id 141
    label "post"
  ]
  node [
    id 142
    label "informowa&#263;"
  ]
  node [
    id 143
    label "powiada&#263;"
  ]
  node [
    id 144
    label "komunikowa&#263;"
  ]
  node [
    id 145
    label "inform"
  ]
  node [
    id 146
    label "podawa&#263;"
  ]
  node [
    id 147
    label "publikowa&#263;"
  ]
  node [
    id 148
    label "obwo&#322;ywa&#263;"
  ]
  node [
    id 149
    label "announce"
  ]
  node [
    id 150
    label "caution"
  ]
  node [
    id 151
    label "uprzedza&#263;"
  ]
  node [
    id 152
    label "supply"
  ]
  node [
    id 153
    label "testify"
  ]
  node [
    id 154
    label "op&#322;aca&#263;"
  ]
  node [
    id 155
    label "by&#263;"
  ]
  node [
    id 156
    label "wyraz"
  ]
  node [
    id 157
    label "sk&#322;ada&#263;"
  ]
  node [
    id 158
    label "pracowa&#263;"
  ]
  node [
    id 159
    label "us&#322;uga"
  ]
  node [
    id 160
    label "represent"
  ]
  node [
    id 161
    label "bespeak"
  ]
  node [
    id 162
    label "opowiada&#263;"
  ]
  node [
    id 163
    label "attest"
  ]
  node [
    id 164
    label "czyni&#263;_dobro"
  ]
  node [
    id 165
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 166
    label "zachowanie"
  ]
  node [
    id 167
    label "zachowywanie"
  ]
  node [
    id 168
    label "rok_ko&#347;cielny"
  ]
  node [
    id 169
    label "czas"
  ]
  node [
    id 170
    label "praktyka"
  ]
  node [
    id 171
    label "zachowa&#263;"
  ]
  node [
    id 172
    label "zachowywa&#263;"
  ]
  node [
    id 173
    label "set"
  ]
  node [
    id 174
    label "przebieg"
  ]
  node [
    id 175
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 176
    label "miesi&#261;czka"
  ]
  node [
    id 177
    label "okres"
  ]
  node [
    id 178
    label "owulacja"
  ]
  node [
    id 179
    label "sekwencja"
  ]
  node [
    id 180
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 181
    label "edycja"
  ]
  node [
    id 182
    label "cycle"
  ]
  node [
    id 183
    label "linia"
  ]
  node [
    id 184
    label "procedura"
  ]
  node [
    id 185
    label "proces"
  ]
  node [
    id 186
    label "room"
  ]
  node [
    id 187
    label "ilo&#347;&#263;"
  ]
  node [
    id 188
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 189
    label "sequence"
  ]
  node [
    id 190
    label "praca"
  ]
  node [
    id 191
    label "poprzedzanie"
  ]
  node [
    id 192
    label "czasoprzestrze&#324;"
  ]
  node [
    id 193
    label "laba"
  ]
  node [
    id 194
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 195
    label "chronometria"
  ]
  node [
    id 196
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 197
    label "rachuba_czasu"
  ]
  node [
    id 198
    label "przep&#322;ywanie"
  ]
  node [
    id 199
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 200
    label "czasokres"
  ]
  node [
    id 201
    label "odczyt"
  ]
  node [
    id 202
    label "chwila"
  ]
  node [
    id 203
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 204
    label "dzieje"
  ]
  node [
    id 205
    label "kategoria_gramatyczna"
  ]
  node [
    id 206
    label "poprzedzenie"
  ]
  node [
    id 207
    label "trawienie"
  ]
  node [
    id 208
    label "pochodzi&#263;"
  ]
  node [
    id 209
    label "period"
  ]
  node [
    id 210
    label "okres_czasu"
  ]
  node [
    id 211
    label "poprzedza&#263;"
  ]
  node [
    id 212
    label "schy&#322;ek"
  ]
  node [
    id 213
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 214
    label "odwlekanie_si&#281;"
  ]
  node [
    id 215
    label "zegar"
  ]
  node [
    id 216
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 217
    label "czwarty_wymiar"
  ]
  node [
    id 218
    label "pochodzenie"
  ]
  node [
    id 219
    label "koniugacja"
  ]
  node [
    id 220
    label "Zeitgeist"
  ]
  node [
    id 221
    label "trawi&#263;"
  ]
  node [
    id 222
    label "pogoda"
  ]
  node [
    id 223
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 224
    label "poprzedzi&#263;"
  ]
  node [
    id 225
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 226
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 227
    label "time_period"
  ]
  node [
    id 228
    label "integer"
  ]
  node [
    id 229
    label "liczba"
  ]
  node [
    id 230
    label "zlewanie_si&#281;"
  ]
  node [
    id 231
    label "uk&#322;ad"
  ]
  node [
    id 232
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 233
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 234
    label "pe&#322;ny"
  ]
  node [
    id 235
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 236
    label "ci&#261;g"
  ]
  node [
    id 237
    label "kompozycja"
  ]
  node [
    id 238
    label "pie&#347;&#324;"
  ]
  node [
    id 239
    label "okres_amazo&#324;ski"
  ]
  node [
    id 240
    label "stater"
  ]
  node [
    id 241
    label "flow"
  ]
  node [
    id 242
    label "choroba_przyrodzona"
  ]
  node [
    id 243
    label "postglacja&#322;"
  ]
  node [
    id 244
    label "sylur"
  ]
  node [
    id 245
    label "kreda"
  ]
  node [
    id 246
    label "ordowik"
  ]
  node [
    id 247
    label "okres_hesperyjski"
  ]
  node [
    id 248
    label "paleogen"
  ]
  node [
    id 249
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 250
    label "okres_halsztacki"
  ]
  node [
    id 251
    label "riak"
  ]
  node [
    id 252
    label "czwartorz&#281;d"
  ]
  node [
    id 253
    label "podokres"
  ]
  node [
    id 254
    label "trzeciorz&#281;d"
  ]
  node [
    id 255
    label "kalim"
  ]
  node [
    id 256
    label "fala"
  ]
  node [
    id 257
    label "perm"
  ]
  node [
    id 258
    label "retoryka"
  ]
  node [
    id 259
    label "prekambr"
  ]
  node [
    id 260
    label "faza"
  ]
  node [
    id 261
    label "neogen"
  ]
  node [
    id 262
    label "pulsacja"
  ]
  node [
    id 263
    label "proces_fizjologiczny"
  ]
  node [
    id 264
    label "kambr"
  ]
  node [
    id 265
    label "kriogen"
  ]
  node [
    id 266
    label "jednostka_geologiczna"
  ]
  node [
    id 267
    label "ton"
  ]
  node [
    id 268
    label "orosir"
  ]
  node [
    id 269
    label "poprzednik"
  ]
  node [
    id 270
    label "spell"
  ]
  node [
    id 271
    label "interstadia&#322;"
  ]
  node [
    id 272
    label "ektas"
  ]
  node [
    id 273
    label "sider"
  ]
  node [
    id 274
    label "epoka"
  ]
  node [
    id 275
    label "rok_akademicki"
  ]
  node [
    id 276
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 277
    label "ciota"
  ]
  node [
    id 278
    label "pierwszorz&#281;d"
  ]
  node [
    id 279
    label "okres_noachijski"
  ]
  node [
    id 280
    label "ediakar"
  ]
  node [
    id 281
    label "zdanie"
  ]
  node [
    id 282
    label "nast&#281;pnik"
  ]
  node [
    id 283
    label "condition"
  ]
  node [
    id 284
    label "jura"
  ]
  node [
    id 285
    label "glacja&#322;"
  ]
  node [
    id 286
    label "sten"
  ]
  node [
    id 287
    label "era"
  ]
  node [
    id 288
    label "trias"
  ]
  node [
    id 289
    label "p&#243;&#322;okres"
  ]
  node [
    id 290
    label "rok_szkolny"
  ]
  node [
    id 291
    label "dewon"
  ]
  node [
    id 292
    label "karbon"
  ]
  node [
    id 293
    label "izochronizm"
  ]
  node [
    id 294
    label "preglacja&#322;"
  ]
  node [
    id 295
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 296
    label "drugorz&#281;d"
  ]
  node [
    id 297
    label "semester"
  ]
  node [
    id 298
    label "gem"
  ]
  node [
    id 299
    label "runda"
  ]
  node [
    id 300
    label "muzyka"
  ]
  node [
    id 301
    label "zestaw"
  ]
  node [
    id 302
    label "egzemplarz"
  ]
  node [
    id 303
    label "impression"
  ]
  node [
    id 304
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 305
    label "odmiana"
  ]
  node [
    id 306
    label "notification"
  ]
  node [
    id 307
    label "zmiana"
  ]
  node [
    id 308
    label "proces_biologiczny"
  ]
  node [
    id 309
    label "niedziela"
  ]
  node [
    id 310
    label "tydzie&#324;"
  ]
  node [
    id 311
    label "sobota"
  ]
  node [
    id 312
    label "Wielka_Sobota"
  ]
  node [
    id 313
    label "dzie&#324;_powszedni"
  ]
  node [
    id 314
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 315
    label "Wielkanoc"
  ]
  node [
    id 316
    label "dzie&#324;_&#347;wi&#281;ty"
  ]
  node [
    id 317
    label "Niedziela_Palmowa"
  ]
  node [
    id 318
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 319
    label "Zes&#322;anie_Ducha_&#346;wi&#281;tego"
  ]
  node [
    id 320
    label "niedziela_przewodnia"
  ]
  node [
    id 321
    label "bia&#322;a_niedziela"
  ]
  node [
    id 322
    label "doba"
  ]
  node [
    id 323
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 324
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 325
    label "miesi&#261;c"
  ]
  node [
    id 326
    label "s&#322;oneczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 231
    target 326
  ]
]
