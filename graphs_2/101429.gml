graph [
  node [
    id 0
    label "sos"
    origin "text"
  ]
  node [
    id 1
    label "bearne&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "mieszanina"
  ]
  node [
    id 3
    label "zaklepka"
  ]
  node [
    id 4
    label "gulasz"
  ]
  node [
    id 5
    label "jedzenie"
  ]
  node [
    id 6
    label "zbi&#243;r"
  ]
  node [
    id 7
    label "frakcja"
  ]
  node [
    id 8
    label "substancja"
  ]
  node [
    id 9
    label "synteza"
  ]
  node [
    id 10
    label "zatruwanie_si&#281;"
  ]
  node [
    id 11
    label "przejadanie_si&#281;"
  ]
  node [
    id 12
    label "szama"
  ]
  node [
    id 13
    label "koryto"
  ]
  node [
    id 14
    label "rzecz"
  ]
  node [
    id 15
    label "odpasanie_si&#281;"
  ]
  node [
    id 16
    label "eating"
  ]
  node [
    id 17
    label "jadanie"
  ]
  node [
    id 18
    label "posilenie"
  ]
  node [
    id 19
    label "wpieprzanie"
  ]
  node [
    id 20
    label "wmuszanie"
  ]
  node [
    id 21
    label "robienie"
  ]
  node [
    id 22
    label "wiwenda"
  ]
  node [
    id 23
    label "polowanie"
  ]
  node [
    id 24
    label "ufetowanie_si&#281;"
  ]
  node [
    id 25
    label "wyjadanie"
  ]
  node [
    id 26
    label "smakowanie"
  ]
  node [
    id 27
    label "przejedzenie"
  ]
  node [
    id 28
    label "jad&#322;o"
  ]
  node [
    id 29
    label "mlaskanie"
  ]
  node [
    id 30
    label "papusianie"
  ]
  node [
    id 31
    label "podawa&#263;"
  ]
  node [
    id 32
    label "poda&#263;"
  ]
  node [
    id 33
    label "posilanie"
  ]
  node [
    id 34
    label "czynno&#347;&#263;"
  ]
  node [
    id 35
    label "podawanie"
  ]
  node [
    id 36
    label "przejedzenie_si&#281;"
  ]
  node [
    id 37
    label "&#380;arcie"
  ]
  node [
    id 38
    label "odpasienie_si&#281;"
  ]
  node [
    id 39
    label "podanie"
  ]
  node [
    id 40
    label "wyjedzenie"
  ]
  node [
    id 41
    label "przejadanie"
  ]
  node [
    id 42
    label "objadanie"
  ]
  node [
    id 43
    label "potrawa"
  ]
  node [
    id 44
    label "zupa"
  ]
  node [
    id 45
    label "zag&#281;stnik"
  ]
  node [
    id 46
    label "zaprawa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
]
