graph [
  node [
    id 0
    label "kampania"
    origin "text"
  ]
  node [
    id 1
    label "zdrowotny"
    origin "text"
  ]
  node [
    id 2
    label "ministerstwo"
    origin "text"
  ]
  node [
    id 3
    label "zdrowie"
    origin "text"
  ]
  node [
    id 4
    label "przeciw"
    origin "text"
  ]
  node [
    id 5
    label "oty&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "dzia&#322;anie"
  ]
  node [
    id 7
    label "campaign"
  ]
  node [
    id 8
    label "wydarzenie"
  ]
  node [
    id 9
    label "akcja"
  ]
  node [
    id 10
    label "Kampania_Wrze&#347;niowa"
  ]
  node [
    id 11
    label "przebiec"
  ]
  node [
    id 12
    label "charakter"
  ]
  node [
    id 13
    label "czynno&#347;&#263;"
  ]
  node [
    id 14
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 15
    label "motyw"
  ]
  node [
    id 16
    label "przebiegni&#281;cie"
  ]
  node [
    id 17
    label "fabu&#322;a"
  ]
  node [
    id 18
    label "dywidenda"
  ]
  node [
    id 19
    label "przebieg"
  ]
  node [
    id 20
    label "operacja"
  ]
  node [
    id 21
    label "zagrywka"
  ]
  node [
    id 22
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 23
    label "udzia&#322;"
  ]
  node [
    id 24
    label "commotion"
  ]
  node [
    id 25
    label "occupation"
  ]
  node [
    id 26
    label "gra"
  ]
  node [
    id 27
    label "jazda"
  ]
  node [
    id 28
    label "czyn"
  ]
  node [
    id 29
    label "stock"
  ]
  node [
    id 30
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 31
    label "w&#281;ze&#322;"
  ]
  node [
    id 32
    label "wysoko&#347;&#263;"
  ]
  node [
    id 33
    label "instrument_strunowy"
  ]
  node [
    id 34
    label "infimum"
  ]
  node [
    id 35
    label "powodowanie"
  ]
  node [
    id 36
    label "liczenie"
  ]
  node [
    id 37
    label "cz&#322;owiek"
  ]
  node [
    id 38
    label "skutek"
  ]
  node [
    id 39
    label "podzia&#322;anie"
  ]
  node [
    id 40
    label "supremum"
  ]
  node [
    id 41
    label "uruchamianie"
  ]
  node [
    id 42
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 43
    label "jednostka"
  ]
  node [
    id 44
    label "hipnotyzowanie"
  ]
  node [
    id 45
    label "robienie"
  ]
  node [
    id 46
    label "uruchomienie"
  ]
  node [
    id 47
    label "nakr&#281;canie"
  ]
  node [
    id 48
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 49
    label "matematyka"
  ]
  node [
    id 50
    label "reakcja_chemiczna"
  ]
  node [
    id 51
    label "tr&#243;jstronny"
  ]
  node [
    id 52
    label "natural_process"
  ]
  node [
    id 53
    label "nakr&#281;cenie"
  ]
  node [
    id 54
    label "zatrzymanie"
  ]
  node [
    id 55
    label "wp&#322;yw"
  ]
  node [
    id 56
    label "rzut"
  ]
  node [
    id 57
    label "podtrzymywanie"
  ]
  node [
    id 58
    label "w&#322;&#261;czanie"
  ]
  node [
    id 59
    label "liczy&#263;"
  ]
  node [
    id 60
    label "operation"
  ]
  node [
    id 61
    label "rezultat"
  ]
  node [
    id 62
    label "dzianie_si&#281;"
  ]
  node [
    id 63
    label "zadzia&#322;anie"
  ]
  node [
    id 64
    label "priorytet"
  ]
  node [
    id 65
    label "bycie"
  ]
  node [
    id 66
    label "kres"
  ]
  node [
    id 67
    label "rozpocz&#281;cie"
  ]
  node [
    id 68
    label "docieranie"
  ]
  node [
    id 69
    label "funkcja"
  ]
  node [
    id 70
    label "czynny"
  ]
  node [
    id 71
    label "impact"
  ]
  node [
    id 72
    label "oferta"
  ]
  node [
    id 73
    label "zako&#324;czenie"
  ]
  node [
    id 74
    label "act"
  ]
  node [
    id 75
    label "wdzieranie_si&#281;"
  ]
  node [
    id 76
    label "w&#322;&#261;czenie"
  ]
  node [
    id 77
    label "zdrowy"
  ]
  node [
    id 78
    label "zdrowotnie"
  ]
  node [
    id 79
    label "dobry"
  ]
  node [
    id 80
    label "prozdrowotny"
  ]
  node [
    id 81
    label "dobrze"
  ]
  node [
    id 82
    label "zdrowo"
  ]
  node [
    id 83
    label "wyzdrowienie"
  ]
  node [
    id 84
    label "wyleczenie_si&#281;"
  ]
  node [
    id 85
    label "uzdrowienie"
  ]
  node [
    id 86
    label "silny"
  ]
  node [
    id 87
    label "zr&#243;wnowa&#380;ony"
  ]
  node [
    id 88
    label "normalny"
  ]
  node [
    id 89
    label "rozs&#261;dny"
  ]
  node [
    id 90
    label "korzystny"
  ]
  node [
    id 91
    label "zdrowienie"
  ]
  node [
    id 92
    label "solidny"
  ]
  node [
    id 93
    label "uzdrawianie"
  ]
  node [
    id 94
    label "dobroczynny"
  ]
  node [
    id 95
    label "czw&#243;rka"
  ]
  node [
    id 96
    label "spokojny"
  ]
  node [
    id 97
    label "skuteczny"
  ]
  node [
    id 98
    label "&#347;mieszny"
  ]
  node [
    id 99
    label "mi&#322;y"
  ]
  node [
    id 100
    label "grzeczny"
  ]
  node [
    id 101
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 102
    label "powitanie"
  ]
  node [
    id 103
    label "ca&#322;y"
  ]
  node [
    id 104
    label "zwrot"
  ]
  node [
    id 105
    label "pomy&#347;lny"
  ]
  node [
    id 106
    label "moralny"
  ]
  node [
    id 107
    label "drogi"
  ]
  node [
    id 108
    label "pozytywny"
  ]
  node [
    id 109
    label "odpowiedni"
  ]
  node [
    id 110
    label "pos&#322;uszny"
  ]
  node [
    id 111
    label "departament"
  ]
  node [
    id 112
    label "urz&#261;d"
  ]
  node [
    id 113
    label "NKWD"
  ]
  node [
    id 114
    label "ministerium"
  ]
  node [
    id 115
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 116
    label "MSW"
  ]
  node [
    id 117
    label "resort"
  ]
  node [
    id 118
    label "stanowisko"
  ]
  node [
    id 119
    label "position"
  ]
  node [
    id 120
    label "instytucja"
  ]
  node [
    id 121
    label "siedziba"
  ]
  node [
    id 122
    label "organ"
  ]
  node [
    id 123
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 124
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 125
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 126
    label "mianowaniec"
  ]
  node [
    id 127
    label "dzia&#322;"
  ]
  node [
    id 128
    label "okienko"
  ]
  node [
    id 129
    label "w&#322;adza"
  ]
  node [
    id 130
    label "jednostka_organizacyjna"
  ]
  node [
    id 131
    label "relation"
  ]
  node [
    id 132
    label "podsekcja"
  ]
  node [
    id 133
    label "ministry"
  ]
  node [
    id 134
    label "Martynika"
  ]
  node [
    id 135
    label "Gwadelupa"
  ]
  node [
    id 136
    label "Moza"
  ]
  node [
    id 137
    label "jednostka_administracyjna"
  ]
  node [
    id 138
    label "os&#322;abianie"
  ]
  node [
    id 139
    label "kondycja"
  ]
  node [
    id 140
    label "os&#322;abienie"
  ]
  node [
    id 141
    label "zniszczenie"
  ]
  node [
    id 142
    label "os&#322;abia&#263;"
  ]
  node [
    id 143
    label "zedrze&#263;"
  ]
  node [
    id 144
    label "niszczenie"
  ]
  node [
    id 145
    label "os&#322;abi&#263;"
  ]
  node [
    id 146
    label "soundness"
  ]
  node [
    id 147
    label "stan"
  ]
  node [
    id 148
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 149
    label "niszczy&#263;"
  ]
  node [
    id 150
    label "zdarcie"
  ]
  node [
    id 151
    label "firmness"
  ]
  node [
    id 152
    label "cecha"
  ]
  node [
    id 153
    label "rozsypanie_si&#281;"
  ]
  node [
    id 154
    label "zniszczy&#263;"
  ]
  node [
    id 155
    label "Ohio"
  ]
  node [
    id 156
    label "wci&#281;cie"
  ]
  node [
    id 157
    label "Nowy_York"
  ]
  node [
    id 158
    label "warstwa"
  ]
  node [
    id 159
    label "samopoczucie"
  ]
  node [
    id 160
    label "Illinois"
  ]
  node [
    id 161
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 162
    label "state"
  ]
  node [
    id 163
    label "Jukatan"
  ]
  node [
    id 164
    label "Kalifornia"
  ]
  node [
    id 165
    label "Wirginia"
  ]
  node [
    id 166
    label "wektor"
  ]
  node [
    id 167
    label "by&#263;"
  ]
  node [
    id 168
    label "Goa"
  ]
  node [
    id 169
    label "Teksas"
  ]
  node [
    id 170
    label "Waszyngton"
  ]
  node [
    id 171
    label "miejsce"
  ]
  node [
    id 172
    label "Massachusetts"
  ]
  node [
    id 173
    label "Alaska"
  ]
  node [
    id 174
    label "Arakan"
  ]
  node [
    id 175
    label "Hawaje"
  ]
  node [
    id 176
    label "Maryland"
  ]
  node [
    id 177
    label "punkt"
  ]
  node [
    id 178
    label "Michigan"
  ]
  node [
    id 179
    label "Arizona"
  ]
  node [
    id 180
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 181
    label "Georgia"
  ]
  node [
    id 182
    label "poziom"
  ]
  node [
    id 183
    label "Pensylwania"
  ]
  node [
    id 184
    label "shape"
  ]
  node [
    id 185
    label "Luizjana"
  ]
  node [
    id 186
    label "Nowy_Meksyk"
  ]
  node [
    id 187
    label "Alabama"
  ]
  node [
    id 188
    label "ilo&#347;&#263;"
  ]
  node [
    id 189
    label "Kansas"
  ]
  node [
    id 190
    label "Oregon"
  ]
  node [
    id 191
    label "Oklahoma"
  ]
  node [
    id 192
    label "Floryda"
  ]
  node [
    id 193
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 194
    label "charakterystyka"
  ]
  node [
    id 195
    label "m&#322;ot"
  ]
  node [
    id 196
    label "znak"
  ]
  node [
    id 197
    label "drzewo"
  ]
  node [
    id 198
    label "pr&#243;ba"
  ]
  node [
    id 199
    label "attribute"
  ]
  node [
    id 200
    label "marka"
  ]
  node [
    id 201
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 202
    label "dyspozycja"
  ]
  node [
    id 203
    label "situation"
  ]
  node [
    id 204
    label "rank"
  ]
  node [
    id 205
    label "zdolno&#347;&#263;"
  ]
  node [
    id 206
    label "sytuacja"
  ]
  node [
    id 207
    label "zaszkodzi&#263;"
  ]
  node [
    id 208
    label "kondycja_fizyczna"
  ]
  node [
    id 209
    label "zu&#380;y&#263;"
  ]
  node [
    id 210
    label "spoil"
  ]
  node [
    id 211
    label "spowodowa&#263;"
  ]
  node [
    id 212
    label "consume"
  ]
  node [
    id 213
    label "pamper"
  ]
  node [
    id 214
    label "wygra&#263;"
  ]
  node [
    id 215
    label "destruction"
  ]
  node [
    id 216
    label "pl&#261;drowanie"
  ]
  node [
    id 217
    label "ravaging"
  ]
  node [
    id 218
    label "gnojenie"
  ]
  node [
    id 219
    label "szkodzenie"
  ]
  node [
    id 220
    label "pustoszenie"
  ]
  node [
    id 221
    label "decay"
  ]
  node [
    id 222
    label "poniewieranie_si&#281;"
  ]
  node [
    id 223
    label "devastation"
  ]
  node [
    id 224
    label "zu&#380;ywanie"
  ]
  node [
    id 225
    label "stawanie_si&#281;"
  ]
  node [
    id 226
    label "wear"
  ]
  node [
    id 227
    label "zarobi&#263;"
  ]
  node [
    id 228
    label "nadwyr&#281;&#380;y&#263;"
  ]
  node [
    id 229
    label "wzi&#261;&#263;"
  ]
  node [
    id 230
    label "zrani&#263;"
  ]
  node [
    id 231
    label "gard&#322;o"
  ]
  node [
    id 232
    label "&#347;ci&#261;gn&#261;&#263;"
  ]
  node [
    id 233
    label "suppress"
  ]
  node [
    id 234
    label "robi&#263;"
  ]
  node [
    id 235
    label "powodowa&#263;"
  ]
  node [
    id 236
    label "zmniejsza&#263;"
  ]
  node [
    id 237
    label "bate"
  ]
  node [
    id 238
    label "health"
  ]
  node [
    id 239
    label "destroy"
  ]
  node [
    id 240
    label "uszkadza&#263;"
  ]
  node [
    id 241
    label "szkodzi&#263;"
  ]
  node [
    id 242
    label "mar"
  ]
  node [
    id 243
    label "wygrywa&#263;"
  ]
  node [
    id 244
    label "reduce"
  ]
  node [
    id 245
    label "zmniejszy&#263;"
  ]
  node [
    id 246
    label "cushion"
  ]
  node [
    id 247
    label "doznanie"
  ]
  node [
    id 248
    label "fatigue_duty"
  ]
  node [
    id 249
    label "zmniejszenie"
  ]
  node [
    id 250
    label "spowodowanie"
  ]
  node [
    id 251
    label "infirmity"
  ]
  node [
    id 252
    label "s&#322;abszy"
  ]
  node [
    id 253
    label "pogorszenie"
  ]
  node [
    id 254
    label "de-escalation"
  ]
  node [
    id 255
    label "debilitation"
  ]
  node [
    id 256
    label "zmniejszanie"
  ]
  node [
    id 257
    label "pogarszanie"
  ]
  node [
    id 258
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 259
    label "nadwyr&#281;&#380;enie"
  ]
  node [
    id 260
    label "attrition"
  ]
  node [
    id 261
    label "zranienie"
  ]
  node [
    id 262
    label "s&#322;ony"
  ]
  node [
    id 263
    label "wzi&#281;cie"
  ]
  node [
    id 264
    label "zu&#380;ycie"
  ]
  node [
    id 265
    label "zaszkodzenie"
  ]
  node [
    id 266
    label "podpalenie"
  ]
  node [
    id 267
    label "strata"
  ]
  node [
    id 268
    label "spl&#261;drowanie"
  ]
  node [
    id 269
    label "poniszczenie"
  ]
  node [
    id 270
    label "ruin"
  ]
  node [
    id 271
    label "stanie_si&#281;"
  ]
  node [
    id 272
    label "poniszczenie_si&#281;"
  ]
  node [
    id 273
    label "grubo&#347;&#263;"
  ]
  node [
    id 274
    label "rozmiar"
  ]
  node [
    id 275
    label "largeness"
  ]
  node [
    id 276
    label "nadwaga"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
]
