graph [
  node [
    id 0
    label "obszerny"
    origin "text"
  ]
  node [
    id 1
    label "rozmowa"
    origin "text"
  ]
  node [
    id 2
    label "ksi&#281;ga_habakuka"
    origin "text"
  ]
  node [
    id 3
    label "zenon"
    origin "text"
  ]
  node [
    id 4
    label "piechem"
    origin "text"
  ]
  node [
    id 5
    label "profesor"
    origin "text"
  ]
  node [
    id 6
    label "temat"
    origin "text"
  ]
  node [
    id 7
    label "herb"
    origin "text"
  ]
  node [
    id 8
    label "krak&#243;w"
    origin "text"
  ]
  node [
    id 9
    label "obecna"
    origin "text"
  ]
  node [
    id 10
    label "tendencja"
    origin "text"
  ]
  node [
    id 11
    label "magistrat"
    origin "text"
  ]
  node [
    id 12
    label "miejski"
    origin "text"
  ]
  node [
    id 13
    label "usuwa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "d&#322;ugi"
  ]
  node [
    id 15
    label "du&#380;y"
  ]
  node [
    id 16
    label "rozdeptanie"
  ]
  node [
    id 17
    label "rozdeptywanie"
  ]
  node [
    id 18
    label "obszernie"
  ]
  node [
    id 19
    label "lu&#378;no"
  ]
  node [
    id 20
    label "daleki"
  ]
  node [
    id 21
    label "ruch"
  ]
  node [
    id 22
    label "d&#322;ugo"
  ]
  node [
    id 23
    label "doros&#322;y"
  ]
  node [
    id 24
    label "znaczny"
  ]
  node [
    id 25
    label "niema&#322;o"
  ]
  node [
    id 26
    label "wiele"
  ]
  node [
    id 27
    label "rozwini&#281;ty"
  ]
  node [
    id 28
    label "dorodny"
  ]
  node [
    id 29
    label "wa&#380;ny"
  ]
  node [
    id 30
    label "prawdziwy"
  ]
  node [
    id 31
    label "du&#380;o"
  ]
  node [
    id 32
    label "lu&#378;ny"
  ]
  node [
    id 33
    label "rozci&#261;ganie"
  ]
  node [
    id 34
    label "niszczenie"
  ]
  node [
    id 35
    label "rozprowadzanie"
  ]
  node [
    id 36
    label "rozci&#261;gni&#281;cie"
  ]
  node [
    id 37
    label "rozprowadzenie"
  ]
  node [
    id 38
    label "zniszczenie"
  ]
  node [
    id 39
    label "lekko"
  ]
  node [
    id 40
    label "&#322;atwo"
  ]
  node [
    id 41
    label "odlegle"
  ]
  node [
    id 42
    label "thinly"
  ]
  node [
    id 43
    label "przyjemnie"
  ]
  node [
    id 44
    label "nieformalnie"
  ]
  node [
    id 45
    label "cisza"
  ]
  node [
    id 46
    label "odpowied&#378;"
  ]
  node [
    id 47
    label "rozhowor"
  ]
  node [
    id 48
    label "discussion"
  ]
  node [
    id 49
    label "czynno&#347;&#263;"
  ]
  node [
    id 50
    label "activity"
  ]
  node [
    id 51
    label "bezproblemowy"
  ]
  node [
    id 52
    label "wydarzenie"
  ]
  node [
    id 53
    label "g&#322;adki"
  ]
  node [
    id 54
    label "cicha_praca"
  ]
  node [
    id 55
    label "przerwa"
  ]
  node [
    id 56
    label "cicha_msza"
  ]
  node [
    id 57
    label "pok&#243;j"
  ]
  node [
    id 58
    label "motionlessness"
  ]
  node [
    id 59
    label "spok&#243;j"
  ]
  node [
    id 60
    label "zjawisko"
  ]
  node [
    id 61
    label "cecha"
  ]
  node [
    id 62
    label "czas"
  ]
  node [
    id 63
    label "ci&#261;g"
  ]
  node [
    id 64
    label "tajemno&#347;&#263;"
  ]
  node [
    id 65
    label "peace"
  ]
  node [
    id 66
    label "cicha_modlitwa"
  ]
  node [
    id 67
    label "react"
  ]
  node [
    id 68
    label "replica"
  ]
  node [
    id 69
    label "wyj&#347;cie"
  ]
  node [
    id 70
    label "respondent"
  ]
  node [
    id 71
    label "dokument"
  ]
  node [
    id 72
    label "reakcja"
  ]
  node [
    id 73
    label "nauczyciel"
  ]
  node [
    id 74
    label "stopie&#324;_naukowy"
  ]
  node [
    id 75
    label "nauczyciel_akademicki"
  ]
  node [
    id 76
    label "tytu&#322;"
  ]
  node [
    id 77
    label "profesura"
  ]
  node [
    id 78
    label "konsulent"
  ]
  node [
    id 79
    label "wirtuoz"
  ]
  node [
    id 80
    label "stan"
  ]
  node [
    id 81
    label "stanowisko"
  ]
  node [
    id 82
    label "professorship"
  ]
  node [
    id 83
    label "debit"
  ]
  node [
    id 84
    label "redaktor"
  ]
  node [
    id 85
    label "druk"
  ]
  node [
    id 86
    label "publikacja"
  ]
  node [
    id 87
    label "nadtytu&#322;"
  ]
  node [
    id 88
    label "szata_graficzna"
  ]
  node [
    id 89
    label "tytulatura"
  ]
  node [
    id 90
    label "wydawa&#263;"
  ]
  node [
    id 91
    label "elevation"
  ]
  node [
    id 92
    label "wyda&#263;"
  ]
  node [
    id 93
    label "mianowaniec"
  ]
  node [
    id 94
    label "poster"
  ]
  node [
    id 95
    label "nazwa"
  ]
  node [
    id 96
    label "podtytu&#322;"
  ]
  node [
    id 97
    label "wymiatacz"
  ]
  node [
    id 98
    label "gigant"
  ]
  node [
    id 99
    label "maestro"
  ]
  node [
    id 100
    label "mistrz"
  ]
  node [
    id 101
    label "instrumentalista"
  ]
  node [
    id 102
    label "ekspert"
  ]
  node [
    id 103
    label "kraj_zwi&#261;zkowy"
  ]
  node [
    id 104
    label "Austria"
  ]
  node [
    id 105
    label "doradca"
  ]
  node [
    id 106
    label "belfer"
  ]
  node [
    id 107
    label "kszta&#322;ciciel"
  ]
  node [
    id 108
    label "preceptor"
  ]
  node [
    id 109
    label "pedagog"
  ]
  node [
    id 110
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 111
    label "szkolnik"
  ]
  node [
    id 112
    label "popularyzator"
  ]
  node [
    id 113
    label "sprawa"
  ]
  node [
    id 114
    label "wyraz_pochodny"
  ]
  node [
    id 115
    label "zboczenie"
  ]
  node [
    id 116
    label "om&#243;wienie"
  ]
  node [
    id 117
    label "rzecz"
  ]
  node [
    id 118
    label "omawia&#263;"
  ]
  node [
    id 119
    label "fraza"
  ]
  node [
    id 120
    label "tre&#347;&#263;"
  ]
  node [
    id 121
    label "entity"
  ]
  node [
    id 122
    label "forum"
  ]
  node [
    id 123
    label "topik"
  ]
  node [
    id 124
    label "tematyka"
  ]
  node [
    id 125
    label "w&#261;tek"
  ]
  node [
    id 126
    label "zbaczanie"
  ]
  node [
    id 127
    label "forma"
  ]
  node [
    id 128
    label "om&#243;wi&#263;"
  ]
  node [
    id 129
    label "omawianie"
  ]
  node [
    id 130
    label "melodia"
  ]
  node [
    id 131
    label "otoczka"
  ]
  node [
    id 132
    label "istota"
  ]
  node [
    id 133
    label "zbacza&#263;"
  ]
  node [
    id 134
    label "zboczy&#263;"
  ]
  node [
    id 135
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 136
    label "zbi&#243;r"
  ]
  node [
    id 137
    label "wypowiedzenie"
  ]
  node [
    id 138
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 139
    label "zdanie"
  ]
  node [
    id 140
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 141
    label "motyw"
  ]
  node [
    id 142
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 143
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 144
    label "informacja"
  ]
  node [
    id 145
    label "zawarto&#347;&#263;"
  ]
  node [
    id 146
    label "kszta&#322;t"
  ]
  node [
    id 147
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 148
    label "jednostka_systematyczna"
  ]
  node [
    id 149
    label "poznanie"
  ]
  node [
    id 150
    label "leksem"
  ]
  node [
    id 151
    label "dzie&#322;o"
  ]
  node [
    id 152
    label "blaszka"
  ]
  node [
    id 153
    label "poj&#281;cie"
  ]
  node [
    id 154
    label "kantyzm"
  ]
  node [
    id 155
    label "zdolno&#347;&#263;"
  ]
  node [
    id 156
    label "do&#322;ek"
  ]
  node [
    id 157
    label "gwiazda"
  ]
  node [
    id 158
    label "formality"
  ]
  node [
    id 159
    label "struktura"
  ]
  node [
    id 160
    label "wygl&#261;d"
  ]
  node [
    id 161
    label "mode"
  ]
  node [
    id 162
    label "morfem"
  ]
  node [
    id 163
    label "rdze&#324;"
  ]
  node [
    id 164
    label "posta&#263;"
  ]
  node [
    id 165
    label "kielich"
  ]
  node [
    id 166
    label "ornamentyka"
  ]
  node [
    id 167
    label "pasmo"
  ]
  node [
    id 168
    label "zwyczaj"
  ]
  node [
    id 169
    label "punkt_widzenia"
  ]
  node [
    id 170
    label "g&#322;owa"
  ]
  node [
    id 171
    label "naczynie"
  ]
  node [
    id 172
    label "p&#322;at"
  ]
  node [
    id 173
    label "maszyna_drukarska"
  ]
  node [
    id 174
    label "obiekt"
  ]
  node [
    id 175
    label "style"
  ]
  node [
    id 176
    label "linearno&#347;&#263;"
  ]
  node [
    id 177
    label "wyra&#380;enie"
  ]
  node [
    id 178
    label "formacja"
  ]
  node [
    id 179
    label "spirala"
  ]
  node [
    id 180
    label "dyspozycja"
  ]
  node [
    id 181
    label "odmiana"
  ]
  node [
    id 182
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 183
    label "wz&#243;r"
  ]
  node [
    id 184
    label "October"
  ]
  node [
    id 185
    label "creation"
  ]
  node [
    id 186
    label "p&#281;tla"
  ]
  node [
    id 187
    label "arystotelizm"
  ]
  node [
    id 188
    label "szablon"
  ]
  node [
    id 189
    label "miniatura"
  ]
  node [
    id 190
    label "zanucenie"
  ]
  node [
    id 191
    label "nuta"
  ]
  node [
    id 192
    label "zakosztowa&#263;"
  ]
  node [
    id 193
    label "zajawka"
  ]
  node [
    id 194
    label "zanuci&#263;"
  ]
  node [
    id 195
    label "emocja"
  ]
  node [
    id 196
    label "oskoma"
  ]
  node [
    id 197
    label "melika"
  ]
  node [
    id 198
    label "nucenie"
  ]
  node [
    id 199
    label "nuci&#263;"
  ]
  node [
    id 200
    label "brzmienie"
  ]
  node [
    id 201
    label "taste"
  ]
  node [
    id 202
    label "muzyka"
  ]
  node [
    id 203
    label "inclination"
  ]
  node [
    id 204
    label "charakterystyka"
  ]
  node [
    id 205
    label "m&#322;ot"
  ]
  node [
    id 206
    label "znak"
  ]
  node [
    id 207
    label "drzewo"
  ]
  node [
    id 208
    label "pr&#243;ba"
  ]
  node [
    id 209
    label "attribute"
  ]
  node [
    id 210
    label "marka"
  ]
  node [
    id 211
    label "mentalno&#347;&#263;"
  ]
  node [
    id 212
    label "superego"
  ]
  node [
    id 213
    label "psychika"
  ]
  node [
    id 214
    label "znaczenie"
  ]
  node [
    id 215
    label "wn&#281;trze"
  ]
  node [
    id 216
    label "charakter"
  ]
  node [
    id 217
    label "matter"
  ]
  node [
    id 218
    label "splot"
  ]
  node [
    id 219
    label "wytw&#243;r"
  ]
  node [
    id 220
    label "ceg&#322;a"
  ]
  node [
    id 221
    label "socket"
  ]
  node [
    id 222
    label "rozmieszczenie"
  ]
  node [
    id 223
    label "fabu&#322;a"
  ]
  node [
    id 224
    label "okrywa"
  ]
  node [
    id 225
    label "kontekst"
  ]
  node [
    id 226
    label "object"
  ]
  node [
    id 227
    label "przedmiot"
  ]
  node [
    id 228
    label "wpadni&#281;cie"
  ]
  node [
    id 229
    label "mienie"
  ]
  node [
    id 230
    label "przyroda"
  ]
  node [
    id 231
    label "kultura"
  ]
  node [
    id 232
    label "wpa&#347;&#263;"
  ]
  node [
    id 233
    label "wpadanie"
  ]
  node [
    id 234
    label "wpada&#263;"
  ]
  node [
    id 235
    label "rozpatrywanie"
  ]
  node [
    id 236
    label "dyskutowanie"
  ]
  node [
    id 237
    label "omowny"
  ]
  node [
    id 238
    label "figura_stylistyczna"
  ]
  node [
    id 239
    label "sformu&#322;owanie"
  ]
  node [
    id 240
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 241
    label "odchodzenie"
  ]
  node [
    id 242
    label "aberrance"
  ]
  node [
    id 243
    label "swerve"
  ]
  node [
    id 244
    label "kierunek"
  ]
  node [
    id 245
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 246
    label "distract"
  ]
  node [
    id 247
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 248
    label "odej&#347;&#263;"
  ]
  node [
    id 249
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 250
    label "twist"
  ]
  node [
    id 251
    label "zmieni&#263;"
  ]
  node [
    id 252
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 253
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 254
    label "przedyskutowa&#263;"
  ]
  node [
    id 255
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 256
    label "publicize"
  ]
  node [
    id 257
    label "digress"
  ]
  node [
    id 258
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 259
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 260
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 261
    label "odchodzi&#263;"
  ]
  node [
    id 262
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 263
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 264
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 265
    label "perversion"
  ]
  node [
    id 266
    label "death"
  ]
  node [
    id 267
    label "odej&#347;cie"
  ]
  node [
    id 268
    label "turn"
  ]
  node [
    id 269
    label "k&#261;t"
  ]
  node [
    id 270
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 271
    label "odchylenie_si&#281;"
  ]
  node [
    id 272
    label "deviation"
  ]
  node [
    id 273
    label "patologia"
  ]
  node [
    id 274
    label "dyskutowa&#263;"
  ]
  node [
    id 275
    label "formu&#322;owa&#263;"
  ]
  node [
    id 276
    label "discourse"
  ]
  node [
    id 277
    label "kognicja"
  ]
  node [
    id 278
    label "rozprawa"
  ]
  node [
    id 279
    label "szczeg&#243;&#322;"
  ]
  node [
    id 280
    label "proposition"
  ]
  node [
    id 281
    label "przes&#322;anka"
  ]
  node [
    id 282
    label "idea"
  ]
  node [
    id 283
    label "paj&#261;k"
  ]
  node [
    id 284
    label "przewodnik"
  ]
  node [
    id 285
    label "odcinek"
  ]
  node [
    id 286
    label "topikowate"
  ]
  node [
    id 287
    label "grupa_dyskusyjna"
  ]
  node [
    id 288
    label "s&#261;d"
  ]
  node [
    id 289
    label "plac"
  ]
  node [
    id 290
    label "bazylika"
  ]
  node [
    id 291
    label "przestrze&#324;"
  ]
  node [
    id 292
    label "miejsce"
  ]
  node [
    id 293
    label "portal"
  ]
  node [
    id 294
    label "konferencja"
  ]
  node [
    id 295
    label "agora"
  ]
  node [
    id 296
    label "grupa"
  ]
  node [
    id 297
    label "strona"
  ]
  node [
    id 298
    label "klejnot_herbowy"
  ]
  node [
    id 299
    label "barwy"
  ]
  node [
    id 300
    label "blazonowa&#263;"
  ]
  node [
    id 301
    label "symbol"
  ]
  node [
    id 302
    label "blazonowanie"
  ]
  node [
    id 303
    label "korona_rangowa"
  ]
  node [
    id 304
    label "heraldyka"
  ]
  node [
    id 305
    label "tarcza_herbowa"
  ]
  node [
    id 306
    label "trzymacz"
  ]
  node [
    id 307
    label "znak_pisarski"
  ]
  node [
    id 308
    label "notacja"
  ]
  node [
    id 309
    label "wcielenie"
  ]
  node [
    id 310
    label "brzoza_kr&#243;lewska"
  ]
  node [
    id 311
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 312
    label "character"
  ]
  node [
    id 313
    label "symbolizowanie"
  ]
  node [
    id 314
    label "baretka"
  ]
  node [
    id 315
    label "dow&#243;d"
  ]
  node [
    id 316
    label "oznakowanie"
  ]
  node [
    id 317
    label "fakt"
  ]
  node [
    id 318
    label "stawia&#263;"
  ]
  node [
    id 319
    label "point"
  ]
  node [
    id 320
    label "kodzik"
  ]
  node [
    id 321
    label "postawi&#263;"
  ]
  node [
    id 322
    label "mark"
  ]
  node [
    id 323
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 324
    label "implikowa&#263;"
  ]
  node [
    id 325
    label "opisywa&#263;"
  ]
  node [
    id 326
    label "coat_of_arms"
  ]
  node [
    id 327
    label "opisywanie"
  ]
  node [
    id 328
    label "oksza"
  ]
  node [
    id 329
    label "pas"
  ]
  node [
    id 330
    label "s&#322;up"
  ]
  node [
    id 331
    label "historia"
  ]
  node [
    id 332
    label "barwa_heraldyczna"
  ]
  node [
    id 333
    label "or&#281;&#380;"
  ]
  node [
    id 334
    label "ideologia"
  ]
  node [
    id 335
    label "podatno&#347;&#263;"
  ]
  node [
    id 336
    label "metoda"
  ]
  node [
    id 337
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 338
    label "praktyka"
  ]
  node [
    id 339
    label "system"
  ]
  node [
    id 340
    label "byt"
  ]
  node [
    id 341
    label "intelekt"
  ]
  node [
    id 342
    label "Kant"
  ]
  node [
    id 343
    label "p&#322;&#243;d"
  ]
  node [
    id 344
    label "cel"
  ]
  node [
    id 345
    label "pomys&#322;"
  ]
  node [
    id 346
    label "ideacja"
  ]
  node [
    id 347
    label "practice"
  ]
  node [
    id 348
    label "wiedza"
  ]
  node [
    id 349
    label "znawstwo"
  ]
  node [
    id 350
    label "skill"
  ]
  node [
    id 351
    label "czyn"
  ]
  node [
    id 352
    label "nauka"
  ]
  node [
    id 353
    label "eksperiencja"
  ]
  node [
    id 354
    label "praca"
  ]
  node [
    id 355
    label "j&#261;dro"
  ]
  node [
    id 356
    label "systemik"
  ]
  node [
    id 357
    label "rozprz&#261;c"
  ]
  node [
    id 358
    label "oprogramowanie"
  ]
  node [
    id 359
    label "systemat"
  ]
  node [
    id 360
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 361
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 362
    label "model"
  ]
  node [
    id 363
    label "usenet"
  ]
  node [
    id 364
    label "porz&#261;dek"
  ]
  node [
    id 365
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 366
    label "przyn&#281;ta"
  ]
  node [
    id 367
    label "net"
  ]
  node [
    id 368
    label "w&#281;dkarstwo"
  ]
  node [
    id 369
    label "eratem"
  ]
  node [
    id 370
    label "oddzia&#322;"
  ]
  node [
    id 371
    label "doktryna"
  ]
  node [
    id 372
    label "pulpit"
  ]
  node [
    id 373
    label "konstelacja"
  ]
  node [
    id 374
    label "jednostka_geologiczna"
  ]
  node [
    id 375
    label "o&#347;"
  ]
  node [
    id 376
    label "podsystem"
  ]
  node [
    id 377
    label "ryba"
  ]
  node [
    id 378
    label "Leopard"
  ]
  node [
    id 379
    label "spos&#243;b"
  ]
  node [
    id 380
    label "Android"
  ]
  node [
    id 381
    label "zachowanie"
  ]
  node [
    id 382
    label "cybernetyk"
  ]
  node [
    id 383
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 384
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 385
    label "method"
  ]
  node [
    id 386
    label "sk&#322;ad"
  ]
  node [
    id 387
    label "podstawa"
  ]
  node [
    id 388
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 389
    label "zasada"
  ]
  node [
    id 390
    label "relacja"
  ]
  node [
    id 391
    label "political_orientation"
  ]
  node [
    id 392
    label "szko&#322;a"
  ]
  node [
    id 393
    label "urz&#281;dnik"
  ]
  node [
    id 394
    label "rynek"
  ]
  node [
    id 395
    label "plac_ratuszowy"
  ]
  node [
    id 396
    label "urz&#261;d"
  ]
  node [
    id 397
    label "zwierzchnik"
  ]
  node [
    id 398
    label "sekretariat"
  ]
  node [
    id 399
    label "siedziba"
  ]
  node [
    id 400
    label "urz&#281;dnik_miejski"
  ]
  node [
    id 401
    label "pracownik"
  ]
  node [
    id 402
    label "korpus_urz&#281;dniczy"
  ]
  node [
    id 403
    label "pragmatyka"
  ]
  node [
    id 404
    label "pryncypa&#322;"
  ]
  node [
    id 405
    label "kierowa&#263;"
  ]
  node [
    id 406
    label "kierownictwo"
  ]
  node [
    id 407
    label "cz&#322;owiek"
  ]
  node [
    id 408
    label "position"
  ]
  node [
    id 409
    label "instytucja"
  ]
  node [
    id 410
    label "organ"
  ]
  node [
    id 411
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 412
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 413
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 414
    label "dzia&#322;"
  ]
  node [
    id 415
    label "okienko"
  ]
  node [
    id 416
    label "w&#322;adza"
  ]
  node [
    id 417
    label "&#321;ubianka"
  ]
  node [
    id 418
    label "miejsce_pracy"
  ]
  node [
    id 419
    label "dzia&#322;_personalny"
  ]
  node [
    id 420
    label "Kreml"
  ]
  node [
    id 421
    label "Bia&#322;y_Dom"
  ]
  node [
    id 422
    label "budynek"
  ]
  node [
    id 423
    label "sadowisko"
  ]
  node [
    id 424
    label "biuro"
  ]
  node [
    id 425
    label "stoisko"
  ]
  node [
    id 426
    label "rynek_podstawowy"
  ]
  node [
    id 427
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 428
    label "konsument"
  ]
  node [
    id 429
    label "pojawienie_si&#281;"
  ]
  node [
    id 430
    label "obiekt_handlowy"
  ]
  node [
    id 431
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 432
    label "wytw&#243;rca"
  ]
  node [
    id 433
    label "rynek_wt&#243;rny"
  ]
  node [
    id 434
    label "wprowadzanie"
  ]
  node [
    id 435
    label "wprowadza&#263;"
  ]
  node [
    id 436
    label "kram"
  ]
  node [
    id 437
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 438
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 439
    label "emitowa&#263;"
  ]
  node [
    id 440
    label "wprowadzi&#263;"
  ]
  node [
    id 441
    label "emitowanie"
  ]
  node [
    id 442
    label "gospodarka"
  ]
  node [
    id 443
    label "biznes"
  ]
  node [
    id 444
    label "segment_rynku"
  ]
  node [
    id 445
    label "wprowadzenie"
  ]
  node [
    id 446
    label "targowica"
  ]
  node [
    id 447
    label "publiczny"
  ]
  node [
    id 448
    label "typowy"
  ]
  node [
    id 449
    label "miastowy"
  ]
  node [
    id 450
    label "miejsko"
  ]
  node [
    id 451
    label "upublicznianie"
  ]
  node [
    id 452
    label "jawny"
  ]
  node [
    id 453
    label "upublicznienie"
  ]
  node [
    id 454
    label "publicznie"
  ]
  node [
    id 455
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 456
    label "zwyczajny"
  ]
  node [
    id 457
    label "typowo"
  ]
  node [
    id 458
    label "cz&#281;sty"
  ]
  node [
    id 459
    label "zwyk&#322;y"
  ]
  node [
    id 460
    label "obywatel"
  ]
  node [
    id 461
    label "mieszczanin"
  ]
  node [
    id 462
    label "nowoczesny"
  ]
  node [
    id 463
    label "mieszcza&#324;stwo"
  ]
  node [
    id 464
    label "charakterystycznie"
  ]
  node [
    id 465
    label "pozbywa&#263;_si&#281;"
  ]
  node [
    id 466
    label "zabija&#263;"
  ]
  node [
    id 467
    label "robi&#263;"
  ]
  node [
    id 468
    label "undo"
  ]
  node [
    id 469
    label "przesuwa&#263;"
  ]
  node [
    id 470
    label "rugowa&#263;"
  ]
  node [
    id 471
    label "powodowa&#263;"
  ]
  node [
    id 472
    label "blurt_out"
  ]
  node [
    id 473
    label "przenosi&#263;"
  ]
  node [
    id 474
    label "powodowa&#263;_&#347;mier&#263;"
  ]
  node [
    id 475
    label "dispatch"
  ]
  node [
    id 476
    label "krzywdzi&#263;"
  ]
  node [
    id 477
    label "beat"
  ]
  node [
    id 478
    label "rzuca&#263;_na_kolana"
  ]
  node [
    id 479
    label "os&#322;ania&#263;"
  ]
  node [
    id 480
    label "niszczy&#263;"
  ]
  node [
    id 481
    label "karci&#263;"
  ]
  node [
    id 482
    label "mordowa&#263;"
  ]
  node [
    id 483
    label "bi&#263;"
  ]
  node [
    id 484
    label "zako&#324;cza&#263;"
  ]
  node [
    id 485
    label "rozbraja&#263;"
  ]
  node [
    id 486
    label "przybija&#263;"
  ]
  node [
    id 487
    label "morzy&#263;"
  ]
  node [
    id 488
    label "zakrywa&#263;"
  ]
  node [
    id 489
    label "kill"
  ]
  node [
    id 490
    label "zwalcza&#263;"
  ]
  node [
    id 491
    label "mie&#263;_miejsce"
  ]
  node [
    id 492
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 493
    label "motywowa&#263;"
  ]
  node [
    id 494
    label "act"
  ]
  node [
    id 495
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 496
    label "organizowa&#263;"
  ]
  node [
    id 497
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 498
    label "czyni&#263;"
  ]
  node [
    id 499
    label "give"
  ]
  node [
    id 500
    label "stylizowa&#263;"
  ]
  node [
    id 501
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 502
    label "falowa&#263;"
  ]
  node [
    id 503
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 504
    label "peddle"
  ]
  node [
    id 505
    label "wydala&#263;"
  ]
  node [
    id 506
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 507
    label "tentegowa&#263;"
  ]
  node [
    id 508
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 509
    label "urz&#261;dza&#263;"
  ]
  node [
    id 510
    label "oszukiwa&#263;"
  ]
  node [
    id 511
    label "work"
  ]
  node [
    id 512
    label "ukazywa&#263;"
  ]
  node [
    id 513
    label "przerabia&#263;"
  ]
  node [
    id 514
    label "post&#281;powa&#263;"
  ]
  node [
    id 515
    label "dostosowywa&#263;"
  ]
  node [
    id 516
    label "estrange"
  ]
  node [
    id 517
    label "transfer"
  ]
  node [
    id 518
    label "translate"
  ]
  node [
    id 519
    label "go"
  ]
  node [
    id 520
    label "zmienia&#263;"
  ]
  node [
    id 521
    label "postpone"
  ]
  node [
    id 522
    label "przestawia&#263;"
  ]
  node [
    id 523
    label "rusza&#263;"
  ]
  node [
    id 524
    label "kopiowa&#263;"
  ]
  node [
    id 525
    label "ponosi&#263;"
  ]
  node [
    id 526
    label "rozpowszechnia&#263;"
  ]
  node [
    id 527
    label "move"
  ]
  node [
    id 528
    label "rozprzestrzenia&#263;"
  ]
  node [
    id 529
    label "circulate"
  ]
  node [
    id 530
    label "pocisk"
  ]
  node [
    id 531
    label "przemieszcza&#263;"
  ]
  node [
    id 532
    label "wytrzyma&#263;"
  ]
  node [
    id 533
    label "umieszcza&#263;"
  ]
  node [
    id 534
    label "przelatywa&#263;"
  ]
  node [
    id 535
    label "infest"
  ]
  node [
    id 536
    label "strzela&#263;"
  ]
  node [
    id 537
    label "liczy&#263;"
  ]
  node [
    id 538
    label "Zenon"
  ]
  node [
    id 539
    label "Piechem"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 292
  ]
  edge [
    source 11
    target 311
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 289
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 538
    target 539
  ]
]
