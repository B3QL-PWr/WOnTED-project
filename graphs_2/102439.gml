graph [
  node [
    id 0
    label "bardzo"
    origin "text"
  ]
  node [
    id 1
    label "podoba"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "idea"
    origin "text"
  ]
  node [
    id 4
    label "blog"
    origin "text"
  ]
  node [
    id 5
    label "dwu&#347;cie&#380;kowego"
    origin "text"
  ]
  node [
    id 6
    label "taki"
    origin "text"
  ]
  node [
    id 7
    label "jak"
    origin "text"
  ]
  node [
    id 8
    label "jeden"
    origin "text"
  ]
  node [
    id 9
    label "&#347;cie&#380;ek"
    origin "text"
  ]
  node [
    id 10
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 11
    label "tekst"
    origin "text"
  ]
  node [
    id 12
    label "druga"
    origin "text"
  ]
  node [
    id 13
    label "kr&#243;tki"
    origin "text"
  ]
  node [
    id 14
    label "m&#243;cbyby&#263;"
    origin "text"
  ]
  node [
    id 15
    label "ten"
    origin "text"
  ]
  node [
    id 16
    label "wrzuca&#263;"
    origin "text"
  ]
  node [
    id 17
    label "notka"
    origin "text"
  ]
  node [
    id 18
    label "w_chuj"
  ]
  node [
    id 19
    label "ideologia"
  ]
  node [
    id 20
    label "byt"
  ]
  node [
    id 21
    label "intelekt"
  ]
  node [
    id 22
    label "Kant"
  ]
  node [
    id 23
    label "p&#322;&#243;d"
  ]
  node [
    id 24
    label "cel"
  ]
  node [
    id 25
    label "poj&#281;cie"
  ]
  node [
    id 26
    label "istota"
  ]
  node [
    id 27
    label "pomys&#322;"
  ]
  node [
    id 28
    label "ideacja"
  ]
  node [
    id 29
    label "mentalno&#347;&#263;"
  ]
  node [
    id 30
    label "superego"
  ]
  node [
    id 31
    label "psychika"
  ]
  node [
    id 32
    label "znaczenie"
  ]
  node [
    id 33
    label "wn&#281;trze"
  ]
  node [
    id 34
    label "charakter"
  ]
  node [
    id 35
    label "cecha"
  ]
  node [
    id 36
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 37
    label "wytw&#243;r"
  ]
  node [
    id 38
    label "moczownik"
  ]
  node [
    id 39
    label "embryo"
  ]
  node [
    id 40
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 41
    label "zarodek"
  ]
  node [
    id 42
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 43
    label "latawiec"
  ]
  node [
    id 44
    label "punkt"
  ]
  node [
    id 45
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 46
    label "miejsce"
  ]
  node [
    id 47
    label "rezultat"
  ]
  node [
    id 48
    label "thing"
  ]
  node [
    id 49
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 50
    label "rzecz"
  ]
  node [
    id 51
    label "pos&#322;uchanie"
  ]
  node [
    id 52
    label "skumanie"
  ]
  node [
    id 53
    label "orientacja"
  ]
  node [
    id 54
    label "zorientowanie"
  ]
  node [
    id 55
    label "teoria"
  ]
  node [
    id 56
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 57
    label "clasp"
  ]
  node [
    id 58
    label "forma"
  ]
  node [
    id 59
    label "przem&#243;wienie"
  ]
  node [
    id 60
    label "umys&#322;"
  ]
  node [
    id 61
    label "wiedza"
  ]
  node [
    id 62
    label "noosfera"
  ]
  node [
    id 63
    label "utrzymywanie"
  ]
  node [
    id 64
    label "bycie"
  ]
  node [
    id 65
    label "entity"
  ]
  node [
    id 66
    label "subsystencja"
  ]
  node [
    id 67
    label "utrzyma&#263;"
  ]
  node [
    id 68
    label "egzystencja"
  ]
  node [
    id 69
    label "wy&#380;ywienie"
  ]
  node [
    id 70
    label "ontologicznie"
  ]
  node [
    id 71
    label "utrzymanie"
  ]
  node [
    id 72
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 73
    label "potencja"
  ]
  node [
    id 74
    label "utrzymywa&#263;"
  ]
  node [
    id 75
    label "pocz&#261;tki"
  ]
  node [
    id 76
    label "ukra&#347;&#263;"
  ]
  node [
    id 77
    label "ukradzenie"
  ]
  node [
    id 78
    label "system"
  ]
  node [
    id 79
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 80
    label "czysty_rozum"
  ]
  node [
    id 81
    label "noumenon"
  ]
  node [
    id 82
    label "filozofia"
  ]
  node [
    id 83
    label "kantysta"
  ]
  node [
    id 84
    label "fenomenologia"
  ]
  node [
    id 85
    label "zdolno&#347;&#263;"
  ]
  node [
    id 86
    label "political_orientation"
  ]
  node [
    id 87
    label "szko&#322;a"
  ]
  node [
    id 88
    label "komcio"
  ]
  node [
    id 89
    label "blogosfera"
  ]
  node [
    id 90
    label "pami&#281;tnik"
  ]
  node [
    id 91
    label "strona"
  ]
  node [
    id 92
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 93
    label "pami&#261;tka"
  ]
  node [
    id 94
    label "notes"
  ]
  node [
    id 95
    label "zapiski"
  ]
  node [
    id 96
    label "raptularz"
  ]
  node [
    id 97
    label "album"
  ]
  node [
    id 98
    label "utw&#243;r_epicki"
  ]
  node [
    id 99
    label "kartka"
  ]
  node [
    id 100
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 101
    label "logowanie"
  ]
  node [
    id 102
    label "plik"
  ]
  node [
    id 103
    label "s&#261;d"
  ]
  node [
    id 104
    label "adres_internetowy"
  ]
  node [
    id 105
    label "linia"
  ]
  node [
    id 106
    label "serwis_internetowy"
  ]
  node [
    id 107
    label "posta&#263;"
  ]
  node [
    id 108
    label "bok"
  ]
  node [
    id 109
    label "skr&#281;canie"
  ]
  node [
    id 110
    label "skr&#281;ca&#263;"
  ]
  node [
    id 111
    label "orientowanie"
  ]
  node [
    id 112
    label "skr&#281;ci&#263;"
  ]
  node [
    id 113
    label "uj&#281;cie"
  ]
  node [
    id 114
    label "ty&#322;"
  ]
  node [
    id 115
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 116
    label "fragment"
  ]
  node [
    id 117
    label "layout"
  ]
  node [
    id 118
    label "obiekt"
  ]
  node [
    id 119
    label "zorientowa&#263;"
  ]
  node [
    id 120
    label "pagina"
  ]
  node [
    id 121
    label "podmiot"
  ]
  node [
    id 122
    label "g&#243;ra"
  ]
  node [
    id 123
    label "orientowa&#263;"
  ]
  node [
    id 124
    label "voice"
  ]
  node [
    id 125
    label "prz&#243;d"
  ]
  node [
    id 126
    label "internet"
  ]
  node [
    id 127
    label "powierzchnia"
  ]
  node [
    id 128
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 129
    label "skr&#281;cenie"
  ]
  node [
    id 130
    label "zbi&#243;r"
  ]
  node [
    id 131
    label "komentarz"
  ]
  node [
    id 132
    label "okre&#347;lony"
  ]
  node [
    id 133
    label "jaki&#347;"
  ]
  node [
    id 134
    label "przyzwoity"
  ]
  node [
    id 135
    label "ciekawy"
  ]
  node [
    id 136
    label "jako&#347;"
  ]
  node [
    id 137
    label "jako_tako"
  ]
  node [
    id 138
    label "niez&#322;y"
  ]
  node [
    id 139
    label "dziwny"
  ]
  node [
    id 140
    label "charakterystyczny"
  ]
  node [
    id 141
    label "wiadomy"
  ]
  node [
    id 142
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 143
    label "zobo"
  ]
  node [
    id 144
    label "yakalo"
  ]
  node [
    id 145
    label "byd&#322;o"
  ]
  node [
    id 146
    label "dzo"
  ]
  node [
    id 147
    label "kr&#281;torogie"
  ]
  node [
    id 148
    label "g&#322;owa"
  ]
  node [
    id 149
    label "czochrad&#322;o"
  ]
  node [
    id 150
    label "posp&#243;lstwo"
  ]
  node [
    id 151
    label "kraal"
  ]
  node [
    id 152
    label "livestock"
  ]
  node [
    id 153
    label "prze&#380;uwacz"
  ]
  node [
    id 154
    label "zebu"
  ]
  node [
    id 155
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 156
    label "bizon"
  ]
  node [
    id 157
    label "byd&#322;o_domowe"
  ]
  node [
    id 158
    label "shot"
  ]
  node [
    id 159
    label "jednakowy"
  ]
  node [
    id 160
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 161
    label "ujednolicenie"
  ]
  node [
    id 162
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 163
    label "jednolicie"
  ]
  node [
    id 164
    label "kieliszek"
  ]
  node [
    id 165
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 166
    label "w&#243;dka"
  ]
  node [
    id 167
    label "szk&#322;o"
  ]
  node [
    id 168
    label "zawarto&#347;&#263;"
  ]
  node [
    id 169
    label "naczynie"
  ]
  node [
    id 170
    label "alkohol"
  ]
  node [
    id 171
    label "sznaps"
  ]
  node [
    id 172
    label "nap&#243;j"
  ]
  node [
    id 173
    label "gorza&#322;ka"
  ]
  node [
    id 174
    label "mohorycz"
  ]
  node [
    id 175
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 176
    label "mundurowanie"
  ]
  node [
    id 177
    label "zr&#243;wnanie"
  ]
  node [
    id 178
    label "taki&#380;"
  ]
  node [
    id 179
    label "mundurowa&#263;"
  ]
  node [
    id 180
    label "jednakowo"
  ]
  node [
    id 181
    label "zr&#243;wnywanie"
  ]
  node [
    id 182
    label "identyczny"
  ]
  node [
    id 183
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 184
    label "z&#322;o&#380;ony"
  ]
  node [
    id 185
    label "g&#322;&#281;bszy"
  ]
  node [
    id 186
    label "drink"
  ]
  node [
    id 187
    label "jednolity"
  ]
  node [
    id 188
    label "upodobnienie"
  ]
  node [
    id 189
    label "calibration"
  ]
  node [
    id 190
    label "daleki"
  ]
  node [
    id 191
    label "ruch"
  ]
  node [
    id 192
    label "d&#322;ugo"
  ]
  node [
    id 193
    label "mechanika"
  ]
  node [
    id 194
    label "move"
  ]
  node [
    id 195
    label "poruszenie"
  ]
  node [
    id 196
    label "movement"
  ]
  node [
    id 197
    label "myk"
  ]
  node [
    id 198
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 199
    label "zjawisko"
  ]
  node [
    id 200
    label "travel"
  ]
  node [
    id 201
    label "kanciasty"
  ]
  node [
    id 202
    label "commercial_enterprise"
  ]
  node [
    id 203
    label "model"
  ]
  node [
    id 204
    label "strumie&#324;"
  ]
  node [
    id 205
    label "proces"
  ]
  node [
    id 206
    label "aktywno&#347;&#263;"
  ]
  node [
    id 207
    label "taktyka"
  ]
  node [
    id 208
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 209
    label "apraksja"
  ]
  node [
    id 210
    label "natural_process"
  ]
  node [
    id 211
    label "wydarzenie"
  ]
  node [
    id 212
    label "dyssypacja_energii"
  ]
  node [
    id 213
    label "tumult"
  ]
  node [
    id 214
    label "stopek"
  ]
  node [
    id 215
    label "czynno&#347;&#263;"
  ]
  node [
    id 216
    label "zmiana"
  ]
  node [
    id 217
    label "manewr"
  ]
  node [
    id 218
    label "lokomocja"
  ]
  node [
    id 219
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 220
    label "komunikacja"
  ]
  node [
    id 221
    label "drift"
  ]
  node [
    id 222
    label "dawny"
  ]
  node [
    id 223
    label "ogl&#281;dny"
  ]
  node [
    id 224
    label "du&#380;y"
  ]
  node [
    id 225
    label "daleko"
  ]
  node [
    id 226
    label "odleg&#322;y"
  ]
  node [
    id 227
    label "zwi&#261;zany"
  ]
  node [
    id 228
    label "r&#243;&#380;ny"
  ]
  node [
    id 229
    label "s&#322;aby"
  ]
  node [
    id 230
    label "odlegle"
  ]
  node [
    id 231
    label "oddalony"
  ]
  node [
    id 232
    label "g&#322;&#281;boki"
  ]
  node [
    id 233
    label "obcy"
  ]
  node [
    id 234
    label "nieobecny"
  ]
  node [
    id 235
    label "przysz&#322;y"
  ]
  node [
    id 236
    label "ekscerpcja"
  ]
  node [
    id 237
    label "j&#281;zykowo"
  ]
  node [
    id 238
    label "wypowied&#378;"
  ]
  node [
    id 239
    label "redakcja"
  ]
  node [
    id 240
    label "pomini&#281;cie"
  ]
  node [
    id 241
    label "dzie&#322;o"
  ]
  node [
    id 242
    label "preparacja"
  ]
  node [
    id 243
    label "odmianka"
  ]
  node [
    id 244
    label "opu&#347;ci&#263;"
  ]
  node [
    id 245
    label "koniektura"
  ]
  node [
    id 246
    label "pisa&#263;"
  ]
  node [
    id 247
    label "obelga"
  ]
  node [
    id 248
    label "przedmiot"
  ]
  node [
    id 249
    label "work"
  ]
  node [
    id 250
    label "obrazowanie"
  ]
  node [
    id 251
    label "dorobek"
  ]
  node [
    id 252
    label "tre&#347;&#263;"
  ]
  node [
    id 253
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 254
    label "retrospektywa"
  ]
  node [
    id 255
    label "works"
  ]
  node [
    id 256
    label "creation"
  ]
  node [
    id 257
    label "tetralogia"
  ]
  node [
    id 258
    label "komunikat"
  ]
  node [
    id 259
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 260
    label "praca"
  ]
  node [
    id 261
    label "sparafrazowanie"
  ]
  node [
    id 262
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 263
    label "strawestowa&#263;"
  ]
  node [
    id 264
    label "sparafrazowa&#263;"
  ]
  node [
    id 265
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 266
    label "trawestowa&#263;"
  ]
  node [
    id 267
    label "sformu&#322;owanie"
  ]
  node [
    id 268
    label "parafrazowanie"
  ]
  node [
    id 269
    label "ozdobnik"
  ]
  node [
    id 270
    label "delimitacja"
  ]
  node [
    id 271
    label "parafrazowa&#263;"
  ]
  node [
    id 272
    label "stylizacja"
  ]
  node [
    id 273
    label "trawestowanie"
  ]
  node [
    id 274
    label "strawestowanie"
  ]
  node [
    id 275
    label "cholera"
  ]
  node [
    id 276
    label "ubliga"
  ]
  node [
    id 277
    label "niedorobek"
  ]
  node [
    id 278
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 279
    label "chuj"
  ]
  node [
    id 280
    label "bluzg"
  ]
  node [
    id 281
    label "wyzwisko"
  ]
  node [
    id 282
    label "indignation"
  ]
  node [
    id 283
    label "pies"
  ]
  node [
    id 284
    label "wrzuta"
  ]
  node [
    id 285
    label "chujowy"
  ]
  node [
    id 286
    label "krzywda"
  ]
  node [
    id 287
    label "szmata"
  ]
  node [
    id 288
    label "odmiana"
  ]
  node [
    id 289
    label "formu&#322;owa&#263;"
  ]
  node [
    id 290
    label "ozdabia&#263;"
  ]
  node [
    id 291
    label "stawia&#263;"
  ]
  node [
    id 292
    label "spell"
  ]
  node [
    id 293
    label "styl"
  ]
  node [
    id 294
    label "skryba"
  ]
  node [
    id 295
    label "read"
  ]
  node [
    id 296
    label "donosi&#263;"
  ]
  node [
    id 297
    label "code"
  ]
  node [
    id 298
    label "dysgrafia"
  ]
  node [
    id 299
    label "dysortografia"
  ]
  node [
    id 300
    label "tworzy&#263;"
  ]
  node [
    id 301
    label "prasa"
  ]
  node [
    id 302
    label "preparation"
  ]
  node [
    id 303
    label "proces_technologiczny"
  ]
  node [
    id 304
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 305
    label "pozostawi&#263;"
  ]
  node [
    id 306
    label "obni&#380;y&#263;"
  ]
  node [
    id 307
    label "zostawi&#263;"
  ]
  node [
    id 308
    label "przesta&#263;"
  ]
  node [
    id 309
    label "potani&#263;"
  ]
  node [
    id 310
    label "drop"
  ]
  node [
    id 311
    label "evacuate"
  ]
  node [
    id 312
    label "humiliate"
  ]
  node [
    id 313
    label "leave"
  ]
  node [
    id 314
    label "straci&#263;"
  ]
  node [
    id 315
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 316
    label "authorize"
  ]
  node [
    id 317
    label "omin&#261;&#263;"
  ]
  node [
    id 318
    label "przypuszczenie"
  ]
  node [
    id 319
    label "conjecture"
  ]
  node [
    id 320
    label "obr&#243;bka"
  ]
  node [
    id 321
    label "wniosek"
  ]
  node [
    id 322
    label "redaktor"
  ]
  node [
    id 323
    label "radio"
  ]
  node [
    id 324
    label "zesp&#243;&#322;"
  ]
  node [
    id 325
    label "siedziba"
  ]
  node [
    id 326
    label "composition"
  ]
  node [
    id 327
    label "wydawnictwo"
  ]
  node [
    id 328
    label "redaction"
  ]
  node [
    id 329
    label "telewizja"
  ]
  node [
    id 330
    label "wyb&#243;r"
  ]
  node [
    id 331
    label "dokumentacja"
  ]
  node [
    id 332
    label "u&#380;ytkownik"
  ]
  node [
    id 333
    label "komunikacyjnie"
  ]
  node [
    id 334
    label "ellipsis"
  ]
  node [
    id 335
    label "wykluczenie"
  ]
  node [
    id 336
    label "figura_my&#347;li"
  ]
  node [
    id 337
    label "zrobienie"
  ]
  node [
    id 338
    label "godzina"
  ]
  node [
    id 339
    label "time"
  ]
  node [
    id 340
    label "doba"
  ]
  node [
    id 341
    label "p&#243;&#322;godzina"
  ]
  node [
    id 342
    label "jednostka_czasu"
  ]
  node [
    id 343
    label "czas"
  ]
  node [
    id 344
    label "minuta"
  ]
  node [
    id 345
    label "kwadrans"
  ]
  node [
    id 346
    label "szybki"
  ]
  node [
    id 347
    label "jednowyrazowy"
  ]
  node [
    id 348
    label "bliski"
  ]
  node [
    id 349
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 350
    label "kr&#243;tko"
  ]
  node [
    id 351
    label "drobny"
  ]
  node [
    id 352
    label "brak"
  ]
  node [
    id 353
    label "z&#322;y"
  ]
  node [
    id 354
    label "pieski"
  ]
  node [
    id 355
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 356
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 357
    label "niekorzystny"
  ]
  node [
    id 358
    label "z&#322;oszczenie"
  ]
  node [
    id 359
    label "sierdzisty"
  ]
  node [
    id 360
    label "niegrzeczny"
  ]
  node [
    id 361
    label "zez&#322;oszczenie"
  ]
  node [
    id 362
    label "zdenerwowany"
  ]
  node [
    id 363
    label "negatywny"
  ]
  node [
    id 364
    label "rozgniewanie"
  ]
  node [
    id 365
    label "gniewanie"
  ]
  node [
    id 366
    label "niemoralny"
  ]
  node [
    id 367
    label "&#378;le"
  ]
  node [
    id 368
    label "niepomy&#347;lny"
  ]
  node [
    id 369
    label "syf"
  ]
  node [
    id 370
    label "sprawny"
  ]
  node [
    id 371
    label "efektywny"
  ]
  node [
    id 372
    label "zwi&#281;&#378;le"
  ]
  node [
    id 373
    label "oszcz&#281;dny"
  ]
  node [
    id 374
    label "nietrwa&#322;y"
  ]
  node [
    id 375
    label "mizerny"
  ]
  node [
    id 376
    label "marnie"
  ]
  node [
    id 377
    label "delikatny"
  ]
  node [
    id 378
    label "po&#347;ledni"
  ]
  node [
    id 379
    label "niezdrowy"
  ]
  node [
    id 380
    label "nieumiej&#281;tny"
  ]
  node [
    id 381
    label "s&#322;abo"
  ]
  node [
    id 382
    label "nieznaczny"
  ]
  node [
    id 383
    label "lura"
  ]
  node [
    id 384
    label "nieudany"
  ]
  node [
    id 385
    label "s&#322;abowity"
  ]
  node [
    id 386
    label "zawodny"
  ]
  node [
    id 387
    label "&#322;agodny"
  ]
  node [
    id 388
    label "md&#322;y"
  ]
  node [
    id 389
    label "niedoskona&#322;y"
  ]
  node [
    id 390
    label "przemijaj&#261;cy"
  ]
  node [
    id 391
    label "niemocny"
  ]
  node [
    id 392
    label "niefajny"
  ]
  node [
    id 393
    label "kiepsko"
  ]
  node [
    id 394
    label "intensywny"
  ]
  node [
    id 395
    label "prosty"
  ]
  node [
    id 396
    label "temperamentny"
  ]
  node [
    id 397
    label "bystrolotny"
  ]
  node [
    id 398
    label "dynamiczny"
  ]
  node [
    id 399
    label "szybko"
  ]
  node [
    id 400
    label "bezpo&#347;redni"
  ]
  node [
    id 401
    label "energiczny"
  ]
  node [
    id 402
    label "blisko"
  ]
  node [
    id 403
    label "cz&#322;owiek"
  ]
  node [
    id 404
    label "znajomy"
  ]
  node [
    id 405
    label "przesz&#322;y"
  ]
  node [
    id 406
    label "silny"
  ]
  node [
    id 407
    label "zbli&#380;enie"
  ]
  node [
    id 408
    label "dok&#322;adny"
  ]
  node [
    id 409
    label "nieodleg&#322;y"
  ]
  node [
    id 410
    label "gotowy"
  ]
  node [
    id 411
    label "ma&#322;y"
  ]
  node [
    id 412
    label "skromny"
  ]
  node [
    id 413
    label "niesamodzielny"
  ]
  node [
    id 414
    label "niewa&#380;ny"
  ]
  node [
    id 415
    label "podhala&#324;ski"
  ]
  node [
    id 416
    label "taniec_ludowy"
  ]
  node [
    id 417
    label "szczup&#322;y"
  ]
  node [
    id 418
    label "drobno"
  ]
  node [
    id 419
    label "ma&#322;oletni"
  ]
  node [
    id 420
    label "nieistnienie"
  ]
  node [
    id 421
    label "odej&#347;cie"
  ]
  node [
    id 422
    label "defect"
  ]
  node [
    id 423
    label "gap"
  ]
  node [
    id 424
    label "odej&#347;&#263;"
  ]
  node [
    id 425
    label "wada"
  ]
  node [
    id 426
    label "odchodzi&#263;"
  ]
  node [
    id 427
    label "wyr&#243;b"
  ]
  node [
    id 428
    label "odchodzenie"
  ]
  node [
    id 429
    label "prywatywny"
  ]
  node [
    id 430
    label "jednocz&#322;onowy"
  ]
  node [
    id 431
    label "give"
  ]
  node [
    id 432
    label "umieszcza&#263;"
  ]
  node [
    id 433
    label "plasowa&#263;"
  ]
  node [
    id 434
    label "umie&#347;ci&#263;"
  ]
  node [
    id 435
    label "robi&#263;"
  ]
  node [
    id 436
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 437
    label "pomieszcza&#263;"
  ]
  node [
    id 438
    label "accommodate"
  ]
  node [
    id 439
    label "zmienia&#263;"
  ]
  node [
    id 440
    label "powodowa&#263;"
  ]
  node [
    id 441
    label "venture"
  ]
  node [
    id 442
    label "wpiernicza&#263;"
  ]
  node [
    id 443
    label "okre&#347;la&#263;"
  ]
  node [
    id 444
    label "note"
  ]
  node [
    id 445
    label "notatka"
  ]
  node [
    id 446
    label "informacja"
  ]
  node [
    id 447
    label "przypis"
  ]
  node [
    id 448
    label "zapis"
  ]
  node [
    id 449
    label "konotatka"
  ]
  node [
    id 450
    label "tre&#347;ciwy"
  ]
  node [
    id 451
    label "gloss"
  ]
  node [
    id 452
    label "aparat_krytyczny"
  ]
  node [
    id 453
    label "dopisek"
  ]
  node [
    id 454
    label "obja&#347;nienie"
  ]
  node [
    id 455
    label "publikacja"
  ]
  node [
    id 456
    label "doj&#347;cie"
  ]
  node [
    id 457
    label "obiega&#263;"
  ]
  node [
    id 458
    label "powzi&#281;cie"
  ]
  node [
    id 459
    label "dane"
  ]
  node [
    id 460
    label "obiegni&#281;cie"
  ]
  node [
    id 461
    label "sygna&#322;"
  ]
  node [
    id 462
    label "obieganie"
  ]
  node [
    id 463
    label "powzi&#261;&#263;"
  ]
  node [
    id 464
    label "obiec"
  ]
  node [
    id 465
    label "doj&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 37
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 246
  ]
  edge [
    source 11
    target 247
  ]
  edge [
    source 11
    target 248
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 47
  ]
  edge [
    source 11
    target 250
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 251
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 260
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 261
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 285
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 11
    target 287
  ]
  edge [
    source 11
    target 288
  ]
  edge [
    source 11
    target 289
  ]
  edge [
    source 11
    target 290
  ]
  edge [
    source 11
    target 291
  ]
  edge [
    source 11
    target 292
  ]
  edge [
    source 11
    target 293
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 295
  ]
  edge [
    source 11
    target 296
  ]
  edge [
    source 11
    target 297
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 300
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 11
    target 309
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 11
    target 311
  ]
  edge [
    source 11
    target 312
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 229
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 352
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 355
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 227
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 406
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 231
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 235
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 201
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 206
  ]
  edge [
    source 13
    target 207
  ]
  edge [
    source 13
    target 208
  ]
  edge [
    source 13
    target 209
  ]
  edge [
    source 13
    target 210
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 211
  ]
  edge [
    source 13
    target 212
  ]
  edge [
    source 13
    target 213
  ]
  edge [
    source 13
    target 214
  ]
  edge [
    source 13
    target 215
  ]
  edge [
    source 13
    target 216
  ]
  edge [
    source 13
    target 217
  ]
  edge [
    source 13
    target 218
  ]
  edge [
    source 13
    target 219
  ]
  edge [
    source 13
    target 220
  ]
  edge [
    source 13
    target 221
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 16
    target 431
  ]
  edge [
    source 16
    target 432
  ]
  edge [
    source 16
    target 433
  ]
  edge [
    source 16
    target 434
  ]
  edge [
    source 16
    target 435
  ]
  edge [
    source 16
    target 436
  ]
  edge [
    source 16
    target 437
  ]
  edge [
    source 16
    target 438
  ]
  edge [
    source 16
    target 439
  ]
  edge [
    source 16
    target 440
  ]
  edge [
    source 16
    target 441
  ]
  edge [
    source 16
    target 442
  ]
  edge [
    source 16
    target 443
  ]
  edge [
    source 17
    target 444
  ]
  edge [
    source 17
    target 445
  ]
  edge [
    source 17
    target 446
  ]
  edge [
    source 17
    target 447
  ]
  edge [
    source 17
    target 448
  ]
  edge [
    source 17
    target 449
  ]
  edge [
    source 17
    target 349
  ]
  edge [
    source 17
    target 450
  ]
  edge [
    source 17
    target 451
  ]
  edge [
    source 17
    target 452
  ]
  edge [
    source 17
    target 453
  ]
  edge [
    source 17
    target 454
  ]
  edge [
    source 17
    target 44
  ]
  edge [
    source 17
    target 455
  ]
  edge [
    source 17
    target 61
  ]
  edge [
    source 17
    target 456
  ]
  edge [
    source 17
    target 457
  ]
  edge [
    source 17
    target 458
  ]
  edge [
    source 17
    target 459
  ]
  edge [
    source 17
    target 460
  ]
  edge [
    source 17
    target 461
  ]
  edge [
    source 17
    target 462
  ]
  edge [
    source 17
    target 463
  ]
  edge [
    source 17
    target 464
  ]
  edge [
    source 17
    target 465
  ]
]
