graph [
  node [
    id 0
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "pan"
    origin "text"
  ]
  node [
    id 3
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 4
    label "piotr"
    origin "text"
  ]
  node [
    id 5
    label "babinetz"
    origin "text"
  ]
  node [
    id 6
    label "prawo"
    origin "text"
  ]
  node [
    id 7
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "zesp&#243;&#322;"
  ]
  node [
    id 9
    label "wpadni&#281;cie"
  ]
  node [
    id 10
    label "wydawa&#263;"
  ]
  node [
    id 11
    label "regestr"
  ]
  node [
    id 12
    label "zjawisko"
  ]
  node [
    id 13
    label "zdolno&#347;&#263;"
  ]
  node [
    id 14
    label "wyda&#263;"
  ]
  node [
    id 15
    label "wpa&#347;&#263;"
  ]
  node [
    id 16
    label "d&#378;wi&#281;k"
  ]
  node [
    id 17
    label "note"
  ]
  node [
    id 18
    label "partia"
  ]
  node [
    id 19
    label "&#347;piewak_operowy"
  ]
  node [
    id 20
    label "onomatopeja"
  ]
  node [
    id 21
    label "decyzja"
  ]
  node [
    id 22
    label "linia_melodyczna"
  ]
  node [
    id 23
    label "sound"
  ]
  node [
    id 24
    label "opinion"
  ]
  node [
    id 25
    label "grupa"
  ]
  node [
    id 26
    label "wpada&#263;"
  ]
  node [
    id 27
    label "nakaz"
  ]
  node [
    id 28
    label "matowie&#263;"
  ]
  node [
    id 29
    label "foniatra"
  ]
  node [
    id 30
    label "stanowisko"
  ]
  node [
    id 31
    label "ch&#243;rzysta"
  ]
  node [
    id 32
    label "mutacja"
  ]
  node [
    id 33
    label "&#347;piewaczka"
  ]
  node [
    id 34
    label "zmatowienie"
  ]
  node [
    id 35
    label "wydanie"
  ]
  node [
    id 36
    label "wokal"
  ]
  node [
    id 37
    label "wypowied&#378;"
  ]
  node [
    id 38
    label "emisja"
  ]
  node [
    id 39
    label "zmatowie&#263;"
  ]
  node [
    id 40
    label "&#347;piewak"
  ]
  node [
    id 41
    label "matowienie"
  ]
  node [
    id 42
    label "brzmienie"
  ]
  node [
    id 43
    label "wpadanie"
  ]
  node [
    id 44
    label "posiada&#263;"
  ]
  node [
    id 45
    label "cecha"
  ]
  node [
    id 46
    label "potencja&#322;"
  ]
  node [
    id 47
    label "zapomnienie"
  ]
  node [
    id 48
    label "zapomina&#263;"
  ]
  node [
    id 49
    label "zapominanie"
  ]
  node [
    id 50
    label "ability"
  ]
  node [
    id 51
    label "obliczeniowo"
  ]
  node [
    id 52
    label "zapomnie&#263;"
  ]
  node [
    id 53
    label "odm&#322;adzanie"
  ]
  node [
    id 54
    label "liga"
  ]
  node [
    id 55
    label "jednostka_systematyczna"
  ]
  node [
    id 56
    label "asymilowanie"
  ]
  node [
    id 57
    label "gromada"
  ]
  node [
    id 58
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 59
    label "asymilowa&#263;"
  ]
  node [
    id 60
    label "egzemplarz"
  ]
  node [
    id 61
    label "Entuzjastki"
  ]
  node [
    id 62
    label "zbi&#243;r"
  ]
  node [
    id 63
    label "kompozycja"
  ]
  node [
    id 64
    label "Terranie"
  ]
  node [
    id 65
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 66
    label "category"
  ]
  node [
    id 67
    label "pakiet_klimatyczny"
  ]
  node [
    id 68
    label "oddzia&#322;"
  ]
  node [
    id 69
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 70
    label "cz&#261;steczka"
  ]
  node [
    id 71
    label "stage_set"
  ]
  node [
    id 72
    label "type"
  ]
  node [
    id 73
    label "specgrupa"
  ]
  node [
    id 74
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 75
    label "&#346;wietliki"
  ]
  node [
    id 76
    label "odm&#322;odzenie"
  ]
  node [
    id 77
    label "Eurogrupa"
  ]
  node [
    id 78
    label "odm&#322;adza&#263;"
  ]
  node [
    id 79
    label "formacja_geologiczna"
  ]
  node [
    id 80
    label "harcerze_starsi"
  ]
  node [
    id 81
    label "statement"
  ]
  node [
    id 82
    label "polecenie"
  ]
  node [
    id 83
    label "bodziec"
  ]
  node [
    id 84
    label "proces"
  ]
  node [
    id 85
    label "boski"
  ]
  node [
    id 86
    label "krajobraz"
  ]
  node [
    id 87
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 88
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 89
    label "przywidzenie"
  ]
  node [
    id 90
    label "presence"
  ]
  node [
    id 91
    label "charakter"
  ]
  node [
    id 92
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 93
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 94
    label "management"
  ]
  node [
    id 95
    label "resolution"
  ]
  node [
    id 96
    label "wytw&#243;r"
  ]
  node [
    id 97
    label "zdecydowanie"
  ]
  node [
    id 98
    label "dokument"
  ]
  node [
    id 99
    label "Bund"
  ]
  node [
    id 100
    label "PPR"
  ]
  node [
    id 101
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 102
    label "wybranek"
  ]
  node [
    id 103
    label "Jakobici"
  ]
  node [
    id 104
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 105
    label "SLD"
  ]
  node [
    id 106
    label "Razem"
  ]
  node [
    id 107
    label "PiS"
  ]
  node [
    id 108
    label "package"
  ]
  node [
    id 109
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 110
    label "Kuomintang"
  ]
  node [
    id 111
    label "ZSL"
  ]
  node [
    id 112
    label "organizacja"
  ]
  node [
    id 113
    label "AWS"
  ]
  node [
    id 114
    label "gra"
  ]
  node [
    id 115
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 116
    label "game"
  ]
  node [
    id 117
    label "blok"
  ]
  node [
    id 118
    label "materia&#322;"
  ]
  node [
    id 119
    label "PO"
  ]
  node [
    id 120
    label "si&#322;a"
  ]
  node [
    id 121
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 122
    label "niedoczas"
  ]
  node [
    id 123
    label "Federali&#347;ci"
  ]
  node [
    id 124
    label "PSL"
  ]
  node [
    id 125
    label "Wigowie"
  ]
  node [
    id 126
    label "ZChN"
  ]
  node [
    id 127
    label "egzekutywa"
  ]
  node [
    id 128
    label "aktyw"
  ]
  node [
    id 129
    label "wybranka"
  ]
  node [
    id 130
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 131
    label "unit"
  ]
  node [
    id 132
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 133
    label "ptak_&#347;piewaj&#261;cy"
  ]
  node [
    id 134
    label "muzyk"
  ]
  node [
    id 135
    label "phone"
  ]
  node [
    id 136
    label "intonacja"
  ]
  node [
    id 137
    label "modalizm"
  ]
  node [
    id 138
    label "nadlecenie"
  ]
  node [
    id 139
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 140
    label "solmizacja"
  ]
  node [
    id 141
    label "seria"
  ]
  node [
    id 142
    label "dobiec"
  ]
  node [
    id 143
    label "transmiter"
  ]
  node [
    id 144
    label "heksachord"
  ]
  node [
    id 145
    label "akcent"
  ]
  node [
    id 146
    label "repetycja"
  ]
  node [
    id 147
    label "po&#322;o&#380;enie"
  ]
  node [
    id 148
    label "punkt"
  ]
  node [
    id 149
    label "pogl&#261;d"
  ]
  node [
    id 150
    label "wojsko"
  ]
  node [
    id 151
    label "awansowa&#263;"
  ]
  node [
    id 152
    label "stawia&#263;"
  ]
  node [
    id 153
    label "uprawianie"
  ]
  node [
    id 154
    label "wakowa&#263;"
  ]
  node [
    id 155
    label "powierzanie"
  ]
  node [
    id 156
    label "postawi&#263;"
  ]
  node [
    id 157
    label "miejsce"
  ]
  node [
    id 158
    label "awansowanie"
  ]
  node [
    id 159
    label "praca"
  ]
  node [
    id 160
    label "pos&#322;uchanie"
  ]
  node [
    id 161
    label "s&#261;d"
  ]
  node [
    id 162
    label "sparafrazowanie"
  ]
  node [
    id 163
    label "strawestowa&#263;"
  ]
  node [
    id 164
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 165
    label "trawestowa&#263;"
  ]
  node [
    id 166
    label "sparafrazowa&#263;"
  ]
  node [
    id 167
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 168
    label "sformu&#322;owanie"
  ]
  node [
    id 169
    label "parafrazowanie"
  ]
  node [
    id 170
    label "ozdobnik"
  ]
  node [
    id 171
    label "delimitacja"
  ]
  node [
    id 172
    label "parafrazowa&#263;"
  ]
  node [
    id 173
    label "stylizacja"
  ]
  node [
    id 174
    label "komunikat"
  ]
  node [
    id 175
    label "trawestowanie"
  ]
  node [
    id 176
    label "strawestowanie"
  ]
  node [
    id 177
    label "rezultat"
  ]
  node [
    id 178
    label "Mazowsze"
  ]
  node [
    id 179
    label "whole"
  ]
  node [
    id 180
    label "skupienie"
  ]
  node [
    id 181
    label "The_Beatles"
  ]
  node [
    id 182
    label "zabudowania"
  ]
  node [
    id 183
    label "group"
  ]
  node [
    id 184
    label "zespolik"
  ]
  node [
    id 185
    label "schorzenie"
  ]
  node [
    id 186
    label "ro&#347;lina"
  ]
  node [
    id 187
    label "Depeche_Mode"
  ]
  node [
    id 188
    label "batch"
  ]
  node [
    id 189
    label "decline"
  ]
  node [
    id 190
    label "kolor"
  ]
  node [
    id 191
    label "niszczy&#263;_si&#281;"
  ]
  node [
    id 192
    label "dematerializowa&#263;_si&#281;"
  ]
  node [
    id 193
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 194
    label "tarnish"
  ]
  node [
    id 195
    label "przype&#322;za&#263;"
  ]
  node [
    id 196
    label "bledn&#261;&#263;"
  ]
  node [
    id 197
    label "burze&#263;"
  ]
  node [
    id 198
    label "sta&#263;_si&#281;"
  ]
  node [
    id 199
    label "zbledn&#261;&#263;"
  ]
  node [
    id 200
    label "pale"
  ]
  node [
    id 201
    label "zniszczy&#263;_si&#281;"
  ]
  node [
    id 202
    label "przype&#322;zn&#261;&#263;"
  ]
  node [
    id 203
    label "laryngolog"
  ]
  node [
    id 204
    label "publikacja"
  ]
  node [
    id 205
    label "expense"
  ]
  node [
    id 206
    label "introdukcja"
  ]
  node [
    id 207
    label "wydobywanie"
  ]
  node [
    id 208
    label "przesy&#322;"
  ]
  node [
    id 209
    label "consequence"
  ]
  node [
    id 210
    label "wydzielanie"
  ]
  node [
    id 211
    label "zniszczenie_si&#281;"
  ]
  node [
    id 212
    label "przype&#322;&#378;ni&#281;cie"
  ]
  node [
    id 213
    label "zja&#347;nienie"
  ]
  node [
    id 214
    label "odbarwienie_si&#281;"
  ]
  node [
    id 215
    label "spowodowanie"
  ]
  node [
    id 216
    label "przyt&#322;umiony"
  ]
  node [
    id 217
    label "matowy"
  ]
  node [
    id 218
    label "zmienienie"
  ]
  node [
    id 219
    label "stanie_si&#281;"
  ]
  node [
    id 220
    label "czynno&#347;&#263;"
  ]
  node [
    id 221
    label "zga&#347;ni&#281;cie"
  ]
  node [
    id 222
    label "wyblak&#322;y"
  ]
  node [
    id 223
    label "wyraz_pochodny"
  ]
  node [
    id 224
    label "variation"
  ]
  node [
    id 225
    label "zaburzenie"
  ]
  node [
    id 226
    label "change"
  ]
  node [
    id 227
    label "operator"
  ]
  node [
    id 228
    label "odmiana"
  ]
  node [
    id 229
    label "variety"
  ]
  node [
    id 230
    label "proces_fizjologiczny"
  ]
  node [
    id 231
    label "zamiana"
  ]
  node [
    id 232
    label "mutagenny"
  ]
  node [
    id 233
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 234
    label "gen"
  ]
  node [
    id 235
    label "ga&#347;ni&#281;cie"
  ]
  node [
    id 236
    label "stawanie_si&#281;"
  ]
  node [
    id 237
    label "burzenie"
  ]
  node [
    id 238
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 239
    label "odbarwianie_si&#281;"
  ]
  node [
    id 240
    label "przype&#322;zanie"
  ]
  node [
    id 241
    label "ja&#347;nienie"
  ]
  node [
    id 242
    label "niszczenie_si&#281;"
  ]
  node [
    id 243
    label "choreuta"
  ]
  node [
    id 244
    label "ch&#243;r"
  ]
  node [
    id 245
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 246
    label "strike"
  ]
  node [
    id 247
    label "zaziera&#263;"
  ]
  node [
    id 248
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 249
    label "czu&#263;"
  ]
  node [
    id 250
    label "spotyka&#263;"
  ]
  node [
    id 251
    label "drop"
  ]
  node [
    id 252
    label "pogo"
  ]
  node [
    id 253
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 254
    label "rzecz"
  ]
  node [
    id 255
    label "ogrom"
  ]
  node [
    id 256
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 257
    label "zapach"
  ]
  node [
    id 258
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 259
    label "popada&#263;"
  ]
  node [
    id 260
    label "odwiedza&#263;"
  ]
  node [
    id 261
    label "wymy&#347;la&#263;"
  ]
  node [
    id 262
    label "przypomina&#263;"
  ]
  node [
    id 263
    label "ujmowa&#263;"
  ]
  node [
    id 264
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 265
    label "&#347;wiat&#322;o"
  ]
  node [
    id 266
    label "fall"
  ]
  node [
    id 267
    label "chowa&#263;"
  ]
  node [
    id 268
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 269
    label "demaskowa&#263;"
  ]
  node [
    id 270
    label "ulega&#263;"
  ]
  node [
    id 271
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 272
    label "emocja"
  ]
  node [
    id 273
    label "flatten"
  ]
  node [
    id 274
    label "delivery"
  ]
  node [
    id 275
    label "zdarzenie_si&#281;"
  ]
  node [
    id 276
    label "rendition"
  ]
  node [
    id 277
    label "impression"
  ]
  node [
    id 278
    label "zadenuncjowanie"
  ]
  node [
    id 279
    label "reszta"
  ]
  node [
    id 280
    label "wytworzenie"
  ]
  node [
    id 281
    label "issue"
  ]
  node [
    id 282
    label "danie"
  ]
  node [
    id 283
    label "czasopismo"
  ]
  node [
    id 284
    label "podanie"
  ]
  node [
    id 285
    label "wprowadzenie"
  ]
  node [
    id 286
    label "ujawnienie"
  ]
  node [
    id 287
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 288
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 289
    label "urz&#261;dzenie"
  ]
  node [
    id 290
    label "zrobienie"
  ]
  node [
    id 291
    label "robi&#263;"
  ]
  node [
    id 292
    label "mie&#263;_miejsce"
  ]
  node [
    id 293
    label "plon"
  ]
  node [
    id 294
    label "give"
  ]
  node [
    id 295
    label "surrender"
  ]
  node [
    id 296
    label "kojarzy&#263;"
  ]
  node [
    id 297
    label "impart"
  ]
  node [
    id 298
    label "dawa&#263;"
  ]
  node [
    id 299
    label "wydawnictwo"
  ]
  node [
    id 300
    label "wiano"
  ]
  node [
    id 301
    label "produkcja"
  ]
  node [
    id 302
    label "wprowadza&#263;"
  ]
  node [
    id 303
    label "podawa&#263;"
  ]
  node [
    id 304
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 305
    label "ujawnia&#263;"
  ]
  node [
    id 306
    label "placard"
  ]
  node [
    id 307
    label "powierza&#263;"
  ]
  node [
    id 308
    label "denuncjowa&#263;"
  ]
  node [
    id 309
    label "tajemnica"
  ]
  node [
    id 310
    label "panna_na_wydaniu"
  ]
  node [
    id 311
    label "wytwarza&#263;"
  ]
  node [
    id 312
    label "train"
  ]
  node [
    id 313
    label "wymy&#347;lenie"
  ]
  node [
    id 314
    label "spotkanie"
  ]
  node [
    id 315
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 316
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 317
    label "ulegni&#281;cie"
  ]
  node [
    id 318
    label "collapse"
  ]
  node [
    id 319
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 320
    label "poniesienie"
  ]
  node [
    id 321
    label "ciecz"
  ]
  node [
    id 322
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 323
    label "odwiedzenie"
  ]
  node [
    id 324
    label "uderzenie"
  ]
  node [
    id 325
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 326
    label "rzeka"
  ]
  node [
    id 327
    label "postrzeganie"
  ]
  node [
    id 328
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 329
    label "dostanie_si&#281;"
  ]
  node [
    id 330
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 331
    label "release"
  ]
  node [
    id 332
    label "rozbicie_si&#281;"
  ]
  node [
    id 333
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 334
    label "powierzy&#263;"
  ]
  node [
    id 335
    label "pieni&#261;dze"
  ]
  node [
    id 336
    label "skojarzy&#263;"
  ]
  node [
    id 337
    label "zadenuncjowa&#263;"
  ]
  node [
    id 338
    label "da&#263;"
  ]
  node [
    id 339
    label "zrobi&#263;"
  ]
  node [
    id 340
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 341
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 342
    label "translate"
  ]
  node [
    id 343
    label "picture"
  ]
  node [
    id 344
    label "poda&#263;"
  ]
  node [
    id 345
    label "wprowadzi&#263;"
  ]
  node [
    id 346
    label "wytworzy&#263;"
  ]
  node [
    id 347
    label "dress"
  ]
  node [
    id 348
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 349
    label "supply"
  ]
  node [
    id 350
    label "ujawni&#263;"
  ]
  node [
    id 351
    label "uleganie"
  ]
  node [
    id 352
    label "dostawanie_si&#281;"
  ]
  node [
    id 353
    label "odwiedzanie"
  ]
  node [
    id 354
    label "spotykanie"
  ]
  node [
    id 355
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 356
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 357
    label "wymy&#347;lanie"
  ]
  node [
    id 358
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 359
    label "ingress"
  ]
  node [
    id 360
    label "dzianie_si&#281;"
  ]
  node [
    id 361
    label "wp&#322;ywanie"
  ]
  node [
    id 362
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 363
    label "overlap"
  ]
  node [
    id 364
    label "wkl&#281;sanie"
  ]
  node [
    id 365
    label "ulec"
  ]
  node [
    id 366
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 367
    label "fall_upon"
  ]
  node [
    id 368
    label "ponie&#347;&#263;"
  ]
  node [
    id 369
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 370
    label "uderzy&#263;"
  ]
  node [
    id 371
    label "wymy&#347;li&#263;"
  ]
  node [
    id 372
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 373
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 374
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 375
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 376
    label "spotka&#263;"
  ]
  node [
    id 377
    label "odwiedzi&#263;"
  ]
  node [
    id 378
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 379
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 380
    label "catalog"
  ]
  node [
    id 381
    label "stock"
  ]
  node [
    id 382
    label "pozycja"
  ]
  node [
    id 383
    label "sumariusz"
  ]
  node [
    id 384
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 385
    label "spis"
  ]
  node [
    id 386
    label "tekst"
  ]
  node [
    id 387
    label "check"
  ]
  node [
    id 388
    label "book"
  ]
  node [
    id 389
    label "figurowa&#263;"
  ]
  node [
    id 390
    label "rejestr"
  ]
  node [
    id 391
    label "wyliczanka"
  ]
  node [
    id 392
    label "instrumentacja_g&#322;oskowa"
  ]
  node [
    id 393
    label "leksem"
  ]
  node [
    id 394
    label "figura_s&#322;&#243;w"
  ]
  node [
    id 395
    label "&#347;piew"
  ]
  node [
    id 396
    label "wyra&#380;anie"
  ]
  node [
    id 397
    label "tone"
  ]
  node [
    id 398
    label "wydawanie"
  ]
  node [
    id 399
    label "spirit"
  ]
  node [
    id 400
    label "kolorystyka"
  ]
  node [
    id 401
    label "czyj&#347;"
  ]
  node [
    id 402
    label "m&#261;&#380;"
  ]
  node [
    id 403
    label "prywatny"
  ]
  node [
    id 404
    label "ma&#322;&#380;onek"
  ]
  node [
    id 405
    label "ch&#322;op"
  ]
  node [
    id 406
    label "cz&#322;owiek"
  ]
  node [
    id 407
    label "pan_m&#322;ody"
  ]
  node [
    id 408
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 409
    label "&#347;lubny"
  ]
  node [
    id 410
    label "pan_domu"
  ]
  node [
    id 411
    label "pan_i_w&#322;adca"
  ]
  node [
    id 412
    label "stary"
  ]
  node [
    id 413
    label "belfer"
  ]
  node [
    id 414
    label "murza"
  ]
  node [
    id 415
    label "ojciec"
  ]
  node [
    id 416
    label "samiec"
  ]
  node [
    id 417
    label "androlog"
  ]
  node [
    id 418
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 419
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 420
    label "efendi"
  ]
  node [
    id 421
    label "opiekun"
  ]
  node [
    id 422
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 423
    label "pa&#324;stwo"
  ]
  node [
    id 424
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 425
    label "bratek"
  ]
  node [
    id 426
    label "Mieszko_I"
  ]
  node [
    id 427
    label "Midas"
  ]
  node [
    id 428
    label "bogaty"
  ]
  node [
    id 429
    label "popularyzator"
  ]
  node [
    id 430
    label "pracodawca"
  ]
  node [
    id 431
    label "kszta&#322;ciciel"
  ]
  node [
    id 432
    label "preceptor"
  ]
  node [
    id 433
    label "nabab"
  ]
  node [
    id 434
    label "pupil"
  ]
  node [
    id 435
    label "andropauza"
  ]
  node [
    id 436
    label "zwrot"
  ]
  node [
    id 437
    label "przyw&#243;dca"
  ]
  node [
    id 438
    label "doros&#322;y"
  ]
  node [
    id 439
    label "pedagog"
  ]
  node [
    id 440
    label "rz&#261;dzenie"
  ]
  node [
    id 441
    label "jegomo&#347;&#263;"
  ]
  node [
    id 442
    label "szkolnik"
  ]
  node [
    id 443
    label "ch&#322;opina"
  ]
  node [
    id 444
    label "w&#322;odarz"
  ]
  node [
    id 445
    label "profesor"
  ]
  node [
    id 446
    label "gra_w_karty"
  ]
  node [
    id 447
    label "w&#322;adza"
  ]
  node [
    id 448
    label "Fidel_Castro"
  ]
  node [
    id 449
    label "Anders"
  ]
  node [
    id 450
    label "Ko&#347;ciuszko"
  ]
  node [
    id 451
    label "Tito"
  ]
  node [
    id 452
    label "Miko&#322;ajczyk"
  ]
  node [
    id 453
    label "Sabataj_Cwi"
  ]
  node [
    id 454
    label "lider"
  ]
  node [
    id 455
    label "Mao"
  ]
  node [
    id 456
    label "p&#322;atnik"
  ]
  node [
    id 457
    label "zwierzchnik"
  ]
  node [
    id 458
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 459
    label "nadzorca"
  ]
  node [
    id 460
    label "funkcjonariusz"
  ]
  node [
    id 461
    label "podmiot"
  ]
  node [
    id 462
    label "wykupienie"
  ]
  node [
    id 463
    label "bycie_w_posiadaniu"
  ]
  node [
    id 464
    label "wykupywanie"
  ]
  node [
    id 465
    label "rozszerzyciel"
  ]
  node [
    id 466
    label "ludzko&#347;&#263;"
  ]
  node [
    id 467
    label "wapniak"
  ]
  node [
    id 468
    label "os&#322;abia&#263;"
  ]
  node [
    id 469
    label "posta&#263;"
  ]
  node [
    id 470
    label "hominid"
  ]
  node [
    id 471
    label "podw&#322;adny"
  ]
  node [
    id 472
    label "os&#322;abianie"
  ]
  node [
    id 473
    label "g&#322;owa"
  ]
  node [
    id 474
    label "figura"
  ]
  node [
    id 475
    label "portrecista"
  ]
  node [
    id 476
    label "dwun&#243;g"
  ]
  node [
    id 477
    label "profanum"
  ]
  node [
    id 478
    label "mikrokosmos"
  ]
  node [
    id 479
    label "nasada"
  ]
  node [
    id 480
    label "duch"
  ]
  node [
    id 481
    label "antropochoria"
  ]
  node [
    id 482
    label "osoba"
  ]
  node [
    id 483
    label "wz&#243;r"
  ]
  node [
    id 484
    label "senior"
  ]
  node [
    id 485
    label "oddzia&#322;ywanie"
  ]
  node [
    id 486
    label "Adam"
  ]
  node [
    id 487
    label "homo_sapiens"
  ]
  node [
    id 488
    label "polifag"
  ]
  node [
    id 489
    label "wydoro&#347;lenie"
  ]
  node [
    id 490
    label "du&#380;y"
  ]
  node [
    id 491
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 492
    label "doro&#347;lenie"
  ]
  node [
    id 493
    label "&#378;ra&#322;y"
  ]
  node [
    id 494
    label "doro&#347;le"
  ]
  node [
    id 495
    label "dojrzale"
  ]
  node [
    id 496
    label "dojrza&#322;y"
  ]
  node [
    id 497
    label "m&#261;dry"
  ]
  node [
    id 498
    label "doletni"
  ]
  node [
    id 499
    label "turn"
  ]
  node [
    id 500
    label "turning"
  ]
  node [
    id 501
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 502
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 503
    label "skr&#281;t"
  ]
  node [
    id 504
    label "obr&#243;t"
  ]
  node [
    id 505
    label "fraza_czasownikowa"
  ]
  node [
    id 506
    label "jednostka_leksykalna"
  ]
  node [
    id 507
    label "zmiana"
  ]
  node [
    id 508
    label "wyra&#380;enie"
  ]
  node [
    id 509
    label "starosta"
  ]
  node [
    id 510
    label "zarz&#261;dca"
  ]
  node [
    id 511
    label "w&#322;adca"
  ]
  node [
    id 512
    label "nauczyciel"
  ]
  node [
    id 513
    label "stopie&#324;_naukowy"
  ]
  node [
    id 514
    label "nauczyciel_akademicki"
  ]
  node [
    id 515
    label "tytu&#322;"
  ]
  node [
    id 516
    label "profesura"
  ]
  node [
    id 517
    label "konsulent"
  ]
  node [
    id 518
    label "wirtuoz"
  ]
  node [
    id 519
    label "autor"
  ]
  node [
    id 520
    label "wyprawka"
  ]
  node [
    id 521
    label "mundurek"
  ]
  node [
    id 522
    label "szko&#322;a"
  ]
  node [
    id 523
    label "tarcza"
  ]
  node [
    id 524
    label "elew"
  ]
  node [
    id 525
    label "absolwent"
  ]
  node [
    id 526
    label "klasa"
  ]
  node [
    id 527
    label "ekspert"
  ]
  node [
    id 528
    label "ochotnik"
  ]
  node [
    id 529
    label "pomocnik"
  ]
  node [
    id 530
    label "student"
  ]
  node [
    id 531
    label "nauczyciel_muzyki"
  ]
  node [
    id 532
    label "zakonnik"
  ]
  node [
    id 533
    label "urz&#281;dnik"
  ]
  node [
    id 534
    label "bogacz"
  ]
  node [
    id 535
    label "dostojnik"
  ]
  node [
    id 536
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 537
    label "kuwada"
  ]
  node [
    id 538
    label "tworzyciel"
  ]
  node [
    id 539
    label "rodzice"
  ]
  node [
    id 540
    label "&#347;w"
  ]
  node [
    id 541
    label "pomys&#322;odawca"
  ]
  node [
    id 542
    label "rodzic"
  ]
  node [
    id 543
    label "wykonawca"
  ]
  node [
    id 544
    label "ojczym"
  ]
  node [
    id 545
    label "przodek"
  ]
  node [
    id 546
    label "papa"
  ]
  node [
    id 547
    label "kochanek"
  ]
  node [
    id 548
    label "fio&#322;ek"
  ]
  node [
    id 549
    label "facet"
  ]
  node [
    id 550
    label "brat"
  ]
  node [
    id 551
    label "zwierz&#281;"
  ]
  node [
    id 552
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 553
    label "mo&#347;&#263;"
  ]
  node [
    id 554
    label "Frygia"
  ]
  node [
    id 555
    label "sprawowanie"
  ]
  node [
    id 556
    label "dominion"
  ]
  node [
    id 557
    label "dominowanie"
  ]
  node [
    id 558
    label "reign"
  ]
  node [
    id 559
    label "rule"
  ]
  node [
    id 560
    label "zwierz&#281;_domowe"
  ]
  node [
    id 561
    label "J&#281;drzejewicz"
  ]
  node [
    id 562
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 563
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 564
    label "John_Dewey"
  ]
  node [
    id 565
    label "specjalista"
  ]
  node [
    id 566
    label "&#380;ycie"
  ]
  node [
    id 567
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 568
    label "Turek"
  ]
  node [
    id 569
    label "effendi"
  ]
  node [
    id 570
    label "obfituj&#261;cy"
  ]
  node [
    id 571
    label "r&#243;&#380;norodny"
  ]
  node [
    id 572
    label "spania&#322;y"
  ]
  node [
    id 573
    label "obficie"
  ]
  node [
    id 574
    label "sytuowany"
  ]
  node [
    id 575
    label "och&#281;do&#380;ny"
  ]
  node [
    id 576
    label "forsiasty"
  ]
  node [
    id 577
    label "zapa&#347;ny"
  ]
  node [
    id 578
    label "bogato"
  ]
  node [
    id 579
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 580
    label "Katar"
  ]
  node [
    id 581
    label "Libia"
  ]
  node [
    id 582
    label "Gwatemala"
  ]
  node [
    id 583
    label "Ekwador"
  ]
  node [
    id 584
    label "Afganistan"
  ]
  node [
    id 585
    label "Tad&#380;ykistan"
  ]
  node [
    id 586
    label "Bhutan"
  ]
  node [
    id 587
    label "Argentyna"
  ]
  node [
    id 588
    label "D&#380;ibuti"
  ]
  node [
    id 589
    label "Wenezuela"
  ]
  node [
    id 590
    label "Gabon"
  ]
  node [
    id 591
    label "Ukraina"
  ]
  node [
    id 592
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 593
    label "Rwanda"
  ]
  node [
    id 594
    label "Liechtenstein"
  ]
  node [
    id 595
    label "Sri_Lanka"
  ]
  node [
    id 596
    label "Madagaskar"
  ]
  node [
    id 597
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 598
    label "Kongo"
  ]
  node [
    id 599
    label "Tonga"
  ]
  node [
    id 600
    label "Bangladesz"
  ]
  node [
    id 601
    label "Kanada"
  ]
  node [
    id 602
    label "Wehrlen"
  ]
  node [
    id 603
    label "Algieria"
  ]
  node [
    id 604
    label "Uganda"
  ]
  node [
    id 605
    label "Surinam"
  ]
  node [
    id 606
    label "Sahara_Zachodnia"
  ]
  node [
    id 607
    label "Chile"
  ]
  node [
    id 608
    label "W&#281;gry"
  ]
  node [
    id 609
    label "Birma"
  ]
  node [
    id 610
    label "Kazachstan"
  ]
  node [
    id 611
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 612
    label "Armenia"
  ]
  node [
    id 613
    label "Tuwalu"
  ]
  node [
    id 614
    label "Timor_Wschodni"
  ]
  node [
    id 615
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 616
    label "Izrael"
  ]
  node [
    id 617
    label "Estonia"
  ]
  node [
    id 618
    label "Komory"
  ]
  node [
    id 619
    label "Kamerun"
  ]
  node [
    id 620
    label "Haiti"
  ]
  node [
    id 621
    label "Belize"
  ]
  node [
    id 622
    label "Sierra_Leone"
  ]
  node [
    id 623
    label "Luksemburg"
  ]
  node [
    id 624
    label "USA"
  ]
  node [
    id 625
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 626
    label "Barbados"
  ]
  node [
    id 627
    label "San_Marino"
  ]
  node [
    id 628
    label "Bu&#322;garia"
  ]
  node [
    id 629
    label "Indonezja"
  ]
  node [
    id 630
    label "Wietnam"
  ]
  node [
    id 631
    label "Malawi"
  ]
  node [
    id 632
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 633
    label "Francja"
  ]
  node [
    id 634
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 635
    label "Zambia"
  ]
  node [
    id 636
    label "Angola"
  ]
  node [
    id 637
    label "Grenada"
  ]
  node [
    id 638
    label "Nepal"
  ]
  node [
    id 639
    label "Panama"
  ]
  node [
    id 640
    label "Rumunia"
  ]
  node [
    id 641
    label "Czarnog&#243;ra"
  ]
  node [
    id 642
    label "Malediwy"
  ]
  node [
    id 643
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 644
    label "S&#322;owacja"
  ]
  node [
    id 645
    label "para"
  ]
  node [
    id 646
    label "Egipt"
  ]
  node [
    id 647
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 648
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 649
    label "Mozambik"
  ]
  node [
    id 650
    label "Kolumbia"
  ]
  node [
    id 651
    label "Laos"
  ]
  node [
    id 652
    label "Burundi"
  ]
  node [
    id 653
    label "Suazi"
  ]
  node [
    id 654
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 655
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 656
    label "Czechy"
  ]
  node [
    id 657
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 658
    label "Wyspy_Marshalla"
  ]
  node [
    id 659
    label "Dominika"
  ]
  node [
    id 660
    label "Trynidad_i_Tobago"
  ]
  node [
    id 661
    label "Syria"
  ]
  node [
    id 662
    label "Palau"
  ]
  node [
    id 663
    label "Gwinea_Bissau"
  ]
  node [
    id 664
    label "Liberia"
  ]
  node [
    id 665
    label "Jamajka"
  ]
  node [
    id 666
    label "Zimbabwe"
  ]
  node [
    id 667
    label "Polska"
  ]
  node [
    id 668
    label "Dominikana"
  ]
  node [
    id 669
    label "Senegal"
  ]
  node [
    id 670
    label "Togo"
  ]
  node [
    id 671
    label "Gujana"
  ]
  node [
    id 672
    label "Gruzja"
  ]
  node [
    id 673
    label "Albania"
  ]
  node [
    id 674
    label "Zair"
  ]
  node [
    id 675
    label "Meksyk"
  ]
  node [
    id 676
    label "Macedonia"
  ]
  node [
    id 677
    label "Chorwacja"
  ]
  node [
    id 678
    label "Kambod&#380;a"
  ]
  node [
    id 679
    label "Monako"
  ]
  node [
    id 680
    label "Mauritius"
  ]
  node [
    id 681
    label "Gwinea"
  ]
  node [
    id 682
    label "Mali"
  ]
  node [
    id 683
    label "Nigeria"
  ]
  node [
    id 684
    label "Kostaryka"
  ]
  node [
    id 685
    label "Hanower"
  ]
  node [
    id 686
    label "Paragwaj"
  ]
  node [
    id 687
    label "W&#322;ochy"
  ]
  node [
    id 688
    label "Seszele"
  ]
  node [
    id 689
    label "Wyspy_Salomona"
  ]
  node [
    id 690
    label "Hiszpania"
  ]
  node [
    id 691
    label "Boliwia"
  ]
  node [
    id 692
    label "Kirgistan"
  ]
  node [
    id 693
    label "Irlandia"
  ]
  node [
    id 694
    label "Czad"
  ]
  node [
    id 695
    label "Irak"
  ]
  node [
    id 696
    label "Lesoto"
  ]
  node [
    id 697
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 698
    label "Malta"
  ]
  node [
    id 699
    label "Andora"
  ]
  node [
    id 700
    label "Chiny"
  ]
  node [
    id 701
    label "Filipiny"
  ]
  node [
    id 702
    label "Antarktis"
  ]
  node [
    id 703
    label "Niemcy"
  ]
  node [
    id 704
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 705
    label "Pakistan"
  ]
  node [
    id 706
    label "terytorium"
  ]
  node [
    id 707
    label "Nikaragua"
  ]
  node [
    id 708
    label "Brazylia"
  ]
  node [
    id 709
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 710
    label "Maroko"
  ]
  node [
    id 711
    label "Portugalia"
  ]
  node [
    id 712
    label "Niger"
  ]
  node [
    id 713
    label "Kenia"
  ]
  node [
    id 714
    label "Botswana"
  ]
  node [
    id 715
    label "Fid&#380;i"
  ]
  node [
    id 716
    label "Tunezja"
  ]
  node [
    id 717
    label "Australia"
  ]
  node [
    id 718
    label "Tajlandia"
  ]
  node [
    id 719
    label "Burkina_Faso"
  ]
  node [
    id 720
    label "interior"
  ]
  node [
    id 721
    label "Tanzania"
  ]
  node [
    id 722
    label "Benin"
  ]
  node [
    id 723
    label "Indie"
  ]
  node [
    id 724
    label "&#321;otwa"
  ]
  node [
    id 725
    label "Kiribati"
  ]
  node [
    id 726
    label "Antigua_i_Barbuda"
  ]
  node [
    id 727
    label "Rodezja"
  ]
  node [
    id 728
    label "Cypr"
  ]
  node [
    id 729
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 730
    label "Peru"
  ]
  node [
    id 731
    label "Austria"
  ]
  node [
    id 732
    label "Urugwaj"
  ]
  node [
    id 733
    label "Jordania"
  ]
  node [
    id 734
    label "Grecja"
  ]
  node [
    id 735
    label "Azerbejd&#380;an"
  ]
  node [
    id 736
    label "Turcja"
  ]
  node [
    id 737
    label "Samoa"
  ]
  node [
    id 738
    label "Sudan"
  ]
  node [
    id 739
    label "Oman"
  ]
  node [
    id 740
    label "ziemia"
  ]
  node [
    id 741
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 742
    label "Uzbekistan"
  ]
  node [
    id 743
    label "Portoryko"
  ]
  node [
    id 744
    label "Honduras"
  ]
  node [
    id 745
    label "Mongolia"
  ]
  node [
    id 746
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 747
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 748
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 749
    label "Serbia"
  ]
  node [
    id 750
    label "Tajwan"
  ]
  node [
    id 751
    label "Wielka_Brytania"
  ]
  node [
    id 752
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 753
    label "Liban"
  ]
  node [
    id 754
    label "Japonia"
  ]
  node [
    id 755
    label "Ghana"
  ]
  node [
    id 756
    label "Belgia"
  ]
  node [
    id 757
    label "Bahrajn"
  ]
  node [
    id 758
    label "Mikronezja"
  ]
  node [
    id 759
    label "Etiopia"
  ]
  node [
    id 760
    label "Kuwejt"
  ]
  node [
    id 761
    label "Bahamy"
  ]
  node [
    id 762
    label "Rosja"
  ]
  node [
    id 763
    label "Mo&#322;dawia"
  ]
  node [
    id 764
    label "Litwa"
  ]
  node [
    id 765
    label "S&#322;owenia"
  ]
  node [
    id 766
    label "Szwajcaria"
  ]
  node [
    id 767
    label "Erytrea"
  ]
  node [
    id 768
    label "Arabia_Saudyjska"
  ]
  node [
    id 769
    label "Kuba"
  ]
  node [
    id 770
    label "granica_pa&#324;stwa"
  ]
  node [
    id 771
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 772
    label "Malezja"
  ]
  node [
    id 773
    label "Korea"
  ]
  node [
    id 774
    label "Jemen"
  ]
  node [
    id 775
    label "Nowa_Zelandia"
  ]
  node [
    id 776
    label "Namibia"
  ]
  node [
    id 777
    label "Nauru"
  ]
  node [
    id 778
    label "holoarktyka"
  ]
  node [
    id 779
    label "Brunei"
  ]
  node [
    id 780
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 781
    label "Khitai"
  ]
  node [
    id 782
    label "Mauretania"
  ]
  node [
    id 783
    label "Iran"
  ]
  node [
    id 784
    label "Gambia"
  ]
  node [
    id 785
    label "Somalia"
  ]
  node [
    id 786
    label "Holandia"
  ]
  node [
    id 787
    label "Turkmenistan"
  ]
  node [
    id 788
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 789
    label "Salwador"
  ]
  node [
    id 790
    label "ablegat"
  ]
  node [
    id 791
    label "izba_ni&#380;sza"
  ]
  node [
    id 792
    label "Korwin"
  ]
  node [
    id 793
    label "dyscyplina_partyjna"
  ]
  node [
    id 794
    label "kurier_dyplomatyczny"
  ]
  node [
    id 795
    label "wys&#322;annik"
  ]
  node [
    id 796
    label "poselstwo"
  ]
  node [
    id 797
    label "parlamentarzysta"
  ]
  node [
    id 798
    label "przedstawiciel"
  ]
  node [
    id 799
    label "dyplomata"
  ]
  node [
    id 800
    label "klubista"
  ]
  node [
    id 801
    label "reprezentacja"
  ]
  node [
    id 802
    label "klub"
  ]
  node [
    id 803
    label "cz&#322;onek"
  ]
  node [
    id 804
    label "mandatariusz"
  ]
  node [
    id 805
    label "grupa_bilateralna"
  ]
  node [
    id 806
    label "polityk"
  ]
  node [
    id 807
    label "parlament"
  ]
  node [
    id 808
    label "mi&#322;y"
  ]
  node [
    id 809
    label "korpus_dyplomatyczny"
  ]
  node [
    id 810
    label "dyplomatyczny"
  ]
  node [
    id 811
    label "takt"
  ]
  node [
    id 812
    label "Metternich"
  ]
  node [
    id 813
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 814
    label "przyk&#322;ad"
  ]
  node [
    id 815
    label "substytuowa&#263;"
  ]
  node [
    id 816
    label "substytuowanie"
  ]
  node [
    id 817
    label "zast&#281;pca"
  ]
  node [
    id 818
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 819
    label "umocowa&#263;"
  ]
  node [
    id 820
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 821
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 822
    label "procesualistyka"
  ]
  node [
    id 823
    label "regu&#322;a_Allena"
  ]
  node [
    id 824
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 825
    label "kryminalistyka"
  ]
  node [
    id 826
    label "struktura"
  ]
  node [
    id 827
    label "kierunek"
  ]
  node [
    id 828
    label "zasada_d'Alemberta"
  ]
  node [
    id 829
    label "obserwacja"
  ]
  node [
    id 830
    label "normatywizm"
  ]
  node [
    id 831
    label "jurisprudence"
  ]
  node [
    id 832
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 833
    label "kultura_duchowa"
  ]
  node [
    id 834
    label "przepis"
  ]
  node [
    id 835
    label "prawo_karne_procesowe"
  ]
  node [
    id 836
    label "criterion"
  ]
  node [
    id 837
    label "kazuistyka"
  ]
  node [
    id 838
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 839
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 840
    label "kryminologia"
  ]
  node [
    id 841
    label "opis"
  ]
  node [
    id 842
    label "regu&#322;a_Glogera"
  ]
  node [
    id 843
    label "prawo_Mendla"
  ]
  node [
    id 844
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 845
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 846
    label "prawo_karne"
  ]
  node [
    id 847
    label "legislacyjnie"
  ]
  node [
    id 848
    label "twierdzenie"
  ]
  node [
    id 849
    label "cywilistyka"
  ]
  node [
    id 850
    label "judykatura"
  ]
  node [
    id 851
    label "kanonistyka"
  ]
  node [
    id 852
    label "standard"
  ]
  node [
    id 853
    label "nauka_prawa"
  ]
  node [
    id 854
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 855
    label "law"
  ]
  node [
    id 856
    label "qualification"
  ]
  node [
    id 857
    label "wykonawczy"
  ]
  node [
    id 858
    label "zasada"
  ]
  node [
    id 859
    label "normalizacja"
  ]
  node [
    id 860
    label "exposition"
  ]
  node [
    id 861
    label "obja&#347;nienie"
  ]
  node [
    id 862
    label "model"
  ]
  node [
    id 863
    label "organizowa&#263;"
  ]
  node [
    id 864
    label "ordinariness"
  ]
  node [
    id 865
    label "instytucja"
  ]
  node [
    id 866
    label "zorganizowa&#263;"
  ]
  node [
    id 867
    label "taniec_towarzyski"
  ]
  node [
    id 868
    label "organizowanie"
  ]
  node [
    id 869
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 870
    label "zorganizowanie"
  ]
  node [
    id 871
    label "mechanika"
  ]
  node [
    id 872
    label "o&#347;"
  ]
  node [
    id 873
    label "usenet"
  ]
  node [
    id 874
    label "rozprz&#261;c"
  ]
  node [
    id 875
    label "zachowanie"
  ]
  node [
    id 876
    label "cybernetyk"
  ]
  node [
    id 877
    label "podsystem"
  ]
  node [
    id 878
    label "system"
  ]
  node [
    id 879
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 880
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 881
    label "sk&#322;ad"
  ]
  node [
    id 882
    label "systemat"
  ]
  node [
    id 883
    label "konstrukcja"
  ]
  node [
    id 884
    label "konstelacja"
  ]
  node [
    id 885
    label "przebieg"
  ]
  node [
    id 886
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 887
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 888
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 889
    label "praktyka"
  ]
  node [
    id 890
    label "przeorientowywanie"
  ]
  node [
    id 891
    label "studia"
  ]
  node [
    id 892
    label "linia"
  ]
  node [
    id 893
    label "bok"
  ]
  node [
    id 894
    label "skr&#281;canie"
  ]
  node [
    id 895
    label "skr&#281;ca&#263;"
  ]
  node [
    id 896
    label "przeorientowywa&#263;"
  ]
  node [
    id 897
    label "orientowanie"
  ]
  node [
    id 898
    label "skr&#281;ci&#263;"
  ]
  node [
    id 899
    label "przeorientowanie"
  ]
  node [
    id 900
    label "zorientowanie"
  ]
  node [
    id 901
    label "przeorientowa&#263;"
  ]
  node [
    id 902
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 903
    label "metoda"
  ]
  node [
    id 904
    label "ty&#322;"
  ]
  node [
    id 905
    label "zorientowa&#263;"
  ]
  node [
    id 906
    label "g&#243;ra"
  ]
  node [
    id 907
    label "orientowa&#263;"
  ]
  node [
    id 908
    label "spos&#243;b"
  ]
  node [
    id 909
    label "ideologia"
  ]
  node [
    id 910
    label "orientacja"
  ]
  node [
    id 911
    label "prz&#243;d"
  ]
  node [
    id 912
    label "bearing"
  ]
  node [
    id 913
    label "skr&#281;cenie"
  ]
  node [
    id 914
    label "do&#347;wiadczenie"
  ]
  node [
    id 915
    label "teren_szko&#322;y"
  ]
  node [
    id 916
    label "wiedza"
  ]
  node [
    id 917
    label "Mickiewicz"
  ]
  node [
    id 918
    label "kwalifikacje"
  ]
  node [
    id 919
    label "podr&#281;cznik"
  ]
  node [
    id 920
    label "school"
  ]
  node [
    id 921
    label "zda&#263;"
  ]
  node [
    id 922
    label "gabinet"
  ]
  node [
    id 923
    label "urszulanki"
  ]
  node [
    id 924
    label "sztuba"
  ]
  node [
    id 925
    label "&#322;awa_szkolna"
  ]
  node [
    id 926
    label "nauka"
  ]
  node [
    id 927
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 928
    label "przepisa&#263;"
  ]
  node [
    id 929
    label "muzyka"
  ]
  node [
    id 930
    label "form"
  ]
  node [
    id 931
    label "lekcja"
  ]
  node [
    id 932
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 933
    label "przepisanie"
  ]
  node [
    id 934
    label "czas"
  ]
  node [
    id 935
    label "skolaryzacja"
  ]
  node [
    id 936
    label "zdanie"
  ]
  node [
    id 937
    label "stopek"
  ]
  node [
    id 938
    label "sekretariat"
  ]
  node [
    id 939
    label "lesson"
  ]
  node [
    id 940
    label "niepokalanki"
  ]
  node [
    id 941
    label "siedziba"
  ]
  node [
    id 942
    label "szkolenie"
  ]
  node [
    id 943
    label "kara"
  ]
  node [
    id 944
    label "tablica"
  ]
  node [
    id 945
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 946
    label "wydarzenie"
  ]
  node [
    id 947
    label "wyb&#243;r"
  ]
  node [
    id 948
    label "prospect"
  ]
  node [
    id 949
    label "alternatywa"
  ]
  node [
    id 950
    label "operator_modalny"
  ]
  node [
    id 951
    label "badanie"
  ]
  node [
    id 952
    label "proces_my&#347;lowy"
  ]
  node [
    id 953
    label "remark"
  ]
  node [
    id 954
    label "stwierdzenie"
  ]
  node [
    id 955
    label "observation"
  ]
  node [
    id 956
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 957
    label "alternatywa_Fredholma"
  ]
  node [
    id 958
    label "oznajmianie"
  ]
  node [
    id 959
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 960
    label "teoria"
  ]
  node [
    id 961
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 962
    label "paradoks_Leontiefa"
  ]
  node [
    id 963
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 964
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 965
    label "teza"
  ]
  node [
    id 966
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 967
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 968
    label "twierdzenie_Pettisa"
  ]
  node [
    id 969
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 970
    label "twierdzenie_Maya"
  ]
  node [
    id 971
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 972
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 973
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 974
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 975
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 976
    label "zapewnianie"
  ]
  node [
    id 977
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 978
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 979
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 980
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 981
    label "twierdzenie_Stokesa"
  ]
  node [
    id 982
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 983
    label "twierdzenie_Cevy"
  ]
  node [
    id 984
    label "twierdzenie_Pascala"
  ]
  node [
    id 985
    label "proposition"
  ]
  node [
    id 986
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 987
    label "komunikowanie"
  ]
  node [
    id 988
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 989
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 990
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 991
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 992
    label "relacja"
  ]
  node [
    id 993
    label "calibration"
  ]
  node [
    id 994
    label "operacja"
  ]
  node [
    id 995
    label "porz&#261;dek"
  ]
  node [
    id 996
    label "dominance"
  ]
  node [
    id 997
    label "zabieg"
  ]
  node [
    id 998
    label "standardization"
  ]
  node [
    id 999
    label "orzecznictwo"
  ]
  node [
    id 1000
    label "wykonawczo"
  ]
  node [
    id 1001
    label "byt"
  ]
  node [
    id 1002
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1003
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 1004
    label "set"
  ]
  node [
    id 1005
    label "nada&#263;"
  ]
  node [
    id 1006
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 1007
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 1008
    label "cook"
  ]
  node [
    id 1009
    label "status"
  ]
  node [
    id 1010
    label "procedura"
  ]
  node [
    id 1011
    label "norma_prawna"
  ]
  node [
    id 1012
    label "przedawnienie_si&#281;"
  ]
  node [
    id 1013
    label "przedawnianie_si&#281;"
  ]
  node [
    id 1014
    label "porada"
  ]
  node [
    id 1015
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 1016
    label "regulation"
  ]
  node [
    id 1017
    label "recepta"
  ]
  node [
    id 1018
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 1019
    label "kodeks"
  ]
  node [
    id 1020
    label "base"
  ]
  node [
    id 1021
    label "umowa"
  ]
  node [
    id 1022
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1023
    label "moralno&#347;&#263;"
  ]
  node [
    id 1024
    label "occupation"
  ]
  node [
    id 1025
    label "podstawa"
  ]
  node [
    id 1026
    label "substancja"
  ]
  node [
    id 1027
    label "prawid&#322;o"
  ]
  node [
    id 1028
    label "casuistry"
  ]
  node [
    id 1029
    label "manipulacja"
  ]
  node [
    id 1030
    label "probabilizm"
  ]
  node [
    id 1031
    label "dermatoglifika"
  ]
  node [
    id 1032
    label "mikro&#347;lad"
  ]
  node [
    id 1033
    label "technika_&#347;ledcza"
  ]
  node [
    id 1034
    label "dzia&#322;"
  ]
  node [
    id 1035
    label "nemezis"
  ]
  node [
    id 1036
    label "konsekwencja"
  ]
  node [
    id 1037
    label "punishment"
  ]
  node [
    id 1038
    label "righteousness"
  ]
  node [
    id 1039
    label "roboty_przymusowe"
  ]
  node [
    id 1040
    label "odczuwa&#263;"
  ]
  node [
    id 1041
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 1042
    label "skrupienie_si&#281;"
  ]
  node [
    id 1043
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 1044
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 1045
    label "odczucie"
  ]
  node [
    id 1046
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 1047
    label "koszula_Dejaniry"
  ]
  node [
    id 1048
    label "odczuwanie"
  ]
  node [
    id 1049
    label "event"
  ]
  node [
    id 1050
    label "skrupianie_si&#281;"
  ]
  node [
    id 1051
    label "odczu&#263;"
  ]
  node [
    id 1052
    label "charakterystyka"
  ]
  node [
    id 1053
    label "m&#322;ot"
  ]
  node [
    id 1054
    label "znak"
  ]
  node [
    id 1055
    label "drzewo"
  ]
  node [
    id 1056
    label "pr&#243;ba"
  ]
  node [
    id 1057
    label "attribute"
  ]
  node [
    id 1058
    label "marka"
  ]
  node [
    id 1059
    label "Piotr"
  ]
  node [
    id 1060
    label "Babinetz"
  ]
  node [
    id 1061
    label "i"
  ]
  node [
    id 1062
    label "Mariusz"
  ]
  node [
    id 1063
    label "Kami&#324;ski"
  ]
  node [
    id 1064
    label "republika&#324;ski"
  ]
  node [
    id 1065
    label "unia"
  ]
  node [
    id 1066
    label "wolno&#347;&#263;"
  ]
  node [
    id 1067
    label "konstytucja"
  ]
  node [
    id 1068
    label "kwietniowy"
  ]
  node [
    id 1069
    label "rz&#261;d"
  ]
  node [
    id 1070
    label "polski"
  ]
  node [
    id 1071
    label "na"
  ]
  node [
    id 1072
    label "uchod&#378;stwo"
  ]
  node [
    id 1073
    label "urz&#261;d"
  ]
  node [
    id 1074
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 1075
    label "informacja"
  ]
  node [
    id 1076
    label "wojskowy"
  ]
  node [
    id 1077
    label "s&#322;u&#380;ba"
  ]
  node [
    id 1078
    label "wewn&#281;trzny"
  ]
  node [
    id 1079
    label "zwi&#261;zkowy"
  ]
  node [
    id 1080
    label "sowiecki"
  ]
  node [
    id 1081
    label "zwi&#261;zek"
  ]
  node [
    id 1082
    label "platforma"
  ]
  node [
    id 1083
    label "obywatelski"
  ]
  node [
    id 1084
    label "J&#243;zefa"
  ]
  node [
    id 1085
    label "Franczaka"
  ]
  node [
    id 1086
    label "Jerzy"
  ]
  node [
    id 1087
    label "Szmajdzi&#324;ski"
  ]
  node [
    id 1088
    label "Franciszka"
  ]
  node [
    id 1089
    label "Stefaniuk"
  ]
  node [
    id 1090
    label "polskie"
  ]
  node [
    id 1091
    label "stronnictwo"
  ]
  node [
    id 1092
    label "ludowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 1
    target 402
  ]
  edge [
    source 1
    target 403
  ]
  edge [
    source 1
    target 404
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 405
  ]
  edge [
    source 1
    target 406
  ]
  edge [
    source 1
    target 407
  ]
  edge [
    source 1
    target 408
  ]
  edge [
    source 1
    target 409
  ]
  edge [
    source 1
    target 410
  ]
  edge [
    source 1
    target 411
  ]
  edge [
    source 1
    target 412
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 2
    target 520
  ]
  edge [
    source 2
    target 521
  ]
  edge [
    source 2
    target 522
  ]
  edge [
    source 2
    target 523
  ]
  edge [
    source 2
    target 524
  ]
  edge [
    source 2
    target 525
  ]
  edge [
    source 2
    target 526
  ]
  edge [
    source 2
    target 527
  ]
  edge [
    source 2
    target 528
  ]
  edge [
    source 2
    target 529
  ]
  edge [
    source 2
    target 530
  ]
  edge [
    source 2
    target 531
  ]
  edge [
    source 2
    target 532
  ]
  edge [
    source 2
    target 533
  ]
  edge [
    source 2
    target 534
  ]
  edge [
    source 2
    target 535
  ]
  edge [
    source 2
    target 536
  ]
  edge [
    source 2
    target 537
  ]
  edge [
    source 2
    target 538
  ]
  edge [
    source 2
    target 539
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 540
  ]
  edge [
    source 2
    target 541
  ]
  edge [
    source 2
    target 542
  ]
  edge [
    source 2
    target 543
  ]
  edge [
    source 2
    target 544
  ]
  edge [
    source 2
    target 545
  ]
  edge [
    source 2
    target 546
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 547
  ]
  edge [
    source 2
    target 548
  ]
  edge [
    source 2
    target 549
  ]
  edge [
    source 2
    target 550
  ]
  edge [
    source 2
    target 551
  ]
  edge [
    source 2
    target 552
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 553
  ]
  edge [
    source 2
    target 554
  ]
  edge [
    source 2
    target 555
  ]
  edge [
    source 2
    target 556
  ]
  edge [
    source 2
    target 557
  ]
  edge [
    source 2
    target 558
  ]
  edge [
    source 2
    target 559
  ]
  edge [
    source 2
    target 560
  ]
  edge [
    source 2
    target 561
  ]
  edge [
    source 2
    target 562
  ]
  edge [
    source 2
    target 563
  ]
  edge [
    source 2
    target 564
  ]
  edge [
    source 2
    target 565
  ]
  edge [
    source 2
    target 566
  ]
  edge [
    source 2
    target 567
  ]
  edge [
    source 2
    target 568
  ]
  edge [
    source 2
    target 569
  ]
  edge [
    source 2
    target 570
  ]
  edge [
    source 2
    target 571
  ]
  edge [
    source 2
    target 572
  ]
  edge [
    source 2
    target 573
  ]
  edge [
    source 2
    target 574
  ]
  edge [
    source 2
    target 575
  ]
  edge [
    source 2
    target 576
  ]
  edge [
    source 2
    target 577
  ]
  edge [
    source 2
    target 578
  ]
  edge [
    source 2
    target 579
  ]
  edge [
    source 2
    target 580
  ]
  edge [
    source 2
    target 581
  ]
  edge [
    source 2
    target 582
  ]
  edge [
    source 2
    target 583
  ]
  edge [
    source 2
    target 584
  ]
  edge [
    source 2
    target 585
  ]
  edge [
    source 2
    target 586
  ]
  edge [
    source 2
    target 587
  ]
  edge [
    source 2
    target 588
  ]
  edge [
    source 2
    target 589
  ]
  edge [
    source 2
    target 590
  ]
  edge [
    source 2
    target 591
  ]
  edge [
    source 2
    target 592
  ]
  edge [
    source 2
    target 593
  ]
  edge [
    source 2
    target 594
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 595
  ]
  edge [
    source 2
    target 596
  ]
  edge [
    source 2
    target 597
  ]
  edge [
    source 2
    target 598
  ]
  edge [
    source 2
    target 599
  ]
  edge [
    source 2
    target 600
  ]
  edge [
    source 2
    target 601
  ]
  edge [
    source 2
    target 602
  ]
  edge [
    source 2
    target 603
  ]
  edge [
    source 2
    target 604
  ]
  edge [
    source 2
    target 605
  ]
  edge [
    source 2
    target 606
  ]
  edge [
    source 2
    target 607
  ]
  edge [
    source 2
    target 608
  ]
  edge [
    source 2
    target 609
  ]
  edge [
    source 2
    target 610
  ]
  edge [
    source 2
    target 611
  ]
  edge [
    source 2
    target 612
  ]
  edge [
    source 2
    target 613
  ]
  edge [
    source 2
    target 614
  ]
  edge [
    source 2
    target 615
  ]
  edge [
    source 2
    target 616
  ]
  edge [
    source 2
    target 617
  ]
  edge [
    source 2
    target 618
  ]
  edge [
    source 2
    target 619
  ]
  edge [
    source 2
    target 620
  ]
  edge [
    source 2
    target 621
  ]
  edge [
    source 2
    target 622
  ]
  edge [
    source 2
    target 623
  ]
  edge [
    source 2
    target 624
  ]
  edge [
    source 2
    target 625
  ]
  edge [
    source 2
    target 626
  ]
  edge [
    source 2
    target 627
  ]
  edge [
    source 2
    target 628
  ]
  edge [
    source 2
    target 629
  ]
  edge [
    source 2
    target 630
  ]
  edge [
    source 2
    target 631
  ]
  edge [
    source 2
    target 632
  ]
  edge [
    source 2
    target 633
  ]
  edge [
    source 2
    target 634
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 635
  ]
  edge [
    source 2
    target 636
  ]
  edge [
    source 2
    target 637
  ]
  edge [
    source 2
    target 638
  ]
  edge [
    source 2
    target 639
  ]
  edge [
    source 2
    target 640
  ]
  edge [
    source 2
    target 641
  ]
  edge [
    source 2
    target 642
  ]
  edge [
    source 2
    target 643
  ]
  edge [
    source 2
    target 644
  ]
  edge [
    source 2
    target 645
  ]
  edge [
    source 2
    target 646
  ]
  edge [
    source 2
    target 647
  ]
  edge [
    source 2
    target 648
  ]
  edge [
    source 2
    target 649
  ]
  edge [
    source 2
    target 650
  ]
  edge [
    source 2
    target 651
  ]
  edge [
    source 2
    target 652
  ]
  edge [
    source 2
    target 653
  ]
  edge [
    source 2
    target 654
  ]
  edge [
    source 2
    target 655
  ]
  edge [
    source 2
    target 656
  ]
  edge [
    source 2
    target 657
  ]
  edge [
    source 2
    target 658
  ]
  edge [
    source 2
    target 659
  ]
  edge [
    source 2
    target 660
  ]
  edge [
    source 2
    target 661
  ]
  edge [
    source 2
    target 662
  ]
  edge [
    source 2
    target 663
  ]
  edge [
    source 2
    target 664
  ]
  edge [
    source 2
    target 665
  ]
  edge [
    source 2
    target 666
  ]
  edge [
    source 2
    target 667
  ]
  edge [
    source 2
    target 668
  ]
  edge [
    source 2
    target 669
  ]
  edge [
    source 2
    target 670
  ]
  edge [
    source 2
    target 671
  ]
  edge [
    source 2
    target 672
  ]
  edge [
    source 2
    target 673
  ]
  edge [
    source 2
    target 674
  ]
  edge [
    source 2
    target 675
  ]
  edge [
    source 2
    target 676
  ]
  edge [
    source 2
    target 677
  ]
  edge [
    source 2
    target 678
  ]
  edge [
    source 2
    target 679
  ]
  edge [
    source 2
    target 680
  ]
  edge [
    source 2
    target 681
  ]
  edge [
    source 2
    target 682
  ]
  edge [
    source 2
    target 683
  ]
  edge [
    source 2
    target 684
  ]
  edge [
    source 2
    target 685
  ]
  edge [
    source 2
    target 686
  ]
  edge [
    source 2
    target 687
  ]
  edge [
    source 2
    target 688
  ]
  edge [
    source 2
    target 689
  ]
  edge [
    source 2
    target 690
  ]
  edge [
    source 2
    target 691
  ]
  edge [
    source 2
    target 692
  ]
  edge [
    source 2
    target 693
  ]
  edge [
    source 2
    target 694
  ]
  edge [
    source 2
    target 695
  ]
  edge [
    source 2
    target 696
  ]
  edge [
    source 2
    target 697
  ]
  edge [
    source 2
    target 698
  ]
  edge [
    source 2
    target 699
  ]
  edge [
    source 2
    target 700
  ]
  edge [
    source 2
    target 701
  ]
  edge [
    source 2
    target 702
  ]
  edge [
    source 2
    target 703
  ]
  edge [
    source 2
    target 704
  ]
  edge [
    source 2
    target 705
  ]
  edge [
    source 2
    target 706
  ]
  edge [
    source 2
    target 707
  ]
  edge [
    source 2
    target 708
  ]
  edge [
    source 2
    target 709
  ]
  edge [
    source 2
    target 710
  ]
  edge [
    source 2
    target 711
  ]
  edge [
    source 2
    target 712
  ]
  edge [
    source 2
    target 713
  ]
  edge [
    source 2
    target 714
  ]
  edge [
    source 2
    target 715
  ]
  edge [
    source 2
    target 716
  ]
  edge [
    source 2
    target 717
  ]
  edge [
    source 2
    target 718
  ]
  edge [
    source 2
    target 719
  ]
  edge [
    source 2
    target 720
  ]
  edge [
    source 2
    target 721
  ]
  edge [
    source 2
    target 722
  ]
  edge [
    source 2
    target 723
  ]
  edge [
    source 2
    target 724
  ]
  edge [
    source 2
    target 725
  ]
  edge [
    source 2
    target 726
  ]
  edge [
    source 2
    target 727
  ]
  edge [
    source 2
    target 728
  ]
  edge [
    source 2
    target 729
  ]
  edge [
    source 2
    target 730
  ]
  edge [
    source 2
    target 731
  ]
  edge [
    source 2
    target 732
  ]
  edge [
    source 2
    target 733
  ]
  edge [
    source 2
    target 734
  ]
  edge [
    source 2
    target 735
  ]
  edge [
    source 2
    target 736
  ]
  edge [
    source 2
    target 737
  ]
  edge [
    source 2
    target 738
  ]
  edge [
    source 2
    target 739
  ]
  edge [
    source 2
    target 740
  ]
  edge [
    source 2
    target 741
  ]
  edge [
    source 2
    target 742
  ]
  edge [
    source 2
    target 743
  ]
  edge [
    source 2
    target 744
  ]
  edge [
    source 2
    target 745
  ]
  edge [
    source 2
    target 746
  ]
  edge [
    source 2
    target 747
  ]
  edge [
    source 2
    target 748
  ]
  edge [
    source 2
    target 749
  ]
  edge [
    source 2
    target 750
  ]
  edge [
    source 2
    target 751
  ]
  edge [
    source 2
    target 752
  ]
  edge [
    source 2
    target 753
  ]
  edge [
    source 2
    target 754
  ]
  edge [
    source 2
    target 755
  ]
  edge [
    source 2
    target 756
  ]
  edge [
    source 2
    target 757
  ]
  edge [
    source 2
    target 758
  ]
  edge [
    source 2
    target 759
  ]
  edge [
    source 2
    target 760
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 761
  ]
  edge [
    source 2
    target 762
  ]
  edge [
    source 2
    target 763
  ]
  edge [
    source 2
    target 764
  ]
  edge [
    source 2
    target 765
  ]
  edge [
    source 2
    target 766
  ]
  edge [
    source 2
    target 767
  ]
  edge [
    source 2
    target 768
  ]
  edge [
    source 2
    target 769
  ]
  edge [
    source 2
    target 770
  ]
  edge [
    source 2
    target 771
  ]
  edge [
    source 2
    target 772
  ]
  edge [
    source 2
    target 773
  ]
  edge [
    source 2
    target 774
  ]
  edge [
    source 2
    target 775
  ]
  edge [
    source 2
    target 776
  ]
  edge [
    source 2
    target 777
  ]
  edge [
    source 2
    target 778
  ]
  edge [
    source 2
    target 779
  ]
  edge [
    source 2
    target 780
  ]
  edge [
    source 2
    target 781
  ]
  edge [
    source 2
    target 782
  ]
  edge [
    source 2
    target 783
  ]
  edge [
    source 2
    target 784
  ]
  edge [
    source 2
    target 785
  ]
  edge [
    source 2
    target 786
  ]
  edge [
    source 2
    target 787
  ]
  edge [
    source 2
    target 788
  ]
  edge [
    source 2
    target 789
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 790
  ]
  edge [
    source 3
    target 791
  ]
  edge [
    source 3
    target 792
  ]
  edge [
    source 3
    target 793
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 794
  ]
  edge [
    source 3
    target 795
  ]
  edge [
    source 3
    target 796
  ]
  edge [
    source 3
    target 797
  ]
  edge [
    source 3
    target 798
  ]
  edge [
    source 3
    target 799
  ]
  edge [
    source 3
    target 800
  ]
  edge [
    source 3
    target 801
  ]
  edge [
    source 3
    target 802
  ]
  edge [
    source 3
    target 803
  ]
  edge [
    source 3
    target 804
  ]
  edge [
    source 3
    target 805
  ]
  edge [
    source 3
    target 806
  ]
  edge [
    source 3
    target 807
  ]
  edge [
    source 3
    target 808
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 809
  ]
  edge [
    source 3
    target 810
  ]
  edge [
    source 3
    target 811
  ]
  edge [
    source 3
    target 812
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 813
  ]
  edge [
    source 3
    target 814
  ]
  edge [
    source 3
    target 815
  ]
  edge [
    source 3
    target 816
  ]
  edge [
    source 3
    target 817
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 818
  ]
  edge [
    source 6
    target 819
  ]
  edge [
    source 6
    target 820
  ]
  edge [
    source 6
    target 821
  ]
  edge [
    source 6
    target 822
  ]
  edge [
    source 6
    target 823
  ]
  edge [
    source 6
    target 824
  ]
  edge [
    source 6
    target 825
  ]
  edge [
    source 6
    target 826
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 827
  ]
  edge [
    source 6
    target 828
  ]
  edge [
    source 6
    target 829
  ]
  edge [
    source 6
    target 830
  ]
  edge [
    source 6
    target 831
  ]
  edge [
    source 6
    target 832
  ]
  edge [
    source 6
    target 833
  ]
  edge [
    source 6
    target 834
  ]
  edge [
    source 6
    target 835
  ]
  edge [
    source 6
    target 836
  ]
  edge [
    source 6
    target 837
  ]
  edge [
    source 6
    target 838
  ]
  edge [
    source 6
    target 839
  ]
  edge [
    source 6
    target 840
  ]
  edge [
    source 6
    target 841
  ]
  edge [
    source 6
    target 842
  ]
  edge [
    source 6
    target 843
  ]
  edge [
    source 6
    target 844
  ]
  edge [
    source 6
    target 845
  ]
  edge [
    source 6
    target 846
  ]
  edge [
    source 6
    target 847
  ]
  edge [
    source 6
    target 848
  ]
  edge [
    source 6
    target 849
  ]
  edge [
    source 6
    target 850
  ]
  edge [
    source 6
    target 851
  ]
  edge [
    source 6
    target 852
  ]
  edge [
    source 6
    target 853
  ]
  edge [
    source 6
    target 854
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 855
  ]
  edge [
    source 6
    target 856
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 857
  ]
  edge [
    source 6
    target 858
  ]
  edge [
    source 6
    target 859
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 860
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 861
  ]
  edge [
    source 6
    target 862
  ]
  edge [
    source 6
    target 863
  ]
  edge [
    source 6
    target 864
  ]
  edge [
    source 6
    target 865
  ]
  edge [
    source 6
    target 866
  ]
  edge [
    source 6
    target 867
  ]
  edge [
    source 6
    target 868
  ]
  edge [
    source 6
    target 869
  ]
  edge [
    source 6
    target 870
  ]
  edge [
    source 6
    target 871
  ]
  edge [
    source 6
    target 872
  ]
  edge [
    source 6
    target 873
  ]
  edge [
    source 6
    target 874
  ]
  edge [
    source 6
    target 875
  ]
  edge [
    source 6
    target 876
  ]
  edge [
    source 6
    target 877
  ]
  edge [
    source 6
    target 878
  ]
  edge [
    source 6
    target 879
  ]
  edge [
    source 6
    target 880
  ]
  edge [
    source 6
    target 881
  ]
  edge [
    source 6
    target 882
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 883
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 884
  ]
  edge [
    source 6
    target 885
  ]
  edge [
    source 6
    target 886
  ]
  edge [
    source 6
    target 887
  ]
  edge [
    source 6
    target 888
  ]
  edge [
    source 6
    target 889
  ]
  edge [
    source 6
    target 890
  ]
  edge [
    source 6
    target 891
  ]
  edge [
    source 6
    target 892
  ]
  edge [
    source 6
    target 893
  ]
  edge [
    source 6
    target 894
  ]
  edge [
    source 6
    target 895
  ]
  edge [
    source 6
    target 896
  ]
  edge [
    source 6
    target 897
  ]
  edge [
    source 6
    target 898
  ]
  edge [
    source 6
    target 899
  ]
  edge [
    source 6
    target 900
  ]
  edge [
    source 6
    target 901
  ]
  edge [
    source 6
    target 902
  ]
  edge [
    source 6
    target 903
  ]
  edge [
    source 6
    target 904
  ]
  edge [
    source 6
    target 905
  ]
  edge [
    source 6
    target 906
  ]
  edge [
    source 6
    target 907
  ]
  edge [
    source 6
    target 908
  ]
  edge [
    source 6
    target 909
  ]
  edge [
    source 6
    target 910
  ]
  edge [
    source 6
    target 911
  ]
  edge [
    source 6
    target 912
  ]
  edge [
    source 6
    target 913
  ]
  edge [
    source 6
    target 914
  ]
  edge [
    source 6
    target 915
  ]
  edge [
    source 6
    target 916
  ]
  edge [
    source 6
    target 917
  ]
  edge [
    source 6
    target 918
  ]
  edge [
    source 6
    target 919
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 920
  ]
  edge [
    source 6
    target 921
  ]
  edge [
    source 6
    target 922
  ]
  edge [
    source 6
    target 923
  ]
  edge [
    source 6
    target 924
  ]
  edge [
    source 6
    target 925
  ]
  edge [
    source 6
    target 926
  ]
  edge [
    source 6
    target 927
  ]
  edge [
    source 6
    target 928
  ]
  edge [
    source 6
    target 929
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 930
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 931
  ]
  edge [
    source 6
    target 932
  ]
  edge [
    source 6
    target 933
  ]
  edge [
    source 6
    target 934
  ]
  edge [
    source 6
    target 935
  ]
  edge [
    source 6
    target 936
  ]
  edge [
    source 6
    target 937
  ]
  edge [
    source 6
    target 938
  ]
  edge [
    source 6
    target 939
  ]
  edge [
    source 6
    target 940
  ]
  edge [
    source 6
    target 941
  ]
  edge [
    source 6
    target 942
  ]
  edge [
    source 6
    target 943
  ]
  edge [
    source 6
    target 944
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 945
  ]
  edge [
    source 6
    target 946
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 947
  ]
  edge [
    source 6
    target 948
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 949
  ]
  edge [
    source 6
    target 950
  ]
  edge [
    source 6
    target 951
  ]
  edge [
    source 6
    target 952
  ]
  edge [
    source 6
    target 953
  ]
  edge [
    source 6
    target 954
  ]
  edge [
    source 6
    target 955
  ]
  edge [
    source 6
    target 956
  ]
  edge [
    source 6
    target 957
  ]
  edge [
    source 6
    target 958
  ]
  edge [
    source 6
    target 959
  ]
  edge [
    source 6
    target 960
  ]
  edge [
    source 6
    target 961
  ]
  edge [
    source 6
    target 962
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 963
  ]
  edge [
    source 6
    target 964
  ]
  edge [
    source 6
    target 965
  ]
  edge [
    source 6
    target 966
  ]
  edge [
    source 6
    target 967
  ]
  edge [
    source 6
    target 968
  ]
  edge [
    source 6
    target 969
  ]
  edge [
    source 6
    target 970
  ]
  edge [
    source 6
    target 971
  ]
  edge [
    source 6
    target 972
  ]
  edge [
    source 6
    target 973
  ]
  edge [
    source 6
    target 974
  ]
  edge [
    source 6
    target 975
  ]
  edge [
    source 6
    target 976
  ]
  edge [
    source 6
    target 977
  ]
  edge [
    source 6
    target 978
  ]
  edge [
    source 6
    target 979
  ]
  edge [
    source 6
    target 980
  ]
  edge [
    source 6
    target 981
  ]
  edge [
    source 6
    target 982
  ]
  edge [
    source 6
    target 983
  ]
  edge [
    source 6
    target 984
  ]
  edge [
    source 6
    target 985
  ]
  edge [
    source 6
    target 986
  ]
  edge [
    source 6
    target 987
  ]
  edge [
    source 6
    target 988
  ]
  edge [
    source 6
    target 989
  ]
  edge [
    source 6
    target 990
  ]
  edge [
    source 6
    target 991
  ]
  edge [
    source 6
    target 992
  ]
  edge [
    source 6
    target 993
  ]
  edge [
    source 6
    target 994
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 995
  ]
  edge [
    source 6
    target 996
  ]
  edge [
    source 6
    target 997
  ]
  edge [
    source 6
    target 998
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 999
  ]
  edge [
    source 6
    target 1000
  ]
  edge [
    source 6
    target 1001
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 1002
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 1003
  ]
  edge [
    source 6
    target 1004
  ]
  edge [
    source 6
    target 1005
  ]
  edge [
    source 6
    target 1006
  ]
  edge [
    source 6
    target 1007
  ]
  edge [
    source 6
    target 1008
  ]
  edge [
    source 6
    target 1009
  ]
  edge [
    source 6
    target 1010
  ]
  edge [
    source 6
    target 1011
  ]
  edge [
    source 6
    target 1012
  ]
  edge [
    source 6
    target 1013
  ]
  edge [
    source 6
    target 1014
  ]
  edge [
    source 6
    target 1015
  ]
  edge [
    source 6
    target 1016
  ]
  edge [
    source 6
    target 1017
  ]
  edge [
    source 6
    target 1018
  ]
  edge [
    source 6
    target 1019
  ]
  edge [
    source 6
    target 1020
  ]
  edge [
    source 6
    target 1021
  ]
  edge [
    source 6
    target 1022
  ]
  edge [
    source 6
    target 1023
  ]
  edge [
    source 6
    target 1024
  ]
  edge [
    source 6
    target 1025
  ]
  edge [
    source 6
    target 1026
  ]
  edge [
    source 6
    target 1027
  ]
  edge [
    source 6
    target 1028
  ]
  edge [
    source 6
    target 1029
  ]
  edge [
    source 6
    target 1030
  ]
  edge [
    source 6
    target 1031
  ]
  edge [
    source 6
    target 1032
  ]
  edge [
    source 6
    target 1033
  ]
  edge [
    source 6
    target 1034
  ]
  edge [
    source 6
    target 1061
  ]
  edge [
    source 7
    target 1035
  ]
  edge [
    source 7
    target 1036
  ]
  edge [
    source 7
    target 1037
  ]
  edge [
    source 7
    target 1038
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 1039
  ]
  edge [
    source 7
    target 1040
  ]
  edge [
    source 7
    target 1041
  ]
  edge [
    source 7
    target 1042
  ]
  edge [
    source 7
    target 1043
  ]
  edge [
    source 7
    target 1044
  ]
  edge [
    source 7
    target 1045
  ]
  edge [
    source 7
    target 1046
  ]
  edge [
    source 7
    target 1047
  ]
  edge [
    source 7
    target 1048
  ]
  edge [
    source 7
    target 1049
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 1050
  ]
  edge [
    source 7
    target 1051
  ]
  edge [
    source 7
    target 1052
  ]
  edge [
    source 7
    target 1053
  ]
  edge [
    source 7
    target 1054
  ]
  edge [
    source 7
    target 1055
  ]
  edge [
    source 7
    target 1056
  ]
  edge [
    source 7
    target 1057
  ]
  edge [
    source 7
    target 1058
  ]
  edge [
    source 7
    target 943
  ]
  edge [
    source 7
    target 1061
  ]
  edge [
    source 54
    target 1064
  ]
  edge [
    source 1059
    target 1060
  ]
  edge [
    source 1062
    target 1063
  ]
  edge [
    source 1065
    target 1066
  ]
  edge [
    source 1067
    target 1068
  ]
  edge [
    source 1069
    target 1070
  ]
  edge [
    source 1069
    target 1071
  ]
  edge [
    source 1069
    target 1072
  ]
  edge [
    source 1070
    target 1071
  ]
  edge [
    source 1070
    target 1072
  ]
  edge [
    source 1071
    target 1072
  ]
  edge [
    source 1073
    target 1074
  ]
  edge [
    source 1074
    target 1077
  ]
  edge [
    source 1075
    target 1076
  ]
  edge [
    source 1076
    target 1077
  ]
  edge [
    source 1076
    target 1078
  ]
  edge [
    source 1077
    target 1078
  ]
  edge [
    source 1079
    target 1080
  ]
  edge [
    source 1080
    target 1081
  ]
  edge [
    source 1082
    target 1083
  ]
  edge [
    source 1084
    target 1085
  ]
  edge [
    source 1086
    target 1087
  ]
  edge [
    source 1086
    target 1088
  ]
  edge [
    source 1086
    target 1089
  ]
  edge [
    source 1088
    target 1089
  ]
  edge [
    source 1090
    target 1091
  ]
  edge [
    source 1090
    target 1092
  ]
  edge [
    source 1091
    target 1092
  ]
]
