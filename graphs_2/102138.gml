graph [
  node [
    id 0
    label "niezale&#380;nie"
    origin "text"
  ]
  node [
    id 1
    label "postaw"
    origin "text"
  ]
  node [
    id 2
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "respondent"
    origin "text"
  ]
  node [
    id 4
    label "popiera&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wprowadzenie"
    origin "text"
  ]
  node [
    id 6
    label "zakaz"
    origin "text"
  ]
  node [
    id 7
    label "palenie"
    origin "text"
  ]
  node [
    id 8
    label "miejsce"
    origin "text"
  ]
  node [
    id 9
    label "publiczny"
    origin "text"
  ]
  node [
    id 10
    label "gdy"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "zdecydowanie"
    origin "text"
  ]
  node [
    id 13
    label "przeciwny"
    origin "text"
  ]
  node [
    id 14
    label "ogranicza&#263;"
    origin "text"
  ]
  node [
    id 15
    label "wolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "palacz"
    origin "text"
  ]
  node [
    id 17
    label "ciekawy"
    origin "text"
  ]
  node [
    id 18
    label "ostatni"
    origin "text"
  ]
  node [
    id 19
    label "dwa"
    origin "text"
  ]
  node [
    id 20
    label "lato"
    origin "text"
  ]
  node [
    id 21
    label "grupa"
    origin "text"
  ]
  node [
    id 22
    label "nieznacznie"
    origin "text"
  ]
  node [
    id 23
    label "si&#281;"
    origin "text"
  ]
  node [
    id 24
    label "powi&#281;kszy&#263;"
    origin "text"
  ]
  node [
    id 25
    label "uby&#263;"
    origin "text"
  ]
  node [
    id 26
    label "zwolennik"
    origin "text"
  ]
  node [
    id 27
    label "polak"
    origin "text"
  ]
  node [
    id 28
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 29
    label "papieros"
    origin "text"
  ]
  node [
    id 30
    label "niezale&#380;ny"
  ]
  node [
    id 31
    label "usamodzielnianie_si&#281;"
  ]
  node [
    id 32
    label "usamodzielnienie"
  ]
  node [
    id 33
    label "usamodzielnianie"
  ]
  node [
    id 34
    label "jednostka"
  ]
  node [
    id 35
    label "przyswoi&#263;"
  ]
  node [
    id 36
    label "ludzko&#347;&#263;"
  ]
  node [
    id 37
    label "one"
  ]
  node [
    id 38
    label "poj&#281;cie"
  ]
  node [
    id 39
    label "ewoluowanie"
  ]
  node [
    id 40
    label "supremum"
  ]
  node [
    id 41
    label "skala"
  ]
  node [
    id 42
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 43
    label "przyswajanie"
  ]
  node [
    id 44
    label "wyewoluowanie"
  ]
  node [
    id 45
    label "reakcja"
  ]
  node [
    id 46
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 47
    label "przeliczy&#263;"
  ]
  node [
    id 48
    label "wyewoluowa&#263;"
  ]
  node [
    id 49
    label "ewoluowa&#263;"
  ]
  node [
    id 50
    label "matematyka"
  ]
  node [
    id 51
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 52
    label "rzut"
  ]
  node [
    id 53
    label "liczba_naturalna"
  ]
  node [
    id 54
    label "czynnik_biotyczny"
  ]
  node [
    id 55
    label "g&#322;owa"
  ]
  node [
    id 56
    label "figura"
  ]
  node [
    id 57
    label "individual"
  ]
  node [
    id 58
    label "portrecista"
  ]
  node [
    id 59
    label "obiekt"
  ]
  node [
    id 60
    label "przyswaja&#263;"
  ]
  node [
    id 61
    label "przyswojenie"
  ]
  node [
    id 62
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 63
    label "profanum"
  ]
  node [
    id 64
    label "mikrokosmos"
  ]
  node [
    id 65
    label "starzenie_si&#281;"
  ]
  node [
    id 66
    label "duch"
  ]
  node [
    id 67
    label "przeliczanie"
  ]
  node [
    id 68
    label "osoba"
  ]
  node [
    id 69
    label "oddzia&#322;ywanie"
  ]
  node [
    id 70
    label "antropochoria"
  ]
  node [
    id 71
    label "funkcja"
  ]
  node [
    id 72
    label "homo_sapiens"
  ]
  node [
    id 73
    label "przelicza&#263;"
  ]
  node [
    id 74
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 75
    label "infimum"
  ]
  node [
    id 76
    label "przeliczenie"
  ]
  node [
    id 77
    label "majority"
  ]
  node [
    id 78
    label "Rzym_Zachodni"
  ]
  node [
    id 79
    label "whole"
  ]
  node [
    id 80
    label "ilo&#347;&#263;"
  ]
  node [
    id 81
    label "element"
  ]
  node [
    id 82
    label "Rzym_Wschodni"
  ]
  node [
    id 83
    label "urz&#261;dzenie"
  ]
  node [
    id 84
    label "odpowied&#378;"
  ]
  node [
    id 85
    label "badany"
  ]
  node [
    id 86
    label "pacjent"
  ]
  node [
    id 87
    label "analizowa&#263;"
  ]
  node [
    id 88
    label "uczestnik"
  ]
  node [
    id 89
    label "react"
  ]
  node [
    id 90
    label "replica"
  ]
  node [
    id 91
    label "rozmowa"
  ]
  node [
    id 92
    label "wyj&#347;cie"
  ]
  node [
    id 93
    label "dokument"
  ]
  node [
    id 94
    label "pomaga&#263;"
  ]
  node [
    id 95
    label "unbosom"
  ]
  node [
    id 96
    label "uzasadnia&#263;"
  ]
  node [
    id 97
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 98
    label "explain"
  ]
  node [
    id 99
    label "mie&#263;_miejsce"
  ]
  node [
    id 100
    label "robi&#263;"
  ]
  node [
    id 101
    label "aid"
  ]
  node [
    id 102
    label "u&#322;atwia&#263;"
  ]
  node [
    id 103
    label "concur"
  ]
  node [
    id 104
    label "sprzyja&#263;"
  ]
  node [
    id 105
    label "skutkowa&#263;"
  ]
  node [
    id 106
    label "digest"
  ]
  node [
    id 107
    label "Warszawa"
  ]
  node [
    id 108
    label "powodowa&#263;"
  ]
  node [
    id 109
    label "back"
  ]
  node [
    id 110
    label "rynek"
  ]
  node [
    id 111
    label "nuklearyzacja"
  ]
  node [
    id 112
    label "deduction"
  ]
  node [
    id 113
    label "entrance"
  ]
  node [
    id 114
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 115
    label "wst&#281;p"
  ]
  node [
    id 116
    label "spowodowanie"
  ]
  node [
    id 117
    label "wej&#347;cie"
  ]
  node [
    id 118
    label "issue"
  ]
  node [
    id 119
    label "doprowadzenie"
  ]
  node [
    id 120
    label "umieszczenie"
  ]
  node [
    id 121
    label "umo&#380;liwienie"
  ]
  node [
    id 122
    label "wpisanie"
  ]
  node [
    id 123
    label "podstawy"
  ]
  node [
    id 124
    label "czynno&#347;&#263;"
  ]
  node [
    id 125
    label "evocation"
  ]
  node [
    id 126
    label "zapoznanie"
  ]
  node [
    id 127
    label "w&#322;&#261;czenie"
  ]
  node [
    id 128
    label "zacz&#281;cie"
  ]
  node [
    id 129
    label "przewietrzenie"
  ]
  node [
    id 130
    label "zrobienie"
  ]
  node [
    id 131
    label "mo&#380;liwy"
  ]
  node [
    id 132
    label "upowa&#380;nienie"
  ]
  node [
    id 133
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 134
    label "pos&#322;uchanie"
  ]
  node [
    id 135
    label "obejrzenie"
  ]
  node [
    id 136
    label "involvement"
  ]
  node [
    id 137
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 138
    label "za&#347;wiecenie"
  ]
  node [
    id 139
    label "nastawienie"
  ]
  node [
    id 140
    label "uruchomienie"
  ]
  node [
    id 141
    label "funkcjonowanie"
  ]
  node [
    id 142
    label "wy&#322;&#261;czenie"
  ]
  node [
    id 143
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 144
    label "narobienie"
  ]
  node [
    id 145
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 146
    label "creation"
  ]
  node [
    id 147
    label "porobienie"
  ]
  node [
    id 148
    label "wnikni&#281;cie"
  ]
  node [
    id 149
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 150
    label "spotkanie"
  ]
  node [
    id 151
    label "poznanie"
  ]
  node [
    id 152
    label "pojawienie_si&#281;"
  ]
  node [
    id 153
    label "zdarzenie_si&#281;"
  ]
  node [
    id 154
    label "przenikni&#281;cie"
  ]
  node [
    id 155
    label "wpuszczenie"
  ]
  node [
    id 156
    label "zaatakowanie"
  ]
  node [
    id 157
    label "trespass"
  ]
  node [
    id 158
    label "dost&#281;p"
  ]
  node [
    id 159
    label "doj&#347;cie"
  ]
  node [
    id 160
    label "przekroczenie"
  ]
  node [
    id 161
    label "otw&#243;r"
  ]
  node [
    id 162
    label "wzi&#281;cie"
  ]
  node [
    id 163
    label "vent"
  ]
  node [
    id 164
    label "stimulation"
  ]
  node [
    id 165
    label "dostanie_si&#281;"
  ]
  node [
    id 166
    label "pocz&#261;tek"
  ]
  node [
    id 167
    label "approach"
  ]
  node [
    id 168
    label "release"
  ]
  node [
    id 169
    label "wnij&#347;cie"
  ]
  node [
    id 170
    label "bramka"
  ]
  node [
    id 171
    label "wzniesienie_si&#281;"
  ]
  node [
    id 172
    label "podw&#243;rze"
  ]
  node [
    id 173
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 174
    label "dom"
  ]
  node [
    id 175
    label "wch&#243;d"
  ]
  node [
    id 176
    label "nast&#261;pienie"
  ]
  node [
    id 177
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 178
    label "cz&#322;onek"
  ]
  node [
    id 179
    label "stanie_si&#281;"
  ]
  node [
    id 180
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 181
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 182
    label "campaign"
  ]
  node [
    id 183
    label "causing"
  ]
  node [
    id 184
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 185
    label "przeszkoda"
  ]
  node [
    id 186
    label "perturbation"
  ]
  node [
    id 187
    label "aberration"
  ]
  node [
    id 188
    label "sygna&#322;"
  ]
  node [
    id 189
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 190
    label "hindrance"
  ]
  node [
    id 191
    label "disorder"
  ]
  node [
    id 192
    label "zjawisko"
  ]
  node [
    id 193
    label "naruszenie"
  ]
  node [
    id 194
    label "sytuacja"
  ]
  node [
    id 195
    label "discourtesy"
  ]
  node [
    id 196
    label "odj&#281;cie"
  ]
  node [
    id 197
    label "post&#261;pienie"
  ]
  node [
    id 198
    label "opening"
  ]
  node [
    id 199
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 200
    label "wyra&#380;enie"
  ]
  node [
    id 201
    label "inscription"
  ]
  node [
    id 202
    label "wype&#322;nienie"
  ]
  node [
    id 203
    label "napisanie"
  ]
  node [
    id 204
    label "record"
  ]
  node [
    id 205
    label "wiedza"
  ]
  node [
    id 206
    label "detail"
  ]
  node [
    id 207
    label "activity"
  ]
  node [
    id 208
    label "bezproblemowy"
  ]
  node [
    id 209
    label "wydarzenie"
  ]
  node [
    id 210
    label "zapowied&#378;"
  ]
  node [
    id 211
    label "tekst"
  ]
  node [
    id 212
    label "utw&#243;r"
  ]
  node [
    id 213
    label "g&#322;oska"
  ]
  node [
    id 214
    label "wymowa"
  ]
  node [
    id 215
    label "spe&#322;nienie"
  ]
  node [
    id 216
    label "lead"
  ]
  node [
    id 217
    label "wzbudzenie"
  ]
  node [
    id 218
    label "pos&#322;anie"
  ]
  node [
    id 219
    label "znalezienie_si&#281;"
  ]
  node [
    id 220
    label "introduction"
  ]
  node [
    id 221
    label "sp&#281;dzenie"
  ]
  node [
    id 222
    label "zainstalowanie"
  ]
  node [
    id 223
    label "poumieszczanie"
  ]
  node [
    id 224
    label "ustalenie"
  ]
  node [
    id 225
    label "uplasowanie"
  ]
  node [
    id 226
    label "ulokowanie_si&#281;"
  ]
  node [
    id 227
    label "prze&#322;adowanie"
  ]
  node [
    id 228
    label "layout"
  ]
  node [
    id 229
    label "pomieszczenie"
  ]
  node [
    id 230
    label "siedzenie"
  ]
  node [
    id 231
    label "zakrycie"
  ]
  node [
    id 232
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 233
    label "representation"
  ]
  node [
    id 234
    label "zawarcie"
  ]
  node [
    id 235
    label "znajomy"
  ]
  node [
    id 236
    label "obznajomienie"
  ]
  node [
    id 237
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 238
    label "gathering"
  ]
  node [
    id 239
    label "poinformowanie"
  ]
  node [
    id 240
    label "knowing"
  ]
  node [
    id 241
    label "refresher_course"
  ]
  node [
    id 242
    label "powietrze"
  ]
  node [
    id 243
    label "oczyszczenie"
  ]
  node [
    id 244
    label "wymienienie"
  ]
  node [
    id 245
    label "vaporization"
  ]
  node [
    id 246
    label "potraktowanie"
  ]
  node [
    id 247
    label "przewietrzenie_si&#281;"
  ]
  node [
    id 248
    label "ventilation"
  ]
  node [
    id 249
    label "rozpowszechnianie"
  ]
  node [
    id 250
    label "proces"
  ]
  node [
    id 251
    label "stoisko"
  ]
  node [
    id 252
    label "rynek_podstawowy"
  ]
  node [
    id 253
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 254
    label "konsument"
  ]
  node [
    id 255
    label "obiekt_handlowy"
  ]
  node [
    id 256
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 257
    label "wytw&#243;rca"
  ]
  node [
    id 258
    label "rynek_wt&#243;rny"
  ]
  node [
    id 259
    label "wprowadzanie"
  ]
  node [
    id 260
    label "wprowadza&#263;"
  ]
  node [
    id 261
    label "kram"
  ]
  node [
    id 262
    label "plac"
  ]
  node [
    id 263
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 264
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 265
    label "emitowa&#263;"
  ]
  node [
    id 266
    label "wprowadzi&#263;"
  ]
  node [
    id 267
    label "emitowanie"
  ]
  node [
    id 268
    label "gospodarka"
  ]
  node [
    id 269
    label "biznes"
  ]
  node [
    id 270
    label "segment_rynku"
  ]
  node [
    id 271
    label "targowica"
  ]
  node [
    id 272
    label "polecenie"
  ]
  node [
    id 273
    label "rozporz&#261;dzenie"
  ]
  node [
    id 274
    label "ukaz"
  ]
  node [
    id 275
    label "pognanie"
  ]
  node [
    id 276
    label "rekomendacja"
  ]
  node [
    id 277
    label "wypowied&#378;"
  ]
  node [
    id 278
    label "pobiegni&#281;cie"
  ]
  node [
    id 279
    label "education"
  ]
  node [
    id 280
    label "doradzenie"
  ]
  node [
    id 281
    label "statement"
  ]
  node [
    id 282
    label "recommendation"
  ]
  node [
    id 283
    label "zadanie"
  ]
  node [
    id 284
    label "zaordynowanie"
  ]
  node [
    id 285
    label "powierzenie"
  ]
  node [
    id 286
    label "przesadzenie"
  ]
  node [
    id 287
    label "consign"
  ]
  node [
    id 288
    label "arrangement"
  ]
  node [
    id 289
    label "zarz&#261;dzenie"
  ]
  node [
    id 290
    label "commission"
  ]
  node [
    id 291
    label "ordonans"
  ]
  node [
    id 292
    label "akt"
  ]
  node [
    id 293
    label "rule"
  ]
  node [
    id 294
    label "dopalenie"
  ]
  node [
    id 295
    label "powodowanie"
  ]
  node [
    id 296
    label "burning"
  ]
  node [
    id 297
    label "na&#322;&#243;g"
  ]
  node [
    id 298
    label "emergency"
  ]
  node [
    id 299
    label "burn"
  ]
  node [
    id 300
    label "dokuczanie"
  ]
  node [
    id 301
    label "cygaro"
  ]
  node [
    id 302
    label "napalenie"
  ]
  node [
    id 303
    label "robienie"
  ]
  node [
    id 304
    label "podpalanie"
  ]
  node [
    id 305
    label "rozpalanie"
  ]
  node [
    id 306
    label "pykni&#281;cie"
  ]
  node [
    id 307
    label "zu&#380;ywanie"
  ]
  node [
    id 308
    label "wypalanie"
  ]
  node [
    id 309
    label "fajka"
  ]
  node [
    id 310
    label "podtrzymywanie"
  ]
  node [
    id 311
    label "strzelanie"
  ]
  node [
    id 312
    label "dra&#380;nienie"
  ]
  node [
    id 313
    label "kadzenie"
  ]
  node [
    id 314
    label "wypalenie"
  ]
  node [
    id 315
    label "przygotowywanie"
  ]
  node [
    id 316
    label "ogie&#324;"
  ]
  node [
    id 317
    label "dowcip"
  ]
  node [
    id 318
    label "popalenie"
  ]
  node [
    id 319
    label "niszczenie"
  ]
  node [
    id 320
    label "grzanie"
  ]
  node [
    id 321
    label "paliwo"
  ]
  node [
    id 322
    label "bolenie"
  ]
  node [
    id 323
    label "palenie_si&#281;"
  ]
  node [
    id 324
    label "incineration"
  ]
  node [
    id 325
    label "psucie"
  ]
  node [
    id 326
    label "jaranie"
  ]
  node [
    id 327
    label "poniszczenie"
  ]
  node [
    id 328
    label "zadymienie"
  ]
  node [
    id 329
    label "zagrzanie"
  ]
  node [
    id 330
    label "zaliczanie"
  ]
  node [
    id 331
    label "zapalenie"
  ]
  node [
    id 332
    label "wydanie"
  ]
  node [
    id 333
    label "wydobycie"
  ]
  node [
    id 334
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 335
    label "sparzenie"
  ]
  node [
    id 336
    label "spalenie"
  ]
  node [
    id 337
    label "powt&#243;rzenie"
  ]
  node [
    id 338
    label "cautery"
  ]
  node [
    id 339
    label "zu&#380;ycie"
  ]
  node [
    id 340
    label "dolecenie"
  ]
  node [
    id 341
    label "zniszczenie"
  ]
  node [
    id 342
    label "wylecenie"
  ]
  node [
    id 343
    label "odbezpieczenie"
  ]
  node [
    id 344
    label "pluni&#281;cie"
  ]
  node [
    id 345
    label "dolatywanie"
  ]
  node [
    id 346
    label "shooting"
  ]
  node [
    id 347
    label "odpalenie"
  ]
  node [
    id 348
    label "postrzelenie"
  ]
  node [
    id 349
    label "fire"
  ]
  node [
    id 350
    label "zestrzeliwanie"
  ]
  node [
    id 351
    label "odstrzeliwanie"
  ]
  node [
    id 352
    label "zestrzelenie"
  ]
  node [
    id 353
    label "wystrzelanie"
  ]
  node [
    id 354
    label "wylatywanie"
  ]
  node [
    id 355
    label "chybianie"
  ]
  node [
    id 356
    label "przestrzeliwanie"
  ]
  node [
    id 357
    label "walczenie"
  ]
  node [
    id 358
    label "ostrzelanie"
  ]
  node [
    id 359
    label "ostrzeliwanie"
  ]
  node [
    id 360
    label "trafianie"
  ]
  node [
    id 361
    label "odpalanie"
  ]
  node [
    id 362
    label "wstrzeliwanie_si&#281;"
  ]
  node [
    id 363
    label "odstrzelenie"
  ]
  node [
    id 364
    label "obrabianie"
  ]
  node [
    id 365
    label "postrzelanie"
  ]
  node [
    id 366
    label "chybienie"
  ]
  node [
    id 367
    label "kropni&#281;cie"
  ]
  node [
    id 368
    label "prze&#322;adowywanie"
  ]
  node [
    id 369
    label "plucie"
  ]
  node [
    id 370
    label "nasycanie"
  ]
  node [
    id 371
    label "blarney"
  ]
  node [
    id 372
    label "pochlebianie"
  ]
  node [
    id 373
    label "fabrication"
  ]
  node [
    id 374
    label "przedmiot"
  ]
  node [
    id 375
    label "bycie"
  ]
  node [
    id 376
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 377
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 378
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 379
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 380
    label "act"
  ]
  node [
    id 381
    label "tentegowanie"
  ]
  node [
    id 382
    label "destruction"
  ]
  node [
    id 383
    label "os&#322;abianie"
  ]
  node [
    id 384
    label "pl&#261;drowanie"
  ]
  node [
    id 385
    label "ravaging"
  ]
  node [
    id 386
    label "gnojenie"
  ]
  node [
    id 387
    label "szkodzenie"
  ]
  node [
    id 388
    label "pustoszenie"
  ]
  node [
    id 389
    label "decay"
  ]
  node [
    id 390
    label "poniewieranie_si&#281;"
  ]
  node [
    id 391
    label "zdrowie"
  ]
  node [
    id 392
    label "devastation"
  ]
  node [
    id 393
    label "stawanie_si&#281;"
  ]
  node [
    id 394
    label "utrzymywanie"
  ]
  node [
    id 395
    label "obstawanie"
  ]
  node [
    id 396
    label "preservation"
  ]
  node [
    id 397
    label "boost"
  ]
  node [
    id 398
    label "continuance"
  ]
  node [
    id 399
    label "pocieszanie"
  ]
  node [
    id 400
    label "kszta&#322;cenie"
  ]
  node [
    id 401
    label "homework"
  ]
  node [
    id 402
    label "cooking"
  ]
  node [
    id 403
    label "sposobienie"
  ]
  node [
    id 404
    label "preparation"
  ]
  node [
    id 405
    label "wykonywanie"
  ]
  node [
    id 406
    label "usposabianie"
  ]
  node [
    id 407
    label "zaplanowanie"
  ]
  node [
    id 408
    label "organizowanie"
  ]
  node [
    id 409
    label "gotowy"
  ]
  node [
    id 410
    label "g&#322;&#243;d_nikotynowy"
  ]
  node [
    id 411
    label "g&#322;&#243;d_narkotyczny"
  ]
  node [
    id 412
    label "dysfunkcja"
  ]
  node [
    id 413
    label "nawyk"
  ]
  node [
    id 414
    label "g&#322;&#243;d_alkoholowy"
  ]
  node [
    id 415
    label "addiction"
  ]
  node [
    id 416
    label "gassing"
  ]
  node [
    id 417
    label "fracture"
  ]
  node [
    id 418
    label "roztapianie"
  ]
  node [
    id 419
    label "podnoszenie"
  ]
  node [
    id 420
    label "odpuszczanie"
  ]
  node [
    id 421
    label "wypra&#380;anie"
  ]
  node [
    id 422
    label "zw&#281;glanie"
  ]
  node [
    id 423
    label "zw&#281;glenie"
  ]
  node [
    id 424
    label "spieczenie"
  ]
  node [
    id 425
    label "bicie"
  ]
  node [
    id 426
    label "picie"
  ]
  node [
    id 427
    label "narkotyzowanie_si&#281;"
  ]
  node [
    id 428
    label "zat&#322;uczenie"
  ]
  node [
    id 429
    label "spiekanie"
  ]
  node [
    id 430
    label "wypra&#380;enie"
  ]
  node [
    id 431
    label "pobudzanie"
  ]
  node [
    id 432
    label "rozgrzewanie_si&#281;"
  ]
  node [
    id 433
    label "odwadnianie"
  ]
  node [
    id 434
    label "ciep&#322;y"
  ]
  node [
    id 435
    label "gnanie"
  ]
  node [
    id 436
    label "wydzielanie"
  ]
  node [
    id 437
    label "heating"
  ]
  node [
    id 438
    label "irritation"
  ]
  node [
    id 439
    label "dojmowanie"
  ]
  node [
    id 440
    label "denerwowanie"
  ]
  node [
    id 441
    label "impairment"
  ]
  node [
    id 442
    label "przekupywanie"
  ]
  node [
    id 443
    label "uszkadzanie"
  ]
  node [
    id 444
    label "pogarszanie"
  ]
  node [
    id 445
    label "damage"
  ]
  node [
    id 446
    label "deformowanie"
  ]
  node [
    id 447
    label "napierdalanie"
  ]
  node [
    id 448
    label "napieprzanie"
  ]
  node [
    id 449
    label "grief"
  ]
  node [
    id 450
    label "namartwienie_si&#281;"
  ]
  node [
    id 451
    label "czucie"
  ]
  node [
    id 452
    label "dzianie_si&#281;"
  ]
  node [
    id 453
    label "rozdra&#380;nianie"
  ]
  node [
    id 454
    label "rozdra&#380;nienie"
  ]
  node [
    id 455
    label "wydawanie"
  ]
  node [
    id 456
    label "depreciation"
  ]
  node [
    id 457
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 458
    label "cause"
  ]
  node [
    id 459
    label "causal_agent"
  ]
  node [
    id 460
    label "uderzanie"
  ]
  node [
    id 461
    label "u&#380;ywka"
  ]
  node [
    id 462
    label "cybuch"
  ]
  node [
    id 463
    label "rurka"
  ]
  node [
    id 464
    label "dzik"
  ]
  node [
    id 465
    label "odpali&#263;"
  ]
  node [
    id 466
    label "wyr&#243;b_tytoniowy"
  ]
  node [
    id 467
    label "tyto&#324;"
  ]
  node [
    id 468
    label "pipe"
  ]
  node [
    id 469
    label "dulec"
  ]
  node [
    id 470
    label "filtr"
  ]
  node [
    id 471
    label "fidybus"
  ]
  node [
    id 472
    label "trafika"
  ]
  node [
    id 473
    label "smoke"
  ]
  node [
    id 474
    label "kie&#322;"
  ]
  node [
    id 475
    label "pali&#263;"
  ]
  node [
    id 476
    label "ustnik"
  ]
  node [
    id 477
    label "szlug"
  ]
  node [
    id 478
    label "znaczek"
  ]
  node [
    id 479
    label "bibu&#322;ka"
  ]
  node [
    id 480
    label "zako&#324;czenie"
  ]
  node [
    id 481
    label "przek&#261;ska"
  ]
  node [
    id 482
    label "humidor"
  ]
  node [
    id 483
    label "przysmak"
  ]
  node [
    id 484
    label "paluch"
  ]
  node [
    id 485
    label "gilotynka"
  ]
  node [
    id 486
    label "pies"
  ]
  node [
    id 487
    label "wk&#322;adka"
  ]
  node [
    id 488
    label "kot"
  ]
  node [
    id 489
    label "spalanie"
  ]
  node [
    id 490
    label "zapalanie"
  ]
  node [
    id 491
    label "przypalanie"
  ]
  node [
    id 492
    label "energia"
  ]
  node [
    id 493
    label "rumieniec"
  ]
  node [
    id 494
    label "nowotw&#243;r_niez&#322;o&#347;liwy"
  ]
  node [
    id 495
    label "sk&#243;ra"
  ]
  node [
    id 496
    label "hell"
  ]
  node [
    id 497
    label "rozpalenie"
  ]
  node [
    id 498
    label "co&#347;"
  ]
  node [
    id 499
    label "iskra"
  ]
  node [
    id 500
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 501
    label "light"
  ]
  node [
    id 502
    label "deszcz"
  ]
  node [
    id 503
    label "akcesorium"
  ]
  node [
    id 504
    label "pali&#263;_si&#281;"
  ]
  node [
    id 505
    label "&#347;wiat&#322;o"
  ]
  node [
    id 506
    label "zarzewie"
  ]
  node [
    id 507
    label "ciep&#322;o"
  ]
  node [
    id 508
    label "znami&#281;"
  ]
  node [
    id 509
    label "war"
  ]
  node [
    id 510
    label "kolor"
  ]
  node [
    id 511
    label "przyp&#322;yw"
  ]
  node [
    id 512
    label "p&#322;omie&#324;"
  ]
  node [
    id 513
    label "&#380;ywio&#322;"
  ]
  node [
    id 514
    label "incandescence"
  ]
  node [
    id 515
    label "atak"
  ]
  node [
    id 516
    label "ardor"
  ]
  node [
    id 517
    label "turn"
  ]
  node [
    id 518
    label "&#380;art"
  ]
  node [
    id 519
    label "koncept"
  ]
  node [
    id 520
    label "raptularz"
  ]
  node [
    id 521
    label "gryps"
  ]
  node [
    id 522
    label "opowiadanie"
  ]
  node [
    id 523
    label "anecdote"
  ]
  node [
    id 524
    label "tankowanie"
  ]
  node [
    id 525
    label "spali&#263;"
  ]
  node [
    id 526
    label "Orlen"
  ]
  node [
    id 527
    label "fuel"
  ]
  node [
    id 528
    label "zgazowa&#263;"
  ]
  node [
    id 529
    label "pompa_wtryskowa"
  ]
  node [
    id 530
    label "antydetonator"
  ]
  node [
    id 531
    label "spala&#263;"
  ]
  node [
    id 532
    label "substancja"
  ]
  node [
    id 533
    label "tankowa&#263;"
  ]
  node [
    id 534
    label "o&#347;wietlanie"
  ]
  node [
    id 535
    label "distraction"
  ]
  node [
    id 536
    label "rozja&#347;nianie_si&#281;"
  ]
  node [
    id 537
    label "roz&#380;arzanie_si&#281;"
  ]
  node [
    id 538
    label "ignition"
  ]
  node [
    id 539
    label "rozp&#322;omienianie_si&#281;"
  ]
  node [
    id 540
    label "wzbudzanie"
  ]
  node [
    id 541
    label "warunek_lokalowy"
  ]
  node [
    id 542
    label "location"
  ]
  node [
    id 543
    label "uwaga"
  ]
  node [
    id 544
    label "przestrze&#324;"
  ]
  node [
    id 545
    label "status"
  ]
  node [
    id 546
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 547
    label "chwila"
  ]
  node [
    id 548
    label "cia&#322;o"
  ]
  node [
    id 549
    label "cecha"
  ]
  node [
    id 550
    label "praca"
  ]
  node [
    id 551
    label "rz&#261;d"
  ]
  node [
    id 552
    label "charakterystyka"
  ]
  node [
    id 553
    label "m&#322;ot"
  ]
  node [
    id 554
    label "znak"
  ]
  node [
    id 555
    label "drzewo"
  ]
  node [
    id 556
    label "pr&#243;ba"
  ]
  node [
    id 557
    label "attribute"
  ]
  node [
    id 558
    label "marka"
  ]
  node [
    id 559
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 560
    label "stan"
  ]
  node [
    id 561
    label "nagana"
  ]
  node [
    id 562
    label "upomnienie"
  ]
  node [
    id 563
    label "dzienniczek"
  ]
  node [
    id 564
    label "wzgl&#261;d"
  ]
  node [
    id 565
    label "gossip"
  ]
  node [
    id 566
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 567
    label "najem"
  ]
  node [
    id 568
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 569
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 570
    label "zak&#322;ad"
  ]
  node [
    id 571
    label "stosunek_pracy"
  ]
  node [
    id 572
    label "benedykty&#324;ski"
  ]
  node [
    id 573
    label "poda&#380;_pracy"
  ]
  node [
    id 574
    label "pracowanie"
  ]
  node [
    id 575
    label "tyrka"
  ]
  node [
    id 576
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 577
    label "wytw&#243;r"
  ]
  node [
    id 578
    label "zaw&#243;d"
  ]
  node [
    id 579
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 580
    label "tynkarski"
  ]
  node [
    id 581
    label "pracowa&#263;"
  ]
  node [
    id 582
    label "zmiana"
  ]
  node [
    id 583
    label "czynnik_produkcji"
  ]
  node [
    id 584
    label "zobowi&#261;zanie"
  ]
  node [
    id 585
    label "kierownictwo"
  ]
  node [
    id 586
    label "siedziba"
  ]
  node [
    id 587
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 588
    label "rozdzielanie"
  ]
  node [
    id 589
    label "bezbrze&#380;e"
  ]
  node [
    id 590
    label "punkt"
  ]
  node [
    id 591
    label "czasoprzestrze&#324;"
  ]
  node [
    id 592
    label "zbi&#243;r"
  ]
  node [
    id 593
    label "niezmierzony"
  ]
  node [
    id 594
    label "przedzielenie"
  ]
  node [
    id 595
    label "nielito&#347;ciwy"
  ]
  node [
    id 596
    label "rozdziela&#263;"
  ]
  node [
    id 597
    label "oktant"
  ]
  node [
    id 598
    label "przedzieli&#263;"
  ]
  node [
    id 599
    label "przestw&#243;r"
  ]
  node [
    id 600
    label "condition"
  ]
  node [
    id 601
    label "awansowa&#263;"
  ]
  node [
    id 602
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 603
    label "znaczenie"
  ]
  node [
    id 604
    label "awans"
  ]
  node [
    id 605
    label "podmiotowo"
  ]
  node [
    id 606
    label "awansowanie"
  ]
  node [
    id 607
    label "time"
  ]
  node [
    id 608
    label "czas"
  ]
  node [
    id 609
    label "rozmiar"
  ]
  node [
    id 610
    label "liczba"
  ]
  node [
    id 611
    label "circumference"
  ]
  node [
    id 612
    label "leksem"
  ]
  node [
    id 613
    label "cyrkumferencja"
  ]
  node [
    id 614
    label "strona"
  ]
  node [
    id 615
    label "ekshumowanie"
  ]
  node [
    id 616
    label "uk&#322;ad"
  ]
  node [
    id 617
    label "jednostka_organizacyjna"
  ]
  node [
    id 618
    label "p&#322;aszczyzna"
  ]
  node [
    id 619
    label "odwadnia&#263;"
  ]
  node [
    id 620
    label "zabalsamowanie"
  ]
  node [
    id 621
    label "zesp&#243;&#322;"
  ]
  node [
    id 622
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 623
    label "odwodni&#263;"
  ]
  node [
    id 624
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 625
    label "staw"
  ]
  node [
    id 626
    label "ow&#322;osienie"
  ]
  node [
    id 627
    label "mi&#281;so"
  ]
  node [
    id 628
    label "zabalsamowa&#263;"
  ]
  node [
    id 629
    label "Izba_Konsyliarska"
  ]
  node [
    id 630
    label "unerwienie"
  ]
  node [
    id 631
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 632
    label "kremacja"
  ]
  node [
    id 633
    label "biorytm"
  ]
  node [
    id 634
    label "sekcja"
  ]
  node [
    id 635
    label "istota_&#380;ywa"
  ]
  node [
    id 636
    label "otworzy&#263;"
  ]
  node [
    id 637
    label "otwiera&#263;"
  ]
  node [
    id 638
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 639
    label "otworzenie"
  ]
  node [
    id 640
    label "materia"
  ]
  node [
    id 641
    label "pochowanie"
  ]
  node [
    id 642
    label "otwieranie"
  ]
  node [
    id 643
    label "szkielet"
  ]
  node [
    id 644
    label "ty&#322;"
  ]
  node [
    id 645
    label "tanatoplastyk"
  ]
  node [
    id 646
    label "Komitet_Region&#243;w"
  ]
  node [
    id 647
    label "odwodnienie"
  ]
  node [
    id 648
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 649
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 650
    label "pochowa&#263;"
  ]
  node [
    id 651
    label "tanatoplastyka"
  ]
  node [
    id 652
    label "balsamowa&#263;"
  ]
  node [
    id 653
    label "nieumar&#322;y"
  ]
  node [
    id 654
    label "temperatura"
  ]
  node [
    id 655
    label "balsamowanie"
  ]
  node [
    id 656
    label "ekshumowa&#263;"
  ]
  node [
    id 657
    label "l&#281;d&#378;wie"
  ]
  node [
    id 658
    label "prz&#243;d"
  ]
  node [
    id 659
    label "pogrzeb"
  ]
  node [
    id 660
    label "&#321;ubianka"
  ]
  node [
    id 661
    label "area"
  ]
  node [
    id 662
    label "Majdan"
  ]
  node [
    id 663
    label "pole_bitwy"
  ]
  node [
    id 664
    label "obszar"
  ]
  node [
    id 665
    label "pierzeja"
  ]
  node [
    id 666
    label "zgromadzenie"
  ]
  node [
    id 667
    label "miasto"
  ]
  node [
    id 668
    label "przybli&#380;enie"
  ]
  node [
    id 669
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 670
    label "kategoria"
  ]
  node [
    id 671
    label "szpaler"
  ]
  node [
    id 672
    label "lon&#380;a"
  ]
  node [
    id 673
    label "uporz&#261;dkowanie"
  ]
  node [
    id 674
    label "instytucja"
  ]
  node [
    id 675
    label "jednostka_systematyczna"
  ]
  node [
    id 676
    label "egzekutywa"
  ]
  node [
    id 677
    label "premier"
  ]
  node [
    id 678
    label "Londyn"
  ]
  node [
    id 679
    label "gabinet_cieni"
  ]
  node [
    id 680
    label "gromada"
  ]
  node [
    id 681
    label "number"
  ]
  node [
    id 682
    label "Konsulat"
  ]
  node [
    id 683
    label "tract"
  ]
  node [
    id 684
    label "klasa"
  ]
  node [
    id 685
    label "w&#322;adza"
  ]
  node [
    id 686
    label "upublicznianie"
  ]
  node [
    id 687
    label "jawny"
  ]
  node [
    id 688
    label "upublicznienie"
  ]
  node [
    id 689
    label "publicznie"
  ]
  node [
    id 690
    label "jawnie"
  ]
  node [
    id 691
    label "udost&#281;pnianie"
  ]
  node [
    id 692
    label "udost&#281;pnienie"
  ]
  node [
    id 693
    label "ujawnienie_si&#281;"
  ]
  node [
    id 694
    label "ujawnianie_si&#281;"
  ]
  node [
    id 695
    label "zdecydowany"
  ]
  node [
    id 696
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 697
    label "ujawnienie"
  ]
  node [
    id 698
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 699
    label "ujawnianie"
  ]
  node [
    id 700
    label "ewidentny"
  ]
  node [
    id 701
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 702
    label "equal"
  ]
  node [
    id 703
    label "trwa&#263;"
  ]
  node [
    id 704
    label "chodzi&#263;"
  ]
  node [
    id 705
    label "si&#281;ga&#263;"
  ]
  node [
    id 706
    label "obecno&#347;&#263;"
  ]
  node [
    id 707
    label "stand"
  ]
  node [
    id 708
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 709
    label "uczestniczy&#263;"
  ]
  node [
    id 710
    label "participate"
  ]
  node [
    id 711
    label "istnie&#263;"
  ]
  node [
    id 712
    label "pozostawa&#263;"
  ]
  node [
    id 713
    label "zostawa&#263;"
  ]
  node [
    id 714
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 715
    label "adhere"
  ]
  node [
    id 716
    label "compass"
  ]
  node [
    id 717
    label "korzysta&#263;"
  ]
  node [
    id 718
    label "appreciation"
  ]
  node [
    id 719
    label "osi&#261;ga&#263;"
  ]
  node [
    id 720
    label "dociera&#263;"
  ]
  node [
    id 721
    label "get"
  ]
  node [
    id 722
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 723
    label "mierzy&#263;"
  ]
  node [
    id 724
    label "u&#380;ywa&#263;"
  ]
  node [
    id 725
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 726
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 727
    label "exsert"
  ]
  node [
    id 728
    label "being"
  ]
  node [
    id 729
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 730
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 731
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 732
    label "p&#322;ywa&#263;"
  ]
  node [
    id 733
    label "run"
  ]
  node [
    id 734
    label "bangla&#263;"
  ]
  node [
    id 735
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 736
    label "przebiega&#263;"
  ]
  node [
    id 737
    label "wk&#322;ada&#263;"
  ]
  node [
    id 738
    label "proceed"
  ]
  node [
    id 739
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 740
    label "carry"
  ]
  node [
    id 741
    label "bywa&#263;"
  ]
  node [
    id 742
    label "dziama&#263;"
  ]
  node [
    id 743
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 744
    label "stara&#263;_si&#281;"
  ]
  node [
    id 745
    label "para"
  ]
  node [
    id 746
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 747
    label "str&#243;j"
  ]
  node [
    id 748
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 749
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 750
    label "krok"
  ]
  node [
    id 751
    label "tryb"
  ]
  node [
    id 752
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 753
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 754
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 755
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 756
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 757
    label "continue"
  ]
  node [
    id 758
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 759
    label "Ohio"
  ]
  node [
    id 760
    label "wci&#281;cie"
  ]
  node [
    id 761
    label "Nowy_York"
  ]
  node [
    id 762
    label "warstwa"
  ]
  node [
    id 763
    label "samopoczucie"
  ]
  node [
    id 764
    label "Illinois"
  ]
  node [
    id 765
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 766
    label "state"
  ]
  node [
    id 767
    label "Jukatan"
  ]
  node [
    id 768
    label "Kalifornia"
  ]
  node [
    id 769
    label "Wirginia"
  ]
  node [
    id 770
    label "wektor"
  ]
  node [
    id 771
    label "Goa"
  ]
  node [
    id 772
    label "Teksas"
  ]
  node [
    id 773
    label "Waszyngton"
  ]
  node [
    id 774
    label "Massachusetts"
  ]
  node [
    id 775
    label "Alaska"
  ]
  node [
    id 776
    label "Arakan"
  ]
  node [
    id 777
    label "Hawaje"
  ]
  node [
    id 778
    label "Maryland"
  ]
  node [
    id 779
    label "Michigan"
  ]
  node [
    id 780
    label "Arizona"
  ]
  node [
    id 781
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 782
    label "Georgia"
  ]
  node [
    id 783
    label "poziom"
  ]
  node [
    id 784
    label "Pensylwania"
  ]
  node [
    id 785
    label "shape"
  ]
  node [
    id 786
    label "Luizjana"
  ]
  node [
    id 787
    label "Nowy_Meksyk"
  ]
  node [
    id 788
    label "Alabama"
  ]
  node [
    id 789
    label "Kansas"
  ]
  node [
    id 790
    label "Oregon"
  ]
  node [
    id 791
    label "Oklahoma"
  ]
  node [
    id 792
    label "Floryda"
  ]
  node [
    id 793
    label "jednostka_administracyjna"
  ]
  node [
    id 794
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 795
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 796
    label "decyzja"
  ]
  node [
    id 797
    label "pewnie"
  ]
  node [
    id 798
    label "zauwa&#380;alnie"
  ]
  node [
    id 799
    label "oddzia&#322;anie"
  ]
  node [
    id 800
    label "podj&#281;cie"
  ]
  node [
    id 801
    label "resoluteness"
  ]
  node [
    id 802
    label "judgment"
  ]
  node [
    id 803
    label "najpewniej"
  ]
  node [
    id 804
    label "pewny"
  ]
  node [
    id 805
    label "wiarygodnie"
  ]
  node [
    id 806
    label "mocno"
  ]
  node [
    id 807
    label "pewniej"
  ]
  node [
    id 808
    label "bezpiecznie"
  ]
  node [
    id 809
    label "zwinnie"
  ]
  node [
    id 810
    label "zauwa&#380;alny"
  ]
  node [
    id 811
    label "postrzegalnie"
  ]
  node [
    id 812
    label "entertainment"
  ]
  node [
    id 813
    label "consumption"
  ]
  node [
    id 814
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 815
    label "erecting"
  ]
  node [
    id 816
    label "movement"
  ]
  node [
    id 817
    label "zareagowanie"
  ]
  node [
    id 818
    label "reply"
  ]
  node [
    id 819
    label "zahipnotyzowanie"
  ]
  node [
    id 820
    label "chemia"
  ]
  node [
    id 821
    label "wdarcie_si&#281;"
  ]
  node [
    id 822
    label "dotarcie"
  ]
  node [
    id 823
    label "reakcja_chemiczna"
  ]
  node [
    id 824
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 825
    label "management"
  ]
  node [
    id 826
    label "resolution"
  ]
  node [
    id 827
    label "odwrotno&#347;&#263;"
  ]
  node [
    id 828
    label "odmienny"
  ]
  node [
    id 829
    label "inny"
  ]
  node [
    id 830
    label "odwrotnie"
  ]
  node [
    id 831
    label "po_przeciwnej_stronie"
  ]
  node [
    id 832
    label "przeciwnie"
  ]
  node [
    id 833
    label "niech&#281;tny"
  ]
  node [
    id 834
    label "reverse"
  ]
  node [
    id 835
    label "odwrotny"
  ]
  node [
    id 836
    label "na_abarot"
  ]
  node [
    id 837
    label "odmiennie"
  ]
  node [
    id 838
    label "drugi"
  ]
  node [
    id 839
    label "inaczej"
  ]
  node [
    id 840
    label "spornie"
  ]
  node [
    id 841
    label "r&#243;&#380;ny"
  ]
  node [
    id 842
    label "wyj&#261;tkowy"
  ]
  node [
    id 843
    label "specyficzny"
  ]
  node [
    id 844
    label "dziwny"
  ]
  node [
    id 845
    label "niemi&#322;y"
  ]
  node [
    id 846
    label "nie&#380;yczliwie"
  ]
  node [
    id 847
    label "negatywny"
  ]
  node [
    id 848
    label "wstr&#281;tliwy"
  ]
  node [
    id 849
    label "kolejny"
  ]
  node [
    id 850
    label "osobno"
  ]
  node [
    id 851
    label "inszy"
  ]
  node [
    id 852
    label "suppress"
  ]
  node [
    id 853
    label "wytycza&#263;"
  ]
  node [
    id 854
    label "zmniejsza&#263;"
  ]
  node [
    id 855
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 856
    label "environment"
  ]
  node [
    id 857
    label "bound"
  ]
  node [
    id 858
    label "stanowi&#263;"
  ]
  node [
    id 859
    label "wi&#281;zienie"
  ]
  node [
    id 860
    label "decide"
  ]
  node [
    id 861
    label "pies_my&#347;liwski"
  ]
  node [
    id 862
    label "decydowa&#263;"
  ]
  node [
    id 863
    label "represent"
  ]
  node [
    id 864
    label "zatrzymywa&#263;"
  ]
  node [
    id 865
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 866
    label "typify"
  ]
  node [
    id 867
    label "zmienia&#263;"
  ]
  node [
    id 868
    label "control"
  ]
  node [
    id 869
    label "ustala&#263;"
  ]
  node [
    id 870
    label "wyznacza&#263;"
  ]
  node [
    id 871
    label "determine"
  ]
  node [
    id 872
    label "work"
  ]
  node [
    id 873
    label "performance"
  ]
  node [
    id 874
    label "sztuka"
  ]
  node [
    id 875
    label "pierdel"
  ]
  node [
    id 876
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 877
    label "ciupa"
  ]
  node [
    id 878
    label "reedukator"
  ]
  node [
    id 879
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 880
    label "Butyrki"
  ]
  node [
    id 881
    label "imprisonment"
  ]
  node [
    id 882
    label "miejsce_odosobnienia"
  ]
  node [
    id 883
    label "uniemo&#380;liwianie"
  ]
  node [
    id 884
    label "freedom"
  ]
  node [
    id 885
    label "absolutno&#347;&#263;"
  ]
  node [
    id 886
    label "niezale&#380;no&#347;&#263;"
  ]
  node [
    id 887
    label "uwi&#281;zi&#263;"
  ]
  node [
    id 888
    label "nieokre&#347;lono&#347;&#263;"
  ]
  node [
    id 889
    label "uwi&#281;zienie"
  ]
  node [
    id 890
    label "relacja"
  ]
  node [
    id 891
    label "independence"
  ]
  node [
    id 892
    label "charakter"
  ]
  node [
    id 893
    label "brak"
  ]
  node [
    id 894
    label "monarchiczno&#347;&#263;"
  ]
  node [
    id 895
    label "zabranie"
  ]
  node [
    id 896
    label "uniemo&#380;liwienie"
  ]
  node [
    id 897
    label "captivity"
  ]
  node [
    id 898
    label "perpetrate"
  ]
  node [
    id 899
    label "zabra&#263;"
  ]
  node [
    id 900
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 901
    label "na&#322;ogowiec"
  ]
  node [
    id 902
    label "pal&#261;cy"
  ]
  node [
    id 903
    label "robotnik"
  ]
  node [
    id 904
    label "u&#380;ytkownik"
  ]
  node [
    id 905
    label "odbiorca"
  ]
  node [
    id 906
    label "klient"
  ]
  node [
    id 907
    label "restauracja"
  ]
  node [
    id 908
    label "zjadacz"
  ]
  node [
    id 909
    label "&#322;a&#324;cuch_pokarmowy"
  ]
  node [
    id 910
    label "heterotrof"
  ]
  node [
    id 911
    label "go&#347;&#263;"
  ]
  node [
    id 912
    label "uzale&#380;nianie"
  ]
  node [
    id 913
    label "cz&#322;owiek"
  ]
  node [
    id 914
    label "uzale&#380;nienie_si&#281;"
  ]
  node [
    id 915
    label "uzale&#380;nianie_si&#281;"
  ]
  node [
    id 916
    label "uzale&#380;nienie"
  ]
  node [
    id 917
    label "robol"
  ]
  node [
    id 918
    label "przedstawiciel"
  ]
  node [
    id 919
    label "dni&#243;wkarz"
  ]
  node [
    id 920
    label "proletariusz"
  ]
  node [
    id 921
    label "pracownik_fizyczny"
  ]
  node [
    id 922
    label "intensywny"
  ]
  node [
    id 923
    label "gor&#261;cy"
  ]
  node [
    id 924
    label "pilnie"
  ]
  node [
    id 925
    label "dojmuj&#261;co"
  ]
  node [
    id 926
    label "wyra&#378;ny"
  ]
  node [
    id 927
    label "pal&#261;co"
  ]
  node [
    id 928
    label "&#380;ywy"
  ]
  node [
    id 929
    label "nietuzinkowy"
  ]
  node [
    id 930
    label "intryguj&#261;cy"
  ]
  node [
    id 931
    label "ch&#281;tny"
  ]
  node [
    id 932
    label "swoisty"
  ]
  node [
    id 933
    label "interesowanie"
  ]
  node [
    id 934
    label "interesuj&#261;cy"
  ]
  node [
    id 935
    label "ciekawie"
  ]
  node [
    id 936
    label "indagator"
  ]
  node [
    id 937
    label "niespotykany"
  ]
  node [
    id 938
    label "nietuzinkowo"
  ]
  node [
    id 939
    label "interesuj&#261;co"
  ]
  node [
    id 940
    label "atrakcyjny"
  ]
  node [
    id 941
    label "intryguj&#261;co"
  ]
  node [
    id 942
    label "ch&#281;tliwy"
  ]
  node [
    id 943
    label "ch&#281;tnie"
  ]
  node [
    id 944
    label "napalony"
  ]
  node [
    id 945
    label "chy&#380;y"
  ]
  node [
    id 946
    label "&#380;yczliwy"
  ]
  node [
    id 947
    label "przychylny"
  ]
  node [
    id 948
    label "asymilowanie"
  ]
  node [
    id 949
    label "wapniak"
  ]
  node [
    id 950
    label "asymilowa&#263;"
  ]
  node [
    id 951
    label "os&#322;abia&#263;"
  ]
  node [
    id 952
    label "posta&#263;"
  ]
  node [
    id 953
    label "hominid"
  ]
  node [
    id 954
    label "podw&#322;adny"
  ]
  node [
    id 955
    label "dwun&#243;g"
  ]
  node [
    id 956
    label "nasada"
  ]
  node [
    id 957
    label "wz&#243;r"
  ]
  node [
    id 958
    label "senior"
  ]
  node [
    id 959
    label "Adam"
  ]
  node [
    id 960
    label "polifag"
  ]
  node [
    id 961
    label "dziwnie"
  ]
  node [
    id 962
    label "dziwy"
  ]
  node [
    id 963
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 964
    label "odr&#281;bny"
  ]
  node [
    id 965
    label "swoi&#347;cie"
  ]
  node [
    id 966
    label "occupation"
  ]
  node [
    id 967
    label "dobrze"
  ]
  node [
    id 968
    label "ciekawski"
  ]
  node [
    id 969
    label "niedawno"
  ]
  node [
    id 970
    label "poprzedni"
  ]
  node [
    id 971
    label "pozosta&#322;y"
  ]
  node [
    id 972
    label "ostatnio"
  ]
  node [
    id 973
    label "sko&#324;czony"
  ]
  node [
    id 974
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 975
    label "aktualny"
  ]
  node [
    id 976
    label "najgorszy"
  ]
  node [
    id 977
    label "w&#261;tpliwy"
  ]
  node [
    id 978
    label "nast&#281;pnie"
  ]
  node [
    id 979
    label "nastopny"
  ]
  node [
    id 980
    label "kolejno"
  ]
  node [
    id 981
    label "kt&#243;ry&#347;"
  ]
  node [
    id 982
    label "przesz&#322;y"
  ]
  node [
    id 983
    label "wcze&#347;niejszy"
  ]
  node [
    id 984
    label "poprzednio"
  ]
  node [
    id 985
    label "w&#261;tpliwie"
  ]
  node [
    id 986
    label "pozorny"
  ]
  node [
    id 987
    label "ostateczny"
  ]
  node [
    id 988
    label "wa&#380;ny"
  ]
  node [
    id 989
    label "wykszta&#322;cony"
  ]
  node [
    id 990
    label "dyplomowany"
  ]
  node [
    id 991
    label "wykwalifikowany"
  ]
  node [
    id 992
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 993
    label "kompletny"
  ]
  node [
    id 994
    label "sko&#324;czenie"
  ]
  node [
    id 995
    label "okre&#347;lony"
  ]
  node [
    id 996
    label "wielki"
  ]
  node [
    id 997
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 998
    label "aktualnie"
  ]
  node [
    id 999
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 1000
    label "aktualizowanie"
  ]
  node [
    id 1001
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 1002
    label "uaktualnienie"
  ]
  node [
    id 1003
    label "pora_roku"
  ]
  node [
    id 1004
    label "odm&#322;adzanie"
  ]
  node [
    id 1005
    label "liga"
  ]
  node [
    id 1006
    label "egzemplarz"
  ]
  node [
    id 1007
    label "Entuzjastki"
  ]
  node [
    id 1008
    label "kompozycja"
  ]
  node [
    id 1009
    label "Terranie"
  ]
  node [
    id 1010
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1011
    label "category"
  ]
  node [
    id 1012
    label "pakiet_klimatyczny"
  ]
  node [
    id 1013
    label "oddzia&#322;"
  ]
  node [
    id 1014
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1015
    label "cz&#261;steczka"
  ]
  node [
    id 1016
    label "stage_set"
  ]
  node [
    id 1017
    label "type"
  ]
  node [
    id 1018
    label "specgrupa"
  ]
  node [
    id 1019
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1020
    label "&#346;wietliki"
  ]
  node [
    id 1021
    label "odm&#322;odzenie"
  ]
  node [
    id 1022
    label "Eurogrupa"
  ]
  node [
    id 1023
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1024
    label "formacja_geologiczna"
  ]
  node [
    id 1025
    label "harcerze_starsi"
  ]
  node [
    id 1026
    label "konfiguracja"
  ]
  node [
    id 1027
    label "cz&#261;stka"
  ]
  node [
    id 1028
    label "masa_cz&#261;steczkowa"
  ]
  node [
    id 1029
    label "diadochia"
  ]
  node [
    id 1030
    label "grupa_funkcyjna"
  ]
  node [
    id 1031
    label "integer"
  ]
  node [
    id 1032
    label "zlewanie_si&#281;"
  ]
  node [
    id 1033
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1034
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1035
    label "pe&#322;ny"
  ]
  node [
    id 1036
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1037
    label "series"
  ]
  node [
    id 1038
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1039
    label "uprawianie"
  ]
  node [
    id 1040
    label "praca_rolnicza"
  ]
  node [
    id 1041
    label "collection"
  ]
  node [
    id 1042
    label "dane"
  ]
  node [
    id 1043
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1044
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1045
    label "sum"
  ]
  node [
    id 1046
    label "album"
  ]
  node [
    id 1047
    label "lias"
  ]
  node [
    id 1048
    label "dzia&#322;"
  ]
  node [
    id 1049
    label "system"
  ]
  node [
    id 1050
    label "pi&#281;tro"
  ]
  node [
    id 1051
    label "jednostka_geologiczna"
  ]
  node [
    id 1052
    label "filia"
  ]
  node [
    id 1053
    label "malm"
  ]
  node [
    id 1054
    label "dogger"
  ]
  node [
    id 1055
    label "promocja"
  ]
  node [
    id 1056
    label "kurs"
  ]
  node [
    id 1057
    label "bank"
  ]
  node [
    id 1058
    label "formacja"
  ]
  node [
    id 1059
    label "ajencja"
  ]
  node [
    id 1060
    label "wojsko"
  ]
  node [
    id 1061
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 1062
    label "agencja"
  ]
  node [
    id 1063
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 1064
    label "szpital"
  ]
  node [
    id 1065
    label "blend"
  ]
  node [
    id 1066
    label "struktura"
  ]
  node [
    id 1067
    label "prawo_karne"
  ]
  node [
    id 1068
    label "dzie&#322;o"
  ]
  node [
    id 1069
    label "figuracja"
  ]
  node [
    id 1070
    label "chwyt"
  ]
  node [
    id 1071
    label "okup"
  ]
  node [
    id 1072
    label "muzykologia"
  ]
  node [
    id 1073
    label "&#347;redniowiecze"
  ]
  node [
    id 1074
    label "okaz"
  ]
  node [
    id 1075
    label "part"
  ]
  node [
    id 1076
    label "agent"
  ]
  node [
    id 1077
    label "nicpo&#324;"
  ]
  node [
    id 1078
    label "feminizm"
  ]
  node [
    id 1079
    label "Unia_Europejska"
  ]
  node [
    id 1080
    label "odtwarzanie"
  ]
  node [
    id 1081
    label "uatrakcyjnianie"
  ]
  node [
    id 1082
    label "zast&#281;powanie"
  ]
  node [
    id 1083
    label "odbudowywanie"
  ]
  node [
    id 1084
    label "rejuvenation"
  ]
  node [
    id 1085
    label "m&#322;odszy"
  ]
  node [
    id 1086
    label "odbudowywa&#263;"
  ]
  node [
    id 1087
    label "m&#322;odzi&#263;"
  ]
  node [
    id 1088
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 1089
    label "przewietrza&#263;"
  ]
  node [
    id 1090
    label "wymienia&#263;"
  ]
  node [
    id 1091
    label "odtwarza&#263;"
  ]
  node [
    id 1092
    label "uatrakcyjni&#263;"
  ]
  node [
    id 1093
    label "przewietrzy&#263;"
  ]
  node [
    id 1094
    label "regenerate"
  ]
  node [
    id 1095
    label "odtworzy&#263;"
  ]
  node [
    id 1096
    label "wymieni&#263;"
  ]
  node [
    id 1097
    label "odbudowa&#263;"
  ]
  node [
    id 1098
    label "uatrakcyjnienie"
  ]
  node [
    id 1099
    label "odbudowanie"
  ]
  node [
    id 1100
    label "odtworzenie"
  ]
  node [
    id 1101
    label "asymilowanie_si&#281;"
  ]
  node [
    id 1102
    label "absorption"
  ]
  node [
    id 1103
    label "pobieranie"
  ]
  node [
    id 1104
    label "czerpanie"
  ]
  node [
    id 1105
    label "acquisition"
  ]
  node [
    id 1106
    label "zmienianie"
  ]
  node [
    id 1107
    label "organizm"
  ]
  node [
    id 1108
    label "assimilation"
  ]
  node [
    id 1109
    label "upodabnianie"
  ]
  node [
    id 1110
    label "kultura"
  ]
  node [
    id 1111
    label "podobny"
  ]
  node [
    id 1112
    label "fonetyka"
  ]
  node [
    id 1113
    label "mecz_mistrzowski"
  ]
  node [
    id 1114
    label "&#347;rodowisko"
  ]
  node [
    id 1115
    label "obrona"
  ]
  node [
    id 1116
    label "pomoc"
  ]
  node [
    id 1117
    label "organizacja"
  ]
  node [
    id 1118
    label "rezerwa"
  ]
  node [
    id 1119
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 1120
    label "moneta"
  ]
  node [
    id 1121
    label "union"
  ]
  node [
    id 1122
    label "assimilate"
  ]
  node [
    id 1123
    label "dostosowywa&#263;"
  ]
  node [
    id 1124
    label "dostosowa&#263;"
  ]
  node [
    id 1125
    label "przejmowa&#263;"
  ]
  node [
    id 1126
    label "upodobni&#263;"
  ]
  node [
    id 1127
    label "przej&#261;&#263;"
  ]
  node [
    id 1128
    label "upodabnia&#263;"
  ]
  node [
    id 1129
    label "pobiera&#263;"
  ]
  node [
    id 1130
    label "pobra&#263;"
  ]
  node [
    id 1131
    label "typ"
  ]
  node [
    id 1132
    label "zoologia"
  ]
  node [
    id 1133
    label "skupienie"
  ]
  node [
    id 1134
    label "kr&#243;lestwo"
  ]
  node [
    id 1135
    label "tribe"
  ]
  node [
    id 1136
    label "hurma"
  ]
  node [
    id 1137
    label "botanika"
  ]
  node [
    id 1138
    label "nieistotnie"
  ]
  node [
    id 1139
    label "nieznaczny"
  ]
  node [
    id 1140
    label "niepowa&#380;nie"
  ]
  node [
    id 1141
    label "niewa&#380;ny"
  ]
  node [
    id 1142
    label "drobnostkowy"
  ]
  node [
    id 1143
    label "ma&#322;y"
  ]
  node [
    id 1144
    label "ascend"
  ]
  node [
    id 1145
    label "zmieni&#263;"
  ]
  node [
    id 1146
    label "sprawi&#263;"
  ]
  node [
    id 1147
    label "change"
  ]
  node [
    id 1148
    label "zrobi&#263;"
  ]
  node [
    id 1149
    label "zast&#261;pi&#263;"
  ]
  node [
    id 1150
    label "come_up"
  ]
  node [
    id 1151
    label "przej&#347;&#263;"
  ]
  node [
    id 1152
    label "straci&#263;"
  ]
  node [
    id 1153
    label "zyska&#263;"
  ]
  node [
    id 1154
    label "zabrakn&#261;&#263;"
  ]
  node [
    id 1155
    label "become"
  ]
  node [
    id 1156
    label "stracenie"
  ]
  node [
    id 1157
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1158
    label "leave_office"
  ]
  node [
    id 1159
    label "zabi&#263;"
  ]
  node [
    id 1160
    label "forfeit"
  ]
  node [
    id 1161
    label "wytraci&#263;"
  ]
  node [
    id 1162
    label "waste"
  ]
  node [
    id 1163
    label "spowodowa&#263;"
  ]
  node [
    id 1164
    label "przegra&#263;"
  ]
  node [
    id 1165
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 1166
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 1167
    label "execute"
  ]
  node [
    id 1168
    label "omin&#261;&#263;"
  ]
  node [
    id 1169
    label "lack"
  ]
  node [
    id 1170
    label "polski"
  ]
  node [
    id 1171
    label "Polish"
  ]
  node [
    id 1172
    label "goniony"
  ]
  node [
    id 1173
    label "oberek"
  ]
  node [
    id 1174
    label "ryba_po_grecku"
  ]
  node [
    id 1175
    label "sztajer"
  ]
  node [
    id 1176
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 1177
    label "krakowiak"
  ]
  node [
    id 1178
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 1179
    label "pierogi_ruskie"
  ]
  node [
    id 1180
    label "lacki"
  ]
  node [
    id 1181
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 1182
    label "chodzony"
  ]
  node [
    id 1183
    label "po_polsku"
  ]
  node [
    id 1184
    label "mazur"
  ]
  node [
    id 1185
    label "polsko"
  ]
  node [
    id 1186
    label "skoczny"
  ]
  node [
    id 1187
    label "drabant"
  ]
  node [
    id 1188
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 1189
    label "j&#281;zyk"
  ]
  node [
    id 1190
    label "s&#261;d"
  ]
  node [
    id 1191
    label "szko&#322;a"
  ]
  node [
    id 1192
    label "p&#322;&#243;d"
  ]
  node [
    id 1193
    label "thinking"
  ]
  node [
    id 1194
    label "umys&#322;"
  ]
  node [
    id 1195
    label "political_orientation"
  ]
  node [
    id 1196
    label "istota"
  ]
  node [
    id 1197
    label "pomys&#322;"
  ]
  node [
    id 1198
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 1199
    label "idea"
  ]
  node [
    id 1200
    label "fantomatyka"
  ]
  node [
    id 1201
    label "pami&#281;&#263;"
  ]
  node [
    id 1202
    label "intelekt"
  ]
  node [
    id 1203
    label "pomieszanie_si&#281;"
  ]
  node [
    id 1204
    label "wn&#281;trze"
  ]
  node [
    id 1205
    label "wyobra&#378;nia"
  ]
  node [
    id 1206
    label "rezultat"
  ]
  node [
    id 1207
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1208
    label "superego"
  ]
  node [
    id 1209
    label "psychika"
  ]
  node [
    id 1210
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 1211
    label "moczownik"
  ]
  node [
    id 1212
    label "embryo"
  ]
  node [
    id 1213
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 1214
    label "zarodek"
  ]
  node [
    id 1215
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 1216
    label "latawiec"
  ]
  node [
    id 1217
    label "j&#261;dro"
  ]
  node [
    id 1218
    label "systemik"
  ]
  node [
    id 1219
    label "rozprz&#261;c"
  ]
  node [
    id 1220
    label "oprogramowanie"
  ]
  node [
    id 1221
    label "systemat"
  ]
  node [
    id 1222
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1223
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1224
    label "model"
  ]
  node [
    id 1225
    label "usenet"
  ]
  node [
    id 1226
    label "porz&#261;dek"
  ]
  node [
    id 1227
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1228
    label "przyn&#281;ta"
  ]
  node [
    id 1229
    label "net"
  ]
  node [
    id 1230
    label "w&#281;dkarstwo"
  ]
  node [
    id 1231
    label "eratem"
  ]
  node [
    id 1232
    label "doktryna"
  ]
  node [
    id 1233
    label "pulpit"
  ]
  node [
    id 1234
    label "konstelacja"
  ]
  node [
    id 1235
    label "o&#347;"
  ]
  node [
    id 1236
    label "podsystem"
  ]
  node [
    id 1237
    label "metoda"
  ]
  node [
    id 1238
    label "ryba"
  ]
  node [
    id 1239
    label "Leopard"
  ]
  node [
    id 1240
    label "spos&#243;b"
  ]
  node [
    id 1241
    label "Android"
  ]
  node [
    id 1242
    label "zachowanie"
  ]
  node [
    id 1243
    label "cybernetyk"
  ]
  node [
    id 1244
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1245
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1246
    label "method"
  ]
  node [
    id 1247
    label "sk&#322;ad"
  ]
  node [
    id 1248
    label "podstawa"
  ]
  node [
    id 1249
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1250
    label "podejrzany"
  ]
  node [
    id 1251
    label "s&#261;downictwo"
  ]
  node [
    id 1252
    label "biuro"
  ]
  node [
    id 1253
    label "court"
  ]
  node [
    id 1254
    label "forum"
  ]
  node [
    id 1255
    label "bronienie"
  ]
  node [
    id 1256
    label "urz&#261;d"
  ]
  node [
    id 1257
    label "oskar&#380;yciel"
  ]
  node [
    id 1258
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1259
    label "skazany"
  ]
  node [
    id 1260
    label "post&#281;powanie"
  ]
  node [
    id 1261
    label "broni&#263;"
  ]
  node [
    id 1262
    label "pods&#261;dny"
  ]
  node [
    id 1263
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1264
    label "antylogizm"
  ]
  node [
    id 1265
    label "konektyw"
  ]
  node [
    id 1266
    label "&#347;wiadek"
  ]
  node [
    id 1267
    label "procesowicz"
  ]
  node [
    id 1268
    label "technika"
  ]
  node [
    id 1269
    label "pocz&#261;tki"
  ]
  node [
    id 1270
    label "ukradzenie"
  ]
  node [
    id 1271
    label "ukra&#347;&#263;"
  ]
  node [
    id 1272
    label "do&#347;wiadczenie"
  ]
  node [
    id 1273
    label "teren_szko&#322;y"
  ]
  node [
    id 1274
    label "Mickiewicz"
  ]
  node [
    id 1275
    label "kwalifikacje"
  ]
  node [
    id 1276
    label "podr&#281;cznik"
  ]
  node [
    id 1277
    label "absolwent"
  ]
  node [
    id 1278
    label "praktyka"
  ]
  node [
    id 1279
    label "school"
  ]
  node [
    id 1280
    label "zda&#263;"
  ]
  node [
    id 1281
    label "gabinet"
  ]
  node [
    id 1282
    label "urszulanki"
  ]
  node [
    id 1283
    label "sztuba"
  ]
  node [
    id 1284
    label "&#322;awa_szkolna"
  ]
  node [
    id 1285
    label "nauka"
  ]
  node [
    id 1286
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 1287
    label "przepisa&#263;"
  ]
  node [
    id 1288
    label "muzyka"
  ]
  node [
    id 1289
    label "form"
  ]
  node [
    id 1290
    label "lekcja"
  ]
  node [
    id 1291
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 1292
    label "przepisanie"
  ]
  node [
    id 1293
    label "skolaryzacja"
  ]
  node [
    id 1294
    label "zdanie"
  ]
  node [
    id 1295
    label "stopek"
  ]
  node [
    id 1296
    label "sekretariat"
  ]
  node [
    id 1297
    label "ideologia"
  ]
  node [
    id 1298
    label "lesson"
  ]
  node [
    id 1299
    label "niepokalanki"
  ]
  node [
    id 1300
    label "szkolenie"
  ]
  node [
    id 1301
    label "kara"
  ]
  node [
    id 1302
    label "tablica"
  ]
  node [
    id 1303
    label "byt"
  ]
  node [
    id 1304
    label "Kant"
  ]
  node [
    id 1305
    label "cel"
  ]
  node [
    id 1306
    label "ideacja"
  ]
  node [
    id 1307
    label "sklep"
  ]
  node [
    id 1308
    label "towar"
  ]
  node [
    id 1309
    label "&#347;rodek_pobudzaj&#261;cy"
  ]
  node [
    id 1310
    label "kosmetyk"
  ]
  node [
    id 1311
    label "filter"
  ]
  node [
    id 1312
    label "program"
  ]
  node [
    id 1313
    label "faktor"
  ]
  node [
    id 1314
    label "ochrona"
  ]
  node [
    id 1315
    label "zajaranie"
  ]
  node [
    id 1316
    label "roz&#380;arzenie"
  ]
  node [
    id 1317
    label "pozapalanie"
  ]
  node [
    id 1318
    label "zaburzenie"
  ]
  node [
    id 1319
    label "&#347;wiecenie"
  ]
  node [
    id 1320
    label "rozja&#347;nienie"
  ]
  node [
    id 1321
    label "rozdra&#380;ni&#263;"
  ]
  node [
    id 1322
    label "rozdra&#380;nia&#263;"
  ]
  node [
    id 1323
    label "blaze"
  ]
  node [
    id 1324
    label "psu&#263;"
  ]
  node [
    id 1325
    label "reek"
  ]
  node [
    id 1326
    label "podra&#380;nia&#263;"
  ]
  node [
    id 1327
    label "bole&#263;"
  ]
  node [
    id 1328
    label "doskwiera&#263;"
  ]
  node [
    id 1329
    label "podtrzymywa&#263;"
  ]
  node [
    id 1330
    label "ridicule"
  ]
  node [
    id 1331
    label "odstawia&#263;"
  ]
  node [
    id 1332
    label "flash"
  ]
  node [
    id 1333
    label "grza&#263;"
  ]
  node [
    id 1334
    label "poddawa&#263;"
  ]
  node [
    id 1335
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 1336
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 1337
    label "niszczy&#263;"
  ]
  node [
    id 1338
    label "strzela&#263;"
  ]
  node [
    id 1339
    label "da&#263;"
  ]
  node [
    id 1340
    label "zapali&#263;"
  ]
  node [
    id 1341
    label "zadzia&#322;a&#263;"
  ]
  node [
    id 1342
    label "reject"
  ]
  node [
    id 1343
    label "resist"
  ]
  node [
    id 1344
    label "za&#347;wieci&#263;"
  ]
  node [
    id 1345
    label "odpowiedzie&#263;"
  ]
  node [
    id 1346
    label "paln&#261;&#263;"
  ]
  node [
    id 1347
    label "strzeli&#263;"
  ]
  node [
    id 1348
    label "arouse"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 772
  ]
  edge [
    source 11
    target 773
  ]
  edge [
    source 11
    target 774
  ]
  edge [
    source 11
    target 775
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 777
  ]
  edge [
    source 11
    target 778
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 779
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 781
  ]
  edge [
    source 11
    target 782
  ]
  edge [
    source 11
    target 783
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 852
  ]
  edge [
    source 14
    target 853
  ]
  edge [
    source 14
    target 854
  ]
  edge [
    source 14
    target 855
  ]
  edge [
    source 14
    target 856
  ]
  edge [
    source 14
    target 857
  ]
  edge [
    source 14
    target 858
  ]
  edge [
    source 14
    target 859
  ]
  edge [
    source 14
    target 860
  ]
  edge [
    source 14
    target 861
  ]
  edge [
    source 14
    target 862
  ]
  edge [
    source 14
    target 863
  ]
  edge [
    source 14
    target 864
  ]
  edge [
    source 14
    target 865
  ]
  edge [
    source 14
    target 866
  ]
  edge [
    source 14
    target 867
  ]
  edge [
    source 14
    target 868
  ]
  edge [
    source 14
    target 869
  ]
  edge [
    source 14
    target 870
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 871
  ]
  edge [
    source 14
    target 872
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 823
  ]
  edge [
    source 14
    target 873
  ]
  edge [
    source 14
    target 874
  ]
  edge [
    source 14
    target 875
  ]
  edge [
    source 14
    target 660
  ]
  edge [
    source 14
    target 876
  ]
  edge [
    source 14
    target 877
  ]
  edge [
    source 14
    target 878
  ]
  edge [
    source 14
    target 879
  ]
  edge [
    source 14
    target 880
  ]
  edge [
    source 14
    target 881
  ]
  edge [
    source 14
    target 882
  ]
  edge [
    source 14
    target 883
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 887
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 549
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 552
  ]
  edge [
    source 15
    target 553
  ]
  edge [
    source 15
    target 554
  ]
  edge [
    source 15
    target 555
  ]
  edge [
    source 15
    target 556
  ]
  edge [
    source 15
    target 557
  ]
  edge [
    source 15
    target 558
  ]
  edge [
    source 15
    target 545
  ]
  edge [
    source 15
    target 890
  ]
  edge [
    source 15
    target 891
  ]
  edge [
    source 15
    target 892
  ]
  edge [
    source 15
    target 893
  ]
  edge [
    source 15
    target 560
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 894
  ]
  edge [
    source 15
    target 895
  ]
  edge [
    source 15
    target 896
  ]
  edge [
    source 15
    target 897
  ]
  edge [
    source 15
    target 898
  ]
  edge [
    source 15
    target 899
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 901
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 254
  ]
  edge [
    source 16
    target 903
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 929
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 409
  ]
  edge [
    source 17
    target 36
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 383
  ]
  edge [
    source 17
    target 55
  ]
  edge [
    source 17
    target 56
  ]
  edge [
    source 17
    target 58
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 63
  ]
  edge [
    source 17
    target 64
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 66
  ]
  edge [
    source 17
    target 70
  ]
  edge [
    source 17
    target 68
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 69
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 72
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 17
    target 375
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 968
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 635
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 829
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 383
  ]
  edge [
    source 18
    target 55
  ]
  edge [
    source 18
    target 56
  ]
  edge [
    source 18
    target 58
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 63
  ]
  edge [
    source 18
    target 64
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 66
  ]
  edge [
    source 18
    target 70
  ]
  edge [
    source 18
    target 68
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 69
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 72
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 18
    target 1002
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1003
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1004
  ]
  edge [
    source 21
    target 1005
  ]
  edge [
    source 21
    target 675
  ]
  edge [
    source 21
    target 948
  ]
  edge [
    source 21
    target 680
  ]
  edge [
    source 21
    target 42
  ]
  edge [
    source 21
    target 950
  ]
  edge [
    source 21
    target 1006
  ]
  edge [
    source 21
    target 1007
  ]
  edge [
    source 21
    target 592
  ]
  edge [
    source 21
    target 1008
  ]
  edge [
    source 21
    target 1009
  ]
  edge [
    source 21
    target 1010
  ]
  edge [
    source 21
    target 1011
  ]
  edge [
    source 21
    target 1012
  ]
  edge [
    source 21
    target 1013
  ]
  edge [
    source 21
    target 1014
  ]
  edge [
    source 21
    target 1015
  ]
  edge [
    source 21
    target 1016
  ]
  edge [
    source 21
    target 1017
  ]
  edge [
    source 21
    target 1018
  ]
  edge [
    source 21
    target 1019
  ]
  edge [
    source 21
    target 1020
  ]
  edge [
    source 21
    target 1021
  ]
  edge [
    source 21
    target 1022
  ]
  edge [
    source 21
    target 1023
  ]
  edge [
    source 21
    target 1024
  ]
  edge [
    source 21
    target 1025
  ]
  edge [
    source 21
    target 1026
  ]
  edge [
    source 21
    target 1027
  ]
  edge [
    source 21
    target 1028
  ]
  edge [
    source 21
    target 1029
  ]
  edge [
    source 21
    target 532
  ]
  edge [
    source 21
    target 1030
  ]
  edge [
    source 21
    target 1031
  ]
  edge [
    source 21
    target 610
  ]
  edge [
    source 21
    target 1032
  ]
  edge [
    source 21
    target 80
  ]
  edge [
    source 21
    target 616
  ]
  edge [
    source 21
    target 1033
  ]
  edge [
    source 21
    target 1034
  ]
  edge [
    source 21
    target 1035
  ]
  edge [
    source 21
    target 1036
  ]
  edge [
    source 21
    target 1037
  ]
  edge [
    source 21
    target 1038
  ]
  edge [
    source 21
    target 1039
  ]
  edge [
    source 21
    target 1040
  ]
  edge [
    source 21
    target 1041
  ]
  edge [
    source 21
    target 1042
  ]
  edge [
    source 21
    target 1043
  ]
  edge [
    source 21
    target 38
  ]
  edge [
    source 21
    target 1044
  ]
  edge [
    source 21
    target 1045
  ]
  edge [
    source 21
    target 238
  ]
  edge [
    source 21
    target 1046
  ]
  edge [
    source 21
    target 621
  ]
  edge [
    source 21
    target 1047
  ]
  edge [
    source 21
    target 1048
  ]
  edge [
    source 21
    target 1049
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 1050
  ]
  edge [
    source 21
    target 684
  ]
  edge [
    source 21
    target 1051
  ]
  edge [
    source 21
    target 1052
  ]
  edge [
    source 21
    target 1053
  ]
  edge [
    source 21
    target 79
  ]
  edge [
    source 21
    target 1054
  ]
  edge [
    source 21
    target 783
  ]
  edge [
    source 21
    target 1055
  ]
  edge [
    source 21
    target 1056
  ]
  edge [
    source 21
    target 1057
  ]
  edge [
    source 21
    target 1058
  ]
  edge [
    source 21
    target 1059
  ]
  edge [
    source 21
    target 1060
  ]
  edge [
    source 21
    target 586
  ]
  edge [
    source 21
    target 1061
  ]
  edge [
    source 21
    target 1062
  ]
  edge [
    source 21
    target 1063
  ]
  edge [
    source 21
    target 1064
  ]
  edge [
    source 21
    target 1065
  ]
  edge [
    source 21
    target 1066
  ]
  edge [
    source 21
    target 1067
  ]
  edge [
    source 21
    target 612
  ]
  edge [
    source 21
    target 1068
  ]
  edge [
    source 21
    target 1069
  ]
  edge [
    source 21
    target 1070
  ]
  edge [
    source 21
    target 1071
  ]
  edge [
    source 21
    target 1072
  ]
  edge [
    source 21
    target 1073
  ]
  edge [
    source 21
    target 54
  ]
  edge [
    source 21
    target 44
  ]
  edge [
    source 21
    target 45
  ]
  edge [
    source 21
    target 57
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 21
    target 577
  ]
  edge [
    source 21
    target 65
  ]
  edge [
    source 21
    target 48
  ]
  edge [
    source 21
    target 1074
  ]
  edge [
    source 21
    target 1075
  ]
  edge [
    source 21
    target 49
  ]
  edge [
    source 21
    target 61
  ]
  edge [
    source 21
    target 39
  ]
  edge [
    source 21
    target 59
  ]
  edge [
    source 21
    target 874
  ]
  edge [
    source 21
    target 1076
  ]
  edge [
    source 21
    target 51
  ]
  edge [
    source 21
    target 60
  ]
  edge [
    source 21
    target 1077
  ]
  edge [
    source 21
    target 43
  ]
  edge [
    source 21
    target 1078
  ]
  edge [
    source 21
    target 1079
  ]
  edge [
    source 21
    target 1080
  ]
  edge [
    source 21
    target 1081
  ]
  edge [
    source 21
    target 1082
  ]
  edge [
    source 21
    target 1083
  ]
  edge [
    source 21
    target 1084
  ]
  edge [
    source 21
    target 1085
  ]
  edge [
    source 21
    target 1086
  ]
  edge [
    source 21
    target 1087
  ]
  edge [
    source 21
    target 1088
  ]
  edge [
    source 21
    target 1089
  ]
  edge [
    source 21
    target 1090
  ]
  edge [
    source 21
    target 1091
  ]
  edge [
    source 21
    target 1092
  ]
  edge [
    source 21
    target 1093
  ]
  edge [
    source 21
    target 1094
  ]
  edge [
    source 21
    target 1095
  ]
  edge [
    source 21
    target 1096
  ]
  edge [
    source 21
    target 1097
  ]
  edge [
    source 21
    target 244
  ]
  edge [
    source 21
    target 1098
  ]
  edge [
    source 21
    target 1099
  ]
  edge [
    source 21
    target 1100
  ]
  edge [
    source 21
    target 1101
  ]
  edge [
    source 21
    target 1102
  ]
  edge [
    source 21
    target 1103
  ]
  edge [
    source 21
    target 1104
  ]
  edge [
    source 21
    target 1105
  ]
  edge [
    source 21
    target 913
  ]
  edge [
    source 21
    target 1106
  ]
  edge [
    source 21
    target 1107
  ]
  edge [
    source 21
    target 1108
  ]
  edge [
    source 21
    target 1109
  ]
  edge [
    source 21
    target 213
  ]
  edge [
    source 21
    target 1110
  ]
  edge [
    source 21
    target 1111
  ]
  edge [
    source 21
    target 1112
  ]
  edge [
    source 21
    target 1113
  ]
  edge [
    source 21
    target 1114
  ]
  edge [
    source 21
    target 288
  ]
  edge [
    source 21
    target 1115
  ]
  edge [
    source 21
    target 1116
  ]
  edge [
    source 21
    target 1117
  ]
  edge [
    source 21
    target 1118
  ]
  edge [
    source 21
    target 1119
  ]
  edge [
    source 21
    target 556
  ]
  edge [
    source 21
    target 515
  ]
  edge [
    source 21
    target 1120
  ]
  edge [
    source 21
    target 1121
  ]
  edge [
    source 21
    target 1122
  ]
  edge [
    source 21
    target 1123
  ]
  edge [
    source 21
    target 1124
  ]
  edge [
    source 21
    target 1125
  ]
  edge [
    source 21
    target 1126
  ]
  edge [
    source 21
    target 1127
  ]
  edge [
    source 21
    target 1128
  ]
  edge [
    source 21
    target 1129
  ]
  edge [
    source 21
    target 1130
  ]
  edge [
    source 21
    target 1131
  ]
  edge [
    source 21
    target 793
  ]
  edge [
    source 21
    target 1132
  ]
  edge [
    source 21
    target 1133
  ]
  edge [
    source 21
    target 1134
  ]
  edge [
    source 21
    target 1135
  ]
  edge [
    source 21
    target 1136
  ]
  edge [
    source 21
    target 1137
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1138
  ]
  edge [
    source 22
    target 1139
  ]
  edge [
    source 22
    target 1140
  ]
  edge [
    source 22
    target 1141
  ]
  edge [
    source 22
    target 1142
  ]
  edge [
    source 22
    target 1143
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1144
  ]
  edge [
    source 24
    target 1145
  ]
  edge [
    source 24
    target 1146
  ]
  edge [
    source 24
    target 1147
  ]
  edge [
    source 24
    target 1148
  ]
  edge [
    source 24
    target 1149
  ]
  edge [
    source 24
    target 1150
  ]
  edge [
    source 24
    target 1151
  ]
  edge [
    source 24
    target 1152
  ]
  edge [
    source 24
    target 1153
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1154
  ]
  edge [
    source 25
    target 1152
  ]
  edge [
    source 25
    target 1155
  ]
  edge [
    source 25
    target 1156
  ]
  edge [
    source 25
    target 1157
  ]
  edge [
    source 25
    target 1158
  ]
  edge [
    source 25
    target 1159
  ]
  edge [
    source 25
    target 1160
  ]
  edge [
    source 25
    target 1161
  ]
  edge [
    source 25
    target 1162
  ]
  edge [
    source 25
    target 1163
  ]
  edge [
    source 25
    target 1164
  ]
  edge [
    source 25
    target 1165
  ]
  edge [
    source 25
    target 1166
  ]
  edge [
    source 25
    target 1167
  ]
  edge [
    source 25
    target 1168
  ]
  edge [
    source 25
    target 1169
  ]
  edge [
    source 26
    target 913
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 26
    target 948
  ]
  edge [
    source 26
    target 949
  ]
  edge [
    source 26
    target 950
  ]
  edge [
    source 26
    target 951
  ]
  edge [
    source 26
    target 952
  ]
  edge [
    source 26
    target 953
  ]
  edge [
    source 26
    target 954
  ]
  edge [
    source 26
    target 383
  ]
  edge [
    source 26
    target 55
  ]
  edge [
    source 26
    target 56
  ]
  edge [
    source 26
    target 58
  ]
  edge [
    source 26
    target 955
  ]
  edge [
    source 26
    target 63
  ]
  edge [
    source 26
    target 64
  ]
  edge [
    source 26
    target 956
  ]
  edge [
    source 26
    target 66
  ]
  edge [
    source 26
    target 70
  ]
  edge [
    source 26
    target 68
  ]
  edge [
    source 26
    target 957
  ]
  edge [
    source 26
    target 958
  ]
  edge [
    source 26
    target 69
  ]
  edge [
    source 26
    target 959
  ]
  edge [
    source 26
    target 72
  ]
  edge [
    source 26
    target 960
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1170
  ]
  edge [
    source 27
    target 374
  ]
  edge [
    source 27
    target 1171
  ]
  edge [
    source 27
    target 1172
  ]
  edge [
    source 27
    target 1173
  ]
  edge [
    source 27
    target 1174
  ]
  edge [
    source 27
    target 1175
  ]
  edge [
    source 27
    target 1176
  ]
  edge [
    source 27
    target 1177
  ]
  edge [
    source 27
    target 1178
  ]
  edge [
    source 27
    target 1179
  ]
  edge [
    source 27
    target 1180
  ]
  edge [
    source 27
    target 1181
  ]
  edge [
    source 27
    target 1182
  ]
  edge [
    source 27
    target 1183
  ]
  edge [
    source 27
    target 1184
  ]
  edge [
    source 27
    target 1185
  ]
  edge [
    source 27
    target 1186
  ]
  edge [
    source 27
    target 1187
  ]
  edge [
    source 27
    target 1188
  ]
  edge [
    source 27
    target 1189
  ]
  edge [
    source 28
    target 1190
  ]
  edge [
    source 28
    target 1191
  ]
  edge [
    source 28
    target 577
  ]
  edge [
    source 28
    target 1192
  ]
  edge [
    source 28
    target 1193
  ]
  edge [
    source 28
    target 1194
  ]
  edge [
    source 28
    target 1195
  ]
  edge [
    source 28
    target 1196
  ]
  edge [
    source 28
    target 1197
  ]
  edge [
    source 28
    target 1198
  ]
  edge [
    source 28
    target 1199
  ]
  edge [
    source 28
    target 1049
  ]
  edge [
    source 28
    target 1200
  ]
  edge [
    source 28
    target 1201
  ]
  edge [
    source 28
    target 913
  ]
  edge [
    source 28
    target 1202
  ]
  edge [
    source 28
    target 1203
  ]
  edge [
    source 28
    target 1204
  ]
  edge [
    source 28
    target 1205
  ]
  edge [
    source 28
    target 374
  ]
  edge [
    source 28
    target 872
  ]
  edge [
    source 28
    target 1206
  ]
  edge [
    source 28
    target 1207
  ]
  edge [
    source 28
    target 1208
  ]
  edge [
    source 28
    target 1209
  ]
  edge [
    source 28
    target 603
  ]
  edge [
    source 28
    target 892
  ]
  edge [
    source 28
    target 549
  ]
  edge [
    source 28
    target 1210
  ]
  edge [
    source 28
    target 1211
  ]
  edge [
    source 28
    target 1212
  ]
  edge [
    source 28
    target 1213
  ]
  edge [
    source 28
    target 1214
  ]
  edge [
    source 28
    target 1215
  ]
  edge [
    source 28
    target 1216
  ]
  edge [
    source 28
    target 1217
  ]
  edge [
    source 28
    target 1218
  ]
  edge [
    source 28
    target 1219
  ]
  edge [
    source 28
    target 1220
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 28
    target 1221
  ]
  edge [
    source 28
    target 1222
  ]
  edge [
    source 28
    target 42
  ]
  edge [
    source 28
    target 1223
  ]
  edge [
    source 28
    target 1224
  ]
  edge [
    source 28
    target 1066
  ]
  edge [
    source 28
    target 1225
  ]
  edge [
    source 28
    target 592
  ]
  edge [
    source 28
    target 1226
  ]
  edge [
    source 28
    target 1227
  ]
  edge [
    source 28
    target 1228
  ]
  edge [
    source 28
    target 1229
  ]
  edge [
    source 28
    target 1230
  ]
  edge [
    source 28
    target 1231
  ]
  edge [
    source 28
    target 1013
  ]
  edge [
    source 28
    target 1232
  ]
  edge [
    source 28
    target 1233
  ]
  edge [
    source 28
    target 1234
  ]
  edge [
    source 28
    target 1051
  ]
  edge [
    source 28
    target 1235
  ]
  edge [
    source 28
    target 1236
  ]
  edge [
    source 28
    target 1237
  ]
  edge [
    source 28
    target 1238
  ]
  edge [
    source 28
    target 1239
  ]
  edge [
    source 28
    target 1240
  ]
  edge [
    source 28
    target 1241
  ]
  edge [
    source 28
    target 1242
  ]
  edge [
    source 28
    target 1243
  ]
  edge [
    source 28
    target 1244
  ]
  edge [
    source 28
    target 1245
  ]
  edge [
    source 28
    target 1246
  ]
  edge [
    source 28
    target 1247
  ]
  edge [
    source 28
    target 1248
  ]
  edge [
    source 28
    target 1249
  ]
  edge [
    source 28
    target 621
  ]
  edge [
    source 28
    target 1250
  ]
  edge [
    source 28
    target 1251
  ]
  edge [
    source 28
    target 1252
  ]
  edge [
    source 28
    target 1253
  ]
  edge [
    source 28
    target 1254
  ]
  edge [
    source 28
    target 1255
  ]
  edge [
    source 28
    target 1256
  ]
  edge [
    source 28
    target 209
  ]
  edge [
    source 28
    target 1257
  ]
  edge [
    source 28
    target 1258
  ]
  edge [
    source 28
    target 1259
  ]
  edge [
    source 28
    target 1260
  ]
  edge [
    source 28
    target 1261
  ]
  edge [
    source 28
    target 1262
  ]
  edge [
    source 28
    target 1263
  ]
  edge [
    source 28
    target 1115
  ]
  edge [
    source 28
    target 277
  ]
  edge [
    source 28
    target 674
  ]
  edge [
    source 28
    target 1264
  ]
  edge [
    source 28
    target 1265
  ]
  edge [
    source 28
    target 1266
  ]
  edge [
    source 28
    target 1267
  ]
  edge [
    source 28
    target 614
  ]
  edge [
    source 28
    target 1268
  ]
  edge [
    source 28
    target 1269
  ]
  edge [
    source 28
    target 1270
  ]
  edge [
    source 28
    target 1271
  ]
  edge [
    source 28
    target 1272
  ]
  edge [
    source 28
    target 1273
  ]
  edge [
    source 28
    target 205
  ]
  edge [
    source 28
    target 1274
  ]
  edge [
    source 28
    target 1275
  ]
  edge [
    source 28
    target 1276
  ]
  edge [
    source 28
    target 1277
  ]
  edge [
    source 28
    target 1278
  ]
  edge [
    source 28
    target 1279
  ]
  edge [
    source 28
    target 1280
  ]
  edge [
    source 28
    target 1281
  ]
  edge [
    source 28
    target 1282
  ]
  edge [
    source 28
    target 1283
  ]
  edge [
    source 28
    target 1284
  ]
  edge [
    source 28
    target 1285
  ]
  edge [
    source 28
    target 1286
  ]
  edge [
    source 28
    target 1287
  ]
  edge [
    source 28
    target 1288
  ]
  edge [
    source 28
    target 1289
  ]
  edge [
    source 28
    target 684
  ]
  edge [
    source 28
    target 1290
  ]
  edge [
    source 28
    target 1291
  ]
  edge [
    source 28
    target 1292
  ]
  edge [
    source 28
    target 608
  ]
  edge [
    source 28
    target 1293
  ]
  edge [
    source 28
    target 1294
  ]
  edge [
    source 28
    target 1295
  ]
  edge [
    source 28
    target 1296
  ]
  edge [
    source 28
    target 1297
  ]
  edge [
    source 28
    target 1298
  ]
  edge [
    source 28
    target 1299
  ]
  edge [
    source 28
    target 586
  ]
  edge [
    source 28
    target 1300
  ]
  edge [
    source 28
    target 1301
  ]
  edge [
    source 28
    target 1302
  ]
  edge [
    source 28
    target 1303
  ]
  edge [
    source 28
    target 1304
  ]
  edge [
    source 28
    target 1305
  ]
  edge [
    source 28
    target 1306
  ]
  edge [
    source 29
    target 472
  ]
  edge [
    source 29
    target 465
  ]
  edge [
    source 29
    target 331
  ]
  edge [
    source 29
    target 461
  ]
  edge [
    source 29
    target 466
  ]
  edge [
    source 29
    target 475
  ]
  edge [
    source 29
    target 473
  ]
  edge [
    source 29
    target 469
  ]
  edge [
    source 29
    target 470
  ]
  edge [
    source 29
    target 477
  ]
  edge [
    source 29
    target 479
  ]
  edge [
    source 29
    target 1307
  ]
  edge [
    source 29
    target 1308
  ]
  edge [
    source 29
    target 1309
  ]
  edge [
    source 29
    target 164
  ]
  edge [
    source 29
    target 1310
  ]
  edge [
    source 29
    target 1311
  ]
  edge [
    source 29
    target 476
  ]
  edge [
    source 29
    target 1312
  ]
  edge [
    source 29
    target 1313
  ]
  edge [
    source 29
    target 1314
  ]
  edge [
    source 29
    target 83
  ]
  edge [
    source 29
    target 1315
  ]
  edge [
    source 29
    target 1316
  ]
  edge [
    source 29
    target 309
  ]
  edge [
    source 29
    target 1317
  ]
  edge [
    source 29
    target 1318
  ]
  edge [
    source 29
    target 1319
  ]
  edge [
    source 29
    target 1320
  ]
  edge [
    source 29
    target 538
  ]
  edge [
    source 29
    target 453
  ]
  edge [
    source 29
    target 501
  ]
  edge [
    source 29
    target 1321
  ]
  edge [
    source 29
    target 1322
  ]
  edge [
    source 29
    target 347
  ]
  edge [
    source 29
    target 454
  ]
  edge [
    source 29
    target 497
  ]
  edge [
    source 29
    target 127
  ]
  edge [
    source 29
    target 130
  ]
  edge [
    source 29
    target 100
  ]
  edge [
    source 29
    target 1323
  ]
  edge [
    source 29
    target 1324
  ]
  edge [
    source 29
    target 299
  ]
  edge [
    source 29
    target 1325
  ]
  edge [
    source 29
    target 301
  ]
  edge [
    source 29
    target 1326
  ]
  edge [
    source 29
    target 1327
  ]
  edge [
    source 29
    target 1328
  ]
  edge [
    source 29
    target 1329
  ]
  edge [
    source 29
    target 1330
  ]
  edge [
    source 29
    target 1331
  ]
  edge [
    source 29
    target 1332
  ]
  edge [
    source 29
    target 1333
  ]
  edge [
    source 29
    target 1334
  ]
  edge [
    source 29
    target 855
  ]
  edge [
    source 29
    target 1335
  ]
  edge [
    source 29
    target 321
  ]
  edge [
    source 29
    target 1336
  ]
  edge [
    source 29
    target 1337
  ]
  edge [
    source 29
    target 108
  ]
  edge [
    source 29
    target 349
  ]
  edge [
    source 29
    target 1338
  ]
  edge [
    source 29
    target 294
  ]
  edge [
    source 29
    target 295
  ]
  edge [
    source 29
    target 296
  ]
  edge [
    source 29
    target 297
  ]
  edge [
    source 29
    target 298
  ]
  edge [
    source 29
    target 300
  ]
  edge [
    source 29
    target 302
  ]
  edge [
    source 29
    target 303
  ]
  edge [
    source 29
    target 304
  ]
  edge [
    source 29
    target 305
  ]
  edge [
    source 29
    target 306
  ]
  edge [
    source 29
    target 307
  ]
  edge [
    source 29
    target 308
  ]
  edge [
    source 29
    target 310
  ]
  edge [
    source 29
    target 311
  ]
  edge [
    source 29
    target 312
  ]
  edge [
    source 29
    target 313
  ]
  edge [
    source 29
    target 314
  ]
  edge [
    source 29
    target 124
  ]
  edge [
    source 29
    target 315
  ]
  edge [
    source 29
    target 316
  ]
  edge [
    source 29
    target 317
  ]
  edge [
    source 29
    target 318
  ]
  edge [
    source 29
    target 319
  ]
  edge [
    source 29
    target 320
  ]
  edge [
    source 29
    target 322
  ]
  edge [
    source 29
    target 323
  ]
  edge [
    source 29
    target 324
  ]
  edge [
    source 29
    target 325
  ]
  edge [
    source 29
    target 326
  ]
  edge [
    source 29
    target 1339
  ]
  edge [
    source 29
    target 527
  ]
  edge [
    source 29
    target 1340
  ]
  edge [
    source 29
    target 1341
  ]
  edge [
    source 29
    target 1342
  ]
  edge [
    source 29
    target 1343
  ]
  edge [
    source 29
    target 1344
  ]
  edge [
    source 29
    target 1345
  ]
  edge [
    source 29
    target 1346
  ]
  edge [
    source 29
    target 1347
  ]
  edge [
    source 29
    target 1348
  ]
]
