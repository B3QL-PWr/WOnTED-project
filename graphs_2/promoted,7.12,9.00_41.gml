graph [
  node [
    id 0
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kampania"
    origin "text"
  ]
  node [
    id 2
    label "rosyjski"
    origin "text"
  ]
  node [
    id 3
    label "rok"
    origin "text"
  ]
  node [
    id 4
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 5
    label "wspomnie&#263;"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 7
    label "bitwa"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 10
    label "miejsce"
    origin "text"
  ]
  node [
    id 11
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 12
    label "pod"
    origin "text"
  ]
  node [
    id 13
    label "borodino"
    origin "text"
  ]
  node [
    id 14
    label "gaworzy&#263;"
  ]
  node [
    id 15
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 16
    label "remark"
  ]
  node [
    id 17
    label "rozmawia&#263;"
  ]
  node [
    id 18
    label "wyra&#380;a&#263;"
  ]
  node [
    id 19
    label "umie&#263;"
  ]
  node [
    id 20
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 21
    label "dziama&#263;"
  ]
  node [
    id 22
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 23
    label "formu&#322;owa&#263;"
  ]
  node [
    id 24
    label "dysfonia"
  ]
  node [
    id 25
    label "express"
  ]
  node [
    id 26
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 27
    label "talk"
  ]
  node [
    id 28
    label "u&#380;ywa&#263;"
  ]
  node [
    id 29
    label "prawi&#263;"
  ]
  node [
    id 30
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 31
    label "powiada&#263;"
  ]
  node [
    id 32
    label "tell"
  ]
  node [
    id 33
    label "chew_the_fat"
  ]
  node [
    id 34
    label "say"
  ]
  node [
    id 35
    label "j&#281;zyk"
  ]
  node [
    id 36
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 37
    label "informowa&#263;"
  ]
  node [
    id 38
    label "wydobywa&#263;"
  ]
  node [
    id 39
    label "okre&#347;la&#263;"
  ]
  node [
    id 40
    label "korzysta&#263;"
  ]
  node [
    id 41
    label "distribute"
  ]
  node [
    id 42
    label "give"
  ]
  node [
    id 43
    label "bash"
  ]
  node [
    id 44
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 45
    label "doznawa&#263;"
  ]
  node [
    id 46
    label "decydowa&#263;"
  ]
  node [
    id 47
    label "signify"
  ]
  node [
    id 48
    label "style"
  ]
  node [
    id 49
    label "powodowa&#263;"
  ]
  node [
    id 50
    label "komunikowa&#263;"
  ]
  node [
    id 51
    label "inform"
  ]
  node [
    id 52
    label "znaczy&#263;"
  ]
  node [
    id 53
    label "give_voice"
  ]
  node [
    id 54
    label "oznacza&#263;"
  ]
  node [
    id 55
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 56
    label "represent"
  ]
  node [
    id 57
    label "convey"
  ]
  node [
    id 58
    label "arouse"
  ]
  node [
    id 59
    label "robi&#263;"
  ]
  node [
    id 60
    label "determine"
  ]
  node [
    id 61
    label "work"
  ]
  node [
    id 62
    label "reakcja_chemiczna"
  ]
  node [
    id 63
    label "uwydatnia&#263;"
  ]
  node [
    id 64
    label "eksploatowa&#263;"
  ]
  node [
    id 65
    label "uzyskiwa&#263;"
  ]
  node [
    id 66
    label "wydostawa&#263;"
  ]
  node [
    id 67
    label "wyjmowa&#263;"
  ]
  node [
    id 68
    label "train"
  ]
  node [
    id 69
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 70
    label "wydawa&#263;"
  ]
  node [
    id 71
    label "dobywa&#263;"
  ]
  node [
    id 72
    label "ocala&#263;"
  ]
  node [
    id 73
    label "excavate"
  ]
  node [
    id 74
    label "g&#243;rnictwo"
  ]
  node [
    id 75
    label "raise"
  ]
  node [
    id 76
    label "wiedzie&#263;"
  ]
  node [
    id 77
    label "can"
  ]
  node [
    id 78
    label "m&#243;c"
  ]
  node [
    id 79
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 80
    label "rozumie&#263;"
  ]
  node [
    id 81
    label "szczeka&#263;"
  ]
  node [
    id 82
    label "funkcjonowa&#263;"
  ]
  node [
    id 83
    label "mawia&#263;"
  ]
  node [
    id 84
    label "opowiada&#263;"
  ]
  node [
    id 85
    label "chatter"
  ]
  node [
    id 86
    label "niemowl&#281;"
  ]
  node [
    id 87
    label "kosmetyk"
  ]
  node [
    id 88
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 89
    label "stanowisko_archeologiczne"
  ]
  node [
    id 90
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 91
    label "artykulator"
  ]
  node [
    id 92
    label "kod"
  ]
  node [
    id 93
    label "kawa&#322;ek"
  ]
  node [
    id 94
    label "przedmiot"
  ]
  node [
    id 95
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 96
    label "gramatyka"
  ]
  node [
    id 97
    label "stylik"
  ]
  node [
    id 98
    label "przet&#322;umaczenie"
  ]
  node [
    id 99
    label "formalizowanie"
  ]
  node [
    id 100
    label "ssanie"
  ]
  node [
    id 101
    label "ssa&#263;"
  ]
  node [
    id 102
    label "language"
  ]
  node [
    id 103
    label "liza&#263;"
  ]
  node [
    id 104
    label "napisa&#263;"
  ]
  node [
    id 105
    label "konsonantyzm"
  ]
  node [
    id 106
    label "wokalizm"
  ]
  node [
    id 107
    label "pisa&#263;"
  ]
  node [
    id 108
    label "fonetyka"
  ]
  node [
    id 109
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 110
    label "jeniec"
  ]
  node [
    id 111
    label "but"
  ]
  node [
    id 112
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 113
    label "po_koroniarsku"
  ]
  node [
    id 114
    label "kultura_duchowa"
  ]
  node [
    id 115
    label "t&#322;umaczenie"
  ]
  node [
    id 116
    label "m&#243;wienie"
  ]
  node [
    id 117
    label "pype&#263;"
  ]
  node [
    id 118
    label "lizanie"
  ]
  node [
    id 119
    label "pismo"
  ]
  node [
    id 120
    label "formalizowa&#263;"
  ]
  node [
    id 121
    label "organ"
  ]
  node [
    id 122
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 123
    label "rozumienie"
  ]
  node [
    id 124
    label "makroglosja"
  ]
  node [
    id 125
    label "jama_ustna"
  ]
  node [
    id 126
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 127
    label "formacja_geologiczna"
  ]
  node [
    id 128
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 129
    label "natural_language"
  ]
  node [
    id 130
    label "s&#322;ownictwo"
  ]
  node [
    id 131
    label "urz&#261;dzenie"
  ]
  node [
    id 132
    label "dysphonia"
  ]
  node [
    id 133
    label "dysleksja"
  ]
  node [
    id 134
    label "dzia&#322;anie"
  ]
  node [
    id 135
    label "campaign"
  ]
  node [
    id 136
    label "wydarzenie"
  ]
  node [
    id 137
    label "akcja"
  ]
  node [
    id 138
    label "Kampania_Wrze&#347;niowa"
  ]
  node [
    id 139
    label "przebiec"
  ]
  node [
    id 140
    label "charakter"
  ]
  node [
    id 141
    label "czynno&#347;&#263;"
  ]
  node [
    id 142
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 143
    label "motyw"
  ]
  node [
    id 144
    label "przebiegni&#281;cie"
  ]
  node [
    id 145
    label "fabu&#322;a"
  ]
  node [
    id 146
    label "dywidenda"
  ]
  node [
    id 147
    label "przebieg"
  ]
  node [
    id 148
    label "operacja"
  ]
  node [
    id 149
    label "zagrywka"
  ]
  node [
    id 150
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 151
    label "udzia&#322;"
  ]
  node [
    id 152
    label "commotion"
  ]
  node [
    id 153
    label "occupation"
  ]
  node [
    id 154
    label "gra"
  ]
  node [
    id 155
    label "jazda"
  ]
  node [
    id 156
    label "czyn"
  ]
  node [
    id 157
    label "stock"
  ]
  node [
    id 158
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 159
    label "w&#281;ze&#322;"
  ]
  node [
    id 160
    label "wysoko&#347;&#263;"
  ]
  node [
    id 161
    label "instrument_strunowy"
  ]
  node [
    id 162
    label "infimum"
  ]
  node [
    id 163
    label "powodowanie"
  ]
  node [
    id 164
    label "liczenie"
  ]
  node [
    id 165
    label "cz&#322;owiek"
  ]
  node [
    id 166
    label "skutek"
  ]
  node [
    id 167
    label "podzia&#322;anie"
  ]
  node [
    id 168
    label "supremum"
  ]
  node [
    id 169
    label "uruchamianie"
  ]
  node [
    id 170
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 171
    label "jednostka"
  ]
  node [
    id 172
    label "hipnotyzowanie"
  ]
  node [
    id 173
    label "robienie"
  ]
  node [
    id 174
    label "uruchomienie"
  ]
  node [
    id 175
    label "nakr&#281;canie"
  ]
  node [
    id 176
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 177
    label "matematyka"
  ]
  node [
    id 178
    label "tr&#243;jstronny"
  ]
  node [
    id 179
    label "natural_process"
  ]
  node [
    id 180
    label "nakr&#281;cenie"
  ]
  node [
    id 181
    label "zatrzymanie"
  ]
  node [
    id 182
    label "wp&#322;yw"
  ]
  node [
    id 183
    label "rzut"
  ]
  node [
    id 184
    label "podtrzymywanie"
  ]
  node [
    id 185
    label "w&#322;&#261;czanie"
  ]
  node [
    id 186
    label "liczy&#263;"
  ]
  node [
    id 187
    label "operation"
  ]
  node [
    id 188
    label "rezultat"
  ]
  node [
    id 189
    label "dzianie_si&#281;"
  ]
  node [
    id 190
    label "zadzia&#322;anie"
  ]
  node [
    id 191
    label "priorytet"
  ]
  node [
    id 192
    label "bycie"
  ]
  node [
    id 193
    label "kres"
  ]
  node [
    id 194
    label "rozpocz&#281;cie"
  ]
  node [
    id 195
    label "docieranie"
  ]
  node [
    id 196
    label "funkcja"
  ]
  node [
    id 197
    label "czynny"
  ]
  node [
    id 198
    label "impact"
  ]
  node [
    id 199
    label "oferta"
  ]
  node [
    id 200
    label "zako&#324;czenie"
  ]
  node [
    id 201
    label "act"
  ]
  node [
    id 202
    label "wdzieranie_si&#281;"
  ]
  node [
    id 203
    label "w&#322;&#261;czenie"
  ]
  node [
    id 204
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 205
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 206
    label "kacapski"
  ]
  node [
    id 207
    label "po_rosyjsku"
  ]
  node [
    id 208
    label "wielkoruski"
  ]
  node [
    id 209
    label "Russian"
  ]
  node [
    id 210
    label "rusek"
  ]
  node [
    id 211
    label "wschodnioeuropejski"
  ]
  node [
    id 212
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 213
    label "poga&#324;ski"
  ]
  node [
    id 214
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 215
    label "topielec"
  ]
  node [
    id 216
    label "europejski"
  ]
  node [
    id 217
    label "j&#281;zyk_rosyjski"
  ]
  node [
    id 218
    label "po_kacapsku"
  ]
  node [
    id 219
    label "ruski"
  ]
  node [
    id 220
    label "imperialny"
  ]
  node [
    id 221
    label "po_wielkorusku"
  ]
  node [
    id 222
    label "p&#243;&#322;rocze"
  ]
  node [
    id 223
    label "martwy_sezon"
  ]
  node [
    id 224
    label "kalendarz"
  ]
  node [
    id 225
    label "cykl_astronomiczny"
  ]
  node [
    id 226
    label "lata"
  ]
  node [
    id 227
    label "pora_roku"
  ]
  node [
    id 228
    label "stulecie"
  ]
  node [
    id 229
    label "kurs"
  ]
  node [
    id 230
    label "czas"
  ]
  node [
    id 231
    label "jubileusz"
  ]
  node [
    id 232
    label "grupa"
  ]
  node [
    id 233
    label "kwarta&#322;"
  ]
  node [
    id 234
    label "miesi&#261;c"
  ]
  node [
    id 235
    label "summer"
  ]
  node [
    id 236
    label "odm&#322;adzanie"
  ]
  node [
    id 237
    label "liga"
  ]
  node [
    id 238
    label "jednostka_systematyczna"
  ]
  node [
    id 239
    label "asymilowanie"
  ]
  node [
    id 240
    label "gromada"
  ]
  node [
    id 241
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 242
    label "asymilowa&#263;"
  ]
  node [
    id 243
    label "egzemplarz"
  ]
  node [
    id 244
    label "Entuzjastki"
  ]
  node [
    id 245
    label "zbi&#243;r"
  ]
  node [
    id 246
    label "kompozycja"
  ]
  node [
    id 247
    label "Terranie"
  ]
  node [
    id 248
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 249
    label "category"
  ]
  node [
    id 250
    label "pakiet_klimatyczny"
  ]
  node [
    id 251
    label "oddzia&#322;"
  ]
  node [
    id 252
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 253
    label "cz&#261;steczka"
  ]
  node [
    id 254
    label "stage_set"
  ]
  node [
    id 255
    label "type"
  ]
  node [
    id 256
    label "specgrupa"
  ]
  node [
    id 257
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 258
    label "&#346;wietliki"
  ]
  node [
    id 259
    label "odm&#322;odzenie"
  ]
  node [
    id 260
    label "Eurogrupa"
  ]
  node [
    id 261
    label "odm&#322;adza&#263;"
  ]
  node [
    id 262
    label "harcerze_starsi"
  ]
  node [
    id 263
    label "poprzedzanie"
  ]
  node [
    id 264
    label "czasoprzestrze&#324;"
  ]
  node [
    id 265
    label "laba"
  ]
  node [
    id 266
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 267
    label "chronometria"
  ]
  node [
    id 268
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 269
    label "rachuba_czasu"
  ]
  node [
    id 270
    label "przep&#322;ywanie"
  ]
  node [
    id 271
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 272
    label "czasokres"
  ]
  node [
    id 273
    label "odczyt"
  ]
  node [
    id 274
    label "chwila"
  ]
  node [
    id 275
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 276
    label "dzieje"
  ]
  node [
    id 277
    label "kategoria_gramatyczna"
  ]
  node [
    id 278
    label "poprzedzenie"
  ]
  node [
    id 279
    label "trawienie"
  ]
  node [
    id 280
    label "pochodzi&#263;"
  ]
  node [
    id 281
    label "period"
  ]
  node [
    id 282
    label "okres_czasu"
  ]
  node [
    id 283
    label "poprzedza&#263;"
  ]
  node [
    id 284
    label "schy&#322;ek"
  ]
  node [
    id 285
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 286
    label "odwlekanie_si&#281;"
  ]
  node [
    id 287
    label "zegar"
  ]
  node [
    id 288
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 289
    label "czwarty_wymiar"
  ]
  node [
    id 290
    label "pochodzenie"
  ]
  node [
    id 291
    label "koniugacja"
  ]
  node [
    id 292
    label "Zeitgeist"
  ]
  node [
    id 293
    label "trawi&#263;"
  ]
  node [
    id 294
    label "pogoda"
  ]
  node [
    id 295
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 296
    label "poprzedzi&#263;"
  ]
  node [
    id 297
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 298
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 299
    label "time_period"
  ]
  node [
    id 300
    label "term"
  ]
  node [
    id 301
    label "rok_akademicki"
  ]
  node [
    id 302
    label "rok_szkolny"
  ]
  node [
    id 303
    label "semester"
  ]
  node [
    id 304
    label "anniwersarz"
  ]
  node [
    id 305
    label "rocznica"
  ]
  node [
    id 306
    label "obszar"
  ]
  node [
    id 307
    label "tydzie&#324;"
  ]
  node [
    id 308
    label "miech"
  ]
  node [
    id 309
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 310
    label "kalendy"
  ]
  node [
    id 311
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 312
    label "long_time"
  ]
  node [
    id 313
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 314
    label "almanac"
  ]
  node [
    id 315
    label "rozk&#322;ad"
  ]
  node [
    id 316
    label "wydawnictwo"
  ]
  node [
    id 317
    label "Juliusz_Cezar"
  ]
  node [
    id 318
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 319
    label "zwy&#380;kowanie"
  ]
  node [
    id 320
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 321
    label "zaj&#281;cia"
  ]
  node [
    id 322
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 323
    label "trasa"
  ]
  node [
    id 324
    label "przeorientowywanie"
  ]
  node [
    id 325
    label "przejazd"
  ]
  node [
    id 326
    label "kierunek"
  ]
  node [
    id 327
    label "przeorientowywa&#263;"
  ]
  node [
    id 328
    label "nauka"
  ]
  node [
    id 329
    label "przeorientowanie"
  ]
  node [
    id 330
    label "klasa"
  ]
  node [
    id 331
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 332
    label "przeorientowa&#263;"
  ]
  node [
    id 333
    label "manner"
  ]
  node [
    id 334
    label "course"
  ]
  node [
    id 335
    label "passage"
  ]
  node [
    id 336
    label "zni&#380;kowanie"
  ]
  node [
    id 337
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 338
    label "seria"
  ]
  node [
    id 339
    label "stawka"
  ]
  node [
    id 340
    label "way"
  ]
  node [
    id 341
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 342
    label "deprecjacja"
  ]
  node [
    id 343
    label "cedu&#322;a"
  ]
  node [
    id 344
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 345
    label "drive"
  ]
  node [
    id 346
    label "bearing"
  ]
  node [
    id 347
    label "Lira"
  ]
  node [
    id 348
    label "model"
  ]
  node [
    id 349
    label "narz&#281;dzie"
  ]
  node [
    id 350
    label "tryb"
  ]
  node [
    id 351
    label "nature"
  ]
  node [
    id 352
    label "series"
  ]
  node [
    id 353
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 354
    label "uprawianie"
  ]
  node [
    id 355
    label "praca_rolnicza"
  ]
  node [
    id 356
    label "collection"
  ]
  node [
    id 357
    label "dane"
  ]
  node [
    id 358
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 359
    label "poj&#281;cie"
  ]
  node [
    id 360
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 361
    label "sum"
  ]
  node [
    id 362
    label "gathering"
  ]
  node [
    id 363
    label "album"
  ]
  node [
    id 364
    label "&#347;rodek"
  ]
  node [
    id 365
    label "niezb&#281;dnik"
  ]
  node [
    id 366
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 367
    label "tylec"
  ]
  node [
    id 368
    label "ko&#322;o"
  ]
  node [
    id 369
    label "modalno&#347;&#263;"
  ]
  node [
    id 370
    label "z&#261;b"
  ]
  node [
    id 371
    label "cecha"
  ]
  node [
    id 372
    label "skala"
  ]
  node [
    id 373
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 374
    label "prezenter"
  ]
  node [
    id 375
    label "typ"
  ]
  node [
    id 376
    label "mildew"
  ]
  node [
    id 377
    label "zi&#243;&#322;ko"
  ]
  node [
    id 378
    label "motif"
  ]
  node [
    id 379
    label "pozowanie"
  ]
  node [
    id 380
    label "ideal"
  ]
  node [
    id 381
    label "wz&#243;r"
  ]
  node [
    id 382
    label "matryca"
  ]
  node [
    id 383
    label "adaptation"
  ]
  node [
    id 384
    label "ruch"
  ]
  node [
    id 385
    label "pozowa&#263;"
  ]
  node [
    id 386
    label "imitacja"
  ]
  node [
    id 387
    label "orygina&#322;"
  ]
  node [
    id 388
    label "facet"
  ]
  node [
    id 389
    label "miniatura"
  ]
  node [
    id 390
    label "mention"
  ]
  node [
    id 391
    label "doda&#263;"
  ]
  node [
    id 392
    label "hint"
  ]
  node [
    id 393
    label "pomy&#347;le&#263;"
  ]
  node [
    id 394
    label "set"
  ]
  node [
    id 395
    label "nada&#263;"
  ]
  node [
    id 396
    label "policzy&#263;"
  ]
  node [
    id 397
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 398
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 399
    label "complete"
  ]
  node [
    id 400
    label "rozwa&#380;y&#263;"
  ]
  node [
    id 401
    label "oceni&#263;"
  ]
  node [
    id 402
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 403
    label "zrobi&#263;"
  ]
  node [
    id 404
    label "uzna&#263;"
  ]
  node [
    id 405
    label "porobi&#263;"
  ]
  node [
    id 406
    label "wymy&#347;li&#263;"
  ]
  node [
    id 407
    label "think"
  ]
  node [
    id 408
    label "doros&#322;y"
  ]
  node [
    id 409
    label "znaczny"
  ]
  node [
    id 410
    label "niema&#322;o"
  ]
  node [
    id 411
    label "wiele"
  ]
  node [
    id 412
    label "rozwini&#281;ty"
  ]
  node [
    id 413
    label "dorodny"
  ]
  node [
    id 414
    label "wa&#380;ny"
  ]
  node [
    id 415
    label "prawdziwy"
  ]
  node [
    id 416
    label "du&#380;o"
  ]
  node [
    id 417
    label "&#380;ywny"
  ]
  node [
    id 418
    label "szczery"
  ]
  node [
    id 419
    label "naturalny"
  ]
  node [
    id 420
    label "naprawd&#281;"
  ]
  node [
    id 421
    label "realnie"
  ]
  node [
    id 422
    label "podobny"
  ]
  node [
    id 423
    label "zgodny"
  ]
  node [
    id 424
    label "m&#261;dry"
  ]
  node [
    id 425
    label "prawdziwie"
  ]
  node [
    id 426
    label "znacznie"
  ]
  node [
    id 427
    label "zauwa&#380;alny"
  ]
  node [
    id 428
    label "wynios&#322;y"
  ]
  node [
    id 429
    label "dono&#347;ny"
  ]
  node [
    id 430
    label "silny"
  ]
  node [
    id 431
    label "wa&#380;nie"
  ]
  node [
    id 432
    label "istotnie"
  ]
  node [
    id 433
    label "eksponowany"
  ]
  node [
    id 434
    label "dobry"
  ]
  node [
    id 435
    label "ukszta&#322;towany"
  ]
  node [
    id 436
    label "do&#347;cig&#322;y"
  ]
  node [
    id 437
    label "&#378;ra&#322;y"
  ]
  node [
    id 438
    label "zdr&#243;w"
  ]
  node [
    id 439
    label "dorodnie"
  ]
  node [
    id 440
    label "okaza&#322;y"
  ]
  node [
    id 441
    label "mocno"
  ]
  node [
    id 442
    label "wiela"
  ]
  node [
    id 443
    label "bardzo"
  ]
  node [
    id 444
    label "cz&#281;sto"
  ]
  node [
    id 445
    label "wydoro&#347;lenie"
  ]
  node [
    id 446
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 447
    label "doro&#347;lenie"
  ]
  node [
    id 448
    label "doro&#347;le"
  ]
  node [
    id 449
    label "senior"
  ]
  node [
    id 450
    label "dojrzale"
  ]
  node [
    id 451
    label "wapniak"
  ]
  node [
    id 452
    label "dojrza&#322;y"
  ]
  node [
    id 453
    label "doletni"
  ]
  node [
    id 454
    label "walka"
  ]
  node [
    id 455
    label "action"
  ]
  node [
    id 456
    label "batalista"
  ]
  node [
    id 457
    label "bitwa_pod_Pi&#322;awcami"
  ]
  node [
    id 458
    label "zaj&#347;cie"
  ]
  node [
    id 459
    label "obrona"
  ]
  node [
    id 460
    label "zaatakowanie"
  ]
  node [
    id 461
    label "konfrontacyjny"
  ]
  node [
    id 462
    label "contest"
  ]
  node [
    id 463
    label "sambo"
  ]
  node [
    id 464
    label "rywalizacja"
  ]
  node [
    id 465
    label "trudno&#347;&#263;"
  ]
  node [
    id 466
    label "sp&#243;r"
  ]
  node [
    id 467
    label "wrestle"
  ]
  node [
    id 468
    label "military_action"
  ]
  node [
    id 469
    label "ploy"
  ]
  node [
    id 470
    label "doj&#347;cie"
  ]
  node [
    id 471
    label "skrycie_si&#281;"
  ]
  node [
    id 472
    label "odwiedzenie"
  ]
  node [
    id 473
    label "zakrycie"
  ]
  node [
    id 474
    label "happening"
  ]
  node [
    id 475
    label "porobienie_si&#281;"
  ]
  node [
    id 476
    label "krajobraz"
  ]
  node [
    id 477
    label "zaniesienie"
  ]
  node [
    id 478
    label "przyobleczenie_si&#281;"
  ]
  node [
    id 479
    label "stanie_si&#281;"
  ]
  node [
    id 480
    label "event"
  ]
  node [
    id 481
    label "entrance"
  ]
  node [
    id 482
    label "podej&#347;cie"
  ]
  node [
    id 483
    label "przestanie"
  ]
  node [
    id 484
    label "artysta"
  ]
  node [
    id 485
    label "hide"
  ]
  node [
    id 486
    label "czu&#263;"
  ]
  node [
    id 487
    label "support"
  ]
  node [
    id 488
    label "need"
  ]
  node [
    id 489
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 490
    label "wykonawca"
  ]
  node [
    id 491
    label "interpretator"
  ]
  node [
    id 492
    label "cover"
  ]
  node [
    id 493
    label "postrzega&#263;"
  ]
  node [
    id 494
    label "przewidywa&#263;"
  ]
  node [
    id 495
    label "by&#263;"
  ]
  node [
    id 496
    label "smell"
  ]
  node [
    id 497
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 498
    label "uczuwa&#263;"
  ]
  node [
    id 499
    label "spirit"
  ]
  node [
    id 500
    label "anticipate"
  ]
  node [
    id 501
    label "warunek_lokalowy"
  ]
  node [
    id 502
    label "plac"
  ]
  node [
    id 503
    label "location"
  ]
  node [
    id 504
    label "uwaga"
  ]
  node [
    id 505
    label "przestrze&#324;"
  ]
  node [
    id 506
    label "status"
  ]
  node [
    id 507
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 508
    label "cia&#322;o"
  ]
  node [
    id 509
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 510
    label "praca"
  ]
  node [
    id 511
    label "rz&#261;d"
  ]
  node [
    id 512
    label "charakterystyka"
  ]
  node [
    id 513
    label "m&#322;ot"
  ]
  node [
    id 514
    label "znak"
  ]
  node [
    id 515
    label "drzewo"
  ]
  node [
    id 516
    label "pr&#243;ba"
  ]
  node [
    id 517
    label "attribute"
  ]
  node [
    id 518
    label "marka"
  ]
  node [
    id 519
    label "Rzym_Zachodni"
  ]
  node [
    id 520
    label "whole"
  ]
  node [
    id 521
    label "ilo&#347;&#263;"
  ]
  node [
    id 522
    label "element"
  ]
  node [
    id 523
    label "Rzym_Wschodni"
  ]
  node [
    id 524
    label "wypowied&#378;"
  ]
  node [
    id 525
    label "stan"
  ]
  node [
    id 526
    label "nagana"
  ]
  node [
    id 527
    label "tekst"
  ]
  node [
    id 528
    label "upomnienie"
  ]
  node [
    id 529
    label "dzienniczek"
  ]
  node [
    id 530
    label "wzgl&#261;d"
  ]
  node [
    id 531
    label "gossip"
  ]
  node [
    id 532
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 533
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 534
    label "najem"
  ]
  node [
    id 535
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 536
    label "zak&#322;ad"
  ]
  node [
    id 537
    label "stosunek_pracy"
  ]
  node [
    id 538
    label "benedykty&#324;ski"
  ]
  node [
    id 539
    label "poda&#380;_pracy"
  ]
  node [
    id 540
    label "pracowanie"
  ]
  node [
    id 541
    label "tyrka"
  ]
  node [
    id 542
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 543
    label "wytw&#243;r"
  ]
  node [
    id 544
    label "zaw&#243;d"
  ]
  node [
    id 545
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 546
    label "tynkarski"
  ]
  node [
    id 547
    label "pracowa&#263;"
  ]
  node [
    id 548
    label "zmiana"
  ]
  node [
    id 549
    label "czynnik_produkcji"
  ]
  node [
    id 550
    label "zobowi&#261;zanie"
  ]
  node [
    id 551
    label "kierownictwo"
  ]
  node [
    id 552
    label "siedziba"
  ]
  node [
    id 553
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 554
    label "rozdzielanie"
  ]
  node [
    id 555
    label "bezbrze&#380;e"
  ]
  node [
    id 556
    label "punkt"
  ]
  node [
    id 557
    label "niezmierzony"
  ]
  node [
    id 558
    label "przedzielenie"
  ]
  node [
    id 559
    label "nielito&#347;ciwy"
  ]
  node [
    id 560
    label "rozdziela&#263;"
  ]
  node [
    id 561
    label "oktant"
  ]
  node [
    id 562
    label "przedzieli&#263;"
  ]
  node [
    id 563
    label "przestw&#243;r"
  ]
  node [
    id 564
    label "condition"
  ]
  node [
    id 565
    label "awansowa&#263;"
  ]
  node [
    id 566
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 567
    label "znaczenie"
  ]
  node [
    id 568
    label "awans"
  ]
  node [
    id 569
    label "podmiotowo"
  ]
  node [
    id 570
    label "awansowanie"
  ]
  node [
    id 571
    label "sytuacja"
  ]
  node [
    id 572
    label "time"
  ]
  node [
    id 573
    label "rozmiar"
  ]
  node [
    id 574
    label "liczba"
  ]
  node [
    id 575
    label "circumference"
  ]
  node [
    id 576
    label "leksem"
  ]
  node [
    id 577
    label "cyrkumferencja"
  ]
  node [
    id 578
    label "strona"
  ]
  node [
    id 579
    label "ekshumowanie"
  ]
  node [
    id 580
    label "jednostka_organizacyjna"
  ]
  node [
    id 581
    label "p&#322;aszczyzna"
  ]
  node [
    id 582
    label "odwadnia&#263;"
  ]
  node [
    id 583
    label "zabalsamowanie"
  ]
  node [
    id 584
    label "zesp&#243;&#322;"
  ]
  node [
    id 585
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 586
    label "odwodni&#263;"
  ]
  node [
    id 587
    label "sk&#243;ra"
  ]
  node [
    id 588
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 589
    label "staw"
  ]
  node [
    id 590
    label "ow&#322;osienie"
  ]
  node [
    id 591
    label "mi&#281;so"
  ]
  node [
    id 592
    label "zabalsamowa&#263;"
  ]
  node [
    id 593
    label "Izba_Konsyliarska"
  ]
  node [
    id 594
    label "unerwienie"
  ]
  node [
    id 595
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 596
    label "kremacja"
  ]
  node [
    id 597
    label "biorytm"
  ]
  node [
    id 598
    label "sekcja"
  ]
  node [
    id 599
    label "istota_&#380;ywa"
  ]
  node [
    id 600
    label "otworzy&#263;"
  ]
  node [
    id 601
    label "otwiera&#263;"
  ]
  node [
    id 602
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 603
    label "otworzenie"
  ]
  node [
    id 604
    label "materia"
  ]
  node [
    id 605
    label "pochowanie"
  ]
  node [
    id 606
    label "otwieranie"
  ]
  node [
    id 607
    label "ty&#322;"
  ]
  node [
    id 608
    label "szkielet"
  ]
  node [
    id 609
    label "tanatoplastyk"
  ]
  node [
    id 610
    label "odwadnianie"
  ]
  node [
    id 611
    label "Komitet_Region&#243;w"
  ]
  node [
    id 612
    label "odwodnienie"
  ]
  node [
    id 613
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 614
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 615
    label "nieumar&#322;y"
  ]
  node [
    id 616
    label "pochowa&#263;"
  ]
  node [
    id 617
    label "balsamowa&#263;"
  ]
  node [
    id 618
    label "tanatoplastyka"
  ]
  node [
    id 619
    label "temperatura"
  ]
  node [
    id 620
    label "ekshumowa&#263;"
  ]
  node [
    id 621
    label "balsamowanie"
  ]
  node [
    id 622
    label "uk&#322;ad"
  ]
  node [
    id 623
    label "prz&#243;d"
  ]
  node [
    id 624
    label "l&#281;d&#378;wie"
  ]
  node [
    id 625
    label "cz&#322;onek"
  ]
  node [
    id 626
    label "pogrzeb"
  ]
  node [
    id 627
    label "&#321;ubianka"
  ]
  node [
    id 628
    label "area"
  ]
  node [
    id 629
    label "Majdan"
  ]
  node [
    id 630
    label "pole_bitwy"
  ]
  node [
    id 631
    label "stoisko"
  ]
  node [
    id 632
    label "pierzeja"
  ]
  node [
    id 633
    label "obiekt_handlowy"
  ]
  node [
    id 634
    label "zgromadzenie"
  ]
  node [
    id 635
    label "miasto"
  ]
  node [
    id 636
    label "targowica"
  ]
  node [
    id 637
    label "kram"
  ]
  node [
    id 638
    label "przybli&#380;enie"
  ]
  node [
    id 639
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 640
    label "kategoria"
  ]
  node [
    id 641
    label "szpaler"
  ]
  node [
    id 642
    label "lon&#380;a"
  ]
  node [
    id 643
    label "uporz&#261;dkowanie"
  ]
  node [
    id 644
    label "egzekutywa"
  ]
  node [
    id 645
    label "instytucja"
  ]
  node [
    id 646
    label "premier"
  ]
  node [
    id 647
    label "Londyn"
  ]
  node [
    id 648
    label "gabinet_cieni"
  ]
  node [
    id 649
    label "number"
  ]
  node [
    id 650
    label "Konsulat"
  ]
  node [
    id 651
    label "tract"
  ]
  node [
    id 652
    label "w&#322;adza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 11
    target 309
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 12
    target 13
  ]
]
