graph [
  node [
    id 0
    label "zdoby&#263;"
    origin "text"
  ]
  node [
    id 1
    label "flaga"
    origin "text"
  ]
  node [
    id 2
    label "realize"
  ]
  node [
    id 3
    label "stage"
  ]
  node [
    id 4
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 5
    label "uzyska&#263;"
  ]
  node [
    id 6
    label "dosta&#263;"
  ]
  node [
    id 7
    label "manipulate"
  ]
  node [
    id 8
    label "uzale&#380;ni&#263;"
  ]
  node [
    id 9
    label "hyponym"
  ]
  node [
    id 10
    label "dostosowa&#263;"
  ]
  node [
    id 11
    label "wytworzy&#263;"
  ]
  node [
    id 12
    label "zrobi&#263;"
  ]
  node [
    id 13
    label "give_birth"
  ]
  node [
    id 14
    label "make"
  ]
  node [
    id 15
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 16
    label "promocja"
  ]
  node [
    id 17
    label "kupi&#263;"
  ]
  node [
    id 18
    label "naby&#263;"
  ]
  node [
    id 19
    label "nabawianie_si&#281;"
  ]
  node [
    id 20
    label "wysta&#263;"
  ]
  node [
    id 21
    label "schorzenie"
  ]
  node [
    id 22
    label "range"
  ]
  node [
    id 23
    label "get"
  ]
  node [
    id 24
    label "doczeka&#263;"
  ]
  node [
    id 25
    label "wystarczy&#263;"
  ]
  node [
    id 26
    label "zapanowa&#263;"
  ]
  node [
    id 27
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 28
    label "develop"
  ]
  node [
    id 29
    label "zwiastun"
  ]
  node [
    id 30
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 31
    label "catch"
  ]
  node [
    id 32
    label "obskoczy&#263;"
  ]
  node [
    id 33
    label "wzi&#261;&#263;"
  ]
  node [
    id 34
    label "nabawienie_si&#281;"
  ]
  node [
    id 35
    label "flag"
  ]
  node [
    id 36
    label "oznaka"
  ]
  node [
    id 37
    label "transparent"
  ]
  node [
    id 38
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 39
    label "signal"
  ]
  node [
    id 40
    label "implikowa&#263;"
  ]
  node [
    id 41
    label "fakt"
  ]
  node [
    id 42
    label "symbol"
  ]
  node [
    id 43
    label "tablica"
  ]
  node [
    id 44
    label "przedmiot"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
]
