graph [
  node [
    id 0
    label "nietypowy"
    origin "text"
  ]
  node [
    id 1
    label "zachowanie"
    origin "text"
  ]
  node [
    id 2
    label "kur"
    origin "text"
  ]
  node [
    id 3
    label "inny"
  ]
  node [
    id 4
    label "nietypowo"
  ]
  node [
    id 5
    label "kolejny"
  ]
  node [
    id 6
    label "osobno"
  ]
  node [
    id 7
    label "r&#243;&#380;ny"
  ]
  node [
    id 8
    label "inszy"
  ]
  node [
    id 9
    label "inaczej"
  ]
  node [
    id 10
    label "atypically"
  ]
  node [
    id 11
    label "odmiennie"
  ]
  node [
    id 12
    label "niezwykle"
  ]
  node [
    id 13
    label "reakcja"
  ]
  node [
    id 14
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 15
    label "tajemnica"
  ]
  node [
    id 16
    label "struktura"
  ]
  node [
    id 17
    label "spos&#243;b"
  ]
  node [
    id 18
    label "wydarzenie"
  ]
  node [
    id 19
    label "pochowanie"
  ]
  node [
    id 20
    label "zdyscyplinowanie"
  ]
  node [
    id 21
    label "post&#261;pienie"
  ]
  node [
    id 22
    label "post"
  ]
  node [
    id 23
    label "bearing"
  ]
  node [
    id 24
    label "zwierz&#281;"
  ]
  node [
    id 25
    label "behawior"
  ]
  node [
    id 26
    label "observation"
  ]
  node [
    id 27
    label "dieta"
  ]
  node [
    id 28
    label "podtrzymanie"
  ]
  node [
    id 29
    label "etolog"
  ]
  node [
    id 30
    label "przechowanie"
  ]
  node [
    id 31
    label "zrobienie"
  ]
  node [
    id 32
    label "model"
  ]
  node [
    id 33
    label "narz&#281;dzie"
  ]
  node [
    id 34
    label "zbi&#243;r"
  ]
  node [
    id 35
    label "tryb"
  ]
  node [
    id 36
    label "nature"
  ]
  node [
    id 37
    label "p&#243;j&#347;cie"
  ]
  node [
    id 38
    label "behavior"
  ]
  node [
    id 39
    label "comfort"
  ]
  node [
    id 40
    label "pocieszenie"
  ]
  node [
    id 41
    label "uniesienie"
  ]
  node [
    id 42
    label "sustainability"
  ]
  node [
    id 43
    label "support"
  ]
  node [
    id 44
    label "utrzymanie"
  ]
  node [
    id 45
    label "czynno&#347;&#263;"
  ]
  node [
    id 46
    label "narobienie"
  ]
  node [
    id 47
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 48
    label "creation"
  ]
  node [
    id 49
    label "porobienie"
  ]
  node [
    id 50
    label "ukrycie"
  ]
  node [
    id 51
    label "retention"
  ]
  node [
    id 52
    label "preserve"
  ]
  node [
    id 53
    label "zmagazynowanie"
  ]
  node [
    id 54
    label "uchronienie"
  ]
  node [
    id 55
    label "przebiec"
  ]
  node [
    id 56
    label "charakter"
  ]
  node [
    id 57
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 58
    label "motyw"
  ]
  node [
    id 59
    label "przebiegni&#281;cie"
  ]
  node [
    id 60
    label "fabu&#322;a"
  ]
  node [
    id 61
    label "react"
  ]
  node [
    id 62
    label "reaction"
  ]
  node [
    id 63
    label "organizm"
  ]
  node [
    id 64
    label "rozmowa"
  ]
  node [
    id 65
    label "response"
  ]
  node [
    id 66
    label "rezultat"
  ]
  node [
    id 67
    label "respondent"
  ]
  node [
    id 68
    label "degenerat"
  ]
  node [
    id 69
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 70
    label "cz&#322;owiek"
  ]
  node [
    id 71
    label "zwyrol"
  ]
  node [
    id 72
    label "czerniak"
  ]
  node [
    id 73
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 74
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 75
    label "paszcza"
  ]
  node [
    id 76
    label "popapraniec"
  ]
  node [
    id 77
    label "skuba&#263;"
  ]
  node [
    id 78
    label "skubanie"
  ]
  node [
    id 79
    label "skubni&#281;cie"
  ]
  node [
    id 80
    label "agresja"
  ]
  node [
    id 81
    label "zwierz&#281;ta"
  ]
  node [
    id 82
    label "fukni&#281;cie"
  ]
  node [
    id 83
    label "farba"
  ]
  node [
    id 84
    label "fukanie"
  ]
  node [
    id 85
    label "istota_&#380;ywa"
  ]
  node [
    id 86
    label "gad"
  ]
  node [
    id 87
    label "siedzie&#263;"
  ]
  node [
    id 88
    label "oswaja&#263;"
  ]
  node [
    id 89
    label "tresowa&#263;"
  ]
  node [
    id 90
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 91
    label "poligamia"
  ]
  node [
    id 92
    label "oz&#243;r"
  ]
  node [
    id 93
    label "skubn&#261;&#263;"
  ]
  node [
    id 94
    label "wios&#322;owa&#263;"
  ]
  node [
    id 95
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 96
    label "le&#380;enie"
  ]
  node [
    id 97
    label "niecz&#322;owiek"
  ]
  node [
    id 98
    label "wios&#322;owanie"
  ]
  node [
    id 99
    label "napasienie_si&#281;"
  ]
  node [
    id 100
    label "wiwarium"
  ]
  node [
    id 101
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 102
    label "animalista"
  ]
  node [
    id 103
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 104
    label "budowa"
  ]
  node [
    id 105
    label "hodowla"
  ]
  node [
    id 106
    label "pasienie_si&#281;"
  ]
  node [
    id 107
    label "sodomita"
  ]
  node [
    id 108
    label "monogamia"
  ]
  node [
    id 109
    label "przyssawka"
  ]
  node [
    id 110
    label "budowa_cia&#322;a"
  ]
  node [
    id 111
    label "okrutnik"
  ]
  node [
    id 112
    label "grzbiet"
  ]
  node [
    id 113
    label "weterynarz"
  ]
  node [
    id 114
    label "&#322;eb"
  ]
  node [
    id 115
    label "wylinka"
  ]
  node [
    id 116
    label "bestia"
  ]
  node [
    id 117
    label "poskramia&#263;"
  ]
  node [
    id 118
    label "fauna"
  ]
  node [
    id 119
    label "treser"
  ]
  node [
    id 120
    label "siedzenie"
  ]
  node [
    id 121
    label "le&#380;e&#263;"
  ]
  node [
    id 122
    label "zoopsycholog"
  ]
  node [
    id 123
    label "zoolog"
  ]
  node [
    id 124
    label "wypaplanie"
  ]
  node [
    id 125
    label "enigmat"
  ]
  node [
    id 126
    label "wiedza"
  ]
  node [
    id 127
    label "zachowywanie"
  ]
  node [
    id 128
    label "secret"
  ]
  node [
    id 129
    label "wydawa&#263;"
  ]
  node [
    id 130
    label "obowi&#261;zek"
  ]
  node [
    id 131
    label "dyskrecja"
  ]
  node [
    id 132
    label "informacja"
  ]
  node [
    id 133
    label "wyda&#263;"
  ]
  node [
    id 134
    label "rzecz"
  ]
  node [
    id 135
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 136
    label "taj&#324;"
  ]
  node [
    id 137
    label "zachowa&#263;"
  ]
  node [
    id 138
    label "zachowywa&#263;"
  ]
  node [
    id 139
    label "podporz&#261;dkowanie"
  ]
  node [
    id 140
    label "porz&#261;dek"
  ]
  node [
    id 141
    label "mores"
  ]
  node [
    id 142
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 143
    label "nauczenie"
  ]
  node [
    id 144
    label "chart"
  ]
  node [
    id 145
    label "wynagrodzenie"
  ]
  node [
    id 146
    label "regimen"
  ]
  node [
    id 147
    label "r&#243;wnowarto&#347;&#263;"
  ]
  node [
    id 148
    label "terapia"
  ]
  node [
    id 149
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 150
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 151
    label "rok_ko&#347;cielny"
  ]
  node [
    id 152
    label "tekst"
  ]
  node [
    id 153
    label "czas"
  ]
  node [
    id 154
    label "praktyka"
  ]
  node [
    id 155
    label "mechanika"
  ]
  node [
    id 156
    label "o&#347;"
  ]
  node [
    id 157
    label "usenet"
  ]
  node [
    id 158
    label "rozprz&#261;c"
  ]
  node [
    id 159
    label "cybernetyk"
  ]
  node [
    id 160
    label "podsystem"
  ]
  node [
    id 161
    label "system"
  ]
  node [
    id 162
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 163
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 164
    label "sk&#322;ad"
  ]
  node [
    id 165
    label "systemat"
  ]
  node [
    id 166
    label "cecha"
  ]
  node [
    id 167
    label "konstrukcja"
  ]
  node [
    id 168
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 169
    label "konstelacja"
  ]
  node [
    id 170
    label "poumieszczanie"
  ]
  node [
    id 171
    label "burying"
  ]
  node [
    id 172
    label "powk&#322;adanie"
  ]
  node [
    id 173
    label "zw&#322;oki"
  ]
  node [
    id 174
    label "burial"
  ]
  node [
    id 175
    label "w&#322;o&#380;enie"
  ]
  node [
    id 176
    label "gr&#243;b"
  ]
  node [
    id 177
    label "spocz&#281;cie"
  ]
  node [
    id 178
    label "zapia&#263;"
  ]
  node [
    id 179
    label "pia&#263;"
  ]
  node [
    id 180
    label "kura"
  ]
  node [
    id 181
    label "r&#243;&#380;yczka"
  ]
  node [
    id 182
    label "zapianie"
  ]
  node [
    id 183
    label "samiec"
  ]
  node [
    id 184
    label "pianie"
  ]
  node [
    id 185
    label "dr&#243;b"
  ]
  node [
    id 186
    label "kurczak"
  ]
  node [
    id 187
    label "ptak"
  ]
  node [
    id 188
    label "samica"
  ]
  node [
    id 189
    label "gdakanie"
  ]
  node [
    id 190
    label "no&#347;no&#347;&#263;"
  ]
  node [
    id 191
    label "zagdaka&#263;"
  ]
  node [
    id 192
    label "kur_bankiwa"
  ]
  node [
    id 193
    label "ptak_&#322;owny"
  ]
  node [
    id 194
    label "gdakni&#281;cie"
  ]
  node [
    id 195
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 196
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 197
    label "kogut"
  ]
  node [
    id 198
    label "wydanie"
  ]
  node [
    id 199
    label "gaworzy&#263;"
  ]
  node [
    id 200
    label "gloat"
  ]
  node [
    id 201
    label "wys&#322;awia&#263;"
  ]
  node [
    id 202
    label "ba&#380;ant"
  ]
  node [
    id 203
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 204
    label "brag"
  ]
  node [
    id 205
    label "wys&#322;awianie"
  ]
  node [
    id 206
    label "pienie"
  ]
  node [
    id 207
    label "wydawanie"
  ]
  node [
    id 208
    label "cz&#261;stka"
  ]
  node [
    id 209
    label "wirus_r&#243;&#380;yczki"
  ]
  node [
    id 210
    label "choroba_wirusowa"
  ]
  node [
    id 211
    label "choroba_zara&#378;liwa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
]
