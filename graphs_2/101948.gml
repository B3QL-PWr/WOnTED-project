graph [
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "ustawa"
    origin "text"
  ]
  node [
    id 2
    label "czerwiec"
    origin "text"
  ]
  node [
    id 3
    label "poz"
    origin "text"
  ]
  node [
    id 4
    label "program_informacyjny"
  ]
  node [
    id 5
    label "journal"
  ]
  node [
    id 6
    label "diariusz"
  ]
  node [
    id 7
    label "spis"
  ]
  node [
    id 8
    label "ksi&#281;ga"
  ]
  node [
    id 9
    label "sheet"
  ]
  node [
    id 10
    label "pami&#281;tnik"
  ]
  node [
    id 11
    label "gazeta"
  ]
  node [
    id 12
    label "tytu&#322;"
  ]
  node [
    id 13
    label "redakcja"
  ]
  node [
    id 14
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 15
    label "czasopismo"
  ]
  node [
    id 16
    label "prasa"
  ]
  node [
    id 17
    label "rozdzia&#322;"
  ]
  node [
    id 18
    label "pismo"
  ]
  node [
    id 19
    label "Ewangelia"
  ]
  node [
    id 20
    label "book"
  ]
  node [
    id 21
    label "dokument"
  ]
  node [
    id 22
    label "tome"
  ]
  node [
    id 23
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 24
    label "pami&#261;tka"
  ]
  node [
    id 25
    label "notes"
  ]
  node [
    id 26
    label "zapiski"
  ]
  node [
    id 27
    label "raptularz"
  ]
  node [
    id 28
    label "album"
  ]
  node [
    id 29
    label "utw&#243;r_epicki"
  ]
  node [
    id 30
    label "zbi&#243;r"
  ]
  node [
    id 31
    label "catalog"
  ]
  node [
    id 32
    label "pozycja"
  ]
  node [
    id 33
    label "akt"
  ]
  node [
    id 34
    label "tekst"
  ]
  node [
    id 35
    label "sumariusz"
  ]
  node [
    id 36
    label "stock"
  ]
  node [
    id 37
    label "figurowa&#263;"
  ]
  node [
    id 38
    label "czynno&#347;&#263;"
  ]
  node [
    id 39
    label "wyliczanka"
  ]
  node [
    id 40
    label "Karta_Nauczyciela"
  ]
  node [
    id 41
    label "przej&#347;cie"
  ]
  node [
    id 42
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 43
    label "przej&#347;&#263;"
  ]
  node [
    id 44
    label "charter"
  ]
  node [
    id 45
    label "marc&#243;wka"
  ]
  node [
    id 46
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 47
    label "podnieci&#263;"
  ]
  node [
    id 48
    label "scena"
  ]
  node [
    id 49
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 50
    label "numer"
  ]
  node [
    id 51
    label "po&#380;ycie"
  ]
  node [
    id 52
    label "poj&#281;cie"
  ]
  node [
    id 53
    label "podniecenie"
  ]
  node [
    id 54
    label "nago&#347;&#263;"
  ]
  node [
    id 55
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 56
    label "fascyku&#322;"
  ]
  node [
    id 57
    label "seks"
  ]
  node [
    id 58
    label "podniecanie"
  ]
  node [
    id 59
    label "imisja"
  ]
  node [
    id 60
    label "zwyczaj"
  ]
  node [
    id 61
    label "rozmna&#380;anie"
  ]
  node [
    id 62
    label "ruch_frykcyjny"
  ]
  node [
    id 63
    label "ontologia"
  ]
  node [
    id 64
    label "wydarzenie"
  ]
  node [
    id 65
    label "na_pieska"
  ]
  node [
    id 66
    label "pozycja_misjonarska"
  ]
  node [
    id 67
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 68
    label "fragment"
  ]
  node [
    id 69
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 70
    label "z&#322;&#261;czenie"
  ]
  node [
    id 71
    label "gra_wst&#281;pna"
  ]
  node [
    id 72
    label "erotyka"
  ]
  node [
    id 73
    label "urzeczywistnienie"
  ]
  node [
    id 74
    label "baraszki"
  ]
  node [
    id 75
    label "certificate"
  ]
  node [
    id 76
    label "po&#380;&#261;danie"
  ]
  node [
    id 77
    label "wzw&#243;d"
  ]
  node [
    id 78
    label "funkcja"
  ]
  node [
    id 79
    label "act"
  ]
  node [
    id 80
    label "arystotelizm"
  ]
  node [
    id 81
    label "podnieca&#263;"
  ]
  node [
    id 82
    label "zabory"
  ]
  node [
    id 83
    label "ci&#281;&#380;arna"
  ]
  node [
    id 84
    label "rozwi&#261;zanie"
  ]
  node [
    id 85
    label "mini&#281;cie"
  ]
  node [
    id 86
    label "wymienienie"
  ]
  node [
    id 87
    label "zaliczenie"
  ]
  node [
    id 88
    label "traversal"
  ]
  node [
    id 89
    label "zdarzenie_si&#281;"
  ]
  node [
    id 90
    label "przewy&#380;szenie"
  ]
  node [
    id 91
    label "experience"
  ]
  node [
    id 92
    label "przepuszczenie"
  ]
  node [
    id 93
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 94
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 95
    label "strain"
  ]
  node [
    id 96
    label "faza"
  ]
  node [
    id 97
    label "przerobienie"
  ]
  node [
    id 98
    label "wydeptywanie"
  ]
  node [
    id 99
    label "miejsce"
  ]
  node [
    id 100
    label "crack"
  ]
  node [
    id 101
    label "wydeptanie"
  ]
  node [
    id 102
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 103
    label "wstawka"
  ]
  node [
    id 104
    label "prze&#380;ycie"
  ]
  node [
    id 105
    label "uznanie"
  ]
  node [
    id 106
    label "doznanie"
  ]
  node [
    id 107
    label "dostanie_si&#281;"
  ]
  node [
    id 108
    label "trwanie"
  ]
  node [
    id 109
    label "przebycie"
  ]
  node [
    id 110
    label "wytyczenie"
  ]
  node [
    id 111
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 112
    label "przepojenie"
  ]
  node [
    id 113
    label "nas&#261;czenie"
  ]
  node [
    id 114
    label "nale&#380;enie"
  ]
  node [
    id 115
    label "mienie"
  ]
  node [
    id 116
    label "odmienienie"
  ]
  node [
    id 117
    label "przedostanie_si&#281;"
  ]
  node [
    id 118
    label "przemokni&#281;cie"
  ]
  node [
    id 119
    label "nasycenie_si&#281;"
  ]
  node [
    id 120
    label "zacz&#281;cie"
  ]
  node [
    id 121
    label "stanie_si&#281;"
  ]
  node [
    id 122
    label "offense"
  ]
  node [
    id 123
    label "przestanie"
  ]
  node [
    id 124
    label "podlec"
  ]
  node [
    id 125
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 126
    label "min&#261;&#263;"
  ]
  node [
    id 127
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 128
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 129
    label "zaliczy&#263;"
  ]
  node [
    id 130
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 131
    label "zmieni&#263;"
  ]
  node [
    id 132
    label "przeby&#263;"
  ]
  node [
    id 133
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 134
    label "die"
  ]
  node [
    id 135
    label "dozna&#263;"
  ]
  node [
    id 136
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 137
    label "zacz&#261;&#263;"
  ]
  node [
    id 138
    label "happen"
  ]
  node [
    id 139
    label "pass"
  ]
  node [
    id 140
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 141
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 142
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 143
    label "beat"
  ]
  node [
    id 144
    label "absorb"
  ]
  node [
    id 145
    label "przerobi&#263;"
  ]
  node [
    id 146
    label "pique"
  ]
  node [
    id 147
    label "przesta&#263;"
  ]
  node [
    id 148
    label "odnaj&#281;cie"
  ]
  node [
    id 149
    label "naj&#281;cie"
  ]
  node [
    id 150
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 151
    label "ro&#347;lina_zielna"
  ]
  node [
    id 152
    label "go&#378;dzikowate"
  ]
  node [
    id 153
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 154
    label "miesi&#261;c"
  ]
  node [
    id 155
    label "tydzie&#324;"
  ]
  node [
    id 156
    label "miech"
  ]
  node [
    id 157
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 158
    label "czas"
  ]
  node [
    id 159
    label "rok"
  ]
  node [
    id 160
    label "kalendy"
  ]
  node [
    id 161
    label "go&#378;dzikowce"
  ]
  node [
    id 162
    label "ustawi&#263;"
  ]
  node [
    id 163
    label "trybuna&#322;"
  ]
  node [
    id 164
    label "konstytucyjny"
  ]
  node [
    id 165
    label "Marian"
  ]
  node [
    id 166
    label "grzybowski"
  ]
  node [
    id 167
    label "Jerzy"
  ]
  node [
    id 168
    label "St&#281;pie&#324;"
  ]
  node [
    id 169
    label "Gra&#380;yna"
  ]
  node [
    id 170
    label "Sza&#322;ygo"
  ]
  node [
    id 171
    label "marka"
  ]
  node [
    id 172
    label "Mazurkiewicz"
  ]
  node [
    id 173
    label "Jadwiga"
  ]
  node [
    id 174
    label "Sk&#243;rzewska"
  ]
  node [
    id 175
    label "&#321;osiak"
  ]
  node [
    id 176
    label "Janusz"
  ]
  node [
    id 177
    label "Niemcewicz"
  ]
  node [
    id 178
    label "prokurator"
  ]
  node [
    id 179
    label "generalny"
  ]
  node [
    id 180
    label "minister"
  ]
  node [
    id 181
    label "zdrowie"
  ]
  node [
    id 182
    label "Zbys&#322;awa"
  ]
  node [
    id 183
    label "kleczewski"
  ]
  node [
    id 184
    label "zeszyt"
  ]
  node [
    id 185
    label "dzie&#324;"
  ]
  node [
    id 186
    label "26"
  ]
  node [
    id 187
    label "pa&#378;dziernik"
  ]
  node [
    id 188
    label "1982"
  ]
  node [
    id 189
    label "ojciec"
  ]
  node [
    id 190
    label "wychowa&#263;"
  ]
  node [
    id 191
    label "wyspa"
  ]
  node [
    id 192
    label "trze&#378;wo&#347;&#263;"
  ]
  node [
    id 193
    label "i"
  ]
  node [
    id 194
    label "przeciwdzia&#322;a&#263;"
  ]
  node [
    id 195
    label "alkoholizm"
  ]
  node [
    id 196
    label "u"
  ]
  node [
    id 197
    label "rozporz&#261;dzi&#263;"
  ]
  node [
    id 198
    label "opieka"
  ]
  node [
    id 199
    label "spo&#322;eczny"
  ]
  node [
    id 200
    label "6"
  ]
  node [
    id 201
    label "maja"
  ]
  node [
    id 202
    label "1983"
  ]
  node [
    id 203
    label "sprawa"
  ]
  node [
    id 204
    label "warunki"
  ]
  node [
    id 205
    label "spos&#243;b"
  ]
  node [
    id 206
    label "dokonywa&#263;"
  ]
  node [
    id 207
    label "badanie"
  ]
  node [
    id 208
    label "na"
  ]
  node [
    id 209
    label "zawarto&#347;&#263;"
  ]
  node [
    id 210
    label "alkohol"
  ]
  node [
    id 211
    label "organizm"
  ]
  node [
    id 212
    label "konstytucja"
  ]
  node [
    id 213
    label "rzeczpospolita"
  ]
  node [
    id 214
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 159
    target 184
  ]
  edge [
    source 159
    target 185
  ]
  edge [
    source 159
    target 186
  ]
  edge [
    source 159
    target 187
  ]
  edge [
    source 159
    target 188
  ]
  edge [
    source 159
    target 189
  ]
  edge [
    source 159
    target 190
  ]
  edge [
    source 159
    target 191
  ]
  edge [
    source 159
    target 192
  ]
  edge [
    source 159
    target 193
  ]
  edge [
    source 159
    target 194
  ]
  edge [
    source 159
    target 195
  ]
  edge [
    source 159
    target 197
  ]
  edge [
    source 159
    target 180
  ]
  edge [
    source 159
    target 181
  ]
  edge [
    source 159
    target 198
  ]
  edge [
    source 159
    target 199
  ]
  edge [
    source 159
    target 200
  ]
  edge [
    source 159
    target 201
  ]
  edge [
    source 159
    target 202
  ]
  edge [
    source 159
    target 203
  ]
  edge [
    source 159
    target 204
  ]
  edge [
    source 159
    target 205
  ]
  edge [
    source 159
    target 206
  ]
  edge [
    source 159
    target 207
  ]
  edge [
    source 159
    target 208
  ]
  edge [
    source 159
    target 209
  ]
  edge [
    source 159
    target 210
  ]
  edge [
    source 159
    target 211
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 169
    target 170
  ]
  edge [
    source 171
    target 172
  ]
  edge [
    source 173
    target 174
  ]
  edge [
    source 173
    target 175
  ]
  edge [
    source 174
    target 175
  ]
  edge [
    source 176
    target 177
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 180
    target 181
  ]
  edge [
    source 180
    target 197
  ]
  edge [
    source 180
    target 193
  ]
  edge [
    source 180
    target 198
  ]
  edge [
    source 180
    target 199
  ]
  edge [
    source 180
    target 184
  ]
  edge [
    source 180
    target 185
  ]
  edge [
    source 180
    target 200
  ]
  edge [
    source 180
    target 201
  ]
  edge [
    source 180
    target 202
  ]
  edge [
    source 180
    target 191
  ]
  edge [
    source 180
    target 203
  ]
  edge [
    source 180
    target 204
  ]
  edge [
    source 180
    target 205
  ]
  edge [
    source 180
    target 206
  ]
  edge [
    source 180
    target 207
  ]
  edge [
    source 180
    target 208
  ]
  edge [
    source 180
    target 209
  ]
  edge [
    source 180
    target 210
  ]
  edge [
    source 180
    target 211
  ]
  edge [
    source 181
    target 197
  ]
  edge [
    source 181
    target 193
  ]
  edge [
    source 181
    target 198
  ]
  edge [
    source 181
    target 199
  ]
  edge [
    source 181
    target 184
  ]
  edge [
    source 181
    target 185
  ]
  edge [
    source 181
    target 200
  ]
  edge [
    source 181
    target 201
  ]
  edge [
    source 181
    target 202
  ]
  edge [
    source 181
    target 191
  ]
  edge [
    source 181
    target 203
  ]
  edge [
    source 181
    target 204
  ]
  edge [
    source 181
    target 205
  ]
  edge [
    source 181
    target 206
  ]
  edge [
    source 181
    target 207
  ]
  edge [
    source 181
    target 208
  ]
  edge [
    source 181
    target 209
  ]
  edge [
    source 181
    target 210
  ]
  edge [
    source 181
    target 211
  ]
  edge [
    source 182
    target 183
  ]
  edge [
    source 184
    target 185
  ]
  edge [
    source 184
    target 186
  ]
  edge [
    source 184
    target 187
  ]
  edge [
    source 184
    target 188
  ]
  edge [
    source 184
    target 189
  ]
  edge [
    source 184
    target 190
  ]
  edge [
    source 184
    target 191
  ]
  edge [
    source 184
    target 192
  ]
  edge [
    source 184
    target 193
  ]
  edge [
    source 184
    target 194
  ]
  edge [
    source 184
    target 195
  ]
  edge [
    source 184
    target 197
  ]
  edge [
    source 184
    target 198
  ]
  edge [
    source 184
    target 199
  ]
  edge [
    source 184
    target 200
  ]
  edge [
    source 184
    target 201
  ]
  edge [
    source 184
    target 202
  ]
  edge [
    source 184
    target 203
  ]
  edge [
    source 184
    target 204
  ]
  edge [
    source 184
    target 205
  ]
  edge [
    source 184
    target 206
  ]
  edge [
    source 184
    target 207
  ]
  edge [
    source 184
    target 208
  ]
  edge [
    source 184
    target 209
  ]
  edge [
    source 184
    target 210
  ]
  edge [
    source 184
    target 211
  ]
  edge [
    source 185
    target 186
  ]
  edge [
    source 185
    target 187
  ]
  edge [
    source 185
    target 188
  ]
  edge [
    source 185
    target 189
  ]
  edge [
    source 185
    target 190
  ]
  edge [
    source 185
    target 191
  ]
  edge [
    source 185
    target 192
  ]
  edge [
    source 185
    target 193
  ]
  edge [
    source 185
    target 194
  ]
  edge [
    source 185
    target 195
  ]
  edge [
    source 185
    target 197
  ]
  edge [
    source 185
    target 198
  ]
  edge [
    source 185
    target 199
  ]
  edge [
    source 185
    target 200
  ]
  edge [
    source 185
    target 201
  ]
  edge [
    source 185
    target 202
  ]
  edge [
    source 185
    target 203
  ]
  edge [
    source 185
    target 204
  ]
  edge [
    source 185
    target 205
  ]
  edge [
    source 185
    target 206
  ]
  edge [
    source 185
    target 207
  ]
  edge [
    source 185
    target 208
  ]
  edge [
    source 185
    target 209
  ]
  edge [
    source 185
    target 210
  ]
  edge [
    source 185
    target 211
  ]
  edge [
    source 186
    target 187
  ]
  edge [
    source 186
    target 188
  ]
  edge [
    source 186
    target 189
  ]
  edge [
    source 186
    target 190
  ]
  edge [
    source 186
    target 191
  ]
  edge [
    source 186
    target 192
  ]
  edge [
    source 186
    target 193
  ]
  edge [
    source 186
    target 194
  ]
  edge [
    source 186
    target 195
  ]
  edge [
    source 187
    target 188
  ]
  edge [
    source 187
    target 189
  ]
  edge [
    source 187
    target 190
  ]
  edge [
    source 187
    target 191
  ]
  edge [
    source 187
    target 192
  ]
  edge [
    source 187
    target 193
  ]
  edge [
    source 187
    target 194
  ]
  edge [
    source 187
    target 195
  ]
  edge [
    source 188
    target 189
  ]
  edge [
    source 188
    target 190
  ]
  edge [
    source 188
    target 191
  ]
  edge [
    source 188
    target 192
  ]
  edge [
    source 188
    target 193
  ]
  edge [
    source 188
    target 194
  ]
  edge [
    source 188
    target 195
  ]
  edge [
    source 189
    target 190
  ]
  edge [
    source 189
    target 191
  ]
  edge [
    source 189
    target 192
  ]
  edge [
    source 189
    target 193
  ]
  edge [
    source 189
    target 194
  ]
  edge [
    source 189
    target 195
  ]
  edge [
    source 190
    target 191
  ]
  edge [
    source 190
    target 192
  ]
  edge [
    source 190
    target 193
  ]
  edge [
    source 190
    target 194
  ]
  edge [
    source 190
    target 195
  ]
  edge [
    source 191
    target 192
  ]
  edge [
    source 191
    target 193
  ]
  edge [
    source 191
    target 194
  ]
  edge [
    source 191
    target 195
  ]
  edge [
    source 191
    target 197
  ]
  edge [
    source 191
    target 198
  ]
  edge [
    source 191
    target 199
  ]
  edge [
    source 191
    target 200
  ]
  edge [
    source 191
    target 201
  ]
  edge [
    source 191
    target 202
  ]
  edge [
    source 191
    target 203
  ]
  edge [
    source 191
    target 204
  ]
  edge [
    source 191
    target 205
  ]
  edge [
    source 191
    target 206
  ]
  edge [
    source 191
    target 207
  ]
  edge [
    source 191
    target 208
  ]
  edge [
    source 191
    target 209
  ]
  edge [
    source 191
    target 210
  ]
  edge [
    source 191
    target 191
  ]
  edge [
    source 191
    target 211
  ]
  edge [
    source 192
    target 193
  ]
  edge [
    source 192
    target 194
  ]
  edge [
    source 192
    target 195
  ]
  edge [
    source 193
    target 194
  ]
  edge [
    source 193
    target 195
  ]
  edge [
    source 193
    target 197
  ]
  edge [
    source 193
    target 198
  ]
  edge [
    source 193
    target 199
  ]
  edge [
    source 193
    target 200
  ]
  edge [
    source 193
    target 201
  ]
  edge [
    source 193
    target 202
  ]
  edge [
    source 193
    target 203
  ]
  edge [
    source 193
    target 204
  ]
  edge [
    source 193
    target 193
  ]
  edge [
    source 193
    target 205
  ]
  edge [
    source 193
    target 206
  ]
  edge [
    source 193
    target 207
  ]
  edge [
    source 193
    target 208
  ]
  edge [
    source 193
    target 209
  ]
  edge [
    source 193
    target 210
  ]
  edge [
    source 193
    target 211
  ]
  edge [
    source 194
    target 195
  ]
  edge [
    source 197
    target 198
  ]
  edge [
    source 197
    target 199
  ]
  edge [
    source 197
    target 200
  ]
  edge [
    source 197
    target 201
  ]
  edge [
    source 197
    target 202
  ]
  edge [
    source 197
    target 203
  ]
  edge [
    source 197
    target 204
  ]
  edge [
    source 197
    target 205
  ]
  edge [
    source 197
    target 206
  ]
  edge [
    source 197
    target 207
  ]
  edge [
    source 197
    target 208
  ]
  edge [
    source 197
    target 209
  ]
  edge [
    source 197
    target 210
  ]
  edge [
    source 197
    target 211
  ]
  edge [
    source 198
    target 199
  ]
  edge [
    source 198
    target 200
  ]
  edge [
    source 198
    target 201
  ]
  edge [
    source 198
    target 202
  ]
  edge [
    source 198
    target 203
  ]
  edge [
    source 198
    target 204
  ]
  edge [
    source 198
    target 205
  ]
  edge [
    source 198
    target 206
  ]
  edge [
    source 198
    target 207
  ]
  edge [
    source 198
    target 208
  ]
  edge [
    source 198
    target 209
  ]
  edge [
    source 198
    target 210
  ]
  edge [
    source 198
    target 211
  ]
  edge [
    source 199
    target 200
  ]
  edge [
    source 199
    target 201
  ]
  edge [
    source 199
    target 202
  ]
  edge [
    source 199
    target 203
  ]
  edge [
    source 199
    target 204
  ]
  edge [
    source 199
    target 205
  ]
  edge [
    source 199
    target 206
  ]
  edge [
    source 199
    target 207
  ]
  edge [
    source 199
    target 208
  ]
  edge [
    source 199
    target 209
  ]
  edge [
    source 199
    target 210
  ]
  edge [
    source 199
    target 211
  ]
  edge [
    source 200
    target 201
  ]
  edge [
    source 200
    target 202
  ]
  edge [
    source 200
    target 203
  ]
  edge [
    source 200
    target 204
  ]
  edge [
    source 200
    target 205
  ]
  edge [
    source 200
    target 206
  ]
  edge [
    source 200
    target 207
  ]
  edge [
    source 200
    target 208
  ]
  edge [
    source 200
    target 209
  ]
  edge [
    source 200
    target 210
  ]
  edge [
    source 200
    target 211
  ]
  edge [
    source 201
    target 202
  ]
  edge [
    source 201
    target 203
  ]
  edge [
    source 201
    target 204
  ]
  edge [
    source 201
    target 205
  ]
  edge [
    source 201
    target 206
  ]
  edge [
    source 201
    target 207
  ]
  edge [
    source 201
    target 208
  ]
  edge [
    source 201
    target 209
  ]
  edge [
    source 201
    target 210
  ]
  edge [
    source 201
    target 211
  ]
  edge [
    source 202
    target 203
  ]
  edge [
    source 202
    target 204
  ]
  edge [
    source 202
    target 205
  ]
  edge [
    source 202
    target 206
  ]
  edge [
    source 202
    target 207
  ]
  edge [
    source 202
    target 208
  ]
  edge [
    source 202
    target 209
  ]
  edge [
    source 202
    target 210
  ]
  edge [
    source 202
    target 211
  ]
  edge [
    source 203
    target 204
  ]
  edge [
    source 203
    target 205
  ]
  edge [
    source 203
    target 206
  ]
  edge [
    source 203
    target 207
  ]
  edge [
    source 203
    target 208
  ]
  edge [
    source 203
    target 209
  ]
  edge [
    source 203
    target 210
  ]
  edge [
    source 203
    target 211
  ]
  edge [
    source 204
    target 205
  ]
  edge [
    source 204
    target 206
  ]
  edge [
    source 204
    target 207
  ]
  edge [
    source 204
    target 208
  ]
  edge [
    source 204
    target 209
  ]
  edge [
    source 204
    target 210
  ]
  edge [
    source 204
    target 211
  ]
  edge [
    source 205
    target 206
  ]
  edge [
    source 205
    target 207
  ]
  edge [
    source 205
    target 208
  ]
  edge [
    source 205
    target 209
  ]
  edge [
    source 205
    target 210
  ]
  edge [
    source 205
    target 211
  ]
  edge [
    source 206
    target 207
  ]
  edge [
    source 206
    target 208
  ]
  edge [
    source 206
    target 209
  ]
  edge [
    source 206
    target 210
  ]
  edge [
    source 206
    target 211
  ]
  edge [
    source 207
    target 208
  ]
  edge [
    source 207
    target 209
  ]
  edge [
    source 207
    target 210
  ]
  edge [
    source 207
    target 211
  ]
  edge [
    source 208
    target 209
  ]
  edge [
    source 208
    target 210
  ]
  edge [
    source 208
    target 211
  ]
  edge [
    source 209
    target 210
  ]
  edge [
    source 209
    target 211
  ]
  edge [
    source 210
    target 211
  ]
  edge [
    source 212
    target 213
  ]
  edge [
    source 212
    target 214
  ]
  edge [
    source 213
    target 214
  ]
]
