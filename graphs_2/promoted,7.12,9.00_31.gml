graph [
  node [
    id 0
    label "cena"
    origin "text"
  ]
  node [
    id 1
    label "lidl"
    origin "text"
  ]
  node [
    id 2
    label "versus"
    origin "text"
  ]
  node [
    id 3
    label "wyceni&#263;"
  ]
  node [
    id 4
    label "kosztowa&#263;"
  ]
  node [
    id 5
    label "wycenienie"
  ]
  node [
    id 6
    label "worth"
  ]
  node [
    id 7
    label "cenowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 8
    label "dyskryminacja_cenowa"
  ]
  node [
    id 9
    label "kupowanie"
  ]
  node [
    id 10
    label "inflacja"
  ]
  node [
    id 11
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 12
    label "warto&#347;&#263;"
  ]
  node [
    id 13
    label "kosztowanie"
  ]
  node [
    id 14
    label "poj&#281;cie"
  ]
  node [
    id 15
    label "zmienna"
  ]
  node [
    id 16
    label "wskazywanie"
  ]
  node [
    id 17
    label "korzy&#347;&#263;"
  ]
  node [
    id 18
    label "rewaluowanie"
  ]
  node [
    id 19
    label "zrewaluowa&#263;"
  ]
  node [
    id 20
    label "cecha"
  ]
  node [
    id 21
    label "rewaluowa&#263;"
  ]
  node [
    id 22
    label "rozmiar"
  ]
  node [
    id 23
    label "cel"
  ]
  node [
    id 24
    label "wabik"
  ]
  node [
    id 25
    label "strona"
  ]
  node [
    id 26
    label "wskazywa&#263;"
  ]
  node [
    id 27
    label "zrewaluowanie"
  ]
  node [
    id 28
    label "policzenie"
  ]
  node [
    id 29
    label "ustalenie"
  ]
  node [
    id 30
    label "appraisal"
  ]
  node [
    id 31
    label "proces_ekonomiczny"
  ]
  node [
    id 32
    label "wzrost"
  ]
  node [
    id 33
    label "kosmologia"
  ]
  node [
    id 34
    label "faza"
  ]
  node [
    id 35
    label "ewolucja_kosmosu"
  ]
  node [
    id 36
    label "handlowanie"
  ]
  node [
    id 37
    label "granie"
  ]
  node [
    id 38
    label "pozyskiwanie"
  ]
  node [
    id 39
    label "uznawanie"
  ]
  node [
    id 40
    label "importowanie"
  ]
  node [
    id 41
    label "wierzenie"
  ]
  node [
    id 42
    label "wkupywanie_si&#281;"
  ]
  node [
    id 43
    label "wkupienie_si&#281;"
  ]
  node [
    id 44
    label "purchase"
  ]
  node [
    id 45
    label "wykupywanie"
  ]
  node [
    id 46
    label "ustawianie"
  ]
  node [
    id 47
    label "kupienie"
  ]
  node [
    id 48
    label "buying"
  ]
  node [
    id 49
    label "bycie"
  ]
  node [
    id 50
    label "badanie"
  ]
  node [
    id 51
    label "tasting"
  ]
  node [
    id 52
    label "jedzenie"
  ]
  node [
    id 53
    label "zaznawanie"
  ]
  node [
    id 54
    label "kiperstwo"
  ]
  node [
    id 55
    label "policzy&#263;"
  ]
  node [
    id 56
    label "estimate"
  ]
  node [
    id 57
    label "ustali&#263;"
  ]
  node [
    id 58
    label "savor"
  ]
  node [
    id 59
    label "try"
  ]
  node [
    id 60
    label "essay"
  ]
  node [
    id 61
    label "doznawa&#263;"
  ]
  node [
    id 62
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 63
    label "by&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 1
    target 2
  ]
]
