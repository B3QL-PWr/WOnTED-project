graph [
  node [
    id 0
    label "rublowka"
    origin "text"
  ]
  node [
    id 1
    label "J&#243;zefa"
  ]
  node [
    id 2
    label "Stalin"
  ]
  node [
    id 3
    label "Borys"
  ]
  node [
    id 4
    label "Jelcyn"
  ]
  node [
    id 5
    label "leonida"
  ]
  node [
    id 6
    label "Bre&#380;niew"
  ]
  node [
    id 7
    label "W&#322;odzimierz"
  ]
  node [
    id 8
    label "Lenin"
  ]
  node [
    id 9
    label "prospekt"
  ]
  node [
    id 10
    label "Kutuzow"
  ]
  node [
    id 11
    label "Rublowsko"
  ]
  node [
    id 12
    label "Uspienskiej"
  ]
  node [
    id 13
    label "Barvicha"
  ]
  node [
    id 14
    label "Luxury"
  ]
  node [
    id 15
    label "Village"
  ]
  node [
    id 16
    label "Barwicha"
  ]
  node [
    id 17
    label "wioska"
  ]
  node [
    id 18
    label "luksus"
  ]
  node [
    id 19
    label "dolec"
  ]
  node [
    id 20
    label "Gabbana"
  ]
  node [
    id 21
    label "Jab&#322;oniewyj"
  ]
  node [
    id 22
    label "sad"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
]
