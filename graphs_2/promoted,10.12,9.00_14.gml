graph [
  node [
    id 0
    label "dla"
    origin "text"
  ]
  node [
    id 1
    label "niecierpliwy"
    origin "text"
  ]
  node [
    id 2
    label "akcja"
    origin "text"
  ]
  node [
    id 3
    label "niespokojny"
  ]
  node [
    id 4
    label "niecierpliwie"
  ]
  node [
    id 5
    label "niespokojnie"
  ]
  node [
    id 6
    label "impatiently"
  ]
  node [
    id 7
    label "nerwowo"
  ]
  node [
    id 8
    label "dywidenda"
  ]
  node [
    id 9
    label "przebieg"
  ]
  node [
    id 10
    label "operacja"
  ]
  node [
    id 11
    label "zagrywka"
  ]
  node [
    id 12
    label "wydarzenie"
  ]
  node [
    id 13
    label "udzia&#322;"
  ]
  node [
    id 14
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 15
    label "commotion"
  ]
  node [
    id 16
    label "occupation"
  ]
  node [
    id 17
    label "gra"
  ]
  node [
    id 18
    label "jazda"
  ]
  node [
    id 19
    label "czyn"
  ]
  node [
    id 20
    label "stock"
  ]
  node [
    id 21
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 22
    label "w&#281;ze&#322;"
  ]
  node [
    id 23
    label "wysoko&#347;&#263;"
  ]
  node [
    id 24
    label "czynno&#347;&#263;"
  ]
  node [
    id 25
    label "instrument_strunowy"
  ]
  node [
    id 26
    label "activity"
  ]
  node [
    id 27
    label "bezproblemowy"
  ]
  node [
    id 28
    label "funkcja"
  ]
  node [
    id 29
    label "act"
  ]
  node [
    id 30
    label "tallness"
  ]
  node [
    id 31
    label "altitude"
  ]
  node [
    id 32
    label "rozmiar"
  ]
  node [
    id 33
    label "degree"
  ]
  node [
    id 34
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 35
    label "odcinek"
  ]
  node [
    id 36
    label "k&#261;t"
  ]
  node [
    id 37
    label "wielko&#347;&#263;"
  ]
  node [
    id 38
    label "brzmienie"
  ]
  node [
    id 39
    label "sum"
  ]
  node [
    id 40
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 41
    label "gambit"
  ]
  node [
    id 42
    label "rozgrywka"
  ]
  node [
    id 43
    label "move"
  ]
  node [
    id 44
    label "manewr"
  ]
  node [
    id 45
    label "uderzenie"
  ]
  node [
    id 46
    label "posuni&#281;cie"
  ]
  node [
    id 47
    label "myk"
  ]
  node [
    id 48
    label "gra_w_karty"
  ]
  node [
    id 49
    label "mecz"
  ]
  node [
    id 50
    label "travel"
  ]
  node [
    id 51
    label "przebiec"
  ]
  node [
    id 52
    label "charakter"
  ]
  node [
    id 53
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 54
    label "motyw"
  ]
  node [
    id 55
    label "przebiegni&#281;cie"
  ]
  node [
    id 56
    label "fabu&#322;a"
  ]
  node [
    id 57
    label "proces_my&#347;lowy"
  ]
  node [
    id 58
    label "liczenie"
  ]
  node [
    id 59
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 60
    label "supremum"
  ]
  node [
    id 61
    label "laparotomia"
  ]
  node [
    id 62
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 63
    label "jednostka"
  ]
  node [
    id 64
    label "matematyka"
  ]
  node [
    id 65
    label "rzut"
  ]
  node [
    id 66
    label "liczy&#263;"
  ]
  node [
    id 67
    label "strategia"
  ]
  node [
    id 68
    label "torakotomia"
  ]
  node [
    id 69
    label "chirurg"
  ]
  node [
    id 70
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 71
    label "zabieg"
  ]
  node [
    id 72
    label "szew"
  ]
  node [
    id 73
    label "mathematical_process"
  ]
  node [
    id 74
    label "infimum"
  ]
  node [
    id 75
    label "linia"
  ]
  node [
    id 76
    label "procedura"
  ]
  node [
    id 77
    label "zbi&#243;r"
  ]
  node [
    id 78
    label "proces"
  ]
  node [
    id 79
    label "room"
  ]
  node [
    id 80
    label "ilo&#347;&#263;"
  ]
  node [
    id 81
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 82
    label "sequence"
  ]
  node [
    id 83
    label "praca"
  ]
  node [
    id 84
    label "cycle"
  ]
  node [
    id 85
    label "obecno&#347;&#263;"
  ]
  node [
    id 86
    label "kwota"
  ]
  node [
    id 87
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 88
    label "zmienno&#347;&#263;"
  ]
  node [
    id 89
    label "play"
  ]
  node [
    id 90
    label "apparent_motion"
  ]
  node [
    id 91
    label "contest"
  ]
  node [
    id 92
    label "komplet"
  ]
  node [
    id 93
    label "zabawa"
  ]
  node [
    id 94
    label "zasada"
  ]
  node [
    id 95
    label "rywalizacja"
  ]
  node [
    id 96
    label "zbijany"
  ]
  node [
    id 97
    label "post&#281;powanie"
  ]
  node [
    id 98
    label "game"
  ]
  node [
    id 99
    label "odg&#322;os"
  ]
  node [
    id 100
    label "Pok&#233;mon"
  ]
  node [
    id 101
    label "synteza"
  ]
  node [
    id 102
    label "odtworzenie"
  ]
  node [
    id 103
    label "rekwizyt_do_gry"
  ]
  node [
    id 104
    label "formacja"
  ]
  node [
    id 105
    label "szwadron"
  ]
  node [
    id 106
    label "wykrzyknik"
  ]
  node [
    id 107
    label "awantura"
  ]
  node [
    id 108
    label "journey"
  ]
  node [
    id 109
    label "sport"
  ]
  node [
    id 110
    label "heca"
  ]
  node [
    id 111
    label "ruch"
  ]
  node [
    id 112
    label "cavalry"
  ]
  node [
    id 113
    label "szale&#324;stwo"
  ]
  node [
    id 114
    label "chor&#261;giew"
  ]
  node [
    id 115
    label "doch&#243;d"
  ]
  node [
    id 116
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 117
    label "wi&#261;zanie"
  ]
  node [
    id 118
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 119
    label "poj&#281;cie"
  ]
  node [
    id 120
    label "bratnia_dusza"
  ]
  node [
    id 121
    label "trasa"
  ]
  node [
    id 122
    label "uczesanie"
  ]
  node [
    id 123
    label "orbita"
  ]
  node [
    id 124
    label "kryszta&#322;"
  ]
  node [
    id 125
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 126
    label "zwi&#261;zanie"
  ]
  node [
    id 127
    label "graf"
  ]
  node [
    id 128
    label "hitch"
  ]
  node [
    id 129
    label "struktura_anatomiczna"
  ]
  node [
    id 130
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 131
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 132
    label "o&#347;rodek"
  ]
  node [
    id 133
    label "marriage"
  ]
  node [
    id 134
    label "punkt"
  ]
  node [
    id 135
    label "ekliptyka"
  ]
  node [
    id 136
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 137
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 138
    label "problem"
  ]
  node [
    id 139
    label "zawi&#261;za&#263;"
  ]
  node [
    id 140
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 141
    label "fala_stoj&#261;ca"
  ]
  node [
    id 142
    label "tying"
  ]
  node [
    id 143
    label "argument"
  ]
  node [
    id 144
    label "zwi&#261;za&#263;"
  ]
  node [
    id 145
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 146
    label "mila_morska"
  ]
  node [
    id 147
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 148
    label "skupienie"
  ]
  node [
    id 149
    label "zgrubienie"
  ]
  node [
    id 150
    label "pismo_klinowe"
  ]
  node [
    id 151
    label "przeci&#281;cie"
  ]
  node [
    id 152
    label "band"
  ]
  node [
    id 153
    label "zwi&#261;zek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
]
