graph [
  node [
    id 0
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 2
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 3
    label "zobaczy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "jak"
    origin "text"
  ]
  node [
    id 5
    label "wewn&#261;trz"
    origin "text"
  ]
  node [
    id 6
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "chyba"
    origin "text"
  ]
  node [
    id 8
    label "najbardziej"
    origin "text"
  ]
  node [
    id 9
    label "traumatyczny"
    origin "text"
  ]
  node [
    id 10
    label "do&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 11
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 12
    label "ostatni"
    origin "text"
  ]
  node [
    id 13
    label "dwa"
    origin "text"
  ]
  node [
    id 14
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 15
    label "doba"
  ]
  node [
    id 16
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 17
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 18
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 19
    label "dzi&#347;"
  ]
  node [
    id 20
    label "teraz"
  ]
  node [
    id 21
    label "czas"
  ]
  node [
    id 22
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 23
    label "jednocze&#347;nie"
  ]
  node [
    id 24
    label "noc"
  ]
  node [
    id 25
    label "dzie&#324;"
  ]
  node [
    id 26
    label "godzina"
  ]
  node [
    id 27
    label "long_time"
  ]
  node [
    id 28
    label "jednostka_geologiczna"
  ]
  node [
    id 29
    label "u&#347;wiadomi&#263;_sobie"
  ]
  node [
    id 30
    label "spoziera&#263;"
  ]
  node [
    id 31
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 32
    label "peek"
  ]
  node [
    id 33
    label "postrzec"
  ]
  node [
    id 34
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 35
    label "cognizance"
  ]
  node [
    id 36
    label "popatrze&#263;"
  ]
  node [
    id 37
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 38
    label "pojrze&#263;"
  ]
  node [
    id 39
    label "dostrzec"
  ]
  node [
    id 40
    label "spot"
  ]
  node [
    id 41
    label "przekona&#263;_si&#281;"
  ]
  node [
    id 42
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 43
    label "go_steady"
  ]
  node [
    id 44
    label "zinterpretowa&#263;"
  ]
  node [
    id 45
    label "spotka&#263;"
  ]
  node [
    id 46
    label "obejrze&#263;"
  ]
  node [
    id 47
    label "znale&#378;&#263;"
  ]
  node [
    id 48
    label "see"
  ]
  node [
    id 49
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 50
    label "oceni&#263;"
  ]
  node [
    id 51
    label "zagra&#263;"
  ]
  node [
    id 52
    label "illustrate"
  ]
  node [
    id 53
    label "zanalizowa&#263;"
  ]
  node [
    id 54
    label "read"
  ]
  node [
    id 55
    label "think"
  ]
  node [
    id 56
    label "visualize"
  ]
  node [
    id 57
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 58
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 59
    label "sta&#263;_si&#281;"
  ]
  node [
    id 60
    label "zrobi&#263;"
  ]
  node [
    id 61
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 62
    label "notice"
  ]
  node [
    id 63
    label "respect"
  ]
  node [
    id 64
    label "insert"
  ]
  node [
    id 65
    label "pozna&#263;"
  ]
  node [
    id 66
    label "befall"
  ]
  node [
    id 67
    label "spowodowa&#263;"
  ]
  node [
    id 68
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 69
    label "discover"
  ]
  node [
    id 70
    label "zorientowa&#263;_si&#281;"
  ]
  node [
    id 71
    label "pozyska&#263;"
  ]
  node [
    id 72
    label "devise"
  ]
  node [
    id 73
    label "dozna&#263;"
  ]
  node [
    id 74
    label "wykry&#263;"
  ]
  node [
    id 75
    label "odzyska&#263;"
  ]
  node [
    id 76
    label "znaj&#347;&#263;"
  ]
  node [
    id 77
    label "invent"
  ]
  node [
    id 78
    label "wymy&#347;li&#263;"
  ]
  node [
    id 79
    label "nagranie_d&#378;wi&#281;kowe"
  ]
  node [
    id 80
    label "radio"
  ]
  node [
    id 81
    label "lampa"
  ]
  node [
    id 82
    label "film"
  ]
  node [
    id 83
    label "pomiar"
  ]
  node [
    id 84
    label "booklet"
  ]
  node [
    id 85
    label "transakcja"
  ]
  node [
    id 86
    label "ekspozycja"
  ]
  node [
    id 87
    label "reklama"
  ]
  node [
    id 88
    label "fotografia"
  ]
  node [
    id 89
    label "spojrze&#263;"
  ]
  node [
    id 90
    label "spogl&#261;da&#263;"
  ]
  node [
    id 91
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 92
    label "zobo"
  ]
  node [
    id 93
    label "yakalo"
  ]
  node [
    id 94
    label "byd&#322;o"
  ]
  node [
    id 95
    label "dzo"
  ]
  node [
    id 96
    label "kr&#281;torogie"
  ]
  node [
    id 97
    label "zbi&#243;r"
  ]
  node [
    id 98
    label "g&#322;owa"
  ]
  node [
    id 99
    label "czochrad&#322;o"
  ]
  node [
    id 100
    label "posp&#243;lstwo"
  ]
  node [
    id 101
    label "kraal"
  ]
  node [
    id 102
    label "livestock"
  ]
  node [
    id 103
    label "prze&#380;uwacz"
  ]
  node [
    id 104
    label "bizon"
  ]
  node [
    id 105
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 106
    label "zebu"
  ]
  node [
    id 107
    label "byd&#322;o_domowe"
  ]
  node [
    id 108
    label "psychicznie"
  ]
  node [
    id 109
    label "niematerialnie"
  ]
  node [
    id 110
    label "anormalnie"
  ]
  node [
    id 111
    label "psychologicznie"
  ]
  node [
    id 112
    label "psychically"
  ]
  node [
    id 113
    label "psychiczny"
  ]
  node [
    id 114
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 115
    label "wydoby&#263;"
  ]
  node [
    id 116
    label "okre&#347;li&#263;"
  ]
  node [
    id 117
    label "poda&#263;"
  ]
  node [
    id 118
    label "express"
  ]
  node [
    id 119
    label "wyrazi&#263;"
  ]
  node [
    id 120
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 121
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 122
    label "rzekn&#261;&#263;"
  ]
  node [
    id 123
    label "unwrap"
  ]
  node [
    id 124
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 125
    label "convey"
  ]
  node [
    id 126
    label "tenis"
  ]
  node [
    id 127
    label "supply"
  ]
  node [
    id 128
    label "da&#263;"
  ]
  node [
    id 129
    label "ustawi&#263;"
  ]
  node [
    id 130
    label "siatk&#243;wka"
  ]
  node [
    id 131
    label "give"
  ]
  node [
    id 132
    label "jedzenie"
  ]
  node [
    id 133
    label "poinformowa&#263;"
  ]
  node [
    id 134
    label "introduce"
  ]
  node [
    id 135
    label "nafaszerowa&#263;"
  ]
  node [
    id 136
    label "zaserwowa&#263;"
  ]
  node [
    id 137
    label "draw"
  ]
  node [
    id 138
    label "doby&#263;"
  ]
  node [
    id 139
    label "g&#243;rnictwo"
  ]
  node [
    id 140
    label "wyeksploatowa&#263;"
  ]
  node [
    id 141
    label "extract"
  ]
  node [
    id 142
    label "obtain"
  ]
  node [
    id 143
    label "wyj&#261;&#263;"
  ]
  node [
    id 144
    label "ocali&#263;"
  ]
  node [
    id 145
    label "uzyska&#263;"
  ]
  node [
    id 146
    label "wyda&#263;"
  ]
  node [
    id 147
    label "wydosta&#263;"
  ]
  node [
    id 148
    label "uwydatni&#263;"
  ]
  node [
    id 149
    label "distill"
  ]
  node [
    id 150
    label "raise"
  ]
  node [
    id 151
    label "testify"
  ]
  node [
    id 152
    label "zakomunikowa&#263;"
  ]
  node [
    id 153
    label "oznaczy&#263;"
  ]
  node [
    id 154
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 155
    label "vent"
  ]
  node [
    id 156
    label "zdecydowa&#263;"
  ]
  node [
    id 157
    label "situate"
  ]
  node [
    id 158
    label "nominate"
  ]
  node [
    id 159
    label "pourazowy"
  ]
  node [
    id 160
    label "dramatyczny"
  ]
  node [
    id 161
    label "bolesny"
  ]
  node [
    id 162
    label "traumatycznie"
  ]
  node [
    id 163
    label "pourazowo"
  ]
  node [
    id 164
    label "chory"
  ]
  node [
    id 165
    label "straszny"
  ]
  node [
    id 166
    label "emocjonuj&#261;cy"
  ]
  node [
    id 167
    label "tragicznie"
  ]
  node [
    id 168
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 169
    label "przej&#281;ty"
  ]
  node [
    id 170
    label "powa&#380;ny"
  ]
  node [
    id 171
    label "dynamiczny"
  ]
  node [
    id 172
    label "dramatycznie"
  ]
  node [
    id 173
    label "wstrz&#261;saj&#261;cy"
  ]
  node [
    id 174
    label "przykry"
  ]
  node [
    id 175
    label "bole&#347;nie"
  ]
  node [
    id 176
    label "niezadowolony"
  ]
  node [
    id 177
    label "cierpi&#261;cy"
  ]
  node [
    id 178
    label "&#380;a&#322;osny"
  ]
  node [
    id 179
    label "dojmuj&#261;cy"
  ]
  node [
    id 180
    label "bole&#347;ny"
  ]
  node [
    id 181
    label "tkliwy"
  ]
  node [
    id 182
    label "dotkliwy"
  ]
  node [
    id 183
    label "bolesno"
  ]
  node [
    id 184
    label "badanie"
  ]
  node [
    id 185
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 186
    label "szko&#322;a"
  ]
  node [
    id 187
    label "obserwowanie"
  ]
  node [
    id 188
    label "wiedza"
  ]
  node [
    id 189
    label "wy&#347;wiadczenie"
  ]
  node [
    id 190
    label "wydarzenie"
  ]
  node [
    id 191
    label "assay"
  ]
  node [
    id 192
    label "znawstwo"
  ]
  node [
    id 193
    label "skill"
  ]
  node [
    id 194
    label "checkup"
  ]
  node [
    id 195
    label "spotkanie"
  ]
  node [
    id 196
    label "do&#347;wiadczanie"
  ]
  node [
    id 197
    label "zbadanie"
  ]
  node [
    id 198
    label "potraktowanie"
  ]
  node [
    id 199
    label "eksperiencja"
  ]
  node [
    id 200
    label "poczucie"
  ]
  node [
    id 201
    label "zrecenzowanie"
  ]
  node [
    id 202
    label "kontrola"
  ]
  node [
    id 203
    label "analysis"
  ]
  node [
    id 204
    label "rektalny"
  ]
  node [
    id 205
    label "ustalenie"
  ]
  node [
    id 206
    label "macanie"
  ]
  node [
    id 207
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 208
    label "usi&#322;owanie"
  ]
  node [
    id 209
    label "udowadnianie"
  ]
  node [
    id 210
    label "praca"
  ]
  node [
    id 211
    label "bia&#322;a_niedziela"
  ]
  node [
    id 212
    label "diagnostyka"
  ]
  node [
    id 213
    label "dociekanie"
  ]
  node [
    id 214
    label "rezultat"
  ]
  node [
    id 215
    label "sprawdzanie"
  ]
  node [
    id 216
    label "penetrowanie"
  ]
  node [
    id 217
    label "czynno&#347;&#263;"
  ]
  node [
    id 218
    label "krytykowanie"
  ]
  node [
    id 219
    label "omawianie"
  ]
  node [
    id 220
    label "ustalanie"
  ]
  node [
    id 221
    label "rozpatrywanie"
  ]
  node [
    id 222
    label "investigation"
  ]
  node [
    id 223
    label "wziernikowanie"
  ]
  node [
    id 224
    label "examination"
  ]
  node [
    id 225
    label "cognition"
  ]
  node [
    id 226
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 227
    label "intelekt"
  ]
  node [
    id 228
    label "pozwolenie"
  ]
  node [
    id 229
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 230
    label "zaawansowanie"
  ]
  node [
    id 231
    label "wykszta&#322;cenie"
  ]
  node [
    id 232
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 233
    label "ekstraspekcja"
  ]
  node [
    id 234
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 235
    label "feeling"
  ]
  node [
    id 236
    label "doznanie"
  ]
  node [
    id 237
    label "smell"
  ]
  node [
    id 238
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 239
    label "zdarzenie_si&#281;"
  ]
  node [
    id 240
    label "opanowanie"
  ]
  node [
    id 241
    label "os&#322;upienie"
  ]
  node [
    id 242
    label "zareagowanie"
  ]
  node [
    id 243
    label "intuition"
  ]
  node [
    id 244
    label "przebiec"
  ]
  node [
    id 245
    label "charakter"
  ]
  node [
    id 246
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 247
    label "motyw"
  ]
  node [
    id 248
    label "przebiegni&#281;cie"
  ]
  node [
    id 249
    label "fabu&#322;a"
  ]
  node [
    id 250
    label "udowodnienie"
  ]
  node [
    id 251
    label "przebadanie"
  ]
  node [
    id 252
    label "skontrolowanie"
  ]
  node [
    id 253
    label "rozwa&#380;enie"
  ]
  node [
    id 254
    label "validation"
  ]
  node [
    id 255
    label "delivery"
  ]
  node [
    id 256
    label "oddzia&#322;anie"
  ]
  node [
    id 257
    label "zrobienie"
  ]
  node [
    id 258
    label "patrzenie"
  ]
  node [
    id 259
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 260
    label "doszukanie_si&#281;"
  ]
  node [
    id 261
    label "dostrzeganie"
  ]
  node [
    id 262
    label "poobserwowanie"
  ]
  node [
    id 263
    label "observation"
  ]
  node [
    id 264
    label "bocianie_gniazdo"
  ]
  node [
    id 265
    label "teren_szko&#322;y"
  ]
  node [
    id 266
    label "Mickiewicz"
  ]
  node [
    id 267
    label "kwalifikacje"
  ]
  node [
    id 268
    label "podr&#281;cznik"
  ]
  node [
    id 269
    label "absolwent"
  ]
  node [
    id 270
    label "praktyka"
  ]
  node [
    id 271
    label "school"
  ]
  node [
    id 272
    label "system"
  ]
  node [
    id 273
    label "zda&#263;"
  ]
  node [
    id 274
    label "gabinet"
  ]
  node [
    id 275
    label "urszulanki"
  ]
  node [
    id 276
    label "sztuba"
  ]
  node [
    id 277
    label "&#322;awa_szkolna"
  ]
  node [
    id 278
    label "nauka"
  ]
  node [
    id 279
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 280
    label "przepisa&#263;"
  ]
  node [
    id 281
    label "muzyka"
  ]
  node [
    id 282
    label "grupa"
  ]
  node [
    id 283
    label "form"
  ]
  node [
    id 284
    label "klasa"
  ]
  node [
    id 285
    label "lekcja"
  ]
  node [
    id 286
    label "metoda"
  ]
  node [
    id 287
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 288
    label "przepisanie"
  ]
  node [
    id 289
    label "skolaryzacja"
  ]
  node [
    id 290
    label "zdanie"
  ]
  node [
    id 291
    label "stopek"
  ]
  node [
    id 292
    label "sekretariat"
  ]
  node [
    id 293
    label "ideologia"
  ]
  node [
    id 294
    label "lesson"
  ]
  node [
    id 295
    label "instytucja"
  ]
  node [
    id 296
    label "niepokalanki"
  ]
  node [
    id 297
    label "siedziba"
  ]
  node [
    id 298
    label "szkolenie"
  ]
  node [
    id 299
    label "kara"
  ]
  node [
    id 300
    label "tablica"
  ]
  node [
    id 301
    label "gathering"
  ]
  node [
    id 302
    label "zawarcie"
  ]
  node [
    id 303
    label "znajomy"
  ]
  node [
    id 304
    label "powitanie"
  ]
  node [
    id 305
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 306
    label "spowodowanie"
  ]
  node [
    id 307
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 308
    label "znalezienie"
  ]
  node [
    id 309
    label "match"
  ]
  node [
    id 310
    label "employment"
  ]
  node [
    id 311
    label "po&#380;egnanie"
  ]
  node [
    id 312
    label "gather"
  ]
  node [
    id 313
    label "spotykanie"
  ]
  node [
    id 314
    label "spotkanie_si&#281;"
  ]
  node [
    id 315
    label "mini&#281;cie"
  ]
  node [
    id 316
    label "zaistnienie"
  ]
  node [
    id 317
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 318
    label "przebycie"
  ]
  node [
    id 319
    label "cruise"
  ]
  node [
    id 320
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 321
    label "przep&#322;ywanie"
  ]
  node [
    id 322
    label "zawdzi&#281;czanie"
  ]
  node [
    id 323
    label "uczynienie_dobra"
  ]
  node [
    id 324
    label "znajomo&#347;&#263;"
  ]
  node [
    id 325
    label "information"
  ]
  node [
    id 326
    label "kiperstwo"
  ]
  node [
    id 327
    label "traktowanie"
  ]
  node [
    id 328
    label "recognition"
  ]
  node [
    id 329
    label "czucie"
  ]
  node [
    id 330
    label "lot"
  ]
  node [
    id 331
    label "pr&#261;d"
  ]
  node [
    id 332
    label "przebieg"
  ]
  node [
    id 333
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 334
    label "k&#322;us"
  ]
  node [
    id 335
    label "si&#322;a"
  ]
  node [
    id 336
    label "cable"
  ]
  node [
    id 337
    label "lina"
  ]
  node [
    id 338
    label "way"
  ]
  node [
    id 339
    label "stan"
  ]
  node [
    id 340
    label "ch&#243;d"
  ]
  node [
    id 341
    label "current"
  ]
  node [
    id 342
    label "trasa"
  ]
  node [
    id 343
    label "progression"
  ]
  node [
    id 344
    label "rz&#261;d"
  ]
  node [
    id 345
    label "egzemplarz"
  ]
  node [
    id 346
    label "series"
  ]
  node [
    id 347
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 348
    label "uprawianie"
  ]
  node [
    id 349
    label "praca_rolnicza"
  ]
  node [
    id 350
    label "collection"
  ]
  node [
    id 351
    label "dane"
  ]
  node [
    id 352
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 353
    label "pakiet_klimatyczny"
  ]
  node [
    id 354
    label "poj&#281;cie"
  ]
  node [
    id 355
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 356
    label "sum"
  ]
  node [
    id 357
    label "album"
  ]
  node [
    id 358
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 359
    label "energia"
  ]
  node [
    id 360
    label "przep&#322;yw"
  ]
  node [
    id 361
    label "apparent_motion"
  ]
  node [
    id 362
    label "przyp&#322;yw"
  ]
  node [
    id 363
    label "electricity"
  ]
  node [
    id 364
    label "dreszcz"
  ]
  node [
    id 365
    label "ruch"
  ]
  node [
    id 366
    label "zjawisko"
  ]
  node [
    id 367
    label "parametr"
  ]
  node [
    id 368
    label "rozwi&#261;zanie"
  ]
  node [
    id 369
    label "wojsko"
  ]
  node [
    id 370
    label "cecha"
  ]
  node [
    id 371
    label "wuchta"
  ]
  node [
    id 372
    label "zaleta"
  ]
  node [
    id 373
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 374
    label "moment_si&#322;y"
  ]
  node [
    id 375
    label "mn&#243;stwo"
  ]
  node [
    id 376
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 377
    label "zdolno&#347;&#263;"
  ]
  node [
    id 378
    label "capacity"
  ]
  node [
    id 379
    label "magnitude"
  ]
  node [
    id 380
    label "potencja"
  ]
  node [
    id 381
    label "przemoc"
  ]
  node [
    id 382
    label "podr&#243;&#380;"
  ]
  node [
    id 383
    label "migracja"
  ]
  node [
    id 384
    label "hike"
  ]
  node [
    id 385
    label "przedmiot"
  ]
  node [
    id 386
    label "wyluzowanie"
  ]
  node [
    id 387
    label "skr&#281;tka"
  ]
  node [
    id 388
    label "pika-pina"
  ]
  node [
    id 389
    label "bom"
  ]
  node [
    id 390
    label "abaka"
  ]
  node [
    id 391
    label "wyluzowa&#263;"
  ]
  node [
    id 392
    label "bieg"
  ]
  node [
    id 393
    label "trot"
  ]
  node [
    id 394
    label "step"
  ]
  node [
    id 395
    label "lekkoatletyka"
  ]
  node [
    id 396
    label "konkurencja"
  ]
  node [
    id 397
    label "czerwona_kartka"
  ]
  node [
    id 398
    label "krok"
  ]
  node [
    id 399
    label "wy&#347;cig"
  ]
  node [
    id 400
    label "Ohio"
  ]
  node [
    id 401
    label "wci&#281;cie"
  ]
  node [
    id 402
    label "Nowy_York"
  ]
  node [
    id 403
    label "warstwa"
  ]
  node [
    id 404
    label "samopoczucie"
  ]
  node [
    id 405
    label "Illinois"
  ]
  node [
    id 406
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 407
    label "state"
  ]
  node [
    id 408
    label "Jukatan"
  ]
  node [
    id 409
    label "Kalifornia"
  ]
  node [
    id 410
    label "Wirginia"
  ]
  node [
    id 411
    label "wektor"
  ]
  node [
    id 412
    label "by&#263;"
  ]
  node [
    id 413
    label "Goa"
  ]
  node [
    id 414
    label "Teksas"
  ]
  node [
    id 415
    label "Waszyngton"
  ]
  node [
    id 416
    label "miejsce"
  ]
  node [
    id 417
    label "Massachusetts"
  ]
  node [
    id 418
    label "Alaska"
  ]
  node [
    id 419
    label "Arakan"
  ]
  node [
    id 420
    label "Hawaje"
  ]
  node [
    id 421
    label "Maryland"
  ]
  node [
    id 422
    label "punkt"
  ]
  node [
    id 423
    label "Michigan"
  ]
  node [
    id 424
    label "Arizona"
  ]
  node [
    id 425
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 426
    label "Georgia"
  ]
  node [
    id 427
    label "poziom"
  ]
  node [
    id 428
    label "Pensylwania"
  ]
  node [
    id 429
    label "shape"
  ]
  node [
    id 430
    label "Luizjana"
  ]
  node [
    id 431
    label "Nowy_Meksyk"
  ]
  node [
    id 432
    label "Alabama"
  ]
  node [
    id 433
    label "ilo&#347;&#263;"
  ]
  node [
    id 434
    label "Kansas"
  ]
  node [
    id 435
    label "Oregon"
  ]
  node [
    id 436
    label "Oklahoma"
  ]
  node [
    id 437
    label "Floryda"
  ]
  node [
    id 438
    label "jednostka_administracyjna"
  ]
  node [
    id 439
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 440
    label "droga"
  ]
  node [
    id 441
    label "infrastruktura"
  ]
  node [
    id 442
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 443
    label "w&#281;ze&#322;"
  ]
  node [
    id 444
    label "marszrutyzacja"
  ]
  node [
    id 445
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 446
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 447
    label "podbieg"
  ]
  node [
    id 448
    label "przybli&#380;enie"
  ]
  node [
    id 449
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 450
    label "kategoria"
  ]
  node [
    id 451
    label "szpaler"
  ]
  node [
    id 452
    label "lon&#380;a"
  ]
  node [
    id 453
    label "uporz&#261;dkowanie"
  ]
  node [
    id 454
    label "egzekutywa"
  ]
  node [
    id 455
    label "jednostka_systematyczna"
  ]
  node [
    id 456
    label "premier"
  ]
  node [
    id 457
    label "Londyn"
  ]
  node [
    id 458
    label "gabinet_cieni"
  ]
  node [
    id 459
    label "gromada"
  ]
  node [
    id 460
    label "number"
  ]
  node [
    id 461
    label "Konsulat"
  ]
  node [
    id 462
    label "tract"
  ]
  node [
    id 463
    label "w&#322;adza"
  ]
  node [
    id 464
    label "chronometra&#380;ysta"
  ]
  node [
    id 465
    label "odlot"
  ]
  node [
    id 466
    label "l&#261;dowanie"
  ]
  node [
    id 467
    label "start"
  ]
  node [
    id 468
    label "flight"
  ]
  node [
    id 469
    label "linia"
  ]
  node [
    id 470
    label "procedura"
  ]
  node [
    id 471
    label "proces"
  ]
  node [
    id 472
    label "room"
  ]
  node [
    id 473
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 474
    label "sequence"
  ]
  node [
    id 475
    label "cycle"
  ]
  node [
    id 476
    label "kolejny"
  ]
  node [
    id 477
    label "cz&#322;owiek"
  ]
  node [
    id 478
    label "niedawno"
  ]
  node [
    id 479
    label "poprzedni"
  ]
  node [
    id 480
    label "pozosta&#322;y"
  ]
  node [
    id 481
    label "ostatnio"
  ]
  node [
    id 482
    label "sko&#324;czony"
  ]
  node [
    id 483
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 484
    label "aktualny"
  ]
  node [
    id 485
    label "najgorszy"
  ]
  node [
    id 486
    label "istota_&#380;ywa"
  ]
  node [
    id 487
    label "w&#261;tpliwy"
  ]
  node [
    id 488
    label "nast&#281;pnie"
  ]
  node [
    id 489
    label "inny"
  ]
  node [
    id 490
    label "nastopny"
  ]
  node [
    id 491
    label "kolejno"
  ]
  node [
    id 492
    label "kt&#243;ry&#347;"
  ]
  node [
    id 493
    label "przesz&#322;y"
  ]
  node [
    id 494
    label "wcze&#347;niejszy"
  ]
  node [
    id 495
    label "poprzednio"
  ]
  node [
    id 496
    label "w&#261;tpliwie"
  ]
  node [
    id 497
    label "pozorny"
  ]
  node [
    id 498
    label "&#380;ywy"
  ]
  node [
    id 499
    label "ostateczny"
  ]
  node [
    id 500
    label "wa&#380;ny"
  ]
  node [
    id 501
    label "ludzko&#347;&#263;"
  ]
  node [
    id 502
    label "asymilowanie"
  ]
  node [
    id 503
    label "wapniak"
  ]
  node [
    id 504
    label "asymilowa&#263;"
  ]
  node [
    id 505
    label "os&#322;abia&#263;"
  ]
  node [
    id 506
    label "posta&#263;"
  ]
  node [
    id 507
    label "hominid"
  ]
  node [
    id 508
    label "podw&#322;adny"
  ]
  node [
    id 509
    label "os&#322;abianie"
  ]
  node [
    id 510
    label "figura"
  ]
  node [
    id 511
    label "portrecista"
  ]
  node [
    id 512
    label "dwun&#243;g"
  ]
  node [
    id 513
    label "profanum"
  ]
  node [
    id 514
    label "mikrokosmos"
  ]
  node [
    id 515
    label "nasada"
  ]
  node [
    id 516
    label "duch"
  ]
  node [
    id 517
    label "antropochoria"
  ]
  node [
    id 518
    label "osoba"
  ]
  node [
    id 519
    label "wz&#243;r"
  ]
  node [
    id 520
    label "senior"
  ]
  node [
    id 521
    label "oddzia&#322;ywanie"
  ]
  node [
    id 522
    label "Adam"
  ]
  node [
    id 523
    label "homo_sapiens"
  ]
  node [
    id 524
    label "polifag"
  ]
  node [
    id 525
    label "wykszta&#322;cony"
  ]
  node [
    id 526
    label "dyplomowany"
  ]
  node [
    id 527
    label "wykwalifikowany"
  ]
  node [
    id 528
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 529
    label "kompletny"
  ]
  node [
    id 530
    label "sko&#324;czenie"
  ]
  node [
    id 531
    label "okre&#347;lony"
  ]
  node [
    id 532
    label "wielki"
  ]
  node [
    id 533
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 534
    label "aktualnie"
  ]
  node [
    id 535
    label "aktualizowanie"
  ]
  node [
    id 536
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 537
    label "uaktualnienie"
  ]
  node [
    id 538
    label "weekend"
  ]
  node [
    id 539
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 540
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 541
    label "miesi&#261;c"
  ]
  node [
    id 542
    label "poprzedzanie"
  ]
  node [
    id 543
    label "czasoprzestrze&#324;"
  ]
  node [
    id 544
    label "laba"
  ]
  node [
    id 545
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 546
    label "chronometria"
  ]
  node [
    id 547
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 548
    label "rachuba_czasu"
  ]
  node [
    id 549
    label "czasokres"
  ]
  node [
    id 550
    label "odczyt"
  ]
  node [
    id 551
    label "chwila"
  ]
  node [
    id 552
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 553
    label "dzieje"
  ]
  node [
    id 554
    label "kategoria_gramatyczna"
  ]
  node [
    id 555
    label "poprzedzenie"
  ]
  node [
    id 556
    label "trawienie"
  ]
  node [
    id 557
    label "pochodzi&#263;"
  ]
  node [
    id 558
    label "period"
  ]
  node [
    id 559
    label "okres_czasu"
  ]
  node [
    id 560
    label "poprzedza&#263;"
  ]
  node [
    id 561
    label "schy&#322;ek"
  ]
  node [
    id 562
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 563
    label "odwlekanie_si&#281;"
  ]
  node [
    id 564
    label "zegar"
  ]
  node [
    id 565
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 566
    label "czwarty_wymiar"
  ]
  node [
    id 567
    label "pochodzenie"
  ]
  node [
    id 568
    label "koniugacja"
  ]
  node [
    id 569
    label "Zeitgeist"
  ]
  node [
    id 570
    label "trawi&#263;"
  ]
  node [
    id 571
    label "pogoda"
  ]
  node [
    id 572
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 573
    label "poprzedzi&#263;"
  ]
  node [
    id 574
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 575
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 576
    label "time_period"
  ]
  node [
    id 577
    label "niedziela"
  ]
  node [
    id 578
    label "sobota"
  ]
  node [
    id 579
    label "miech"
  ]
  node [
    id 580
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 581
    label "rok"
  ]
  node [
    id 582
    label "kalendy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 21
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 293
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 295
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 217
  ]
  edge [
    source 11
    target 246
  ]
  edge [
    source 11
    target 247
  ]
  edge [
    source 11
    target 248
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 210
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 22
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 21
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 321
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 567
  ]
  edge [
    source 14
    target 568
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 570
  ]
  edge [
    source 14
    target 571
  ]
  edge [
    source 14
    target 572
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 574
  ]
  edge [
    source 14
    target 575
  ]
  edge [
    source 14
    target 576
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 14
    target 25
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 577
  ]
  edge [
    source 14
    target 578
  ]
  edge [
    source 14
    target 579
  ]
  edge [
    source 14
    target 580
  ]
  edge [
    source 14
    target 581
  ]
  edge [
    source 14
    target 582
  ]
]
