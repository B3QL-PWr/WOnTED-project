graph [
  node [
    id 0
    label "bochum"
    origin "text"
  ]
  node [
    id 1
    label "jarmark"
    origin "text"
  ]
  node [
    id 2
    label "bo&#380;onarodzeniowy"
    origin "text"
  ]
  node [
    id 3
    label "opisa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wraz"
    origin "text"
  ]
  node [
    id 5
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 6
    label "informacja"
    origin "text"
  ]
  node [
    id 7
    label "praktyczny"
    origin "text"
  ]
  node [
    id 8
    label "taki"
    origin "text"
  ]
  node [
    id 9
    label "termin"
    origin "text"
  ]
  node [
    id 10
    label "cena"
    origin "text"
  ]
  node [
    id 11
    label "stoisko"
    origin "text"
  ]
  node [
    id 12
    label "miejsce"
    origin "text"
  ]
  node [
    id 13
    label "gdzie"
    origin "text"
  ]
  node [
    id 14
    label "si&#281;"
    origin "text"
  ]
  node [
    id 15
    label "odbywa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "obiekt_handlowy"
  ]
  node [
    id 17
    label "plac"
  ]
  node [
    id 18
    label "targowica"
  ]
  node [
    id 19
    label "targ"
  ]
  node [
    id 20
    label "kram"
  ]
  node [
    id 21
    label "market"
  ]
  node [
    id 22
    label "przestrze&#324;"
  ]
  node [
    id 23
    label "miasto"
  ]
  node [
    id 24
    label "area"
  ]
  node [
    id 25
    label "obszar"
  ]
  node [
    id 26
    label "pole_bitwy"
  ]
  node [
    id 27
    label "&#321;ubianka"
  ]
  node [
    id 28
    label "zgromadzenie"
  ]
  node [
    id 29
    label "Majdan"
  ]
  node [
    id 30
    label "pierzeja"
  ]
  node [
    id 31
    label "sprzeda&#380;"
  ]
  node [
    id 32
    label "kramnica"
  ]
  node [
    id 33
    label "szmartuz"
  ]
  node [
    id 34
    label "zdrada"
  ]
  node [
    id 35
    label "sklep"
  ]
  node [
    id 36
    label "delineate"
  ]
  node [
    id 37
    label "zapozna&#263;"
  ]
  node [
    id 38
    label "zinterpretowa&#263;"
  ]
  node [
    id 39
    label "relate"
  ]
  node [
    id 40
    label "obznajomi&#263;"
  ]
  node [
    id 41
    label "teach"
  ]
  node [
    id 42
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 43
    label "poinformowa&#263;"
  ]
  node [
    id 44
    label "pozna&#263;"
  ]
  node [
    id 45
    label "insert"
  ]
  node [
    id 46
    label "zawrze&#263;"
  ]
  node [
    id 47
    label "think"
  ]
  node [
    id 48
    label "oceni&#263;"
  ]
  node [
    id 49
    label "illustrate"
  ]
  node [
    id 50
    label "zanalizowa&#263;"
  ]
  node [
    id 51
    label "zagra&#263;"
  ]
  node [
    id 52
    label "read"
  ]
  node [
    id 53
    label "punkt"
  ]
  node [
    id 54
    label "powzi&#281;cie"
  ]
  node [
    id 55
    label "obieganie"
  ]
  node [
    id 56
    label "sygna&#322;"
  ]
  node [
    id 57
    label "obiec"
  ]
  node [
    id 58
    label "doj&#347;&#263;"
  ]
  node [
    id 59
    label "wiedza"
  ]
  node [
    id 60
    label "publikacja"
  ]
  node [
    id 61
    label "powzi&#261;&#263;"
  ]
  node [
    id 62
    label "doj&#347;cie"
  ]
  node [
    id 63
    label "obiega&#263;"
  ]
  node [
    id 64
    label "obiegni&#281;cie"
  ]
  node [
    id 65
    label "dane"
  ]
  node [
    id 66
    label "obiekt_matematyczny"
  ]
  node [
    id 67
    label "stopie&#324;_pisma"
  ]
  node [
    id 68
    label "pozycja"
  ]
  node [
    id 69
    label "problemat"
  ]
  node [
    id 70
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 71
    label "obiekt"
  ]
  node [
    id 72
    label "point"
  ]
  node [
    id 73
    label "plamka"
  ]
  node [
    id 74
    label "mark"
  ]
  node [
    id 75
    label "ust&#281;p"
  ]
  node [
    id 76
    label "po&#322;o&#380;enie"
  ]
  node [
    id 77
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 78
    label "kres"
  ]
  node [
    id 79
    label "plan"
  ]
  node [
    id 80
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 81
    label "chwila"
  ]
  node [
    id 82
    label "podpunkt"
  ]
  node [
    id 83
    label "jednostka"
  ]
  node [
    id 84
    label "sprawa"
  ]
  node [
    id 85
    label "problematyka"
  ]
  node [
    id 86
    label "prosta"
  ]
  node [
    id 87
    label "wojsko"
  ]
  node [
    id 88
    label "zapunktowa&#263;"
  ]
  node [
    id 89
    label "pozwolenie"
  ]
  node [
    id 90
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 91
    label "zaawansowanie"
  ]
  node [
    id 92
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 93
    label "intelekt"
  ]
  node [
    id 94
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 95
    label "wykszta&#322;cenie"
  ]
  node [
    id 96
    label "cognition"
  ]
  node [
    id 97
    label "pulsation"
  ]
  node [
    id 98
    label "d&#378;wi&#281;k"
  ]
  node [
    id 99
    label "wizja"
  ]
  node [
    id 100
    label "fala"
  ]
  node [
    id 101
    label "czynnik"
  ]
  node [
    id 102
    label "modulacja"
  ]
  node [
    id 103
    label "po&#322;&#261;czenie"
  ]
  node [
    id 104
    label "przewodzenie"
  ]
  node [
    id 105
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 106
    label "demodulacja"
  ]
  node [
    id 107
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 108
    label "medium_transmisyjne"
  ]
  node [
    id 109
    label "drift"
  ]
  node [
    id 110
    label "przekazywa&#263;"
  ]
  node [
    id 111
    label "przekazywanie"
  ]
  node [
    id 112
    label "aliasing"
  ]
  node [
    id 113
    label "znak"
  ]
  node [
    id 114
    label "przekazanie"
  ]
  node [
    id 115
    label "przewodzi&#263;"
  ]
  node [
    id 116
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 117
    label "przekaza&#263;"
  ]
  node [
    id 118
    label "zapowied&#378;"
  ]
  node [
    id 119
    label "druk"
  ]
  node [
    id 120
    label "produkcja"
  ]
  node [
    id 121
    label "tekst"
  ]
  node [
    id 122
    label "notification"
  ]
  node [
    id 123
    label "jednostka_informacji"
  ]
  node [
    id 124
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 125
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 126
    label "pakowanie"
  ]
  node [
    id 127
    label "edytowanie"
  ]
  node [
    id 128
    label "sekwencjonowa&#263;"
  ]
  node [
    id 129
    label "zbi&#243;r"
  ]
  node [
    id 130
    label "rozpakowywanie"
  ]
  node [
    id 131
    label "rozpakowa&#263;"
  ]
  node [
    id 132
    label "nap&#322;ywanie"
  ]
  node [
    id 133
    label "spakowa&#263;"
  ]
  node [
    id 134
    label "edytowa&#263;"
  ]
  node [
    id 135
    label "evidence"
  ]
  node [
    id 136
    label "sekwencjonowanie"
  ]
  node [
    id 137
    label "rozpakowanie"
  ]
  node [
    id 138
    label "wyci&#261;ganie"
  ]
  node [
    id 139
    label "korelator"
  ]
  node [
    id 140
    label "rekord"
  ]
  node [
    id 141
    label "spakowanie"
  ]
  node [
    id 142
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 143
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 144
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 145
    label "konwersja"
  ]
  node [
    id 146
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 147
    label "rozpakowywa&#263;"
  ]
  node [
    id 148
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 149
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 150
    label "pakowa&#263;"
  ]
  node [
    id 151
    label "spowodowanie"
  ]
  node [
    id 152
    label "przesy&#322;ka"
  ]
  node [
    id 153
    label "dolecenie"
  ]
  node [
    id 154
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 155
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 156
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 157
    label "avenue"
  ]
  node [
    id 158
    label "dochrapanie_si&#281;"
  ]
  node [
    id 159
    label "dochodzenie"
  ]
  node [
    id 160
    label "dor&#281;czenie"
  ]
  node [
    id 161
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 162
    label "dop&#322;ata"
  ]
  node [
    id 163
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 164
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 165
    label "dojrza&#322;y"
  ]
  node [
    id 166
    label "dodatek"
  ]
  node [
    id 167
    label "stanie_si&#281;"
  ]
  node [
    id 168
    label "gotowy"
  ]
  node [
    id 169
    label "orgazm"
  ]
  node [
    id 170
    label "dojechanie"
  ]
  node [
    id 171
    label "czynno&#347;&#263;"
  ]
  node [
    id 172
    label "skill"
  ]
  node [
    id 173
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 174
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 175
    label "znajomo&#347;ci"
  ]
  node [
    id 176
    label "strzelenie"
  ]
  node [
    id 177
    label "ingress"
  ]
  node [
    id 178
    label "powi&#261;zanie"
  ]
  node [
    id 179
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 180
    label "uzyskanie"
  ]
  node [
    id 181
    label "affiliation"
  ]
  node [
    id 182
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 183
    label "entrance"
  ]
  node [
    id 184
    label "bodziec"
  ]
  node [
    id 185
    label "doznanie"
  ]
  node [
    id 186
    label "postrzeganie"
  ]
  node [
    id 187
    label "dost&#281;p"
  ]
  node [
    id 188
    label "rozpowszechnienie"
  ]
  node [
    id 189
    label "zrobienie"
  ]
  node [
    id 190
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 191
    label "orzekni&#281;cie"
  ]
  node [
    id 192
    label "odwiedzi&#263;"
  ]
  node [
    id 193
    label "rozpowszechni&#263;_si&#281;"
  ]
  node [
    id 194
    label "orb"
  ]
  node [
    id 195
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 196
    label "zaj&#347;&#263;"
  ]
  node [
    id 197
    label "heed"
  ]
  node [
    id 198
    label "uzyska&#263;"
  ]
  node [
    id 199
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 200
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 201
    label "postrzega&#263;"
  ]
  node [
    id 202
    label "drive"
  ]
  node [
    id 203
    label "sta&#263;_si&#281;"
  ]
  node [
    id 204
    label "dokoptowa&#263;"
  ]
  node [
    id 205
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 206
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 207
    label "become"
  ]
  node [
    id 208
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 209
    label "get"
  ]
  node [
    id 210
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 211
    label "spowodowa&#263;"
  ]
  node [
    id 212
    label "dozna&#263;"
  ]
  node [
    id 213
    label "dolecie&#263;"
  ]
  node [
    id 214
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 215
    label "supervene"
  ]
  node [
    id 216
    label "dotrze&#263;"
  ]
  node [
    id 217
    label "catch"
  ]
  node [
    id 218
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 219
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 220
    label "otrzyma&#263;"
  ]
  node [
    id 221
    label "podj&#261;&#263;"
  ]
  node [
    id 222
    label "zacz&#261;&#263;"
  ]
  node [
    id 223
    label "odwiedza&#263;"
  ]
  node [
    id 224
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 225
    label "flow"
  ]
  node [
    id 226
    label "authorize"
  ]
  node [
    id 227
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 228
    label "rotate"
  ]
  node [
    id 229
    label "biegni&#281;cie"
  ]
  node [
    id 230
    label "odwiedzanie"
  ]
  node [
    id 231
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 232
    label "okr&#261;&#380;anie"
  ]
  node [
    id 233
    label "zakre&#347;lanie"
  ]
  node [
    id 234
    label "zakre&#347;lenie"
  ]
  node [
    id 235
    label "rozpowszechnienie_si&#281;"
  ]
  node [
    id 236
    label "okr&#261;&#380;enie"
  ]
  node [
    id 237
    label "odwiedzenie"
  ]
  node [
    id 238
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 239
    label "otrzymanie"
  ]
  node [
    id 240
    label "podj&#281;cie"
  ]
  node [
    id 241
    label "u&#380;yteczny"
  ]
  node [
    id 242
    label "praktycznie"
  ]
  node [
    id 243
    label "racjonalny"
  ]
  node [
    id 244
    label "przydatny"
  ]
  node [
    id 245
    label "wykorzystywanie"
  ]
  node [
    id 246
    label "wyzyskanie"
  ]
  node [
    id 247
    label "u&#380;ytecznie"
  ]
  node [
    id 248
    label "przydanie_si&#281;"
  ]
  node [
    id 249
    label "przydawanie_si&#281;"
  ]
  node [
    id 250
    label "pragmatyczny"
  ]
  node [
    id 251
    label "rozumowy"
  ]
  node [
    id 252
    label "racjonalnie"
  ]
  node [
    id 253
    label "rozs&#261;dny"
  ]
  node [
    id 254
    label "jaki&#347;"
  ]
  node [
    id 255
    label "okre&#347;lony"
  ]
  node [
    id 256
    label "jako&#347;"
  ]
  node [
    id 257
    label "niez&#322;y"
  ]
  node [
    id 258
    label "charakterystyczny"
  ]
  node [
    id 259
    label "jako_tako"
  ]
  node [
    id 260
    label "ciekawy"
  ]
  node [
    id 261
    label "dziwny"
  ]
  node [
    id 262
    label "przyzwoity"
  ]
  node [
    id 263
    label "wiadomy"
  ]
  node [
    id 264
    label "term"
  ]
  node [
    id 265
    label "ekspiracja"
  ]
  node [
    id 266
    label "praktyka"
  ]
  node [
    id 267
    label "chronogram"
  ]
  node [
    id 268
    label "przypa&#347;&#263;"
  ]
  node [
    id 269
    label "nazewnictwo"
  ]
  node [
    id 270
    label "nazwa"
  ]
  node [
    id 271
    label "przypadni&#281;cie"
  ]
  node [
    id 272
    label "czas"
  ]
  node [
    id 273
    label "leksem"
  ]
  node [
    id 274
    label "patron"
  ]
  node [
    id 275
    label "wezwanie"
  ]
  node [
    id 276
    label "chronometria"
  ]
  node [
    id 277
    label "odczyt"
  ]
  node [
    id 278
    label "laba"
  ]
  node [
    id 279
    label "czasoprzestrze&#324;"
  ]
  node [
    id 280
    label "time_period"
  ]
  node [
    id 281
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 282
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 283
    label "Zeitgeist"
  ]
  node [
    id 284
    label "pochodzenie"
  ]
  node [
    id 285
    label "przep&#322;ywanie"
  ]
  node [
    id 286
    label "schy&#322;ek"
  ]
  node [
    id 287
    label "czwarty_wymiar"
  ]
  node [
    id 288
    label "kategoria_gramatyczna"
  ]
  node [
    id 289
    label "poprzedzi&#263;"
  ]
  node [
    id 290
    label "pogoda"
  ]
  node [
    id 291
    label "czasokres"
  ]
  node [
    id 292
    label "poprzedzenie"
  ]
  node [
    id 293
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 294
    label "dzieje"
  ]
  node [
    id 295
    label "zegar"
  ]
  node [
    id 296
    label "koniugacja"
  ]
  node [
    id 297
    label "trawi&#263;"
  ]
  node [
    id 298
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 299
    label "poprzedza&#263;"
  ]
  node [
    id 300
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 301
    label "trawienie"
  ]
  node [
    id 302
    label "rachuba_czasu"
  ]
  node [
    id 303
    label "poprzedzanie"
  ]
  node [
    id 304
    label "okres_czasu"
  ]
  node [
    id 305
    label "period"
  ]
  node [
    id 306
    label "odwlekanie_si&#281;"
  ]
  node [
    id 307
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 308
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 309
    label "pochodzi&#263;"
  ]
  node [
    id 310
    label "czyn"
  ]
  node [
    id 311
    label "znawstwo"
  ]
  node [
    id 312
    label "zwyczaj"
  ]
  node [
    id 313
    label "praca"
  ]
  node [
    id 314
    label "practice"
  ]
  node [
    id 315
    label "eksperiencja"
  ]
  node [
    id 316
    label "nauka"
  ]
  node [
    id 317
    label "s&#322;ownictwo"
  ]
  node [
    id 318
    label "terminology"
  ]
  node [
    id 319
    label "poj&#281;cie"
  ]
  node [
    id 320
    label "rzuci&#263;_si&#281;"
  ]
  node [
    id 321
    label "trafi&#263;_si&#281;"
  ]
  node [
    id 322
    label "fall"
  ]
  node [
    id 323
    label "pa&#347;&#263;"
  ]
  node [
    id 324
    label "wypa&#347;&#263;"
  ]
  node [
    id 325
    label "przywrze&#263;"
  ]
  node [
    id 326
    label "spodoba&#263;_si&#281;"
  ]
  node [
    id 327
    label "wydech"
  ]
  node [
    id 328
    label "ekspirowanie"
  ]
  node [
    id 329
    label "barok"
  ]
  node [
    id 330
    label "zapis"
  ]
  node [
    id 331
    label "przytulenie_si&#281;"
  ]
  node [
    id 332
    label "okrojenie_si&#281;"
  ]
  node [
    id 333
    label "zdarzenie_si&#281;"
  ]
  node [
    id 334
    label "prolapse"
  ]
  node [
    id 335
    label "spadni&#281;cie"
  ]
  node [
    id 336
    label "wyceni&#263;"
  ]
  node [
    id 337
    label "kosztowa&#263;"
  ]
  node [
    id 338
    label "wycenienie"
  ]
  node [
    id 339
    label "worth"
  ]
  node [
    id 340
    label "cenowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 341
    label "dyskryminacja_cenowa"
  ]
  node [
    id 342
    label "kupowanie"
  ]
  node [
    id 343
    label "inflacja"
  ]
  node [
    id 344
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 345
    label "warto&#347;&#263;"
  ]
  node [
    id 346
    label "kosztowanie"
  ]
  node [
    id 347
    label "zmienna"
  ]
  node [
    id 348
    label "wskazywanie"
  ]
  node [
    id 349
    label "korzy&#347;&#263;"
  ]
  node [
    id 350
    label "rewaluowanie"
  ]
  node [
    id 351
    label "zrewaluowa&#263;"
  ]
  node [
    id 352
    label "cecha"
  ]
  node [
    id 353
    label "rewaluowa&#263;"
  ]
  node [
    id 354
    label "rozmiar"
  ]
  node [
    id 355
    label "cel"
  ]
  node [
    id 356
    label "wabik"
  ]
  node [
    id 357
    label "strona"
  ]
  node [
    id 358
    label "wskazywa&#263;"
  ]
  node [
    id 359
    label "zrewaluowanie"
  ]
  node [
    id 360
    label "policzenie"
  ]
  node [
    id 361
    label "ustalenie"
  ]
  node [
    id 362
    label "appraisal"
  ]
  node [
    id 363
    label "proces_ekonomiczny"
  ]
  node [
    id 364
    label "wzrost"
  ]
  node [
    id 365
    label "kosmologia"
  ]
  node [
    id 366
    label "faza"
  ]
  node [
    id 367
    label "ewolucja_kosmosu"
  ]
  node [
    id 368
    label "handlowanie"
  ]
  node [
    id 369
    label "granie"
  ]
  node [
    id 370
    label "pozyskiwanie"
  ]
  node [
    id 371
    label "uznawanie"
  ]
  node [
    id 372
    label "importowanie"
  ]
  node [
    id 373
    label "wierzenie"
  ]
  node [
    id 374
    label "wkupywanie_si&#281;"
  ]
  node [
    id 375
    label "wkupienie_si&#281;"
  ]
  node [
    id 376
    label "purchase"
  ]
  node [
    id 377
    label "wykupywanie"
  ]
  node [
    id 378
    label "ustawianie"
  ]
  node [
    id 379
    label "kupienie"
  ]
  node [
    id 380
    label "buying"
  ]
  node [
    id 381
    label "bycie"
  ]
  node [
    id 382
    label "badanie"
  ]
  node [
    id 383
    label "tasting"
  ]
  node [
    id 384
    label "jedzenie"
  ]
  node [
    id 385
    label "zaznawanie"
  ]
  node [
    id 386
    label "kiperstwo"
  ]
  node [
    id 387
    label "policzy&#263;"
  ]
  node [
    id 388
    label "estimate"
  ]
  node [
    id 389
    label "ustali&#263;"
  ]
  node [
    id 390
    label "savor"
  ]
  node [
    id 391
    label "try"
  ]
  node [
    id 392
    label "essay"
  ]
  node [
    id 393
    label "doznawa&#263;"
  ]
  node [
    id 394
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 395
    label "by&#263;"
  ]
  node [
    id 396
    label "rz&#261;d"
  ]
  node [
    id 397
    label "uwaga"
  ]
  node [
    id 398
    label "location"
  ]
  node [
    id 399
    label "warunek_lokalowy"
  ]
  node [
    id 400
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 401
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 402
    label "cia&#322;o"
  ]
  node [
    id 403
    label "status"
  ]
  node [
    id 404
    label "charakterystyka"
  ]
  node [
    id 405
    label "m&#322;ot"
  ]
  node [
    id 406
    label "marka"
  ]
  node [
    id 407
    label "pr&#243;ba"
  ]
  node [
    id 408
    label "attribute"
  ]
  node [
    id 409
    label "drzewo"
  ]
  node [
    id 410
    label "stan"
  ]
  node [
    id 411
    label "wzgl&#261;d"
  ]
  node [
    id 412
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 413
    label "nagana"
  ]
  node [
    id 414
    label "upomnienie"
  ]
  node [
    id 415
    label "wypowied&#378;"
  ]
  node [
    id 416
    label "gossip"
  ]
  node [
    id 417
    label "dzienniczek"
  ]
  node [
    id 418
    label "Rzym_Zachodni"
  ]
  node [
    id 419
    label "Rzym_Wschodni"
  ]
  node [
    id 420
    label "element"
  ]
  node [
    id 421
    label "ilo&#347;&#263;"
  ]
  node [
    id 422
    label "whole"
  ]
  node [
    id 423
    label "urz&#261;dzenie"
  ]
  node [
    id 424
    label "zaw&#243;d"
  ]
  node [
    id 425
    label "zmiana"
  ]
  node [
    id 426
    label "pracowanie"
  ]
  node [
    id 427
    label "pracowa&#263;"
  ]
  node [
    id 428
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 429
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 430
    label "czynnik_produkcji"
  ]
  node [
    id 431
    label "stosunek_pracy"
  ]
  node [
    id 432
    label "kierownictwo"
  ]
  node [
    id 433
    label "najem"
  ]
  node [
    id 434
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 435
    label "siedziba"
  ]
  node [
    id 436
    label "zak&#322;ad"
  ]
  node [
    id 437
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 438
    label "tynkarski"
  ]
  node [
    id 439
    label "tyrka"
  ]
  node [
    id 440
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 441
    label "benedykty&#324;ski"
  ]
  node [
    id 442
    label "poda&#380;_pracy"
  ]
  node [
    id 443
    label "wytw&#243;r"
  ]
  node [
    id 444
    label "zobowi&#261;zanie"
  ]
  node [
    id 445
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 446
    label "oktant"
  ]
  node [
    id 447
    label "przedzielenie"
  ]
  node [
    id 448
    label "przedzieli&#263;"
  ]
  node [
    id 449
    label "przestw&#243;r"
  ]
  node [
    id 450
    label "rozdziela&#263;"
  ]
  node [
    id 451
    label "nielito&#347;ciwy"
  ]
  node [
    id 452
    label "niezmierzony"
  ]
  node [
    id 453
    label "bezbrze&#380;e"
  ]
  node [
    id 454
    label "rozdzielanie"
  ]
  node [
    id 455
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 456
    label "awansowa&#263;"
  ]
  node [
    id 457
    label "podmiotowo"
  ]
  node [
    id 458
    label "awans"
  ]
  node [
    id 459
    label "condition"
  ]
  node [
    id 460
    label "znaczenie"
  ]
  node [
    id 461
    label "awansowanie"
  ]
  node [
    id 462
    label "sytuacja"
  ]
  node [
    id 463
    label "time"
  ]
  node [
    id 464
    label "liczba"
  ]
  node [
    id 465
    label "circumference"
  ]
  node [
    id 466
    label "cyrkumferencja"
  ]
  node [
    id 467
    label "tanatoplastyk"
  ]
  node [
    id 468
    label "odwadnianie"
  ]
  node [
    id 469
    label "Komitet_Region&#243;w"
  ]
  node [
    id 470
    label "tanatoplastyka"
  ]
  node [
    id 471
    label "odwodni&#263;"
  ]
  node [
    id 472
    label "pochowanie"
  ]
  node [
    id 473
    label "ty&#322;"
  ]
  node [
    id 474
    label "zabalsamowanie"
  ]
  node [
    id 475
    label "biorytm"
  ]
  node [
    id 476
    label "unerwienie"
  ]
  node [
    id 477
    label "istota_&#380;ywa"
  ]
  node [
    id 478
    label "nieumar&#322;y"
  ]
  node [
    id 479
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 480
    label "uk&#322;ad"
  ]
  node [
    id 481
    label "balsamowanie"
  ]
  node [
    id 482
    label "balsamowa&#263;"
  ]
  node [
    id 483
    label "sekcja"
  ]
  node [
    id 484
    label "sk&#243;ra"
  ]
  node [
    id 485
    label "pochowa&#263;"
  ]
  node [
    id 486
    label "odwodnienie"
  ]
  node [
    id 487
    label "otwieranie"
  ]
  node [
    id 488
    label "materia"
  ]
  node [
    id 489
    label "cz&#322;onek"
  ]
  node [
    id 490
    label "mi&#281;so"
  ]
  node [
    id 491
    label "temperatura"
  ]
  node [
    id 492
    label "ekshumowanie"
  ]
  node [
    id 493
    label "p&#322;aszczyzna"
  ]
  node [
    id 494
    label "pogrzeb"
  ]
  node [
    id 495
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 496
    label "kremacja"
  ]
  node [
    id 497
    label "otworzy&#263;"
  ]
  node [
    id 498
    label "odwadnia&#263;"
  ]
  node [
    id 499
    label "staw"
  ]
  node [
    id 500
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 501
    label "prz&#243;d"
  ]
  node [
    id 502
    label "szkielet"
  ]
  node [
    id 503
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 504
    label "ow&#322;osienie"
  ]
  node [
    id 505
    label "otworzenie"
  ]
  node [
    id 506
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 507
    label "l&#281;d&#378;wie"
  ]
  node [
    id 508
    label "otwiera&#263;"
  ]
  node [
    id 509
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 510
    label "Izba_Konsyliarska"
  ]
  node [
    id 511
    label "zesp&#243;&#322;"
  ]
  node [
    id 512
    label "ekshumowa&#263;"
  ]
  node [
    id 513
    label "zabalsamowa&#263;"
  ]
  node [
    id 514
    label "jednostka_organizacyjna"
  ]
  node [
    id 515
    label "szpaler"
  ]
  node [
    id 516
    label "number"
  ]
  node [
    id 517
    label "Londyn"
  ]
  node [
    id 518
    label "przybli&#380;enie"
  ]
  node [
    id 519
    label "premier"
  ]
  node [
    id 520
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 521
    label "tract"
  ]
  node [
    id 522
    label "uporz&#261;dkowanie"
  ]
  node [
    id 523
    label "egzekutywa"
  ]
  node [
    id 524
    label "klasa"
  ]
  node [
    id 525
    label "w&#322;adza"
  ]
  node [
    id 526
    label "Konsulat"
  ]
  node [
    id 527
    label "gabinet_cieni"
  ]
  node [
    id 528
    label "lon&#380;a"
  ]
  node [
    id 529
    label "gromada"
  ]
  node [
    id 530
    label "jednostka_systematyczna"
  ]
  node [
    id 531
    label "kategoria"
  ]
  node [
    id 532
    label "instytucja"
  ]
  node [
    id 533
    label "przechodzi&#263;"
  ]
  node [
    id 534
    label "hold"
  ]
  node [
    id 535
    label "uczestniczy&#263;"
  ]
  node [
    id 536
    label "move"
  ]
  node [
    id 537
    label "proceed"
  ]
  node [
    id 538
    label "conflict"
  ]
  node [
    id 539
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 540
    label "mie&#263;_miejsce"
  ]
  node [
    id 541
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 542
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 543
    label "przebywa&#263;"
  ]
  node [
    id 544
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 545
    label "continue"
  ]
  node [
    id 546
    label "go"
  ]
  node [
    id 547
    label "mija&#263;"
  ]
  node [
    id 548
    label "przerabia&#263;"
  ]
  node [
    id 549
    label "zmienia&#263;"
  ]
  node [
    id 550
    label "saturate"
  ]
  node [
    id 551
    label "przestawa&#263;"
  ]
  node [
    id 552
    label "zaczyna&#263;"
  ]
  node [
    id 553
    label "pass"
  ]
  node [
    id 554
    label "test"
  ]
  node [
    id 555
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 556
    label "zalicza&#263;"
  ]
  node [
    id 557
    label "i&#347;&#263;"
  ]
  node [
    id 558
    label "podlega&#263;"
  ]
  node [
    id 559
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 560
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 561
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 562
    label "robi&#263;"
  ]
  node [
    id 563
    label "participate"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 22
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 313
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 404
  ]
  edge [
    source 12
    target 405
  ]
  edge [
    source 12
    target 406
  ]
  edge [
    source 12
    target 407
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 12
    target 413
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 418
  ]
  edge [
    source 12
    target 419
  ]
  edge [
    source 12
    target 420
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 53
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 24
  ]
  edge [
    source 12
    target 25
  ]
  edge [
    source 12
    target 26
  ]
  edge [
    source 12
    target 27
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 28
  ]
  edge [
    source 12
    target 29
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 533
  ]
  edge [
    source 15
    target 534
  ]
  edge [
    source 15
    target 535
  ]
  edge [
    source 15
    target 536
  ]
  edge [
    source 15
    target 537
  ]
  edge [
    source 15
    target 538
  ]
  edge [
    source 15
    target 393
  ]
  edge [
    source 15
    target 539
  ]
  edge [
    source 15
    target 540
  ]
  edge [
    source 15
    target 541
  ]
  edge [
    source 15
    target 542
  ]
  edge [
    source 15
    target 543
  ]
  edge [
    source 15
    target 544
  ]
  edge [
    source 15
    target 545
  ]
  edge [
    source 15
    target 546
  ]
  edge [
    source 15
    target 547
  ]
  edge [
    source 15
    target 548
  ]
  edge [
    source 15
    target 549
  ]
  edge [
    source 15
    target 550
  ]
  edge [
    source 15
    target 551
  ]
  edge [
    source 15
    target 552
  ]
  edge [
    source 15
    target 553
  ]
  edge [
    source 15
    target 554
  ]
  edge [
    source 15
    target 555
  ]
  edge [
    source 15
    target 556
  ]
  edge [
    source 15
    target 557
  ]
  edge [
    source 15
    target 558
  ]
  edge [
    source 15
    target 559
  ]
  edge [
    source 15
    target 560
  ]
  edge [
    source 15
    target 561
  ]
  edge [
    source 15
    target 562
  ]
  edge [
    source 15
    target 395
  ]
  edge [
    source 15
    target 563
  ]
]
