graph [
  node [
    id 0
    label "bloomsbury"
    origin "text"
  ]
  node [
    id 1
    label "market"
    origin "text"
  ]
  node [
    id 2
    label "sklep"
  ]
  node [
    id 3
    label "p&#243;&#322;ka"
  ]
  node [
    id 4
    label "firma"
  ]
  node [
    id 5
    label "stoisko"
  ]
  node [
    id 6
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 7
    label "sk&#322;ad"
  ]
  node [
    id 8
    label "obiekt_handlowy"
  ]
  node [
    id 9
    label "zaplecze"
  ]
  node [
    id 10
    label "witryna"
  ]
  node [
    id 11
    label "Bloomsbury"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
]
