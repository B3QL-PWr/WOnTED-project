graph [
  node [
    id 0
    label "prokuratura"
    origin "text"
  ]
  node [
    id 1
    label "ropczyce"
    origin "text"
  ]
  node [
    id 2
    label "podkarpacie"
    origin "text"
  ]
  node [
    id 3
    label "wszcz&#261;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "post&#281;powanie"
    origin "text"
  ]
  node [
    id 5
    label "sprawa"
    origin "text"
  ]
  node [
    id 6
    label "por&#243;d"
    origin "text"
  ]
  node [
    id 7
    label "jak"
    origin "text"
  ]
  node [
    id 8
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 9
    label "miejsce"
    origin "text"
  ]
  node [
    id 10
    label "teren"
    origin "text"
  ]
  node [
    id 11
    label "tamtejszy"
    origin "text"
  ]
  node [
    id 12
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 13
    label "zdrowie"
    origin "text"
  ]
  node [
    id 14
    label "urz&#261;d"
  ]
  node [
    id 15
    label "siedziba"
  ]
  node [
    id 16
    label "organ"
  ]
  node [
    id 17
    label "miejsce_pracy"
  ]
  node [
    id 18
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 19
    label "budynek"
  ]
  node [
    id 20
    label "&#321;ubianka"
  ]
  node [
    id 21
    label "Bia&#322;y_Dom"
  ]
  node [
    id 22
    label "dzia&#322;_personalny"
  ]
  node [
    id 23
    label "Kreml"
  ]
  node [
    id 24
    label "sadowisko"
  ]
  node [
    id 25
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 26
    label "uk&#322;ad"
  ]
  node [
    id 27
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 28
    label "Komitet_Region&#243;w"
  ]
  node [
    id 29
    label "struktura_anatomiczna"
  ]
  node [
    id 30
    label "organogeneza"
  ]
  node [
    id 31
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 32
    label "tw&#243;r"
  ]
  node [
    id 33
    label "tkanka"
  ]
  node [
    id 34
    label "stomia"
  ]
  node [
    id 35
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 36
    label "budowa"
  ]
  node [
    id 37
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 38
    label "okolica"
  ]
  node [
    id 39
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 40
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 41
    label "dekortykacja"
  ]
  node [
    id 42
    label "Izba_Konsyliarska"
  ]
  node [
    id 43
    label "zesp&#243;&#322;"
  ]
  node [
    id 44
    label "jednostka_organizacyjna"
  ]
  node [
    id 45
    label "dzia&#322;"
  ]
  node [
    id 46
    label "mianowaniec"
  ]
  node [
    id 47
    label "okienko"
  ]
  node [
    id 48
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 49
    label "position"
  ]
  node [
    id 50
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 51
    label "stanowisko"
  ]
  node [
    id 52
    label "w&#322;adza"
  ]
  node [
    id 53
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 54
    label "instytucja"
  ]
  node [
    id 55
    label "zrobi&#263;"
  ]
  node [
    id 56
    label "zacz&#261;&#263;"
  ]
  node [
    id 57
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 58
    label "spowodowa&#263;"
  ]
  node [
    id 59
    label "do"
  ]
  node [
    id 60
    label "originate"
  ]
  node [
    id 61
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 62
    label "act"
  ]
  node [
    id 63
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 64
    label "odj&#261;&#263;"
  ]
  node [
    id 65
    label "introduce"
  ]
  node [
    id 66
    label "post&#261;pi&#263;"
  ]
  node [
    id 67
    label "cause"
  ]
  node [
    id 68
    label "begin"
  ]
  node [
    id 69
    label "zorganizowa&#263;"
  ]
  node [
    id 70
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 71
    label "wydali&#263;"
  ]
  node [
    id 72
    label "make"
  ]
  node [
    id 73
    label "wystylizowa&#263;"
  ]
  node [
    id 74
    label "appoint"
  ]
  node [
    id 75
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 76
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 77
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 78
    label "przerobi&#263;"
  ]
  node [
    id 79
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 80
    label "nabra&#263;"
  ]
  node [
    id 81
    label "ut"
  ]
  node [
    id 82
    label "C"
  ]
  node [
    id 83
    label "d&#378;wi&#281;k"
  ]
  node [
    id 84
    label "his"
  ]
  node [
    id 85
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 86
    label "zachowanie"
  ]
  node [
    id 87
    label "rozprawa"
  ]
  node [
    id 88
    label "kognicja"
  ]
  node [
    id 89
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 90
    label "campaign"
  ]
  node [
    id 91
    label "zmierzanie"
  ]
  node [
    id 92
    label "wydarzenie"
  ]
  node [
    id 93
    label "robienie"
  ]
  node [
    id 94
    label "przes&#322;anka"
  ]
  node [
    id 95
    label "s&#261;d"
  ]
  node [
    id 96
    label "fashion"
  ]
  node [
    id 97
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 98
    label "kazanie"
  ]
  node [
    id 99
    label "czynno&#347;&#263;"
  ]
  node [
    id 100
    label "charakter"
  ]
  node [
    id 101
    label "przebiegni&#281;cie"
  ]
  node [
    id 102
    label "przebiec"
  ]
  node [
    id 103
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 104
    label "motyw"
  ]
  node [
    id 105
    label "fabu&#322;a"
  ]
  node [
    id 106
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 107
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 108
    label "przemierzenie"
  ]
  node [
    id 109
    label "wodzenie"
  ]
  node [
    id 110
    label "spotykanie"
  ]
  node [
    id 111
    label "odchodzenie"
  ]
  node [
    id 112
    label "przyj&#347;cie"
  ]
  node [
    id 113
    label "udawanie_si&#281;"
  ]
  node [
    id 114
    label "podej&#347;cie"
  ]
  node [
    id 115
    label "wyprzedzenie"
  ]
  node [
    id 116
    label "podchodzenie"
  ]
  node [
    id 117
    label "przemierzanie"
  ]
  node [
    id 118
    label "przychodzenie"
  ]
  node [
    id 119
    label "dochodzenie"
  ]
  node [
    id 120
    label "przej&#347;cie"
  ]
  node [
    id 121
    label "wyprzedzanie"
  ]
  node [
    id 122
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 123
    label "bycie"
  ]
  node [
    id 124
    label "przedmiot"
  ]
  node [
    id 125
    label "fabrication"
  ]
  node [
    id 126
    label "tentegowanie"
  ]
  node [
    id 127
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 128
    label "porobienie"
  ]
  node [
    id 129
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 130
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 131
    label "creation"
  ]
  node [
    id 132
    label "bezproblemowy"
  ]
  node [
    id 133
    label "activity"
  ]
  node [
    id 134
    label "post"
  ]
  node [
    id 135
    label "spos&#243;b"
  ]
  node [
    id 136
    label "etolog"
  ]
  node [
    id 137
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 138
    label "dieta"
  ]
  node [
    id 139
    label "zdyscyplinowanie"
  ]
  node [
    id 140
    label "struktura"
  ]
  node [
    id 141
    label "zwierz&#281;"
  ]
  node [
    id 142
    label "bearing"
  ]
  node [
    id 143
    label "observation"
  ]
  node [
    id 144
    label "behawior"
  ]
  node [
    id 145
    label "reakcja"
  ]
  node [
    id 146
    label "zrobienie"
  ]
  node [
    id 147
    label "tajemnica"
  ]
  node [
    id 148
    label "przechowanie"
  ]
  node [
    id 149
    label "pochowanie"
  ]
  node [
    id 150
    label "podtrzymanie"
  ]
  node [
    id 151
    label "post&#261;pienie"
  ]
  node [
    id 152
    label "tekst"
  ]
  node [
    id 153
    label "proces"
  ]
  node [
    id 154
    label "cytat"
  ]
  node [
    id 155
    label "opracowanie"
  ]
  node [
    id 156
    label "obja&#347;nienie"
  ]
  node [
    id 157
    label "s&#261;dzenie"
  ]
  node [
    id 158
    label "obrady"
  ]
  node [
    id 159
    label "rozumowanie"
  ]
  node [
    id 160
    label "przyczyna"
  ]
  node [
    id 161
    label "wnioskowanie"
  ]
  node [
    id 162
    label "fakt"
  ]
  node [
    id 163
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 164
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 165
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 166
    label "forum"
  ]
  node [
    id 167
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 168
    label "s&#261;downictwo"
  ]
  node [
    id 169
    label "podejrzany"
  ]
  node [
    id 170
    label "&#347;wiadek"
  ]
  node [
    id 171
    label "biuro"
  ]
  node [
    id 172
    label "court"
  ]
  node [
    id 173
    label "my&#347;l"
  ]
  node [
    id 174
    label "obrona"
  ]
  node [
    id 175
    label "system"
  ]
  node [
    id 176
    label "broni&#263;"
  ]
  node [
    id 177
    label "antylogizm"
  ]
  node [
    id 178
    label "strona"
  ]
  node [
    id 179
    label "oskar&#380;yciel"
  ]
  node [
    id 180
    label "skazany"
  ]
  node [
    id 181
    label "konektyw"
  ]
  node [
    id 182
    label "wypowied&#378;"
  ]
  node [
    id 183
    label "bronienie"
  ]
  node [
    id 184
    label "wytw&#243;r"
  ]
  node [
    id 185
    label "pods&#261;dny"
  ]
  node [
    id 186
    label "procesowicz"
  ]
  node [
    id 187
    label "wyg&#322;oszenie"
  ]
  node [
    id 188
    label "wymaganie"
  ]
  node [
    id 189
    label "command"
  ]
  node [
    id 190
    label "order"
  ]
  node [
    id 191
    label "sermon"
  ]
  node [
    id 192
    label "krytyka"
  ]
  node [
    id 193
    label "wyg&#322;aszanie"
  ]
  node [
    id 194
    label "zmuszanie"
  ]
  node [
    id 195
    label "zmuszenie"
  ]
  node [
    id 196
    label "nakazywanie"
  ]
  node [
    id 197
    label "nakazanie"
  ]
  node [
    id 198
    label "msza"
  ]
  node [
    id 199
    label "nauka"
  ]
  node [
    id 200
    label "rzecz"
  ]
  node [
    id 201
    label "proposition"
  ]
  node [
    id 202
    label "object"
  ]
  node [
    id 203
    label "idea"
  ]
  node [
    id 204
    label "szczeg&#243;&#322;"
  ]
  node [
    id 205
    label "temat"
  ]
  node [
    id 206
    label "poj&#281;cie"
  ]
  node [
    id 207
    label "istota"
  ]
  node [
    id 208
    label "Kant"
  ]
  node [
    id 209
    label "ideacja"
  ]
  node [
    id 210
    label "byt"
  ]
  node [
    id 211
    label "p&#322;&#243;d"
  ]
  node [
    id 212
    label "cel"
  ]
  node [
    id 213
    label "intelekt"
  ]
  node [
    id 214
    label "ideologia"
  ]
  node [
    id 215
    label "pomys&#322;"
  ]
  node [
    id 216
    label "wpada&#263;"
  ]
  node [
    id 217
    label "przyroda"
  ]
  node [
    id 218
    label "wpa&#347;&#263;"
  ]
  node [
    id 219
    label "kultura"
  ]
  node [
    id 220
    label "mienie"
  ]
  node [
    id 221
    label "obiekt"
  ]
  node [
    id 222
    label "wpadni&#281;cie"
  ]
  node [
    id 223
    label "wpadanie"
  ]
  node [
    id 224
    label "sk&#322;adnik"
  ]
  node [
    id 225
    label "zniuansowa&#263;"
  ]
  node [
    id 226
    label "niuansowa&#263;"
  ]
  node [
    id 227
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 228
    label "element"
  ]
  node [
    id 229
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 230
    label "zboczy&#263;"
  ]
  node [
    id 231
    label "w&#261;tek"
  ]
  node [
    id 232
    label "fraza"
  ]
  node [
    id 233
    label "entity"
  ]
  node [
    id 234
    label "otoczka"
  ]
  node [
    id 235
    label "forma"
  ]
  node [
    id 236
    label "zboczenie"
  ]
  node [
    id 237
    label "om&#243;wi&#263;"
  ]
  node [
    id 238
    label "tre&#347;&#263;"
  ]
  node [
    id 239
    label "topik"
  ]
  node [
    id 240
    label "melodia"
  ]
  node [
    id 241
    label "cecha"
  ]
  node [
    id 242
    label "wyraz_pochodny"
  ]
  node [
    id 243
    label "zbacza&#263;"
  ]
  node [
    id 244
    label "om&#243;wienie"
  ]
  node [
    id 245
    label "tematyka"
  ]
  node [
    id 246
    label "omawianie"
  ]
  node [
    id 247
    label "omawia&#263;"
  ]
  node [
    id 248
    label "zbaczanie"
  ]
  node [
    id 249
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 250
    label "marc&#243;wka"
  ]
  node [
    id 251
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 252
    label "po&#322;&#243;g"
  ]
  node [
    id 253
    label "proces_fizjologiczny"
  ]
  node [
    id 254
    label "dula"
  ]
  node [
    id 255
    label "po&#322;o&#380;na"
  ]
  node [
    id 256
    label "szok_poporodowy"
  ]
  node [
    id 257
    label "rozwi&#261;zanie"
  ]
  node [
    id 258
    label "zlec"
  ]
  node [
    id 259
    label "&#380;ycie"
  ]
  node [
    id 260
    label "czas"
  ]
  node [
    id 261
    label "zlegni&#281;cie"
  ]
  node [
    id 262
    label "pomoc"
  ]
  node [
    id 263
    label "asystentka"
  ]
  node [
    id 264
    label "zabory"
  ]
  node [
    id 265
    label "ci&#281;&#380;arna"
  ]
  node [
    id 266
    label "przyj&#261;&#263;_por&#243;d"
  ]
  node [
    id 267
    label "babka"
  ]
  node [
    id 268
    label "piel&#281;gniarka"
  ]
  node [
    id 269
    label "przyjmowa&#263;_por&#243;d"
  ]
  node [
    id 270
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 271
    label "zobo"
  ]
  node [
    id 272
    label "byd&#322;o"
  ]
  node [
    id 273
    label "dzo"
  ]
  node [
    id 274
    label "yakalo"
  ]
  node [
    id 275
    label "zbi&#243;r"
  ]
  node [
    id 276
    label "kr&#281;torogie"
  ]
  node [
    id 277
    label "g&#322;owa"
  ]
  node [
    id 278
    label "livestock"
  ]
  node [
    id 279
    label "posp&#243;lstwo"
  ]
  node [
    id 280
    label "kraal"
  ]
  node [
    id 281
    label "czochrad&#322;o"
  ]
  node [
    id 282
    label "prze&#380;uwacz"
  ]
  node [
    id 283
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 284
    label "bizon"
  ]
  node [
    id 285
    label "zebu"
  ]
  node [
    id 286
    label "byd&#322;o_domowe"
  ]
  node [
    id 287
    label "proszek"
  ]
  node [
    id 288
    label "tablet"
  ]
  node [
    id 289
    label "dawka"
  ]
  node [
    id 290
    label "lekarstwo"
  ]
  node [
    id 291
    label "blister"
  ]
  node [
    id 292
    label "przestrze&#324;"
  ]
  node [
    id 293
    label "rz&#261;d"
  ]
  node [
    id 294
    label "uwaga"
  ]
  node [
    id 295
    label "praca"
  ]
  node [
    id 296
    label "plac"
  ]
  node [
    id 297
    label "location"
  ]
  node [
    id 298
    label "warunek_lokalowy"
  ]
  node [
    id 299
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 300
    label "cia&#322;o"
  ]
  node [
    id 301
    label "status"
  ]
  node [
    id 302
    label "chwila"
  ]
  node [
    id 303
    label "charakterystyka"
  ]
  node [
    id 304
    label "m&#322;ot"
  ]
  node [
    id 305
    label "marka"
  ]
  node [
    id 306
    label "pr&#243;ba"
  ]
  node [
    id 307
    label "attribute"
  ]
  node [
    id 308
    label "drzewo"
  ]
  node [
    id 309
    label "znak"
  ]
  node [
    id 310
    label "stan"
  ]
  node [
    id 311
    label "wzgl&#261;d"
  ]
  node [
    id 312
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 313
    label "nagana"
  ]
  node [
    id 314
    label "upomnienie"
  ]
  node [
    id 315
    label "gossip"
  ]
  node [
    id 316
    label "dzienniczek"
  ]
  node [
    id 317
    label "Rzym_Zachodni"
  ]
  node [
    id 318
    label "Rzym_Wschodni"
  ]
  node [
    id 319
    label "ilo&#347;&#263;"
  ]
  node [
    id 320
    label "whole"
  ]
  node [
    id 321
    label "urz&#261;dzenie"
  ]
  node [
    id 322
    label "zaw&#243;d"
  ]
  node [
    id 323
    label "zmiana"
  ]
  node [
    id 324
    label "pracowanie"
  ]
  node [
    id 325
    label "pracowa&#263;"
  ]
  node [
    id 326
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 327
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 328
    label "czynnik_produkcji"
  ]
  node [
    id 329
    label "stosunek_pracy"
  ]
  node [
    id 330
    label "kierownictwo"
  ]
  node [
    id 331
    label "najem"
  ]
  node [
    id 332
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 333
    label "zak&#322;ad"
  ]
  node [
    id 334
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 335
    label "tynkarski"
  ]
  node [
    id 336
    label "tyrka"
  ]
  node [
    id 337
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 338
    label "benedykty&#324;ski"
  ]
  node [
    id 339
    label "poda&#380;_pracy"
  ]
  node [
    id 340
    label "zobowi&#261;zanie"
  ]
  node [
    id 341
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 342
    label "punkt"
  ]
  node [
    id 343
    label "oktant"
  ]
  node [
    id 344
    label "przedzielenie"
  ]
  node [
    id 345
    label "przedzieli&#263;"
  ]
  node [
    id 346
    label "przestw&#243;r"
  ]
  node [
    id 347
    label "rozdziela&#263;"
  ]
  node [
    id 348
    label "nielito&#347;ciwy"
  ]
  node [
    id 349
    label "czasoprzestrze&#324;"
  ]
  node [
    id 350
    label "niezmierzony"
  ]
  node [
    id 351
    label "bezbrze&#380;e"
  ]
  node [
    id 352
    label "rozdzielanie"
  ]
  node [
    id 353
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 354
    label "awansowa&#263;"
  ]
  node [
    id 355
    label "podmiotowo"
  ]
  node [
    id 356
    label "awans"
  ]
  node [
    id 357
    label "condition"
  ]
  node [
    id 358
    label "znaczenie"
  ]
  node [
    id 359
    label "awansowanie"
  ]
  node [
    id 360
    label "sytuacja"
  ]
  node [
    id 361
    label "time"
  ]
  node [
    id 362
    label "leksem"
  ]
  node [
    id 363
    label "liczba"
  ]
  node [
    id 364
    label "rozmiar"
  ]
  node [
    id 365
    label "circumference"
  ]
  node [
    id 366
    label "cyrkumferencja"
  ]
  node [
    id 367
    label "tanatoplastyk"
  ]
  node [
    id 368
    label "odwadnianie"
  ]
  node [
    id 369
    label "tanatoplastyka"
  ]
  node [
    id 370
    label "odwodni&#263;"
  ]
  node [
    id 371
    label "ty&#322;"
  ]
  node [
    id 372
    label "zabalsamowanie"
  ]
  node [
    id 373
    label "biorytm"
  ]
  node [
    id 374
    label "unerwienie"
  ]
  node [
    id 375
    label "istota_&#380;ywa"
  ]
  node [
    id 376
    label "nieumar&#322;y"
  ]
  node [
    id 377
    label "balsamowanie"
  ]
  node [
    id 378
    label "balsamowa&#263;"
  ]
  node [
    id 379
    label "sekcja"
  ]
  node [
    id 380
    label "sk&#243;ra"
  ]
  node [
    id 381
    label "pochowa&#263;"
  ]
  node [
    id 382
    label "odwodnienie"
  ]
  node [
    id 383
    label "otwieranie"
  ]
  node [
    id 384
    label "materia"
  ]
  node [
    id 385
    label "cz&#322;onek"
  ]
  node [
    id 386
    label "mi&#281;so"
  ]
  node [
    id 387
    label "ekshumowanie"
  ]
  node [
    id 388
    label "p&#322;aszczyzna"
  ]
  node [
    id 389
    label "pogrzeb"
  ]
  node [
    id 390
    label "kremacja"
  ]
  node [
    id 391
    label "otworzy&#263;"
  ]
  node [
    id 392
    label "odwadnia&#263;"
  ]
  node [
    id 393
    label "staw"
  ]
  node [
    id 394
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 395
    label "szkielet"
  ]
  node [
    id 396
    label "prz&#243;d"
  ]
  node [
    id 397
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 398
    label "ow&#322;osienie"
  ]
  node [
    id 399
    label "otworzenie"
  ]
  node [
    id 400
    label "l&#281;d&#378;wie"
  ]
  node [
    id 401
    label "otwiera&#263;"
  ]
  node [
    id 402
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 403
    label "ekshumowa&#263;"
  ]
  node [
    id 404
    label "zabalsamowa&#263;"
  ]
  node [
    id 405
    label "temperatura"
  ]
  node [
    id 406
    label "miasto"
  ]
  node [
    id 407
    label "obiekt_handlowy"
  ]
  node [
    id 408
    label "area"
  ]
  node [
    id 409
    label "stoisko"
  ]
  node [
    id 410
    label "obszar"
  ]
  node [
    id 411
    label "pole_bitwy"
  ]
  node [
    id 412
    label "targowica"
  ]
  node [
    id 413
    label "kram"
  ]
  node [
    id 414
    label "zgromadzenie"
  ]
  node [
    id 415
    label "Majdan"
  ]
  node [
    id 416
    label "pierzeja"
  ]
  node [
    id 417
    label "szpaler"
  ]
  node [
    id 418
    label "number"
  ]
  node [
    id 419
    label "Londyn"
  ]
  node [
    id 420
    label "przybli&#380;enie"
  ]
  node [
    id 421
    label "premier"
  ]
  node [
    id 422
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 423
    label "tract"
  ]
  node [
    id 424
    label "uporz&#261;dkowanie"
  ]
  node [
    id 425
    label "egzekutywa"
  ]
  node [
    id 426
    label "klasa"
  ]
  node [
    id 427
    label "Konsulat"
  ]
  node [
    id 428
    label "gabinet_cieni"
  ]
  node [
    id 429
    label "lon&#380;a"
  ]
  node [
    id 430
    label "gromada"
  ]
  node [
    id 431
    label "jednostka_systematyczna"
  ]
  node [
    id 432
    label "kategoria"
  ]
  node [
    id 433
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 434
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 435
    label "zakres"
  ]
  node [
    id 436
    label "wymiar"
  ]
  node [
    id 437
    label "nation"
  ]
  node [
    id 438
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 439
    label "kontekst"
  ]
  node [
    id 440
    label "krajobraz"
  ]
  node [
    id 441
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 442
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 443
    label "integer"
  ]
  node [
    id 444
    label "zlewanie_si&#281;"
  ]
  node [
    id 445
    label "pe&#322;ny"
  ]
  node [
    id 446
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 447
    label "wielko&#347;&#263;"
  ]
  node [
    id 448
    label "dymensja"
  ]
  node [
    id 449
    label "parametr"
  ]
  node [
    id 450
    label "poziom"
  ]
  node [
    id 451
    label "dane"
  ]
  node [
    id 452
    label "Kosowo"
  ]
  node [
    id 453
    label "zach&#243;d"
  ]
  node [
    id 454
    label "Zabu&#380;e"
  ]
  node [
    id 455
    label "antroposfera"
  ]
  node [
    id 456
    label "Arktyka"
  ]
  node [
    id 457
    label "Notogea"
  ]
  node [
    id 458
    label "Piotrowo"
  ]
  node [
    id 459
    label "akrecja"
  ]
  node [
    id 460
    label "Ruda_Pabianicka"
  ]
  node [
    id 461
    label "Ludwin&#243;w"
  ]
  node [
    id 462
    label "po&#322;udnie"
  ]
  node [
    id 463
    label "wsch&#243;d"
  ]
  node [
    id 464
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 465
    label "Pow&#261;zki"
  ]
  node [
    id 466
    label "&#321;&#281;g"
  ]
  node [
    id 467
    label "p&#243;&#322;noc"
  ]
  node [
    id 468
    label "Rakowice"
  ]
  node [
    id 469
    label "Syberia_Wschodnia"
  ]
  node [
    id 470
    label "Zab&#322;ocie"
  ]
  node [
    id 471
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 472
    label "Kresy_Zachodnie"
  ]
  node [
    id 473
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 474
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 475
    label "holarktyka"
  ]
  node [
    id 476
    label "terytorium"
  ]
  node [
    id 477
    label "Antarktyka"
  ]
  node [
    id 478
    label "pas_planetoid"
  ]
  node [
    id 479
    label "Syberia_Zachodnia"
  ]
  node [
    id 480
    label "Neogea"
  ]
  node [
    id 481
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 482
    label "Olszanica"
  ]
  node [
    id 483
    label "background"
  ]
  node [
    id 484
    label "&#347;rodowisko"
  ]
  node [
    id 485
    label "warunki"
  ]
  node [
    id 486
    label "interpretacja"
  ]
  node [
    id 487
    label "fragment"
  ]
  node [
    id 488
    label "context"
  ]
  node [
    id 489
    label "causal_agent"
  ]
  node [
    id 490
    label "otoczenie"
  ]
  node [
    id 491
    label "odniesienie"
  ]
  node [
    id 492
    label "cz&#322;owiek"
  ]
  node [
    id 493
    label "panowanie"
  ]
  node [
    id 494
    label "wydolno&#347;&#263;"
  ]
  node [
    id 495
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 496
    label "prawo"
  ]
  node [
    id 497
    label "grupa"
  ]
  node [
    id 498
    label "rz&#261;dzenie"
  ]
  node [
    id 499
    label "podzakres"
  ]
  node [
    id 500
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 501
    label "granica"
  ]
  node [
    id 502
    label "circle"
  ]
  node [
    id 503
    label "desygnat"
  ]
  node [
    id 504
    label "dziedzina"
  ]
  node [
    id 505
    label "sfera"
  ]
  node [
    id 506
    label "widok"
  ]
  node [
    id 507
    label "obraz"
  ]
  node [
    id 508
    label "zjawisko"
  ]
  node [
    id 509
    label "dzie&#322;o"
  ]
  node [
    id 510
    label "human_body"
  ]
  node [
    id 511
    label "zaj&#347;cie"
  ]
  node [
    id 512
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 513
    label "przyra"
  ]
  node [
    id 514
    label "wszechstworzenie"
  ]
  node [
    id 515
    label "mikrokosmos"
  ]
  node [
    id 516
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 517
    label "woda"
  ]
  node [
    id 518
    label "biota"
  ]
  node [
    id 519
    label "environment"
  ]
  node [
    id 520
    label "obiekt_naturalny"
  ]
  node [
    id 521
    label "ekosystem"
  ]
  node [
    id 522
    label "fauna"
  ]
  node [
    id 523
    label "Ziemia"
  ]
  node [
    id 524
    label "stw&#243;r"
  ]
  node [
    id 525
    label "nietutejszy"
  ]
  node [
    id 526
    label "obcy"
  ]
  node [
    id 527
    label "&#347;rodek"
  ]
  node [
    id 528
    label "zal&#261;&#380;ek"
  ]
  node [
    id 529
    label "skupisko"
  ]
  node [
    id 530
    label "Hollywood"
  ]
  node [
    id 531
    label "center"
  ]
  node [
    id 532
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 533
    label "spowodowanie"
  ]
  node [
    id 534
    label "okrycie"
  ]
  node [
    id 535
    label "zdarzenie_si&#281;"
  ]
  node [
    id 536
    label "cortege"
  ]
  node [
    id 537
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 538
    label "huczek"
  ]
  node [
    id 539
    label "class"
  ]
  node [
    id 540
    label "crack"
  ]
  node [
    id 541
    label "Wielki_Atraktor"
  ]
  node [
    id 542
    label "chemikalia"
  ]
  node [
    id 543
    label "abstrakcja"
  ]
  node [
    id 544
    label "substancja"
  ]
  node [
    id 545
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 546
    label "afiliowa&#263;"
  ]
  node [
    id 547
    label "establishment"
  ]
  node [
    id 548
    label "zamyka&#263;"
  ]
  node [
    id 549
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 550
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 551
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 552
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 553
    label "standard"
  ]
  node [
    id 554
    label "Fundusze_Unijne"
  ]
  node [
    id 555
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 556
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 557
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 558
    label "zamykanie"
  ]
  node [
    id 559
    label "organizacja"
  ]
  node [
    id 560
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 561
    label "osoba_prawna"
  ]
  node [
    id 562
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 563
    label "zar&#243;d&#378;"
  ]
  node [
    id 564
    label "pocz&#261;tek"
  ]
  node [
    id 565
    label "integument"
  ]
  node [
    id 566
    label "Los_Angeles"
  ]
  node [
    id 567
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 568
    label "os&#322;abia&#263;"
  ]
  node [
    id 569
    label "firmness"
  ]
  node [
    id 570
    label "niszczy&#263;"
  ]
  node [
    id 571
    label "zniszczenie"
  ]
  node [
    id 572
    label "zedrze&#263;"
  ]
  node [
    id 573
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 574
    label "kondycja"
  ]
  node [
    id 575
    label "zniszczy&#263;"
  ]
  node [
    id 576
    label "zdarcie"
  ]
  node [
    id 577
    label "os&#322;abi&#263;"
  ]
  node [
    id 578
    label "os&#322;abienie"
  ]
  node [
    id 579
    label "rozsypanie_si&#281;"
  ]
  node [
    id 580
    label "niszczenie"
  ]
  node [
    id 581
    label "soundness"
  ]
  node [
    id 582
    label "os&#322;abianie"
  ]
  node [
    id 583
    label "Arakan"
  ]
  node [
    id 584
    label "Teksas"
  ]
  node [
    id 585
    label "Georgia"
  ]
  node [
    id 586
    label "Maryland"
  ]
  node [
    id 587
    label "warstwa"
  ]
  node [
    id 588
    label "Michigan"
  ]
  node [
    id 589
    label "Massachusetts"
  ]
  node [
    id 590
    label "Luizjana"
  ]
  node [
    id 591
    label "by&#263;"
  ]
  node [
    id 592
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 593
    label "samopoczucie"
  ]
  node [
    id 594
    label "Floryda"
  ]
  node [
    id 595
    label "Ohio"
  ]
  node [
    id 596
    label "Alaska"
  ]
  node [
    id 597
    label "Nowy_Meksyk"
  ]
  node [
    id 598
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 599
    label "wci&#281;cie"
  ]
  node [
    id 600
    label "Kansas"
  ]
  node [
    id 601
    label "Alabama"
  ]
  node [
    id 602
    label "Kalifornia"
  ]
  node [
    id 603
    label "Wirginia"
  ]
  node [
    id 604
    label "Nowy_York"
  ]
  node [
    id 605
    label "Waszyngton"
  ]
  node [
    id 606
    label "Pensylwania"
  ]
  node [
    id 607
    label "wektor"
  ]
  node [
    id 608
    label "Hawaje"
  ]
  node [
    id 609
    label "state"
  ]
  node [
    id 610
    label "jednostka_administracyjna"
  ]
  node [
    id 611
    label "Illinois"
  ]
  node [
    id 612
    label "Oklahoma"
  ]
  node [
    id 613
    label "Oregon"
  ]
  node [
    id 614
    label "Arizona"
  ]
  node [
    id 615
    label "Jukatan"
  ]
  node [
    id 616
    label "shape"
  ]
  node [
    id 617
    label "Goa"
  ]
  node [
    id 618
    label "zdolno&#347;&#263;"
  ]
  node [
    id 619
    label "rank"
  ]
  node [
    id 620
    label "dyspozycja"
  ]
  node [
    id 621
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 622
    label "situation"
  ]
  node [
    id 623
    label "pogorszenie"
  ]
  node [
    id 624
    label "fatigue_duty"
  ]
  node [
    id 625
    label "kondycja_fizyczna"
  ]
  node [
    id 626
    label "s&#322;abszy"
  ]
  node [
    id 627
    label "doznanie"
  ]
  node [
    id 628
    label "infirmity"
  ]
  node [
    id 629
    label "zmniejszenie"
  ]
  node [
    id 630
    label "robi&#263;"
  ]
  node [
    id 631
    label "bate"
  ]
  node [
    id 632
    label "suppress"
  ]
  node [
    id 633
    label "powodowa&#263;"
  ]
  node [
    id 634
    label "zmniejsza&#263;"
  ]
  node [
    id 635
    label "debilitation"
  ]
  node [
    id 636
    label "powodowanie"
  ]
  node [
    id 637
    label "de-escalation"
  ]
  node [
    id 638
    label "zmniejszanie"
  ]
  node [
    id 639
    label "pogarszanie"
  ]
  node [
    id 640
    label "poniszczenie"
  ]
  node [
    id 641
    label "podpalenie"
  ]
  node [
    id 642
    label "strata"
  ]
  node [
    id 643
    label "spl&#261;drowanie"
  ]
  node [
    id 644
    label "zaszkodzenie"
  ]
  node [
    id 645
    label "rezultat"
  ]
  node [
    id 646
    label "ruin"
  ]
  node [
    id 647
    label "wear"
  ]
  node [
    id 648
    label "stanie_si&#281;"
  ]
  node [
    id 649
    label "destruction"
  ]
  node [
    id 650
    label "zu&#380;ycie"
  ]
  node [
    id 651
    label "attrition"
  ]
  node [
    id 652
    label "poniszczenie_si&#281;"
  ]
  node [
    id 653
    label "zmniejszy&#263;"
  ]
  node [
    id 654
    label "reduce"
  ]
  node [
    id 655
    label "cushion"
  ]
  node [
    id 656
    label "health"
  ]
  node [
    id 657
    label "wp&#322;yw"
  ]
  node [
    id 658
    label "szkodzi&#263;"
  ]
  node [
    id 659
    label "destroy"
  ]
  node [
    id 660
    label "uszkadza&#263;"
  ]
  node [
    id 661
    label "pamper"
  ]
  node [
    id 662
    label "wygrywa&#263;"
  ]
  node [
    id 663
    label "mar"
  ]
  node [
    id 664
    label "consume"
  ]
  node [
    id 665
    label "spoil"
  ]
  node [
    id 666
    label "zaszkodzi&#263;"
  ]
  node [
    id 667
    label "wygra&#263;"
  ]
  node [
    id 668
    label "zu&#380;y&#263;"
  ]
  node [
    id 669
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 670
    label "gard&#322;o"
  ]
  node [
    id 671
    label "nadwyr&#281;&#380;enie"
  ]
  node [
    id 672
    label "zranienie"
  ]
  node [
    id 673
    label "wzi&#281;cie"
  ]
  node [
    id 674
    label "s&#322;ony"
  ]
  node [
    id 675
    label "zu&#380;ywanie"
  ]
  node [
    id 676
    label "pl&#261;drowanie"
  ]
  node [
    id 677
    label "pustoszenie"
  ]
  node [
    id 678
    label "stawanie_si&#281;"
  ]
  node [
    id 679
    label "devastation"
  ]
  node [
    id 680
    label "gnojenie"
  ]
  node [
    id 681
    label "decay"
  ]
  node [
    id 682
    label "poniewieranie_si&#281;"
  ]
  node [
    id 683
    label "szkodzenie"
  ]
  node [
    id 684
    label "ravaging"
  ]
  node [
    id 685
    label "zarobi&#263;"
  ]
  node [
    id 686
    label "zrani&#263;"
  ]
  node [
    id 687
    label "nadwyr&#281;&#380;y&#263;"
  ]
  node [
    id 688
    label "wzi&#261;&#263;"
  ]
  node [
    id 689
    label "&#347;ci&#261;gn&#261;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 26
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 54
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 38
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 241
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 40
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 260
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 206
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 310
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 241
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 303
  ]
  edge [
    source 13
    target 304
  ]
  edge [
    source 13
    target 305
  ]
  edge [
    source 13
    target 306
  ]
  edge [
    source 13
    target 307
  ]
  edge [
    source 13
    target 308
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 319
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 58
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
]
