graph [
  node [
    id 0
    label "granica"
    origin "text"
  ]
  node [
    id 1
    label "dopuszczalny"
    origin "text"
  ]
  node [
    id 2
    label "krytyka"
    origin "text"
  ]
  node [
    id 3
    label "internet"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "bowiem"
    origin "text"
  ]
  node [
    id 6
    label "szeroki"
    origin "text"
  ]
  node [
    id 7
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 8
    label "przypadek"
    origin "text"
  ]
  node [
    id 9
    label "osoba"
    origin "text"
  ]
  node [
    id 10
    label "uczestniczy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "taki"
    origin "text"
  ]
  node [
    id 12
    label "dyskusja"
    origin "text"
  ]
  node [
    id 13
    label "dokonywa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "ocena"
    origin "text"
  ]
  node [
    id 15
    label "zachowanie"
    origin "text"
  ]
  node [
    id 16
    label "wypowied&#378;"
    origin "text"
  ]
  node [
    id 17
    label "publiczny"
    origin "text"
  ]
  node [
    id 18
    label "ile"
    origin "text"
  ]
  node [
    id 19
    label "wykracza&#263;"
    origin "text"
  ]
  node [
    id 20
    label "poza"
    origin "text"
  ]
  node [
    id 21
    label "akceptowa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "spo&#322;ecznie"
    origin "text"
  ]
  node [
    id 23
    label "standard"
    origin "text"
  ]
  node [
    id 24
    label "dzia&#322;anie"
    origin "text"
  ]
  node [
    id 25
    label "bezprawny"
    origin "text"
  ]
  node [
    id 26
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 27
    label "zdanie"
    origin "text"
  ]
  node [
    id 28
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 29
    label "apelacyjny"
    origin "text"
  ]
  node [
    id 30
    label "muszy"
    origin "text"
  ]
  node [
    id 31
    label "uwzgl&#281;dnia&#263;"
    origin "text"
  ]
  node [
    id 32
    label "fakt"
    origin "text"
  ]
  node [
    id 33
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 34
    label "internauta"
    origin "text"
  ]
  node [
    id 35
    label "dosadny"
    origin "text"
  ]
  node [
    id 36
    label "odbiega&#263;"
    origin "text"
  ]
  node [
    id 37
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 38
    label "komunikacja"
    origin "text"
  ]
  node [
    id 39
    label "obowi&#261;zywa&#263;"
    origin "text"
  ]
  node [
    id 40
    label "spo&#322;ecze&#324;stwo"
    origin "text"
  ]
  node [
    id 41
    label "przej&#347;cie"
  ]
  node [
    id 42
    label "zakres"
  ]
  node [
    id 43
    label "kres"
  ]
  node [
    id 44
    label "granica_pa&#324;stwa"
  ]
  node [
    id 45
    label "Ural"
  ]
  node [
    id 46
    label "miara"
  ]
  node [
    id 47
    label "poj&#281;cie"
  ]
  node [
    id 48
    label "end"
  ]
  node [
    id 49
    label "pu&#322;ap"
  ]
  node [
    id 50
    label "koniec"
  ]
  node [
    id 51
    label "granice"
  ]
  node [
    id 52
    label "frontier"
  ]
  node [
    id 53
    label "mini&#281;cie"
  ]
  node [
    id 54
    label "ustawa"
  ]
  node [
    id 55
    label "wymienienie"
  ]
  node [
    id 56
    label "zaliczenie"
  ]
  node [
    id 57
    label "traversal"
  ]
  node [
    id 58
    label "zdarzenie_si&#281;"
  ]
  node [
    id 59
    label "przewy&#380;szenie"
  ]
  node [
    id 60
    label "experience"
  ]
  node [
    id 61
    label "przepuszczenie"
  ]
  node [
    id 62
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 63
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 64
    label "strain"
  ]
  node [
    id 65
    label "faza"
  ]
  node [
    id 66
    label "przerobienie"
  ]
  node [
    id 67
    label "wydeptywanie"
  ]
  node [
    id 68
    label "miejsce"
  ]
  node [
    id 69
    label "crack"
  ]
  node [
    id 70
    label "wydeptanie"
  ]
  node [
    id 71
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 72
    label "wstawka"
  ]
  node [
    id 73
    label "prze&#380;ycie"
  ]
  node [
    id 74
    label "uznanie"
  ]
  node [
    id 75
    label "doznanie"
  ]
  node [
    id 76
    label "dostanie_si&#281;"
  ]
  node [
    id 77
    label "trwanie"
  ]
  node [
    id 78
    label "przebycie"
  ]
  node [
    id 79
    label "wytyczenie"
  ]
  node [
    id 80
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 81
    label "przepojenie"
  ]
  node [
    id 82
    label "nas&#261;czenie"
  ]
  node [
    id 83
    label "nale&#380;enie"
  ]
  node [
    id 84
    label "mienie"
  ]
  node [
    id 85
    label "odmienienie"
  ]
  node [
    id 86
    label "przedostanie_si&#281;"
  ]
  node [
    id 87
    label "przemokni&#281;cie"
  ]
  node [
    id 88
    label "nasycenie_si&#281;"
  ]
  node [
    id 89
    label "zacz&#281;cie"
  ]
  node [
    id 90
    label "stanie_si&#281;"
  ]
  node [
    id 91
    label "offense"
  ]
  node [
    id 92
    label "przestanie"
  ]
  node [
    id 93
    label "pos&#322;uchanie"
  ]
  node [
    id 94
    label "skumanie"
  ]
  node [
    id 95
    label "orientacja"
  ]
  node [
    id 96
    label "wytw&#243;r"
  ]
  node [
    id 97
    label "teoria"
  ]
  node [
    id 98
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 99
    label "clasp"
  ]
  node [
    id 100
    label "przem&#243;wienie"
  ]
  node [
    id 101
    label "forma"
  ]
  node [
    id 102
    label "zorientowanie"
  ]
  node [
    id 103
    label "strop"
  ]
  node [
    id 104
    label "poziom"
  ]
  node [
    id 105
    label "powa&#322;a"
  ]
  node [
    id 106
    label "wysoko&#347;&#263;"
  ]
  node [
    id 107
    label "ostatnie_podrygi"
  ]
  node [
    id 108
    label "punkt"
  ]
  node [
    id 109
    label "chwila"
  ]
  node [
    id 110
    label "visitation"
  ]
  node [
    id 111
    label "agonia"
  ]
  node [
    id 112
    label "defenestracja"
  ]
  node [
    id 113
    label "wydarzenie"
  ]
  node [
    id 114
    label "mogi&#322;a"
  ]
  node [
    id 115
    label "kres_&#380;ycia"
  ]
  node [
    id 116
    label "szereg"
  ]
  node [
    id 117
    label "szeol"
  ]
  node [
    id 118
    label "pogrzebanie"
  ]
  node [
    id 119
    label "&#380;a&#322;oba"
  ]
  node [
    id 120
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 121
    label "zabicie"
  ]
  node [
    id 122
    label "obszar"
  ]
  node [
    id 123
    label "zbi&#243;r"
  ]
  node [
    id 124
    label "sfera"
  ]
  node [
    id 125
    label "wielko&#347;&#263;"
  ]
  node [
    id 126
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 127
    label "podzakres"
  ]
  node [
    id 128
    label "dziedzina"
  ]
  node [
    id 129
    label "desygnat"
  ]
  node [
    id 130
    label "circle"
  ]
  node [
    id 131
    label "proportion"
  ]
  node [
    id 132
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 133
    label "continence"
  ]
  node [
    id 134
    label "supremum"
  ]
  node [
    id 135
    label "cecha"
  ]
  node [
    id 136
    label "skala"
  ]
  node [
    id 137
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 138
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 139
    label "jednostka"
  ]
  node [
    id 140
    label "przeliczy&#263;"
  ]
  node [
    id 141
    label "matematyka"
  ]
  node [
    id 142
    label "rzut"
  ]
  node [
    id 143
    label "odwiedziny"
  ]
  node [
    id 144
    label "liczba"
  ]
  node [
    id 145
    label "warunek_lokalowy"
  ]
  node [
    id 146
    label "ilo&#347;&#263;"
  ]
  node [
    id 147
    label "przeliczanie"
  ]
  node [
    id 148
    label "dymensja"
  ]
  node [
    id 149
    label "funkcja"
  ]
  node [
    id 150
    label "przelicza&#263;"
  ]
  node [
    id 151
    label "infimum"
  ]
  node [
    id 152
    label "przeliczenie"
  ]
  node [
    id 153
    label "Eurazja"
  ]
  node [
    id 154
    label "mo&#380;liwy"
  ]
  node [
    id 155
    label "dopuszczalnie"
  ]
  node [
    id 156
    label "urealnianie"
  ]
  node [
    id 157
    label "mo&#380;ebny"
  ]
  node [
    id 158
    label "umo&#380;liwianie"
  ]
  node [
    id 159
    label "zno&#347;ny"
  ]
  node [
    id 160
    label "umo&#380;liwienie"
  ]
  node [
    id 161
    label "mo&#380;liwie"
  ]
  node [
    id 162
    label "urealnienie"
  ]
  node [
    id 163
    label "dost&#281;pny"
  ]
  node [
    id 164
    label "streszczenie"
  ]
  node [
    id 165
    label "publicystyka"
  ]
  node [
    id 166
    label "criticism"
  ]
  node [
    id 167
    label "tekst"
  ]
  node [
    id 168
    label "publiczno&#347;&#263;"
  ]
  node [
    id 169
    label "cenzura"
  ]
  node [
    id 170
    label "diatryba"
  ]
  node [
    id 171
    label "review"
  ]
  node [
    id 172
    label "krytyka_literacka"
  ]
  node [
    id 173
    label "ekscerpcja"
  ]
  node [
    id 174
    label "j&#281;zykowo"
  ]
  node [
    id 175
    label "redakcja"
  ]
  node [
    id 176
    label "pomini&#281;cie"
  ]
  node [
    id 177
    label "dzie&#322;o"
  ]
  node [
    id 178
    label "preparacja"
  ]
  node [
    id 179
    label "odmianka"
  ]
  node [
    id 180
    label "opu&#347;ci&#263;"
  ]
  node [
    id 181
    label "koniektura"
  ]
  node [
    id 182
    label "pisa&#263;"
  ]
  node [
    id 183
    label "obelga"
  ]
  node [
    id 184
    label "pogl&#261;d"
  ]
  node [
    id 185
    label "decyzja"
  ]
  node [
    id 186
    label "sofcik"
  ]
  node [
    id 187
    label "kryterium"
  ]
  node [
    id 188
    label "informacja"
  ]
  node [
    id 189
    label "appraisal"
  ]
  node [
    id 190
    label "odbiorca"
  ]
  node [
    id 191
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 192
    label "audience"
  ]
  node [
    id 193
    label "publiczka"
  ]
  node [
    id 194
    label "widzownia"
  ]
  node [
    id 195
    label "literatura"
  ]
  node [
    id 196
    label "skr&#243;t"
  ]
  node [
    id 197
    label "podsumowanie"
  ]
  node [
    id 198
    label "przed&#322;o&#380;enie"
  ]
  node [
    id 199
    label "overview"
  ]
  node [
    id 200
    label "urz&#261;d"
  ]
  node [
    id 201
    label "&#347;wiadectwo"
  ]
  node [
    id 202
    label "proces"
  ]
  node [
    id 203
    label "drugi_obieg"
  ]
  node [
    id 204
    label "zdejmowanie"
  ]
  node [
    id 205
    label "bell_ringer"
  ]
  node [
    id 206
    label "crisscross"
  ]
  node [
    id 207
    label "p&#243;&#322;kownik"
  ]
  node [
    id 208
    label "ekskomunikowa&#263;"
  ]
  node [
    id 209
    label "kontrola"
  ]
  node [
    id 210
    label "mark"
  ]
  node [
    id 211
    label "zdejmowa&#263;"
  ]
  node [
    id 212
    label "zjawisko"
  ]
  node [
    id 213
    label "zdj&#281;cie"
  ]
  node [
    id 214
    label "zdj&#261;&#263;"
  ]
  node [
    id 215
    label "kara"
  ]
  node [
    id 216
    label "ekskomunikowanie"
  ]
  node [
    id 217
    label "harangue"
  ]
  node [
    id 218
    label "rozmowa"
  ]
  node [
    id 219
    label "utw&#243;r"
  ]
  node [
    id 220
    label "pamflet"
  ]
  node [
    id 221
    label "sprzeciw"
  ]
  node [
    id 222
    label "provider"
  ]
  node [
    id 223
    label "hipertekst"
  ]
  node [
    id 224
    label "cyberprzestrze&#324;"
  ]
  node [
    id 225
    label "mem"
  ]
  node [
    id 226
    label "gra_sieciowa"
  ]
  node [
    id 227
    label "grooming"
  ]
  node [
    id 228
    label "media"
  ]
  node [
    id 229
    label "biznes_elektroniczny"
  ]
  node [
    id 230
    label "sie&#263;_komputerowa"
  ]
  node [
    id 231
    label "punkt_dost&#281;pu"
  ]
  node [
    id 232
    label "us&#322;uga_internetowa"
  ]
  node [
    id 233
    label "netbook"
  ]
  node [
    id 234
    label "e-hazard"
  ]
  node [
    id 235
    label "podcast"
  ]
  node [
    id 236
    label "strona"
  ]
  node [
    id 237
    label "mass-media"
  ]
  node [
    id 238
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 239
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 240
    label "przekazior"
  ]
  node [
    id 241
    label "uzbrajanie"
  ]
  node [
    id 242
    label "medium"
  ]
  node [
    id 243
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 244
    label "cyberprzest&#281;pczo&#347;&#263;"
  ]
  node [
    id 245
    label "kartka"
  ]
  node [
    id 246
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 247
    label "logowanie"
  ]
  node [
    id 248
    label "plik"
  ]
  node [
    id 249
    label "adres_internetowy"
  ]
  node [
    id 250
    label "linia"
  ]
  node [
    id 251
    label "serwis_internetowy"
  ]
  node [
    id 252
    label "posta&#263;"
  ]
  node [
    id 253
    label "bok"
  ]
  node [
    id 254
    label "skr&#281;canie"
  ]
  node [
    id 255
    label "skr&#281;ca&#263;"
  ]
  node [
    id 256
    label "orientowanie"
  ]
  node [
    id 257
    label "skr&#281;ci&#263;"
  ]
  node [
    id 258
    label "uj&#281;cie"
  ]
  node [
    id 259
    label "ty&#322;"
  ]
  node [
    id 260
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 261
    label "fragment"
  ]
  node [
    id 262
    label "layout"
  ]
  node [
    id 263
    label "obiekt"
  ]
  node [
    id 264
    label "zorientowa&#263;"
  ]
  node [
    id 265
    label "pagina"
  ]
  node [
    id 266
    label "podmiot"
  ]
  node [
    id 267
    label "g&#243;ra"
  ]
  node [
    id 268
    label "orientowa&#263;"
  ]
  node [
    id 269
    label "voice"
  ]
  node [
    id 270
    label "prz&#243;d"
  ]
  node [
    id 271
    label "powierzchnia"
  ]
  node [
    id 272
    label "skr&#281;cenie"
  ]
  node [
    id 273
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 274
    label "ma&#322;y"
  ]
  node [
    id 275
    label "dostawca"
  ]
  node [
    id 276
    label "telefonia"
  ]
  node [
    id 277
    label "wydawnictwo"
  ]
  node [
    id 278
    label "meme"
  ]
  node [
    id 279
    label "hazard"
  ]
  node [
    id 280
    label "molestowanie_seksualne"
  ]
  node [
    id 281
    label "piel&#281;gnacja"
  ]
  node [
    id 282
    label "zwierz&#281;_domowe"
  ]
  node [
    id 283
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 284
    label "mie&#263;_miejsce"
  ]
  node [
    id 285
    label "equal"
  ]
  node [
    id 286
    label "trwa&#263;"
  ]
  node [
    id 287
    label "chodzi&#263;"
  ]
  node [
    id 288
    label "si&#281;ga&#263;"
  ]
  node [
    id 289
    label "stan"
  ]
  node [
    id 290
    label "obecno&#347;&#263;"
  ]
  node [
    id 291
    label "stand"
  ]
  node [
    id 292
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 293
    label "participate"
  ]
  node [
    id 294
    label "robi&#263;"
  ]
  node [
    id 295
    label "istnie&#263;"
  ]
  node [
    id 296
    label "pozostawa&#263;"
  ]
  node [
    id 297
    label "zostawa&#263;"
  ]
  node [
    id 298
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 299
    label "adhere"
  ]
  node [
    id 300
    label "compass"
  ]
  node [
    id 301
    label "korzysta&#263;"
  ]
  node [
    id 302
    label "appreciation"
  ]
  node [
    id 303
    label "osi&#261;ga&#263;"
  ]
  node [
    id 304
    label "dociera&#263;"
  ]
  node [
    id 305
    label "get"
  ]
  node [
    id 306
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 307
    label "mierzy&#263;"
  ]
  node [
    id 308
    label "u&#380;ywa&#263;"
  ]
  node [
    id 309
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 310
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 311
    label "exsert"
  ]
  node [
    id 312
    label "being"
  ]
  node [
    id 313
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 314
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 315
    label "p&#322;ywa&#263;"
  ]
  node [
    id 316
    label "run"
  ]
  node [
    id 317
    label "bangla&#263;"
  ]
  node [
    id 318
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 319
    label "przebiega&#263;"
  ]
  node [
    id 320
    label "wk&#322;ada&#263;"
  ]
  node [
    id 321
    label "proceed"
  ]
  node [
    id 322
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 323
    label "carry"
  ]
  node [
    id 324
    label "bywa&#263;"
  ]
  node [
    id 325
    label "dziama&#263;"
  ]
  node [
    id 326
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 327
    label "stara&#263;_si&#281;"
  ]
  node [
    id 328
    label "para"
  ]
  node [
    id 329
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 330
    label "str&#243;j"
  ]
  node [
    id 331
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 332
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 333
    label "krok"
  ]
  node [
    id 334
    label "tryb"
  ]
  node [
    id 335
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 336
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 337
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 338
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 339
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 340
    label "continue"
  ]
  node [
    id 341
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 342
    label "Ohio"
  ]
  node [
    id 343
    label "wci&#281;cie"
  ]
  node [
    id 344
    label "Nowy_York"
  ]
  node [
    id 345
    label "warstwa"
  ]
  node [
    id 346
    label "samopoczucie"
  ]
  node [
    id 347
    label "Illinois"
  ]
  node [
    id 348
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 349
    label "state"
  ]
  node [
    id 350
    label "Jukatan"
  ]
  node [
    id 351
    label "Kalifornia"
  ]
  node [
    id 352
    label "Wirginia"
  ]
  node [
    id 353
    label "wektor"
  ]
  node [
    id 354
    label "Teksas"
  ]
  node [
    id 355
    label "Goa"
  ]
  node [
    id 356
    label "Waszyngton"
  ]
  node [
    id 357
    label "Massachusetts"
  ]
  node [
    id 358
    label "Alaska"
  ]
  node [
    id 359
    label "Arakan"
  ]
  node [
    id 360
    label "Hawaje"
  ]
  node [
    id 361
    label "Maryland"
  ]
  node [
    id 362
    label "Michigan"
  ]
  node [
    id 363
    label "Arizona"
  ]
  node [
    id 364
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 365
    label "Georgia"
  ]
  node [
    id 366
    label "Pensylwania"
  ]
  node [
    id 367
    label "shape"
  ]
  node [
    id 368
    label "Luizjana"
  ]
  node [
    id 369
    label "Nowy_Meksyk"
  ]
  node [
    id 370
    label "Alabama"
  ]
  node [
    id 371
    label "Kansas"
  ]
  node [
    id 372
    label "Oregon"
  ]
  node [
    id 373
    label "Floryda"
  ]
  node [
    id 374
    label "Oklahoma"
  ]
  node [
    id 375
    label "jednostka_administracyjna"
  ]
  node [
    id 376
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 377
    label "szeroko"
  ]
  node [
    id 378
    label "rozdeptanie"
  ]
  node [
    id 379
    label "du&#380;y"
  ]
  node [
    id 380
    label "rozdeptywanie"
  ]
  node [
    id 381
    label "rozlegle"
  ]
  node [
    id 382
    label "rozleg&#322;y"
  ]
  node [
    id 383
    label "lu&#378;no"
  ]
  node [
    id 384
    label "lu&#378;ny"
  ]
  node [
    id 385
    label "rozci&#261;gle"
  ]
  node [
    id 386
    label "across_the_board"
  ]
  node [
    id 387
    label "rozci&#261;gni&#281;cie"
  ]
  node [
    id 388
    label "rozprowadzenie"
  ]
  node [
    id 389
    label "zniszczenie"
  ]
  node [
    id 390
    label "rozci&#261;ganie"
  ]
  node [
    id 391
    label "niszczenie"
  ]
  node [
    id 392
    label "rozprowadzanie"
  ]
  node [
    id 393
    label "lekko"
  ]
  node [
    id 394
    label "&#322;atwo"
  ]
  node [
    id 395
    label "odlegle"
  ]
  node [
    id 396
    label "thinly"
  ]
  node [
    id 397
    label "przyjemnie"
  ]
  node [
    id 398
    label "nieformalnie"
  ]
  node [
    id 399
    label "doros&#322;y"
  ]
  node [
    id 400
    label "znaczny"
  ]
  node [
    id 401
    label "niema&#322;o"
  ]
  node [
    id 402
    label "wiele"
  ]
  node [
    id 403
    label "rozwini&#281;ty"
  ]
  node [
    id 404
    label "dorodny"
  ]
  node [
    id 405
    label "wa&#380;ny"
  ]
  node [
    id 406
    label "prawdziwy"
  ]
  node [
    id 407
    label "nizina"
  ]
  node [
    id 408
    label "depression"
  ]
  node [
    id 409
    label "Wybrze&#380;e_Koromandelskie"
  ]
  node [
    id 410
    label "l&#261;d"
  ]
  node [
    id 411
    label "Pampa"
  ]
  node [
    id 412
    label "Nizina_Nadwi&#347;la&#324;ska"
  ]
  node [
    id 413
    label "boski"
  ]
  node [
    id 414
    label "krajobraz"
  ]
  node [
    id 415
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 416
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 417
    label "przywidzenie"
  ]
  node [
    id 418
    label "presence"
  ]
  node [
    id 419
    label "charakter"
  ]
  node [
    id 420
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 421
    label "cykl_astronomiczny"
  ]
  node [
    id 422
    label "coil"
  ]
  node [
    id 423
    label "fotoelement"
  ]
  node [
    id 424
    label "komutowanie"
  ]
  node [
    id 425
    label "stan_skupienia"
  ]
  node [
    id 426
    label "nastr&#243;j"
  ]
  node [
    id 427
    label "przerywacz"
  ]
  node [
    id 428
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 429
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 430
    label "kraw&#281;d&#378;"
  ]
  node [
    id 431
    label "obsesja"
  ]
  node [
    id 432
    label "dw&#243;jnik"
  ]
  node [
    id 433
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 434
    label "okres"
  ]
  node [
    id 435
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 436
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 437
    label "przew&#243;d"
  ]
  node [
    id 438
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 439
    label "czas"
  ]
  node [
    id 440
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 441
    label "obw&#243;d"
  ]
  node [
    id 442
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 443
    label "degree"
  ]
  node [
    id 444
    label "komutowa&#263;"
  ]
  node [
    id 445
    label "po&#322;o&#380;enie"
  ]
  node [
    id 446
    label "jako&#347;&#263;"
  ]
  node [
    id 447
    label "p&#322;aszczyzna"
  ]
  node [
    id 448
    label "punkt_widzenia"
  ]
  node [
    id 449
    label "kierunek"
  ]
  node [
    id 450
    label "wyk&#322;adnik"
  ]
  node [
    id 451
    label "szczebel"
  ]
  node [
    id 452
    label "budynek"
  ]
  node [
    id 453
    label "ranga"
  ]
  node [
    id 454
    label "pacjent"
  ]
  node [
    id 455
    label "happening"
  ]
  node [
    id 456
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 457
    label "schorzenie"
  ]
  node [
    id 458
    label "przyk&#322;ad"
  ]
  node [
    id 459
    label "kategoria_gramatyczna"
  ]
  node [
    id 460
    label "przeznaczenie"
  ]
  node [
    id 461
    label "cz&#322;owiek"
  ]
  node [
    id 462
    label "czyn"
  ]
  node [
    id 463
    label "ilustracja"
  ]
  node [
    id 464
    label "przedstawiciel"
  ]
  node [
    id 465
    label "przebiec"
  ]
  node [
    id 466
    label "czynno&#347;&#263;"
  ]
  node [
    id 467
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 468
    label "motyw"
  ]
  node [
    id 469
    label "przebiegni&#281;cie"
  ]
  node [
    id 470
    label "fabu&#322;a"
  ]
  node [
    id 471
    label "rzuci&#263;"
  ]
  node [
    id 472
    label "destiny"
  ]
  node [
    id 473
    label "si&#322;a"
  ]
  node [
    id 474
    label "ustalenie"
  ]
  node [
    id 475
    label "przymus"
  ]
  node [
    id 476
    label "przydzielenie"
  ]
  node [
    id 477
    label "p&#243;j&#347;cie"
  ]
  node [
    id 478
    label "oblat"
  ]
  node [
    id 479
    label "obowi&#261;zek"
  ]
  node [
    id 480
    label "rzucenie"
  ]
  node [
    id 481
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 482
    label "wybranie"
  ]
  node [
    id 483
    label "zrobienie"
  ]
  node [
    id 484
    label "ognisko"
  ]
  node [
    id 485
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 486
    label "powalenie"
  ]
  node [
    id 487
    label "odezwanie_si&#281;"
  ]
  node [
    id 488
    label "atakowanie"
  ]
  node [
    id 489
    label "grupa_ryzyka"
  ]
  node [
    id 490
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 491
    label "nabawienie_si&#281;"
  ]
  node [
    id 492
    label "inkubacja"
  ]
  node [
    id 493
    label "kryzys"
  ]
  node [
    id 494
    label "powali&#263;"
  ]
  node [
    id 495
    label "remisja"
  ]
  node [
    id 496
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 497
    label "zajmowa&#263;"
  ]
  node [
    id 498
    label "zaburzenie"
  ]
  node [
    id 499
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 500
    label "badanie_histopatologiczne"
  ]
  node [
    id 501
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 502
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 503
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 504
    label "odzywanie_si&#281;"
  ]
  node [
    id 505
    label "diagnoza"
  ]
  node [
    id 506
    label "atakowa&#263;"
  ]
  node [
    id 507
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 508
    label "nabawianie_si&#281;"
  ]
  node [
    id 509
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 510
    label "zajmowanie"
  ]
  node [
    id 511
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 512
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 513
    label "klient"
  ]
  node [
    id 514
    label "piel&#281;gniarz"
  ]
  node [
    id 515
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 516
    label "od&#322;&#261;czanie"
  ]
  node [
    id 517
    label "od&#322;&#261;czenie"
  ]
  node [
    id 518
    label "chory"
  ]
  node [
    id 519
    label "szpitalnik"
  ]
  node [
    id 520
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 521
    label "przedstawienie"
  ]
  node [
    id 522
    label "Chocho&#322;"
  ]
  node [
    id 523
    label "Herkules_Poirot"
  ]
  node [
    id 524
    label "Edyp"
  ]
  node [
    id 525
    label "ludzko&#347;&#263;"
  ]
  node [
    id 526
    label "parali&#380;owa&#263;"
  ]
  node [
    id 527
    label "Harry_Potter"
  ]
  node [
    id 528
    label "Casanova"
  ]
  node [
    id 529
    label "Zgredek"
  ]
  node [
    id 530
    label "Gargantua"
  ]
  node [
    id 531
    label "Winnetou"
  ]
  node [
    id 532
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 533
    label "Dulcynea"
  ]
  node [
    id 534
    label "g&#322;owa"
  ]
  node [
    id 535
    label "figura"
  ]
  node [
    id 536
    label "portrecista"
  ]
  node [
    id 537
    label "person"
  ]
  node [
    id 538
    label "Plastu&#347;"
  ]
  node [
    id 539
    label "Quasimodo"
  ]
  node [
    id 540
    label "Sherlock_Holmes"
  ]
  node [
    id 541
    label "Faust"
  ]
  node [
    id 542
    label "Wallenrod"
  ]
  node [
    id 543
    label "Dwukwiat"
  ]
  node [
    id 544
    label "Don_Juan"
  ]
  node [
    id 545
    label "profanum"
  ]
  node [
    id 546
    label "koniugacja"
  ]
  node [
    id 547
    label "Don_Kiszot"
  ]
  node [
    id 548
    label "mikrokosmos"
  ]
  node [
    id 549
    label "duch"
  ]
  node [
    id 550
    label "antropochoria"
  ]
  node [
    id 551
    label "oddzia&#322;ywanie"
  ]
  node [
    id 552
    label "Hamlet"
  ]
  node [
    id 553
    label "Werter"
  ]
  node [
    id 554
    label "istota"
  ]
  node [
    id 555
    label "Szwejk"
  ]
  node [
    id 556
    label "homo_sapiens"
  ]
  node [
    id 557
    label "mentalno&#347;&#263;"
  ]
  node [
    id 558
    label "superego"
  ]
  node [
    id 559
    label "psychika"
  ]
  node [
    id 560
    label "znaczenie"
  ]
  node [
    id 561
    label "wn&#281;trze"
  ]
  node [
    id 562
    label "charakterystyka"
  ]
  node [
    id 563
    label "zaistnie&#263;"
  ]
  node [
    id 564
    label "Osjan"
  ]
  node [
    id 565
    label "kto&#347;"
  ]
  node [
    id 566
    label "wygl&#261;d"
  ]
  node [
    id 567
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 568
    label "osobowo&#347;&#263;"
  ]
  node [
    id 569
    label "trim"
  ]
  node [
    id 570
    label "poby&#263;"
  ]
  node [
    id 571
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 572
    label "Aspazja"
  ]
  node [
    id 573
    label "kompleksja"
  ]
  node [
    id 574
    label "wytrzyma&#263;"
  ]
  node [
    id 575
    label "budowa"
  ]
  node [
    id 576
    label "formacja"
  ]
  node [
    id 577
    label "pozosta&#263;"
  ]
  node [
    id 578
    label "point"
  ]
  node [
    id 579
    label "go&#347;&#263;"
  ]
  node [
    id 580
    label "hamper"
  ]
  node [
    id 581
    label "spasm"
  ]
  node [
    id 582
    label "mrozi&#263;"
  ]
  node [
    id 583
    label "pora&#380;a&#263;"
  ]
  node [
    id 584
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 585
    label "fleksja"
  ]
  node [
    id 586
    label "coupling"
  ]
  node [
    id 587
    label "czasownik"
  ]
  node [
    id 588
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 589
    label "orz&#281;sek"
  ]
  node [
    id 590
    label "fotograf"
  ]
  node [
    id 591
    label "malarz"
  ]
  node [
    id 592
    label "artysta"
  ]
  node [
    id 593
    label "powodowanie"
  ]
  node [
    id 594
    label "hipnotyzowanie"
  ]
  node [
    id 595
    label "&#347;lad"
  ]
  node [
    id 596
    label "docieranie"
  ]
  node [
    id 597
    label "natural_process"
  ]
  node [
    id 598
    label "reakcja_chemiczna"
  ]
  node [
    id 599
    label "wdzieranie_si&#281;"
  ]
  node [
    id 600
    label "act"
  ]
  node [
    id 601
    label "rezultat"
  ]
  node [
    id 602
    label "lobbysta"
  ]
  node [
    id 603
    label "pryncypa&#322;"
  ]
  node [
    id 604
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 605
    label "kszta&#322;t"
  ]
  node [
    id 606
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 607
    label "wiedza"
  ]
  node [
    id 608
    label "kierowa&#263;"
  ]
  node [
    id 609
    label "alkohol"
  ]
  node [
    id 610
    label "zdolno&#347;&#263;"
  ]
  node [
    id 611
    label "&#380;ycie"
  ]
  node [
    id 612
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 613
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 614
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 615
    label "sztuka"
  ]
  node [
    id 616
    label "dekiel"
  ]
  node [
    id 617
    label "ro&#347;lina"
  ]
  node [
    id 618
    label "&#347;ci&#281;cie"
  ]
  node [
    id 619
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 620
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 621
    label "&#347;ci&#281;gno"
  ]
  node [
    id 622
    label "noosfera"
  ]
  node [
    id 623
    label "byd&#322;o"
  ]
  node [
    id 624
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 625
    label "makrocefalia"
  ]
  node [
    id 626
    label "ucho"
  ]
  node [
    id 627
    label "m&#243;zg"
  ]
  node [
    id 628
    label "kierownictwo"
  ]
  node [
    id 629
    label "fryzura"
  ]
  node [
    id 630
    label "umys&#322;"
  ]
  node [
    id 631
    label "cia&#322;o"
  ]
  node [
    id 632
    label "cz&#322;onek"
  ]
  node [
    id 633
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 634
    label "czaszka"
  ]
  node [
    id 635
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 636
    label "allochoria"
  ]
  node [
    id 637
    label "przedmiot"
  ]
  node [
    id 638
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 639
    label "bierka_szachowa"
  ]
  node [
    id 640
    label "obiekt_matematyczny"
  ]
  node [
    id 641
    label "gestaltyzm"
  ]
  node [
    id 642
    label "styl"
  ]
  node [
    id 643
    label "obraz"
  ]
  node [
    id 644
    label "rzecz"
  ]
  node [
    id 645
    label "d&#378;wi&#281;k"
  ]
  node [
    id 646
    label "character"
  ]
  node [
    id 647
    label "rze&#378;ba"
  ]
  node [
    id 648
    label "stylistyka"
  ]
  node [
    id 649
    label "figure"
  ]
  node [
    id 650
    label "antycypacja"
  ]
  node [
    id 651
    label "ornamentyka"
  ]
  node [
    id 652
    label "facet"
  ]
  node [
    id 653
    label "popis"
  ]
  node [
    id 654
    label "wiersz"
  ]
  node [
    id 655
    label "symetria"
  ]
  node [
    id 656
    label "lingwistyka_kognitywna"
  ]
  node [
    id 657
    label "karta"
  ]
  node [
    id 658
    label "podzbi&#243;r"
  ]
  node [
    id 659
    label "perspektywa"
  ]
  node [
    id 660
    label "Szekspir"
  ]
  node [
    id 661
    label "Mickiewicz"
  ]
  node [
    id 662
    label "cierpienie"
  ]
  node [
    id 663
    label "piek&#322;o"
  ]
  node [
    id 664
    label "human_body"
  ]
  node [
    id 665
    label "ofiarowywanie"
  ]
  node [
    id 666
    label "sfera_afektywna"
  ]
  node [
    id 667
    label "nekromancja"
  ]
  node [
    id 668
    label "Po&#347;wist"
  ]
  node [
    id 669
    label "podekscytowanie"
  ]
  node [
    id 670
    label "deformowanie"
  ]
  node [
    id 671
    label "sumienie"
  ]
  node [
    id 672
    label "deformowa&#263;"
  ]
  node [
    id 673
    label "zjawa"
  ]
  node [
    id 674
    label "zmar&#322;y"
  ]
  node [
    id 675
    label "istota_nadprzyrodzona"
  ]
  node [
    id 676
    label "power"
  ]
  node [
    id 677
    label "entity"
  ]
  node [
    id 678
    label "ofiarowywa&#263;"
  ]
  node [
    id 679
    label "oddech"
  ]
  node [
    id 680
    label "seksualno&#347;&#263;"
  ]
  node [
    id 681
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 682
    label "byt"
  ]
  node [
    id 683
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 684
    label "ego"
  ]
  node [
    id 685
    label "ofiarowanie"
  ]
  node [
    id 686
    label "fizjonomia"
  ]
  node [
    id 687
    label "kompleks"
  ]
  node [
    id 688
    label "zapalno&#347;&#263;"
  ]
  node [
    id 689
    label "T&#281;sknica"
  ]
  node [
    id 690
    label "ofiarowa&#263;"
  ]
  node [
    id 691
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 692
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 693
    label "passion"
  ]
  node [
    id 694
    label "atom"
  ]
  node [
    id 695
    label "odbicie"
  ]
  node [
    id 696
    label "przyroda"
  ]
  node [
    id 697
    label "Ziemia"
  ]
  node [
    id 698
    label "kosmos"
  ]
  node [
    id 699
    label "miniatura"
  ]
  node [
    id 700
    label "organizowa&#263;"
  ]
  node [
    id 701
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 702
    label "czyni&#263;"
  ]
  node [
    id 703
    label "give"
  ]
  node [
    id 704
    label "stylizowa&#263;"
  ]
  node [
    id 705
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 706
    label "falowa&#263;"
  ]
  node [
    id 707
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 708
    label "peddle"
  ]
  node [
    id 709
    label "praca"
  ]
  node [
    id 710
    label "wydala&#263;"
  ]
  node [
    id 711
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 712
    label "tentegowa&#263;"
  ]
  node [
    id 713
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 714
    label "urz&#261;dza&#263;"
  ]
  node [
    id 715
    label "oszukiwa&#263;"
  ]
  node [
    id 716
    label "work"
  ]
  node [
    id 717
    label "ukazywa&#263;"
  ]
  node [
    id 718
    label "przerabia&#263;"
  ]
  node [
    id 719
    label "post&#281;powa&#263;"
  ]
  node [
    id 720
    label "okre&#347;lony"
  ]
  node [
    id 721
    label "jaki&#347;"
  ]
  node [
    id 722
    label "przyzwoity"
  ]
  node [
    id 723
    label "ciekawy"
  ]
  node [
    id 724
    label "jako&#347;"
  ]
  node [
    id 725
    label "jako_tako"
  ]
  node [
    id 726
    label "niez&#322;y"
  ]
  node [
    id 727
    label "dziwny"
  ]
  node [
    id 728
    label "charakterystyczny"
  ]
  node [
    id 729
    label "wiadomy"
  ]
  node [
    id 730
    label "sympozjon"
  ]
  node [
    id 731
    label "conference"
  ]
  node [
    id 732
    label "cisza"
  ]
  node [
    id 733
    label "odpowied&#378;"
  ]
  node [
    id 734
    label "rozhowor"
  ]
  node [
    id 735
    label "discussion"
  ]
  node [
    id 736
    label "esej"
  ]
  node [
    id 737
    label "sympozjarcha"
  ]
  node [
    id 738
    label "rozrywka"
  ]
  node [
    id 739
    label "symposium"
  ]
  node [
    id 740
    label "przyj&#281;cie"
  ]
  node [
    id 741
    label "konferencja"
  ]
  node [
    id 742
    label "make"
  ]
  node [
    id 743
    label "determine"
  ]
  node [
    id 744
    label "przestawa&#263;"
  ]
  node [
    id 745
    label "&#380;y&#263;"
  ]
  node [
    id 746
    label "coating"
  ]
  node [
    id 747
    label "przebywa&#263;"
  ]
  node [
    id 748
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 749
    label "ko&#324;czy&#263;"
  ]
  node [
    id 750
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 751
    label "finish_up"
  ]
  node [
    id 752
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 753
    label "management"
  ]
  node [
    id 754
    label "resolution"
  ]
  node [
    id 755
    label "zdecydowanie"
  ]
  node [
    id 756
    label "dokument"
  ]
  node [
    id 757
    label "teologicznie"
  ]
  node [
    id 758
    label "belief"
  ]
  node [
    id 759
    label "zderzenie_si&#281;"
  ]
  node [
    id 760
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 761
    label "teoria_Arrheniusa"
  ]
  node [
    id 762
    label "publikacja"
  ]
  node [
    id 763
    label "doj&#347;cie"
  ]
  node [
    id 764
    label "obiega&#263;"
  ]
  node [
    id 765
    label "powzi&#281;cie"
  ]
  node [
    id 766
    label "dane"
  ]
  node [
    id 767
    label "obiegni&#281;cie"
  ]
  node [
    id 768
    label "sygna&#322;"
  ]
  node [
    id 769
    label "obieganie"
  ]
  node [
    id 770
    label "powzi&#261;&#263;"
  ]
  node [
    id 771
    label "obiec"
  ]
  node [
    id 772
    label "doj&#347;&#263;"
  ]
  node [
    id 773
    label "czynnik"
  ]
  node [
    id 774
    label "pornografia_mi&#281;kka"
  ]
  node [
    id 775
    label "drobiazg"
  ]
  node [
    id 776
    label "pornografia"
  ]
  node [
    id 777
    label "reakcja"
  ]
  node [
    id 778
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 779
    label "tajemnica"
  ]
  node [
    id 780
    label "struktura"
  ]
  node [
    id 781
    label "pochowanie"
  ]
  node [
    id 782
    label "zdyscyplinowanie"
  ]
  node [
    id 783
    label "post&#261;pienie"
  ]
  node [
    id 784
    label "post"
  ]
  node [
    id 785
    label "bearing"
  ]
  node [
    id 786
    label "zwierz&#281;"
  ]
  node [
    id 787
    label "behawior"
  ]
  node [
    id 788
    label "observation"
  ]
  node [
    id 789
    label "dieta"
  ]
  node [
    id 790
    label "podtrzymanie"
  ]
  node [
    id 791
    label "etolog"
  ]
  node [
    id 792
    label "przechowanie"
  ]
  node [
    id 793
    label "model"
  ]
  node [
    id 794
    label "narz&#281;dzie"
  ]
  node [
    id 795
    label "nature"
  ]
  node [
    id 796
    label "behavior"
  ]
  node [
    id 797
    label "comfort"
  ]
  node [
    id 798
    label "pocieszenie"
  ]
  node [
    id 799
    label "uniesienie"
  ]
  node [
    id 800
    label "sustainability"
  ]
  node [
    id 801
    label "support"
  ]
  node [
    id 802
    label "utrzymanie"
  ]
  node [
    id 803
    label "narobienie"
  ]
  node [
    id 804
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 805
    label "creation"
  ]
  node [
    id 806
    label "porobienie"
  ]
  node [
    id 807
    label "ukrycie"
  ]
  node [
    id 808
    label "retention"
  ]
  node [
    id 809
    label "preserve"
  ]
  node [
    id 810
    label "zmagazynowanie"
  ]
  node [
    id 811
    label "uchronienie"
  ]
  node [
    id 812
    label "react"
  ]
  node [
    id 813
    label "reaction"
  ]
  node [
    id 814
    label "organizm"
  ]
  node [
    id 815
    label "response"
  ]
  node [
    id 816
    label "respondent"
  ]
  node [
    id 817
    label "degenerat"
  ]
  node [
    id 818
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 819
    label "zwyrol"
  ]
  node [
    id 820
    label "czerniak"
  ]
  node [
    id 821
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 822
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 823
    label "paszcza"
  ]
  node [
    id 824
    label "popapraniec"
  ]
  node [
    id 825
    label "skuba&#263;"
  ]
  node [
    id 826
    label "skubanie"
  ]
  node [
    id 827
    label "skubni&#281;cie"
  ]
  node [
    id 828
    label "agresja"
  ]
  node [
    id 829
    label "zwierz&#281;ta"
  ]
  node [
    id 830
    label "fukni&#281;cie"
  ]
  node [
    id 831
    label "farba"
  ]
  node [
    id 832
    label "fukanie"
  ]
  node [
    id 833
    label "istota_&#380;ywa"
  ]
  node [
    id 834
    label "gad"
  ]
  node [
    id 835
    label "siedzie&#263;"
  ]
  node [
    id 836
    label "oswaja&#263;"
  ]
  node [
    id 837
    label "tresowa&#263;"
  ]
  node [
    id 838
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 839
    label "poligamia"
  ]
  node [
    id 840
    label "oz&#243;r"
  ]
  node [
    id 841
    label "skubn&#261;&#263;"
  ]
  node [
    id 842
    label "wios&#322;owa&#263;"
  ]
  node [
    id 843
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 844
    label "le&#380;enie"
  ]
  node [
    id 845
    label "niecz&#322;owiek"
  ]
  node [
    id 846
    label "wios&#322;owanie"
  ]
  node [
    id 847
    label "napasienie_si&#281;"
  ]
  node [
    id 848
    label "wiwarium"
  ]
  node [
    id 849
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 850
    label "animalista"
  ]
  node [
    id 851
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 852
    label "hodowla"
  ]
  node [
    id 853
    label "pasienie_si&#281;"
  ]
  node [
    id 854
    label "sodomita"
  ]
  node [
    id 855
    label "monogamia"
  ]
  node [
    id 856
    label "przyssawka"
  ]
  node [
    id 857
    label "budowa_cia&#322;a"
  ]
  node [
    id 858
    label "okrutnik"
  ]
  node [
    id 859
    label "grzbiet"
  ]
  node [
    id 860
    label "weterynarz"
  ]
  node [
    id 861
    label "&#322;eb"
  ]
  node [
    id 862
    label "wylinka"
  ]
  node [
    id 863
    label "bestia"
  ]
  node [
    id 864
    label "poskramia&#263;"
  ]
  node [
    id 865
    label "fauna"
  ]
  node [
    id 866
    label "treser"
  ]
  node [
    id 867
    label "siedzenie"
  ]
  node [
    id 868
    label "le&#380;e&#263;"
  ]
  node [
    id 869
    label "zoopsycholog"
  ]
  node [
    id 870
    label "zoolog"
  ]
  node [
    id 871
    label "wypaplanie"
  ]
  node [
    id 872
    label "enigmat"
  ]
  node [
    id 873
    label "zachowywanie"
  ]
  node [
    id 874
    label "secret"
  ]
  node [
    id 875
    label "wydawa&#263;"
  ]
  node [
    id 876
    label "dyskrecja"
  ]
  node [
    id 877
    label "wyda&#263;"
  ]
  node [
    id 878
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 879
    label "taj&#324;"
  ]
  node [
    id 880
    label "zachowa&#263;"
  ]
  node [
    id 881
    label "zachowywa&#263;"
  ]
  node [
    id 882
    label "chart"
  ]
  node [
    id 883
    label "wynagrodzenie"
  ]
  node [
    id 884
    label "regimen"
  ]
  node [
    id 885
    label "r&#243;wnowarto&#347;&#263;"
  ]
  node [
    id 886
    label "terapia"
  ]
  node [
    id 887
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 888
    label "podporz&#261;dkowanie"
  ]
  node [
    id 889
    label "porz&#261;dek"
  ]
  node [
    id 890
    label "mores"
  ]
  node [
    id 891
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 892
    label "nauczenie"
  ]
  node [
    id 893
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 894
    label "rok_ko&#347;cielny"
  ]
  node [
    id 895
    label "praktyka"
  ]
  node [
    id 896
    label "mechanika"
  ]
  node [
    id 897
    label "o&#347;"
  ]
  node [
    id 898
    label "usenet"
  ]
  node [
    id 899
    label "rozprz&#261;c"
  ]
  node [
    id 900
    label "cybernetyk"
  ]
  node [
    id 901
    label "podsystem"
  ]
  node [
    id 902
    label "system"
  ]
  node [
    id 903
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 904
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 905
    label "sk&#322;ad"
  ]
  node [
    id 906
    label "systemat"
  ]
  node [
    id 907
    label "konstrukcja"
  ]
  node [
    id 908
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 909
    label "konstelacja"
  ]
  node [
    id 910
    label "poumieszczanie"
  ]
  node [
    id 911
    label "burying"
  ]
  node [
    id 912
    label "powk&#322;adanie"
  ]
  node [
    id 913
    label "zw&#322;oki"
  ]
  node [
    id 914
    label "burial"
  ]
  node [
    id 915
    label "w&#322;o&#380;enie"
  ]
  node [
    id 916
    label "gr&#243;b"
  ]
  node [
    id 917
    label "spocz&#281;cie"
  ]
  node [
    id 918
    label "sparafrazowanie"
  ]
  node [
    id 919
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 920
    label "strawestowa&#263;"
  ]
  node [
    id 921
    label "sparafrazowa&#263;"
  ]
  node [
    id 922
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 923
    label "trawestowa&#263;"
  ]
  node [
    id 924
    label "sformu&#322;owanie"
  ]
  node [
    id 925
    label "parafrazowanie"
  ]
  node [
    id 926
    label "ozdobnik"
  ]
  node [
    id 927
    label "delimitacja"
  ]
  node [
    id 928
    label "parafrazowa&#263;"
  ]
  node [
    id 929
    label "stylizacja"
  ]
  node [
    id 930
    label "komunikat"
  ]
  node [
    id 931
    label "trawestowanie"
  ]
  node [
    id 932
    label "strawestowanie"
  ]
  node [
    id 933
    label "typ"
  ]
  node [
    id 934
    label "event"
  ]
  node [
    id 935
    label "przyczyna"
  ]
  node [
    id 936
    label "communication"
  ]
  node [
    id 937
    label "kreacjonista"
  ]
  node [
    id 938
    label "roi&#263;_si&#281;"
  ]
  node [
    id 939
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 940
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 941
    label "na&#347;ladownictwo"
  ]
  node [
    id 942
    label "stylization"
  ]
  node [
    id 943
    label "otoczka"
  ]
  node [
    id 944
    label "modyfikacja"
  ]
  node [
    id 945
    label "imitacja"
  ]
  node [
    id 946
    label "zestawienie"
  ]
  node [
    id 947
    label "szafiarka"
  ]
  node [
    id 948
    label "wording"
  ]
  node [
    id 949
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 950
    label "statement"
  ]
  node [
    id 951
    label "zapisanie"
  ]
  node [
    id 952
    label "poinformowanie"
  ]
  node [
    id 953
    label "dekor"
  ]
  node [
    id 954
    label "okre&#347;lenie"
  ]
  node [
    id 955
    label "wyra&#380;enie"
  ]
  node [
    id 956
    label "dekoracja"
  ]
  node [
    id 957
    label "boundary_line"
  ]
  node [
    id 958
    label "podzia&#322;"
  ]
  node [
    id 959
    label "satyra"
  ]
  node [
    id 960
    label "parodiowanie"
  ]
  node [
    id 961
    label "rozwlec_si&#281;"
  ]
  node [
    id 962
    label "rozwlekanie_si&#281;"
  ]
  node [
    id 963
    label "rozwleczenie_si&#281;"
  ]
  node [
    id 964
    label "zmodyfikowa&#263;"
  ]
  node [
    id 965
    label "powt&#243;rzy&#263;"
  ]
  node [
    id 966
    label "rozwleka&#263;_si&#281;"
  ]
  node [
    id 967
    label "przetworzy&#263;"
  ]
  node [
    id 968
    label "powtarza&#263;"
  ]
  node [
    id 969
    label "modyfikowa&#263;"
  ]
  node [
    id 970
    label "paraphrase"
  ]
  node [
    id 971
    label "przetwarza&#263;"
  ]
  node [
    id 972
    label "spotkanie"
  ]
  node [
    id 973
    label "wys&#322;uchanie"
  ]
  node [
    id 974
    label "w&#322;&#261;czenie"
  ]
  node [
    id 975
    label "zesp&#243;&#322;"
  ]
  node [
    id 976
    label "podejrzany"
  ]
  node [
    id 977
    label "s&#261;downictwo"
  ]
  node [
    id 978
    label "biuro"
  ]
  node [
    id 979
    label "court"
  ]
  node [
    id 980
    label "forum"
  ]
  node [
    id 981
    label "bronienie"
  ]
  node [
    id 982
    label "oskar&#380;yciel"
  ]
  node [
    id 983
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 984
    label "skazany"
  ]
  node [
    id 985
    label "post&#281;powanie"
  ]
  node [
    id 986
    label "broni&#263;"
  ]
  node [
    id 987
    label "my&#347;l"
  ]
  node [
    id 988
    label "pods&#261;dny"
  ]
  node [
    id 989
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 990
    label "obrona"
  ]
  node [
    id 991
    label "instytucja"
  ]
  node [
    id 992
    label "antylogizm"
  ]
  node [
    id 993
    label "konektyw"
  ]
  node [
    id 994
    label "&#347;wiadek"
  ]
  node [
    id 995
    label "procesowicz"
  ]
  node [
    id 996
    label "return"
  ]
  node [
    id 997
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 998
    label "sta&#263;_si&#281;"
  ]
  node [
    id 999
    label "zrobi&#263;"
  ]
  node [
    id 1000
    label "porobi&#263;"
  ]
  node [
    id 1001
    label "sparodiowanie"
  ]
  node [
    id 1002
    label "parodiowa&#263;"
  ]
  node [
    id 1003
    label "farce"
  ]
  node [
    id 1004
    label "przetworzenie"
  ]
  node [
    id 1005
    label "powt&#243;rzenie"
  ]
  node [
    id 1006
    label "sparodiowa&#263;"
  ]
  node [
    id 1007
    label "powtarzanie"
  ]
  node [
    id 1008
    label "przetwarzanie"
  ]
  node [
    id 1009
    label "upublicznianie"
  ]
  node [
    id 1010
    label "jawny"
  ]
  node [
    id 1011
    label "upublicznienie"
  ]
  node [
    id 1012
    label "publicznie"
  ]
  node [
    id 1013
    label "jawnie"
  ]
  node [
    id 1014
    label "udost&#281;pnianie"
  ]
  node [
    id 1015
    label "udost&#281;pnienie"
  ]
  node [
    id 1016
    label "ujawnienie_si&#281;"
  ]
  node [
    id 1017
    label "ujawnianie_si&#281;"
  ]
  node [
    id 1018
    label "zdecydowany"
  ]
  node [
    id 1019
    label "znajomy"
  ]
  node [
    id 1020
    label "ujawnienie"
  ]
  node [
    id 1021
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 1022
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 1023
    label "ujawnianie"
  ]
  node [
    id 1024
    label "ewidentny"
  ]
  node [
    id 1025
    label "transgress"
  ]
  node [
    id 1026
    label "cross"
  ]
  node [
    id 1027
    label "przerzut"
  ]
  node [
    id 1028
    label "traverse"
  ]
  node [
    id 1029
    label "wy&#347;cig"
  ]
  node [
    id 1030
    label "ustawienie"
  ]
  node [
    id 1031
    label "mode"
  ]
  node [
    id 1032
    label "przesada"
  ]
  node [
    id 1033
    label "gra"
  ]
  node [
    id 1034
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1035
    label "erection"
  ]
  node [
    id 1036
    label "setup"
  ]
  node [
    id 1037
    label "spowodowanie"
  ]
  node [
    id 1038
    label "erecting"
  ]
  node [
    id 1039
    label "rozmieszczenie"
  ]
  node [
    id 1040
    label "poustawianie"
  ]
  node [
    id 1041
    label "zinterpretowanie"
  ]
  node [
    id 1042
    label "porozstawianie"
  ]
  node [
    id 1043
    label "rola"
  ]
  node [
    id 1044
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 1045
    label "zmienno&#347;&#263;"
  ]
  node [
    id 1046
    label "play"
  ]
  node [
    id 1047
    label "rozgrywka"
  ]
  node [
    id 1048
    label "apparent_motion"
  ]
  node [
    id 1049
    label "contest"
  ]
  node [
    id 1050
    label "akcja"
  ]
  node [
    id 1051
    label "komplet"
  ]
  node [
    id 1052
    label "zabawa"
  ]
  node [
    id 1053
    label "zasada"
  ]
  node [
    id 1054
    label "rywalizacja"
  ]
  node [
    id 1055
    label "zbijany"
  ]
  node [
    id 1056
    label "game"
  ]
  node [
    id 1057
    label "odg&#322;os"
  ]
  node [
    id 1058
    label "Pok&#233;mon"
  ]
  node [
    id 1059
    label "synteza"
  ]
  node [
    id 1060
    label "odtworzenie"
  ]
  node [
    id 1061
    label "rekwizyt_do_gry"
  ]
  node [
    id 1062
    label "sztuczno&#347;&#263;"
  ]
  node [
    id 1063
    label "nadmiar"
  ]
  node [
    id 1064
    label "assent"
  ]
  node [
    id 1065
    label "godzi&#263;_si&#281;"
  ]
  node [
    id 1066
    label "zezwala&#263;"
  ]
  node [
    id 1067
    label "uznawa&#263;"
  ]
  node [
    id 1068
    label "authorize"
  ]
  node [
    id 1069
    label "os&#261;dza&#263;"
  ]
  node [
    id 1070
    label "consider"
  ]
  node [
    id 1071
    label "notice"
  ]
  node [
    id 1072
    label "stwierdza&#263;"
  ]
  node [
    id 1073
    label "przyznawa&#263;"
  ]
  node [
    id 1074
    label "spo&#322;eczny"
  ]
  node [
    id 1075
    label "niepubliczny"
  ]
  node [
    id 1076
    label "ordinariness"
  ]
  node [
    id 1077
    label "zorganizowa&#263;"
  ]
  node [
    id 1078
    label "taniec_towarzyski"
  ]
  node [
    id 1079
    label "organizowanie"
  ]
  node [
    id 1080
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 1081
    label "criterion"
  ]
  node [
    id 1082
    label "zorganizowanie"
  ]
  node [
    id 1083
    label "prezenter"
  ]
  node [
    id 1084
    label "mildew"
  ]
  node [
    id 1085
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1086
    label "motif"
  ]
  node [
    id 1087
    label "pozowanie"
  ]
  node [
    id 1088
    label "ideal"
  ]
  node [
    id 1089
    label "wz&#243;r"
  ]
  node [
    id 1090
    label "matryca"
  ]
  node [
    id 1091
    label "adaptation"
  ]
  node [
    id 1092
    label "ruch"
  ]
  node [
    id 1093
    label "pozowa&#263;"
  ]
  node [
    id 1094
    label "orygina&#322;"
  ]
  node [
    id 1095
    label "osoba_prawna"
  ]
  node [
    id 1096
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1097
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1098
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1099
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1100
    label "organizacja"
  ]
  node [
    id 1101
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1102
    label "Fundusze_Unijne"
  ]
  node [
    id 1103
    label "zamyka&#263;"
  ]
  node [
    id 1104
    label "establishment"
  ]
  node [
    id 1105
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1106
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1107
    label "afiliowa&#263;"
  ]
  node [
    id 1108
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1109
    label "zamykanie"
  ]
  node [
    id 1110
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1111
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1112
    label "planowa&#263;"
  ]
  node [
    id 1113
    label "dostosowywa&#263;"
  ]
  node [
    id 1114
    label "treat"
  ]
  node [
    id 1115
    label "pozyskiwa&#263;"
  ]
  node [
    id 1116
    label "ensnare"
  ]
  node [
    id 1117
    label "skupia&#263;"
  ]
  node [
    id 1118
    label "create"
  ]
  node [
    id 1119
    label "przygotowywa&#263;"
  ]
  node [
    id 1120
    label "tworzy&#263;"
  ]
  node [
    id 1121
    label "wprowadza&#263;"
  ]
  node [
    id 1122
    label "tworzenie"
  ]
  node [
    id 1123
    label "organizowanie_si&#281;"
  ]
  node [
    id 1124
    label "dyscyplinowanie"
  ]
  node [
    id 1125
    label "organization"
  ]
  node [
    id 1126
    label "uregulowanie"
  ]
  node [
    id 1127
    label "handling"
  ]
  node [
    id 1128
    label "pozyskiwanie"
  ]
  node [
    id 1129
    label "szykowanie"
  ]
  node [
    id 1130
    label "wprowadzanie"
  ]
  node [
    id 1131
    label "skupianie"
  ]
  node [
    id 1132
    label "dostosowa&#263;"
  ]
  node [
    id 1133
    label "pozyska&#263;"
  ]
  node [
    id 1134
    label "stworzy&#263;"
  ]
  node [
    id 1135
    label "plan"
  ]
  node [
    id 1136
    label "stage"
  ]
  node [
    id 1137
    label "urobi&#263;"
  ]
  node [
    id 1138
    label "wprowadzi&#263;"
  ]
  node [
    id 1139
    label "zaplanowa&#263;"
  ]
  node [
    id 1140
    label "przygotowa&#263;"
  ]
  node [
    id 1141
    label "skupi&#263;"
  ]
  node [
    id 1142
    label "zorganizowanie_si&#281;"
  ]
  node [
    id 1143
    label "stworzenie"
  ]
  node [
    id 1144
    label "skupienie"
  ]
  node [
    id 1145
    label "wprowadzenie"
  ]
  node [
    id 1146
    label "bargain"
  ]
  node [
    id 1147
    label "pozyskanie"
  ]
  node [
    id 1148
    label "constitution"
  ]
  node [
    id 1149
    label "liczenie"
  ]
  node [
    id 1150
    label "skutek"
  ]
  node [
    id 1151
    label "podzia&#322;anie"
  ]
  node [
    id 1152
    label "kampania"
  ]
  node [
    id 1153
    label "uruchamianie"
  ]
  node [
    id 1154
    label "operacja"
  ]
  node [
    id 1155
    label "robienie"
  ]
  node [
    id 1156
    label "uruchomienie"
  ]
  node [
    id 1157
    label "nakr&#281;canie"
  ]
  node [
    id 1158
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1159
    label "tr&#243;jstronny"
  ]
  node [
    id 1160
    label "nakr&#281;cenie"
  ]
  node [
    id 1161
    label "zatrzymanie"
  ]
  node [
    id 1162
    label "wp&#322;yw"
  ]
  node [
    id 1163
    label "podtrzymywanie"
  ]
  node [
    id 1164
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1165
    label "liczy&#263;"
  ]
  node [
    id 1166
    label "operation"
  ]
  node [
    id 1167
    label "dzianie_si&#281;"
  ]
  node [
    id 1168
    label "zadzia&#322;anie"
  ]
  node [
    id 1169
    label "priorytet"
  ]
  node [
    id 1170
    label "bycie"
  ]
  node [
    id 1171
    label "rozpocz&#281;cie"
  ]
  node [
    id 1172
    label "czynny"
  ]
  node [
    id 1173
    label "impact"
  ]
  node [
    id 1174
    label "oferta"
  ]
  node [
    id 1175
    label "zako&#324;czenie"
  ]
  node [
    id 1176
    label "cause"
  ]
  node [
    id 1177
    label "causal_agent"
  ]
  node [
    id 1178
    label "proces_my&#347;lowy"
  ]
  node [
    id 1179
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 1180
    label "laparotomia"
  ]
  node [
    id 1181
    label "strategia"
  ]
  node [
    id 1182
    label "torakotomia"
  ]
  node [
    id 1183
    label "chirurg"
  ]
  node [
    id 1184
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 1185
    label "zabieg"
  ]
  node [
    id 1186
    label "szew"
  ]
  node [
    id 1187
    label "mathematical_process"
  ]
  node [
    id 1188
    label "kwota"
  ]
  node [
    id 1189
    label "doch&#243;d_narodowy"
  ]
  node [
    id 1190
    label "offer"
  ]
  node [
    id 1191
    label "propozycja"
  ]
  node [
    id 1192
    label "obejrzenie"
  ]
  node [
    id 1193
    label "widzenie"
  ]
  node [
    id 1194
    label "urzeczywistnianie"
  ]
  node [
    id 1195
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1196
    label "przeszkodzenie"
  ]
  node [
    id 1197
    label "produkowanie"
  ]
  node [
    id 1198
    label "znikni&#281;cie"
  ]
  node [
    id 1199
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 1200
    label "przeszkadzanie"
  ]
  node [
    id 1201
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 1202
    label "wyprodukowanie"
  ]
  node [
    id 1203
    label "fabrication"
  ]
  node [
    id 1204
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 1205
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 1206
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 1207
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 1208
    label "tentegowanie"
  ]
  node [
    id 1209
    label "activity"
  ]
  node [
    id 1210
    label "bezproblemowy"
  ]
  node [
    id 1211
    label "addytywno&#347;&#263;"
  ]
  node [
    id 1212
    label "function"
  ]
  node [
    id 1213
    label "zastosowanie"
  ]
  node [
    id 1214
    label "funkcjonowanie"
  ]
  node [
    id 1215
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 1216
    label "powierzanie"
  ]
  node [
    id 1217
    label "cel"
  ]
  node [
    id 1218
    label "przeciwdziedzina"
  ]
  node [
    id 1219
    label "awansowa&#263;"
  ]
  node [
    id 1220
    label "stawia&#263;"
  ]
  node [
    id 1221
    label "wakowa&#263;"
  ]
  node [
    id 1222
    label "postawi&#263;"
  ]
  node [
    id 1223
    label "awansowanie"
  ]
  node [
    id 1224
    label "closing"
  ]
  node [
    id 1225
    label "termination"
  ]
  node [
    id 1226
    label "zrezygnowanie"
  ]
  node [
    id 1227
    label "closure"
  ]
  node [
    id 1228
    label "ukszta&#322;towanie"
  ]
  node [
    id 1229
    label "conclusion"
  ]
  node [
    id 1230
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 1231
    label "adjustment"
  ]
  node [
    id 1232
    label "opening"
  ]
  node [
    id 1233
    label "start"
  ]
  node [
    id 1234
    label "pocz&#261;tek"
  ]
  node [
    id 1235
    label "znalezienie_si&#281;"
  ]
  node [
    id 1236
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 1237
    label "wy&#322;&#261;czanie"
  ]
  node [
    id 1238
    label "incorporation"
  ]
  node [
    id 1239
    label "attachment"
  ]
  node [
    id 1240
    label "zaczynanie"
  ]
  node [
    id 1241
    label "nastawianie"
  ]
  node [
    id 1242
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 1243
    label "zapalanie"
  ]
  node [
    id 1244
    label "inclusion"
  ]
  node [
    id 1245
    label "przes&#322;uchiwanie"
  ]
  node [
    id 1246
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 1247
    label "uczestniczenie"
  ]
  node [
    id 1248
    label "przefiltrowanie"
  ]
  node [
    id 1249
    label "zamkni&#281;cie"
  ]
  node [
    id 1250
    label "career"
  ]
  node [
    id 1251
    label "zaaresztowanie"
  ]
  node [
    id 1252
    label "uniemo&#380;liwienie"
  ]
  node [
    id 1253
    label "discontinuance"
  ]
  node [
    id 1254
    label "przerwanie"
  ]
  node [
    id 1255
    label "zaczepienie"
  ]
  node [
    id 1256
    label "pozajmowanie"
  ]
  node [
    id 1257
    label "hipostaza"
  ]
  node [
    id 1258
    label "capture"
  ]
  node [
    id 1259
    label "przetrzymanie"
  ]
  node [
    id 1260
    label "oddzia&#322;anie"
  ]
  node [
    id 1261
    label "&#322;apanie"
  ]
  node [
    id 1262
    label "z&#322;apanie"
  ]
  node [
    id 1263
    label "check"
  ]
  node [
    id 1264
    label "unieruchomienie"
  ]
  node [
    id 1265
    label "zabranie"
  ]
  node [
    id 1266
    label "kapita&#322;"
  ]
  node [
    id 1267
    label "propulsion"
  ]
  node [
    id 1268
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 1269
    label "involvement"
  ]
  node [
    id 1270
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 1271
    label "za&#347;wiecenie"
  ]
  node [
    id 1272
    label "nastawienie"
  ]
  node [
    id 1273
    label "wy&#322;&#261;czenie"
  ]
  node [
    id 1274
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1275
    label "ukr&#281;cenie"
  ]
  node [
    id 1276
    label "gyration"
  ]
  node [
    id 1277
    label "ruszanie"
  ]
  node [
    id 1278
    label "dokr&#281;cenie"
  ]
  node [
    id 1279
    label "kr&#281;cenie"
  ]
  node [
    id 1280
    label "zakr&#281;canie"
  ]
  node [
    id 1281
    label "nagrywanie"
  ]
  node [
    id 1282
    label "wind"
  ]
  node [
    id 1283
    label "nak&#322;adanie"
  ]
  node [
    id 1284
    label "okr&#281;canie"
  ]
  node [
    id 1285
    label "wzmaganie"
  ]
  node [
    id 1286
    label "wzbudzanie"
  ]
  node [
    id 1287
    label "dokr&#281;canie"
  ]
  node [
    id 1288
    label "pozawijanie"
  ]
  node [
    id 1289
    label "nagranie"
  ]
  node [
    id 1290
    label "wzbudzenie"
  ]
  node [
    id 1291
    label "ruszenie"
  ]
  node [
    id 1292
    label "zakr&#281;cenie"
  ]
  node [
    id 1293
    label "naniesienie"
  ]
  node [
    id 1294
    label "suppression"
  ]
  node [
    id 1295
    label "wzmo&#380;enie"
  ]
  node [
    id 1296
    label "okr&#281;cenie"
  ]
  node [
    id 1297
    label "nak&#322;amanie"
  ]
  node [
    id 1298
    label "utrzymywanie"
  ]
  node [
    id 1299
    label "obstawanie"
  ]
  node [
    id 1300
    label "preservation"
  ]
  node [
    id 1301
    label "boost"
  ]
  node [
    id 1302
    label "continuance"
  ]
  node [
    id 1303
    label "pocieszanie"
  ]
  node [
    id 1304
    label "asymilowanie"
  ]
  node [
    id 1305
    label "wapniak"
  ]
  node [
    id 1306
    label "asymilowa&#263;"
  ]
  node [
    id 1307
    label "os&#322;abia&#263;"
  ]
  node [
    id 1308
    label "hominid"
  ]
  node [
    id 1309
    label "podw&#322;adny"
  ]
  node [
    id 1310
    label "os&#322;abianie"
  ]
  node [
    id 1311
    label "dwun&#243;g"
  ]
  node [
    id 1312
    label "nasada"
  ]
  node [
    id 1313
    label "senior"
  ]
  node [
    id 1314
    label "Adam"
  ]
  node [
    id 1315
    label "polifag"
  ]
  node [
    id 1316
    label "badanie"
  ]
  node [
    id 1317
    label "rachowanie"
  ]
  node [
    id 1318
    label "dyskalkulia"
  ]
  node [
    id 1319
    label "rozliczanie"
  ]
  node [
    id 1320
    label "wymienianie"
  ]
  node [
    id 1321
    label "oznaczanie"
  ]
  node [
    id 1322
    label "wychodzenie"
  ]
  node [
    id 1323
    label "naliczenie_si&#281;"
  ]
  node [
    id 1324
    label "wyznaczanie"
  ]
  node [
    id 1325
    label "dodawanie"
  ]
  node [
    id 1326
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 1327
    label "bang"
  ]
  node [
    id 1328
    label "spodziewanie_si&#281;"
  ]
  node [
    id 1329
    label "rozliczenie"
  ]
  node [
    id 1330
    label "kwotowanie"
  ]
  node [
    id 1331
    label "mierzenie"
  ]
  node [
    id 1332
    label "count"
  ]
  node [
    id 1333
    label "wycenianie"
  ]
  node [
    id 1334
    label "branie"
  ]
  node [
    id 1335
    label "sprowadzanie"
  ]
  node [
    id 1336
    label "uwzgl&#281;dnianie"
  ]
  node [
    id 1337
    label "odliczanie"
  ]
  node [
    id 1338
    label "przyswoi&#263;"
  ]
  node [
    id 1339
    label "one"
  ]
  node [
    id 1340
    label "ewoluowanie"
  ]
  node [
    id 1341
    label "przyswajanie"
  ]
  node [
    id 1342
    label "wyewoluowanie"
  ]
  node [
    id 1343
    label "wyewoluowa&#263;"
  ]
  node [
    id 1344
    label "ewoluowa&#263;"
  ]
  node [
    id 1345
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1346
    label "liczba_naturalna"
  ]
  node [
    id 1347
    label "czynnik_biotyczny"
  ]
  node [
    id 1348
    label "individual"
  ]
  node [
    id 1349
    label "przyswaja&#263;"
  ]
  node [
    id 1350
    label "przyswojenie"
  ]
  node [
    id 1351
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 1352
    label "starzenie_si&#281;"
  ]
  node [
    id 1353
    label "ograniczenie"
  ]
  node [
    id 1354
    label "armia"
  ]
  node [
    id 1355
    label "nawr&#243;t_choroby"
  ]
  node [
    id 1356
    label "potomstwo"
  ]
  node [
    id 1357
    label "odwzorowanie"
  ]
  node [
    id 1358
    label "rysunek"
  ]
  node [
    id 1359
    label "scene"
  ]
  node [
    id 1360
    label "throw"
  ]
  node [
    id 1361
    label "float"
  ]
  node [
    id 1362
    label "projection"
  ]
  node [
    id 1363
    label "injection"
  ]
  node [
    id 1364
    label "blow"
  ]
  node [
    id 1365
    label "pomys&#322;"
  ]
  node [
    id 1366
    label "k&#322;ad"
  ]
  node [
    id 1367
    label "mold"
  ]
  node [
    id 1368
    label "report"
  ]
  node [
    id 1369
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1370
    label "wymienia&#263;"
  ]
  node [
    id 1371
    label "posiada&#263;"
  ]
  node [
    id 1372
    label "wycenia&#263;"
  ]
  node [
    id 1373
    label "bra&#263;"
  ]
  node [
    id 1374
    label "rachowa&#263;"
  ]
  node [
    id 1375
    label "tell"
  ]
  node [
    id 1376
    label "odlicza&#263;"
  ]
  node [
    id 1377
    label "dodawa&#263;"
  ]
  node [
    id 1378
    label "wyznacza&#263;"
  ]
  node [
    id 1379
    label "admit"
  ]
  node [
    id 1380
    label "policza&#263;"
  ]
  node [
    id 1381
    label "okre&#347;la&#263;"
  ]
  node [
    id 1382
    label "rachunek_operatorowy"
  ]
  node [
    id 1383
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 1384
    label "kryptologia"
  ]
  node [
    id 1385
    label "logicyzm"
  ]
  node [
    id 1386
    label "logika"
  ]
  node [
    id 1387
    label "matematyka_czysta"
  ]
  node [
    id 1388
    label "forsing"
  ]
  node [
    id 1389
    label "modelowanie_matematyczne"
  ]
  node [
    id 1390
    label "matma"
  ]
  node [
    id 1391
    label "teoria_katastrof"
  ]
  node [
    id 1392
    label "fizyka_matematyczna"
  ]
  node [
    id 1393
    label "teoria_graf&#243;w"
  ]
  node [
    id 1394
    label "rachunki"
  ]
  node [
    id 1395
    label "topologia_algebraiczna"
  ]
  node [
    id 1396
    label "matematyka_stosowana"
  ]
  node [
    id 1397
    label "reply"
  ]
  node [
    id 1398
    label "chemia"
  ]
  node [
    id 1399
    label "intensywny"
  ]
  node [
    id 1400
    label "realny"
  ]
  node [
    id 1401
    label "dzia&#322;alny"
  ]
  node [
    id 1402
    label "faktyczny"
  ]
  node [
    id 1403
    label "zdolny"
  ]
  node [
    id 1404
    label "czynnie"
  ]
  node [
    id 1405
    label "uczynnianie"
  ]
  node [
    id 1406
    label "aktywnie"
  ]
  node [
    id 1407
    label "zaanga&#380;owany"
  ]
  node [
    id 1408
    label "istotny"
  ]
  node [
    id 1409
    label "zaj&#281;ty"
  ]
  node [
    id 1410
    label "uczynnienie"
  ]
  node [
    id 1411
    label "dobry"
  ]
  node [
    id 1412
    label "hypnotism"
  ]
  node [
    id 1413
    label "zachwycanie"
  ]
  node [
    id 1414
    label "usypianie"
  ]
  node [
    id 1415
    label "magnetyzowanie"
  ]
  node [
    id 1416
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 1417
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 1418
    label "silnik"
  ]
  node [
    id 1419
    label "dorabianie"
  ]
  node [
    id 1420
    label "tarcie"
  ]
  node [
    id 1421
    label "dopasowywanie"
  ]
  node [
    id 1422
    label "g&#322;adzenie"
  ]
  node [
    id 1423
    label "dostawanie_si&#281;"
  ]
  node [
    id 1424
    label "zahipnotyzowanie"
  ]
  node [
    id 1425
    label "wdarcie_si&#281;"
  ]
  node [
    id 1426
    label "dotarcie"
  ]
  node [
    id 1427
    label "sprawa"
  ]
  node [
    id 1428
    label "us&#322;uga"
  ]
  node [
    id 1429
    label "pierwszy_plan"
  ]
  node [
    id 1430
    label "przesy&#322;ka"
  ]
  node [
    id 1431
    label "absolutorium"
  ]
  node [
    id 1432
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1433
    label "campaign"
  ]
  node [
    id 1434
    label "Kampania_Wrze&#347;niowa"
  ]
  node [
    id 1435
    label "bezpodstawny"
  ]
  node [
    id 1436
    label "nielegalny"
  ]
  node [
    id 1437
    label "bezprawnie"
  ]
  node [
    id 1438
    label "nieoficjalny"
  ]
  node [
    id 1439
    label "zdelegalizowanie"
  ]
  node [
    id 1440
    label "nielegalnie"
  ]
  node [
    id 1441
    label "delegalizowanie"
  ]
  node [
    id 1442
    label "bezpodstawnie"
  ]
  node [
    id 1443
    label "nieuzasadniony"
  ]
  node [
    id 1444
    label "illegally"
  ]
  node [
    id 1445
    label "unlawfully"
  ]
  node [
    id 1446
    label "mocno"
  ]
  node [
    id 1447
    label "wiela"
  ]
  node [
    id 1448
    label "bardzo"
  ]
  node [
    id 1449
    label "cz&#281;sto"
  ]
  node [
    id 1450
    label "mocny"
  ]
  node [
    id 1451
    label "silny"
  ]
  node [
    id 1452
    label "przekonuj&#261;co"
  ]
  node [
    id 1453
    label "powerfully"
  ]
  node [
    id 1454
    label "widocznie"
  ]
  node [
    id 1455
    label "szczerze"
  ]
  node [
    id 1456
    label "konkretnie"
  ]
  node [
    id 1457
    label "niepodwa&#380;alnie"
  ]
  node [
    id 1458
    label "stabilnie"
  ]
  node [
    id 1459
    label "silnie"
  ]
  node [
    id 1460
    label "strongly"
  ]
  node [
    id 1461
    label "w_chuj"
  ]
  node [
    id 1462
    label "cz&#281;sty"
  ]
  node [
    id 1463
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 1464
    label "szko&#322;a"
  ]
  node [
    id 1465
    label "fraza"
  ]
  node [
    id 1466
    label "przekazanie"
  ]
  node [
    id 1467
    label "stanowisko"
  ]
  node [
    id 1468
    label "wypowiedzenie"
  ]
  node [
    id 1469
    label "prison_term"
  ]
  node [
    id 1470
    label "zmuszenie"
  ]
  node [
    id 1471
    label "attitude"
  ]
  node [
    id 1472
    label "powierzenie"
  ]
  node [
    id 1473
    label "adjudication"
  ]
  node [
    id 1474
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 1475
    label "pass"
  ]
  node [
    id 1476
    label "spe&#322;nienie"
  ]
  node [
    id 1477
    label "wliczenie"
  ]
  node [
    id 1478
    label "zaliczanie"
  ]
  node [
    id 1479
    label "zaklasyfikowanie_si&#281;"
  ]
  node [
    id 1480
    label "zadanie"
  ]
  node [
    id 1481
    label "odb&#281;bnienie"
  ]
  node [
    id 1482
    label "ocenienie"
  ]
  node [
    id 1483
    label "number"
  ]
  node [
    id 1484
    label "policzenie"
  ]
  node [
    id 1485
    label "przeklasyfikowanie"
  ]
  node [
    id 1486
    label "zaliczanie_si&#281;"
  ]
  node [
    id 1487
    label "wzi&#281;cie"
  ]
  node [
    id 1488
    label "dor&#281;czenie"
  ]
  node [
    id 1489
    label "wys&#322;anie"
  ]
  node [
    id 1490
    label "podanie"
  ]
  node [
    id 1491
    label "delivery"
  ]
  node [
    id 1492
    label "transfer"
  ]
  node [
    id 1493
    label "wp&#322;acenie"
  ]
  node [
    id 1494
    label "z&#322;o&#380;enie"
  ]
  node [
    id 1495
    label "leksem"
  ]
  node [
    id 1496
    label "kompozycja"
  ]
  node [
    id 1497
    label "oznaczenie"
  ]
  node [
    id 1498
    label "znak_j&#281;zykowy"
  ]
  node [
    id 1499
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1500
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 1501
    label "grupa_imienna"
  ]
  node [
    id 1502
    label "jednostka_leksykalna"
  ]
  node [
    id 1503
    label "term"
  ]
  node [
    id 1504
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 1505
    label "affirmation"
  ]
  node [
    id 1506
    label "pr&#243;bowanie"
  ]
  node [
    id 1507
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 1508
    label "zademonstrowanie"
  ]
  node [
    id 1509
    label "obgadanie"
  ]
  node [
    id 1510
    label "realizacja"
  ]
  node [
    id 1511
    label "scena"
  ]
  node [
    id 1512
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 1513
    label "narration"
  ]
  node [
    id 1514
    label "cyrk"
  ]
  node [
    id 1515
    label "theatrical_performance"
  ]
  node [
    id 1516
    label "opisanie"
  ]
  node [
    id 1517
    label "malarstwo"
  ]
  node [
    id 1518
    label "scenografia"
  ]
  node [
    id 1519
    label "teatr"
  ]
  node [
    id 1520
    label "ukazanie"
  ]
  node [
    id 1521
    label "zapoznanie"
  ]
  node [
    id 1522
    label "pokaz"
  ]
  node [
    id 1523
    label "ods&#322;ona"
  ]
  node [
    id 1524
    label "exhibit"
  ]
  node [
    id 1525
    label "pokazanie"
  ]
  node [
    id 1526
    label "wyst&#261;pienie"
  ]
  node [
    id 1527
    label "przedstawi&#263;"
  ]
  node [
    id 1528
    label "przedstawianie"
  ]
  node [
    id 1529
    label "przedstawia&#263;"
  ]
  node [
    id 1530
    label "constraint"
  ]
  node [
    id 1531
    label "zadanie_gwa&#322;tu"
  ]
  node [
    id 1532
    label "force"
  ]
  node [
    id 1533
    label "pop&#281;dzenie_"
  ]
  node [
    id 1534
    label "konwersja"
  ]
  node [
    id 1535
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 1536
    label "przepowiedzenie"
  ]
  node [
    id 1537
    label "rozwi&#261;zanie"
  ]
  node [
    id 1538
    label "generowa&#263;"
  ]
  node [
    id 1539
    label "wydanie"
  ]
  node [
    id 1540
    label "message"
  ]
  node [
    id 1541
    label "generowanie"
  ]
  node [
    id 1542
    label "wydobycie"
  ]
  node [
    id 1543
    label "zwerbalizowanie"
  ]
  node [
    id 1544
    label "szyk"
  ]
  node [
    id 1545
    label "notification"
  ]
  node [
    id 1546
    label "powiedzenie"
  ]
  node [
    id 1547
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 1548
    label "denunciation"
  ]
  node [
    id 1549
    label "wojsko"
  ]
  node [
    id 1550
    label "uprawianie"
  ]
  node [
    id 1551
    label "wyznanie"
  ]
  node [
    id 1552
    label "zlecenie"
  ]
  node [
    id 1553
    label "ufanie"
  ]
  node [
    id 1554
    label "commitment"
  ]
  node [
    id 1555
    label "perpetration"
  ]
  node [
    id 1556
    label "oddanie"
  ]
  node [
    id 1557
    label "do&#347;wiadczenie"
  ]
  node [
    id 1558
    label "teren_szko&#322;y"
  ]
  node [
    id 1559
    label "kwalifikacje"
  ]
  node [
    id 1560
    label "podr&#281;cznik"
  ]
  node [
    id 1561
    label "absolwent"
  ]
  node [
    id 1562
    label "school"
  ]
  node [
    id 1563
    label "zda&#263;"
  ]
  node [
    id 1564
    label "gabinet"
  ]
  node [
    id 1565
    label "urszulanki"
  ]
  node [
    id 1566
    label "sztuba"
  ]
  node [
    id 1567
    label "&#322;awa_szkolna"
  ]
  node [
    id 1568
    label "nauka"
  ]
  node [
    id 1569
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 1570
    label "przepisa&#263;"
  ]
  node [
    id 1571
    label "muzyka"
  ]
  node [
    id 1572
    label "grupa"
  ]
  node [
    id 1573
    label "form"
  ]
  node [
    id 1574
    label "klasa"
  ]
  node [
    id 1575
    label "lekcja"
  ]
  node [
    id 1576
    label "metoda"
  ]
  node [
    id 1577
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 1578
    label "przepisanie"
  ]
  node [
    id 1579
    label "skolaryzacja"
  ]
  node [
    id 1580
    label "stopek"
  ]
  node [
    id 1581
    label "sekretariat"
  ]
  node [
    id 1582
    label "ideologia"
  ]
  node [
    id 1583
    label "lesson"
  ]
  node [
    id 1584
    label "niepokalanki"
  ]
  node [
    id 1585
    label "siedziba"
  ]
  node [
    id 1586
    label "szkolenie"
  ]
  node [
    id 1587
    label "tablica"
  ]
  node [
    id 1588
    label "funktor"
  ]
  node [
    id 1589
    label "j&#261;dro"
  ]
  node [
    id 1590
    label "systemik"
  ]
  node [
    id 1591
    label "oprogramowanie"
  ]
  node [
    id 1592
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1593
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1594
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1595
    label "przyn&#281;ta"
  ]
  node [
    id 1596
    label "p&#322;&#243;d"
  ]
  node [
    id 1597
    label "net"
  ]
  node [
    id 1598
    label "w&#281;dkarstwo"
  ]
  node [
    id 1599
    label "eratem"
  ]
  node [
    id 1600
    label "oddzia&#322;"
  ]
  node [
    id 1601
    label "doktryna"
  ]
  node [
    id 1602
    label "pulpit"
  ]
  node [
    id 1603
    label "jednostka_geologiczna"
  ]
  node [
    id 1604
    label "ryba"
  ]
  node [
    id 1605
    label "Leopard"
  ]
  node [
    id 1606
    label "Android"
  ]
  node [
    id 1607
    label "method"
  ]
  node [
    id 1608
    label "podstawa"
  ]
  node [
    id 1609
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1610
    label "relacja_logiczna"
  ]
  node [
    id 1611
    label "okres_amazo&#324;ski"
  ]
  node [
    id 1612
    label "stater"
  ]
  node [
    id 1613
    label "flow"
  ]
  node [
    id 1614
    label "choroba_przyrodzona"
  ]
  node [
    id 1615
    label "ordowik"
  ]
  node [
    id 1616
    label "postglacja&#322;"
  ]
  node [
    id 1617
    label "kreda"
  ]
  node [
    id 1618
    label "okres_hesperyjski"
  ]
  node [
    id 1619
    label "sylur"
  ]
  node [
    id 1620
    label "paleogen"
  ]
  node [
    id 1621
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 1622
    label "okres_halsztacki"
  ]
  node [
    id 1623
    label "riak"
  ]
  node [
    id 1624
    label "czwartorz&#281;d"
  ]
  node [
    id 1625
    label "podokres"
  ]
  node [
    id 1626
    label "trzeciorz&#281;d"
  ]
  node [
    id 1627
    label "kalim"
  ]
  node [
    id 1628
    label "fala"
  ]
  node [
    id 1629
    label "perm"
  ]
  node [
    id 1630
    label "retoryka"
  ]
  node [
    id 1631
    label "prekambr"
  ]
  node [
    id 1632
    label "neogen"
  ]
  node [
    id 1633
    label "pulsacja"
  ]
  node [
    id 1634
    label "proces_fizjologiczny"
  ]
  node [
    id 1635
    label "kambr"
  ]
  node [
    id 1636
    label "dzieje"
  ]
  node [
    id 1637
    label "kriogen"
  ]
  node [
    id 1638
    label "time_period"
  ]
  node [
    id 1639
    label "period"
  ]
  node [
    id 1640
    label "ton"
  ]
  node [
    id 1641
    label "orosir"
  ]
  node [
    id 1642
    label "okres_czasu"
  ]
  node [
    id 1643
    label "poprzednik"
  ]
  node [
    id 1644
    label "spell"
  ]
  node [
    id 1645
    label "sider"
  ]
  node [
    id 1646
    label "interstadia&#322;"
  ]
  node [
    id 1647
    label "ektas"
  ]
  node [
    id 1648
    label "epoka"
  ]
  node [
    id 1649
    label "rok_akademicki"
  ]
  node [
    id 1650
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 1651
    label "schy&#322;ek"
  ]
  node [
    id 1652
    label "cykl"
  ]
  node [
    id 1653
    label "ciota"
  ]
  node [
    id 1654
    label "okres_noachijski"
  ]
  node [
    id 1655
    label "pierwszorz&#281;d"
  ]
  node [
    id 1656
    label "ediakar"
  ]
  node [
    id 1657
    label "nast&#281;pnik"
  ]
  node [
    id 1658
    label "condition"
  ]
  node [
    id 1659
    label "jura"
  ]
  node [
    id 1660
    label "glacja&#322;"
  ]
  node [
    id 1661
    label "sten"
  ]
  node [
    id 1662
    label "Zeitgeist"
  ]
  node [
    id 1663
    label "era"
  ]
  node [
    id 1664
    label "trias"
  ]
  node [
    id 1665
    label "p&#243;&#322;okres"
  ]
  node [
    id 1666
    label "rok_szkolny"
  ]
  node [
    id 1667
    label "dewon"
  ]
  node [
    id 1668
    label "karbon"
  ]
  node [
    id 1669
    label "izochronizm"
  ]
  node [
    id 1670
    label "preglacja&#322;"
  ]
  node [
    id 1671
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1672
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 1673
    label "drugorz&#281;d"
  ]
  node [
    id 1674
    label "semester"
  ]
  node [
    id 1675
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 1676
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 1677
    label "Mazowsze"
  ]
  node [
    id 1678
    label "odm&#322;adzanie"
  ]
  node [
    id 1679
    label "&#346;wietliki"
  ]
  node [
    id 1680
    label "whole"
  ]
  node [
    id 1681
    label "The_Beatles"
  ]
  node [
    id 1682
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1683
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1684
    label "zabudowania"
  ]
  node [
    id 1685
    label "group"
  ]
  node [
    id 1686
    label "zespolik"
  ]
  node [
    id 1687
    label "Depeche_Mode"
  ]
  node [
    id 1688
    label "batch"
  ]
  node [
    id 1689
    label "odm&#322;odzenie"
  ]
  node [
    id 1690
    label "position"
  ]
  node [
    id 1691
    label "organ"
  ]
  node [
    id 1692
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 1693
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1694
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1695
    label "mianowaniec"
  ]
  node [
    id 1696
    label "dzia&#322;"
  ]
  node [
    id 1697
    label "okienko"
  ]
  node [
    id 1698
    label "w&#322;adza"
  ]
  node [
    id 1699
    label "thinking"
  ]
  node [
    id 1700
    label "political_orientation"
  ]
  node [
    id 1701
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 1702
    label "idea"
  ]
  node [
    id 1703
    label "fantomatyka"
  ]
  node [
    id 1704
    label "kognicja"
  ]
  node [
    id 1705
    label "rozprawa"
  ]
  node [
    id 1706
    label "fashion"
  ]
  node [
    id 1707
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1708
    label "zmierzanie"
  ]
  node [
    id 1709
    label "przes&#322;anka"
  ]
  node [
    id 1710
    label "kazanie"
  ]
  node [
    id 1711
    label "ob&#380;a&#322;owany"
  ]
  node [
    id 1712
    label "skar&#380;yciel"
  ]
  node [
    id 1713
    label "dysponowanie"
  ]
  node [
    id 1714
    label "dysponowa&#263;"
  ]
  node [
    id 1715
    label "podejrzanie"
  ]
  node [
    id 1716
    label "pos&#261;dzanie"
  ]
  node [
    id 1717
    label "nieprzejrzysty"
  ]
  node [
    id 1718
    label "niepewny"
  ]
  node [
    id 1719
    label "z&#322;y"
  ]
  node [
    id 1720
    label "egzamin"
  ]
  node [
    id 1721
    label "walka"
  ]
  node [
    id 1722
    label "liga"
  ]
  node [
    id 1723
    label "gracz"
  ]
  node [
    id 1724
    label "protection"
  ]
  node [
    id 1725
    label "poparcie"
  ]
  node [
    id 1726
    label "mecz"
  ]
  node [
    id 1727
    label "defense"
  ]
  node [
    id 1728
    label "auspices"
  ]
  node [
    id 1729
    label "ochrona"
  ]
  node [
    id 1730
    label "sp&#243;r"
  ]
  node [
    id 1731
    label "manewr"
  ]
  node [
    id 1732
    label "defensive_structure"
  ]
  node [
    id 1733
    label "guard_duty"
  ]
  node [
    id 1734
    label "uczestnik"
  ]
  node [
    id 1735
    label "dru&#380;ba"
  ]
  node [
    id 1736
    label "obserwator"
  ]
  node [
    id 1737
    label "osoba_fizyczna"
  ]
  node [
    id 1738
    label "niedost&#281;pny"
  ]
  node [
    id 1739
    label "adwokatowanie"
  ]
  node [
    id 1740
    label "zdawanie"
  ]
  node [
    id 1741
    label "walczenie"
  ]
  node [
    id 1742
    label "zabezpieczenie"
  ]
  node [
    id 1743
    label "t&#322;umaczenie"
  ]
  node [
    id 1744
    label "parry"
  ]
  node [
    id 1745
    label "or&#281;dowanie"
  ]
  node [
    id 1746
    label "granie"
  ]
  node [
    id 1747
    label "fend"
  ]
  node [
    id 1748
    label "reprezentowa&#263;"
  ]
  node [
    id 1749
    label "zdawa&#263;"
  ]
  node [
    id 1750
    label "czuwa&#263;"
  ]
  node [
    id 1751
    label "preach"
  ]
  node [
    id 1752
    label "chroni&#263;"
  ]
  node [
    id 1753
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 1754
    label "walczy&#263;"
  ]
  node [
    id 1755
    label "resist"
  ]
  node [
    id 1756
    label "adwokatowa&#263;"
  ]
  node [
    id 1757
    label "rebuff"
  ]
  node [
    id 1758
    label "udowadnia&#263;"
  ]
  node [
    id 1759
    label "gra&#263;"
  ]
  node [
    id 1760
    label "sprawowa&#263;"
  ]
  node [
    id 1761
    label "refuse"
  ]
  node [
    id 1762
    label "boks"
  ]
  node [
    id 1763
    label "biurko"
  ]
  node [
    id 1764
    label "palestra"
  ]
  node [
    id 1765
    label "Biuro_Lustracyjne"
  ]
  node [
    id 1766
    label "agency"
  ]
  node [
    id 1767
    label "board"
  ]
  node [
    id 1768
    label "pomieszczenie"
  ]
  node [
    id 1769
    label "judiciary"
  ]
  node [
    id 1770
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 1771
    label "grupa_dyskusyjna"
  ]
  node [
    id 1772
    label "plac"
  ]
  node [
    id 1773
    label "bazylika"
  ]
  node [
    id 1774
    label "przestrze&#324;"
  ]
  node [
    id 1775
    label "portal"
  ]
  node [
    id 1776
    label "agora"
  ]
  node [
    id 1777
    label "appellate"
  ]
  node [
    id 1778
    label "my&#347;le&#263;"
  ]
  node [
    id 1779
    label "involve"
  ]
  node [
    id 1780
    label "take_care"
  ]
  node [
    id 1781
    label "troska&#263;_si&#281;"
  ]
  node [
    id 1782
    label "deliver"
  ]
  node [
    id 1783
    label "rozpatrywa&#263;"
  ]
  node [
    id 1784
    label "zamierza&#263;"
  ]
  node [
    id 1785
    label "argue"
  ]
  node [
    id 1786
    label "bia&#322;e_plamy"
  ]
  node [
    id 1787
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 1788
    label "artykulator"
  ]
  node [
    id 1789
    label "kod"
  ]
  node [
    id 1790
    label "kawa&#322;ek"
  ]
  node [
    id 1791
    label "gramatyka"
  ]
  node [
    id 1792
    label "stylik"
  ]
  node [
    id 1793
    label "przet&#322;umaczenie"
  ]
  node [
    id 1794
    label "formalizowanie"
  ]
  node [
    id 1795
    label "ssanie"
  ]
  node [
    id 1796
    label "ssa&#263;"
  ]
  node [
    id 1797
    label "language"
  ]
  node [
    id 1798
    label "liza&#263;"
  ]
  node [
    id 1799
    label "napisa&#263;"
  ]
  node [
    id 1800
    label "konsonantyzm"
  ]
  node [
    id 1801
    label "wokalizm"
  ]
  node [
    id 1802
    label "fonetyka"
  ]
  node [
    id 1803
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1804
    label "jeniec"
  ]
  node [
    id 1805
    label "but"
  ]
  node [
    id 1806
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1807
    label "po_koroniarsku"
  ]
  node [
    id 1808
    label "kultura_duchowa"
  ]
  node [
    id 1809
    label "m&#243;wienie"
  ]
  node [
    id 1810
    label "pype&#263;"
  ]
  node [
    id 1811
    label "lizanie"
  ]
  node [
    id 1812
    label "pismo"
  ]
  node [
    id 1813
    label "formalizowa&#263;"
  ]
  node [
    id 1814
    label "rozumie&#263;"
  ]
  node [
    id 1815
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1816
    label "rozumienie"
  ]
  node [
    id 1817
    label "makroglosja"
  ]
  node [
    id 1818
    label "m&#243;wi&#263;"
  ]
  node [
    id 1819
    label "jama_ustna"
  ]
  node [
    id 1820
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1821
    label "formacja_geologiczna"
  ]
  node [
    id 1822
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1823
    label "natural_language"
  ]
  node [
    id 1824
    label "s&#322;ownictwo"
  ]
  node [
    id 1825
    label "urz&#261;dzenie"
  ]
  node [
    id 1826
    label "kawa&#322;"
  ]
  node [
    id 1827
    label "plot"
  ]
  node [
    id 1828
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 1829
    label "piece"
  ]
  node [
    id 1830
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 1831
    label "podp&#322;ywanie"
  ]
  node [
    id 1832
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 1833
    label "code"
  ]
  node [
    id 1834
    label "szyfrowanie"
  ]
  node [
    id 1835
    label "ci&#261;g"
  ]
  node [
    id 1836
    label "szablon"
  ]
  node [
    id 1837
    label "&#380;o&#322;nierz"
  ]
  node [
    id 1838
    label "internowanie"
  ]
  node [
    id 1839
    label "ojczyc"
  ]
  node [
    id 1840
    label "pojmaniec"
  ]
  node [
    id 1841
    label "niewolnik"
  ]
  node [
    id 1842
    label "internowa&#263;"
  ]
  node [
    id 1843
    label "aparat_artykulacyjny"
  ]
  node [
    id 1844
    label "tkanka"
  ]
  node [
    id 1845
    label "jednostka_organizacyjna"
  ]
  node [
    id 1846
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1847
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1848
    label "tw&#243;r"
  ]
  node [
    id 1849
    label "organogeneza"
  ]
  node [
    id 1850
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 1851
    label "struktura_anatomiczna"
  ]
  node [
    id 1852
    label "uk&#322;ad"
  ]
  node [
    id 1853
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 1854
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1855
    label "Izba_Konsyliarska"
  ]
  node [
    id 1856
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1857
    label "stomia"
  ]
  node [
    id 1858
    label "dekortykacja"
  ]
  node [
    id 1859
    label "okolica"
  ]
  node [
    id 1860
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1861
    label "zboczenie"
  ]
  node [
    id 1862
    label "om&#243;wienie"
  ]
  node [
    id 1863
    label "sponiewieranie"
  ]
  node [
    id 1864
    label "discipline"
  ]
  node [
    id 1865
    label "omawia&#263;"
  ]
  node [
    id 1866
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1867
    label "tre&#347;&#263;"
  ]
  node [
    id 1868
    label "sponiewiera&#263;"
  ]
  node [
    id 1869
    label "element"
  ]
  node [
    id 1870
    label "tematyka"
  ]
  node [
    id 1871
    label "w&#261;tek"
  ]
  node [
    id 1872
    label "zbaczanie"
  ]
  node [
    id 1873
    label "program_nauczania"
  ]
  node [
    id 1874
    label "om&#243;wi&#263;"
  ]
  node [
    id 1875
    label "omawianie"
  ]
  node [
    id 1876
    label "thing"
  ]
  node [
    id 1877
    label "kultura"
  ]
  node [
    id 1878
    label "zbacza&#263;"
  ]
  node [
    id 1879
    label "zboczy&#263;"
  ]
  node [
    id 1880
    label "zapi&#281;tek"
  ]
  node [
    id 1881
    label "sznurowad&#322;o"
  ]
  node [
    id 1882
    label "rozbijarka"
  ]
  node [
    id 1883
    label "podeszwa"
  ]
  node [
    id 1884
    label "obcas"
  ]
  node [
    id 1885
    label "wzuwanie"
  ]
  node [
    id 1886
    label "wzu&#263;"
  ]
  node [
    id 1887
    label "przyszwa"
  ]
  node [
    id 1888
    label "raki"
  ]
  node [
    id 1889
    label "cholewa"
  ]
  node [
    id 1890
    label "cholewka"
  ]
  node [
    id 1891
    label "zel&#243;wka"
  ]
  node [
    id 1892
    label "obuwie"
  ]
  node [
    id 1893
    label "napi&#281;tek"
  ]
  node [
    id 1894
    label "wzucie"
  ]
  node [
    id 1895
    label "kom&#243;rka"
  ]
  node [
    id 1896
    label "furnishing"
  ]
  node [
    id 1897
    label "wyrz&#261;dzenie"
  ]
  node [
    id 1898
    label "zagospodarowanie"
  ]
  node [
    id 1899
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 1900
    label "ig&#322;a"
  ]
  node [
    id 1901
    label "wirnik"
  ]
  node [
    id 1902
    label "aparatura"
  ]
  node [
    id 1903
    label "system_energetyczny"
  ]
  node [
    id 1904
    label "impulsator"
  ]
  node [
    id 1905
    label "mechanizm"
  ]
  node [
    id 1906
    label "sprz&#281;t"
  ]
  node [
    id 1907
    label "blokowanie"
  ]
  node [
    id 1908
    label "set"
  ]
  node [
    id 1909
    label "zablokowanie"
  ]
  node [
    id 1910
    label "przygotowanie"
  ]
  node [
    id 1911
    label "komora"
  ]
  node [
    id 1912
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 1913
    label "public_speaking"
  ]
  node [
    id 1914
    label "powiadanie"
  ]
  node [
    id 1915
    label "przepowiadanie"
  ]
  node [
    id 1916
    label "wyznawanie"
  ]
  node [
    id 1917
    label "wypowiadanie"
  ]
  node [
    id 1918
    label "wydobywanie"
  ]
  node [
    id 1919
    label "gaworzenie"
  ]
  node [
    id 1920
    label "stosowanie"
  ]
  node [
    id 1921
    label "wyra&#380;anie"
  ]
  node [
    id 1922
    label "formu&#322;owanie"
  ]
  node [
    id 1923
    label "dowalenie"
  ]
  node [
    id 1924
    label "przerywanie"
  ]
  node [
    id 1925
    label "wydawanie"
  ]
  node [
    id 1926
    label "dogadywanie_si&#281;"
  ]
  node [
    id 1927
    label "prawienie"
  ]
  node [
    id 1928
    label "opowiadanie"
  ]
  node [
    id 1929
    label "ozywanie_si&#281;"
  ]
  node [
    id 1930
    label "zapeszanie"
  ]
  node [
    id 1931
    label "zwracanie_si&#281;"
  ]
  node [
    id 1932
    label "dysfonia"
  ]
  node [
    id 1933
    label "speaking"
  ]
  node [
    id 1934
    label "zauwa&#380;enie"
  ]
  node [
    id 1935
    label "mawianie"
  ]
  node [
    id 1936
    label "opowiedzenie"
  ]
  node [
    id 1937
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 1938
    label "informowanie"
  ]
  node [
    id 1939
    label "dogadanie_si&#281;"
  ]
  node [
    id 1940
    label "wygadanie"
  ]
  node [
    id 1941
    label "psychotest"
  ]
  node [
    id 1942
    label "wk&#322;ad"
  ]
  node [
    id 1943
    label "handwriting"
  ]
  node [
    id 1944
    label "przekaz"
  ]
  node [
    id 1945
    label "paleograf"
  ]
  node [
    id 1946
    label "interpunkcja"
  ]
  node [
    id 1947
    label "grafia"
  ]
  node [
    id 1948
    label "egzemplarz"
  ]
  node [
    id 1949
    label "script"
  ]
  node [
    id 1950
    label "zajawka"
  ]
  node [
    id 1951
    label "list"
  ]
  node [
    id 1952
    label "adres"
  ]
  node [
    id 1953
    label "Zwrotnica"
  ]
  node [
    id 1954
    label "czasopismo"
  ]
  node [
    id 1955
    label "ok&#322;adka"
  ]
  node [
    id 1956
    label "ortografia"
  ]
  node [
    id 1957
    label "letter"
  ]
  node [
    id 1958
    label "paleografia"
  ]
  node [
    id 1959
    label "prasa"
  ]
  node [
    id 1960
    label "terminology"
  ]
  node [
    id 1961
    label "termin"
  ]
  node [
    id 1962
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 1963
    label "sk&#322;adnia"
  ]
  node [
    id 1964
    label "morfologia"
  ]
  node [
    id 1965
    label "g&#322;osownia"
  ]
  node [
    id 1966
    label "zasymilowa&#263;"
  ]
  node [
    id 1967
    label "phonetics"
  ]
  node [
    id 1968
    label "palatogram"
  ]
  node [
    id 1969
    label "transkrypcja"
  ]
  node [
    id 1970
    label "zasymilowanie"
  ]
  node [
    id 1971
    label "formu&#322;owa&#263;"
  ]
  node [
    id 1972
    label "ozdabia&#263;"
  ]
  node [
    id 1973
    label "skryba"
  ]
  node [
    id 1974
    label "read"
  ]
  node [
    id 1975
    label "donosi&#263;"
  ]
  node [
    id 1976
    label "dysgrafia"
  ]
  node [
    id 1977
    label "dysortografia"
  ]
  node [
    id 1978
    label "write"
  ]
  node [
    id 1979
    label "donie&#347;&#263;"
  ]
  node [
    id 1980
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 1981
    label "explanation"
  ]
  node [
    id 1982
    label "remark"
  ]
  node [
    id 1983
    label "przek&#322;adanie"
  ]
  node [
    id 1984
    label "zrozumia&#322;y"
  ]
  node [
    id 1985
    label "przekonywanie"
  ]
  node [
    id 1986
    label "uzasadnianie"
  ]
  node [
    id 1987
    label "rozwianie"
  ]
  node [
    id 1988
    label "rozwiewanie"
  ]
  node [
    id 1989
    label "gossip"
  ]
  node [
    id 1990
    label "rendition"
  ]
  node [
    id 1991
    label "kr&#281;ty"
  ]
  node [
    id 1992
    label "zinterpretowa&#263;"
  ]
  node [
    id 1993
    label "put"
  ]
  node [
    id 1994
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 1995
    label "przekona&#263;"
  ]
  node [
    id 1996
    label "frame"
  ]
  node [
    id 1997
    label "poja&#347;nia&#263;"
  ]
  node [
    id 1998
    label "u&#322;atwia&#263;"
  ]
  node [
    id 1999
    label "elaborate"
  ]
  node [
    id 2000
    label "suplikowa&#263;"
  ]
  node [
    id 2001
    label "przek&#322;ada&#263;"
  ]
  node [
    id 2002
    label "przekonywa&#263;"
  ]
  node [
    id 2003
    label "interpretowa&#263;"
  ]
  node [
    id 2004
    label "explain"
  ]
  node [
    id 2005
    label "uzasadnia&#263;"
  ]
  node [
    id 2006
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 2007
    label "gaworzy&#263;"
  ]
  node [
    id 2008
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 2009
    label "rozmawia&#263;"
  ]
  node [
    id 2010
    label "wyra&#380;a&#263;"
  ]
  node [
    id 2011
    label "umie&#263;"
  ]
  node [
    id 2012
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 2013
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 2014
    label "express"
  ]
  node [
    id 2015
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 2016
    label "talk"
  ]
  node [
    id 2017
    label "prawi&#263;"
  ]
  node [
    id 2018
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 2019
    label "powiada&#263;"
  ]
  node [
    id 2020
    label "chew_the_fat"
  ]
  node [
    id 2021
    label "say"
  ]
  node [
    id 2022
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 2023
    label "informowa&#263;"
  ]
  node [
    id 2024
    label "wydobywa&#263;"
  ]
  node [
    id 2025
    label "hermeneutyka"
  ]
  node [
    id 2026
    label "kontekst"
  ]
  node [
    id 2027
    label "apprehension"
  ]
  node [
    id 2028
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 2029
    label "interpretation"
  ]
  node [
    id 2030
    label "obja&#347;nienie"
  ]
  node [
    id 2031
    label "czucie"
  ]
  node [
    id 2032
    label "realization"
  ]
  node [
    id 2033
    label "kumanie"
  ]
  node [
    id 2034
    label "wnioskowanie"
  ]
  node [
    id 2035
    label "wiedzie&#263;"
  ]
  node [
    id 2036
    label "kuma&#263;"
  ]
  node [
    id 2037
    label "czu&#263;"
  ]
  node [
    id 2038
    label "match"
  ]
  node [
    id 2039
    label "empatia"
  ]
  node [
    id 2040
    label "odbiera&#263;"
  ]
  node [
    id 2041
    label "see"
  ]
  node [
    id 2042
    label "zna&#263;"
  ]
  node [
    id 2043
    label "validate"
  ]
  node [
    id 2044
    label "nadawa&#263;"
  ]
  node [
    id 2045
    label "precyzowa&#263;"
  ]
  node [
    id 2046
    label "nadawanie"
  ]
  node [
    id 2047
    label "precyzowanie"
  ]
  node [
    id 2048
    label "formalny"
  ]
  node [
    id 2049
    label "picie"
  ]
  node [
    id 2050
    label "usta"
  ]
  node [
    id 2051
    label "&#347;lina"
  ]
  node [
    id 2052
    label "consumption"
  ]
  node [
    id 2053
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 2054
    label "rozpuszczanie"
  ]
  node [
    id 2055
    label "aspiration"
  ]
  node [
    id 2056
    label "wci&#261;ganie"
  ]
  node [
    id 2057
    label "odci&#261;ganie"
  ]
  node [
    id 2058
    label "wessanie"
  ]
  node [
    id 2059
    label "ga&#378;nik"
  ]
  node [
    id 2060
    label "wysysanie"
  ]
  node [
    id 2061
    label "wyssanie"
  ]
  node [
    id 2062
    label "wada_wrodzona"
  ]
  node [
    id 2063
    label "znami&#281;"
  ]
  node [
    id 2064
    label "krosta"
  ]
  node [
    id 2065
    label "spot"
  ]
  node [
    id 2066
    label "brodawka"
  ]
  node [
    id 2067
    label "pip"
  ]
  node [
    id 2068
    label "dotykanie"
  ]
  node [
    id 2069
    label "przesuwanie"
  ]
  node [
    id 2070
    label "zlizanie"
  ]
  node [
    id 2071
    label "g&#322;askanie"
  ]
  node [
    id 2072
    label "wylizywanie"
  ]
  node [
    id 2073
    label "zlizywanie"
  ]
  node [
    id 2074
    label "wylizanie"
  ]
  node [
    id 2075
    label "pi&#263;"
  ]
  node [
    id 2076
    label "sponge"
  ]
  node [
    id 2077
    label "mleko"
  ]
  node [
    id 2078
    label "rozpuszcza&#263;"
  ]
  node [
    id 2079
    label "wci&#261;ga&#263;"
  ]
  node [
    id 2080
    label "rusza&#263;"
  ]
  node [
    id 2081
    label "sucking"
  ]
  node [
    id 2082
    label "smoczek"
  ]
  node [
    id 2083
    label "salt_lick"
  ]
  node [
    id 2084
    label "dotyka&#263;"
  ]
  node [
    id 2085
    label "muska&#263;"
  ]
  node [
    id 2086
    label "u&#380;ytkownik"
  ]
  node [
    id 2087
    label "dosadnie"
  ]
  node [
    id 2088
    label "rabelaisowski"
  ]
  node [
    id 2089
    label "bezpo&#347;redni"
  ]
  node [
    id 2090
    label "bliski"
  ]
  node [
    id 2091
    label "bezpo&#347;rednio"
  ]
  node [
    id 2092
    label "szczery"
  ]
  node [
    id 2093
    label "niepodwa&#380;alny"
  ]
  node [
    id 2094
    label "stabilny"
  ]
  node [
    id 2095
    label "trudny"
  ]
  node [
    id 2096
    label "krzepki"
  ]
  node [
    id 2097
    label "wyrazisty"
  ]
  node [
    id 2098
    label "przekonuj&#261;cy"
  ]
  node [
    id 2099
    label "widoczny"
  ]
  node [
    id 2100
    label "wzmocni&#263;"
  ]
  node [
    id 2101
    label "wzmacnia&#263;"
  ]
  node [
    id 2102
    label "konkretny"
  ]
  node [
    id 2103
    label "wytrzyma&#322;y"
  ]
  node [
    id 2104
    label "intensywnie"
  ]
  node [
    id 2105
    label "meflochina"
  ]
  node [
    id 2106
    label "po_rabelaisowsku"
  ]
  node [
    id 2107
    label "wyrazi&#347;cie"
  ]
  node [
    id 2108
    label "digress"
  ]
  node [
    id 2109
    label "biec"
  ]
  node [
    id 2110
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 2111
    label "odchodzi&#263;"
  ]
  node [
    id 2112
    label "r&#243;&#380;ni&#263;_si&#281;"
  ]
  node [
    id 2113
    label "blend"
  ]
  node [
    id 2114
    label "rozgrywa&#263;_si&#281;"
  ]
  node [
    id 2115
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 2116
    label "opuszcza&#263;"
  ]
  node [
    id 2117
    label "impart"
  ]
  node [
    id 2118
    label "wyrusza&#263;"
  ]
  node [
    id 2119
    label "odrzut"
  ]
  node [
    id 2120
    label "go"
  ]
  node [
    id 2121
    label "seclude"
  ]
  node [
    id 2122
    label "gasn&#261;&#263;"
  ]
  node [
    id 2123
    label "odstawa&#263;"
  ]
  node [
    id 2124
    label "rezygnowa&#263;"
  ]
  node [
    id 2125
    label "i&#347;&#263;"
  ]
  node [
    id 2126
    label "mija&#263;"
  ]
  node [
    id 2127
    label "startowa&#263;"
  ]
  node [
    id 2128
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 2129
    label "draw"
  ]
  node [
    id 2130
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 2131
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 2132
    label "bie&#380;e&#263;"
  ]
  node [
    id 2133
    label "spieszy&#263;_si&#281;"
  ]
  node [
    id 2134
    label "rush"
  ]
  node [
    id 2135
    label "przesuwa&#263;"
  ]
  node [
    id 2136
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 2137
    label "t&#322;oczy&#263;_si&#281;"
  ]
  node [
    id 2138
    label "wie&#347;&#263;"
  ]
  node [
    id 2139
    label "biega&#263;"
  ]
  node [
    id 2140
    label "tent-fly"
  ]
  node [
    id 2141
    label "przybywa&#263;"
  ]
  node [
    id 2142
    label "funkcjonowa&#263;"
  ]
  node [
    id 2143
    label "series"
  ]
  node [
    id 2144
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 2145
    label "praca_rolnicza"
  ]
  node [
    id 2146
    label "collection"
  ]
  node [
    id 2147
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 2148
    label "pakiet_klimatyczny"
  ]
  node [
    id 2149
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 2150
    label "sum"
  ]
  node [
    id 2151
    label "gathering"
  ]
  node [
    id 2152
    label "album"
  ]
  node [
    id 2153
    label "&#347;rodek"
  ]
  node [
    id 2154
    label "niezb&#281;dnik"
  ]
  node [
    id 2155
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 2156
    label "tylec"
  ]
  node [
    id 2157
    label "ko&#322;o"
  ]
  node [
    id 2158
    label "modalno&#347;&#263;"
  ]
  node [
    id 2159
    label "z&#261;b"
  ]
  node [
    id 2160
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 2161
    label "transportation_system"
  ]
  node [
    id 2162
    label "explicite"
  ]
  node [
    id 2163
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 2164
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 2165
    label "implicite"
  ]
  node [
    id 2166
    label "ekspedytor"
  ]
  node [
    id 2167
    label "location"
  ]
  node [
    id 2168
    label "uwaga"
  ]
  node [
    id 2169
    label "status"
  ]
  node [
    id 2170
    label "rz&#261;d"
  ]
  node [
    id 2171
    label "kszta&#322;towanie"
  ]
  node [
    id 2172
    label "egress"
  ]
  node [
    id 2173
    label "skombinowanie"
  ]
  node [
    id 2174
    label "pracownik"
  ]
  node [
    id 2175
    label "weryfikator"
  ]
  node [
    id 2176
    label "po&#347;rednio"
  ]
  node [
    id 2177
    label "bind"
  ]
  node [
    id 2178
    label "panowa&#263;"
  ]
  node [
    id 2179
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 2180
    label "zjednywa&#263;"
  ]
  node [
    id 2181
    label "manipulate"
  ]
  node [
    id 2182
    label "kontrolowa&#263;"
  ]
  node [
    id 2183
    label "dominowa&#263;"
  ]
  node [
    id 2184
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 2185
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 2186
    label "control"
  ]
  node [
    id 2187
    label "przewa&#380;a&#263;"
  ]
  node [
    id 2188
    label "winnings"
  ]
  node [
    id 2189
    label "zdobywa&#263;"
  ]
  node [
    id 2190
    label "wzbudza&#263;"
  ]
  node [
    id 2191
    label "raise"
  ]
  node [
    id 2192
    label "prosecute"
  ]
  node [
    id 2193
    label "cywilizacja"
  ]
  node [
    id 2194
    label "pole"
  ]
  node [
    id 2195
    label "elita"
  ]
  node [
    id 2196
    label "aspo&#322;eczny"
  ]
  node [
    id 2197
    label "ludzie_pracy"
  ]
  node [
    id 2198
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 2199
    label "pozaklasowy"
  ]
  node [
    id 2200
    label "uwarstwienie"
  ]
  node [
    id 2201
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 2202
    label "community"
  ]
  node [
    id 2203
    label "kastowo&#347;&#263;"
  ]
  node [
    id 2204
    label "facylitacja"
  ]
  node [
    id 2205
    label "uprawienie"
  ]
  node [
    id 2206
    label "p&#322;osa"
  ]
  node [
    id 2207
    label "ziemia"
  ]
  node [
    id 2208
    label "t&#322;o"
  ]
  node [
    id 2209
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 2210
    label "gospodarstwo"
  ]
  node [
    id 2211
    label "uprawi&#263;"
  ]
  node [
    id 2212
    label "room"
  ]
  node [
    id 2213
    label "dw&#243;r"
  ]
  node [
    id 2214
    label "okazja"
  ]
  node [
    id 2215
    label "rozmiar"
  ]
  node [
    id 2216
    label "irygowanie"
  ]
  node [
    id 2217
    label "square"
  ]
  node [
    id 2218
    label "zmienna"
  ]
  node [
    id 2219
    label "irygowa&#263;"
  ]
  node [
    id 2220
    label "socjologia"
  ]
  node [
    id 2221
    label "boisko"
  ]
  node [
    id 2222
    label "baza_danych"
  ]
  node [
    id 2223
    label "region"
  ]
  node [
    id 2224
    label "zagon"
  ]
  node [
    id 2225
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 2226
    label "plane"
  ]
  node [
    id 2227
    label "radlina"
  ]
  node [
    id 2228
    label "Fremeni"
  ]
  node [
    id 2229
    label "wagon"
  ]
  node [
    id 2230
    label "mecz_mistrzowski"
  ]
  node [
    id 2231
    label "arrangement"
  ]
  node [
    id 2232
    label "class"
  ]
  node [
    id 2233
    label "&#322;awka"
  ]
  node [
    id 2234
    label "wykrzyknik"
  ]
  node [
    id 2235
    label "zaleta"
  ]
  node [
    id 2236
    label "jednostka_systematyczna"
  ]
  node [
    id 2237
    label "programowanie_obiektowe"
  ]
  node [
    id 2238
    label "rezerwa"
  ]
  node [
    id 2239
    label "gromada"
  ]
  node [
    id 2240
    label "Ekwici"
  ]
  node [
    id 2241
    label "&#347;rodowisko"
  ]
  node [
    id 2242
    label "sala"
  ]
  node [
    id 2243
    label "pomoc"
  ]
  node [
    id 2244
    label "znak_jako&#347;ci"
  ]
  node [
    id 2245
    label "type"
  ]
  node [
    id 2246
    label "promocja"
  ]
  node [
    id 2247
    label "kurs"
  ]
  node [
    id 2248
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 2249
    label "dziennik_lekcyjny"
  ]
  node [
    id 2250
    label "fakcja"
  ]
  node [
    id 2251
    label "atak"
  ]
  node [
    id 2252
    label "botanika"
  ]
  node [
    id 2253
    label "elite"
  ]
  node [
    id 2254
    label "awans"
  ]
  node [
    id 2255
    label "podmiotowo"
  ]
  node [
    id 2256
    label "sytuacja"
  ]
  node [
    id 2257
    label "niekorzystny"
  ]
  node [
    id 2258
    label "aspo&#322;ecznie"
  ]
  node [
    id 2259
    label "typowy"
  ]
  node [
    id 2260
    label "niech&#281;tny"
  ]
  node [
    id 2261
    label "niskogatunkowy"
  ]
  node [
    id 2262
    label "asymilowanie_si&#281;"
  ]
  node [
    id 2263
    label "Wsch&#243;d"
  ]
  node [
    id 2264
    label "przejmowanie"
  ]
  node [
    id 2265
    label "makrokosmos"
  ]
  node [
    id 2266
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 2267
    label "civilization"
  ]
  node [
    id 2268
    label "przejmowa&#263;"
  ]
  node [
    id 2269
    label "technika"
  ]
  node [
    id 2270
    label "kuchnia"
  ]
  node [
    id 2271
    label "rozw&#243;j"
  ]
  node [
    id 2272
    label "populace"
  ]
  node [
    id 2273
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 2274
    label "przej&#281;cie"
  ]
  node [
    id 2275
    label "przej&#261;&#263;"
  ]
  node [
    id 2276
    label "cywilizowanie"
  ]
  node [
    id 2277
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 2278
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 2279
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 2280
    label "stratification"
  ]
  node [
    id 2281
    label "lamination"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 742
  ]
  edge [
    source 13
    target 294
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 186
  ]
  edge [
    source 14
    target 187
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 752
  ]
  edge [
    source 14
    target 753
  ]
  edge [
    source 14
    target 754
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 755
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 757
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 758
  ]
  edge [
    source 14
    target 759
  ]
  edge [
    source 14
    target 760
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 762
  ]
  edge [
    source 14
    target 607
  ]
  edge [
    source 14
    target 763
  ]
  edge [
    source 14
    target 764
  ]
  edge [
    source 14
    target 765
  ]
  edge [
    source 14
    target 766
  ]
  edge [
    source 14
    target 767
  ]
  edge [
    source 14
    target 768
  ]
  edge [
    source 14
    target 769
  ]
  edge [
    source 14
    target 770
  ]
  edge [
    source 14
    target 771
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 773
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 14
    target 38
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 37
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 483
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 334
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 477
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 466
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 465
  ]
  edge [
    source 15
    target 419
  ]
  edge [
    source 15
    target 467
  ]
  edge [
    source 15
    target 468
  ]
  edge [
    source 15
    target 469
  ]
  edge [
    source 15
    target 470
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 817
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 461
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 820
  ]
  edge [
    source 15
    target 821
  ]
  edge [
    source 15
    target 822
  ]
  edge [
    source 15
    target 823
  ]
  edge [
    source 15
    target 824
  ]
  edge [
    source 15
    target 825
  ]
  edge [
    source 15
    target 826
  ]
  edge [
    source 15
    target 827
  ]
  edge [
    source 15
    target 828
  ]
  edge [
    source 15
    target 829
  ]
  edge [
    source 15
    target 830
  ]
  edge [
    source 15
    target 831
  ]
  edge [
    source 15
    target 832
  ]
  edge [
    source 15
    target 833
  ]
  edge [
    source 15
    target 834
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 836
  ]
  edge [
    source 15
    target 837
  ]
  edge [
    source 15
    target 838
  ]
  edge [
    source 15
    target 839
  ]
  edge [
    source 15
    target 840
  ]
  edge [
    source 15
    target 841
  ]
  edge [
    source 15
    target 842
  ]
  edge [
    source 15
    target 843
  ]
  edge [
    source 15
    target 844
  ]
  edge [
    source 15
    target 845
  ]
  edge [
    source 15
    target 846
  ]
  edge [
    source 15
    target 847
  ]
  edge [
    source 15
    target 848
  ]
  edge [
    source 15
    target 849
  ]
  edge [
    source 15
    target 850
  ]
  edge [
    source 15
    target 851
  ]
  edge [
    source 15
    target 575
  ]
  edge [
    source 15
    target 852
  ]
  edge [
    source 15
    target 853
  ]
  edge [
    source 15
    target 854
  ]
  edge [
    source 15
    target 855
  ]
  edge [
    source 15
    target 856
  ]
  edge [
    source 15
    target 857
  ]
  edge [
    source 15
    target 858
  ]
  edge [
    source 15
    target 859
  ]
  edge [
    source 15
    target 860
  ]
  edge [
    source 15
    target 861
  ]
  edge [
    source 15
    target 862
  ]
  edge [
    source 15
    target 863
  ]
  edge [
    source 15
    target 864
  ]
  edge [
    source 15
    target 865
  ]
  edge [
    source 15
    target 866
  ]
  edge [
    source 15
    target 867
  ]
  edge [
    source 15
    target 868
  ]
  edge [
    source 15
    target 869
  ]
  edge [
    source 15
    target 870
  ]
  edge [
    source 15
    target 871
  ]
  edge [
    source 15
    target 872
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 873
  ]
  edge [
    source 15
    target 874
  ]
  edge [
    source 15
    target 875
  ]
  edge [
    source 15
    target 479
  ]
  edge [
    source 15
    target 876
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 877
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 878
  ]
  edge [
    source 15
    target 879
  ]
  edge [
    source 15
    target 880
  ]
  edge [
    source 15
    target 881
  ]
  edge [
    source 15
    target 882
  ]
  edge [
    source 15
    target 883
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 887
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 890
  ]
  edge [
    source 15
    target 891
  ]
  edge [
    source 15
    target 892
  ]
  edge [
    source 15
    target 893
  ]
  edge [
    source 15
    target 894
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 439
  ]
  edge [
    source 15
    target 895
  ]
  edge [
    source 15
    target 896
  ]
  edge [
    source 15
    target 897
  ]
  edge [
    source 15
    target 898
  ]
  edge [
    source 15
    target 899
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 15
    target 901
  ]
  edge [
    source 15
    target 902
  ]
  edge [
    source 15
    target 903
  ]
  edge [
    source 15
    target 904
  ]
  edge [
    source 15
    target 905
  ]
  edge [
    source 15
    target 906
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 907
  ]
  edge [
    source 15
    target 908
  ]
  edge [
    source 15
    target 909
  ]
  edge [
    source 15
    target 910
  ]
  edge [
    source 15
    target 911
  ]
  edge [
    source 15
    target 912
  ]
  edge [
    source 15
    target 913
  ]
  edge [
    source 15
    target 914
  ]
  edge [
    source 15
    target 915
  ]
  edge [
    source 15
    target 916
  ]
  edge [
    source 15
    target 917
  ]
  edge [
    source 15
    target 27
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 16
    target 30
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 16
    target 28
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 16
    target 930
  ]
  edge [
    source 16
    target 931
  ]
  edge [
    source 16
    target 932
  ]
  edge [
    source 16
    target 601
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 933
  ]
  edge [
    source 16
    target 934
  ]
  edge [
    source 16
    target 935
  ]
  edge [
    source 16
    target 936
  ]
  edge [
    source 16
    target 937
  ]
  edge [
    source 16
    target 938
  ]
  edge [
    source 16
    target 96
  ]
  edge [
    source 16
    target 939
  ]
  edge [
    source 16
    target 940
  ]
  edge [
    source 16
    target 566
  ]
  edge [
    source 16
    target 941
  ]
  edge [
    source 16
    target 642
  ]
  edge [
    source 16
    target 942
  ]
  edge [
    source 16
    target 943
  ]
  edge [
    source 16
    target 944
  ]
  edge [
    source 16
    target 945
  ]
  edge [
    source 16
    target 946
  ]
  edge [
    source 16
    target 947
  ]
  edge [
    source 16
    target 948
  ]
  edge [
    source 16
    target 949
  ]
  edge [
    source 16
    target 950
  ]
  edge [
    source 16
    target 951
  ]
  edge [
    source 16
    target 480
  ]
  edge [
    source 16
    target 952
  ]
  edge [
    source 16
    target 637
  ]
  edge [
    source 16
    target 953
  ]
  edge [
    source 16
    target 638
  ]
  edge [
    source 16
    target 954
  ]
  edge [
    source 16
    target 651
  ]
  edge [
    source 16
    target 463
  ]
  edge [
    source 16
    target 645
  ]
  edge [
    source 16
    target 955
  ]
  edge [
    source 16
    target 956
  ]
  edge [
    source 16
    target 957
  ]
  edge [
    source 16
    target 958
  ]
  edge [
    source 16
    target 959
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 961
  ]
  edge [
    source 16
    target 962
  ]
  edge [
    source 16
    target 963
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 966
  ]
  edge [
    source 16
    target 967
  ]
  edge [
    source 16
    target 968
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 16
    target 971
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 972
  ]
  edge [
    source 16
    target 973
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 974
  ]
  edge [
    source 16
    target 483
  ]
  edge [
    source 16
    target 975
  ]
  edge [
    source 16
    target 976
  ]
  edge [
    source 16
    target 977
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 978
  ]
  edge [
    source 16
    target 979
  ]
  edge [
    source 16
    target 980
  ]
  edge [
    source 16
    target 981
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 982
  ]
  edge [
    source 16
    target 983
  ]
  edge [
    source 16
    target 984
  ]
  edge [
    source 16
    target 985
  ]
  edge [
    source 16
    target 986
  ]
  edge [
    source 16
    target 987
  ]
  edge [
    source 16
    target 988
  ]
  edge [
    source 16
    target 989
  ]
  edge [
    source 16
    target 990
  ]
  edge [
    source 16
    target 991
  ]
  edge [
    source 16
    target 992
  ]
  edge [
    source 16
    target 993
  ]
  edge [
    source 16
    target 994
  ]
  edge [
    source 16
    target 995
  ]
  edge [
    source 16
    target 236
  ]
  edge [
    source 16
    target 996
  ]
  edge [
    source 16
    target 997
  ]
  edge [
    source 16
    target 998
  ]
  edge [
    source 16
    target 999
  ]
  edge [
    source 16
    target 1000
  ]
  edge [
    source 16
    target 1001
  ]
  edge [
    source 16
    target 1002
  ]
  edge [
    source 16
    target 1003
  ]
  edge [
    source 16
    target 1004
  ]
  edge [
    source 16
    target 1005
  ]
  edge [
    source 16
    target 1006
  ]
  edge [
    source 16
    target 1007
  ]
  edge [
    source 16
    target 1008
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 19
    target 294
  ]
  edge [
    source 19
    target 1026
  ]
  edge [
    source 19
    target 1027
  ]
  edge [
    source 19
    target 1028
  ]
  edge [
    source 19
    target 1029
  ]
  edge [
    source 19
    target 700
  ]
  edge [
    source 19
    target 701
  ]
  edge [
    source 19
    target 702
  ]
  edge [
    source 19
    target 703
  ]
  edge [
    source 19
    target 704
  ]
  edge [
    source 19
    target 705
  ]
  edge [
    source 19
    target 706
  ]
  edge [
    source 19
    target 707
  ]
  edge [
    source 19
    target 708
  ]
  edge [
    source 19
    target 709
  ]
  edge [
    source 19
    target 710
  ]
  edge [
    source 19
    target 711
  ]
  edge [
    source 19
    target 712
  ]
  edge [
    source 19
    target 713
  ]
  edge [
    source 19
    target 714
  ]
  edge [
    source 19
    target 715
  ]
  edge [
    source 19
    target 716
  ]
  edge [
    source 19
    target 717
  ]
  edge [
    source 19
    target 718
  ]
  edge [
    source 19
    target 600
  ]
  edge [
    source 19
    target 719
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1030
  ]
  edge [
    source 20
    target 1031
  ]
  edge [
    source 20
    target 1032
  ]
  edge [
    source 20
    target 246
  ]
  edge [
    source 20
    target 1033
  ]
  edge [
    source 20
    target 1034
  ]
  edge [
    source 20
    target 474
  ]
  edge [
    source 20
    target 1035
  ]
  edge [
    source 20
    target 1036
  ]
  edge [
    source 20
    target 1037
  ]
  edge [
    source 20
    target 1038
  ]
  edge [
    source 20
    target 1039
  ]
  edge [
    source 20
    target 1040
  ]
  edge [
    source 20
    target 1041
  ]
  edge [
    source 20
    target 1042
  ]
  edge [
    source 20
    target 466
  ]
  edge [
    source 20
    target 1043
  ]
  edge [
    source 20
    target 1044
  ]
  edge [
    source 20
    target 1045
  ]
  edge [
    source 20
    target 1046
  ]
  edge [
    source 20
    target 1047
  ]
  edge [
    source 20
    target 1048
  ]
  edge [
    source 20
    target 113
  ]
  edge [
    source 20
    target 1049
  ]
  edge [
    source 20
    target 1050
  ]
  edge [
    source 20
    target 1051
  ]
  edge [
    source 20
    target 1052
  ]
  edge [
    source 20
    target 1053
  ]
  edge [
    source 20
    target 1054
  ]
  edge [
    source 20
    target 1055
  ]
  edge [
    source 20
    target 985
  ]
  edge [
    source 20
    target 1056
  ]
  edge [
    source 20
    target 1057
  ]
  edge [
    source 20
    target 1058
  ]
  edge [
    source 20
    target 1059
  ]
  edge [
    source 20
    target 1060
  ]
  edge [
    source 20
    target 1061
  ]
  edge [
    source 20
    target 1062
  ]
  edge [
    source 20
    target 1063
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 1064
  ]
  edge [
    source 21
    target 1065
  ]
  edge [
    source 21
    target 1066
  ]
  edge [
    source 21
    target 1067
  ]
  edge [
    source 21
    target 1068
  ]
  edge [
    source 21
    target 1069
  ]
  edge [
    source 21
    target 1070
  ]
  edge [
    source 21
    target 1071
  ]
  edge [
    source 21
    target 1072
  ]
  edge [
    source 21
    target 1073
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 29
  ]
  edge [
    source 22
    target 1074
  ]
  edge [
    source 22
    target 1012
  ]
  edge [
    source 22
    target 1013
  ]
  edge [
    source 22
    target 1075
  ]
  edge [
    source 22
    target 37
  ]
  edge [
    source 23
    target 793
  ]
  edge [
    source 23
    target 700
  ]
  edge [
    source 23
    target 1076
  ]
  edge [
    source 23
    target 991
  ]
  edge [
    source 23
    target 1077
  ]
  edge [
    source 23
    target 1078
  ]
  edge [
    source 23
    target 1079
  ]
  edge [
    source 23
    target 1080
  ]
  edge [
    source 23
    target 1081
  ]
  edge [
    source 23
    target 1082
  ]
  edge [
    source 23
    target 37
  ]
  edge [
    source 23
    target 461
  ]
  edge [
    source 23
    target 1083
  ]
  edge [
    source 23
    target 933
  ]
  edge [
    source 23
    target 1084
  ]
  edge [
    source 23
    target 1085
  ]
  edge [
    source 23
    target 1086
  ]
  edge [
    source 23
    target 1087
  ]
  edge [
    source 23
    target 1088
  ]
  edge [
    source 23
    target 1089
  ]
  edge [
    source 23
    target 1090
  ]
  edge [
    source 23
    target 1091
  ]
  edge [
    source 23
    target 1092
  ]
  edge [
    source 23
    target 1093
  ]
  edge [
    source 23
    target 945
  ]
  edge [
    source 23
    target 1094
  ]
  edge [
    source 23
    target 652
  ]
  edge [
    source 23
    target 699
  ]
  edge [
    source 23
    target 419
  ]
  edge [
    source 23
    target 1095
  ]
  edge [
    source 23
    target 1096
  ]
  edge [
    source 23
    target 1097
  ]
  edge [
    source 23
    target 47
  ]
  edge [
    source 23
    target 1098
  ]
  edge [
    source 23
    target 1099
  ]
  edge [
    source 23
    target 978
  ]
  edge [
    source 23
    target 1100
  ]
  edge [
    source 23
    target 1101
  ]
  edge [
    source 23
    target 1102
  ]
  edge [
    source 23
    target 1103
  ]
  edge [
    source 23
    target 1104
  ]
  edge [
    source 23
    target 1105
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 1106
  ]
  edge [
    source 23
    target 1107
  ]
  edge [
    source 23
    target 1108
  ]
  edge [
    source 23
    target 1109
  ]
  edge [
    source 23
    target 1110
  ]
  edge [
    source 23
    target 1111
  ]
  edge [
    source 23
    target 1112
  ]
  edge [
    source 23
    target 1113
  ]
  edge [
    source 23
    target 1114
  ]
  edge [
    source 23
    target 1115
  ]
  edge [
    source 23
    target 1116
  ]
  edge [
    source 23
    target 1117
  ]
  edge [
    source 23
    target 1118
  ]
  edge [
    source 23
    target 1119
  ]
  edge [
    source 23
    target 1120
  ]
  edge [
    source 23
    target 1121
  ]
  edge [
    source 23
    target 1122
  ]
  edge [
    source 23
    target 1123
  ]
  edge [
    source 23
    target 1124
  ]
  edge [
    source 23
    target 1125
  ]
  edge [
    source 23
    target 1126
  ]
  edge [
    source 23
    target 1127
  ]
  edge [
    source 23
    target 1128
  ]
  edge [
    source 23
    target 1129
  ]
  edge [
    source 23
    target 1130
  ]
  edge [
    source 23
    target 1131
  ]
  edge [
    source 23
    target 1132
  ]
  edge [
    source 23
    target 1133
  ]
  edge [
    source 23
    target 1134
  ]
  edge [
    source 23
    target 1135
  ]
  edge [
    source 23
    target 1136
  ]
  edge [
    source 23
    target 1137
  ]
  edge [
    source 23
    target 1138
  ]
  edge [
    source 23
    target 1139
  ]
  edge [
    source 23
    target 1140
  ]
  edge [
    source 23
    target 1141
  ]
  edge [
    source 23
    target 1142
  ]
  edge [
    source 23
    target 1143
  ]
  edge [
    source 23
    target 782
  ]
  edge [
    source 23
    target 1144
  ]
  edge [
    source 23
    target 1145
  ]
  edge [
    source 23
    target 1146
  ]
  edge [
    source 23
    target 1147
  ]
  edge [
    source 23
    target 1148
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 151
  ]
  edge [
    source 24
    target 593
  ]
  edge [
    source 24
    target 1149
  ]
  edge [
    source 24
    target 461
  ]
  edge [
    source 24
    target 1150
  ]
  edge [
    source 24
    target 1151
  ]
  edge [
    source 24
    target 134
  ]
  edge [
    source 24
    target 1152
  ]
  edge [
    source 24
    target 1153
  ]
  edge [
    source 24
    target 137
  ]
  edge [
    source 24
    target 1154
  ]
  edge [
    source 24
    target 139
  ]
  edge [
    source 24
    target 594
  ]
  edge [
    source 24
    target 1155
  ]
  edge [
    source 24
    target 1156
  ]
  edge [
    source 24
    target 1157
  ]
  edge [
    source 24
    target 1158
  ]
  edge [
    source 24
    target 141
  ]
  edge [
    source 24
    target 598
  ]
  edge [
    source 24
    target 1159
  ]
  edge [
    source 24
    target 597
  ]
  edge [
    source 24
    target 1160
  ]
  edge [
    source 24
    target 1161
  ]
  edge [
    source 24
    target 1162
  ]
  edge [
    source 24
    target 142
  ]
  edge [
    source 24
    target 1163
  ]
  edge [
    source 24
    target 1164
  ]
  edge [
    source 24
    target 1165
  ]
  edge [
    source 24
    target 1166
  ]
  edge [
    source 24
    target 601
  ]
  edge [
    source 24
    target 466
  ]
  edge [
    source 24
    target 1167
  ]
  edge [
    source 24
    target 1168
  ]
  edge [
    source 24
    target 1169
  ]
  edge [
    source 24
    target 1170
  ]
  edge [
    source 24
    target 43
  ]
  edge [
    source 24
    target 1171
  ]
  edge [
    source 24
    target 596
  ]
  edge [
    source 24
    target 149
  ]
  edge [
    source 24
    target 1172
  ]
  edge [
    source 24
    target 1173
  ]
  edge [
    source 24
    target 1174
  ]
  edge [
    source 24
    target 1175
  ]
  edge [
    source 24
    target 600
  ]
  edge [
    source 24
    target 599
  ]
  edge [
    source 24
    target 974
  ]
  edge [
    source 24
    target 1176
  ]
  edge [
    source 24
    target 1177
  ]
  edge [
    source 24
    target 1178
  ]
  edge [
    source 24
    target 462
  ]
  edge [
    source 24
    target 1179
  ]
  edge [
    source 24
    target 1180
  ]
  edge [
    source 24
    target 1181
  ]
  edge [
    source 24
    target 1182
  ]
  edge [
    source 24
    target 1183
  ]
  edge [
    source 24
    target 1184
  ]
  edge [
    source 24
    target 1185
  ]
  edge [
    source 24
    target 1186
  ]
  edge [
    source 24
    target 1187
  ]
  edge [
    source 24
    target 1188
  ]
  edge [
    source 24
    target 595
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 602
  ]
  edge [
    source 24
    target 1189
  ]
  edge [
    source 24
    target 1190
  ]
  edge [
    source 24
    target 1191
  ]
  edge [
    source 24
    target 1192
  ]
  edge [
    source 24
    target 1193
  ]
  edge [
    source 24
    target 1194
  ]
  edge [
    source 24
    target 1195
  ]
  edge [
    source 24
    target 682
  ]
  edge [
    source 24
    target 1196
  ]
  edge [
    source 24
    target 1197
  ]
  edge [
    source 24
    target 312
  ]
  edge [
    source 24
    target 1198
  ]
  edge [
    source 24
    target 1199
  ]
  edge [
    source 24
    target 1200
  ]
  edge [
    source 24
    target 1201
  ]
  edge [
    source 24
    target 1202
  ]
  edge [
    source 24
    target 1203
  ]
  edge [
    source 24
    target 806
  ]
  edge [
    source 24
    target 637
  ]
  edge [
    source 24
    target 1204
  ]
  edge [
    source 24
    target 1205
  ]
  edge [
    source 24
    target 805
  ]
  edge [
    source 24
    target 1206
  ]
  edge [
    source 24
    target 1207
  ]
  edge [
    source 24
    target 1208
  ]
  edge [
    source 24
    target 1209
  ]
  edge [
    source 24
    target 1210
  ]
  edge [
    source 24
    target 113
  ]
  edge [
    source 24
    target 1211
  ]
  edge [
    source 24
    target 1212
  ]
  edge [
    source 24
    target 1213
  ]
  edge [
    source 24
    target 1214
  ]
  edge [
    source 24
    target 709
  ]
  edge [
    source 24
    target 1215
  ]
  edge [
    source 24
    target 1216
  ]
  edge [
    source 24
    target 1217
  ]
  edge [
    source 24
    target 128
  ]
  edge [
    source 24
    target 1218
  ]
  edge [
    source 24
    target 1219
  ]
  edge [
    source 24
    target 1220
  ]
  edge [
    source 24
    target 1221
  ]
  edge [
    source 24
    target 560
  ]
  edge [
    source 24
    target 1222
  ]
  edge [
    source 24
    target 1223
  ]
  edge [
    source 24
    target 107
  ]
  edge [
    source 24
    target 108
  ]
  edge [
    source 24
    target 109
  ]
  edge [
    source 24
    target 50
  ]
  edge [
    source 24
    target 933
  ]
  edge [
    source 24
    target 934
  ]
  edge [
    source 24
    target 935
  ]
  edge [
    source 24
    target 1224
  ]
  edge [
    source 24
    target 1225
  ]
  edge [
    source 24
    target 1226
  ]
  edge [
    source 24
    target 1227
  ]
  edge [
    source 24
    target 1228
  ]
  edge [
    source 24
    target 1229
  ]
  edge [
    source 24
    target 1230
  ]
  edge [
    source 24
    target 1231
  ]
  edge [
    source 24
    target 483
  ]
  edge [
    source 24
    target 1232
  ]
  edge [
    source 24
    target 1233
  ]
  edge [
    source 24
    target 1234
  ]
  edge [
    source 24
    target 1235
  ]
  edge [
    source 24
    target 89
  ]
  edge [
    source 24
    target 1236
  ]
  edge [
    source 24
    target 1237
  ]
  edge [
    source 24
    target 1238
  ]
  edge [
    source 24
    target 1239
  ]
  edge [
    source 24
    target 1240
  ]
  edge [
    source 24
    target 1241
  ]
  edge [
    source 24
    target 1242
  ]
  edge [
    source 24
    target 1243
  ]
  edge [
    source 24
    target 1244
  ]
  edge [
    source 24
    target 1245
  ]
  edge [
    source 24
    target 1246
  ]
  edge [
    source 24
    target 1247
  ]
  edge [
    source 24
    target 1248
  ]
  edge [
    source 24
    target 1249
  ]
  edge [
    source 24
    target 1250
  ]
  edge [
    source 24
    target 1251
  ]
  edge [
    source 24
    target 792
  ]
  edge [
    source 24
    target 1037
  ]
  edge [
    source 24
    target 788
  ]
  edge [
    source 24
    target 1252
  ]
  edge [
    source 24
    target 781
  ]
  edge [
    source 24
    target 1253
  ]
  edge [
    source 24
    target 1254
  ]
  edge [
    source 24
    target 1255
  ]
  edge [
    source 24
    target 1256
  ]
  edge [
    source 24
    target 1257
  ]
  edge [
    source 24
    target 1258
  ]
  edge [
    source 24
    target 1259
  ]
  edge [
    source 24
    target 1260
  ]
  edge [
    source 24
    target 1261
  ]
  edge [
    source 24
    target 1262
  ]
  edge [
    source 24
    target 1263
  ]
  edge [
    source 24
    target 1264
  ]
  edge [
    source 24
    target 1265
  ]
  edge [
    source 24
    target 92
  ]
  edge [
    source 24
    target 1266
  ]
  edge [
    source 24
    target 1267
  ]
  edge [
    source 24
    target 1268
  ]
  edge [
    source 24
    target 93
  ]
  edge [
    source 24
    target 1269
  ]
  edge [
    source 24
    target 1270
  ]
  edge [
    source 24
    target 1271
  ]
  edge [
    source 24
    target 1272
  ]
  edge [
    source 24
    target 1273
  ]
  edge [
    source 24
    target 1274
  ]
  edge [
    source 24
    target 1275
  ]
  edge [
    source 24
    target 1276
  ]
  edge [
    source 24
    target 1277
  ]
  edge [
    source 24
    target 1278
  ]
  edge [
    source 24
    target 1279
  ]
  edge [
    source 24
    target 1280
  ]
  edge [
    source 24
    target 1122
  ]
  edge [
    source 24
    target 1281
  ]
  edge [
    source 24
    target 1282
  ]
  edge [
    source 24
    target 1283
  ]
  edge [
    source 24
    target 1284
  ]
  edge [
    source 24
    target 1285
  ]
  edge [
    source 24
    target 1286
  ]
  edge [
    source 24
    target 1287
  ]
  edge [
    source 24
    target 1288
  ]
  edge [
    source 24
    target 1143
  ]
  edge [
    source 24
    target 1289
  ]
  edge [
    source 24
    target 1290
  ]
  edge [
    source 24
    target 1291
  ]
  edge [
    source 24
    target 1292
  ]
  edge [
    source 24
    target 1293
  ]
  edge [
    source 24
    target 1294
  ]
  edge [
    source 24
    target 1295
  ]
  edge [
    source 24
    target 1296
  ]
  edge [
    source 24
    target 1297
  ]
  edge [
    source 24
    target 1298
  ]
  edge [
    source 24
    target 1299
  ]
  edge [
    source 24
    target 1300
  ]
  edge [
    source 24
    target 1301
  ]
  edge [
    source 24
    target 1302
  ]
  edge [
    source 24
    target 1303
  ]
  edge [
    source 24
    target 525
  ]
  edge [
    source 24
    target 1304
  ]
  edge [
    source 24
    target 1305
  ]
  edge [
    source 24
    target 1306
  ]
  edge [
    source 24
    target 1307
  ]
  edge [
    source 24
    target 252
  ]
  edge [
    source 24
    target 1308
  ]
  edge [
    source 24
    target 1309
  ]
  edge [
    source 24
    target 1310
  ]
  edge [
    source 24
    target 534
  ]
  edge [
    source 24
    target 535
  ]
  edge [
    source 24
    target 536
  ]
  edge [
    source 24
    target 1311
  ]
  edge [
    source 24
    target 545
  ]
  edge [
    source 24
    target 548
  ]
  edge [
    source 24
    target 1312
  ]
  edge [
    source 24
    target 549
  ]
  edge [
    source 24
    target 550
  ]
  edge [
    source 24
    target 1089
  ]
  edge [
    source 24
    target 1313
  ]
  edge [
    source 24
    target 551
  ]
  edge [
    source 24
    target 1314
  ]
  edge [
    source 24
    target 556
  ]
  edge [
    source 24
    target 1315
  ]
  edge [
    source 24
    target 1316
  ]
  edge [
    source 24
    target 1317
  ]
  edge [
    source 24
    target 1318
  ]
  edge [
    source 24
    target 883
  ]
  edge [
    source 24
    target 1319
  ]
  edge [
    source 24
    target 1320
  ]
  edge [
    source 24
    target 1321
  ]
  edge [
    source 24
    target 1322
  ]
  edge [
    source 24
    target 1323
  ]
  edge [
    source 24
    target 1324
  ]
  edge [
    source 24
    target 1325
  ]
  edge [
    source 24
    target 1326
  ]
  edge [
    source 24
    target 1327
  ]
  edge [
    source 24
    target 1328
  ]
  edge [
    source 24
    target 1329
  ]
  edge [
    source 24
    target 1330
  ]
  edge [
    source 24
    target 1331
  ]
  edge [
    source 24
    target 1332
  ]
  edge [
    source 24
    target 1333
  ]
  edge [
    source 24
    target 1334
  ]
  edge [
    source 24
    target 1335
  ]
  edge [
    source 24
    target 147
  ]
  edge [
    source 24
    target 1336
  ]
  edge [
    source 24
    target 1337
  ]
  edge [
    source 24
    target 152
  ]
  edge [
    source 24
    target 1338
  ]
  edge [
    source 24
    target 1339
  ]
  edge [
    source 24
    target 47
  ]
  edge [
    source 24
    target 1340
  ]
  edge [
    source 24
    target 136
  ]
  edge [
    source 24
    target 908
  ]
  edge [
    source 24
    target 1341
  ]
  edge [
    source 24
    target 1342
  ]
  edge [
    source 24
    target 777
  ]
  edge [
    source 24
    target 140
  ]
  edge [
    source 24
    target 1343
  ]
  edge [
    source 24
    target 1344
  ]
  edge [
    source 24
    target 1345
  ]
  edge [
    source 24
    target 1346
  ]
  edge [
    source 24
    target 1347
  ]
  edge [
    source 24
    target 1348
  ]
  edge [
    source 24
    target 263
  ]
  edge [
    source 24
    target 1349
  ]
  edge [
    source 24
    target 1350
  ]
  edge [
    source 24
    target 1351
  ]
  edge [
    source 24
    target 1352
  ]
  edge [
    source 24
    target 150
  ]
  edge [
    source 24
    target 120
  ]
  edge [
    source 24
    target 1353
  ]
  edge [
    source 24
    target 1354
  ]
  edge [
    source 24
    target 1355
  ]
  edge [
    source 24
    target 1356
  ]
  edge [
    source 24
    target 1357
  ]
  edge [
    source 24
    target 1358
  ]
  edge [
    source 24
    target 1359
  ]
  edge [
    source 24
    target 1360
  ]
  edge [
    source 24
    target 1361
  ]
  edge [
    source 24
    target 1362
  ]
  edge [
    source 24
    target 1363
  ]
  edge [
    source 24
    target 1364
  ]
  edge [
    source 24
    target 1365
  ]
  edge [
    source 24
    target 1092
  ]
  edge [
    source 24
    target 1366
  ]
  edge [
    source 24
    target 1367
  ]
  edge [
    source 24
    target 1368
  ]
  edge [
    source 24
    target 1369
  ]
  edge [
    source 24
    target 303
  ]
  edge [
    source 24
    target 1370
  ]
  edge [
    source 24
    target 1371
  ]
  edge [
    source 24
    target 1372
  ]
  edge [
    source 24
    target 1373
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 307
  ]
  edge [
    source 24
    target 1374
  ]
  edge [
    source 24
    target 1375
  ]
  edge [
    source 24
    target 1376
  ]
  edge [
    source 24
    target 1377
  ]
  edge [
    source 24
    target 1378
  ]
  edge [
    source 24
    target 1379
  ]
  edge [
    source 24
    target 1380
  ]
  edge [
    source 24
    target 1381
  ]
  edge [
    source 24
    target 1382
  ]
  edge [
    source 24
    target 1383
  ]
  edge [
    source 24
    target 1384
  ]
  edge [
    source 24
    target 1385
  ]
  edge [
    source 24
    target 1386
  ]
  edge [
    source 24
    target 1387
  ]
  edge [
    source 24
    target 1388
  ]
  edge [
    source 24
    target 1389
  ]
  edge [
    source 24
    target 1390
  ]
  edge [
    source 24
    target 1391
  ]
  edge [
    source 24
    target 449
  ]
  edge [
    source 24
    target 1392
  ]
  edge [
    source 24
    target 1393
  ]
  edge [
    source 24
    target 1394
  ]
  edge [
    source 24
    target 1395
  ]
  edge [
    source 24
    target 1396
  ]
  edge [
    source 24
    target 1397
  ]
  edge [
    source 24
    target 58
  ]
  edge [
    source 24
    target 1398
  ]
  edge [
    source 24
    target 815
  ]
  edge [
    source 24
    target 98
  ]
  edge [
    source 24
    target 1399
  ]
  edge [
    source 24
    target 723
  ]
  edge [
    source 24
    target 1400
  ]
  edge [
    source 24
    target 1401
  ]
  edge [
    source 24
    target 1402
  ]
  edge [
    source 24
    target 1403
  ]
  edge [
    source 24
    target 1404
  ]
  edge [
    source 24
    target 1405
  ]
  edge [
    source 24
    target 1406
  ]
  edge [
    source 24
    target 1407
  ]
  edge [
    source 24
    target 405
  ]
  edge [
    source 24
    target 1408
  ]
  edge [
    source 24
    target 1409
  ]
  edge [
    source 24
    target 1410
  ]
  edge [
    source 24
    target 1411
  ]
  edge [
    source 24
    target 1412
  ]
  edge [
    source 24
    target 1413
  ]
  edge [
    source 24
    target 1414
  ]
  edge [
    source 24
    target 1415
  ]
  edge [
    source 24
    target 1416
  ]
  edge [
    source 24
    target 1417
  ]
  edge [
    source 24
    target 1418
  ]
  edge [
    source 24
    target 1419
  ]
  edge [
    source 24
    target 1420
  ]
  edge [
    source 24
    target 1421
  ]
  edge [
    source 24
    target 1422
  ]
  edge [
    source 24
    target 1423
  ]
  edge [
    source 24
    target 1424
  ]
  edge [
    source 24
    target 1425
  ]
  edge [
    source 24
    target 1426
  ]
  edge [
    source 24
    target 1427
  ]
  edge [
    source 24
    target 1428
  ]
  edge [
    source 24
    target 1429
  ]
  edge [
    source 24
    target 1430
  ]
  edge [
    source 24
    target 1431
  ]
  edge [
    source 24
    target 1432
  ]
  edge [
    source 24
    target 1433
  ]
  edge [
    source 24
    target 1050
  ]
  edge [
    source 24
    target 1434
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1435
  ]
  edge [
    source 25
    target 1436
  ]
  edge [
    source 25
    target 1437
  ]
  edge [
    source 25
    target 1438
  ]
  edge [
    source 25
    target 1439
  ]
  edge [
    source 25
    target 1440
  ]
  edge [
    source 25
    target 1441
  ]
  edge [
    source 25
    target 1442
  ]
  edge [
    source 25
    target 1443
  ]
  edge [
    source 25
    target 1444
  ]
  edge [
    source 25
    target 1445
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 379
  ]
  edge [
    source 26
    target 1446
  ]
  edge [
    source 26
    target 1447
  ]
  edge [
    source 26
    target 1448
  ]
  edge [
    source 26
    target 1449
  ]
  edge [
    source 26
    target 402
  ]
  edge [
    source 26
    target 399
  ]
  edge [
    source 26
    target 400
  ]
  edge [
    source 26
    target 401
  ]
  edge [
    source 26
    target 403
  ]
  edge [
    source 26
    target 404
  ]
  edge [
    source 26
    target 405
  ]
  edge [
    source 26
    target 406
  ]
  edge [
    source 26
    target 1399
  ]
  edge [
    source 26
    target 1450
  ]
  edge [
    source 26
    target 1451
  ]
  edge [
    source 26
    target 1452
  ]
  edge [
    source 26
    target 1453
  ]
  edge [
    source 26
    target 1454
  ]
  edge [
    source 26
    target 1455
  ]
  edge [
    source 26
    target 1456
  ]
  edge [
    source 26
    target 1457
  ]
  edge [
    source 26
    target 1458
  ]
  edge [
    source 26
    target 1459
  ]
  edge [
    source 26
    target 755
  ]
  edge [
    source 26
    target 1460
  ]
  edge [
    source 26
    target 1461
  ]
  edge [
    source 26
    target 1462
  ]
  edge [
    source 26
    target 1463
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1464
  ]
  edge [
    source 27
    target 1465
  ]
  edge [
    source 27
    target 1466
  ]
  edge [
    source 27
    target 1467
  ]
  edge [
    source 27
    target 1468
  ]
  edge [
    source 27
    target 1469
  ]
  edge [
    source 27
    target 902
  ]
  edge [
    source 27
    target 434
  ]
  edge [
    source 27
    target 521
  ]
  edge [
    source 27
    target 955
  ]
  edge [
    source 27
    target 56
  ]
  edge [
    source 27
    target 992
  ]
  edge [
    source 27
    target 1470
  ]
  edge [
    source 27
    target 993
  ]
  edge [
    source 27
    target 1471
  ]
  edge [
    source 27
    target 1472
  ]
  edge [
    source 27
    target 1473
  ]
  edge [
    source 27
    target 1474
  ]
  edge [
    source 27
    target 1475
  ]
  edge [
    source 27
    target 1476
  ]
  edge [
    source 27
    target 1477
  ]
  edge [
    source 27
    target 1478
  ]
  edge [
    source 27
    target 1479
  ]
  edge [
    source 27
    target 69
  ]
  edge [
    source 27
    target 1480
  ]
  edge [
    source 27
    target 1481
  ]
  edge [
    source 27
    target 1482
  ]
  edge [
    source 27
    target 1483
  ]
  edge [
    source 27
    target 1484
  ]
  edge [
    source 27
    target 1270
  ]
  edge [
    source 27
    target 1485
  ]
  edge [
    source 27
    target 1486
  ]
  edge [
    source 27
    target 1487
  ]
  edge [
    source 27
    target 1488
  ]
  edge [
    source 27
    target 1489
  ]
  edge [
    source 27
    target 1490
  ]
  edge [
    source 27
    target 1491
  ]
  edge [
    source 27
    target 1492
  ]
  edge [
    source 27
    target 1493
  ]
  edge [
    source 27
    target 1494
  ]
  edge [
    source 27
    target 768
  ]
  edge [
    source 27
    target 483
  ]
  edge [
    source 27
    target 1495
  ]
  edge [
    source 27
    target 924
  ]
  edge [
    source 27
    target 58
  ]
  edge [
    source 27
    target 47
  ]
  edge [
    source 27
    target 952
  ]
  edge [
    source 27
    target 948
  ]
  edge [
    source 27
    target 1496
  ]
  edge [
    source 27
    target 1497
  ]
  edge [
    source 27
    target 1498
  ]
  edge [
    source 27
    target 1499
  ]
  edge [
    source 27
    target 926
  ]
  edge [
    source 27
    target 1500
  ]
  edge [
    source 27
    target 1501
  ]
  edge [
    source 27
    target 1502
  ]
  edge [
    source 27
    target 1503
  ]
  edge [
    source 27
    target 949
  ]
  edge [
    source 27
    target 1504
  ]
  edge [
    source 27
    target 1020
  ]
  edge [
    source 27
    target 1505
  ]
  edge [
    source 27
    target 951
  ]
  edge [
    source 27
    target 480
  ]
  edge [
    source 27
    target 1506
  ]
  edge [
    source 27
    target 1507
  ]
  edge [
    source 27
    target 1508
  ]
  edge [
    source 27
    target 1368
  ]
  edge [
    source 27
    target 1509
  ]
  edge [
    source 27
    target 1510
  ]
  edge [
    source 27
    target 1511
  ]
  edge [
    source 27
    target 1512
  ]
  edge [
    source 27
    target 1513
  ]
  edge [
    source 27
    target 1514
  ]
  edge [
    source 27
    target 96
  ]
  edge [
    source 27
    target 252
  ]
  edge [
    source 27
    target 1515
  ]
  edge [
    source 27
    target 1516
  ]
  edge [
    source 27
    target 1517
  ]
  edge [
    source 27
    target 1518
  ]
  edge [
    source 27
    target 1519
  ]
  edge [
    source 27
    target 1520
  ]
  edge [
    source 27
    target 1521
  ]
  edge [
    source 27
    target 1522
  ]
  edge [
    source 27
    target 37
  ]
  edge [
    source 27
    target 1523
  ]
  edge [
    source 27
    target 1524
  ]
  edge [
    source 27
    target 1525
  ]
  edge [
    source 27
    target 1526
  ]
  edge [
    source 27
    target 1527
  ]
  edge [
    source 27
    target 1528
  ]
  edge [
    source 27
    target 1529
  ]
  edge [
    source 27
    target 1043
  ]
  edge [
    source 27
    target 1530
  ]
  edge [
    source 27
    target 1531
  ]
  edge [
    source 27
    target 1260
  ]
  edge [
    source 27
    target 1037
  ]
  edge [
    source 27
    target 1532
  ]
  edge [
    source 27
    target 1533
  ]
  edge [
    source 27
    target 1534
  ]
  edge [
    source 27
    target 1071
  ]
  edge [
    source 27
    target 1535
  ]
  edge [
    source 27
    target 1536
  ]
  edge [
    source 27
    target 1537
  ]
  edge [
    source 27
    target 1538
  ]
  edge [
    source 27
    target 1539
  ]
  edge [
    source 27
    target 1540
  ]
  edge [
    source 27
    target 1541
  ]
  edge [
    source 27
    target 1542
  ]
  edge [
    source 27
    target 1543
  ]
  edge [
    source 27
    target 1544
  ]
  edge [
    source 27
    target 1545
  ]
  edge [
    source 27
    target 1546
  ]
  edge [
    source 27
    target 1547
  ]
  edge [
    source 27
    target 1548
  ]
  edge [
    source 27
    target 445
  ]
  edge [
    source 27
    target 108
  ]
  edge [
    source 27
    target 184
  ]
  edge [
    source 27
    target 1549
  ]
  edge [
    source 27
    target 1219
  ]
  edge [
    source 27
    target 1220
  ]
  edge [
    source 27
    target 1550
  ]
  edge [
    source 27
    target 1221
  ]
  edge [
    source 27
    target 1216
  ]
  edge [
    source 27
    target 1222
  ]
  edge [
    source 27
    target 68
  ]
  edge [
    source 27
    target 1223
  ]
  edge [
    source 27
    target 709
  ]
  edge [
    source 27
    target 1551
  ]
  edge [
    source 27
    target 1552
  ]
  edge [
    source 27
    target 1553
  ]
  edge [
    source 27
    target 1554
  ]
  edge [
    source 27
    target 1555
  ]
  edge [
    source 27
    target 1556
  ]
  edge [
    source 27
    target 1557
  ]
  edge [
    source 27
    target 1558
  ]
  edge [
    source 27
    target 607
  ]
  edge [
    source 27
    target 661
  ]
  edge [
    source 27
    target 1559
  ]
  edge [
    source 27
    target 1560
  ]
  edge [
    source 27
    target 1561
  ]
  edge [
    source 27
    target 895
  ]
  edge [
    source 27
    target 1562
  ]
  edge [
    source 27
    target 1563
  ]
  edge [
    source 27
    target 1564
  ]
  edge [
    source 27
    target 1565
  ]
  edge [
    source 27
    target 1566
  ]
  edge [
    source 27
    target 1567
  ]
  edge [
    source 27
    target 1568
  ]
  edge [
    source 27
    target 1569
  ]
  edge [
    source 27
    target 1570
  ]
  edge [
    source 27
    target 1571
  ]
  edge [
    source 27
    target 1572
  ]
  edge [
    source 27
    target 1573
  ]
  edge [
    source 27
    target 1574
  ]
  edge [
    source 27
    target 1575
  ]
  edge [
    source 27
    target 1576
  ]
  edge [
    source 27
    target 1577
  ]
  edge [
    source 27
    target 1578
  ]
  edge [
    source 27
    target 439
  ]
  edge [
    source 27
    target 1579
  ]
  edge [
    source 27
    target 1580
  ]
  edge [
    source 27
    target 1581
  ]
  edge [
    source 27
    target 1582
  ]
  edge [
    source 27
    target 1583
  ]
  edge [
    source 27
    target 991
  ]
  edge [
    source 27
    target 1584
  ]
  edge [
    source 27
    target 1585
  ]
  edge [
    source 27
    target 1586
  ]
  edge [
    source 27
    target 215
  ]
  edge [
    source 27
    target 1587
  ]
  edge [
    source 27
    target 1588
  ]
  edge [
    source 27
    target 1589
  ]
  edge [
    source 27
    target 1590
  ]
  edge [
    source 27
    target 899
  ]
  edge [
    source 27
    target 1591
  ]
  edge [
    source 27
    target 906
  ]
  edge [
    source 27
    target 1592
  ]
  edge [
    source 27
    target 908
  ]
  edge [
    source 27
    target 1593
  ]
  edge [
    source 27
    target 793
  ]
  edge [
    source 27
    target 780
  ]
  edge [
    source 27
    target 898
  ]
  edge [
    source 27
    target 123
  ]
  edge [
    source 27
    target 889
  ]
  edge [
    source 27
    target 1594
  ]
  edge [
    source 27
    target 1595
  ]
  edge [
    source 27
    target 1596
  ]
  edge [
    source 27
    target 1597
  ]
  edge [
    source 27
    target 1598
  ]
  edge [
    source 27
    target 1599
  ]
  edge [
    source 27
    target 1600
  ]
  edge [
    source 27
    target 1601
  ]
  edge [
    source 27
    target 1602
  ]
  edge [
    source 27
    target 909
  ]
  edge [
    source 27
    target 1603
  ]
  edge [
    source 27
    target 897
  ]
  edge [
    source 27
    target 901
  ]
  edge [
    source 27
    target 1604
  ]
  edge [
    source 27
    target 1605
  ]
  edge [
    source 27
    target 1606
  ]
  edge [
    source 27
    target 900
  ]
  edge [
    source 27
    target 903
  ]
  edge [
    source 27
    target 904
  ]
  edge [
    source 27
    target 1607
  ]
  edge [
    source 27
    target 905
  ]
  edge [
    source 27
    target 1608
  ]
  edge [
    source 27
    target 1609
  ]
  edge [
    source 27
    target 1610
  ]
  edge [
    source 27
    target 1611
  ]
  edge [
    source 27
    target 1612
  ]
  edge [
    source 27
    target 1613
  ]
  edge [
    source 27
    target 1614
  ]
  edge [
    source 27
    target 1615
  ]
  edge [
    source 27
    target 1616
  ]
  edge [
    source 27
    target 1617
  ]
  edge [
    source 27
    target 1618
  ]
  edge [
    source 27
    target 1619
  ]
  edge [
    source 27
    target 1620
  ]
  edge [
    source 27
    target 1621
  ]
  edge [
    source 27
    target 1622
  ]
  edge [
    source 27
    target 1623
  ]
  edge [
    source 27
    target 1624
  ]
  edge [
    source 27
    target 1625
  ]
  edge [
    source 27
    target 1626
  ]
  edge [
    source 27
    target 1627
  ]
  edge [
    source 27
    target 1628
  ]
  edge [
    source 27
    target 1629
  ]
  edge [
    source 27
    target 1630
  ]
  edge [
    source 27
    target 1631
  ]
  edge [
    source 27
    target 65
  ]
  edge [
    source 27
    target 1632
  ]
  edge [
    source 27
    target 1633
  ]
  edge [
    source 27
    target 1634
  ]
  edge [
    source 27
    target 1635
  ]
  edge [
    source 27
    target 1636
  ]
  edge [
    source 27
    target 1637
  ]
  edge [
    source 27
    target 1638
  ]
  edge [
    source 27
    target 1639
  ]
  edge [
    source 27
    target 1640
  ]
  edge [
    source 27
    target 1641
  ]
  edge [
    source 27
    target 1642
  ]
  edge [
    source 27
    target 1643
  ]
  edge [
    source 27
    target 1644
  ]
  edge [
    source 27
    target 1645
  ]
  edge [
    source 27
    target 1646
  ]
  edge [
    source 27
    target 1647
  ]
  edge [
    source 27
    target 1648
  ]
  edge [
    source 27
    target 1649
  ]
  edge [
    source 27
    target 1650
  ]
  edge [
    source 27
    target 1651
  ]
  edge [
    source 27
    target 1652
  ]
  edge [
    source 27
    target 1653
  ]
  edge [
    source 27
    target 1654
  ]
  edge [
    source 27
    target 1655
  ]
  edge [
    source 27
    target 1656
  ]
  edge [
    source 27
    target 1657
  ]
  edge [
    source 27
    target 1658
  ]
  edge [
    source 27
    target 1659
  ]
  edge [
    source 27
    target 1660
  ]
  edge [
    source 27
    target 1661
  ]
  edge [
    source 27
    target 1662
  ]
  edge [
    source 27
    target 1663
  ]
  edge [
    source 27
    target 1664
  ]
  edge [
    source 27
    target 1665
  ]
  edge [
    source 27
    target 1666
  ]
  edge [
    source 27
    target 1667
  ]
  edge [
    source 27
    target 1668
  ]
  edge [
    source 27
    target 1669
  ]
  edge [
    source 27
    target 1670
  ]
  edge [
    source 27
    target 1671
  ]
  edge [
    source 27
    target 1672
  ]
  edge [
    source 27
    target 1673
  ]
  edge [
    source 27
    target 1674
  ]
  edge [
    source 27
    target 1675
  ]
  edge [
    source 27
    target 1676
  ]
  edge [
    source 27
    target 468
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 975
  ]
  edge [
    source 28
    target 976
  ]
  edge [
    source 28
    target 977
  ]
  edge [
    source 28
    target 902
  ]
  edge [
    source 28
    target 978
  ]
  edge [
    source 28
    target 96
  ]
  edge [
    source 28
    target 979
  ]
  edge [
    source 28
    target 980
  ]
  edge [
    source 28
    target 981
  ]
  edge [
    source 28
    target 200
  ]
  edge [
    source 28
    target 113
  ]
  edge [
    source 28
    target 982
  ]
  edge [
    source 28
    target 983
  ]
  edge [
    source 28
    target 984
  ]
  edge [
    source 28
    target 985
  ]
  edge [
    source 28
    target 986
  ]
  edge [
    source 28
    target 987
  ]
  edge [
    source 28
    target 988
  ]
  edge [
    source 28
    target 989
  ]
  edge [
    source 28
    target 990
  ]
  edge [
    source 28
    target 991
  ]
  edge [
    source 28
    target 992
  ]
  edge [
    source 28
    target 993
  ]
  edge [
    source 28
    target 994
  ]
  edge [
    source 28
    target 995
  ]
  edge [
    source 28
    target 236
  ]
  edge [
    source 28
    target 637
  ]
  edge [
    source 28
    target 1596
  ]
  edge [
    source 28
    target 716
  ]
  edge [
    source 28
    target 601
  ]
  edge [
    source 28
    target 1677
  ]
  edge [
    source 28
    target 1678
  ]
  edge [
    source 28
    target 1679
  ]
  edge [
    source 28
    target 123
  ]
  edge [
    source 28
    target 1680
  ]
  edge [
    source 28
    target 1144
  ]
  edge [
    source 28
    target 1681
  ]
  edge [
    source 28
    target 1682
  ]
  edge [
    source 28
    target 1683
  ]
  edge [
    source 28
    target 1684
  ]
  edge [
    source 28
    target 1685
  ]
  edge [
    source 28
    target 1686
  ]
  edge [
    source 28
    target 457
  ]
  edge [
    source 28
    target 617
  ]
  edge [
    source 28
    target 1572
  ]
  edge [
    source 28
    target 1687
  ]
  edge [
    source 28
    target 1688
  ]
  edge [
    source 28
    target 1689
  ]
  edge [
    source 28
    target 1467
  ]
  edge [
    source 28
    target 1690
  ]
  edge [
    source 28
    target 1585
  ]
  edge [
    source 28
    target 1691
  ]
  edge [
    source 28
    target 1692
  ]
  edge [
    source 28
    target 1693
  ]
  edge [
    source 28
    target 1694
  ]
  edge [
    source 28
    target 1695
  ]
  edge [
    source 28
    target 1696
  ]
  edge [
    source 28
    target 1697
  ]
  edge [
    source 28
    target 1698
  ]
  edge [
    source 28
    target 465
  ]
  edge [
    source 28
    target 419
  ]
  edge [
    source 28
    target 466
  ]
  edge [
    source 28
    target 467
  ]
  edge [
    source 28
    target 468
  ]
  edge [
    source 28
    target 469
  ]
  edge [
    source 28
    target 470
  ]
  edge [
    source 28
    target 1095
  ]
  edge [
    source 28
    target 1096
  ]
  edge [
    source 28
    target 1097
  ]
  edge [
    source 28
    target 47
  ]
  edge [
    source 28
    target 1098
  ]
  edge [
    source 28
    target 1099
  ]
  edge [
    source 28
    target 1100
  ]
  edge [
    source 28
    target 1101
  ]
  edge [
    source 28
    target 1102
  ]
  edge [
    source 28
    target 1103
  ]
  edge [
    source 28
    target 1104
  ]
  edge [
    source 28
    target 1105
  ]
  edge [
    source 28
    target 1106
  ]
  edge [
    source 28
    target 1107
  ]
  edge [
    source 28
    target 1108
  ]
  edge [
    source 28
    target 1109
  ]
  edge [
    source 28
    target 1110
  ]
  edge [
    source 28
    target 1111
  ]
  edge [
    source 28
    target 1464
  ]
  edge [
    source 28
    target 1699
  ]
  edge [
    source 28
    target 630
  ]
  edge [
    source 28
    target 1700
  ]
  edge [
    source 28
    target 554
  ]
  edge [
    source 28
    target 1365
  ]
  edge [
    source 28
    target 1701
  ]
  edge [
    source 28
    target 1702
  ]
  edge [
    source 28
    target 1703
  ]
  edge [
    source 28
    target 93
  ]
  edge [
    source 28
    target 918
  ]
  edge [
    source 28
    target 920
  ]
  edge [
    source 28
    target 919
  ]
  edge [
    source 28
    target 923
  ]
  edge [
    source 28
    target 921
  ]
  edge [
    source 28
    target 922
  ]
  edge [
    source 28
    target 924
  ]
  edge [
    source 28
    target 925
  ]
  edge [
    source 28
    target 926
  ]
  edge [
    source 28
    target 927
  ]
  edge [
    source 28
    target 928
  ]
  edge [
    source 28
    target 929
  ]
  edge [
    source 28
    target 930
  ]
  edge [
    source 28
    target 931
  ]
  edge [
    source 28
    target 932
  ]
  edge [
    source 28
    target 1704
  ]
  edge [
    source 28
    target 1433
  ]
  edge [
    source 28
    target 1705
  ]
  edge [
    source 28
    target 1195
  ]
  edge [
    source 28
    target 1706
  ]
  edge [
    source 28
    target 1155
  ]
  edge [
    source 28
    target 1707
  ]
  edge [
    source 28
    target 1708
  ]
  edge [
    source 28
    target 1709
  ]
  edge [
    source 28
    target 1201
  ]
  edge [
    source 28
    target 1710
  ]
  edge [
    source 28
    target 1588
  ]
  edge [
    source 28
    target 1711
  ]
  edge [
    source 28
    target 1712
  ]
  edge [
    source 28
    target 461
  ]
  edge [
    source 28
    target 1713
  ]
  edge [
    source 28
    target 1714
  ]
  edge [
    source 28
    target 1715
  ]
  edge [
    source 28
    target 266
  ]
  edge [
    source 28
    target 1716
  ]
  edge [
    source 28
    target 1717
  ]
  edge [
    source 28
    target 1718
  ]
  edge [
    source 28
    target 1719
  ]
  edge [
    source 28
    target 1720
  ]
  edge [
    source 28
    target 1721
  ]
  edge [
    source 28
    target 1722
  ]
  edge [
    source 28
    target 1723
  ]
  edge [
    source 28
    target 1724
  ]
  edge [
    source 28
    target 1725
  ]
  edge [
    source 28
    target 1726
  ]
  edge [
    source 28
    target 777
  ]
  edge [
    source 28
    target 1727
  ]
  edge [
    source 28
    target 1728
  ]
  edge [
    source 28
    target 1033
  ]
  edge [
    source 28
    target 1729
  ]
  edge [
    source 28
    target 1730
  ]
  edge [
    source 28
    target 1549
  ]
  edge [
    source 28
    target 1731
  ]
  edge [
    source 28
    target 1732
  ]
  edge [
    source 28
    target 1733
  ]
  edge [
    source 28
    target 1734
  ]
  edge [
    source 28
    target 1735
  ]
  edge [
    source 28
    target 1736
  ]
  edge [
    source 28
    target 1737
  ]
  edge [
    source 28
    target 1738
  ]
  edge [
    source 28
    target 1299
  ]
  edge [
    source 28
    target 1739
  ]
  edge [
    source 28
    target 1740
  ]
  edge [
    source 28
    target 1741
  ]
  edge [
    source 28
    target 1742
  ]
  edge [
    source 28
    target 1743
  ]
  edge [
    source 28
    target 1744
  ]
  edge [
    source 28
    target 1745
  ]
  edge [
    source 28
    target 1746
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 246
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 252
  ]
  edge [
    source 28
    target 253
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 102
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 260
  ]
  edge [
    source 28
    target 261
  ]
  edge [
    source 28
    target 262
  ]
  edge [
    source 28
    target 263
  ]
  edge [
    source 28
    target 264
  ]
  edge [
    source 28
    target 265
  ]
  edge [
    source 28
    target 267
  ]
  edge [
    source 28
    target 268
  ]
  edge [
    source 28
    target 269
  ]
  edge [
    source 28
    target 95
  ]
  edge [
    source 28
    target 270
  ]
  edge [
    source 28
    target 271
  ]
  edge [
    source 28
    target 120
  ]
  edge [
    source 28
    target 101
  ]
  edge [
    source 28
    target 272
  ]
  edge [
    source 28
    target 1747
  ]
  edge [
    source 28
    target 1748
  ]
  edge [
    source 28
    target 294
  ]
  edge [
    source 28
    target 1749
  ]
  edge [
    source 28
    target 1750
  ]
  edge [
    source 28
    target 1751
  ]
  edge [
    source 28
    target 1752
  ]
  edge [
    source 28
    target 1753
  ]
  edge [
    source 28
    target 1754
  ]
  edge [
    source 28
    target 1755
  ]
  edge [
    source 28
    target 1756
  ]
  edge [
    source 28
    target 1757
  ]
  edge [
    source 28
    target 313
  ]
  edge [
    source 28
    target 1758
  ]
  edge [
    source 28
    target 1759
  ]
  edge [
    source 28
    target 1760
  ]
  edge [
    source 28
    target 1761
  ]
  edge [
    source 28
    target 1762
  ]
  edge [
    source 28
    target 1763
  ]
  edge [
    source 28
    target 1764
  ]
  edge [
    source 28
    target 1765
  ]
  edge [
    source 28
    target 1766
  ]
  edge [
    source 28
    target 1767
  ]
  edge [
    source 28
    target 1768
  ]
  edge [
    source 28
    target 1589
  ]
  edge [
    source 28
    target 1590
  ]
  edge [
    source 28
    target 899
  ]
  edge [
    source 28
    target 1591
  ]
  edge [
    source 28
    target 906
  ]
  edge [
    source 28
    target 1592
  ]
  edge [
    source 28
    target 908
  ]
  edge [
    source 28
    target 1593
  ]
  edge [
    source 28
    target 793
  ]
  edge [
    source 28
    target 780
  ]
  edge [
    source 28
    target 898
  ]
  edge [
    source 28
    target 889
  ]
  edge [
    source 28
    target 1594
  ]
  edge [
    source 28
    target 1595
  ]
  edge [
    source 28
    target 1597
  ]
  edge [
    source 28
    target 1598
  ]
  edge [
    source 28
    target 1599
  ]
  edge [
    source 28
    target 1600
  ]
  edge [
    source 28
    target 1601
  ]
  edge [
    source 28
    target 1602
  ]
  edge [
    source 28
    target 909
  ]
  edge [
    source 28
    target 1603
  ]
  edge [
    source 28
    target 897
  ]
  edge [
    source 28
    target 901
  ]
  edge [
    source 28
    target 1576
  ]
  edge [
    source 28
    target 1604
  ]
  edge [
    source 28
    target 1605
  ]
  edge [
    source 28
    target 37
  ]
  edge [
    source 28
    target 1606
  ]
  edge [
    source 28
    target 900
  ]
  edge [
    source 28
    target 903
  ]
  edge [
    source 28
    target 904
  ]
  edge [
    source 28
    target 1607
  ]
  edge [
    source 28
    target 905
  ]
  edge [
    source 28
    target 1608
  ]
  edge [
    source 28
    target 1609
  ]
  edge [
    source 28
    target 1610
  ]
  edge [
    source 28
    target 1769
  ]
  edge [
    source 28
    target 1770
  ]
  edge [
    source 28
    target 1771
  ]
  edge [
    source 28
    target 1772
  ]
  edge [
    source 28
    target 1773
  ]
  edge [
    source 28
    target 1774
  ]
  edge [
    source 28
    target 68
  ]
  edge [
    source 28
    target 1775
  ]
  edge [
    source 28
    target 741
  ]
  edge [
    source 28
    target 1776
  ]
  edge [
    source 29
    target 1777
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1778
  ]
  edge [
    source 31
    target 1779
  ]
  edge [
    source 31
    target 294
  ]
  edge [
    source 31
    target 1780
  ]
  edge [
    source 31
    target 1781
  ]
  edge [
    source 31
    target 1782
  ]
  edge [
    source 31
    target 1783
  ]
  edge [
    source 31
    target 1784
  ]
  edge [
    source 31
    target 1785
  ]
  edge [
    source 31
    target 1069
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1786
  ]
  edge [
    source 32
    target 113
  ]
  edge [
    source 32
    target 465
  ]
  edge [
    source 32
    target 419
  ]
  edge [
    source 32
    target 466
  ]
  edge [
    source 32
    target 467
  ]
  edge [
    source 32
    target 468
  ]
  edge [
    source 32
    target 469
  ]
  edge [
    source 32
    target 470
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1787
  ]
  edge [
    source 33
    target 1788
  ]
  edge [
    source 33
    target 1789
  ]
  edge [
    source 33
    target 1790
  ]
  edge [
    source 33
    target 637
  ]
  edge [
    source 33
    target 638
  ]
  edge [
    source 33
    target 1791
  ]
  edge [
    source 33
    target 1792
  ]
  edge [
    source 33
    target 1793
  ]
  edge [
    source 33
    target 1794
  ]
  edge [
    source 33
    target 1795
  ]
  edge [
    source 33
    target 1796
  ]
  edge [
    source 33
    target 1797
  ]
  edge [
    source 33
    target 1798
  ]
  edge [
    source 33
    target 1799
  ]
  edge [
    source 33
    target 1800
  ]
  edge [
    source 33
    target 1801
  ]
  edge [
    source 33
    target 182
  ]
  edge [
    source 33
    target 1802
  ]
  edge [
    source 33
    target 1803
  ]
  edge [
    source 33
    target 1804
  ]
  edge [
    source 33
    target 1805
  ]
  edge [
    source 33
    target 1806
  ]
  edge [
    source 33
    target 1807
  ]
  edge [
    source 33
    target 1808
  ]
  edge [
    source 33
    target 1743
  ]
  edge [
    source 33
    target 1809
  ]
  edge [
    source 33
    target 1810
  ]
  edge [
    source 33
    target 1811
  ]
  edge [
    source 33
    target 1812
  ]
  edge [
    source 33
    target 1813
  ]
  edge [
    source 33
    target 1814
  ]
  edge [
    source 33
    target 1691
  ]
  edge [
    source 33
    target 1815
  ]
  edge [
    source 33
    target 1816
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 1817
  ]
  edge [
    source 33
    target 1818
  ]
  edge [
    source 33
    target 1819
  ]
  edge [
    source 33
    target 1820
  ]
  edge [
    source 33
    target 1821
  ]
  edge [
    source 33
    target 1822
  ]
  edge [
    source 33
    target 1823
  ]
  edge [
    source 33
    target 1824
  ]
  edge [
    source 33
    target 1825
  ]
  edge [
    source 33
    target 1826
  ]
  edge [
    source 33
    target 1827
  ]
  edge [
    source 33
    target 1828
  ]
  edge [
    source 33
    target 219
  ]
  edge [
    source 33
    target 1829
  ]
  edge [
    source 33
    target 1830
  ]
  edge [
    source 33
    target 120
  ]
  edge [
    source 33
    target 1831
  ]
  edge [
    source 33
    target 1832
  ]
  edge [
    source 33
    target 793
  ]
  edge [
    source 33
    target 794
  ]
  edge [
    source 33
    target 123
  ]
  edge [
    source 33
    target 334
  ]
  edge [
    source 33
    target 795
  ]
  edge [
    source 33
    target 780
  ]
  edge [
    source 33
    target 1833
  ]
  edge [
    source 33
    target 1834
  ]
  edge [
    source 33
    target 1835
  ]
  edge [
    source 33
    target 1836
  ]
  edge [
    source 33
    target 1837
  ]
  edge [
    source 33
    target 1838
  ]
  edge [
    source 33
    target 1839
  ]
  edge [
    source 33
    target 1840
  ]
  edge [
    source 33
    target 1841
  ]
  edge [
    source 33
    target 1842
  ]
  edge [
    source 33
    target 1843
  ]
  edge [
    source 33
    target 1844
  ]
  edge [
    source 33
    target 1845
  ]
  edge [
    source 33
    target 575
  ]
  edge [
    source 33
    target 1846
  ]
  edge [
    source 33
    target 1847
  ]
  edge [
    source 33
    target 1848
  ]
  edge [
    source 33
    target 1849
  ]
  edge [
    source 33
    target 975
  ]
  edge [
    source 33
    target 1850
  ]
  edge [
    source 33
    target 1851
  ]
  edge [
    source 33
    target 1852
  ]
  edge [
    source 33
    target 1853
  ]
  edge [
    source 33
    target 1854
  ]
  edge [
    source 33
    target 1855
  ]
  edge [
    source 33
    target 1856
  ]
  edge [
    source 33
    target 1857
  ]
  edge [
    source 33
    target 1858
  ]
  edge [
    source 33
    target 1859
  ]
  edge [
    source 33
    target 1860
  ]
  edge [
    source 33
    target 1861
  ]
  edge [
    source 33
    target 1862
  ]
  edge [
    source 33
    target 1863
  ]
  edge [
    source 33
    target 1864
  ]
  edge [
    source 33
    target 644
  ]
  edge [
    source 33
    target 1865
  ]
  edge [
    source 33
    target 1866
  ]
  edge [
    source 33
    target 1867
  ]
  edge [
    source 33
    target 1155
  ]
  edge [
    source 33
    target 1868
  ]
  edge [
    source 33
    target 1869
  ]
  edge [
    source 33
    target 677
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 33
    target 1870
  ]
  edge [
    source 33
    target 1871
  ]
  edge [
    source 33
    target 419
  ]
  edge [
    source 33
    target 1872
  ]
  edge [
    source 33
    target 1873
  ]
  edge [
    source 33
    target 1874
  ]
  edge [
    source 33
    target 1875
  ]
  edge [
    source 33
    target 1876
  ]
  edge [
    source 33
    target 1877
  ]
  edge [
    source 33
    target 554
  ]
  edge [
    source 33
    target 1878
  ]
  edge [
    source 33
    target 1879
  ]
  edge [
    source 33
    target 1880
  ]
  edge [
    source 33
    target 1881
  ]
  edge [
    source 33
    target 1882
  ]
  edge [
    source 33
    target 1883
  ]
  edge [
    source 33
    target 1884
  ]
  edge [
    source 33
    target 96
  ]
  edge [
    source 33
    target 1885
  ]
  edge [
    source 33
    target 1886
  ]
  edge [
    source 33
    target 1887
  ]
  edge [
    source 33
    target 1888
  ]
  edge [
    source 33
    target 1889
  ]
  edge [
    source 33
    target 1890
  ]
  edge [
    source 33
    target 1891
  ]
  edge [
    source 33
    target 1892
  ]
  edge [
    source 33
    target 1893
  ]
  edge [
    source 33
    target 1894
  ]
  edge [
    source 33
    target 1895
  ]
  edge [
    source 33
    target 1896
  ]
  edge [
    source 33
    target 1742
  ]
  edge [
    source 33
    target 483
  ]
  edge [
    source 33
    target 1897
  ]
  edge [
    source 33
    target 1898
  ]
  edge [
    source 33
    target 1899
  ]
  edge [
    source 33
    target 1900
  ]
  edge [
    source 33
    target 1901
  ]
  edge [
    source 33
    target 1902
  ]
  edge [
    source 33
    target 1903
  ]
  edge [
    source 33
    target 1904
  ]
  edge [
    source 33
    target 1905
  ]
  edge [
    source 33
    target 1906
  ]
  edge [
    source 33
    target 466
  ]
  edge [
    source 33
    target 1907
  ]
  edge [
    source 33
    target 1908
  ]
  edge [
    source 33
    target 1909
  ]
  edge [
    source 33
    target 1910
  ]
  edge [
    source 33
    target 1911
  ]
  edge [
    source 33
    target 1912
  ]
  edge [
    source 33
    target 1913
  ]
  edge [
    source 33
    target 1914
  ]
  edge [
    source 33
    target 1915
  ]
  edge [
    source 33
    target 1916
  ]
  edge [
    source 33
    target 1917
  ]
  edge [
    source 33
    target 1918
  ]
  edge [
    source 33
    target 1919
  ]
  edge [
    source 33
    target 1920
  ]
  edge [
    source 33
    target 1921
  ]
  edge [
    source 33
    target 1922
  ]
  edge [
    source 33
    target 1923
  ]
  edge [
    source 33
    target 1924
  ]
  edge [
    source 33
    target 1925
  ]
  edge [
    source 33
    target 1926
  ]
  edge [
    source 33
    target 1325
  ]
  edge [
    source 33
    target 1927
  ]
  edge [
    source 33
    target 1928
  ]
  edge [
    source 33
    target 1929
  ]
  edge [
    source 33
    target 1930
  ]
  edge [
    source 33
    target 1931
  ]
  edge [
    source 33
    target 1932
  ]
  edge [
    source 33
    target 1205
  ]
  edge [
    source 33
    target 1933
  ]
  edge [
    source 33
    target 1934
  ]
  edge [
    source 33
    target 1935
  ]
  edge [
    source 33
    target 1936
  ]
  edge [
    source 33
    target 1937
  ]
  edge [
    source 33
    target 922
  ]
  edge [
    source 33
    target 1204
  ]
  edge [
    source 33
    target 1938
  ]
  edge [
    source 33
    target 1939
  ]
  edge [
    source 33
    target 1940
  ]
  edge [
    source 33
    target 1941
  ]
  edge [
    source 33
    target 1942
  ]
  edge [
    source 33
    target 1943
  ]
  edge [
    source 33
    target 1944
  ]
  edge [
    source 33
    target 177
  ]
  edge [
    source 33
    target 1945
  ]
  edge [
    source 33
    target 1946
  ]
  edge [
    source 33
    target 135
  ]
  edge [
    source 33
    target 1696
  ]
  edge [
    source 33
    target 1947
  ]
  edge [
    source 33
    target 1948
  ]
  edge [
    source 33
    target 936
  ]
  edge [
    source 33
    target 1949
  ]
  edge [
    source 33
    target 1950
  ]
  edge [
    source 33
    target 1951
  ]
  edge [
    source 33
    target 1952
  ]
  edge [
    source 33
    target 1953
  ]
  edge [
    source 33
    target 1954
  ]
  edge [
    source 33
    target 1955
  ]
  edge [
    source 33
    target 1956
  ]
  edge [
    source 33
    target 1957
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 33
    target 1958
  ]
  edge [
    source 33
    target 756
  ]
  edge [
    source 33
    target 1959
  ]
  edge [
    source 33
    target 1960
  ]
  edge [
    source 33
    target 1961
  ]
  edge [
    source 33
    target 585
  ]
  edge [
    source 33
    target 1962
  ]
  edge [
    source 33
    target 1963
  ]
  edge [
    source 33
    target 459
  ]
  edge [
    source 33
    target 1964
  ]
  edge [
    source 33
    target 1306
  ]
  edge [
    source 33
    target 1965
  ]
  edge [
    source 33
    target 1966
  ]
  edge [
    source 33
    target 1967
  ]
  edge [
    source 33
    target 1304
  ]
  edge [
    source 33
    target 1968
  ]
  edge [
    source 33
    target 1969
  ]
  edge [
    source 33
    target 1970
  ]
  edge [
    source 33
    target 642
  ]
  edge [
    source 33
    target 1971
  ]
  edge [
    source 33
    target 1972
  ]
  edge [
    source 33
    target 1220
  ]
  edge [
    source 33
    target 1644
  ]
  edge [
    source 33
    target 1973
  ]
  edge [
    source 33
    target 1974
  ]
  edge [
    source 33
    target 1975
  ]
  edge [
    source 33
    target 167
  ]
  edge [
    source 33
    target 1976
  ]
  edge [
    source 33
    target 1977
  ]
  edge [
    source 33
    target 1120
  ]
  edge [
    source 33
    target 1134
  ]
  edge [
    source 33
    target 1222
  ]
  edge [
    source 33
    target 1978
  ]
  edge [
    source 33
    target 1979
  ]
  edge [
    source 33
    target 1980
  ]
  edge [
    source 33
    target 1981
  ]
  edge [
    source 33
    target 981
  ]
  edge [
    source 33
    target 1982
  ]
  edge [
    source 33
    target 1983
  ]
  edge [
    source 33
    target 1984
  ]
  edge [
    source 33
    target 1985
  ]
  edge [
    source 33
    target 1986
  ]
  edge [
    source 33
    target 1987
  ]
  edge [
    source 33
    target 1988
  ]
  edge [
    source 33
    target 1989
  ]
  edge [
    source 33
    target 1528
  ]
  edge [
    source 33
    target 1990
  ]
  edge [
    source 33
    target 1991
  ]
  edge [
    source 33
    target 1992
  ]
  edge [
    source 33
    target 1993
  ]
  edge [
    source 33
    target 1994
  ]
  edge [
    source 33
    target 999
  ]
  edge [
    source 33
    target 1995
  ]
  edge [
    source 33
    target 1996
  ]
  edge [
    source 33
    target 1997
  ]
  edge [
    source 33
    target 294
  ]
  edge [
    source 33
    target 1998
  ]
  edge [
    source 33
    target 1999
  ]
  edge [
    source 33
    target 703
  ]
  edge [
    source 33
    target 2000
  ]
  edge [
    source 33
    target 2001
  ]
  edge [
    source 33
    target 2002
  ]
  edge [
    source 33
    target 2003
  ]
  edge [
    source 33
    target 986
  ]
  edge [
    source 33
    target 2004
  ]
  edge [
    source 33
    target 1529
  ]
  edge [
    source 33
    target 1760
  ]
  edge [
    source 33
    target 2005
  ]
  edge [
    source 33
    target 2006
  ]
  edge [
    source 33
    target 2007
  ]
  edge [
    source 33
    target 2008
  ]
  edge [
    source 33
    target 2009
  ]
  edge [
    source 33
    target 2010
  ]
  edge [
    source 33
    target 2011
  ]
  edge [
    source 33
    target 2012
  ]
  edge [
    source 33
    target 325
  ]
  edge [
    source 33
    target 2013
  ]
  edge [
    source 33
    target 2014
  ]
  edge [
    source 33
    target 2015
  ]
  edge [
    source 33
    target 2016
  ]
  edge [
    source 33
    target 308
  ]
  edge [
    source 33
    target 2017
  ]
  edge [
    source 33
    target 2018
  ]
  edge [
    source 33
    target 2019
  ]
  edge [
    source 33
    target 1375
  ]
  edge [
    source 33
    target 2020
  ]
  edge [
    source 33
    target 2021
  ]
  edge [
    source 33
    target 2022
  ]
  edge [
    source 33
    target 2023
  ]
  edge [
    source 33
    target 2024
  ]
  edge [
    source 33
    target 1381
  ]
  edge [
    source 33
    target 2025
  ]
  edge [
    source 33
    target 1170
  ]
  edge [
    source 33
    target 2026
  ]
  edge [
    source 33
    target 2027
  ]
  edge [
    source 33
    target 2028
  ]
  edge [
    source 33
    target 2029
  ]
  edge [
    source 33
    target 2030
  ]
  edge [
    source 33
    target 2031
  ]
  edge [
    source 33
    target 2032
  ]
  edge [
    source 33
    target 2033
  ]
  edge [
    source 33
    target 2034
  ]
  edge [
    source 33
    target 2035
  ]
  edge [
    source 33
    target 2036
  ]
  edge [
    source 33
    target 2037
  ]
  edge [
    source 33
    target 2038
  ]
  edge [
    source 33
    target 2039
  ]
  edge [
    source 33
    target 2040
  ]
  edge [
    source 33
    target 2041
  ]
  edge [
    source 33
    target 2042
  ]
  edge [
    source 33
    target 2043
  ]
  edge [
    source 33
    target 2044
  ]
  edge [
    source 33
    target 2045
  ]
  edge [
    source 33
    target 2046
  ]
  edge [
    source 33
    target 2047
  ]
  edge [
    source 33
    target 2048
  ]
  edge [
    source 33
    target 2049
  ]
  edge [
    source 33
    target 2050
  ]
  edge [
    source 33
    target 1277
  ]
  edge [
    source 33
    target 2051
  ]
  edge [
    source 33
    target 2052
  ]
  edge [
    source 33
    target 2053
  ]
  edge [
    source 33
    target 2054
  ]
  edge [
    source 33
    target 2055
  ]
  edge [
    source 33
    target 2056
  ]
  edge [
    source 33
    target 2057
  ]
  edge [
    source 33
    target 2058
  ]
  edge [
    source 33
    target 2059
  ]
  edge [
    source 33
    target 2060
  ]
  edge [
    source 33
    target 2061
  ]
  edge [
    source 33
    target 2062
  ]
  edge [
    source 33
    target 2063
  ]
  edge [
    source 33
    target 2064
  ]
  edge [
    source 33
    target 2065
  ]
  edge [
    source 33
    target 457
  ]
  edge [
    source 33
    target 2066
  ]
  edge [
    source 33
    target 2067
  ]
  edge [
    source 33
    target 2068
  ]
  edge [
    source 33
    target 2069
  ]
  edge [
    source 33
    target 2070
  ]
  edge [
    source 33
    target 2071
  ]
  edge [
    source 33
    target 2072
  ]
  edge [
    source 33
    target 2073
  ]
  edge [
    source 33
    target 2074
  ]
  edge [
    source 33
    target 2075
  ]
  edge [
    source 33
    target 2076
  ]
  edge [
    source 33
    target 2077
  ]
  edge [
    source 33
    target 2078
  ]
  edge [
    source 33
    target 2079
  ]
  edge [
    source 33
    target 2080
  ]
  edge [
    source 33
    target 2081
  ]
  edge [
    source 33
    target 2082
  ]
  edge [
    source 33
    target 2083
  ]
  edge [
    source 33
    target 2084
  ]
  edge [
    source 33
    target 2085
  ]
  edge [
    source 34
    target 2086
  ]
  edge [
    source 34
    target 266
  ]
  edge [
    source 34
    target 174
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 2087
  ]
  edge [
    source 35
    target 2088
  ]
  edge [
    source 35
    target 1450
  ]
  edge [
    source 35
    target 2089
  ]
  edge [
    source 35
    target 2090
  ]
  edge [
    source 35
    target 2091
  ]
  edge [
    source 35
    target 2092
  ]
  edge [
    source 35
    target 2093
  ]
  edge [
    source 35
    target 1018
  ]
  edge [
    source 35
    target 2094
  ]
  edge [
    source 35
    target 2095
  ]
  edge [
    source 35
    target 2096
  ]
  edge [
    source 35
    target 1451
  ]
  edge [
    source 35
    target 379
  ]
  edge [
    source 35
    target 2097
  ]
  edge [
    source 35
    target 2098
  ]
  edge [
    source 35
    target 2099
  ]
  edge [
    source 35
    target 1446
  ]
  edge [
    source 35
    target 2100
  ]
  edge [
    source 35
    target 2101
  ]
  edge [
    source 35
    target 2102
  ]
  edge [
    source 35
    target 2103
  ]
  edge [
    source 35
    target 1459
  ]
  edge [
    source 35
    target 2104
  ]
  edge [
    source 35
    target 2105
  ]
  edge [
    source 35
    target 1411
  ]
  edge [
    source 35
    target 728
  ]
  edge [
    source 35
    target 2106
  ]
  edge [
    source 35
    target 2107
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 2108
  ]
  edge [
    source 36
    target 2109
  ]
  edge [
    source 36
    target 2110
  ]
  edge [
    source 36
    target 2111
  ]
  edge [
    source 36
    target 2112
  ]
  edge [
    source 36
    target 2113
  ]
  edge [
    source 36
    target 2114
  ]
  edge [
    source 36
    target 2115
  ]
  edge [
    source 36
    target 2116
  ]
  edge [
    source 36
    target 2117
  ]
  edge [
    source 36
    target 2118
  ]
  edge [
    source 36
    target 2119
  ]
  edge [
    source 36
    target 2120
  ]
  edge [
    source 36
    target 2121
  ]
  edge [
    source 36
    target 2122
  ]
  edge [
    source 36
    target 744
  ]
  edge [
    source 36
    target 2123
  ]
  edge [
    source 36
    target 2124
  ]
  edge [
    source 36
    target 2125
  ]
  edge [
    source 36
    target 2126
  ]
  edge [
    source 36
    target 321
  ]
  edge [
    source 36
    target 2127
  ]
  edge [
    source 36
    target 329
  ]
  edge [
    source 36
    target 2128
  ]
  edge [
    source 36
    target 2129
  ]
  edge [
    source 36
    target 2130
  ]
  edge [
    source 36
    target 2131
  ]
  edge [
    source 36
    target 322
  ]
  edge [
    source 36
    target 2132
  ]
  edge [
    source 36
    target 2133
  ]
  edge [
    source 36
    target 2134
  ]
  edge [
    source 36
    target 316
  ]
  edge [
    source 36
    target 2135
  ]
  edge [
    source 36
    target 2136
  ]
  edge [
    source 36
    target 2137
  ]
  edge [
    source 36
    target 2138
  ]
  edge [
    source 36
    target 2139
  ]
  edge [
    source 36
    target 2140
  ]
  edge [
    source 36
    target 2141
  ]
  edge [
    source 36
    target 2142
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 793
  ]
  edge [
    source 37
    target 794
  ]
  edge [
    source 37
    target 123
  ]
  edge [
    source 37
    target 334
  ]
  edge [
    source 37
    target 795
  ]
  edge [
    source 37
    target 1948
  ]
  edge [
    source 37
    target 2143
  ]
  edge [
    source 37
    target 2144
  ]
  edge [
    source 37
    target 1550
  ]
  edge [
    source 37
    target 2145
  ]
  edge [
    source 37
    target 2146
  ]
  edge [
    source 37
    target 766
  ]
  edge [
    source 37
    target 2147
  ]
  edge [
    source 37
    target 2148
  ]
  edge [
    source 37
    target 47
  ]
  edge [
    source 37
    target 2149
  ]
  edge [
    source 37
    target 2150
  ]
  edge [
    source 37
    target 2151
  ]
  edge [
    source 37
    target 908
  ]
  edge [
    source 37
    target 2152
  ]
  edge [
    source 37
    target 2153
  ]
  edge [
    source 37
    target 2154
  ]
  edge [
    source 37
    target 637
  ]
  edge [
    source 37
    target 461
  ]
  edge [
    source 37
    target 2155
  ]
  edge [
    source 37
    target 2156
  ]
  edge [
    source 37
    target 1825
  ]
  edge [
    source 37
    target 2157
  ]
  edge [
    source 37
    target 2158
  ]
  edge [
    source 37
    target 2159
  ]
  edge [
    source 37
    target 135
  ]
  edge [
    source 37
    target 459
  ]
  edge [
    source 37
    target 136
  ]
  edge [
    source 37
    target 2142
  ]
  edge [
    source 37
    target 2160
  ]
  edge [
    source 37
    target 546
  ]
  edge [
    source 37
    target 1083
  ]
  edge [
    source 37
    target 933
  ]
  edge [
    source 37
    target 1084
  ]
  edge [
    source 37
    target 1085
  ]
  edge [
    source 37
    target 1086
  ]
  edge [
    source 37
    target 1087
  ]
  edge [
    source 37
    target 1088
  ]
  edge [
    source 37
    target 1089
  ]
  edge [
    source 37
    target 1090
  ]
  edge [
    source 37
    target 1091
  ]
  edge [
    source 37
    target 1092
  ]
  edge [
    source 37
    target 1093
  ]
  edge [
    source 37
    target 945
  ]
  edge [
    source 37
    target 1094
  ]
  edge [
    source 37
    target 652
  ]
  edge [
    source 37
    target 699
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 2161
  ]
  edge [
    source 38
    target 2162
  ]
  edge [
    source 38
    target 2163
  ]
  edge [
    source 38
    target 1707
  ]
  edge [
    source 38
    target 67
  ]
  edge [
    source 38
    target 68
  ]
  edge [
    source 38
    target 70
  ]
  edge [
    source 38
    target 2164
  ]
  edge [
    source 38
    target 2165
  ]
  edge [
    source 38
    target 2166
  ]
  edge [
    source 38
    target 145
  ]
  edge [
    source 38
    target 1772
  ]
  edge [
    source 38
    target 2167
  ]
  edge [
    source 38
    target 2168
  ]
  edge [
    source 38
    target 1774
  ]
  edge [
    source 38
    target 2169
  ]
  edge [
    source 38
    target 260
  ]
  edge [
    source 38
    target 109
  ]
  edge [
    source 38
    target 631
  ]
  edge [
    source 38
    target 135
  ]
  edge [
    source 38
    target 120
  ]
  edge [
    source 38
    target 709
  ]
  edge [
    source 38
    target 2170
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 38
    target 391
  ]
  edge [
    source 38
    target 2171
  ]
  edge [
    source 38
    target 1128
  ]
  edge [
    source 38
    target 389
  ]
  edge [
    source 38
    target 2172
  ]
  edge [
    source 38
    target 1228
  ]
  edge [
    source 38
    target 2173
  ]
  edge [
    source 38
    target 2174
  ]
  edge [
    source 38
    target 2175
  ]
  edge [
    source 38
    target 331
  ]
  edge [
    source 38
    target 2091
  ]
  edge [
    source 38
    target 1944
  ]
  edge [
    source 38
    target 2176
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 295
  ]
  edge [
    source 39
    target 1212
  ]
  edge [
    source 39
    target 2177
  ]
  edge [
    source 39
    target 2178
  ]
  edge [
    source 39
    target 2179
  ]
  edge [
    source 39
    target 2180
  ]
  edge [
    source 39
    target 2181
  ]
  edge [
    source 39
    target 2182
  ]
  edge [
    source 39
    target 608
  ]
  edge [
    source 39
    target 2183
  ]
  edge [
    source 39
    target 2184
  ]
  edge [
    source 39
    target 2185
  ]
  edge [
    source 39
    target 2186
  ]
  edge [
    source 39
    target 2187
  ]
  edge [
    source 39
    target 291
  ]
  edge [
    source 39
    target 2188
  ]
  edge [
    source 39
    target 2189
  ]
  edge [
    source 39
    target 2190
  ]
  edge [
    source 39
    target 2191
  ]
  edge [
    source 39
    target 2018
  ]
  edge [
    source 39
    target 2192
  ]
  edge [
    source 40
    target 2193
  ]
  edge [
    source 40
    target 2194
  ]
  edge [
    source 40
    target 2195
  ]
  edge [
    source 40
    target 2169
  ]
  edge [
    source 40
    target 191
  ]
  edge [
    source 40
    target 2196
  ]
  edge [
    source 40
    target 2197
  ]
  edge [
    source 40
    target 2198
  ]
  edge [
    source 40
    target 2199
  ]
  edge [
    source 40
    target 2200
  ]
  edge [
    source 40
    target 2201
  ]
  edge [
    source 40
    target 2202
  ]
  edge [
    source 40
    target 1574
  ]
  edge [
    source 40
    target 2203
  ]
  edge [
    source 40
    target 2204
  ]
  edge [
    source 40
    target 123
  ]
  edge [
    source 40
    target 2205
  ]
  edge [
    source 40
    target 1034
  ]
  edge [
    source 40
    target 2206
  ]
  edge [
    source 40
    target 2207
  ]
  edge [
    source 40
    target 135
  ]
  edge [
    source 40
    target 2208
  ]
  edge [
    source 40
    target 2209
  ]
  edge [
    source 40
    target 2210
  ]
  edge [
    source 40
    target 2211
  ]
  edge [
    source 40
    target 2212
  ]
  edge [
    source 40
    target 2213
  ]
  edge [
    source 40
    target 2214
  ]
  edge [
    source 40
    target 2215
  ]
  edge [
    source 40
    target 2216
  ]
  edge [
    source 40
    target 300
  ]
  edge [
    source 40
    target 2217
  ]
  edge [
    source 40
    target 2218
  ]
  edge [
    source 40
    target 2219
  ]
  edge [
    source 40
    target 2220
  ]
  edge [
    source 40
    target 2221
  ]
  edge [
    source 40
    target 128
  ]
  edge [
    source 40
    target 2222
  ]
  edge [
    source 40
    target 2223
  ]
  edge [
    source 40
    target 1774
  ]
  edge [
    source 40
    target 2224
  ]
  edge [
    source 40
    target 122
  ]
  edge [
    source 40
    target 905
  ]
  edge [
    source 40
    target 271
  ]
  edge [
    source 40
    target 2225
  ]
  edge [
    source 40
    target 2226
  ]
  edge [
    source 40
    target 2227
  ]
  edge [
    source 40
    target 2228
  ]
  edge [
    source 40
    target 2229
  ]
  edge [
    source 40
    target 2230
  ]
  edge [
    source 40
    target 637
  ]
  edge [
    source 40
    target 2231
  ]
  edge [
    source 40
    target 2232
  ]
  edge [
    source 40
    target 2233
  ]
  edge [
    source 40
    target 2234
  ]
  edge [
    source 40
    target 2235
  ]
  edge [
    source 40
    target 2236
  ]
  edge [
    source 40
    target 2237
  ]
  edge [
    source 40
    target 1587
  ]
  edge [
    source 40
    target 345
  ]
  edge [
    source 40
    target 2238
  ]
  edge [
    source 40
    target 2239
  ]
  edge [
    source 40
    target 2240
  ]
  edge [
    source 40
    target 2241
  ]
  edge [
    source 40
    target 1464
  ]
  edge [
    source 40
    target 1100
  ]
  edge [
    source 40
    target 2242
  ]
  edge [
    source 40
    target 2243
  ]
  edge [
    source 40
    target 1573
  ]
  edge [
    source 40
    target 1572
  ]
  edge [
    source 40
    target 1570
  ]
  edge [
    source 40
    target 446
  ]
  edge [
    source 40
    target 2244
  ]
  edge [
    source 40
    target 104
  ]
  edge [
    source 40
    target 2245
  ]
  edge [
    source 40
    target 2246
  ]
  edge [
    source 40
    target 1578
  ]
  edge [
    source 40
    target 2247
  ]
  edge [
    source 40
    target 263
  ]
  edge [
    source 40
    target 2248
  ]
  edge [
    source 40
    target 2249
  ]
  edge [
    source 40
    target 933
  ]
  edge [
    source 40
    target 2250
  ]
  edge [
    source 40
    target 990
  ]
  edge [
    source 40
    target 2251
  ]
  edge [
    source 40
    target 2252
  ]
  edge [
    source 40
    target 2253
  ]
  edge [
    source 40
    target 1658
  ]
  edge [
    source 40
    target 1219
  ]
  edge [
    source 40
    target 560
  ]
  edge [
    source 40
    target 289
  ]
  edge [
    source 40
    target 2254
  ]
  edge [
    source 40
    target 2255
  ]
  edge [
    source 40
    target 1223
  ]
  edge [
    source 40
    target 2256
  ]
  edge [
    source 40
    target 2257
  ]
  edge [
    source 40
    target 2258
  ]
  edge [
    source 40
    target 2259
  ]
  edge [
    source 40
    target 2260
  ]
  edge [
    source 40
    target 2261
  ]
  edge [
    source 40
    target 2262
  ]
  edge [
    source 40
    target 2263
  ]
  edge [
    source 40
    target 2264
  ]
  edge [
    source 40
    target 212
  ]
  edge [
    source 40
    target 644
  ]
  edge [
    source 40
    target 2265
  ]
  edge [
    source 40
    target 2266
  ]
  edge [
    source 40
    target 2267
  ]
  edge [
    source 40
    target 2268
  ]
  edge [
    source 40
    target 65
  ]
  edge [
    source 40
    target 2269
  ]
  edge [
    source 40
    target 2270
  ]
  edge [
    source 40
    target 2271
  ]
  edge [
    source 40
    target 2272
  ]
  edge [
    source 40
    target 2273
  ]
  edge [
    source 40
    target 2274
  ]
  edge [
    source 40
    target 2275
  ]
  edge [
    source 40
    target 2276
  ]
  edge [
    source 40
    target 2277
  ]
  edge [
    source 40
    target 2278
  ]
  edge [
    source 40
    target 2279
  ]
  edge [
    source 40
    target 780
  ]
  edge [
    source 40
    target 2280
  ]
  edge [
    source 40
    target 2281
  ]
  edge [
    source 40
    target 958
  ]
]
