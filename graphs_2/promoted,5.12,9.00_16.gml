graph [
  node [
    id 0
    label "polak"
    origin "text"
  ]
  node [
    id 1
    label "dop&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 2
    label "g&#243;rnictwo"
    origin "text"
  ]
  node [
    id 3
    label "energetyka"
    origin "text"
  ]
  node [
    id 4
    label "oprze&#263;"
    origin "text"
  ]
  node [
    id 5
    label "w&#281;giel"
    origin "text"
  ]
  node [
    id 6
    label "lato"
    origin "text"
  ]
  node [
    id 7
    label "blisko"
    origin "text"
  ]
  node [
    id 8
    label "dwa"
    origin "text"
  ]
  node [
    id 9
    label "bilion"
    origin "text"
  ]
  node [
    id 10
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 11
    label "polski"
  ]
  node [
    id 12
    label "przedmiot"
  ]
  node [
    id 13
    label "Polish"
  ]
  node [
    id 14
    label "goniony"
  ]
  node [
    id 15
    label "oberek"
  ]
  node [
    id 16
    label "ryba_po_grecku"
  ]
  node [
    id 17
    label "sztajer"
  ]
  node [
    id 18
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 19
    label "krakowiak"
  ]
  node [
    id 20
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 21
    label "pierogi_ruskie"
  ]
  node [
    id 22
    label "lacki"
  ]
  node [
    id 23
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 24
    label "chodzony"
  ]
  node [
    id 25
    label "po_polsku"
  ]
  node [
    id 26
    label "mazur"
  ]
  node [
    id 27
    label "polsko"
  ]
  node [
    id 28
    label "skoczny"
  ]
  node [
    id 29
    label "drabant"
  ]
  node [
    id 30
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 31
    label "j&#281;zyk"
  ]
  node [
    id 32
    label "zap&#322;aci&#263;"
  ]
  node [
    id 33
    label "wy&#322;oi&#263;"
  ]
  node [
    id 34
    label "picture"
  ]
  node [
    id 35
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 36
    label "zabuli&#263;"
  ]
  node [
    id 37
    label "wyda&#263;"
  ]
  node [
    id 38
    label "pay"
  ]
  node [
    id 39
    label "wydobywa&#263;"
  ]
  node [
    id 40
    label "odstrzeliwa&#263;"
  ]
  node [
    id 41
    label "rozpierak"
  ]
  node [
    id 42
    label "krzeska"
  ]
  node [
    id 43
    label "wydoby&#263;"
  ]
  node [
    id 44
    label "przemys&#322;_ci&#281;&#380;ki"
  ]
  node [
    id 45
    label "obrywak"
  ]
  node [
    id 46
    label "wydobycie"
  ]
  node [
    id 47
    label "wydobywanie"
  ]
  node [
    id 48
    label "&#322;adownik"
  ]
  node [
    id 49
    label "zgarniacz"
  ]
  node [
    id 50
    label "nauka"
  ]
  node [
    id 51
    label "wcinka"
  ]
  node [
    id 52
    label "solnictwo"
  ]
  node [
    id 53
    label "odstrzeliwanie"
  ]
  node [
    id 54
    label "aerologia_g&#243;rnicza"
  ]
  node [
    id 55
    label "wiertnictwo"
  ]
  node [
    id 56
    label "przesyp"
  ]
  node [
    id 57
    label "wiedza"
  ]
  node [
    id 58
    label "miasteczko_rowerowe"
  ]
  node [
    id 59
    label "porada"
  ]
  node [
    id 60
    label "fotowoltaika"
  ]
  node [
    id 61
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 62
    label "przem&#243;wienie"
  ]
  node [
    id 63
    label "nauki_o_poznaniu"
  ]
  node [
    id 64
    label "nomotetyczny"
  ]
  node [
    id 65
    label "systematyka"
  ]
  node [
    id 66
    label "proces"
  ]
  node [
    id 67
    label "typologia"
  ]
  node [
    id 68
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 69
    label "kultura_duchowa"
  ]
  node [
    id 70
    label "&#322;awa_szkolna"
  ]
  node [
    id 71
    label "nauki_penalne"
  ]
  node [
    id 72
    label "dziedzina"
  ]
  node [
    id 73
    label "imagineskopia"
  ]
  node [
    id 74
    label "teoria_naukowa"
  ]
  node [
    id 75
    label "inwentyka"
  ]
  node [
    id 76
    label "metodologia"
  ]
  node [
    id 77
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 78
    label "nauki_o_Ziemi"
  ]
  node [
    id 79
    label "od&#322;upywanie"
  ]
  node [
    id 80
    label "urywanie"
  ]
  node [
    id 81
    label "zabijanie"
  ]
  node [
    id 82
    label "uwydatnia&#263;"
  ]
  node [
    id 83
    label "eksploatowa&#263;"
  ]
  node [
    id 84
    label "uzyskiwa&#263;"
  ]
  node [
    id 85
    label "wydostawa&#263;"
  ]
  node [
    id 86
    label "wyjmowa&#263;"
  ]
  node [
    id 87
    label "train"
  ]
  node [
    id 88
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 89
    label "wydawa&#263;"
  ]
  node [
    id 90
    label "dobywa&#263;"
  ]
  node [
    id 91
    label "ocala&#263;"
  ]
  node [
    id 92
    label "excavate"
  ]
  node [
    id 93
    label "raise"
  ]
  node [
    id 94
    label "draw"
  ]
  node [
    id 95
    label "doby&#263;"
  ]
  node [
    id 96
    label "wyeksploatowa&#263;"
  ]
  node [
    id 97
    label "extract"
  ]
  node [
    id 98
    label "obtain"
  ]
  node [
    id 99
    label "wyj&#261;&#263;"
  ]
  node [
    id 100
    label "ocali&#263;"
  ]
  node [
    id 101
    label "uzyska&#263;"
  ]
  node [
    id 102
    label "wydosta&#263;"
  ]
  node [
    id 103
    label "uwydatni&#263;"
  ]
  node [
    id 104
    label "distill"
  ]
  node [
    id 105
    label "dobywanie"
  ]
  node [
    id 106
    label "powodowanie"
  ]
  node [
    id 107
    label "u&#380;ytkowanie"
  ]
  node [
    id 108
    label "eksploatowanie"
  ]
  node [
    id 109
    label "wydostawanie"
  ]
  node [
    id 110
    label "wyjmowanie"
  ]
  node [
    id 111
    label "ratowanie"
  ]
  node [
    id 112
    label "robienie"
  ]
  node [
    id 113
    label "uzyskiwanie"
  ]
  node [
    id 114
    label "evocation"
  ]
  node [
    id 115
    label "czynno&#347;&#263;"
  ]
  node [
    id 116
    label "uwydatnianie"
  ]
  node [
    id 117
    label "extraction"
  ]
  node [
    id 118
    label "wyeksploatowanie"
  ]
  node [
    id 119
    label "uwydatnienie"
  ]
  node [
    id 120
    label "uzyskanie"
  ]
  node [
    id 121
    label "fusillade"
  ]
  node [
    id 122
    label "spowodowanie"
  ]
  node [
    id 123
    label "wyratowanie"
  ]
  node [
    id 124
    label "wyj&#281;cie"
  ]
  node [
    id 125
    label "powyci&#261;ganie"
  ]
  node [
    id 126
    label "wydostanie"
  ]
  node [
    id 127
    label "dobycie"
  ]
  node [
    id 128
    label "explosion"
  ]
  node [
    id 129
    label "produkcja"
  ]
  node [
    id 130
    label "zrobienie"
  ]
  node [
    id 131
    label "zabija&#263;"
  ]
  node [
    id 132
    label "od&#322;upywa&#263;"
  ]
  node [
    id 133
    label "urywa&#263;"
  ]
  node [
    id 134
    label "fire"
  ]
  node [
    id 135
    label "wa&#322;"
  ]
  node [
    id 136
    label "ilo&#347;&#263;"
  ]
  node [
    id 137
    label "miejsce"
  ]
  node [
    id 138
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 139
    label "toporek"
  ]
  node [
    id 140
    label "przerywnik"
  ]
  node [
    id 141
    label "przy&#322;&#261;cze"
  ]
  node [
    id 142
    label "zagrywka"
  ]
  node [
    id 143
    label "wn&#281;ka"
  ]
  node [
    id 144
    label "wci&#281;cie"
  ]
  node [
    id 145
    label "film"
  ]
  node [
    id 146
    label "sztuczka"
  ]
  node [
    id 147
    label "fortel"
  ]
  node [
    id 148
    label "podci&#261;gnik"
  ]
  node [
    id 149
    label "budownictwo"
  ]
  node [
    id 150
    label "urz&#261;dzenie"
  ]
  node [
    id 151
    label "&#322;adowarka"
  ]
  node [
    id 152
    label "robotnik"
  ]
  node [
    id 153
    label "zasobnik"
  ]
  node [
    id 154
    label "mocowanie"
  ]
  node [
    id 155
    label "bro&#324;_maszynowa"
  ]
  node [
    id 156
    label "&#322;om"
  ]
  node [
    id 157
    label "rolnictwo"
  ]
  node [
    id 158
    label "scraper"
  ]
  node [
    id 159
    label "przemys&#322;"
  ]
  node [
    id 160
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 161
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 162
    label "uprzemys&#322;owienie"
  ]
  node [
    id 163
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 164
    label "przechowalnictwo"
  ]
  node [
    id 165
    label "uprzemys&#322;awianie"
  ]
  node [
    id 166
    label "gospodarka"
  ]
  node [
    id 167
    label "ustawi&#263;"
  ]
  node [
    id 168
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 169
    label "establish"
  ]
  node [
    id 170
    label "podstawa"
  ]
  node [
    id 171
    label "recline"
  ]
  node [
    id 172
    label "osnowa&#263;"
  ]
  node [
    id 173
    label "woda"
  ]
  node [
    id 174
    label "hoax"
  ]
  node [
    id 175
    label "return"
  ]
  node [
    id 176
    label "wzi&#261;&#263;"
  ]
  node [
    id 177
    label "seize"
  ]
  node [
    id 178
    label "skorzysta&#263;"
  ]
  node [
    id 179
    label "poprawi&#263;"
  ]
  node [
    id 180
    label "nada&#263;"
  ]
  node [
    id 181
    label "peddle"
  ]
  node [
    id 182
    label "marshal"
  ]
  node [
    id 183
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 184
    label "wyznaczy&#263;"
  ]
  node [
    id 185
    label "stanowisko"
  ]
  node [
    id 186
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 187
    label "spowodowa&#263;"
  ]
  node [
    id 188
    label "zabezpieczy&#263;"
  ]
  node [
    id 189
    label "umie&#347;ci&#263;"
  ]
  node [
    id 190
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 191
    label "zinterpretowa&#263;"
  ]
  node [
    id 192
    label "wskaza&#263;"
  ]
  node [
    id 193
    label "set"
  ]
  node [
    id 194
    label "przyzna&#263;"
  ]
  node [
    id 195
    label "sk&#322;oni&#263;"
  ]
  node [
    id 196
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 197
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 198
    label "zdecydowa&#263;"
  ]
  node [
    id 199
    label "accommodate"
  ]
  node [
    id 200
    label "ustali&#263;"
  ]
  node [
    id 201
    label "situate"
  ]
  node [
    id 202
    label "rola"
  ]
  node [
    id 203
    label "osnu&#263;"
  ]
  node [
    id 204
    label "zasadzi&#263;"
  ]
  node [
    id 205
    label "pot&#281;ga"
  ]
  node [
    id 206
    label "documentation"
  ]
  node [
    id 207
    label "column"
  ]
  node [
    id 208
    label "zasadzenie"
  ]
  node [
    id 209
    label "za&#322;o&#380;enie"
  ]
  node [
    id 210
    label "punkt_odniesienia"
  ]
  node [
    id 211
    label "bok"
  ]
  node [
    id 212
    label "d&#243;&#322;"
  ]
  node [
    id 213
    label "dzieci&#281;ctwo"
  ]
  node [
    id 214
    label "background"
  ]
  node [
    id 215
    label "podstawowy"
  ]
  node [
    id 216
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 217
    label "strategia"
  ]
  node [
    id 218
    label "pomys&#322;"
  ]
  node [
    id 219
    label "&#347;ciana"
  ]
  node [
    id 220
    label "coal"
  ]
  node [
    id 221
    label "fulleren"
  ]
  node [
    id 222
    label "niemetal"
  ]
  node [
    id 223
    label "rysunek"
  ]
  node [
    id 224
    label "ska&#322;a"
  ]
  node [
    id 225
    label "zsypnik"
  ]
  node [
    id 226
    label "surowiec_energetyczny"
  ]
  node [
    id 227
    label "zw&#281;glanie_si&#281;"
  ]
  node [
    id 228
    label "coil"
  ]
  node [
    id 229
    label "przybory_do_pisania"
  ]
  node [
    id 230
    label "w&#281;glarka"
  ]
  node [
    id 231
    label "bry&#322;a"
  ]
  node [
    id 232
    label "w&#281;glowodan"
  ]
  node [
    id 233
    label "makroelement"
  ]
  node [
    id 234
    label "zw&#281;glenie_si&#281;"
  ]
  node [
    id 235
    label "kopalina_podstawowa"
  ]
  node [
    id 236
    label "w&#281;glowiec"
  ]
  node [
    id 237
    label "carbon"
  ]
  node [
    id 238
    label "pierwiastek"
  ]
  node [
    id 239
    label "sk&#322;adnik_mineralny"
  ]
  node [
    id 240
    label "kszta&#322;t"
  ]
  node [
    id 241
    label "figura_geometryczna"
  ]
  node [
    id 242
    label "bezkszta&#322;tno&#347;&#263;"
  ]
  node [
    id 243
    label "kawa&#322;ek"
  ]
  node [
    id 244
    label "block"
  ]
  node [
    id 245
    label "solid"
  ]
  node [
    id 246
    label "kreska"
  ]
  node [
    id 247
    label "teka"
  ]
  node [
    id 248
    label "photograph"
  ]
  node [
    id 249
    label "ilustracja"
  ]
  node [
    id 250
    label "grafika"
  ]
  node [
    id 251
    label "plastyka"
  ]
  node [
    id 252
    label "shape"
  ]
  node [
    id 253
    label "uskoczenie"
  ]
  node [
    id 254
    label "mieszanina"
  ]
  node [
    id 255
    label "zmetamorfizowanie"
  ]
  node [
    id 256
    label "soczewa"
  ]
  node [
    id 257
    label "opoka"
  ]
  node [
    id 258
    label "uskakiwa&#263;"
  ]
  node [
    id 259
    label "sklerometr"
  ]
  node [
    id 260
    label "&#322;upkowato&#347;&#263;"
  ]
  node [
    id 261
    label "uskakiwanie"
  ]
  node [
    id 262
    label "uskoczy&#263;"
  ]
  node [
    id 263
    label "rock"
  ]
  node [
    id 264
    label "obiekt"
  ]
  node [
    id 265
    label "porwak"
  ]
  node [
    id 266
    label "bloczno&#347;&#263;"
  ]
  node [
    id 267
    label "zmetamorfizowa&#263;"
  ]
  node [
    id 268
    label "lepiszcze_skalne"
  ]
  node [
    id 269
    label "rygiel"
  ]
  node [
    id 270
    label "lamina"
  ]
  node [
    id 271
    label "wyg&#322;ad_lodowcowy"
  ]
  node [
    id 272
    label "cz&#261;steczka"
  ]
  node [
    id 273
    label "fullerene"
  ]
  node [
    id 274
    label "zbiornik"
  ]
  node [
    id 275
    label "tworzywo"
  ]
  node [
    id 276
    label "grupa_hydroksylowa"
  ]
  node [
    id 277
    label "carbohydrate"
  ]
  node [
    id 278
    label "sk&#322;adnik_pokarmowy"
  ]
  node [
    id 279
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 280
    label "w&#243;z"
  ]
  node [
    id 281
    label "wagon_towarowy"
  ]
  node [
    id 282
    label "pora_roku"
  ]
  node [
    id 283
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 284
    label "bliski"
  ]
  node [
    id 285
    label "dok&#322;adnie"
  ]
  node [
    id 286
    label "silnie"
  ]
  node [
    id 287
    label "cz&#322;owiek"
  ]
  node [
    id 288
    label "znajomy"
  ]
  node [
    id 289
    label "zwi&#261;zany"
  ]
  node [
    id 290
    label "przesz&#322;y"
  ]
  node [
    id 291
    label "silny"
  ]
  node [
    id 292
    label "zbli&#380;enie"
  ]
  node [
    id 293
    label "kr&#243;tki"
  ]
  node [
    id 294
    label "oddalony"
  ]
  node [
    id 295
    label "dok&#322;adny"
  ]
  node [
    id 296
    label "nieodleg&#322;y"
  ]
  node [
    id 297
    label "przysz&#322;y"
  ]
  node [
    id 298
    label "gotowy"
  ]
  node [
    id 299
    label "ma&#322;y"
  ]
  node [
    id 300
    label "meticulously"
  ]
  node [
    id 301
    label "punctiliously"
  ]
  node [
    id 302
    label "precyzyjnie"
  ]
  node [
    id 303
    label "rzetelnie"
  ]
  node [
    id 304
    label "mocny"
  ]
  node [
    id 305
    label "zajebi&#347;cie"
  ]
  node [
    id 306
    label "przekonuj&#261;co"
  ]
  node [
    id 307
    label "powerfully"
  ]
  node [
    id 308
    label "konkretnie"
  ]
  node [
    id 309
    label "niepodwa&#380;alnie"
  ]
  node [
    id 310
    label "zdecydowanie"
  ]
  node [
    id 311
    label "dusznie"
  ]
  node [
    id 312
    label "intensywnie"
  ]
  node [
    id 313
    label "strongly"
  ]
  node [
    id 314
    label "liczba"
  ]
  node [
    id 315
    label "kategoria"
  ]
  node [
    id 316
    label "rozmiar"
  ]
  node [
    id 317
    label "wyra&#380;enie"
  ]
  node [
    id 318
    label "poj&#281;cie"
  ]
  node [
    id 319
    label "number"
  ]
  node [
    id 320
    label "cecha"
  ]
  node [
    id 321
    label "kategoria_gramatyczna"
  ]
  node [
    id 322
    label "grupa"
  ]
  node [
    id 323
    label "kwadrat_magiczny"
  ]
  node [
    id 324
    label "koniugacja"
  ]
  node [
    id 325
    label "jednostka_monetarna"
  ]
  node [
    id 326
    label "wspania&#322;y"
  ]
  node [
    id 327
    label "metaliczny"
  ]
  node [
    id 328
    label "Polska"
  ]
  node [
    id 329
    label "szlachetny"
  ]
  node [
    id 330
    label "kochany"
  ]
  node [
    id 331
    label "doskona&#322;y"
  ]
  node [
    id 332
    label "grosz"
  ]
  node [
    id 333
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 334
    label "poz&#322;ocenie"
  ]
  node [
    id 335
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 336
    label "utytu&#322;owany"
  ]
  node [
    id 337
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 338
    label "z&#322;ocenie"
  ]
  node [
    id 339
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 340
    label "prominentny"
  ]
  node [
    id 341
    label "znany"
  ]
  node [
    id 342
    label "wybitny"
  ]
  node [
    id 343
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 344
    label "naj"
  ]
  node [
    id 345
    label "&#347;wietny"
  ]
  node [
    id 346
    label "pe&#322;ny"
  ]
  node [
    id 347
    label "doskonale"
  ]
  node [
    id 348
    label "szlachetnie"
  ]
  node [
    id 349
    label "uczciwy"
  ]
  node [
    id 350
    label "zacny"
  ]
  node [
    id 351
    label "harmonijny"
  ]
  node [
    id 352
    label "gatunkowy"
  ]
  node [
    id 353
    label "pi&#281;kny"
  ]
  node [
    id 354
    label "dobry"
  ]
  node [
    id 355
    label "typowy"
  ]
  node [
    id 356
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 357
    label "metaloplastyczny"
  ]
  node [
    id 358
    label "metalicznie"
  ]
  node [
    id 359
    label "kochanek"
  ]
  node [
    id 360
    label "wybranek"
  ]
  node [
    id 361
    label "umi&#322;owany"
  ]
  node [
    id 362
    label "drogi"
  ]
  node [
    id 363
    label "kochanie"
  ]
  node [
    id 364
    label "wspaniale"
  ]
  node [
    id 365
    label "pomy&#347;lny"
  ]
  node [
    id 366
    label "pozytywny"
  ]
  node [
    id 367
    label "&#347;wietnie"
  ]
  node [
    id 368
    label "spania&#322;y"
  ]
  node [
    id 369
    label "och&#281;do&#380;ny"
  ]
  node [
    id 370
    label "warto&#347;ciowy"
  ]
  node [
    id 371
    label "zajebisty"
  ]
  node [
    id 372
    label "bogato"
  ]
  node [
    id 373
    label "typ_mongoloidalny"
  ]
  node [
    id 374
    label "kolorowy"
  ]
  node [
    id 375
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 376
    label "ciep&#322;y"
  ]
  node [
    id 377
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 378
    label "jasny"
  ]
  node [
    id 379
    label "kwota"
  ]
  node [
    id 380
    label "groszak"
  ]
  node [
    id 381
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 382
    label "szyling_austryjacki"
  ]
  node [
    id 383
    label "moneta"
  ]
  node [
    id 384
    label "Mazowsze"
  ]
  node [
    id 385
    label "Pa&#322;uki"
  ]
  node [
    id 386
    label "Pomorze_Zachodnie"
  ]
  node [
    id 387
    label "Powi&#347;le"
  ]
  node [
    id 388
    label "Wolin"
  ]
  node [
    id 389
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 390
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 391
    label "So&#322;a"
  ]
  node [
    id 392
    label "Unia_Europejska"
  ]
  node [
    id 393
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 394
    label "Opolskie"
  ]
  node [
    id 395
    label "Suwalszczyzna"
  ]
  node [
    id 396
    label "Krajna"
  ]
  node [
    id 397
    label "barwy_polskie"
  ]
  node [
    id 398
    label "Nadbu&#380;e"
  ]
  node [
    id 399
    label "Podlasie"
  ]
  node [
    id 400
    label "Izera"
  ]
  node [
    id 401
    label "Ma&#322;opolska"
  ]
  node [
    id 402
    label "Warmia"
  ]
  node [
    id 403
    label "Mazury"
  ]
  node [
    id 404
    label "NATO"
  ]
  node [
    id 405
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 406
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 407
    label "Lubelszczyzna"
  ]
  node [
    id 408
    label "Kaczawa"
  ]
  node [
    id 409
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 410
    label "Kielecczyzna"
  ]
  node [
    id 411
    label "Lubuskie"
  ]
  node [
    id 412
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 413
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 414
    label "&#321;&#243;dzkie"
  ]
  node [
    id 415
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 416
    label "Kujawy"
  ]
  node [
    id 417
    label "Podkarpacie"
  ]
  node [
    id 418
    label "Wielkopolska"
  ]
  node [
    id 419
    label "Wis&#322;a"
  ]
  node [
    id 420
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 421
    label "Bory_Tucholskie"
  ]
  node [
    id 422
    label "z&#322;ocisty"
  ]
  node [
    id 423
    label "powleczenie"
  ]
  node [
    id 424
    label "zabarwienie"
  ]
  node [
    id 425
    label "platerowanie"
  ]
  node [
    id 426
    label "barwienie"
  ]
  node [
    id 427
    label "gilt"
  ]
  node [
    id 428
    label "plating"
  ]
  node [
    id 429
    label "zdobienie"
  ]
  node [
    id 430
    label "club"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
]
