graph [
  node [
    id 0
    label "bohater"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "imi&#281;"
    origin "text"
  ]
  node [
    id 3
    label "oskar"
    origin "text"
  ]
  node [
    id 4
    label "gra"
    origin "text"
  ]
  node [
    id 5
    label "tamburyn"
    origin "text"
  ]
  node [
    id 6
    label "Messi"
  ]
  node [
    id 7
    label "Herkules_Poirot"
  ]
  node [
    id 8
    label "cz&#322;owiek"
  ]
  node [
    id 9
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 10
    label "Achilles"
  ]
  node [
    id 11
    label "Harry_Potter"
  ]
  node [
    id 12
    label "Casanova"
  ]
  node [
    id 13
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 14
    label "Zgredek"
  ]
  node [
    id 15
    label "Asterix"
  ]
  node [
    id 16
    label "Winnetou"
  ]
  node [
    id 17
    label "uczestnik"
  ]
  node [
    id 18
    label "posta&#263;"
  ]
  node [
    id 19
    label "&#347;mia&#322;ek"
  ]
  node [
    id 20
    label "Mario"
  ]
  node [
    id 21
    label "Borewicz"
  ]
  node [
    id 22
    label "Quasimodo"
  ]
  node [
    id 23
    label "Sherlock_Holmes"
  ]
  node [
    id 24
    label "Wallenrod"
  ]
  node [
    id 25
    label "Herkules"
  ]
  node [
    id 26
    label "bohaterski"
  ]
  node [
    id 27
    label "Don_Juan"
  ]
  node [
    id 28
    label "podmiot"
  ]
  node [
    id 29
    label "Don_Kiszot"
  ]
  node [
    id 30
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 31
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 32
    label "Hamlet"
  ]
  node [
    id 33
    label "Werter"
  ]
  node [
    id 34
    label "Szwejk"
  ]
  node [
    id 35
    label "charakterystyka"
  ]
  node [
    id 36
    label "zaistnie&#263;"
  ]
  node [
    id 37
    label "Osjan"
  ]
  node [
    id 38
    label "cecha"
  ]
  node [
    id 39
    label "kto&#347;"
  ]
  node [
    id 40
    label "wygl&#261;d"
  ]
  node [
    id 41
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 42
    label "osobowo&#347;&#263;"
  ]
  node [
    id 43
    label "wytw&#243;r"
  ]
  node [
    id 44
    label "trim"
  ]
  node [
    id 45
    label "poby&#263;"
  ]
  node [
    id 46
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 47
    label "Aspazja"
  ]
  node [
    id 48
    label "punkt_widzenia"
  ]
  node [
    id 49
    label "kompleksja"
  ]
  node [
    id 50
    label "wytrzyma&#263;"
  ]
  node [
    id 51
    label "budowa"
  ]
  node [
    id 52
    label "formacja"
  ]
  node [
    id 53
    label "pozosta&#263;"
  ]
  node [
    id 54
    label "point"
  ]
  node [
    id 55
    label "przedstawienie"
  ]
  node [
    id 56
    label "go&#347;&#263;"
  ]
  node [
    id 57
    label "ludzko&#347;&#263;"
  ]
  node [
    id 58
    label "asymilowanie"
  ]
  node [
    id 59
    label "wapniak"
  ]
  node [
    id 60
    label "asymilowa&#263;"
  ]
  node [
    id 61
    label "os&#322;abia&#263;"
  ]
  node [
    id 62
    label "hominid"
  ]
  node [
    id 63
    label "podw&#322;adny"
  ]
  node [
    id 64
    label "os&#322;abianie"
  ]
  node [
    id 65
    label "g&#322;owa"
  ]
  node [
    id 66
    label "figura"
  ]
  node [
    id 67
    label "portrecista"
  ]
  node [
    id 68
    label "dwun&#243;g"
  ]
  node [
    id 69
    label "profanum"
  ]
  node [
    id 70
    label "mikrokosmos"
  ]
  node [
    id 71
    label "nasada"
  ]
  node [
    id 72
    label "duch"
  ]
  node [
    id 73
    label "antropochoria"
  ]
  node [
    id 74
    label "osoba"
  ]
  node [
    id 75
    label "wz&#243;r"
  ]
  node [
    id 76
    label "senior"
  ]
  node [
    id 77
    label "oddzia&#322;ywanie"
  ]
  node [
    id 78
    label "Adam"
  ]
  node [
    id 79
    label "homo_sapiens"
  ]
  node [
    id 80
    label "polifag"
  ]
  node [
    id 81
    label "zuch"
  ]
  node [
    id 82
    label "rycerzyk"
  ]
  node [
    id 83
    label "odwa&#380;ny"
  ]
  node [
    id 84
    label "ryzykant"
  ]
  node [
    id 85
    label "morowiec"
  ]
  node [
    id 86
    label "trawa"
  ]
  node [
    id 87
    label "ro&#347;lina"
  ]
  node [
    id 88
    label "twardziel"
  ]
  node [
    id 89
    label "owsowe"
  ]
  node [
    id 90
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 91
    label "byt"
  ]
  node [
    id 92
    label "organizacja"
  ]
  node [
    id 93
    label "prawo"
  ]
  node [
    id 94
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 95
    label "nauka_prawa"
  ]
  node [
    id 96
    label "Szekspir"
  ]
  node [
    id 97
    label "Mickiewicz"
  ]
  node [
    id 98
    label "cierpienie"
  ]
  node [
    id 99
    label "Atomowa_Pch&#322;a"
  ]
  node [
    id 100
    label "bohatersko"
  ]
  node [
    id 101
    label "dzielny"
  ]
  node [
    id 102
    label "silny"
  ]
  node [
    id 103
    label "waleczny"
  ]
  node [
    id 104
    label "czyj&#347;"
  ]
  node [
    id 105
    label "m&#261;&#380;"
  ]
  node [
    id 106
    label "prywatny"
  ]
  node [
    id 107
    label "ma&#322;&#380;onek"
  ]
  node [
    id 108
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 109
    label "ch&#322;op"
  ]
  node [
    id 110
    label "pan_m&#322;ody"
  ]
  node [
    id 111
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 112
    label "&#347;lubny"
  ]
  node [
    id 113
    label "pan_domu"
  ]
  node [
    id 114
    label "pan_i_w&#322;adca"
  ]
  node [
    id 115
    label "stary"
  ]
  node [
    id 116
    label "reputacja"
  ]
  node [
    id 117
    label "deklinacja"
  ]
  node [
    id 118
    label "term"
  ]
  node [
    id 119
    label "personalia"
  ]
  node [
    id 120
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 121
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 122
    label "leksem"
  ]
  node [
    id 123
    label "wezwanie"
  ]
  node [
    id 124
    label "wielko&#347;&#263;"
  ]
  node [
    id 125
    label "nazwa_w&#322;asna"
  ]
  node [
    id 126
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 127
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 128
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 129
    label "patron"
  ]
  node [
    id 130
    label "imiennictwo"
  ]
  node [
    id 131
    label "wordnet"
  ]
  node [
    id 132
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 133
    label "wypowiedzenie"
  ]
  node [
    id 134
    label "morfem"
  ]
  node [
    id 135
    label "s&#322;ownictwo"
  ]
  node [
    id 136
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 137
    label "wykrzyknik"
  ]
  node [
    id 138
    label "pole_semantyczne"
  ]
  node [
    id 139
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 140
    label "pisanie_si&#281;"
  ]
  node [
    id 141
    label "nag&#322;os"
  ]
  node [
    id 142
    label "wyg&#322;os"
  ]
  node [
    id 143
    label "jednostka_leksykalna"
  ]
  node [
    id 144
    label "znaczenie"
  ]
  node [
    id 145
    label "opinia"
  ]
  node [
    id 146
    label "perversion"
  ]
  node [
    id 147
    label "k&#261;t"
  ]
  node [
    id 148
    label "poj&#281;cie"
  ]
  node [
    id 149
    label "fleksja"
  ]
  node [
    id 150
    label "&#322;uska"
  ]
  node [
    id 151
    label "opiekun"
  ]
  node [
    id 152
    label "patrycjusz"
  ]
  node [
    id 153
    label "prawnik"
  ]
  node [
    id 154
    label "nab&#243;j"
  ]
  node [
    id 155
    label "&#347;wi&#281;ty"
  ]
  node [
    id 156
    label "zmar&#322;y"
  ]
  node [
    id 157
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 158
    label "&#347;w"
  ]
  node [
    id 159
    label "nazwa"
  ]
  node [
    id 160
    label "szablon"
  ]
  node [
    id 161
    label "warunek_lokalowy"
  ]
  node [
    id 162
    label "rozmiar"
  ]
  node [
    id 163
    label "liczba"
  ]
  node [
    id 164
    label "rzadko&#347;&#263;"
  ]
  node [
    id 165
    label "zaleta"
  ]
  node [
    id 166
    label "ilo&#347;&#263;"
  ]
  node [
    id 167
    label "measure"
  ]
  node [
    id 168
    label "dymensja"
  ]
  node [
    id 169
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 170
    label "zdolno&#347;&#263;"
  ]
  node [
    id 171
    label "potencja"
  ]
  node [
    id 172
    label "property"
  ]
  node [
    id 173
    label "nakaz"
  ]
  node [
    id 174
    label "pismo_urz&#281;dowe"
  ]
  node [
    id 175
    label "zwo&#322;anie_si&#281;"
  ]
  node [
    id 176
    label "wst&#281;p"
  ]
  node [
    id 177
    label "pro&#347;ba"
  ]
  node [
    id 178
    label "nakazanie"
  ]
  node [
    id 179
    label "admonition"
  ]
  node [
    id 180
    label "summons"
  ]
  node [
    id 181
    label "poproszenie"
  ]
  node [
    id 182
    label "bid"
  ]
  node [
    id 183
    label "apostrofa"
  ]
  node [
    id 184
    label "zach&#281;cenie"
  ]
  node [
    id 185
    label "poinformowanie"
  ]
  node [
    id 186
    label "fitonimia"
  ]
  node [
    id 187
    label "ideonimia"
  ]
  node [
    id 188
    label "leksykologia"
  ]
  node [
    id 189
    label "etnonimia"
  ]
  node [
    id 190
    label "zas&#243;b"
  ]
  node [
    id 191
    label "antroponimia"
  ]
  node [
    id 192
    label "toponimia"
  ]
  node [
    id 193
    label "chrematonimia"
  ]
  node [
    id 194
    label "NN"
  ]
  node [
    id 195
    label "nazwisko"
  ]
  node [
    id 196
    label "adres"
  ]
  node [
    id 197
    label "dane"
  ]
  node [
    id 198
    label "to&#380;samo&#347;&#263;_osobista"
  ]
  node [
    id 199
    label "pesel"
  ]
  node [
    id 200
    label "zmienno&#347;&#263;"
  ]
  node [
    id 201
    label "play"
  ]
  node [
    id 202
    label "rozgrywka"
  ]
  node [
    id 203
    label "apparent_motion"
  ]
  node [
    id 204
    label "wydarzenie"
  ]
  node [
    id 205
    label "contest"
  ]
  node [
    id 206
    label "akcja"
  ]
  node [
    id 207
    label "komplet"
  ]
  node [
    id 208
    label "zabawa"
  ]
  node [
    id 209
    label "zasada"
  ]
  node [
    id 210
    label "rywalizacja"
  ]
  node [
    id 211
    label "zbijany"
  ]
  node [
    id 212
    label "post&#281;powanie"
  ]
  node [
    id 213
    label "game"
  ]
  node [
    id 214
    label "odg&#322;os"
  ]
  node [
    id 215
    label "Pok&#233;mon"
  ]
  node [
    id 216
    label "czynno&#347;&#263;"
  ]
  node [
    id 217
    label "synteza"
  ]
  node [
    id 218
    label "odtworzenie"
  ]
  node [
    id 219
    label "rekwizyt_do_gry"
  ]
  node [
    id 220
    label "resonance"
  ]
  node [
    id 221
    label "wydanie"
  ]
  node [
    id 222
    label "wpadni&#281;cie"
  ]
  node [
    id 223
    label "d&#378;wi&#281;k"
  ]
  node [
    id 224
    label "wpadanie"
  ]
  node [
    id 225
    label "wydawa&#263;"
  ]
  node [
    id 226
    label "sound"
  ]
  node [
    id 227
    label "brzmienie"
  ]
  node [
    id 228
    label "zjawisko"
  ]
  node [
    id 229
    label "wyda&#263;"
  ]
  node [
    id 230
    label "wpa&#347;&#263;"
  ]
  node [
    id 231
    label "note"
  ]
  node [
    id 232
    label "onomatopeja"
  ]
  node [
    id 233
    label "wpada&#263;"
  ]
  node [
    id 234
    label "s&#261;d"
  ]
  node [
    id 235
    label "kognicja"
  ]
  node [
    id 236
    label "campaign"
  ]
  node [
    id 237
    label "rozprawa"
  ]
  node [
    id 238
    label "zachowanie"
  ]
  node [
    id 239
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 240
    label "fashion"
  ]
  node [
    id 241
    label "robienie"
  ]
  node [
    id 242
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 243
    label "zmierzanie"
  ]
  node [
    id 244
    label "przes&#322;anka"
  ]
  node [
    id 245
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 246
    label "kazanie"
  ]
  node [
    id 247
    label "trafienie"
  ]
  node [
    id 248
    label "rewan&#380;owy"
  ]
  node [
    id 249
    label "zagrywka"
  ]
  node [
    id 250
    label "faza"
  ]
  node [
    id 251
    label "euroliga"
  ]
  node [
    id 252
    label "interliga"
  ]
  node [
    id 253
    label "runda"
  ]
  node [
    id 254
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 255
    label "rozrywka"
  ]
  node [
    id 256
    label "impreza"
  ]
  node [
    id 257
    label "igraszka"
  ]
  node [
    id 258
    label "taniec"
  ]
  node [
    id 259
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 260
    label "gambling"
  ]
  node [
    id 261
    label "chwyt"
  ]
  node [
    id 262
    label "igra"
  ]
  node [
    id 263
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 264
    label "nabawienie_si&#281;"
  ]
  node [
    id 265
    label "ubaw"
  ]
  node [
    id 266
    label "wodzirej"
  ]
  node [
    id 267
    label "activity"
  ]
  node [
    id 268
    label "bezproblemowy"
  ]
  node [
    id 269
    label "przebiec"
  ]
  node [
    id 270
    label "charakter"
  ]
  node [
    id 271
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 272
    label "motyw"
  ]
  node [
    id 273
    label "przebiegni&#281;cie"
  ]
  node [
    id 274
    label "fabu&#322;a"
  ]
  node [
    id 275
    label "proces_technologiczny"
  ]
  node [
    id 276
    label "mieszanina"
  ]
  node [
    id 277
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 278
    label "fusion"
  ]
  node [
    id 279
    label "reakcja_chemiczna"
  ]
  node [
    id 280
    label "zestawienie"
  ]
  node [
    id 281
    label "uog&#243;lnienie"
  ]
  node [
    id 282
    label "puszczenie"
  ]
  node [
    id 283
    label "ustalenie"
  ]
  node [
    id 284
    label "wyst&#281;p"
  ]
  node [
    id 285
    label "reproduction"
  ]
  node [
    id 286
    label "przywr&#243;cenie"
  ]
  node [
    id 287
    label "w&#322;&#261;czenie"
  ]
  node [
    id 288
    label "zregenerowanie_si&#281;"
  ]
  node [
    id 289
    label "restoration"
  ]
  node [
    id 290
    label "odbudowanie"
  ]
  node [
    id 291
    label "lekcja"
  ]
  node [
    id 292
    label "ensemble"
  ]
  node [
    id 293
    label "grupa"
  ]
  node [
    id 294
    label "klasa"
  ]
  node [
    id 295
    label "zestaw"
  ]
  node [
    id 296
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 297
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 298
    label "regu&#322;a_Allena"
  ]
  node [
    id 299
    label "base"
  ]
  node [
    id 300
    label "umowa"
  ]
  node [
    id 301
    label "obserwacja"
  ]
  node [
    id 302
    label "zasada_d'Alemberta"
  ]
  node [
    id 303
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 304
    label "normalizacja"
  ]
  node [
    id 305
    label "moralno&#347;&#263;"
  ]
  node [
    id 306
    label "criterion"
  ]
  node [
    id 307
    label "opis"
  ]
  node [
    id 308
    label "regu&#322;a_Glogera"
  ]
  node [
    id 309
    label "prawo_Mendla"
  ]
  node [
    id 310
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 311
    label "twierdzenie"
  ]
  node [
    id 312
    label "standard"
  ]
  node [
    id 313
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 314
    label "spos&#243;b"
  ]
  node [
    id 315
    label "dominion"
  ]
  node [
    id 316
    label "qualification"
  ]
  node [
    id 317
    label "occupation"
  ]
  node [
    id 318
    label "podstawa"
  ]
  node [
    id 319
    label "substancja"
  ]
  node [
    id 320
    label "prawid&#322;o"
  ]
  node [
    id 321
    label "dywidenda"
  ]
  node [
    id 322
    label "przebieg"
  ]
  node [
    id 323
    label "operacja"
  ]
  node [
    id 324
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 325
    label "udzia&#322;"
  ]
  node [
    id 326
    label "commotion"
  ]
  node [
    id 327
    label "jazda"
  ]
  node [
    id 328
    label "czyn"
  ]
  node [
    id 329
    label "stock"
  ]
  node [
    id 330
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 331
    label "w&#281;ze&#322;"
  ]
  node [
    id 332
    label "wysoko&#347;&#263;"
  ]
  node [
    id 333
    label "instrument_strunowy"
  ]
  node [
    id 334
    label "pi&#322;ka"
  ]
  node [
    id 335
    label "b&#281;benek"
  ]
  node [
    id 336
    label "instrument_perkusyjny"
  ]
  node [
    id 337
    label "narz&#281;dzie"
  ]
  node [
    id 338
    label "maszyna"
  ]
  node [
    id 339
    label "element_anatomiczny"
  ]
  node [
    id 340
    label "b&#322;ona"
  ]
  node [
    id 341
    label "eardrum"
  ]
  node [
    id 342
    label "ucho_&#347;rodkowe"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
]
