graph [
  node [
    id 0
    label "lori"
    origin "text"
  ]
  node [
    id 1
    label "glazier"
    origin "text"
  ]
  node [
    id 2
    label "Glazier"
  ]
  node [
    id 3
    label "madonna"
  ]
  node [
    id 4
    label "di"
  ]
  node [
    id 5
    label "Campiglio"
  ]
  node [
    id 6
    label "puchar"
  ]
  node [
    id 7
    label "&#347;wiat"
  ]
  node [
    id 8
    label "Sun"
  ]
  node [
    id 9
    label "Peaks"
  ]
  node [
    id 10
    label "Boreal"
  ]
  node [
    id 11
    label "Ridge"
  ]
  node [
    id 12
    label "Mount"
  ]
  node [
    id 13
    label "Bachelor"
  ]
  node [
    id 14
    label "Mont"
  ]
  node [
    id 15
    label "Sainte"
  ]
  node [
    id 16
    label "Anne"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
]
