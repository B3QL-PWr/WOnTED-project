graph [
  node [
    id 0
    label "ostatni"
    origin "text"
  ]
  node [
    id 1
    label "program"
    origin "text"
  ]
  node [
    id 2
    label "info"
    origin "text"
  ]
  node [
    id 3
    label "wideo"
    origin "text"
  ]
  node [
    id 4
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "grafik"
    origin "text"
  ]
  node [
    id 7
    label "por&#243;wnywa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "rosja"
    origin "text"
  ]
  node [
    id 9
    label "zbrodniczy"
    origin "text"
  ]
  node [
    id 10
    label "nazistowski"
    origin "text"
  ]
  node [
    id 11
    label "organizacja"
    origin "text"
  ]
  node [
    id 12
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 13
    label "istota_&#380;ywa"
  ]
  node [
    id 14
    label "cz&#322;owiek"
  ]
  node [
    id 15
    label "najgorszy"
  ]
  node [
    id 16
    label "pozosta&#322;y"
  ]
  node [
    id 17
    label "w&#261;tpliwy"
  ]
  node [
    id 18
    label "poprzedni"
  ]
  node [
    id 19
    label "sko&#324;czony"
  ]
  node [
    id 20
    label "ostatnio"
  ]
  node [
    id 21
    label "kolejny"
  ]
  node [
    id 22
    label "aktualny"
  ]
  node [
    id 23
    label "niedawno"
  ]
  node [
    id 24
    label "inny"
  ]
  node [
    id 25
    label "nast&#281;pnie"
  ]
  node [
    id 26
    label "kolejno"
  ]
  node [
    id 27
    label "kt&#243;ry&#347;"
  ]
  node [
    id 28
    label "nastopny"
  ]
  node [
    id 29
    label "przesz&#322;y"
  ]
  node [
    id 30
    label "wcze&#347;niejszy"
  ]
  node [
    id 31
    label "poprzednio"
  ]
  node [
    id 32
    label "w&#261;tpliwie"
  ]
  node [
    id 33
    label "pozorny"
  ]
  node [
    id 34
    label "&#380;ywy"
  ]
  node [
    id 35
    label "wa&#380;ny"
  ]
  node [
    id 36
    label "ostateczny"
  ]
  node [
    id 37
    label "asymilowa&#263;"
  ]
  node [
    id 38
    label "nasada"
  ]
  node [
    id 39
    label "profanum"
  ]
  node [
    id 40
    label "wz&#243;r"
  ]
  node [
    id 41
    label "senior"
  ]
  node [
    id 42
    label "asymilowanie"
  ]
  node [
    id 43
    label "os&#322;abia&#263;"
  ]
  node [
    id 44
    label "homo_sapiens"
  ]
  node [
    id 45
    label "osoba"
  ]
  node [
    id 46
    label "ludzko&#347;&#263;"
  ]
  node [
    id 47
    label "Adam"
  ]
  node [
    id 48
    label "hominid"
  ]
  node [
    id 49
    label "posta&#263;"
  ]
  node [
    id 50
    label "portrecista"
  ]
  node [
    id 51
    label "polifag"
  ]
  node [
    id 52
    label "podw&#322;adny"
  ]
  node [
    id 53
    label "dwun&#243;g"
  ]
  node [
    id 54
    label "wapniak"
  ]
  node [
    id 55
    label "duch"
  ]
  node [
    id 56
    label "os&#322;abianie"
  ]
  node [
    id 57
    label "antropochoria"
  ]
  node [
    id 58
    label "figura"
  ]
  node [
    id 59
    label "g&#322;owa"
  ]
  node [
    id 60
    label "mikrokosmos"
  ]
  node [
    id 61
    label "oddzia&#322;ywanie"
  ]
  node [
    id 62
    label "kompletny"
  ]
  node [
    id 63
    label "sko&#324;czenie"
  ]
  node [
    id 64
    label "dyplomowany"
  ]
  node [
    id 65
    label "wykszta&#322;cony"
  ]
  node [
    id 66
    label "wykwalifikowany"
  ]
  node [
    id 67
    label "wielki"
  ]
  node [
    id 68
    label "okre&#347;lony"
  ]
  node [
    id 69
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 70
    label "aktualizowanie"
  ]
  node [
    id 71
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 72
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 73
    label "aktualnie"
  ]
  node [
    id 74
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 75
    label "uaktualnienie"
  ]
  node [
    id 76
    label "za&#322;o&#380;enie"
  ]
  node [
    id 77
    label "dzia&#322;"
  ]
  node [
    id 78
    label "odinstalowa&#263;"
  ]
  node [
    id 79
    label "spis"
  ]
  node [
    id 80
    label "broszura"
  ]
  node [
    id 81
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 82
    label "informatyka"
  ]
  node [
    id 83
    label "odinstalowywa&#263;"
  ]
  node [
    id 84
    label "furkacja"
  ]
  node [
    id 85
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 86
    label "ogranicznik_referencyjny"
  ]
  node [
    id 87
    label "oprogramowanie"
  ]
  node [
    id 88
    label "blok"
  ]
  node [
    id 89
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 90
    label "prezentowa&#263;"
  ]
  node [
    id 91
    label "emitowa&#263;"
  ]
  node [
    id 92
    label "kana&#322;"
  ]
  node [
    id 93
    label "sekcja_krytyczna"
  ]
  node [
    id 94
    label "pirat"
  ]
  node [
    id 95
    label "folder"
  ]
  node [
    id 96
    label "zaprezentowa&#263;"
  ]
  node [
    id 97
    label "course_of_study"
  ]
  node [
    id 98
    label "punkt"
  ]
  node [
    id 99
    label "zainstalowa&#263;"
  ]
  node [
    id 100
    label "emitowanie"
  ]
  node [
    id 101
    label "teleferie"
  ]
  node [
    id 102
    label "podstawa"
  ]
  node [
    id 103
    label "deklaracja"
  ]
  node [
    id 104
    label "instrukcja"
  ]
  node [
    id 105
    label "zainstalowanie"
  ]
  node [
    id 106
    label "zaprezentowanie"
  ]
  node [
    id 107
    label "instalowa&#263;"
  ]
  node [
    id 108
    label "oferta"
  ]
  node [
    id 109
    label "odinstalowanie"
  ]
  node [
    id 110
    label "odinstalowywanie"
  ]
  node [
    id 111
    label "okno"
  ]
  node [
    id 112
    label "ram&#243;wka"
  ]
  node [
    id 113
    label "tryb"
  ]
  node [
    id 114
    label "menu"
  ]
  node [
    id 115
    label "podprogram"
  ]
  node [
    id 116
    label "instalowanie"
  ]
  node [
    id 117
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 118
    label "booklet"
  ]
  node [
    id 119
    label "struktura_organizacyjna"
  ]
  node [
    id 120
    label "wytw&#243;r"
  ]
  node [
    id 121
    label "interfejs"
  ]
  node [
    id 122
    label "prezentowanie"
  ]
  node [
    id 123
    label "wydawnictwo"
  ]
  node [
    id 124
    label "druk_ulotny"
  ]
  node [
    id 125
    label "zasi&#261;g"
  ]
  node [
    id 126
    label "distribution"
  ]
  node [
    id 127
    label "zakres"
  ]
  node [
    id 128
    label "rozmiar"
  ]
  node [
    id 129
    label "bridge"
  ]
  node [
    id 130
    label "izochronizm"
  ]
  node [
    id 131
    label "strategia"
  ]
  node [
    id 132
    label "background"
  ]
  node [
    id 133
    label "przedmiot"
  ]
  node [
    id 134
    label "punkt_odniesienia"
  ]
  node [
    id 135
    label "zasadzenie"
  ]
  node [
    id 136
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 137
    label "&#347;ciana"
  ]
  node [
    id 138
    label "podstawowy"
  ]
  node [
    id 139
    label "dzieci&#281;ctwo"
  ]
  node [
    id 140
    label "d&#243;&#322;"
  ]
  node [
    id 141
    label "documentation"
  ]
  node [
    id 142
    label "bok"
  ]
  node [
    id 143
    label "pomys&#322;"
  ]
  node [
    id 144
    label "zasadzi&#263;"
  ]
  node [
    id 145
    label "column"
  ]
  node [
    id 146
    label "pot&#281;ga"
  ]
  node [
    id 147
    label "rezultat"
  ]
  node [
    id 148
    label "p&#322;&#243;d"
  ]
  node [
    id 149
    label "work"
  ]
  node [
    id 150
    label "spos&#243;b"
  ]
  node [
    id 151
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 152
    label "modalno&#347;&#263;"
  ]
  node [
    id 153
    label "cecha"
  ]
  node [
    id 154
    label "z&#261;b"
  ]
  node [
    id 155
    label "koniugacja"
  ]
  node [
    id 156
    label "kategoria_gramatyczna"
  ]
  node [
    id 157
    label "funkcjonowa&#263;"
  ]
  node [
    id 158
    label "skala"
  ]
  node [
    id 159
    label "ko&#322;o"
  ]
  node [
    id 160
    label "propozycja"
  ]
  node [
    id 161
    label "offer"
  ]
  node [
    id 162
    label "formularz"
  ]
  node [
    id 163
    label "announcement"
  ]
  node [
    id 164
    label "o&#347;wiadczenie"
  ]
  node [
    id 165
    label "akt"
  ]
  node [
    id 166
    label "digest"
  ]
  node [
    id 167
    label "obietnica"
  ]
  node [
    id 168
    label "dokument"
  ]
  node [
    id 169
    label "konstrukcja"
  ]
  node [
    id 170
    label "o&#347;wiadczyny"
  ]
  node [
    id 171
    label "statement"
  ]
  node [
    id 172
    label "struktura_anatomiczna"
  ]
  node [
    id 173
    label "gara&#380;"
  ]
  node [
    id 174
    label "syfon"
  ]
  node [
    id 175
    label "przew&#243;d"
  ]
  node [
    id 176
    label "chody"
  ]
  node [
    id 177
    label "urz&#261;dzenie"
  ]
  node [
    id 178
    label "ciek"
  ]
  node [
    id 179
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 180
    label "miejsce"
  ]
  node [
    id 181
    label "grodzisko"
  ]
  node [
    id 182
    label "szaniec"
  ]
  node [
    id 183
    label "warsztat"
  ]
  node [
    id 184
    label "zrzutowy"
  ]
  node [
    id 185
    label "kanalizacja"
  ]
  node [
    id 186
    label "budowa"
  ]
  node [
    id 187
    label "teatr"
  ]
  node [
    id 188
    label "klarownia"
  ]
  node [
    id 189
    label "pit"
  ]
  node [
    id 190
    label "piaskownik"
  ]
  node [
    id 191
    label "bystrza"
  ]
  node [
    id 192
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 193
    label "topologia_magistrali"
  ]
  node [
    id 194
    label "tarapaty"
  ]
  node [
    id 195
    label "odwa&#322;"
  ]
  node [
    id 196
    label "odk&#322;ad"
  ]
  node [
    id 197
    label "catalog"
  ]
  node [
    id 198
    label "figurowa&#263;"
  ]
  node [
    id 199
    label "tekst"
  ]
  node [
    id 200
    label "pozycja"
  ]
  node [
    id 201
    label "zbi&#243;r"
  ]
  node [
    id 202
    label "wyliczanka"
  ]
  node [
    id 203
    label "sumariusz"
  ]
  node [
    id 204
    label "stock"
  ]
  node [
    id 205
    label "book"
  ]
  node [
    id 206
    label "czynno&#347;&#263;"
  ]
  node [
    id 207
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 208
    label "usuwanie"
  ]
  node [
    id 209
    label "usuni&#281;cie"
  ]
  node [
    id 210
    label "zamek"
  ]
  node [
    id 211
    label "infa"
  ]
  node [
    id 212
    label "gramatyka_formalna"
  ]
  node [
    id 213
    label "baza_danych"
  ]
  node [
    id 214
    label "HP"
  ]
  node [
    id 215
    label "kryptologia"
  ]
  node [
    id 216
    label "przetwarzanie_informacji"
  ]
  node [
    id 217
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 218
    label "dost&#281;p"
  ]
  node [
    id 219
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 220
    label "dziedzina_informatyki"
  ]
  node [
    id 221
    label "kierunek"
  ]
  node [
    id 222
    label "sztuczna_inteligencja"
  ]
  node [
    id 223
    label "artefakt"
  ]
  node [
    id 224
    label "komputer"
  ]
  node [
    id 225
    label "zrobi&#263;"
  ]
  node [
    id 226
    label "install"
  ]
  node [
    id 227
    label "umie&#347;ci&#263;"
  ]
  node [
    id 228
    label "dostosowa&#263;"
  ]
  node [
    id 229
    label "dostosowywa&#263;"
  ]
  node [
    id 230
    label "robi&#263;"
  ]
  node [
    id 231
    label "supply"
  ]
  node [
    id 232
    label "fit"
  ]
  node [
    id 233
    label "accommodate"
  ]
  node [
    id 234
    label "umieszcza&#263;"
  ]
  node [
    id 235
    label "usuwa&#263;"
  ]
  node [
    id 236
    label "zdolno&#347;&#263;"
  ]
  node [
    id 237
    label "usun&#261;&#263;"
  ]
  node [
    id 238
    label "layout"
  ]
  node [
    id 239
    label "installation"
  ]
  node [
    id 240
    label "proposition"
  ]
  node [
    id 241
    label "pozak&#322;adanie"
  ]
  node [
    id 242
    label "dostosowanie"
  ]
  node [
    id 243
    label "umieszczenie"
  ]
  node [
    id 244
    label "zrobienie"
  ]
  node [
    id 245
    label "parapet"
  ]
  node [
    id 246
    label "okiennica"
  ]
  node [
    id 247
    label "lufcik"
  ]
  node [
    id 248
    label "futryna"
  ]
  node [
    id 249
    label "prze&#347;wit"
  ]
  node [
    id 250
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 251
    label "inspekt"
  ]
  node [
    id 252
    label "szyba"
  ]
  node [
    id 253
    label "nora"
  ]
  node [
    id 254
    label "pulpit"
  ]
  node [
    id 255
    label "nadokiennik"
  ]
  node [
    id 256
    label "skrzyd&#322;o"
  ]
  node [
    id 257
    label "transenna"
  ]
  node [
    id 258
    label "kwatera_okienna"
  ]
  node [
    id 259
    label "otw&#243;r"
  ]
  node [
    id 260
    label "menad&#380;er_okien"
  ]
  node [
    id 261
    label "casement"
  ]
  node [
    id 262
    label "wmontowanie"
  ]
  node [
    id 263
    label "wmontowywanie"
  ]
  node [
    id 264
    label "dostosowywanie"
  ]
  node [
    id 265
    label "umieszczanie"
  ]
  node [
    id 266
    label "robienie"
  ]
  node [
    id 267
    label "fitting"
  ]
  node [
    id 268
    label "collection"
  ]
  node [
    id 269
    label "uprzedzi&#263;"
  ]
  node [
    id 270
    label "testify"
  ]
  node [
    id 271
    label "pokaza&#263;"
  ]
  node [
    id 272
    label "zapozna&#263;"
  ]
  node [
    id 273
    label "attest"
  ]
  node [
    id 274
    label "typify"
  ]
  node [
    id 275
    label "represent"
  ]
  node [
    id 276
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 277
    label "przedstawi&#263;"
  ]
  node [
    id 278
    label "present"
  ]
  node [
    id 279
    label "wyra&#380;anie"
  ]
  node [
    id 280
    label "presentation"
  ]
  node [
    id 281
    label "granie"
  ]
  node [
    id 282
    label "zapoznawanie"
  ]
  node [
    id 283
    label "demonstrowanie"
  ]
  node [
    id 284
    label "display"
  ]
  node [
    id 285
    label "representation"
  ]
  node [
    id 286
    label "uprzedzanie"
  ]
  node [
    id 287
    label "przedstawianie"
  ]
  node [
    id 288
    label "rozb&#243;jnik"
  ]
  node [
    id 289
    label "kopiowa&#263;"
  ]
  node [
    id 290
    label "podr&#243;bka"
  ]
  node [
    id 291
    label "&#380;agl&#243;wka"
  ]
  node [
    id 292
    label "rum"
  ]
  node [
    id 293
    label "przest&#281;pca"
  ]
  node [
    id 294
    label "postrzeleniec"
  ]
  node [
    id 295
    label "kieruj&#261;cy"
  ]
  node [
    id 296
    label "uprzedzenie"
  ]
  node [
    id 297
    label "przedstawienie"
  ]
  node [
    id 298
    label "exhibit"
  ]
  node [
    id 299
    label "pokazanie"
  ]
  node [
    id 300
    label "zapoznanie_si&#281;"
  ]
  node [
    id 301
    label "wyst&#261;pienie"
  ]
  node [
    id 302
    label "zapoznanie"
  ]
  node [
    id 303
    label "uprzedza&#263;"
  ]
  node [
    id 304
    label "gra&#263;"
  ]
  node [
    id 305
    label "przedstawia&#263;"
  ]
  node [
    id 306
    label "wyra&#380;a&#263;"
  ]
  node [
    id 307
    label "zapoznawa&#263;"
  ]
  node [
    id 308
    label "tembr"
  ]
  node [
    id 309
    label "wprowadzanie"
  ]
  node [
    id 310
    label "wydzielanie"
  ]
  node [
    id 311
    label "wydzielenie"
  ]
  node [
    id 312
    label "wysy&#322;anie"
  ]
  node [
    id 313
    label "wprowadzenie"
  ]
  node [
    id 314
    label "energia"
  ]
  node [
    id 315
    label "wydobywanie"
  ]
  node [
    id 316
    label "issue"
  ]
  node [
    id 317
    label "nadanie"
  ]
  node [
    id 318
    label "wydobycie"
  ]
  node [
    id 319
    label "emission"
  ]
  node [
    id 320
    label "wys&#322;anie"
  ]
  node [
    id 321
    label "rynek"
  ]
  node [
    id 322
    label "nadawanie"
  ]
  node [
    id 323
    label "wys&#322;a&#263;"
  ]
  node [
    id 324
    label "emit"
  ]
  node [
    id 325
    label "nadawa&#263;"
  ]
  node [
    id 326
    label "air"
  ]
  node [
    id 327
    label "wydoby&#263;"
  ]
  node [
    id 328
    label "nada&#263;"
  ]
  node [
    id 329
    label "wprowadzi&#263;"
  ]
  node [
    id 330
    label "wysy&#322;a&#263;"
  ]
  node [
    id 331
    label "wprowadza&#263;"
  ]
  node [
    id 332
    label "wydobywa&#263;"
  ]
  node [
    id 333
    label "wydziela&#263;"
  ]
  node [
    id 334
    label "wydzieli&#263;"
  ]
  node [
    id 335
    label "instruktarz"
  ]
  node [
    id 336
    label "wskaz&#243;wka"
  ]
  node [
    id 337
    label "ulotka"
  ]
  node [
    id 338
    label "routine"
  ]
  node [
    id 339
    label "proceduralnie"
  ]
  node [
    id 340
    label "danie"
  ]
  node [
    id 341
    label "cennik"
  ]
  node [
    id 342
    label "chart"
  ]
  node [
    id 343
    label "restauracja"
  ]
  node [
    id 344
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 345
    label "zestaw"
  ]
  node [
    id 346
    label "karta"
  ]
  node [
    id 347
    label "skorupa_ziemska"
  ]
  node [
    id 348
    label "budynek"
  ]
  node [
    id 349
    label "przeszkoda"
  ]
  node [
    id 350
    label "bry&#322;a"
  ]
  node [
    id 351
    label "j&#261;kanie"
  ]
  node [
    id 352
    label "square"
  ]
  node [
    id 353
    label "bloking"
  ]
  node [
    id 354
    label "kontynent"
  ]
  node [
    id 355
    label "ok&#322;adka"
  ]
  node [
    id 356
    label "kr&#261;g"
  ]
  node [
    id 357
    label "start"
  ]
  node [
    id 358
    label "blockage"
  ]
  node [
    id 359
    label "blokowisko"
  ]
  node [
    id 360
    label "artyku&#322;"
  ]
  node [
    id 361
    label "blokada"
  ]
  node [
    id 362
    label "whole"
  ]
  node [
    id 363
    label "stok_kontynentalny"
  ]
  node [
    id 364
    label "bajt"
  ]
  node [
    id 365
    label "barak"
  ]
  node [
    id 366
    label "referat"
  ]
  node [
    id 367
    label "nastawnia"
  ]
  node [
    id 368
    label "obrona"
  ]
  node [
    id 369
    label "grupa"
  ]
  node [
    id 370
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 371
    label "dom_wielorodzinny"
  ]
  node [
    id 372
    label "zeszyt"
  ]
  node [
    id 373
    label "siatk&#243;wka"
  ]
  node [
    id 374
    label "block"
  ]
  node [
    id 375
    label "bie&#380;nia"
  ]
  node [
    id 376
    label "zesp&#243;&#322;"
  ]
  node [
    id 377
    label "urz&#261;d"
  ]
  node [
    id 378
    label "miejsce_pracy"
  ]
  node [
    id 379
    label "poddzia&#322;"
  ]
  node [
    id 380
    label "bezdro&#380;e"
  ]
  node [
    id 381
    label "insourcing"
  ]
  node [
    id 382
    label "stopie&#324;"
  ]
  node [
    id 383
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 384
    label "competence"
  ]
  node [
    id 385
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 386
    label "sfera"
  ]
  node [
    id 387
    label "jednostka_organizacyjna"
  ]
  node [
    id 388
    label "infliction"
  ]
  node [
    id 389
    label "spowodowanie"
  ]
  node [
    id 390
    label "przygotowanie"
  ]
  node [
    id 391
    label "point"
  ]
  node [
    id 392
    label "poubieranie"
  ]
  node [
    id 393
    label "rozebranie"
  ]
  node [
    id 394
    label "str&#243;j"
  ]
  node [
    id 395
    label "budowla"
  ]
  node [
    id 396
    label "przewidzenie"
  ]
  node [
    id 397
    label "zak&#322;adka"
  ]
  node [
    id 398
    label "twierdzenie"
  ]
  node [
    id 399
    label "przygotowywanie"
  ]
  node [
    id 400
    label "podwini&#281;cie"
  ]
  node [
    id 401
    label "zap&#322;acenie"
  ]
  node [
    id 402
    label "wyko&#324;czenie"
  ]
  node [
    id 403
    label "struktura"
  ]
  node [
    id 404
    label "utworzenie"
  ]
  node [
    id 405
    label "przebranie"
  ]
  node [
    id 406
    label "obleczenie"
  ]
  node [
    id 407
    label "przymierzenie"
  ]
  node [
    id 408
    label "obleczenie_si&#281;"
  ]
  node [
    id 409
    label "przywdzianie"
  ]
  node [
    id 410
    label "przyodzianie"
  ]
  node [
    id 411
    label "pokrycie"
  ]
  node [
    id 412
    label "obiekt_matematyczny"
  ]
  node [
    id 413
    label "stopie&#324;_pisma"
  ]
  node [
    id 414
    label "problemat"
  ]
  node [
    id 415
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 416
    label "obiekt"
  ]
  node [
    id 417
    label "plamka"
  ]
  node [
    id 418
    label "przestrze&#324;"
  ]
  node [
    id 419
    label "mark"
  ]
  node [
    id 420
    label "ust&#281;p"
  ]
  node [
    id 421
    label "po&#322;o&#380;enie"
  ]
  node [
    id 422
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 423
    label "kres"
  ]
  node [
    id 424
    label "plan"
  ]
  node [
    id 425
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 426
    label "chwila"
  ]
  node [
    id 427
    label "podpunkt"
  ]
  node [
    id 428
    label "jednostka"
  ]
  node [
    id 429
    label "sprawa"
  ]
  node [
    id 430
    label "problematyka"
  ]
  node [
    id 431
    label "prosta"
  ]
  node [
    id 432
    label "wojsko"
  ]
  node [
    id 433
    label "zapunktowa&#263;"
  ]
  node [
    id 434
    label "reengineering"
  ]
  node [
    id 435
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 436
    label "okienko"
  ]
  node [
    id 437
    label "scheduling"
  ]
  node [
    id 438
    label "film"
  ]
  node [
    id 439
    label "wideokaseta"
  ]
  node [
    id 440
    label "g&#322;owica_elektromagnetyczna"
  ]
  node [
    id 441
    label "technika"
  ]
  node [
    id 442
    label "odtwarzacz"
  ]
  node [
    id 443
    label "mechanika_precyzyjna"
  ]
  node [
    id 444
    label "technologia"
  ]
  node [
    id 445
    label "fotowoltaika"
  ]
  node [
    id 446
    label "cywilizacja"
  ]
  node [
    id 447
    label "telekomunikacja"
  ]
  node [
    id 448
    label "teletechnika"
  ]
  node [
    id 449
    label "wiedza"
  ]
  node [
    id 450
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 451
    label "engineering"
  ]
  node [
    id 452
    label "sprawno&#347;&#263;"
  ]
  node [
    id 453
    label "b&#322;ona"
  ]
  node [
    id 454
    label "trawiarnia"
  ]
  node [
    id 455
    label "sztuka"
  ]
  node [
    id 456
    label "emulsja_fotograficzna"
  ]
  node [
    id 457
    label "rozbieg&#243;wka"
  ]
  node [
    id 458
    label "sklejarka"
  ]
  node [
    id 459
    label "odczuli&#263;"
  ]
  node [
    id 460
    label "filmoteka"
  ]
  node [
    id 461
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 462
    label "dorobek"
  ]
  node [
    id 463
    label "animatronika"
  ]
  node [
    id 464
    label "napisy"
  ]
  node [
    id 465
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 466
    label "blik"
  ]
  node [
    id 467
    label "odczula&#263;"
  ]
  node [
    id 468
    label "odczulenie"
  ]
  node [
    id 469
    label "photograph"
  ]
  node [
    id 470
    label "scena"
  ]
  node [
    id 471
    label "klatka"
  ]
  node [
    id 472
    label "ta&#347;ma"
  ]
  node [
    id 473
    label "uj&#281;cie"
  ]
  node [
    id 474
    label "anamorfoza"
  ]
  node [
    id 475
    label "czo&#322;&#243;wka"
  ]
  node [
    id 476
    label "odczulanie"
  ]
  node [
    id 477
    label "postprodukcja"
  ]
  node [
    id 478
    label "muza"
  ]
  node [
    id 479
    label "rola"
  ]
  node [
    id 480
    label "ty&#322;&#243;wka"
  ]
  node [
    id 481
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 482
    label "sprz&#281;t_audio"
  ]
  node [
    id 483
    label "wideoteka"
  ]
  node [
    id 484
    label "wypo&#380;yczalnia_wideo"
  ]
  node [
    id 485
    label "kaseta"
  ]
  node [
    id 486
    label "magnetowid"
  ]
  node [
    id 487
    label "plastyk"
  ]
  node [
    id 488
    label "diagram"
  ]
  node [
    id 489
    label "rozk&#322;ad"
  ]
  node [
    id 490
    label "informatyk"
  ]
  node [
    id 491
    label "tworzywo"
  ]
  node [
    id 492
    label "sztuczny"
  ]
  node [
    id 493
    label "przeciwutleniacz"
  ]
  node [
    id 494
    label "artysta"
  ]
  node [
    id 495
    label "nauczyciel"
  ]
  node [
    id 496
    label "plastic"
  ]
  node [
    id 497
    label "dissociation"
  ]
  node [
    id 498
    label "inclination"
  ]
  node [
    id 499
    label "manner"
  ]
  node [
    id 500
    label "czas_p&#243;&#322;trwania"
  ]
  node [
    id 501
    label "zwierzyna"
  ]
  node [
    id 502
    label "katabolizm"
  ]
  node [
    id 503
    label "proces_fizyczny"
  ]
  node [
    id 504
    label "u&#322;o&#380;enie"
  ]
  node [
    id 505
    label "miara_probabilistyczna"
  ]
  node [
    id 506
    label "wyst&#281;powanie"
  ]
  node [
    id 507
    label "reducent"
  ]
  node [
    id 508
    label "uk&#322;ad"
  ]
  node [
    id 509
    label "rozmieszczenie"
  ]
  node [
    id 510
    label "proces_chemiczny"
  ]
  node [
    id 511
    label "reticule"
  ]
  node [
    id 512
    label "kondycja"
  ]
  node [
    id 513
    label "antykataboliczny"
  ]
  node [
    id 514
    label "proces"
  ]
  node [
    id 515
    label "dissolution"
  ]
  node [
    id 516
    label "specjalista"
  ]
  node [
    id 517
    label "plot"
  ]
  node [
    id 518
    label "wykres"
  ]
  node [
    id 519
    label "rozmiar&#243;wka"
  ]
  node [
    id 520
    label "rubryka"
  ]
  node [
    id 521
    label "szachownica_Punnetta"
  ]
  node [
    id 522
    label "analizowa&#263;"
  ]
  node [
    id 523
    label "szacowa&#263;"
  ]
  node [
    id 524
    label "rozpatrywa&#263;"
  ]
  node [
    id 525
    label "bada&#263;"
  ]
  node [
    id 526
    label "consider"
  ]
  node [
    id 527
    label "poddawa&#263;"
  ]
  node [
    id 528
    label "badany"
  ]
  node [
    id 529
    label "gauge"
  ]
  node [
    id 530
    label "okre&#347;la&#263;"
  ]
  node [
    id 531
    label "liczy&#263;"
  ]
  node [
    id 532
    label "spaczony"
  ]
  node [
    id 533
    label "barbarzy&#324;ski"
  ]
  node [
    id 534
    label "zbrodniczo"
  ]
  node [
    id 535
    label "nieokrzesany"
  ]
  node [
    id 536
    label "niestosowny"
  ]
  node [
    id 537
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 538
    label "okrutny"
  ]
  node [
    id 539
    label "barbarzy&#324;sko"
  ]
  node [
    id 540
    label "po_barbarzy&#324;sku"
  ]
  node [
    id 541
    label "prymitywny"
  ]
  node [
    id 542
    label "nieprawid&#322;owy"
  ]
  node [
    id 543
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 544
    label "rasistowski"
  ]
  node [
    id 545
    label "antykomunistyczny"
  ]
  node [
    id 546
    label "po_nazistowsku"
  ]
  node [
    id 547
    label "faszystowski"
  ]
  node [
    id 548
    label "hitlerowsko"
  ]
  node [
    id 549
    label "anty&#380;ydowski"
  ]
  node [
    id 550
    label "totalitarny"
  ]
  node [
    id 551
    label "typowy"
  ]
  node [
    id 552
    label "po_faszystowsku"
  ]
  node [
    id 553
    label "faszystowsko"
  ]
  node [
    id 554
    label "rasistowsko"
  ]
  node [
    id 555
    label "nietolerancyjny"
  ]
  node [
    id 556
    label "przeciwny"
  ]
  node [
    id 557
    label "anty&#380;ydowsko"
  ]
  node [
    id 558
    label "antytotalitarny"
  ]
  node [
    id 559
    label "antykomunistycznie"
  ]
  node [
    id 560
    label "typowo"
  ]
  node [
    id 561
    label "hitlerowski"
  ]
  node [
    id 562
    label "charakterystycznie"
  ]
  node [
    id 563
    label "przybud&#243;wka"
  ]
  node [
    id 564
    label "organization"
  ]
  node [
    id 565
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 566
    label "od&#322;am"
  ]
  node [
    id 567
    label "TOPR"
  ]
  node [
    id 568
    label "komitet_koordynacyjny"
  ]
  node [
    id 569
    label "przedstawicielstwo"
  ]
  node [
    id 570
    label "ZMP"
  ]
  node [
    id 571
    label "Cepelia"
  ]
  node [
    id 572
    label "GOPR"
  ]
  node [
    id 573
    label "endecki"
  ]
  node [
    id 574
    label "ZBoWiD"
  ]
  node [
    id 575
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 576
    label "podmiot"
  ]
  node [
    id 577
    label "boj&#243;wka"
  ]
  node [
    id 578
    label "ZOMO"
  ]
  node [
    id 579
    label "centrala"
  ]
  node [
    id 580
    label "o&#347;"
  ]
  node [
    id 581
    label "zachowanie"
  ]
  node [
    id 582
    label "podsystem"
  ]
  node [
    id 583
    label "systemat"
  ]
  node [
    id 584
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 585
    label "system"
  ]
  node [
    id 586
    label "rozprz&#261;c"
  ]
  node [
    id 587
    label "cybernetyk"
  ]
  node [
    id 588
    label "mechanika"
  ]
  node [
    id 589
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 590
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 591
    label "konstelacja"
  ]
  node [
    id 592
    label "usenet"
  ]
  node [
    id 593
    label "sk&#322;ad"
  ]
  node [
    id 594
    label "group"
  ]
  node [
    id 595
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 596
    label "The_Beatles"
  ]
  node [
    id 597
    label "odm&#322;odzenie"
  ]
  node [
    id 598
    label "ro&#347;lina"
  ]
  node [
    id 599
    label "odm&#322;adzanie"
  ]
  node [
    id 600
    label "Depeche_Mode"
  ]
  node [
    id 601
    label "odm&#322;adza&#263;"
  ]
  node [
    id 602
    label "&#346;wietliki"
  ]
  node [
    id 603
    label "zespolik"
  ]
  node [
    id 604
    label "Mazowsze"
  ]
  node [
    id 605
    label "schorzenie"
  ]
  node [
    id 606
    label "skupienie"
  ]
  node [
    id 607
    label "batch"
  ]
  node [
    id 608
    label "zabudowania"
  ]
  node [
    id 609
    label "siedziba"
  ]
  node [
    id 610
    label "filia"
  ]
  node [
    id 611
    label "bank"
  ]
  node [
    id 612
    label "agencja"
  ]
  node [
    id 613
    label "ajencja"
  ]
  node [
    id 614
    label "struktura_geologiczna"
  ]
  node [
    id 615
    label "fragment"
  ]
  node [
    id 616
    label "kawa&#322;"
  ]
  node [
    id 617
    label "section"
  ]
  node [
    id 618
    label "b&#281;ben_wielki"
  ]
  node [
    id 619
    label "zarz&#261;d"
  ]
  node [
    id 620
    label "o&#347;rodek"
  ]
  node [
    id 621
    label "Bruksela"
  ]
  node [
    id 622
    label "w&#322;adza"
  ]
  node [
    id 623
    label "stopa"
  ]
  node [
    id 624
    label "administration"
  ]
  node [
    id 625
    label "ratownictwo"
  ]
  node [
    id 626
    label "milicja_obywatelska"
  ]
  node [
    id 627
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 628
    label "byt"
  ]
  node [
    id 629
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 630
    label "nauka_prawa"
  ]
  node [
    id 631
    label "prawo"
  ]
  node [
    id 632
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 633
    label "osobowo&#347;&#263;"
  ]
  node [
    id 634
    label "min&#261;&#263;"
  ]
  node [
    id 635
    label "20"
  ]
  node [
    id 636
    label "TVP"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 1
    target 393
  ]
  edge [
    source 1
    target 394
  ]
  edge [
    source 1
    target 395
  ]
  edge [
    source 1
    target 396
  ]
  edge [
    source 1
    target 397
  ]
  edge [
    source 1
    target 398
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 400
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 1
    target 402
  ]
  edge [
    source 1
    target 403
  ]
  edge [
    source 1
    target 404
  ]
  edge [
    source 1
    target 405
  ]
  edge [
    source 1
    target 406
  ]
  edge [
    source 1
    target 407
  ]
  edge [
    source 1
    target 408
  ]
  edge [
    source 1
    target 409
  ]
  edge [
    source 1
    target 410
  ]
  edge [
    source 1
    target 411
  ]
  edge [
    source 1
    target 412
  ]
  edge [
    source 1
    target 413
  ]
  edge [
    source 1
    target 414
  ]
  edge [
    source 1
    target 415
  ]
  edge [
    source 1
    target 416
  ]
  edge [
    source 1
    target 417
  ]
  edge [
    source 1
    target 418
  ]
  edge [
    source 1
    target 419
  ]
  edge [
    source 1
    target 420
  ]
  edge [
    source 1
    target 421
  ]
  edge [
    source 1
    target 422
  ]
  edge [
    source 1
    target 423
  ]
  edge [
    source 1
    target 424
  ]
  edge [
    source 1
    target 425
  ]
  edge [
    source 1
    target 426
  ]
  edge [
    source 1
    target 427
  ]
  edge [
    source 1
    target 428
  ]
  edge [
    source 1
    target 429
  ]
  edge [
    source 1
    target 430
  ]
  edge [
    source 1
    target 431
  ]
  edge [
    source 1
    target 432
  ]
  edge [
    source 1
    target 433
  ]
  edge [
    source 1
    target 434
  ]
  edge [
    source 1
    target 435
  ]
  edge [
    source 1
    target 436
  ]
  edge [
    source 1
    target 437
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 636
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 201
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 634
    target 635
  ]
]
