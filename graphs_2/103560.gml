graph [
  node [
    id 0
    label "lena"
    origin "text"
  ]
  node [
    id 1
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nuci&#263;"
    origin "text"
  ]
  node [
    id 3
    label "fragment"
    origin "text"
  ]
  node [
    id 4
    label "oskar"
    origin "text"
  ]
  node [
    id 5
    label "w&#322;&#261;cza&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "chwila"
    origin "text"
  ]
  node [
    id 8
    label "bankrupt"
  ]
  node [
    id 9
    label "open"
  ]
  node [
    id 10
    label "post&#281;powa&#263;"
  ]
  node [
    id 11
    label "odejmowa&#263;"
  ]
  node [
    id 12
    label "set_about"
  ]
  node [
    id 13
    label "begin"
  ]
  node [
    id 14
    label "mie&#263;_miejsce"
  ]
  node [
    id 15
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 16
    label "take"
  ]
  node [
    id 17
    label "ujemny"
  ]
  node [
    id 18
    label "abstract"
  ]
  node [
    id 19
    label "liczy&#263;"
  ]
  node [
    id 20
    label "zabiera&#263;"
  ]
  node [
    id 21
    label "oddziela&#263;"
  ]
  node [
    id 22
    label "oddala&#263;"
  ]
  node [
    id 23
    label "reduce"
  ]
  node [
    id 24
    label "robi&#263;"
  ]
  node [
    id 25
    label "przybiera&#263;"
  ]
  node [
    id 26
    label "act"
  ]
  node [
    id 27
    label "i&#347;&#263;"
  ]
  node [
    id 28
    label "go"
  ]
  node [
    id 29
    label "use"
  ]
  node [
    id 30
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 31
    label "gaworzy&#263;"
  ]
  node [
    id 32
    label "chant"
  ]
  node [
    id 33
    label "hum"
  ]
  node [
    id 34
    label "melodia"
  ]
  node [
    id 35
    label "wykonywa&#263;"
  ]
  node [
    id 36
    label "piosenka"
  ]
  node [
    id 37
    label "chatter"
  ]
  node [
    id 38
    label "m&#243;wi&#263;"
  ]
  node [
    id 39
    label "rozmawia&#263;"
  ]
  node [
    id 40
    label "niemowl&#281;"
  ]
  node [
    id 41
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 42
    label "muzyka"
  ]
  node [
    id 43
    label "praca"
  ]
  node [
    id 44
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 45
    label "wytwarza&#263;"
  ]
  node [
    id 46
    label "rola"
  ]
  node [
    id 47
    label "work"
  ]
  node [
    id 48
    label "create"
  ]
  node [
    id 49
    label "kras_wie&#380;owy"
  ]
  node [
    id 50
    label "ostaniec"
  ]
  node [
    id 51
    label "zanucenie"
  ]
  node [
    id 52
    label "tekst"
  ]
  node [
    id 53
    label "piosnka"
  ]
  node [
    id 54
    label "zanuci&#263;"
  ]
  node [
    id 55
    label "utw&#243;r"
  ]
  node [
    id 56
    label "zwrotka"
  ]
  node [
    id 57
    label "nucenie"
  ]
  node [
    id 58
    label "istota"
  ]
  node [
    id 59
    label "brzmienie"
  ]
  node [
    id 60
    label "zakosztowa&#263;"
  ]
  node [
    id 61
    label "zajawka"
  ]
  node [
    id 62
    label "inclination"
  ]
  node [
    id 63
    label "zjawisko"
  ]
  node [
    id 64
    label "nuta"
  ]
  node [
    id 65
    label "emocja"
  ]
  node [
    id 66
    label "melika"
  ]
  node [
    id 67
    label "oskoma"
  ]
  node [
    id 68
    label "taste"
  ]
  node [
    id 69
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 70
    label "komunikat"
  ]
  node [
    id 71
    label "part"
  ]
  node [
    id 72
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 73
    label "tre&#347;&#263;"
  ]
  node [
    id 74
    label "obrazowanie"
  ]
  node [
    id 75
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 76
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 77
    label "element_anatomiczny"
  ]
  node [
    id 78
    label "organ"
  ]
  node [
    id 79
    label "Rzym_Zachodni"
  ]
  node [
    id 80
    label "Rzym_Wschodni"
  ]
  node [
    id 81
    label "element"
  ]
  node [
    id 82
    label "ilo&#347;&#263;"
  ]
  node [
    id 83
    label "whole"
  ]
  node [
    id 84
    label "urz&#261;dzenie"
  ]
  node [
    id 85
    label "nastawia&#263;"
  ]
  node [
    id 86
    label "get_in_touch"
  ]
  node [
    id 87
    label "uruchamia&#263;"
  ]
  node [
    id 88
    label "connect"
  ]
  node [
    id 89
    label "involve"
  ]
  node [
    id 90
    label "odkr&#281;ca&#263;_kurek"
  ]
  node [
    id 91
    label "dokoptowywa&#263;"
  ]
  node [
    id 92
    label "ogl&#261;da&#263;"
  ]
  node [
    id 93
    label "umieszcza&#263;"
  ]
  node [
    id 94
    label "kapita&#322;"
  ]
  node [
    id 95
    label "zmienia&#263;"
  ]
  node [
    id 96
    label "okre&#347;la&#263;"
  ]
  node [
    id 97
    label "wpiernicza&#263;"
  ]
  node [
    id 98
    label "plasowa&#263;"
  ]
  node [
    id 99
    label "powodowa&#263;"
  ]
  node [
    id 100
    label "pomieszcza&#263;"
  ]
  node [
    id 101
    label "umie&#347;ci&#263;"
  ]
  node [
    id 102
    label "accommodate"
  ]
  node [
    id 103
    label "venture"
  ]
  node [
    id 104
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 105
    label "styka&#263;_si&#281;"
  ]
  node [
    id 106
    label "notice"
  ]
  node [
    id 107
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 108
    label "marshal"
  ]
  node [
    id 109
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 110
    label "powyznacza&#263;"
  ]
  node [
    id 111
    label "pobudowa&#263;"
  ]
  node [
    id 112
    label "poumieszcza&#263;"
  ]
  node [
    id 113
    label "set"
  ]
  node [
    id 114
    label "wyznacza&#263;"
  ]
  node [
    id 115
    label "ustawia&#263;"
  ]
  node [
    id 116
    label "kierowa&#263;"
  ]
  node [
    id 117
    label "deliver"
  ]
  node [
    id 118
    label "z&#322;amanie"
  ]
  node [
    id 119
    label "sk&#322;ada&#263;"
  ]
  node [
    id 120
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 121
    label "poprawia&#263;"
  ]
  node [
    id 122
    label "indicate"
  ]
  node [
    id 123
    label "narobi&#263;"
  ]
  node [
    id 124
    label "predispose"
  ]
  node [
    id 125
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 126
    label "dokooptowywa&#263;"
  ]
  node [
    id 127
    label "time"
  ]
  node [
    id 128
    label "czas"
  ]
  node [
    id 129
    label "chronometria"
  ]
  node [
    id 130
    label "odczyt"
  ]
  node [
    id 131
    label "laba"
  ]
  node [
    id 132
    label "czasoprzestrze&#324;"
  ]
  node [
    id 133
    label "time_period"
  ]
  node [
    id 134
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 135
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 136
    label "Zeitgeist"
  ]
  node [
    id 137
    label "pochodzenie"
  ]
  node [
    id 138
    label "przep&#322;ywanie"
  ]
  node [
    id 139
    label "schy&#322;ek"
  ]
  node [
    id 140
    label "czwarty_wymiar"
  ]
  node [
    id 141
    label "kategoria_gramatyczna"
  ]
  node [
    id 142
    label "poprzedzi&#263;"
  ]
  node [
    id 143
    label "pogoda"
  ]
  node [
    id 144
    label "czasokres"
  ]
  node [
    id 145
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 146
    label "poprzedzenie"
  ]
  node [
    id 147
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 148
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 149
    label "dzieje"
  ]
  node [
    id 150
    label "zegar"
  ]
  node [
    id 151
    label "koniugacja"
  ]
  node [
    id 152
    label "trawi&#263;"
  ]
  node [
    id 153
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 154
    label "poprzedza&#263;"
  ]
  node [
    id 155
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 156
    label "trawienie"
  ]
  node [
    id 157
    label "rachuba_czasu"
  ]
  node [
    id 158
    label "poprzedzanie"
  ]
  node [
    id 159
    label "okres_czasu"
  ]
  node [
    id 160
    label "period"
  ]
  node [
    id 161
    label "odwlekanie_si&#281;"
  ]
  node [
    id 162
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 163
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 164
    label "pochodzi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
]
