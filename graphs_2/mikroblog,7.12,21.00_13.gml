graph [
  node [
    id 0
    label "nikt"
    origin "text"
  ]
  node [
    id 1
    label "od&#322;&#261;cza&#263;"
    origin "text"
  ]
  node [
    id 2
    label "internet"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "rodzi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "fake"
    origin "text"
  ]
  node [
    id 6
    label "news"
    origin "text"
  ]
  node [
    id 7
    label "miernota"
  ]
  node [
    id 8
    label "ciura"
  ]
  node [
    id 9
    label "jako&#347;&#263;"
  ]
  node [
    id 10
    label "cz&#322;owiek"
  ]
  node [
    id 11
    label "podrz&#281;dno&#347;&#263;"
  ]
  node [
    id 12
    label "tandetno&#347;&#263;"
  ]
  node [
    id 13
    label "nieporz&#261;dno&#347;&#263;"
  ]
  node [
    id 14
    label "chor&#261;&#380;y"
  ]
  node [
    id 15
    label "zero"
  ]
  node [
    id 16
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 17
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 18
    label "pacjent"
  ]
  node [
    id 19
    label "take"
  ]
  node [
    id 20
    label "przerywa&#263;"
  ]
  node [
    id 21
    label "odcina&#263;"
  ]
  node [
    id 22
    label "abstract"
  ]
  node [
    id 23
    label "oddziela&#263;"
  ]
  node [
    id 24
    label "challenge"
  ]
  node [
    id 25
    label "dzieli&#263;"
  ]
  node [
    id 26
    label "odosobnia&#263;"
  ]
  node [
    id 27
    label "transgress"
  ]
  node [
    id 28
    label "przeszkadza&#263;"
  ]
  node [
    id 29
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 30
    label "rozrywa&#263;"
  ]
  node [
    id 31
    label "suspend"
  ]
  node [
    id 32
    label "wstrzymywa&#263;"
  ]
  node [
    id 33
    label "kultywar"
  ]
  node [
    id 34
    label "przerzedza&#263;"
  ]
  node [
    id 35
    label "przestawa&#263;"
  ]
  node [
    id 36
    label "dziurawi&#263;"
  ]
  node [
    id 37
    label "przerwanie"
  ]
  node [
    id 38
    label "strive"
  ]
  node [
    id 39
    label "urywa&#263;"
  ]
  node [
    id 40
    label "przerywanie"
  ]
  node [
    id 41
    label "przerwa&#263;"
  ]
  node [
    id 42
    label "przecina&#263;"
  ]
  node [
    id 43
    label "zamyka&#263;"
  ]
  node [
    id 44
    label "trim"
  ]
  node [
    id 45
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 46
    label "odcina&#263;_si&#281;"
  ]
  node [
    id 47
    label "kontrola"
  ]
  node [
    id 48
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 49
    label "klient"
  ]
  node [
    id 50
    label "przypadek"
  ]
  node [
    id 51
    label "piel&#281;gniarz"
  ]
  node [
    id 52
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 53
    label "od&#322;&#261;czanie"
  ]
  node [
    id 54
    label "chory"
  ]
  node [
    id 55
    label "od&#322;&#261;czenie"
  ]
  node [
    id 56
    label "szpitalnik"
  ]
  node [
    id 57
    label "provider"
  ]
  node [
    id 58
    label "hipertekst"
  ]
  node [
    id 59
    label "cyberprzestrze&#324;"
  ]
  node [
    id 60
    label "mem"
  ]
  node [
    id 61
    label "grooming"
  ]
  node [
    id 62
    label "gra_sieciowa"
  ]
  node [
    id 63
    label "media"
  ]
  node [
    id 64
    label "biznes_elektroniczny"
  ]
  node [
    id 65
    label "sie&#263;_komputerowa"
  ]
  node [
    id 66
    label "punkt_dost&#281;pu"
  ]
  node [
    id 67
    label "us&#322;uga_internetowa"
  ]
  node [
    id 68
    label "netbook"
  ]
  node [
    id 69
    label "e-hazard"
  ]
  node [
    id 70
    label "podcast"
  ]
  node [
    id 71
    label "strona"
  ]
  node [
    id 72
    label "mass-media"
  ]
  node [
    id 73
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 74
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 75
    label "przekazior"
  ]
  node [
    id 76
    label "uzbrajanie"
  ]
  node [
    id 77
    label "medium"
  ]
  node [
    id 78
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 79
    label "tekst"
  ]
  node [
    id 80
    label "cyberprzest&#281;pczo&#347;&#263;"
  ]
  node [
    id 81
    label "kartka"
  ]
  node [
    id 82
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 83
    label "logowanie"
  ]
  node [
    id 84
    label "plik"
  ]
  node [
    id 85
    label "s&#261;d"
  ]
  node [
    id 86
    label "adres_internetowy"
  ]
  node [
    id 87
    label "linia"
  ]
  node [
    id 88
    label "serwis_internetowy"
  ]
  node [
    id 89
    label "posta&#263;"
  ]
  node [
    id 90
    label "bok"
  ]
  node [
    id 91
    label "skr&#281;canie"
  ]
  node [
    id 92
    label "skr&#281;ca&#263;"
  ]
  node [
    id 93
    label "orientowanie"
  ]
  node [
    id 94
    label "skr&#281;ci&#263;"
  ]
  node [
    id 95
    label "uj&#281;cie"
  ]
  node [
    id 96
    label "zorientowanie"
  ]
  node [
    id 97
    label "ty&#322;"
  ]
  node [
    id 98
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 99
    label "fragment"
  ]
  node [
    id 100
    label "layout"
  ]
  node [
    id 101
    label "obiekt"
  ]
  node [
    id 102
    label "zorientowa&#263;"
  ]
  node [
    id 103
    label "pagina"
  ]
  node [
    id 104
    label "podmiot"
  ]
  node [
    id 105
    label "g&#243;ra"
  ]
  node [
    id 106
    label "orientowa&#263;"
  ]
  node [
    id 107
    label "voice"
  ]
  node [
    id 108
    label "orientacja"
  ]
  node [
    id 109
    label "prz&#243;d"
  ]
  node [
    id 110
    label "powierzchnia"
  ]
  node [
    id 111
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 112
    label "forma"
  ]
  node [
    id 113
    label "skr&#281;cenie"
  ]
  node [
    id 114
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 115
    label "ma&#322;y"
  ]
  node [
    id 116
    label "poj&#281;cie"
  ]
  node [
    id 117
    label "meme"
  ]
  node [
    id 118
    label "wydawnictwo"
  ]
  node [
    id 119
    label "molestowanie_seksualne"
  ]
  node [
    id 120
    label "piel&#281;gnacja"
  ]
  node [
    id 121
    label "zwierz&#281;_domowe"
  ]
  node [
    id 122
    label "hazard"
  ]
  node [
    id 123
    label "dostawca"
  ]
  node [
    id 124
    label "telefonia"
  ]
  node [
    id 125
    label "create"
  ]
  node [
    id 126
    label "plon"
  ]
  node [
    id 127
    label "give"
  ]
  node [
    id 128
    label "wytwarza&#263;"
  ]
  node [
    id 129
    label "robi&#263;"
  ]
  node [
    id 130
    label "return"
  ]
  node [
    id 131
    label "wydawa&#263;"
  ]
  node [
    id 132
    label "metr"
  ]
  node [
    id 133
    label "wyda&#263;"
  ]
  node [
    id 134
    label "rezultat"
  ]
  node [
    id 135
    label "produkcja"
  ]
  node [
    id 136
    label "naturalia"
  ]
  node [
    id 137
    label "message"
  ]
  node [
    id 138
    label "nius"
  ]
  node [
    id 139
    label "nowostka"
  ]
  node [
    id 140
    label "informacja"
  ]
  node [
    id 141
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 142
    label "system"
  ]
  node [
    id 143
    label "doniesienie"
  ]
  node [
    id 144
    label "punkt"
  ]
  node [
    id 145
    label "publikacja"
  ]
  node [
    id 146
    label "wiedza"
  ]
  node [
    id 147
    label "doj&#347;cie"
  ]
  node [
    id 148
    label "obiega&#263;"
  ]
  node [
    id 149
    label "powzi&#281;cie"
  ]
  node [
    id 150
    label "dane"
  ]
  node [
    id 151
    label "obiegni&#281;cie"
  ]
  node [
    id 152
    label "sygna&#322;"
  ]
  node [
    id 153
    label "obieganie"
  ]
  node [
    id 154
    label "powzi&#261;&#263;"
  ]
  node [
    id 155
    label "obiec"
  ]
  node [
    id 156
    label "doj&#347;&#263;"
  ]
  node [
    id 157
    label "do&#322;&#261;czenie"
  ]
  node [
    id 158
    label "naznoszenie"
  ]
  node [
    id 159
    label "zawiadomienie"
  ]
  node [
    id 160
    label "zniesienie"
  ]
  node [
    id 161
    label "zaniesienie"
  ]
  node [
    id 162
    label "announcement"
  ]
  node [
    id 163
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 164
    label "fetch"
  ]
  node [
    id 165
    label "poinformowanie"
  ]
  node [
    id 166
    label "znoszenie"
  ]
  node [
    id 167
    label "nap&#322;ywanie"
  ]
  node [
    id 168
    label "communication"
  ]
  node [
    id 169
    label "signal"
  ]
  node [
    id 170
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 171
    label "znie&#347;&#263;"
  ]
  node [
    id 172
    label "znosi&#263;"
  ]
  node [
    id 173
    label "zarys"
  ]
  node [
    id 174
    label "komunikat"
  ]
  node [
    id 175
    label "depesza_emska"
  ]
  node [
    id 176
    label "j&#261;dro"
  ]
  node [
    id 177
    label "systemik"
  ]
  node [
    id 178
    label "rozprz&#261;c"
  ]
  node [
    id 179
    label "oprogramowanie"
  ]
  node [
    id 180
    label "systemat"
  ]
  node [
    id 181
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 182
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 183
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 184
    label "model"
  ]
  node [
    id 185
    label "struktura"
  ]
  node [
    id 186
    label "usenet"
  ]
  node [
    id 187
    label "zbi&#243;r"
  ]
  node [
    id 188
    label "porz&#261;dek"
  ]
  node [
    id 189
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 190
    label "przyn&#281;ta"
  ]
  node [
    id 191
    label "p&#322;&#243;d"
  ]
  node [
    id 192
    label "net"
  ]
  node [
    id 193
    label "w&#281;dkarstwo"
  ]
  node [
    id 194
    label "eratem"
  ]
  node [
    id 195
    label "oddzia&#322;"
  ]
  node [
    id 196
    label "doktryna"
  ]
  node [
    id 197
    label "pulpit"
  ]
  node [
    id 198
    label "konstelacja"
  ]
  node [
    id 199
    label "jednostka_geologiczna"
  ]
  node [
    id 200
    label "o&#347;"
  ]
  node [
    id 201
    label "podsystem"
  ]
  node [
    id 202
    label "metoda"
  ]
  node [
    id 203
    label "ryba"
  ]
  node [
    id 204
    label "Leopard"
  ]
  node [
    id 205
    label "spos&#243;b"
  ]
  node [
    id 206
    label "Android"
  ]
  node [
    id 207
    label "zachowanie"
  ]
  node [
    id 208
    label "cybernetyk"
  ]
  node [
    id 209
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 210
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 211
    label "method"
  ]
  node [
    id 212
    label "sk&#322;ad"
  ]
  node [
    id 213
    label "podstawa"
  ]
  node [
    id 214
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 215
    label "nowina"
  ]
  node [
    id 216
    label "Daniel"
  ]
  node [
    id 217
    label "Magicalowi"
  ]
  node [
    id 218
    label "rzecznik"
  ]
  node [
    id 219
    label "Orange"
  ]
  node [
    id 220
    label "Wojtek"
  ]
  node [
    id 221
    label "Jabczy&#324;skiego"
  ]
  node [
    id 222
    label "okr&#261;g&#322;y"
  ]
  node [
    id 223
    label "st&#243;&#322;"
  ]
  node [
    id 224
    label "prawy"
  ]
  node [
    id 225
    label "obywatelski"
  ]
  node [
    id 226
    label "deklaracja"
  ]
  node [
    id 227
    label "uczestnik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 216
    target 217
  ]
  edge [
    source 218
    target 219
  ]
  edge [
    source 218
    target 220
  ]
  edge [
    source 218
    target 221
  ]
  edge [
    source 218
    target 222
  ]
  edge [
    source 218
    target 223
  ]
  edge [
    source 218
    target 224
  ]
  edge [
    source 218
    target 225
  ]
  edge [
    source 218
    target 226
  ]
  edge [
    source 218
    target 227
  ]
  edge [
    source 219
    target 220
  ]
  edge [
    source 219
    target 221
  ]
  edge [
    source 219
    target 226
  ]
  edge [
    source 219
    target 227
  ]
  edge [
    source 219
    target 222
  ]
  edge [
    source 219
    target 223
  ]
  edge [
    source 219
    target 224
  ]
  edge [
    source 219
    target 225
  ]
  edge [
    source 220
    target 221
  ]
  edge [
    source 222
    target 223
  ]
  edge [
    source 222
    target 224
  ]
  edge [
    source 222
    target 225
  ]
  edge [
    source 222
    target 226
  ]
  edge [
    source 222
    target 227
  ]
  edge [
    source 223
    target 224
  ]
  edge [
    source 223
    target 225
  ]
  edge [
    source 223
    target 226
  ]
  edge [
    source 223
    target 227
  ]
  edge [
    source 224
    target 225
  ]
  edge [
    source 224
    target 226
  ]
  edge [
    source 224
    target 227
  ]
  edge [
    source 225
    target 226
  ]
  edge [
    source 225
    target 227
  ]
  edge [
    source 226
    target 227
  ]
]
