graph [
  node [
    id 0
    label "op&#322;ata"
    origin "text"
  ]
  node [
    id 1
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 2
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 3
    label "wrzuci&#263;"
    origin "text"
  ]
  node [
    id 4
    label "koszt"
    origin "text"
  ]
  node [
    id 5
    label "nawet"
    origin "text"
  ]
  node [
    id 6
    label "gdy"
    origin "text"
  ]
  node [
    id 7
    label "lokal"
    origin "text"
  ]
  node [
    id 8
    label "wyodr&#281;bni&#263;"
    origin "text"
  ]
  node [
    id 9
    label "specjalny"
    origin "text"
  ]
  node [
    id 10
    label "pomieszczenie"
    origin "text"
  ]
  node [
    id 11
    label "wy&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 12
    label "praca"
    origin "text"
  ]
  node [
    id 13
    label "kwota"
  ]
  node [
    id 14
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 15
    label "transakcja"
  ]
  node [
    id 16
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 17
    label "rozliczenie"
  ]
  node [
    id 18
    label "wynie&#347;&#263;"
  ]
  node [
    id 19
    label "pieni&#261;dze"
  ]
  node [
    id 20
    label "ilo&#347;&#263;"
  ]
  node [
    id 21
    label "limit"
  ]
  node [
    id 22
    label "wynosi&#263;"
  ]
  node [
    id 23
    label "adjustment"
  ]
  node [
    id 24
    label "panowanie"
  ]
  node [
    id 25
    label "przebywanie"
  ]
  node [
    id 26
    label "animation"
  ]
  node [
    id 27
    label "kwadrat"
  ]
  node [
    id 28
    label "stanie"
  ]
  node [
    id 29
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 30
    label "pomieszkanie"
  ]
  node [
    id 31
    label "dom"
  ]
  node [
    id 32
    label "zajmowanie"
  ]
  node [
    id 33
    label "sprawowanie"
  ]
  node [
    id 34
    label "bycie"
  ]
  node [
    id 35
    label "kierowanie"
  ]
  node [
    id 36
    label "w&#322;adca"
  ]
  node [
    id 37
    label "dominowanie"
  ]
  node [
    id 38
    label "przewaga"
  ]
  node [
    id 39
    label "przewa&#380;anie"
  ]
  node [
    id 40
    label "znaczenie"
  ]
  node [
    id 41
    label "laterality"
  ]
  node [
    id 42
    label "control"
  ]
  node [
    id 43
    label "dominance"
  ]
  node [
    id 44
    label "kontrolowanie"
  ]
  node [
    id 45
    label "temper"
  ]
  node [
    id 46
    label "rule"
  ]
  node [
    id 47
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 48
    label "prym"
  ]
  node [
    id 49
    label "w&#322;adza"
  ]
  node [
    id 50
    label "ocieranie_si&#281;"
  ]
  node [
    id 51
    label "otoczenie_si&#281;"
  ]
  node [
    id 52
    label "posiedzenie"
  ]
  node [
    id 53
    label "otarcie_si&#281;"
  ]
  node [
    id 54
    label "atakowanie"
  ]
  node [
    id 55
    label "otaczanie_si&#281;"
  ]
  node [
    id 56
    label "wyj&#347;cie"
  ]
  node [
    id 57
    label "zmierzanie"
  ]
  node [
    id 58
    label "residency"
  ]
  node [
    id 59
    label "sojourn"
  ]
  node [
    id 60
    label "wychodzenie"
  ]
  node [
    id 61
    label "tkwienie"
  ]
  node [
    id 62
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 63
    label "powodowanie"
  ]
  node [
    id 64
    label "lokowanie_si&#281;"
  ]
  node [
    id 65
    label "schorzenie"
  ]
  node [
    id 66
    label "zajmowanie_si&#281;"
  ]
  node [
    id 67
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 68
    label "stosowanie"
  ]
  node [
    id 69
    label "anektowanie"
  ]
  node [
    id 70
    label "ciekawy"
  ]
  node [
    id 71
    label "zabieranie"
  ]
  node [
    id 72
    label "robienie"
  ]
  node [
    id 73
    label "sytuowanie_si&#281;"
  ]
  node [
    id 74
    label "wype&#322;nianie"
  ]
  node [
    id 75
    label "obejmowanie"
  ]
  node [
    id 76
    label "klasyfikacja"
  ]
  node [
    id 77
    label "czynno&#347;&#263;"
  ]
  node [
    id 78
    label "dzianie_si&#281;"
  ]
  node [
    id 79
    label "branie"
  ]
  node [
    id 80
    label "rz&#261;dzenie"
  ]
  node [
    id 81
    label "occupation"
  ]
  node [
    id 82
    label "zadawanie"
  ]
  node [
    id 83
    label "zaj&#281;ty"
  ]
  node [
    id 84
    label "miejsce"
  ]
  node [
    id 85
    label "gastronomia"
  ]
  node [
    id 86
    label "zak&#322;ad"
  ]
  node [
    id 87
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 88
    label "rodzina"
  ]
  node [
    id 89
    label "substancja_mieszkaniowa"
  ]
  node [
    id 90
    label "instytucja"
  ]
  node [
    id 91
    label "siedziba"
  ]
  node [
    id 92
    label "dom_rodzinny"
  ]
  node [
    id 93
    label "budynek"
  ]
  node [
    id 94
    label "grupa"
  ]
  node [
    id 95
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 96
    label "poj&#281;cie"
  ]
  node [
    id 97
    label "stead"
  ]
  node [
    id 98
    label "garderoba"
  ]
  node [
    id 99
    label "wiecha"
  ]
  node [
    id 100
    label "fratria"
  ]
  node [
    id 101
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 102
    label "trwanie"
  ]
  node [
    id 103
    label "ustanie"
  ]
  node [
    id 104
    label "wystanie"
  ]
  node [
    id 105
    label "postanie"
  ]
  node [
    id 106
    label "wystawanie"
  ]
  node [
    id 107
    label "kosztowanie"
  ]
  node [
    id 108
    label "przestanie"
  ]
  node [
    id 109
    label "pot&#281;ga"
  ]
  node [
    id 110
    label "wielok&#261;t_foremny"
  ]
  node [
    id 111
    label "stopie&#324;_pisma"
  ]
  node [
    id 112
    label "prostok&#261;t"
  ]
  node [
    id 113
    label "square"
  ]
  node [
    id 114
    label "romb"
  ]
  node [
    id 115
    label "justunek"
  ]
  node [
    id 116
    label "dzielnica"
  ]
  node [
    id 117
    label "poletko"
  ]
  node [
    id 118
    label "ekologia"
  ]
  node [
    id 119
    label "tango"
  ]
  node [
    id 120
    label "figura_taneczna"
  ]
  node [
    id 121
    label "free"
  ]
  node [
    id 122
    label "umie&#347;ci&#263;"
  ]
  node [
    id 123
    label "insert"
  ]
  node [
    id 124
    label "set"
  ]
  node [
    id 125
    label "put"
  ]
  node [
    id 126
    label "uplasowa&#263;"
  ]
  node [
    id 127
    label "wpierniczy&#263;"
  ]
  node [
    id 128
    label "okre&#347;li&#263;"
  ]
  node [
    id 129
    label "zrobi&#263;"
  ]
  node [
    id 130
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 131
    label "zmieni&#263;"
  ]
  node [
    id 132
    label "umieszcza&#263;"
  ]
  node [
    id 133
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 134
    label "wydatek"
  ]
  node [
    id 135
    label "sumpt"
  ]
  node [
    id 136
    label "nak&#322;ad"
  ]
  node [
    id 137
    label "liczba"
  ]
  node [
    id 138
    label "wych&#243;d"
  ]
  node [
    id 139
    label "warunek_lokalowy"
  ]
  node [
    id 140
    label "plac"
  ]
  node [
    id 141
    label "location"
  ]
  node [
    id 142
    label "uwaga"
  ]
  node [
    id 143
    label "przestrze&#324;"
  ]
  node [
    id 144
    label "status"
  ]
  node [
    id 145
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 146
    label "chwila"
  ]
  node [
    id 147
    label "cia&#322;o"
  ]
  node [
    id 148
    label "cecha"
  ]
  node [
    id 149
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 150
    label "rz&#261;d"
  ]
  node [
    id 151
    label "kuchnia"
  ]
  node [
    id 152
    label "horeca"
  ]
  node [
    id 153
    label "sztuka"
  ]
  node [
    id 154
    label "us&#322;ugi"
  ]
  node [
    id 155
    label "zak&#322;adka"
  ]
  node [
    id 156
    label "jednostka_organizacyjna"
  ]
  node [
    id 157
    label "miejsce_pracy"
  ]
  node [
    id 158
    label "wyko&#324;czenie"
  ]
  node [
    id 159
    label "firma"
  ]
  node [
    id 160
    label "czyn"
  ]
  node [
    id 161
    label "company"
  ]
  node [
    id 162
    label "instytut"
  ]
  node [
    id 163
    label "umowa"
  ]
  node [
    id 164
    label "signalize"
  ]
  node [
    id 165
    label "wyznaczy&#263;"
  ]
  node [
    id 166
    label "wykroi&#263;"
  ]
  node [
    id 167
    label "oddzieli&#263;"
  ]
  node [
    id 168
    label "position"
  ]
  node [
    id 169
    label "aim"
  ]
  node [
    id 170
    label "zaznaczy&#263;"
  ]
  node [
    id 171
    label "sign"
  ]
  node [
    id 172
    label "ustali&#263;"
  ]
  node [
    id 173
    label "wybra&#263;"
  ]
  node [
    id 174
    label "divide"
  ]
  node [
    id 175
    label "detach"
  ]
  node [
    id 176
    label "spowodowa&#263;"
  ]
  node [
    id 177
    label "podzieli&#263;"
  ]
  node [
    id 178
    label "odseparowa&#263;"
  ]
  node [
    id 179
    label "remove"
  ]
  node [
    id 180
    label "wydzieli&#263;"
  ]
  node [
    id 181
    label "punch"
  ]
  node [
    id 182
    label "wyci&#261;&#263;"
  ]
  node [
    id 183
    label "intencjonalny"
  ]
  node [
    id 184
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 185
    label "niedorozw&#243;j"
  ]
  node [
    id 186
    label "szczeg&#243;lny"
  ]
  node [
    id 187
    label "specjalnie"
  ]
  node [
    id 188
    label "nieetatowy"
  ]
  node [
    id 189
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 190
    label "nienormalny"
  ]
  node [
    id 191
    label "umy&#347;lnie"
  ]
  node [
    id 192
    label "odpowiedni"
  ]
  node [
    id 193
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 194
    label "nienormalnie"
  ]
  node [
    id 195
    label "anormalnie"
  ]
  node [
    id 196
    label "schizol"
  ]
  node [
    id 197
    label "pochytany"
  ]
  node [
    id 198
    label "popaprany"
  ]
  node [
    id 199
    label "niestandardowy"
  ]
  node [
    id 200
    label "chory_psychicznie"
  ]
  node [
    id 201
    label "nieprawid&#322;owy"
  ]
  node [
    id 202
    label "dziwny"
  ]
  node [
    id 203
    label "psychol"
  ]
  node [
    id 204
    label "powalony"
  ]
  node [
    id 205
    label "stracenie_rozumu"
  ]
  node [
    id 206
    label "chory"
  ]
  node [
    id 207
    label "z&#322;y"
  ]
  node [
    id 208
    label "nieprzypadkowy"
  ]
  node [
    id 209
    label "intencjonalnie"
  ]
  node [
    id 210
    label "szczeg&#243;lnie"
  ]
  node [
    id 211
    label "wyj&#261;tkowy"
  ]
  node [
    id 212
    label "zaburzenie"
  ]
  node [
    id 213
    label "niedoskona&#322;o&#347;&#263;"
  ]
  node [
    id 214
    label "wada"
  ]
  node [
    id 215
    label "zacofanie"
  ]
  node [
    id 216
    label "g&#322;upek"
  ]
  node [
    id 217
    label "zesp&#243;&#322;_Downa"
  ]
  node [
    id 218
    label "idiotyzm"
  ]
  node [
    id 219
    label "zdarzony"
  ]
  node [
    id 220
    label "odpowiednio"
  ]
  node [
    id 221
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 222
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 223
    label "nale&#380;ny"
  ]
  node [
    id 224
    label "nale&#380;yty"
  ]
  node [
    id 225
    label "stosownie"
  ]
  node [
    id 226
    label "odpowiadanie"
  ]
  node [
    id 227
    label "umy&#347;lny"
  ]
  node [
    id 228
    label "niesprawno&#347;&#263;"
  ]
  node [
    id 229
    label "nieetatowo"
  ]
  node [
    id 230
    label "nieoficjalny"
  ]
  node [
    id 231
    label "inny"
  ]
  node [
    id 232
    label "zatrudniony"
  ]
  node [
    id 233
    label "amfilada"
  ]
  node [
    id 234
    label "front"
  ]
  node [
    id 235
    label "apartment"
  ]
  node [
    id 236
    label "pod&#322;oga"
  ]
  node [
    id 237
    label "udost&#281;pnienie"
  ]
  node [
    id 238
    label "sklepienie"
  ]
  node [
    id 239
    label "sufit"
  ]
  node [
    id 240
    label "umieszczenie"
  ]
  node [
    id 241
    label "zakamarek"
  ]
  node [
    id 242
    label "umo&#380;liwienie"
  ]
  node [
    id 243
    label "poumieszczanie"
  ]
  node [
    id 244
    label "ustalenie"
  ]
  node [
    id 245
    label "uplasowanie"
  ]
  node [
    id 246
    label "spowodowanie"
  ]
  node [
    id 247
    label "ulokowanie_si&#281;"
  ]
  node [
    id 248
    label "prze&#322;adowanie"
  ]
  node [
    id 249
    label "zrobienie"
  ]
  node [
    id 250
    label "layout"
  ]
  node [
    id 251
    label "siedzenie"
  ]
  node [
    id 252
    label "zakrycie"
  ]
  node [
    id 253
    label "p&#322;aszczyzna"
  ]
  node [
    id 254
    label "zapadnia"
  ]
  node [
    id 255
    label "pojazd"
  ]
  node [
    id 256
    label "posadzka"
  ]
  node [
    id 257
    label "kaseton"
  ]
  node [
    id 258
    label "trompa"
  ]
  node [
    id 259
    label "wysklepia&#263;"
  ]
  node [
    id 260
    label "wysklepi&#263;"
  ]
  node [
    id 261
    label "budowla"
  ]
  node [
    id 262
    label "wysklepianie"
  ]
  node [
    id 263
    label "arch"
  ]
  node [
    id 264
    label "konstrukcja"
  ]
  node [
    id 265
    label "&#380;agielek"
  ]
  node [
    id 266
    label "kozub"
  ]
  node [
    id 267
    label "koleba"
  ]
  node [
    id 268
    label "wysklepienie"
  ]
  node [
    id 269
    label "struktura_anatomiczna"
  ]
  node [
    id 270
    label "brosza"
  ]
  node [
    id 271
    label "luneta"
  ]
  node [
    id 272
    label "z&#322;&#261;czenie"
  ]
  node [
    id 273
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 274
    label "na&#322;&#281;czka"
  ]
  node [
    id 275
    label "vault"
  ]
  node [
    id 276
    label "kresom&#243;zgowie_&#347;rodkowe"
  ]
  node [
    id 277
    label "rokada"
  ]
  node [
    id 278
    label "linia"
  ]
  node [
    id 279
    label "pole_bitwy"
  ]
  node [
    id 280
    label "sfera"
  ]
  node [
    id 281
    label "powietrze"
  ]
  node [
    id 282
    label "zaleganie"
  ]
  node [
    id 283
    label "zjednoczenie"
  ]
  node [
    id 284
    label "przedpole"
  ]
  node [
    id 285
    label "prz&#243;d"
  ]
  node [
    id 286
    label "szczyt"
  ]
  node [
    id 287
    label "zalega&#263;"
  ]
  node [
    id 288
    label "przycz&#243;&#322;ek"
  ]
  node [
    id 289
    label "zjawisko"
  ]
  node [
    id 290
    label "elewacja"
  ]
  node [
    id 291
    label "stowarzyszenie"
  ]
  node [
    id 292
    label "ci&#261;g"
  ]
  node [
    id 293
    label "wy&#322;&#261;czny"
  ]
  node [
    id 294
    label "w&#322;asny"
  ]
  node [
    id 295
    label "unikatowy"
  ]
  node [
    id 296
    label "jedyny"
  ]
  node [
    id 297
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 298
    label "najem"
  ]
  node [
    id 299
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 300
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 301
    label "stosunek_pracy"
  ]
  node [
    id 302
    label "benedykty&#324;ski"
  ]
  node [
    id 303
    label "poda&#380;_pracy"
  ]
  node [
    id 304
    label "pracowanie"
  ]
  node [
    id 305
    label "tyrka"
  ]
  node [
    id 306
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 307
    label "wytw&#243;r"
  ]
  node [
    id 308
    label "zaw&#243;d"
  ]
  node [
    id 309
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 310
    label "tynkarski"
  ]
  node [
    id 311
    label "pracowa&#263;"
  ]
  node [
    id 312
    label "zmiana"
  ]
  node [
    id 313
    label "czynnik_produkcji"
  ]
  node [
    id 314
    label "zobowi&#261;zanie"
  ]
  node [
    id 315
    label "kierownictwo"
  ]
  node [
    id 316
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 317
    label "przedmiot"
  ]
  node [
    id 318
    label "p&#322;&#243;d"
  ]
  node [
    id 319
    label "work"
  ]
  node [
    id 320
    label "rezultat"
  ]
  node [
    id 321
    label "activity"
  ]
  node [
    id 322
    label "bezproblemowy"
  ]
  node [
    id 323
    label "wydarzenie"
  ]
  node [
    id 324
    label "stosunek_prawny"
  ]
  node [
    id 325
    label "oblig"
  ]
  node [
    id 326
    label "uregulowa&#263;"
  ]
  node [
    id 327
    label "oddzia&#322;anie"
  ]
  node [
    id 328
    label "duty"
  ]
  node [
    id 329
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 330
    label "zapowied&#378;"
  ]
  node [
    id 331
    label "obowi&#261;zek"
  ]
  node [
    id 332
    label "statement"
  ]
  node [
    id 333
    label "zapewnienie"
  ]
  node [
    id 334
    label "&#321;ubianka"
  ]
  node [
    id 335
    label "dzia&#322;_personalny"
  ]
  node [
    id 336
    label "Kreml"
  ]
  node [
    id 337
    label "Bia&#322;y_Dom"
  ]
  node [
    id 338
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 339
    label "sadowisko"
  ]
  node [
    id 340
    label "rewizja"
  ]
  node [
    id 341
    label "passage"
  ]
  node [
    id 342
    label "oznaka"
  ]
  node [
    id 343
    label "change"
  ]
  node [
    id 344
    label "ferment"
  ]
  node [
    id 345
    label "komplet"
  ]
  node [
    id 346
    label "anatomopatolog"
  ]
  node [
    id 347
    label "zmianka"
  ]
  node [
    id 348
    label "czas"
  ]
  node [
    id 349
    label "amendment"
  ]
  node [
    id 350
    label "odmienianie"
  ]
  node [
    id 351
    label "tura"
  ]
  node [
    id 352
    label "cierpliwy"
  ]
  node [
    id 353
    label "mozolny"
  ]
  node [
    id 354
    label "wytrwa&#322;y"
  ]
  node [
    id 355
    label "benedykty&#324;sko"
  ]
  node [
    id 356
    label "typowy"
  ]
  node [
    id 357
    label "po_benedykty&#324;sku"
  ]
  node [
    id 358
    label "endeavor"
  ]
  node [
    id 359
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 360
    label "mie&#263;_miejsce"
  ]
  node [
    id 361
    label "podejmowa&#263;"
  ]
  node [
    id 362
    label "dziama&#263;"
  ]
  node [
    id 363
    label "do"
  ]
  node [
    id 364
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 365
    label "bangla&#263;"
  ]
  node [
    id 366
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 367
    label "maszyna"
  ]
  node [
    id 368
    label "dzia&#322;a&#263;"
  ]
  node [
    id 369
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 370
    label "tryb"
  ]
  node [
    id 371
    label "funkcjonowa&#263;"
  ]
  node [
    id 372
    label "zawodoznawstwo"
  ]
  node [
    id 373
    label "emocja"
  ]
  node [
    id 374
    label "office"
  ]
  node [
    id 375
    label "kwalifikacje"
  ]
  node [
    id 376
    label "craft"
  ]
  node [
    id 377
    label "przepracowanie_si&#281;"
  ]
  node [
    id 378
    label "zarz&#261;dzanie"
  ]
  node [
    id 379
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 380
    label "podlizanie_si&#281;"
  ]
  node [
    id 381
    label "dopracowanie"
  ]
  node [
    id 382
    label "podlizywanie_si&#281;"
  ]
  node [
    id 383
    label "uruchamianie"
  ]
  node [
    id 384
    label "dzia&#322;anie"
  ]
  node [
    id 385
    label "d&#261;&#380;enie"
  ]
  node [
    id 386
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 387
    label "uruchomienie"
  ]
  node [
    id 388
    label "nakr&#281;canie"
  ]
  node [
    id 389
    label "funkcjonowanie"
  ]
  node [
    id 390
    label "tr&#243;jstronny"
  ]
  node [
    id 391
    label "postaranie_si&#281;"
  ]
  node [
    id 392
    label "odpocz&#281;cie"
  ]
  node [
    id 393
    label "nakr&#281;cenie"
  ]
  node [
    id 394
    label "zatrzymanie"
  ]
  node [
    id 395
    label "spracowanie_si&#281;"
  ]
  node [
    id 396
    label "skakanie"
  ]
  node [
    id 397
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 398
    label "podtrzymywanie"
  ]
  node [
    id 399
    label "w&#322;&#261;czanie"
  ]
  node [
    id 400
    label "zaprz&#281;ganie"
  ]
  node [
    id 401
    label "podejmowanie"
  ]
  node [
    id 402
    label "wyrabianie"
  ]
  node [
    id 403
    label "use"
  ]
  node [
    id 404
    label "przepracowanie"
  ]
  node [
    id 405
    label "poruszanie_si&#281;"
  ]
  node [
    id 406
    label "funkcja"
  ]
  node [
    id 407
    label "impact"
  ]
  node [
    id 408
    label "przepracowywanie"
  ]
  node [
    id 409
    label "awansowanie"
  ]
  node [
    id 410
    label "courtship"
  ]
  node [
    id 411
    label "zapracowanie"
  ]
  node [
    id 412
    label "wyrobienie"
  ]
  node [
    id 413
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 414
    label "w&#322;&#261;czenie"
  ]
  node [
    id 415
    label "biuro"
  ]
  node [
    id 416
    label "lead"
  ]
  node [
    id 417
    label "zesp&#243;&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 293
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 295
  ]
  edge [
    source 11
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 305
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 12
    target 309
  ]
  edge [
    source 12
    target 310
  ]
  edge [
    source 12
    target 311
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 312
  ]
  edge [
    source 12
    target 313
  ]
  edge [
    source 12
    target 314
  ]
  edge [
    source 12
    target 315
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 316
  ]
  edge [
    source 12
    target 317
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 319
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 321
  ]
  edge [
    source 12
    target 322
  ]
  edge [
    source 12
    target 323
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 324
  ]
  edge [
    source 12
    target 325
  ]
  edge [
    source 12
    target 326
  ]
  edge [
    source 12
    target 327
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 12
    target 331
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 335
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 337
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 364
  ]
  edge [
    source 12
    target 365
  ]
  edge [
    source 12
    target 366
  ]
  edge [
    source 12
    target 367
  ]
  edge [
    source 12
    target 368
  ]
  edge [
    source 12
    target 369
  ]
  edge [
    source 12
    target 370
  ]
  edge [
    source 12
    target 371
  ]
  edge [
    source 12
    target 372
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 375
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 12
    target 382
  ]
  edge [
    source 12
    target 383
  ]
  edge [
    source 12
    target 384
  ]
  edge [
    source 12
    target 385
  ]
  edge [
    source 12
    target 386
  ]
  edge [
    source 12
    target 387
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 404
  ]
  edge [
    source 12
    target 405
  ]
  edge [
    source 12
    target 406
  ]
  edge [
    source 12
    target 407
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 12
    target 413
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 49
  ]
]
