graph [
  node [
    id 0
    label "andora"
    origin "text"
  ]
  node [
    id 1
    label "konkurs"
    origin "text"
  ]
  node [
    id 2
    label "piosenka"
    origin "text"
  ]
  node [
    id 3
    label "eurowizja"
    origin "text"
  ]
  node [
    id 4
    label "nab&#243;r"
  ]
  node [
    id 5
    label "impreza"
  ]
  node [
    id 6
    label "Interwizja"
  ]
  node [
    id 7
    label "casting"
  ]
  node [
    id 8
    label "emulation"
  ]
  node [
    id 9
    label "Eurowizja"
  ]
  node [
    id 10
    label "eliminacje"
  ]
  node [
    id 11
    label "party"
  ]
  node [
    id 12
    label "impra"
  ]
  node [
    id 13
    label "rozrywka"
  ]
  node [
    id 14
    label "przyj&#281;cie"
  ]
  node [
    id 15
    label "okazja"
  ]
  node [
    id 16
    label "recruitment"
  ]
  node [
    id 17
    label "wyb&#243;r"
  ]
  node [
    id 18
    label "turniej"
  ]
  node [
    id 19
    label "retirement"
  ]
  node [
    id 20
    label "runda"
  ]
  node [
    id 21
    label "faza"
  ]
  node [
    id 22
    label "przes&#322;uchanie"
  ]
  node [
    id 23
    label "w&#281;dkarstwo"
  ]
  node [
    id 24
    label "zanucenie"
  ]
  node [
    id 25
    label "tekst"
  ]
  node [
    id 26
    label "piosnka"
  ]
  node [
    id 27
    label "zanuci&#263;"
  ]
  node [
    id 28
    label "nuci&#263;"
  ]
  node [
    id 29
    label "utw&#243;r"
  ]
  node [
    id 30
    label "zwrotka"
  ]
  node [
    id 31
    label "nucenie"
  ]
  node [
    id 32
    label "komunikat"
  ]
  node [
    id 33
    label "part"
  ]
  node [
    id 34
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 35
    label "tre&#347;&#263;"
  ]
  node [
    id 36
    label "obrazowanie"
  ]
  node [
    id 37
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 38
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 39
    label "element_anatomiczny"
  ]
  node [
    id 40
    label "organ"
  ]
  node [
    id 41
    label "stanza"
  ]
  node [
    id 42
    label "wiersz"
  ]
  node [
    id 43
    label "strofoida"
  ]
  node [
    id 44
    label "pie&#347;&#324;"
  ]
  node [
    id 45
    label "pisa&#263;"
  ]
  node [
    id 46
    label "j&#281;zykowo"
  ]
  node [
    id 47
    label "redakcja"
  ]
  node [
    id 48
    label "preparacja"
  ]
  node [
    id 49
    label "dzie&#322;o"
  ]
  node [
    id 50
    label "wypowied&#378;"
  ]
  node [
    id 51
    label "obelga"
  ]
  node [
    id 52
    label "wytw&#243;r"
  ]
  node [
    id 53
    label "odmianka"
  ]
  node [
    id 54
    label "opu&#347;ci&#263;"
  ]
  node [
    id 55
    label "pomini&#281;cie"
  ]
  node [
    id 56
    label "koniektura"
  ]
  node [
    id 57
    label "ekscerpcja"
  ]
  node [
    id 58
    label "busyness"
  ]
  node [
    id 59
    label "melodia"
  ]
  node [
    id 60
    label "wykona&#263;"
  ]
  node [
    id 61
    label "gaworzy&#263;"
  ]
  node [
    id 62
    label "chant"
  ]
  node [
    id 63
    label "hum"
  ]
  node [
    id 64
    label "wykonywa&#263;"
  ]
  node [
    id 65
    label "wykonanie"
  ]
  node [
    id 66
    label "wykonywanie"
  ]
  node [
    id 67
    label "wydawanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 9
  ]
]
