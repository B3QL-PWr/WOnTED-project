graph [
  node [
    id 0
    label "akcja"
    origin "text"
  ]
  node [
    id 1
    label "dzieje"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "leeds"
    origin "text"
  ]
  node [
    id 4
    label "dywidenda"
  ]
  node [
    id 5
    label "przebieg"
  ]
  node [
    id 6
    label "operacja"
  ]
  node [
    id 7
    label "zagrywka"
  ]
  node [
    id 8
    label "wydarzenie"
  ]
  node [
    id 9
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 10
    label "udzia&#322;"
  ]
  node [
    id 11
    label "commotion"
  ]
  node [
    id 12
    label "occupation"
  ]
  node [
    id 13
    label "gra"
  ]
  node [
    id 14
    label "jazda"
  ]
  node [
    id 15
    label "czyn"
  ]
  node [
    id 16
    label "stock"
  ]
  node [
    id 17
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 18
    label "w&#281;ze&#322;"
  ]
  node [
    id 19
    label "wysoko&#347;&#263;"
  ]
  node [
    id 20
    label "czynno&#347;&#263;"
  ]
  node [
    id 21
    label "instrument_strunowy"
  ]
  node [
    id 22
    label "activity"
  ]
  node [
    id 23
    label "bezproblemowy"
  ]
  node [
    id 24
    label "funkcja"
  ]
  node [
    id 25
    label "act"
  ]
  node [
    id 26
    label "tallness"
  ]
  node [
    id 27
    label "altitude"
  ]
  node [
    id 28
    label "rozmiar"
  ]
  node [
    id 29
    label "degree"
  ]
  node [
    id 30
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 31
    label "odcinek"
  ]
  node [
    id 32
    label "k&#261;t"
  ]
  node [
    id 33
    label "wielko&#347;&#263;"
  ]
  node [
    id 34
    label "brzmienie"
  ]
  node [
    id 35
    label "sum"
  ]
  node [
    id 36
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 37
    label "gambit"
  ]
  node [
    id 38
    label "rozgrywka"
  ]
  node [
    id 39
    label "move"
  ]
  node [
    id 40
    label "manewr"
  ]
  node [
    id 41
    label "uderzenie"
  ]
  node [
    id 42
    label "posuni&#281;cie"
  ]
  node [
    id 43
    label "myk"
  ]
  node [
    id 44
    label "gra_w_karty"
  ]
  node [
    id 45
    label "mecz"
  ]
  node [
    id 46
    label "travel"
  ]
  node [
    id 47
    label "przebiec"
  ]
  node [
    id 48
    label "charakter"
  ]
  node [
    id 49
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 50
    label "motyw"
  ]
  node [
    id 51
    label "przebiegni&#281;cie"
  ]
  node [
    id 52
    label "fabu&#322;a"
  ]
  node [
    id 53
    label "proces_my&#347;lowy"
  ]
  node [
    id 54
    label "liczenie"
  ]
  node [
    id 55
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 56
    label "supremum"
  ]
  node [
    id 57
    label "laparotomia"
  ]
  node [
    id 58
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 59
    label "jednostka"
  ]
  node [
    id 60
    label "matematyka"
  ]
  node [
    id 61
    label "rzut"
  ]
  node [
    id 62
    label "liczy&#263;"
  ]
  node [
    id 63
    label "strategia"
  ]
  node [
    id 64
    label "torakotomia"
  ]
  node [
    id 65
    label "chirurg"
  ]
  node [
    id 66
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 67
    label "zabieg"
  ]
  node [
    id 68
    label "szew"
  ]
  node [
    id 69
    label "mathematical_process"
  ]
  node [
    id 70
    label "infimum"
  ]
  node [
    id 71
    label "linia"
  ]
  node [
    id 72
    label "procedura"
  ]
  node [
    id 73
    label "zbi&#243;r"
  ]
  node [
    id 74
    label "proces"
  ]
  node [
    id 75
    label "room"
  ]
  node [
    id 76
    label "ilo&#347;&#263;"
  ]
  node [
    id 77
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 78
    label "sequence"
  ]
  node [
    id 79
    label "praca"
  ]
  node [
    id 80
    label "cycle"
  ]
  node [
    id 81
    label "obecno&#347;&#263;"
  ]
  node [
    id 82
    label "kwota"
  ]
  node [
    id 83
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 84
    label "zmienno&#347;&#263;"
  ]
  node [
    id 85
    label "play"
  ]
  node [
    id 86
    label "apparent_motion"
  ]
  node [
    id 87
    label "contest"
  ]
  node [
    id 88
    label "komplet"
  ]
  node [
    id 89
    label "zabawa"
  ]
  node [
    id 90
    label "zasada"
  ]
  node [
    id 91
    label "rywalizacja"
  ]
  node [
    id 92
    label "zbijany"
  ]
  node [
    id 93
    label "post&#281;powanie"
  ]
  node [
    id 94
    label "game"
  ]
  node [
    id 95
    label "odg&#322;os"
  ]
  node [
    id 96
    label "Pok&#233;mon"
  ]
  node [
    id 97
    label "synteza"
  ]
  node [
    id 98
    label "odtworzenie"
  ]
  node [
    id 99
    label "rekwizyt_do_gry"
  ]
  node [
    id 100
    label "formacja"
  ]
  node [
    id 101
    label "szwadron"
  ]
  node [
    id 102
    label "wykrzyknik"
  ]
  node [
    id 103
    label "awantura"
  ]
  node [
    id 104
    label "journey"
  ]
  node [
    id 105
    label "sport"
  ]
  node [
    id 106
    label "heca"
  ]
  node [
    id 107
    label "ruch"
  ]
  node [
    id 108
    label "cavalry"
  ]
  node [
    id 109
    label "szale&#324;stwo"
  ]
  node [
    id 110
    label "chor&#261;giew"
  ]
  node [
    id 111
    label "doch&#243;d"
  ]
  node [
    id 112
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 113
    label "wi&#261;zanie"
  ]
  node [
    id 114
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 115
    label "poj&#281;cie"
  ]
  node [
    id 116
    label "bratnia_dusza"
  ]
  node [
    id 117
    label "trasa"
  ]
  node [
    id 118
    label "uczesanie"
  ]
  node [
    id 119
    label "orbita"
  ]
  node [
    id 120
    label "kryszta&#322;"
  ]
  node [
    id 121
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 122
    label "zwi&#261;zanie"
  ]
  node [
    id 123
    label "graf"
  ]
  node [
    id 124
    label "hitch"
  ]
  node [
    id 125
    label "struktura_anatomiczna"
  ]
  node [
    id 126
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 127
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 128
    label "o&#347;rodek"
  ]
  node [
    id 129
    label "marriage"
  ]
  node [
    id 130
    label "punkt"
  ]
  node [
    id 131
    label "ekliptyka"
  ]
  node [
    id 132
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 133
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 134
    label "problem"
  ]
  node [
    id 135
    label "zawi&#261;za&#263;"
  ]
  node [
    id 136
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 137
    label "fala_stoj&#261;ca"
  ]
  node [
    id 138
    label "tying"
  ]
  node [
    id 139
    label "argument"
  ]
  node [
    id 140
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 141
    label "zwi&#261;za&#263;"
  ]
  node [
    id 142
    label "mila_morska"
  ]
  node [
    id 143
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 144
    label "skupienie"
  ]
  node [
    id 145
    label "zgrubienie"
  ]
  node [
    id 146
    label "pismo_klinowe"
  ]
  node [
    id 147
    label "przeci&#281;cie"
  ]
  node [
    id 148
    label "band"
  ]
  node [
    id 149
    label "zwi&#261;zek"
  ]
  node [
    id 150
    label "epoka"
  ]
  node [
    id 151
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 152
    label "koleje_losu"
  ]
  node [
    id 153
    label "&#380;ycie"
  ]
  node [
    id 154
    label "czas"
  ]
  node [
    id 155
    label "aalen"
  ]
  node [
    id 156
    label "jura_wczesna"
  ]
  node [
    id 157
    label "holocen"
  ]
  node [
    id 158
    label "pliocen"
  ]
  node [
    id 159
    label "plejstocen"
  ]
  node [
    id 160
    label "paleocen"
  ]
  node [
    id 161
    label "bajos"
  ]
  node [
    id 162
    label "kelowej"
  ]
  node [
    id 163
    label "eocen"
  ]
  node [
    id 164
    label "jednostka_geologiczna"
  ]
  node [
    id 165
    label "okres"
  ]
  node [
    id 166
    label "schy&#322;ek"
  ]
  node [
    id 167
    label "miocen"
  ]
  node [
    id 168
    label "&#347;rodkowy_trias"
  ]
  node [
    id 169
    label "term"
  ]
  node [
    id 170
    label "Zeitgeist"
  ]
  node [
    id 171
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 172
    label "wczesny_trias"
  ]
  node [
    id 173
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 174
    label "jura_&#347;rodkowa"
  ]
  node [
    id 175
    label "oligocen"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 2
    target 3
  ]
]
