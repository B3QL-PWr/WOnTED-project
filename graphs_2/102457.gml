graph [
  node [
    id 0
    label "ksi&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 1
    label "gryz"
    origin "text"
  ]
  node [
    id 2
    label "luty"
    origin "text"
  ]
  node [
    id 3
    label "godz"
    origin "text"
  ]
  node [
    id 4
    label "filia"
    origin "text"
  ]
  node [
    id 5
    label "dla"
    origin "text"
  ]
  node [
    id 6
    label "doros&#322;a"
    origin "text"
  ]
  node [
    id 7
    label "przy"
    origin "text"
  ]
  node [
    id 8
    label "ula"
    origin "text"
  ]
  node [
    id 9
    label "mielczarski"
    origin "text"
  ]
  node [
    id 10
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "spotkanie"
    origin "text"
  ]
  node [
    id 13
    label "organizacyjny"
    origin "text"
  ]
  node [
    id 14
    label "dyskusyjny"
    origin "text"
  ]
  node [
    id 15
    label "klub"
    origin "text"
  ]
  node [
    id 16
    label "ksi&#261;&#380;kowy"
    origin "text"
  ]
  node [
    id 17
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 18
    label "walentynki"
  ]
  node [
    id 19
    label "Matka_Boska_Gromniczna"
  ]
  node [
    id 20
    label "miesi&#261;c"
  ]
  node [
    id 21
    label "tydzie&#324;"
  ]
  node [
    id 22
    label "miech"
  ]
  node [
    id 23
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 24
    label "czas"
  ]
  node [
    id 25
    label "rok"
  ]
  node [
    id 26
    label "kalendy"
  ]
  node [
    id 27
    label "agencja"
  ]
  node [
    id 28
    label "dzia&#322;"
  ]
  node [
    id 29
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 30
    label "jednostka_organizacyjna"
  ]
  node [
    id 31
    label "urz&#261;d"
  ]
  node [
    id 32
    label "sfera"
  ]
  node [
    id 33
    label "zakres"
  ]
  node [
    id 34
    label "miejsce_pracy"
  ]
  node [
    id 35
    label "zesp&#243;&#322;"
  ]
  node [
    id 36
    label "insourcing"
  ]
  node [
    id 37
    label "whole"
  ]
  node [
    id 38
    label "wytw&#243;r"
  ]
  node [
    id 39
    label "column"
  ]
  node [
    id 40
    label "distribution"
  ]
  node [
    id 41
    label "stopie&#324;"
  ]
  node [
    id 42
    label "competence"
  ]
  node [
    id 43
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 44
    label "bezdro&#380;e"
  ]
  node [
    id 45
    label "poddzia&#322;"
  ]
  node [
    id 46
    label "ajencja"
  ]
  node [
    id 47
    label "przedstawicielstwo"
  ]
  node [
    id 48
    label "instytucja"
  ]
  node [
    id 49
    label "firma"
  ]
  node [
    id 50
    label "siedziba"
  ]
  node [
    id 51
    label "NASA"
  ]
  node [
    id 52
    label "oddzia&#322;"
  ]
  node [
    id 53
    label "bank"
  ]
  node [
    id 54
    label "reserve"
  ]
  node [
    id 55
    label "przej&#347;&#263;"
  ]
  node [
    id 56
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 57
    label "ustawa"
  ]
  node [
    id 58
    label "podlec"
  ]
  node [
    id 59
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 60
    label "min&#261;&#263;"
  ]
  node [
    id 61
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 62
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 63
    label "zaliczy&#263;"
  ]
  node [
    id 64
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 65
    label "zmieni&#263;"
  ]
  node [
    id 66
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 67
    label "przeby&#263;"
  ]
  node [
    id 68
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 69
    label "die"
  ]
  node [
    id 70
    label "dozna&#263;"
  ]
  node [
    id 71
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 72
    label "zacz&#261;&#263;"
  ]
  node [
    id 73
    label "happen"
  ]
  node [
    id 74
    label "pass"
  ]
  node [
    id 75
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 76
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 77
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 78
    label "beat"
  ]
  node [
    id 79
    label "mienie"
  ]
  node [
    id 80
    label "absorb"
  ]
  node [
    id 81
    label "przerobi&#263;"
  ]
  node [
    id 82
    label "pique"
  ]
  node [
    id 83
    label "przesta&#263;"
  ]
  node [
    id 84
    label "doznanie"
  ]
  node [
    id 85
    label "gathering"
  ]
  node [
    id 86
    label "zawarcie"
  ]
  node [
    id 87
    label "wydarzenie"
  ]
  node [
    id 88
    label "znajomy"
  ]
  node [
    id 89
    label "powitanie"
  ]
  node [
    id 90
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 91
    label "spowodowanie"
  ]
  node [
    id 92
    label "zdarzenie_si&#281;"
  ]
  node [
    id 93
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 94
    label "znalezienie"
  ]
  node [
    id 95
    label "match"
  ]
  node [
    id 96
    label "employment"
  ]
  node [
    id 97
    label "po&#380;egnanie"
  ]
  node [
    id 98
    label "gather"
  ]
  node [
    id 99
    label "spotykanie"
  ]
  node [
    id 100
    label "spotkanie_si&#281;"
  ]
  node [
    id 101
    label "dzianie_si&#281;"
  ]
  node [
    id 102
    label "zaznawanie"
  ]
  node [
    id 103
    label "znajdowanie"
  ]
  node [
    id 104
    label "zdarzanie_si&#281;"
  ]
  node [
    id 105
    label "merging"
  ]
  node [
    id 106
    label "meeting"
  ]
  node [
    id 107
    label "zawieranie"
  ]
  node [
    id 108
    label "czynno&#347;&#263;"
  ]
  node [
    id 109
    label "campaign"
  ]
  node [
    id 110
    label "causing"
  ]
  node [
    id 111
    label "przebiec"
  ]
  node [
    id 112
    label "charakter"
  ]
  node [
    id 113
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 114
    label "motyw"
  ]
  node [
    id 115
    label "przebiegni&#281;cie"
  ]
  node [
    id 116
    label "fabu&#322;a"
  ]
  node [
    id 117
    label "postaranie_si&#281;"
  ]
  node [
    id 118
    label "discovery"
  ]
  node [
    id 119
    label "wymy&#347;lenie"
  ]
  node [
    id 120
    label "determination"
  ]
  node [
    id 121
    label "dorwanie"
  ]
  node [
    id 122
    label "znalezienie_si&#281;"
  ]
  node [
    id 123
    label "wykrycie"
  ]
  node [
    id 124
    label "poszukanie"
  ]
  node [
    id 125
    label "invention"
  ]
  node [
    id 126
    label "pozyskanie"
  ]
  node [
    id 127
    label "zmieszczenie"
  ]
  node [
    id 128
    label "umawianie_si&#281;"
  ]
  node [
    id 129
    label "zapoznanie"
  ]
  node [
    id 130
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 131
    label "zapoznanie_si&#281;"
  ]
  node [
    id 132
    label "ustalenie"
  ]
  node [
    id 133
    label "dissolution"
  ]
  node [
    id 134
    label "przyskrzynienie"
  ]
  node [
    id 135
    label "uk&#322;ad"
  ]
  node [
    id 136
    label "pozamykanie"
  ]
  node [
    id 137
    label "inclusion"
  ]
  node [
    id 138
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 139
    label "uchwalenie"
  ]
  node [
    id 140
    label "umowa"
  ]
  node [
    id 141
    label "zrobienie"
  ]
  node [
    id 142
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 143
    label "wy&#347;wiadczenie"
  ]
  node [
    id 144
    label "zmys&#322;"
  ]
  node [
    id 145
    label "przeczulica"
  ]
  node [
    id 146
    label "czucie"
  ]
  node [
    id 147
    label "poczucie"
  ]
  node [
    id 148
    label "znany"
  ]
  node [
    id 149
    label "sw&#243;j"
  ]
  node [
    id 150
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 151
    label "znajomek"
  ]
  node [
    id 152
    label "zapoznawanie"
  ]
  node [
    id 153
    label "znajomo"
  ]
  node [
    id 154
    label "pewien"
  ]
  node [
    id 155
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 156
    label "przyj&#281;ty"
  ]
  node [
    id 157
    label "za_pan_brat"
  ]
  node [
    id 158
    label "welcome"
  ]
  node [
    id 159
    label "pozdrowienie"
  ]
  node [
    id 160
    label "zwyczaj"
  ]
  node [
    id 161
    label "greeting"
  ]
  node [
    id 162
    label "wyra&#380;enie"
  ]
  node [
    id 163
    label "rozstanie_si&#281;"
  ]
  node [
    id 164
    label "adieu"
  ]
  node [
    id 165
    label "farewell"
  ]
  node [
    id 166
    label "kontrowersyjnie"
  ]
  node [
    id 167
    label "dyskusyjnie"
  ]
  node [
    id 168
    label "w&#261;tpliwy"
  ]
  node [
    id 169
    label "w&#261;tpliwie"
  ]
  node [
    id 170
    label "pozorny"
  ]
  node [
    id 171
    label "controversially"
  ]
  node [
    id 172
    label "kontrowersyjny"
  ]
  node [
    id 173
    label "od&#322;am"
  ]
  node [
    id 174
    label "society"
  ]
  node [
    id 175
    label "bar"
  ]
  node [
    id 176
    label "jakobini"
  ]
  node [
    id 177
    label "lokal"
  ]
  node [
    id 178
    label "stowarzyszenie"
  ]
  node [
    id 179
    label "klubista"
  ]
  node [
    id 180
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 181
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 182
    label "Chewra_Kadisza"
  ]
  node [
    id 183
    label "organizacja"
  ]
  node [
    id 184
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 185
    label "Rotary_International"
  ]
  node [
    id 186
    label "fabianie"
  ]
  node [
    id 187
    label "Eleusis"
  ]
  node [
    id 188
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 189
    label "Monar"
  ]
  node [
    id 190
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 191
    label "grupa"
  ]
  node [
    id 192
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 193
    label "&#321;ubianka"
  ]
  node [
    id 194
    label "dzia&#322;_personalny"
  ]
  node [
    id 195
    label "Kreml"
  ]
  node [
    id 196
    label "Bia&#322;y_Dom"
  ]
  node [
    id 197
    label "budynek"
  ]
  node [
    id 198
    label "miejsce"
  ]
  node [
    id 199
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 200
    label "sadowisko"
  ]
  node [
    id 201
    label "kawa&#322;"
  ]
  node [
    id 202
    label "bry&#322;a"
  ]
  node [
    id 203
    label "fragment"
  ]
  node [
    id 204
    label "struktura_geologiczna"
  ]
  node [
    id 205
    label "section"
  ]
  node [
    id 206
    label "gastronomia"
  ]
  node [
    id 207
    label "zak&#322;ad"
  ]
  node [
    id 208
    label "cz&#322;onek"
  ]
  node [
    id 209
    label "lada"
  ]
  node [
    id 210
    label "blat"
  ]
  node [
    id 211
    label "berylowiec"
  ]
  node [
    id 212
    label "milibar"
  ]
  node [
    id 213
    label "kawiarnia"
  ]
  node [
    id 214
    label "buffet"
  ]
  node [
    id 215
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 216
    label "mikrobar"
  ]
  node [
    id 217
    label "podr&#281;cznikowo"
  ]
  node [
    id 218
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 219
    label "ksi&#261;&#380;kowo"
  ]
  node [
    id 220
    label "klasyczny"
  ]
  node [
    id 221
    label "wzorcowy"
  ]
  node [
    id 222
    label "wzorowy"
  ]
  node [
    id 223
    label "nieklasyczny"
  ]
  node [
    id 224
    label "modelowy"
  ]
  node [
    id 225
    label "klasyczno"
  ]
  node [
    id 226
    label "zwyczajny"
  ]
  node [
    id 227
    label "typowy"
  ]
  node [
    id 228
    label "staro&#380;ytny"
  ]
  node [
    id 229
    label "tradycyjny"
  ]
  node [
    id 230
    label "normatywny"
  ]
  node [
    id 231
    label "klasycznie"
  ]
  node [
    id 232
    label "doskona&#322;y"
  ]
  node [
    id 233
    label "przyk&#322;adny"
  ]
  node [
    id 234
    label "&#322;adny"
  ]
  node [
    id 235
    label "dobry"
  ]
  node [
    id 236
    label "wzorowo"
  ]
  node [
    id 237
    label "wzorcowo"
  ]
  node [
    id 238
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 239
    label "nale&#380;ny"
  ]
  node [
    id 240
    label "nale&#380;yty"
  ]
  node [
    id 241
    label "uprawniony"
  ]
  node [
    id 242
    label "zasadniczy"
  ]
  node [
    id 243
    label "stosownie"
  ]
  node [
    id 244
    label "taki"
  ]
  node [
    id 245
    label "charakterystyczny"
  ]
  node [
    id 246
    label "prawdziwy"
  ]
  node [
    id 247
    label "ten"
  ]
  node [
    id 248
    label "teoretycznie"
  ]
  node [
    id 249
    label "schematycznie"
  ]
  node [
    id 250
    label "sucho"
  ]
  node [
    id 251
    label "podr&#281;cznikowy"
  ]
  node [
    id 252
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 253
    label "invite"
  ]
  node [
    id 254
    label "ask"
  ]
  node [
    id 255
    label "oferowa&#263;"
  ]
  node [
    id 256
    label "zach&#281;ca&#263;"
  ]
  node [
    id 257
    label "volunteer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 50
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 189
  ]
  edge [
    source 15
    target 190
  ]
  edge [
    source 15
    target 191
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 193
  ]
  edge [
    source 15
    target 34
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 195
  ]
  edge [
    source 15
    target 196
  ]
  edge [
    source 15
    target 197
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 199
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 201
  ]
  edge [
    source 15
    target 202
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 15
    target 204
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 15
    target 206
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 209
  ]
  edge [
    source 15
    target 210
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 215
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 217
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 220
  ]
  edge [
    source 16
    target 221
  ]
  edge [
    source 16
    target 222
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 224
  ]
  edge [
    source 16
    target 225
  ]
  edge [
    source 16
    target 226
  ]
  edge [
    source 16
    target 227
  ]
  edge [
    source 16
    target 228
  ]
  edge [
    source 16
    target 229
  ]
  edge [
    source 16
    target 230
  ]
  edge [
    source 16
    target 231
  ]
  edge [
    source 16
    target 232
  ]
  edge [
    source 16
    target 233
  ]
  edge [
    source 16
    target 234
  ]
  edge [
    source 16
    target 235
  ]
  edge [
    source 16
    target 236
  ]
  edge [
    source 16
    target 237
  ]
  edge [
    source 16
    target 238
  ]
  edge [
    source 16
    target 239
  ]
  edge [
    source 16
    target 240
  ]
  edge [
    source 16
    target 241
  ]
  edge [
    source 16
    target 242
  ]
  edge [
    source 16
    target 243
  ]
  edge [
    source 16
    target 244
  ]
  edge [
    source 16
    target 245
  ]
  edge [
    source 16
    target 246
  ]
  edge [
    source 16
    target 247
  ]
  edge [
    source 16
    target 248
  ]
  edge [
    source 16
    target 249
  ]
  edge [
    source 16
    target 250
  ]
  edge [
    source 16
    target 251
  ]
  edge [
    source 17
    target 252
  ]
  edge [
    source 17
    target 253
  ]
  edge [
    source 17
    target 254
  ]
  edge [
    source 17
    target 255
  ]
  edge [
    source 17
    target 256
  ]
  edge [
    source 17
    target 257
  ]
]
