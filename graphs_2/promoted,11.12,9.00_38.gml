graph [
  node [
    id 0
    label "powspomina&#263;"
    origin "text"
  ]
  node [
    id 1
    label "gra"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "wychowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 6
    label "pokolenie"
    origin "text"
  ]
  node [
    id 7
    label "gracz"
    origin "text"
  ]
  node [
    id 8
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 9
    label "zmienno&#347;&#263;"
  ]
  node [
    id 10
    label "play"
  ]
  node [
    id 11
    label "rozgrywka"
  ]
  node [
    id 12
    label "apparent_motion"
  ]
  node [
    id 13
    label "wydarzenie"
  ]
  node [
    id 14
    label "contest"
  ]
  node [
    id 15
    label "akcja"
  ]
  node [
    id 16
    label "komplet"
  ]
  node [
    id 17
    label "zabawa"
  ]
  node [
    id 18
    label "zasada"
  ]
  node [
    id 19
    label "rywalizacja"
  ]
  node [
    id 20
    label "zbijany"
  ]
  node [
    id 21
    label "post&#281;powanie"
  ]
  node [
    id 22
    label "game"
  ]
  node [
    id 23
    label "odg&#322;os"
  ]
  node [
    id 24
    label "Pok&#233;mon"
  ]
  node [
    id 25
    label "czynno&#347;&#263;"
  ]
  node [
    id 26
    label "synteza"
  ]
  node [
    id 27
    label "odtworzenie"
  ]
  node [
    id 28
    label "rekwizyt_do_gry"
  ]
  node [
    id 29
    label "resonance"
  ]
  node [
    id 30
    label "wydanie"
  ]
  node [
    id 31
    label "wpadni&#281;cie"
  ]
  node [
    id 32
    label "d&#378;wi&#281;k"
  ]
  node [
    id 33
    label "wpadanie"
  ]
  node [
    id 34
    label "wydawa&#263;"
  ]
  node [
    id 35
    label "sound"
  ]
  node [
    id 36
    label "brzmienie"
  ]
  node [
    id 37
    label "zjawisko"
  ]
  node [
    id 38
    label "wyda&#263;"
  ]
  node [
    id 39
    label "wpa&#347;&#263;"
  ]
  node [
    id 40
    label "note"
  ]
  node [
    id 41
    label "onomatopeja"
  ]
  node [
    id 42
    label "wpada&#263;"
  ]
  node [
    id 43
    label "cecha"
  ]
  node [
    id 44
    label "s&#261;d"
  ]
  node [
    id 45
    label "kognicja"
  ]
  node [
    id 46
    label "campaign"
  ]
  node [
    id 47
    label "rozprawa"
  ]
  node [
    id 48
    label "zachowanie"
  ]
  node [
    id 49
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 50
    label "fashion"
  ]
  node [
    id 51
    label "robienie"
  ]
  node [
    id 52
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 53
    label "zmierzanie"
  ]
  node [
    id 54
    label "przes&#322;anka"
  ]
  node [
    id 55
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 56
    label "kazanie"
  ]
  node [
    id 57
    label "trafienie"
  ]
  node [
    id 58
    label "rewan&#380;owy"
  ]
  node [
    id 59
    label "zagrywka"
  ]
  node [
    id 60
    label "faza"
  ]
  node [
    id 61
    label "euroliga"
  ]
  node [
    id 62
    label "interliga"
  ]
  node [
    id 63
    label "runda"
  ]
  node [
    id 64
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 65
    label "rozrywka"
  ]
  node [
    id 66
    label "impreza"
  ]
  node [
    id 67
    label "igraszka"
  ]
  node [
    id 68
    label "taniec"
  ]
  node [
    id 69
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 70
    label "gambling"
  ]
  node [
    id 71
    label "chwyt"
  ]
  node [
    id 72
    label "igra"
  ]
  node [
    id 73
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 74
    label "nabawienie_si&#281;"
  ]
  node [
    id 75
    label "ubaw"
  ]
  node [
    id 76
    label "wodzirej"
  ]
  node [
    id 77
    label "activity"
  ]
  node [
    id 78
    label "bezproblemowy"
  ]
  node [
    id 79
    label "przebiec"
  ]
  node [
    id 80
    label "charakter"
  ]
  node [
    id 81
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 82
    label "motyw"
  ]
  node [
    id 83
    label "przebiegni&#281;cie"
  ]
  node [
    id 84
    label "fabu&#322;a"
  ]
  node [
    id 85
    label "proces_technologiczny"
  ]
  node [
    id 86
    label "mieszanina"
  ]
  node [
    id 87
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 88
    label "fusion"
  ]
  node [
    id 89
    label "poj&#281;cie"
  ]
  node [
    id 90
    label "reakcja_chemiczna"
  ]
  node [
    id 91
    label "zestawienie"
  ]
  node [
    id 92
    label "uog&#243;lnienie"
  ]
  node [
    id 93
    label "puszczenie"
  ]
  node [
    id 94
    label "ustalenie"
  ]
  node [
    id 95
    label "wyst&#281;p"
  ]
  node [
    id 96
    label "reproduction"
  ]
  node [
    id 97
    label "przedstawienie"
  ]
  node [
    id 98
    label "przywr&#243;cenie"
  ]
  node [
    id 99
    label "w&#322;&#261;czenie"
  ]
  node [
    id 100
    label "zregenerowanie_si&#281;"
  ]
  node [
    id 101
    label "restoration"
  ]
  node [
    id 102
    label "odbudowanie"
  ]
  node [
    id 103
    label "lekcja"
  ]
  node [
    id 104
    label "ensemble"
  ]
  node [
    id 105
    label "grupa"
  ]
  node [
    id 106
    label "klasa"
  ]
  node [
    id 107
    label "zestaw"
  ]
  node [
    id 108
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 109
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 110
    label "regu&#322;a_Allena"
  ]
  node [
    id 111
    label "base"
  ]
  node [
    id 112
    label "umowa"
  ]
  node [
    id 113
    label "obserwacja"
  ]
  node [
    id 114
    label "zasada_d'Alemberta"
  ]
  node [
    id 115
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 116
    label "normalizacja"
  ]
  node [
    id 117
    label "moralno&#347;&#263;"
  ]
  node [
    id 118
    label "criterion"
  ]
  node [
    id 119
    label "opis"
  ]
  node [
    id 120
    label "regu&#322;a_Glogera"
  ]
  node [
    id 121
    label "prawo_Mendla"
  ]
  node [
    id 122
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 123
    label "twierdzenie"
  ]
  node [
    id 124
    label "prawo"
  ]
  node [
    id 125
    label "standard"
  ]
  node [
    id 126
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 127
    label "spos&#243;b"
  ]
  node [
    id 128
    label "dominion"
  ]
  node [
    id 129
    label "qualification"
  ]
  node [
    id 130
    label "occupation"
  ]
  node [
    id 131
    label "podstawa"
  ]
  node [
    id 132
    label "substancja"
  ]
  node [
    id 133
    label "prawid&#322;o"
  ]
  node [
    id 134
    label "dywidenda"
  ]
  node [
    id 135
    label "przebieg"
  ]
  node [
    id 136
    label "operacja"
  ]
  node [
    id 137
    label "udzia&#322;"
  ]
  node [
    id 138
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 139
    label "commotion"
  ]
  node [
    id 140
    label "jazda"
  ]
  node [
    id 141
    label "czyn"
  ]
  node [
    id 142
    label "stock"
  ]
  node [
    id 143
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 144
    label "w&#281;ze&#322;"
  ]
  node [
    id 145
    label "wysoko&#347;&#263;"
  ]
  node [
    id 146
    label "instrument_strunowy"
  ]
  node [
    id 147
    label "pi&#322;ka"
  ]
  node [
    id 148
    label "wyszkoli&#263;"
  ]
  node [
    id 149
    label "train"
  ]
  node [
    id 150
    label "zapewni&#263;"
  ]
  node [
    id 151
    label "pom&#243;c"
  ]
  node [
    id 152
    label "o&#347;wieci&#263;"
  ]
  node [
    id 153
    label "poinformowa&#263;"
  ]
  node [
    id 154
    label "give"
  ]
  node [
    id 155
    label "translate"
  ]
  node [
    id 156
    label "spowodowa&#263;"
  ]
  node [
    id 157
    label "jedyny"
  ]
  node [
    id 158
    label "du&#380;y"
  ]
  node [
    id 159
    label "zdr&#243;w"
  ]
  node [
    id 160
    label "calu&#347;ko"
  ]
  node [
    id 161
    label "kompletny"
  ]
  node [
    id 162
    label "&#380;ywy"
  ]
  node [
    id 163
    label "pe&#322;ny"
  ]
  node [
    id 164
    label "podobny"
  ]
  node [
    id 165
    label "ca&#322;o"
  ]
  node [
    id 166
    label "kompletnie"
  ]
  node [
    id 167
    label "zupe&#322;ny"
  ]
  node [
    id 168
    label "w_pizdu"
  ]
  node [
    id 169
    label "przypominanie"
  ]
  node [
    id 170
    label "podobnie"
  ]
  node [
    id 171
    label "asymilowanie"
  ]
  node [
    id 172
    label "upodabnianie_si&#281;"
  ]
  node [
    id 173
    label "upodobnienie"
  ]
  node [
    id 174
    label "drugi"
  ]
  node [
    id 175
    label "taki"
  ]
  node [
    id 176
    label "charakterystyczny"
  ]
  node [
    id 177
    label "upodobnienie_si&#281;"
  ]
  node [
    id 178
    label "zasymilowanie"
  ]
  node [
    id 179
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 180
    label "ukochany"
  ]
  node [
    id 181
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 182
    label "najlepszy"
  ]
  node [
    id 183
    label "optymalnie"
  ]
  node [
    id 184
    label "doros&#322;y"
  ]
  node [
    id 185
    label "znaczny"
  ]
  node [
    id 186
    label "niema&#322;o"
  ]
  node [
    id 187
    label "wiele"
  ]
  node [
    id 188
    label "rozwini&#281;ty"
  ]
  node [
    id 189
    label "dorodny"
  ]
  node [
    id 190
    label "wa&#380;ny"
  ]
  node [
    id 191
    label "prawdziwy"
  ]
  node [
    id 192
    label "du&#380;o"
  ]
  node [
    id 193
    label "ciekawy"
  ]
  node [
    id 194
    label "szybki"
  ]
  node [
    id 195
    label "&#380;ywotny"
  ]
  node [
    id 196
    label "naturalny"
  ]
  node [
    id 197
    label "&#380;ywo"
  ]
  node [
    id 198
    label "cz&#322;owiek"
  ]
  node [
    id 199
    label "o&#380;ywianie"
  ]
  node [
    id 200
    label "&#380;ycie"
  ]
  node [
    id 201
    label "silny"
  ]
  node [
    id 202
    label "g&#322;&#281;boki"
  ]
  node [
    id 203
    label "wyra&#378;ny"
  ]
  node [
    id 204
    label "czynny"
  ]
  node [
    id 205
    label "aktualny"
  ]
  node [
    id 206
    label "zgrabny"
  ]
  node [
    id 207
    label "realistyczny"
  ]
  node [
    id 208
    label "energiczny"
  ]
  node [
    id 209
    label "zdrowy"
  ]
  node [
    id 210
    label "nieograniczony"
  ]
  node [
    id 211
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 212
    label "satysfakcja"
  ]
  node [
    id 213
    label "bezwzgl&#281;dny"
  ]
  node [
    id 214
    label "otwarty"
  ]
  node [
    id 215
    label "wype&#322;nienie"
  ]
  node [
    id 216
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 217
    label "pe&#322;no"
  ]
  node [
    id 218
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 219
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 220
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 221
    label "r&#243;wny"
  ]
  node [
    id 222
    label "nieuszkodzony"
  ]
  node [
    id 223
    label "odpowiednio"
  ]
  node [
    id 224
    label "zbi&#243;r"
  ]
  node [
    id 225
    label "rocznik"
  ]
  node [
    id 226
    label "epoka"
  ]
  node [
    id 227
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 228
    label "drzewo_genealogiczne"
  ]
  node [
    id 229
    label "facylitacja"
  ]
  node [
    id 230
    label "egzemplarz"
  ]
  node [
    id 231
    label "series"
  ]
  node [
    id 232
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 233
    label "uprawianie"
  ]
  node [
    id 234
    label "praca_rolnicza"
  ]
  node [
    id 235
    label "collection"
  ]
  node [
    id 236
    label "dane"
  ]
  node [
    id 237
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 238
    label "pakiet_klimatyczny"
  ]
  node [
    id 239
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 240
    label "sum"
  ]
  node [
    id 241
    label "gathering"
  ]
  node [
    id 242
    label "album"
  ]
  node [
    id 243
    label "aalen"
  ]
  node [
    id 244
    label "jura_wczesna"
  ]
  node [
    id 245
    label "holocen"
  ]
  node [
    id 246
    label "pliocen"
  ]
  node [
    id 247
    label "plejstocen"
  ]
  node [
    id 248
    label "paleocen"
  ]
  node [
    id 249
    label "dzieje"
  ]
  node [
    id 250
    label "bajos"
  ]
  node [
    id 251
    label "kelowej"
  ]
  node [
    id 252
    label "eocen"
  ]
  node [
    id 253
    label "jednostka_geologiczna"
  ]
  node [
    id 254
    label "okres"
  ]
  node [
    id 255
    label "schy&#322;ek"
  ]
  node [
    id 256
    label "miocen"
  ]
  node [
    id 257
    label "&#347;rodkowy_trias"
  ]
  node [
    id 258
    label "czas"
  ]
  node [
    id 259
    label "term"
  ]
  node [
    id 260
    label "Zeitgeist"
  ]
  node [
    id 261
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 262
    label "wczesny_trias"
  ]
  node [
    id 263
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 264
    label "jura_&#347;rodkowa"
  ]
  node [
    id 265
    label "oligocen"
  ]
  node [
    id 266
    label "formacja"
  ]
  node [
    id 267
    label "yearbook"
  ]
  node [
    id 268
    label "czasopismo"
  ]
  node [
    id 269
    label "kronika"
  ]
  node [
    id 270
    label "uczestnik"
  ]
  node [
    id 271
    label "posta&#263;"
  ]
  node [
    id 272
    label "zwierz&#281;"
  ]
  node [
    id 273
    label "bohater"
  ]
  node [
    id 274
    label "spryciarz"
  ]
  node [
    id 275
    label "rozdawa&#263;_karty"
  ]
  node [
    id 276
    label "ludzko&#347;&#263;"
  ]
  node [
    id 277
    label "wapniak"
  ]
  node [
    id 278
    label "asymilowa&#263;"
  ]
  node [
    id 279
    label "os&#322;abia&#263;"
  ]
  node [
    id 280
    label "hominid"
  ]
  node [
    id 281
    label "podw&#322;adny"
  ]
  node [
    id 282
    label "os&#322;abianie"
  ]
  node [
    id 283
    label "g&#322;owa"
  ]
  node [
    id 284
    label "figura"
  ]
  node [
    id 285
    label "portrecista"
  ]
  node [
    id 286
    label "dwun&#243;g"
  ]
  node [
    id 287
    label "profanum"
  ]
  node [
    id 288
    label "mikrokosmos"
  ]
  node [
    id 289
    label "nasada"
  ]
  node [
    id 290
    label "duch"
  ]
  node [
    id 291
    label "antropochoria"
  ]
  node [
    id 292
    label "osoba"
  ]
  node [
    id 293
    label "wz&#243;r"
  ]
  node [
    id 294
    label "senior"
  ]
  node [
    id 295
    label "oddzia&#322;ywanie"
  ]
  node [
    id 296
    label "Adam"
  ]
  node [
    id 297
    label "homo_sapiens"
  ]
  node [
    id 298
    label "polifag"
  ]
  node [
    id 299
    label "morowiec"
  ]
  node [
    id 300
    label "majster"
  ]
  node [
    id 301
    label "degenerat"
  ]
  node [
    id 302
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 303
    label "zwyrol"
  ]
  node [
    id 304
    label "czerniak"
  ]
  node [
    id 305
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 306
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 307
    label "paszcza"
  ]
  node [
    id 308
    label "popapraniec"
  ]
  node [
    id 309
    label "skuba&#263;"
  ]
  node [
    id 310
    label "skubanie"
  ]
  node [
    id 311
    label "skubni&#281;cie"
  ]
  node [
    id 312
    label "agresja"
  ]
  node [
    id 313
    label "zwierz&#281;ta"
  ]
  node [
    id 314
    label "fukni&#281;cie"
  ]
  node [
    id 315
    label "farba"
  ]
  node [
    id 316
    label "fukanie"
  ]
  node [
    id 317
    label "istota_&#380;ywa"
  ]
  node [
    id 318
    label "gad"
  ]
  node [
    id 319
    label "siedzie&#263;"
  ]
  node [
    id 320
    label "oswaja&#263;"
  ]
  node [
    id 321
    label "tresowa&#263;"
  ]
  node [
    id 322
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 323
    label "poligamia"
  ]
  node [
    id 324
    label "oz&#243;r"
  ]
  node [
    id 325
    label "skubn&#261;&#263;"
  ]
  node [
    id 326
    label "wios&#322;owa&#263;"
  ]
  node [
    id 327
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 328
    label "le&#380;enie"
  ]
  node [
    id 329
    label "niecz&#322;owiek"
  ]
  node [
    id 330
    label "wios&#322;owanie"
  ]
  node [
    id 331
    label "napasienie_si&#281;"
  ]
  node [
    id 332
    label "wiwarium"
  ]
  node [
    id 333
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 334
    label "animalista"
  ]
  node [
    id 335
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 336
    label "budowa"
  ]
  node [
    id 337
    label "hodowla"
  ]
  node [
    id 338
    label "pasienie_si&#281;"
  ]
  node [
    id 339
    label "sodomita"
  ]
  node [
    id 340
    label "monogamia"
  ]
  node [
    id 341
    label "przyssawka"
  ]
  node [
    id 342
    label "budowa_cia&#322;a"
  ]
  node [
    id 343
    label "okrutnik"
  ]
  node [
    id 344
    label "grzbiet"
  ]
  node [
    id 345
    label "weterynarz"
  ]
  node [
    id 346
    label "&#322;eb"
  ]
  node [
    id 347
    label "wylinka"
  ]
  node [
    id 348
    label "bestia"
  ]
  node [
    id 349
    label "poskramia&#263;"
  ]
  node [
    id 350
    label "fauna"
  ]
  node [
    id 351
    label "treser"
  ]
  node [
    id 352
    label "siedzenie"
  ]
  node [
    id 353
    label "le&#380;e&#263;"
  ]
  node [
    id 354
    label "charakterystyka"
  ]
  node [
    id 355
    label "zaistnie&#263;"
  ]
  node [
    id 356
    label "Osjan"
  ]
  node [
    id 357
    label "kto&#347;"
  ]
  node [
    id 358
    label "wygl&#261;d"
  ]
  node [
    id 359
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 360
    label "osobowo&#347;&#263;"
  ]
  node [
    id 361
    label "wytw&#243;r"
  ]
  node [
    id 362
    label "trim"
  ]
  node [
    id 363
    label "poby&#263;"
  ]
  node [
    id 364
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 365
    label "Aspazja"
  ]
  node [
    id 366
    label "punkt_widzenia"
  ]
  node [
    id 367
    label "kompleksja"
  ]
  node [
    id 368
    label "wytrzyma&#263;"
  ]
  node [
    id 369
    label "pozosta&#263;"
  ]
  node [
    id 370
    label "point"
  ]
  node [
    id 371
    label "go&#347;&#263;"
  ]
  node [
    id 372
    label "Messi"
  ]
  node [
    id 373
    label "Herkules_Poirot"
  ]
  node [
    id 374
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 375
    label "Achilles"
  ]
  node [
    id 376
    label "Harry_Potter"
  ]
  node [
    id 377
    label "Casanova"
  ]
  node [
    id 378
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 379
    label "Zgredek"
  ]
  node [
    id 380
    label "Asterix"
  ]
  node [
    id 381
    label "Winnetou"
  ]
  node [
    id 382
    label "&#347;mia&#322;ek"
  ]
  node [
    id 383
    label "Mario"
  ]
  node [
    id 384
    label "Borewicz"
  ]
  node [
    id 385
    label "Quasimodo"
  ]
  node [
    id 386
    label "Sherlock_Holmes"
  ]
  node [
    id 387
    label "Wallenrod"
  ]
  node [
    id 388
    label "Herkules"
  ]
  node [
    id 389
    label "bohaterski"
  ]
  node [
    id 390
    label "Don_Juan"
  ]
  node [
    id 391
    label "podmiot"
  ]
  node [
    id 392
    label "Don_Kiszot"
  ]
  node [
    id 393
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 394
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 395
    label "Hamlet"
  ]
  node [
    id 396
    label "Werter"
  ]
  node [
    id 397
    label "Szwejk"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
]
