graph [
  node [
    id 0
    label "podwierzbie"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 2
    label "lubelskie"
    origin "text"
  ]
  node [
    id 3
    label "powiat"
  ]
  node [
    id 4
    label "mikroregion"
  ]
  node [
    id 5
    label "makroregion"
  ]
  node [
    id 6
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 7
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 8
    label "pa&#324;stwo"
  ]
  node [
    id 9
    label "jednostka_administracyjna"
  ]
  node [
    id 10
    label "region"
  ]
  node [
    id 11
    label "gmina"
  ]
  node [
    id 12
    label "mezoregion"
  ]
  node [
    id 13
    label "Jura"
  ]
  node [
    id 14
    label "Beskidy_Zachodnie"
  ]
  node [
    id 15
    label "Katar"
  ]
  node [
    id 16
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 17
    label "Libia"
  ]
  node [
    id 18
    label "Gwatemala"
  ]
  node [
    id 19
    label "Afganistan"
  ]
  node [
    id 20
    label "Ekwador"
  ]
  node [
    id 21
    label "Tad&#380;ykistan"
  ]
  node [
    id 22
    label "Bhutan"
  ]
  node [
    id 23
    label "Argentyna"
  ]
  node [
    id 24
    label "D&#380;ibuti"
  ]
  node [
    id 25
    label "Wenezuela"
  ]
  node [
    id 26
    label "Ukraina"
  ]
  node [
    id 27
    label "Gabon"
  ]
  node [
    id 28
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 29
    label "Rwanda"
  ]
  node [
    id 30
    label "Liechtenstein"
  ]
  node [
    id 31
    label "organizacja"
  ]
  node [
    id 32
    label "Sri_Lanka"
  ]
  node [
    id 33
    label "Madagaskar"
  ]
  node [
    id 34
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 35
    label "Tonga"
  ]
  node [
    id 36
    label "Kongo"
  ]
  node [
    id 37
    label "Bangladesz"
  ]
  node [
    id 38
    label "Kanada"
  ]
  node [
    id 39
    label "Wehrlen"
  ]
  node [
    id 40
    label "Algieria"
  ]
  node [
    id 41
    label "Surinam"
  ]
  node [
    id 42
    label "Chile"
  ]
  node [
    id 43
    label "Sahara_Zachodnia"
  ]
  node [
    id 44
    label "Uganda"
  ]
  node [
    id 45
    label "W&#281;gry"
  ]
  node [
    id 46
    label "Birma"
  ]
  node [
    id 47
    label "Kazachstan"
  ]
  node [
    id 48
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 49
    label "Armenia"
  ]
  node [
    id 50
    label "Tuwalu"
  ]
  node [
    id 51
    label "Timor_Wschodni"
  ]
  node [
    id 52
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 53
    label "Izrael"
  ]
  node [
    id 54
    label "Estonia"
  ]
  node [
    id 55
    label "Komory"
  ]
  node [
    id 56
    label "Kamerun"
  ]
  node [
    id 57
    label "Haiti"
  ]
  node [
    id 58
    label "Belize"
  ]
  node [
    id 59
    label "Sierra_Leone"
  ]
  node [
    id 60
    label "Luksemburg"
  ]
  node [
    id 61
    label "USA"
  ]
  node [
    id 62
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 63
    label "Barbados"
  ]
  node [
    id 64
    label "San_Marino"
  ]
  node [
    id 65
    label "Bu&#322;garia"
  ]
  node [
    id 66
    label "Wietnam"
  ]
  node [
    id 67
    label "Indonezja"
  ]
  node [
    id 68
    label "Malawi"
  ]
  node [
    id 69
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 70
    label "Francja"
  ]
  node [
    id 71
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 72
    label "partia"
  ]
  node [
    id 73
    label "Zambia"
  ]
  node [
    id 74
    label "Angola"
  ]
  node [
    id 75
    label "Grenada"
  ]
  node [
    id 76
    label "Nepal"
  ]
  node [
    id 77
    label "Panama"
  ]
  node [
    id 78
    label "Rumunia"
  ]
  node [
    id 79
    label "Czarnog&#243;ra"
  ]
  node [
    id 80
    label "Malediwy"
  ]
  node [
    id 81
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 82
    label "S&#322;owacja"
  ]
  node [
    id 83
    label "para"
  ]
  node [
    id 84
    label "Egipt"
  ]
  node [
    id 85
    label "zwrot"
  ]
  node [
    id 86
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 87
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 88
    label "Kolumbia"
  ]
  node [
    id 89
    label "Mozambik"
  ]
  node [
    id 90
    label "Laos"
  ]
  node [
    id 91
    label "Burundi"
  ]
  node [
    id 92
    label "Suazi"
  ]
  node [
    id 93
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 94
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 95
    label "Czechy"
  ]
  node [
    id 96
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 97
    label "Wyspy_Marshalla"
  ]
  node [
    id 98
    label "Trynidad_i_Tobago"
  ]
  node [
    id 99
    label "Dominika"
  ]
  node [
    id 100
    label "Palau"
  ]
  node [
    id 101
    label "Syria"
  ]
  node [
    id 102
    label "Gwinea_Bissau"
  ]
  node [
    id 103
    label "Liberia"
  ]
  node [
    id 104
    label "Zimbabwe"
  ]
  node [
    id 105
    label "Polska"
  ]
  node [
    id 106
    label "Jamajka"
  ]
  node [
    id 107
    label "Dominikana"
  ]
  node [
    id 108
    label "Senegal"
  ]
  node [
    id 109
    label "Gruzja"
  ]
  node [
    id 110
    label "Togo"
  ]
  node [
    id 111
    label "Chorwacja"
  ]
  node [
    id 112
    label "Meksyk"
  ]
  node [
    id 113
    label "Macedonia"
  ]
  node [
    id 114
    label "Gujana"
  ]
  node [
    id 115
    label "Zair"
  ]
  node [
    id 116
    label "Albania"
  ]
  node [
    id 117
    label "Kambod&#380;a"
  ]
  node [
    id 118
    label "Mauritius"
  ]
  node [
    id 119
    label "Monako"
  ]
  node [
    id 120
    label "Gwinea"
  ]
  node [
    id 121
    label "Mali"
  ]
  node [
    id 122
    label "Nigeria"
  ]
  node [
    id 123
    label "Kostaryka"
  ]
  node [
    id 124
    label "Hanower"
  ]
  node [
    id 125
    label "Paragwaj"
  ]
  node [
    id 126
    label "W&#322;ochy"
  ]
  node [
    id 127
    label "Wyspy_Salomona"
  ]
  node [
    id 128
    label "Seszele"
  ]
  node [
    id 129
    label "Hiszpania"
  ]
  node [
    id 130
    label "Boliwia"
  ]
  node [
    id 131
    label "Kirgistan"
  ]
  node [
    id 132
    label "Irlandia"
  ]
  node [
    id 133
    label "Czad"
  ]
  node [
    id 134
    label "Irak"
  ]
  node [
    id 135
    label "Lesoto"
  ]
  node [
    id 136
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 137
    label "Malta"
  ]
  node [
    id 138
    label "Andora"
  ]
  node [
    id 139
    label "Chiny"
  ]
  node [
    id 140
    label "Filipiny"
  ]
  node [
    id 141
    label "Antarktis"
  ]
  node [
    id 142
    label "Niemcy"
  ]
  node [
    id 143
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 144
    label "Brazylia"
  ]
  node [
    id 145
    label "terytorium"
  ]
  node [
    id 146
    label "Nikaragua"
  ]
  node [
    id 147
    label "Pakistan"
  ]
  node [
    id 148
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 149
    label "Kenia"
  ]
  node [
    id 150
    label "Niger"
  ]
  node [
    id 151
    label "Tunezja"
  ]
  node [
    id 152
    label "Portugalia"
  ]
  node [
    id 153
    label "Fid&#380;i"
  ]
  node [
    id 154
    label "Maroko"
  ]
  node [
    id 155
    label "Botswana"
  ]
  node [
    id 156
    label "Tajlandia"
  ]
  node [
    id 157
    label "Australia"
  ]
  node [
    id 158
    label "Burkina_Faso"
  ]
  node [
    id 159
    label "interior"
  ]
  node [
    id 160
    label "Benin"
  ]
  node [
    id 161
    label "Tanzania"
  ]
  node [
    id 162
    label "Indie"
  ]
  node [
    id 163
    label "&#321;otwa"
  ]
  node [
    id 164
    label "Kiribati"
  ]
  node [
    id 165
    label "Antigua_i_Barbuda"
  ]
  node [
    id 166
    label "Rodezja"
  ]
  node [
    id 167
    label "Cypr"
  ]
  node [
    id 168
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 169
    label "Peru"
  ]
  node [
    id 170
    label "Austria"
  ]
  node [
    id 171
    label "Urugwaj"
  ]
  node [
    id 172
    label "Jordania"
  ]
  node [
    id 173
    label "Grecja"
  ]
  node [
    id 174
    label "Azerbejd&#380;an"
  ]
  node [
    id 175
    label "Turcja"
  ]
  node [
    id 176
    label "Samoa"
  ]
  node [
    id 177
    label "Sudan"
  ]
  node [
    id 178
    label "Oman"
  ]
  node [
    id 179
    label "ziemia"
  ]
  node [
    id 180
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 181
    label "Uzbekistan"
  ]
  node [
    id 182
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 183
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 184
    label "Honduras"
  ]
  node [
    id 185
    label "Mongolia"
  ]
  node [
    id 186
    label "Portoryko"
  ]
  node [
    id 187
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 188
    label "Serbia"
  ]
  node [
    id 189
    label "Tajwan"
  ]
  node [
    id 190
    label "Wielka_Brytania"
  ]
  node [
    id 191
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 192
    label "Liban"
  ]
  node [
    id 193
    label "Japonia"
  ]
  node [
    id 194
    label "Ghana"
  ]
  node [
    id 195
    label "Bahrajn"
  ]
  node [
    id 196
    label "Belgia"
  ]
  node [
    id 197
    label "Etiopia"
  ]
  node [
    id 198
    label "Mikronezja"
  ]
  node [
    id 199
    label "Kuwejt"
  ]
  node [
    id 200
    label "grupa"
  ]
  node [
    id 201
    label "Bahamy"
  ]
  node [
    id 202
    label "Rosja"
  ]
  node [
    id 203
    label "Mo&#322;dawia"
  ]
  node [
    id 204
    label "Litwa"
  ]
  node [
    id 205
    label "S&#322;owenia"
  ]
  node [
    id 206
    label "Szwajcaria"
  ]
  node [
    id 207
    label "Erytrea"
  ]
  node [
    id 208
    label "Kuba"
  ]
  node [
    id 209
    label "Arabia_Saudyjska"
  ]
  node [
    id 210
    label "granica_pa&#324;stwa"
  ]
  node [
    id 211
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 212
    label "Malezja"
  ]
  node [
    id 213
    label "Korea"
  ]
  node [
    id 214
    label "Jemen"
  ]
  node [
    id 215
    label "Nowa_Zelandia"
  ]
  node [
    id 216
    label "Namibia"
  ]
  node [
    id 217
    label "Nauru"
  ]
  node [
    id 218
    label "holoarktyka"
  ]
  node [
    id 219
    label "Brunei"
  ]
  node [
    id 220
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 221
    label "Khitai"
  ]
  node [
    id 222
    label "Mauretania"
  ]
  node [
    id 223
    label "Iran"
  ]
  node [
    id 224
    label "Gambia"
  ]
  node [
    id 225
    label "Somalia"
  ]
  node [
    id 226
    label "Holandia"
  ]
  node [
    id 227
    label "Turkmenistan"
  ]
  node [
    id 228
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 229
    label "Salwador"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
]
