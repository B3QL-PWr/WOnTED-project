graph [
  node [
    id 0
    label "niemiec"
    origin "text"
  ]
  node [
    id 1
    label "niemiecki"
  ]
  node [
    id 2
    label "po_niemiecku"
  ]
  node [
    id 3
    label "German"
  ]
  node [
    id 4
    label "niemiecko"
  ]
  node [
    id 5
    label "cenar"
  ]
  node [
    id 6
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 7
    label "europejski"
  ]
  node [
    id 8
    label "strudel"
  ]
  node [
    id 9
    label "pionier"
  ]
  node [
    id 10
    label "zachodnioeuropejski"
  ]
  node [
    id 11
    label "j&#281;zyk"
  ]
  node [
    id 12
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 13
    label "junkers"
  ]
  node [
    id 14
    label "szwabski"
  ]
  node [
    id 15
    label "podludzie"
  ]
  node [
    id 16
    label "HERP"
  ]
  node [
    id 17
    label "wielki"
  ]
  node [
    id 18
    label "Polska"
  ]
  node [
    id 19
    label "wstawa&#263;"
  ]
  node [
    id 20
    label "z"
  ]
  node [
    id 21
    label "kolano"
  ]
  node [
    id 22
    label "DERP"
  ]
  node [
    id 23
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 24
    label "Beata"
  ]
  node [
    id 25
    label "szyd&#322;o"
  ]
  node [
    id 26
    label "Niemcy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 15
    target 24
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 15
    target 26
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 17
    target 25
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 18
    target 25
  ]
  edge [
    source 18
    target 26
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 25
    target 26
  ]
]
