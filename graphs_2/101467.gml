graph [
  node [
    id 0
    label "turniej"
    origin "text"
  ]
  node [
    id 1
    label "br&#261;zowy"
    origin "text"
  ]
  node [
    id 2
    label "kask"
    origin "text"
  ]
  node [
    id 3
    label "impreza"
  ]
  node [
    id 4
    label "Wielki_Szlem"
  ]
  node [
    id 5
    label "pojedynek"
  ]
  node [
    id 6
    label "drive"
  ]
  node [
    id 7
    label "runda"
  ]
  node [
    id 8
    label "rywalizacja"
  ]
  node [
    id 9
    label "zawody"
  ]
  node [
    id 10
    label "eliminacje"
  ]
  node [
    id 11
    label "tournament"
  ]
  node [
    id 12
    label "impra"
  ]
  node [
    id 13
    label "rozrywka"
  ]
  node [
    id 14
    label "przyj&#281;cie"
  ]
  node [
    id 15
    label "okazja"
  ]
  node [
    id 16
    label "party"
  ]
  node [
    id 17
    label "contest"
  ]
  node [
    id 18
    label "wydarzenie"
  ]
  node [
    id 19
    label "walczy&#263;"
  ]
  node [
    id 20
    label "walczenie"
  ]
  node [
    id 21
    label "tysi&#281;cznik"
  ]
  node [
    id 22
    label "champion"
  ]
  node [
    id 23
    label "spadochroniarstwo"
  ]
  node [
    id 24
    label "kategoria_open"
  ]
  node [
    id 25
    label "engagement"
  ]
  node [
    id 26
    label "walka"
  ]
  node [
    id 27
    label "wyzwanie"
  ]
  node [
    id 28
    label "wyzwa&#263;"
  ]
  node [
    id 29
    label "odyniec"
  ]
  node [
    id 30
    label "wyzywa&#263;"
  ]
  node [
    id 31
    label "sekundant"
  ]
  node [
    id 32
    label "competitiveness"
  ]
  node [
    id 33
    label "sp&#243;r"
  ]
  node [
    id 34
    label "bout"
  ]
  node [
    id 35
    label "wyzywanie"
  ]
  node [
    id 36
    label "konkurs"
  ]
  node [
    id 37
    label "faza"
  ]
  node [
    id 38
    label "retirement"
  ]
  node [
    id 39
    label "rozgrywka"
  ]
  node [
    id 40
    label "seria"
  ]
  node [
    id 41
    label "rhythm"
  ]
  node [
    id 42
    label "czas"
  ]
  node [
    id 43
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 44
    label "okr&#261;&#380;enie"
  ]
  node [
    id 45
    label "br&#261;zowienie"
  ]
  node [
    id 46
    label "metaliczny"
  ]
  node [
    id 47
    label "opalony"
  ]
  node [
    id 48
    label "ciemny"
  ]
  node [
    id 49
    label "ciep&#322;y"
  ]
  node [
    id 50
    label "utytu&#322;owany"
  ]
  node [
    id 51
    label "br&#261;zowo"
  ]
  node [
    id 52
    label "opalenie_si&#281;"
  ]
  node [
    id 53
    label "opalanie_si&#281;"
  ]
  node [
    id 54
    label "&#347;niady"
  ]
  node [
    id 55
    label "opalanie"
  ]
  node [
    id 56
    label "typowy"
  ]
  node [
    id 57
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 58
    label "metaloplastyczny"
  ]
  node [
    id 59
    label "metalicznie"
  ]
  node [
    id 60
    label "podejrzanie"
  ]
  node [
    id 61
    label "&#263;my"
  ]
  node [
    id 62
    label "ciemnow&#322;osy"
  ]
  node [
    id 63
    label "pe&#322;ny"
  ]
  node [
    id 64
    label "nierozumny"
  ]
  node [
    id 65
    label "ciemno"
  ]
  node [
    id 66
    label "g&#322;upi"
  ]
  node [
    id 67
    label "zdrowy"
  ]
  node [
    id 68
    label "zacofany"
  ]
  node [
    id 69
    label "t&#281;py"
  ]
  node [
    id 70
    label "niewykszta&#322;cony"
  ]
  node [
    id 71
    label "nieprzejrzysty"
  ]
  node [
    id 72
    label "niepewny"
  ]
  node [
    id 73
    label "z&#322;y"
  ]
  node [
    id 74
    label "prominentny"
  ]
  node [
    id 75
    label "znany"
  ]
  node [
    id 76
    label "wybitny"
  ]
  node [
    id 77
    label "mi&#322;y"
  ]
  node [
    id 78
    label "ocieplanie_si&#281;"
  ]
  node [
    id 79
    label "ocieplanie"
  ]
  node [
    id 80
    label "grzanie"
  ]
  node [
    id 81
    label "ocieplenie_si&#281;"
  ]
  node [
    id 82
    label "zagrzanie"
  ]
  node [
    id 83
    label "ocieplenie"
  ]
  node [
    id 84
    label "korzystny"
  ]
  node [
    id 85
    label "przyjemny"
  ]
  node [
    id 86
    label "ciep&#322;o"
  ]
  node [
    id 87
    label "dobry"
  ]
  node [
    id 88
    label "&#347;niado"
  ]
  node [
    id 89
    label "idealizowanie"
  ]
  node [
    id 90
    label "odcinanie_si&#281;"
  ]
  node [
    id 91
    label "barwienie"
  ]
  node [
    id 92
    label "barwienie_si&#281;"
  ]
  node [
    id 93
    label "he&#322;m"
  ]
  node [
    id 94
    label "zwie&#324;czenie"
  ]
  node [
    id 95
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 96
    label "grzebie&#324;"
  ]
  node [
    id 97
    label "grzebie&#324;_he&#322;mu"
  ]
  node [
    id 98
    label "wie&#380;a"
  ]
  node [
    id 99
    label "daszek"
  ]
  node [
    id 100
    label "dach"
  ]
  node [
    id 101
    label "bro&#324;_ochronna"
  ]
  node [
    id 102
    label "labry"
  ]
  node [
    id 103
    label "podbr&#243;dek"
  ]
  node [
    id 104
    label "ubranie_ochronne"
  ]
  node [
    id 105
    label "nausznik"
  ]
  node [
    id 106
    label "bro&#324;"
  ]
  node [
    id 107
    label "ojciec"
  ]
  node [
    id 108
    label "2010"
  ]
  node [
    id 109
    label "polski"
  ]
  node [
    id 110
    label "zwi&#261;zka"
  ]
  node [
    id 111
    label "motorowy"
  ]
  node [
    id 112
    label "Patryk"
  ]
  node [
    id 113
    label "dudka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 111
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 112
    target 113
  ]
]
