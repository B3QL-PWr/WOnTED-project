graph [
  node [
    id 0
    label "ktora"
    origin "text"
  ]
  node [
    id 1
    label "rozowa"
    origin "text"
  ]
  node [
    id 2
    label "wrzucila"
    origin "text"
  ]
  node [
    id 3
    label "zaraz"
    origin "text"
  ]
  node [
    id 4
    label "usunela"
    origin "text"
  ]
  node [
    id 5
    label "nied&#322;ugo"
  ]
  node [
    id 6
    label "blisko"
  ]
  node [
    id 7
    label "zara"
  ]
  node [
    id 8
    label "wpr&#281;dce"
  ]
  node [
    id 9
    label "nied&#322;ugi"
  ]
  node [
    id 10
    label "kr&#243;tki"
  ]
  node [
    id 11
    label "dok&#322;adnie"
  ]
  node [
    id 12
    label "bliski"
  ]
  node [
    id 13
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 14
    label "silnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
]
