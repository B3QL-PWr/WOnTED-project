graph [
  node [
    id 0
    label "g&#322;up"
    origin "text"
  ]
  node [
    id 1
    label "szmul"
    origin "text"
  ]
  node [
    id 2
    label "tinder"
    origin "text"
  ]
  node [
    id 3
    label "heheszki"
    origin "text"
  ]
  node [
    id 4
    label "g&#322;owa"
  ]
  node [
    id 5
    label "g&#322;upek"
  ]
  node [
    id 6
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 7
    label "zdolno&#347;&#263;"
  ]
  node [
    id 8
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 9
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 10
    label "umys&#322;"
  ]
  node [
    id 11
    label "kierowa&#263;"
  ]
  node [
    id 12
    label "obiekt"
  ]
  node [
    id 13
    label "sztuka"
  ]
  node [
    id 14
    label "czaszka"
  ]
  node [
    id 15
    label "g&#243;ra"
  ]
  node [
    id 16
    label "wiedza"
  ]
  node [
    id 17
    label "fryzura"
  ]
  node [
    id 18
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 19
    label "pryncypa&#322;"
  ]
  node [
    id 20
    label "ro&#347;lina"
  ]
  node [
    id 21
    label "ucho"
  ]
  node [
    id 22
    label "byd&#322;o"
  ]
  node [
    id 23
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 24
    label "alkohol"
  ]
  node [
    id 25
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 26
    label "kierownictwo"
  ]
  node [
    id 27
    label "&#347;ci&#281;cie"
  ]
  node [
    id 28
    label "cz&#322;owiek"
  ]
  node [
    id 29
    label "cz&#322;onek"
  ]
  node [
    id 30
    label "makrocefalia"
  ]
  node [
    id 31
    label "cecha"
  ]
  node [
    id 32
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 33
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 34
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 35
    label "&#380;ycie"
  ]
  node [
    id 36
    label "dekiel"
  ]
  node [
    id 37
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 38
    label "m&#243;zg"
  ]
  node [
    id 39
    label "&#347;ci&#281;gno"
  ]
  node [
    id 40
    label "cia&#322;o"
  ]
  node [
    id 41
    label "kszta&#322;t"
  ]
  node [
    id 42
    label "noosfera"
  ]
  node [
    id 43
    label "g&#322;upienie"
  ]
  node [
    id 44
    label "istota_&#380;ywa"
  ]
  node [
    id 45
    label "g&#322;uptas"
  ]
  node [
    id 46
    label "g&#322;upiec"
  ]
  node [
    id 47
    label "mondzio&#322;"
  ]
  node [
    id 48
    label "zg&#322;upienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
