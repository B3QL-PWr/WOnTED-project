graph [
  node [
    id 0
    label "zawsze"
    origin "text"
  ]
  node [
    id 1
    label "bawi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "tak"
    origin "text"
  ]
  node [
    id 3
    label "samo"
    origin "text"
  ]
  node [
    id 4
    label "thebestofmirko"
    origin "text"
  ]
  node [
    id 5
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 6
    label "cz&#281;sto"
  ]
  node [
    id 7
    label "ci&#261;gle"
  ]
  node [
    id 8
    label "zaw&#380;dy"
  ]
  node [
    id 9
    label "na_zawsze"
  ]
  node [
    id 10
    label "cz&#281;sty"
  ]
  node [
    id 11
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 12
    label "stale"
  ]
  node [
    id 13
    label "ci&#261;g&#322;y"
  ]
  node [
    id 14
    label "nieprzerwanie"
  ]
  node [
    id 15
    label "play"
  ]
  node [
    id 16
    label "zajmowa&#263;"
  ]
  node [
    id 17
    label "amuse"
  ]
  node [
    id 18
    label "przebywa&#263;"
  ]
  node [
    id 19
    label "ubawia&#263;"
  ]
  node [
    id 20
    label "zabawia&#263;"
  ]
  node [
    id 21
    label "wzbudza&#263;"
  ]
  node [
    id 22
    label "sprawia&#263;"
  ]
  node [
    id 23
    label "go"
  ]
  node [
    id 24
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 25
    label "kupywa&#263;"
  ]
  node [
    id 26
    label "bra&#263;"
  ]
  node [
    id 27
    label "bind"
  ]
  node [
    id 28
    label "get"
  ]
  node [
    id 29
    label "act"
  ]
  node [
    id 30
    label "powodowa&#263;"
  ]
  node [
    id 31
    label "przygotowywa&#263;"
  ]
  node [
    id 32
    label "tkwi&#263;"
  ]
  node [
    id 33
    label "istnie&#263;"
  ]
  node [
    id 34
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 35
    label "pause"
  ]
  node [
    id 36
    label "przestawa&#263;"
  ]
  node [
    id 37
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 38
    label "hesitate"
  ]
  node [
    id 39
    label "dostarcza&#263;"
  ]
  node [
    id 40
    label "robi&#263;"
  ]
  node [
    id 41
    label "korzysta&#263;"
  ]
  node [
    id 42
    label "schorzenie"
  ]
  node [
    id 43
    label "komornik"
  ]
  node [
    id 44
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 45
    label "return"
  ]
  node [
    id 46
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 47
    label "trwa&#263;"
  ]
  node [
    id 48
    label "rozciekawia&#263;"
  ]
  node [
    id 49
    label "klasyfikacja"
  ]
  node [
    id 50
    label "zadawa&#263;"
  ]
  node [
    id 51
    label "fill"
  ]
  node [
    id 52
    label "zabiera&#263;"
  ]
  node [
    id 53
    label "topographic_point"
  ]
  node [
    id 54
    label "obejmowa&#263;"
  ]
  node [
    id 55
    label "pali&#263;_si&#281;"
  ]
  node [
    id 56
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 57
    label "aim"
  ]
  node [
    id 58
    label "anektowa&#263;"
  ]
  node [
    id 59
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 60
    label "prosecute"
  ]
  node [
    id 61
    label "sake"
  ]
  node [
    id 62
    label "do"
  ]
  node [
    id 63
    label "roz&#347;miesza&#263;"
  ]
  node [
    id 64
    label "absorbowa&#263;"
  ]
  node [
    id 65
    label "sp&#281;dza&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
]
