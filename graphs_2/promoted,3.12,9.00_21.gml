graph [
  node [
    id 0
    label "przewodzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kraj"
    origin "text"
  ]
  node [
    id 2
    label "bit"
    origin "text"
  ]
  node [
    id 3
    label "zsrr"
    origin "text"
  ]
  node [
    id 4
    label "przekazywa&#263;"
  ]
  node [
    id 5
    label "neuron"
  ]
  node [
    id 6
    label "manipulate"
  ]
  node [
    id 7
    label "&#322;yko"
  ]
  node [
    id 8
    label "preside"
  ]
  node [
    id 9
    label "control"
  ]
  node [
    id 10
    label "prowadzi&#263;"
  ]
  node [
    id 11
    label "przepuszcza&#263;"
  ]
  node [
    id 12
    label "bodziec"
  ]
  node [
    id 13
    label "doprowadza&#263;"
  ]
  node [
    id 14
    label "rig"
  ]
  node [
    id 15
    label "message"
  ]
  node [
    id 16
    label "wykonywa&#263;"
  ]
  node [
    id 17
    label "deliver"
  ]
  node [
    id 18
    label "powodowa&#263;"
  ]
  node [
    id 19
    label "wzbudza&#263;"
  ]
  node [
    id 20
    label "moderate"
  ]
  node [
    id 21
    label "wprowadza&#263;"
  ]
  node [
    id 22
    label "trwoni&#263;"
  ]
  node [
    id 23
    label "obrabia&#263;"
  ]
  node [
    id 24
    label "puszcza&#263;"
  ]
  node [
    id 25
    label "przeocza&#263;"
  ]
  node [
    id 26
    label "base_on_balls"
  ]
  node [
    id 27
    label "omija&#263;"
  ]
  node [
    id 28
    label "ust&#281;powa&#263;"
  ]
  node [
    id 29
    label "pob&#322;a&#380;a&#263;"
  ]
  node [
    id 30
    label "darowywa&#263;"
  ]
  node [
    id 31
    label "wpuszcza&#263;"
  ]
  node [
    id 32
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 33
    label "authorize"
  ]
  node [
    id 34
    label "&#380;y&#263;"
  ]
  node [
    id 35
    label "robi&#263;"
  ]
  node [
    id 36
    label "kierowa&#263;"
  ]
  node [
    id 37
    label "g&#243;rowa&#263;"
  ]
  node [
    id 38
    label "tworzy&#263;"
  ]
  node [
    id 39
    label "krzywa"
  ]
  node [
    id 40
    label "linia_melodyczna"
  ]
  node [
    id 41
    label "string"
  ]
  node [
    id 42
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 43
    label "ukierunkowywa&#263;"
  ]
  node [
    id 44
    label "sterowa&#263;"
  ]
  node [
    id 45
    label "kre&#347;li&#263;"
  ]
  node [
    id 46
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 47
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 48
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 49
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 50
    label "eksponowa&#263;"
  ]
  node [
    id 51
    label "navigate"
  ]
  node [
    id 52
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 53
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 54
    label "przesuwa&#263;"
  ]
  node [
    id 55
    label "partner"
  ]
  node [
    id 56
    label "prowadzenie"
  ]
  node [
    id 57
    label "wysy&#322;a&#263;"
  ]
  node [
    id 58
    label "podawa&#263;"
  ]
  node [
    id 59
    label "give"
  ]
  node [
    id 60
    label "wp&#322;aca&#263;"
  ]
  node [
    id 61
    label "sygna&#322;"
  ]
  node [
    id 62
    label "impart"
  ]
  node [
    id 63
    label "krzew"
  ]
  node [
    id 64
    label "drzewo"
  ]
  node [
    id 65
    label "przewodzenie"
  ]
  node [
    id 66
    label "tkanka_sta&#322;a"
  ]
  node [
    id 67
    label "pobudka"
  ]
  node [
    id 68
    label "ankus"
  ]
  node [
    id 69
    label "przedmiot"
  ]
  node [
    id 70
    label "czynnik"
  ]
  node [
    id 71
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 72
    label "doj&#347;cie"
  ]
  node [
    id 73
    label "zach&#281;ta"
  ]
  node [
    id 74
    label "drift"
  ]
  node [
    id 75
    label "o&#347;cie&#324;"
  ]
  node [
    id 76
    label "doj&#347;&#263;"
  ]
  node [
    id 77
    label "substancja_szara"
  ]
  node [
    id 78
    label "dendryt"
  ]
  node [
    id 79
    label "otoczka_mielinowa"
  ]
  node [
    id 80
    label "pole"
  ]
  node [
    id 81
    label "neuroprzeka&#378;nik"
  ]
  node [
    id 82
    label "nerve_cell"
  ]
  node [
    id 83
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 84
    label "substancja_bia&#322;a"
  ]
  node [
    id 85
    label "neurofibryl"
  ]
  node [
    id 86
    label "zw&#243;j_nerwowy"
  ]
  node [
    id 87
    label "tkanka_nerwowa"
  ]
  node [
    id 88
    label "akson"
  ]
  node [
    id 89
    label "kom&#243;rka_zwierz&#281;ca"
  ]
  node [
    id 90
    label "klawisz"
  ]
  node [
    id 91
    label "Katar"
  ]
  node [
    id 92
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 93
    label "Mazowsze"
  ]
  node [
    id 94
    label "Libia"
  ]
  node [
    id 95
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 96
    label "Gwatemala"
  ]
  node [
    id 97
    label "Anglia"
  ]
  node [
    id 98
    label "Amazonia"
  ]
  node [
    id 99
    label "Afganistan"
  ]
  node [
    id 100
    label "Ekwador"
  ]
  node [
    id 101
    label "Bordeaux"
  ]
  node [
    id 102
    label "Tad&#380;ykistan"
  ]
  node [
    id 103
    label "Bhutan"
  ]
  node [
    id 104
    label "Argentyna"
  ]
  node [
    id 105
    label "D&#380;ibuti"
  ]
  node [
    id 106
    label "Wenezuela"
  ]
  node [
    id 107
    label "Ukraina"
  ]
  node [
    id 108
    label "Gabon"
  ]
  node [
    id 109
    label "Naddniestrze"
  ]
  node [
    id 110
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 111
    label "Europa_Zachodnia"
  ]
  node [
    id 112
    label "Armagnac"
  ]
  node [
    id 113
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 114
    label "Rwanda"
  ]
  node [
    id 115
    label "Liechtenstein"
  ]
  node [
    id 116
    label "Amhara"
  ]
  node [
    id 117
    label "organizacja"
  ]
  node [
    id 118
    label "Sri_Lanka"
  ]
  node [
    id 119
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 120
    label "Zamojszczyzna"
  ]
  node [
    id 121
    label "Madagaskar"
  ]
  node [
    id 122
    label "Tonga"
  ]
  node [
    id 123
    label "Kongo"
  ]
  node [
    id 124
    label "Bangladesz"
  ]
  node [
    id 125
    label "Kanada"
  ]
  node [
    id 126
    label "Ma&#322;opolska"
  ]
  node [
    id 127
    label "Wehrlen"
  ]
  node [
    id 128
    label "Turkiestan"
  ]
  node [
    id 129
    label "Algieria"
  ]
  node [
    id 130
    label "Noworosja"
  ]
  node [
    id 131
    label "Surinam"
  ]
  node [
    id 132
    label "Chile"
  ]
  node [
    id 133
    label "Sahara_Zachodnia"
  ]
  node [
    id 134
    label "Uganda"
  ]
  node [
    id 135
    label "Lubelszczyzna"
  ]
  node [
    id 136
    label "W&#281;gry"
  ]
  node [
    id 137
    label "Mezoameryka"
  ]
  node [
    id 138
    label "Birma"
  ]
  node [
    id 139
    label "Ba&#322;kany"
  ]
  node [
    id 140
    label "Kurdystan"
  ]
  node [
    id 141
    label "Kazachstan"
  ]
  node [
    id 142
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 143
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 144
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 145
    label "Armenia"
  ]
  node [
    id 146
    label "Tuwalu"
  ]
  node [
    id 147
    label "Timor_Wschodni"
  ]
  node [
    id 148
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 149
    label "Szkocja"
  ]
  node [
    id 150
    label "Baszkiria"
  ]
  node [
    id 151
    label "Tonkin"
  ]
  node [
    id 152
    label "Maghreb"
  ]
  node [
    id 153
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 154
    label "Izrael"
  ]
  node [
    id 155
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 156
    label "Nadrenia"
  ]
  node [
    id 157
    label "Estonia"
  ]
  node [
    id 158
    label "Komory"
  ]
  node [
    id 159
    label "Podhale"
  ]
  node [
    id 160
    label "Wielkopolska"
  ]
  node [
    id 161
    label "Zabajkale"
  ]
  node [
    id 162
    label "Kamerun"
  ]
  node [
    id 163
    label "Haiti"
  ]
  node [
    id 164
    label "Belize"
  ]
  node [
    id 165
    label "Sierra_Leone"
  ]
  node [
    id 166
    label "Apulia"
  ]
  node [
    id 167
    label "Luksemburg"
  ]
  node [
    id 168
    label "brzeg"
  ]
  node [
    id 169
    label "USA"
  ]
  node [
    id 170
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 171
    label "Barbados"
  ]
  node [
    id 172
    label "San_Marino"
  ]
  node [
    id 173
    label "Bu&#322;garia"
  ]
  node [
    id 174
    label "Wietnam"
  ]
  node [
    id 175
    label "Indonezja"
  ]
  node [
    id 176
    label "Bojkowszczyzna"
  ]
  node [
    id 177
    label "Malawi"
  ]
  node [
    id 178
    label "Francja"
  ]
  node [
    id 179
    label "Zambia"
  ]
  node [
    id 180
    label "Kujawy"
  ]
  node [
    id 181
    label "Angola"
  ]
  node [
    id 182
    label "Liguria"
  ]
  node [
    id 183
    label "Grenada"
  ]
  node [
    id 184
    label "Pamir"
  ]
  node [
    id 185
    label "Nepal"
  ]
  node [
    id 186
    label "Panama"
  ]
  node [
    id 187
    label "Rumunia"
  ]
  node [
    id 188
    label "Indochiny"
  ]
  node [
    id 189
    label "Podlasie"
  ]
  node [
    id 190
    label "Polinezja"
  ]
  node [
    id 191
    label "Kurpie"
  ]
  node [
    id 192
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 193
    label "S&#261;decczyzna"
  ]
  node [
    id 194
    label "Umbria"
  ]
  node [
    id 195
    label "Czarnog&#243;ra"
  ]
  node [
    id 196
    label "Malediwy"
  ]
  node [
    id 197
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 198
    label "S&#322;owacja"
  ]
  node [
    id 199
    label "Karaiby"
  ]
  node [
    id 200
    label "Ukraina_Zachodnia"
  ]
  node [
    id 201
    label "Kielecczyzna"
  ]
  node [
    id 202
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 203
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 204
    label "Egipt"
  ]
  node [
    id 205
    label "Kolumbia"
  ]
  node [
    id 206
    label "Mozambik"
  ]
  node [
    id 207
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 208
    label "Laos"
  ]
  node [
    id 209
    label "Burundi"
  ]
  node [
    id 210
    label "Suazi"
  ]
  node [
    id 211
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 212
    label "Czechy"
  ]
  node [
    id 213
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 214
    label "Wyspy_Marshalla"
  ]
  node [
    id 215
    label "Trynidad_i_Tobago"
  ]
  node [
    id 216
    label "Dominika"
  ]
  node [
    id 217
    label "Palau"
  ]
  node [
    id 218
    label "Syria"
  ]
  node [
    id 219
    label "Skandynawia"
  ]
  node [
    id 220
    label "Gwinea_Bissau"
  ]
  node [
    id 221
    label "Liberia"
  ]
  node [
    id 222
    label "Zimbabwe"
  ]
  node [
    id 223
    label "Polska"
  ]
  node [
    id 224
    label "Jamajka"
  ]
  node [
    id 225
    label "Tyrol"
  ]
  node [
    id 226
    label "Huculszczyzna"
  ]
  node [
    id 227
    label "Bory_Tucholskie"
  ]
  node [
    id 228
    label "Turyngia"
  ]
  node [
    id 229
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 230
    label "Dominikana"
  ]
  node [
    id 231
    label "Senegal"
  ]
  node [
    id 232
    label "Gruzja"
  ]
  node [
    id 233
    label "Chorwacja"
  ]
  node [
    id 234
    label "Togo"
  ]
  node [
    id 235
    label "Meksyk"
  ]
  node [
    id 236
    label "jednostka_administracyjna"
  ]
  node [
    id 237
    label "Macedonia"
  ]
  node [
    id 238
    label "Gujana"
  ]
  node [
    id 239
    label "Zair"
  ]
  node [
    id 240
    label "Kambod&#380;a"
  ]
  node [
    id 241
    label "Albania"
  ]
  node [
    id 242
    label "Mauritius"
  ]
  node [
    id 243
    label "Monako"
  ]
  node [
    id 244
    label "Gwinea"
  ]
  node [
    id 245
    label "Mali"
  ]
  node [
    id 246
    label "Nigeria"
  ]
  node [
    id 247
    label "Kalabria"
  ]
  node [
    id 248
    label "Hercegowina"
  ]
  node [
    id 249
    label "Kostaryka"
  ]
  node [
    id 250
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 251
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 252
    label "Lotaryngia"
  ]
  node [
    id 253
    label "Hanower"
  ]
  node [
    id 254
    label "Paragwaj"
  ]
  node [
    id 255
    label "W&#322;ochy"
  ]
  node [
    id 256
    label "Wyspy_Salomona"
  ]
  node [
    id 257
    label "Seszele"
  ]
  node [
    id 258
    label "Hiszpania"
  ]
  node [
    id 259
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 260
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 261
    label "Walia"
  ]
  node [
    id 262
    label "Boliwia"
  ]
  node [
    id 263
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 264
    label "Opolskie"
  ]
  node [
    id 265
    label "Kirgistan"
  ]
  node [
    id 266
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 267
    label "Irlandia"
  ]
  node [
    id 268
    label "Kampania"
  ]
  node [
    id 269
    label "Czad"
  ]
  node [
    id 270
    label "Irak"
  ]
  node [
    id 271
    label "Lesoto"
  ]
  node [
    id 272
    label "Malta"
  ]
  node [
    id 273
    label "Andora"
  ]
  node [
    id 274
    label "Sand&#380;ak"
  ]
  node [
    id 275
    label "Chiny"
  ]
  node [
    id 276
    label "Filipiny"
  ]
  node [
    id 277
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 278
    label "Syjon"
  ]
  node [
    id 279
    label "Niemcy"
  ]
  node [
    id 280
    label "Kabylia"
  ]
  node [
    id 281
    label "Lombardia"
  ]
  node [
    id 282
    label "Warmia"
  ]
  node [
    id 283
    label "Brazylia"
  ]
  node [
    id 284
    label "Nikaragua"
  ]
  node [
    id 285
    label "Pakistan"
  ]
  node [
    id 286
    label "&#321;emkowszczyzna"
  ]
  node [
    id 287
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 288
    label "Kaszmir"
  ]
  node [
    id 289
    label "Kenia"
  ]
  node [
    id 290
    label "Niger"
  ]
  node [
    id 291
    label "Tunezja"
  ]
  node [
    id 292
    label "Portugalia"
  ]
  node [
    id 293
    label "Fid&#380;i"
  ]
  node [
    id 294
    label "Maroko"
  ]
  node [
    id 295
    label "Botswana"
  ]
  node [
    id 296
    label "Tajlandia"
  ]
  node [
    id 297
    label "Australia"
  ]
  node [
    id 298
    label "&#321;&#243;dzkie"
  ]
  node [
    id 299
    label "Europa_Wschodnia"
  ]
  node [
    id 300
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 301
    label "Burkina_Faso"
  ]
  node [
    id 302
    label "Benin"
  ]
  node [
    id 303
    label "Tanzania"
  ]
  node [
    id 304
    label "interior"
  ]
  node [
    id 305
    label "Indie"
  ]
  node [
    id 306
    label "&#321;otwa"
  ]
  node [
    id 307
    label "Biskupizna"
  ]
  node [
    id 308
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 309
    label "Kiribati"
  ]
  node [
    id 310
    label "Kaukaz"
  ]
  node [
    id 311
    label "Antigua_i_Barbuda"
  ]
  node [
    id 312
    label "Rodezja"
  ]
  node [
    id 313
    label "Afryka_Wschodnia"
  ]
  node [
    id 314
    label "Cypr"
  ]
  node [
    id 315
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 316
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 317
    label "Podkarpacie"
  ]
  node [
    id 318
    label "obszar"
  ]
  node [
    id 319
    label "Peru"
  ]
  node [
    id 320
    label "Toskania"
  ]
  node [
    id 321
    label "Afryka_Zachodnia"
  ]
  node [
    id 322
    label "Austria"
  ]
  node [
    id 323
    label "Podbeskidzie"
  ]
  node [
    id 324
    label "Urugwaj"
  ]
  node [
    id 325
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 326
    label "Jordania"
  ]
  node [
    id 327
    label "Bo&#347;nia"
  ]
  node [
    id 328
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 329
    label "Grecja"
  ]
  node [
    id 330
    label "Azerbejd&#380;an"
  ]
  node [
    id 331
    label "Oceania"
  ]
  node [
    id 332
    label "Turcja"
  ]
  node [
    id 333
    label "Pomorze_Zachodnie"
  ]
  node [
    id 334
    label "Samoa"
  ]
  node [
    id 335
    label "Powi&#347;le"
  ]
  node [
    id 336
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 337
    label "ziemia"
  ]
  node [
    id 338
    label "Oman"
  ]
  node [
    id 339
    label "Sudan"
  ]
  node [
    id 340
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 341
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 342
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 343
    label "Uzbekistan"
  ]
  node [
    id 344
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 345
    label "Honduras"
  ]
  node [
    id 346
    label "Mongolia"
  ]
  node [
    id 347
    label "Portoryko"
  ]
  node [
    id 348
    label "Kaszuby"
  ]
  node [
    id 349
    label "Ko&#322;yma"
  ]
  node [
    id 350
    label "Szlezwik"
  ]
  node [
    id 351
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 352
    label "Serbia"
  ]
  node [
    id 353
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 354
    label "Tajwan"
  ]
  node [
    id 355
    label "Wielka_Brytania"
  ]
  node [
    id 356
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 357
    label "Liban"
  ]
  node [
    id 358
    label "Japonia"
  ]
  node [
    id 359
    label "Ghana"
  ]
  node [
    id 360
    label "Bahrajn"
  ]
  node [
    id 361
    label "Belgia"
  ]
  node [
    id 362
    label "Etiopia"
  ]
  node [
    id 363
    label "Mikronezja"
  ]
  node [
    id 364
    label "Polesie"
  ]
  node [
    id 365
    label "Kuwejt"
  ]
  node [
    id 366
    label "Kerala"
  ]
  node [
    id 367
    label "Mazury"
  ]
  node [
    id 368
    label "Bahamy"
  ]
  node [
    id 369
    label "Rosja"
  ]
  node [
    id 370
    label "Mo&#322;dawia"
  ]
  node [
    id 371
    label "Palestyna"
  ]
  node [
    id 372
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 373
    label "Lauda"
  ]
  node [
    id 374
    label "Azja_Wschodnia"
  ]
  node [
    id 375
    label "Litwa"
  ]
  node [
    id 376
    label "S&#322;owenia"
  ]
  node [
    id 377
    label "Szwajcaria"
  ]
  node [
    id 378
    label "Erytrea"
  ]
  node [
    id 379
    label "Lubuskie"
  ]
  node [
    id 380
    label "Kuba"
  ]
  node [
    id 381
    label "Arabia_Saudyjska"
  ]
  node [
    id 382
    label "Galicja"
  ]
  node [
    id 383
    label "Zakarpacie"
  ]
  node [
    id 384
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 385
    label "Laponia"
  ]
  node [
    id 386
    label "granica_pa&#324;stwa"
  ]
  node [
    id 387
    label "Malezja"
  ]
  node [
    id 388
    label "Korea"
  ]
  node [
    id 389
    label "Yorkshire"
  ]
  node [
    id 390
    label "Bawaria"
  ]
  node [
    id 391
    label "Zag&#243;rze"
  ]
  node [
    id 392
    label "Jemen"
  ]
  node [
    id 393
    label "Nowa_Zelandia"
  ]
  node [
    id 394
    label "Andaluzja"
  ]
  node [
    id 395
    label "Namibia"
  ]
  node [
    id 396
    label "Nauru"
  ]
  node [
    id 397
    label "&#379;ywiecczyzna"
  ]
  node [
    id 398
    label "Brunei"
  ]
  node [
    id 399
    label "Oksytania"
  ]
  node [
    id 400
    label "Opolszczyzna"
  ]
  node [
    id 401
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 402
    label "Kociewie"
  ]
  node [
    id 403
    label "Khitai"
  ]
  node [
    id 404
    label "Mauretania"
  ]
  node [
    id 405
    label "Iran"
  ]
  node [
    id 406
    label "Gambia"
  ]
  node [
    id 407
    label "Somalia"
  ]
  node [
    id 408
    label "Holandia"
  ]
  node [
    id 409
    label "Lasko"
  ]
  node [
    id 410
    label "Turkmenistan"
  ]
  node [
    id 411
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 412
    label "Salwador"
  ]
  node [
    id 413
    label "woda"
  ]
  node [
    id 414
    label "linia"
  ]
  node [
    id 415
    label "zbi&#243;r"
  ]
  node [
    id 416
    label "ekoton"
  ]
  node [
    id 417
    label "str&#261;d"
  ]
  node [
    id 418
    label "koniec"
  ]
  node [
    id 419
    label "plantowa&#263;"
  ]
  node [
    id 420
    label "zapadnia"
  ]
  node [
    id 421
    label "budynek"
  ]
  node [
    id 422
    label "skorupa_ziemska"
  ]
  node [
    id 423
    label "glinowanie"
  ]
  node [
    id 424
    label "martwica"
  ]
  node [
    id 425
    label "teren"
  ]
  node [
    id 426
    label "litosfera"
  ]
  node [
    id 427
    label "penetrator"
  ]
  node [
    id 428
    label "glinowa&#263;"
  ]
  node [
    id 429
    label "domain"
  ]
  node [
    id 430
    label "podglebie"
  ]
  node [
    id 431
    label "kompleks_sorpcyjny"
  ]
  node [
    id 432
    label "miejsce"
  ]
  node [
    id 433
    label "kort"
  ]
  node [
    id 434
    label "czynnik_produkcji"
  ]
  node [
    id 435
    label "pojazd"
  ]
  node [
    id 436
    label "powierzchnia"
  ]
  node [
    id 437
    label "pr&#243;chnica"
  ]
  node [
    id 438
    label "pomieszczenie"
  ]
  node [
    id 439
    label "ryzosfera"
  ]
  node [
    id 440
    label "p&#322;aszczyzna"
  ]
  node [
    id 441
    label "dotleni&#263;"
  ]
  node [
    id 442
    label "glej"
  ]
  node [
    id 443
    label "pa&#324;stwo"
  ]
  node [
    id 444
    label "posadzka"
  ]
  node [
    id 445
    label "geosystem"
  ]
  node [
    id 446
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 447
    label "przestrze&#324;"
  ]
  node [
    id 448
    label "podmiot"
  ]
  node [
    id 449
    label "jednostka_organizacyjna"
  ]
  node [
    id 450
    label "struktura"
  ]
  node [
    id 451
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 452
    label "TOPR"
  ]
  node [
    id 453
    label "endecki"
  ]
  node [
    id 454
    label "zesp&#243;&#322;"
  ]
  node [
    id 455
    label "przedstawicielstwo"
  ]
  node [
    id 456
    label "od&#322;am"
  ]
  node [
    id 457
    label "Cepelia"
  ]
  node [
    id 458
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 459
    label "ZBoWiD"
  ]
  node [
    id 460
    label "organization"
  ]
  node [
    id 461
    label "centrala"
  ]
  node [
    id 462
    label "GOPR"
  ]
  node [
    id 463
    label "ZOMO"
  ]
  node [
    id 464
    label "ZMP"
  ]
  node [
    id 465
    label "komitet_koordynacyjny"
  ]
  node [
    id 466
    label "przybud&#243;wka"
  ]
  node [
    id 467
    label "boj&#243;wka"
  ]
  node [
    id 468
    label "p&#243;&#322;noc"
  ]
  node [
    id 469
    label "Kosowo"
  ]
  node [
    id 470
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 471
    label "Zab&#322;ocie"
  ]
  node [
    id 472
    label "zach&#243;d"
  ]
  node [
    id 473
    label "po&#322;udnie"
  ]
  node [
    id 474
    label "Pow&#261;zki"
  ]
  node [
    id 475
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 476
    label "Piotrowo"
  ]
  node [
    id 477
    label "Olszanica"
  ]
  node [
    id 478
    label "holarktyka"
  ]
  node [
    id 479
    label "Ruda_Pabianicka"
  ]
  node [
    id 480
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 481
    label "Ludwin&#243;w"
  ]
  node [
    id 482
    label "Arktyka"
  ]
  node [
    id 483
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 484
    label "Zabu&#380;e"
  ]
  node [
    id 485
    label "antroposfera"
  ]
  node [
    id 486
    label "terytorium"
  ]
  node [
    id 487
    label "Neogea"
  ]
  node [
    id 488
    label "Syberia_Zachodnia"
  ]
  node [
    id 489
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 490
    label "zakres"
  ]
  node [
    id 491
    label "pas_planetoid"
  ]
  node [
    id 492
    label "Syberia_Wschodnia"
  ]
  node [
    id 493
    label "Antarktyka"
  ]
  node [
    id 494
    label "Rakowice"
  ]
  node [
    id 495
    label "akrecja"
  ]
  node [
    id 496
    label "wymiar"
  ]
  node [
    id 497
    label "&#321;&#281;g"
  ]
  node [
    id 498
    label "Kresy_Zachodnie"
  ]
  node [
    id 499
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 500
    label "wsch&#243;d"
  ]
  node [
    id 501
    label "Notogea"
  ]
  node [
    id 502
    label "inti"
  ]
  node [
    id 503
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 504
    label "sol"
  ]
  node [
    id 505
    label "baht"
  ]
  node [
    id 506
    label "boliviano"
  ]
  node [
    id 507
    label "dong"
  ]
  node [
    id 508
    label "Annam"
  ]
  node [
    id 509
    label "colon"
  ]
  node [
    id 510
    label "Ameryka_Centralna"
  ]
  node [
    id 511
    label "Piemont"
  ]
  node [
    id 512
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 513
    label "NATO"
  ]
  node [
    id 514
    label "Sardynia"
  ]
  node [
    id 515
    label "Italia"
  ]
  node [
    id 516
    label "strefa_euro"
  ]
  node [
    id 517
    label "Ok&#281;cie"
  ]
  node [
    id 518
    label "Karyntia"
  ]
  node [
    id 519
    label "Romania"
  ]
  node [
    id 520
    label "Warszawa"
  ]
  node [
    id 521
    label "lir"
  ]
  node [
    id 522
    label "Sycylia"
  ]
  node [
    id 523
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 524
    label "Ad&#380;aria"
  ]
  node [
    id 525
    label "lari"
  ]
  node [
    id 526
    label "Jukatan"
  ]
  node [
    id 527
    label "dolar_Belize"
  ]
  node [
    id 528
    label "dolar"
  ]
  node [
    id 529
    label "Ohio"
  ]
  node [
    id 530
    label "P&#243;&#322;noc"
  ]
  node [
    id 531
    label "Nowy_York"
  ]
  node [
    id 532
    label "Illinois"
  ]
  node [
    id 533
    label "Po&#322;udnie"
  ]
  node [
    id 534
    label "Kalifornia"
  ]
  node [
    id 535
    label "Wirginia"
  ]
  node [
    id 536
    label "Teksas"
  ]
  node [
    id 537
    label "Waszyngton"
  ]
  node [
    id 538
    label "zielona_karta"
  ]
  node [
    id 539
    label "Massachusetts"
  ]
  node [
    id 540
    label "Alaska"
  ]
  node [
    id 541
    label "Hawaje"
  ]
  node [
    id 542
    label "Maryland"
  ]
  node [
    id 543
    label "Michigan"
  ]
  node [
    id 544
    label "Arizona"
  ]
  node [
    id 545
    label "Georgia"
  ]
  node [
    id 546
    label "stan_wolny"
  ]
  node [
    id 547
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 548
    label "Pensylwania"
  ]
  node [
    id 549
    label "Luizjana"
  ]
  node [
    id 550
    label "Nowy_Meksyk"
  ]
  node [
    id 551
    label "Wuj_Sam"
  ]
  node [
    id 552
    label "Alabama"
  ]
  node [
    id 553
    label "Kansas"
  ]
  node [
    id 554
    label "Oregon"
  ]
  node [
    id 555
    label "Zach&#243;d"
  ]
  node [
    id 556
    label "Floryda"
  ]
  node [
    id 557
    label "Oklahoma"
  ]
  node [
    id 558
    label "Hudson"
  ]
  node [
    id 559
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 560
    label "somoni"
  ]
  node [
    id 561
    label "perper"
  ]
  node [
    id 562
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 563
    label "euro"
  ]
  node [
    id 564
    label "Bengal"
  ]
  node [
    id 565
    label "taka"
  ]
  node [
    id 566
    label "Karelia"
  ]
  node [
    id 567
    label "Mari_El"
  ]
  node [
    id 568
    label "Inguszetia"
  ]
  node [
    id 569
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 570
    label "Udmurcja"
  ]
  node [
    id 571
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 572
    label "Newa"
  ]
  node [
    id 573
    label "&#321;adoga"
  ]
  node [
    id 574
    label "Czeczenia"
  ]
  node [
    id 575
    label "Anadyr"
  ]
  node [
    id 576
    label "Syberia"
  ]
  node [
    id 577
    label "Tatarstan"
  ]
  node [
    id 578
    label "Wszechrosja"
  ]
  node [
    id 579
    label "Azja"
  ]
  node [
    id 580
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 581
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 582
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 583
    label "Kamczatka"
  ]
  node [
    id 584
    label "Jama&#322;"
  ]
  node [
    id 585
    label "Dagestan"
  ]
  node [
    id 586
    label "Witim"
  ]
  node [
    id 587
    label "Tuwa"
  ]
  node [
    id 588
    label "car"
  ]
  node [
    id 589
    label "Komi"
  ]
  node [
    id 590
    label "Czuwaszja"
  ]
  node [
    id 591
    label "Chakasja"
  ]
  node [
    id 592
    label "Perm"
  ]
  node [
    id 593
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 594
    label "Ajon"
  ]
  node [
    id 595
    label "Adygeja"
  ]
  node [
    id 596
    label "Dniepr"
  ]
  node [
    id 597
    label "rubel_rosyjski"
  ]
  node [
    id 598
    label "Don"
  ]
  node [
    id 599
    label "Mordowia"
  ]
  node [
    id 600
    label "s&#322;owianofilstwo"
  ]
  node [
    id 601
    label "gourde"
  ]
  node [
    id 602
    label "escudo_angolskie"
  ]
  node [
    id 603
    label "kwanza"
  ]
  node [
    id 604
    label "ariary"
  ]
  node [
    id 605
    label "Ocean_Indyjski"
  ]
  node [
    id 606
    label "frank_malgaski"
  ]
  node [
    id 607
    label "Unia_Europejska"
  ]
  node [
    id 608
    label "Wile&#324;szczyzna"
  ]
  node [
    id 609
    label "Windawa"
  ]
  node [
    id 610
    label "&#379;mud&#378;"
  ]
  node [
    id 611
    label "lit"
  ]
  node [
    id 612
    label "Synaj"
  ]
  node [
    id 613
    label "paraszyt"
  ]
  node [
    id 614
    label "funt_egipski"
  ]
  node [
    id 615
    label "birr"
  ]
  node [
    id 616
    label "negus"
  ]
  node [
    id 617
    label "peso_kolumbijskie"
  ]
  node [
    id 618
    label "Orinoko"
  ]
  node [
    id 619
    label "rial_katarski"
  ]
  node [
    id 620
    label "dram"
  ]
  node [
    id 621
    label "Limburgia"
  ]
  node [
    id 622
    label "gulden"
  ]
  node [
    id 623
    label "Zelandia"
  ]
  node [
    id 624
    label "Niderlandy"
  ]
  node [
    id 625
    label "Brabancja"
  ]
  node [
    id 626
    label "cedi"
  ]
  node [
    id 627
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 628
    label "milrejs"
  ]
  node [
    id 629
    label "cruzado"
  ]
  node [
    id 630
    label "real"
  ]
  node [
    id 631
    label "frank_monakijski"
  ]
  node [
    id 632
    label "Fryburg"
  ]
  node [
    id 633
    label "Bazylea"
  ]
  node [
    id 634
    label "Alpy"
  ]
  node [
    id 635
    label "frank_szwajcarski"
  ]
  node [
    id 636
    label "Helwecja"
  ]
  node [
    id 637
    label "Berno"
  ]
  node [
    id 638
    label "lej_mo&#322;dawski"
  ]
  node [
    id 639
    label "Dniestr"
  ]
  node [
    id 640
    label "Gagauzja"
  ]
  node [
    id 641
    label "Indie_Zachodnie"
  ]
  node [
    id 642
    label "Sikkim"
  ]
  node [
    id 643
    label "Asam"
  ]
  node [
    id 644
    label "rupia_indyjska"
  ]
  node [
    id 645
    label "Indie_Portugalskie"
  ]
  node [
    id 646
    label "Indie_Wschodnie"
  ]
  node [
    id 647
    label "Bollywood"
  ]
  node [
    id 648
    label "Pend&#380;ab"
  ]
  node [
    id 649
    label "boliwar"
  ]
  node [
    id 650
    label "naira"
  ]
  node [
    id 651
    label "frank_gwinejski"
  ]
  node [
    id 652
    label "sum"
  ]
  node [
    id 653
    label "Karaka&#322;pacja"
  ]
  node [
    id 654
    label "dolar_liberyjski"
  ]
  node [
    id 655
    label "Dacja"
  ]
  node [
    id 656
    label "lej_rumu&#324;ski"
  ]
  node [
    id 657
    label "Siedmiogr&#243;d"
  ]
  node [
    id 658
    label "Dobrud&#380;a"
  ]
  node [
    id 659
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 660
    label "dolar_namibijski"
  ]
  node [
    id 661
    label "kuna"
  ]
  node [
    id 662
    label "Rugia"
  ]
  node [
    id 663
    label "Saksonia"
  ]
  node [
    id 664
    label "Dolna_Saksonia"
  ]
  node [
    id 665
    label "Anglosas"
  ]
  node [
    id 666
    label "Hesja"
  ]
  node [
    id 667
    label "Wirtembergia"
  ]
  node [
    id 668
    label "Po&#322;abie"
  ]
  node [
    id 669
    label "Germania"
  ]
  node [
    id 670
    label "Frankonia"
  ]
  node [
    id 671
    label "Badenia"
  ]
  node [
    id 672
    label "Holsztyn"
  ]
  node [
    id 673
    label "marka"
  ]
  node [
    id 674
    label "Brandenburgia"
  ]
  node [
    id 675
    label "Szwabia"
  ]
  node [
    id 676
    label "Niemcy_Zachodnie"
  ]
  node [
    id 677
    label "Westfalia"
  ]
  node [
    id 678
    label "Helgoland"
  ]
  node [
    id 679
    label "Karlsbad"
  ]
  node [
    id 680
    label "Niemcy_Wschodnie"
  ]
  node [
    id 681
    label "korona_w&#281;gierska"
  ]
  node [
    id 682
    label "forint"
  ]
  node [
    id 683
    label "Lipt&#243;w"
  ]
  node [
    id 684
    label "tenge"
  ]
  node [
    id 685
    label "szach"
  ]
  node [
    id 686
    label "Baktria"
  ]
  node [
    id 687
    label "afgani"
  ]
  node [
    id 688
    label "kip"
  ]
  node [
    id 689
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 690
    label "Salzburg"
  ]
  node [
    id 691
    label "Rakuzy"
  ]
  node [
    id 692
    label "Dyja"
  ]
  node [
    id 693
    label "konsulent"
  ]
  node [
    id 694
    label "szyling_austryjacki"
  ]
  node [
    id 695
    label "peso_urugwajskie"
  ]
  node [
    id 696
    label "rial_jeme&#324;ski"
  ]
  node [
    id 697
    label "korona_esto&#324;ska"
  ]
  node [
    id 698
    label "Inflanty"
  ]
  node [
    id 699
    label "marka_esto&#324;ska"
  ]
  node [
    id 700
    label "tala"
  ]
  node [
    id 701
    label "Podole"
  ]
  node [
    id 702
    label "Wsch&#243;d"
  ]
  node [
    id 703
    label "Naddnieprze"
  ]
  node [
    id 704
    label "Ma&#322;orosja"
  ]
  node [
    id 705
    label "Wo&#322;y&#324;"
  ]
  node [
    id 706
    label "Nadbu&#380;e"
  ]
  node [
    id 707
    label "hrywna"
  ]
  node [
    id 708
    label "Zaporo&#380;e"
  ]
  node [
    id 709
    label "Krym"
  ]
  node [
    id 710
    label "Przykarpacie"
  ]
  node [
    id 711
    label "Kozaczyzna"
  ]
  node [
    id 712
    label "karbowaniec"
  ]
  node [
    id 713
    label "riel"
  ]
  node [
    id 714
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 715
    label "kyat"
  ]
  node [
    id 716
    label "Arakan"
  ]
  node [
    id 717
    label "funt_liba&#324;ski"
  ]
  node [
    id 718
    label "Mariany"
  ]
  node [
    id 719
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 720
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 721
    label "dinar_algierski"
  ]
  node [
    id 722
    label "ringgit"
  ]
  node [
    id 723
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 724
    label "Borneo"
  ]
  node [
    id 725
    label "peso_dominika&#324;skie"
  ]
  node [
    id 726
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 727
    label "peso_kuba&#324;skie"
  ]
  node [
    id 728
    label "lira_izraelska"
  ]
  node [
    id 729
    label "szekel"
  ]
  node [
    id 730
    label "Galilea"
  ]
  node [
    id 731
    label "Judea"
  ]
  node [
    id 732
    label "tolar"
  ]
  node [
    id 733
    label "frank_luksemburski"
  ]
  node [
    id 734
    label "lempira"
  ]
  node [
    id 735
    label "Pozna&#324;"
  ]
  node [
    id 736
    label "lira_malta&#324;ska"
  ]
  node [
    id 737
    label "Gozo"
  ]
  node [
    id 738
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 739
    label "Paros"
  ]
  node [
    id 740
    label "Epir"
  ]
  node [
    id 741
    label "panhellenizm"
  ]
  node [
    id 742
    label "Eubea"
  ]
  node [
    id 743
    label "Rodos"
  ]
  node [
    id 744
    label "Achaja"
  ]
  node [
    id 745
    label "Termopile"
  ]
  node [
    id 746
    label "Attyka"
  ]
  node [
    id 747
    label "Hellada"
  ]
  node [
    id 748
    label "Etolia"
  ]
  node [
    id 749
    label "palestra"
  ]
  node [
    id 750
    label "Kreta"
  ]
  node [
    id 751
    label "drachma"
  ]
  node [
    id 752
    label "Olimp"
  ]
  node [
    id 753
    label "Tesalia"
  ]
  node [
    id 754
    label "Peloponez"
  ]
  node [
    id 755
    label "Eolia"
  ]
  node [
    id 756
    label "Beocja"
  ]
  node [
    id 757
    label "Parnas"
  ]
  node [
    id 758
    label "Lesbos"
  ]
  node [
    id 759
    label "Atlantyk"
  ]
  node [
    id 760
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 761
    label "Ulster"
  ]
  node [
    id 762
    label "funt_irlandzki"
  ]
  node [
    id 763
    label "tugrik"
  ]
  node [
    id 764
    label "Buriaci"
  ]
  node [
    id 765
    label "ajmak"
  ]
  node [
    id 766
    label "denar_macedo&#324;ski"
  ]
  node [
    id 767
    label "Pikardia"
  ]
  node [
    id 768
    label "Masyw_Centralny"
  ]
  node [
    id 769
    label "Akwitania"
  ]
  node [
    id 770
    label "Alzacja"
  ]
  node [
    id 771
    label "Sekwana"
  ]
  node [
    id 772
    label "Langwedocja"
  ]
  node [
    id 773
    label "Martynika"
  ]
  node [
    id 774
    label "Bretania"
  ]
  node [
    id 775
    label "Sabaudia"
  ]
  node [
    id 776
    label "Korsyka"
  ]
  node [
    id 777
    label "Normandia"
  ]
  node [
    id 778
    label "Gaskonia"
  ]
  node [
    id 779
    label "Burgundia"
  ]
  node [
    id 780
    label "frank_francuski"
  ]
  node [
    id 781
    label "Wandea"
  ]
  node [
    id 782
    label "Prowansja"
  ]
  node [
    id 783
    label "Gwadelupa"
  ]
  node [
    id 784
    label "lew"
  ]
  node [
    id 785
    label "c&#243;rdoba"
  ]
  node [
    id 786
    label "dolar_Zimbabwe"
  ]
  node [
    id 787
    label "frank_rwandyjski"
  ]
  node [
    id 788
    label "kwacha_zambijska"
  ]
  node [
    id 789
    label "Kurlandia"
  ]
  node [
    id 790
    label "&#322;at"
  ]
  node [
    id 791
    label "Liwonia"
  ]
  node [
    id 792
    label "rubel_&#322;otewski"
  ]
  node [
    id 793
    label "Himalaje"
  ]
  node [
    id 794
    label "rupia_nepalska"
  ]
  node [
    id 795
    label "funt_suda&#324;ski"
  ]
  node [
    id 796
    label "dolar_bahamski"
  ]
  node [
    id 797
    label "Wielka_Bahama"
  ]
  node [
    id 798
    label "Pa&#322;uki"
  ]
  node [
    id 799
    label "Wolin"
  ]
  node [
    id 800
    label "z&#322;oty"
  ]
  node [
    id 801
    label "So&#322;a"
  ]
  node [
    id 802
    label "Krajna"
  ]
  node [
    id 803
    label "Suwalszczyzna"
  ]
  node [
    id 804
    label "barwy_polskie"
  ]
  node [
    id 805
    label "Izera"
  ]
  node [
    id 806
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 807
    label "Kaczawa"
  ]
  node [
    id 808
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 809
    label "Wis&#322;a"
  ]
  node [
    id 810
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 811
    label "Antyle"
  ]
  node [
    id 812
    label "dolar_Tuvalu"
  ]
  node [
    id 813
    label "dinar_iracki"
  ]
  node [
    id 814
    label "korona_s&#322;owacka"
  ]
  node [
    id 815
    label "Turiec"
  ]
  node [
    id 816
    label "jen"
  ]
  node [
    id 817
    label "jinja"
  ]
  node [
    id 818
    label "Okinawa"
  ]
  node [
    id 819
    label "Japonica"
  ]
  node [
    id 820
    label "manat_turkme&#324;ski"
  ]
  node [
    id 821
    label "szyling_kenijski"
  ]
  node [
    id 822
    label "peso_chilijskie"
  ]
  node [
    id 823
    label "Zanzibar"
  ]
  node [
    id 824
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 825
    label "peso_filipi&#324;skie"
  ]
  node [
    id 826
    label "Cebu"
  ]
  node [
    id 827
    label "Sahara"
  ]
  node [
    id 828
    label "Tasmania"
  ]
  node [
    id 829
    label "Nowy_&#346;wiat"
  ]
  node [
    id 830
    label "dolar_australijski"
  ]
  node [
    id 831
    label "Quebec"
  ]
  node [
    id 832
    label "dolar_kanadyjski"
  ]
  node [
    id 833
    label "Nowa_Fundlandia"
  ]
  node [
    id 834
    label "quetzal"
  ]
  node [
    id 835
    label "Manica"
  ]
  node [
    id 836
    label "escudo_mozambickie"
  ]
  node [
    id 837
    label "Cabo_Delgado"
  ]
  node [
    id 838
    label "Inhambane"
  ]
  node [
    id 839
    label "Maputo"
  ]
  node [
    id 840
    label "Gaza"
  ]
  node [
    id 841
    label "Niasa"
  ]
  node [
    id 842
    label "Nampula"
  ]
  node [
    id 843
    label "metical"
  ]
  node [
    id 844
    label "frank_tunezyjski"
  ]
  node [
    id 845
    label "dinar_tunezyjski"
  ]
  node [
    id 846
    label "lud"
  ]
  node [
    id 847
    label "frank_kongijski"
  ]
  node [
    id 848
    label "peso_argenty&#324;skie"
  ]
  node [
    id 849
    label "dinar_Bahrajnu"
  ]
  node [
    id 850
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 851
    label "escudo_portugalskie"
  ]
  node [
    id 852
    label "Melanezja"
  ]
  node [
    id 853
    label "dolar_Fid&#380;i"
  ]
  node [
    id 854
    label "d&#380;amahirijja"
  ]
  node [
    id 855
    label "dinar_libijski"
  ]
  node [
    id 856
    label "balboa"
  ]
  node [
    id 857
    label "dolar_surinamski"
  ]
  node [
    id 858
    label "dolar_Brunei"
  ]
  node [
    id 859
    label "Estremadura"
  ]
  node [
    id 860
    label "Kastylia"
  ]
  node [
    id 861
    label "Rzym_Zachodni"
  ]
  node [
    id 862
    label "Aragonia"
  ]
  node [
    id 863
    label "hacjender"
  ]
  node [
    id 864
    label "Asturia"
  ]
  node [
    id 865
    label "Baskonia"
  ]
  node [
    id 866
    label "Majorka"
  ]
  node [
    id 867
    label "Walencja"
  ]
  node [
    id 868
    label "peseta"
  ]
  node [
    id 869
    label "Katalonia"
  ]
  node [
    id 870
    label "Luksemburgia"
  ]
  node [
    id 871
    label "frank_belgijski"
  ]
  node [
    id 872
    label "Walonia"
  ]
  node [
    id 873
    label "Flandria"
  ]
  node [
    id 874
    label "dolar_guja&#324;ski"
  ]
  node [
    id 875
    label "dolar_Barbadosu"
  ]
  node [
    id 876
    label "korona_czeska"
  ]
  node [
    id 877
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 878
    label "Wojwodina"
  ]
  node [
    id 879
    label "dinar_serbski"
  ]
  node [
    id 880
    label "funt_syryjski"
  ]
  node [
    id 881
    label "alawizm"
  ]
  node [
    id 882
    label "Szantung"
  ]
  node [
    id 883
    label "Chiny_Zachodnie"
  ]
  node [
    id 884
    label "Kuantung"
  ]
  node [
    id 885
    label "D&#380;ungaria"
  ]
  node [
    id 886
    label "yuan"
  ]
  node [
    id 887
    label "Hongkong"
  ]
  node [
    id 888
    label "Chiny_Wschodnie"
  ]
  node [
    id 889
    label "Guangdong"
  ]
  node [
    id 890
    label "Junnan"
  ]
  node [
    id 891
    label "Mand&#380;uria"
  ]
  node [
    id 892
    label "Syczuan"
  ]
  node [
    id 893
    label "zair"
  ]
  node [
    id 894
    label "Katanga"
  ]
  node [
    id 895
    label "ugija"
  ]
  node [
    id 896
    label "dalasi"
  ]
  node [
    id 897
    label "funt_cypryjski"
  ]
  node [
    id 898
    label "Afrodyzje"
  ]
  node [
    id 899
    label "para"
  ]
  node [
    id 900
    label "lek"
  ]
  node [
    id 901
    label "frank_alba&#324;ski"
  ]
  node [
    id 902
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 903
    label "dolar_jamajski"
  ]
  node [
    id 904
    label "kafar"
  ]
  node [
    id 905
    label "Ocean_Spokojny"
  ]
  node [
    id 906
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 907
    label "som"
  ]
  node [
    id 908
    label "guarani"
  ]
  node [
    id 909
    label "rial_ira&#324;ski"
  ]
  node [
    id 910
    label "mu&#322;&#322;a"
  ]
  node [
    id 911
    label "Persja"
  ]
  node [
    id 912
    label "Jawa"
  ]
  node [
    id 913
    label "Sumatra"
  ]
  node [
    id 914
    label "rupia_indonezyjska"
  ]
  node [
    id 915
    label "Nowa_Gwinea"
  ]
  node [
    id 916
    label "Moluki"
  ]
  node [
    id 917
    label "szyling_somalijski"
  ]
  node [
    id 918
    label "szyling_ugandyjski"
  ]
  node [
    id 919
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 920
    label "Ujgur"
  ]
  node [
    id 921
    label "Azja_Mniejsza"
  ]
  node [
    id 922
    label "lira_turecka"
  ]
  node [
    id 923
    label "Pireneje"
  ]
  node [
    id 924
    label "nakfa"
  ]
  node [
    id 925
    label "won"
  ]
  node [
    id 926
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 927
    label "&#346;wite&#378;"
  ]
  node [
    id 928
    label "dinar_kuwejcki"
  ]
  node [
    id 929
    label "Nachiczewan"
  ]
  node [
    id 930
    label "manat_azerski"
  ]
  node [
    id 931
    label "Karabach"
  ]
  node [
    id 932
    label "dolar_Kiribati"
  ]
  node [
    id 933
    label "moszaw"
  ]
  node [
    id 934
    label "Kanaan"
  ]
  node [
    id 935
    label "Aruba"
  ]
  node [
    id 936
    label "Kajmany"
  ]
  node [
    id 937
    label "Anguilla"
  ]
  node [
    id 938
    label "Mogielnica"
  ]
  node [
    id 939
    label "jezioro"
  ]
  node [
    id 940
    label "Rumelia"
  ]
  node [
    id 941
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 942
    label "Poprad"
  ]
  node [
    id 943
    label "Tatry"
  ]
  node [
    id 944
    label "Podtatrze"
  ]
  node [
    id 945
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 946
    label "Austro-W&#281;gry"
  ]
  node [
    id 947
    label "Biskupice"
  ]
  node [
    id 948
    label "Iwanowice"
  ]
  node [
    id 949
    label "Ziemia_Sandomierska"
  ]
  node [
    id 950
    label "Rogo&#378;nik"
  ]
  node [
    id 951
    label "Ropa"
  ]
  node [
    id 952
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 953
    label "Karpaty"
  ]
  node [
    id 954
    label "Beskidy_Zachodnie"
  ]
  node [
    id 955
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 956
    label "Beskid_Niski"
  ]
  node [
    id 957
    label "Etruria"
  ]
  node [
    id 958
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 959
    label "Bojanowo"
  ]
  node [
    id 960
    label "Obra"
  ]
  node [
    id 961
    label "Wilkowo_Polskie"
  ]
  node [
    id 962
    label "Dobra"
  ]
  node [
    id 963
    label "Buriacja"
  ]
  node [
    id 964
    label "Rozewie"
  ]
  node [
    id 965
    label "&#346;l&#261;sk"
  ]
  node [
    id 966
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 967
    label "Norwegia"
  ]
  node [
    id 968
    label "Szwecja"
  ]
  node [
    id 969
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 970
    label "Finlandia"
  ]
  node [
    id 971
    label "Wiktoria"
  ]
  node [
    id 972
    label "Guernsey"
  ]
  node [
    id 973
    label "Conrad"
  ]
  node [
    id 974
    label "funt_szterling"
  ]
  node [
    id 975
    label "Portland"
  ]
  node [
    id 976
    label "El&#380;bieta_I"
  ]
  node [
    id 977
    label "Kornwalia"
  ]
  node [
    id 978
    label "Amazonka"
  ]
  node [
    id 979
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 980
    label "Imperium_Rosyjskie"
  ]
  node [
    id 981
    label "Moza"
  ]
  node [
    id 982
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 983
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 984
    label "Paj&#281;czno"
  ]
  node [
    id 985
    label "Tar&#322;&#243;w"
  ]
  node [
    id 986
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 987
    label "Gop&#322;o"
  ]
  node [
    id 988
    label "Jerozolima"
  ]
  node [
    id 989
    label "Dolna_Frankonia"
  ]
  node [
    id 990
    label "funt_szkocki"
  ]
  node [
    id 991
    label "Kaledonia"
  ]
  node [
    id 992
    label "Abchazja"
  ]
  node [
    id 993
    label "Sarmata"
  ]
  node [
    id 994
    label "Eurazja"
  ]
  node [
    id 995
    label "Mariensztat"
  ]
  node [
    id 996
    label "bajt"
  ]
  node [
    id 997
    label "s&#322;owo_maszynowe"
  ]
  node [
    id 998
    label "jednostka_informacji"
  ]
  node [
    id 999
    label "oktet"
  ]
  node [
    id 1000
    label "p&#243;&#322;bajt"
  ]
  node [
    id 1001
    label "cyfra"
  ]
  node [
    id 1002
    label "system_dw&#243;jkowy"
  ]
  node [
    id 1003
    label "rytm"
  ]
  node [
    id 1004
    label "znak_pisarski"
  ]
  node [
    id 1005
    label "inicja&#322;"
  ]
  node [
    id 1006
    label "wz&#243;r"
  ]
  node [
    id 1007
    label "time"
  ]
  node [
    id 1008
    label "rytmika"
  ]
  node [
    id 1009
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 1010
    label "wybicie"
  ]
  node [
    id 1011
    label "tempo"
  ]
  node [
    id 1012
    label "metrum"
  ]
  node [
    id 1013
    label "wybijanie"
  ]
  node [
    id 1014
    label "rozmieszczenie"
  ]
  node [
    id 1015
    label "miarowo&#347;&#263;"
  ]
  node [
    id 1016
    label "eurytmia"
  ]
  node [
    id 1017
    label "&#243;semka"
  ]
  node [
    id 1018
    label "octet"
  ]
  node [
    id 1019
    label "utw&#243;r"
  ]
  node [
    id 1020
    label "blok"
  ]
  node [
    id 1021
    label "megabajt"
  ]
  node [
    id 1022
    label "partycja"
  ]
  node [
    id 1023
    label "gazel"
  ]
  node [
    id 1024
    label "terabajt"
  ]
  node [
    id 1025
    label "wers"
  ]
  node [
    id 1026
    label "gigabajt"
  ]
  node [
    id 1027
    label "jednostka_alokacji"
  ]
  node [
    id 1028
    label "kilobajt"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 1
    target 393
  ]
  edge [
    source 1
    target 394
  ]
  edge [
    source 1
    target 395
  ]
  edge [
    source 1
    target 396
  ]
  edge [
    source 1
    target 397
  ]
  edge [
    source 1
    target 398
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 400
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 1
    target 402
  ]
  edge [
    source 1
    target 403
  ]
  edge [
    source 1
    target 404
  ]
  edge [
    source 1
    target 405
  ]
  edge [
    source 1
    target 406
  ]
  edge [
    source 1
    target 407
  ]
  edge [
    source 1
    target 408
  ]
  edge [
    source 1
    target 409
  ]
  edge [
    source 1
    target 410
  ]
  edge [
    source 1
    target 411
  ]
  edge [
    source 1
    target 412
  ]
  edge [
    source 1
    target 413
  ]
  edge [
    source 1
    target 414
  ]
  edge [
    source 1
    target 415
  ]
  edge [
    source 1
    target 416
  ]
  edge [
    source 1
    target 417
  ]
  edge [
    source 1
    target 418
  ]
  edge [
    source 1
    target 419
  ]
  edge [
    source 1
    target 420
  ]
  edge [
    source 1
    target 421
  ]
  edge [
    source 1
    target 422
  ]
  edge [
    source 1
    target 423
  ]
  edge [
    source 1
    target 424
  ]
  edge [
    source 1
    target 425
  ]
  edge [
    source 1
    target 426
  ]
  edge [
    source 1
    target 427
  ]
  edge [
    source 1
    target 428
  ]
  edge [
    source 1
    target 429
  ]
  edge [
    source 1
    target 430
  ]
  edge [
    source 1
    target 431
  ]
  edge [
    source 1
    target 432
  ]
  edge [
    source 1
    target 433
  ]
  edge [
    source 1
    target 434
  ]
  edge [
    source 1
    target 435
  ]
  edge [
    source 1
    target 436
  ]
  edge [
    source 1
    target 437
  ]
  edge [
    source 1
    target 438
  ]
  edge [
    source 1
    target 439
  ]
  edge [
    source 1
    target 440
  ]
  edge [
    source 1
    target 441
  ]
  edge [
    source 1
    target 442
  ]
  edge [
    source 1
    target 443
  ]
  edge [
    source 1
    target 444
  ]
  edge [
    source 1
    target 445
  ]
  edge [
    source 1
    target 446
  ]
  edge [
    source 1
    target 447
  ]
  edge [
    source 1
    target 448
  ]
  edge [
    source 1
    target 449
  ]
  edge [
    source 1
    target 450
  ]
  edge [
    source 1
    target 451
  ]
  edge [
    source 1
    target 452
  ]
  edge [
    source 1
    target 453
  ]
  edge [
    source 1
    target 454
  ]
  edge [
    source 1
    target 455
  ]
  edge [
    source 1
    target 456
  ]
  edge [
    source 1
    target 457
  ]
  edge [
    source 1
    target 458
  ]
  edge [
    source 1
    target 459
  ]
  edge [
    source 1
    target 460
  ]
  edge [
    source 1
    target 461
  ]
  edge [
    source 1
    target 462
  ]
  edge [
    source 1
    target 463
  ]
  edge [
    source 1
    target 464
  ]
  edge [
    source 1
    target 465
  ]
  edge [
    source 1
    target 466
  ]
  edge [
    source 1
    target 467
  ]
  edge [
    source 1
    target 468
  ]
  edge [
    source 1
    target 469
  ]
  edge [
    source 1
    target 470
  ]
  edge [
    source 1
    target 471
  ]
  edge [
    source 1
    target 472
  ]
  edge [
    source 1
    target 473
  ]
  edge [
    source 1
    target 474
  ]
  edge [
    source 1
    target 475
  ]
  edge [
    source 1
    target 476
  ]
  edge [
    source 1
    target 477
  ]
  edge [
    source 1
    target 478
  ]
  edge [
    source 1
    target 479
  ]
  edge [
    source 1
    target 480
  ]
  edge [
    source 1
    target 481
  ]
  edge [
    source 1
    target 482
  ]
  edge [
    source 1
    target 483
  ]
  edge [
    source 1
    target 484
  ]
  edge [
    source 1
    target 485
  ]
  edge [
    source 1
    target 486
  ]
  edge [
    source 1
    target 487
  ]
  edge [
    source 1
    target 488
  ]
  edge [
    source 1
    target 489
  ]
  edge [
    source 1
    target 490
  ]
  edge [
    source 1
    target 491
  ]
  edge [
    source 1
    target 492
  ]
  edge [
    source 1
    target 493
  ]
  edge [
    source 1
    target 494
  ]
  edge [
    source 1
    target 495
  ]
  edge [
    source 1
    target 496
  ]
  edge [
    source 1
    target 497
  ]
  edge [
    source 1
    target 498
  ]
  edge [
    source 1
    target 499
  ]
  edge [
    source 1
    target 500
  ]
  edge [
    source 1
    target 501
  ]
  edge [
    source 1
    target 502
  ]
  edge [
    source 1
    target 503
  ]
  edge [
    source 1
    target 504
  ]
  edge [
    source 1
    target 505
  ]
  edge [
    source 1
    target 506
  ]
  edge [
    source 1
    target 507
  ]
  edge [
    source 1
    target 508
  ]
  edge [
    source 1
    target 509
  ]
  edge [
    source 1
    target 510
  ]
  edge [
    source 1
    target 511
  ]
  edge [
    source 1
    target 512
  ]
  edge [
    source 1
    target 513
  ]
  edge [
    source 1
    target 514
  ]
  edge [
    source 1
    target 515
  ]
  edge [
    source 1
    target 516
  ]
  edge [
    source 1
    target 517
  ]
  edge [
    source 1
    target 518
  ]
  edge [
    source 1
    target 519
  ]
  edge [
    source 1
    target 520
  ]
  edge [
    source 1
    target 521
  ]
  edge [
    source 1
    target 522
  ]
  edge [
    source 1
    target 523
  ]
  edge [
    source 1
    target 524
  ]
  edge [
    source 1
    target 525
  ]
  edge [
    source 1
    target 526
  ]
  edge [
    source 1
    target 527
  ]
  edge [
    source 1
    target 528
  ]
  edge [
    source 1
    target 529
  ]
  edge [
    source 1
    target 530
  ]
  edge [
    source 1
    target 531
  ]
  edge [
    source 1
    target 532
  ]
  edge [
    source 1
    target 533
  ]
  edge [
    source 1
    target 534
  ]
  edge [
    source 1
    target 535
  ]
  edge [
    source 1
    target 536
  ]
  edge [
    source 1
    target 537
  ]
  edge [
    source 1
    target 538
  ]
  edge [
    source 1
    target 539
  ]
  edge [
    source 1
    target 540
  ]
  edge [
    source 1
    target 541
  ]
  edge [
    source 1
    target 542
  ]
  edge [
    source 1
    target 543
  ]
  edge [
    source 1
    target 544
  ]
  edge [
    source 1
    target 545
  ]
  edge [
    source 1
    target 546
  ]
  edge [
    source 1
    target 547
  ]
  edge [
    source 1
    target 548
  ]
  edge [
    source 1
    target 549
  ]
  edge [
    source 1
    target 550
  ]
  edge [
    source 1
    target 551
  ]
  edge [
    source 1
    target 552
  ]
  edge [
    source 1
    target 553
  ]
  edge [
    source 1
    target 554
  ]
  edge [
    source 1
    target 555
  ]
  edge [
    source 1
    target 556
  ]
  edge [
    source 1
    target 557
  ]
  edge [
    source 1
    target 558
  ]
  edge [
    source 1
    target 559
  ]
  edge [
    source 1
    target 560
  ]
  edge [
    source 1
    target 561
  ]
  edge [
    source 1
    target 562
  ]
  edge [
    source 1
    target 563
  ]
  edge [
    source 1
    target 564
  ]
  edge [
    source 1
    target 565
  ]
  edge [
    source 1
    target 566
  ]
  edge [
    source 1
    target 567
  ]
  edge [
    source 1
    target 568
  ]
  edge [
    source 1
    target 569
  ]
  edge [
    source 1
    target 570
  ]
  edge [
    source 1
    target 571
  ]
  edge [
    source 1
    target 572
  ]
  edge [
    source 1
    target 573
  ]
  edge [
    source 1
    target 574
  ]
  edge [
    source 1
    target 575
  ]
  edge [
    source 1
    target 576
  ]
  edge [
    source 1
    target 577
  ]
  edge [
    source 1
    target 578
  ]
  edge [
    source 1
    target 579
  ]
  edge [
    source 1
    target 580
  ]
  edge [
    source 1
    target 581
  ]
  edge [
    source 1
    target 582
  ]
  edge [
    source 1
    target 583
  ]
  edge [
    source 1
    target 584
  ]
  edge [
    source 1
    target 585
  ]
  edge [
    source 1
    target 586
  ]
  edge [
    source 1
    target 587
  ]
  edge [
    source 1
    target 588
  ]
  edge [
    source 1
    target 589
  ]
  edge [
    source 1
    target 590
  ]
  edge [
    source 1
    target 591
  ]
  edge [
    source 1
    target 592
  ]
  edge [
    source 1
    target 593
  ]
  edge [
    source 1
    target 594
  ]
  edge [
    source 1
    target 595
  ]
  edge [
    source 1
    target 596
  ]
  edge [
    source 1
    target 597
  ]
  edge [
    source 1
    target 598
  ]
  edge [
    source 1
    target 599
  ]
  edge [
    source 1
    target 600
  ]
  edge [
    source 1
    target 601
  ]
  edge [
    source 1
    target 602
  ]
  edge [
    source 1
    target 603
  ]
  edge [
    source 1
    target 604
  ]
  edge [
    source 1
    target 605
  ]
  edge [
    source 1
    target 606
  ]
  edge [
    source 1
    target 607
  ]
  edge [
    source 1
    target 608
  ]
  edge [
    source 1
    target 609
  ]
  edge [
    source 1
    target 610
  ]
  edge [
    source 1
    target 611
  ]
  edge [
    source 1
    target 612
  ]
  edge [
    source 1
    target 613
  ]
  edge [
    source 1
    target 614
  ]
  edge [
    source 1
    target 615
  ]
  edge [
    source 1
    target 616
  ]
  edge [
    source 1
    target 617
  ]
  edge [
    source 1
    target 618
  ]
  edge [
    source 1
    target 619
  ]
  edge [
    source 1
    target 620
  ]
  edge [
    source 1
    target 621
  ]
  edge [
    source 1
    target 622
  ]
  edge [
    source 1
    target 623
  ]
  edge [
    source 1
    target 624
  ]
  edge [
    source 1
    target 625
  ]
  edge [
    source 1
    target 626
  ]
  edge [
    source 1
    target 627
  ]
  edge [
    source 1
    target 628
  ]
  edge [
    source 1
    target 629
  ]
  edge [
    source 1
    target 630
  ]
  edge [
    source 1
    target 631
  ]
  edge [
    source 1
    target 632
  ]
  edge [
    source 1
    target 633
  ]
  edge [
    source 1
    target 634
  ]
  edge [
    source 1
    target 635
  ]
  edge [
    source 1
    target 636
  ]
  edge [
    source 1
    target 637
  ]
  edge [
    source 1
    target 638
  ]
  edge [
    source 1
    target 639
  ]
  edge [
    source 1
    target 640
  ]
  edge [
    source 1
    target 641
  ]
  edge [
    source 1
    target 642
  ]
  edge [
    source 1
    target 643
  ]
  edge [
    source 1
    target 644
  ]
  edge [
    source 1
    target 645
  ]
  edge [
    source 1
    target 646
  ]
  edge [
    source 1
    target 647
  ]
  edge [
    source 1
    target 648
  ]
  edge [
    source 1
    target 649
  ]
  edge [
    source 1
    target 650
  ]
  edge [
    source 1
    target 651
  ]
  edge [
    source 1
    target 652
  ]
  edge [
    source 1
    target 653
  ]
  edge [
    source 1
    target 654
  ]
  edge [
    source 1
    target 655
  ]
  edge [
    source 1
    target 656
  ]
  edge [
    source 1
    target 657
  ]
  edge [
    source 1
    target 658
  ]
  edge [
    source 1
    target 659
  ]
  edge [
    source 1
    target 660
  ]
  edge [
    source 1
    target 661
  ]
  edge [
    source 1
    target 662
  ]
  edge [
    source 1
    target 663
  ]
  edge [
    source 1
    target 664
  ]
  edge [
    source 1
    target 665
  ]
  edge [
    source 1
    target 666
  ]
  edge [
    source 1
    target 667
  ]
  edge [
    source 1
    target 668
  ]
  edge [
    source 1
    target 669
  ]
  edge [
    source 1
    target 670
  ]
  edge [
    source 1
    target 671
  ]
  edge [
    source 1
    target 672
  ]
  edge [
    source 1
    target 673
  ]
  edge [
    source 1
    target 674
  ]
  edge [
    source 1
    target 675
  ]
  edge [
    source 1
    target 676
  ]
  edge [
    source 1
    target 677
  ]
  edge [
    source 1
    target 678
  ]
  edge [
    source 1
    target 679
  ]
  edge [
    source 1
    target 680
  ]
  edge [
    source 1
    target 681
  ]
  edge [
    source 1
    target 682
  ]
  edge [
    source 1
    target 683
  ]
  edge [
    source 1
    target 684
  ]
  edge [
    source 1
    target 685
  ]
  edge [
    source 1
    target 686
  ]
  edge [
    source 1
    target 687
  ]
  edge [
    source 1
    target 688
  ]
  edge [
    source 1
    target 689
  ]
  edge [
    source 1
    target 690
  ]
  edge [
    source 1
    target 691
  ]
  edge [
    source 1
    target 692
  ]
  edge [
    source 1
    target 693
  ]
  edge [
    source 1
    target 694
  ]
  edge [
    source 1
    target 695
  ]
  edge [
    source 1
    target 696
  ]
  edge [
    source 1
    target 697
  ]
  edge [
    source 1
    target 698
  ]
  edge [
    source 1
    target 699
  ]
  edge [
    source 1
    target 700
  ]
  edge [
    source 1
    target 701
  ]
  edge [
    source 1
    target 702
  ]
  edge [
    source 1
    target 703
  ]
  edge [
    source 1
    target 704
  ]
  edge [
    source 1
    target 705
  ]
  edge [
    source 1
    target 706
  ]
  edge [
    source 1
    target 707
  ]
  edge [
    source 1
    target 708
  ]
  edge [
    source 1
    target 709
  ]
  edge [
    source 1
    target 710
  ]
  edge [
    source 1
    target 711
  ]
  edge [
    source 1
    target 712
  ]
  edge [
    source 1
    target 713
  ]
  edge [
    source 1
    target 714
  ]
  edge [
    source 1
    target 715
  ]
  edge [
    source 1
    target 716
  ]
  edge [
    source 1
    target 717
  ]
  edge [
    source 1
    target 718
  ]
  edge [
    source 1
    target 719
  ]
  edge [
    source 1
    target 720
  ]
  edge [
    source 1
    target 721
  ]
  edge [
    source 1
    target 722
  ]
  edge [
    source 1
    target 723
  ]
  edge [
    source 1
    target 724
  ]
  edge [
    source 1
    target 725
  ]
  edge [
    source 1
    target 726
  ]
  edge [
    source 1
    target 727
  ]
  edge [
    source 1
    target 728
  ]
  edge [
    source 1
    target 729
  ]
  edge [
    source 1
    target 730
  ]
  edge [
    source 1
    target 731
  ]
  edge [
    source 1
    target 732
  ]
  edge [
    source 1
    target 733
  ]
  edge [
    source 1
    target 734
  ]
  edge [
    source 1
    target 735
  ]
  edge [
    source 1
    target 736
  ]
  edge [
    source 1
    target 737
  ]
  edge [
    source 1
    target 738
  ]
  edge [
    source 1
    target 739
  ]
  edge [
    source 1
    target 740
  ]
  edge [
    source 1
    target 741
  ]
  edge [
    source 1
    target 742
  ]
  edge [
    source 1
    target 743
  ]
  edge [
    source 1
    target 744
  ]
  edge [
    source 1
    target 745
  ]
  edge [
    source 1
    target 746
  ]
  edge [
    source 1
    target 747
  ]
  edge [
    source 1
    target 748
  ]
  edge [
    source 1
    target 749
  ]
  edge [
    source 1
    target 750
  ]
  edge [
    source 1
    target 751
  ]
  edge [
    source 1
    target 752
  ]
  edge [
    source 1
    target 753
  ]
  edge [
    source 1
    target 754
  ]
  edge [
    source 1
    target 755
  ]
  edge [
    source 1
    target 756
  ]
  edge [
    source 1
    target 757
  ]
  edge [
    source 1
    target 758
  ]
  edge [
    source 1
    target 759
  ]
  edge [
    source 1
    target 760
  ]
  edge [
    source 1
    target 761
  ]
  edge [
    source 1
    target 762
  ]
  edge [
    source 1
    target 763
  ]
  edge [
    source 1
    target 764
  ]
  edge [
    source 1
    target 765
  ]
  edge [
    source 1
    target 766
  ]
  edge [
    source 1
    target 767
  ]
  edge [
    source 1
    target 768
  ]
  edge [
    source 1
    target 769
  ]
  edge [
    source 1
    target 770
  ]
  edge [
    source 1
    target 771
  ]
  edge [
    source 1
    target 772
  ]
  edge [
    source 1
    target 773
  ]
  edge [
    source 1
    target 774
  ]
  edge [
    source 1
    target 775
  ]
  edge [
    source 1
    target 776
  ]
  edge [
    source 1
    target 777
  ]
  edge [
    source 1
    target 778
  ]
  edge [
    source 1
    target 779
  ]
  edge [
    source 1
    target 780
  ]
  edge [
    source 1
    target 781
  ]
  edge [
    source 1
    target 782
  ]
  edge [
    source 1
    target 783
  ]
  edge [
    source 1
    target 784
  ]
  edge [
    source 1
    target 785
  ]
  edge [
    source 1
    target 786
  ]
  edge [
    source 1
    target 787
  ]
  edge [
    source 1
    target 788
  ]
  edge [
    source 1
    target 789
  ]
  edge [
    source 1
    target 790
  ]
  edge [
    source 1
    target 791
  ]
  edge [
    source 1
    target 792
  ]
  edge [
    source 1
    target 793
  ]
  edge [
    source 1
    target 794
  ]
  edge [
    source 1
    target 795
  ]
  edge [
    source 1
    target 796
  ]
  edge [
    source 1
    target 797
  ]
  edge [
    source 1
    target 798
  ]
  edge [
    source 1
    target 799
  ]
  edge [
    source 1
    target 800
  ]
  edge [
    source 1
    target 801
  ]
  edge [
    source 1
    target 802
  ]
  edge [
    source 1
    target 803
  ]
  edge [
    source 1
    target 804
  ]
  edge [
    source 1
    target 805
  ]
  edge [
    source 1
    target 806
  ]
  edge [
    source 1
    target 807
  ]
  edge [
    source 1
    target 808
  ]
  edge [
    source 1
    target 809
  ]
  edge [
    source 1
    target 810
  ]
  edge [
    source 1
    target 811
  ]
  edge [
    source 1
    target 812
  ]
  edge [
    source 1
    target 813
  ]
  edge [
    source 1
    target 814
  ]
  edge [
    source 1
    target 815
  ]
  edge [
    source 1
    target 816
  ]
  edge [
    source 1
    target 817
  ]
  edge [
    source 1
    target 818
  ]
  edge [
    source 1
    target 819
  ]
  edge [
    source 1
    target 820
  ]
  edge [
    source 1
    target 821
  ]
  edge [
    source 1
    target 822
  ]
  edge [
    source 1
    target 823
  ]
  edge [
    source 1
    target 824
  ]
  edge [
    source 1
    target 825
  ]
  edge [
    source 1
    target 826
  ]
  edge [
    source 1
    target 827
  ]
  edge [
    source 1
    target 828
  ]
  edge [
    source 1
    target 829
  ]
  edge [
    source 1
    target 830
  ]
  edge [
    source 1
    target 831
  ]
  edge [
    source 1
    target 832
  ]
  edge [
    source 1
    target 833
  ]
  edge [
    source 1
    target 834
  ]
  edge [
    source 1
    target 835
  ]
  edge [
    source 1
    target 836
  ]
  edge [
    source 1
    target 837
  ]
  edge [
    source 1
    target 838
  ]
  edge [
    source 1
    target 839
  ]
  edge [
    source 1
    target 840
  ]
  edge [
    source 1
    target 841
  ]
  edge [
    source 1
    target 842
  ]
  edge [
    source 1
    target 843
  ]
  edge [
    source 1
    target 844
  ]
  edge [
    source 1
    target 845
  ]
  edge [
    source 1
    target 846
  ]
  edge [
    source 1
    target 847
  ]
  edge [
    source 1
    target 848
  ]
  edge [
    source 1
    target 849
  ]
  edge [
    source 1
    target 850
  ]
  edge [
    source 1
    target 851
  ]
  edge [
    source 1
    target 852
  ]
  edge [
    source 1
    target 853
  ]
  edge [
    source 1
    target 854
  ]
  edge [
    source 1
    target 855
  ]
  edge [
    source 1
    target 856
  ]
  edge [
    source 1
    target 857
  ]
  edge [
    source 1
    target 858
  ]
  edge [
    source 1
    target 859
  ]
  edge [
    source 1
    target 860
  ]
  edge [
    source 1
    target 861
  ]
  edge [
    source 1
    target 862
  ]
  edge [
    source 1
    target 863
  ]
  edge [
    source 1
    target 864
  ]
  edge [
    source 1
    target 865
  ]
  edge [
    source 1
    target 866
  ]
  edge [
    source 1
    target 867
  ]
  edge [
    source 1
    target 868
  ]
  edge [
    source 1
    target 869
  ]
  edge [
    source 1
    target 870
  ]
  edge [
    source 1
    target 871
  ]
  edge [
    source 1
    target 872
  ]
  edge [
    source 1
    target 873
  ]
  edge [
    source 1
    target 874
  ]
  edge [
    source 1
    target 875
  ]
  edge [
    source 1
    target 876
  ]
  edge [
    source 1
    target 877
  ]
  edge [
    source 1
    target 878
  ]
  edge [
    source 1
    target 879
  ]
  edge [
    source 1
    target 880
  ]
  edge [
    source 1
    target 881
  ]
  edge [
    source 1
    target 882
  ]
  edge [
    source 1
    target 883
  ]
  edge [
    source 1
    target 884
  ]
  edge [
    source 1
    target 885
  ]
  edge [
    source 1
    target 886
  ]
  edge [
    source 1
    target 887
  ]
  edge [
    source 1
    target 888
  ]
  edge [
    source 1
    target 889
  ]
  edge [
    source 1
    target 890
  ]
  edge [
    source 1
    target 891
  ]
  edge [
    source 1
    target 892
  ]
  edge [
    source 1
    target 893
  ]
  edge [
    source 1
    target 894
  ]
  edge [
    source 1
    target 895
  ]
  edge [
    source 1
    target 896
  ]
  edge [
    source 1
    target 897
  ]
  edge [
    source 1
    target 898
  ]
  edge [
    source 1
    target 899
  ]
  edge [
    source 1
    target 900
  ]
  edge [
    source 1
    target 901
  ]
  edge [
    source 1
    target 902
  ]
  edge [
    source 1
    target 903
  ]
  edge [
    source 1
    target 904
  ]
  edge [
    source 1
    target 905
  ]
  edge [
    source 1
    target 906
  ]
  edge [
    source 1
    target 907
  ]
  edge [
    source 1
    target 908
  ]
  edge [
    source 1
    target 909
  ]
  edge [
    source 1
    target 910
  ]
  edge [
    source 1
    target 911
  ]
  edge [
    source 1
    target 912
  ]
  edge [
    source 1
    target 913
  ]
  edge [
    source 1
    target 914
  ]
  edge [
    source 1
    target 915
  ]
  edge [
    source 1
    target 916
  ]
  edge [
    source 1
    target 917
  ]
  edge [
    source 1
    target 918
  ]
  edge [
    source 1
    target 919
  ]
  edge [
    source 1
    target 920
  ]
  edge [
    source 1
    target 921
  ]
  edge [
    source 1
    target 922
  ]
  edge [
    source 1
    target 923
  ]
  edge [
    source 1
    target 924
  ]
  edge [
    source 1
    target 925
  ]
  edge [
    source 1
    target 926
  ]
  edge [
    source 1
    target 927
  ]
  edge [
    source 1
    target 928
  ]
  edge [
    source 1
    target 929
  ]
  edge [
    source 1
    target 930
  ]
  edge [
    source 1
    target 931
  ]
  edge [
    source 1
    target 932
  ]
  edge [
    source 1
    target 933
  ]
  edge [
    source 1
    target 934
  ]
  edge [
    source 1
    target 935
  ]
  edge [
    source 1
    target 936
  ]
  edge [
    source 1
    target 937
  ]
  edge [
    source 1
    target 938
  ]
  edge [
    source 1
    target 939
  ]
  edge [
    source 1
    target 940
  ]
  edge [
    source 1
    target 941
  ]
  edge [
    source 1
    target 942
  ]
  edge [
    source 1
    target 943
  ]
  edge [
    source 1
    target 944
  ]
  edge [
    source 1
    target 945
  ]
  edge [
    source 1
    target 946
  ]
  edge [
    source 1
    target 947
  ]
  edge [
    source 1
    target 948
  ]
  edge [
    source 1
    target 949
  ]
  edge [
    source 1
    target 950
  ]
  edge [
    source 1
    target 951
  ]
  edge [
    source 1
    target 952
  ]
  edge [
    source 1
    target 953
  ]
  edge [
    source 1
    target 954
  ]
  edge [
    source 1
    target 955
  ]
  edge [
    source 1
    target 956
  ]
  edge [
    source 1
    target 957
  ]
  edge [
    source 1
    target 958
  ]
  edge [
    source 1
    target 959
  ]
  edge [
    source 1
    target 960
  ]
  edge [
    source 1
    target 961
  ]
  edge [
    source 1
    target 962
  ]
  edge [
    source 1
    target 963
  ]
  edge [
    source 1
    target 964
  ]
  edge [
    source 1
    target 965
  ]
  edge [
    source 1
    target 966
  ]
  edge [
    source 1
    target 967
  ]
  edge [
    source 1
    target 968
  ]
  edge [
    source 1
    target 969
  ]
  edge [
    source 1
    target 970
  ]
  edge [
    source 1
    target 971
  ]
  edge [
    source 1
    target 972
  ]
  edge [
    source 1
    target 973
  ]
  edge [
    source 1
    target 974
  ]
  edge [
    source 1
    target 975
  ]
  edge [
    source 1
    target 976
  ]
  edge [
    source 1
    target 977
  ]
  edge [
    source 1
    target 978
  ]
  edge [
    source 1
    target 979
  ]
  edge [
    source 1
    target 980
  ]
  edge [
    source 1
    target 981
  ]
  edge [
    source 1
    target 982
  ]
  edge [
    source 1
    target 983
  ]
  edge [
    source 1
    target 984
  ]
  edge [
    source 1
    target 985
  ]
  edge [
    source 1
    target 986
  ]
  edge [
    source 1
    target 987
  ]
  edge [
    source 1
    target 988
  ]
  edge [
    source 1
    target 989
  ]
  edge [
    source 1
    target 990
  ]
  edge [
    source 1
    target 991
  ]
  edge [
    source 1
    target 992
  ]
  edge [
    source 1
    target 993
  ]
  edge [
    source 1
    target 994
  ]
  edge [
    source 1
    target 995
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 996
  ]
  edge [
    source 2
    target 997
  ]
  edge [
    source 2
    target 998
  ]
  edge [
    source 2
    target 999
  ]
  edge [
    source 2
    target 1000
  ]
  edge [
    source 2
    target 1001
  ]
  edge [
    source 2
    target 1002
  ]
  edge [
    source 2
    target 1003
  ]
  edge [
    source 2
    target 1004
  ]
  edge [
    source 2
    target 1005
  ]
  edge [
    source 2
    target 1006
  ]
  edge [
    source 2
    target 1007
  ]
  edge [
    source 2
    target 1008
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 1009
  ]
  edge [
    source 2
    target 1010
  ]
  edge [
    source 2
    target 1011
  ]
  edge [
    source 2
    target 1012
  ]
  edge [
    source 2
    target 1013
  ]
  edge [
    source 2
    target 1014
  ]
  edge [
    source 2
    target 1015
  ]
  edge [
    source 2
    target 1016
  ]
  edge [
    source 2
    target 1017
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 1018
  ]
  edge [
    source 2
    target 1019
  ]
  edge [
    source 2
    target 1020
  ]
  edge [
    source 2
    target 1021
  ]
  edge [
    source 2
    target 1022
  ]
  edge [
    source 2
    target 1023
  ]
  edge [
    source 2
    target 1024
  ]
  edge [
    source 2
    target 1025
  ]
  edge [
    source 2
    target 1026
  ]
  edge [
    source 2
    target 1027
  ]
  edge [
    source 2
    target 1028
  ]
]
