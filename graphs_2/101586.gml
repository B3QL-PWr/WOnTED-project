graph [
  node [
    id 0
    label "stwierdza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "sejm"
    origin "text"
  ]
  node [
    id 2
    label "wys&#322;ucha&#263;"
    origin "text"
  ]
  node [
    id 3
    label "sprawozdanie"
    origin "text"
  ]
  node [
    id 4
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "narodowy"
    origin "text"
  ]
  node [
    id 6
    label "bank"
    origin "text"
  ]
  node [
    id 7
    label "polski"
    origin "text"
  ]
  node [
    id 8
    label "rok"
    origin "text"
  ]
  node [
    id 9
    label "przedstawi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "przez"
    origin "text"
  ]
  node [
    id 11
    label "pan"
    origin "text"
  ]
  node [
    id 12
    label "prezes"
    origin "text"
  ]
  node [
    id 13
    label "s&#322;awomir"
    origin "text"
  ]
  node [
    id 14
    label "skrzypek"
    origin "text"
  ]
  node [
    id 15
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 16
    label "dyskusja"
    origin "text"
  ]
  node [
    id 17
    label "uznawa&#263;"
  ]
  node [
    id 18
    label "attest"
  ]
  node [
    id 19
    label "oznajmia&#263;"
  ]
  node [
    id 20
    label "consider"
  ]
  node [
    id 21
    label "przyznawa&#263;"
  ]
  node [
    id 22
    label "os&#261;dza&#263;"
  ]
  node [
    id 23
    label "notice"
  ]
  node [
    id 24
    label "informowa&#263;"
  ]
  node [
    id 25
    label "inform"
  ]
  node [
    id 26
    label "parlament"
  ]
  node [
    id 27
    label "siedziba"
  ]
  node [
    id 28
    label "grupa"
  ]
  node [
    id 29
    label "centrum"
  ]
  node [
    id 30
    label "lewica"
  ]
  node [
    id 31
    label "prawica"
  ]
  node [
    id 32
    label "izba_ni&#380;sza"
  ]
  node [
    id 33
    label "zgromadzenie"
  ]
  node [
    id 34
    label "obrady"
  ]
  node [
    id 35
    label "parliament"
  ]
  node [
    id 36
    label "miejsce_pracy"
  ]
  node [
    id 37
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 38
    label "budynek"
  ]
  node [
    id 39
    label "&#321;ubianka"
  ]
  node [
    id 40
    label "Bia&#322;y_Dom"
  ]
  node [
    id 41
    label "miejsce"
  ]
  node [
    id 42
    label "dzia&#322;_personalny"
  ]
  node [
    id 43
    label "Kreml"
  ]
  node [
    id 44
    label "sadowisko"
  ]
  node [
    id 45
    label "asymilowa&#263;"
  ]
  node [
    id 46
    label "kompozycja"
  ]
  node [
    id 47
    label "pakiet_klimatyczny"
  ]
  node [
    id 48
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 49
    label "type"
  ]
  node [
    id 50
    label "cz&#261;steczka"
  ]
  node [
    id 51
    label "gromada"
  ]
  node [
    id 52
    label "specgrupa"
  ]
  node [
    id 53
    label "egzemplarz"
  ]
  node [
    id 54
    label "stage_set"
  ]
  node [
    id 55
    label "asymilowanie"
  ]
  node [
    id 56
    label "zbi&#243;r"
  ]
  node [
    id 57
    label "odm&#322;odzenie"
  ]
  node [
    id 58
    label "odm&#322;adza&#263;"
  ]
  node [
    id 59
    label "harcerze_starsi"
  ]
  node [
    id 60
    label "jednostka_systematyczna"
  ]
  node [
    id 61
    label "oddzia&#322;"
  ]
  node [
    id 62
    label "category"
  ]
  node [
    id 63
    label "liga"
  ]
  node [
    id 64
    label "&#346;wietliki"
  ]
  node [
    id 65
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 66
    label "formacja_geologiczna"
  ]
  node [
    id 67
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 68
    label "Eurogrupa"
  ]
  node [
    id 69
    label "Terranie"
  ]
  node [
    id 70
    label "odm&#322;adzanie"
  ]
  node [
    id 71
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 72
    label "Entuzjastki"
  ]
  node [
    id 73
    label "konsylium"
  ]
  node [
    id 74
    label "conference"
  ]
  node [
    id 75
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 76
    label "spotkanie"
  ]
  node [
    id 77
    label "spowodowanie"
  ]
  node [
    id 78
    label "pozyskanie"
  ]
  node [
    id 79
    label "kongregacja"
  ]
  node [
    id 80
    label "templum"
  ]
  node [
    id 81
    label "gromadzenie"
  ]
  node [
    id 82
    label "gathering"
  ]
  node [
    id 83
    label "konwentykiel"
  ]
  node [
    id 84
    label "klasztor"
  ]
  node [
    id 85
    label "caucus"
  ]
  node [
    id 86
    label "skupienie"
  ]
  node [
    id 87
    label "wsp&#243;lnota"
  ]
  node [
    id 88
    label "organ"
  ]
  node [
    id 89
    label "concourse"
  ]
  node [
    id 90
    label "czynno&#347;&#263;"
  ]
  node [
    id 91
    label "punkt"
  ]
  node [
    id 92
    label "blok"
  ]
  node [
    id 93
    label "centroprawica"
  ]
  node [
    id 94
    label "core"
  ]
  node [
    id 95
    label "o&#347;rodek"
  ]
  node [
    id 96
    label "Hollywood"
  ]
  node [
    id 97
    label "centrolew"
  ]
  node [
    id 98
    label "hand"
  ]
  node [
    id 99
    label "szko&#322;a"
  ]
  node [
    id 100
    label "left"
  ]
  node [
    id 101
    label "urz&#261;d"
  ]
  node [
    id 102
    label "plankton_polityczny"
  ]
  node [
    id 103
    label "europarlament"
  ]
  node [
    id 104
    label "ustawodawca"
  ]
  node [
    id 105
    label "grupa_bilateralna"
  ]
  node [
    id 106
    label "spe&#322;ni&#263;"
  ]
  node [
    id 107
    label "muzyka"
  ]
  node [
    id 108
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 109
    label "nagranie"
  ]
  node [
    id 110
    label "wys&#322;uchanie"
  ]
  node [
    id 111
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 112
    label "zrobi&#263;"
  ]
  node [
    id 113
    label "porobi&#263;"
  ]
  node [
    id 114
    label "pos&#322;uchanie"
  ]
  node [
    id 115
    label "wypowied&#378;"
  ]
  node [
    id 116
    label "return"
  ]
  node [
    id 117
    label "sta&#263;_si&#281;"
  ]
  node [
    id 118
    label "play_along"
  ]
  node [
    id 119
    label "urzeczywistni&#263;"
  ]
  node [
    id 120
    label "perform"
  ]
  node [
    id 121
    label "utrwalenie"
  ]
  node [
    id 122
    label "recording"
  ]
  node [
    id 123
    label "wytw&#243;r"
  ]
  node [
    id 124
    label "wokalistyka"
  ]
  node [
    id 125
    label "harmonia"
  ]
  node [
    id 126
    label "instrumentalistyka"
  ]
  node [
    id 127
    label "sztuka"
  ]
  node [
    id 128
    label "beatbox"
  ]
  node [
    id 129
    label "wykonywa&#263;"
  ]
  node [
    id 130
    label "kapela"
  ]
  node [
    id 131
    label "przedmiot"
  ]
  node [
    id 132
    label "komponowa&#263;"
  ]
  node [
    id 133
    label "nauka"
  ]
  node [
    id 134
    label "wykonywanie"
  ]
  node [
    id 135
    label "pasa&#380;"
  ]
  node [
    id 136
    label "komponowanie"
  ]
  node [
    id 137
    label "notacja_muzyczna"
  ]
  node [
    id 138
    label "kontrapunkt"
  ]
  node [
    id 139
    label "zjawisko"
  ]
  node [
    id 140
    label "britpop"
  ]
  node [
    id 141
    label "set"
  ]
  node [
    id 142
    label "muza"
  ]
  node [
    id 143
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 144
    label "korespondent"
  ]
  node [
    id 145
    label "sprawko"
  ]
  node [
    id 146
    label "message"
  ]
  node [
    id 147
    label "parafrazowanie"
  ]
  node [
    id 148
    label "komunikat"
  ]
  node [
    id 149
    label "stylizacja"
  ]
  node [
    id 150
    label "sparafrazowanie"
  ]
  node [
    id 151
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 152
    label "strawestowanie"
  ]
  node [
    id 153
    label "sformu&#322;owanie"
  ]
  node [
    id 154
    label "strawestowa&#263;"
  ]
  node [
    id 155
    label "parafrazowa&#263;"
  ]
  node [
    id 156
    label "delimitacja"
  ]
  node [
    id 157
    label "rezultat"
  ]
  node [
    id 158
    label "ozdobnik"
  ]
  node [
    id 159
    label "sparafrazowa&#263;"
  ]
  node [
    id 160
    label "s&#261;d"
  ]
  node [
    id 161
    label "trawestowa&#263;"
  ]
  node [
    id 162
    label "trawestowanie"
  ]
  node [
    id 163
    label "relacja"
  ]
  node [
    id 164
    label "reporter"
  ]
  node [
    id 165
    label "activity"
  ]
  node [
    id 166
    label "absolutorium"
  ]
  node [
    id 167
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 168
    label "dzia&#322;anie"
  ]
  node [
    id 169
    label "nakr&#281;canie"
  ]
  node [
    id 170
    label "nakr&#281;cenie"
  ]
  node [
    id 171
    label "zatrzymanie"
  ]
  node [
    id 172
    label "dzianie_si&#281;"
  ]
  node [
    id 173
    label "liczenie"
  ]
  node [
    id 174
    label "docieranie"
  ]
  node [
    id 175
    label "natural_process"
  ]
  node [
    id 176
    label "skutek"
  ]
  node [
    id 177
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 178
    label "w&#322;&#261;czanie"
  ]
  node [
    id 179
    label "liczy&#263;"
  ]
  node [
    id 180
    label "powodowanie"
  ]
  node [
    id 181
    label "w&#322;&#261;czenie"
  ]
  node [
    id 182
    label "rozpocz&#281;cie"
  ]
  node [
    id 183
    label "priorytet"
  ]
  node [
    id 184
    label "matematyka"
  ]
  node [
    id 185
    label "czynny"
  ]
  node [
    id 186
    label "uruchomienie"
  ]
  node [
    id 187
    label "podzia&#322;anie"
  ]
  node [
    id 188
    label "cz&#322;owiek"
  ]
  node [
    id 189
    label "bycie"
  ]
  node [
    id 190
    label "impact"
  ]
  node [
    id 191
    label "kampania"
  ]
  node [
    id 192
    label "kres"
  ]
  node [
    id 193
    label "podtrzymywanie"
  ]
  node [
    id 194
    label "tr&#243;jstronny"
  ]
  node [
    id 195
    label "funkcja"
  ]
  node [
    id 196
    label "act"
  ]
  node [
    id 197
    label "uruchamianie"
  ]
  node [
    id 198
    label "oferta"
  ]
  node [
    id 199
    label "rzut"
  ]
  node [
    id 200
    label "zadzia&#322;anie"
  ]
  node [
    id 201
    label "operacja"
  ]
  node [
    id 202
    label "wp&#322;yw"
  ]
  node [
    id 203
    label "zako&#324;czenie"
  ]
  node [
    id 204
    label "jednostka"
  ]
  node [
    id 205
    label "hipnotyzowanie"
  ]
  node [
    id 206
    label "operation"
  ]
  node [
    id 207
    label "supremum"
  ]
  node [
    id 208
    label "reakcja_chemiczna"
  ]
  node [
    id 209
    label "robienie"
  ]
  node [
    id 210
    label "infimum"
  ]
  node [
    id 211
    label "wdzieranie_si&#281;"
  ]
  node [
    id 212
    label "ocena"
  ]
  node [
    id 213
    label "uko&#324;czenie"
  ]
  node [
    id 214
    label "graduation"
  ]
  node [
    id 215
    label "kapita&#322;"
  ]
  node [
    id 216
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 217
    label "narodowo"
  ]
  node [
    id 218
    label "wa&#380;ny"
  ]
  node [
    id 219
    label "nacjonalistyczny"
  ]
  node [
    id 220
    label "znaczny"
  ]
  node [
    id 221
    label "silny"
  ]
  node [
    id 222
    label "wa&#380;nie"
  ]
  node [
    id 223
    label "eksponowany"
  ]
  node [
    id 224
    label "wynios&#322;y"
  ]
  node [
    id 225
    label "dobry"
  ]
  node [
    id 226
    label "istotnie"
  ]
  node [
    id 227
    label "dono&#347;ny"
  ]
  node [
    id 228
    label "taki"
  ]
  node [
    id 229
    label "stosownie"
  ]
  node [
    id 230
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 231
    label "prawdziwy"
  ]
  node [
    id 232
    label "typowy"
  ]
  node [
    id 233
    label "zasadniczy"
  ]
  node [
    id 234
    label "charakterystyczny"
  ]
  node [
    id 235
    label "uprawniony"
  ]
  node [
    id 236
    label "nale&#380;yty"
  ]
  node [
    id 237
    label "ten"
  ]
  node [
    id 238
    label "nale&#380;ny"
  ]
  node [
    id 239
    label "nacjonalistycznie"
  ]
  node [
    id 240
    label "polityczny"
  ]
  node [
    id 241
    label "narodowo&#347;ciowy"
  ]
  node [
    id 242
    label "eurorynek"
  ]
  node [
    id 243
    label "konto"
  ]
  node [
    id 244
    label "wk&#322;adca"
  ]
  node [
    id 245
    label "agencja"
  ]
  node [
    id 246
    label "agent_rozliczeniowy"
  ]
  node [
    id 247
    label "kwota"
  ]
  node [
    id 248
    label "instytucja"
  ]
  node [
    id 249
    label "poj&#281;cie"
  ]
  node [
    id 250
    label "uprawianie"
  ]
  node [
    id 251
    label "collection"
  ]
  node [
    id 252
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 253
    label "album"
  ]
  node [
    id 254
    label "praca_rolnicza"
  ]
  node [
    id 255
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 256
    label "sum"
  ]
  node [
    id 257
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 258
    label "series"
  ]
  node [
    id 259
    label "dane"
  ]
  node [
    id 260
    label "pieni&#261;dze"
  ]
  node [
    id 261
    label "wynie&#347;&#263;"
  ]
  node [
    id 262
    label "ilo&#347;&#263;"
  ]
  node [
    id 263
    label "limit"
  ]
  node [
    id 264
    label "wynosi&#263;"
  ]
  node [
    id 265
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 266
    label "afiliowa&#263;"
  ]
  node [
    id 267
    label "establishment"
  ]
  node [
    id 268
    label "zamyka&#263;"
  ]
  node [
    id 269
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 270
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 271
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 272
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 273
    label "standard"
  ]
  node [
    id 274
    label "Fundusze_Unijne"
  ]
  node [
    id 275
    label "biuro"
  ]
  node [
    id 276
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 277
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 278
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 279
    label "zamykanie"
  ]
  node [
    id 280
    label "organizacja"
  ]
  node [
    id 281
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 282
    label "osoba_prawna"
  ]
  node [
    id 283
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 284
    label "rynek_mi&#281;dzynarodowy"
  ]
  node [
    id 285
    label "sie&#263;"
  ]
  node [
    id 286
    label "rynek_finansowy"
  ]
  node [
    id 287
    label "dzia&#322;"
  ]
  node [
    id 288
    label "przedstawicielstwo"
  ]
  node [
    id 289
    label "filia"
  ]
  node [
    id 290
    label "NASA"
  ]
  node [
    id 291
    label "whole"
  ]
  node [
    id 292
    label "ajencja"
  ]
  node [
    id 293
    label "firma"
  ]
  node [
    id 294
    label "reprezentacja"
  ]
  node [
    id 295
    label "kredyt"
  ]
  node [
    id 296
    label "kariera"
  ]
  node [
    id 297
    label "rejestrowa&#263;_si&#281;"
  ]
  node [
    id 298
    label "debet"
  ]
  node [
    id 299
    label "dorobek"
  ]
  node [
    id 300
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 301
    label "mienie"
  ]
  node [
    id 302
    label "dost&#281;p"
  ]
  node [
    id 303
    label "logowa&#263;_si&#281;"
  ]
  node [
    id 304
    label "rachunek"
  ]
  node [
    id 305
    label "subkonto"
  ]
  node [
    id 306
    label "klient"
  ]
  node [
    id 307
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 308
    label "pierogi_ruskie"
  ]
  node [
    id 309
    label "oberek"
  ]
  node [
    id 310
    label "po_polsku"
  ]
  node [
    id 311
    label "goniony"
  ]
  node [
    id 312
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 313
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 314
    label "polsko"
  ]
  node [
    id 315
    label "Polish"
  ]
  node [
    id 316
    label "mazur"
  ]
  node [
    id 317
    label "skoczny"
  ]
  node [
    id 318
    label "j&#281;zyk"
  ]
  node [
    id 319
    label "chodzony"
  ]
  node [
    id 320
    label "krakowiak"
  ]
  node [
    id 321
    label "ryba_po_grecku"
  ]
  node [
    id 322
    label "polak"
  ]
  node [
    id 323
    label "lacki"
  ]
  node [
    id 324
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 325
    label "sztajer"
  ]
  node [
    id 326
    label "drabant"
  ]
  node [
    id 327
    label "pisa&#263;"
  ]
  node [
    id 328
    label "kod"
  ]
  node [
    id 329
    label "pype&#263;"
  ]
  node [
    id 330
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 331
    label "gramatyka"
  ]
  node [
    id 332
    label "language"
  ]
  node [
    id 333
    label "fonetyka"
  ]
  node [
    id 334
    label "t&#322;umaczenie"
  ]
  node [
    id 335
    label "artykulator"
  ]
  node [
    id 336
    label "rozumienie"
  ]
  node [
    id 337
    label "jama_ustna"
  ]
  node [
    id 338
    label "urz&#261;dzenie"
  ]
  node [
    id 339
    label "ssanie"
  ]
  node [
    id 340
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 341
    label "lizanie"
  ]
  node [
    id 342
    label "liza&#263;"
  ]
  node [
    id 343
    label "makroglosja"
  ]
  node [
    id 344
    label "natural_language"
  ]
  node [
    id 345
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 346
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 347
    label "napisa&#263;"
  ]
  node [
    id 348
    label "m&#243;wienie"
  ]
  node [
    id 349
    label "s&#322;ownictwo"
  ]
  node [
    id 350
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 351
    label "konsonantyzm"
  ]
  node [
    id 352
    label "ssa&#263;"
  ]
  node [
    id 353
    label "wokalizm"
  ]
  node [
    id 354
    label "kultura_duchowa"
  ]
  node [
    id 355
    label "formalizowanie"
  ]
  node [
    id 356
    label "jeniec"
  ]
  node [
    id 357
    label "m&#243;wi&#263;"
  ]
  node [
    id 358
    label "kawa&#322;ek"
  ]
  node [
    id 359
    label "po_koroniarsku"
  ]
  node [
    id 360
    label "rozumie&#263;"
  ]
  node [
    id 361
    label "stylik"
  ]
  node [
    id 362
    label "przet&#322;umaczenie"
  ]
  node [
    id 363
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 364
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 365
    label "spos&#243;b"
  ]
  node [
    id 366
    label "but"
  ]
  node [
    id 367
    label "pismo"
  ]
  node [
    id 368
    label "formalizowa&#263;"
  ]
  node [
    id 369
    label "wschodnioeuropejski"
  ]
  node [
    id 370
    label "europejski"
  ]
  node [
    id 371
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 372
    label "topielec"
  ]
  node [
    id 373
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 374
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 375
    label "poga&#324;ski"
  ]
  node [
    id 376
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 377
    label "langosz"
  ]
  node [
    id 378
    label "discipline"
  ]
  node [
    id 379
    label "zboczy&#263;"
  ]
  node [
    id 380
    label "w&#261;tek"
  ]
  node [
    id 381
    label "kultura"
  ]
  node [
    id 382
    label "entity"
  ]
  node [
    id 383
    label "sponiewiera&#263;"
  ]
  node [
    id 384
    label "zboczenie"
  ]
  node [
    id 385
    label "zbaczanie"
  ]
  node [
    id 386
    label "charakter"
  ]
  node [
    id 387
    label "thing"
  ]
  node [
    id 388
    label "om&#243;wi&#263;"
  ]
  node [
    id 389
    label "tre&#347;&#263;"
  ]
  node [
    id 390
    label "element"
  ]
  node [
    id 391
    label "kr&#261;&#380;enie"
  ]
  node [
    id 392
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 393
    label "istota"
  ]
  node [
    id 394
    label "zbacza&#263;"
  ]
  node [
    id 395
    label "om&#243;wienie"
  ]
  node [
    id 396
    label "rzecz"
  ]
  node [
    id 397
    label "tematyka"
  ]
  node [
    id 398
    label "omawianie"
  ]
  node [
    id 399
    label "omawia&#263;"
  ]
  node [
    id 400
    label "program_nauczania"
  ]
  node [
    id 401
    label "sponiewieranie"
  ]
  node [
    id 402
    label "gwardzista"
  ]
  node [
    id 403
    label "taniec"
  ]
  node [
    id 404
    label "&#347;redniowieczny"
  ]
  node [
    id 405
    label "taniec_ludowy"
  ]
  node [
    id 406
    label "melodia"
  ]
  node [
    id 407
    label "rytmiczny"
  ]
  node [
    id 408
    label "sprawny"
  ]
  node [
    id 409
    label "skocznie"
  ]
  node [
    id 410
    label "weso&#322;y"
  ]
  node [
    id 411
    label "energiczny"
  ]
  node [
    id 412
    label "specjalny"
  ]
  node [
    id 413
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 414
    label "austriacki"
  ]
  node [
    id 415
    label "lendler"
  ]
  node [
    id 416
    label "polka"
  ]
  node [
    id 417
    label "europejsko"
  ]
  node [
    id 418
    label "przytup"
  ]
  node [
    id 419
    label "wodzi&#263;"
  ]
  node [
    id 420
    label "ho&#322;ubiec"
  ]
  node [
    id 421
    label "ludowy"
  ]
  node [
    id 422
    label "krakauer"
  ]
  node [
    id 423
    label "lalka"
  ]
  node [
    id 424
    label "mieszkaniec"
  ]
  node [
    id 425
    label "centu&#347;"
  ]
  node [
    id 426
    label "Ma&#322;opolanin"
  ]
  node [
    id 427
    label "pie&#347;&#324;"
  ]
  node [
    id 428
    label "pora_roku"
  ]
  node [
    id 429
    label "kwarta&#322;"
  ]
  node [
    id 430
    label "jubileusz"
  ]
  node [
    id 431
    label "miesi&#261;c"
  ]
  node [
    id 432
    label "martwy_sezon"
  ]
  node [
    id 433
    label "kurs"
  ]
  node [
    id 434
    label "stulecie"
  ]
  node [
    id 435
    label "cykl_astronomiczny"
  ]
  node [
    id 436
    label "czas"
  ]
  node [
    id 437
    label "lata"
  ]
  node [
    id 438
    label "p&#243;&#322;rocze"
  ]
  node [
    id 439
    label "kalendarz"
  ]
  node [
    id 440
    label "summer"
  ]
  node [
    id 441
    label "chronometria"
  ]
  node [
    id 442
    label "odczyt"
  ]
  node [
    id 443
    label "laba"
  ]
  node [
    id 444
    label "czasoprzestrze&#324;"
  ]
  node [
    id 445
    label "time_period"
  ]
  node [
    id 446
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 447
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 448
    label "Zeitgeist"
  ]
  node [
    id 449
    label "pochodzenie"
  ]
  node [
    id 450
    label "przep&#322;ywanie"
  ]
  node [
    id 451
    label "schy&#322;ek"
  ]
  node [
    id 452
    label "czwarty_wymiar"
  ]
  node [
    id 453
    label "kategoria_gramatyczna"
  ]
  node [
    id 454
    label "poprzedzi&#263;"
  ]
  node [
    id 455
    label "pogoda"
  ]
  node [
    id 456
    label "czasokres"
  ]
  node [
    id 457
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 458
    label "poprzedzenie"
  ]
  node [
    id 459
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 460
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 461
    label "dzieje"
  ]
  node [
    id 462
    label "zegar"
  ]
  node [
    id 463
    label "koniugacja"
  ]
  node [
    id 464
    label "trawi&#263;"
  ]
  node [
    id 465
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 466
    label "poprzedza&#263;"
  ]
  node [
    id 467
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 468
    label "trawienie"
  ]
  node [
    id 469
    label "chwila"
  ]
  node [
    id 470
    label "rachuba_czasu"
  ]
  node [
    id 471
    label "poprzedzanie"
  ]
  node [
    id 472
    label "okres_czasu"
  ]
  node [
    id 473
    label "period"
  ]
  node [
    id 474
    label "odwlekanie_si&#281;"
  ]
  node [
    id 475
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 476
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 477
    label "pochodzi&#263;"
  ]
  node [
    id 478
    label "rok_szkolny"
  ]
  node [
    id 479
    label "term"
  ]
  node [
    id 480
    label "rok_akademicki"
  ]
  node [
    id 481
    label "semester"
  ]
  node [
    id 482
    label "rocznica"
  ]
  node [
    id 483
    label "anniwersarz"
  ]
  node [
    id 484
    label "obszar"
  ]
  node [
    id 485
    label "tydzie&#324;"
  ]
  node [
    id 486
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 487
    label "miech"
  ]
  node [
    id 488
    label "kalendy"
  ]
  node [
    id 489
    label "long_time"
  ]
  node [
    id 490
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 491
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 492
    label "almanac"
  ]
  node [
    id 493
    label "wydawnictwo"
  ]
  node [
    id 494
    label "rozk&#322;ad"
  ]
  node [
    id 495
    label "Juliusz_Cezar"
  ]
  node [
    id 496
    label "cedu&#322;a"
  ]
  node [
    id 497
    label "zwy&#380;kowanie"
  ]
  node [
    id 498
    label "manner"
  ]
  node [
    id 499
    label "przeorientowanie"
  ]
  node [
    id 500
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 501
    label "przejazd"
  ]
  node [
    id 502
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 503
    label "deprecjacja"
  ]
  node [
    id 504
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 505
    label "klasa"
  ]
  node [
    id 506
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 507
    label "drive"
  ]
  node [
    id 508
    label "stawka"
  ]
  node [
    id 509
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 510
    label "przeorientowywanie"
  ]
  node [
    id 511
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 512
    label "seria"
  ]
  node [
    id 513
    label "Lira"
  ]
  node [
    id 514
    label "course"
  ]
  node [
    id 515
    label "passage"
  ]
  node [
    id 516
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 517
    label "trasa"
  ]
  node [
    id 518
    label "przeorientowa&#263;"
  ]
  node [
    id 519
    label "bearing"
  ]
  node [
    id 520
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 521
    label "way"
  ]
  node [
    id 522
    label "zni&#380;kowanie"
  ]
  node [
    id 523
    label "przeorientowywa&#263;"
  ]
  node [
    id 524
    label "kierunek"
  ]
  node [
    id 525
    label "zaj&#281;cia"
  ]
  node [
    id 526
    label "opisa&#263;"
  ]
  node [
    id 527
    label "przedstawienie"
  ]
  node [
    id 528
    label "express"
  ]
  node [
    id 529
    label "ukaza&#263;"
  ]
  node [
    id 530
    label "poda&#263;"
  ]
  node [
    id 531
    label "pokaza&#263;"
  ]
  node [
    id 532
    label "zaproponowa&#263;"
  ]
  node [
    id 533
    label "zademonstrowa&#263;"
  ]
  node [
    id 534
    label "zapozna&#263;"
  ]
  node [
    id 535
    label "typify"
  ]
  node [
    id 536
    label "represent"
  ]
  node [
    id 537
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 538
    label "siatk&#243;wka"
  ]
  node [
    id 539
    label "nafaszerowa&#263;"
  ]
  node [
    id 540
    label "supply"
  ]
  node [
    id 541
    label "tenis"
  ]
  node [
    id 542
    label "jedzenie"
  ]
  node [
    id 543
    label "ustawi&#263;"
  ]
  node [
    id 544
    label "poinformowa&#263;"
  ]
  node [
    id 545
    label "give"
  ]
  node [
    id 546
    label "zagra&#263;"
  ]
  node [
    id 547
    label "da&#263;"
  ]
  node [
    id 548
    label "zaserwowa&#263;"
  ]
  node [
    id 549
    label "introduce"
  ]
  node [
    id 550
    label "testify"
  ]
  node [
    id 551
    label "przeszkoli&#263;"
  ]
  node [
    id 552
    label "udowodni&#263;"
  ]
  node [
    id 553
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 554
    label "wyrazi&#263;"
  ]
  node [
    id 555
    label "spowodowa&#263;"
  ]
  node [
    id 556
    label "point"
  ]
  node [
    id 557
    label "indicate"
  ]
  node [
    id 558
    label "kandydatura"
  ]
  node [
    id 559
    label "volunteer"
  ]
  node [
    id 560
    label "announce"
  ]
  node [
    id 561
    label "zach&#281;ci&#263;"
  ]
  node [
    id 562
    label "nak&#322;oni&#263;"
  ]
  node [
    id 563
    label "odst&#261;pi&#263;"
  ]
  node [
    id 564
    label "zacz&#261;&#263;"
  ]
  node [
    id 565
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 566
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 567
    label "wyj&#347;&#263;"
  ]
  node [
    id 568
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 569
    label "zrezygnowa&#263;"
  ]
  node [
    id 570
    label "appear"
  ]
  node [
    id 571
    label "happen"
  ]
  node [
    id 572
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 573
    label "unwrap"
  ]
  node [
    id 574
    label "delineate"
  ]
  node [
    id 575
    label "zinterpretowa&#263;"
  ]
  node [
    id 576
    label "relate"
  ]
  node [
    id 577
    label "obznajomi&#263;"
  ]
  node [
    id 578
    label "teach"
  ]
  node [
    id 579
    label "pozna&#263;"
  ]
  node [
    id 580
    label "insert"
  ]
  node [
    id 581
    label "zawrze&#263;"
  ]
  node [
    id 582
    label "podanie"
  ]
  node [
    id 583
    label "ods&#322;ona"
  ]
  node [
    id 584
    label "opisanie"
  ]
  node [
    id 585
    label "pokazanie"
  ]
  node [
    id 586
    label "wyst&#261;pienie"
  ]
  node [
    id 587
    label "ukazanie"
  ]
  node [
    id 588
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 589
    label "zapoznanie"
  ]
  node [
    id 590
    label "przedstawia&#263;"
  ]
  node [
    id 591
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 592
    label "posta&#263;"
  ]
  node [
    id 593
    label "scena"
  ]
  node [
    id 594
    label "malarstwo"
  ]
  node [
    id 595
    label "theatrical_performance"
  ]
  node [
    id 596
    label "pokaz"
  ]
  node [
    id 597
    label "teatr"
  ]
  node [
    id 598
    label "pr&#243;bowanie"
  ]
  node [
    id 599
    label "obgadanie"
  ]
  node [
    id 600
    label "przedstawianie"
  ]
  node [
    id 601
    label "report"
  ]
  node [
    id 602
    label "exhibit"
  ]
  node [
    id 603
    label "narration"
  ]
  node [
    id 604
    label "cyrk"
  ]
  node [
    id 605
    label "scenografia"
  ]
  node [
    id 606
    label "realizacja"
  ]
  node [
    id 607
    label "rola"
  ]
  node [
    id 608
    label "zademonstrowanie"
  ]
  node [
    id 609
    label "murza"
  ]
  node [
    id 610
    label "belfer"
  ]
  node [
    id 611
    label "szkolnik"
  ]
  node [
    id 612
    label "pupil"
  ]
  node [
    id 613
    label "ojciec"
  ]
  node [
    id 614
    label "kszta&#322;ciciel"
  ]
  node [
    id 615
    label "Midas"
  ]
  node [
    id 616
    label "przyw&#243;dca"
  ]
  node [
    id 617
    label "opiekun"
  ]
  node [
    id 618
    label "Mieszko_I"
  ]
  node [
    id 619
    label "doros&#322;y"
  ]
  node [
    id 620
    label "pracodawca"
  ]
  node [
    id 621
    label "profesor"
  ]
  node [
    id 622
    label "m&#261;&#380;"
  ]
  node [
    id 623
    label "rz&#261;dzenie"
  ]
  node [
    id 624
    label "bogaty"
  ]
  node [
    id 625
    label "pa&#324;stwo"
  ]
  node [
    id 626
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 627
    label "w&#322;odarz"
  ]
  node [
    id 628
    label "nabab"
  ]
  node [
    id 629
    label "preceptor"
  ]
  node [
    id 630
    label "samiec"
  ]
  node [
    id 631
    label "pedagog"
  ]
  node [
    id 632
    label "efendi"
  ]
  node [
    id 633
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 634
    label "popularyzator"
  ]
  node [
    id 635
    label "gra_w_karty"
  ]
  node [
    id 636
    label "zwrot"
  ]
  node [
    id 637
    label "jegomo&#347;&#263;"
  ]
  node [
    id 638
    label "androlog"
  ]
  node [
    id 639
    label "bratek"
  ]
  node [
    id 640
    label "andropauza"
  ]
  node [
    id 641
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 642
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 643
    label "ch&#322;opina"
  ]
  node [
    id 644
    label "w&#322;adza"
  ]
  node [
    id 645
    label "Sabataj_Cwi"
  ]
  node [
    id 646
    label "lider"
  ]
  node [
    id 647
    label "Mao"
  ]
  node [
    id 648
    label "Anders"
  ]
  node [
    id 649
    label "Fidel_Castro"
  ]
  node [
    id 650
    label "Miko&#322;ajczyk"
  ]
  node [
    id 651
    label "Tito"
  ]
  node [
    id 652
    label "Ko&#347;ciuszko"
  ]
  node [
    id 653
    label "p&#322;atnik"
  ]
  node [
    id 654
    label "zwierzchnik"
  ]
  node [
    id 655
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 656
    label "nadzorca"
  ]
  node [
    id 657
    label "funkcjonariusz"
  ]
  node [
    id 658
    label "podmiot"
  ]
  node [
    id 659
    label "wykupywanie"
  ]
  node [
    id 660
    label "wykupienie"
  ]
  node [
    id 661
    label "bycie_w_posiadaniu"
  ]
  node [
    id 662
    label "rozszerzyciel"
  ]
  node [
    id 663
    label "nasada"
  ]
  node [
    id 664
    label "profanum"
  ]
  node [
    id 665
    label "wz&#243;r"
  ]
  node [
    id 666
    label "senior"
  ]
  node [
    id 667
    label "os&#322;abia&#263;"
  ]
  node [
    id 668
    label "homo_sapiens"
  ]
  node [
    id 669
    label "osoba"
  ]
  node [
    id 670
    label "ludzko&#347;&#263;"
  ]
  node [
    id 671
    label "Adam"
  ]
  node [
    id 672
    label "hominid"
  ]
  node [
    id 673
    label "portrecista"
  ]
  node [
    id 674
    label "polifag"
  ]
  node [
    id 675
    label "podw&#322;adny"
  ]
  node [
    id 676
    label "dwun&#243;g"
  ]
  node [
    id 677
    label "wapniak"
  ]
  node [
    id 678
    label "duch"
  ]
  node [
    id 679
    label "os&#322;abianie"
  ]
  node [
    id 680
    label "antropochoria"
  ]
  node [
    id 681
    label "figura"
  ]
  node [
    id 682
    label "g&#322;owa"
  ]
  node [
    id 683
    label "mikrokosmos"
  ]
  node [
    id 684
    label "oddzia&#322;ywanie"
  ]
  node [
    id 685
    label "du&#380;y"
  ]
  node [
    id 686
    label "dojrza&#322;y"
  ]
  node [
    id 687
    label "dojrzale"
  ]
  node [
    id 688
    label "doro&#347;lenie"
  ]
  node [
    id 689
    label "wydoro&#347;lenie"
  ]
  node [
    id 690
    label "m&#261;dry"
  ]
  node [
    id 691
    label "&#378;ra&#322;y"
  ]
  node [
    id 692
    label "doletni"
  ]
  node [
    id 693
    label "doro&#347;le"
  ]
  node [
    id 694
    label "zmiana"
  ]
  node [
    id 695
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 696
    label "turn"
  ]
  node [
    id 697
    label "wyra&#380;enie"
  ]
  node [
    id 698
    label "fraza_czasownikowa"
  ]
  node [
    id 699
    label "turning"
  ]
  node [
    id 700
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 701
    label "skr&#281;t"
  ]
  node [
    id 702
    label "jednostka_leksykalna"
  ]
  node [
    id 703
    label "obr&#243;t"
  ]
  node [
    id 704
    label "starosta"
  ]
  node [
    id 705
    label "w&#322;adca"
  ]
  node [
    id 706
    label "zarz&#261;dca"
  ]
  node [
    id 707
    label "nauczyciel"
  ]
  node [
    id 708
    label "autor"
  ]
  node [
    id 709
    label "tarcza"
  ]
  node [
    id 710
    label "elew"
  ]
  node [
    id 711
    label "wyprawka"
  ]
  node [
    id 712
    label "mundurek"
  ]
  node [
    id 713
    label "absolwent"
  ]
  node [
    id 714
    label "nauczyciel_akademicki"
  ]
  node [
    id 715
    label "tytu&#322;"
  ]
  node [
    id 716
    label "stopie&#324;_naukowy"
  ]
  node [
    id 717
    label "konsulent"
  ]
  node [
    id 718
    label "profesura"
  ]
  node [
    id 719
    label "wirtuoz"
  ]
  node [
    id 720
    label "ochotnik"
  ]
  node [
    id 721
    label "nauczyciel_muzyki"
  ]
  node [
    id 722
    label "pomocnik"
  ]
  node [
    id 723
    label "zakonnik"
  ]
  node [
    id 724
    label "student"
  ]
  node [
    id 725
    label "ekspert"
  ]
  node [
    id 726
    label "bogacz"
  ]
  node [
    id 727
    label "dostojnik"
  ]
  node [
    id 728
    label "urz&#281;dnik"
  ]
  node [
    id 729
    label "mo&#347;&#263;"
  ]
  node [
    id 730
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 731
    label "&#347;w"
  ]
  node [
    id 732
    label "rodzic"
  ]
  node [
    id 733
    label "pomys&#322;odawca"
  ]
  node [
    id 734
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 735
    label "rodzice"
  ]
  node [
    id 736
    label "wykonawca"
  ]
  node [
    id 737
    label "stary"
  ]
  node [
    id 738
    label "kuwada"
  ]
  node [
    id 739
    label "ojczym"
  ]
  node [
    id 740
    label "papa"
  ]
  node [
    id 741
    label "przodek"
  ]
  node [
    id 742
    label "tworzyciel"
  ]
  node [
    id 743
    label "zwierz&#281;"
  ]
  node [
    id 744
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 745
    label "facet"
  ]
  node [
    id 746
    label "kochanek"
  ]
  node [
    id 747
    label "fio&#322;ek"
  ]
  node [
    id 748
    label "brat"
  ]
  node [
    id 749
    label "pan_i_w&#322;adca"
  ]
  node [
    id 750
    label "pan_m&#322;ody"
  ]
  node [
    id 751
    label "ch&#322;op"
  ]
  node [
    id 752
    label "&#347;lubny"
  ]
  node [
    id 753
    label "m&#243;j"
  ]
  node [
    id 754
    label "pan_domu"
  ]
  node [
    id 755
    label "ma&#322;&#380;onek"
  ]
  node [
    id 756
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 757
    label "Frygia"
  ]
  node [
    id 758
    label "dominowanie"
  ]
  node [
    id 759
    label "reign"
  ]
  node [
    id 760
    label "sprawowanie"
  ]
  node [
    id 761
    label "dominion"
  ]
  node [
    id 762
    label "rule"
  ]
  node [
    id 763
    label "zwierz&#281;_domowe"
  ]
  node [
    id 764
    label "John_Dewey"
  ]
  node [
    id 765
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 766
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 767
    label "J&#281;drzejewicz"
  ]
  node [
    id 768
    label "specjalista"
  ]
  node [
    id 769
    label "&#380;ycie"
  ]
  node [
    id 770
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 771
    label "Turek"
  ]
  node [
    id 772
    label "effendi"
  ]
  node [
    id 773
    label "och&#281;do&#380;ny"
  ]
  node [
    id 774
    label "zapa&#347;ny"
  ]
  node [
    id 775
    label "sytuowany"
  ]
  node [
    id 776
    label "obfituj&#261;cy"
  ]
  node [
    id 777
    label "forsiasty"
  ]
  node [
    id 778
    label "spania&#322;y"
  ]
  node [
    id 779
    label "obficie"
  ]
  node [
    id 780
    label "r&#243;&#380;norodny"
  ]
  node [
    id 781
    label "bogato"
  ]
  node [
    id 782
    label "Japonia"
  ]
  node [
    id 783
    label "Zair"
  ]
  node [
    id 784
    label "Belize"
  ]
  node [
    id 785
    label "San_Marino"
  ]
  node [
    id 786
    label "Tanzania"
  ]
  node [
    id 787
    label "Antigua_i_Barbuda"
  ]
  node [
    id 788
    label "granica_pa&#324;stwa"
  ]
  node [
    id 789
    label "Senegal"
  ]
  node [
    id 790
    label "Seszele"
  ]
  node [
    id 791
    label "Mauretania"
  ]
  node [
    id 792
    label "Indie"
  ]
  node [
    id 793
    label "Filipiny"
  ]
  node [
    id 794
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 795
    label "Zimbabwe"
  ]
  node [
    id 796
    label "Malezja"
  ]
  node [
    id 797
    label "Rumunia"
  ]
  node [
    id 798
    label "Surinam"
  ]
  node [
    id 799
    label "Ukraina"
  ]
  node [
    id 800
    label "Syria"
  ]
  node [
    id 801
    label "Wyspy_Marshalla"
  ]
  node [
    id 802
    label "Burkina_Faso"
  ]
  node [
    id 803
    label "Grecja"
  ]
  node [
    id 804
    label "Polska"
  ]
  node [
    id 805
    label "Wenezuela"
  ]
  node [
    id 806
    label "Suazi"
  ]
  node [
    id 807
    label "Nepal"
  ]
  node [
    id 808
    label "S&#322;owacja"
  ]
  node [
    id 809
    label "Algieria"
  ]
  node [
    id 810
    label "Chiny"
  ]
  node [
    id 811
    label "Grenada"
  ]
  node [
    id 812
    label "Barbados"
  ]
  node [
    id 813
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 814
    label "Pakistan"
  ]
  node [
    id 815
    label "Niemcy"
  ]
  node [
    id 816
    label "Bahrajn"
  ]
  node [
    id 817
    label "Komory"
  ]
  node [
    id 818
    label "Australia"
  ]
  node [
    id 819
    label "Rodezja"
  ]
  node [
    id 820
    label "Malawi"
  ]
  node [
    id 821
    label "Gwinea"
  ]
  node [
    id 822
    label "Wehrlen"
  ]
  node [
    id 823
    label "Meksyk"
  ]
  node [
    id 824
    label "Liechtenstein"
  ]
  node [
    id 825
    label "Czarnog&#243;ra"
  ]
  node [
    id 826
    label "Wielka_Brytania"
  ]
  node [
    id 827
    label "Kuwejt"
  ]
  node [
    id 828
    label "Monako"
  ]
  node [
    id 829
    label "Angola"
  ]
  node [
    id 830
    label "Jemen"
  ]
  node [
    id 831
    label "Etiopia"
  ]
  node [
    id 832
    label "Madagaskar"
  ]
  node [
    id 833
    label "terytorium"
  ]
  node [
    id 834
    label "Kolumbia"
  ]
  node [
    id 835
    label "Portoryko"
  ]
  node [
    id 836
    label "Mauritius"
  ]
  node [
    id 837
    label "Kostaryka"
  ]
  node [
    id 838
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 839
    label "Tajlandia"
  ]
  node [
    id 840
    label "Argentyna"
  ]
  node [
    id 841
    label "Zambia"
  ]
  node [
    id 842
    label "Sri_Lanka"
  ]
  node [
    id 843
    label "Gwatemala"
  ]
  node [
    id 844
    label "Kirgistan"
  ]
  node [
    id 845
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 846
    label "Hiszpania"
  ]
  node [
    id 847
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 848
    label "Salwador"
  ]
  node [
    id 849
    label "Korea"
  ]
  node [
    id 850
    label "Macedonia"
  ]
  node [
    id 851
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 852
    label "Brunei"
  ]
  node [
    id 853
    label "Mozambik"
  ]
  node [
    id 854
    label "Turcja"
  ]
  node [
    id 855
    label "Kambod&#380;a"
  ]
  node [
    id 856
    label "Benin"
  ]
  node [
    id 857
    label "Bhutan"
  ]
  node [
    id 858
    label "Tunezja"
  ]
  node [
    id 859
    label "Austria"
  ]
  node [
    id 860
    label "Izrael"
  ]
  node [
    id 861
    label "Sierra_Leone"
  ]
  node [
    id 862
    label "Jamajka"
  ]
  node [
    id 863
    label "Rosja"
  ]
  node [
    id 864
    label "Rwanda"
  ]
  node [
    id 865
    label "holoarktyka"
  ]
  node [
    id 866
    label "Nigeria"
  ]
  node [
    id 867
    label "USA"
  ]
  node [
    id 868
    label "Oman"
  ]
  node [
    id 869
    label "Luksemburg"
  ]
  node [
    id 870
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 871
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 872
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 873
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 874
    label "Dominikana"
  ]
  node [
    id 875
    label "Irlandia"
  ]
  node [
    id 876
    label "Liban"
  ]
  node [
    id 877
    label "Hanower"
  ]
  node [
    id 878
    label "Estonia"
  ]
  node [
    id 879
    label "Iran"
  ]
  node [
    id 880
    label "Nowa_Zelandia"
  ]
  node [
    id 881
    label "Gabon"
  ]
  node [
    id 882
    label "Samoa"
  ]
  node [
    id 883
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 884
    label "S&#322;owenia"
  ]
  node [
    id 885
    label "Kiribati"
  ]
  node [
    id 886
    label "Egipt"
  ]
  node [
    id 887
    label "Togo"
  ]
  node [
    id 888
    label "Mongolia"
  ]
  node [
    id 889
    label "Sudan"
  ]
  node [
    id 890
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 891
    label "Bahamy"
  ]
  node [
    id 892
    label "Bangladesz"
  ]
  node [
    id 893
    label "partia"
  ]
  node [
    id 894
    label "Serbia"
  ]
  node [
    id 895
    label "Czechy"
  ]
  node [
    id 896
    label "Holandia"
  ]
  node [
    id 897
    label "Birma"
  ]
  node [
    id 898
    label "Albania"
  ]
  node [
    id 899
    label "Mikronezja"
  ]
  node [
    id 900
    label "Gambia"
  ]
  node [
    id 901
    label "Kazachstan"
  ]
  node [
    id 902
    label "interior"
  ]
  node [
    id 903
    label "Uzbekistan"
  ]
  node [
    id 904
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 905
    label "Malta"
  ]
  node [
    id 906
    label "Lesoto"
  ]
  node [
    id 907
    label "para"
  ]
  node [
    id 908
    label "Antarktis"
  ]
  node [
    id 909
    label "Andora"
  ]
  node [
    id 910
    label "Nauru"
  ]
  node [
    id 911
    label "Kuba"
  ]
  node [
    id 912
    label "Wietnam"
  ]
  node [
    id 913
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 914
    label "ziemia"
  ]
  node [
    id 915
    label "Kamerun"
  ]
  node [
    id 916
    label "Chorwacja"
  ]
  node [
    id 917
    label "Urugwaj"
  ]
  node [
    id 918
    label "Niger"
  ]
  node [
    id 919
    label "Turkmenistan"
  ]
  node [
    id 920
    label "Szwajcaria"
  ]
  node [
    id 921
    label "Palau"
  ]
  node [
    id 922
    label "Litwa"
  ]
  node [
    id 923
    label "Gruzja"
  ]
  node [
    id 924
    label "Tajwan"
  ]
  node [
    id 925
    label "Kongo"
  ]
  node [
    id 926
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 927
    label "Honduras"
  ]
  node [
    id 928
    label "Boliwia"
  ]
  node [
    id 929
    label "Uganda"
  ]
  node [
    id 930
    label "Namibia"
  ]
  node [
    id 931
    label "Azerbejd&#380;an"
  ]
  node [
    id 932
    label "Erytrea"
  ]
  node [
    id 933
    label "Gujana"
  ]
  node [
    id 934
    label "Panama"
  ]
  node [
    id 935
    label "Somalia"
  ]
  node [
    id 936
    label "Burundi"
  ]
  node [
    id 937
    label "Tuwalu"
  ]
  node [
    id 938
    label "Libia"
  ]
  node [
    id 939
    label "Katar"
  ]
  node [
    id 940
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 941
    label "Sahara_Zachodnia"
  ]
  node [
    id 942
    label "Trynidad_i_Tobago"
  ]
  node [
    id 943
    label "Gwinea_Bissau"
  ]
  node [
    id 944
    label "Bu&#322;garia"
  ]
  node [
    id 945
    label "Fid&#380;i"
  ]
  node [
    id 946
    label "Nikaragua"
  ]
  node [
    id 947
    label "Tonga"
  ]
  node [
    id 948
    label "Timor_Wschodni"
  ]
  node [
    id 949
    label "Laos"
  ]
  node [
    id 950
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 951
    label "Ghana"
  ]
  node [
    id 952
    label "Brazylia"
  ]
  node [
    id 953
    label "Belgia"
  ]
  node [
    id 954
    label "Irak"
  ]
  node [
    id 955
    label "Peru"
  ]
  node [
    id 956
    label "Arabia_Saudyjska"
  ]
  node [
    id 957
    label "Indonezja"
  ]
  node [
    id 958
    label "Malediwy"
  ]
  node [
    id 959
    label "Afganistan"
  ]
  node [
    id 960
    label "Jordania"
  ]
  node [
    id 961
    label "Kenia"
  ]
  node [
    id 962
    label "Czad"
  ]
  node [
    id 963
    label "Liberia"
  ]
  node [
    id 964
    label "W&#281;gry"
  ]
  node [
    id 965
    label "Chile"
  ]
  node [
    id 966
    label "Mali"
  ]
  node [
    id 967
    label "Armenia"
  ]
  node [
    id 968
    label "Kanada"
  ]
  node [
    id 969
    label "Cypr"
  ]
  node [
    id 970
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 971
    label "Ekwador"
  ]
  node [
    id 972
    label "Mo&#322;dawia"
  ]
  node [
    id 973
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 974
    label "W&#322;ochy"
  ]
  node [
    id 975
    label "Wyspy_Salomona"
  ]
  node [
    id 976
    label "&#321;otwa"
  ]
  node [
    id 977
    label "D&#380;ibuti"
  ]
  node [
    id 978
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 979
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 980
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 981
    label "Portugalia"
  ]
  node [
    id 982
    label "Botswana"
  ]
  node [
    id 983
    label "Maroko"
  ]
  node [
    id 984
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 985
    label "Francja"
  ]
  node [
    id 986
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 987
    label "Dominika"
  ]
  node [
    id 988
    label "Paragwaj"
  ]
  node [
    id 989
    label "Tad&#380;ykistan"
  ]
  node [
    id 990
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 991
    label "Haiti"
  ]
  node [
    id 992
    label "Khitai"
  ]
  node [
    id 993
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 994
    label "gruba_ryba"
  ]
  node [
    id 995
    label "kierowa&#263;"
  ]
  node [
    id 996
    label "pryncypa&#322;"
  ]
  node [
    id 997
    label "kierownictwo"
  ]
  node [
    id 998
    label "instrumentalista"
  ]
  node [
    id 999
    label "muzyk"
  ]
  node [
    id 1000
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 1001
    label "wykona&#263;"
  ]
  node [
    id 1002
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 1003
    label "leave"
  ]
  node [
    id 1004
    label "przewie&#347;&#263;"
  ]
  node [
    id 1005
    label "zbudowa&#263;"
  ]
  node [
    id 1006
    label "pom&#243;c"
  ]
  node [
    id 1007
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 1008
    label "draw"
  ]
  node [
    id 1009
    label "carry"
  ]
  node [
    id 1010
    label "dotrze&#263;"
  ]
  node [
    id 1011
    label "make"
  ]
  node [
    id 1012
    label "score"
  ]
  node [
    id 1013
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1014
    label "uzyska&#263;"
  ]
  node [
    id 1015
    label "profit"
  ]
  node [
    id 1016
    label "picture"
  ]
  node [
    id 1017
    label "manufacture"
  ]
  node [
    id 1018
    label "wytworzy&#263;"
  ]
  node [
    id 1019
    label "go"
  ]
  node [
    id 1020
    label "travel"
  ]
  node [
    id 1021
    label "establish"
  ]
  node [
    id 1022
    label "zaplanowa&#263;"
  ]
  node [
    id 1023
    label "budowla"
  ]
  node [
    id 1024
    label "stworzy&#263;"
  ]
  node [
    id 1025
    label "evolve"
  ]
  node [
    id 1026
    label "pokry&#263;"
  ]
  node [
    id 1027
    label "zmieni&#263;"
  ]
  node [
    id 1028
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1029
    label "plant"
  ]
  node [
    id 1030
    label "pozostawi&#263;"
  ]
  node [
    id 1031
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 1032
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 1033
    label "stagger"
  ]
  node [
    id 1034
    label "wear"
  ]
  node [
    id 1035
    label "przygotowa&#263;"
  ]
  node [
    id 1036
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 1037
    label "wygra&#263;"
  ]
  node [
    id 1038
    label "zepsu&#263;"
  ]
  node [
    id 1039
    label "raise"
  ]
  node [
    id 1040
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1041
    label "znak"
  ]
  node [
    id 1042
    label "zaskutkowa&#263;"
  ]
  node [
    id 1043
    label "help"
  ]
  node [
    id 1044
    label "u&#322;atwi&#263;"
  ]
  node [
    id 1045
    label "concur"
  ]
  node [
    id 1046
    label "aid"
  ]
  node [
    id 1047
    label "sympozjon"
  ]
  node [
    id 1048
    label "rozmowa"
  ]
  node [
    id 1049
    label "cisza"
  ]
  node [
    id 1050
    label "odpowied&#378;"
  ]
  node [
    id 1051
    label "rozhowor"
  ]
  node [
    id 1052
    label "discussion"
  ]
  node [
    id 1053
    label "symposium"
  ]
  node [
    id 1054
    label "sympozjarcha"
  ]
  node [
    id 1055
    label "utw&#243;r"
  ]
  node [
    id 1056
    label "rozrywka"
  ]
  node [
    id 1057
    label "konferencja"
  ]
  node [
    id 1058
    label "przyj&#281;cie"
  ]
  node [
    id 1059
    label "faza"
  ]
  node [
    id 1060
    label "esej"
  ]
  node [
    id 1061
    label "polskie"
  ]
  node [
    id 1062
    label "S&#322;awomira"
  ]
  node [
    id 1063
    label "skrzypka"
  ]
  node [
    id 1064
    label "gospodarstwo"
  ]
  node [
    id 1065
    label "krajowy"
  ]
  node [
    id 1066
    label "fundusz"
  ]
  node [
    id 1067
    label "por&#281;czenie"
  ]
  node [
    id 1068
    label "kredytowy"
  ]
  node [
    id 1069
    label "skarb"
  ]
  node [
    id 1070
    label "wysoki"
  ]
  node [
    id 1071
    label "izba"
  ]
  node [
    id 1072
    label "kontrola"
  ]
  node [
    id 1073
    label "komisja"
  ]
  node [
    id 1074
    label "do"
  ]
  node [
    id 1075
    label "sprawi&#263;"
  ]
  node [
    id 1076
    label "pa&#324;stwowy"
  ]
  node [
    id 1077
    label "Jack"
  ]
  node [
    id 1078
    label "jezierski"
  ]
  node [
    id 1079
    label "Jacek"
  ]
  node [
    id 1080
    label "rzeczpospolita"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 1061
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 1061
  ]
  edge [
    source 6
    target 1064
  ]
  edge [
    source 6
    target 1065
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 1080
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 772
  ]
  edge [
    source 11
    target 773
  ]
  edge [
    source 11
    target 774
  ]
  edge [
    source 11
    target 775
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 777
  ]
  edge [
    source 11
    target 778
  ]
  edge [
    source 11
    target 779
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 781
  ]
  edge [
    source 11
    target 782
  ]
  edge [
    source 11
    target 783
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 824
  ]
  edge [
    source 11
    target 825
  ]
  edge [
    source 11
    target 826
  ]
  edge [
    source 11
    target 827
  ]
  edge [
    source 11
    target 828
  ]
  edge [
    source 11
    target 829
  ]
  edge [
    source 11
    target 830
  ]
  edge [
    source 11
    target 831
  ]
  edge [
    source 11
    target 832
  ]
  edge [
    source 11
    target 833
  ]
  edge [
    source 11
    target 834
  ]
  edge [
    source 11
    target 835
  ]
  edge [
    source 11
    target 836
  ]
  edge [
    source 11
    target 837
  ]
  edge [
    source 11
    target 838
  ]
  edge [
    source 11
    target 839
  ]
  edge [
    source 11
    target 840
  ]
  edge [
    source 11
    target 841
  ]
  edge [
    source 11
    target 842
  ]
  edge [
    source 11
    target 843
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 845
  ]
  edge [
    source 11
    target 846
  ]
  edge [
    source 11
    target 847
  ]
  edge [
    source 11
    target 848
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 850
  ]
  edge [
    source 11
    target 851
  ]
  edge [
    source 11
    target 852
  ]
  edge [
    source 11
    target 853
  ]
  edge [
    source 11
    target 854
  ]
  edge [
    source 11
    target 855
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 858
  ]
  edge [
    source 11
    target 859
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 861
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 879
  ]
  edge [
    source 11
    target 880
  ]
  edge [
    source 11
    target 881
  ]
  edge [
    source 11
    target 882
  ]
  edge [
    source 11
    target 883
  ]
  edge [
    source 11
    target 884
  ]
  edge [
    source 11
    target 885
  ]
  edge [
    source 11
    target 886
  ]
  edge [
    source 11
    target 887
  ]
  edge [
    source 11
    target 888
  ]
  edge [
    source 11
    target 889
  ]
  edge [
    source 11
    target 890
  ]
  edge [
    source 11
    target 891
  ]
  edge [
    source 11
    target 892
  ]
  edge [
    source 11
    target 893
  ]
  edge [
    source 11
    target 894
  ]
  edge [
    source 11
    target 895
  ]
  edge [
    source 11
    target 896
  ]
  edge [
    source 11
    target 897
  ]
  edge [
    source 11
    target 898
  ]
  edge [
    source 11
    target 899
  ]
  edge [
    source 11
    target 900
  ]
  edge [
    source 11
    target 901
  ]
  edge [
    source 11
    target 902
  ]
  edge [
    source 11
    target 903
  ]
  edge [
    source 11
    target 904
  ]
  edge [
    source 11
    target 905
  ]
  edge [
    source 11
    target 906
  ]
  edge [
    source 11
    target 907
  ]
  edge [
    source 11
    target 908
  ]
  edge [
    source 11
    target 909
  ]
  edge [
    source 11
    target 910
  ]
  edge [
    source 11
    target 911
  ]
  edge [
    source 11
    target 912
  ]
  edge [
    source 11
    target 913
  ]
  edge [
    source 11
    target 914
  ]
  edge [
    source 11
    target 915
  ]
  edge [
    source 11
    target 916
  ]
  edge [
    source 11
    target 917
  ]
  edge [
    source 11
    target 918
  ]
  edge [
    source 11
    target 919
  ]
  edge [
    source 11
    target 920
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 11
    target 921
  ]
  edge [
    source 11
    target 922
  ]
  edge [
    source 11
    target 923
  ]
  edge [
    source 11
    target 924
  ]
  edge [
    source 11
    target 925
  ]
  edge [
    source 11
    target 926
  ]
  edge [
    source 11
    target 927
  ]
  edge [
    source 11
    target 928
  ]
  edge [
    source 11
    target 929
  ]
  edge [
    source 11
    target 930
  ]
  edge [
    source 11
    target 931
  ]
  edge [
    source 11
    target 932
  ]
  edge [
    source 11
    target 933
  ]
  edge [
    source 11
    target 934
  ]
  edge [
    source 11
    target 935
  ]
  edge [
    source 11
    target 936
  ]
  edge [
    source 11
    target 937
  ]
  edge [
    source 11
    target 938
  ]
  edge [
    source 11
    target 939
  ]
  edge [
    source 11
    target 940
  ]
  edge [
    source 11
    target 941
  ]
  edge [
    source 11
    target 942
  ]
  edge [
    source 11
    target 943
  ]
  edge [
    source 11
    target 944
  ]
  edge [
    source 11
    target 945
  ]
  edge [
    source 11
    target 946
  ]
  edge [
    source 11
    target 947
  ]
  edge [
    source 11
    target 948
  ]
  edge [
    source 11
    target 949
  ]
  edge [
    source 11
    target 950
  ]
  edge [
    source 11
    target 951
  ]
  edge [
    source 11
    target 952
  ]
  edge [
    source 11
    target 953
  ]
  edge [
    source 11
    target 954
  ]
  edge [
    source 11
    target 955
  ]
  edge [
    source 11
    target 956
  ]
  edge [
    source 11
    target 957
  ]
  edge [
    source 11
    target 958
  ]
  edge [
    source 11
    target 959
  ]
  edge [
    source 11
    target 960
  ]
  edge [
    source 11
    target 961
  ]
  edge [
    source 11
    target 962
  ]
  edge [
    source 11
    target 963
  ]
  edge [
    source 11
    target 964
  ]
  edge [
    source 11
    target 965
  ]
  edge [
    source 11
    target 966
  ]
  edge [
    source 11
    target 967
  ]
  edge [
    source 11
    target 968
  ]
  edge [
    source 11
    target 969
  ]
  edge [
    source 11
    target 970
  ]
  edge [
    source 11
    target 971
  ]
  edge [
    source 11
    target 972
  ]
  edge [
    source 11
    target 973
  ]
  edge [
    source 11
    target 974
  ]
  edge [
    source 11
    target 975
  ]
  edge [
    source 11
    target 976
  ]
  edge [
    source 11
    target 977
  ]
  edge [
    source 11
    target 978
  ]
  edge [
    source 11
    target 979
  ]
  edge [
    source 11
    target 980
  ]
  edge [
    source 11
    target 981
  ]
  edge [
    source 11
    target 982
  ]
  edge [
    source 11
    target 983
  ]
  edge [
    source 11
    target 984
  ]
  edge [
    source 11
    target 985
  ]
  edge [
    source 11
    target 986
  ]
  edge [
    source 11
    target 987
  ]
  edge [
    source 11
    target 988
  ]
  edge [
    source 11
    target 989
  ]
  edge [
    source 11
    target 990
  ]
  edge [
    source 11
    target 991
  ]
  edge [
    source 11
    target 992
  ]
  edge [
    source 11
    target 993
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 994
  ]
  edge [
    source 12
    target 995
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 996
  ]
  edge [
    source 12
    target 997
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 998
  ]
  edge [
    source 14
    target 999
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1000
  ]
  edge [
    source 15
    target 1001
  ]
  edge [
    source 15
    target 1002
  ]
  edge [
    source 15
    target 1003
  ]
  edge [
    source 15
    target 1004
  ]
  edge [
    source 15
    target 1005
  ]
  edge [
    source 15
    target 1006
  ]
  edge [
    source 15
    target 1007
  ]
  edge [
    source 15
    target 1008
  ]
  edge [
    source 15
    target 1009
  ]
  edge [
    source 15
    target 1010
  ]
  edge [
    source 15
    target 1011
  ]
  edge [
    source 15
    target 1012
  ]
  edge [
    source 15
    target 1013
  ]
  edge [
    source 15
    target 1014
  ]
  edge [
    source 15
    target 1015
  ]
  edge [
    source 15
    target 1016
  ]
  edge [
    source 15
    target 1017
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 1018
  ]
  edge [
    source 15
    target 1019
  ]
  edge [
    source 15
    target 555
  ]
  edge [
    source 15
    target 1020
  ]
  edge [
    source 15
    target 1021
  ]
  edge [
    source 15
    target 1022
  ]
  edge [
    source 15
    target 1023
  ]
  edge [
    source 15
    target 1024
  ]
  edge [
    source 15
    target 1025
  ]
  edge [
    source 15
    target 1026
  ]
  edge [
    source 15
    target 1027
  ]
  edge [
    source 15
    target 1028
  ]
  edge [
    source 15
    target 1029
  ]
  edge [
    source 15
    target 564
  ]
  edge [
    source 15
    target 1030
  ]
  edge [
    source 15
    target 1031
  ]
  edge [
    source 15
    target 1032
  ]
  edge [
    source 15
    target 1033
  ]
  edge [
    source 15
    target 1034
  ]
  edge [
    source 15
    target 1035
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 1036
  ]
  edge [
    source 15
    target 1037
  ]
  edge [
    source 15
    target 1038
  ]
  edge [
    source 15
    target 1039
  ]
  edge [
    source 15
    target 1040
  ]
  edge [
    source 15
    target 1041
  ]
  edge [
    source 15
    target 1042
  ]
  edge [
    source 15
    target 1043
  ]
  edge [
    source 15
    target 1044
  ]
  edge [
    source 15
    target 1045
  ]
  edge [
    source 15
    target 1046
  ]
  edge [
    source 16
    target 74
  ]
  edge [
    source 16
    target 1047
  ]
  edge [
    source 16
    target 1048
  ]
  edge [
    source 16
    target 1049
  ]
  edge [
    source 16
    target 1050
  ]
  edge [
    source 16
    target 1051
  ]
  edge [
    source 16
    target 90
  ]
  edge [
    source 16
    target 1052
  ]
  edge [
    source 16
    target 56
  ]
  edge [
    source 16
    target 1053
  ]
  edge [
    source 16
    target 1054
  ]
  edge [
    source 16
    target 1055
  ]
  edge [
    source 16
    target 1056
  ]
  edge [
    source 16
    target 1057
  ]
  edge [
    source 16
    target 1058
  ]
  edge [
    source 16
    target 1059
  ]
  edge [
    source 16
    target 1060
  ]
  edge [
    source 625
    target 1069
  ]
  edge [
    source 1062
    target 1063
  ]
  edge [
    source 1064
    target 1065
  ]
  edge [
    source 1065
    target 1066
  ]
  edge [
    source 1065
    target 1067
  ]
  edge [
    source 1065
    target 1068
  ]
  edge [
    source 1066
    target 1067
  ]
  edge [
    source 1066
    target 1068
  ]
  edge [
    source 1067
    target 1068
  ]
  edge [
    source 1070
    target 1071
  ]
  edge [
    source 1070
    target 1072
  ]
  edge [
    source 1071
    target 1072
  ]
  edge [
    source 1072
    target 1073
  ]
  edge [
    source 1072
    target 1074
  ]
  edge [
    source 1072
    target 1075
  ]
  edge [
    source 1072
    target 1076
  ]
  edge [
    source 1073
    target 1074
  ]
  edge [
    source 1073
    target 1075
  ]
  edge [
    source 1073
    target 1076
  ]
  edge [
    source 1074
    target 1075
  ]
  edge [
    source 1074
    target 1076
  ]
  edge [
    source 1075
    target 1076
  ]
  edge [
    source 1077
    target 1078
  ]
  edge [
    source 1078
    target 1079
  ]
]
