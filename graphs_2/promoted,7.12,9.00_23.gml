graph [
  node [
    id 0
    label "pijany"
    origin "text"
  ]
  node [
    id 1
    label "patostreamer"
    origin "text"
  ]
  node [
    id 2
    label "dawid"
    origin "text"
  ]
  node [
    id 3
    label "zn&#281;ca&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "nad"
    origin "text"
  ]
  node [
    id 6
    label "bezbronny"
    origin "text"
  ]
  node [
    id 7
    label "pies"
    origin "text"
  ]
  node [
    id 8
    label "chwali&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wszystek"
    origin "text"
  ]
  node [
    id 10
    label "swoje"
    origin "text"
  ]
  node [
    id 11
    label "owczarek"
    origin "text"
  ]
  node [
    id 12
    label "niemiecki"
    origin "text"
  ]
  node [
    id 13
    label "tresowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "kij"
    origin "text"
  ]
  node [
    id 15
    label "upijanie_si&#281;"
  ]
  node [
    id 16
    label "szalony"
  ]
  node [
    id 17
    label "nieprzytomny"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "d&#281;tka"
  ]
  node [
    id 20
    label "pij&#261;cy"
  ]
  node [
    id 21
    label "napi&#322;y"
  ]
  node [
    id 22
    label "upicie_si&#281;"
  ]
  node [
    id 23
    label "s&#322;abeusz"
  ]
  node [
    id 24
    label "kicha"
  ]
  node [
    id 25
    label "nieudany"
  ]
  node [
    id 26
    label "pi&#322;ka"
  ]
  node [
    id 27
    label "ogumienie"
  ]
  node [
    id 28
    label "wentyl"
  ]
  node [
    id 29
    label "zm&#281;czony"
  ]
  node [
    id 30
    label "mato&#322;"
  ]
  node [
    id 31
    label "baloney"
  ]
  node [
    id 32
    label "sytuacja"
  ]
  node [
    id 33
    label "&#322;amaga"
  ]
  node [
    id 34
    label "szybki"
  ]
  node [
    id 35
    label "podupcony"
  ]
  node [
    id 36
    label "nietuzinkowy"
  ]
  node [
    id 37
    label "niezr&#243;wnowa&#380;ony"
  ]
  node [
    id 38
    label "niepokoj&#261;cy"
  ]
  node [
    id 39
    label "stukni&#281;ty"
  ]
  node [
    id 40
    label "nieprzewidywalny"
  ]
  node [
    id 41
    label "nienormalny"
  ]
  node [
    id 42
    label "zwariowanie"
  ]
  node [
    id 43
    label "oryginalny"
  ]
  node [
    id 44
    label "jebni&#281;ty"
  ]
  node [
    id 45
    label "dziwny"
  ]
  node [
    id 46
    label "szale&#324;czo"
  ]
  node [
    id 47
    label "g&#261;bczasta_encefalopatia_byd&#322;a"
  ]
  node [
    id 48
    label "chory"
  ]
  node [
    id 49
    label "wielki"
  ]
  node [
    id 50
    label "szalenie"
  ]
  node [
    id 51
    label "nierozs&#261;dny"
  ]
  node [
    id 52
    label "konsument"
  ]
  node [
    id 53
    label "trunkowy"
  ]
  node [
    id 54
    label "ludzko&#347;&#263;"
  ]
  node [
    id 55
    label "asymilowanie"
  ]
  node [
    id 56
    label "wapniak"
  ]
  node [
    id 57
    label "asymilowa&#263;"
  ]
  node [
    id 58
    label "os&#322;abia&#263;"
  ]
  node [
    id 59
    label "posta&#263;"
  ]
  node [
    id 60
    label "hominid"
  ]
  node [
    id 61
    label "podw&#322;adny"
  ]
  node [
    id 62
    label "os&#322;abianie"
  ]
  node [
    id 63
    label "g&#322;owa"
  ]
  node [
    id 64
    label "figura"
  ]
  node [
    id 65
    label "portrecista"
  ]
  node [
    id 66
    label "dwun&#243;g"
  ]
  node [
    id 67
    label "profanum"
  ]
  node [
    id 68
    label "mikrokosmos"
  ]
  node [
    id 69
    label "nasada"
  ]
  node [
    id 70
    label "duch"
  ]
  node [
    id 71
    label "antropochoria"
  ]
  node [
    id 72
    label "osoba"
  ]
  node [
    id 73
    label "wz&#243;r"
  ]
  node [
    id 74
    label "senior"
  ]
  node [
    id 75
    label "oddzia&#322;ywanie"
  ]
  node [
    id 76
    label "Adam"
  ]
  node [
    id 77
    label "homo_sapiens"
  ]
  node [
    id 78
    label "polifag"
  ]
  node [
    id 79
    label "niesw&#243;j"
  ]
  node [
    id 80
    label "kosmiczny"
  ]
  node [
    id 81
    label "nieprzytomnie"
  ]
  node [
    id 82
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 83
    label "pull"
  ]
  node [
    id 84
    label "mani&#263;"
  ]
  node [
    id 85
    label "robi&#263;"
  ]
  node [
    id 86
    label "determine"
  ]
  node [
    id 87
    label "work"
  ]
  node [
    id 88
    label "powodowa&#263;"
  ]
  node [
    id 89
    label "reakcja_chemiczna"
  ]
  node [
    id 90
    label "poci&#261;ga&#263;"
  ]
  node [
    id 91
    label "interesowa&#263;"
  ]
  node [
    id 92
    label "wabi&#263;"
  ]
  node [
    id 93
    label "zwodzi&#263;"
  ]
  node [
    id 94
    label "niezdolny"
  ]
  node [
    id 95
    label "s&#322;aby"
  ]
  node [
    id 96
    label "bezradny"
  ]
  node [
    id 97
    label "bezbronnie"
  ]
  node [
    id 98
    label "bezradnie"
  ]
  node [
    id 99
    label "nieskuteczny"
  ]
  node [
    id 100
    label "nietrwa&#322;y"
  ]
  node [
    id 101
    label "mizerny"
  ]
  node [
    id 102
    label "marnie"
  ]
  node [
    id 103
    label "delikatny"
  ]
  node [
    id 104
    label "po&#347;ledni"
  ]
  node [
    id 105
    label "niezdrowy"
  ]
  node [
    id 106
    label "z&#322;y"
  ]
  node [
    id 107
    label "nieumiej&#281;tny"
  ]
  node [
    id 108
    label "s&#322;abo"
  ]
  node [
    id 109
    label "nieznaczny"
  ]
  node [
    id 110
    label "lura"
  ]
  node [
    id 111
    label "s&#322;abowity"
  ]
  node [
    id 112
    label "zawodny"
  ]
  node [
    id 113
    label "&#322;agodny"
  ]
  node [
    id 114
    label "md&#322;y"
  ]
  node [
    id 115
    label "niedoskona&#322;y"
  ]
  node [
    id 116
    label "przemijaj&#261;cy"
  ]
  node [
    id 117
    label "niemocny"
  ]
  node [
    id 118
    label "niefajny"
  ]
  node [
    id 119
    label "kiepsko"
  ]
  node [
    id 120
    label "biernie"
  ]
  node [
    id 121
    label "piese&#322;"
  ]
  node [
    id 122
    label "Cerber"
  ]
  node [
    id 123
    label "szczeka&#263;"
  ]
  node [
    id 124
    label "&#322;ajdak"
  ]
  node [
    id 125
    label "kabanos"
  ]
  node [
    id 126
    label "wyzwisko"
  ]
  node [
    id 127
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 128
    label "samiec"
  ]
  node [
    id 129
    label "spragniony"
  ]
  node [
    id 130
    label "policjant"
  ]
  node [
    id 131
    label "rakarz"
  ]
  node [
    id 132
    label "szczu&#263;"
  ]
  node [
    id 133
    label "wycie"
  ]
  node [
    id 134
    label "istota_&#380;ywa"
  ]
  node [
    id 135
    label "trufla"
  ]
  node [
    id 136
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 137
    label "zawy&#263;"
  ]
  node [
    id 138
    label "sobaka"
  ]
  node [
    id 139
    label "dogoterapia"
  ]
  node [
    id 140
    label "s&#322;u&#380;enie"
  ]
  node [
    id 141
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 142
    label "psowate"
  ]
  node [
    id 143
    label "wy&#263;"
  ]
  node [
    id 144
    label "szczucie"
  ]
  node [
    id 145
    label "czworon&#243;g"
  ]
  node [
    id 146
    label "sympatyk"
  ]
  node [
    id 147
    label "entuzjasta"
  ]
  node [
    id 148
    label "critter"
  ]
  node [
    id 149
    label "zwierz&#281;_domowe"
  ]
  node [
    id 150
    label "kr&#281;gowiec"
  ]
  node [
    id 151
    label "tetrapody"
  ]
  node [
    id 152
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 153
    label "zwierz&#281;"
  ]
  node [
    id 154
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 155
    label "palconogie"
  ]
  node [
    id 156
    label "stra&#380;nik"
  ]
  node [
    id 157
    label "wielog&#322;owy"
  ]
  node [
    id 158
    label "przek&#261;ska"
  ]
  node [
    id 159
    label "w&#281;dzi&#263;"
  ]
  node [
    id 160
    label "przysmak"
  ]
  node [
    id 161
    label "kie&#322;basa"
  ]
  node [
    id 162
    label "cygaro"
  ]
  node [
    id 163
    label "kot"
  ]
  node [
    id 164
    label "zooterapia"
  ]
  node [
    id 165
    label "&#380;o&#322;nierz"
  ]
  node [
    id 166
    label "by&#263;"
  ]
  node [
    id 167
    label "trwa&#263;"
  ]
  node [
    id 168
    label "use"
  ]
  node [
    id 169
    label "suffice"
  ]
  node [
    id 170
    label "cel"
  ]
  node [
    id 171
    label "pracowa&#263;"
  ]
  node [
    id 172
    label "match"
  ]
  node [
    id 173
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 174
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 175
    label "wait"
  ]
  node [
    id 176
    label "pomaga&#263;"
  ]
  node [
    id 177
    label "czekoladka"
  ]
  node [
    id 178
    label "afrodyzjak"
  ]
  node [
    id 179
    label "workowiec"
  ]
  node [
    id 180
    label "nos"
  ]
  node [
    id 181
    label "grzyb_owocnikowy"
  ]
  node [
    id 182
    label "truflowate"
  ]
  node [
    id 183
    label "grzyb_mikoryzowy"
  ]
  node [
    id 184
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 185
    label "powodowanie"
  ]
  node [
    id 186
    label "pod&#380;eganie"
  ]
  node [
    id 187
    label "atakowanie"
  ]
  node [
    id 188
    label "fomentation"
  ]
  node [
    id 189
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 190
    label "bark"
  ]
  node [
    id 191
    label "m&#243;wi&#263;"
  ]
  node [
    id 192
    label "hum"
  ]
  node [
    id 193
    label "obgadywa&#263;"
  ]
  node [
    id 194
    label "kozio&#322;"
  ]
  node [
    id 195
    label "szcz&#281;ka&#263;"
  ]
  node [
    id 196
    label "karabin"
  ]
  node [
    id 197
    label "wymy&#347;la&#263;"
  ]
  node [
    id 198
    label "wilk"
  ]
  node [
    id 199
    label "ha&#322;asowa&#263;"
  ]
  node [
    id 200
    label "fa&#322;szowa&#263;"
  ]
  node [
    id 201
    label "p&#322;aka&#263;"
  ]
  node [
    id 202
    label "snivel"
  ]
  node [
    id 203
    label "yip"
  ]
  node [
    id 204
    label "pracownik_komunalny"
  ]
  node [
    id 205
    label "uczynny"
  ]
  node [
    id 206
    label "s&#322;ugiwanie"
  ]
  node [
    id 207
    label "pomaganie"
  ]
  node [
    id 208
    label "bycie"
  ]
  node [
    id 209
    label "wys&#322;u&#380;enie_si&#281;"
  ]
  node [
    id 210
    label "request"
  ]
  node [
    id 211
    label "trwanie"
  ]
  node [
    id 212
    label "robienie"
  ]
  node [
    id 213
    label "service"
  ]
  node [
    id 214
    label "przydawanie_si&#281;"
  ]
  node [
    id 215
    label "czynno&#347;&#263;"
  ]
  node [
    id 216
    label "pracowanie"
  ]
  node [
    id 217
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 218
    label "wydoby&#263;"
  ]
  node [
    id 219
    label "rant"
  ]
  node [
    id 220
    label "rave"
  ]
  node [
    id 221
    label "zabrzmie&#263;"
  ]
  node [
    id 222
    label "tease"
  ]
  node [
    id 223
    label "pod&#380;ega&#263;"
  ]
  node [
    id 224
    label "podjudza&#263;"
  ]
  node [
    id 225
    label "wo&#322;anie"
  ]
  node [
    id 226
    label "wydobywanie"
  ]
  node [
    id 227
    label "brzmienie"
  ]
  node [
    id 228
    label "wydawanie"
  ]
  node [
    id 229
    label "d&#378;wi&#281;k"
  ]
  node [
    id 230
    label "whimper"
  ]
  node [
    id 231
    label "cholera"
  ]
  node [
    id 232
    label "wypowied&#378;"
  ]
  node [
    id 233
    label "chuj"
  ]
  node [
    id 234
    label "bluzg"
  ]
  node [
    id 235
    label "chujowy"
  ]
  node [
    id 236
    label "obelga"
  ]
  node [
    id 237
    label "szmata"
  ]
  node [
    id 238
    label "ch&#281;tny"
  ]
  node [
    id 239
    label "z&#322;akniony"
  ]
  node [
    id 240
    label "upodlenie_si&#281;"
  ]
  node [
    id 241
    label "skurwysyn"
  ]
  node [
    id 242
    label "upadlanie_si&#281;"
  ]
  node [
    id 243
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 244
    label "psubrat"
  ]
  node [
    id 245
    label "policja"
  ]
  node [
    id 246
    label "blacharz"
  ]
  node [
    id 247
    label "str&#243;&#380;"
  ]
  node [
    id 248
    label "pa&#322;a"
  ]
  node [
    id 249
    label "mundurowy"
  ]
  node [
    id 250
    label "glina"
  ]
  node [
    id 251
    label "bless"
  ]
  node [
    id 252
    label "wywy&#380;sza&#263;"
  ]
  node [
    id 253
    label "glorify"
  ]
  node [
    id 254
    label "nagradza&#263;"
  ]
  node [
    id 255
    label "wielmo&#380;y&#263;"
  ]
  node [
    id 256
    label "wys&#322;awia&#263;"
  ]
  node [
    id 257
    label "patrze&#263;_jak_w_&#347;wi&#281;ty_obraz"
  ]
  node [
    id 258
    label "pia&#263;"
  ]
  node [
    id 259
    label "express"
  ]
  node [
    id 260
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 261
    label "os&#322;awia&#263;"
  ]
  node [
    id 262
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 263
    label "raise"
  ]
  node [
    id 264
    label "render"
  ]
  node [
    id 265
    label "reinforce"
  ]
  node [
    id 266
    label "nadgradza&#263;"
  ]
  node [
    id 267
    label "dawa&#263;"
  ]
  node [
    id 268
    label "wielbi&#263;"
  ]
  node [
    id 269
    label "wzmaga&#263;"
  ]
  node [
    id 270
    label "ca&#322;y"
  ]
  node [
    id 271
    label "jedyny"
  ]
  node [
    id 272
    label "du&#380;y"
  ]
  node [
    id 273
    label "zdr&#243;w"
  ]
  node [
    id 274
    label "calu&#347;ko"
  ]
  node [
    id 275
    label "kompletny"
  ]
  node [
    id 276
    label "&#380;ywy"
  ]
  node [
    id 277
    label "pe&#322;ny"
  ]
  node [
    id 278
    label "podobny"
  ]
  node [
    id 279
    label "ca&#322;o"
  ]
  node [
    id 280
    label "pies_pasterski"
  ]
  node [
    id 281
    label "owczarz"
  ]
  node [
    id 282
    label "pastuszek"
  ]
  node [
    id 283
    label "ch&#322;opiec"
  ]
  node [
    id 284
    label "pasterz"
  ]
  node [
    id 285
    label "czaban"
  ]
  node [
    id 286
    label "szatra"
  ]
  node [
    id 287
    label "po_niemiecku"
  ]
  node [
    id 288
    label "German"
  ]
  node [
    id 289
    label "niemiecko"
  ]
  node [
    id 290
    label "cenar"
  ]
  node [
    id 291
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 292
    label "europejski"
  ]
  node [
    id 293
    label "strudel"
  ]
  node [
    id 294
    label "niemiec"
  ]
  node [
    id 295
    label "pionier"
  ]
  node [
    id 296
    label "zachodnioeuropejski"
  ]
  node [
    id 297
    label "j&#281;zyk"
  ]
  node [
    id 298
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 299
    label "junkers"
  ]
  node [
    id 300
    label "szwabski"
  ]
  node [
    id 301
    label "szwabsko"
  ]
  node [
    id 302
    label "j&#281;zyk_niemiecki"
  ]
  node [
    id 303
    label "po_szwabsku"
  ]
  node [
    id 304
    label "platt"
  ]
  node [
    id 305
    label "europejsko"
  ]
  node [
    id 306
    label "ciasto"
  ]
  node [
    id 307
    label "saper"
  ]
  node [
    id 308
    label "prekursor"
  ]
  node [
    id 309
    label "osadnik"
  ]
  node [
    id 310
    label "skaut"
  ]
  node [
    id 311
    label "g&#322;osiciel"
  ]
  node [
    id 312
    label "taniec_ludowy"
  ]
  node [
    id 313
    label "melodia"
  ]
  node [
    id 314
    label "taniec"
  ]
  node [
    id 315
    label "podgrzewacz"
  ]
  node [
    id 316
    label "samolot_wojskowy"
  ]
  node [
    id 317
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 318
    label "langosz"
  ]
  node [
    id 319
    label "moreska"
  ]
  node [
    id 320
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 321
    label "zachodni"
  ]
  node [
    id 322
    label "po_europejsku"
  ]
  node [
    id 323
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 324
    label "European"
  ]
  node [
    id 325
    label "typowy"
  ]
  node [
    id 326
    label "charakterystyczny"
  ]
  node [
    id 327
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 328
    label "artykulator"
  ]
  node [
    id 329
    label "kod"
  ]
  node [
    id 330
    label "kawa&#322;ek"
  ]
  node [
    id 331
    label "przedmiot"
  ]
  node [
    id 332
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 333
    label "gramatyka"
  ]
  node [
    id 334
    label "stylik"
  ]
  node [
    id 335
    label "przet&#322;umaczenie"
  ]
  node [
    id 336
    label "formalizowanie"
  ]
  node [
    id 337
    label "ssa&#263;"
  ]
  node [
    id 338
    label "ssanie"
  ]
  node [
    id 339
    label "language"
  ]
  node [
    id 340
    label "liza&#263;"
  ]
  node [
    id 341
    label "napisa&#263;"
  ]
  node [
    id 342
    label "konsonantyzm"
  ]
  node [
    id 343
    label "wokalizm"
  ]
  node [
    id 344
    label "pisa&#263;"
  ]
  node [
    id 345
    label "fonetyka"
  ]
  node [
    id 346
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 347
    label "jeniec"
  ]
  node [
    id 348
    label "but"
  ]
  node [
    id 349
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 350
    label "po_koroniarsku"
  ]
  node [
    id 351
    label "kultura_duchowa"
  ]
  node [
    id 352
    label "t&#322;umaczenie"
  ]
  node [
    id 353
    label "m&#243;wienie"
  ]
  node [
    id 354
    label "pype&#263;"
  ]
  node [
    id 355
    label "lizanie"
  ]
  node [
    id 356
    label "pismo"
  ]
  node [
    id 357
    label "formalizowa&#263;"
  ]
  node [
    id 358
    label "rozumie&#263;"
  ]
  node [
    id 359
    label "organ"
  ]
  node [
    id 360
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 361
    label "rozumienie"
  ]
  node [
    id 362
    label "spos&#243;b"
  ]
  node [
    id 363
    label "makroglosja"
  ]
  node [
    id 364
    label "jama_ustna"
  ]
  node [
    id 365
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 366
    label "formacja_geologiczna"
  ]
  node [
    id 367
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 368
    label "natural_language"
  ]
  node [
    id 369
    label "s&#322;ownictwo"
  ]
  node [
    id 370
    label "urz&#261;dzenie"
  ]
  node [
    id 371
    label "educate"
  ]
  node [
    id 372
    label "uczy&#263;"
  ]
  node [
    id 373
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 374
    label "doskonali&#263;"
  ]
  node [
    id 375
    label "rozwija&#263;"
  ]
  node [
    id 376
    label "train"
  ]
  node [
    id 377
    label "szkoli&#263;"
  ]
  node [
    id 378
    label "zapoznawa&#263;"
  ]
  node [
    id 379
    label "teach"
  ]
  node [
    id 380
    label "dostosowywa&#263;"
  ]
  node [
    id 381
    label "subordinate"
  ]
  node [
    id 382
    label "hyponym"
  ]
  node [
    id 383
    label "prowadzi&#263;_na_pasku"
  ]
  node [
    id 384
    label "dyrygowa&#263;"
  ]
  node [
    id 385
    label "uzale&#380;nia&#263;"
  ]
  node [
    id 386
    label "hone"
  ]
  node [
    id 387
    label "ulepsza&#263;"
  ]
  node [
    id 388
    label "degenerat"
  ]
  node [
    id 389
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 390
    label "zwyrol"
  ]
  node [
    id 391
    label "czerniak"
  ]
  node [
    id 392
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 393
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 394
    label "paszcza"
  ]
  node [
    id 395
    label "popapraniec"
  ]
  node [
    id 396
    label "skuba&#263;"
  ]
  node [
    id 397
    label "skubanie"
  ]
  node [
    id 398
    label "agresja"
  ]
  node [
    id 399
    label "skubni&#281;cie"
  ]
  node [
    id 400
    label "zwierz&#281;ta"
  ]
  node [
    id 401
    label "fukni&#281;cie"
  ]
  node [
    id 402
    label "farba"
  ]
  node [
    id 403
    label "fukanie"
  ]
  node [
    id 404
    label "gad"
  ]
  node [
    id 405
    label "siedzie&#263;"
  ]
  node [
    id 406
    label "oswaja&#263;"
  ]
  node [
    id 407
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 408
    label "poligamia"
  ]
  node [
    id 409
    label "oz&#243;r"
  ]
  node [
    id 410
    label "skubn&#261;&#263;"
  ]
  node [
    id 411
    label "wios&#322;owa&#263;"
  ]
  node [
    id 412
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 413
    label "le&#380;enie"
  ]
  node [
    id 414
    label "niecz&#322;owiek"
  ]
  node [
    id 415
    label "wios&#322;owanie"
  ]
  node [
    id 416
    label "napasienie_si&#281;"
  ]
  node [
    id 417
    label "wiwarium"
  ]
  node [
    id 418
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 419
    label "animalista"
  ]
  node [
    id 420
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 421
    label "budowa"
  ]
  node [
    id 422
    label "hodowla"
  ]
  node [
    id 423
    label "pasienie_si&#281;"
  ]
  node [
    id 424
    label "sodomita"
  ]
  node [
    id 425
    label "monogamia"
  ]
  node [
    id 426
    label "przyssawka"
  ]
  node [
    id 427
    label "zachowanie"
  ]
  node [
    id 428
    label "budowa_cia&#322;a"
  ]
  node [
    id 429
    label "okrutnik"
  ]
  node [
    id 430
    label "grzbiet"
  ]
  node [
    id 431
    label "weterynarz"
  ]
  node [
    id 432
    label "&#322;eb"
  ]
  node [
    id 433
    label "wylinka"
  ]
  node [
    id 434
    label "bestia"
  ]
  node [
    id 435
    label "poskramia&#263;"
  ]
  node [
    id 436
    label "fauna"
  ]
  node [
    id 437
    label "treser"
  ]
  node [
    id 438
    label "siedzenie"
  ]
  node [
    id 439
    label "le&#380;e&#263;"
  ]
  node [
    id 440
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 441
    label "obiekt_naturalny"
  ]
  node [
    id 442
    label "nalewak"
  ]
  node [
    id 443
    label "patyk"
  ]
  node [
    id 444
    label "cake"
  ]
  node [
    id 445
    label "piwo"
  ]
  node [
    id 446
    label "zboczenie"
  ]
  node [
    id 447
    label "om&#243;wienie"
  ]
  node [
    id 448
    label "sponiewieranie"
  ]
  node [
    id 449
    label "discipline"
  ]
  node [
    id 450
    label "rzecz"
  ]
  node [
    id 451
    label "omawia&#263;"
  ]
  node [
    id 452
    label "kr&#261;&#380;enie"
  ]
  node [
    id 453
    label "tre&#347;&#263;"
  ]
  node [
    id 454
    label "sponiewiera&#263;"
  ]
  node [
    id 455
    label "element"
  ]
  node [
    id 456
    label "entity"
  ]
  node [
    id 457
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 458
    label "tematyka"
  ]
  node [
    id 459
    label "w&#261;tek"
  ]
  node [
    id 460
    label "charakter"
  ]
  node [
    id 461
    label "zbaczanie"
  ]
  node [
    id 462
    label "program_nauczania"
  ]
  node [
    id 463
    label "om&#243;wi&#263;"
  ]
  node [
    id 464
    label "omawianie"
  ]
  node [
    id 465
    label "thing"
  ]
  node [
    id 466
    label "kultura"
  ]
  node [
    id 467
    label "istota"
  ]
  node [
    id 468
    label "zbacza&#263;"
  ]
  node [
    id 469
    label "zboczy&#263;"
  ]
  node [
    id 470
    label "dozownik"
  ]
  node [
    id 471
    label "warzenie"
  ]
  node [
    id 472
    label "nawarzy&#263;"
  ]
  node [
    id 473
    label "alkohol"
  ]
  node [
    id 474
    label "nap&#243;j"
  ]
  node [
    id 475
    label "bacik"
  ]
  node [
    id 476
    label "wyj&#347;cie"
  ]
  node [
    id 477
    label "uwarzy&#263;"
  ]
  node [
    id 478
    label "birofilia"
  ]
  node [
    id 479
    label "warzy&#263;"
  ]
  node [
    id 480
    label "uwarzenie"
  ]
  node [
    id 481
    label "browarnia"
  ]
  node [
    id 482
    label "nawarzenie"
  ]
  node [
    id 483
    label "anta&#322;"
  ]
  node [
    id 484
    label "pr&#281;t"
  ]
  node [
    id 485
    label "chudzielec"
  ]
  node [
    id 486
    label "tysi&#261;c"
  ]
  node [
    id 487
    label "rod"
  ]
  node [
    id 488
    label "Dawid"
  ]
  node [
    id 489
    label "R"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 285
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 287
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 290
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 305
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 12
    target 309
  ]
  edge [
    source 12
    target 310
  ]
  edge [
    source 12
    target 311
  ]
  edge [
    source 12
    target 312
  ]
  edge [
    source 12
    target 313
  ]
  edge [
    source 12
    target 314
  ]
  edge [
    source 12
    target 315
  ]
  edge [
    source 12
    target 316
  ]
  edge [
    source 12
    target 317
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 319
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 321
  ]
  edge [
    source 12
    target 322
  ]
  edge [
    source 12
    target 323
  ]
  edge [
    source 12
    target 324
  ]
  edge [
    source 12
    target 325
  ]
  edge [
    source 12
    target 326
  ]
  edge [
    source 12
    target 327
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 12
    target 331
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 335
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 337
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 364
  ]
  edge [
    source 12
    target 365
  ]
  edge [
    source 12
    target 366
  ]
  edge [
    source 12
    target 367
  ]
  edge [
    source 12
    target 368
  ]
  edge [
    source 12
    target 369
  ]
  edge [
    source 12
    target 370
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 406
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 433
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 14
    target 331
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 212
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 456
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 458
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 467
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 469
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 370
  ]
  edge [
    source 14
    target 471
  ]
  edge [
    source 14
    target 472
  ]
  edge [
    source 14
    target 473
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 488
    target 489
  ]
]
