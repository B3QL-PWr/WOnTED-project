graph [
  node [
    id 0
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kilka"
    origin "text"
  ]
  node [
    id 2
    label "dni"
    origin "text"
  ]
  node [
    id 3
    label "temu"
    origin "text"
  ]
  node [
    id 4
    label "nowa"
    origin "text"
  ]
  node [
    id 5
    label "wersja"
    origin "text"
  ]
  node [
    id 6
    label "firefoksa"
    origin "text"
  ]
  node [
    id 7
    label "skupi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "kwestia"
    origin "text"
  ]
  node [
    id 10
    label "otwarto&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "funkcjonalno&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "atrakcja"
    origin "text"
  ]
  node [
    id 13
    label "przemawia&#263;"
    origin "text"
  ]
  node [
    id 14
    label "korzysta&#263;"
    origin "text"
  ]
  node [
    id 15
    label "ten"
    origin "text"
  ]
  node [
    id 16
    label "przegl&#261;darka"
    origin "text"
  ]
  node [
    id 17
    label "zmieni&#263;by&#263;"
    origin "text"
  ]
  node [
    id 18
    label "zdanie"
    origin "text"
  ]
  node [
    id 19
    label "zas&#322;ugiwa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "uwaga"
    origin "text"
  ]
  node [
    id 21
    label "formu&#322;owa&#263;"
  ]
  node [
    id 22
    label "ozdabia&#263;"
  ]
  node [
    id 23
    label "stawia&#263;"
  ]
  node [
    id 24
    label "spell"
  ]
  node [
    id 25
    label "styl"
  ]
  node [
    id 26
    label "skryba"
  ]
  node [
    id 27
    label "read"
  ]
  node [
    id 28
    label "donosi&#263;"
  ]
  node [
    id 29
    label "code"
  ]
  node [
    id 30
    label "tekst"
  ]
  node [
    id 31
    label "dysgrafia"
  ]
  node [
    id 32
    label "dysortografia"
  ]
  node [
    id 33
    label "tworzy&#263;"
  ]
  node [
    id 34
    label "prasa"
  ]
  node [
    id 35
    label "robi&#263;"
  ]
  node [
    id 36
    label "pope&#322;nia&#263;"
  ]
  node [
    id 37
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 38
    label "wytwarza&#263;"
  ]
  node [
    id 39
    label "get"
  ]
  node [
    id 40
    label "consist"
  ]
  node [
    id 41
    label "stanowi&#263;"
  ]
  node [
    id 42
    label "raise"
  ]
  node [
    id 43
    label "spill_the_beans"
  ]
  node [
    id 44
    label "przeby&#263;"
  ]
  node [
    id 45
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 46
    label "zanosi&#263;"
  ]
  node [
    id 47
    label "inform"
  ]
  node [
    id 48
    label "give"
  ]
  node [
    id 49
    label "zu&#380;y&#263;"
  ]
  node [
    id 50
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 51
    label "introduce"
  ]
  node [
    id 52
    label "render"
  ]
  node [
    id 53
    label "ci&#261;&#380;a"
  ]
  node [
    id 54
    label "informowa&#263;"
  ]
  node [
    id 55
    label "komunikowa&#263;"
  ]
  node [
    id 56
    label "convey"
  ]
  node [
    id 57
    label "pozostawia&#263;"
  ]
  node [
    id 58
    label "czyni&#263;"
  ]
  node [
    id 59
    label "wydawa&#263;"
  ]
  node [
    id 60
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 61
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 62
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 63
    label "przewidywa&#263;"
  ]
  node [
    id 64
    label "przyznawa&#263;"
  ]
  node [
    id 65
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 66
    label "go"
  ]
  node [
    id 67
    label "obstawia&#263;"
  ]
  node [
    id 68
    label "umieszcza&#263;"
  ]
  node [
    id 69
    label "ocenia&#263;"
  ]
  node [
    id 70
    label "zastawia&#263;"
  ]
  node [
    id 71
    label "stanowisko"
  ]
  node [
    id 72
    label "znak"
  ]
  node [
    id 73
    label "wskazywa&#263;"
  ]
  node [
    id 74
    label "uruchamia&#263;"
  ]
  node [
    id 75
    label "fundowa&#263;"
  ]
  node [
    id 76
    label "zmienia&#263;"
  ]
  node [
    id 77
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 78
    label "deliver"
  ]
  node [
    id 79
    label "powodowa&#263;"
  ]
  node [
    id 80
    label "wyznacza&#263;"
  ]
  node [
    id 81
    label "przedstawia&#263;"
  ]
  node [
    id 82
    label "wydobywa&#263;"
  ]
  node [
    id 83
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 84
    label "trim"
  ]
  node [
    id 85
    label "gryzipi&#243;rek"
  ]
  node [
    id 86
    label "cz&#322;owiek"
  ]
  node [
    id 87
    label "pisarz"
  ]
  node [
    id 88
    label "ekscerpcja"
  ]
  node [
    id 89
    label "j&#281;zykowo"
  ]
  node [
    id 90
    label "wypowied&#378;"
  ]
  node [
    id 91
    label "redakcja"
  ]
  node [
    id 92
    label "wytw&#243;r"
  ]
  node [
    id 93
    label "pomini&#281;cie"
  ]
  node [
    id 94
    label "dzie&#322;o"
  ]
  node [
    id 95
    label "preparacja"
  ]
  node [
    id 96
    label "odmianka"
  ]
  node [
    id 97
    label "opu&#347;ci&#263;"
  ]
  node [
    id 98
    label "koniektura"
  ]
  node [
    id 99
    label "obelga"
  ]
  node [
    id 100
    label "zesp&#243;&#322;"
  ]
  node [
    id 101
    label "t&#322;oczysko"
  ]
  node [
    id 102
    label "depesza"
  ]
  node [
    id 103
    label "maszyna"
  ]
  node [
    id 104
    label "media"
  ]
  node [
    id 105
    label "napisa&#263;"
  ]
  node [
    id 106
    label "czasopismo"
  ]
  node [
    id 107
    label "dziennikarz_prasowy"
  ]
  node [
    id 108
    label "kiosk"
  ]
  node [
    id 109
    label "maszyna_rolnicza"
  ]
  node [
    id 110
    label "gazeta"
  ]
  node [
    id 111
    label "trzonek"
  ]
  node [
    id 112
    label "reakcja"
  ]
  node [
    id 113
    label "narz&#281;dzie"
  ]
  node [
    id 114
    label "spos&#243;b"
  ]
  node [
    id 115
    label "zbi&#243;r"
  ]
  node [
    id 116
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 117
    label "zachowanie"
  ]
  node [
    id 118
    label "stylik"
  ]
  node [
    id 119
    label "dyscyplina_sportowa"
  ]
  node [
    id 120
    label "handle"
  ]
  node [
    id 121
    label "stroke"
  ]
  node [
    id 122
    label "line"
  ]
  node [
    id 123
    label "charakter"
  ]
  node [
    id 124
    label "natural_language"
  ]
  node [
    id 125
    label "kanon"
  ]
  node [
    id 126
    label "behawior"
  ]
  node [
    id 127
    label "dysleksja"
  ]
  node [
    id 128
    label "pisanie"
  ]
  node [
    id 129
    label "dysgraphia"
  ]
  node [
    id 130
    label "ryba"
  ]
  node [
    id 131
    label "&#347;ledziowate"
  ]
  node [
    id 132
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 133
    label "kr&#281;gowiec"
  ]
  node [
    id 134
    label "systemik"
  ]
  node [
    id 135
    label "doniczkowiec"
  ]
  node [
    id 136
    label "mi&#281;so"
  ]
  node [
    id 137
    label "system"
  ]
  node [
    id 138
    label "patroszy&#263;"
  ]
  node [
    id 139
    label "rakowato&#347;&#263;"
  ]
  node [
    id 140
    label "w&#281;dkarstwo"
  ]
  node [
    id 141
    label "ryby"
  ]
  node [
    id 142
    label "fish"
  ]
  node [
    id 143
    label "linia_boczna"
  ]
  node [
    id 144
    label "tar&#322;o"
  ]
  node [
    id 145
    label "wyrostek_filtracyjny"
  ]
  node [
    id 146
    label "m&#281;tnooki"
  ]
  node [
    id 147
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 148
    label "pokrywa_skrzelowa"
  ]
  node [
    id 149
    label "ikra"
  ]
  node [
    id 150
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 151
    label "szczelina_skrzelowa"
  ]
  node [
    id 152
    label "&#347;ledziokszta&#322;tne"
  ]
  node [
    id 153
    label "czas"
  ]
  node [
    id 154
    label "poprzedzanie"
  ]
  node [
    id 155
    label "czasoprzestrze&#324;"
  ]
  node [
    id 156
    label "laba"
  ]
  node [
    id 157
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 158
    label "chronometria"
  ]
  node [
    id 159
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 160
    label "rachuba_czasu"
  ]
  node [
    id 161
    label "przep&#322;ywanie"
  ]
  node [
    id 162
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 163
    label "czasokres"
  ]
  node [
    id 164
    label "odczyt"
  ]
  node [
    id 165
    label "chwila"
  ]
  node [
    id 166
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 167
    label "dzieje"
  ]
  node [
    id 168
    label "kategoria_gramatyczna"
  ]
  node [
    id 169
    label "poprzedzenie"
  ]
  node [
    id 170
    label "trawienie"
  ]
  node [
    id 171
    label "pochodzi&#263;"
  ]
  node [
    id 172
    label "period"
  ]
  node [
    id 173
    label "okres_czasu"
  ]
  node [
    id 174
    label "poprzedza&#263;"
  ]
  node [
    id 175
    label "schy&#322;ek"
  ]
  node [
    id 176
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 177
    label "odwlekanie_si&#281;"
  ]
  node [
    id 178
    label "zegar"
  ]
  node [
    id 179
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 180
    label "czwarty_wymiar"
  ]
  node [
    id 181
    label "pochodzenie"
  ]
  node [
    id 182
    label "koniugacja"
  ]
  node [
    id 183
    label "Zeitgeist"
  ]
  node [
    id 184
    label "trawi&#263;"
  ]
  node [
    id 185
    label "pogoda"
  ]
  node [
    id 186
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 187
    label "poprzedzi&#263;"
  ]
  node [
    id 188
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 189
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 190
    label "time_period"
  ]
  node [
    id 191
    label "gwiazda"
  ]
  node [
    id 192
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 193
    label "Arktur"
  ]
  node [
    id 194
    label "kszta&#322;t"
  ]
  node [
    id 195
    label "Gwiazda_Polarna"
  ]
  node [
    id 196
    label "agregatka"
  ]
  node [
    id 197
    label "gromada"
  ]
  node [
    id 198
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 199
    label "S&#322;o&#324;ce"
  ]
  node [
    id 200
    label "Nibiru"
  ]
  node [
    id 201
    label "konstelacja"
  ]
  node [
    id 202
    label "ornament"
  ]
  node [
    id 203
    label "delta_Scuti"
  ]
  node [
    id 204
    label "&#347;wiat&#322;o"
  ]
  node [
    id 205
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 206
    label "obiekt"
  ]
  node [
    id 207
    label "s&#322;awa"
  ]
  node [
    id 208
    label "promie&#324;"
  ]
  node [
    id 209
    label "star"
  ]
  node [
    id 210
    label "gwiazdosz"
  ]
  node [
    id 211
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 212
    label "asocjacja_gwiazd"
  ]
  node [
    id 213
    label "supergrupa"
  ]
  node [
    id 214
    label "posta&#263;"
  ]
  node [
    id 215
    label "typ"
  ]
  node [
    id 216
    label "charakterystyka"
  ]
  node [
    id 217
    label "zaistnie&#263;"
  ]
  node [
    id 218
    label "cecha"
  ]
  node [
    id 219
    label "Osjan"
  ]
  node [
    id 220
    label "kto&#347;"
  ]
  node [
    id 221
    label "wygl&#261;d"
  ]
  node [
    id 222
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 223
    label "osobowo&#347;&#263;"
  ]
  node [
    id 224
    label "poby&#263;"
  ]
  node [
    id 225
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 226
    label "Aspazja"
  ]
  node [
    id 227
    label "punkt_widzenia"
  ]
  node [
    id 228
    label "kompleksja"
  ]
  node [
    id 229
    label "wytrzyma&#263;"
  ]
  node [
    id 230
    label "budowa"
  ]
  node [
    id 231
    label "formacja"
  ]
  node [
    id 232
    label "pozosta&#263;"
  ]
  node [
    id 233
    label "point"
  ]
  node [
    id 234
    label "przedstawienie"
  ]
  node [
    id 235
    label "go&#347;&#263;"
  ]
  node [
    id 236
    label "facet"
  ]
  node [
    id 237
    label "jednostka_systematyczna"
  ]
  node [
    id 238
    label "kr&#243;lestwo"
  ]
  node [
    id 239
    label "autorament"
  ]
  node [
    id 240
    label "variety"
  ]
  node [
    id 241
    label "antycypacja"
  ]
  node [
    id 242
    label "przypuszczenie"
  ]
  node [
    id 243
    label "cynk"
  ]
  node [
    id 244
    label "sztuka"
  ]
  node [
    id 245
    label "rezultat"
  ]
  node [
    id 246
    label "design"
  ]
  node [
    id 247
    label "sprawa"
  ]
  node [
    id 248
    label "dialog"
  ]
  node [
    id 249
    label "problemat"
  ]
  node [
    id 250
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 251
    label "problematyka"
  ]
  node [
    id 252
    label "subject"
  ]
  node [
    id 253
    label "kognicja"
  ]
  node [
    id 254
    label "object"
  ]
  node [
    id 255
    label "rozprawa"
  ]
  node [
    id 256
    label "temat"
  ]
  node [
    id 257
    label "wydarzenie"
  ]
  node [
    id 258
    label "szczeg&#243;&#322;"
  ]
  node [
    id 259
    label "proposition"
  ]
  node [
    id 260
    label "przes&#322;anka"
  ]
  node [
    id 261
    label "rzecz"
  ]
  node [
    id 262
    label "idea"
  ]
  node [
    id 263
    label "pos&#322;uchanie"
  ]
  node [
    id 264
    label "s&#261;d"
  ]
  node [
    id 265
    label "sparafrazowanie"
  ]
  node [
    id 266
    label "strawestowa&#263;"
  ]
  node [
    id 267
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 268
    label "trawestowa&#263;"
  ]
  node [
    id 269
    label "sparafrazowa&#263;"
  ]
  node [
    id 270
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 271
    label "sformu&#322;owanie"
  ]
  node [
    id 272
    label "parafrazowanie"
  ]
  node [
    id 273
    label "ozdobnik"
  ]
  node [
    id 274
    label "delimitacja"
  ]
  node [
    id 275
    label "parafrazowa&#263;"
  ]
  node [
    id 276
    label "stylizacja"
  ]
  node [
    id 277
    label "komunikat"
  ]
  node [
    id 278
    label "trawestowanie"
  ]
  node [
    id 279
    label "strawestowanie"
  ]
  node [
    id 280
    label "problem"
  ]
  node [
    id 281
    label "rozmowa"
  ]
  node [
    id 282
    label "cisza"
  ]
  node [
    id 283
    label "odpowied&#378;"
  ]
  node [
    id 284
    label "utw&#243;r"
  ]
  node [
    id 285
    label "rozhowor"
  ]
  node [
    id 286
    label "discussion"
  ]
  node [
    id 287
    label "czynno&#347;&#263;"
  ]
  node [
    id 288
    label "porozumienie"
  ]
  node [
    id 289
    label "rola"
  ]
  node [
    id 290
    label "kontaktowo&#347;&#263;"
  ]
  node [
    id 291
    label "rozmiar"
  ]
  node [
    id 292
    label "nieograniczono&#347;&#263;"
  ]
  node [
    id 293
    label "prostoduszno&#347;&#263;"
  ]
  node [
    id 294
    label "jawno&#347;&#263;"
  ]
  node [
    id 295
    label "frankness"
  ]
  node [
    id 296
    label "gotowo&#347;&#263;"
  ]
  node [
    id 297
    label "stan"
  ]
  node [
    id 298
    label "nastawienie"
  ]
  node [
    id 299
    label "readiness"
  ]
  node [
    id 300
    label "m&#322;ot"
  ]
  node [
    id 301
    label "drzewo"
  ]
  node [
    id 302
    label "pr&#243;ba"
  ]
  node [
    id 303
    label "attribute"
  ]
  node [
    id 304
    label "marka"
  ]
  node [
    id 305
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 306
    label "warunek_lokalowy"
  ]
  node [
    id 307
    label "liczba"
  ]
  node [
    id 308
    label "circumference"
  ]
  node [
    id 309
    label "odzie&#380;"
  ]
  node [
    id 310
    label "ilo&#347;&#263;"
  ]
  node [
    id 311
    label "znaczenie"
  ]
  node [
    id 312
    label "dymensja"
  ]
  node [
    id 313
    label "brak"
  ]
  node [
    id 314
    label "u&#380;ytkowo&#347;&#263;"
  ]
  node [
    id 315
    label "praktyczno&#347;&#263;"
  ]
  node [
    id 316
    label "functionality"
  ]
  node [
    id 317
    label "m&#261;dro&#347;&#263;"
  ]
  node [
    id 318
    label "practicality"
  ]
  node [
    id 319
    label "rzeczowo&#347;&#263;"
  ]
  node [
    id 320
    label "zmy&#347;lno&#347;&#263;"
  ]
  node [
    id 321
    label "u&#380;yteczno&#347;&#263;"
  ]
  node [
    id 322
    label "urozmaicenie"
  ]
  node [
    id 323
    label "punkt"
  ]
  node [
    id 324
    label "urok"
  ]
  node [
    id 325
    label "ciekawostka"
  ]
  node [
    id 326
    label "sensacja"
  ]
  node [
    id 327
    label "informacja"
  ]
  node [
    id 328
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 329
    label "co&#347;"
  ]
  node [
    id 330
    label "podzielenie"
  ]
  node [
    id 331
    label "nadanie"
  ]
  node [
    id 332
    label "differentiation"
  ]
  node [
    id 333
    label "novum"
  ]
  node [
    id 334
    label "zamieszanie"
  ]
  node [
    id 335
    label "rozg&#322;os"
  ]
  node [
    id 336
    label "disclosure"
  ]
  node [
    id 337
    label "niespodzianka"
  ]
  node [
    id 338
    label "podekscytowanie"
  ]
  node [
    id 339
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 340
    label "po&#322;o&#380;enie"
  ]
  node [
    id 341
    label "ust&#281;p"
  ]
  node [
    id 342
    label "plan"
  ]
  node [
    id 343
    label "obiekt_matematyczny"
  ]
  node [
    id 344
    label "plamka"
  ]
  node [
    id 345
    label "stopie&#324;_pisma"
  ]
  node [
    id 346
    label "jednostka"
  ]
  node [
    id 347
    label "miejsce"
  ]
  node [
    id 348
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 349
    label "mark"
  ]
  node [
    id 350
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 351
    label "prosta"
  ]
  node [
    id 352
    label "zapunktowa&#263;"
  ]
  node [
    id 353
    label "podpunkt"
  ]
  node [
    id 354
    label "wojsko"
  ]
  node [
    id 355
    label "kres"
  ]
  node [
    id 356
    label "przestrze&#324;"
  ]
  node [
    id 357
    label "pozycja"
  ]
  node [
    id 358
    label "agreeableness"
  ]
  node [
    id 359
    label "attraction"
  ]
  node [
    id 360
    label "czar"
  ]
  node [
    id 361
    label "spoke"
  ]
  node [
    id 362
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 363
    label "zaczyna&#263;"
  ]
  node [
    id 364
    label "wyg&#322;asza&#263;"
  ]
  node [
    id 365
    label "address"
  ]
  node [
    id 366
    label "talk"
  ]
  node [
    id 367
    label "say"
  ]
  node [
    id 368
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 369
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 370
    label "odejmowa&#263;"
  ]
  node [
    id 371
    label "mie&#263;_miejsce"
  ]
  node [
    id 372
    label "bankrupt"
  ]
  node [
    id 373
    label "open"
  ]
  node [
    id 374
    label "set_about"
  ]
  node [
    id 375
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 376
    label "begin"
  ]
  node [
    id 377
    label "post&#281;powa&#263;"
  ]
  node [
    id 378
    label "uwydatnia&#263;"
  ]
  node [
    id 379
    label "eksploatowa&#263;"
  ]
  node [
    id 380
    label "uzyskiwa&#263;"
  ]
  node [
    id 381
    label "wydostawa&#263;"
  ]
  node [
    id 382
    label "wyjmowa&#263;"
  ]
  node [
    id 383
    label "train"
  ]
  node [
    id 384
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 385
    label "dobywa&#263;"
  ]
  node [
    id 386
    label "ocala&#263;"
  ]
  node [
    id 387
    label "excavate"
  ]
  node [
    id 388
    label "g&#243;rnictwo"
  ]
  node [
    id 389
    label "wypowiada&#263;"
  ]
  node [
    id 390
    label "determine"
  ]
  node [
    id 391
    label "work"
  ]
  node [
    id 392
    label "reakcja_chemiczna"
  ]
  node [
    id 393
    label "cover"
  ]
  node [
    id 394
    label "kosmetyk"
  ]
  node [
    id 395
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 396
    label "u&#380;ywa&#263;"
  ]
  node [
    id 397
    label "use"
  ]
  node [
    id 398
    label "take"
  ]
  node [
    id 399
    label "distribute"
  ]
  node [
    id 400
    label "bash"
  ]
  node [
    id 401
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 402
    label "doznawa&#263;"
  ]
  node [
    id 403
    label "okre&#347;lony"
  ]
  node [
    id 404
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 405
    label "wiadomy"
  ]
  node [
    id 406
    label "projektor"
  ]
  node [
    id 407
    label "przyrz&#261;d"
  ]
  node [
    id 408
    label "viewer"
  ]
  node [
    id 409
    label "browser"
  ]
  node [
    id 410
    label "program"
  ]
  node [
    id 411
    label "instalowa&#263;"
  ]
  node [
    id 412
    label "oprogramowanie"
  ]
  node [
    id 413
    label "odinstalowywa&#263;"
  ]
  node [
    id 414
    label "spis"
  ]
  node [
    id 415
    label "zaprezentowanie"
  ]
  node [
    id 416
    label "podprogram"
  ]
  node [
    id 417
    label "ogranicznik_referencyjny"
  ]
  node [
    id 418
    label "course_of_study"
  ]
  node [
    id 419
    label "booklet"
  ]
  node [
    id 420
    label "dzia&#322;"
  ]
  node [
    id 421
    label "odinstalowanie"
  ]
  node [
    id 422
    label "broszura"
  ]
  node [
    id 423
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 424
    label "kana&#322;"
  ]
  node [
    id 425
    label "teleferie"
  ]
  node [
    id 426
    label "zainstalowanie"
  ]
  node [
    id 427
    label "struktura_organizacyjna"
  ]
  node [
    id 428
    label "pirat"
  ]
  node [
    id 429
    label "zaprezentowa&#263;"
  ]
  node [
    id 430
    label "prezentowanie"
  ]
  node [
    id 431
    label "prezentowa&#263;"
  ]
  node [
    id 432
    label "interfejs"
  ]
  node [
    id 433
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 434
    label "okno"
  ]
  node [
    id 435
    label "blok"
  ]
  node [
    id 436
    label "folder"
  ]
  node [
    id 437
    label "zainstalowa&#263;"
  ]
  node [
    id 438
    label "za&#322;o&#380;enie"
  ]
  node [
    id 439
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 440
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 441
    label "ram&#243;wka"
  ]
  node [
    id 442
    label "tryb"
  ]
  node [
    id 443
    label "emitowa&#263;"
  ]
  node [
    id 444
    label "emitowanie"
  ]
  node [
    id 445
    label "odinstalowywanie"
  ]
  node [
    id 446
    label "instrukcja"
  ]
  node [
    id 447
    label "informatyka"
  ]
  node [
    id 448
    label "deklaracja"
  ]
  node [
    id 449
    label "menu"
  ]
  node [
    id 450
    label "sekcja_krytyczna"
  ]
  node [
    id 451
    label "furkacja"
  ]
  node [
    id 452
    label "podstawa"
  ]
  node [
    id 453
    label "instalowanie"
  ]
  node [
    id 454
    label "oferta"
  ]
  node [
    id 455
    label "odinstalowa&#263;"
  ]
  node [
    id 456
    label "utensylia"
  ]
  node [
    id 457
    label "operatornia"
  ]
  node [
    id 458
    label "projector"
  ]
  node [
    id 459
    label "kondensor"
  ]
  node [
    id 460
    label "szko&#322;a"
  ]
  node [
    id 461
    label "fraza"
  ]
  node [
    id 462
    label "przekazanie"
  ]
  node [
    id 463
    label "wypowiedzenie"
  ]
  node [
    id 464
    label "prison_term"
  ]
  node [
    id 465
    label "okres"
  ]
  node [
    id 466
    label "wyra&#380;enie"
  ]
  node [
    id 467
    label "zaliczenie"
  ]
  node [
    id 468
    label "antylogizm"
  ]
  node [
    id 469
    label "zmuszenie"
  ]
  node [
    id 470
    label "konektyw"
  ]
  node [
    id 471
    label "attitude"
  ]
  node [
    id 472
    label "powierzenie"
  ]
  node [
    id 473
    label "adjudication"
  ]
  node [
    id 474
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 475
    label "pass"
  ]
  node [
    id 476
    label "spe&#322;nienie"
  ]
  node [
    id 477
    label "wliczenie"
  ]
  node [
    id 478
    label "zaliczanie"
  ]
  node [
    id 479
    label "zaklasyfikowanie_si&#281;"
  ]
  node [
    id 480
    label "crack"
  ]
  node [
    id 481
    label "zadanie"
  ]
  node [
    id 482
    label "odb&#281;bnienie"
  ]
  node [
    id 483
    label "ocenienie"
  ]
  node [
    id 484
    label "number"
  ]
  node [
    id 485
    label "policzenie"
  ]
  node [
    id 486
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 487
    label "przeklasyfikowanie"
  ]
  node [
    id 488
    label "zaliczanie_si&#281;"
  ]
  node [
    id 489
    label "wzi&#281;cie"
  ]
  node [
    id 490
    label "dor&#281;czenie"
  ]
  node [
    id 491
    label "wys&#322;anie"
  ]
  node [
    id 492
    label "podanie"
  ]
  node [
    id 493
    label "delivery"
  ]
  node [
    id 494
    label "transfer"
  ]
  node [
    id 495
    label "wp&#322;acenie"
  ]
  node [
    id 496
    label "z&#322;o&#380;enie"
  ]
  node [
    id 497
    label "sygna&#322;"
  ]
  node [
    id 498
    label "zrobienie"
  ]
  node [
    id 499
    label "leksem"
  ]
  node [
    id 500
    label "zdarzenie_si&#281;"
  ]
  node [
    id 501
    label "poj&#281;cie"
  ]
  node [
    id 502
    label "poinformowanie"
  ]
  node [
    id 503
    label "wording"
  ]
  node [
    id 504
    label "kompozycja"
  ]
  node [
    id 505
    label "oznaczenie"
  ]
  node [
    id 506
    label "znak_j&#281;zykowy"
  ]
  node [
    id 507
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 508
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 509
    label "grupa_imienna"
  ]
  node [
    id 510
    label "jednostka_leksykalna"
  ]
  node [
    id 511
    label "term"
  ]
  node [
    id 512
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 513
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 514
    label "ujawnienie"
  ]
  node [
    id 515
    label "affirmation"
  ]
  node [
    id 516
    label "zapisanie"
  ]
  node [
    id 517
    label "rzucenie"
  ]
  node [
    id 518
    label "pr&#243;bowanie"
  ]
  node [
    id 519
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 520
    label "zademonstrowanie"
  ]
  node [
    id 521
    label "report"
  ]
  node [
    id 522
    label "obgadanie"
  ]
  node [
    id 523
    label "realizacja"
  ]
  node [
    id 524
    label "scena"
  ]
  node [
    id 525
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 526
    label "narration"
  ]
  node [
    id 527
    label "cyrk"
  ]
  node [
    id 528
    label "theatrical_performance"
  ]
  node [
    id 529
    label "opisanie"
  ]
  node [
    id 530
    label "malarstwo"
  ]
  node [
    id 531
    label "scenografia"
  ]
  node [
    id 532
    label "teatr"
  ]
  node [
    id 533
    label "ukazanie"
  ]
  node [
    id 534
    label "zapoznanie"
  ]
  node [
    id 535
    label "pokaz"
  ]
  node [
    id 536
    label "ods&#322;ona"
  ]
  node [
    id 537
    label "exhibit"
  ]
  node [
    id 538
    label "pokazanie"
  ]
  node [
    id 539
    label "wyst&#261;pienie"
  ]
  node [
    id 540
    label "przedstawi&#263;"
  ]
  node [
    id 541
    label "przedstawianie"
  ]
  node [
    id 542
    label "constraint"
  ]
  node [
    id 543
    label "zadanie_gwa&#322;tu"
  ]
  node [
    id 544
    label "oddzia&#322;anie"
  ]
  node [
    id 545
    label "spowodowanie"
  ]
  node [
    id 546
    label "force"
  ]
  node [
    id 547
    label "pop&#281;dzenie_"
  ]
  node [
    id 548
    label "konwersja"
  ]
  node [
    id 549
    label "notice"
  ]
  node [
    id 550
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 551
    label "przepowiedzenie"
  ]
  node [
    id 552
    label "rozwi&#261;zanie"
  ]
  node [
    id 553
    label "generowa&#263;"
  ]
  node [
    id 554
    label "wydanie"
  ]
  node [
    id 555
    label "message"
  ]
  node [
    id 556
    label "generowanie"
  ]
  node [
    id 557
    label "wydobycie"
  ]
  node [
    id 558
    label "zwerbalizowanie"
  ]
  node [
    id 559
    label "szyk"
  ]
  node [
    id 560
    label "notification"
  ]
  node [
    id 561
    label "powiedzenie"
  ]
  node [
    id 562
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 563
    label "denunciation"
  ]
  node [
    id 564
    label "pogl&#261;d"
  ]
  node [
    id 565
    label "awansowa&#263;"
  ]
  node [
    id 566
    label "uprawianie"
  ]
  node [
    id 567
    label "wakowa&#263;"
  ]
  node [
    id 568
    label "powierzanie"
  ]
  node [
    id 569
    label "postawi&#263;"
  ]
  node [
    id 570
    label "awansowanie"
  ]
  node [
    id 571
    label "praca"
  ]
  node [
    id 572
    label "wyznanie"
  ]
  node [
    id 573
    label "zlecenie"
  ]
  node [
    id 574
    label "ufanie"
  ]
  node [
    id 575
    label "commitment"
  ]
  node [
    id 576
    label "perpetration"
  ]
  node [
    id 577
    label "oddanie"
  ]
  node [
    id 578
    label "do&#347;wiadczenie"
  ]
  node [
    id 579
    label "teren_szko&#322;y"
  ]
  node [
    id 580
    label "wiedza"
  ]
  node [
    id 581
    label "Mickiewicz"
  ]
  node [
    id 582
    label "kwalifikacje"
  ]
  node [
    id 583
    label "podr&#281;cznik"
  ]
  node [
    id 584
    label "absolwent"
  ]
  node [
    id 585
    label "praktyka"
  ]
  node [
    id 586
    label "school"
  ]
  node [
    id 587
    label "zda&#263;"
  ]
  node [
    id 588
    label "gabinet"
  ]
  node [
    id 589
    label "urszulanki"
  ]
  node [
    id 590
    label "sztuba"
  ]
  node [
    id 591
    label "&#322;awa_szkolna"
  ]
  node [
    id 592
    label "nauka"
  ]
  node [
    id 593
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 594
    label "przepisa&#263;"
  ]
  node [
    id 595
    label "muzyka"
  ]
  node [
    id 596
    label "grupa"
  ]
  node [
    id 597
    label "form"
  ]
  node [
    id 598
    label "klasa"
  ]
  node [
    id 599
    label "lekcja"
  ]
  node [
    id 600
    label "metoda"
  ]
  node [
    id 601
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 602
    label "przepisanie"
  ]
  node [
    id 603
    label "skolaryzacja"
  ]
  node [
    id 604
    label "stopek"
  ]
  node [
    id 605
    label "sekretariat"
  ]
  node [
    id 606
    label "ideologia"
  ]
  node [
    id 607
    label "lesson"
  ]
  node [
    id 608
    label "instytucja"
  ]
  node [
    id 609
    label "niepokalanki"
  ]
  node [
    id 610
    label "siedziba"
  ]
  node [
    id 611
    label "szkolenie"
  ]
  node [
    id 612
    label "kara"
  ]
  node [
    id 613
    label "tablica"
  ]
  node [
    id 614
    label "funktor"
  ]
  node [
    id 615
    label "j&#261;dro"
  ]
  node [
    id 616
    label "rozprz&#261;c"
  ]
  node [
    id 617
    label "systemat"
  ]
  node [
    id 618
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 619
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 620
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 621
    label "model"
  ]
  node [
    id 622
    label "struktura"
  ]
  node [
    id 623
    label "usenet"
  ]
  node [
    id 624
    label "porz&#261;dek"
  ]
  node [
    id 625
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 626
    label "przyn&#281;ta"
  ]
  node [
    id 627
    label "p&#322;&#243;d"
  ]
  node [
    id 628
    label "net"
  ]
  node [
    id 629
    label "eratem"
  ]
  node [
    id 630
    label "oddzia&#322;"
  ]
  node [
    id 631
    label "doktryna"
  ]
  node [
    id 632
    label "pulpit"
  ]
  node [
    id 633
    label "jednostka_geologiczna"
  ]
  node [
    id 634
    label "o&#347;"
  ]
  node [
    id 635
    label "podsystem"
  ]
  node [
    id 636
    label "Leopard"
  ]
  node [
    id 637
    label "Android"
  ]
  node [
    id 638
    label "cybernetyk"
  ]
  node [
    id 639
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 640
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 641
    label "method"
  ]
  node [
    id 642
    label "sk&#322;ad"
  ]
  node [
    id 643
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 644
    label "relacja_logiczna"
  ]
  node [
    id 645
    label "okres_amazo&#324;ski"
  ]
  node [
    id 646
    label "stater"
  ]
  node [
    id 647
    label "flow"
  ]
  node [
    id 648
    label "choroba_przyrodzona"
  ]
  node [
    id 649
    label "postglacja&#322;"
  ]
  node [
    id 650
    label "sylur"
  ]
  node [
    id 651
    label "kreda"
  ]
  node [
    id 652
    label "ordowik"
  ]
  node [
    id 653
    label "okres_hesperyjski"
  ]
  node [
    id 654
    label "paleogen"
  ]
  node [
    id 655
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 656
    label "okres_halsztacki"
  ]
  node [
    id 657
    label "riak"
  ]
  node [
    id 658
    label "czwartorz&#281;d"
  ]
  node [
    id 659
    label "podokres"
  ]
  node [
    id 660
    label "trzeciorz&#281;d"
  ]
  node [
    id 661
    label "kalim"
  ]
  node [
    id 662
    label "fala"
  ]
  node [
    id 663
    label "perm"
  ]
  node [
    id 664
    label "retoryka"
  ]
  node [
    id 665
    label "prekambr"
  ]
  node [
    id 666
    label "faza"
  ]
  node [
    id 667
    label "neogen"
  ]
  node [
    id 668
    label "pulsacja"
  ]
  node [
    id 669
    label "proces_fizjologiczny"
  ]
  node [
    id 670
    label "kambr"
  ]
  node [
    id 671
    label "kriogen"
  ]
  node [
    id 672
    label "ton"
  ]
  node [
    id 673
    label "orosir"
  ]
  node [
    id 674
    label "poprzednik"
  ]
  node [
    id 675
    label "interstadia&#322;"
  ]
  node [
    id 676
    label "ektas"
  ]
  node [
    id 677
    label "sider"
  ]
  node [
    id 678
    label "epoka"
  ]
  node [
    id 679
    label "rok_akademicki"
  ]
  node [
    id 680
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 681
    label "cykl"
  ]
  node [
    id 682
    label "ciota"
  ]
  node [
    id 683
    label "pierwszorz&#281;d"
  ]
  node [
    id 684
    label "okres_noachijski"
  ]
  node [
    id 685
    label "ediakar"
  ]
  node [
    id 686
    label "nast&#281;pnik"
  ]
  node [
    id 687
    label "condition"
  ]
  node [
    id 688
    label "jura"
  ]
  node [
    id 689
    label "glacja&#322;"
  ]
  node [
    id 690
    label "sten"
  ]
  node [
    id 691
    label "era"
  ]
  node [
    id 692
    label "trias"
  ]
  node [
    id 693
    label "p&#243;&#322;okres"
  ]
  node [
    id 694
    label "rok_szkolny"
  ]
  node [
    id 695
    label "dewon"
  ]
  node [
    id 696
    label "karbon"
  ]
  node [
    id 697
    label "izochronizm"
  ]
  node [
    id 698
    label "preglacja&#322;"
  ]
  node [
    id 699
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 700
    label "drugorz&#281;d"
  ]
  node [
    id 701
    label "semester"
  ]
  node [
    id 702
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 703
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 704
    label "motyw"
  ]
  node [
    id 705
    label "addition"
  ]
  node [
    id 706
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 707
    label "nagana"
  ]
  node [
    id 708
    label "upomnienie"
  ]
  node [
    id 709
    label "dzienniczek"
  ]
  node [
    id 710
    label "wzgl&#261;d"
  ]
  node [
    id 711
    label "gossip"
  ]
  node [
    id 712
    label "Ohio"
  ]
  node [
    id 713
    label "wci&#281;cie"
  ]
  node [
    id 714
    label "Nowy_York"
  ]
  node [
    id 715
    label "warstwa"
  ]
  node [
    id 716
    label "samopoczucie"
  ]
  node [
    id 717
    label "Illinois"
  ]
  node [
    id 718
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 719
    label "state"
  ]
  node [
    id 720
    label "Jukatan"
  ]
  node [
    id 721
    label "Kalifornia"
  ]
  node [
    id 722
    label "Wirginia"
  ]
  node [
    id 723
    label "wektor"
  ]
  node [
    id 724
    label "by&#263;"
  ]
  node [
    id 725
    label "Teksas"
  ]
  node [
    id 726
    label "Goa"
  ]
  node [
    id 727
    label "Waszyngton"
  ]
  node [
    id 728
    label "Massachusetts"
  ]
  node [
    id 729
    label "Alaska"
  ]
  node [
    id 730
    label "Arakan"
  ]
  node [
    id 731
    label "Hawaje"
  ]
  node [
    id 732
    label "Maryland"
  ]
  node [
    id 733
    label "Michigan"
  ]
  node [
    id 734
    label "Arizona"
  ]
  node [
    id 735
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 736
    label "Georgia"
  ]
  node [
    id 737
    label "poziom"
  ]
  node [
    id 738
    label "Pensylwania"
  ]
  node [
    id 739
    label "shape"
  ]
  node [
    id 740
    label "Luizjana"
  ]
  node [
    id 741
    label "Nowy_Meksyk"
  ]
  node [
    id 742
    label "Alabama"
  ]
  node [
    id 743
    label "Kansas"
  ]
  node [
    id 744
    label "Oregon"
  ]
  node [
    id 745
    label "Floryda"
  ]
  node [
    id 746
    label "Oklahoma"
  ]
  node [
    id 747
    label "jednostka_administracyjna"
  ]
  node [
    id 748
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 749
    label "admonicja"
  ]
  node [
    id 750
    label "ukaranie"
  ]
  node [
    id 751
    label "krytyka"
  ]
  node [
    id 752
    label "censure"
  ]
  node [
    id 753
    label "wezwanie"
  ]
  node [
    id 754
    label "pouczenie"
  ]
  node [
    id 755
    label "monitorium"
  ]
  node [
    id 756
    label "napomnienie"
  ]
  node [
    id 757
    label "steering"
  ]
  node [
    id 758
    label "przyczyna"
  ]
  node [
    id 759
    label "trypanosomoza"
  ]
  node [
    id 760
    label "criticism"
  ]
  node [
    id 761
    label "schorzenie"
  ]
  node [
    id 762
    label "&#347;widrowiec_nagany"
  ]
  node [
    id 763
    label "zeszyt"
  ]
  node [
    id 764
    label "federacja"
  ]
  node [
    id 765
    label "biblioteka"
  ]
  node [
    id 766
    label "cyfrowy"
  ]
  node [
    id 767
    label "OAI"
  ]
  node [
    id 768
    label "PMH"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 322
  ]
  edge [
    source 12
    target 323
  ]
  edge [
    source 12
    target 324
  ]
  edge [
    source 12
    target 325
  ]
  edge [
    source 12
    target 326
  ]
  edge [
    source 12
    target 327
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 12
    target 331
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 335
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 337
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 249
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 250
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 251
  ]
  edge [
    source 12
    target 206
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 233
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 59
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 42
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 35
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 396
  ]
  edge [
    source 14
    target 397
  ]
  edge [
    source 14
    target 380
  ]
  edge [
    source 14
    target 38
  ]
  edge [
    source 14
    target 398
  ]
  edge [
    source 14
    target 39
  ]
  edge [
    source 14
    target 349
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 399
  ]
  edge [
    source 14
    target 48
  ]
  edge [
    source 14
    target 400
  ]
  edge [
    source 14
    target 401
  ]
  edge [
    source 14
    target 402
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 403
  ]
  edge [
    source 15
    target 404
  ]
  edge [
    source 15
    target 405
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 406
  ]
  edge [
    source 16
    target 407
  ]
  edge [
    source 16
    target 408
  ]
  edge [
    source 16
    target 409
  ]
  edge [
    source 16
    target 410
  ]
  edge [
    source 16
    target 411
  ]
  edge [
    source 16
    target 412
  ]
  edge [
    source 16
    target 413
  ]
  edge [
    source 16
    target 414
  ]
  edge [
    source 16
    target 415
  ]
  edge [
    source 16
    target 416
  ]
  edge [
    source 16
    target 417
  ]
  edge [
    source 16
    target 418
  ]
  edge [
    source 16
    target 419
  ]
  edge [
    source 16
    target 420
  ]
  edge [
    source 16
    target 421
  ]
  edge [
    source 16
    target 422
  ]
  edge [
    source 16
    target 92
  ]
  edge [
    source 16
    target 423
  ]
  edge [
    source 16
    target 424
  ]
  edge [
    source 16
    target 425
  ]
  edge [
    source 16
    target 426
  ]
  edge [
    source 16
    target 427
  ]
  edge [
    source 16
    target 428
  ]
  edge [
    source 16
    target 429
  ]
  edge [
    source 16
    target 430
  ]
  edge [
    source 16
    target 431
  ]
  edge [
    source 16
    target 432
  ]
  edge [
    source 16
    target 433
  ]
  edge [
    source 16
    target 434
  ]
  edge [
    source 16
    target 435
  ]
  edge [
    source 16
    target 323
  ]
  edge [
    source 16
    target 436
  ]
  edge [
    source 16
    target 437
  ]
  edge [
    source 16
    target 438
  ]
  edge [
    source 16
    target 439
  ]
  edge [
    source 16
    target 440
  ]
  edge [
    source 16
    target 441
  ]
  edge [
    source 16
    target 442
  ]
  edge [
    source 16
    target 443
  ]
  edge [
    source 16
    target 444
  ]
  edge [
    source 16
    target 445
  ]
  edge [
    source 16
    target 446
  ]
  edge [
    source 16
    target 447
  ]
  edge [
    source 16
    target 448
  ]
  edge [
    source 16
    target 449
  ]
  edge [
    source 16
    target 450
  ]
  edge [
    source 16
    target 451
  ]
  edge [
    source 16
    target 452
  ]
  edge [
    source 16
    target 453
  ]
  edge [
    source 16
    target 454
  ]
  edge [
    source 16
    target 455
  ]
  edge [
    source 16
    target 456
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 457
  ]
  edge [
    source 16
    target 458
  ]
  edge [
    source 16
    target 459
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 460
  ]
  edge [
    source 18
    target 461
  ]
  edge [
    source 18
    target 462
  ]
  edge [
    source 18
    target 71
  ]
  edge [
    source 18
    target 463
  ]
  edge [
    source 18
    target 464
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 465
  ]
  edge [
    source 18
    target 234
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 468
  ]
  edge [
    source 18
    target 469
  ]
  edge [
    source 18
    target 470
  ]
  edge [
    source 18
    target 471
  ]
  edge [
    source 18
    target 472
  ]
  edge [
    source 18
    target 473
  ]
  edge [
    source 18
    target 474
  ]
  edge [
    source 18
    target 475
  ]
  edge [
    source 18
    target 476
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 478
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 480
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 482
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 484
  ]
  edge [
    source 18
    target 485
  ]
  edge [
    source 18
    target 486
  ]
  edge [
    source 18
    target 487
  ]
  edge [
    source 18
    target 488
  ]
  edge [
    source 18
    target 489
  ]
  edge [
    source 18
    target 490
  ]
  edge [
    source 18
    target 491
  ]
  edge [
    source 18
    target 492
  ]
  edge [
    source 18
    target 493
  ]
  edge [
    source 18
    target 494
  ]
  edge [
    source 18
    target 495
  ]
  edge [
    source 18
    target 496
  ]
  edge [
    source 18
    target 497
  ]
  edge [
    source 18
    target 498
  ]
  edge [
    source 18
    target 499
  ]
  edge [
    source 18
    target 271
  ]
  edge [
    source 18
    target 500
  ]
  edge [
    source 18
    target 501
  ]
  edge [
    source 18
    target 502
  ]
  edge [
    source 18
    target 503
  ]
  edge [
    source 18
    target 504
  ]
  edge [
    source 18
    target 505
  ]
  edge [
    source 18
    target 506
  ]
  edge [
    source 18
    target 507
  ]
  edge [
    source 18
    target 273
  ]
  edge [
    source 18
    target 508
  ]
  edge [
    source 18
    target 509
  ]
  edge [
    source 18
    target 510
  ]
  edge [
    source 18
    target 511
  ]
  edge [
    source 18
    target 512
  ]
  edge [
    source 18
    target 513
  ]
  edge [
    source 18
    target 514
  ]
  edge [
    source 18
    target 515
  ]
  edge [
    source 18
    target 516
  ]
  edge [
    source 18
    target 517
  ]
  edge [
    source 18
    target 518
  ]
  edge [
    source 18
    target 519
  ]
  edge [
    source 18
    target 520
  ]
  edge [
    source 18
    target 521
  ]
  edge [
    source 18
    target 522
  ]
  edge [
    source 18
    target 523
  ]
  edge [
    source 18
    target 524
  ]
  edge [
    source 18
    target 525
  ]
  edge [
    source 18
    target 526
  ]
  edge [
    source 18
    target 527
  ]
  edge [
    source 18
    target 92
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 528
  ]
  edge [
    source 18
    target 529
  ]
  edge [
    source 18
    target 530
  ]
  edge [
    source 18
    target 531
  ]
  edge [
    source 18
    target 532
  ]
  edge [
    source 18
    target 533
  ]
  edge [
    source 18
    target 534
  ]
  edge [
    source 18
    target 535
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 536
  ]
  edge [
    source 18
    target 537
  ]
  edge [
    source 18
    target 538
  ]
  edge [
    source 18
    target 539
  ]
  edge [
    source 18
    target 540
  ]
  edge [
    source 18
    target 541
  ]
  edge [
    source 18
    target 81
  ]
  edge [
    source 18
    target 289
  ]
  edge [
    source 18
    target 542
  ]
  edge [
    source 18
    target 543
  ]
  edge [
    source 18
    target 544
  ]
  edge [
    source 18
    target 545
  ]
  edge [
    source 18
    target 546
  ]
  edge [
    source 18
    target 547
  ]
  edge [
    source 18
    target 548
  ]
  edge [
    source 18
    target 549
  ]
  edge [
    source 18
    target 550
  ]
  edge [
    source 18
    target 551
  ]
  edge [
    source 18
    target 552
  ]
  edge [
    source 18
    target 553
  ]
  edge [
    source 18
    target 554
  ]
  edge [
    source 18
    target 555
  ]
  edge [
    source 18
    target 556
  ]
  edge [
    source 18
    target 557
  ]
  edge [
    source 18
    target 558
  ]
  edge [
    source 18
    target 559
  ]
  edge [
    source 18
    target 560
  ]
  edge [
    source 18
    target 561
  ]
  edge [
    source 18
    target 562
  ]
  edge [
    source 18
    target 563
  ]
  edge [
    source 18
    target 340
  ]
  edge [
    source 18
    target 323
  ]
  edge [
    source 18
    target 564
  ]
  edge [
    source 18
    target 354
  ]
  edge [
    source 18
    target 565
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 566
  ]
  edge [
    source 18
    target 567
  ]
  edge [
    source 18
    target 568
  ]
  edge [
    source 18
    target 569
  ]
  edge [
    source 18
    target 347
  ]
  edge [
    source 18
    target 570
  ]
  edge [
    source 18
    target 571
  ]
  edge [
    source 18
    target 572
  ]
  edge [
    source 18
    target 573
  ]
  edge [
    source 18
    target 574
  ]
  edge [
    source 18
    target 575
  ]
  edge [
    source 18
    target 576
  ]
  edge [
    source 18
    target 577
  ]
  edge [
    source 18
    target 578
  ]
  edge [
    source 18
    target 579
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 581
  ]
  edge [
    source 18
    target 582
  ]
  edge [
    source 18
    target 583
  ]
  edge [
    source 18
    target 584
  ]
  edge [
    source 18
    target 585
  ]
  edge [
    source 18
    target 586
  ]
  edge [
    source 18
    target 587
  ]
  edge [
    source 18
    target 588
  ]
  edge [
    source 18
    target 589
  ]
  edge [
    source 18
    target 590
  ]
  edge [
    source 18
    target 591
  ]
  edge [
    source 18
    target 592
  ]
  edge [
    source 18
    target 593
  ]
  edge [
    source 18
    target 594
  ]
  edge [
    source 18
    target 595
  ]
  edge [
    source 18
    target 596
  ]
  edge [
    source 18
    target 597
  ]
  edge [
    source 18
    target 598
  ]
  edge [
    source 18
    target 599
  ]
  edge [
    source 18
    target 600
  ]
  edge [
    source 18
    target 601
  ]
  edge [
    source 18
    target 602
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 603
  ]
  edge [
    source 18
    target 604
  ]
  edge [
    source 18
    target 605
  ]
  edge [
    source 18
    target 606
  ]
  edge [
    source 18
    target 607
  ]
  edge [
    source 18
    target 608
  ]
  edge [
    source 18
    target 609
  ]
  edge [
    source 18
    target 610
  ]
  edge [
    source 18
    target 611
  ]
  edge [
    source 18
    target 612
  ]
  edge [
    source 18
    target 613
  ]
  edge [
    source 18
    target 264
  ]
  edge [
    source 18
    target 614
  ]
  edge [
    source 18
    target 615
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 616
  ]
  edge [
    source 18
    target 412
  ]
  edge [
    source 18
    target 617
  ]
  edge [
    source 18
    target 618
  ]
  edge [
    source 18
    target 619
  ]
  edge [
    source 18
    target 620
  ]
  edge [
    source 18
    target 621
  ]
  edge [
    source 18
    target 622
  ]
  edge [
    source 18
    target 623
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 624
  ]
  edge [
    source 18
    target 625
  ]
  edge [
    source 18
    target 626
  ]
  edge [
    source 18
    target 627
  ]
  edge [
    source 18
    target 628
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 629
  ]
  edge [
    source 18
    target 630
  ]
  edge [
    source 18
    target 631
  ]
  edge [
    source 18
    target 632
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 18
    target 633
  ]
  edge [
    source 18
    target 634
  ]
  edge [
    source 18
    target 635
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 636
  ]
  edge [
    source 18
    target 637
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 638
  ]
  edge [
    source 18
    target 639
  ]
  edge [
    source 18
    target 640
  ]
  edge [
    source 18
    target 641
  ]
  edge [
    source 18
    target 642
  ]
  edge [
    source 18
    target 452
  ]
  edge [
    source 18
    target 643
  ]
  edge [
    source 18
    target 644
  ]
  edge [
    source 18
    target 645
  ]
  edge [
    source 18
    target 646
  ]
  edge [
    source 18
    target 647
  ]
  edge [
    source 18
    target 648
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 650
  ]
  edge [
    source 18
    target 651
  ]
  edge [
    source 18
    target 652
  ]
  edge [
    source 18
    target 653
  ]
  edge [
    source 18
    target 654
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 656
  ]
  edge [
    source 18
    target 657
  ]
  edge [
    source 18
    target 658
  ]
  edge [
    source 18
    target 659
  ]
  edge [
    source 18
    target 660
  ]
  edge [
    source 18
    target 661
  ]
  edge [
    source 18
    target 662
  ]
  edge [
    source 18
    target 663
  ]
  edge [
    source 18
    target 664
  ]
  edge [
    source 18
    target 665
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 667
  ]
  edge [
    source 18
    target 668
  ]
  edge [
    source 18
    target 669
  ]
  edge [
    source 18
    target 670
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 671
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 672
  ]
  edge [
    source 18
    target 673
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 679
  ]
  edge [
    source 18
    target 680
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 681
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 685
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 705
  ]
  edge [
    source 20
    target 90
  ]
  edge [
    source 20
    target 706
  ]
  edge [
    source 20
    target 297
  ]
  edge [
    source 20
    target 707
  ]
  edge [
    source 20
    target 30
  ]
  edge [
    source 20
    target 708
  ]
  edge [
    source 20
    target 709
  ]
  edge [
    source 20
    target 710
  ]
  edge [
    source 20
    target 711
  ]
  edge [
    source 20
    target 712
  ]
  edge [
    source 20
    target 713
  ]
  edge [
    source 20
    target 714
  ]
  edge [
    source 20
    target 715
  ]
  edge [
    source 20
    target 716
  ]
  edge [
    source 20
    target 717
  ]
  edge [
    source 20
    target 718
  ]
  edge [
    source 20
    target 719
  ]
  edge [
    source 20
    target 720
  ]
  edge [
    source 20
    target 721
  ]
  edge [
    source 20
    target 722
  ]
  edge [
    source 20
    target 723
  ]
  edge [
    source 20
    target 724
  ]
  edge [
    source 20
    target 725
  ]
  edge [
    source 20
    target 726
  ]
  edge [
    source 20
    target 727
  ]
  edge [
    source 20
    target 347
  ]
  edge [
    source 20
    target 728
  ]
  edge [
    source 20
    target 729
  ]
  edge [
    source 20
    target 730
  ]
  edge [
    source 20
    target 731
  ]
  edge [
    source 20
    target 732
  ]
  edge [
    source 20
    target 323
  ]
  edge [
    source 20
    target 733
  ]
  edge [
    source 20
    target 734
  ]
  edge [
    source 20
    target 735
  ]
  edge [
    source 20
    target 736
  ]
  edge [
    source 20
    target 737
  ]
  edge [
    source 20
    target 738
  ]
  edge [
    source 20
    target 739
  ]
  edge [
    source 20
    target 740
  ]
  edge [
    source 20
    target 741
  ]
  edge [
    source 20
    target 742
  ]
  edge [
    source 20
    target 310
  ]
  edge [
    source 20
    target 743
  ]
  edge [
    source 20
    target 744
  ]
  edge [
    source 20
    target 745
  ]
  edge [
    source 20
    target 746
  ]
  edge [
    source 20
    target 747
  ]
  edge [
    source 20
    target 748
  ]
  edge [
    source 20
    target 88
  ]
  edge [
    source 20
    target 89
  ]
  edge [
    source 20
    target 91
  ]
  edge [
    source 20
    target 92
  ]
  edge [
    source 20
    target 93
  ]
  edge [
    source 20
    target 94
  ]
  edge [
    source 20
    target 95
  ]
  edge [
    source 20
    target 96
  ]
  edge [
    source 20
    target 97
  ]
  edge [
    source 20
    target 98
  ]
  edge [
    source 20
    target 99
  ]
  edge [
    source 20
    target 263
  ]
  edge [
    source 20
    target 264
  ]
  edge [
    source 20
    target 265
  ]
  edge [
    source 20
    target 267
  ]
  edge [
    source 20
    target 266
  ]
  edge [
    source 20
    target 269
  ]
  edge [
    source 20
    target 270
  ]
  edge [
    source 20
    target 268
  ]
  edge [
    source 20
    target 271
  ]
  edge [
    source 20
    target 272
  ]
  edge [
    source 20
    target 273
  ]
  edge [
    source 20
    target 274
  ]
  edge [
    source 20
    target 275
  ]
  edge [
    source 20
    target 276
  ]
  edge [
    source 20
    target 277
  ]
  edge [
    source 20
    target 278
  ]
  edge [
    source 20
    target 279
  ]
  edge [
    source 20
    target 245
  ]
  edge [
    source 20
    target 749
  ]
  edge [
    source 20
    target 750
  ]
  edge [
    source 20
    target 751
  ]
  edge [
    source 20
    target 752
  ]
  edge [
    source 20
    target 753
  ]
  edge [
    source 20
    target 754
  ]
  edge [
    source 20
    target 755
  ]
  edge [
    source 20
    target 756
  ]
  edge [
    source 20
    target 757
  ]
  edge [
    source 20
    target 612
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 758
  ]
  edge [
    source 20
    target 759
  ]
  edge [
    source 20
    target 760
  ]
  edge [
    source 20
    target 761
  ]
  edge [
    source 20
    target 762
  ]
  edge [
    source 20
    target 763
  ]
  edge [
    source 764
    target 765
  ]
  edge [
    source 764
    target 766
  ]
  edge [
    source 765
    target 766
  ]
  edge [
    source 767
    target 768
  ]
]
