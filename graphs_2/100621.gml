graph [
  node [
    id 0
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 1
    label "specjalista"
    origin "text"
  ]
  node [
    id 2
    label "dziedzina"
    origin "text"
  ]
  node [
    id 3
    label "medycyna"
    origin "text"
  ]
  node [
    id 4
    label "wieszczy"
    origin "text"
  ]
  node [
    id 5
    label "pozosta&#263;"
    origin "text"
  ]
  node [
    id 6
    label "prze&#380;ycie"
    origin "text"
  ]
  node [
    id 7
    label "dwa"
    origin "text"
  ]
  node [
    id 8
    label "dni"
    origin "text"
  ]
  node [
    id 9
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 10
    label "wszystko"
    origin "text"
  ]
  node [
    id 11
    label "czczy"
    origin "text"
  ]
  node [
    id 12
    label "spekulacja"
    origin "text"
  ]
  node [
    id 13
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 14
    label "reagowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "u&#347;miech"
    origin "text"
  ]
  node [
    id 16
    label "spogl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 17
    label "przysz&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "bez"
    origin "text"
  ]
  node [
    id 19
    label "zacietrzewi&#263;"
    origin "text"
  ]
  node [
    id 20
    label "optymizm"
    origin "text"
  ]
  node [
    id 21
    label "ale"
    origin "text"
  ]
  node [
    id 22
    label "strach"
    origin "text"
  ]
  node [
    id 23
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 24
    label "jedyna"
    origin "text"
  ]
  node [
    id 25
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 26
    label "ten"
    origin "text"
  ]
  node [
    id 27
    label "chwila"
    origin "text"
  ]
  node [
    id 28
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 29
    label "przes&#322;a&#263;"
    origin "text"
  ]
  node [
    id 30
    label "dow&#243;d"
    origin "text"
  ]
  node [
    id 31
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 32
    label "mi&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 33
    label "zach&#281;ci&#263;"
    origin "text"
  ]
  node [
    id 34
    label "abyby&#263;"
    origin "text"
  ]
  node [
    id 35
    label "utrzymywa&#263;"
    origin "text"
  ]
  node [
    id 36
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 37
    label "ruch"
    origin "text"
  ]
  node [
    id 38
    label "wybaczy&#263;"
    origin "text"
  ]
  node [
    id 39
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 40
    label "moja"
    origin "text"
  ]
  node [
    id 41
    label "lekkomy&#347;lno&#347;&#263;"
    origin "text"
  ]
  node [
    id 42
    label "by&#263;"
    origin "text"
  ]
  node [
    id 43
    label "stan"
    origin "text"
  ]
  node [
    id 44
    label "traktowa&#263;"
    origin "text"
  ]
  node [
    id 45
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 46
    label "serio"
    origin "text"
  ]
  node [
    id 47
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 48
    label "si&#281;"
    origin "text"
  ]
  node [
    id 49
    label "niedorzeczny"
    origin "text"
  ]
  node [
    id 50
    label "inny"
  ]
  node [
    id 51
    label "jaki&#347;"
  ]
  node [
    id 52
    label "r&#243;&#380;nie"
  ]
  node [
    id 53
    label "przyzwoity"
  ]
  node [
    id 54
    label "ciekawy"
  ]
  node [
    id 55
    label "jako&#347;"
  ]
  node [
    id 56
    label "jako_tako"
  ]
  node [
    id 57
    label "niez&#322;y"
  ]
  node [
    id 58
    label "dziwny"
  ]
  node [
    id 59
    label "charakterystyczny"
  ]
  node [
    id 60
    label "kolejny"
  ]
  node [
    id 61
    label "osobno"
  ]
  node [
    id 62
    label "inszy"
  ]
  node [
    id 63
    label "inaczej"
  ]
  node [
    id 64
    label "osobnie"
  ]
  node [
    id 65
    label "cz&#322;owiek"
  ]
  node [
    id 66
    label "znawca"
  ]
  node [
    id 67
    label "wyspecjalizowanie_si&#281;"
  ]
  node [
    id 68
    label "lekarz"
  ]
  node [
    id 69
    label "spec"
  ]
  node [
    id 70
    label "Mesmer"
  ]
  node [
    id 71
    label "pracownik"
  ]
  node [
    id 72
    label "Galen"
  ]
  node [
    id 73
    label "zbada&#263;"
  ]
  node [
    id 74
    label "medyk"
  ]
  node [
    id 75
    label "eskulap"
  ]
  node [
    id 76
    label "lekarze"
  ]
  node [
    id 77
    label "Hipokrates"
  ]
  node [
    id 78
    label "dokt&#243;r"
  ]
  node [
    id 79
    label "ludzko&#347;&#263;"
  ]
  node [
    id 80
    label "asymilowanie"
  ]
  node [
    id 81
    label "wapniak"
  ]
  node [
    id 82
    label "asymilowa&#263;"
  ]
  node [
    id 83
    label "os&#322;abia&#263;"
  ]
  node [
    id 84
    label "posta&#263;"
  ]
  node [
    id 85
    label "hominid"
  ]
  node [
    id 86
    label "podw&#322;adny"
  ]
  node [
    id 87
    label "os&#322;abianie"
  ]
  node [
    id 88
    label "g&#322;owa"
  ]
  node [
    id 89
    label "figura"
  ]
  node [
    id 90
    label "portrecista"
  ]
  node [
    id 91
    label "dwun&#243;g"
  ]
  node [
    id 92
    label "profanum"
  ]
  node [
    id 93
    label "mikrokosmos"
  ]
  node [
    id 94
    label "nasada"
  ]
  node [
    id 95
    label "duch"
  ]
  node [
    id 96
    label "antropochoria"
  ]
  node [
    id 97
    label "osoba"
  ]
  node [
    id 98
    label "wz&#243;r"
  ]
  node [
    id 99
    label "senior"
  ]
  node [
    id 100
    label "oddzia&#322;ywanie"
  ]
  node [
    id 101
    label "Adam"
  ]
  node [
    id 102
    label "homo_sapiens"
  ]
  node [
    id 103
    label "polifag"
  ]
  node [
    id 104
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 105
    label "sfera"
  ]
  node [
    id 106
    label "zbi&#243;r"
  ]
  node [
    id 107
    label "zakres"
  ]
  node [
    id 108
    label "funkcja"
  ]
  node [
    id 109
    label "bezdro&#380;e"
  ]
  node [
    id 110
    label "poddzia&#322;"
  ]
  node [
    id 111
    label "egzemplarz"
  ]
  node [
    id 112
    label "series"
  ]
  node [
    id 113
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 114
    label "uprawianie"
  ]
  node [
    id 115
    label "praca_rolnicza"
  ]
  node [
    id 116
    label "collection"
  ]
  node [
    id 117
    label "dane"
  ]
  node [
    id 118
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 119
    label "pakiet_klimatyczny"
  ]
  node [
    id 120
    label "poj&#281;cie"
  ]
  node [
    id 121
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 122
    label "sum"
  ]
  node [
    id 123
    label "gathering"
  ]
  node [
    id 124
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 125
    label "album"
  ]
  node [
    id 126
    label "wymiar"
  ]
  node [
    id 127
    label "strefa"
  ]
  node [
    id 128
    label "kula"
  ]
  node [
    id 129
    label "class"
  ]
  node [
    id 130
    label "sector"
  ]
  node [
    id 131
    label "przestrze&#324;"
  ]
  node [
    id 132
    label "p&#243;&#322;kula"
  ]
  node [
    id 133
    label "huczek"
  ]
  node [
    id 134
    label "p&#243;&#322;sfera"
  ]
  node [
    id 135
    label "powierzchnia"
  ]
  node [
    id 136
    label "kolur"
  ]
  node [
    id 137
    label "grupa"
  ]
  node [
    id 138
    label "czyn"
  ]
  node [
    id 139
    label "supremum"
  ]
  node [
    id 140
    label "addytywno&#347;&#263;"
  ]
  node [
    id 141
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 142
    label "jednostka"
  ]
  node [
    id 143
    label "function"
  ]
  node [
    id 144
    label "zastosowanie"
  ]
  node [
    id 145
    label "matematyka"
  ]
  node [
    id 146
    label "funkcjonowanie"
  ]
  node [
    id 147
    label "praca"
  ]
  node [
    id 148
    label "rzut"
  ]
  node [
    id 149
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 150
    label "powierzanie"
  ]
  node [
    id 151
    label "cel"
  ]
  node [
    id 152
    label "przeciwdziedzina"
  ]
  node [
    id 153
    label "awansowa&#263;"
  ]
  node [
    id 154
    label "stawia&#263;"
  ]
  node [
    id 155
    label "wakowa&#263;"
  ]
  node [
    id 156
    label "znaczenie"
  ]
  node [
    id 157
    label "postawi&#263;"
  ]
  node [
    id 158
    label "awansowanie"
  ]
  node [
    id 159
    label "infimum"
  ]
  node [
    id 160
    label "sytuacja"
  ]
  node [
    id 161
    label "obszar"
  ]
  node [
    id 162
    label "wilderness"
  ]
  node [
    id 163
    label "granica"
  ]
  node [
    id 164
    label "wielko&#347;&#263;"
  ]
  node [
    id 165
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 166
    label "podzakres"
  ]
  node [
    id 167
    label "desygnat"
  ]
  node [
    id 168
    label "circle"
  ]
  node [
    id 169
    label "balneologia"
  ]
  node [
    id 170
    label "serologia"
  ]
  node [
    id 171
    label "proktologia"
  ]
  node [
    id 172
    label "symptomatologia"
  ]
  node [
    id 173
    label "neurologia"
  ]
  node [
    id 174
    label "radiologia"
  ]
  node [
    id 175
    label "chirurgia"
  ]
  node [
    id 176
    label "medycyna_ratunkowa"
  ]
  node [
    id 177
    label "okulistyka"
  ]
  node [
    id 178
    label "transfuzjologia"
  ]
  node [
    id 179
    label "hemodynamika"
  ]
  node [
    id 180
    label "psychosomatyka"
  ]
  node [
    id 181
    label "medycyna_weterynaryjna"
  ]
  node [
    id 182
    label "medycyna_zapobiegawcza"
  ]
  node [
    id 183
    label "medycyna_kosmiczna"
  ]
  node [
    id 184
    label "reumatologia"
  ]
  node [
    id 185
    label "gerontologia"
  ]
  node [
    id 186
    label "geriatria"
  ]
  node [
    id 187
    label "stomatologia"
  ]
  node [
    id 188
    label "biomedycyna"
  ]
  node [
    id 189
    label "ginekologia"
  ]
  node [
    id 190
    label "epidemiologia"
  ]
  node [
    id 191
    label "andrologia"
  ]
  node [
    id 192
    label "toksykologia"
  ]
  node [
    id 193
    label "hipertensjologia"
  ]
  node [
    id 194
    label "kierunek"
  ]
  node [
    id 195
    label "medycyna_s&#261;dowa"
  ]
  node [
    id 196
    label "endokrynologia"
  ]
  node [
    id 197
    label "medycyna_tropikalna"
  ]
  node [
    id 198
    label "p&#322;yn_fizjologiczny"
  ]
  node [
    id 199
    label "psychiatria"
  ]
  node [
    id 200
    label "psychoonkologia"
  ]
  node [
    id 201
    label "neuropsychiatria"
  ]
  node [
    id 202
    label "podologia"
  ]
  node [
    id 203
    label "polisomnografia"
  ]
  node [
    id 204
    label "nefrologia"
  ]
  node [
    id 205
    label "medycyna_lotnicza"
  ]
  node [
    id 206
    label "elektromedycyna"
  ]
  node [
    id 207
    label "higiena"
  ]
  node [
    id 208
    label "zio&#322;olecznictwo"
  ]
  node [
    id 209
    label "antropotomia"
  ]
  node [
    id 210
    label "osmologia"
  ]
  node [
    id 211
    label "ortopedia"
  ]
  node [
    id 212
    label "diabetologia"
  ]
  node [
    id 213
    label "protetyka"
  ]
  node [
    id 214
    label "medycyna_wewn&#281;trzna"
  ]
  node [
    id 215
    label "hepatologia"
  ]
  node [
    id 216
    label "transplantologia"
  ]
  node [
    id 217
    label "diagnostyka"
  ]
  node [
    id 218
    label "psychofizjologia"
  ]
  node [
    id 219
    label "medycyna_nuklearna"
  ]
  node [
    id 220
    label "torakotomia"
  ]
  node [
    id 221
    label "anestezjologia"
  ]
  node [
    id 222
    label "otorynolaryngologia"
  ]
  node [
    id 223
    label "hematologia"
  ]
  node [
    id 224
    label "laryngologia"
  ]
  node [
    id 225
    label "audiologia"
  ]
  node [
    id 226
    label "pediatria"
  ]
  node [
    id 227
    label "gastroenterologia"
  ]
  node [
    id 228
    label "dermatologia"
  ]
  node [
    id 229
    label "nozologia"
  ]
  node [
    id 230
    label "bariatria"
  ]
  node [
    id 231
    label "onkologia"
  ]
  node [
    id 232
    label "kardiologia"
  ]
  node [
    id 233
    label "urologia"
  ]
  node [
    id 234
    label "wenerologia"
  ]
  node [
    id 235
    label "etiologia"
  ]
  node [
    id 236
    label "seksuologia"
  ]
  node [
    id 237
    label "medycyna_sportowa"
  ]
  node [
    id 238
    label "pulmonologia"
  ]
  node [
    id 239
    label "sztuka_leczenia"
  ]
  node [
    id 240
    label "cytologia"
  ]
  node [
    id 241
    label "elektromiografia"
  ]
  node [
    id 242
    label "alergologia"
  ]
  node [
    id 243
    label "ftyzjologia"
  ]
  node [
    id 244
    label "patologia"
  ]
  node [
    id 245
    label "po&#322;o&#380;nictwo"
  ]
  node [
    id 246
    label "neurofizjologia"
  ]
  node [
    id 247
    label "immunologia"
  ]
  node [
    id 248
    label "przebieg"
  ]
  node [
    id 249
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 250
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 251
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 252
    label "praktyka"
  ]
  node [
    id 253
    label "system"
  ]
  node [
    id 254
    label "przeorientowywanie"
  ]
  node [
    id 255
    label "studia"
  ]
  node [
    id 256
    label "linia"
  ]
  node [
    id 257
    label "bok"
  ]
  node [
    id 258
    label "skr&#281;canie"
  ]
  node [
    id 259
    label "skr&#281;ca&#263;"
  ]
  node [
    id 260
    label "przeorientowywa&#263;"
  ]
  node [
    id 261
    label "orientowanie"
  ]
  node [
    id 262
    label "skr&#281;ci&#263;"
  ]
  node [
    id 263
    label "przeorientowanie"
  ]
  node [
    id 264
    label "zorientowanie"
  ]
  node [
    id 265
    label "przeorientowa&#263;"
  ]
  node [
    id 266
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 267
    label "metoda"
  ]
  node [
    id 268
    label "ty&#322;"
  ]
  node [
    id 269
    label "zorientowa&#263;"
  ]
  node [
    id 270
    label "g&#243;ra"
  ]
  node [
    id 271
    label "orientowa&#263;"
  ]
  node [
    id 272
    label "spos&#243;b"
  ]
  node [
    id 273
    label "ideologia"
  ]
  node [
    id 274
    label "orientacja"
  ]
  node [
    id 275
    label "prz&#243;d"
  ]
  node [
    id 276
    label "bearing"
  ]
  node [
    id 277
    label "skr&#281;cenie"
  ]
  node [
    id 278
    label "oddzia&#322;"
  ]
  node [
    id 279
    label "thoracotomy"
  ]
  node [
    id 280
    label "operacja"
  ]
  node [
    id 281
    label "audiometria"
  ]
  node [
    id 282
    label "herboryzowanie"
  ]
  node [
    id 283
    label "herboryzowa&#263;"
  ]
  node [
    id 284
    label "och&#281;d&#243;stwo"
  ]
  node [
    id 285
    label "kultura_fizyczna"
  ]
  node [
    id 286
    label "porz&#261;dek"
  ]
  node [
    id 287
    label "cecha"
  ]
  node [
    id 288
    label "k&#261;piel_lecznicza"
  ]
  node [
    id 289
    label "balneoterapia"
  ]
  node [
    id 290
    label "dystrakcja"
  ]
  node [
    id 291
    label "zaopatrzenie_ortopedyczne"
  ]
  node [
    id 292
    label "protetyka_ortopedyczna"
  ]
  node [
    id 293
    label "po&#322;amaniec"
  ]
  node [
    id 294
    label "translacja"
  ]
  node [
    id 295
    label "anatomia_topograficzna"
  ]
  node [
    id 296
    label "anatomia_radiologiczna"
  ]
  node [
    id 297
    label "anatomia_opisowa"
  ]
  node [
    id 298
    label "osteologia"
  ]
  node [
    id 299
    label "anatomia"
  ]
  node [
    id 300
    label "morfologia"
  ]
  node [
    id 301
    label "rentgenologia"
  ]
  node [
    id 302
    label "rentgenodiagnostyka"
  ]
  node [
    id 303
    label "artefakt"
  ]
  node [
    id 304
    label "cholangiografia"
  ]
  node [
    id 305
    label "neonatologia"
  ]
  node [
    id 306
    label "nauka"
  ]
  node [
    id 307
    label "gastrologia"
  ]
  node [
    id 308
    label "chirurgia_gastroenterologiczna"
  ]
  node [
    id 309
    label "mi&#281;sie&#324;"
  ]
  node [
    id 310
    label "badanie"
  ]
  node [
    id 311
    label "electromyography"
  ]
  node [
    id 312
    label "pedicure"
  ]
  node [
    id 313
    label "psychiatria_kliniczna"
  ]
  node [
    id 314
    label "psychiatria_biologiczna"
  ]
  node [
    id 315
    label "psychiatria_s&#261;dowa"
  ]
  node [
    id 316
    label "psychiatria_wojskowa"
  ]
  node [
    id 317
    label "&#347;pi&#261;czka_insulinowa"
  ]
  node [
    id 318
    label "psychogeriatria"
  ]
  node [
    id 319
    label "psychiatria_kulturowa"
  ]
  node [
    id 320
    label "cia&#322;o_szkliste"
  ]
  node [
    id 321
    label "keratoskop"
  ]
  node [
    id 322
    label "diafanoskopia"
  ]
  node [
    id 323
    label "oftalmika"
  ]
  node [
    id 324
    label "ortoptyka"
  ]
  node [
    id 325
    label "kosmetologia"
  ]
  node [
    id 326
    label "dermatoza"
  ]
  node [
    id 327
    label "immunochemia"
  ]
  node [
    id 328
    label "immunopatologia"
  ]
  node [
    id 329
    label "immunologia_transplantologiczna"
  ]
  node [
    id 330
    label "mikrobiologia"
  ]
  node [
    id 331
    label "immunologia_kliniczna"
  ]
  node [
    id 332
    label "immunologia_por&#243;wnawcza"
  ]
  node [
    id 333
    label "immunoonkologia"
  ]
  node [
    id 334
    label "immunogenetyka"
  ]
  node [
    id 335
    label "immunohematologia"
  ]
  node [
    id 336
    label "immunofarmakologia"
  ]
  node [
    id 337
    label "immunoprofilaktyka"
  ]
  node [
    id 338
    label "chirurgia_podstawy_czaszki"
  ]
  node [
    id 339
    label "angina_Plauta-Vincenta"
  ]
  node [
    id 340
    label "onkologia_laryngologiczna"
  ]
  node [
    id 341
    label "otochirurgia"
  ]
  node [
    id 342
    label "otologia"
  ]
  node [
    id 343
    label "foniatria"
  ]
  node [
    id 344
    label "walsartan"
  ]
  node [
    id 345
    label "gabinet_stomatologiczny"
  ]
  node [
    id 346
    label "ortodoncja"
  ]
  node [
    id 347
    label "chirurgia_szcz&#281;kowo-twarzowa"
  ]
  node [
    id 348
    label "odontologia"
  ]
  node [
    id 349
    label "stomatologia_zachowawcza"
  ]
  node [
    id 350
    label "chirurgia_stomatologiczna"
  ]
  node [
    id 351
    label "stomatologia_dzieci&#281;ca"
  ]
  node [
    id 352
    label "stomatologia_estetyczna"
  ]
  node [
    id 353
    label "kiesze&#324;"
  ]
  node [
    id 354
    label "periodontologia"
  ]
  node [
    id 355
    label "chirurgia_urazowa"
  ]
  node [
    id 356
    label "torakochirurgia"
  ]
  node [
    id 357
    label "szew"
  ]
  node [
    id 358
    label "kardiochirurgia"
  ]
  node [
    id 359
    label "chirurgia_dzieci&#281;ca"
  ]
  node [
    id 360
    label "bezoperacyjny"
  ]
  node [
    id 361
    label "mikrochirurgia"
  ]
  node [
    id 362
    label "chirurgia_plastyczna"
  ]
  node [
    id 363
    label "neurochirurgia"
  ]
  node [
    id 364
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 365
    label "urodynamika"
  ]
  node [
    id 366
    label "weterynaria"
  ]
  node [
    id 367
    label "surowica"
  ]
  node [
    id 368
    label "ognisko"
  ]
  node [
    id 369
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 370
    label "powalenie"
  ]
  node [
    id 371
    label "odezwanie_si&#281;"
  ]
  node [
    id 372
    label "atakowanie"
  ]
  node [
    id 373
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 374
    label "patomorfologia"
  ]
  node [
    id 375
    label "grupa_ryzyka"
  ]
  node [
    id 376
    label "przypadek"
  ]
  node [
    id 377
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 378
    label "patolnia"
  ]
  node [
    id 379
    label "nabawienie_si&#281;"
  ]
  node [
    id 380
    label "przemoc"
  ]
  node [
    id 381
    label "&#347;rodowisko"
  ]
  node [
    id 382
    label "inkubacja"
  ]
  node [
    id 383
    label "szambo"
  ]
  node [
    id 384
    label "gangsterski"
  ]
  node [
    id 385
    label "fizjologia_patologiczna"
  ]
  node [
    id 386
    label "kryzys"
  ]
  node [
    id 387
    label "powali&#263;"
  ]
  node [
    id 388
    label "remisja"
  ]
  node [
    id 389
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 390
    label "zajmowa&#263;"
  ]
  node [
    id 391
    label "zaburzenie"
  ]
  node [
    id 392
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 393
    label "neuropatologia"
  ]
  node [
    id 394
    label "aspo&#322;eczny"
  ]
  node [
    id 395
    label "badanie_histopatologiczne"
  ]
  node [
    id 396
    label "abnormality"
  ]
  node [
    id 397
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 398
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 399
    label "patogeneza"
  ]
  node [
    id 400
    label "psychopatologia"
  ]
  node [
    id 401
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 402
    label "paleopatologia"
  ]
  node [
    id 403
    label "logopatologia"
  ]
  node [
    id 404
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 405
    label "osteopatologia"
  ]
  node [
    id 406
    label "odzywanie_si&#281;"
  ]
  node [
    id 407
    label "diagnoza"
  ]
  node [
    id 408
    label "atakowa&#263;"
  ]
  node [
    id 409
    label "histopatologia"
  ]
  node [
    id 410
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 411
    label "nabawianie_si&#281;"
  ]
  node [
    id 412
    label "underworld"
  ]
  node [
    id 413
    label "meteoropatologia"
  ]
  node [
    id 414
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 415
    label "zajmowanie"
  ]
  node [
    id 416
    label "neuropsychiatria_dzieci&#281;ca"
  ]
  node [
    id 417
    label "cytodiagnostyka"
  ]
  node [
    id 418
    label "kariologia"
  ]
  node [
    id 419
    label "cytogenetyka"
  ]
  node [
    id 420
    label "diagnosis"
  ]
  node [
    id 421
    label "kontrola"
  ]
  node [
    id 422
    label "anamneza"
  ]
  node [
    id 423
    label "biomarker"
  ]
  node [
    id 424
    label "neurolingwistyka"
  ]
  node [
    id 425
    label "neuropsychologia"
  ]
  node [
    id 426
    label "neurogeriatria"
  ]
  node [
    id 427
    label "epileptologia"
  ]
  node [
    id 428
    label "neurobiologia"
  ]
  node [
    id 429
    label "neurochemia"
  ]
  node [
    id 430
    label "etiopatogeneza"
  ]
  node [
    id 431
    label "przyczyna"
  ]
  node [
    id 432
    label "obja&#347;nienie"
  ]
  node [
    id 433
    label "neurofizjologia_kliniczna"
  ]
  node [
    id 434
    label "medycyna_regeneracyjna"
  ]
  node [
    id 435
    label "biogerontologia"
  ]
  node [
    id 436
    label "choroba_endokrynologiczna"
  ]
  node [
    id 437
    label "perinatologia"
  ]
  node [
    id 438
    label "wr&#243;&#380;ny"
  ]
  node [
    id 439
    label "nadprzyrodzony"
  ]
  node [
    id 440
    label "proroczo"
  ]
  node [
    id 441
    label "pozaziemski"
  ]
  node [
    id 442
    label "nadnaturalnie"
  ]
  node [
    id 443
    label "proroczy"
  ]
  node [
    id 444
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 445
    label "osta&#263;_si&#281;"
  ]
  node [
    id 446
    label "catch"
  ]
  node [
    id 447
    label "support"
  ]
  node [
    id 448
    label "prze&#380;y&#263;"
  ]
  node [
    id 449
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 450
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 451
    label "proceed"
  ]
  node [
    id 452
    label "poradzi&#263;_sobie"
  ]
  node [
    id 453
    label "visualize"
  ]
  node [
    id 454
    label "dozna&#263;"
  ]
  node [
    id 455
    label "wytrzyma&#263;"
  ]
  node [
    id 456
    label "przej&#347;&#263;"
  ]
  node [
    id 457
    label "see"
  ]
  node [
    id 458
    label "wykonawca"
  ]
  node [
    id 459
    label "interpretator"
  ]
  node [
    id 460
    label "wra&#380;enie"
  ]
  node [
    id 461
    label "przej&#347;cie"
  ]
  node [
    id 462
    label "doznanie"
  ]
  node [
    id 463
    label "poradzenie_sobie"
  ]
  node [
    id 464
    label "przetrwanie"
  ]
  node [
    id 465
    label "survival"
  ]
  node [
    id 466
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 467
    label "wy&#347;wiadczenie"
  ]
  node [
    id 468
    label "zmys&#322;"
  ]
  node [
    id 469
    label "spotkanie"
  ]
  node [
    id 470
    label "czucie"
  ]
  node [
    id 471
    label "przeczulica"
  ]
  node [
    id 472
    label "poczucie"
  ]
  node [
    id 473
    label "&#380;ycie"
  ]
  node [
    id 474
    label "pozostanie"
  ]
  node [
    id 475
    label "experience"
  ]
  node [
    id 476
    label "odczucia"
  ]
  node [
    id 477
    label "proces"
  ]
  node [
    id 478
    label "zjawisko"
  ]
  node [
    id 479
    label "reakcja"
  ]
  node [
    id 480
    label "mini&#281;cie"
  ]
  node [
    id 481
    label "ustawa"
  ]
  node [
    id 482
    label "wymienienie"
  ]
  node [
    id 483
    label "zaliczenie"
  ]
  node [
    id 484
    label "traversal"
  ]
  node [
    id 485
    label "zdarzenie_si&#281;"
  ]
  node [
    id 486
    label "przewy&#380;szenie"
  ]
  node [
    id 487
    label "przepuszczenie"
  ]
  node [
    id 488
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 489
    label "strain"
  ]
  node [
    id 490
    label "faza"
  ]
  node [
    id 491
    label "przerobienie"
  ]
  node [
    id 492
    label "wydeptywanie"
  ]
  node [
    id 493
    label "miejsce"
  ]
  node [
    id 494
    label "crack"
  ]
  node [
    id 495
    label "wydeptanie"
  ]
  node [
    id 496
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 497
    label "wstawka"
  ]
  node [
    id 498
    label "uznanie"
  ]
  node [
    id 499
    label "dostanie_si&#281;"
  ]
  node [
    id 500
    label "trwanie"
  ]
  node [
    id 501
    label "przebycie"
  ]
  node [
    id 502
    label "wytyczenie"
  ]
  node [
    id 503
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 504
    label "przepojenie"
  ]
  node [
    id 505
    label "nas&#261;czenie"
  ]
  node [
    id 506
    label "nale&#380;enie"
  ]
  node [
    id 507
    label "mienie"
  ]
  node [
    id 508
    label "odmienienie"
  ]
  node [
    id 509
    label "przedostanie_si&#281;"
  ]
  node [
    id 510
    label "przemokni&#281;cie"
  ]
  node [
    id 511
    label "nasycenie_si&#281;"
  ]
  node [
    id 512
    label "zacz&#281;cie"
  ]
  node [
    id 513
    label "stanie_si&#281;"
  ]
  node [
    id 514
    label "offense"
  ]
  node [
    id 515
    label "przestanie"
  ]
  node [
    id 516
    label "sport"
  ]
  node [
    id 517
    label "szkolenie"
  ]
  node [
    id 518
    label "czas"
  ]
  node [
    id 519
    label "poprzedzanie"
  ]
  node [
    id 520
    label "czasoprzestrze&#324;"
  ]
  node [
    id 521
    label "laba"
  ]
  node [
    id 522
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 523
    label "chronometria"
  ]
  node [
    id 524
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 525
    label "rachuba_czasu"
  ]
  node [
    id 526
    label "przep&#322;ywanie"
  ]
  node [
    id 527
    label "czasokres"
  ]
  node [
    id 528
    label "odczyt"
  ]
  node [
    id 529
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 530
    label "dzieje"
  ]
  node [
    id 531
    label "kategoria_gramatyczna"
  ]
  node [
    id 532
    label "poprzedzenie"
  ]
  node [
    id 533
    label "trawienie"
  ]
  node [
    id 534
    label "pochodzi&#263;"
  ]
  node [
    id 535
    label "period"
  ]
  node [
    id 536
    label "okres_czasu"
  ]
  node [
    id 537
    label "poprzedza&#263;"
  ]
  node [
    id 538
    label "schy&#322;ek"
  ]
  node [
    id 539
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 540
    label "odwlekanie_si&#281;"
  ]
  node [
    id 541
    label "zegar"
  ]
  node [
    id 542
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 543
    label "czwarty_wymiar"
  ]
  node [
    id 544
    label "pochodzenie"
  ]
  node [
    id 545
    label "koniugacja"
  ]
  node [
    id 546
    label "Zeitgeist"
  ]
  node [
    id 547
    label "trawi&#263;"
  ]
  node [
    id 548
    label "pogoda"
  ]
  node [
    id 549
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 550
    label "poprzedzi&#263;"
  ]
  node [
    id 551
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 552
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 553
    label "time_period"
  ]
  node [
    id 554
    label "tydzie&#324;"
  ]
  node [
    id 555
    label "miech"
  ]
  node [
    id 556
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 557
    label "rok"
  ]
  node [
    id 558
    label "kalendy"
  ]
  node [
    id 559
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 560
    label "satelita"
  ]
  node [
    id 561
    label "peryselenium"
  ]
  node [
    id 562
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 563
    label "&#347;wiat&#322;o"
  ]
  node [
    id 564
    label "aposelenium"
  ]
  node [
    id 565
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 566
    label "Tytan"
  ]
  node [
    id 567
    label "moon"
  ]
  node [
    id 568
    label "aparat_fotograficzny"
  ]
  node [
    id 569
    label "bag"
  ]
  node [
    id 570
    label "sakwa"
  ]
  node [
    id 571
    label "torba"
  ]
  node [
    id 572
    label "przyrz&#261;d"
  ]
  node [
    id 573
    label "w&#243;r"
  ]
  node [
    id 574
    label "doba"
  ]
  node [
    id 575
    label "weekend"
  ]
  node [
    id 576
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 577
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 578
    label "p&#243;&#322;rocze"
  ]
  node [
    id 579
    label "martwy_sezon"
  ]
  node [
    id 580
    label "kalendarz"
  ]
  node [
    id 581
    label "cykl_astronomiczny"
  ]
  node [
    id 582
    label "lata"
  ]
  node [
    id 583
    label "pora_roku"
  ]
  node [
    id 584
    label "stulecie"
  ]
  node [
    id 585
    label "kurs"
  ]
  node [
    id 586
    label "jubileusz"
  ]
  node [
    id 587
    label "kwarta&#322;"
  ]
  node [
    id 588
    label "lock"
  ]
  node [
    id 589
    label "absolut"
  ]
  node [
    id 590
    label "integer"
  ]
  node [
    id 591
    label "liczba"
  ]
  node [
    id 592
    label "zlewanie_si&#281;"
  ]
  node [
    id 593
    label "ilo&#347;&#263;"
  ]
  node [
    id 594
    label "uk&#322;ad"
  ]
  node [
    id 595
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 596
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 597
    label "pe&#322;ny"
  ]
  node [
    id 598
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 599
    label "olejek_eteryczny"
  ]
  node [
    id 600
    label "byt"
  ]
  node [
    id 601
    label "czczo"
  ]
  node [
    id 602
    label "ja&#322;owy"
  ]
  node [
    id 603
    label "lekki"
  ]
  node [
    id 604
    label "banalny"
  ]
  node [
    id 605
    label "istny"
  ]
  node [
    id 606
    label "bezwarto&#347;ciowy"
  ]
  node [
    id 607
    label "pusty"
  ]
  node [
    id 608
    label "g&#322;odny"
  ]
  node [
    id 609
    label "czysty"
  ]
  node [
    id 610
    label "ubogi"
  ]
  node [
    id 611
    label "nieciekawy"
  ]
  node [
    id 612
    label "bankrutowanie"
  ]
  node [
    id 613
    label "ubo&#380;enie"
  ]
  node [
    id 614
    label "prosty"
  ]
  node [
    id 615
    label "go&#322;odupiec"
  ]
  node [
    id 616
    label "biedny"
  ]
  node [
    id 617
    label "zubo&#380;enie"
  ]
  node [
    id 618
    label "raw_material"
  ]
  node [
    id 619
    label "zubo&#380;anie"
  ]
  node [
    id 620
    label "ho&#322;ysz"
  ]
  node [
    id 621
    label "zbiednienie"
  ]
  node [
    id 622
    label "proletariusz"
  ]
  node [
    id 623
    label "sytuowany"
  ]
  node [
    id 624
    label "biedota"
  ]
  node [
    id 625
    label "ubogo"
  ]
  node [
    id 626
    label "biednie"
  ]
  node [
    id 627
    label "&#322;atwo"
  ]
  node [
    id 628
    label "delikatny"
  ]
  node [
    id 629
    label "przewiewny"
  ]
  node [
    id 630
    label "nieg&#322;&#281;boko"
  ]
  node [
    id 631
    label "subtelny"
  ]
  node [
    id 632
    label "bezpieczny"
  ]
  node [
    id 633
    label "lekko"
  ]
  node [
    id 634
    label "polotny"
  ]
  node [
    id 635
    label "przyswajalny"
  ]
  node [
    id 636
    label "beztrosko"
  ]
  node [
    id 637
    label "&#322;atwy"
  ]
  node [
    id 638
    label "piaszczysty"
  ]
  node [
    id 639
    label "suchy"
  ]
  node [
    id 640
    label "letki"
  ]
  node [
    id 641
    label "p&#322;ynny"
  ]
  node [
    id 642
    label "dietetyczny"
  ]
  node [
    id 643
    label "lekkozbrojny"
  ]
  node [
    id 644
    label "delikatnie"
  ]
  node [
    id 645
    label "s&#322;aby"
  ]
  node [
    id 646
    label "&#322;acny"
  ]
  node [
    id 647
    label "snadny"
  ]
  node [
    id 648
    label "nierozwa&#380;ny"
  ]
  node [
    id 649
    label "g&#322;adko"
  ]
  node [
    id 650
    label "zgrabny"
  ]
  node [
    id 651
    label "przyjemny"
  ]
  node [
    id 652
    label "zwinnie"
  ]
  node [
    id 653
    label "beztroskliwy"
  ]
  node [
    id 654
    label "zr&#281;czny"
  ]
  node [
    id 655
    label "cienki"
  ]
  node [
    id 656
    label "nieznacznie"
  ]
  node [
    id 657
    label "pewny"
  ]
  node [
    id 658
    label "przezroczy&#347;cie"
  ]
  node [
    id 659
    label "nieemisyjny"
  ]
  node [
    id 660
    label "bezb&#322;&#281;dny"
  ]
  node [
    id 661
    label "kompletny"
  ]
  node [
    id 662
    label "umycie"
  ]
  node [
    id 663
    label "ekologiczny"
  ]
  node [
    id 664
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 665
    label "dobry"
  ]
  node [
    id 666
    label "dopuszczalny"
  ]
  node [
    id 667
    label "ca&#322;y"
  ]
  node [
    id 668
    label "mycie"
  ]
  node [
    id 669
    label "jednolity"
  ]
  node [
    id 670
    label "udany"
  ]
  node [
    id 671
    label "czysto"
  ]
  node [
    id 672
    label "klarowanie"
  ]
  node [
    id 673
    label "bezchmurny"
  ]
  node [
    id 674
    label "ostry"
  ]
  node [
    id 675
    label "legalny"
  ]
  node [
    id 676
    label "wyczyszczenie_si&#281;"
  ]
  node [
    id 677
    label "prze&#378;roczy"
  ]
  node [
    id 678
    label "wolny"
  ]
  node [
    id 679
    label "czyszczenie_si&#281;"
  ]
  node [
    id 680
    label "uczciwy"
  ]
  node [
    id 681
    label "do_czysta"
  ]
  node [
    id 682
    label "klarowanie_si&#281;"
  ]
  node [
    id 683
    label "sklarowanie"
  ]
  node [
    id 684
    label "zdrowy"
  ]
  node [
    id 685
    label "prawdziwy"
  ]
  node [
    id 686
    label "klarowny"
  ]
  node [
    id 687
    label "cnotliwy"
  ]
  node [
    id 688
    label "ewidentny"
  ]
  node [
    id 689
    label "wspinaczka"
  ]
  node [
    id 690
    label "porz&#261;dny"
  ]
  node [
    id 691
    label "schludny"
  ]
  node [
    id 692
    label "doskona&#322;y"
  ]
  node [
    id 693
    label "orze&#378;wiaj&#261;cy"
  ]
  node [
    id 694
    label "nieodrodny"
  ]
  node [
    id 695
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 696
    label "istnie"
  ]
  node [
    id 697
    label "zbanalizowanie_si&#281;"
  ]
  node [
    id 698
    label "&#322;atwiutko"
  ]
  node [
    id 699
    label "b&#322;aho"
  ]
  node [
    id 700
    label "strywializowanie"
  ]
  node [
    id 701
    label "trywializowanie"
  ]
  node [
    id 702
    label "niepoczesny"
  ]
  node [
    id 703
    label "banalnie"
  ]
  node [
    id 704
    label "duperelny"
  ]
  node [
    id 705
    label "g&#322;upi"
  ]
  node [
    id 706
    label "banalnienie"
  ]
  node [
    id 707
    label "pospolity"
  ]
  node [
    id 708
    label "wyja&#322;owienie"
  ]
  node [
    id 709
    label "p&#322;onny"
  ]
  node [
    id 710
    label "ja&#322;owo"
  ]
  node [
    id 711
    label "wyja&#322;awianie"
  ]
  node [
    id 712
    label "nieurodzajnie"
  ]
  node [
    id 713
    label "nijaki"
  ]
  node [
    id 714
    label "nieatrakcyjny"
  ]
  node [
    id 715
    label "niezabawny"
  ]
  node [
    id 716
    label "nieciekawie"
  ]
  node [
    id 717
    label "zwyczajny"
  ]
  node [
    id 718
    label "oboj&#281;tny"
  ]
  node [
    id 719
    label "poszarzenie"
  ]
  node [
    id 720
    label "szarzenie"
  ]
  node [
    id 721
    label "bezbarwnie"
  ]
  node [
    id 722
    label "z&#322;y"
  ]
  node [
    id 723
    label "bezideowy"
  ]
  node [
    id 724
    label "zepsuty"
  ]
  node [
    id 725
    label "niewa&#380;ny"
  ]
  node [
    id 726
    label "bezproduktywny"
  ]
  node [
    id 727
    label "nadaremny"
  ]
  node [
    id 728
    label "niekonstruktywny"
  ]
  node [
    id 729
    label "wysychanie"
  ]
  node [
    id 730
    label "zoboj&#281;tnia&#322;y"
  ]
  node [
    id 731
    label "wyschni&#281;cie"
  ]
  node [
    id 732
    label "opr&#243;&#380;nianie_si&#281;"
  ]
  node [
    id 733
    label "bezmy&#347;lny"
  ]
  node [
    id 734
    label "pusto"
  ]
  node [
    id 735
    label "opr&#243;&#380;nienie_si&#281;"
  ]
  node [
    id 736
    label "niezaspokojony"
  ]
  node [
    id 737
    label "tani"
  ]
  node [
    id 738
    label "ch&#281;tny"
  ]
  node [
    id 739
    label "g&#322;odno"
  ]
  node [
    id 740
    label "zg&#322;odnienie"
  ]
  node [
    id 741
    label "z&#322;akniony"
  ]
  node [
    id 742
    label "g&#322;odnie"
  ]
  node [
    id 743
    label "g&#322;odnienie"
  ]
  node [
    id 744
    label "na_czczo"
  ]
  node [
    id 745
    label "&#378;le"
  ]
  node [
    id 746
    label "przest&#281;pstwo"
  ]
  node [
    id 747
    label "manipulacja"
  ]
  node [
    id 748
    label "domys&#322;"
  ]
  node [
    id 749
    label "transakcja"
  ]
  node [
    id 750
    label "dywagacja"
  ]
  node [
    id 751
    label "adventure"
  ]
  node [
    id 752
    label "wytw&#243;r"
  ]
  node [
    id 753
    label "rozwa&#380;anie"
  ]
  node [
    id 754
    label "czynno&#347;&#263;"
  ]
  node [
    id 755
    label "podst&#281;p"
  ]
  node [
    id 756
    label "brudny"
  ]
  node [
    id 757
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 758
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 759
    label "crime"
  ]
  node [
    id 760
    label "sprawstwo"
  ]
  node [
    id 761
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 762
    label "zam&#243;wienie"
  ]
  node [
    id 763
    label "cena_transferowa"
  ]
  node [
    id 764
    label "arbitra&#380;"
  ]
  node [
    id 765
    label "kontrakt_terminowy"
  ]
  node [
    id 766
    label "facjenda"
  ]
  node [
    id 767
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 768
    label "react"
  ]
  node [
    id 769
    label "answer"
  ]
  node [
    id 770
    label "odpowiada&#263;"
  ]
  node [
    id 771
    label "uczestniczy&#263;"
  ]
  node [
    id 772
    label "dawa&#263;"
  ]
  node [
    id 773
    label "ponosi&#263;"
  ]
  node [
    id 774
    label "report"
  ]
  node [
    id 775
    label "pytanie"
  ]
  node [
    id 776
    label "equate"
  ]
  node [
    id 777
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 778
    label "powodowa&#263;"
  ]
  node [
    id 779
    label "tone"
  ]
  node [
    id 780
    label "contend"
  ]
  node [
    id 781
    label "impart"
  ]
  node [
    id 782
    label "participate"
  ]
  node [
    id 783
    label "robi&#263;"
  ]
  node [
    id 784
    label "mina"
  ]
  node [
    id 785
    label "smile"
  ]
  node [
    id 786
    label "zachowanie"
  ]
  node [
    id 787
    label "reaction"
  ]
  node [
    id 788
    label "organizm"
  ]
  node [
    id 789
    label "rozmowa"
  ]
  node [
    id 790
    label "response"
  ]
  node [
    id 791
    label "rezultat"
  ]
  node [
    id 792
    label "respondent"
  ]
  node [
    id 793
    label "ka&#322;"
  ]
  node [
    id 794
    label "wyraz_twarzy"
  ]
  node [
    id 795
    label "air"
  ]
  node [
    id 796
    label "korytarz"
  ]
  node [
    id 797
    label "zacinanie"
  ]
  node [
    id 798
    label "klipa"
  ]
  node [
    id 799
    label "zacina&#263;"
  ]
  node [
    id 800
    label "nab&#243;j"
  ]
  node [
    id 801
    label "pies_przeciwpancerny"
  ]
  node [
    id 802
    label "zaci&#281;cie"
  ]
  node [
    id 803
    label "sanction"
  ]
  node [
    id 804
    label "niespodzianka"
  ]
  node [
    id 805
    label "zapalnik"
  ]
  node [
    id 806
    label "pole_minowe"
  ]
  node [
    id 807
    label "kopalnia"
  ]
  node [
    id 808
    label "go_steady"
  ]
  node [
    id 809
    label "pogl&#261;da&#263;"
  ]
  node [
    id 810
    label "patrze&#263;"
  ]
  node [
    id 811
    label "spoziera&#263;"
  ]
  node [
    id 812
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 813
    label "punkt_widzenia"
  ]
  node [
    id 814
    label "koso"
  ]
  node [
    id 815
    label "dba&#263;"
  ]
  node [
    id 816
    label "szuka&#263;"
  ]
  node [
    id 817
    label "uwa&#380;a&#263;"
  ]
  node [
    id 818
    label "look"
  ]
  node [
    id 819
    label "os&#261;dza&#263;"
  ]
  node [
    id 820
    label "spojrze&#263;"
  ]
  node [
    id 821
    label "jutro"
  ]
  node [
    id 822
    label "blisko"
  ]
  node [
    id 823
    label "dzie&#324;_jutrzejszy"
  ]
  node [
    id 824
    label "dzie&#324;"
  ]
  node [
    id 825
    label "jutrzejszy"
  ]
  node [
    id 826
    label "punkt"
  ]
  node [
    id 827
    label "thing"
  ]
  node [
    id 828
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 829
    label "rzecz"
  ]
  node [
    id 830
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 831
    label "krzew"
  ]
  node [
    id 832
    label "delfinidyna"
  ]
  node [
    id 833
    label "pi&#380;maczkowate"
  ]
  node [
    id 834
    label "ki&#347;&#263;"
  ]
  node [
    id 835
    label "hy&#263;ka"
  ]
  node [
    id 836
    label "pestkowiec"
  ]
  node [
    id 837
    label "kwiat"
  ]
  node [
    id 838
    label "ro&#347;lina"
  ]
  node [
    id 839
    label "owoc"
  ]
  node [
    id 840
    label "oliwkowate"
  ]
  node [
    id 841
    label "lilac"
  ]
  node [
    id 842
    label "kostka"
  ]
  node [
    id 843
    label "kita"
  ]
  node [
    id 844
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 845
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 846
    label "d&#322;o&#324;"
  ]
  node [
    id 847
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 848
    label "powerball"
  ]
  node [
    id 849
    label "&#380;ubr"
  ]
  node [
    id 850
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 851
    label "p&#281;k"
  ]
  node [
    id 852
    label "r&#281;ka"
  ]
  node [
    id 853
    label "ogon"
  ]
  node [
    id 854
    label "zako&#324;czenie"
  ]
  node [
    id 855
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 856
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 857
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 858
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 859
    label "flakon"
  ]
  node [
    id 860
    label "przykoronek"
  ]
  node [
    id 861
    label "kielich"
  ]
  node [
    id 862
    label "dno_kwiatowe"
  ]
  node [
    id 863
    label "organ_ro&#347;linny"
  ]
  node [
    id 864
    label "warga"
  ]
  node [
    id 865
    label "korona"
  ]
  node [
    id 866
    label "rurka"
  ]
  node [
    id 867
    label "ozdoba"
  ]
  node [
    id 868
    label "&#322;yko"
  ]
  node [
    id 869
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 870
    label "karczowa&#263;"
  ]
  node [
    id 871
    label "wykarczowanie"
  ]
  node [
    id 872
    label "skupina"
  ]
  node [
    id 873
    label "wykarczowa&#263;"
  ]
  node [
    id 874
    label "karczowanie"
  ]
  node [
    id 875
    label "fanerofit"
  ]
  node [
    id 876
    label "zbiorowisko"
  ]
  node [
    id 877
    label "ro&#347;liny"
  ]
  node [
    id 878
    label "p&#281;d"
  ]
  node [
    id 879
    label "wegetowanie"
  ]
  node [
    id 880
    label "zadziorek"
  ]
  node [
    id 881
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 882
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 883
    label "do&#322;owa&#263;"
  ]
  node [
    id 884
    label "wegetacja"
  ]
  node [
    id 885
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 886
    label "strzyc"
  ]
  node [
    id 887
    label "w&#322;&#243;kno"
  ]
  node [
    id 888
    label "g&#322;uszenie"
  ]
  node [
    id 889
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 890
    label "fitotron"
  ]
  node [
    id 891
    label "bulwka"
  ]
  node [
    id 892
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 893
    label "odn&#243;&#380;ka"
  ]
  node [
    id 894
    label "epiderma"
  ]
  node [
    id 895
    label "gumoza"
  ]
  node [
    id 896
    label "strzy&#380;enie"
  ]
  node [
    id 897
    label "wypotnik"
  ]
  node [
    id 898
    label "flawonoid"
  ]
  node [
    id 899
    label "wyro&#347;le"
  ]
  node [
    id 900
    label "do&#322;owanie"
  ]
  node [
    id 901
    label "g&#322;uszy&#263;"
  ]
  node [
    id 902
    label "pora&#380;a&#263;"
  ]
  node [
    id 903
    label "fitocenoza"
  ]
  node [
    id 904
    label "hodowla"
  ]
  node [
    id 905
    label "fotoautotrof"
  ]
  node [
    id 906
    label "nieuleczalnie_chory"
  ]
  node [
    id 907
    label "wegetowa&#263;"
  ]
  node [
    id 908
    label "pochewka"
  ]
  node [
    id 909
    label "sok"
  ]
  node [
    id 910
    label "system_korzeniowy"
  ]
  node [
    id 911
    label "zawi&#261;zek"
  ]
  node [
    id 912
    label "pestka"
  ]
  node [
    id 913
    label "mi&#261;&#380;sz"
  ]
  node [
    id 914
    label "frukt"
  ]
  node [
    id 915
    label "drylowanie"
  ]
  node [
    id 916
    label "produkt"
  ]
  node [
    id 917
    label "owocnia"
  ]
  node [
    id 918
    label "fruktoza"
  ]
  node [
    id 919
    label "obiekt"
  ]
  node [
    id 920
    label "gniazdo_nasienne"
  ]
  node [
    id 921
    label "glukoza"
  ]
  node [
    id 922
    label "antocyjanidyn"
  ]
  node [
    id 923
    label "szczeciowce"
  ]
  node [
    id 924
    label "jasnotowce"
  ]
  node [
    id 925
    label "Oleaceae"
  ]
  node [
    id 926
    label "wielkopolski"
  ]
  node [
    id 927
    label "bez_czarny"
  ]
  node [
    id 928
    label "buoyancy"
  ]
  node [
    id 929
    label "postawa"
  ]
  node [
    id 930
    label "nastawienie"
  ]
  node [
    id 931
    label "pozycja"
  ]
  node [
    id 932
    label "attitude"
  ]
  node [
    id 933
    label "piwo"
  ]
  node [
    id 934
    label "uwarzenie"
  ]
  node [
    id 935
    label "warzenie"
  ]
  node [
    id 936
    label "alkohol"
  ]
  node [
    id 937
    label "nap&#243;j"
  ]
  node [
    id 938
    label "bacik"
  ]
  node [
    id 939
    label "wyj&#347;cie"
  ]
  node [
    id 940
    label "uwarzy&#263;"
  ]
  node [
    id 941
    label "birofilia"
  ]
  node [
    id 942
    label "warzy&#263;"
  ]
  node [
    id 943
    label "nawarzy&#263;"
  ]
  node [
    id 944
    label "browarnia"
  ]
  node [
    id 945
    label "nawarzenie"
  ]
  node [
    id 946
    label "anta&#322;"
  ]
  node [
    id 947
    label "spirit"
  ]
  node [
    id 948
    label "emocja"
  ]
  node [
    id 949
    label "zjawa"
  ]
  node [
    id 950
    label "straszyd&#322;o"
  ]
  node [
    id 951
    label "zastraszanie"
  ]
  node [
    id 952
    label "phobia"
  ]
  node [
    id 953
    label "zastraszenie"
  ]
  node [
    id 954
    label "akatyzja"
  ]
  node [
    id 955
    label "ba&#263;_si&#281;"
  ]
  node [
    id 956
    label "zesp&#243;&#322;_chorobowy"
  ]
  node [
    id 957
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 958
    label "ogrom"
  ]
  node [
    id 959
    label "iskrzy&#263;"
  ]
  node [
    id 960
    label "d&#322;awi&#263;"
  ]
  node [
    id 961
    label "ostygn&#261;&#263;"
  ]
  node [
    id 962
    label "stygn&#261;&#263;"
  ]
  node [
    id 963
    label "temperatura"
  ]
  node [
    id 964
    label "wpa&#347;&#263;"
  ]
  node [
    id 965
    label "afekt"
  ]
  node [
    id 966
    label "wpada&#263;"
  ]
  node [
    id 967
    label "stw&#243;r"
  ]
  node [
    id 968
    label "szkarada"
  ]
  node [
    id 969
    label "istota_fantastyczna"
  ]
  node [
    id 970
    label "refleksja"
  ]
  node [
    id 971
    label "widziad&#322;o"
  ]
  node [
    id 972
    label "l&#281;k"
  ]
  node [
    id 973
    label "bullying"
  ]
  node [
    id 974
    label "oddzia&#322;anie"
  ]
  node [
    id 975
    label "presja"
  ]
  node [
    id 976
    label "cognizance"
  ]
  node [
    id 977
    label "kobieta"
  ]
  node [
    id 978
    label "doros&#322;y"
  ]
  node [
    id 979
    label "&#380;ona"
  ]
  node [
    id 980
    label "samica"
  ]
  node [
    id 981
    label "uleganie"
  ]
  node [
    id 982
    label "ulec"
  ]
  node [
    id 983
    label "m&#281;&#380;yna"
  ]
  node [
    id 984
    label "partnerka"
  ]
  node [
    id 985
    label "ulegni&#281;cie"
  ]
  node [
    id 986
    label "pa&#324;stwo"
  ]
  node [
    id 987
    label "&#322;ono"
  ]
  node [
    id 988
    label "menopauza"
  ]
  node [
    id 989
    label "przekwitanie"
  ]
  node [
    id 990
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 991
    label "babka"
  ]
  node [
    id 992
    label "ulega&#263;"
  ]
  node [
    id 993
    label "droga"
  ]
  node [
    id 994
    label "ukochanie"
  ]
  node [
    id 995
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 996
    label "feblik"
  ]
  node [
    id 997
    label "podnieci&#263;"
  ]
  node [
    id 998
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 999
    label "numer"
  ]
  node [
    id 1000
    label "po&#380;ycie"
  ]
  node [
    id 1001
    label "tendency"
  ]
  node [
    id 1002
    label "podniecenie"
  ]
  node [
    id 1003
    label "zakochanie"
  ]
  node [
    id 1004
    label "zajawka"
  ]
  node [
    id 1005
    label "seks"
  ]
  node [
    id 1006
    label "podniecanie"
  ]
  node [
    id 1007
    label "imisja"
  ]
  node [
    id 1008
    label "love"
  ]
  node [
    id 1009
    label "rozmna&#380;anie"
  ]
  node [
    id 1010
    label "ruch_frykcyjny"
  ]
  node [
    id 1011
    label "na_pieska"
  ]
  node [
    id 1012
    label "serce"
  ]
  node [
    id 1013
    label "pozycja_misjonarska"
  ]
  node [
    id 1014
    label "wi&#281;&#378;"
  ]
  node [
    id 1015
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 1016
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1017
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1018
    label "gra_wst&#281;pna"
  ]
  node [
    id 1019
    label "erotyka"
  ]
  node [
    id 1020
    label "baraszki"
  ]
  node [
    id 1021
    label "drogi"
  ]
  node [
    id 1022
    label "po&#380;&#261;danie"
  ]
  node [
    id 1023
    label "wzw&#243;d"
  ]
  node [
    id 1024
    label "podnieca&#263;"
  ]
  node [
    id 1025
    label "gotowy"
  ]
  node [
    id 1026
    label "might"
  ]
  node [
    id 1027
    label "uprawi&#263;"
  ]
  node [
    id 1028
    label "public_treasury"
  ]
  node [
    id 1029
    label "pole"
  ]
  node [
    id 1030
    label "obrobi&#263;"
  ]
  node [
    id 1031
    label "nietrze&#378;wy"
  ]
  node [
    id 1032
    label "czekanie"
  ]
  node [
    id 1033
    label "martwy"
  ]
  node [
    id 1034
    label "bliski"
  ]
  node [
    id 1035
    label "gotowo"
  ]
  node [
    id 1036
    label "przygotowywanie"
  ]
  node [
    id 1037
    label "przygotowanie"
  ]
  node [
    id 1038
    label "dyspozycyjny"
  ]
  node [
    id 1039
    label "zalany"
  ]
  node [
    id 1040
    label "nieuchronny"
  ]
  node [
    id 1041
    label "doj&#347;cie"
  ]
  node [
    id 1042
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1043
    label "mie&#263;_miejsce"
  ]
  node [
    id 1044
    label "equal"
  ]
  node [
    id 1045
    label "trwa&#263;"
  ]
  node [
    id 1046
    label "chodzi&#263;"
  ]
  node [
    id 1047
    label "si&#281;ga&#263;"
  ]
  node [
    id 1048
    label "obecno&#347;&#263;"
  ]
  node [
    id 1049
    label "stand"
  ]
  node [
    id 1050
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1051
    label "okre&#347;lony"
  ]
  node [
    id 1052
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1053
    label "wiadomy"
  ]
  node [
    id 1054
    label "time"
  ]
  node [
    id 1055
    label "post&#261;pi&#263;"
  ]
  node [
    id 1056
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1057
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1058
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1059
    label "zorganizowa&#263;"
  ]
  node [
    id 1060
    label "appoint"
  ]
  node [
    id 1061
    label "wystylizowa&#263;"
  ]
  node [
    id 1062
    label "cause"
  ]
  node [
    id 1063
    label "przerobi&#263;"
  ]
  node [
    id 1064
    label "nabra&#263;"
  ]
  node [
    id 1065
    label "make"
  ]
  node [
    id 1066
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1067
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1068
    label "wydali&#263;"
  ]
  node [
    id 1069
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 1070
    label "advance"
  ]
  node [
    id 1071
    label "act"
  ]
  node [
    id 1072
    label "usun&#261;&#263;"
  ]
  node [
    id 1073
    label "sack"
  ]
  node [
    id 1074
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 1075
    label "restore"
  ]
  node [
    id 1076
    label "dostosowa&#263;"
  ]
  node [
    id 1077
    label "pozyska&#263;"
  ]
  node [
    id 1078
    label "stworzy&#263;"
  ]
  node [
    id 1079
    label "plan"
  ]
  node [
    id 1080
    label "stage"
  ]
  node [
    id 1081
    label "urobi&#263;"
  ]
  node [
    id 1082
    label "ensnare"
  ]
  node [
    id 1083
    label "wprowadzi&#263;"
  ]
  node [
    id 1084
    label "zaplanowa&#263;"
  ]
  node [
    id 1085
    label "przygotowa&#263;"
  ]
  node [
    id 1086
    label "standard"
  ]
  node [
    id 1087
    label "skupi&#263;"
  ]
  node [
    id 1088
    label "podbi&#263;"
  ]
  node [
    id 1089
    label "umocni&#263;"
  ]
  node [
    id 1090
    label "doprowadzi&#263;"
  ]
  node [
    id 1091
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 1092
    label "zadowoli&#263;"
  ]
  node [
    id 1093
    label "accommodate"
  ]
  node [
    id 1094
    label "zaaran&#380;owa&#263;"
  ]
  node [
    id 1095
    label "zabezpieczy&#263;"
  ]
  node [
    id 1096
    label "wytworzy&#263;"
  ]
  node [
    id 1097
    label "pomy&#347;le&#263;"
  ]
  node [
    id 1098
    label "woda"
  ]
  node [
    id 1099
    label "hoax"
  ]
  node [
    id 1100
    label "deceive"
  ]
  node [
    id 1101
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 1102
    label "oszwabi&#263;"
  ]
  node [
    id 1103
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 1104
    label "gull"
  ]
  node [
    id 1105
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 1106
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 1107
    label "wzi&#261;&#263;"
  ]
  node [
    id 1108
    label "naby&#263;"
  ]
  node [
    id 1109
    label "fraud"
  ]
  node [
    id 1110
    label "kupi&#263;"
  ]
  node [
    id 1111
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1112
    label "objecha&#263;"
  ]
  node [
    id 1113
    label "zaliczy&#263;"
  ]
  node [
    id 1114
    label "overwork"
  ]
  node [
    id 1115
    label "zamieni&#263;"
  ]
  node [
    id 1116
    label "zmodyfikowa&#263;"
  ]
  node [
    id 1117
    label "change"
  ]
  node [
    id 1118
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 1119
    label "zmieni&#263;"
  ]
  node [
    id 1120
    label "convert"
  ]
  node [
    id 1121
    label "przetworzy&#263;"
  ]
  node [
    id 1122
    label "upora&#263;_si&#281;"
  ]
  node [
    id 1123
    label "stylize"
  ]
  node [
    id 1124
    label "nada&#263;"
  ]
  node [
    id 1125
    label "upodobni&#263;"
  ]
  node [
    id 1126
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 1127
    label "sprawi&#263;"
  ]
  node [
    id 1128
    label "przekaza&#263;"
  ]
  node [
    id 1129
    label "convey"
  ]
  node [
    id 1130
    label "grant"
  ]
  node [
    id 1131
    label "propagate"
  ]
  node [
    id 1132
    label "wp&#322;aci&#263;"
  ]
  node [
    id 1133
    label "transfer"
  ]
  node [
    id 1134
    label "wys&#322;a&#263;"
  ]
  node [
    id 1135
    label "give"
  ]
  node [
    id 1136
    label "poda&#263;"
  ]
  node [
    id 1137
    label "sygna&#322;"
  ]
  node [
    id 1138
    label "dotacja"
  ]
  node [
    id 1139
    label "&#347;rodek"
  ]
  node [
    id 1140
    label "rewizja"
  ]
  node [
    id 1141
    label "certificate"
  ]
  node [
    id 1142
    label "argument"
  ]
  node [
    id 1143
    label "forsing"
  ]
  node [
    id 1144
    label "dokument"
  ]
  node [
    id 1145
    label "uzasadnienie"
  ]
  node [
    id 1146
    label "zapis"
  ]
  node [
    id 1147
    label "&#347;wiadectwo"
  ]
  node [
    id 1148
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1149
    label "parafa"
  ]
  node [
    id 1150
    label "plik"
  ]
  node [
    id 1151
    label "raport&#243;wka"
  ]
  node [
    id 1152
    label "utw&#243;r"
  ]
  node [
    id 1153
    label "record"
  ]
  node [
    id 1154
    label "registratura"
  ]
  node [
    id 1155
    label "dokumentacja"
  ]
  node [
    id 1156
    label "fascyku&#322;"
  ]
  node [
    id 1157
    label "artyku&#322;"
  ]
  node [
    id 1158
    label "writing"
  ]
  node [
    id 1159
    label "sygnatariusz"
  ]
  node [
    id 1160
    label "object"
  ]
  node [
    id 1161
    label "przedmiot"
  ]
  node [
    id 1162
    label "temat"
  ]
  node [
    id 1163
    label "wpadni&#281;cie"
  ]
  node [
    id 1164
    label "przyroda"
  ]
  node [
    id 1165
    label "istota"
  ]
  node [
    id 1166
    label "kultura"
  ]
  node [
    id 1167
    label "wpadanie"
  ]
  node [
    id 1168
    label "wyja&#347;nienie"
  ]
  node [
    id 1169
    label "apologetyk"
  ]
  node [
    id 1170
    label "informacja"
  ]
  node [
    id 1171
    label "justyfikacja"
  ]
  node [
    id 1172
    label "gossip"
  ]
  node [
    id 1173
    label "abstrakcja"
  ]
  node [
    id 1174
    label "chemikalia"
  ]
  node [
    id 1175
    label "substancja"
  ]
  node [
    id 1176
    label "parametr"
  ]
  node [
    id 1177
    label "s&#261;d"
  ]
  node [
    id 1178
    label "operand"
  ]
  node [
    id 1179
    label "zmienna"
  ]
  node [
    id 1180
    label "argumentacja"
  ]
  node [
    id 1181
    label "proces_my&#347;lowy"
  ]
  node [
    id 1182
    label "krytyka"
  ]
  node [
    id 1183
    label "rekurs"
  ]
  node [
    id 1184
    label "checkup"
  ]
  node [
    id 1185
    label "odwo&#322;anie"
  ]
  node [
    id 1186
    label "correction"
  ]
  node [
    id 1187
    label "przegl&#261;d"
  ]
  node [
    id 1188
    label "kipisz"
  ]
  node [
    id 1189
    label "amendment"
  ]
  node [
    id 1190
    label "zmiana"
  ]
  node [
    id 1191
    label "korekta"
  ]
  node [
    id 1192
    label "samodzielny"
  ]
  node [
    id 1193
    label "swojak"
  ]
  node [
    id 1194
    label "odpowiedni"
  ]
  node [
    id 1195
    label "bli&#378;ni"
  ]
  node [
    id 1196
    label "odr&#281;bny"
  ]
  node [
    id 1197
    label "sobieradzki"
  ]
  node [
    id 1198
    label "niepodleg&#322;y"
  ]
  node [
    id 1199
    label "czyj&#347;"
  ]
  node [
    id 1200
    label "autonomicznie"
  ]
  node [
    id 1201
    label "indywidualny"
  ]
  node [
    id 1202
    label "samodzielnie"
  ]
  node [
    id 1203
    label "w&#322;asny"
  ]
  node [
    id 1204
    label "osobny"
  ]
  node [
    id 1205
    label "zdarzony"
  ]
  node [
    id 1206
    label "odpowiednio"
  ]
  node [
    id 1207
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1208
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1209
    label "nale&#380;ny"
  ]
  node [
    id 1210
    label "nale&#380;yty"
  ]
  node [
    id 1211
    label "stosownie"
  ]
  node [
    id 1212
    label "odpowiadanie"
  ]
  node [
    id 1213
    label "specjalny"
  ]
  node [
    id 1214
    label "zwi&#261;zanie"
  ]
  node [
    id 1215
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1216
    label "wi&#261;zanie"
  ]
  node [
    id 1217
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1218
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1219
    label "bratnia_dusza"
  ]
  node [
    id 1220
    label "marriage"
  ]
  node [
    id 1221
    label "zwi&#261;zek"
  ]
  node [
    id 1222
    label "zwi&#261;za&#263;"
  ]
  node [
    id 1223
    label "marketing_afiliacyjny"
  ]
  node [
    id 1224
    label "activity"
  ]
  node [
    id 1225
    label "bezproblemowy"
  ]
  node [
    id 1226
    label "wydarzenie"
  ]
  node [
    id 1227
    label "sympatia"
  ]
  node [
    id 1228
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1229
    label "podatno&#347;&#263;"
  ]
  node [
    id 1230
    label "wyraz"
  ]
  node [
    id 1231
    label "afekcja"
  ]
  node [
    id 1232
    label "przytulenie"
  ]
  node [
    id 1233
    label "pokochanie"
  ]
  node [
    id 1234
    label "affection"
  ]
  node [
    id 1235
    label "turn"
  ]
  node [
    id 1236
    label "&#380;art"
  ]
  node [
    id 1237
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1238
    label "publikacja"
  ]
  node [
    id 1239
    label "manewr"
  ]
  node [
    id 1240
    label "impression"
  ]
  node [
    id 1241
    label "wyst&#281;p"
  ]
  node [
    id 1242
    label "sztos"
  ]
  node [
    id 1243
    label "oznaczenie"
  ]
  node [
    id 1244
    label "hotel"
  ]
  node [
    id 1245
    label "pok&#243;j"
  ]
  node [
    id 1246
    label "czasopismo"
  ]
  node [
    id 1247
    label "akt_p&#322;ciowy"
  ]
  node [
    id 1248
    label "orygina&#322;"
  ]
  node [
    id 1249
    label "facet"
  ]
  node [
    id 1250
    label "zabawa"
  ]
  node [
    id 1251
    label "swawola"
  ]
  node [
    id 1252
    label "kszta&#322;t"
  ]
  node [
    id 1253
    label "dobro&#263;"
  ]
  node [
    id 1254
    label "przegroda_mi&#281;dzykomorowa"
  ]
  node [
    id 1255
    label "pulsowa&#263;"
  ]
  node [
    id 1256
    label "koniuszek_serca"
  ]
  node [
    id 1257
    label "pulsowanie"
  ]
  node [
    id 1258
    label "sfera_afektywna"
  ]
  node [
    id 1259
    label "podekscytowanie"
  ]
  node [
    id 1260
    label "deformowanie"
  ]
  node [
    id 1261
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 1262
    label "wola"
  ]
  node [
    id 1263
    label "sumienie"
  ]
  node [
    id 1264
    label "mi&#281;sie&#324;_brodawkowaty"
  ]
  node [
    id 1265
    label "deformowa&#263;"
  ]
  node [
    id 1266
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1267
    label "psychika"
  ]
  node [
    id 1268
    label "courage"
  ]
  node [
    id 1269
    label "przedsionek"
  ]
  node [
    id 1270
    label "systol"
  ]
  node [
    id 1271
    label "w&#281;ze&#322;_zatokowo-przedsionkowy"
  ]
  node [
    id 1272
    label "heart"
  ]
  node [
    id 1273
    label "dzwon"
  ]
  node [
    id 1274
    label "power"
  ]
  node [
    id 1275
    label "strunowiec"
  ]
  node [
    id 1276
    label "kier"
  ]
  node [
    id 1277
    label "elektrokardiografia"
  ]
  node [
    id 1278
    label "entity"
  ]
  node [
    id 1279
    label "&#347;r&#243;dsierdzie"
  ]
  node [
    id 1280
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1281
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1282
    label "podroby"
  ]
  node [
    id 1283
    label "dusza"
  ]
  node [
    id 1284
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1285
    label "organ"
  ]
  node [
    id 1286
    label "ego"
  ]
  node [
    id 1287
    label "kompleksja"
  ]
  node [
    id 1288
    label "charakter"
  ]
  node [
    id 1289
    label "struna_&#347;ci&#281;gnista"
  ]
  node [
    id 1290
    label "fizjonomia"
  ]
  node [
    id 1291
    label "wsierdzie"
  ]
  node [
    id 1292
    label "kompleks"
  ]
  node [
    id 1293
    label "karta"
  ]
  node [
    id 1294
    label "mi&#281;sie&#324;_sercowy"
  ]
  node [
    id 1295
    label "zapalno&#347;&#263;"
  ]
  node [
    id 1296
    label "favor"
  ]
  node [
    id 1297
    label "pikawa"
  ]
  node [
    id 1298
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1299
    label "zastawka"
  ]
  node [
    id 1300
    label "komora"
  ]
  node [
    id 1301
    label "uk&#322;ad_przedsionkowo-komorowy"
  ]
  node [
    id 1302
    label "kardiografia"
  ]
  node [
    id 1303
    label "passion"
  ]
  node [
    id 1304
    label "zatoka_wie&#324;cowa"
  ]
  node [
    id 1305
    label "zami&#322;owanie"
  ]
  node [
    id 1306
    label "streszczenie"
  ]
  node [
    id 1307
    label "harbinger"
  ]
  node [
    id 1308
    label "ch&#281;&#263;"
  ]
  node [
    id 1309
    label "zapowied&#378;"
  ]
  node [
    id 1310
    label "reklama"
  ]
  node [
    id 1311
    label "gadka"
  ]
  node [
    id 1312
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 1313
    label "drogo"
  ]
  node [
    id 1314
    label "mi&#322;y"
  ]
  node [
    id 1315
    label "przyjaciel"
  ]
  node [
    id 1316
    label "warto&#347;ciowy"
  ]
  node [
    id 1317
    label "ekskursja"
  ]
  node [
    id 1318
    label "bezsilnikowy"
  ]
  node [
    id 1319
    label "budowla"
  ]
  node [
    id 1320
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 1321
    label "trasa"
  ]
  node [
    id 1322
    label "podbieg"
  ]
  node [
    id 1323
    label "turystyka"
  ]
  node [
    id 1324
    label "nawierzchnia"
  ]
  node [
    id 1325
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 1326
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 1327
    label "rajza"
  ]
  node [
    id 1328
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1329
    label "korona_drogi"
  ]
  node [
    id 1330
    label "passage"
  ]
  node [
    id 1331
    label "wylot"
  ]
  node [
    id 1332
    label "ekwipunek"
  ]
  node [
    id 1333
    label "zbior&#243;wka"
  ]
  node [
    id 1334
    label "marszrutyzacja"
  ]
  node [
    id 1335
    label "wyb&#243;j"
  ]
  node [
    id 1336
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 1337
    label "drogowskaz"
  ]
  node [
    id 1338
    label "pobocze"
  ]
  node [
    id 1339
    label "journey"
  ]
  node [
    id 1340
    label "nerwowo&#347;&#263;"
  ]
  node [
    id 1341
    label "agitation"
  ]
  node [
    id 1342
    label "fuss"
  ]
  node [
    id 1343
    label "podniecenie_si&#281;"
  ]
  node [
    id 1344
    label "poruszenie"
  ]
  node [
    id 1345
    label "incitation"
  ]
  node [
    id 1346
    label "wzmo&#380;enie"
  ]
  node [
    id 1347
    label "nastr&#243;j"
  ]
  node [
    id 1348
    label "excitation"
  ]
  node [
    id 1349
    label "wprawienie"
  ]
  node [
    id 1350
    label "kompleks_Elektry"
  ]
  node [
    id 1351
    label "uzyskanie"
  ]
  node [
    id 1352
    label "kompleks_Edypa"
  ]
  node [
    id 1353
    label "chcenie"
  ]
  node [
    id 1354
    label "upragnienie"
  ]
  node [
    id 1355
    label "pragnienie"
  ]
  node [
    id 1356
    label "pop&#281;d_p&#322;ciowy"
  ]
  node [
    id 1357
    label "apetyt"
  ]
  node [
    id 1358
    label "reflektowanie"
  ]
  node [
    id 1359
    label "desire"
  ]
  node [
    id 1360
    label "eagerness"
  ]
  node [
    id 1361
    label "wyci&#261;ganie_r&#281;ki"
  ]
  node [
    id 1362
    label "robienie"
  ]
  node [
    id 1363
    label "coexistence"
  ]
  node [
    id 1364
    label "subsistence"
  ]
  node [
    id 1365
    label "&#322;&#261;czenie"
  ]
  node [
    id 1366
    label "wsp&#243;&#322;istnienie"
  ]
  node [
    id 1367
    label "gwa&#322;cenie"
  ]
  node [
    id 1368
    label "poruszanie"
  ]
  node [
    id 1369
    label "podniecanie_si&#281;"
  ]
  node [
    id 1370
    label "wzmaganie"
  ]
  node [
    id 1371
    label "stimulation"
  ]
  node [
    id 1372
    label "wprawianie"
  ]
  node [
    id 1373
    label "heating"
  ]
  node [
    id 1374
    label "excite"
  ]
  node [
    id 1375
    label "wprawi&#263;"
  ]
  node [
    id 1376
    label "inspire"
  ]
  node [
    id 1377
    label "heat"
  ]
  node [
    id 1378
    label "poruszy&#263;"
  ]
  node [
    id 1379
    label "wzm&#243;c"
  ]
  node [
    id 1380
    label "wprawia&#263;"
  ]
  node [
    id 1381
    label "go"
  ]
  node [
    id 1382
    label "porusza&#263;"
  ]
  node [
    id 1383
    label "juszy&#263;"
  ]
  node [
    id 1384
    label "revolutionize"
  ]
  node [
    id 1385
    label "wzmaga&#263;"
  ]
  node [
    id 1386
    label "composing"
  ]
  node [
    id 1387
    label "zespolenie"
  ]
  node [
    id 1388
    label "zjednoczenie"
  ]
  node [
    id 1389
    label "kompozycja"
  ]
  node [
    id 1390
    label "spowodowanie"
  ]
  node [
    id 1391
    label "element"
  ]
  node [
    id 1392
    label "junction"
  ]
  node [
    id 1393
    label "zgrzeina"
  ]
  node [
    id 1394
    label "joining"
  ]
  node [
    id 1395
    label "zrobienie"
  ]
  node [
    id 1396
    label "pieprzn&#261;&#263;"
  ]
  node [
    id 1397
    label "wyci&#261;&#263;"
  ]
  node [
    id 1398
    label "spa&#347;&#263;"
  ]
  node [
    id 1399
    label "fall"
  ]
  node [
    id 1400
    label "wybi&#263;"
  ]
  node [
    id 1401
    label "uderzy&#263;"
  ]
  node [
    id 1402
    label "slaughter"
  ]
  node [
    id 1403
    label "overwhelm"
  ]
  node [
    id 1404
    label "wyrze&#378;bi&#263;"
  ]
  node [
    id 1405
    label "przewr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 1406
    label "przyrzn&#261;&#263;"
  ]
  node [
    id 1407
    label "promiskuityzm"
  ]
  node [
    id 1408
    label "amorousness"
  ]
  node [
    id 1409
    label "niedopasowanie_seksualne"
  ]
  node [
    id 1410
    label "petting"
  ]
  node [
    id 1411
    label "dopasowanie_seksualne"
  ]
  node [
    id 1412
    label "sexual_activity"
  ]
  node [
    id 1413
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 1414
    label "pobudzenie_seksualne"
  ]
  node [
    id 1415
    label "wydzielanie"
  ]
  node [
    id 1416
    label "eroticism"
  ]
  node [
    id 1417
    label "niegrzecznostka"
  ]
  node [
    id 1418
    label "nami&#281;tno&#347;&#263;"
  ]
  node [
    id 1419
    label "addition"
  ]
  node [
    id 1420
    label "rozr&#243;d"
  ]
  node [
    id 1421
    label "powodowanie"
  ]
  node [
    id 1422
    label "stan&#243;wka"
  ]
  node [
    id 1423
    label "ci&#261;&#380;a"
  ]
  node [
    id 1424
    label "zap&#322;odnienie"
  ]
  node [
    id 1425
    label "sukces_reprodukcyjny"
  ]
  node [
    id 1426
    label "wyl&#281;g"
  ]
  node [
    id 1427
    label "rozmna&#380;anie_si&#281;"
  ]
  node [
    id 1428
    label "tarlak"
  ]
  node [
    id 1429
    label "agamia"
  ]
  node [
    id 1430
    label "multiplication"
  ]
  node [
    id 1431
    label "zwi&#281;kszanie"
  ]
  node [
    id 1432
    label "Department_of_Commerce"
  ]
  node [
    id 1433
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 1434
    label "meeting"
  ]
  node [
    id 1435
    label "invite"
  ]
  node [
    id 1436
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 1437
    label "uzyska&#263;"
  ]
  node [
    id 1438
    label "give_birth"
  ]
  node [
    id 1439
    label "argue"
  ]
  node [
    id 1440
    label "podtrzymywa&#263;"
  ]
  node [
    id 1441
    label "s&#261;dzi&#263;"
  ]
  node [
    id 1442
    label "twierdzi&#263;"
  ]
  node [
    id 1443
    label "zapewnia&#263;"
  ]
  node [
    id 1444
    label "corroborate"
  ]
  node [
    id 1445
    label "trzyma&#263;"
  ]
  node [
    id 1446
    label "panowa&#263;"
  ]
  node [
    id 1447
    label "defy"
  ]
  node [
    id 1448
    label "cope"
  ]
  node [
    id 1449
    label "broni&#263;"
  ]
  node [
    id 1450
    label "sprawowa&#263;"
  ]
  node [
    id 1451
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 1452
    label "zachowywa&#263;"
  ]
  node [
    id 1453
    label "tajemnica"
  ]
  node [
    id 1454
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 1455
    label "zdyscyplinowanie"
  ]
  node [
    id 1456
    label "post"
  ]
  node [
    id 1457
    label "control"
  ]
  node [
    id 1458
    label "przechowywa&#263;"
  ]
  node [
    id 1459
    label "behave"
  ]
  node [
    id 1460
    label "dieta"
  ]
  node [
    id 1461
    label "hold"
  ]
  node [
    id 1462
    label "post&#281;powa&#263;"
  ]
  node [
    id 1463
    label "pociesza&#263;"
  ]
  node [
    id 1464
    label "patronize"
  ]
  node [
    id 1465
    label "reinforce"
  ]
  node [
    id 1466
    label "back"
  ]
  node [
    id 1467
    label "treat"
  ]
  node [
    id 1468
    label "wychowywa&#263;"
  ]
  node [
    id 1469
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1470
    label "pozostawa&#263;"
  ]
  node [
    id 1471
    label "dzier&#380;y&#263;"
  ]
  node [
    id 1472
    label "zmusza&#263;"
  ]
  node [
    id 1473
    label "continue"
  ]
  node [
    id 1474
    label "przetrzymywa&#263;"
  ]
  node [
    id 1475
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 1476
    label "hodowa&#263;"
  ]
  node [
    id 1477
    label "administrowa&#263;"
  ]
  node [
    id 1478
    label "sympatyzowa&#263;"
  ]
  node [
    id 1479
    label "adhere"
  ]
  node [
    id 1480
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 1481
    label "oznajmia&#263;"
  ]
  node [
    id 1482
    label "attest"
  ]
  node [
    id 1483
    label "komunikowa&#263;"
  ]
  node [
    id 1484
    label "wsp&#243;&#322;rz&#261;dzi&#263;"
  ]
  node [
    id 1485
    label "my&#347;le&#263;"
  ]
  node [
    id 1486
    label "deliver"
  ]
  node [
    id 1487
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 1488
    label "zas&#261;dza&#263;"
  ]
  node [
    id 1489
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 1490
    label "defray"
  ]
  node [
    id 1491
    label "fend"
  ]
  node [
    id 1492
    label "reprezentowa&#263;"
  ]
  node [
    id 1493
    label "zdawa&#263;"
  ]
  node [
    id 1494
    label "czuwa&#263;"
  ]
  node [
    id 1495
    label "preach"
  ]
  node [
    id 1496
    label "chroni&#263;"
  ]
  node [
    id 1497
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 1498
    label "walczy&#263;"
  ]
  node [
    id 1499
    label "resist"
  ]
  node [
    id 1500
    label "adwokatowa&#263;"
  ]
  node [
    id 1501
    label "rebuff"
  ]
  node [
    id 1502
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1503
    label "udowadnia&#263;"
  ]
  node [
    id 1504
    label "gra&#263;"
  ]
  node [
    id 1505
    label "refuse"
  ]
  node [
    id 1506
    label "prosecute"
  ]
  node [
    id 1507
    label "manipulate"
  ]
  node [
    id 1508
    label "istnie&#263;"
  ]
  node [
    id 1509
    label "kontrolowa&#263;"
  ]
  node [
    id 1510
    label "kierowa&#263;"
  ]
  node [
    id 1511
    label "dominowa&#263;"
  ]
  node [
    id 1512
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 1513
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 1514
    label "przewa&#380;a&#263;"
  ]
  node [
    id 1515
    label "dostarcza&#263;"
  ]
  node [
    id 1516
    label "informowa&#263;"
  ]
  node [
    id 1517
    label "utrzymywanie"
  ]
  node [
    id 1518
    label "move"
  ]
  node [
    id 1519
    label "movement"
  ]
  node [
    id 1520
    label "posuni&#281;cie"
  ]
  node [
    id 1521
    label "myk"
  ]
  node [
    id 1522
    label "taktyka"
  ]
  node [
    id 1523
    label "utrzyma&#263;"
  ]
  node [
    id 1524
    label "maneuver"
  ]
  node [
    id 1525
    label "utrzymanie"
  ]
  node [
    id 1526
    label "bycie"
  ]
  node [
    id 1527
    label "subsystencja"
  ]
  node [
    id 1528
    label "egzystencja"
  ]
  node [
    id 1529
    label "wy&#380;ywienie"
  ]
  node [
    id 1530
    label "ontologicznie"
  ]
  node [
    id 1531
    label "potencja"
  ]
  node [
    id 1532
    label "Stary_&#346;wiat"
  ]
  node [
    id 1533
    label "asymilowanie_si&#281;"
  ]
  node [
    id 1534
    label "p&#243;&#322;noc"
  ]
  node [
    id 1535
    label "Wsch&#243;d"
  ]
  node [
    id 1536
    label "geosfera"
  ]
  node [
    id 1537
    label "obiekt_naturalny"
  ]
  node [
    id 1538
    label "przejmowanie"
  ]
  node [
    id 1539
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1540
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 1541
    label "po&#322;udnie"
  ]
  node [
    id 1542
    label "makrokosmos"
  ]
  node [
    id 1543
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 1544
    label "environment"
  ]
  node [
    id 1545
    label "morze"
  ]
  node [
    id 1546
    label "rze&#378;ba"
  ]
  node [
    id 1547
    label "przejmowa&#263;"
  ]
  node [
    id 1548
    label "hydrosfera"
  ]
  node [
    id 1549
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 1550
    label "ciemna_materia"
  ]
  node [
    id 1551
    label "ekosystem"
  ]
  node [
    id 1552
    label "biota"
  ]
  node [
    id 1553
    label "ekosfera"
  ]
  node [
    id 1554
    label "geotermia"
  ]
  node [
    id 1555
    label "planeta"
  ]
  node [
    id 1556
    label "ozonosfera"
  ]
  node [
    id 1557
    label "wszechstworzenie"
  ]
  node [
    id 1558
    label "kuchnia"
  ]
  node [
    id 1559
    label "biosfera"
  ]
  node [
    id 1560
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 1561
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 1562
    label "populace"
  ]
  node [
    id 1563
    label "magnetosfera"
  ]
  node [
    id 1564
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1565
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 1566
    label "universe"
  ]
  node [
    id 1567
    label "biegun"
  ]
  node [
    id 1568
    label "litosfera"
  ]
  node [
    id 1569
    label "teren"
  ]
  node [
    id 1570
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 1571
    label "przej&#281;cie"
  ]
  node [
    id 1572
    label "barysfera"
  ]
  node [
    id 1573
    label "czarna_dziura"
  ]
  node [
    id 1574
    label "atmosfera"
  ]
  node [
    id 1575
    label "przej&#261;&#263;"
  ]
  node [
    id 1576
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 1577
    label "Ziemia"
  ]
  node [
    id 1578
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 1579
    label "geoida"
  ]
  node [
    id 1580
    label "zagranica"
  ]
  node [
    id 1581
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 1582
    label "fauna"
  ]
  node [
    id 1583
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 1584
    label "odm&#322;adzanie"
  ]
  node [
    id 1585
    label "liga"
  ]
  node [
    id 1586
    label "jednostka_systematyczna"
  ]
  node [
    id 1587
    label "gromada"
  ]
  node [
    id 1588
    label "Entuzjastki"
  ]
  node [
    id 1589
    label "Terranie"
  ]
  node [
    id 1590
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1591
    label "category"
  ]
  node [
    id 1592
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1593
    label "cz&#261;steczka"
  ]
  node [
    id 1594
    label "stage_set"
  ]
  node [
    id 1595
    label "type"
  ]
  node [
    id 1596
    label "specgrupa"
  ]
  node [
    id 1597
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1598
    label "&#346;wietliki"
  ]
  node [
    id 1599
    label "odm&#322;odzenie"
  ]
  node [
    id 1600
    label "Eurogrupa"
  ]
  node [
    id 1601
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1602
    label "formacja_geologiczna"
  ]
  node [
    id 1603
    label "harcerze_starsi"
  ]
  node [
    id 1604
    label "Kosowo"
  ]
  node [
    id 1605
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1606
    label "Zab&#322;ocie"
  ]
  node [
    id 1607
    label "zach&#243;d"
  ]
  node [
    id 1608
    label "Pow&#261;zki"
  ]
  node [
    id 1609
    label "Piotrowo"
  ]
  node [
    id 1610
    label "Olszanica"
  ]
  node [
    id 1611
    label "Ruda_Pabianicka"
  ]
  node [
    id 1612
    label "holarktyka"
  ]
  node [
    id 1613
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1614
    label "Ludwin&#243;w"
  ]
  node [
    id 1615
    label "Arktyka"
  ]
  node [
    id 1616
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1617
    label "Zabu&#380;e"
  ]
  node [
    id 1618
    label "antroposfera"
  ]
  node [
    id 1619
    label "Neogea"
  ]
  node [
    id 1620
    label "terytorium"
  ]
  node [
    id 1621
    label "Syberia_Zachodnia"
  ]
  node [
    id 1622
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1623
    label "pas_planetoid"
  ]
  node [
    id 1624
    label "Syberia_Wschodnia"
  ]
  node [
    id 1625
    label "Antarktyka"
  ]
  node [
    id 1626
    label "Rakowice"
  ]
  node [
    id 1627
    label "akrecja"
  ]
  node [
    id 1628
    label "&#321;&#281;g"
  ]
  node [
    id 1629
    label "Kresy_Zachodnie"
  ]
  node [
    id 1630
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1631
    label "wsch&#243;d"
  ]
  node [
    id 1632
    label "Notogea"
  ]
  node [
    id 1633
    label "boski"
  ]
  node [
    id 1634
    label "krajobraz"
  ]
  node [
    id 1635
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1636
    label "przywidzenie"
  ]
  node [
    id 1637
    label "presence"
  ]
  node [
    id 1638
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1639
    label "rozdzielanie"
  ]
  node [
    id 1640
    label "bezbrze&#380;e"
  ]
  node [
    id 1641
    label "niezmierzony"
  ]
  node [
    id 1642
    label "przedzielenie"
  ]
  node [
    id 1643
    label "nielito&#347;ciwy"
  ]
  node [
    id 1644
    label "rozdziela&#263;"
  ]
  node [
    id 1645
    label "oktant"
  ]
  node [
    id 1646
    label "przedzieli&#263;"
  ]
  node [
    id 1647
    label "przestw&#243;r"
  ]
  node [
    id 1648
    label "rura"
  ]
  node [
    id 1649
    label "grzebiuszka"
  ]
  node [
    id 1650
    label "atom"
  ]
  node [
    id 1651
    label "odbicie"
  ]
  node [
    id 1652
    label "kosmos"
  ]
  node [
    id 1653
    label "miniatura"
  ]
  node [
    id 1654
    label "smok_wawelski"
  ]
  node [
    id 1655
    label "niecz&#322;owiek"
  ]
  node [
    id 1656
    label "monster"
  ]
  node [
    id 1657
    label "istota_&#380;ywa"
  ]
  node [
    id 1658
    label "potw&#243;r"
  ]
  node [
    id 1659
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 1660
    label "ciep&#322;o"
  ]
  node [
    id 1661
    label "energia_termiczna"
  ]
  node [
    id 1662
    label "aspekt"
  ]
  node [
    id 1663
    label "troposfera"
  ]
  node [
    id 1664
    label "klimat"
  ]
  node [
    id 1665
    label "metasfera"
  ]
  node [
    id 1666
    label "atmosferyki"
  ]
  node [
    id 1667
    label "homosfera"
  ]
  node [
    id 1668
    label "powietrznia"
  ]
  node [
    id 1669
    label "jonosfera"
  ]
  node [
    id 1670
    label "termosfera"
  ]
  node [
    id 1671
    label "egzosfera"
  ]
  node [
    id 1672
    label "heterosfera"
  ]
  node [
    id 1673
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 1674
    label "tropopauza"
  ]
  node [
    id 1675
    label "kwas"
  ]
  node [
    id 1676
    label "powietrze"
  ]
  node [
    id 1677
    label "stratosfera"
  ]
  node [
    id 1678
    label "pow&#322;oka"
  ]
  node [
    id 1679
    label "mezosfera"
  ]
  node [
    id 1680
    label "mezopauza"
  ]
  node [
    id 1681
    label "atmosphere"
  ]
  node [
    id 1682
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 1683
    label "sferoida"
  ]
  node [
    id 1684
    label "czerpa&#263;"
  ]
  node [
    id 1685
    label "bra&#263;"
  ]
  node [
    id 1686
    label "handle"
  ]
  node [
    id 1687
    label "wzbudza&#263;"
  ]
  node [
    id 1688
    label "ogarnia&#263;"
  ]
  node [
    id 1689
    label "bang"
  ]
  node [
    id 1690
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1691
    label "stimulate"
  ]
  node [
    id 1692
    label "ogarn&#261;&#263;"
  ]
  node [
    id 1693
    label "wzbudzi&#263;"
  ]
  node [
    id 1694
    label "thrill"
  ]
  node [
    id 1695
    label "czerpanie"
  ]
  node [
    id 1696
    label "acquisition"
  ]
  node [
    id 1697
    label "branie"
  ]
  node [
    id 1698
    label "caparison"
  ]
  node [
    id 1699
    label "wzbudzanie"
  ]
  node [
    id 1700
    label "ogarnianie"
  ]
  node [
    id 1701
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 1702
    label "interception"
  ]
  node [
    id 1703
    label "wzbudzenie"
  ]
  node [
    id 1704
    label "emotion"
  ]
  node [
    id 1705
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1706
    label "wzi&#281;cie"
  ]
  node [
    id 1707
    label "zboczenie"
  ]
  node [
    id 1708
    label "om&#243;wienie"
  ]
  node [
    id 1709
    label "sponiewieranie"
  ]
  node [
    id 1710
    label "discipline"
  ]
  node [
    id 1711
    label "omawia&#263;"
  ]
  node [
    id 1712
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1713
    label "tre&#347;&#263;"
  ]
  node [
    id 1714
    label "sponiewiera&#263;"
  ]
  node [
    id 1715
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1716
    label "tematyka"
  ]
  node [
    id 1717
    label "w&#261;tek"
  ]
  node [
    id 1718
    label "zbaczanie"
  ]
  node [
    id 1719
    label "program_nauczania"
  ]
  node [
    id 1720
    label "om&#243;wi&#263;"
  ]
  node [
    id 1721
    label "omawianie"
  ]
  node [
    id 1722
    label "zbacza&#263;"
  ]
  node [
    id 1723
    label "zboczy&#263;"
  ]
  node [
    id 1724
    label "performance"
  ]
  node [
    id 1725
    label "sztuka"
  ]
  node [
    id 1726
    label "Boreasz"
  ]
  node [
    id 1727
    label "noc"
  ]
  node [
    id 1728
    label "p&#243;&#322;nocek"
  ]
  node [
    id 1729
    label "strona_&#347;wiata"
  ]
  node [
    id 1730
    label "godzina"
  ]
  node [
    id 1731
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1732
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1733
    label "warstwa"
  ]
  node [
    id 1734
    label "kriosfera"
  ]
  node [
    id 1735
    label "lej_polarny"
  ]
  node [
    id 1736
    label "brzeg"
  ]
  node [
    id 1737
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 1738
    label "p&#322;oza"
  ]
  node [
    id 1739
    label "zawiasy"
  ]
  node [
    id 1740
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 1741
    label "element_anatomiczny"
  ]
  node [
    id 1742
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 1743
    label "reda"
  ]
  node [
    id 1744
    label "zbiornik_wodny"
  ]
  node [
    id 1745
    label "przymorze"
  ]
  node [
    id 1746
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 1747
    label "bezmiar"
  ]
  node [
    id 1748
    label "pe&#322;ne_morze"
  ]
  node [
    id 1749
    label "latarnia_morska"
  ]
  node [
    id 1750
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1751
    label "nereida"
  ]
  node [
    id 1752
    label "okeanida"
  ]
  node [
    id 1753
    label "marina"
  ]
  node [
    id 1754
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 1755
    label "Morze_Czerwone"
  ]
  node [
    id 1756
    label "talasoterapia"
  ]
  node [
    id 1757
    label "Morze_Bia&#322;e"
  ]
  node [
    id 1758
    label "paliszcze"
  ]
  node [
    id 1759
    label "Neptun"
  ]
  node [
    id 1760
    label "Morze_Czarne"
  ]
  node [
    id 1761
    label "laguna"
  ]
  node [
    id 1762
    label "Morze_Egejskie"
  ]
  node [
    id 1763
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 1764
    label "Morze_Adriatyckie"
  ]
  node [
    id 1765
    label "rze&#378;biarstwo"
  ]
  node [
    id 1766
    label "planacja"
  ]
  node [
    id 1767
    label "relief"
  ]
  node [
    id 1768
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1769
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1770
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 1771
    label "bozzetto"
  ]
  node [
    id 1772
    label "plastyka"
  ]
  node [
    id 1773
    label "j&#261;dro"
  ]
  node [
    id 1774
    label "dwunasta"
  ]
  node [
    id 1775
    label "pora"
  ]
  node [
    id 1776
    label "ozon"
  ]
  node [
    id 1777
    label "gleba"
  ]
  node [
    id 1778
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 1779
    label "sialma"
  ]
  node [
    id 1780
    label "skorupa_ziemska"
  ]
  node [
    id 1781
    label "warstwa_perydotytowa"
  ]
  node [
    id 1782
    label "warstwa_granitowa"
  ]
  node [
    id 1783
    label "kresom&#243;zgowie"
  ]
  node [
    id 1784
    label "przyra"
  ]
  node [
    id 1785
    label "biom"
  ]
  node [
    id 1786
    label "awifauna"
  ]
  node [
    id 1787
    label "ichtiofauna"
  ]
  node [
    id 1788
    label "geosystem"
  ]
  node [
    id 1789
    label "dotleni&#263;"
  ]
  node [
    id 1790
    label "spi&#281;trza&#263;"
  ]
  node [
    id 1791
    label "spi&#281;trzenie"
  ]
  node [
    id 1792
    label "utylizator"
  ]
  node [
    id 1793
    label "p&#322;ycizna"
  ]
  node [
    id 1794
    label "nabranie"
  ]
  node [
    id 1795
    label "Waruna"
  ]
  node [
    id 1796
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 1797
    label "przybieranie"
  ]
  node [
    id 1798
    label "uci&#261;g"
  ]
  node [
    id 1799
    label "bombast"
  ]
  node [
    id 1800
    label "fala"
  ]
  node [
    id 1801
    label "kryptodepresja"
  ]
  node [
    id 1802
    label "water"
  ]
  node [
    id 1803
    label "wysi&#281;k"
  ]
  node [
    id 1804
    label "pustka"
  ]
  node [
    id 1805
    label "ciecz"
  ]
  node [
    id 1806
    label "przybrze&#380;e"
  ]
  node [
    id 1807
    label "spi&#281;trzanie"
  ]
  node [
    id 1808
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 1809
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 1810
    label "bicie"
  ]
  node [
    id 1811
    label "klarownik"
  ]
  node [
    id 1812
    label "chlastanie"
  ]
  node [
    id 1813
    label "woda_s&#322;odka"
  ]
  node [
    id 1814
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 1815
    label "chlasta&#263;"
  ]
  node [
    id 1816
    label "uj&#281;cie_wody"
  ]
  node [
    id 1817
    label "zrzut"
  ]
  node [
    id 1818
    label "wypowied&#378;"
  ]
  node [
    id 1819
    label "wodnik"
  ]
  node [
    id 1820
    label "pojazd"
  ]
  node [
    id 1821
    label "l&#243;d"
  ]
  node [
    id 1822
    label "wybrze&#380;e"
  ]
  node [
    id 1823
    label "deklamacja"
  ]
  node [
    id 1824
    label "tlenek"
  ]
  node [
    id 1825
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 1826
    label "biotop"
  ]
  node [
    id 1827
    label "biocenoza"
  ]
  node [
    id 1828
    label "kontekst"
  ]
  node [
    id 1829
    label "miejsce_pracy"
  ]
  node [
    id 1830
    label "nation"
  ]
  node [
    id 1831
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 1832
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 1833
    label "w&#322;adza"
  ]
  node [
    id 1834
    label "szata_ro&#347;linna"
  ]
  node [
    id 1835
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1836
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1837
    label "zielono&#347;&#263;"
  ]
  node [
    id 1838
    label "pi&#281;tro"
  ]
  node [
    id 1839
    label "plant"
  ]
  node [
    id 1840
    label "iglak"
  ]
  node [
    id 1841
    label "cyprysowate"
  ]
  node [
    id 1842
    label "zaj&#281;cie"
  ]
  node [
    id 1843
    label "instytucja"
  ]
  node [
    id 1844
    label "tajniki"
  ]
  node [
    id 1845
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 1846
    label "jedzenie"
  ]
  node [
    id 1847
    label "zaplecze"
  ]
  node [
    id 1848
    label "pomieszczenie"
  ]
  node [
    id 1849
    label "zlewozmywak"
  ]
  node [
    id 1850
    label "gotowa&#263;"
  ]
  node [
    id 1851
    label "Jowisz"
  ]
  node [
    id 1852
    label "syzygia"
  ]
  node [
    id 1853
    label "Saturn"
  ]
  node [
    id 1854
    label "Uran"
  ]
  node [
    id 1855
    label "message"
  ]
  node [
    id 1856
    label "dar"
  ]
  node [
    id 1857
    label "real"
  ]
  node [
    id 1858
    label "Ukraina"
  ]
  node [
    id 1859
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1860
    label "blok_wschodni"
  ]
  node [
    id 1861
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1862
    label "Europa_Wschodnia"
  ]
  node [
    id 1863
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 1864
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1865
    label "mechanika"
  ]
  node [
    id 1866
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1867
    label "travel"
  ]
  node [
    id 1868
    label "kanciasty"
  ]
  node [
    id 1869
    label "commercial_enterprise"
  ]
  node [
    id 1870
    label "model"
  ]
  node [
    id 1871
    label "strumie&#324;"
  ]
  node [
    id 1872
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1873
    label "kr&#243;tki"
  ]
  node [
    id 1874
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1875
    label "apraksja"
  ]
  node [
    id 1876
    label "natural_process"
  ]
  node [
    id 1877
    label "d&#322;ugi"
  ]
  node [
    id 1878
    label "dyssypacja_energii"
  ]
  node [
    id 1879
    label "tumult"
  ]
  node [
    id 1880
    label "stopek"
  ]
  node [
    id 1881
    label "lokomocja"
  ]
  node [
    id 1882
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1883
    label "komunikacja"
  ]
  node [
    id 1884
    label "drift"
  ]
  node [
    id 1885
    label "kognicja"
  ]
  node [
    id 1886
    label "rozprawa"
  ]
  node [
    id 1887
    label "legislacyjnie"
  ]
  node [
    id 1888
    label "przes&#322;anka"
  ]
  node [
    id 1889
    label "nast&#281;pstwo"
  ]
  node [
    id 1890
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1891
    label "przebiec"
  ]
  node [
    id 1892
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1893
    label "motyw"
  ]
  node [
    id 1894
    label "przebiegni&#281;cie"
  ]
  node [
    id 1895
    label "fabu&#322;a"
  ]
  node [
    id 1896
    label "action"
  ]
  node [
    id 1897
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 1898
    label "absolutorium"
  ]
  node [
    id 1899
    label "dzia&#322;anie"
  ]
  node [
    id 1900
    label "transportation_system"
  ]
  node [
    id 1901
    label "explicite"
  ]
  node [
    id 1902
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 1903
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 1904
    label "implicite"
  ]
  node [
    id 1905
    label "ekspedytor"
  ]
  node [
    id 1906
    label "oznaka"
  ]
  node [
    id 1907
    label "ferment"
  ]
  node [
    id 1908
    label "komplet"
  ]
  node [
    id 1909
    label "anatomopatolog"
  ]
  node [
    id 1910
    label "zmianka"
  ]
  node [
    id 1911
    label "odmienianie"
  ]
  node [
    id 1912
    label "tura"
  ]
  node [
    id 1913
    label "daleki"
  ]
  node [
    id 1914
    label "d&#322;ugo"
  ]
  node [
    id 1915
    label "struktura"
  ]
  node [
    id 1916
    label "mechanika_teoretyczna"
  ]
  node [
    id 1917
    label "mechanika_gruntu"
  ]
  node [
    id 1918
    label "mechanika_g&#243;rotworu"
  ]
  node [
    id 1919
    label "mechanika_klasyczna"
  ]
  node [
    id 1920
    label "elektromechanika"
  ]
  node [
    id 1921
    label "mechanika_o&#347;rodk&#243;w_ci&#261;g&#322;ych"
  ]
  node [
    id 1922
    label "fizyka"
  ]
  node [
    id 1923
    label "aeromechanika"
  ]
  node [
    id 1924
    label "telemechanika"
  ]
  node [
    id 1925
    label "hydromechanika"
  ]
  node [
    id 1926
    label "disquiet"
  ]
  node [
    id 1927
    label "ha&#322;as"
  ]
  node [
    id 1928
    label "woda_powierzchniowa"
  ]
  node [
    id 1929
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1930
    label "ciek_wodny"
  ]
  node [
    id 1931
    label "mn&#243;stwo"
  ]
  node [
    id 1932
    label "Ajgospotamoj"
  ]
  node [
    id 1933
    label "szybki"
  ]
  node [
    id 1934
    label "jednowyrazowy"
  ]
  node [
    id 1935
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 1936
    label "kr&#243;tko"
  ]
  node [
    id 1937
    label "drobny"
  ]
  node [
    id 1938
    label "brak"
  ]
  node [
    id 1939
    label "obronienie"
  ]
  node [
    id 1940
    label "zap&#322;acenie"
  ]
  node [
    id 1941
    label "potrzymanie"
  ]
  node [
    id 1942
    label "przetrzymanie"
  ]
  node [
    id 1943
    label "preservation"
  ]
  node [
    id 1944
    label "zdo&#322;anie"
  ]
  node [
    id 1945
    label "uniesienie"
  ]
  node [
    id 1946
    label "zapewnienie"
  ]
  node [
    id 1947
    label "podtrzymanie"
  ]
  node [
    id 1948
    label "wychowanie"
  ]
  node [
    id 1949
    label "obroni&#263;"
  ]
  node [
    id 1950
    label "potrzyma&#263;"
  ]
  node [
    id 1951
    label "op&#322;aci&#263;"
  ]
  node [
    id 1952
    label "zdo&#322;a&#263;"
  ]
  node [
    id 1953
    label "podtrzyma&#263;"
  ]
  node [
    id 1954
    label "feed"
  ]
  node [
    id 1955
    label "przetrzyma&#263;"
  ]
  node [
    id 1956
    label "foster"
  ]
  node [
    id 1957
    label "preserve"
  ]
  node [
    id 1958
    label "zapewni&#263;"
  ]
  node [
    id 1959
    label "zachowa&#263;"
  ]
  node [
    id 1960
    label "unie&#347;&#263;"
  ]
  node [
    id 1961
    label "bronienie"
  ]
  node [
    id 1962
    label "trzymanie"
  ]
  node [
    id 1963
    label "podtrzymywanie"
  ]
  node [
    id 1964
    label "wychowywanie"
  ]
  node [
    id 1965
    label "panowanie"
  ]
  node [
    id 1966
    label "zachowywanie"
  ]
  node [
    id 1967
    label "twierdzenie"
  ]
  node [
    id 1968
    label "chowanie"
  ]
  node [
    id 1969
    label "retention"
  ]
  node [
    id 1970
    label "op&#322;acanie"
  ]
  node [
    id 1971
    label "s&#261;dzenie"
  ]
  node [
    id 1972
    label "zapewnianie"
  ]
  node [
    id 1973
    label "gesture"
  ]
  node [
    id 1974
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 1975
    label "poruszanie_si&#281;"
  ]
  node [
    id 1976
    label "nietaktowny"
  ]
  node [
    id 1977
    label "kanciasto"
  ]
  node [
    id 1978
    label "niezgrabny"
  ]
  node [
    id 1979
    label "kanciaty"
  ]
  node [
    id 1980
    label "szorstki"
  ]
  node [
    id 1981
    label "niesk&#322;adny"
  ]
  node [
    id 1982
    label "stra&#380;nik"
  ]
  node [
    id 1983
    label "szko&#322;a"
  ]
  node [
    id 1984
    label "przedszkole"
  ]
  node [
    id 1985
    label "opiekun"
  ]
  node [
    id 1986
    label "prezenter"
  ]
  node [
    id 1987
    label "typ"
  ]
  node [
    id 1988
    label "mildew"
  ]
  node [
    id 1989
    label "motif"
  ]
  node [
    id 1990
    label "pozowanie"
  ]
  node [
    id 1991
    label "ideal"
  ]
  node [
    id 1992
    label "matryca"
  ]
  node [
    id 1993
    label "adaptation"
  ]
  node [
    id 1994
    label "pozowa&#263;"
  ]
  node [
    id 1995
    label "imitacja"
  ]
  node [
    id 1996
    label "apraxia"
  ]
  node [
    id 1997
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1998
    label "sport_motorowy"
  ]
  node [
    id 1999
    label "jazda"
  ]
  node [
    id 2000
    label "zwiad"
  ]
  node [
    id 2001
    label "pocz&#261;tki"
  ]
  node [
    id 2002
    label "wrinkle"
  ]
  node [
    id 2003
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 2004
    label "Sierpie&#324;"
  ]
  node [
    id 2005
    label "Michnik"
  ]
  node [
    id 2006
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 2007
    label "solidarno&#347;ciowiec"
  ]
  node [
    id 2008
    label "udzieli&#263;"
  ]
  node [
    id 2009
    label "pu&#347;ci&#263;_p&#322;azem"
  ]
  node [
    id 2010
    label "zapomnie&#263;"
  ]
  node [
    id 2011
    label "przesta&#263;"
  ]
  node [
    id 2012
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 2013
    label "coating"
  ]
  node [
    id 2014
    label "drop"
  ]
  node [
    id 2015
    label "sko&#324;czy&#263;"
  ]
  node [
    id 2016
    label "leave_office"
  ]
  node [
    id 2017
    label "fail"
  ]
  node [
    id 2018
    label "da&#263;"
  ]
  node [
    id 2019
    label "udost&#281;pni&#263;"
  ]
  node [
    id 2020
    label "przyzna&#263;"
  ]
  node [
    id 2021
    label "picture"
  ]
  node [
    id 2022
    label "promocja"
  ]
  node [
    id 2023
    label "odst&#261;pi&#263;"
  ]
  node [
    id 2024
    label "pozostawi&#263;"
  ]
  node [
    id 2025
    label "zdolno&#347;&#263;"
  ]
  node [
    id 2026
    label "screw"
  ]
  node [
    id 2027
    label "straci&#263;"
  ]
  node [
    id 2028
    label "porzuci&#263;"
  ]
  node [
    id 2029
    label "opu&#347;ci&#263;"
  ]
  node [
    id 2030
    label "zabaczy&#263;"
  ]
  node [
    id 2031
    label "fuck"
  ]
  node [
    id 2032
    label "poleca&#263;"
  ]
  node [
    id 2033
    label "zaprasza&#263;"
  ]
  node [
    id 2034
    label "zach&#281;ca&#263;"
  ]
  node [
    id 2035
    label "suffice"
  ]
  node [
    id 2036
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 2037
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 2038
    label "pies"
  ]
  node [
    id 2039
    label "zezwala&#263;"
  ]
  node [
    id 2040
    label "ask"
  ]
  node [
    id 2041
    label "oferowa&#263;"
  ]
  node [
    id 2042
    label "zostawa&#263;"
  ]
  node [
    id 2043
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 2044
    label "ordynowa&#263;"
  ]
  node [
    id 2045
    label "doradza&#263;"
  ]
  node [
    id 2046
    label "m&#243;wi&#263;"
  ]
  node [
    id 2047
    label "charge"
  ]
  node [
    id 2048
    label "placard"
  ]
  node [
    id 2049
    label "powierza&#263;"
  ]
  node [
    id 2050
    label "zadawa&#263;"
  ]
  node [
    id 2051
    label "pozyskiwa&#263;"
  ]
  node [
    id 2052
    label "uznawa&#263;"
  ]
  node [
    id 2053
    label "authorize"
  ]
  node [
    id 2054
    label "piese&#322;"
  ]
  node [
    id 2055
    label "Cerber"
  ]
  node [
    id 2056
    label "szczeka&#263;"
  ]
  node [
    id 2057
    label "&#322;ajdak"
  ]
  node [
    id 2058
    label "kabanos"
  ]
  node [
    id 2059
    label "wyzwisko"
  ]
  node [
    id 2060
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 2061
    label "samiec"
  ]
  node [
    id 2062
    label "spragniony"
  ]
  node [
    id 2063
    label "policjant"
  ]
  node [
    id 2064
    label "rakarz"
  ]
  node [
    id 2065
    label "szczu&#263;"
  ]
  node [
    id 2066
    label "wycie"
  ]
  node [
    id 2067
    label "trufla"
  ]
  node [
    id 2068
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 2069
    label "zawy&#263;"
  ]
  node [
    id 2070
    label "sobaka"
  ]
  node [
    id 2071
    label "dogoterapia"
  ]
  node [
    id 2072
    label "s&#322;u&#380;enie"
  ]
  node [
    id 2073
    label "psowate"
  ]
  node [
    id 2074
    label "wy&#263;"
  ]
  node [
    id 2075
    label "szczucie"
  ]
  node [
    id 2076
    label "czworon&#243;g"
  ]
  node [
    id 2077
    label "nieodpowiedzialno&#347;&#263;"
  ]
  node [
    id 2078
    label "g&#322;upota"
  ]
  node [
    id 2079
    label "niedojrza&#322;o&#347;&#263;"
  ]
  node [
    id 2080
    label "charakterystyka"
  ]
  node [
    id 2081
    label "m&#322;ot"
  ]
  node [
    id 2082
    label "znak"
  ]
  node [
    id 2083
    label "drzewo"
  ]
  node [
    id 2084
    label "pr&#243;ba"
  ]
  node [
    id 2085
    label "attribute"
  ]
  node [
    id 2086
    label "marka"
  ]
  node [
    id 2087
    label "g&#322;upstwo"
  ]
  node [
    id 2088
    label "pomroczno&#347;&#263;_jasna"
  ]
  node [
    id 2089
    label "intelekt"
  ]
  node [
    id 2090
    label "osielstwo"
  ]
  node [
    id 2091
    label "sofcik"
  ]
  node [
    id 2092
    label "szczeg&#243;&#322;"
  ]
  node [
    id 2093
    label "g&#243;wno"
  ]
  node [
    id 2094
    label "stupidity"
  ]
  node [
    id 2095
    label "furda"
  ]
  node [
    id 2096
    label "hubryzm"
  ]
  node [
    id 2097
    label "wiek"
  ]
  node [
    id 2098
    label "niegotowo&#347;&#263;"
  ]
  node [
    id 2099
    label "immaturity"
  ]
  node [
    id 2100
    label "compass"
  ]
  node [
    id 2101
    label "korzysta&#263;"
  ]
  node [
    id 2102
    label "appreciation"
  ]
  node [
    id 2103
    label "osi&#261;ga&#263;"
  ]
  node [
    id 2104
    label "dociera&#263;"
  ]
  node [
    id 2105
    label "get"
  ]
  node [
    id 2106
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 2107
    label "mierzy&#263;"
  ]
  node [
    id 2108
    label "u&#380;ywa&#263;"
  ]
  node [
    id 2109
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 2110
    label "exsert"
  ]
  node [
    id 2111
    label "being"
  ]
  node [
    id 2112
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 2113
    label "p&#322;ywa&#263;"
  ]
  node [
    id 2114
    label "run"
  ]
  node [
    id 2115
    label "bangla&#263;"
  ]
  node [
    id 2116
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 2117
    label "przebiega&#263;"
  ]
  node [
    id 2118
    label "wk&#322;ada&#263;"
  ]
  node [
    id 2119
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 2120
    label "carry"
  ]
  node [
    id 2121
    label "bywa&#263;"
  ]
  node [
    id 2122
    label "dziama&#263;"
  ]
  node [
    id 2123
    label "stara&#263;_si&#281;"
  ]
  node [
    id 2124
    label "para"
  ]
  node [
    id 2125
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 2126
    label "str&#243;j"
  ]
  node [
    id 2127
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 2128
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 2129
    label "krok"
  ]
  node [
    id 2130
    label "tryb"
  ]
  node [
    id 2131
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 2132
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 2133
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 2134
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 2135
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 2136
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 2137
    label "Ohio"
  ]
  node [
    id 2138
    label "wci&#281;cie"
  ]
  node [
    id 2139
    label "Nowy_York"
  ]
  node [
    id 2140
    label "samopoczucie"
  ]
  node [
    id 2141
    label "Illinois"
  ]
  node [
    id 2142
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 2143
    label "state"
  ]
  node [
    id 2144
    label "Jukatan"
  ]
  node [
    id 2145
    label "Kalifornia"
  ]
  node [
    id 2146
    label "Wirginia"
  ]
  node [
    id 2147
    label "wektor"
  ]
  node [
    id 2148
    label "Goa"
  ]
  node [
    id 2149
    label "Teksas"
  ]
  node [
    id 2150
    label "Waszyngton"
  ]
  node [
    id 2151
    label "Massachusetts"
  ]
  node [
    id 2152
    label "Alaska"
  ]
  node [
    id 2153
    label "Arakan"
  ]
  node [
    id 2154
    label "Hawaje"
  ]
  node [
    id 2155
    label "Maryland"
  ]
  node [
    id 2156
    label "Michigan"
  ]
  node [
    id 2157
    label "Arizona"
  ]
  node [
    id 2158
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 2159
    label "Georgia"
  ]
  node [
    id 2160
    label "poziom"
  ]
  node [
    id 2161
    label "Pensylwania"
  ]
  node [
    id 2162
    label "shape"
  ]
  node [
    id 2163
    label "Luizjana"
  ]
  node [
    id 2164
    label "Nowy_Meksyk"
  ]
  node [
    id 2165
    label "Alabama"
  ]
  node [
    id 2166
    label "Kansas"
  ]
  node [
    id 2167
    label "Oregon"
  ]
  node [
    id 2168
    label "Oklahoma"
  ]
  node [
    id 2169
    label "Floryda"
  ]
  node [
    id 2170
    label "jednostka_administracyjna"
  ]
  node [
    id 2171
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 2172
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 2173
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 2174
    label "indentation"
  ]
  node [
    id 2175
    label "zjedzenie"
  ]
  node [
    id 2176
    label "snub"
  ]
  node [
    id 2177
    label "warunek_lokalowy"
  ]
  node [
    id 2178
    label "plac"
  ]
  node [
    id 2179
    label "location"
  ]
  node [
    id 2180
    label "uwaga"
  ]
  node [
    id 2181
    label "status"
  ]
  node [
    id 2182
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 2183
    label "cia&#322;o"
  ]
  node [
    id 2184
    label "rz&#261;d"
  ]
  node [
    id 2185
    label "sk&#322;adnik"
  ]
  node [
    id 2186
    label "warunki"
  ]
  node [
    id 2187
    label "p&#322;aszczyzna"
  ]
  node [
    id 2188
    label "przek&#322;adaniec"
  ]
  node [
    id 2189
    label "covering"
  ]
  node [
    id 2190
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 2191
    label "podwarstwa"
  ]
  node [
    id 2192
    label "dyspozycja"
  ]
  node [
    id 2193
    label "forma"
  ]
  node [
    id 2194
    label "obiekt_matematyczny"
  ]
  node [
    id 2195
    label "zwrot_wektora"
  ]
  node [
    id 2196
    label "vector"
  ]
  node [
    id 2197
    label "parametryzacja"
  ]
  node [
    id 2198
    label "kwas_deoksyrybonukleinowy"
  ]
  node [
    id 2199
    label "po&#322;o&#380;enie"
  ]
  node [
    id 2200
    label "sprawa"
  ]
  node [
    id 2201
    label "ust&#281;p"
  ]
  node [
    id 2202
    label "problemat"
  ]
  node [
    id 2203
    label "plamka"
  ]
  node [
    id 2204
    label "stopie&#324;_pisma"
  ]
  node [
    id 2205
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 2206
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 2207
    label "mark"
  ]
  node [
    id 2208
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 2209
    label "prosta"
  ]
  node [
    id 2210
    label "problematyka"
  ]
  node [
    id 2211
    label "zapunktowa&#263;"
  ]
  node [
    id 2212
    label "podpunkt"
  ]
  node [
    id 2213
    label "wojsko"
  ]
  node [
    id 2214
    label "kres"
  ]
  node [
    id 2215
    label "point"
  ]
  node [
    id 2216
    label "jako&#347;&#263;"
  ]
  node [
    id 2217
    label "wyk&#322;adnik"
  ]
  node [
    id 2218
    label "szczebel"
  ]
  node [
    id 2219
    label "budynek"
  ]
  node [
    id 2220
    label "wysoko&#347;&#263;"
  ]
  node [
    id 2221
    label "ranga"
  ]
  node [
    id 2222
    label "rozmiar"
  ]
  node [
    id 2223
    label "part"
  ]
  node [
    id 2224
    label "USA"
  ]
  node [
    id 2225
    label "Belize"
  ]
  node [
    id 2226
    label "Meksyk"
  ]
  node [
    id 2227
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 2228
    label "Indie_Portugalskie"
  ]
  node [
    id 2229
    label "Birma"
  ]
  node [
    id 2230
    label "Polinezja"
  ]
  node [
    id 2231
    label "Aleuty"
  ]
  node [
    id 2232
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 2233
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 2234
    label "poddawa&#263;"
  ]
  node [
    id 2235
    label "dotyczy&#263;"
  ]
  node [
    id 2236
    label "use"
  ]
  node [
    id 2237
    label "introduce"
  ]
  node [
    id 2238
    label "opowiada&#263;"
  ]
  node [
    id 2239
    label "krzywdzi&#263;"
  ]
  node [
    id 2240
    label "organizowa&#263;"
  ]
  node [
    id 2241
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 2242
    label "czyni&#263;"
  ]
  node [
    id 2243
    label "stylizowa&#263;"
  ]
  node [
    id 2244
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 2245
    label "falowa&#263;"
  ]
  node [
    id 2246
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 2247
    label "peddle"
  ]
  node [
    id 2248
    label "wydala&#263;"
  ]
  node [
    id 2249
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 2250
    label "tentegowa&#263;"
  ]
  node [
    id 2251
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 2252
    label "urz&#261;dza&#263;"
  ]
  node [
    id 2253
    label "oszukiwa&#263;"
  ]
  node [
    id 2254
    label "work"
  ]
  node [
    id 2255
    label "ukazywa&#263;"
  ]
  node [
    id 2256
    label "przerabia&#263;"
  ]
  node [
    id 2257
    label "podpowiada&#263;"
  ]
  node [
    id 2258
    label "render"
  ]
  node [
    id 2259
    label "decydowa&#263;"
  ]
  node [
    id 2260
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 2261
    label "rezygnowa&#263;"
  ]
  node [
    id 2262
    label "bargain"
  ]
  node [
    id 2263
    label "tycze&#263;"
  ]
  node [
    id 2264
    label "defenestracja"
  ]
  node [
    id 2265
    label "agonia"
  ]
  node [
    id 2266
    label "mogi&#322;a"
  ]
  node [
    id 2267
    label "kres_&#380;ycia"
  ]
  node [
    id 2268
    label "upadek"
  ]
  node [
    id 2269
    label "szeol"
  ]
  node [
    id 2270
    label "pogrzebanie"
  ]
  node [
    id 2271
    label "istota_nadprzyrodzona"
  ]
  node [
    id 2272
    label "&#380;a&#322;oba"
  ]
  node [
    id 2273
    label "pogrzeb"
  ]
  node [
    id 2274
    label "zabicie"
  ]
  node [
    id 2275
    label "ostatnie_podrygi"
  ]
  node [
    id 2276
    label "koniec"
  ]
  node [
    id 2277
    label "kondycja"
  ]
  node [
    id 2278
    label "pogorszenie"
  ]
  node [
    id 2279
    label "inclination"
  ]
  node [
    id 2280
    label "death"
  ]
  node [
    id 2281
    label "zmierzch"
  ]
  node [
    id 2282
    label "spocz&#261;&#263;"
  ]
  node [
    id 2283
    label "spocz&#281;cie"
  ]
  node [
    id 2284
    label "pochowanie"
  ]
  node [
    id 2285
    label "spoczywa&#263;"
  ]
  node [
    id 2286
    label "park_sztywnych"
  ]
  node [
    id 2287
    label "pomnik"
  ]
  node [
    id 2288
    label "nagrobek"
  ]
  node [
    id 2289
    label "prochowisko"
  ]
  node [
    id 2290
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 2291
    label "spoczywanie"
  ]
  node [
    id 2292
    label "za&#347;wiaty"
  ]
  node [
    id 2293
    label "piek&#322;o"
  ]
  node [
    id 2294
    label "judaizm"
  ]
  node [
    id 2295
    label "wyrzucenie"
  ]
  node [
    id 2296
    label "defenestration"
  ]
  node [
    id 2297
    label "zaj&#347;cie"
  ]
  node [
    id 2298
    label "&#380;al"
  ]
  node [
    id 2299
    label "paznokie&#263;"
  ]
  node [
    id 2300
    label "symbol"
  ]
  node [
    id 2301
    label "kir"
  ]
  node [
    id 2302
    label "brud"
  ]
  node [
    id 2303
    label "burying"
  ]
  node [
    id 2304
    label "zasypanie"
  ]
  node [
    id 2305
    label "zw&#322;oki"
  ]
  node [
    id 2306
    label "burial"
  ]
  node [
    id 2307
    label "w&#322;o&#380;enie"
  ]
  node [
    id 2308
    label "porobienie"
  ]
  node [
    id 2309
    label "gr&#243;b"
  ]
  node [
    id 2310
    label "uniemo&#380;liwienie"
  ]
  node [
    id 2311
    label "destruction"
  ]
  node [
    id 2312
    label "zabrzmienie"
  ]
  node [
    id 2313
    label "skrzywdzenie"
  ]
  node [
    id 2314
    label "pozabijanie"
  ]
  node [
    id 2315
    label "zniszczenie"
  ]
  node [
    id 2316
    label "zaszkodzenie"
  ]
  node [
    id 2317
    label "usuni&#281;cie"
  ]
  node [
    id 2318
    label "killing"
  ]
  node [
    id 2319
    label "umarcie"
  ]
  node [
    id 2320
    label "granie"
  ]
  node [
    id 2321
    label "zamkni&#281;cie"
  ]
  node [
    id 2322
    label "compaction"
  ]
  node [
    id 2323
    label "niepowodzenie"
  ]
  node [
    id 2324
    label "stypa"
  ]
  node [
    id 2325
    label "pusta_noc"
  ]
  node [
    id 2326
    label "grabarz"
  ]
  node [
    id 2327
    label "ostatnia_pos&#322;uga"
  ]
  node [
    id 2328
    label "obrz&#281;d"
  ]
  node [
    id 2329
    label "raj_utracony"
  ]
  node [
    id 2330
    label "umieranie"
  ]
  node [
    id 2331
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 2332
    label "prze&#380;ywanie"
  ]
  node [
    id 2333
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 2334
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 2335
    label "po&#322;&#243;g"
  ]
  node [
    id 2336
    label "okres_noworodkowy"
  ]
  node [
    id 2337
    label "wiek_matuzalemowy"
  ]
  node [
    id 2338
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 2339
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 2340
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 2341
    label "do&#380;ywanie"
  ]
  node [
    id 2342
    label "dzieci&#324;stwo"
  ]
  node [
    id 2343
    label "andropauza"
  ]
  node [
    id 2344
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 2345
    label "rozw&#243;j"
  ]
  node [
    id 2346
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 2347
    label "koleje_losu"
  ]
  node [
    id 2348
    label "zegar_biologiczny"
  ]
  node [
    id 2349
    label "szwung"
  ]
  node [
    id 2350
    label "przebywanie"
  ]
  node [
    id 2351
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 2352
    label "niemowl&#281;ctwo"
  ]
  node [
    id 2353
    label "&#380;ywy"
  ]
  node [
    id 2354
    label "life"
  ]
  node [
    id 2355
    label "staro&#347;&#263;"
  ]
  node [
    id 2356
    label "energy"
  ]
  node [
    id 2357
    label "powa&#380;nie"
  ]
  node [
    id 2358
    label "seryjny"
  ]
  node [
    id 2359
    label "naprawd&#281;"
  ]
  node [
    id 2360
    label "gro&#378;nie"
  ]
  node [
    id 2361
    label "bardzo"
  ]
  node [
    id 2362
    label "niema&#322;o"
  ]
  node [
    id 2363
    label "powa&#380;ny"
  ]
  node [
    id 2364
    label "ci&#281;&#380;ki"
  ]
  node [
    id 2365
    label "ci&#281;&#380;ko"
  ]
  node [
    id 2366
    label "notoryczny"
  ]
  node [
    id 2367
    label "powtarzalny"
  ]
  node [
    id 2368
    label "seryjnie"
  ]
  node [
    id 2369
    label "wielocz&#281;&#347;ciowy"
  ]
  node [
    id 2370
    label "nieoryginalny"
  ]
  node [
    id 2371
    label "masowy"
  ]
  node [
    id 2372
    label "plon"
  ]
  node [
    id 2373
    label "surrender"
  ]
  node [
    id 2374
    label "kojarzy&#263;"
  ]
  node [
    id 2375
    label "d&#378;wi&#281;k"
  ]
  node [
    id 2376
    label "reszta"
  ]
  node [
    id 2377
    label "zapach"
  ]
  node [
    id 2378
    label "wydawnictwo"
  ]
  node [
    id 2379
    label "wiano"
  ]
  node [
    id 2380
    label "produkcja"
  ]
  node [
    id 2381
    label "wprowadza&#263;"
  ]
  node [
    id 2382
    label "podawa&#263;"
  ]
  node [
    id 2383
    label "ujawnia&#263;"
  ]
  node [
    id 2384
    label "denuncjowa&#263;"
  ]
  node [
    id 2385
    label "panna_na_wydaniu"
  ]
  node [
    id 2386
    label "wytwarza&#263;"
  ]
  node [
    id 2387
    label "train"
  ]
  node [
    id 2388
    label "przekazywa&#263;"
  ]
  node [
    id 2389
    label "&#322;adowa&#263;"
  ]
  node [
    id 2390
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 2391
    label "przeznacza&#263;"
  ]
  node [
    id 2392
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 2393
    label "obiecywa&#263;"
  ]
  node [
    id 2394
    label "odst&#281;powa&#263;"
  ]
  node [
    id 2395
    label "tender"
  ]
  node [
    id 2396
    label "rap"
  ]
  node [
    id 2397
    label "umieszcza&#263;"
  ]
  node [
    id 2398
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 2399
    label "t&#322;uc"
  ]
  node [
    id 2400
    label "wpiernicza&#263;"
  ]
  node [
    id 2401
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 2402
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 2403
    label "p&#322;aci&#263;"
  ]
  node [
    id 2404
    label "hold_out"
  ]
  node [
    id 2405
    label "nalewa&#263;"
  ]
  node [
    id 2406
    label "rynek"
  ]
  node [
    id 2407
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 2408
    label "zaczyna&#263;"
  ]
  node [
    id 2409
    label "wpisywa&#263;"
  ]
  node [
    id 2410
    label "wchodzi&#263;"
  ]
  node [
    id 2411
    label "take"
  ]
  node [
    id 2412
    label "zapoznawa&#263;"
  ]
  node [
    id 2413
    label "inflict"
  ]
  node [
    id 2414
    label "schodzi&#263;"
  ]
  node [
    id 2415
    label "induct"
  ]
  node [
    id 2416
    label "begin"
  ]
  node [
    id 2417
    label "doprowadza&#263;"
  ]
  node [
    id 2418
    label "create"
  ]
  node [
    id 2419
    label "demaskator"
  ]
  node [
    id 2420
    label "dostrzega&#263;"
  ]
  node [
    id 2421
    label "objawia&#263;"
  ]
  node [
    id 2422
    label "unwrap"
  ]
  node [
    id 2423
    label "indicate"
  ]
  node [
    id 2424
    label "donosi&#263;"
  ]
  node [
    id 2425
    label "inform"
  ]
  node [
    id 2426
    label "zaskakiwa&#263;"
  ]
  node [
    id 2427
    label "cover"
  ]
  node [
    id 2428
    label "rozumie&#263;"
  ]
  node [
    id 2429
    label "swat"
  ]
  node [
    id 2430
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 2431
    label "relate"
  ]
  node [
    id 2432
    label "wyznawa&#263;"
  ]
  node [
    id 2433
    label "oddawa&#263;"
  ]
  node [
    id 2434
    label "confide"
  ]
  node [
    id 2435
    label "zleca&#263;"
  ]
  node [
    id 2436
    label "ufa&#263;"
  ]
  node [
    id 2437
    label "command"
  ]
  node [
    id 2438
    label "tenis"
  ]
  node [
    id 2439
    label "deal"
  ]
  node [
    id 2440
    label "rozgrywa&#263;"
  ]
  node [
    id 2441
    label "kelner"
  ]
  node [
    id 2442
    label "siatk&#243;wka"
  ]
  node [
    id 2443
    label "faszerowa&#263;"
  ]
  node [
    id 2444
    label "serwowa&#263;"
  ]
  node [
    id 2445
    label "kwota"
  ]
  node [
    id 2446
    label "wydanie"
  ]
  node [
    id 2447
    label "remainder"
  ]
  node [
    id 2448
    label "pozosta&#322;y"
  ]
  node [
    id 2449
    label "wyda&#263;"
  ]
  node [
    id 2450
    label "impreza"
  ]
  node [
    id 2451
    label "realizacja"
  ]
  node [
    id 2452
    label "tingel-tangel"
  ]
  node [
    id 2453
    label "monta&#380;"
  ]
  node [
    id 2454
    label "postprodukcja"
  ]
  node [
    id 2455
    label "fabrication"
  ]
  node [
    id 2456
    label "product"
  ]
  node [
    id 2457
    label "uzysk"
  ]
  node [
    id 2458
    label "odtworzenie"
  ]
  node [
    id 2459
    label "dorobek"
  ]
  node [
    id 2460
    label "kreacja"
  ]
  node [
    id 2461
    label "trema"
  ]
  node [
    id 2462
    label "creation"
  ]
  node [
    id 2463
    label "kooperowa&#263;"
  ]
  node [
    id 2464
    label "return"
  ]
  node [
    id 2465
    label "metr"
  ]
  node [
    id 2466
    label "naturalia"
  ]
  node [
    id 2467
    label "wypaplanie"
  ]
  node [
    id 2468
    label "enigmat"
  ]
  node [
    id 2469
    label "wiedza"
  ]
  node [
    id 2470
    label "secret"
  ]
  node [
    id 2471
    label "obowi&#261;zek"
  ]
  node [
    id 2472
    label "dyskrecja"
  ]
  node [
    id 2473
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 2474
    label "taj&#324;"
  ]
  node [
    id 2475
    label "posa&#380;ek"
  ]
  node [
    id 2476
    label "wydawa&#263;_za_m&#261;&#380;"
  ]
  node [
    id 2477
    label "wyda&#263;_za_m&#261;&#380;"
  ]
  node [
    id 2478
    label "debit"
  ]
  node [
    id 2479
    label "redaktor"
  ]
  node [
    id 2480
    label "druk"
  ]
  node [
    id 2481
    label "redakcja"
  ]
  node [
    id 2482
    label "szata_graficzna"
  ]
  node [
    id 2483
    label "firma"
  ]
  node [
    id 2484
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 2485
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 2486
    label "poster"
  ]
  node [
    id 2487
    label "phone"
  ]
  node [
    id 2488
    label "intonacja"
  ]
  node [
    id 2489
    label "note"
  ]
  node [
    id 2490
    label "onomatopeja"
  ]
  node [
    id 2491
    label "modalizm"
  ]
  node [
    id 2492
    label "nadlecenie"
  ]
  node [
    id 2493
    label "sound"
  ]
  node [
    id 2494
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 2495
    label "solmizacja"
  ]
  node [
    id 2496
    label "seria"
  ]
  node [
    id 2497
    label "dobiec"
  ]
  node [
    id 2498
    label "transmiter"
  ]
  node [
    id 2499
    label "heksachord"
  ]
  node [
    id 2500
    label "akcent"
  ]
  node [
    id 2501
    label "repetycja"
  ]
  node [
    id 2502
    label "brzmienie"
  ]
  node [
    id 2503
    label "liczba_kwantowa"
  ]
  node [
    id 2504
    label "kosmetyk"
  ]
  node [
    id 2505
    label "ciasto"
  ]
  node [
    id 2506
    label "aromat"
  ]
  node [
    id 2507
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 2508
    label "puff"
  ]
  node [
    id 2509
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 2510
    label "przyprawa"
  ]
  node [
    id 2511
    label "upojno&#347;&#263;"
  ]
  node [
    id 2512
    label "owiewanie"
  ]
  node [
    id 2513
    label "smak"
  ]
  node [
    id 2514
    label "&#347;mieszny"
  ]
  node [
    id 2515
    label "bezwolny"
  ]
  node [
    id 2516
    label "g&#322;upienie"
  ]
  node [
    id 2517
    label "mondzio&#322;"
  ]
  node [
    id 2518
    label "bezsensowny"
  ]
  node [
    id 2519
    label "niem&#261;dry"
  ]
  node [
    id 2520
    label "niezr&#281;czny"
  ]
  node [
    id 2521
    label "g&#322;uptas"
  ]
  node [
    id 2522
    label "zg&#322;upienie"
  ]
  node [
    id 2523
    label "g&#322;upiec"
  ]
  node [
    id 2524
    label "uprzykrzony"
  ]
  node [
    id 2525
    label "bezcelowy"
  ]
  node [
    id 2526
    label "ma&#322;y"
  ]
  node [
    id 2527
    label "g&#322;upio"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 767
  ]
  edge [
    source 14
    target 768
  ]
  edge [
    source 14
    target 769
  ]
  edge [
    source 14
    target 770
  ]
  edge [
    source 14
    target 771
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 42
  ]
  edge [
    source 14
    target 773
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 14
    target 777
  ]
  edge [
    source 14
    target 778
  ]
  edge [
    source 14
    target 779
  ]
  edge [
    source 14
    target 780
  ]
  edge [
    source 14
    target 781
  ]
  edge [
    source 14
    target 782
  ]
  edge [
    source 14
    target 783
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 479
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 783
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 44
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 518
  ]
  edge [
    source 17
    target 519
  ]
  edge [
    source 17
    target 520
  ]
  edge [
    source 17
    target 521
  ]
  edge [
    source 17
    target 522
  ]
  edge [
    source 17
    target 523
  ]
  edge [
    source 17
    target 524
  ]
  edge [
    source 17
    target 525
  ]
  edge [
    source 17
    target 526
  ]
  edge [
    source 17
    target 466
  ]
  edge [
    source 17
    target 527
  ]
  edge [
    source 17
    target 528
  ]
  edge [
    source 17
    target 27
  ]
  edge [
    source 17
    target 529
  ]
  edge [
    source 17
    target 530
  ]
  edge [
    source 17
    target 531
  ]
  edge [
    source 17
    target 532
  ]
  edge [
    source 17
    target 533
  ]
  edge [
    source 17
    target 534
  ]
  edge [
    source 17
    target 535
  ]
  edge [
    source 17
    target 536
  ]
  edge [
    source 17
    target 537
  ]
  edge [
    source 17
    target 538
  ]
  edge [
    source 17
    target 539
  ]
  edge [
    source 17
    target 540
  ]
  edge [
    source 17
    target 541
  ]
  edge [
    source 17
    target 542
  ]
  edge [
    source 17
    target 543
  ]
  edge [
    source 17
    target 544
  ]
  edge [
    source 17
    target 545
  ]
  edge [
    source 17
    target 546
  ]
  edge [
    source 17
    target 547
  ]
  edge [
    source 17
    target 548
  ]
  edge [
    source 17
    target 549
  ]
  edge [
    source 17
    target 550
  ]
  edge [
    source 17
    target 551
  ]
  edge [
    source 17
    target 552
  ]
  edge [
    source 17
    target 553
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 493
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 830
  ]
  edge [
    source 18
    target 831
  ]
  edge [
    source 18
    target 832
  ]
  edge [
    source 18
    target 833
  ]
  edge [
    source 18
    target 834
  ]
  edge [
    source 18
    target 835
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 791
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 928
  ]
  edge [
    source 20
    target 929
  ]
  edge [
    source 20
    target 43
  ]
  edge [
    source 20
    target 930
  ]
  edge [
    source 20
    target 931
  ]
  edge [
    source 20
    target 932
  ]
  edge [
    source 21
    target 933
  ]
  edge [
    source 21
    target 934
  ]
  edge [
    source 21
    target 935
  ]
  edge [
    source 21
    target 936
  ]
  edge [
    source 21
    target 937
  ]
  edge [
    source 21
    target 938
  ]
  edge [
    source 21
    target 939
  ]
  edge [
    source 21
    target 940
  ]
  edge [
    source 21
    target 941
  ]
  edge [
    source 21
    target 942
  ]
  edge [
    source 21
    target 943
  ]
  edge [
    source 21
    target 944
  ]
  edge [
    source 21
    target 945
  ]
  edge [
    source 21
    target 946
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 947
  ]
  edge [
    source 22
    target 948
  ]
  edge [
    source 22
    target 949
  ]
  edge [
    source 22
    target 950
  ]
  edge [
    source 22
    target 951
  ]
  edge [
    source 22
    target 952
  ]
  edge [
    source 22
    target 953
  ]
  edge [
    source 22
    target 954
  ]
  edge [
    source 22
    target 955
  ]
  edge [
    source 22
    target 956
  ]
  edge [
    source 22
    target 957
  ]
  edge [
    source 22
    target 958
  ]
  edge [
    source 22
    target 959
  ]
  edge [
    source 22
    target 960
  ]
  edge [
    source 22
    target 961
  ]
  edge [
    source 22
    target 962
  ]
  edge [
    source 22
    target 43
  ]
  edge [
    source 22
    target 963
  ]
  edge [
    source 22
    target 964
  ]
  edge [
    source 22
    target 965
  ]
  edge [
    source 22
    target 966
  ]
  edge [
    source 22
    target 967
  ]
  edge [
    source 22
    target 968
  ]
  edge [
    source 22
    target 969
  ]
  edge [
    source 22
    target 970
  ]
  edge [
    source 22
    target 971
  ]
  edge [
    source 22
    target 972
  ]
  edge [
    source 22
    target 973
  ]
  edge [
    source 22
    target 100
  ]
  edge [
    source 22
    target 974
  ]
  edge [
    source 22
    target 975
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 976
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 977
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 24
    target 978
  ]
  edge [
    source 24
    target 979
  ]
  edge [
    source 24
    target 65
  ]
  edge [
    source 24
    target 980
  ]
  edge [
    source 24
    target 981
  ]
  edge [
    source 24
    target 982
  ]
  edge [
    source 24
    target 983
  ]
  edge [
    source 24
    target 984
  ]
  edge [
    source 24
    target 985
  ]
  edge [
    source 24
    target 986
  ]
  edge [
    source 24
    target 987
  ]
  edge [
    source 24
    target 988
  ]
  edge [
    source 24
    target 989
  ]
  edge [
    source 24
    target 990
  ]
  edge [
    source 24
    target 991
  ]
  edge [
    source 24
    target 992
  ]
  edge [
    source 24
    target 993
  ]
  edge [
    source 24
    target 994
  ]
  edge [
    source 24
    target 995
  ]
  edge [
    source 24
    target 996
  ]
  edge [
    source 24
    target 997
  ]
  edge [
    source 24
    target 998
  ]
  edge [
    source 24
    target 999
  ]
  edge [
    source 24
    target 1000
  ]
  edge [
    source 24
    target 1001
  ]
  edge [
    source 24
    target 1002
  ]
  edge [
    source 24
    target 965
  ]
  edge [
    source 24
    target 1003
  ]
  edge [
    source 24
    target 1004
  ]
  edge [
    source 24
    target 1005
  ]
  edge [
    source 24
    target 1006
  ]
  edge [
    source 24
    target 1007
  ]
  edge [
    source 24
    target 1008
  ]
  edge [
    source 24
    target 1009
  ]
  edge [
    source 24
    target 1010
  ]
  edge [
    source 24
    target 1011
  ]
  edge [
    source 24
    target 1012
  ]
  edge [
    source 24
    target 1013
  ]
  edge [
    source 24
    target 1014
  ]
  edge [
    source 24
    target 1015
  ]
  edge [
    source 24
    target 1016
  ]
  edge [
    source 24
    target 1017
  ]
  edge [
    source 24
    target 754
  ]
  edge [
    source 24
    target 1018
  ]
  edge [
    source 24
    target 1019
  ]
  edge [
    source 24
    target 948
  ]
  edge [
    source 24
    target 1020
  ]
  edge [
    source 24
    target 1021
  ]
  edge [
    source 24
    target 1022
  ]
  edge [
    source 24
    target 1023
  ]
  edge [
    source 24
    target 1024
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 42
  ]
  edge [
    source 25
    target 1025
  ]
  edge [
    source 25
    target 1026
  ]
  edge [
    source 25
    target 1027
  ]
  edge [
    source 25
    target 1028
  ]
  edge [
    source 25
    target 1029
  ]
  edge [
    source 25
    target 1030
  ]
  edge [
    source 25
    target 1031
  ]
  edge [
    source 25
    target 1032
  ]
  edge [
    source 25
    target 1033
  ]
  edge [
    source 25
    target 1034
  ]
  edge [
    source 25
    target 1035
  ]
  edge [
    source 25
    target 1036
  ]
  edge [
    source 25
    target 1037
  ]
  edge [
    source 25
    target 1038
  ]
  edge [
    source 25
    target 1039
  ]
  edge [
    source 25
    target 1040
  ]
  edge [
    source 25
    target 1041
  ]
  edge [
    source 25
    target 1042
  ]
  edge [
    source 25
    target 1043
  ]
  edge [
    source 25
    target 1044
  ]
  edge [
    source 25
    target 1045
  ]
  edge [
    source 25
    target 1046
  ]
  edge [
    source 25
    target 1047
  ]
  edge [
    source 25
    target 43
  ]
  edge [
    source 25
    target 1048
  ]
  edge [
    source 25
    target 1049
  ]
  edge [
    source 25
    target 1050
  ]
  edge [
    source 25
    target 771
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1051
  ]
  edge [
    source 26
    target 1052
  ]
  edge [
    source 26
    target 1053
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1054
  ]
  edge [
    source 27
    target 518
  ]
  edge [
    source 27
    target 519
  ]
  edge [
    source 27
    target 520
  ]
  edge [
    source 27
    target 521
  ]
  edge [
    source 27
    target 522
  ]
  edge [
    source 27
    target 523
  ]
  edge [
    source 27
    target 524
  ]
  edge [
    source 27
    target 525
  ]
  edge [
    source 27
    target 526
  ]
  edge [
    source 27
    target 466
  ]
  edge [
    source 27
    target 527
  ]
  edge [
    source 27
    target 528
  ]
  edge [
    source 27
    target 529
  ]
  edge [
    source 27
    target 530
  ]
  edge [
    source 27
    target 531
  ]
  edge [
    source 27
    target 532
  ]
  edge [
    source 27
    target 533
  ]
  edge [
    source 27
    target 534
  ]
  edge [
    source 27
    target 535
  ]
  edge [
    source 27
    target 536
  ]
  edge [
    source 27
    target 537
  ]
  edge [
    source 27
    target 538
  ]
  edge [
    source 27
    target 539
  ]
  edge [
    source 27
    target 540
  ]
  edge [
    source 27
    target 541
  ]
  edge [
    source 27
    target 542
  ]
  edge [
    source 27
    target 543
  ]
  edge [
    source 27
    target 544
  ]
  edge [
    source 27
    target 545
  ]
  edge [
    source 27
    target 546
  ]
  edge [
    source 27
    target 547
  ]
  edge [
    source 27
    target 548
  ]
  edge [
    source 27
    target 549
  ]
  edge [
    source 27
    target 550
  ]
  edge [
    source 27
    target 551
  ]
  edge [
    source 27
    target 552
  ]
  edge [
    source 27
    target 553
  ]
  edge [
    source 27
    target 43
  ]
  edge [
    source 27
    target 45
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1055
  ]
  edge [
    source 28
    target 1056
  ]
  edge [
    source 28
    target 1057
  ]
  edge [
    source 28
    target 1058
  ]
  edge [
    source 28
    target 1059
  ]
  edge [
    source 28
    target 1060
  ]
  edge [
    source 28
    target 1061
  ]
  edge [
    source 28
    target 1062
  ]
  edge [
    source 28
    target 1063
  ]
  edge [
    source 28
    target 1064
  ]
  edge [
    source 28
    target 1065
  ]
  edge [
    source 28
    target 1066
  ]
  edge [
    source 28
    target 1067
  ]
  edge [
    source 28
    target 1068
  ]
  edge [
    source 28
    target 1069
  ]
  edge [
    source 28
    target 1070
  ]
  edge [
    source 28
    target 1071
  ]
  edge [
    source 28
    target 457
  ]
  edge [
    source 28
    target 1072
  ]
  edge [
    source 28
    target 1073
  ]
  edge [
    source 28
    target 1074
  ]
  edge [
    source 28
    target 1075
  ]
  edge [
    source 28
    target 1076
  ]
  edge [
    source 28
    target 1077
  ]
  edge [
    source 28
    target 1078
  ]
  edge [
    source 28
    target 1079
  ]
  edge [
    source 28
    target 1080
  ]
  edge [
    source 28
    target 1081
  ]
  edge [
    source 28
    target 1082
  ]
  edge [
    source 28
    target 1083
  ]
  edge [
    source 28
    target 1084
  ]
  edge [
    source 28
    target 1085
  ]
  edge [
    source 28
    target 1086
  ]
  edge [
    source 28
    target 1087
  ]
  edge [
    source 28
    target 1088
  ]
  edge [
    source 28
    target 1089
  ]
  edge [
    source 28
    target 1090
  ]
  edge [
    source 28
    target 1091
  ]
  edge [
    source 28
    target 1092
  ]
  edge [
    source 28
    target 1093
  ]
  edge [
    source 28
    target 1094
  ]
  edge [
    source 28
    target 1095
  ]
  edge [
    source 28
    target 1096
  ]
  edge [
    source 28
    target 1097
  ]
  edge [
    source 28
    target 1098
  ]
  edge [
    source 28
    target 1099
  ]
  edge [
    source 28
    target 1100
  ]
  edge [
    source 28
    target 1101
  ]
  edge [
    source 28
    target 1102
  ]
  edge [
    source 28
    target 1103
  ]
  edge [
    source 28
    target 1104
  ]
  edge [
    source 28
    target 1105
  ]
  edge [
    source 28
    target 1106
  ]
  edge [
    source 28
    target 1107
  ]
  edge [
    source 28
    target 1108
  ]
  edge [
    source 28
    target 1109
  ]
  edge [
    source 28
    target 1110
  ]
  edge [
    source 28
    target 1111
  ]
  edge [
    source 28
    target 1112
  ]
  edge [
    source 28
    target 1113
  ]
  edge [
    source 28
    target 1114
  ]
  edge [
    source 28
    target 1115
  ]
  edge [
    source 28
    target 1116
  ]
  edge [
    source 28
    target 1117
  ]
  edge [
    source 28
    target 1118
  ]
  edge [
    source 28
    target 456
  ]
  edge [
    source 28
    target 1119
  ]
  edge [
    source 28
    target 1120
  ]
  edge [
    source 28
    target 448
  ]
  edge [
    source 28
    target 1121
  ]
  edge [
    source 28
    target 1122
  ]
  edge [
    source 28
    target 1123
  ]
  edge [
    source 28
    target 1124
  ]
  edge [
    source 28
    target 1125
  ]
  edge [
    source 28
    target 1126
  ]
  edge [
    source 28
    target 1127
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 37
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1128
  ]
  edge [
    source 29
    target 1129
  ]
  edge [
    source 29
    target 1130
  ]
  edge [
    source 29
    target 1131
  ]
  edge [
    source 29
    target 1132
  ]
  edge [
    source 29
    target 1133
  ]
  edge [
    source 29
    target 1134
  ]
  edge [
    source 29
    target 1135
  ]
  edge [
    source 29
    target 1136
  ]
  edge [
    source 29
    target 1137
  ]
  edge [
    source 29
    target 781
  ]
  edge [
    source 29
    target 1138
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1139
  ]
  edge [
    source 30
    target 1140
  ]
  edge [
    source 30
    target 1141
  ]
  edge [
    source 30
    target 1142
  ]
  edge [
    source 30
    target 1071
  ]
  edge [
    source 30
    target 1143
  ]
  edge [
    source 30
    target 829
  ]
  edge [
    source 30
    target 1144
  ]
  edge [
    source 30
    target 1145
  ]
  edge [
    source 30
    target 1146
  ]
  edge [
    source 30
    target 1147
  ]
  edge [
    source 30
    target 1148
  ]
  edge [
    source 30
    target 752
  ]
  edge [
    source 30
    target 1149
  ]
  edge [
    source 30
    target 1150
  ]
  edge [
    source 30
    target 1151
  ]
  edge [
    source 30
    target 1152
  ]
  edge [
    source 30
    target 1153
  ]
  edge [
    source 30
    target 1154
  ]
  edge [
    source 30
    target 1155
  ]
  edge [
    source 30
    target 1156
  ]
  edge [
    source 30
    target 1157
  ]
  edge [
    source 30
    target 1158
  ]
  edge [
    source 30
    target 1159
  ]
  edge [
    source 30
    target 1160
  ]
  edge [
    source 30
    target 1161
  ]
  edge [
    source 30
    target 1162
  ]
  edge [
    source 30
    target 1163
  ]
  edge [
    source 30
    target 507
  ]
  edge [
    source 30
    target 1164
  ]
  edge [
    source 30
    target 1165
  ]
  edge [
    source 30
    target 919
  ]
  edge [
    source 30
    target 1166
  ]
  edge [
    source 30
    target 964
  ]
  edge [
    source 30
    target 1167
  ]
  edge [
    source 30
    target 966
  ]
  edge [
    source 30
    target 1168
  ]
  edge [
    source 30
    target 1169
  ]
  edge [
    source 30
    target 1170
  ]
  edge [
    source 30
    target 1171
  ]
  edge [
    source 30
    target 1172
  ]
  edge [
    source 30
    target 826
  ]
  edge [
    source 30
    target 272
  ]
  edge [
    source 30
    target 493
  ]
  edge [
    source 30
    target 1173
  ]
  edge [
    source 30
    target 518
  ]
  edge [
    source 30
    target 1174
  ]
  edge [
    source 30
    target 1175
  ]
  edge [
    source 30
    target 1176
  ]
  edge [
    source 30
    target 1177
  ]
  edge [
    source 30
    target 1178
  ]
  edge [
    source 30
    target 1179
  ]
  edge [
    source 30
    target 1180
  ]
  edge [
    source 30
    target 267
  ]
  edge [
    source 30
    target 145
  ]
  edge [
    source 30
    target 1181
  ]
  edge [
    source 30
    target 1182
  ]
  edge [
    source 30
    target 1183
  ]
  edge [
    source 30
    target 1184
  ]
  edge [
    source 30
    target 421
  ]
  edge [
    source 30
    target 1185
  ]
  edge [
    source 30
    target 1186
  ]
  edge [
    source 30
    target 1187
  ]
  edge [
    source 30
    target 1188
  ]
  edge [
    source 30
    target 1189
  ]
  edge [
    source 30
    target 1190
  ]
  edge [
    source 30
    target 1191
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1192
  ]
  edge [
    source 31
    target 1193
  ]
  edge [
    source 31
    target 65
  ]
  edge [
    source 31
    target 1194
  ]
  edge [
    source 31
    target 1195
  ]
  edge [
    source 31
    target 1196
  ]
  edge [
    source 31
    target 1197
  ]
  edge [
    source 31
    target 1198
  ]
  edge [
    source 31
    target 1199
  ]
  edge [
    source 31
    target 1200
  ]
  edge [
    source 31
    target 1201
  ]
  edge [
    source 31
    target 1202
  ]
  edge [
    source 31
    target 1203
  ]
  edge [
    source 31
    target 1204
  ]
  edge [
    source 31
    target 79
  ]
  edge [
    source 31
    target 80
  ]
  edge [
    source 31
    target 81
  ]
  edge [
    source 31
    target 82
  ]
  edge [
    source 31
    target 83
  ]
  edge [
    source 31
    target 84
  ]
  edge [
    source 31
    target 85
  ]
  edge [
    source 31
    target 86
  ]
  edge [
    source 31
    target 87
  ]
  edge [
    source 31
    target 88
  ]
  edge [
    source 31
    target 89
  ]
  edge [
    source 31
    target 90
  ]
  edge [
    source 31
    target 91
  ]
  edge [
    source 31
    target 92
  ]
  edge [
    source 31
    target 93
  ]
  edge [
    source 31
    target 94
  ]
  edge [
    source 31
    target 95
  ]
  edge [
    source 31
    target 96
  ]
  edge [
    source 31
    target 97
  ]
  edge [
    source 31
    target 98
  ]
  edge [
    source 31
    target 99
  ]
  edge [
    source 31
    target 100
  ]
  edge [
    source 31
    target 101
  ]
  edge [
    source 31
    target 102
  ]
  edge [
    source 31
    target 103
  ]
  edge [
    source 31
    target 1205
  ]
  edge [
    source 31
    target 1206
  ]
  edge [
    source 31
    target 1207
  ]
  edge [
    source 31
    target 1208
  ]
  edge [
    source 31
    target 1209
  ]
  edge [
    source 31
    target 1210
  ]
  edge [
    source 31
    target 1211
  ]
  edge [
    source 31
    target 1212
  ]
  edge [
    source 31
    target 1213
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 993
  ]
  edge [
    source 32
    target 65
  ]
  edge [
    source 32
    target 994
  ]
  edge [
    source 32
    target 995
  ]
  edge [
    source 32
    target 996
  ]
  edge [
    source 32
    target 998
  ]
  edge [
    source 32
    target 997
  ]
  edge [
    source 32
    target 999
  ]
  edge [
    source 32
    target 1000
  ]
  edge [
    source 32
    target 1001
  ]
  edge [
    source 32
    target 1002
  ]
  edge [
    source 32
    target 965
  ]
  edge [
    source 32
    target 1003
  ]
  edge [
    source 32
    target 1004
  ]
  edge [
    source 32
    target 1005
  ]
  edge [
    source 32
    target 1006
  ]
  edge [
    source 32
    target 1007
  ]
  edge [
    source 32
    target 1008
  ]
  edge [
    source 32
    target 1009
  ]
  edge [
    source 32
    target 1010
  ]
  edge [
    source 32
    target 1011
  ]
  edge [
    source 32
    target 1012
  ]
  edge [
    source 32
    target 1013
  ]
  edge [
    source 32
    target 1014
  ]
  edge [
    source 32
    target 1015
  ]
  edge [
    source 32
    target 1016
  ]
  edge [
    source 32
    target 1017
  ]
  edge [
    source 32
    target 754
  ]
  edge [
    source 32
    target 1018
  ]
  edge [
    source 32
    target 1019
  ]
  edge [
    source 32
    target 948
  ]
  edge [
    source 32
    target 1020
  ]
  edge [
    source 32
    target 1021
  ]
  edge [
    source 32
    target 1022
  ]
  edge [
    source 32
    target 1023
  ]
  edge [
    source 32
    target 1024
  ]
  edge [
    source 32
    target 1214
  ]
  edge [
    source 32
    target 1215
  ]
  edge [
    source 32
    target 1216
  ]
  edge [
    source 32
    target 1217
  ]
  edge [
    source 32
    target 1218
  ]
  edge [
    source 32
    target 1219
  ]
  edge [
    source 32
    target 1220
  ]
  edge [
    source 32
    target 1221
  ]
  edge [
    source 32
    target 1222
  ]
  edge [
    source 32
    target 1223
  ]
  edge [
    source 32
    target 79
  ]
  edge [
    source 32
    target 80
  ]
  edge [
    source 32
    target 81
  ]
  edge [
    source 32
    target 82
  ]
  edge [
    source 32
    target 83
  ]
  edge [
    source 32
    target 84
  ]
  edge [
    source 32
    target 85
  ]
  edge [
    source 32
    target 86
  ]
  edge [
    source 32
    target 87
  ]
  edge [
    source 32
    target 88
  ]
  edge [
    source 32
    target 89
  ]
  edge [
    source 32
    target 90
  ]
  edge [
    source 32
    target 91
  ]
  edge [
    source 32
    target 92
  ]
  edge [
    source 32
    target 93
  ]
  edge [
    source 32
    target 94
  ]
  edge [
    source 32
    target 95
  ]
  edge [
    source 32
    target 96
  ]
  edge [
    source 32
    target 97
  ]
  edge [
    source 32
    target 98
  ]
  edge [
    source 32
    target 99
  ]
  edge [
    source 32
    target 100
  ]
  edge [
    source 32
    target 101
  ]
  edge [
    source 32
    target 102
  ]
  edge [
    source 32
    target 103
  ]
  edge [
    source 32
    target 1224
  ]
  edge [
    source 32
    target 1225
  ]
  edge [
    source 32
    target 1226
  ]
  edge [
    source 32
    target 957
  ]
  edge [
    source 32
    target 958
  ]
  edge [
    source 32
    target 959
  ]
  edge [
    source 32
    target 960
  ]
  edge [
    source 32
    target 961
  ]
  edge [
    source 32
    target 962
  ]
  edge [
    source 32
    target 43
  ]
  edge [
    source 32
    target 963
  ]
  edge [
    source 32
    target 964
  ]
  edge [
    source 32
    target 966
  ]
  edge [
    source 32
    target 1227
  ]
  edge [
    source 32
    target 1228
  ]
  edge [
    source 32
    target 1229
  ]
  edge [
    source 32
    target 1230
  ]
  edge [
    source 32
    target 1231
  ]
  edge [
    source 32
    target 1232
  ]
  edge [
    source 32
    target 1233
  ]
  edge [
    source 32
    target 1234
  ]
  edge [
    source 32
    target 826
  ]
  edge [
    source 32
    target 1235
  ]
  edge [
    source 32
    target 591
  ]
  edge [
    source 32
    target 1236
  ]
  edge [
    source 32
    target 1237
  ]
  edge [
    source 32
    target 1238
  ]
  edge [
    source 32
    target 1239
  ]
  edge [
    source 32
    target 1240
  ]
  edge [
    source 32
    target 1241
  ]
  edge [
    source 32
    target 1242
  ]
  edge [
    source 32
    target 1243
  ]
  edge [
    source 32
    target 1244
  ]
  edge [
    source 32
    target 1245
  ]
  edge [
    source 32
    target 1246
  ]
  edge [
    source 32
    target 1247
  ]
  edge [
    source 32
    target 1248
  ]
  edge [
    source 32
    target 1249
  ]
  edge [
    source 32
    target 1250
  ]
  edge [
    source 32
    target 1251
  ]
  edge [
    source 32
    target 1252
  ]
  edge [
    source 32
    target 1253
  ]
  edge [
    source 32
    target 1254
  ]
  edge [
    source 32
    target 1255
  ]
  edge [
    source 32
    target 1256
  ]
  edge [
    source 32
    target 1257
  ]
  edge [
    source 32
    target 930
  ]
  edge [
    source 32
    target 1258
  ]
  edge [
    source 32
    target 287
  ]
  edge [
    source 32
    target 1259
  ]
  edge [
    source 32
    target 1260
  ]
  edge [
    source 32
    target 1261
  ]
  edge [
    source 32
    target 1262
  ]
  edge [
    source 32
    target 1263
  ]
  edge [
    source 32
    target 1264
  ]
  edge [
    source 32
    target 1265
  ]
  edge [
    source 32
    target 1266
  ]
  edge [
    source 32
    target 1267
  ]
  edge [
    source 32
    target 1268
  ]
  edge [
    source 32
    target 1269
  ]
  edge [
    source 32
    target 1270
  ]
  edge [
    source 32
    target 1271
  ]
  edge [
    source 32
    target 1272
  ]
  edge [
    source 32
    target 1273
  ]
  edge [
    source 32
    target 1274
  ]
  edge [
    source 32
    target 1275
  ]
  edge [
    source 32
    target 1276
  ]
  edge [
    source 32
    target 1277
  ]
  edge [
    source 32
    target 1278
  ]
  edge [
    source 32
    target 1279
  ]
  edge [
    source 32
    target 1280
  ]
  edge [
    source 32
    target 1281
  ]
  edge [
    source 32
    target 1282
  ]
  edge [
    source 32
    target 1283
  ]
  edge [
    source 32
    target 1284
  ]
  edge [
    source 32
    target 1285
  ]
  edge [
    source 32
    target 1286
  ]
  edge [
    source 32
    target 309
  ]
  edge [
    source 32
    target 1287
  ]
  edge [
    source 32
    target 1288
  ]
  edge [
    source 32
    target 1289
  ]
  edge [
    source 32
    target 1290
  ]
  edge [
    source 32
    target 1291
  ]
  edge [
    source 32
    target 1292
  ]
  edge [
    source 32
    target 1293
  ]
  edge [
    source 32
    target 1294
  ]
  edge [
    source 32
    target 1295
  ]
  edge [
    source 32
    target 1296
  ]
  edge [
    source 32
    target 1297
  ]
  edge [
    source 32
    target 1298
  ]
  edge [
    source 32
    target 1299
  ]
  edge [
    source 32
    target 364
  ]
  edge [
    source 32
    target 1300
  ]
  edge [
    source 32
    target 1301
  ]
  edge [
    source 32
    target 1302
  ]
  edge [
    source 32
    target 1303
  ]
  edge [
    source 32
    target 1304
  ]
  edge [
    source 32
    target 1305
  ]
  edge [
    source 32
    target 1306
  ]
  edge [
    source 32
    target 1307
  ]
  edge [
    source 32
    target 1308
  ]
  edge [
    source 32
    target 1309
  ]
  edge [
    source 32
    target 1310
  ]
  edge [
    source 32
    target 1311
  ]
  edge [
    source 32
    target 1312
  ]
  edge [
    source 32
    target 1313
  ]
  edge [
    source 32
    target 1314
  ]
  edge [
    source 32
    target 1034
  ]
  edge [
    source 32
    target 1315
  ]
  edge [
    source 32
    target 1316
  ]
  edge [
    source 32
    target 1317
  ]
  edge [
    source 32
    target 1318
  ]
  edge [
    source 32
    target 1319
  ]
  edge [
    source 32
    target 1320
  ]
  edge [
    source 32
    target 1321
  ]
  edge [
    source 32
    target 1322
  ]
  edge [
    source 32
    target 1323
  ]
  edge [
    source 32
    target 1324
  ]
  edge [
    source 32
    target 1325
  ]
  edge [
    source 32
    target 1326
  ]
  edge [
    source 32
    target 1327
  ]
  edge [
    source 32
    target 1328
  ]
  edge [
    source 32
    target 1329
  ]
  edge [
    source 32
    target 1330
  ]
  edge [
    source 32
    target 1331
  ]
  edge [
    source 32
    target 1332
  ]
  edge [
    source 32
    target 1333
  ]
  edge [
    source 32
    target 1334
  ]
  edge [
    source 32
    target 1335
  ]
  edge [
    source 32
    target 1336
  ]
  edge [
    source 32
    target 1337
  ]
  edge [
    source 32
    target 272
  ]
  edge [
    source 32
    target 1338
  ]
  edge [
    source 32
    target 1339
  ]
  edge [
    source 32
    target 37
  ]
  edge [
    source 32
    target 1340
  ]
  edge [
    source 32
    target 1341
  ]
  edge [
    source 32
    target 1342
  ]
  edge [
    source 32
    target 1343
  ]
  edge [
    source 32
    target 1344
  ]
  edge [
    source 32
    target 1345
  ]
  edge [
    source 32
    target 1346
  ]
  edge [
    source 32
    target 1347
  ]
  edge [
    source 32
    target 1348
  ]
  edge [
    source 32
    target 1349
  ]
  edge [
    source 32
    target 1350
  ]
  edge [
    source 32
    target 1351
  ]
  edge [
    source 32
    target 1352
  ]
  edge [
    source 32
    target 1353
  ]
  edge [
    source 32
    target 1354
  ]
  edge [
    source 32
    target 1355
  ]
  edge [
    source 32
    target 1356
  ]
  edge [
    source 32
    target 1357
  ]
  edge [
    source 32
    target 1358
  ]
  edge [
    source 32
    target 1359
  ]
  edge [
    source 32
    target 1360
  ]
  edge [
    source 32
    target 1361
  ]
  edge [
    source 32
    target 473
  ]
  edge [
    source 32
    target 1362
  ]
  edge [
    source 32
    target 1363
  ]
  edge [
    source 32
    target 1364
  ]
  edge [
    source 32
    target 1365
  ]
  edge [
    source 32
    target 1366
  ]
  edge [
    source 32
    target 1367
  ]
  edge [
    source 32
    target 1368
  ]
  edge [
    source 32
    target 1369
  ]
  edge [
    source 32
    target 1370
  ]
  edge [
    source 32
    target 1371
  ]
  edge [
    source 32
    target 1372
  ]
  edge [
    source 32
    target 1373
  ]
  edge [
    source 32
    target 1374
  ]
  edge [
    source 32
    target 1375
  ]
  edge [
    source 32
    target 1376
  ]
  edge [
    source 32
    target 1377
  ]
  edge [
    source 32
    target 1378
  ]
  edge [
    source 32
    target 1379
  ]
  edge [
    source 32
    target 1380
  ]
  edge [
    source 32
    target 1381
  ]
  edge [
    source 32
    target 1382
  ]
  edge [
    source 32
    target 1383
  ]
  edge [
    source 32
    target 1384
  ]
  edge [
    source 32
    target 1385
  ]
  edge [
    source 32
    target 1386
  ]
  edge [
    source 32
    target 1387
  ]
  edge [
    source 32
    target 1388
  ]
  edge [
    source 32
    target 1389
  ]
  edge [
    source 32
    target 1390
  ]
  edge [
    source 32
    target 1391
  ]
  edge [
    source 32
    target 1392
  ]
  edge [
    source 32
    target 1393
  ]
  edge [
    source 32
    target 478
  ]
  edge [
    source 32
    target 1394
  ]
  edge [
    source 32
    target 1395
  ]
  edge [
    source 32
    target 1396
  ]
  edge [
    source 32
    target 1397
  ]
  edge [
    source 32
    target 1398
  ]
  edge [
    source 32
    target 1399
  ]
  edge [
    source 32
    target 1400
  ]
  edge [
    source 32
    target 1401
  ]
  edge [
    source 32
    target 1402
  ]
  edge [
    source 32
    target 1403
  ]
  edge [
    source 32
    target 1404
  ]
  edge [
    source 32
    target 1405
  ]
  edge [
    source 32
    target 1406
  ]
  edge [
    source 32
    target 1162
  ]
  edge [
    source 32
    target 1407
  ]
  edge [
    source 32
    target 1408
  ]
  edge [
    source 32
    target 1409
  ]
  edge [
    source 32
    target 1410
  ]
  edge [
    source 32
    target 1411
  ]
  edge [
    source 32
    target 1412
  ]
  edge [
    source 32
    target 1413
  ]
  edge [
    source 32
    target 1414
  ]
  edge [
    source 32
    target 1415
  ]
  edge [
    source 32
    target 1416
  ]
  edge [
    source 32
    target 1417
  ]
  edge [
    source 32
    target 1418
  ]
  edge [
    source 32
    target 1419
  ]
  edge [
    source 32
    target 1420
  ]
  edge [
    source 32
    target 1421
  ]
  edge [
    source 32
    target 1422
  ]
  edge [
    source 32
    target 1423
  ]
  edge [
    source 32
    target 1424
  ]
  edge [
    source 32
    target 1425
  ]
  edge [
    source 32
    target 1426
  ]
  edge [
    source 32
    target 1427
  ]
  edge [
    source 32
    target 1428
  ]
  edge [
    source 32
    target 1429
  ]
  edge [
    source 32
    target 1430
  ]
  edge [
    source 32
    target 1431
  ]
  edge [
    source 32
    target 1432
  ]
  edge [
    source 32
    target 1433
  ]
  edge [
    source 32
    target 465
  ]
  edge [
    source 32
    target 1434
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1435
  ]
  edge [
    source 33
    target 1077
  ]
  edge [
    source 33
    target 1436
  ]
  edge [
    source 33
    target 1080
  ]
  edge [
    source 33
    target 1437
  ]
  edge [
    source 33
    target 1096
  ]
  edge [
    source 33
    target 1438
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 600
  ]
  edge [
    source 35
    target 1439
  ]
  edge [
    source 35
    target 1239
  ]
  edge [
    source 35
    target 1440
  ]
  edge [
    source 35
    target 1441
  ]
  edge [
    source 35
    target 1442
  ]
  edge [
    source 35
    target 1443
  ]
  edge [
    source 35
    target 1444
  ]
  edge [
    source 35
    target 1445
  ]
  edge [
    source 35
    target 1446
  ]
  edge [
    source 35
    target 1447
  ]
  edge [
    source 35
    target 1448
  ]
  edge [
    source 35
    target 1449
  ]
  edge [
    source 35
    target 1450
  ]
  edge [
    source 35
    target 1451
  ]
  edge [
    source 35
    target 1452
  ]
  edge [
    source 35
    target 1453
  ]
  edge [
    source 35
    target 783
  ]
  edge [
    source 35
    target 1454
  ]
  edge [
    source 35
    target 1455
  ]
  edge [
    source 35
    target 1456
  ]
  edge [
    source 35
    target 1457
  ]
  edge [
    source 35
    target 1458
  ]
  edge [
    source 35
    target 1459
  ]
  edge [
    source 35
    target 1460
  ]
  edge [
    source 35
    target 1461
  ]
  edge [
    source 35
    target 1462
  ]
  edge [
    source 35
    target 1463
  ]
  edge [
    source 35
    target 1464
  ]
  edge [
    source 35
    target 1465
  ]
  edge [
    source 35
    target 1466
  ]
  edge [
    source 35
    target 1467
  ]
  edge [
    source 35
    target 1468
  ]
  edge [
    source 35
    target 1469
  ]
  edge [
    source 35
    target 1470
  ]
  edge [
    source 35
    target 1471
  ]
  edge [
    source 35
    target 1472
  ]
  edge [
    source 35
    target 1473
  ]
  edge [
    source 35
    target 1474
  ]
  edge [
    source 35
    target 1475
  ]
  edge [
    source 35
    target 1476
  ]
  edge [
    source 35
    target 1477
  ]
  edge [
    source 35
    target 1478
  ]
  edge [
    source 35
    target 1479
  ]
  edge [
    source 35
    target 1480
  ]
  edge [
    source 35
    target 1481
  ]
  edge [
    source 35
    target 1482
  ]
  edge [
    source 35
    target 1483
  ]
  edge [
    source 35
    target 1484
  ]
  edge [
    source 35
    target 1485
  ]
  edge [
    source 35
    target 1486
  ]
  edge [
    source 35
    target 778
  ]
  edge [
    source 35
    target 819
  ]
  edge [
    source 35
    target 1487
  ]
  edge [
    source 35
    target 1488
  ]
  edge [
    source 35
    target 1489
  ]
  edge [
    source 35
    target 1490
  ]
  edge [
    source 35
    target 1491
  ]
  edge [
    source 35
    target 1177
  ]
  edge [
    source 35
    target 1492
  ]
  edge [
    source 35
    target 1493
  ]
  edge [
    source 35
    target 1494
  ]
  edge [
    source 35
    target 1495
  ]
  edge [
    source 35
    target 1496
  ]
  edge [
    source 35
    target 1497
  ]
  edge [
    source 35
    target 1498
  ]
  edge [
    source 35
    target 1499
  ]
  edge [
    source 35
    target 1500
  ]
  edge [
    source 35
    target 1501
  ]
  edge [
    source 35
    target 1502
  ]
  edge [
    source 35
    target 1503
  ]
  edge [
    source 35
    target 1504
  ]
  edge [
    source 35
    target 1505
  ]
  edge [
    source 35
    target 1506
  ]
  edge [
    source 35
    target 42
  ]
  edge [
    source 35
    target 1507
  ]
  edge [
    source 35
    target 1508
  ]
  edge [
    source 35
    target 1509
  ]
  edge [
    source 35
    target 1510
  ]
  edge [
    source 35
    target 1511
  ]
  edge [
    source 35
    target 1512
  ]
  edge [
    source 35
    target 1513
  ]
  edge [
    source 35
    target 1514
  ]
  edge [
    source 35
    target 1515
  ]
  edge [
    source 35
    target 1516
  ]
  edge [
    source 35
    target 1517
  ]
  edge [
    source 35
    target 1518
  ]
  edge [
    source 35
    target 1226
  ]
  edge [
    source 35
    target 1519
  ]
  edge [
    source 35
    target 1520
  ]
  edge [
    source 35
    target 1521
  ]
  edge [
    source 35
    target 1522
  ]
  edge [
    source 35
    target 1523
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 1524
  ]
  edge [
    source 35
    target 1525
  ]
  edge [
    source 35
    target 1526
  ]
  edge [
    source 35
    target 1278
  ]
  edge [
    source 35
    target 1527
  ]
  edge [
    source 35
    target 1528
  ]
  edge [
    source 35
    target 1529
  ]
  edge [
    source 35
    target 1530
  ]
  edge [
    source 35
    target 124
  ]
  edge [
    source 35
    target 1531
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1532
  ]
  edge [
    source 36
    target 1533
  ]
  edge [
    source 36
    target 1534
  ]
  edge [
    source 36
    target 1161
  ]
  edge [
    source 36
    target 1535
  ]
  edge [
    source 36
    target 129
  ]
  edge [
    source 36
    target 1536
  ]
  edge [
    source 36
    target 1537
  ]
  edge [
    source 36
    target 1538
  ]
  edge [
    source 36
    target 1539
  ]
  edge [
    source 36
    target 1164
  ]
  edge [
    source 36
    target 1540
  ]
  edge [
    source 36
    target 1541
  ]
  edge [
    source 36
    target 478
  ]
  edge [
    source 36
    target 829
  ]
  edge [
    source 36
    target 1542
  ]
  edge [
    source 36
    target 133
  ]
  edge [
    source 36
    target 124
  ]
  edge [
    source 36
    target 1543
  ]
  edge [
    source 36
    target 1544
  ]
  edge [
    source 36
    target 1545
  ]
  edge [
    source 36
    target 1546
  ]
  edge [
    source 36
    target 885
  ]
  edge [
    source 36
    target 1547
  ]
  edge [
    source 36
    target 1548
  ]
  edge [
    source 36
    target 1549
  ]
  edge [
    source 36
    target 1550
  ]
  edge [
    source 36
    target 1551
  ]
  edge [
    source 36
    target 1552
  ]
  edge [
    source 36
    target 562
  ]
  edge [
    source 36
    target 1553
  ]
  edge [
    source 36
    target 1554
  ]
  edge [
    source 36
    target 1555
  ]
  edge [
    source 36
    target 1556
  ]
  edge [
    source 36
    target 1557
  ]
  edge [
    source 36
    target 137
  ]
  edge [
    source 36
    target 1098
  ]
  edge [
    source 36
    target 1558
  ]
  edge [
    source 36
    target 1559
  ]
  edge [
    source 36
    target 1560
  ]
  edge [
    source 36
    target 1561
  ]
  edge [
    source 36
    target 1562
  ]
  edge [
    source 36
    target 1563
  ]
  edge [
    source 36
    target 1564
  ]
  edge [
    source 36
    target 1565
  ]
  edge [
    source 36
    target 1566
  ]
  edge [
    source 36
    target 1567
  ]
  edge [
    source 36
    target 104
  ]
  edge [
    source 36
    target 1568
  ]
  edge [
    source 36
    target 1569
  ]
  edge [
    source 36
    target 93
  ]
  edge [
    source 36
    target 1570
  ]
  edge [
    source 36
    target 131
  ]
  edge [
    source 36
    target 967
  ]
  edge [
    source 36
    target 132
  ]
  edge [
    source 36
    target 1571
  ]
  edge [
    source 36
    target 1572
  ]
  edge [
    source 36
    target 161
  ]
  edge [
    source 36
    target 1573
  ]
  edge [
    source 36
    target 1574
  ]
  edge [
    source 36
    target 1575
  ]
  edge [
    source 36
    target 1576
  ]
  edge [
    source 36
    target 1577
  ]
  edge [
    source 36
    target 1578
  ]
  edge [
    source 36
    target 1579
  ]
  edge [
    source 36
    target 1580
  ]
  edge [
    source 36
    target 1581
  ]
  edge [
    source 36
    target 1582
  ]
  edge [
    source 36
    target 1583
  ]
  edge [
    source 36
    target 1584
  ]
  edge [
    source 36
    target 1585
  ]
  edge [
    source 36
    target 1586
  ]
  edge [
    source 36
    target 80
  ]
  edge [
    source 36
    target 1587
  ]
  edge [
    source 36
    target 82
  ]
  edge [
    source 36
    target 111
  ]
  edge [
    source 36
    target 1588
  ]
  edge [
    source 36
    target 106
  ]
  edge [
    source 36
    target 1389
  ]
  edge [
    source 36
    target 1589
  ]
  edge [
    source 36
    target 1590
  ]
  edge [
    source 36
    target 1591
  ]
  edge [
    source 36
    target 119
  ]
  edge [
    source 36
    target 278
  ]
  edge [
    source 36
    target 1592
  ]
  edge [
    source 36
    target 1593
  ]
  edge [
    source 36
    target 1594
  ]
  edge [
    source 36
    target 1595
  ]
  edge [
    source 36
    target 1596
  ]
  edge [
    source 36
    target 1597
  ]
  edge [
    source 36
    target 1598
  ]
  edge [
    source 36
    target 1599
  ]
  edge [
    source 36
    target 1600
  ]
  edge [
    source 36
    target 1601
  ]
  edge [
    source 36
    target 1602
  ]
  edge [
    source 36
    target 1603
  ]
  edge [
    source 36
    target 1604
  ]
  edge [
    source 36
    target 1605
  ]
  edge [
    source 36
    target 1606
  ]
  edge [
    source 36
    target 1607
  ]
  edge [
    source 36
    target 1608
  ]
  edge [
    source 36
    target 1609
  ]
  edge [
    source 36
    target 1610
  ]
  edge [
    source 36
    target 1611
  ]
  edge [
    source 36
    target 1612
  ]
  edge [
    source 36
    target 1613
  ]
  edge [
    source 36
    target 1614
  ]
  edge [
    source 36
    target 1615
  ]
  edge [
    source 36
    target 1616
  ]
  edge [
    source 36
    target 1617
  ]
  edge [
    source 36
    target 493
  ]
  edge [
    source 36
    target 1618
  ]
  edge [
    source 36
    target 1619
  ]
  edge [
    source 36
    target 1620
  ]
  edge [
    source 36
    target 1621
  ]
  edge [
    source 36
    target 1622
  ]
  edge [
    source 36
    target 107
  ]
  edge [
    source 36
    target 1623
  ]
  edge [
    source 36
    target 1624
  ]
  edge [
    source 36
    target 1625
  ]
  edge [
    source 36
    target 1626
  ]
  edge [
    source 36
    target 1627
  ]
  edge [
    source 36
    target 126
  ]
  edge [
    source 36
    target 1628
  ]
  edge [
    source 36
    target 1629
  ]
  edge [
    source 36
    target 1630
  ]
  edge [
    source 36
    target 1631
  ]
  edge [
    source 36
    target 1632
  ]
  edge [
    source 36
    target 590
  ]
  edge [
    source 36
    target 591
  ]
  edge [
    source 36
    target 592
  ]
  edge [
    source 36
    target 593
  ]
  edge [
    source 36
    target 594
  ]
  edge [
    source 36
    target 595
  ]
  edge [
    source 36
    target 596
  ]
  edge [
    source 36
    target 597
  ]
  edge [
    source 36
    target 598
  ]
  edge [
    source 36
    target 477
  ]
  edge [
    source 36
    target 1633
  ]
  edge [
    source 36
    target 1634
  ]
  edge [
    source 36
    target 1635
  ]
  edge [
    source 36
    target 1636
  ]
  edge [
    source 36
    target 1637
  ]
  edge [
    source 36
    target 1288
  ]
  edge [
    source 36
    target 1638
  ]
  edge [
    source 36
    target 1639
  ]
  edge [
    source 36
    target 1640
  ]
  edge [
    source 36
    target 826
  ]
  edge [
    source 36
    target 520
  ]
  edge [
    source 36
    target 1641
  ]
  edge [
    source 36
    target 1642
  ]
  edge [
    source 36
    target 1643
  ]
  edge [
    source 36
    target 1644
  ]
  edge [
    source 36
    target 1645
  ]
  edge [
    source 36
    target 1646
  ]
  edge [
    source 36
    target 1647
  ]
  edge [
    source 36
    target 381
  ]
  edge [
    source 36
    target 1648
  ]
  edge [
    source 36
    target 1649
  ]
  edge [
    source 36
    target 65
  ]
  edge [
    source 36
    target 1650
  ]
  edge [
    source 36
    target 1651
  ]
  edge [
    source 36
    target 1652
  ]
  edge [
    source 36
    target 1653
  ]
  edge [
    source 36
    target 1654
  ]
  edge [
    source 36
    target 1655
  ]
  edge [
    source 36
    target 1656
  ]
  edge [
    source 36
    target 1657
  ]
  edge [
    source 36
    target 1658
  ]
  edge [
    source 36
    target 969
  ]
  edge [
    source 36
    target 1166
  ]
  edge [
    source 36
    target 1659
  ]
  edge [
    source 36
    target 1660
  ]
  edge [
    source 36
    target 1661
  ]
  edge [
    source 36
    target 559
  ]
  edge [
    source 36
    target 556
  ]
  edge [
    source 36
    target 565
  ]
  edge [
    source 36
    target 1662
  ]
  edge [
    source 36
    target 1663
  ]
  edge [
    source 36
    target 1664
  ]
  edge [
    source 36
    target 1665
  ]
  edge [
    source 36
    target 1666
  ]
  edge [
    source 36
    target 1667
  ]
  edge [
    source 36
    target 287
  ]
  edge [
    source 36
    target 1668
  ]
  edge [
    source 36
    target 1669
  ]
  edge [
    source 36
    target 1670
  ]
  edge [
    source 36
    target 1671
  ]
  edge [
    source 36
    target 1672
  ]
  edge [
    source 36
    target 1673
  ]
  edge [
    source 36
    target 1674
  ]
  edge [
    source 36
    target 1675
  ]
  edge [
    source 36
    target 1676
  ]
  edge [
    source 36
    target 1677
  ]
  edge [
    source 36
    target 1678
  ]
  edge [
    source 36
    target 1679
  ]
  edge [
    source 36
    target 1680
  ]
  edge [
    source 36
    target 1681
  ]
  edge [
    source 36
    target 1682
  ]
  edge [
    source 36
    target 1683
  ]
  edge [
    source 36
    target 1160
  ]
  edge [
    source 36
    target 1162
  ]
  edge [
    source 36
    target 1163
  ]
  edge [
    source 36
    target 507
  ]
  edge [
    source 36
    target 1165
  ]
  edge [
    source 36
    target 919
  ]
  edge [
    source 36
    target 964
  ]
  edge [
    source 36
    target 1167
  ]
  edge [
    source 36
    target 966
  ]
  edge [
    source 36
    target 1467
  ]
  edge [
    source 36
    target 1684
  ]
  edge [
    source 36
    target 1685
  ]
  edge [
    source 36
    target 1381
  ]
  edge [
    source 36
    target 1686
  ]
  edge [
    source 36
    target 1687
  ]
  edge [
    source 36
    target 1688
  ]
  edge [
    source 36
    target 1689
  ]
  edge [
    source 36
    target 1107
  ]
  edge [
    source 36
    target 1690
  ]
  edge [
    source 36
    target 1691
  ]
  edge [
    source 36
    target 1692
  ]
  edge [
    source 36
    target 1693
  ]
  edge [
    source 36
    target 1694
  ]
  edge [
    source 36
    target 1695
  ]
  edge [
    source 36
    target 1696
  ]
  edge [
    source 36
    target 1697
  ]
  edge [
    source 36
    target 1698
  ]
  edge [
    source 36
    target 1519
  ]
  edge [
    source 36
    target 1699
  ]
  edge [
    source 36
    target 754
  ]
  edge [
    source 36
    target 1700
  ]
  edge [
    source 36
    target 460
  ]
  edge [
    source 36
    target 1701
  ]
  edge [
    source 36
    target 1702
  ]
  edge [
    source 36
    target 1703
  ]
  edge [
    source 36
    target 1704
  ]
  edge [
    source 36
    target 1705
  ]
  edge [
    source 36
    target 1706
  ]
  edge [
    source 36
    target 1707
  ]
  edge [
    source 36
    target 1708
  ]
  edge [
    source 36
    target 1709
  ]
  edge [
    source 36
    target 1710
  ]
  edge [
    source 36
    target 1711
  ]
  edge [
    source 36
    target 1712
  ]
  edge [
    source 36
    target 1713
  ]
  edge [
    source 36
    target 1362
  ]
  edge [
    source 36
    target 1714
  ]
  edge [
    source 36
    target 1391
  ]
  edge [
    source 36
    target 1278
  ]
  edge [
    source 36
    target 1715
  ]
  edge [
    source 36
    target 1716
  ]
  edge [
    source 36
    target 1717
  ]
  edge [
    source 36
    target 1718
  ]
  edge [
    source 36
    target 1719
  ]
  edge [
    source 36
    target 1720
  ]
  edge [
    source 36
    target 1721
  ]
  edge [
    source 36
    target 827
  ]
  edge [
    source 36
    target 1722
  ]
  edge [
    source 36
    target 1723
  ]
  edge [
    source 36
    target 1724
  ]
  edge [
    source 36
    target 1725
  ]
  edge [
    source 36
    target 1726
  ]
  edge [
    source 36
    target 1727
  ]
  edge [
    source 36
    target 1728
  ]
  edge [
    source 36
    target 1729
  ]
  edge [
    source 36
    target 1730
  ]
  edge [
    source 36
    target 1731
  ]
  edge [
    source 36
    target 1732
  ]
  edge [
    source 36
    target 1733
  ]
  edge [
    source 36
    target 1734
  ]
  edge [
    source 36
    target 1735
  ]
  edge [
    source 36
    target 105
  ]
  edge [
    source 36
    target 1736
  ]
  edge [
    source 36
    target 1737
  ]
  edge [
    source 36
    target 1738
  ]
  edge [
    source 36
    target 1739
  ]
  edge [
    source 36
    target 1740
  ]
  edge [
    source 36
    target 1285
  ]
  edge [
    source 36
    target 1741
  ]
  edge [
    source 36
    target 1742
  ]
  edge [
    source 36
    target 1743
  ]
  edge [
    source 36
    target 1744
  ]
  edge [
    source 36
    target 1745
  ]
  edge [
    source 36
    target 1746
  ]
  edge [
    source 36
    target 1747
  ]
  edge [
    source 36
    target 1748
  ]
  edge [
    source 36
    target 1749
  ]
  edge [
    source 36
    target 1750
  ]
  edge [
    source 36
    target 1751
  ]
  edge [
    source 36
    target 1752
  ]
  edge [
    source 36
    target 1753
  ]
  edge [
    source 36
    target 1754
  ]
  edge [
    source 36
    target 1755
  ]
  edge [
    source 36
    target 1756
  ]
  edge [
    source 36
    target 1757
  ]
  edge [
    source 36
    target 1758
  ]
  edge [
    source 36
    target 1759
  ]
  edge [
    source 36
    target 1760
  ]
  edge [
    source 36
    target 1761
  ]
  edge [
    source 36
    target 1762
  ]
  edge [
    source 36
    target 1763
  ]
  edge [
    source 36
    target 1764
  ]
  edge [
    source 36
    target 1765
  ]
  edge [
    source 36
    target 1766
  ]
  edge [
    source 36
    target 1767
  ]
  edge [
    source 36
    target 1768
  ]
  edge [
    source 36
    target 1769
  ]
  edge [
    source 36
    target 1770
  ]
  edge [
    source 36
    target 1771
  ]
  edge [
    source 36
    target 1772
  ]
  edge [
    source 36
    target 1773
  ]
  edge [
    source 36
    target 1139
  ]
  edge [
    source 36
    target 824
  ]
  edge [
    source 36
    target 1774
  ]
  edge [
    source 36
    target 1775
  ]
  edge [
    source 36
    target 1776
  ]
  edge [
    source 36
    target 1777
  ]
  edge [
    source 36
    target 1778
  ]
  edge [
    source 36
    target 1779
  ]
  edge [
    source 36
    target 1780
  ]
  edge [
    source 36
    target 1781
  ]
  edge [
    source 36
    target 1782
  ]
  edge [
    source 36
    target 128
  ]
  edge [
    source 36
    target 1783
  ]
  edge [
    source 36
    target 1784
  ]
  edge [
    source 36
    target 1785
  ]
  edge [
    source 36
    target 1786
  ]
  edge [
    source 36
    target 1787
  ]
  edge [
    source 36
    target 1788
  ]
  edge [
    source 36
    target 1789
  ]
  edge [
    source 36
    target 1790
  ]
  edge [
    source 36
    target 1791
  ]
  edge [
    source 36
    target 1792
  ]
  edge [
    source 36
    target 1793
  ]
  edge [
    source 36
    target 1794
  ]
  edge [
    source 36
    target 1795
  ]
  edge [
    source 36
    target 1796
  ]
  edge [
    source 36
    target 1797
  ]
  edge [
    source 36
    target 1798
  ]
  edge [
    source 36
    target 1799
  ]
  edge [
    source 36
    target 1800
  ]
  edge [
    source 36
    target 1801
  ]
  edge [
    source 36
    target 1802
  ]
  edge [
    source 36
    target 1803
  ]
  edge [
    source 36
    target 1804
  ]
  edge [
    source 36
    target 1805
  ]
  edge [
    source 36
    target 1806
  ]
  edge [
    source 36
    target 937
  ]
  edge [
    source 36
    target 1807
  ]
  edge [
    source 36
    target 1808
  ]
  edge [
    source 36
    target 1809
  ]
  edge [
    source 36
    target 1810
  ]
  edge [
    source 36
    target 1811
  ]
  edge [
    source 36
    target 1812
  ]
  edge [
    source 36
    target 1813
  ]
  edge [
    source 36
    target 1814
  ]
  edge [
    source 36
    target 1064
  ]
  edge [
    source 36
    target 1815
  ]
  edge [
    source 36
    target 1816
  ]
  edge [
    source 36
    target 1817
  ]
  edge [
    source 36
    target 1818
  ]
  edge [
    source 36
    target 1819
  ]
  edge [
    source 36
    target 1820
  ]
  edge [
    source 36
    target 1821
  ]
  edge [
    source 36
    target 1822
  ]
  edge [
    source 36
    target 1823
  ]
  edge [
    source 36
    target 1824
  ]
  edge [
    source 36
    target 1825
  ]
  edge [
    source 36
    target 1826
  ]
  edge [
    source 36
    target 1827
  ]
  edge [
    source 36
    target 1828
  ]
  edge [
    source 36
    target 1829
  ]
  edge [
    source 36
    target 1830
  ]
  edge [
    source 36
    target 1831
  ]
  edge [
    source 36
    target 1832
  ]
  edge [
    source 36
    target 1833
  ]
  edge [
    source 36
    target 1834
  ]
  edge [
    source 36
    target 1835
  ]
  edge [
    source 36
    target 1836
  ]
  edge [
    source 36
    target 1837
  ]
  edge [
    source 36
    target 1838
  ]
  edge [
    source 36
    target 1839
  ]
  edge [
    source 36
    target 838
  ]
  edge [
    source 36
    target 1840
  ]
  edge [
    source 36
    target 1841
  ]
  edge [
    source 36
    target 1842
  ]
  edge [
    source 36
    target 1843
  ]
  edge [
    source 36
    target 1844
  ]
  edge [
    source 36
    target 1845
  ]
  edge [
    source 36
    target 1846
  ]
  edge [
    source 36
    target 1847
  ]
  edge [
    source 36
    target 1848
  ]
  edge [
    source 36
    target 1849
  ]
  edge [
    source 36
    target 1850
  ]
  edge [
    source 36
    target 127
  ]
  edge [
    source 36
    target 1851
  ]
  edge [
    source 36
    target 1852
  ]
  edge [
    source 36
    target 1853
  ]
  edge [
    source 36
    target 1854
  ]
  edge [
    source 36
    target 1855
  ]
  edge [
    source 36
    target 1856
  ]
  edge [
    source 36
    target 1857
  ]
  edge [
    source 36
    target 1858
  ]
  edge [
    source 36
    target 1859
  ]
  edge [
    source 36
    target 1860
  ]
  edge [
    source 36
    target 1861
  ]
  edge [
    source 36
    target 1862
  ]
  edge [
    source 36
    target 1863
  ]
  edge [
    source 36
    target 1864
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1865
  ]
  edge [
    source 37
    target 1517
  ]
  edge [
    source 37
    target 1518
  ]
  edge [
    source 37
    target 1344
  ]
  edge [
    source 37
    target 1519
  ]
  edge [
    source 37
    target 1521
  ]
  edge [
    source 37
    target 1523
  ]
  edge [
    source 37
    target 1866
  ]
  edge [
    source 37
    target 478
  ]
  edge [
    source 37
    target 1525
  ]
  edge [
    source 37
    target 1867
  ]
  edge [
    source 37
    target 1868
  ]
  edge [
    source 37
    target 1869
  ]
  edge [
    source 37
    target 1870
  ]
  edge [
    source 37
    target 1871
  ]
  edge [
    source 37
    target 477
  ]
  edge [
    source 37
    target 1872
  ]
  edge [
    source 37
    target 1873
  ]
  edge [
    source 37
    target 1522
  ]
  edge [
    source 37
    target 1874
  ]
  edge [
    source 37
    target 1875
  ]
  edge [
    source 37
    target 1876
  ]
  edge [
    source 37
    target 1877
  ]
  edge [
    source 37
    target 1226
  ]
  edge [
    source 37
    target 1878
  ]
  edge [
    source 37
    target 1879
  ]
  edge [
    source 37
    target 1880
  ]
  edge [
    source 37
    target 754
  ]
  edge [
    source 37
    target 1190
  ]
  edge [
    source 37
    target 1239
  ]
  edge [
    source 37
    target 1881
  ]
  edge [
    source 37
    target 1882
  ]
  edge [
    source 37
    target 1883
  ]
  edge [
    source 37
    target 1884
  ]
  edge [
    source 37
    target 1885
  ]
  edge [
    source 37
    target 248
  ]
  edge [
    source 37
    target 1886
  ]
  edge [
    source 37
    target 1887
  ]
  edge [
    source 37
    target 1888
  ]
  edge [
    source 37
    target 1889
  ]
  edge [
    source 37
    target 1890
  ]
  edge [
    source 37
    target 1891
  ]
  edge [
    source 37
    target 1288
  ]
  edge [
    source 37
    target 1892
  ]
  edge [
    source 37
    target 1893
  ]
  edge [
    source 37
    target 1894
  ]
  edge [
    source 37
    target 1895
  ]
  edge [
    source 37
    target 1896
  ]
  edge [
    source 37
    target 43
  ]
  edge [
    source 37
    target 1897
  ]
  edge [
    source 37
    target 929
  ]
  edge [
    source 37
    target 1520
  ]
  edge [
    source 37
    target 1524
  ]
  edge [
    source 37
    target 1898
  ]
  edge [
    source 37
    target 104
  ]
  edge [
    source 37
    target 1899
  ]
  edge [
    source 37
    target 1224
  ]
  edge [
    source 37
    target 1633
  ]
  edge [
    source 37
    target 1634
  ]
  edge [
    source 37
    target 1539
  ]
  edge [
    source 37
    target 1635
  ]
  edge [
    source 37
    target 1636
  ]
  edge [
    source 37
    target 1637
  ]
  edge [
    source 37
    target 1638
  ]
  edge [
    source 37
    target 1900
  ]
  edge [
    source 37
    target 1901
  ]
  edge [
    source 37
    target 1902
  ]
  edge [
    source 37
    target 373
  ]
  edge [
    source 37
    target 492
  ]
  edge [
    source 37
    target 493
  ]
  edge [
    source 37
    target 495
  ]
  edge [
    source 37
    target 1903
  ]
  edge [
    source 37
    target 1904
  ]
  edge [
    source 37
    target 1905
  ]
  edge [
    source 37
    target 1225
  ]
  edge [
    source 37
    target 1140
  ]
  edge [
    source 37
    target 1330
  ]
  edge [
    source 37
    target 1906
  ]
  edge [
    source 37
    target 1117
  ]
  edge [
    source 37
    target 1907
  ]
  edge [
    source 37
    target 1908
  ]
  edge [
    source 37
    target 1909
  ]
  edge [
    source 37
    target 1910
  ]
  edge [
    source 37
    target 518
  ]
  edge [
    source 37
    target 1189
  ]
  edge [
    source 37
    target 147
  ]
  edge [
    source 37
    target 1911
  ]
  edge [
    source 37
    target 1912
  ]
  edge [
    source 37
    target 1913
  ]
  edge [
    source 37
    target 1914
  ]
  edge [
    source 37
    target 1915
  ]
  edge [
    source 37
    target 1916
  ]
  edge [
    source 37
    target 1917
  ]
  edge [
    source 37
    target 1918
  ]
  edge [
    source 37
    target 1919
  ]
  edge [
    source 37
    target 1920
  ]
  edge [
    source 37
    target 1921
  ]
  edge [
    source 37
    target 306
  ]
  edge [
    source 37
    target 287
  ]
  edge [
    source 37
    target 1922
  ]
  edge [
    source 37
    target 1923
  ]
  edge [
    source 37
    target 1924
  ]
  edge [
    source 37
    target 1925
  ]
  edge [
    source 37
    target 1926
  ]
  edge [
    source 37
    target 1927
  ]
  edge [
    source 37
    target 1928
  ]
  edge [
    source 37
    target 1929
  ]
  edge [
    source 37
    target 1930
  ]
  edge [
    source 37
    target 1931
  ]
  edge [
    source 37
    target 1932
  ]
  edge [
    source 37
    target 1800
  ]
  edge [
    source 37
    target 1933
  ]
  edge [
    source 37
    target 1934
  ]
  edge [
    source 37
    target 1034
  ]
  edge [
    source 37
    target 645
  ]
  edge [
    source 37
    target 1935
  ]
  edge [
    source 37
    target 1936
  ]
  edge [
    source 37
    target 1937
  ]
  edge [
    source 37
    target 1938
  ]
  edge [
    source 37
    target 722
  ]
  edge [
    source 37
    target 1939
  ]
  edge [
    source 37
    target 1940
  ]
  edge [
    source 37
    target 786
  ]
  edge [
    source 37
    target 1941
  ]
  edge [
    source 37
    target 1942
  ]
  edge [
    source 37
    target 1943
  ]
  edge [
    source 37
    target 600
  ]
  edge [
    source 37
    target 276
  ]
  edge [
    source 37
    target 1944
  ]
  edge [
    source 37
    target 1527
  ]
  edge [
    source 37
    target 1945
  ]
  edge [
    source 37
    target 1529
  ]
  edge [
    source 37
    target 1946
  ]
  edge [
    source 37
    target 1947
  ]
  edge [
    source 37
    target 1948
  ]
  edge [
    source 37
    target 1395
  ]
  edge [
    source 37
    target 1949
  ]
  edge [
    source 37
    target 1950
  ]
  edge [
    source 37
    target 1951
  ]
  edge [
    source 37
    target 1952
  ]
  edge [
    source 37
    target 1953
  ]
  edge [
    source 37
    target 1954
  ]
  edge [
    source 37
    target 1955
  ]
  edge [
    source 37
    target 1956
  ]
  edge [
    source 37
    target 1957
  ]
  edge [
    source 37
    target 1958
  ]
  edge [
    source 37
    target 1959
  ]
  edge [
    source 37
    target 1960
  ]
  edge [
    source 37
    target 1439
  ]
  edge [
    source 37
    target 1440
  ]
  edge [
    source 37
    target 1441
  ]
  edge [
    source 37
    target 1442
  ]
  edge [
    source 37
    target 1443
  ]
  edge [
    source 37
    target 1444
  ]
  edge [
    source 37
    target 1445
  ]
  edge [
    source 37
    target 1446
  ]
  edge [
    source 37
    target 1447
  ]
  edge [
    source 37
    target 1448
  ]
  edge [
    source 37
    target 1449
  ]
  edge [
    source 37
    target 1450
  ]
  edge [
    source 37
    target 1451
  ]
  edge [
    source 37
    target 1452
  ]
  edge [
    source 37
    target 1961
  ]
  edge [
    source 37
    target 1962
  ]
  edge [
    source 37
    target 1963
  ]
  edge [
    source 37
    target 1526
  ]
  edge [
    source 37
    target 1964
  ]
  edge [
    source 37
    target 1965
  ]
  edge [
    source 37
    target 1966
  ]
  edge [
    source 37
    target 1967
  ]
  edge [
    source 37
    target 1968
  ]
  edge [
    source 37
    target 1969
  ]
  edge [
    source 37
    target 1970
  ]
  edge [
    source 37
    target 1971
  ]
  edge [
    source 37
    target 1972
  ]
  edge [
    source 37
    target 1703
  ]
  edge [
    source 37
    target 1973
  ]
  edge [
    source 37
    target 1390
  ]
  edge [
    source 37
    target 1974
  ]
  edge [
    source 37
    target 1975
  ]
  edge [
    source 37
    target 485
  ]
  edge [
    source 37
    target 1976
  ]
  edge [
    source 37
    target 1977
  ]
  edge [
    source 37
    target 1978
  ]
  edge [
    source 37
    target 1979
  ]
  edge [
    source 37
    target 1980
  ]
  edge [
    source 37
    target 1981
  ]
  edge [
    source 37
    target 1982
  ]
  edge [
    source 37
    target 1983
  ]
  edge [
    source 37
    target 1984
  ]
  edge [
    source 37
    target 1985
  ]
  edge [
    source 37
    target 272
  ]
  edge [
    source 37
    target 65
  ]
  edge [
    source 37
    target 1986
  ]
  edge [
    source 37
    target 1987
  ]
  edge [
    source 37
    target 1988
  ]
  edge [
    source 37
    target 1237
  ]
  edge [
    source 37
    target 1989
  ]
  edge [
    source 37
    target 1990
  ]
  edge [
    source 37
    target 1991
  ]
  edge [
    source 37
    target 98
  ]
  edge [
    source 37
    target 1992
  ]
  edge [
    source 37
    target 1993
  ]
  edge [
    source 37
    target 1994
  ]
  edge [
    source 37
    target 1995
  ]
  edge [
    source 37
    target 1248
  ]
  edge [
    source 37
    target 1249
  ]
  edge [
    source 37
    target 1653
  ]
  edge [
    source 37
    target 1996
  ]
  edge [
    source 37
    target 391
  ]
  edge [
    source 37
    target 1997
  ]
  edge [
    source 37
    target 1998
  ]
  edge [
    source 37
    target 1999
  ]
  edge [
    source 37
    target 2000
  ]
  edge [
    source 37
    target 267
  ]
  edge [
    source 37
    target 2001
  ]
  edge [
    source 37
    target 2002
  ]
  edge [
    source 37
    target 2003
  ]
  edge [
    source 37
    target 2004
  ]
  edge [
    source 37
    target 2005
  ]
  edge [
    source 37
    target 2006
  ]
  edge [
    source 37
    target 2007
  ]
  edge [
    source 37
    target 45
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1135
  ]
  edge [
    source 38
    target 2008
  ]
  edge [
    source 38
    target 2009
  ]
  edge [
    source 38
    target 2010
  ]
  edge [
    source 38
    target 2011
  ]
  edge [
    source 38
    target 2012
  ]
  edge [
    source 38
    target 2013
  ]
  edge [
    source 38
    target 2014
  ]
  edge [
    source 38
    target 2015
  ]
  edge [
    source 38
    target 2016
  ]
  edge [
    source 38
    target 2017
  ]
  edge [
    source 38
    target 2018
  ]
  edge [
    source 38
    target 2019
  ]
  edge [
    source 38
    target 2020
  ]
  edge [
    source 38
    target 2021
  ]
  edge [
    source 38
    target 2022
  ]
  edge [
    source 38
    target 2023
  ]
  edge [
    source 38
    target 2024
  ]
  edge [
    source 38
    target 2025
  ]
  edge [
    source 38
    target 2026
  ]
  edge [
    source 38
    target 2027
  ]
  edge [
    source 38
    target 2028
  ]
  edge [
    source 38
    target 2029
  ]
  edge [
    source 38
    target 2030
  ]
  edge [
    source 38
    target 2031
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1435
  ]
  edge [
    source 39
    target 2032
  ]
  edge [
    source 39
    target 1045
  ]
  edge [
    source 39
    target 2033
  ]
  edge [
    source 39
    target 2034
  ]
  edge [
    source 39
    target 2035
  ]
  edge [
    source 39
    target 1495
  ]
  edge [
    source 39
    target 1497
  ]
  edge [
    source 39
    target 2036
  ]
  edge [
    source 39
    target 2037
  ]
  edge [
    source 39
    target 2038
  ]
  edge [
    source 39
    target 2039
  ]
  edge [
    source 39
    target 2040
  ]
  edge [
    source 39
    target 2041
  ]
  edge [
    source 39
    target 1508
  ]
  edge [
    source 39
    target 1470
  ]
  edge [
    source 39
    target 2042
  ]
  edge [
    source 39
    target 2043
  ]
  edge [
    source 39
    target 1049
  ]
  edge [
    source 39
    target 1479
  ]
  edge [
    source 39
    target 2044
  ]
  edge [
    source 39
    target 2045
  ]
  edge [
    source 39
    target 47
  ]
  edge [
    source 39
    target 2046
  ]
  edge [
    source 39
    target 1457
  ]
  edge [
    source 39
    target 2047
  ]
  edge [
    source 39
    target 2048
  ]
  edge [
    source 39
    target 2049
  ]
  edge [
    source 39
    target 2050
  ]
  edge [
    source 39
    target 2051
  ]
  edge [
    source 39
    target 1071
  ]
  edge [
    source 39
    target 2052
  ]
  edge [
    source 39
    target 2053
  ]
  edge [
    source 39
    target 2054
  ]
  edge [
    source 39
    target 65
  ]
  edge [
    source 39
    target 2055
  ]
  edge [
    source 39
    target 2056
  ]
  edge [
    source 39
    target 2057
  ]
  edge [
    source 39
    target 2058
  ]
  edge [
    source 39
    target 2059
  ]
  edge [
    source 39
    target 2060
  ]
  edge [
    source 39
    target 2061
  ]
  edge [
    source 39
    target 2062
  ]
  edge [
    source 39
    target 2063
  ]
  edge [
    source 39
    target 2064
  ]
  edge [
    source 39
    target 2065
  ]
  edge [
    source 39
    target 2066
  ]
  edge [
    source 39
    target 1657
  ]
  edge [
    source 39
    target 2067
  ]
  edge [
    source 39
    target 2068
  ]
  edge [
    source 39
    target 2069
  ]
  edge [
    source 39
    target 2070
  ]
  edge [
    source 39
    target 2071
  ]
  edge [
    source 39
    target 2072
  ]
  edge [
    source 39
    target 828
  ]
  edge [
    source 39
    target 2073
  ]
  edge [
    source 39
    target 2074
  ]
  edge [
    source 39
    target 2075
  ]
  edge [
    source 39
    target 2076
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 2077
  ]
  edge [
    source 41
    target 2078
  ]
  edge [
    source 41
    target 2079
  ]
  edge [
    source 41
    target 287
  ]
  edge [
    source 41
    target 2080
  ]
  edge [
    source 41
    target 2081
  ]
  edge [
    source 41
    target 2082
  ]
  edge [
    source 41
    target 2083
  ]
  edge [
    source 41
    target 2084
  ]
  edge [
    source 41
    target 2085
  ]
  edge [
    source 41
    target 2086
  ]
  edge [
    source 41
    target 1228
  ]
  edge [
    source 41
    target 2087
  ]
  edge [
    source 41
    target 2088
  ]
  edge [
    source 41
    target 2089
  ]
  edge [
    source 41
    target 604
  ]
  edge [
    source 41
    target 2090
  ]
  edge [
    source 41
    target 2091
  ]
  edge [
    source 41
    target 138
  ]
  edge [
    source 41
    target 2092
  ]
  edge [
    source 41
    target 2093
  ]
  edge [
    source 41
    target 2094
  ]
  edge [
    source 41
    target 2095
  ]
  edge [
    source 41
    target 2096
  ]
  edge [
    source 41
    target 2097
  ]
  edge [
    source 41
    target 2098
  ]
  edge [
    source 41
    target 2099
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1042
  ]
  edge [
    source 42
    target 1043
  ]
  edge [
    source 42
    target 1044
  ]
  edge [
    source 42
    target 1045
  ]
  edge [
    source 42
    target 1046
  ]
  edge [
    source 42
    target 1047
  ]
  edge [
    source 42
    target 1048
  ]
  edge [
    source 42
    target 1049
  ]
  edge [
    source 42
    target 1050
  ]
  edge [
    source 42
    target 771
  ]
  edge [
    source 42
    target 782
  ]
  edge [
    source 42
    target 783
  ]
  edge [
    source 42
    target 1508
  ]
  edge [
    source 42
    target 1470
  ]
  edge [
    source 42
    target 2042
  ]
  edge [
    source 42
    target 2043
  ]
  edge [
    source 42
    target 1479
  ]
  edge [
    source 42
    target 2100
  ]
  edge [
    source 42
    target 2101
  ]
  edge [
    source 42
    target 2102
  ]
  edge [
    source 42
    target 2103
  ]
  edge [
    source 42
    target 2104
  ]
  edge [
    source 42
    target 2105
  ]
  edge [
    source 42
    target 2106
  ]
  edge [
    source 42
    target 2107
  ]
  edge [
    source 42
    target 2108
  ]
  edge [
    source 42
    target 524
  ]
  edge [
    source 42
    target 2109
  ]
  edge [
    source 42
    target 2110
  ]
  edge [
    source 42
    target 2111
  ]
  edge [
    source 42
    target 1502
  ]
  edge [
    source 42
    target 287
  ]
  edge [
    source 42
    target 1469
  ]
  edge [
    source 42
    target 2112
  ]
  edge [
    source 42
    target 2113
  ]
  edge [
    source 42
    target 2114
  ]
  edge [
    source 42
    target 2115
  ]
  edge [
    source 42
    target 2116
  ]
  edge [
    source 42
    target 2117
  ]
  edge [
    source 42
    target 2118
  ]
  edge [
    source 42
    target 451
  ]
  edge [
    source 42
    target 2119
  ]
  edge [
    source 42
    target 2120
  ]
  edge [
    source 42
    target 2121
  ]
  edge [
    source 42
    target 2122
  ]
  edge [
    source 42
    target 1715
  ]
  edge [
    source 42
    target 2123
  ]
  edge [
    source 42
    target 2124
  ]
  edge [
    source 42
    target 2125
  ]
  edge [
    source 42
    target 2126
  ]
  edge [
    source 42
    target 2127
  ]
  edge [
    source 42
    target 2128
  ]
  edge [
    source 42
    target 2129
  ]
  edge [
    source 42
    target 2130
  ]
  edge [
    source 42
    target 2131
  ]
  edge [
    source 42
    target 2132
  ]
  edge [
    source 42
    target 2133
  ]
  edge [
    source 42
    target 2134
  ]
  edge [
    source 42
    target 2135
  ]
  edge [
    source 42
    target 1473
  ]
  edge [
    source 42
    target 2136
  ]
  edge [
    source 42
    target 2137
  ]
  edge [
    source 42
    target 2138
  ]
  edge [
    source 42
    target 2139
  ]
  edge [
    source 42
    target 1733
  ]
  edge [
    source 42
    target 2140
  ]
  edge [
    source 42
    target 2141
  ]
  edge [
    source 42
    target 2142
  ]
  edge [
    source 42
    target 2143
  ]
  edge [
    source 42
    target 2144
  ]
  edge [
    source 42
    target 2145
  ]
  edge [
    source 42
    target 2146
  ]
  edge [
    source 42
    target 2147
  ]
  edge [
    source 42
    target 2148
  ]
  edge [
    source 42
    target 2149
  ]
  edge [
    source 42
    target 2150
  ]
  edge [
    source 42
    target 493
  ]
  edge [
    source 42
    target 2151
  ]
  edge [
    source 42
    target 2152
  ]
  edge [
    source 42
    target 2153
  ]
  edge [
    source 42
    target 2154
  ]
  edge [
    source 42
    target 2155
  ]
  edge [
    source 42
    target 826
  ]
  edge [
    source 42
    target 2156
  ]
  edge [
    source 42
    target 2157
  ]
  edge [
    source 42
    target 2158
  ]
  edge [
    source 42
    target 2159
  ]
  edge [
    source 42
    target 2160
  ]
  edge [
    source 42
    target 2161
  ]
  edge [
    source 42
    target 2162
  ]
  edge [
    source 42
    target 2163
  ]
  edge [
    source 42
    target 2164
  ]
  edge [
    source 42
    target 2165
  ]
  edge [
    source 42
    target 593
  ]
  edge [
    source 42
    target 2166
  ]
  edge [
    source 42
    target 2167
  ]
  edge [
    source 42
    target 2168
  ]
  edge [
    source 42
    target 2169
  ]
  edge [
    source 42
    target 2170
  ]
  edge [
    source 42
    target 2171
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 2137
  ]
  edge [
    source 43
    target 2138
  ]
  edge [
    source 43
    target 2139
  ]
  edge [
    source 43
    target 1733
  ]
  edge [
    source 43
    target 2140
  ]
  edge [
    source 43
    target 2141
  ]
  edge [
    source 43
    target 2142
  ]
  edge [
    source 43
    target 2143
  ]
  edge [
    source 43
    target 2144
  ]
  edge [
    source 43
    target 2145
  ]
  edge [
    source 43
    target 2146
  ]
  edge [
    source 43
    target 2147
  ]
  edge [
    source 43
    target 2148
  ]
  edge [
    source 43
    target 2149
  ]
  edge [
    source 43
    target 2150
  ]
  edge [
    source 43
    target 493
  ]
  edge [
    source 43
    target 2151
  ]
  edge [
    source 43
    target 2152
  ]
  edge [
    source 43
    target 2153
  ]
  edge [
    source 43
    target 2154
  ]
  edge [
    source 43
    target 2155
  ]
  edge [
    source 43
    target 826
  ]
  edge [
    source 43
    target 2156
  ]
  edge [
    source 43
    target 2157
  ]
  edge [
    source 43
    target 2158
  ]
  edge [
    source 43
    target 2159
  ]
  edge [
    source 43
    target 2160
  ]
  edge [
    source 43
    target 2161
  ]
  edge [
    source 43
    target 2162
  ]
  edge [
    source 43
    target 2163
  ]
  edge [
    source 43
    target 2164
  ]
  edge [
    source 43
    target 2165
  ]
  edge [
    source 43
    target 593
  ]
  edge [
    source 43
    target 2166
  ]
  edge [
    source 43
    target 2167
  ]
  edge [
    source 43
    target 2168
  ]
  edge [
    source 43
    target 2169
  ]
  edge [
    source 43
    target 2170
  ]
  edge [
    source 43
    target 2171
  ]
  edge [
    source 43
    target 2172
  ]
  edge [
    source 43
    target 2173
  ]
  edge [
    source 43
    target 2174
  ]
  edge [
    source 43
    target 2175
  ]
  edge [
    source 43
    target 2176
  ]
  edge [
    source 43
    target 2177
  ]
  edge [
    source 43
    target 2178
  ]
  edge [
    source 43
    target 2179
  ]
  edge [
    source 43
    target 2180
  ]
  edge [
    source 43
    target 131
  ]
  edge [
    source 43
    target 2181
  ]
  edge [
    source 43
    target 2182
  ]
  edge [
    source 43
    target 2183
  ]
  edge [
    source 43
    target 287
  ]
  edge [
    source 43
    target 1731
  ]
  edge [
    source 43
    target 147
  ]
  edge [
    source 43
    target 2184
  ]
  edge [
    source 43
    target 2185
  ]
  edge [
    source 43
    target 2186
  ]
  edge [
    source 43
    target 160
  ]
  edge [
    source 43
    target 1226
  ]
  edge [
    source 43
    target 2187
  ]
  edge [
    source 43
    target 2188
  ]
  edge [
    source 43
    target 106
  ]
  edge [
    source 43
    target 2189
  ]
  edge [
    source 43
    target 2190
  ]
  edge [
    source 43
    target 2191
  ]
  edge [
    source 43
    target 1469
  ]
  edge [
    source 43
    target 2192
  ]
  edge [
    source 43
    target 2193
  ]
  edge [
    source 43
    target 194
  ]
  edge [
    source 43
    target 788
  ]
  edge [
    source 43
    target 2194
  ]
  edge [
    source 43
    target 1929
  ]
  edge [
    source 43
    target 2195
  ]
  edge [
    source 43
    target 2196
  ]
  edge [
    source 43
    target 2197
  ]
  edge [
    source 43
    target 2198
  ]
  edge [
    source 43
    target 2199
  ]
  edge [
    source 43
    target 2200
  ]
  edge [
    source 43
    target 2201
  ]
  edge [
    source 43
    target 1079
  ]
  edge [
    source 43
    target 2202
  ]
  edge [
    source 43
    target 2203
  ]
  edge [
    source 43
    target 2204
  ]
  edge [
    source 43
    target 142
  ]
  edge [
    source 43
    target 2205
  ]
  edge [
    source 43
    target 2206
  ]
  edge [
    source 43
    target 2207
  ]
  edge [
    source 43
    target 2208
  ]
  edge [
    source 43
    target 2209
  ]
  edge [
    source 43
    target 2210
  ]
  edge [
    source 43
    target 919
  ]
  edge [
    source 43
    target 2211
  ]
  edge [
    source 43
    target 2212
  ]
  edge [
    source 43
    target 2213
  ]
  edge [
    source 43
    target 2214
  ]
  edge [
    source 43
    target 2215
  ]
  edge [
    source 43
    target 931
  ]
  edge [
    source 43
    target 2216
  ]
  edge [
    source 43
    target 813
  ]
  edge [
    source 43
    target 2217
  ]
  edge [
    source 43
    target 490
  ]
  edge [
    source 43
    target 2218
  ]
  edge [
    source 43
    target 2219
  ]
  edge [
    source 43
    target 2220
  ]
  edge [
    source 43
    target 2221
  ]
  edge [
    source 43
    target 124
  ]
  edge [
    source 43
    target 2222
  ]
  edge [
    source 43
    target 2223
  ]
  edge [
    source 43
    target 2224
  ]
  edge [
    source 43
    target 2225
  ]
  edge [
    source 43
    target 2226
  ]
  edge [
    source 43
    target 2227
  ]
  edge [
    source 43
    target 2228
  ]
  edge [
    source 43
    target 2229
  ]
  edge [
    source 43
    target 2230
  ]
  edge [
    source 43
    target 2231
  ]
  edge [
    source 43
    target 2232
  ]
  edge [
    source 43
    target 1042
  ]
  edge [
    source 43
    target 1043
  ]
  edge [
    source 43
    target 1044
  ]
  edge [
    source 43
    target 1045
  ]
  edge [
    source 43
    target 1046
  ]
  edge [
    source 43
    target 1047
  ]
  edge [
    source 43
    target 1048
  ]
  edge [
    source 43
    target 1049
  ]
  edge [
    source 43
    target 1050
  ]
  edge [
    source 43
    target 771
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 783
  ]
  edge [
    source 44
    target 2233
  ]
  edge [
    source 44
    target 2234
  ]
  edge [
    source 44
    target 2235
  ]
  edge [
    source 44
    target 2236
  ]
  edge [
    source 44
    target 1467
  ]
  edge [
    source 44
    target 2041
  ]
  edge [
    source 44
    target 2237
  ]
  edge [
    source 44
    target 1486
  ]
  edge [
    source 44
    target 2238
  ]
  edge [
    source 44
    target 2239
  ]
  edge [
    source 44
    target 2240
  ]
  edge [
    source 44
    target 2241
  ]
  edge [
    source 44
    target 2242
  ]
  edge [
    source 44
    target 1135
  ]
  edge [
    source 44
    target 2243
  ]
  edge [
    source 44
    target 2244
  ]
  edge [
    source 44
    target 2245
  ]
  edge [
    source 44
    target 2246
  ]
  edge [
    source 44
    target 2247
  ]
  edge [
    source 44
    target 147
  ]
  edge [
    source 44
    target 2248
  ]
  edge [
    source 44
    target 2249
  ]
  edge [
    source 44
    target 2250
  ]
  edge [
    source 44
    target 2251
  ]
  edge [
    source 44
    target 2252
  ]
  edge [
    source 44
    target 2253
  ]
  edge [
    source 44
    target 2254
  ]
  edge [
    source 44
    target 2255
  ]
  edge [
    source 44
    target 2256
  ]
  edge [
    source 44
    target 1071
  ]
  edge [
    source 44
    target 1462
  ]
  edge [
    source 44
    target 2257
  ]
  edge [
    source 44
    target 2258
  ]
  edge [
    source 44
    target 2259
  ]
  edge [
    source 44
    target 2260
  ]
  edge [
    source 44
    target 2261
  ]
  edge [
    source 44
    target 767
  ]
  edge [
    source 44
    target 2262
  ]
  edge [
    source 44
    target 2263
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 2264
  ]
  edge [
    source 45
    target 2265
  ]
  edge [
    source 45
    target 2214
  ]
  edge [
    source 45
    target 2266
  ]
  edge [
    source 45
    target 473
  ]
  edge [
    source 45
    target 2267
  ]
  edge [
    source 45
    target 2268
  ]
  edge [
    source 45
    target 2269
  ]
  edge [
    source 45
    target 2270
  ]
  edge [
    source 45
    target 2271
  ]
  edge [
    source 45
    target 2272
  ]
  edge [
    source 45
    target 2273
  ]
  edge [
    source 45
    target 2274
  ]
  edge [
    source 45
    target 2275
  ]
  edge [
    source 45
    target 826
  ]
  edge [
    source 45
    target 1899
  ]
  edge [
    source 45
    target 2276
  ]
  edge [
    source 45
    target 1777
  ]
  edge [
    source 45
    target 2277
  ]
  edge [
    source 45
    target 2278
  ]
  edge [
    source 45
    target 2279
  ]
  edge [
    source 45
    target 2280
  ]
  edge [
    source 45
    target 2281
  ]
  edge [
    source 45
    target 906
  ]
  edge [
    source 45
    target 2282
  ]
  edge [
    source 45
    target 2283
  ]
  edge [
    source 45
    target 2284
  ]
  edge [
    source 45
    target 2285
  ]
  edge [
    source 45
    target 1968
  ]
  edge [
    source 45
    target 2286
  ]
  edge [
    source 45
    target 2287
  ]
  edge [
    source 45
    target 2288
  ]
  edge [
    source 45
    target 2289
  ]
  edge [
    source 45
    target 2290
  ]
  edge [
    source 45
    target 2291
  ]
  edge [
    source 45
    target 2292
  ]
  edge [
    source 45
    target 2293
  ]
  edge [
    source 45
    target 2294
  ]
  edge [
    source 45
    target 2295
  ]
  edge [
    source 45
    target 2296
  ]
  edge [
    source 45
    target 2297
  ]
  edge [
    source 45
    target 2298
  ]
  edge [
    source 45
    target 2299
  ]
  edge [
    source 45
    target 2300
  ]
  edge [
    source 45
    target 518
  ]
  edge [
    source 45
    target 2301
  ]
  edge [
    source 45
    target 2302
  ]
  edge [
    source 45
    target 2303
  ]
  edge [
    source 45
    target 2304
  ]
  edge [
    source 45
    target 2305
  ]
  edge [
    source 45
    target 2306
  ]
  edge [
    source 45
    target 2307
  ]
  edge [
    source 45
    target 2308
  ]
  edge [
    source 45
    target 2309
  ]
  edge [
    source 45
    target 2310
  ]
  edge [
    source 45
    target 2311
  ]
  edge [
    source 45
    target 2312
  ]
  edge [
    source 45
    target 2313
  ]
  edge [
    source 45
    target 2314
  ]
  edge [
    source 45
    target 2315
  ]
  edge [
    source 45
    target 2316
  ]
  edge [
    source 45
    target 2317
  ]
  edge [
    source 45
    target 1390
  ]
  edge [
    source 45
    target 2318
  ]
  edge [
    source 45
    target 485
  ]
  edge [
    source 45
    target 138
  ]
  edge [
    source 45
    target 2319
  ]
  edge [
    source 45
    target 2320
  ]
  edge [
    source 45
    target 2321
  ]
  edge [
    source 45
    target 2322
  ]
  edge [
    source 45
    target 2323
  ]
  edge [
    source 45
    target 2324
  ]
  edge [
    source 45
    target 2325
  ]
  edge [
    source 45
    target 2326
  ]
  edge [
    source 45
    target 2327
  ]
  edge [
    source 45
    target 2328
  ]
  edge [
    source 45
    target 2329
  ]
  edge [
    source 45
    target 2330
  ]
  edge [
    source 45
    target 2331
  ]
  edge [
    source 45
    target 2332
  ]
  edge [
    source 45
    target 2333
  ]
  edge [
    source 45
    target 2334
  ]
  edge [
    source 45
    target 2335
  ]
  edge [
    source 45
    target 1874
  ]
  edge [
    source 45
    target 1364
  ]
  edge [
    source 45
    target 1274
  ]
  edge [
    source 45
    target 2336
  ]
  edge [
    source 45
    target 2337
  ]
  edge [
    source 45
    target 2338
  ]
  edge [
    source 45
    target 1278
  ]
  edge [
    source 45
    target 2339
  ]
  edge [
    source 45
    target 2340
  ]
  edge [
    source 45
    target 2341
  ]
  edge [
    source 45
    target 600
  ]
  edge [
    source 45
    target 2342
  ]
  edge [
    source 45
    target 2343
  ]
  edge [
    source 45
    target 2344
  ]
  edge [
    source 45
    target 2345
  ]
  edge [
    source 45
    target 2346
  ]
  edge [
    source 45
    target 988
  ]
  edge [
    source 45
    target 2347
  ]
  edge [
    source 45
    target 1526
  ]
  edge [
    source 45
    target 2348
  ]
  edge [
    source 45
    target 2349
  ]
  edge [
    source 45
    target 2350
  ]
  edge [
    source 45
    target 2186
  ]
  edge [
    source 45
    target 2351
  ]
  edge [
    source 45
    target 2352
  ]
  edge [
    source 45
    target 2353
  ]
  edge [
    source 45
    target 2354
  ]
  edge [
    source 45
    target 2355
  ]
  edge [
    source 45
    target 2356
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 2357
  ]
  edge [
    source 46
    target 2358
  ]
  edge [
    source 46
    target 2359
  ]
  edge [
    source 46
    target 2360
  ]
  edge [
    source 46
    target 2361
  ]
  edge [
    source 46
    target 2362
  ]
  edge [
    source 46
    target 2363
  ]
  edge [
    source 46
    target 2364
  ]
  edge [
    source 46
    target 2365
  ]
  edge [
    source 46
    target 685
  ]
  edge [
    source 46
    target 2366
  ]
  edge [
    source 46
    target 2367
  ]
  edge [
    source 46
    target 2368
  ]
  edge [
    source 46
    target 2369
  ]
  edge [
    source 46
    target 2370
  ]
  edge [
    source 46
    target 2371
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 783
  ]
  edge [
    source 47
    target 1043
  ]
  edge [
    source 47
    target 2372
  ]
  edge [
    source 47
    target 1135
  ]
  edge [
    source 47
    target 2373
  ]
  edge [
    source 47
    target 2374
  ]
  edge [
    source 47
    target 2375
  ]
  edge [
    source 47
    target 781
  ]
  edge [
    source 47
    target 772
  ]
  edge [
    source 47
    target 2376
  ]
  edge [
    source 47
    target 2377
  ]
  edge [
    source 47
    target 2378
  ]
  edge [
    source 47
    target 2379
  ]
  edge [
    source 47
    target 2380
  ]
  edge [
    source 47
    target 2381
  ]
  edge [
    source 47
    target 2382
  ]
  edge [
    source 47
    target 2036
  ]
  edge [
    source 47
    target 2383
  ]
  edge [
    source 47
    target 2048
  ]
  edge [
    source 47
    target 2049
  ]
  edge [
    source 47
    target 2384
  ]
  edge [
    source 47
    target 1453
  ]
  edge [
    source 47
    target 2385
  ]
  edge [
    source 47
    target 2386
  ]
  edge [
    source 47
    target 2387
  ]
  edge [
    source 47
    target 2388
  ]
  edge [
    source 47
    target 1515
  ]
  edge [
    source 47
    target 2389
  ]
  edge [
    source 47
    target 2390
  ]
  edge [
    source 47
    target 2391
  ]
  edge [
    source 47
    target 2392
  ]
  edge [
    source 47
    target 2393
  ]
  edge [
    source 47
    target 2394
  ]
  edge [
    source 47
    target 2395
  ]
  edge [
    source 47
    target 2396
  ]
  edge [
    source 47
    target 2397
  ]
  edge [
    source 47
    target 2398
  ]
  edge [
    source 47
    target 2399
  ]
  edge [
    source 47
    target 2258
  ]
  edge [
    source 47
    target 2400
  ]
  edge [
    source 47
    target 2110
  ]
  edge [
    source 47
    target 2401
  ]
  edge [
    source 47
    target 2134
  ]
  edge [
    source 47
    target 2402
  ]
  edge [
    source 47
    target 2403
  ]
  edge [
    source 47
    target 2404
  ]
  edge [
    source 47
    target 2405
  ]
  edge [
    source 47
    target 2039
  ]
  edge [
    source 47
    target 1461
  ]
  edge [
    source 47
    target 2240
  ]
  edge [
    source 47
    target 2241
  ]
  edge [
    source 47
    target 2242
  ]
  edge [
    source 47
    target 2243
  ]
  edge [
    source 47
    target 2244
  ]
  edge [
    source 47
    target 2245
  ]
  edge [
    source 47
    target 2246
  ]
  edge [
    source 47
    target 2247
  ]
  edge [
    source 47
    target 147
  ]
  edge [
    source 47
    target 2248
  ]
  edge [
    source 47
    target 2249
  ]
  edge [
    source 47
    target 2250
  ]
  edge [
    source 47
    target 2251
  ]
  edge [
    source 47
    target 2252
  ]
  edge [
    source 47
    target 2253
  ]
  edge [
    source 47
    target 2254
  ]
  edge [
    source 47
    target 2255
  ]
  edge [
    source 47
    target 2256
  ]
  edge [
    source 47
    target 1071
  ]
  edge [
    source 47
    target 1462
  ]
  edge [
    source 47
    target 2406
  ]
  edge [
    source 47
    target 2407
  ]
  edge [
    source 47
    target 1380
  ]
  edge [
    source 47
    target 2408
  ]
  edge [
    source 47
    target 2409
  ]
  edge [
    source 47
    target 2410
  ]
  edge [
    source 47
    target 2411
  ]
  edge [
    source 47
    target 2412
  ]
  edge [
    source 47
    target 778
  ]
  edge [
    source 47
    target 2413
  ]
  edge [
    source 47
    target 2414
  ]
  edge [
    source 47
    target 2415
  ]
  edge [
    source 47
    target 2416
  ]
  edge [
    source 47
    target 2417
  ]
  edge [
    source 47
    target 2418
  ]
  edge [
    source 47
    target 2419
  ]
  edge [
    source 47
    target 2420
  ]
  edge [
    source 47
    target 2421
  ]
  edge [
    source 47
    target 2422
  ]
  edge [
    source 47
    target 1516
  ]
  edge [
    source 47
    target 2423
  ]
  edge [
    source 47
    target 2424
  ]
  edge [
    source 47
    target 2425
  ]
  edge [
    source 47
    target 2426
  ]
  edge [
    source 47
    target 2427
  ]
  edge [
    source 47
    target 2428
  ]
  edge [
    source 47
    target 2429
  ]
  edge [
    source 47
    target 2430
  ]
  edge [
    source 47
    target 2431
  ]
  edge [
    source 47
    target 2432
  ]
  edge [
    source 47
    target 2433
  ]
  edge [
    source 47
    target 2434
  ]
  edge [
    source 47
    target 2435
  ]
  edge [
    source 47
    target 2436
  ]
  edge [
    source 47
    target 2437
  ]
  edge [
    source 47
    target 1130
  ]
  edge [
    source 47
    target 2438
  ]
  edge [
    source 47
    target 2439
  ]
  edge [
    source 47
    target 154
  ]
  edge [
    source 47
    target 2440
  ]
  edge [
    source 47
    target 2441
  ]
  edge [
    source 47
    target 2442
  ]
  edge [
    source 47
    target 1846
  ]
  edge [
    source 47
    target 2443
  ]
  edge [
    source 47
    target 2237
  ]
  edge [
    source 47
    target 2444
  ]
  edge [
    source 47
    target 2445
  ]
  edge [
    source 47
    target 2446
  ]
  edge [
    source 47
    target 2447
  ]
  edge [
    source 47
    target 2448
  ]
  edge [
    source 47
    target 2449
  ]
  edge [
    source 47
    target 1731
  ]
  edge [
    source 47
    target 2450
  ]
  edge [
    source 47
    target 2451
  ]
  edge [
    source 47
    target 2452
  ]
  edge [
    source 47
    target 999
  ]
  edge [
    source 47
    target 2453
  ]
  edge [
    source 47
    target 2454
  ]
  edge [
    source 47
    target 1724
  ]
  edge [
    source 47
    target 2455
  ]
  edge [
    source 47
    target 106
  ]
  edge [
    source 47
    target 2456
  ]
  edge [
    source 47
    target 1874
  ]
  edge [
    source 47
    target 2457
  ]
  edge [
    source 47
    target 2345
  ]
  edge [
    source 47
    target 2458
  ]
  edge [
    source 47
    target 2459
  ]
  edge [
    source 47
    target 2460
  ]
  edge [
    source 47
    target 2461
  ]
  edge [
    source 47
    target 2462
  ]
  edge [
    source 47
    target 2463
  ]
  edge [
    source 47
    target 2464
  ]
  edge [
    source 47
    target 2465
  ]
  edge [
    source 47
    target 791
  ]
  edge [
    source 47
    target 2466
  ]
  edge [
    source 47
    target 2467
  ]
  edge [
    source 47
    target 2468
  ]
  edge [
    source 47
    target 2469
  ]
  edge [
    source 47
    target 272
  ]
  edge [
    source 47
    target 786
  ]
  edge [
    source 47
    target 1966
  ]
  edge [
    source 47
    target 2470
  ]
  edge [
    source 47
    target 2471
  ]
  edge [
    source 47
    target 2472
  ]
  edge [
    source 47
    target 1170
  ]
  edge [
    source 47
    target 829
  ]
  edge [
    source 47
    target 2473
  ]
  edge [
    source 47
    target 2474
  ]
  edge [
    source 47
    target 1959
  ]
  edge [
    source 47
    target 1452
  ]
  edge [
    source 47
    target 2475
  ]
  edge [
    source 47
    target 507
  ]
  edge [
    source 47
    target 2476
  ]
  edge [
    source 47
    target 2477
  ]
  edge [
    source 47
    target 2478
  ]
  edge [
    source 47
    target 2479
  ]
  edge [
    source 47
    target 2480
  ]
  edge [
    source 47
    target 1238
  ]
  edge [
    source 47
    target 2481
  ]
  edge [
    source 47
    target 2482
  ]
  edge [
    source 47
    target 2483
  ]
  edge [
    source 47
    target 2484
  ]
  edge [
    source 47
    target 2485
  ]
  edge [
    source 47
    target 2486
  ]
  edge [
    source 47
    target 2487
  ]
  edge [
    source 47
    target 1163
  ]
  edge [
    source 47
    target 478
  ]
  edge [
    source 47
    target 2488
  ]
  edge [
    source 47
    target 964
  ]
  edge [
    source 47
    target 2489
  ]
  edge [
    source 47
    target 2490
  ]
  edge [
    source 47
    target 2491
  ]
  edge [
    source 47
    target 2492
  ]
  edge [
    source 47
    target 2493
  ]
  edge [
    source 47
    target 2494
  ]
  edge [
    source 47
    target 966
  ]
  edge [
    source 47
    target 2495
  ]
  edge [
    source 47
    target 2496
  ]
  edge [
    source 47
    target 2497
  ]
  edge [
    source 47
    target 2498
  ]
  edge [
    source 47
    target 2499
  ]
  edge [
    source 47
    target 2500
  ]
  edge [
    source 47
    target 2501
  ]
  edge [
    source 47
    target 2502
  ]
  edge [
    source 47
    target 1167
  ]
  edge [
    source 47
    target 2503
  ]
  edge [
    source 47
    target 2504
  ]
  edge [
    source 47
    target 2505
  ]
  edge [
    source 47
    target 2506
  ]
  edge [
    source 47
    target 2507
  ]
  edge [
    source 47
    target 2508
  ]
  edge [
    source 47
    target 2509
  ]
  edge [
    source 47
    target 2510
  ]
  edge [
    source 47
    target 2511
  ]
  edge [
    source 47
    target 2512
  ]
  edge [
    source 47
    target 2513
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 705
  ]
  edge [
    source 49
    target 2514
  ]
  edge [
    source 49
    target 2515
  ]
  edge [
    source 49
    target 2516
  ]
  edge [
    source 49
    target 65
  ]
  edge [
    source 49
    target 2517
  ]
  edge [
    source 49
    target 725
  ]
  edge [
    source 49
    target 733
  ]
  edge [
    source 49
    target 2518
  ]
  edge [
    source 49
    target 727
  ]
  edge [
    source 49
    target 2519
  ]
  edge [
    source 49
    target 648
  ]
  edge [
    source 49
    target 2520
  ]
  edge [
    source 49
    target 2521
  ]
  edge [
    source 49
    target 2522
  ]
  edge [
    source 49
    target 1657
  ]
  edge [
    source 49
    target 2523
  ]
  edge [
    source 49
    target 2524
  ]
  edge [
    source 49
    target 2525
  ]
  edge [
    source 49
    target 2526
  ]
  edge [
    source 49
    target 2527
  ]
]
