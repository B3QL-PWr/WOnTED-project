graph [
  node [
    id 0
    label "audi"
    origin "text"
  ]
  node [
    id 1
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 2
    label "samoch&#243;d"
  ]
  node [
    id 3
    label "Audi"
  ]
  node [
    id 4
    label "poduszka_powietrzna"
  ]
  node [
    id 5
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 6
    label "pompa_wodna"
  ]
  node [
    id 7
    label "bak"
  ]
  node [
    id 8
    label "deska_rozdzielcza"
  ]
  node [
    id 9
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 10
    label "spryskiwacz"
  ]
  node [
    id 11
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 12
    label "baga&#380;nik"
  ]
  node [
    id 13
    label "poci&#261;g_drogowy"
  ]
  node [
    id 14
    label "immobilizer"
  ]
  node [
    id 15
    label "kierownica"
  ]
  node [
    id 16
    label "ABS"
  ]
  node [
    id 17
    label "dwu&#347;lad"
  ]
  node [
    id 18
    label "tempomat"
  ]
  node [
    id 19
    label "pojazd_drogowy"
  ]
  node [
    id 20
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 21
    label "wycieraczka"
  ]
  node [
    id 22
    label "most"
  ]
  node [
    id 23
    label "silnik"
  ]
  node [
    id 24
    label "t&#322;umik"
  ]
  node [
    id 25
    label "dachowanie"
  ]
  node [
    id 26
    label "50"
  ]
  node [
    id 27
    label "volkswagen"
  ]
  node [
    id 28
    label "polo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 27
    target 28
  ]
]
