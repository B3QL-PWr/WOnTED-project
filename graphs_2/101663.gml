graph [
  node [
    id 0
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 1
    label "marek"
    origin "text"
  ]
  node [
    id 2
    label "polak"
    origin "text"
  ]
  node [
    id 3
    label "ablegat"
  ]
  node [
    id 4
    label "izba_ni&#380;sza"
  ]
  node [
    id 5
    label "Korwin"
  ]
  node [
    id 6
    label "dyscyplina_partyjna"
  ]
  node [
    id 7
    label "Miko&#322;ajczyk"
  ]
  node [
    id 8
    label "kurier_dyplomatyczny"
  ]
  node [
    id 9
    label "wys&#322;annik"
  ]
  node [
    id 10
    label "poselstwo"
  ]
  node [
    id 11
    label "parlamentarzysta"
  ]
  node [
    id 12
    label "przedstawiciel"
  ]
  node [
    id 13
    label "dyplomata"
  ]
  node [
    id 14
    label "klubista"
  ]
  node [
    id 15
    label "reprezentacja"
  ]
  node [
    id 16
    label "klub"
  ]
  node [
    id 17
    label "cz&#322;onek"
  ]
  node [
    id 18
    label "mandatariusz"
  ]
  node [
    id 19
    label "grupa_bilateralna"
  ]
  node [
    id 20
    label "polityk"
  ]
  node [
    id 21
    label "parlament"
  ]
  node [
    id 22
    label "mi&#322;y"
  ]
  node [
    id 23
    label "cz&#322;owiek"
  ]
  node [
    id 24
    label "korpus_dyplomatyczny"
  ]
  node [
    id 25
    label "dyplomatyczny"
  ]
  node [
    id 26
    label "takt"
  ]
  node [
    id 27
    label "Metternich"
  ]
  node [
    id 28
    label "dostojnik"
  ]
  node [
    id 29
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 30
    label "przyk&#322;ad"
  ]
  node [
    id 31
    label "substytuowa&#263;"
  ]
  node [
    id 32
    label "substytuowanie"
  ]
  node [
    id 33
    label "zast&#281;pca"
  ]
  node [
    id 34
    label "hygrofit"
  ]
  node [
    id 35
    label "bylina"
  ]
  node [
    id 36
    label "selerowate"
  ]
  node [
    id 37
    label "higrofil"
  ]
  node [
    id 38
    label "ro&#347;lina"
  ]
  node [
    id 39
    label "ludowy"
  ]
  node [
    id 40
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 41
    label "utw&#243;r_epicki"
  ]
  node [
    id 42
    label "pie&#347;&#324;"
  ]
  node [
    id 43
    label "selerowce"
  ]
  node [
    id 44
    label "polski"
  ]
  node [
    id 45
    label "przedmiot"
  ]
  node [
    id 46
    label "Polish"
  ]
  node [
    id 47
    label "goniony"
  ]
  node [
    id 48
    label "oberek"
  ]
  node [
    id 49
    label "ryba_po_grecku"
  ]
  node [
    id 50
    label "sztajer"
  ]
  node [
    id 51
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 52
    label "krakowiak"
  ]
  node [
    id 53
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 54
    label "pierogi_ruskie"
  ]
  node [
    id 55
    label "lacki"
  ]
  node [
    id 56
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 57
    label "chodzony"
  ]
  node [
    id 58
    label "po_polsku"
  ]
  node [
    id 59
    label "mazur"
  ]
  node [
    id 60
    label "polsko"
  ]
  node [
    id 61
    label "skoczny"
  ]
  node [
    id 62
    label "drabant"
  ]
  node [
    id 63
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 64
    label "j&#281;zyk"
  ]
  node [
    id 65
    label "marka"
  ]
  node [
    id 66
    label "Stefan"
  ]
  node [
    id 67
    label "Niesio&#322;owski"
  ]
  node [
    id 68
    label "Edwarda"
  ]
  node [
    id 69
    label "siarka"
  ]
  node [
    id 70
    label "prawo"
  ]
  node [
    id 71
    label "i"
  ]
  node [
    id 72
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 73
    label "ministerstwo"
  ]
  node [
    id 74
    label "obrona"
  ]
  node [
    id 75
    label "narodowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 75
  ]
  edge [
    source 74
    target 75
  ]
]
