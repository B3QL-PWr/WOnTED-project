graph [
  node [
    id 0
    label "lekarka"
    origin "text"
  ]
  node [
    id 1
    label "linares"
    origin "text"
  ]
  node [
    id 2
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zaatakowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "przez"
    origin "text"
  ]
  node [
    id 5
    label "nielegalny"
    origin "text"
  ]
  node [
    id 6
    label "migrant"
    origin "text"
  ]
  node [
    id 7
    label "pakistan"
    origin "text"
  ]
  node [
    id 8
    label "pr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "udzieli&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pomoc"
    origin "text"
  ]
  node [
    id 11
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 12
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 13
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 14
    label "osta&#263;_si&#281;"
  ]
  node [
    id 15
    label "change"
  ]
  node [
    id 16
    label "pozosta&#263;"
  ]
  node [
    id 17
    label "catch"
  ]
  node [
    id 18
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 19
    label "proceed"
  ]
  node [
    id 20
    label "support"
  ]
  node [
    id 21
    label "prze&#380;y&#263;"
  ]
  node [
    id 22
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 23
    label "nast&#261;pi&#263;"
  ]
  node [
    id 24
    label "attack"
  ]
  node [
    id 25
    label "przeby&#263;"
  ]
  node [
    id 26
    label "spell"
  ]
  node [
    id 27
    label "postara&#263;_si&#281;"
  ]
  node [
    id 28
    label "rozegra&#263;"
  ]
  node [
    id 29
    label "zrobi&#263;"
  ]
  node [
    id 30
    label "powiedzie&#263;"
  ]
  node [
    id 31
    label "anoint"
  ]
  node [
    id 32
    label "sport"
  ]
  node [
    id 33
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 34
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 35
    label "skrytykowa&#263;"
  ]
  node [
    id 36
    label "post&#261;pi&#263;"
  ]
  node [
    id 37
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 38
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 39
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 40
    label "zorganizowa&#263;"
  ]
  node [
    id 41
    label "appoint"
  ]
  node [
    id 42
    label "wystylizowa&#263;"
  ]
  node [
    id 43
    label "cause"
  ]
  node [
    id 44
    label "przerobi&#263;"
  ]
  node [
    id 45
    label "nabra&#263;"
  ]
  node [
    id 46
    label "make"
  ]
  node [
    id 47
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 48
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 49
    label "wydali&#263;"
  ]
  node [
    id 50
    label "work"
  ]
  node [
    id 51
    label "chemia"
  ]
  node [
    id 52
    label "spowodowa&#263;"
  ]
  node [
    id 53
    label "reakcja_chemiczna"
  ]
  node [
    id 54
    label "act"
  ]
  node [
    id 55
    label "discover"
  ]
  node [
    id 56
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 57
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 58
    label "wydoby&#263;"
  ]
  node [
    id 59
    label "poda&#263;"
  ]
  node [
    id 60
    label "okre&#347;li&#263;"
  ]
  node [
    id 61
    label "express"
  ]
  node [
    id 62
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 63
    label "wyrazi&#263;"
  ]
  node [
    id 64
    label "rzekn&#261;&#263;"
  ]
  node [
    id 65
    label "unwrap"
  ]
  node [
    id 66
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 67
    label "convey"
  ]
  node [
    id 68
    label "zaopiniowa&#263;"
  ]
  node [
    id 69
    label "review"
  ]
  node [
    id 70
    label "oceni&#263;"
  ]
  node [
    id 71
    label "play"
  ]
  node [
    id 72
    label "przeprowadzi&#263;"
  ]
  node [
    id 73
    label "spo&#380;y&#263;"
  ]
  node [
    id 74
    label "zakosztowa&#263;"
  ]
  node [
    id 75
    label "sprawdzi&#263;"
  ]
  node [
    id 76
    label "podj&#261;&#263;"
  ]
  node [
    id 77
    label "try"
  ]
  node [
    id 78
    label "pos&#322;u&#380;y&#263;_si&#281;"
  ]
  node [
    id 79
    label "supervene"
  ]
  node [
    id 80
    label "nacisn&#261;&#263;"
  ]
  node [
    id 81
    label "gamble"
  ]
  node [
    id 82
    label "odby&#263;"
  ]
  node [
    id 83
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 84
    label "traversal"
  ]
  node [
    id 85
    label "overwhelm"
  ]
  node [
    id 86
    label "zgrupowanie"
  ]
  node [
    id 87
    label "kultura_fizyczna"
  ]
  node [
    id 88
    label "usportowienie"
  ]
  node [
    id 89
    label "atakowa&#263;"
  ]
  node [
    id 90
    label "zaatakowanie"
  ]
  node [
    id 91
    label "atakowanie"
  ]
  node [
    id 92
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 93
    label "usportowi&#263;"
  ]
  node [
    id 94
    label "sokolstwo"
  ]
  node [
    id 95
    label "nieoficjalny"
  ]
  node [
    id 96
    label "zdelegalizowanie"
  ]
  node [
    id 97
    label "nielegalnie"
  ]
  node [
    id 98
    label "delegalizowanie"
  ]
  node [
    id 99
    label "nieoficjalnie"
  ]
  node [
    id 100
    label "nieformalny"
  ]
  node [
    id 101
    label "powodowanie"
  ]
  node [
    id 102
    label "uniewa&#380;nianie"
  ]
  node [
    id 103
    label "spowodowanie"
  ]
  node [
    id 104
    label "uniewa&#380;nienie"
  ]
  node [
    id 105
    label "delegalizowanie_si&#281;"
  ]
  node [
    id 106
    label "czynno&#347;&#263;"
  ]
  node [
    id 107
    label "unlawfully"
  ]
  node [
    id 108
    label "na_nielegalu"
  ]
  node [
    id 109
    label "cz&#322;owiek"
  ]
  node [
    id 110
    label "ludzko&#347;&#263;"
  ]
  node [
    id 111
    label "asymilowanie"
  ]
  node [
    id 112
    label "wapniak"
  ]
  node [
    id 113
    label "asymilowa&#263;"
  ]
  node [
    id 114
    label "os&#322;abia&#263;"
  ]
  node [
    id 115
    label "posta&#263;"
  ]
  node [
    id 116
    label "hominid"
  ]
  node [
    id 117
    label "podw&#322;adny"
  ]
  node [
    id 118
    label "os&#322;abianie"
  ]
  node [
    id 119
    label "g&#322;owa"
  ]
  node [
    id 120
    label "figura"
  ]
  node [
    id 121
    label "portrecista"
  ]
  node [
    id 122
    label "dwun&#243;g"
  ]
  node [
    id 123
    label "profanum"
  ]
  node [
    id 124
    label "mikrokosmos"
  ]
  node [
    id 125
    label "nasada"
  ]
  node [
    id 126
    label "duch"
  ]
  node [
    id 127
    label "antropochoria"
  ]
  node [
    id 128
    label "osoba"
  ]
  node [
    id 129
    label "wz&#243;r"
  ]
  node [
    id 130
    label "senior"
  ]
  node [
    id 131
    label "oddzia&#322;ywanie"
  ]
  node [
    id 132
    label "Adam"
  ]
  node [
    id 133
    label "homo_sapiens"
  ]
  node [
    id 134
    label "polifag"
  ]
  node [
    id 135
    label "stara&#263;_si&#281;"
  ]
  node [
    id 136
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 137
    label "sprawdza&#263;"
  ]
  node [
    id 138
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 139
    label "feel"
  ]
  node [
    id 140
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 141
    label "przedstawienie"
  ]
  node [
    id 142
    label "kosztowa&#263;"
  ]
  node [
    id 143
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 144
    label "examine"
  ]
  node [
    id 145
    label "robi&#263;"
  ]
  node [
    id 146
    label "szpiegowa&#263;"
  ]
  node [
    id 147
    label "konsumowa&#263;"
  ]
  node [
    id 148
    label "by&#263;"
  ]
  node [
    id 149
    label "savor"
  ]
  node [
    id 150
    label "cena"
  ]
  node [
    id 151
    label "doznawa&#263;"
  ]
  node [
    id 152
    label "essay"
  ]
  node [
    id 153
    label "pr&#243;bowanie"
  ]
  node [
    id 154
    label "zademonstrowanie"
  ]
  node [
    id 155
    label "report"
  ]
  node [
    id 156
    label "obgadanie"
  ]
  node [
    id 157
    label "realizacja"
  ]
  node [
    id 158
    label "scena"
  ]
  node [
    id 159
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 160
    label "narration"
  ]
  node [
    id 161
    label "cyrk"
  ]
  node [
    id 162
    label "wytw&#243;r"
  ]
  node [
    id 163
    label "theatrical_performance"
  ]
  node [
    id 164
    label "opisanie"
  ]
  node [
    id 165
    label "malarstwo"
  ]
  node [
    id 166
    label "scenografia"
  ]
  node [
    id 167
    label "teatr"
  ]
  node [
    id 168
    label "ukazanie"
  ]
  node [
    id 169
    label "zapoznanie"
  ]
  node [
    id 170
    label "pokaz"
  ]
  node [
    id 171
    label "podanie"
  ]
  node [
    id 172
    label "spos&#243;b"
  ]
  node [
    id 173
    label "ods&#322;ona"
  ]
  node [
    id 174
    label "exhibit"
  ]
  node [
    id 175
    label "pokazanie"
  ]
  node [
    id 176
    label "wyst&#261;pienie"
  ]
  node [
    id 177
    label "przedstawi&#263;"
  ]
  node [
    id 178
    label "przedstawianie"
  ]
  node [
    id 179
    label "przedstawia&#263;"
  ]
  node [
    id 180
    label "rola"
  ]
  node [
    id 181
    label "da&#263;"
  ]
  node [
    id 182
    label "udost&#281;pni&#263;"
  ]
  node [
    id 183
    label "przyzna&#263;"
  ]
  node [
    id 184
    label "picture"
  ]
  node [
    id 185
    label "give"
  ]
  node [
    id 186
    label "promocja"
  ]
  node [
    id 187
    label "odst&#261;pi&#263;"
  ]
  node [
    id 188
    label "nada&#263;"
  ]
  node [
    id 189
    label "pozwoli&#263;"
  ]
  node [
    id 190
    label "stwierdzi&#263;"
  ]
  node [
    id 191
    label "powierzy&#263;"
  ]
  node [
    id 192
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 193
    label "obieca&#263;"
  ]
  node [
    id 194
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 195
    label "przywali&#263;"
  ]
  node [
    id 196
    label "wyrzec_si&#281;"
  ]
  node [
    id 197
    label "sztachn&#261;&#263;"
  ]
  node [
    id 198
    label "rap"
  ]
  node [
    id 199
    label "feed"
  ]
  node [
    id 200
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 201
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 202
    label "testify"
  ]
  node [
    id 203
    label "przeznaczy&#263;"
  ]
  node [
    id 204
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 205
    label "zada&#263;"
  ]
  node [
    id 206
    label "dress"
  ]
  node [
    id 207
    label "dostarczy&#263;"
  ]
  node [
    id 208
    label "przekaza&#263;"
  ]
  node [
    id 209
    label "supply"
  ]
  node [
    id 210
    label "doda&#263;"
  ]
  node [
    id 211
    label "zap&#322;aci&#263;"
  ]
  node [
    id 212
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 213
    label "open"
  ]
  node [
    id 214
    label "yield"
  ]
  node [
    id 215
    label "transfer"
  ]
  node [
    id 216
    label "zrzec_si&#281;"
  ]
  node [
    id 217
    label "odsun&#261;&#263;_si&#281;"
  ]
  node [
    id 218
    label "render"
  ]
  node [
    id 219
    label "odwr&#243;t"
  ]
  node [
    id 220
    label "wycofa&#263;_si&#281;"
  ]
  node [
    id 221
    label "damka"
  ]
  node [
    id 222
    label "warcaby"
  ]
  node [
    id 223
    label "promotion"
  ]
  node [
    id 224
    label "impreza"
  ]
  node [
    id 225
    label "sprzeda&#380;"
  ]
  node [
    id 226
    label "zamiana"
  ]
  node [
    id 227
    label "brief"
  ]
  node [
    id 228
    label "decyzja"
  ]
  node [
    id 229
    label "&#347;wiadectwo"
  ]
  node [
    id 230
    label "akcja"
  ]
  node [
    id 231
    label "bran&#380;a"
  ]
  node [
    id 232
    label "commencement"
  ]
  node [
    id 233
    label "okazja"
  ]
  node [
    id 234
    label "informacja"
  ]
  node [
    id 235
    label "klasa"
  ]
  node [
    id 236
    label "promowa&#263;"
  ]
  node [
    id 237
    label "graduacja"
  ]
  node [
    id 238
    label "nominacja"
  ]
  node [
    id 239
    label "szachy"
  ]
  node [
    id 240
    label "popularyzacja"
  ]
  node [
    id 241
    label "wypromowa&#263;"
  ]
  node [
    id 242
    label "gradation"
  ]
  node [
    id 243
    label "uzyska&#263;"
  ]
  node [
    id 244
    label "&#347;rodek"
  ]
  node [
    id 245
    label "darowizna"
  ]
  node [
    id 246
    label "przedmiot"
  ]
  node [
    id 247
    label "liga"
  ]
  node [
    id 248
    label "doch&#243;d"
  ]
  node [
    id 249
    label "telefon_zaufania"
  ]
  node [
    id 250
    label "pomocnik"
  ]
  node [
    id 251
    label "zgodzi&#263;"
  ]
  node [
    id 252
    label "grupa"
  ]
  node [
    id 253
    label "property"
  ]
  node [
    id 254
    label "income"
  ]
  node [
    id 255
    label "stopa_procentowa"
  ]
  node [
    id 256
    label "krzywa_Engla"
  ]
  node [
    id 257
    label "korzy&#347;&#263;"
  ]
  node [
    id 258
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 259
    label "wp&#322;yw"
  ]
  node [
    id 260
    label "zboczenie"
  ]
  node [
    id 261
    label "om&#243;wienie"
  ]
  node [
    id 262
    label "sponiewieranie"
  ]
  node [
    id 263
    label "discipline"
  ]
  node [
    id 264
    label "rzecz"
  ]
  node [
    id 265
    label "omawia&#263;"
  ]
  node [
    id 266
    label "kr&#261;&#380;enie"
  ]
  node [
    id 267
    label "tre&#347;&#263;"
  ]
  node [
    id 268
    label "robienie"
  ]
  node [
    id 269
    label "sponiewiera&#263;"
  ]
  node [
    id 270
    label "element"
  ]
  node [
    id 271
    label "entity"
  ]
  node [
    id 272
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 273
    label "tematyka"
  ]
  node [
    id 274
    label "w&#261;tek"
  ]
  node [
    id 275
    label "charakter"
  ]
  node [
    id 276
    label "zbaczanie"
  ]
  node [
    id 277
    label "program_nauczania"
  ]
  node [
    id 278
    label "om&#243;wi&#263;"
  ]
  node [
    id 279
    label "omawianie"
  ]
  node [
    id 280
    label "thing"
  ]
  node [
    id 281
    label "kultura"
  ]
  node [
    id 282
    label "istota"
  ]
  node [
    id 283
    label "zbacza&#263;"
  ]
  node [
    id 284
    label "zboczy&#263;"
  ]
  node [
    id 285
    label "punkt"
  ]
  node [
    id 286
    label "miejsce"
  ]
  node [
    id 287
    label "abstrakcja"
  ]
  node [
    id 288
    label "czas"
  ]
  node [
    id 289
    label "chemikalia"
  ]
  node [
    id 290
    label "substancja"
  ]
  node [
    id 291
    label "kredens"
  ]
  node [
    id 292
    label "zawodnik"
  ]
  node [
    id 293
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 294
    label "bylina"
  ]
  node [
    id 295
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 296
    label "gracz"
  ]
  node [
    id 297
    label "r&#281;ka"
  ]
  node [
    id 298
    label "wrzosowate"
  ]
  node [
    id 299
    label "pomagacz"
  ]
  node [
    id 300
    label "odm&#322;adzanie"
  ]
  node [
    id 301
    label "jednostka_systematyczna"
  ]
  node [
    id 302
    label "gromada"
  ]
  node [
    id 303
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 304
    label "egzemplarz"
  ]
  node [
    id 305
    label "Entuzjastki"
  ]
  node [
    id 306
    label "zbi&#243;r"
  ]
  node [
    id 307
    label "kompozycja"
  ]
  node [
    id 308
    label "Terranie"
  ]
  node [
    id 309
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 310
    label "category"
  ]
  node [
    id 311
    label "pakiet_klimatyczny"
  ]
  node [
    id 312
    label "oddzia&#322;"
  ]
  node [
    id 313
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 314
    label "cz&#261;steczka"
  ]
  node [
    id 315
    label "stage_set"
  ]
  node [
    id 316
    label "type"
  ]
  node [
    id 317
    label "specgrupa"
  ]
  node [
    id 318
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 319
    label "&#346;wietliki"
  ]
  node [
    id 320
    label "odm&#322;odzenie"
  ]
  node [
    id 321
    label "Eurogrupa"
  ]
  node [
    id 322
    label "odm&#322;adza&#263;"
  ]
  node [
    id 323
    label "formacja_geologiczna"
  ]
  node [
    id 324
    label "harcerze_starsi"
  ]
  node [
    id 325
    label "przeniesienie_praw"
  ]
  node [
    id 326
    label "zapomoga"
  ]
  node [
    id 327
    label "transakcja"
  ]
  node [
    id 328
    label "dar"
  ]
  node [
    id 329
    label "zatrudni&#263;"
  ]
  node [
    id 330
    label "zgodzenie"
  ]
  node [
    id 331
    label "zgadza&#263;"
  ]
  node [
    id 332
    label "mecz_mistrzowski"
  ]
  node [
    id 333
    label "&#347;rodowisko"
  ]
  node [
    id 334
    label "arrangement"
  ]
  node [
    id 335
    label "obrona"
  ]
  node [
    id 336
    label "organizacja"
  ]
  node [
    id 337
    label "poziom"
  ]
  node [
    id 338
    label "rezerwa"
  ]
  node [
    id 339
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 340
    label "pr&#243;ba"
  ]
  node [
    id 341
    label "atak"
  ]
  node [
    id 342
    label "moneta"
  ]
  node [
    id 343
    label "union"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
]
