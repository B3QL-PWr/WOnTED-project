graph [
  node [
    id 0
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 1
    label "osoba"
    origin "text"
  ]
  node [
    id 2
    label "prewencyjnie"
    origin "text"
  ]
  node [
    id 3
    label "zatrzyma&#263;"
    origin "text"
  ]
  node [
    id 4
    label "pary&#380;"
    origin "text"
  ]
  node [
    id 5
    label "region"
    origin "text"
  ]
  node [
    id 6
    label "sto&#322;eczny"
    origin "text"
  ]
  node [
    id 7
    label "przed"
    origin "text"
  ]
  node [
    id 8
    label "rozpocz&#281;cie"
    origin "text"
  ]
  node [
    id 9
    label "kolejny"
    origin "text"
  ]
  node [
    id 10
    label "protest"
    origin "text"
  ]
  node [
    id 11
    label "ruch"
    origin "text"
  ]
  node [
    id 12
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 13
    label "kamizelka"
    origin "text"
  ]
  node [
    id 14
    label "nieznaczny"
  ]
  node [
    id 15
    label "pomiernie"
  ]
  node [
    id 16
    label "kr&#243;tko"
  ]
  node [
    id 17
    label "mikroskopijnie"
  ]
  node [
    id 18
    label "nieliczny"
  ]
  node [
    id 19
    label "mo&#380;liwie"
  ]
  node [
    id 20
    label "nieistotnie"
  ]
  node [
    id 21
    label "ma&#322;y"
  ]
  node [
    id 22
    label "niepowa&#380;nie"
  ]
  node [
    id 23
    label "niewa&#380;ny"
  ]
  node [
    id 24
    label "mo&#380;liwy"
  ]
  node [
    id 25
    label "zno&#347;nie"
  ]
  node [
    id 26
    label "kr&#243;tki"
  ]
  node [
    id 27
    label "nieznacznie"
  ]
  node [
    id 28
    label "drobnostkowy"
  ]
  node [
    id 29
    label "malusie&#324;ko"
  ]
  node [
    id 30
    label "mikroskopijny"
  ]
  node [
    id 31
    label "bardzo"
  ]
  node [
    id 32
    label "szybki"
  ]
  node [
    id 33
    label "przeci&#281;tny"
  ]
  node [
    id 34
    label "wstydliwy"
  ]
  node [
    id 35
    label "s&#322;aby"
  ]
  node [
    id 36
    label "ch&#322;opiec"
  ]
  node [
    id 37
    label "m&#322;ody"
  ]
  node [
    id 38
    label "marny"
  ]
  node [
    id 39
    label "n&#281;dznie"
  ]
  node [
    id 40
    label "nielicznie"
  ]
  node [
    id 41
    label "licho"
  ]
  node [
    id 42
    label "proporcjonalnie"
  ]
  node [
    id 43
    label "pomierny"
  ]
  node [
    id 44
    label "miernie"
  ]
  node [
    id 45
    label "Chocho&#322;"
  ]
  node [
    id 46
    label "Herkules_Poirot"
  ]
  node [
    id 47
    label "Edyp"
  ]
  node [
    id 48
    label "ludzko&#347;&#263;"
  ]
  node [
    id 49
    label "parali&#380;owa&#263;"
  ]
  node [
    id 50
    label "Harry_Potter"
  ]
  node [
    id 51
    label "Casanova"
  ]
  node [
    id 52
    label "Gargantua"
  ]
  node [
    id 53
    label "Zgredek"
  ]
  node [
    id 54
    label "Winnetou"
  ]
  node [
    id 55
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 56
    label "posta&#263;"
  ]
  node [
    id 57
    label "Dulcynea"
  ]
  node [
    id 58
    label "kategoria_gramatyczna"
  ]
  node [
    id 59
    label "g&#322;owa"
  ]
  node [
    id 60
    label "figura"
  ]
  node [
    id 61
    label "portrecista"
  ]
  node [
    id 62
    label "person"
  ]
  node [
    id 63
    label "Sherlock_Holmes"
  ]
  node [
    id 64
    label "Quasimodo"
  ]
  node [
    id 65
    label "Plastu&#347;"
  ]
  node [
    id 66
    label "Faust"
  ]
  node [
    id 67
    label "Wallenrod"
  ]
  node [
    id 68
    label "Dwukwiat"
  ]
  node [
    id 69
    label "koniugacja"
  ]
  node [
    id 70
    label "profanum"
  ]
  node [
    id 71
    label "Don_Juan"
  ]
  node [
    id 72
    label "Don_Kiszot"
  ]
  node [
    id 73
    label "mikrokosmos"
  ]
  node [
    id 74
    label "duch"
  ]
  node [
    id 75
    label "antropochoria"
  ]
  node [
    id 76
    label "oddzia&#322;ywanie"
  ]
  node [
    id 77
    label "Hamlet"
  ]
  node [
    id 78
    label "Werter"
  ]
  node [
    id 79
    label "istota"
  ]
  node [
    id 80
    label "Szwejk"
  ]
  node [
    id 81
    label "homo_sapiens"
  ]
  node [
    id 82
    label "mentalno&#347;&#263;"
  ]
  node [
    id 83
    label "superego"
  ]
  node [
    id 84
    label "psychika"
  ]
  node [
    id 85
    label "znaczenie"
  ]
  node [
    id 86
    label "wn&#281;trze"
  ]
  node [
    id 87
    label "charakter"
  ]
  node [
    id 88
    label "cecha"
  ]
  node [
    id 89
    label "charakterystyka"
  ]
  node [
    id 90
    label "cz&#322;owiek"
  ]
  node [
    id 91
    label "zaistnie&#263;"
  ]
  node [
    id 92
    label "Osjan"
  ]
  node [
    id 93
    label "kto&#347;"
  ]
  node [
    id 94
    label "wygl&#261;d"
  ]
  node [
    id 95
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 96
    label "osobowo&#347;&#263;"
  ]
  node [
    id 97
    label "wytw&#243;r"
  ]
  node [
    id 98
    label "trim"
  ]
  node [
    id 99
    label "poby&#263;"
  ]
  node [
    id 100
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 101
    label "Aspazja"
  ]
  node [
    id 102
    label "punkt_widzenia"
  ]
  node [
    id 103
    label "kompleksja"
  ]
  node [
    id 104
    label "wytrzyma&#263;"
  ]
  node [
    id 105
    label "budowa"
  ]
  node [
    id 106
    label "formacja"
  ]
  node [
    id 107
    label "pozosta&#263;"
  ]
  node [
    id 108
    label "point"
  ]
  node [
    id 109
    label "przedstawienie"
  ]
  node [
    id 110
    label "go&#347;&#263;"
  ]
  node [
    id 111
    label "hamper"
  ]
  node [
    id 112
    label "spasm"
  ]
  node [
    id 113
    label "mrozi&#263;"
  ]
  node [
    id 114
    label "pora&#380;a&#263;"
  ]
  node [
    id 115
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 116
    label "fleksja"
  ]
  node [
    id 117
    label "liczba"
  ]
  node [
    id 118
    label "coupling"
  ]
  node [
    id 119
    label "tryb"
  ]
  node [
    id 120
    label "czas"
  ]
  node [
    id 121
    label "czasownik"
  ]
  node [
    id 122
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 123
    label "orz&#281;sek"
  ]
  node [
    id 124
    label "fotograf"
  ]
  node [
    id 125
    label "malarz"
  ]
  node [
    id 126
    label "artysta"
  ]
  node [
    id 127
    label "powodowanie"
  ]
  node [
    id 128
    label "hipnotyzowanie"
  ]
  node [
    id 129
    label "&#347;lad"
  ]
  node [
    id 130
    label "docieranie"
  ]
  node [
    id 131
    label "natural_process"
  ]
  node [
    id 132
    label "reakcja_chemiczna"
  ]
  node [
    id 133
    label "wdzieranie_si&#281;"
  ]
  node [
    id 134
    label "zjawisko"
  ]
  node [
    id 135
    label "act"
  ]
  node [
    id 136
    label "rezultat"
  ]
  node [
    id 137
    label "lobbysta"
  ]
  node [
    id 138
    label "pryncypa&#322;"
  ]
  node [
    id 139
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 140
    label "kszta&#322;t"
  ]
  node [
    id 141
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 142
    label "wiedza"
  ]
  node [
    id 143
    label "kierowa&#263;"
  ]
  node [
    id 144
    label "alkohol"
  ]
  node [
    id 145
    label "zdolno&#347;&#263;"
  ]
  node [
    id 146
    label "&#380;ycie"
  ]
  node [
    id 147
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 148
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 149
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 150
    label "sztuka"
  ]
  node [
    id 151
    label "dekiel"
  ]
  node [
    id 152
    label "ro&#347;lina"
  ]
  node [
    id 153
    label "&#347;ci&#281;cie"
  ]
  node [
    id 154
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 155
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 156
    label "&#347;ci&#281;gno"
  ]
  node [
    id 157
    label "noosfera"
  ]
  node [
    id 158
    label "byd&#322;o"
  ]
  node [
    id 159
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 160
    label "makrocefalia"
  ]
  node [
    id 161
    label "obiekt"
  ]
  node [
    id 162
    label "ucho"
  ]
  node [
    id 163
    label "g&#243;ra"
  ]
  node [
    id 164
    label "m&#243;zg"
  ]
  node [
    id 165
    label "kierownictwo"
  ]
  node [
    id 166
    label "fryzura"
  ]
  node [
    id 167
    label "umys&#322;"
  ]
  node [
    id 168
    label "cia&#322;o"
  ]
  node [
    id 169
    label "cz&#322;onek"
  ]
  node [
    id 170
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 171
    label "czaszka"
  ]
  node [
    id 172
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 173
    label "allochoria"
  ]
  node [
    id 174
    label "p&#322;aszczyzna"
  ]
  node [
    id 175
    label "przedmiot"
  ]
  node [
    id 176
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 177
    label "bierka_szachowa"
  ]
  node [
    id 178
    label "obiekt_matematyczny"
  ]
  node [
    id 179
    label "gestaltyzm"
  ]
  node [
    id 180
    label "styl"
  ]
  node [
    id 181
    label "obraz"
  ]
  node [
    id 182
    label "rzecz"
  ]
  node [
    id 183
    label "d&#378;wi&#281;k"
  ]
  node [
    id 184
    label "character"
  ]
  node [
    id 185
    label "rze&#378;ba"
  ]
  node [
    id 186
    label "stylistyka"
  ]
  node [
    id 187
    label "figure"
  ]
  node [
    id 188
    label "miejsce"
  ]
  node [
    id 189
    label "antycypacja"
  ]
  node [
    id 190
    label "ornamentyka"
  ]
  node [
    id 191
    label "informacja"
  ]
  node [
    id 192
    label "facet"
  ]
  node [
    id 193
    label "popis"
  ]
  node [
    id 194
    label "wiersz"
  ]
  node [
    id 195
    label "symetria"
  ]
  node [
    id 196
    label "lingwistyka_kognitywna"
  ]
  node [
    id 197
    label "karta"
  ]
  node [
    id 198
    label "shape"
  ]
  node [
    id 199
    label "podzbi&#243;r"
  ]
  node [
    id 200
    label "perspektywa"
  ]
  node [
    id 201
    label "dziedzina"
  ]
  node [
    id 202
    label "Szekspir"
  ]
  node [
    id 203
    label "Mickiewicz"
  ]
  node [
    id 204
    label "cierpienie"
  ]
  node [
    id 205
    label "piek&#322;o"
  ]
  node [
    id 206
    label "human_body"
  ]
  node [
    id 207
    label "ofiarowywanie"
  ]
  node [
    id 208
    label "sfera_afektywna"
  ]
  node [
    id 209
    label "nekromancja"
  ]
  node [
    id 210
    label "Po&#347;wist"
  ]
  node [
    id 211
    label "podekscytowanie"
  ]
  node [
    id 212
    label "deformowanie"
  ]
  node [
    id 213
    label "sumienie"
  ]
  node [
    id 214
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 215
    label "deformowa&#263;"
  ]
  node [
    id 216
    label "zjawa"
  ]
  node [
    id 217
    label "zmar&#322;y"
  ]
  node [
    id 218
    label "istota_nadprzyrodzona"
  ]
  node [
    id 219
    label "power"
  ]
  node [
    id 220
    label "entity"
  ]
  node [
    id 221
    label "ofiarowywa&#263;"
  ]
  node [
    id 222
    label "oddech"
  ]
  node [
    id 223
    label "seksualno&#347;&#263;"
  ]
  node [
    id 224
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 225
    label "byt"
  ]
  node [
    id 226
    label "si&#322;a"
  ]
  node [
    id 227
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 228
    label "ego"
  ]
  node [
    id 229
    label "ofiarowanie"
  ]
  node [
    id 230
    label "fizjonomia"
  ]
  node [
    id 231
    label "kompleks"
  ]
  node [
    id 232
    label "zapalno&#347;&#263;"
  ]
  node [
    id 233
    label "T&#281;sknica"
  ]
  node [
    id 234
    label "ofiarowa&#263;"
  ]
  node [
    id 235
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 236
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 237
    label "passion"
  ]
  node [
    id 238
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 239
    label "odbicie"
  ]
  node [
    id 240
    label "atom"
  ]
  node [
    id 241
    label "przyroda"
  ]
  node [
    id 242
    label "Ziemia"
  ]
  node [
    id 243
    label "kosmos"
  ]
  node [
    id 244
    label "miniatura"
  ]
  node [
    id 245
    label "prewencyjny"
  ]
  node [
    id 246
    label "ochronnie"
  ]
  node [
    id 247
    label "ochronny"
  ]
  node [
    id 248
    label "ulgowo"
  ]
  node [
    id 249
    label "komornik"
  ]
  node [
    id 250
    label "suspend"
  ]
  node [
    id 251
    label "zaczepi&#263;"
  ]
  node [
    id 252
    label "bury"
  ]
  node [
    id 253
    label "bankrupt"
  ]
  node [
    id 254
    label "zabra&#263;"
  ]
  node [
    id 255
    label "continue"
  ]
  node [
    id 256
    label "give"
  ]
  node [
    id 257
    label "spowodowa&#263;"
  ]
  node [
    id 258
    label "zamkn&#261;&#263;"
  ]
  node [
    id 259
    label "przechowa&#263;"
  ]
  node [
    id 260
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 261
    label "zaaresztowa&#263;"
  ]
  node [
    id 262
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 263
    label "przerwa&#263;"
  ]
  node [
    id 264
    label "unieruchomi&#263;"
  ]
  node [
    id 265
    label "anticipate"
  ]
  node [
    id 266
    label "work"
  ]
  node [
    id 267
    label "chemia"
  ]
  node [
    id 268
    label "withdraw"
  ]
  node [
    id 269
    label "doprowadzi&#263;"
  ]
  node [
    id 270
    label "z&#322;apa&#263;"
  ]
  node [
    id 271
    label "wzi&#261;&#263;"
  ]
  node [
    id 272
    label "zaj&#261;&#263;"
  ]
  node [
    id 273
    label "consume"
  ]
  node [
    id 274
    label "deprive"
  ]
  node [
    id 275
    label "przenie&#347;&#263;"
  ]
  node [
    id 276
    label "abstract"
  ]
  node [
    id 277
    label "uda&#263;_si&#281;"
  ]
  node [
    id 278
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 279
    label "przesun&#261;&#263;"
  ]
  node [
    id 280
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 281
    label "zgarn&#261;&#263;"
  ]
  node [
    id 282
    label "close"
  ]
  node [
    id 283
    label "przeszkodzi&#263;"
  ]
  node [
    id 284
    label "calve"
  ]
  node [
    id 285
    label "wstrzyma&#263;"
  ]
  node [
    id 286
    label "rozerwa&#263;"
  ]
  node [
    id 287
    label "przedziurawi&#263;"
  ]
  node [
    id 288
    label "urwa&#263;"
  ]
  node [
    id 289
    label "przerzedzi&#263;"
  ]
  node [
    id 290
    label "kultywar"
  ]
  node [
    id 291
    label "przerywa&#263;"
  ]
  node [
    id 292
    label "przerwanie"
  ]
  node [
    id 293
    label "break"
  ]
  node [
    id 294
    label "przerywanie"
  ]
  node [
    id 295
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 296
    label "forbid"
  ]
  node [
    id 297
    label "przesta&#263;"
  ]
  node [
    id 298
    label "throng"
  ]
  node [
    id 299
    label "zrobi&#263;"
  ]
  node [
    id 300
    label "lock"
  ]
  node [
    id 301
    label "pami&#281;&#263;"
  ]
  node [
    id 302
    label "uchroni&#263;"
  ]
  node [
    id 303
    label "ukry&#263;"
  ]
  node [
    id 304
    label "preserve"
  ]
  node [
    id 305
    label "zachowa&#263;"
  ]
  node [
    id 306
    label "podtrzyma&#263;"
  ]
  node [
    id 307
    label "cover"
  ]
  node [
    id 308
    label "brunatny"
  ]
  node [
    id 309
    label "przygn&#281;biaj&#261;cy"
  ]
  node [
    id 310
    label "ciemnoszary"
  ]
  node [
    id 311
    label "brudnoszary"
  ]
  node [
    id 312
    label "buro"
  ]
  node [
    id 313
    label "urz&#281;dnik"
  ]
  node [
    id 314
    label "podkomorzy"
  ]
  node [
    id 315
    label "ch&#322;op"
  ]
  node [
    id 316
    label "urz&#281;dnik_s&#261;dowy"
  ]
  node [
    id 317
    label "zajmowa&#263;"
  ]
  node [
    id 318
    label "bezrolny"
  ]
  node [
    id 319
    label "lokator"
  ]
  node [
    id 320
    label "sekutnik"
  ]
  node [
    id 321
    label "zagadn&#261;&#263;"
  ]
  node [
    id 322
    label "odwiedzi&#263;"
  ]
  node [
    id 323
    label "napa&#347;&#263;"
  ]
  node [
    id 324
    label "absorb"
  ]
  node [
    id 325
    label "limp"
  ]
  node [
    id 326
    label "zaatakowa&#263;"
  ]
  node [
    id 327
    label "wpa&#347;&#263;"
  ]
  node [
    id 328
    label "prosecute"
  ]
  node [
    id 329
    label "przymocowa&#263;"
  ]
  node [
    id 330
    label "engage"
  ]
  node [
    id 331
    label "zako&#324;czy&#263;"
  ]
  node [
    id 332
    label "put"
  ]
  node [
    id 333
    label "insert"
  ]
  node [
    id 334
    label "zawrze&#263;"
  ]
  node [
    id 335
    label "zablokowa&#263;"
  ]
  node [
    id 336
    label "sko&#324;czy&#263;"
  ]
  node [
    id 337
    label "uj&#261;&#263;"
  ]
  node [
    id 338
    label "rozwi&#261;za&#263;"
  ]
  node [
    id 339
    label "kill"
  ]
  node [
    id 340
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 341
    label "umie&#347;ci&#263;"
  ]
  node [
    id 342
    label "Mazowsze"
  ]
  node [
    id 343
    label "Anglia"
  ]
  node [
    id 344
    label "Amazonia"
  ]
  node [
    id 345
    label "Bordeaux"
  ]
  node [
    id 346
    label "Naddniestrze"
  ]
  node [
    id 347
    label "Europa_Zachodnia"
  ]
  node [
    id 348
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 349
    label "Armagnac"
  ]
  node [
    id 350
    label "Zamojszczyzna"
  ]
  node [
    id 351
    label "Amhara"
  ]
  node [
    id 352
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 353
    label "okr&#281;g"
  ]
  node [
    id 354
    label "Ma&#322;opolska"
  ]
  node [
    id 355
    label "Turkiestan"
  ]
  node [
    id 356
    label "Burgundia"
  ]
  node [
    id 357
    label "Noworosja"
  ]
  node [
    id 358
    label "Mezoameryka"
  ]
  node [
    id 359
    label "Lubelszczyzna"
  ]
  node [
    id 360
    label "Krajina"
  ]
  node [
    id 361
    label "Ba&#322;kany"
  ]
  node [
    id 362
    label "Kurdystan"
  ]
  node [
    id 363
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 364
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 365
    label "Baszkiria"
  ]
  node [
    id 366
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 367
    label "Szkocja"
  ]
  node [
    id 368
    label "Tonkin"
  ]
  node [
    id 369
    label "Maghreb"
  ]
  node [
    id 370
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 371
    label "Nadrenia"
  ]
  node [
    id 372
    label "Wielkopolska"
  ]
  node [
    id 373
    label "Zabajkale"
  ]
  node [
    id 374
    label "Apulia"
  ]
  node [
    id 375
    label "Bojkowszczyzna"
  ]
  node [
    id 376
    label "podregion"
  ]
  node [
    id 377
    label "Liguria"
  ]
  node [
    id 378
    label "Pamir"
  ]
  node [
    id 379
    label "Indochiny"
  ]
  node [
    id 380
    label "Podlasie"
  ]
  node [
    id 381
    label "Polinezja"
  ]
  node [
    id 382
    label "Kurpie"
  ]
  node [
    id 383
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 384
    label "S&#261;decczyzna"
  ]
  node [
    id 385
    label "Umbria"
  ]
  node [
    id 386
    label "Flandria"
  ]
  node [
    id 387
    label "Karaiby"
  ]
  node [
    id 388
    label "Ukraina_Zachodnia"
  ]
  node [
    id 389
    label "Kielecczyzna"
  ]
  node [
    id 390
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 391
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 392
    label "Skandynawia"
  ]
  node [
    id 393
    label "Kujawy"
  ]
  node [
    id 394
    label "Tyrol"
  ]
  node [
    id 395
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 396
    label "Huculszczyzna"
  ]
  node [
    id 397
    label "Turyngia"
  ]
  node [
    id 398
    label "jednostka_administracyjna"
  ]
  node [
    id 399
    label "Toskania"
  ]
  node [
    id 400
    label "Podhale"
  ]
  node [
    id 401
    label "Bory_Tucholskie"
  ]
  node [
    id 402
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 403
    label "country"
  ]
  node [
    id 404
    label "Kalabria"
  ]
  node [
    id 405
    label "Hercegowina"
  ]
  node [
    id 406
    label "Lotaryngia"
  ]
  node [
    id 407
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 408
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 409
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 410
    label "Walia"
  ]
  node [
    id 411
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 412
    label "Opolskie"
  ]
  node [
    id 413
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 414
    label "Kampania"
  ]
  node [
    id 415
    label "Chiny_Zachodnie"
  ]
  node [
    id 416
    label "Sand&#380;ak"
  ]
  node [
    id 417
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 418
    label "Syjon"
  ]
  node [
    id 419
    label "Kabylia"
  ]
  node [
    id 420
    label "Lombardia"
  ]
  node [
    id 421
    label "Warmia"
  ]
  node [
    id 422
    label "Kaszmir"
  ]
  node [
    id 423
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 424
    label "&#321;&#243;dzkie"
  ]
  node [
    id 425
    label "Kaukaz"
  ]
  node [
    id 426
    label "subregion"
  ]
  node [
    id 427
    label "Europa_Wschodnia"
  ]
  node [
    id 428
    label "Biskupizna"
  ]
  node [
    id 429
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 430
    label "Afryka_Wschodnia"
  ]
  node [
    id 431
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 432
    label "Podkarpacie"
  ]
  node [
    id 433
    label "Chiny_Wschodnie"
  ]
  node [
    id 434
    label "obszar"
  ]
  node [
    id 435
    label "Afryka_Zachodnia"
  ]
  node [
    id 436
    label "&#379;mud&#378;"
  ]
  node [
    id 437
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 438
    label "Bo&#347;nia"
  ]
  node [
    id 439
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 440
    label "Oceania"
  ]
  node [
    id 441
    label "Pomorze_Zachodnie"
  ]
  node [
    id 442
    label "Powi&#347;le"
  ]
  node [
    id 443
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 444
    label "Podbeskidzie"
  ]
  node [
    id 445
    label "&#321;emkowszczyzna"
  ]
  node [
    id 446
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 447
    label "Opolszczyzna"
  ]
  node [
    id 448
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 449
    label "Kaszuby"
  ]
  node [
    id 450
    label "Ko&#322;yma"
  ]
  node [
    id 451
    label "Szlezwik"
  ]
  node [
    id 452
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 453
    label "Mikronezja"
  ]
  node [
    id 454
    label "Polesie"
  ]
  node [
    id 455
    label "Kerala"
  ]
  node [
    id 456
    label "Mazury"
  ]
  node [
    id 457
    label "Palestyna"
  ]
  node [
    id 458
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 459
    label "Lauda"
  ]
  node [
    id 460
    label "Azja_Wschodnia"
  ]
  node [
    id 461
    label "Galicja"
  ]
  node [
    id 462
    label "Zakarpacie"
  ]
  node [
    id 463
    label "Lubuskie"
  ]
  node [
    id 464
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 465
    label "Laponia"
  ]
  node [
    id 466
    label "Yorkshire"
  ]
  node [
    id 467
    label "Bawaria"
  ]
  node [
    id 468
    label "Zag&#243;rze"
  ]
  node [
    id 469
    label "Andaluzja"
  ]
  node [
    id 470
    label "Kraina"
  ]
  node [
    id 471
    label "&#379;ywiecczyzna"
  ]
  node [
    id 472
    label "Oksytania"
  ]
  node [
    id 473
    label "Kociewie"
  ]
  node [
    id 474
    label "Lasko"
  ]
  node [
    id 475
    label "p&#243;&#322;noc"
  ]
  node [
    id 476
    label "Kosowo"
  ]
  node [
    id 477
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 478
    label "Zab&#322;ocie"
  ]
  node [
    id 479
    label "zach&#243;d"
  ]
  node [
    id 480
    label "po&#322;udnie"
  ]
  node [
    id 481
    label "Pow&#261;zki"
  ]
  node [
    id 482
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 483
    label "Piotrowo"
  ]
  node [
    id 484
    label "Olszanica"
  ]
  node [
    id 485
    label "zbi&#243;r"
  ]
  node [
    id 486
    label "Ruda_Pabianicka"
  ]
  node [
    id 487
    label "holarktyka"
  ]
  node [
    id 488
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 489
    label "Ludwin&#243;w"
  ]
  node [
    id 490
    label "Arktyka"
  ]
  node [
    id 491
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 492
    label "Zabu&#380;e"
  ]
  node [
    id 493
    label "antroposfera"
  ]
  node [
    id 494
    label "Neogea"
  ]
  node [
    id 495
    label "terytorium"
  ]
  node [
    id 496
    label "Syberia_Zachodnia"
  ]
  node [
    id 497
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 498
    label "zakres"
  ]
  node [
    id 499
    label "pas_planetoid"
  ]
  node [
    id 500
    label "Syberia_Wschodnia"
  ]
  node [
    id 501
    label "Antarktyka"
  ]
  node [
    id 502
    label "Rakowice"
  ]
  node [
    id 503
    label "akrecja"
  ]
  node [
    id 504
    label "wymiar"
  ]
  node [
    id 505
    label "&#321;&#281;g"
  ]
  node [
    id 506
    label "Kresy_Zachodnie"
  ]
  node [
    id 507
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 508
    label "przestrze&#324;"
  ]
  node [
    id 509
    label "wsch&#243;d"
  ]
  node [
    id 510
    label "Notogea"
  ]
  node [
    id 511
    label "Judea"
  ]
  node [
    id 512
    label "moszaw"
  ]
  node [
    id 513
    label "Kanaan"
  ]
  node [
    id 514
    label "Algieria"
  ]
  node [
    id 515
    label "Antigua_i_Barbuda"
  ]
  node [
    id 516
    label "Aruba"
  ]
  node [
    id 517
    label "Jamajka"
  ]
  node [
    id 518
    label "Kuba"
  ]
  node [
    id 519
    label "Haiti"
  ]
  node [
    id 520
    label "Kajmany"
  ]
  node [
    id 521
    label "Portoryko"
  ]
  node [
    id 522
    label "Anguilla"
  ]
  node [
    id 523
    label "Bahamy"
  ]
  node [
    id 524
    label "Antyle"
  ]
  node [
    id 525
    label "Polska"
  ]
  node [
    id 526
    label "Mogielnica"
  ]
  node [
    id 527
    label "Indie"
  ]
  node [
    id 528
    label "jezioro"
  ]
  node [
    id 529
    label "Niemcy"
  ]
  node [
    id 530
    label "Rumelia"
  ]
  node [
    id 531
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 532
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 533
    label "Poprad"
  ]
  node [
    id 534
    label "Tatry"
  ]
  node [
    id 535
    label "Podtatrze"
  ]
  node [
    id 536
    label "Podole"
  ]
  node [
    id 537
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 538
    label "Hiszpania"
  ]
  node [
    id 539
    label "Austro-W&#281;gry"
  ]
  node [
    id 540
    label "W&#322;ochy"
  ]
  node [
    id 541
    label "Biskupice"
  ]
  node [
    id 542
    label "Iwanowice"
  ]
  node [
    id 543
    label "Ziemia_Sandomierska"
  ]
  node [
    id 544
    label "Rogo&#378;nik"
  ]
  node [
    id 545
    label "Ropa"
  ]
  node [
    id 546
    label "Wietnam"
  ]
  node [
    id 547
    label "Etiopia"
  ]
  node [
    id 548
    label "Austria"
  ]
  node [
    id 549
    label "Alpy"
  ]
  node [
    id 550
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 551
    label "Francja"
  ]
  node [
    id 552
    label "Wyspy_Marshalla"
  ]
  node [
    id 553
    label "Nauru"
  ]
  node [
    id 554
    label "Mariany"
  ]
  node [
    id 555
    label "dolar"
  ]
  node [
    id 556
    label "Karpaty"
  ]
  node [
    id 557
    label "Samoa"
  ]
  node [
    id 558
    label "Tonga"
  ]
  node [
    id 559
    label "Tuwalu"
  ]
  node [
    id 560
    label "Hawaje"
  ]
  node [
    id 561
    label "Beskidy_Zachodnie"
  ]
  node [
    id 562
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 563
    label "Rosja"
  ]
  node [
    id 564
    label "Beskid_Niski"
  ]
  node [
    id 565
    label "Etruria"
  ]
  node [
    id 566
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 567
    label "Bojanowo"
  ]
  node [
    id 568
    label "Obra"
  ]
  node [
    id 569
    label "Wilkowo_Polskie"
  ]
  node [
    id 570
    label "Dobra"
  ]
  node [
    id 571
    label "Buriacja"
  ]
  node [
    id 572
    label "Rozewie"
  ]
  node [
    id 573
    label "&#346;l&#261;sk"
  ]
  node [
    id 574
    label "Czechy"
  ]
  node [
    id 575
    label "Ukraina"
  ]
  node [
    id 576
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 577
    label "Mo&#322;dawia"
  ]
  node [
    id 578
    label "Norwegia"
  ]
  node [
    id 579
    label "Szwecja"
  ]
  node [
    id 580
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 581
    label "Finlandia"
  ]
  node [
    id 582
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 583
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 584
    label "Wiktoria"
  ]
  node [
    id 585
    label "Wielka_Brytania"
  ]
  node [
    id 586
    label "Guernsey"
  ]
  node [
    id 587
    label "Conrad"
  ]
  node [
    id 588
    label "funt_szterling"
  ]
  node [
    id 589
    label "Unia_Europejska"
  ]
  node [
    id 590
    label "Portland"
  ]
  node [
    id 591
    label "NATO"
  ]
  node [
    id 592
    label "El&#380;bieta_I"
  ]
  node [
    id 593
    label "Kornwalia"
  ]
  node [
    id 594
    label "Amazonka"
  ]
  node [
    id 595
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 596
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 597
    label "Libia"
  ]
  node [
    id 598
    label "Maroko"
  ]
  node [
    id 599
    label "Tunezja"
  ]
  node [
    id 600
    label "Mauretania"
  ]
  node [
    id 601
    label "Sahara_Zachodnia"
  ]
  node [
    id 602
    label "Imperium_Rosyjskie"
  ]
  node [
    id 603
    label "Anglosas"
  ]
  node [
    id 604
    label "Moza"
  ]
  node [
    id 605
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 606
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 607
    label "Paj&#281;czno"
  ]
  node [
    id 608
    label "Nowa_Zelandia"
  ]
  node [
    id 609
    label "Ocean_Spokojny"
  ]
  node [
    id 610
    label "Palau"
  ]
  node [
    id 611
    label "Melanezja"
  ]
  node [
    id 612
    label "Nowy_&#346;wiat"
  ]
  node [
    id 613
    label "Czarnog&#243;ra"
  ]
  node [
    id 614
    label "Serbia"
  ]
  node [
    id 615
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 616
    label "Tar&#322;&#243;w"
  ]
  node [
    id 617
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 618
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 619
    label "Gop&#322;o"
  ]
  node [
    id 620
    label "Jerozolima"
  ]
  node [
    id 621
    label "Dolna_Frankonia"
  ]
  node [
    id 622
    label "funt_szkocki"
  ]
  node [
    id 623
    label "Kaledonia"
  ]
  node [
    id 624
    label "Czeczenia"
  ]
  node [
    id 625
    label "Inguszetia"
  ]
  node [
    id 626
    label "Abchazja"
  ]
  node [
    id 627
    label "Sarmata"
  ]
  node [
    id 628
    label "Dagestan"
  ]
  node [
    id 629
    label "Eurazja"
  ]
  node [
    id 630
    label "Warszawa"
  ]
  node [
    id 631
    label "Mariensztat"
  ]
  node [
    id 632
    label "Pakistan"
  ]
  node [
    id 633
    label "j&#281;zyk_flamandzki"
  ]
  node [
    id 634
    label "Belgia"
  ]
  node [
    id 635
    label "&#379;mujd&#378;"
  ]
  node [
    id 636
    label "Litwa"
  ]
  node [
    id 637
    label "okr&#281;g_przemys&#322;owy"
  ]
  node [
    id 638
    label "pa&#324;stwo"
  ]
  node [
    id 639
    label "muzyka_rozrywkowa"
  ]
  node [
    id 640
    label "bluegrass"
  ]
  node [
    id 641
    label "miejski"
  ]
  node [
    id 642
    label "sto&#322;ecznie"
  ]
  node [
    id 643
    label "publiczny"
  ]
  node [
    id 644
    label "typowy"
  ]
  node [
    id 645
    label "miastowy"
  ]
  node [
    id 646
    label "miejsko"
  ]
  node [
    id 647
    label "dzia&#322;anie"
  ]
  node [
    id 648
    label "wydarzenie"
  ]
  node [
    id 649
    label "opening"
  ]
  node [
    id 650
    label "start"
  ]
  node [
    id 651
    label "znalezienie_si&#281;"
  ]
  node [
    id 652
    label "pocz&#261;tek"
  ]
  node [
    id 653
    label "zacz&#281;cie"
  ]
  node [
    id 654
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 655
    label "zrobienie"
  ]
  node [
    id 656
    label "infimum"
  ]
  node [
    id 657
    label "liczenie"
  ]
  node [
    id 658
    label "skutek"
  ]
  node [
    id 659
    label "podzia&#322;anie"
  ]
  node [
    id 660
    label "supremum"
  ]
  node [
    id 661
    label "kampania"
  ]
  node [
    id 662
    label "uruchamianie"
  ]
  node [
    id 663
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 664
    label "operacja"
  ]
  node [
    id 665
    label "jednostka"
  ]
  node [
    id 666
    label "robienie"
  ]
  node [
    id 667
    label "uruchomienie"
  ]
  node [
    id 668
    label "nakr&#281;canie"
  ]
  node [
    id 669
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 670
    label "matematyka"
  ]
  node [
    id 671
    label "tr&#243;jstronny"
  ]
  node [
    id 672
    label "nakr&#281;cenie"
  ]
  node [
    id 673
    label "zatrzymanie"
  ]
  node [
    id 674
    label "wp&#322;yw"
  ]
  node [
    id 675
    label "rzut"
  ]
  node [
    id 676
    label "podtrzymywanie"
  ]
  node [
    id 677
    label "w&#322;&#261;czanie"
  ]
  node [
    id 678
    label "liczy&#263;"
  ]
  node [
    id 679
    label "operation"
  ]
  node [
    id 680
    label "czynno&#347;&#263;"
  ]
  node [
    id 681
    label "dzianie_si&#281;"
  ]
  node [
    id 682
    label "zadzia&#322;anie"
  ]
  node [
    id 683
    label "priorytet"
  ]
  node [
    id 684
    label "bycie"
  ]
  node [
    id 685
    label "kres"
  ]
  node [
    id 686
    label "funkcja"
  ]
  node [
    id 687
    label "czynny"
  ]
  node [
    id 688
    label "impact"
  ]
  node [
    id 689
    label "oferta"
  ]
  node [
    id 690
    label "zako&#324;czenie"
  ]
  node [
    id 691
    label "w&#322;&#261;czenie"
  ]
  node [
    id 692
    label "discourtesy"
  ]
  node [
    id 693
    label "odj&#281;cie"
  ]
  node [
    id 694
    label "post&#261;pienie"
  ]
  node [
    id 695
    label "wyra&#380;enie"
  ]
  node [
    id 696
    label "narobienie"
  ]
  node [
    id 697
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 698
    label "creation"
  ]
  node [
    id 699
    label "porobienie"
  ]
  node [
    id 700
    label "przebiec"
  ]
  node [
    id 701
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 702
    label "motyw"
  ]
  node [
    id 703
    label "przebiegni&#281;cie"
  ]
  node [
    id 704
    label "fabu&#322;a"
  ]
  node [
    id 705
    label "pierworodztwo"
  ]
  node [
    id 706
    label "faza"
  ]
  node [
    id 707
    label "upgrade"
  ]
  node [
    id 708
    label "nast&#281;pstwo"
  ]
  node [
    id 709
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 710
    label "lot"
  ]
  node [
    id 711
    label "uczestnictwo"
  ]
  node [
    id 712
    label "okno_startowe"
  ]
  node [
    id 713
    label "blok_startowy"
  ]
  node [
    id 714
    label "wy&#347;cig"
  ]
  node [
    id 715
    label "nast&#281;pnie"
  ]
  node [
    id 716
    label "inny"
  ]
  node [
    id 717
    label "nastopny"
  ]
  node [
    id 718
    label "kolejno"
  ]
  node [
    id 719
    label "kt&#243;ry&#347;"
  ]
  node [
    id 720
    label "osobno"
  ]
  node [
    id 721
    label "r&#243;&#380;ny"
  ]
  node [
    id 722
    label "inszy"
  ]
  node [
    id 723
    label "inaczej"
  ]
  node [
    id 724
    label "czerwona_kartka"
  ]
  node [
    id 725
    label "protestacja"
  ]
  node [
    id 726
    label "reakcja"
  ]
  node [
    id 727
    label "react"
  ]
  node [
    id 728
    label "zachowanie"
  ]
  node [
    id 729
    label "reaction"
  ]
  node [
    id 730
    label "organizm"
  ]
  node [
    id 731
    label "rozmowa"
  ]
  node [
    id 732
    label "response"
  ]
  node [
    id 733
    label "respondent"
  ]
  node [
    id 734
    label "sprzeciw"
  ]
  node [
    id 735
    label "mechanika"
  ]
  node [
    id 736
    label "utrzymywanie"
  ]
  node [
    id 737
    label "move"
  ]
  node [
    id 738
    label "poruszenie"
  ]
  node [
    id 739
    label "movement"
  ]
  node [
    id 740
    label "myk"
  ]
  node [
    id 741
    label "utrzyma&#263;"
  ]
  node [
    id 742
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 743
    label "utrzymanie"
  ]
  node [
    id 744
    label "travel"
  ]
  node [
    id 745
    label "kanciasty"
  ]
  node [
    id 746
    label "commercial_enterprise"
  ]
  node [
    id 747
    label "model"
  ]
  node [
    id 748
    label "strumie&#324;"
  ]
  node [
    id 749
    label "proces"
  ]
  node [
    id 750
    label "aktywno&#347;&#263;"
  ]
  node [
    id 751
    label "taktyka"
  ]
  node [
    id 752
    label "apraksja"
  ]
  node [
    id 753
    label "utrzymywa&#263;"
  ]
  node [
    id 754
    label "d&#322;ugi"
  ]
  node [
    id 755
    label "dyssypacja_energii"
  ]
  node [
    id 756
    label "tumult"
  ]
  node [
    id 757
    label "stopek"
  ]
  node [
    id 758
    label "zmiana"
  ]
  node [
    id 759
    label "manewr"
  ]
  node [
    id 760
    label "lokomocja"
  ]
  node [
    id 761
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 762
    label "komunikacja"
  ]
  node [
    id 763
    label "drift"
  ]
  node [
    id 764
    label "kognicja"
  ]
  node [
    id 765
    label "przebieg"
  ]
  node [
    id 766
    label "rozprawa"
  ]
  node [
    id 767
    label "legislacyjnie"
  ]
  node [
    id 768
    label "przes&#322;anka"
  ]
  node [
    id 769
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 770
    label "action"
  ]
  node [
    id 771
    label "stan"
  ]
  node [
    id 772
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 773
    label "postawa"
  ]
  node [
    id 774
    label "posuni&#281;cie"
  ]
  node [
    id 775
    label "maneuver"
  ]
  node [
    id 776
    label "absolutorium"
  ]
  node [
    id 777
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 778
    label "activity"
  ]
  node [
    id 779
    label "boski"
  ]
  node [
    id 780
    label "krajobraz"
  ]
  node [
    id 781
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 782
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 783
    label "przywidzenie"
  ]
  node [
    id 784
    label "presence"
  ]
  node [
    id 785
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 786
    label "transportation_system"
  ]
  node [
    id 787
    label "explicite"
  ]
  node [
    id 788
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 789
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 790
    label "wydeptywanie"
  ]
  node [
    id 791
    label "wydeptanie"
  ]
  node [
    id 792
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 793
    label "implicite"
  ]
  node [
    id 794
    label "ekspedytor"
  ]
  node [
    id 795
    label "bezproblemowy"
  ]
  node [
    id 796
    label "rewizja"
  ]
  node [
    id 797
    label "passage"
  ]
  node [
    id 798
    label "oznaka"
  ]
  node [
    id 799
    label "change"
  ]
  node [
    id 800
    label "ferment"
  ]
  node [
    id 801
    label "komplet"
  ]
  node [
    id 802
    label "anatomopatolog"
  ]
  node [
    id 803
    label "zmianka"
  ]
  node [
    id 804
    label "amendment"
  ]
  node [
    id 805
    label "praca"
  ]
  node [
    id 806
    label "odmienianie"
  ]
  node [
    id 807
    label "tura"
  ]
  node [
    id 808
    label "woda_powierzchniowa"
  ]
  node [
    id 809
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 810
    label "ciek_wodny"
  ]
  node [
    id 811
    label "mn&#243;stwo"
  ]
  node [
    id 812
    label "Ajgospotamoj"
  ]
  node [
    id 813
    label "fala"
  ]
  node [
    id 814
    label "disquiet"
  ]
  node [
    id 815
    label "ha&#322;as"
  ]
  node [
    id 816
    label "struktura"
  ]
  node [
    id 817
    label "mechanika_teoretyczna"
  ]
  node [
    id 818
    label "mechanika_gruntu"
  ]
  node [
    id 819
    label "mechanika_g&#243;rotworu"
  ]
  node [
    id 820
    label "mechanika_klasyczna"
  ]
  node [
    id 821
    label "elektromechanika"
  ]
  node [
    id 822
    label "mechanika_o&#347;rodk&#243;w_ci&#261;g&#322;ych"
  ]
  node [
    id 823
    label "nauka"
  ]
  node [
    id 824
    label "fizyka"
  ]
  node [
    id 825
    label "aeromechanika"
  ]
  node [
    id 826
    label "telemechanika"
  ]
  node [
    id 827
    label "hydromechanika"
  ]
  node [
    id 828
    label "daleki"
  ]
  node [
    id 829
    label "d&#322;ugo"
  ]
  node [
    id 830
    label "jednowyrazowy"
  ]
  node [
    id 831
    label "bliski"
  ]
  node [
    id 832
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 833
    label "drobny"
  ]
  node [
    id 834
    label "brak"
  ]
  node [
    id 835
    label "z&#322;y"
  ]
  node [
    id 836
    label "argue"
  ]
  node [
    id 837
    label "podtrzymywa&#263;"
  ]
  node [
    id 838
    label "s&#261;dzi&#263;"
  ]
  node [
    id 839
    label "twierdzi&#263;"
  ]
  node [
    id 840
    label "zapewnia&#263;"
  ]
  node [
    id 841
    label "corroborate"
  ]
  node [
    id 842
    label "trzyma&#263;"
  ]
  node [
    id 843
    label "panowa&#263;"
  ]
  node [
    id 844
    label "defy"
  ]
  node [
    id 845
    label "cope"
  ]
  node [
    id 846
    label "broni&#263;"
  ]
  node [
    id 847
    label "sprawowa&#263;"
  ]
  node [
    id 848
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 849
    label "zachowywa&#263;"
  ]
  node [
    id 850
    label "obroni&#263;"
  ]
  node [
    id 851
    label "potrzyma&#263;"
  ]
  node [
    id 852
    label "op&#322;aci&#263;"
  ]
  node [
    id 853
    label "zdo&#322;a&#263;"
  ]
  node [
    id 854
    label "feed"
  ]
  node [
    id 855
    label "przetrzyma&#263;"
  ]
  node [
    id 856
    label "foster"
  ]
  node [
    id 857
    label "zapewni&#263;"
  ]
  node [
    id 858
    label "unie&#347;&#263;"
  ]
  node [
    id 859
    label "bronienie"
  ]
  node [
    id 860
    label "trzymanie"
  ]
  node [
    id 861
    label "potrzymanie"
  ]
  node [
    id 862
    label "wychowywanie"
  ]
  node [
    id 863
    label "panowanie"
  ]
  node [
    id 864
    label "zachowywanie"
  ]
  node [
    id 865
    label "twierdzenie"
  ]
  node [
    id 866
    label "preservation"
  ]
  node [
    id 867
    label "chowanie"
  ]
  node [
    id 868
    label "retention"
  ]
  node [
    id 869
    label "op&#322;acanie"
  ]
  node [
    id 870
    label "zapewnienie"
  ]
  node [
    id 871
    label "s&#261;dzenie"
  ]
  node [
    id 872
    label "zapewnianie"
  ]
  node [
    id 873
    label "obronienie"
  ]
  node [
    id 874
    label "zap&#322;acenie"
  ]
  node [
    id 875
    label "przetrzymanie"
  ]
  node [
    id 876
    label "bearing"
  ]
  node [
    id 877
    label "zdo&#322;anie"
  ]
  node [
    id 878
    label "subsystencja"
  ]
  node [
    id 879
    label "uniesienie"
  ]
  node [
    id 880
    label "wy&#380;ywienie"
  ]
  node [
    id 881
    label "podtrzymanie"
  ]
  node [
    id 882
    label "wychowanie"
  ]
  node [
    id 883
    label "wzbudzenie"
  ]
  node [
    id 884
    label "gesture"
  ]
  node [
    id 885
    label "spowodowanie"
  ]
  node [
    id 886
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 887
    label "poruszanie_si&#281;"
  ]
  node [
    id 888
    label "zdarzenie_si&#281;"
  ]
  node [
    id 889
    label "nietaktowny"
  ]
  node [
    id 890
    label "kanciasto"
  ]
  node [
    id 891
    label "niezgrabny"
  ]
  node [
    id 892
    label "kanciaty"
  ]
  node [
    id 893
    label "szorstki"
  ]
  node [
    id 894
    label "niesk&#322;adny"
  ]
  node [
    id 895
    label "stra&#380;nik"
  ]
  node [
    id 896
    label "szko&#322;a"
  ]
  node [
    id 897
    label "przedszkole"
  ]
  node [
    id 898
    label "opiekun"
  ]
  node [
    id 899
    label "spos&#243;b"
  ]
  node [
    id 900
    label "prezenter"
  ]
  node [
    id 901
    label "typ"
  ]
  node [
    id 902
    label "mildew"
  ]
  node [
    id 903
    label "zi&#243;&#322;ko"
  ]
  node [
    id 904
    label "motif"
  ]
  node [
    id 905
    label "pozowanie"
  ]
  node [
    id 906
    label "ideal"
  ]
  node [
    id 907
    label "wz&#243;r"
  ]
  node [
    id 908
    label "matryca"
  ]
  node [
    id 909
    label "adaptation"
  ]
  node [
    id 910
    label "pozowa&#263;"
  ]
  node [
    id 911
    label "imitacja"
  ]
  node [
    id 912
    label "orygina&#322;"
  ]
  node [
    id 913
    label "apraxia"
  ]
  node [
    id 914
    label "zaburzenie"
  ]
  node [
    id 915
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 916
    label "sport_motorowy"
  ]
  node [
    id 917
    label "jazda"
  ]
  node [
    id 918
    label "zwiad"
  ]
  node [
    id 919
    label "metoda"
  ]
  node [
    id 920
    label "pocz&#261;tki"
  ]
  node [
    id 921
    label "wrinkle"
  ]
  node [
    id 922
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 923
    label "Sierpie&#324;"
  ]
  node [
    id 924
    label "Michnik"
  ]
  node [
    id 925
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 926
    label "solidarno&#347;ciowiec"
  ]
  node [
    id 927
    label "typ_mongoloidalny"
  ]
  node [
    id 928
    label "kolorowy"
  ]
  node [
    id 929
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 930
    label "ciep&#322;y"
  ]
  node [
    id 931
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 932
    label "jasny"
  ]
  node [
    id 933
    label "zabarwienie_si&#281;"
  ]
  node [
    id 934
    label "ciekawy"
  ]
  node [
    id 935
    label "r&#243;&#380;nofarbny"
  ]
  node [
    id 936
    label "r&#243;&#380;nokolorowo"
  ]
  node [
    id 937
    label "weso&#322;y"
  ]
  node [
    id 938
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 939
    label "barwienie"
  ]
  node [
    id 940
    label "kolorowo"
  ]
  node [
    id 941
    label "barwnie"
  ]
  node [
    id 942
    label "kolorowanie"
  ]
  node [
    id 943
    label "barwisty"
  ]
  node [
    id 944
    label "przyjemny"
  ]
  node [
    id 945
    label "barwienie_si&#281;"
  ]
  node [
    id 946
    label "pi&#281;kny"
  ]
  node [
    id 947
    label "ubarwienie"
  ]
  node [
    id 948
    label "mi&#322;y"
  ]
  node [
    id 949
    label "ocieplanie_si&#281;"
  ]
  node [
    id 950
    label "ocieplanie"
  ]
  node [
    id 951
    label "grzanie"
  ]
  node [
    id 952
    label "ocieplenie_si&#281;"
  ]
  node [
    id 953
    label "zagrzanie"
  ]
  node [
    id 954
    label "ocieplenie"
  ]
  node [
    id 955
    label "korzystny"
  ]
  node [
    id 956
    label "ciep&#322;o"
  ]
  node [
    id 957
    label "dobry"
  ]
  node [
    id 958
    label "o&#347;wietlenie"
  ]
  node [
    id 959
    label "szczery"
  ]
  node [
    id 960
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 961
    label "jasno"
  ]
  node [
    id 962
    label "o&#347;wietlanie"
  ]
  node [
    id 963
    label "przytomny"
  ]
  node [
    id 964
    label "zrozumia&#322;y"
  ]
  node [
    id 965
    label "niezm&#261;cony"
  ]
  node [
    id 966
    label "bia&#322;y"
  ]
  node [
    id 967
    label "klarowny"
  ]
  node [
    id 968
    label "jednoznaczny"
  ]
  node [
    id 969
    label "pogodny"
  ]
  node [
    id 970
    label "odcinanie_si&#281;"
  ]
  node [
    id 971
    label "westa"
  ]
  node [
    id 972
    label "przelezienie"
  ]
  node [
    id 973
    label "&#347;piew"
  ]
  node [
    id 974
    label "Synaj"
  ]
  node [
    id 975
    label "Kreml"
  ]
  node [
    id 976
    label "kierunek"
  ]
  node [
    id 977
    label "wysoki"
  ]
  node [
    id 978
    label "element"
  ]
  node [
    id 979
    label "wzniesienie"
  ]
  node [
    id 980
    label "grupa"
  ]
  node [
    id 981
    label "pi&#281;tro"
  ]
  node [
    id 982
    label "kupa"
  ]
  node [
    id 983
    label "przele&#378;&#263;"
  ]
  node [
    id 984
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 985
    label "karczek"
  ]
  node [
    id 986
    label "rami&#261;czko"
  ]
  node [
    id 987
    label "Jaworze"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 5
    target 560
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 563
  ]
  edge [
    source 5
    target 564
  ]
  edge [
    source 5
    target 565
  ]
  edge [
    source 5
    target 566
  ]
  edge [
    source 5
    target 567
  ]
  edge [
    source 5
    target 568
  ]
  edge [
    source 5
    target 569
  ]
  edge [
    source 5
    target 570
  ]
  edge [
    source 5
    target 571
  ]
  edge [
    source 5
    target 572
  ]
  edge [
    source 5
    target 573
  ]
  edge [
    source 5
    target 574
  ]
  edge [
    source 5
    target 575
  ]
  edge [
    source 5
    target 576
  ]
  edge [
    source 5
    target 577
  ]
  edge [
    source 5
    target 578
  ]
  edge [
    source 5
    target 579
  ]
  edge [
    source 5
    target 580
  ]
  edge [
    source 5
    target 581
  ]
  edge [
    source 5
    target 582
  ]
  edge [
    source 5
    target 583
  ]
  edge [
    source 5
    target 584
  ]
  edge [
    source 5
    target 585
  ]
  edge [
    source 5
    target 586
  ]
  edge [
    source 5
    target 587
  ]
  edge [
    source 5
    target 588
  ]
  edge [
    source 5
    target 589
  ]
  edge [
    source 5
    target 590
  ]
  edge [
    source 5
    target 591
  ]
  edge [
    source 5
    target 592
  ]
  edge [
    source 5
    target 593
  ]
  edge [
    source 5
    target 594
  ]
  edge [
    source 5
    target 595
  ]
  edge [
    source 5
    target 596
  ]
  edge [
    source 5
    target 597
  ]
  edge [
    source 5
    target 598
  ]
  edge [
    source 5
    target 599
  ]
  edge [
    source 5
    target 600
  ]
  edge [
    source 5
    target 601
  ]
  edge [
    source 5
    target 602
  ]
  edge [
    source 5
    target 603
  ]
  edge [
    source 5
    target 604
  ]
  edge [
    source 5
    target 605
  ]
  edge [
    source 5
    target 606
  ]
  edge [
    source 5
    target 607
  ]
  edge [
    source 5
    target 608
  ]
  edge [
    source 5
    target 609
  ]
  edge [
    source 5
    target 610
  ]
  edge [
    source 5
    target 611
  ]
  edge [
    source 5
    target 612
  ]
  edge [
    source 5
    target 613
  ]
  edge [
    source 5
    target 614
  ]
  edge [
    source 5
    target 615
  ]
  edge [
    source 5
    target 616
  ]
  edge [
    source 5
    target 617
  ]
  edge [
    source 5
    target 618
  ]
  edge [
    source 5
    target 619
  ]
  edge [
    source 5
    target 620
  ]
  edge [
    source 5
    target 621
  ]
  edge [
    source 5
    target 622
  ]
  edge [
    source 5
    target 623
  ]
  edge [
    source 5
    target 624
  ]
  edge [
    source 5
    target 625
  ]
  edge [
    source 5
    target 626
  ]
  edge [
    source 5
    target 627
  ]
  edge [
    source 5
    target 628
  ]
  edge [
    source 5
    target 629
  ]
  edge [
    source 5
    target 630
  ]
  edge [
    source 5
    target 631
  ]
  edge [
    source 5
    target 632
  ]
  edge [
    source 5
    target 633
  ]
  edge [
    source 5
    target 634
  ]
  edge [
    source 5
    target 635
  ]
  edge [
    source 5
    target 636
  ]
  edge [
    source 5
    target 637
  ]
  edge [
    source 5
    target 638
  ]
  edge [
    source 5
    target 639
  ]
  edge [
    source 5
    target 640
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 772
  ]
  edge [
    source 11
    target 773
  ]
  edge [
    source 11
    target 774
  ]
  edge [
    source 11
    target 775
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 777
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 778
  ]
  edge [
    source 11
    target 779
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 781
  ]
  edge [
    source 11
    target 782
  ]
  edge [
    source 11
    target 783
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 824
  ]
  edge [
    source 11
    target 825
  ]
  edge [
    source 11
    target 826
  ]
  edge [
    source 11
    target 827
  ]
  edge [
    source 11
    target 828
  ]
  edge [
    source 11
    target 829
  ]
  edge [
    source 11
    target 32
  ]
  edge [
    source 11
    target 830
  ]
  edge [
    source 11
    target 831
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 11
    target 832
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 833
  ]
  edge [
    source 11
    target 834
  ]
  edge [
    source 11
    target 835
  ]
  edge [
    source 11
    target 225
  ]
  edge [
    source 11
    target 836
  ]
  edge [
    source 11
    target 837
  ]
  edge [
    source 11
    target 838
  ]
  edge [
    source 11
    target 839
  ]
  edge [
    source 11
    target 840
  ]
  edge [
    source 11
    target 841
  ]
  edge [
    source 11
    target 842
  ]
  edge [
    source 11
    target 843
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 845
  ]
  edge [
    source 11
    target 846
  ]
  edge [
    source 11
    target 847
  ]
  edge [
    source 11
    target 848
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 850
  ]
  edge [
    source 11
    target 851
  ]
  edge [
    source 11
    target 852
  ]
  edge [
    source 11
    target 853
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 854
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 855
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 858
  ]
  edge [
    source 11
    target 859
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 861
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 879
  ]
  edge [
    source 11
    target 880
  ]
  edge [
    source 11
    target 881
  ]
  edge [
    source 11
    target 882
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 883
  ]
  edge [
    source 11
    target 884
  ]
  edge [
    source 11
    target 885
  ]
  edge [
    source 11
    target 886
  ]
  edge [
    source 11
    target 887
  ]
  edge [
    source 11
    target 888
  ]
  edge [
    source 11
    target 889
  ]
  edge [
    source 11
    target 890
  ]
  edge [
    source 11
    target 891
  ]
  edge [
    source 11
    target 892
  ]
  edge [
    source 11
    target 893
  ]
  edge [
    source 11
    target 894
  ]
  edge [
    source 11
    target 895
  ]
  edge [
    source 11
    target 896
  ]
  edge [
    source 11
    target 897
  ]
  edge [
    source 11
    target 898
  ]
  edge [
    source 11
    target 899
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 900
  ]
  edge [
    source 11
    target 901
  ]
  edge [
    source 11
    target 902
  ]
  edge [
    source 11
    target 903
  ]
  edge [
    source 11
    target 904
  ]
  edge [
    source 11
    target 905
  ]
  edge [
    source 11
    target 906
  ]
  edge [
    source 11
    target 907
  ]
  edge [
    source 11
    target 908
  ]
  edge [
    source 11
    target 909
  ]
  edge [
    source 11
    target 910
  ]
  edge [
    source 11
    target 911
  ]
  edge [
    source 11
    target 912
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 913
  ]
  edge [
    source 11
    target 914
  ]
  edge [
    source 11
    target 915
  ]
  edge [
    source 11
    target 916
  ]
  edge [
    source 11
    target 917
  ]
  edge [
    source 11
    target 918
  ]
  edge [
    source 11
    target 919
  ]
  edge [
    source 11
    target 920
  ]
  edge [
    source 11
    target 921
  ]
  edge [
    source 11
    target 922
  ]
  edge [
    source 11
    target 923
  ]
  edge [
    source 11
    target 924
  ]
  edge [
    source 11
    target 925
  ]
  edge [
    source 11
    target 926
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 927
  ]
  edge [
    source 12
    target 928
  ]
  edge [
    source 12
    target 929
  ]
  edge [
    source 12
    target 930
  ]
  edge [
    source 12
    target 931
  ]
  edge [
    source 12
    target 932
  ]
  edge [
    source 12
    target 933
  ]
  edge [
    source 12
    target 934
  ]
  edge [
    source 12
    target 935
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 936
  ]
  edge [
    source 12
    target 937
  ]
  edge [
    source 12
    target 938
  ]
  edge [
    source 12
    target 939
  ]
  edge [
    source 12
    target 940
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 942
  ]
  edge [
    source 12
    target 943
  ]
  edge [
    source 12
    target 944
  ]
  edge [
    source 12
    target 945
  ]
  edge [
    source 12
    target 946
  ]
  edge [
    source 12
    target 947
  ]
  edge [
    source 12
    target 948
  ]
  edge [
    source 12
    target 949
  ]
  edge [
    source 12
    target 950
  ]
  edge [
    source 12
    target 951
  ]
  edge [
    source 12
    target 952
  ]
  edge [
    source 12
    target 953
  ]
  edge [
    source 12
    target 954
  ]
  edge [
    source 12
    target 955
  ]
  edge [
    source 12
    target 956
  ]
  edge [
    source 12
    target 957
  ]
  edge [
    source 12
    target 958
  ]
  edge [
    source 12
    target 959
  ]
  edge [
    source 12
    target 960
  ]
  edge [
    source 12
    target 961
  ]
  edge [
    source 12
    target 962
  ]
  edge [
    source 12
    target 963
  ]
  edge [
    source 12
    target 964
  ]
  edge [
    source 12
    target 965
  ]
  edge [
    source 12
    target 966
  ]
  edge [
    source 12
    target 967
  ]
  edge [
    source 12
    target 968
  ]
  edge [
    source 12
    target 969
  ]
  edge [
    source 12
    target 970
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 971
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 972
  ]
  edge [
    source 13
    target 973
  ]
  edge [
    source 13
    target 974
  ]
  edge [
    source 13
    target 975
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 976
  ]
  edge [
    source 13
    target 977
  ]
  edge [
    source 13
    target 978
  ]
  edge [
    source 13
    target 979
  ]
  edge [
    source 13
    target 980
  ]
  edge [
    source 13
    target 981
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 982
  ]
  edge [
    source 13
    target 983
  ]
  edge [
    source 13
    target 984
  ]
  edge [
    source 13
    target 985
  ]
  edge [
    source 13
    target 986
  ]
  edge [
    source 13
    target 987
  ]
  edge [
    source 13
    target 709
  ]
]
