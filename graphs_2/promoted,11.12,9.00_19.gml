graph [
  node [
    id 0
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 1
    label "ruszy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "proces"
    origin "text"
  ]
  node [
    id 3
    label "sprawa"
    origin "text"
  ]
  node [
    id 4
    label "brutalny"
    origin "text"
  ]
  node [
    id 5
    label "pobi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 7
    label "wiceszef"
    origin "text"
  ]
  node [
    id 8
    label "knf"
    origin "text"
  ]
  node [
    id 9
    label "wojciech"
    origin "text"
  ]
  node [
    id 10
    label "kwa&#347;niaka"
    origin "text"
  ]
  node [
    id 11
    label "dowiedzie&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "reporter"
    origin "text"
  ]
  node [
    id 14
    label "rmf"
    origin "text"
  ]
  node [
    id 15
    label "motivate"
  ]
  node [
    id 16
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 17
    label "zabra&#263;"
  ]
  node [
    id 18
    label "go"
  ]
  node [
    id 19
    label "zrobi&#263;"
  ]
  node [
    id 20
    label "allude"
  ]
  node [
    id 21
    label "cut"
  ]
  node [
    id 22
    label "spowodowa&#263;"
  ]
  node [
    id 23
    label "stimulate"
  ]
  node [
    id 24
    label "zacz&#261;&#263;"
  ]
  node [
    id 25
    label "wzbudzi&#263;"
  ]
  node [
    id 26
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 27
    label "act"
  ]
  node [
    id 28
    label "post&#261;pi&#263;"
  ]
  node [
    id 29
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 30
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 31
    label "odj&#261;&#263;"
  ]
  node [
    id 32
    label "cause"
  ]
  node [
    id 33
    label "introduce"
  ]
  node [
    id 34
    label "begin"
  ]
  node [
    id 35
    label "do"
  ]
  node [
    id 36
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 37
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 38
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 39
    label "zorganizowa&#263;"
  ]
  node [
    id 40
    label "appoint"
  ]
  node [
    id 41
    label "wystylizowa&#263;"
  ]
  node [
    id 42
    label "przerobi&#263;"
  ]
  node [
    id 43
    label "nabra&#263;"
  ]
  node [
    id 44
    label "make"
  ]
  node [
    id 45
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 46
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 47
    label "wydali&#263;"
  ]
  node [
    id 48
    label "withdraw"
  ]
  node [
    id 49
    label "doprowadzi&#263;"
  ]
  node [
    id 50
    label "z&#322;apa&#263;"
  ]
  node [
    id 51
    label "wzi&#261;&#263;"
  ]
  node [
    id 52
    label "zaj&#261;&#263;"
  ]
  node [
    id 53
    label "consume"
  ]
  node [
    id 54
    label "deprive"
  ]
  node [
    id 55
    label "przenie&#347;&#263;"
  ]
  node [
    id 56
    label "abstract"
  ]
  node [
    id 57
    label "uda&#263;_si&#281;"
  ]
  node [
    id 58
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 59
    label "przesun&#261;&#263;"
  ]
  node [
    id 60
    label "wywo&#322;a&#263;"
  ]
  node [
    id 61
    label "arouse"
  ]
  node [
    id 62
    label "goban"
  ]
  node [
    id 63
    label "gra_planszowa"
  ]
  node [
    id 64
    label "sport_umys&#322;owy"
  ]
  node [
    id 65
    label "chi&#324;ski"
  ]
  node [
    id 66
    label "kognicja"
  ]
  node [
    id 67
    label "przebieg"
  ]
  node [
    id 68
    label "rozprawa"
  ]
  node [
    id 69
    label "wydarzenie"
  ]
  node [
    id 70
    label "legislacyjnie"
  ]
  node [
    id 71
    label "przes&#322;anka"
  ]
  node [
    id 72
    label "zjawisko"
  ]
  node [
    id 73
    label "nast&#281;pstwo"
  ]
  node [
    id 74
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 75
    label "przebiec"
  ]
  node [
    id 76
    label "charakter"
  ]
  node [
    id 77
    label "czynno&#347;&#263;"
  ]
  node [
    id 78
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 79
    label "motyw"
  ]
  node [
    id 80
    label "przebiegni&#281;cie"
  ]
  node [
    id 81
    label "fabu&#322;a"
  ]
  node [
    id 82
    label "s&#261;d"
  ]
  node [
    id 83
    label "rozumowanie"
  ]
  node [
    id 84
    label "opracowanie"
  ]
  node [
    id 85
    label "obrady"
  ]
  node [
    id 86
    label "cytat"
  ]
  node [
    id 87
    label "tekst"
  ]
  node [
    id 88
    label "obja&#347;nienie"
  ]
  node [
    id 89
    label "s&#261;dzenie"
  ]
  node [
    id 90
    label "linia"
  ]
  node [
    id 91
    label "procedura"
  ]
  node [
    id 92
    label "zbi&#243;r"
  ]
  node [
    id 93
    label "room"
  ]
  node [
    id 94
    label "ilo&#347;&#263;"
  ]
  node [
    id 95
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 96
    label "sequence"
  ]
  node [
    id 97
    label "praca"
  ]
  node [
    id 98
    label "cycle"
  ]
  node [
    id 99
    label "fakt"
  ]
  node [
    id 100
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 101
    label "przyczyna"
  ]
  node [
    id 102
    label "wnioskowanie"
  ]
  node [
    id 103
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 104
    label "prawo"
  ]
  node [
    id 105
    label "odczuwa&#263;"
  ]
  node [
    id 106
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 107
    label "wydziedziczy&#263;"
  ]
  node [
    id 108
    label "skrupienie_si&#281;"
  ]
  node [
    id 109
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 110
    label "wydziedziczenie"
  ]
  node [
    id 111
    label "odczucie"
  ]
  node [
    id 112
    label "pocz&#261;tek"
  ]
  node [
    id 113
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 114
    label "koszula_Dejaniry"
  ]
  node [
    id 115
    label "kolejno&#347;&#263;"
  ]
  node [
    id 116
    label "odczuwanie"
  ]
  node [
    id 117
    label "event"
  ]
  node [
    id 118
    label "rezultat"
  ]
  node [
    id 119
    label "skrupianie_si&#281;"
  ]
  node [
    id 120
    label "odczu&#263;"
  ]
  node [
    id 121
    label "boski"
  ]
  node [
    id 122
    label "krajobraz"
  ]
  node [
    id 123
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 124
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 125
    label "przywidzenie"
  ]
  node [
    id 126
    label "presence"
  ]
  node [
    id 127
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 128
    label "object"
  ]
  node [
    id 129
    label "temat"
  ]
  node [
    id 130
    label "szczeg&#243;&#322;"
  ]
  node [
    id 131
    label "proposition"
  ]
  node [
    id 132
    label "rzecz"
  ]
  node [
    id 133
    label "idea"
  ]
  node [
    id 134
    label "ideologia"
  ]
  node [
    id 135
    label "byt"
  ]
  node [
    id 136
    label "intelekt"
  ]
  node [
    id 137
    label "Kant"
  ]
  node [
    id 138
    label "p&#322;&#243;d"
  ]
  node [
    id 139
    label "cel"
  ]
  node [
    id 140
    label "poj&#281;cie"
  ]
  node [
    id 141
    label "istota"
  ]
  node [
    id 142
    label "pomys&#322;"
  ]
  node [
    id 143
    label "ideacja"
  ]
  node [
    id 144
    label "przedmiot"
  ]
  node [
    id 145
    label "wpadni&#281;cie"
  ]
  node [
    id 146
    label "mienie"
  ]
  node [
    id 147
    label "przyroda"
  ]
  node [
    id 148
    label "obiekt"
  ]
  node [
    id 149
    label "kultura"
  ]
  node [
    id 150
    label "wpa&#347;&#263;"
  ]
  node [
    id 151
    label "wpadanie"
  ]
  node [
    id 152
    label "wpada&#263;"
  ]
  node [
    id 153
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 154
    label "niuansowa&#263;"
  ]
  node [
    id 155
    label "element"
  ]
  node [
    id 156
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 157
    label "sk&#322;adnik"
  ]
  node [
    id 158
    label "zniuansowa&#263;"
  ]
  node [
    id 159
    label "wyraz_pochodny"
  ]
  node [
    id 160
    label "zboczenie"
  ]
  node [
    id 161
    label "om&#243;wienie"
  ]
  node [
    id 162
    label "cecha"
  ]
  node [
    id 163
    label "omawia&#263;"
  ]
  node [
    id 164
    label "fraza"
  ]
  node [
    id 165
    label "tre&#347;&#263;"
  ]
  node [
    id 166
    label "entity"
  ]
  node [
    id 167
    label "forum"
  ]
  node [
    id 168
    label "topik"
  ]
  node [
    id 169
    label "tematyka"
  ]
  node [
    id 170
    label "w&#261;tek"
  ]
  node [
    id 171
    label "zbaczanie"
  ]
  node [
    id 172
    label "forma"
  ]
  node [
    id 173
    label "om&#243;wi&#263;"
  ]
  node [
    id 174
    label "omawianie"
  ]
  node [
    id 175
    label "melodia"
  ]
  node [
    id 176
    label "otoczka"
  ]
  node [
    id 177
    label "zbacza&#263;"
  ]
  node [
    id 178
    label "zboczy&#263;"
  ]
  node [
    id 179
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 180
    label "szczery"
  ]
  node [
    id 181
    label "mocny"
  ]
  node [
    id 182
    label "przemoc"
  ]
  node [
    id 183
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 184
    label "bezlitosny"
  ]
  node [
    id 185
    label "silny"
  ]
  node [
    id 186
    label "okrutny"
  ]
  node [
    id 187
    label "brutalnie"
  ]
  node [
    id 188
    label "niedelikatny"
  ]
  node [
    id 189
    label "drastycznie"
  ]
  node [
    id 190
    label "straszny"
  ]
  node [
    id 191
    label "przykry"
  ]
  node [
    id 192
    label "bezwzgl&#281;dny"
  ]
  node [
    id 193
    label "trudny"
  ]
  node [
    id 194
    label "bezlito&#347;ny"
  ]
  node [
    id 195
    label "przestrze&#324;"
  ]
  node [
    id 196
    label "bezlito&#347;nie"
  ]
  node [
    id 197
    label "prawdziwy"
  ]
  node [
    id 198
    label "niepob&#322;a&#380;liwy"
  ]
  node [
    id 199
    label "nie&#322;askawy"
  ]
  node [
    id 200
    label "gro&#378;ny"
  ]
  node [
    id 201
    label "okrutnie"
  ]
  node [
    id 202
    label "z&#322;y"
  ]
  node [
    id 203
    label "nieludzki"
  ]
  node [
    id 204
    label "pod&#322;y"
  ]
  node [
    id 205
    label "nieprzyjemny"
  ]
  node [
    id 206
    label "niegrzeczny"
  ]
  node [
    id 207
    label "nieoboj&#281;tny"
  ]
  node [
    id 208
    label "niewra&#380;liwy"
  ]
  node [
    id 209
    label "wytrzyma&#322;y"
  ]
  node [
    id 210
    label "niedelikatnie"
  ]
  node [
    id 211
    label "szczodry"
  ]
  node [
    id 212
    label "s&#322;uszny"
  ]
  node [
    id 213
    label "uczciwy"
  ]
  node [
    id 214
    label "przekonuj&#261;cy"
  ]
  node [
    id 215
    label "prostoduszny"
  ]
  node [
    id 216
    label "szczyry"
  ]
  node [
    id 217
    label "szczerze"
  ]
  node [
    id 218
    label "czysty"
  ]
  node [
    id 219
    label "intensywny"
  ]
  node [
    id 220
    label "krzepienie"
  ]
  node [
    id 221
    label "&#380;ywotny"
  ]
  node [
    id 222
    label "pokrzepienie"
  ]
  node [
    id 223
    label "zdecydowany"
  ]
  node [
    id 224
    label "niepodwa&#380;alny"
  ]
  node [
    id 225
    label "du&#380;y"
  ]
  node [
    id 226
    label "mocno"
  ]
  node [
    id 227
    label "konkretny"
  ]
  node [
    id 228
    label "zdrowy"
  ]
  node [
    id 229
    label "silnie"
  ]
  node [
    id 230
    label "meflochina"
  ]
  node [
    id 231
    label "zajebisty"
  ]
  node [
    id 232
    label "stabilny"
  ]
  node [
    id 233
    label "krzepki"
  ]
  node [
    id 234
    label "wyrazisty"
  ]
  node [
    id 235
    label "widoczny"
  ]
  node [
    id 236
    label "wzmocni&#263;"
  ]
  node [
    id 237
    label "wzmacnia&#263;"
  ]
  node [
    id 238
    label "intensywnie"
  ]
  node [
    id 239
    label "dobry"
  ]
  node [
    id 240
    label "viciously"
  ]
  node [
    id 241
    label "wyrzyna&#263;"
  ]
  node [
    id 242
    label "cruelly"
  ]
  node [
    id 243
    label "bezpardonowo"
  ]
  node [
    id 244
    label "barbarously"
  ]
  node [
    id 245
    label "patologia"
  ]
  node [
    id 246
    label "agresja"
  ]
  node [
    id 247
    label "przewaga"
  ]
  node [
    id 248
    label "drastyczny"
  ]
  node [
    id 249
    label "radykalnie"
  ]
  node [
    id 250
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 251
    label "beat"
  ]
  node [
    id 252
    label "zbi&#263;"
  ]
  node [
    id 253
    label "pozabija&#263;"
  ]
  node [
    id 254
    label "pousuwa&#263;"
  ]
  node [
    id 255
    label "wpierniczy&#263;"
  ]
  node [
    id 256
    label "pomacha&#263;"
  ]
  node [
    id 257
    label "obi&#263;"
  ]
  node [
    id 258
    label "transgress"
  ]
  node [
    id 259
    label "pokona&#263;"
  ]
  node [
    id 260
    label "poniszczy&#263;"
  ]
  node [
    id 261
    label "upset"
  ]
  node [
    id 262
    label "pouderza&#263;"
  ]
  node [
    id 263
    label "nala&#263;"
  ]
  node [
    id 264
    label "wygra&#263;"
  ]
  node [
    id 265
    label "pokry&#263;"
  ]
  node [
    id 266
    label "uderzy&#263;"
  ]
  node [
    id 267
    label "uszkodzi&#263;"
  ]
  node [
    id 268
    label "st&#322;uc"
  ]
  node [
    id 269
    label "zagwarantowa&#263;"
  ]
  node [
    id 270
    label "znie&#347;&#263;"
  ]
  node [
    id 271
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 272
    label "zagra&#263;"
  ]
  node [
    id 273
    label "score"
  ]
  node [
    id 274
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 275
    label "zwojowa&#263;"
  ]
  node [
    id 276
    label "leave"
  ]
  node [
    id 277
    label "net_income"
  ]
  node [
    id 278
    label "instrument_muzyczny"
  ]
  node [
    id 279
    label "zwi&#281;kszy&#263;_si&#281;"
  ]
  node [
    id 280
    label "wyprzedzi&#263;"
  ]
  node [
    id 281
    label "fall"
  ]
  node [
    id 282
    label "przekroczy&#263;"
  ]
  node [
    id 283
    label "blight"
  ]
  node [
    id 284
    label "usun&#261;&#263;"
  ]
  node [
    id 285
    label "nabi&#263;"
  ]
  node [
    id 286
    label "&#347;cisn&#261;&#263;"
  ]
  node [
    id 287
    label "wy&#322;oi&#263;"
  ]
  node [
    id 288
    label "wyrobi&#263;"
  ]
  node [
    id 289
    label "obni&#380;y&#263;"
  ]
  node [
    id 290
    label "ubi&#263;"
  ]
  node [
    id 291
    label "&#347;cie&#347;ni&#263;"
  ]
  node [
    id 292
    label "str&#261;ci&#263;"
  ]
  node [
    id 293
    label "zebra&#263;"
  ]
  node [
    id 294
    label "break"
  ]
  node [
    id 295
    label "obali&#263;"
  ]
  node [
    id 296
    label "zgromadzi&#263;"
  ]
  node [
    id 297
    label "sku&#263;"
  ]
  node [
    id 298
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 299
    label "rozbi&#263;"
  ]
  node [
    id 300
    label "pomordowa&#263;"
  ]
  node [
    id 301
    label "pomorzy&#263;"
  ]
  node [
    id 302
    label "pozbija&#263;"
  ]
  node [
    id 303
    label "pozamyka&#263;"
  ]
  node [
    id 304
    label "killing"
  ]
  node [
    id 305
    label "pokrzywi&#263;"
  ]
  node [
    id 306
    label "pod&#378;wiga&#263;"
  ]
  node [
    id 307
    label "wave"
  ]
  node [
    id 308
    label "poradzi&#263;_sobie"
  ]
  node [
    id 309
    label "zapobiec"
  ]
  node [
    id 310
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 311
    label "z&#322;oi&#263;"
  ]
  node [
    id 312
    label "zaatakowa&#263;"
  ]
  node [
    id 313
    label "overwhelm"
  ]
  node [
    id 314
    label "rytm"
  ]
  node [
    id 315
    label "wype&#322;ni&#263;"
  ]
  node [
    id 316
    label "pour"
  ]
  node [
    id 317
    label "rozla&#263;"
  ]
  node [
    id 318
    label "zala&#263;"
  ]
  node [
    id 319
    label "nasika&#263;"
  ]
  node [
    id 320
    label "spill_over"
  ]
  node [
    id 321
    label "rozgniewa&#263;"
  ]
  node [
    id 322
    label "wkopa&#263;"
  ]
  node [
    id 323
    label "wepchn&#261;&#263;"
  ]
  node [
    id 324
    label "zje&#347;&#263;"
  ]
  node [
    id 325
    label "umie&#347;ci&#263;"
  ]
  node [
    id 326
    label "dawny"
  ]
  node [
    id 327
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 328
    label "eksprezydent"
  ]
  node [
    id 329
    label "partner"
  ]
  node [
    id 330
    label "rozw&#243;d"
  ]
  node [
    id 331
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 332
    label "wcze&#347;niejszy"
  ]
  node [
    id 333
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 334
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 335
    label "pracownik"
  ]
  node [
    id 336
    label "przedsi&#281;biorca"
  ]
  node [
    id 337
    label "cz&#322;owiek"
  ]
  node [
    id 338
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 339
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 340
    label "kolaborator"
  ]
  node [
    id 341
    label "prowadzi&#263;"
  ]
  node [
    id 342
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 343
    label "sp&#243;lnik"
  ]
  node [
    id 344
    label "aktor"
  ]
  node [
    id 345
    label "uczestniczenie"
  ]
  node [
    id 346
    label "wcze&#347;niej"
  ]
  node [
    id 347
    label "przestarza&#322;y"
  ]
  node [
    id 348
    label "odleg&#322;y"
  ]
  node [
    id 349
    label "przesz&#322;y"
  ]
  node [
    id 350
    label "od_dawna"
  ]
  node [
    id 351
    label "poprzedni"
  ]
  node [
    id 352
    label "dawno"
  ]
  node [
    id 353
    label "d&#322;ugoletni"
  ]
  node [
    id 354
    label "anachroniczny"
  ]
  node [
    id 355
    label "dawniej"
  ]
  node [
    id 356
    label "niegdysiejszy"
  ]
  node [
    id 357
    label "kombatant"
  ]
  node [
    id 358
    label "stary"
  ]
  node [
    id 359
    label "rozstanie"
  ]
  node [
    id 360
    label "ekspartner"
  ]
  node [
    id 361
    label "rozbita_rodzina"
  ]
  node [
    id 362
    label "uniewa&#380;nienie"
  ]
  node [
    id 363
    label "separation"
  ]
  node [
    id 364
    label "prezydent"
  ]
  node [
    id 365
    label "zast&#281;pca"
  ]
  node [
    id 366
    label "dziennikarz"
  ]
  node [
    id 367
    label "publicysta"
  ]
  node [
    id 368
    label "nowiniarz"
  ]
  node [
    id 369
    label "bran&#380;owiec"
  ]
  node [
    id 370
    label "akredytowanie"
  ]
  node [
    id 371
    label "akredytowa&#263;"
  ]
  node [
    id 372
    label "Wojciech"
  ]
  node [
    id 373
    label "Kwa&#347;niaka"
  ]
  node [
    id 374
    label "RMF"
  ]
  node [
    id 375
    label "FM"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 372
    target 373
  ]
  edge [
    source 374
    target 375
  ]
]
