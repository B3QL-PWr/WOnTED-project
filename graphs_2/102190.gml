graph [
  node [
    id 0
    label "dwa"
    origin "text"
  ]
  node [
    id 1
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "zdrowie"
    origin "text"
  ]
  node [
    id 3
    label "celina"
    origin "text"
  ]
  node [
    id 4
    label "pozwoli&#263;"
    origin "text"
  ]
  node [
    id 5
    label "siebie"
    origin "text"
  ]
  node [
    id 6
    label "gdzie&#347;"
    origin "text"
  ]
  node [
    id 7
    label "odessa"
    origin "text"
  ]
  node [
    id 8
    label "daleko"
    origin "text"
  ]
  node [
    id 9
    label "doba"
  ]
  node [
    id 10
    label "weekend"
  ]
  node [
    id 11
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 12
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 13
    label "czas"
  ]
  node [
    id 14
    label "miesi&#261;c"
  ]
  node [
    id 15
    label "poprzedzanie"
  ]
  node [
    id 16
    label "czasoprzestrze&#324;"
  ]
  node [
    id 17
    label "laba"
  ]
  node [
    id 18
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 19
    label "chronometria"
  ]
  node [
    id 20
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 21
    label "rachuba_czasu"
  ]
  node [
    id 22
    label "przep&#322;ywanie"
  ]
  node [
    id 23
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 24
    label "czasokres"
  ]
  node [
    id 25
    label "odczyt"
  ]
  node [
    id 26
    label "chwila"
  ]
  node [
    id 27
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 28
    label "dzieje"
  ]
  node [
    id 29
    label "kategoria_gramatyczna"
  ]
  node [
    id 30
    label "poprzedzenie"
  ]
  node [
    id 31
    label "trawienie"
  ]
  node [
    id 32
    label "pochodzi&#263;"
  ]
  node [
    id 33
    label "period"
  ]
  node [
    id 34
    label "okres_czasu"
  ]
  node [
    id 35
    label "poprzedza&#263;"
  ]
  node [
    id 36
    label "schy&#322;ek"
  ]
  node [
    id 37
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 38
    label "odwlekanie_si&#281;"
  ]
  node [
    id 39
    label "zegar"
  ]
  node [
    id 40
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 41
    label "czwarty_wymiar"
  ]
  node [
    id 42
    label "pochodzenie"
  ]
  node [
    id 43
    label "koniugacja"
  ]
  node [
    id 44
    label "Zeitgeist"
  ]
  node [
    id 45
    label "trawi&#263;"
  ]
  node [
    id 46
    label "pogoda"
  ]
  node [
    id 47
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 48
    label "poprzedzi&#263;"
  ]
  node [
    id 49
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 50
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 51
    label "time_period"
  ]
  node [
    id 52
    label "noc"
  ]
  node [
    id 53
    label "dzie&#324;"
  ]
  node [
    id 54
    label "godzina"
  ]
  node [
    id 55
    label "long_time"
  ]
  node [
    id 56
    label "jednostka_geologiczna"
  ]
  node [
    id 57
    label "niedziela"
  ]
  node [
    id 58
    label "sobota"
  ]
  node [
    id 59
    label "miech"
  ]
  node [
    id 60
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 61
    label "rok"
  ]
  node [
    id 62
    label "kalendy"
  ]
  node [
    id 63
    label "os&#322;abianie"
  ]
  node [
    id 64
    label "kondycja"
  ]
  node [
    id 65
    label "os&#322;abienie"
  ]
  node [
    id 66
    label "zniszczenie"
  ]
  node [
    id 67
    label "os&#322;abia&#263;"
  ]
  node [
    id 68
    label "zedrze&#263;"
  ]
  node [
    id 69
    label "niszczenie"
  ]
  node [
    id 70
    label "os&#322;abi&#263;"
  ]
  node [
    id 71
    label "soundness"
  ]
  node [
    id 72
    label "stan"
  ]
  node [
    id 73
    label "niszczy&#263;"
  ]
  node [
    id 74
    label "zniszczy&#263;"
  ]
  node [
    id 75
    label "zdarcie"
  ]
  node [
    id 76
    label "firmness"
  ]
  node [
    id 77
    label "cecha"
  ]
  node [
    id 78
    label "rozsypanie_si&#281;"
  ]
  node [
    id 79
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 80
    label "charakterystyka"
  ]
  node [
    id 81
    label "m&#322;ot"
  ]
  node [
    id 82
    label "znak"
  ]
  node [
    id 83
    label "drzewo"
  ]
  node [
    id 84
    label "pr&#243;ba"
  ]
  node [
    id 85
    label "attribute"
  ]
  node [
    id 86
    label "marka"
  ]
  node [
    id 87
    label "Ohio"
  ]
  node [
    id 88
    label "wci&#281;cie"
  ]
  node [
    id 89
    label "Nowy_York"
  ]
  node [
    id 90
    label "warstwa"
  ]
  node [
    id 91
    label "samopoczucie"
  ]
  node [
    id 92
    label "Illinois"
  ]
  node [
    id 93
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 94
    label "state"
  ]
  node [
    id 95
    label "Jukatan"
  ]
  node [
    id 96
    label "Kalifornia"
  ]
  node [
    id 97
    label "Wirginia"
  ]
  node [
    id 98
    label "wektor"
  ]
  node [
    id 99
    label "by&#263;"
  ]
  node [
    id 100
    label "Goa"
  ]
  node [
    id 101
    label "Teksas"
  ]
  node [
    id 102
    label "Waszyngton"
  ]
  node [
    id 103
    label "miejsce"
  ]
  node [
    id 104
    label "Massachusetts"
  ]
  node [
    id 105
    label "Alaska"
  ]
  node [
    id 106
    label "Arakan"
  ]
  node [
    id 107
    label "Hawaje"
  ]
  node [
    id 108
    label "Maryland"
  ]
  node [
    id 109
    label "punkt"
  ]
  node [
    id 110
    label "Michigan"
  ]
  node [
    id 111
    label "Arizona"
  ]
  node [
    id 112
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 113
    label "Georgia"
  ]
  node [
    id 114
    label "poziom"
  ]
  node [
    id 115
    label "Pensylwania"
  ]
  node [
    id 116
    label "shape"
  ]
  node [
    id 117
    label "Luizjana"
  ]
  node [
    id 118
    label "Nowy_Meksyk"
  ]
  node [
    id 119
    label "Alabama"
  ]
  node [
    id 120
    label "ilo&#347;&#263;"
  ]
  node [
    id 121
    label "Kansas"
  ]
  node [
    id 122
    label "Oregon"
  ]
  node [
    id 123
    label "Oklahoma"
  ]
  node [
    id 124
    label "Floryda"
  ]
  node [
    id 125
    label "jednostka_administracyjna"
  ]
  node [
    id 126
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 127
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 128
    label "dyspozycja"
  ]
  node [
    id 129
    label "situation"
  ]
  node [
    id 130
    label "rank"
  ]
  node [
    id 131
    label "zdolno&#347;&#263;"
  ]
  node [
    id 132
    label "sytuacja"
  ]
  node [
    id 133
    label "doznanie"
  ]
  node [
    id 134
    label "fatigue_duty"
  ]
  node [
    id 135
    label "zmniejszenie"
  ]
  node [
    id 136
    label "kondycja_fizyczna"
  ]
  node [
    id 137
    label "spowodowanie"
  ]
  node [
    id 138
    label "infirmity"
  ]
  node [
    id 139
    label "s&#322;abszy"
  ]
  node [
    id 140
    label "pogorszenie"
  ]
  node [
    id 141
    label "czynno&#347;&#263;"
  ]
  node [
    id 142
    label "suppress"
  ]
  node [
    id 143
    label "robi&#263;"
  ]
  node [
    id 144
    label "cz&#322;owiek"
  ]
  node [
    id 145
    label "powodowa&#263;"
  ]
  node [
    id 146
    label "zmniejsza&#263;"
  ]
  node [
    id 147
    label "bate"
  ]
  node [
    id 148
    label "de-escalation"
  ]
  node [
    id 149
    label "powodowanie"
  ]
  node [
    id 150
    label "debilitation"
  ]
  node [
    id 151
    label "zmniejszanie"
  ]
  node [
    id 152
    label "pogarszanie"
  ]
  node [
    id 153
    label "wear"
  ]
  node [
    id 154
    label "destruction"
  ]
  node [
    id 155
    label "zu&#380;ycie"
  ]
  node [
    id 156
    label "attrition"
  ]
  node [
    id 157
    label "zaszkodzenie"
  ]
  node [
    id 158
    label "podpalenie"
  ]
  node [
    id 159
    label "strata"
  ]
  node [
    id 160
    label "spl&#261;drowanie"
  ]
  node [
    id 161
    label "poniszczenie"
  ]
  node [
    id 162
    label "ruin"
  ]
  node [
    id 163
    label "stanie_si&#281;"
  ]
  node [
    id 164
    label "rezultat"
  ]
  node [
    id 165
    label "poniszczenie_si&#281;"
  ]
  node [
    id 166
    label "reduce"
  ]
  node [
    id 167
    label "spowodowa&#263;"
  ]
  node [
    id 168
    label "zmniejszy&#263;"
  ]
  node [
    id 169
    label "cushion"
  ]
  node [
    id 170
    label "health"
  ]
  node [
    id 171
    label "wp&#322;yw"
  ]
  node [
    id 172
    label "destroy"
  ]
  node [
    id 173
    label "uszkadza&#263;"
  ]
  node [
    id 174
    label "szkodzi&#263;"
  ]
  node [
    id 175
    label "mar"
  ]
  node [
    id 176
    label "wygrywa&#263;"
  ]
  node [
    id 177
    label "pamper"
  ]
  node [
    id 178
    label "zaszkodzi&#263;"
  ]
  node [
    id 179
    label "zu&#380;y&#263;"
  ]
  node [
    id 180
    label "spoil"
  ]
  node [
    id 181
    label "consume"
  ]
  node [
    id 182
    label "wygra&#263;"
  ]
  node [
    id 183
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 184
    label "nadwyr&#281;&#380;enie"
  ]
  node [
    id 185
    label "gard&#322;o"
  ]
  node [
    id 186
    label "zranienie"
  ]
  node [
    id 187
    label "s&#322;ony"
  ]
  node [
    id 188
    label "wzi&#281;cie"
  ]
  node [
    id 189
    label "pl&#261;drowanie"
  ]
  node [
    id 190
    label "ravaging"
  ]
  node [
    id 191
    label "gnojenie"
  ]
  node [
    id 192
    label "szkodzenie"
  ]
  node [
    id 193
    label "pustoszenie"
  ]
  node [
    id 194
    label "decay"
  ]
  node [
    id 195
    label "poniewieranie_si&#281;"
  ]
  node [
    id 196
    label "devastation"
  ]
  node [
    id 197
    label "zu&#380;ywanie"
  ]
  node [
    id 198
    label "stawanie_si&#281;"
  ]
  node [
    id 199
    label "zarobi&#263;"
  ]
  node [
    id 200
    label "nadwyr&#281;&#380;y&#263;"
  ]
  node [
    id 201
    label "wzi&#261;&#263;"
  ]
  node [
    id 202
    label "zrani&#263;"
  ]
  node [
    id 203
    label "&#347;ci&#261;gn&#261;&#263;"
  ]
  node [
    id 204
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 205
    label "pofolgowa&#263;"
  ]
  node [
    id 206
    label "assent"
  ]
  node [
    id 207
    label "uzna&#263;"
  ]
  node [
    id 208
    label "leave"
  ]
  node [
    id 209
    label "oceni&#263;"
  ]
  node [
    id 210
    label "przyzna&#263;"
  ]
  node [
    id 211
    label "stwierdzi&#263;"
  ]
  node [
    id 212
    label "rede"
  ]
  node [
    id 213
    label "see"
  ]
  node [
    id 214
    label "permit"
  ]
  node [
    id 215
    label "nisko"
  ]
  node [
    id 216
    label "znacznie"
  ]
  node [
    id 217
    label "het"
  ]
  node [
    id 218
    label "dawno"
  ]
  node [
    id 219
    label "daleki"
  ]
  node [
    id 220
    label "g&#322;&#281;boko"
  ]
  node [
    id 221
    label "nieobecnie"
  ]
  node [
    id 222
    label "wysoko"
  ]
  node [
    id 223
    label "du&#380;o"
  ]
  node [
    id 224
    label "dawny"
  ]
  node [
    id 225
    label "ogl&#281;dny"
  ]
  node [
    id 226
    label "d&#322;ugi"
  ]
  node [
    id 227
    label "du&#380;y"
  ]
  node [
    id 228
    label "odleg&#322;y"
  ]
  node [
    id 229
    label "zwi&#261;zany"
  ]
  node [
    id 230
    label "r&#243;&#380;ny"
  ]
  node [
    id 231
    label "s&#322;aby"
  ]
  node [
    id 232
    label "odlegle"
  ]
  node [
    id 233
    label "oddalony"
  ]
  node [
    id 234
    label "g&#322;&#281;boki"
  ]
  node [
    id 235
    label "obcy"
  ]
  node [
    id 236
    label "nieobecny"
  ]
  node [
    id 237
    label "przysz&#322;y"
  ]
  node [
    id 238
    label "niepo&#347;lednio"
  ]
  node [
    id 239
    label "wysoki"
  ]
  node [
    id 240
    label "g&#243;rno"
  ]
  node [
    id 241
    label "chwalebnie"
  ]
  node [
    id 242
    label "wznio&#347;le"
  ]
  node [
    id 243
    label "szczytny"
  ]
  node [
    id 244
    label "d&#322;ugotrwale"
  ]
  node [
    id 245
    label "wcze&#347;niej"
  ]
  node [
    id 246
    label "ongi&#347;"
  ]
  node [
    id 247
    label "dawnie"
  ]
  node [
    id 248
    label "zamy&#347;lony"
  ]
  node [
    id 249
    label "uni&#380;enie"
  ]
  node [
    id 250
    label "pospolicie"
  ]
  node [
    id 251
    label "blisko"
  ]
  node [
    id 252
    label "wstydliwie"
  ]
  node [
    id 253
    label "ma&#322;o"
  ]
  node [
    id 254
    label "vilely"
  ]
  node [
    id 255
    label "despicably"
  ]
  node [
    id 256
    label "niski"
  ]
  node [
    id 257
    label "po&#347;lednio"
  ]
  node [
    id 258
    label "ma&#322;y"
  ]
  node [
    id 259
    label "mocno"
  ]
  node [
    id 260
    label "gruntownie"
  ]
  node [
    id 261
    label "szczerze"
  ]
  node [
    id 262
    label "silnie"
  ]
  node [
    id 263
    label "intensywnie"
  ]
  node [
    id 264
    label "zauwa&#380;alnie"
  ]
  node [
    id 265
    label "znaczny"
  ]
  node [
    id 266
    label "wiela"
  ]
  node [
    id 267
    label "bardzo"
  ]
  node [
    id 268
    label "cz&#281;sto"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
]
