graph [
  node [
    id 0
    label "matematyk"
    origin "text"
  ]
  node [
    id 1
    label "magia"
    origin "text"
  ]
  node [
    id 2
    label "nauka"
    origin "text"
  ]
  node [
    id 3
    label "kontynuowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wietny"
    origin "text"
  ]
  node [
    id 5
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 6
    label "klopsztanga"
    origin "text"
  ]
  node [
    id 7
    label "Biot"
  ]
  node [
    id 8
    label "Borel"
  ]
  node [
    id 9
    label "naukowiec"
  ]
  node [
    id 10
    label "Newton"
  ]
  node [
    id 11
    label "Gauss"
  ]
  node [
    id 12
    label "Pitagoras"
  ]
  node [
    id 13
    label "Ptolemeusz"
  ]
  node [
    id 14
    label "Maxwell"
  ]
  node [
    id 15
    label "Doppler"
  ]
  node [
    id 16
    label "Euklides"
  ]
  node [
    id 17
    label "Kepler"
  ]
  node [
    id 18
    label "Galileusz"
  ]
  node [
    id 19
    label "Kartezjusz"
  ]
  node [
    id 20
    label "Laplace"
  ]
  node [
    id 21
    label "nauczyciel"
  ]
  node [
    id 22
    label "Pascal"
  ]
  node [
    id 23
    label "Archimedes"
  ]
  node [
    id 24
    label "Berkeley"
  ]
  node [
    id 25
    label "Fourier"
  ]
  node [
    id 26
    label "Bayes"
  ]
  node [
    id 27
    label "Miczurin"
  ]
  node [
    id 28
    label "uczony"
  ]
  node [
    id 29
    label "&#347;ledziciel"
  ]
  node [
    id 30
    label "belfer"
  ]
  node [
    id 31
    label "szkolnik"
  ]
  node [
    id 32
    label "preceptor"
  ]
  node [
    id 33
    label "kszta&#322;ciciel"
  ]
  node [
    id 34
    label "profesor"
  ]
  node [
    id 35
    label "pedagog"
  ]
  node [
    id 36
    label "popularyzator"
  ]
  node [
    id 37
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 38
    label "rozci&#261;g&#322;y"
  ]
  node [
    id 39
    label "wyra&#378;no&#347;&#263;"
  ]
  node [
    id 40
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 41
    label "filozofia"
  ]
  node [
    id 42
    label "dorobek"
  ]
  node [
    id 43
    label "pogl&#261;dy"
  ]
  node [
    id 44
    label "j&#281;zyk_programowania"
  ]
  node [
    id 45
    label "ideologia"
  ]
  node [
    id 46
    label "pitagorejczyk"
  ]
  node [
    id 47
    label "czarodziejka"
  ]
  node [
    id 48
    label "czarodziej"
  ]
  node [
    id 49
    label "agreeableness"
  ]
  node [
    id 50
    label "wikkanin"
  ]
  node [
    id 51
    label "niecodzienno&#347;&#263;"
  ]
  node [
    id 52
    label "cecha"
  ]
  node [
    id 53
    label "zjawisko"
  ]
  node [
    id 54
    label "praktyki"
  ]
  node [
    id 55
    label "czar"
  ]
  node [
    id 56
    label "czarownica"
  ]
  node [
    id 57
    label "practice"
  ]
  node [
    id 58
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 59
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 60
    label "charakter"
  ]
  node [
    id 61
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 62
    label "proces"
  ]
  node [
    id 63
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 64
    label "przywidzenie"
  ]
  node [
    id 65
    label "boski"
  ]
  node [
    id 66
    label "krajobraz"
  ]
  node [
    id 67
    label "presence"
  ]
  node [
    id 68
    label "charakterystyka"
  ]
  node [
    id 69
    label "m&#322;ot"
  ]
  node [
    id 70
    label "marka"
  ]
  node [
    id 71
    label "pr&#243;ba"
  ]
  node [
    id 72
    label "attribute"
  ]
  node [
    id 73
    label "drzewo"
  ]
  node [
    id 74
    label "znak"
  ]
  node [
    id 75
    label "rzuci&#263;"
  ]
  node [
    id 76
    label "rzuca&#263;"
  ]
  node [
    id 77
    label "zakl&#281;cie"
  ]
  node [
    id 78
    label "rzucanie"
  ]
  node [
    id 79
    label "attraction"
  ]
  node [
    id 80
    label "rzucenie"
  ]
  node [
    id 81
    label "licz"
  ]
  node [
    id 82
    label "czarownik"
  ]
  node [
    id 83
    label "Gandalf"
  ]
  node [
    id 84
    label "istota_fantastyczna"
  ]
  node [
    id 85
    label "Harry_Potter"
  ]
  node [
    id 86
    label "Saruman"
  ]
  node [
    id 87
    label "rzadko&#347;&#263;"
  ]
  node [
    id 88
    label "kobieta"
  ]
  node [
    id 89
    label "Baba_Jaga"
  ]
  node [
    id 90
    label "zo&#322;za"
  ]
  node [
    id 91
    label "neopoganin"
  ]
  node [
    id 92
    label "cz&#322;owiek"
  ]
  node [
    id 93
    label "Meluzyna"
  ]
  node [
    id 94
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 95
    label "typologia"
  ]
  node [
    id 96
    label "nomotetyczny"
  ]
  node [
    id 97
    label "wiedza"
  ]
  node [
    id 98
    label "dziedzina"
  ]
  node [
    id 99
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 100
    label "&#322;awa_szkolna"
  ]
  node [
    id 101
    label "nauki_o_poznaniu"
  ]
  node [
    id 102
    label "kultura_duchowa"
  ]
  node [
    id 103
    label "teoria_naukowa"
  ]
  node [
    id 104
    label "nauki_penalne"
  ]
  node [
    id 105
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 106
    label "nauki_o_Ziemi"
  ]
  node [
    id 107
    label "imagineskopia"
  ]
  node [
    id 108
    label "metodologia"
  ]
  node [
    id 109
    label "fotowoltaika"
  ]
  node [
    id 110
    label "inwentyka"
  ]
  node [
    id 111
    label "systematyka"
  ]
  node [
    id 112
    label "porada"
  ]
  node [
    id 113
    label "miasteczko_rowerowe"
  ]
  node [
    id 114
    label "przem&#243;wienie"
  ]
  node [
    id 115
    label "zbi&#243;r"
  ]
  node [
    id 116
    label "zakres"
  ]
  node [
    id 117
    label "funkcja"
  ]
  node [
    id 118
    label "bezdro&#380;e"
  ]
  node [
    id 119
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 120
    label "sfera"
  ]
  node [
    id 121
    label "poddzia&#322;"
  ]
  node [
    id 122
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 123
    label "przebieg"
  ]
  node [
    id 124
    label "rozprawa"
  ]
  node [
    id 125
    label "kognicja"
  ]
  node [
    id 126
    label "wydarzenie"
  ]
  node [
    id 127
    label "przes&#322;anka"
  ]
  node [
    id 128
    label "legislacyjnie"
  ]
  node [
    id 129
    label "nast&#281;pstwo"
  ]
  node [
    id 130
    label "wyg&#322;oszenie"
  ]
  node [
    id 131
    label "sermon"
  ]
  node [
    id 132
    label "zrozumienie"
  ]
  node [
    id 133
    label "wypowied&#378;"
  ]
  node [
    id 134
    label "wyst&#261;pienie"
  ]
  node [
    id 135
    label "obronienie"
  ]
  node [
    id 136
    label "wydanie"
  ]
  node [
    id 137
    label "talk"
  ]
  node [
    id 138
    label "oddzia&#322;anie"
  ]
  node [
    id 139
    label "address"
  ]
  node [
    id 140
    label "wydobycie"
  ]
  node [
    id 141
    label "odzyskanie"
  ]
  node [
    id 142
    label "pozwolenie"
  ]
  node [
    id 143
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 144
    label "zaawansowanie"
  ]
  node [
    id 145
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 146
    label "intelekt"
  ]
  node [
    id 147
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 148
    label "wykszta&#322;cenie"
  ]
  node [
    id 149
    label "cognition"
  ]
  node [
    id 150
    label "wskaz&#243;wka"
  ]
  node [
    id 151
    label "technika"
  ]
  node [
    id 152
    label "kwantyfikacja"
  ]
  node [
    id 153
    label "typology"
  ]
  node [
    id 154
    label "podzia&#322;"
  ]
  node [
    id 155
    label "taksonomia"
  ]
  node [
    id 156
    label "biosystematyka"
  ]
  node [
    id 157
    label "kohorta"
  ]
  node [
    id 158
    label "biologia"
  ]
  node [
    id 159
    label "kladystyka"
  ]
  node [
    id 160
    label "funkcjonalizm"
  ]
  node [
    id 161
    label "aparat_krytyczny"
  ]
  node [
    id 162
    label "wyobra&#378;nia"
  ]
  node [
    id 163
    label "charakterystyczny"
  ]
  node [
    id 164
    label "robi&#263;"
  ]
  node [
    id 165
    label "prosecute"
  ]
  node [
    id 166
    label "oszukiwa&#263;"
  ]
  node [
    id 167
    label "tentegowa&#263;"
  ]
  node [
    id 168
    label "urz&#261;dza&#263;"
  ]
  node [
    id 169
    label "praca"
  ]
  node [
    id 170
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 171
    label "czyni&#263;"
  ]
  node [
    id 172
    label "work"
  ]
  node [
    id 173
    label "przerabia&#263;"
  ]
  node [
    id 174
    label "act"
  ]
  node [
    id 175
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 176
    label "give"
  ]
  node [
    id 177
    label "post&#281;powa&#263;"
  ]
  node [
    id 178
    label "peddle"
  ]
  node [
    id 179
    label "organizowa&#263;"
  ]
  node [
    id 180
    label "falowa&#263;"
  ]
  node [
    id 181
    label "stylizowa&#263;"
  ]
  node [
    id 182
    label "wydala&#263;"
  ]
  node [
    id 183
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 184
    label "ukazywa&#263;"
  ]
  node [
    id 185
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 186
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 187
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 188
    label "skuteczny"
  ]
  node [
    id 189
    label "wa&#380;ny"
  ]
  node [
    id 190
    label "zajebisty"
  ]
  node [
    id 191
    label "pozytywny"
  ]
  node [
    id 192
    label "spania&#322;y"
  ]
  node [
    id 193
    label "pomy&#347;lny"
  ]
  node [
    id 194
    label "arcydzielny"
  ]
  node [
    id 195
    label "dobry"
  ]
  node [
    id 196
    label "wspaniale"
  ]
  node [
    id 197
    label "superancki"
  ]
  node [
    id 198
    label "&#347;wietnie"
  ]
  node [
    id 199
    label "znaczny"
  ]
  node [
    id 200
    label "silny"
  ]
  node [
    id 201
    label "wa&#380;nie"
  ]
  node [
    id 202
    label "eksponowany"
  ]
  node [
    id 203
    label "wynios&#322;y"
  ]
  node [
    id 204
    label "istotnie"
  ]
  node [
    id 205
    label "dono&#347;ny"
  ]
  node [
    id 206
    label "ca&#322;y"
  ]
  node [
    id 207
    label "czw&#243;rka"
  ]
  node [
    id 208
    label "spokojny"
  ]
  node [
    id 209
    label "pos&#322;uszny"
  ]
  node [
    id 210
    label "korzystny"
  ]
  node [
    id 211
    label "drogi"
  ]
  node [
    id 212
    label "moralny"
  ]
  node [
    id 213
    label "powitanie"
  ]
  node [
    id 214
    label "grzeczny"
  ]
  node [
    id 215
    label "&#347;mieszny"
  ]
  node [
    id 216
    label "odpowiedni"
  ]
  node [
    id 217
    label "zwrot"
  ]
  node [
    id 218
    label "dobrze"
  ]
  node [
    id 219
    label "dobroczynny"
  ]
  node [
    id 220
    label "mi&#322;y"
  ]
  node [
    id 221
    label "taki"
  ]
  node [
    id 222
    label "stosownie"
  ]
  node [
    id 223
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 224
    label "prawdziwy"
  ]
  node [
    id 225
    label "typowy"
  ]
  node [
    id 226
    label "zasadniczy"
  ]
  node [
    id 227
    label "uprawniony"
  ]
  node [
    id 228
    label "nale&#380;yty"
  ]
  node [
    id 229
    label "ten"
  ]
  node [
    id 230
    label "nale&#380;ny"
  ]
  node [
    id 231
    label "po&#380;&#261;dany"
  ]
  node [
    id 232
    label "pomy&#347;lnie"
  ]
  node [
    id 233
    label "sprawny"
  ]
  node [
    id 234
    label "skutkowanie"
  ]
  node [
    id 235
    label "poskutkowanie"
  ]
  node [
    id 236
    label "skutecznie"
  ]
  node [
    id 237
    label "pozytywnie"
  ]
  node [
    id 238
    label "fajny"
  ]
  node [
    id 239
    label "przyjemny"
  ]
  node [
    id 240
    label "dodatnio"
  ]
  node [
    id 241
    label "zajebi&#347;cie"
  ]
  node [
    id 242
    label "wspania&#322;y"
  ]
  node [
    id 243
    label "och&#281;do&#380;nie"
  ]
  node [
    id 244
    label "bogaty"
  ]
  node [
    id 245
    label "kapitalny"
  ]
  node [
    id 246
    label "superancko"
  ]
  node [
    id 247
    label "doskona&#322;y"
  ]
  node [
    id 248
    label "nieustraszony"
  ]
  node [
    id 249
    label "zadzier&#380;ysty"
  ]
  node [
    id 250
    label "ukradzenie"
  ]
  node [
    id 251
    label "pocz&#261;tki"
  ]
  node [
    id 252
    label "idea"
  ]
  node [
    id 253
    label "ukra&#347;&#263;"
  ]
  node [
    id 254
    label "wytw&#243;r"
  ]
  node [
    id 255
    label "system"
  ]
  node [
    id 256
    label "przedmiot"
  ]
  node [
    id 257
    label "rezultat"
  ]
  node [
    id 258
    label "p&#322;&#243;d"
  ]
  node [
    id 259
    label "dzieci&#281;ctwo"
  ]
  node [
    id 260
    label "background"
  ]
  node [
    id 261
    label "strategia"
  ]
  node [
    id 262
    label "zaczerpni&#281;cie"
  ]
  node [
    id 263
    label "zw&#281;dzenie"
  ]
  node [
    id 264
    label "larceny"
  ]
  node [
    id 265
    label "okradzenie"
  ]
  node [
    id 266
    label "przyw&#322;aszczenie"
  ]
  node [
    id 267
    label "nakradzenie"
  ]
  node [
    id 268
    label "podpierdolenie"
  ]
  node [
    id 269
    label "zgini&#281;cie"
  ]
  node [
    id 270
    label "poj&#281;cie"
  ]
  node [
    id 271
    label "istota"
  ]
  node [
    id 272
    label "Kant"
  ]
  node [
    id 273
    label "ideacja"
  ]
  node [
    id 274
    label "byt"
  ]
  node [
    id 275
    label "cel"
  ]
  node [
    id 276
    label "dash_off"
  ]
  node [
    id 277
    label "overcharge"
  ]
  node [
    id 278
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 279
    label "zabra&#263;"
  ]
  node [
    id 280
    label "podpierdoli&#263;"
  ]
  node [
    id 281
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 282
    label "zw&#281;dzi&#263;"
  ]
  node [
    id 283
    label "model"
  ]
  node [
    id 284
    label "systemik"
  ]
  node [
    id 285
    label "Android"
  ]
  node [
    id 286
    label "podsystem"
  ]
  node [
    id 287
    label "systemat"
  ]
  node [
    id 288
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 289
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 290
    label "konstelacja"
  ]
  node [
    id 291
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 292
    label "oprogramowanie"
  ]
  node [
    id 293
    label "j&#261;dro"
  ]
  node [
    id 294
    label "rozprz&#261;c"
  ]
  node [
    id 295
    label "usenet"
  ]
  node [
    id 296
    label "jednostka_geologiczna"
  ]
  node [
    id 297
    label "ryba"
  ]
  node [
    id 298
    label "oddzia&#322;"
  ]
  node [
    id 299
    label "net"
  ]
  node [
    id 300
    label "podstawa"
  ]
  node [
    id 301
    label "metoda"
  ]
  node [
    id 302
    label "method"
  ]
  node [
    id 303
    label "porz&#261;dek"
  ]
  node [
    id 304
    label "struktura"
  ]
  node [
    id 305
    label "spos&#243;b"
  ]
  node [
    id 306
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 307
    label "w&#281;dkarstwo"
  ]
  node [
    id 308
    label "doktryna"
  ]
  node [
    id 309
    label "Leopard"
  ]
  node [
    id 310
    label "zachowanie"
  ]
  node [
    id 311
    label "o&#347;"
  ]
  node [
    id 312
    label "sk&#322;ad"
  ]
  node [
    id 313
    label "pulpit"
  ]
  node [
    id 314
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 315
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 316
    label "cybernetyk"
  ]
  node [
    id 317
    label "przyn&#281;ta"
  ]
  node [
    id 318
    label "s&#261;d"
  ]
  node [
    id 319
    label "eratem"
  ]
  node [
    id 320
    label "Klopsztanga"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
]
