graph [
  node [
    id 0
    label "startowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wprost"
    origin "text"
  ]
  node [
    id 2
    label "pas"
    origin "text"
  ]
  node [
    id 3
    label "skr&#281;t"
    origin "text"
  ]
  node [
    id 4
    label "lewo"
    origin "text"
  ]
  node [
    id 5
    label "spycha&#263;"
    origin "text"
  ]
  node [
    id 6
    label "passat"
    origin "text"
  ]
  node [
    id 7
    label "zaleca&#263;_si&#281;"
  ]
  node [
    id 8
    label "zaczyna&#263;"
  ]
  node [
    id 9
    label "katapultowa&#263;"
  ]
  node [
    id 10
    label "odchodzi&#263;"
  ]
  node [
    id 11
    label "samolot"
  ]
  node [
    id 12
    label "begin"
  ]
  node [
    id 13
    label "uczestniczy&#263;"
  ]
  node [
    id 14
    label "participate"
  ]
  node [
    id 15
    label "robi&#263;"
  ]
  node [
    id 16
    label "by&#263;"
  ]
  node [
    id 17
    label "blend"
  ]
  node [
    id 18
    label "rozgrywa&#263;_si&#281;"
  ]
  node [
    id 19
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 20
    label "opuszcza&#263;"
  ]
  node [
    id 21
    label "impart"
  ]
  node [
    id 22
    label "wyrusza&#263;"
  ]
  node [
    id 23
    label "odrzut"
  ]
  node [
    id 24
    label "go"
  ]
  node [
    id 25
    label "seclude"
  ]
  node [
    id 26
    label "gasn&#261;&#263;"
  ]
  node [
    id 27
    label "przestawa&#263;"
  ]
  node [
    id 28
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 29
    label "odstawa&#263;"
  ]
  node [
    id 30
    label "rezygnowa&#263;"
  ]
  node [
    id 31
    label "i&#347;&#263;"
  ]
  node [
    id 32
    label "mija&#263;"
  ]
  node [
    id 33
    label "proceed"
  ]
  node [
    id 34
    label "odejmowa&#263;"
  ]
  node [
    id 35
    label "mie&#263;_miejsce"
  ]
  node [
    id 36
    label "bankrupt"
  ]
  node [
    id 37
    label "open"
  ]
  node [
    id 38
    label "set_about"
  ]
  node [
    id 39
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 40
    label "post&#281;powa&#263;"
  ]
  node [
    id 41
    label "wyrzuca&#263;"
  ]
  node [
    id 42
    label "wyrzuci&#263;"
  ]
  node [
    id 43
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 44
    label "wylatywa&#263;"
  ]
  node [
    id 45
    label "awaria"
  ]
  node [
    id 46
    label "spalin&#243;wka"
  ]
  node [
    id 47
    label "katapulta"
  ]
  node [
    id 48
    label "pilot_automatyczny"
  ]
  node [
    id 49
    label "kad&#322;ub"
  ]
  node [
    id 50
    label "wiatrochron"
  ]
  node [
    id 51
    label "kabina"
  ]
  node [
    id 52
    label "wylatywanie"
  ]
  node [
    id 53
    label "kapotowanie"
  ]
  node [
    id 54
    label "kapotowa&#263;"
  ]
  node [
    id 55
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 56
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 57
    label "skrzyd&#322;o"
  ]
  node [
    id 58
    label "pok&#322;ad"
  ]
  node [
    id 59
    label "kapota&#380;"
  ]
  node [
    id 60
    label "sta&#322;op&#322;at"
  ]
  node [
    id 61
    label "sterownica"
  ]
  node [
    id 62
    label "p&#322;atowiec"
  ]
  node [
    id 63
    label "wylecenie"
  ]
  node [
    id 64
    label "wylecie&#263;"
  ]
  node [
    id 65
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 66
    label "gondola"
  ]
  node [
    id 67
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 68
    label "dzi&#243;b"
  ]
  node [
    id 69
    label "inhalator_tlenowy"
  ]
  node [
    id 70
    label "kapot"
  ]
  node [
    id 71
    label "kabinka"
  ]
  node [
    id 72
    label "&#380;yroskop"
  ]
  node [
    id 73
    label "czarna_skrzynka"
  ]
  node [
    id 74
    label "lecenie"
  ]
  node [
    id 75
    label "fotel_lotniczy"
  ]
  node [
    id 76
    label "wy&#347;lizg"
  ]
  node [
    id 77
    label "prosto"
  ]
  node [
    id 78
    label "otwarcie"
  ]
  node [
    id 79
    label "naprzeciwko"
  ]
  node [
    id 80
    label "po_przeciwnej_stronie"
  ]
  node [
    id 81
    label "z_naprzeciwka"
  ]
  node [
    id 82
    label "jawno"
  ]
  node [
    id 83
    label "rozpocz&#281;cie"
  ]
  node [
    id 84
    label "udost&#281;pnienie"
  ]
  node [
    id 85
    label "publicznie"
  ]
  node [
    id 86
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 87
    label "ewidentnie"
  ]
  node [
    id 88
    label "jawnie"
  ]
  node [
    id 89
    label "opening"
  ]
  node [
    id 90
    label "jawny"
  ]
  node [
    id 91
    label "otwarty"
  ]
  node [
    id 92
    label "gra&#263;"
  ]
  node [
    id 93
    label "czynno&#347;&#263;"
  ]
  node [
    id 94
    label "bezpo&#347;rednio"
  ]
  node [
    id 95
    label "zdecydowanie"
  ]
  node [
    id 96
    label "granie"
  ]
  node [
    id 97
    label "&#322;atwo"
  ]
  node [
    id 98
    label "prosty"
  ]
  node [
    id 99
    label "skromnie"
  ]
  node [
    id 100
    label "elementarily"
  ]
  node [
    id 101
    label "niepozornie"
  ]
  node [
    id 102
    label "naturalnie"
  ]
  node [
    id 103
    label "dodatek"
  ]
  node [
    id 104
    label "linia"
  ]
  node [
    id 105
    label "licytacja"
  ]
  node [
    id 106
    label "kawa&#322;ek"
  ]
  node [
    id 107
    label "figura_heraldyczna"
  ]
  node [
    id 108
    label "wci&#281;cie"
  ]
  node [
    id 109
    label "obszar"
  ]
  node [
    id 110
    label "bielizna"
  ]
  node [
    id 111
    label "miejsce"
  ]
  node [
    id 112
    label "sk&#322;ad"
  ]
  node [
    id 113
    label "obiekt"
  ]
  node [
    id 114
    label "zagranie"
  ]
  node [
    id 115
    label "heraldyka"
  ]
  node [
    id 116
    label "odznaka"
  ]
  node [
    id 117
    label "tarcza_herbowa"
  ]
  node [
    id 118
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 119
    label "nap&#281;d"
  ]
  node [
    id 120
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 121
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 122
    label "indentation"
  ]
  node [
    id 123
    label "zjedzenie"
  ]
  node [
    id 124
    label "snub"
  ]
  node [
    id 125
    label "warunek_lokalowy"
  ]
  node [
    id 126
    label "plac"
  ]
  node [
    id 127
    label "location"
  ]
  node [
    id 128
    label "uwaga"
  ]
  node [
    id 129
    label "przestrze&#324;"
  ]
  node [
    id 130
    label "status"
  ]
  node [
    id 131
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 132
    label "chwila"
  ]
  node [
    id 133
    label "cia&#322;o"
  ]
  node [
    id 134
    label "cecha"
  ]
  node [
    id 135
    label "praca"
  ]
  node [
    id 136
    label "rz&#261;d"
  ]
  node [
    id 137
    label "dochodzenie"
  ]
  node [
    id 138
    label "przedmiot"
  ]
  node [
    id 139
    label "doj&#347;cie"
  ]
  node [
    id 140
    label "doch&#243;d"
  ]
  node [
    id 141
    label "dziennik"
  ]
  node [
    id 142
    label "element"
  ]
  node [
    id 143
    label "rzecz"
  ]
  node [
    id 144
    label "galanteria"
  ]
  node [
    id 145
    label "doj&#347;&#263;"
  ]
  node [
    id 146
    label "aneks"
  ]
  node [
    id 147
    label "p&#243;&#322;noc"
  ]
  node [
    id 148
    label "Kosowo"
  ]
  node [
    id 149
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 150
    label "Zab&#322;ocie"
  ]
  node [
    id 151
    label "zach&#243;d"
  ]
  node [
    id 152
    label "po&#322;udnie"
  ]
  node [
    id 153
    label "Pow&#261;zki"
  ]
  node [
    id 154
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 155
    label "Piotrowo"
  ]
  node [
    id 156
    label "Olszanica"
  ]
  node [
    id 157
    label "zbi&#243;r"
  ]
  node [
    id 158
    label "Ruda_Pabianicka"
  ]
  node [
    id 159
    label "holarktyka"
  ]
  node [
    id 160
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 161
    label "Ludwin&#243;w"
  ]
  node [
    id 162
    label "Arktyka"
  ]
  node [
    id 163
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 164
    label "Zabu&#380;e"
  ]
  node [
    id 165
    label "antroposfera"
  ]
  node [
    id 166
    label "Neogea"
  ]
  node [
    id 167
    label "terytorium"
  ]
  node [
    id 168
    label "Syberia_Zachodnia"
  ]
  node [
    id 169
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 170
    label "zakres"
  ]
  node [
    id 171
    label "pas_planetoid"
  ]
  node [
    id 172
    label "Syberia_Wschodnia"
  ]
  node [
    id 173
    label "Antarktyka"
  ]
  node [
    id 174
    label "Rakowice"
  ]
  node [
    id 175
    label "akrecja"
  ]
  node [
    id 176
    label "wymiar"
  ]
  node [
    id 177
    label "&#321;&#281;g"
  ]
  node [
    id 178
    label "Kresy_Zachodnie"
  ]
  node [
    id 179
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 180
    label "wsch&#243;d"
  ]
  node [
    id 181
    label "Notogea"
  ]
  node [
    id 182
    label "Rzym_Zachodni"
  ]
  node [
    id 183
    label "whole"
  ]
  node [
    id 184
    label "ilo&#347;&#263;"
  ]
  node [
    id 185
    label "Rzym_Wschodni"
  ]
  node [
    id 186
    label "urz&#261;dzenie"
  ]
  node [
    id 187
    label "kszta&#322;t"
  ]
  node [
    id 188
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 189
    label "armia"
  ]
  node [
    id 190
    label "cz&#322;owiek"
  ]
  node [
    id 191
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 192
    label "poprowadzi&#263;"
  ]
  node [
    id 193
    label "cord"
  ]
  node [
    id 194
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 195
    label "trasa"
  ]
  node [
    id 196
    label "po&#322;&#261;czenie"
  ]
  node [
    id 197
    label "tract"
  ]
  node [
    id 198
    label "materia&#322;_zecerski"
  ]
  node [
    id 199
    label "przeorientowywanie"
  ]
  node [
    id 200
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 201
    label "curve"
  ]
  node [
    id 202
    label "figura_geometryczna"
  ]
  node [
    id 203
    label "wygl&#261;d"
  ]
  node [
    id 204
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 205
    label "jard"
  ]
  node [
    id 206
    label "szczep"
  ]
  node [
    id 207
    label "phreaker"
  ]
  node [
    id 208
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 209
    label "grupa_organizm&#243;w"
  ]
  node [
    id 210
    label "prowadzi&#263;"
  ]
  node [
    id 211
    label "przeorientowywa&#263;"
  ]
  node [
    id 212
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 213
    label "access"
  ]
  node [
    id 214
    label "przeorientowanie"
  ]
  node [
    id 215
    label "przeorientowa&#263;"
  ]
  node [
    id 216
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 217
    label "billing"
  ]
  node [
    id 218
    label "granica"
  ]
  node [
    id 219
    label "szpaler"
  ]
  node [
    id 220
    label "sztrych"
  ]
  node [
    id 221
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 222
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 223
    label "drzewo_genealogiczne"
  ]
  node [
    id 224
    label "transporter"
  ]
  node [
    id 225
    label "line"
  ]
  node [
    id 226
    label "fragment"
  ]
  node [
    id 227
    label "kompleksja"
  ]
  node [
    id 228
    label "przew&#243;d"
  ]
  node [
    id 229
    label "budowa"
  ]
  node [
    id 230
    label "granice"
  ]
  node [
    id 231
    label "kontakt"
  ]
  node [
    id 232
    label "przewo&#378;nik"
  ]
  node [
    id 233
    label "przystanek"
  ]
  node [
    id 234
    label "linijka"
  ]
  node [
    id 235
    label "spos&#243;b"
  ]
  node [
    id 236
    label "uporz&#261;dkowanie"
  ]
  node [
    id 237
    label "coalescence"
  ]
  node [
    id 238
    label "Ural"
  ]
  node [
    id 239
    label "point"
  ]
  node [
    id 240
    label "bearing"
  ]
  node [
    id 241
    label "prowadzenie"
  ]
  node [
    id 242
    label "tekst"
  ]
  node [
    id 243
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 244
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 245
    label "koniec"
  ]
  node [
    id 246
    label "signal"
  ]
  node [
    id 247
    label "oznaka"
  ]
  node [
    id 248
    label "odznaczenie"
  ]
  node [
    id 249
    label "marker"
  ]
  node [
    id 250
    label "znaczek"
  ]
  node [
    id 251
    label "kawa&#322;"
  ]
  node [
    id 252
    label "plot"
  ]
  node [
    id 253
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 254
    label "utw&#243;r"
  ]
  node [
    id 255
    label "piece"
  ]
  node [
    id 256
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 257
    label "podp&#322;ywanie"
  ]
  node [
    id 258
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 259
    label "move"
  ]
  node [
    id 260
    label "zawa&#380;enie"
  ]
  node [
    id 261
    label "za&#347;wiecenie"
  ]
  node [
    id 262
    label "zaszczekanie"
  ]
  node [
    id 263
    label "myk"
  ]
  node [
    id 264
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 265
    label "wykonanie"
  ]
  node [
    id 266
    label "rozegranie"
  ]
  node [
    id 267
    label "travel"
  ]
  node [
    id 268
    label "instrument_muzyczny"
  ]
  node [
    id 269
    label "gra"
  ]
  node [
    id 270
    label "uzewn&#281;trznienie_si&#281;"
  ]
  node [
    id 271
    label "gra_w_karty"
  ]
  node [
    id 272
    label "maneuver"
  ]
  node [
    id 273
    label "rozgrywka"
  ]
  node [
    id 274
    label "accident"
  ]
  node [
    id 275
    label "gambit"
  ]
  node [
    id 276
    label "zabrzmienie"
  ]
  node [
    id 277
    label "zachowanie_si&#281;"
  ]
  node [
    id 278
    label "manewr"
  ]
  node [
    id 279
    label "wyst&#261;pienie"
  ]
  node [
    id 280
    label "posuni&#281;cie"
  ]
  node [
    id 281
    label "udanie_si&#281;"
  ]
  node [
    id 282
    label "zacz&#281;cie"
  ]
  node [
    id 283
    label "rola"
  ]
  node [
    id 284
    label "zrobienie"
  ]
  node [
    id 285
    label "co&#347;"
  ]
  node [
    id 286
    label "budynek"
  ]
  node [
    id 287
    label "thing"
  ]
  node [
    id 288
    label "poj&#281;cie"
  ]
  node [
    id 289
    label "program"
  ]
  node [
    id 290
    label "strona"
  ]
  node [
    id 291
    label "str&#243;j"
  ]
  node [
    id 292
    label "energia"
  ]
  node [
    id 293
    label "most"
  ]
  node [
    id 294
    label "propulsion"
  ]
  node [
    id 295
    label "przetarg"
  ]
  node [
    id 296
    label "rozdanie"
  ]
  node [
    id 297
    label "faza"
  ]
  node [
    id 298
    label "sprzeda&#380;"
  ]
  node [
    id 299
    label "bryd&#380;"
  ]
  node [
    id 300
    label "tysi&#261;c"
  ]
  node [
    id 301
    label "skat"
  ]
  node [
    id 302
    label "oksza"
  ]
  node [
    id 303
    label "s&#322;up"
  ]
  node [
    id 304
    label "historia"
  ]
  node [
    id 305
    label "barwa_heraldyczna"
  ]
  node [
    id 306
    label "herb"
  ]
  node [
    id 307
    label "or&#281;&#380;"
  ]
  node [
    id 308
    label "zesp&#243;&#322;"
  ]
  node [
    id 309
    label "blokada"
  ]
  node [
    id 310
    label "hurtownia"
  ]
  node [
    id 311
    label "pomieszczenie"
  ]
  node [
    id 312
    label "struktura"
  ]
  node [
    id 313
    label "pole"
  ]
  node [
    id 314
    label "basic"
  ]
  node [
    id 315
    label "sk&#322;adnik"
  ]
  node [
    id 316
    label "sklep"
  ]
  node [
    id 317
    label "obr&#243;bka"
  ]
  node [
    id 318
    label "constitution"
  ]
  node [
    id 319
    label "fabryka"
  ]
  node [
    id 320
    label "&#347;wiat&#322;o"
  ]
  node [
    id 321
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 322
    label "syf"
  ]
  node [
    id 323
    label "rank_and_file"
  ]
  node [
    id 324
    label "set"
  ]
  node [
    id 325
    label "tabulacja"
  ]
  node [
    id 326
    label "whirl"
  ]
  node [
    id 327
    label "papieros"
  ]
  node [
    id 328
    label "nadsterowno&#347;&#263;"
  ]
  node [
    id 329
    label "odcinek"
  ]
  node [
    id 330
    label "serpentyna"
  ]
  node [
    id 331
    label "ruch"
  ]
  node [
    id 332
    label "podsterowno&#347;&#263;"
  ]
  node [
    id 333
    label "trafika"
  ]
  node [
    id 334
    label "odpali&#263;"
  ]
  node [
    id 335
    label "zapalenie"
  ]
  node [
    id 336
    label "u&#380;ywka"
  ]
  node [
    id 337
    label "wyr&#243;b_tytoniowy"
  ]
  node [
    id 338
    label "pali&#263;"
  ]
  node [
    id 339
    label "smoke"
  ]
  node [
    id 340
    label "dulec"
  ]
  node [
    id 341
    label "filtr"
  ]
  node [
    id 342
    label "szlug"
  ]
  node [
    id 343
    label "palenie"
  ]
  node [
    id 344
    label "bibu&#322;ka"
  ]
  node [
    id 345
    label "formacja"
  ]
  node [
    id 346
    label "punkt_widzenia"
  ]
  node [
    id 347
    label "g&#322;owa"
  ]
  node [
    id 348
    label "spirala"
  ]
  node [
    id 349
    label "p&#322;at"
  ]
  node [
    id 350
    label "comeliness"
  ]
  node [
    id 351
    label "kielich"
  ]
  node [
    id 352
    label "face"
  ]
  node [
    id 353
    label "blaszka"
  ]
  node [
    id 354
    label "charakter"
  ]
  node [
    id 355
    label "p&#281;tla"
  ]
  node [
    id 356
    label "pasmo"
  ]
  node [
    id 357
    label "linearno&#347;&#263;"
  ]
  node [
    id 358
    label "gwiazda"
  ]
  node [
    id 359
    label "miniatura"
  ]
  node [
    id 360
    label "mechanika"
  ]
  node [
    id 361
    label "utrzymywanie"
  ]
  node [
    id 362
    label "poruszenie"
  ]
  node [
    id 363
    label "movement"
  ]
  node [
    id 364
    label "utrzyma&#263;"
  ]
  node [
    id 365
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 366
    label "zjawisko"
  ]
  node [
    id 367
    label "utrzymanie"
  ]
  node [
    id 368
    label "kanciasty"
  ]
  node [
    id 369
    label "commercial_enterprise"
  ]
  node [
    id 370
    label "model"
  ]
  node [
    id 371
    label "strumie&#324;"
  ]
  node [
    id 372
    label "proces"
  ]
  node [
    id 373
    label "aktywno&#347;&#263;"
  ]
  node [
    id 374
    label "kr&#243;tki"
  ]
  node [
    id 375
    label "taktyka"
  ]
  node [
    id 376
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 377
    label "apraksja"
  ]
  node [
    id 378
    label "natural_process"
  ]
  node [
    id 379
    label "utrzymywa&#263;"
  ]
  node [
    id 380
    label "d&#322;ugi"
  ]
  node [
    id 381
    label "wydarzenie"
  ]
  node [
    id 382
    label "dyssypacja_energii"
  ]
  node [
    id 383
    label "tumult"
  ]
  node [
    id 384
    label "stopek"
  ]
  node [
    id 385
    label "zmiana"
  ]
  node [
    id 386
    label "lokomocja"
  ]
  node [
    id 387
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 388
    label "komunikacja"
  ]
  node [
    id 389
    label "drift"
  ]
  node [
    id 390
    label "teren"
  ]
  node [
    id 391
    label "part"
  ]
  node [
    id 392
    label "coupon"
  ]
  node [
    id 393
    label "pokwitowanie"
  ]
  node [
    id 394
    label "moneta"
  ]
  node [
    id 395
    label "epizod"
  ]
  node [
    id 396
    label "droga"
  ]
  node [
    id 397
    label "ta&#347;ma"
  ]
  node [
    id 398
    label "zakr&#281;t"
  ]
  node [
    id 399
    label "ozdoba"
  ]
  node [
    id 400
    label "przebieg"
  ]
  node [
    id 401
    label "infrastruktura"
  ]
  node [
    id 402
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 403
    label "w&#281;ze&#322;"
  ]
  node [
    id 404
    label "marszrutyzacja"
  ]
  node [
    id 405
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 406
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 407
    label "podbieg"
  ]
  node [
    id 408
    label "op&#243;&#378;nienie"
  ]
  node [
    id 409
    label "podejrzanie"
  ]
  node [
    id 410
    label "lewy"
  ]
  node [
    id 411
    label "szko&#322;a"
  ]
  node [
    id 412
    label "kierunek"
  ]
  node [
    id 413
    label "nielegalnie"
  ]
  node [
    id 414
    label "kiepsko"
  ]
  node [
    id 415
    label "praktyka"
  ]
  node [
    id 416
    label "system"
  ]
  node [
    id 417
    label "studia"
  ]
  node [
    id 418
    label "bok"
  ]
  node [
    id 419
    label "skr&#281;canie"
  ]
  node [
    id 420
    label "skr&#281;ca&#263;"
  ]
  node [
    id 421
    label "orientowanie"
  ]
  node [
    id 422
    label "skr&#281;ci&#263;"
  ]
  node [
    id 423
    label "zorientowanie"
  ]
  node [
    id 424
    label "metoda"
  ]
  node [
    id 425
    label "ty&#322;"
  ]
  node [
    id 426
    label "zorientowa&#263;"
  ]
  node [
    id 427
    label "g&#243;ra"
  ]
  node [
    id 428
    label "orientowa&#263;"
  ]
  node [
    id 429
    label "ideologia"
  ]
  node [
    id 430
    label "orientacja"
  ]
  node [
    id 431
    label "prz&#243;d"
  ]
  node [
    id 432
    label "skr&#281;cenie"
  ]
  node [
    id 433
    label "choro"
  ]
  node [
    id 434
    label "marnie"
  ]
  node [
    id 435
    label "kiepski"
  ]
  node [
    id 436
    label "marny"
  ]
  node [
    id 437
    label "&#378;le"
  ]
  node [
    id 438
    label "unlawfully"
  ]
  node [
    id 439
    label "nieoficjalnie"
  ]
  node [
    id 440
    label "nielegalny"
  ]
  node [
    id 441
    label "na_nielegalu"
  ]
  node [
    id 442
    label "niepewnie"
  ]
  node [
    id 443
    label "ciemny"
  ]
  node [
    id 444
    label "nieprzejrzy&#347;cie"
  ]
  node [
    id 445
    label "do&#347;wiadczenie"
  ]
  node [
    id 446
    label "teren_szko&#322;y"
  ]
  node [
    id 447
    label "wiedza"
  ]
  node [
    id 448
    label "Mickiewicz"
  ]
  node [
    id 449
    label "kwalifikacje"
  ]
  node [
    id 450
    label "podr&#281;cznik"
  ]
  node [
    id 451
    label "absolwent"
  ]
  node [
    id 452
    label "school"
  ]
  node [
    id 453
    label "zda&#263;"
  ]
  node [
    id 454
    label "gabinet"
  ]
  node [
    id 455
    label "urszulanki"
  ]
  node [
    id 456
    label "sztuba"
  ]
  node [
    id 457
    label "&#322;awa_szkolna"
  ]
  node [
    id 458
    label "nauka"
  ]
  node [
    id 459
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 460
    label "przepisa&#263;"
  ]
  node [
    id 461
    label "muzyka"
  ]
  node [
    id 462
    label "grupa"
  ]
  node [
    id 463
    label "form"
  ]
  node [
    id 464
    label "klasa"
  ]
  node [
    id 465
    label "lekcja"
  ]
  node [
    id 466
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 467
    label "przepisanie"
  ]
  node [
    id 468
    label "czas"
  ]
  node [
    id 469
    label "skolaryzacja"
  ]
  node [
    id 470
    label "zdanie"
  ]
  node [
    id 471
    label "sekretariat"
  ]
  node [
    id 472
    label "lesson"
  ]
  node [
    id 473
    label "instytucja"
  ]
  node [
    id 474
    label "niepokalanki"
  ]
  node [
    id 475
    label "siedziba"
  ]
  node [
    id 476
    label "szkolenie"
  ]
  node [
    id 477
    label "kara"
  ]
  node [
    id 478
    label "tablica"
  ]
  node [
    id 479
    label "wewn&#281;trzny"
  ]
  node [
    id 480
    label "w_lewo"
  ]
  node [
    id 481
    label "na_czarno"
  ]
  node [
    id 482
    label "szemrany"
  ]
  node [
    id 483
    label "na_lewo"
  ]
  node [
    id 484
    label "lewicowy"
  ]
  node [
    id 485
    label "z_lewa"
  ]
  node [
    id 486
    label "zabiera&#263;"
  ]
  node [
    id 487
    label "zmusza&#263;"
  ]
  node [
    id 488
    label "give"
  ]
  node [
    id 489
    label "spychologia"
  ]
  node [
    id 490
    label "przemieszcza&#263;"
  ]
  node [
    id 491
    label "oskar&#380;a&#263;"
  ]
  node [
    id 492
    label "obarcza&#263;"
  ]
  node [
    id 493
    label "force"
  ]
  node [
    id 494
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 495
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 496
    label "sandbag"
  ]
  node [
    id 497
    label "powodowa&#263;"
  ]
  node [
    id 498
    label "obci&#261;&#380;a&#263;"
  ]
  node [
    id 499
    label "charge"
  ]
  node [
    id 500
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 501
    label "zajmowa&#263;"
  ]
  node [
    id 502
    label "poci&#261;ga&#263;"
  ]
  node [
    id 503
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 504
    label "fall"
  ]
  node [
    id 505
    label "liszy&#263;"
  ]
  node [
    id 506
    label "&#322;apa&#263;"
  ]
  node [
    id 507
    label "przesuwa&#263;"
  ]
  node [
    id 508
    label "blurt_out"
  ]
  node [
    id 509
    label "konfiskowa&#263;"
  ]
  node [
    id 510
    label "deprive"
  ]
  node [
    id 511
    label "abstract"
  ]
  node [
    id 512
    label "przenosi&#263;"
  ]
  node [
    id 513
    label "translokowa&#263;"
  ]
  node [
    id 514
    label "prosecute"
  ]
  node [
    id 515
    label "twierdzi&#263;"
  ]
  node [
    id 516
    label "zasada"
  ]
  node [
    id 517
    label "zwala&#263;"
  ]
  node [
    id 518
    label "fastback"
  ]
  node [
    id 519
    label "volkswagen"
  ]
  node [
    id 520
    label "samoch&#243;d"
  ]
  node [
    id 521
    label "Passat"
  ]
  node [
    id 522
    label "Volkswagen"
  ]
  node [
    id 523
    label "nadwozie"
  ]
  node [
    id 524
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 525
    label "pojazd_drogowy"
  ]
  node [
    id 526
    label "spryskiwacz"
  ]
  node [
    id 527
    label "baga&#380;nik"
  ]
  node [
    id 528
    label "silnik"
  ]
  node [
    id 529
    label "dachowanie"
  ]
  node [
    id 530
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 531
    label "pompa_wodna"
  ]
  node [
    id 532
    label "poduszka_powietrzna"
  ]
  node [
    id 533
    label "tempomat"
  ]
  node [
    id 534
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 535
    label "deska_rozdzielcza"
  ]
  node [
    id 536
    label "immobilizer"
  ]
  node [
    id 537
    label "t&#322;umik"
  ]
  node [
    id 538
    label "ABS"
  ]
  node [
    id 539
    label "kierownica"
  ]
  node [
    id 540
    label "bak"
  ]
  node [
    id 541
    label "dwu&#347;lad"
  ]
  node [
    id 542
    label "poci&#261;g_drogowy"
  ]
  node [
    id 543
    label "wycieraczka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
]
