graph [
  node [
    id 0
    label "angielski"
    origin "text"
  ]
  node [
    id 1
    label "francuski"
    origin "text"
  ]
  node [
    id 2
    label "rosyjski"
    origin "text"
  ]
  node [
    id 3
    label "hiszpa&#324;ski"
    origin "text"
  ]
  node [
    id 4
    label "polski"
    origin "text"
  ]
  node [
    id 5
    label "English"
  ]
  node [
    id 6
    label "brytyjski"
  ]
  node [
    id 7
    label "po_angielsku"
  ]
  node [
    id 8
    label "angielsko"
  ]
  node [
    id 9
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 10
    label "j&#281;zyki_angielskie"
  ]
  node [
    id 11
    label "anglicki"
  ]
  node [
    id 12
    label "j&#281;zyk"
  ]
  node [
    id 13
    label "angol"
  ]
  node [
    id 14
    label "europejsko"
  ]
  node [
    id 15
    label "po_brytyjsku"
  ]
  node [
    id 16
    label "anglosaski"
  ]
  node [
    id 17
    label "europejski"
  ]
  node [
    id 18
    label "morris"
  ]
  node [
    id 19
    label "j&#281;zyk_martwy"
  ]
  node [
    id 20
    label "brytyjsko"
  ]
  node [
    id 21
    label "j&#281;zyk_celtycki"
  ]
  node [
    id 22
    label "zachodnioeuropejski"
  ]
  node [
    id 23
    label "j&#281;zyk_angielski"
  ]
  node [
    id 24
    label "pisa&#263;"
  ]
  node [
    id 25
    label "kod"
  ]
  node [
    id 26
    label "pype&#263;"
  ]
  node [
    id 27
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 28
    label "gramatyka"
  ]
  node [
    id 29
    label "language"
  ]
  node [
    id 30
    label "fonetyka"
  ]
  node [
    id 31
    label "t&#322;umaczenie"
  ]
  node [
    id 32
    label "artykulator"
  ]
  node [
    id 33
    label "rozumienie"
  ]
  node [
    id 34
    label "jama_ustna"
  ]
  node [
    id 35
    label "urz&#261;dzenie"
  ]
  node [
    id 36
    label "organ"
  ]
  node [
    id 37
    label "ssanie"
  ]
  node [
    id 38
    label "lizanie"
  ]
  node [
    id 39
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 40
    label "przedmiot"
  ]
  node [
    id 41
    label "liza&#263;"
  ]
  node [
    id 42
    label "makroglosja"
  ]
  node [
    id 43
    label "natural_language"
  ]
  node [
    id 44
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 45
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 46
    label "napisa&#263;"
  ]
  node [
    id 47
    label "m&#243;wienie"
  ]
  node [
    id 48
    label "s&#322;ownictwo"
  ]
  node [
    id 49
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 50
    label "konsonantyzm"
  ]
  node [
    id 51
    label "ssa&#263;"
  ]
  node [
    id 52
    label "wokalizm"
  ]
  node [
    id 53
    label "kultura_duchowa"
  ]
  node [
    id 54
    label "formalizowanie"
  ]
  node [
    id 55
    label "jeniec"
  ]
  node [
    id 56
    label "m&#243;wi&#263;"
  ]
  node [
    id 57
    label "kawa&#322;ek"
  ]
  node [
    id 58
    label "po_koroniarsku"
  ]
  node [
    id 59
    label "rozumie&#263;"
  ]
  node [
    id 60
    label "stylik"
  ]
  node [
    id 61
    label "przet&#322;umaczenie"
  ]
  node [
    id 62
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 63
    label "formacja_geologiczna"
  ]
  node [
    id 64
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 65
    label "spos&#243;b"
  ]
  node [
    id 66
    label "but"
  ]
  node [
    id 67
    label "pismo"
  ]
  node [
    id 68
    label "formalizowa&#263;"
  ]
  node [
    id 69
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 70
    label "farandola"
  ]
  node [
    id 71
    label "francuz"
  ]
  node [
    id 72
    label "nami&#281;tny"
  ]
  node [
    id 73
    label "po_francusku"
  ]
  node [
    id 74
    label "frankofonia"
  ]
  node [
    id 75
    label "bourr&#233;e"
  ]
  node [
    id 76
    label "verlan"
  ]
  node [
    id 77
    label "chrancuski"
  ]
  node [
    id 78
    label "French"
  ]
  node [
    id 79
    label "j&#281;zyk_roma&#324;ski"
  ]
  node [
    id 80
    label "kurant"
  ]
  node [
    id 81
    label "menuet"
  ]
  node [
    id 82
    label "zapami&#281;ta&#322;y"
  ]
  node [
    id 83
    label "zmys&#322;owy"
  ]
  node [
    id 84
    label "nami&#281;tnie"
  ]
  node [
    id 85
    label "czu&#322;y"
  ]
  node [
    id 86
    label "&#380;ywy"
  ]
  node [
    id 87
    label "gor&#261;cy"
  ]
  node [
    id 88
    label "g&#322;&#281;boki"
  ]
  node [
    id 89
    label "ciep&#322;y"
  ]
  node [
    id 90
    label "gorliwy"
  ]
  node [
    id 91
    label "kusz&#261;cy"
  ]
  node [
    id 92
    label "moreska"
  ]
  node [
    id 93
    label "zachodni"
  ]
  node [
    id 94
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 95
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 96
    label "typowy"
  ]
  node [
    id 97
    label "charakterystyczny"
  ]
  node [
    id 98
    label "po_europejsku"
  ]
  node [
    id 99
    label "European"
  ]
  node [
    id 100
    label "taniec"
  ]
  node [
    id 101
    label "taniec_dworski"
  ]
  node [
    id 102
    label "melodia"
  ]
  node [
    id 103
    label "taniec_ludowy"
  ]
  node [
    id 104
    label "zegar"
  ]
  node [
    id 105
    label "system_monetarny"
  ]
  node [
    id 106
    label "wsp&#243;lnota"
  ]
  node [
    id 107
    label "po_chrancusku"
  ]
  node [
    id 108
    label "&#347;l&#261;ski"
  ]
  node [
    id 109
    label "klucz_nastawny"
  ]
  node [
    id 110
    label "warkocz"
  ]
  node [
    id 111
    label "bu&#322;ka_paryska"
  ]
  node [
    id 112
    label "inwersja"
  ]
  node [
    id 113
    label "slang"
  ]
  node [
    id 114
    label "Russian"
  ]
  node [
    id 115
    label "rusek"
  ]
  node [
    id 116
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 117
    label "wielkoruski"
  ]
  node [
    id 118
    label "po_rosyjsku"
  ]
  node [
    id 119
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 120
    label "kacapski"
  ]
  node [
    id 121
    label "wschodnioeuropejski"
  ]
  node [
    id 122
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 123
    label "topielec"
  ]
  node [
    id 124
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 125
    label "poga&#324;ski"
  ]
  node [
    id 126
    label "j&#281;zyk_rosyjski"
  ]
  node [
    id 127
    label "po_kacapsku"
  ]
  node [
    id 128
    label "po_wielkorusku"
  ]
  node [
    id 129
    label "ruski"
  ]
  node [
    id 130
    label "imperialny"
  ]
  node [
    id 131
    label "po&#322;udniowoeuropejski"
  ]
  node [
    id 132
    label "hispanoj&#281;zyczny"
  ]
  node [
    id 133
    label "hispanistyka"
  ]
  node [
    id 134
    label "fandango"
  ]
  node [
    id 135
    label "paso_doble"
  ]
  node [
    id 136
    label "&#347;r&#243;dziemnomorski"
  ]
  node [
    id 137
    label "hiszpan"
  ]
  node [
    id 138
    label "pawana"
  ]
  node [
    id 139
    label "sarabanda"
  ]
  node [
    id 140
    label "po_hiszpa&#324;sku"
  ]
  node [
    id 141
    label "Spanish"
  ]
  node [
    id 142
    label "po&#322;udniowy"
  ]
  node [
    id 143
    label "&#347;r&#243;dziemnomorsko"
  ]
  node [
    id 144
    label "po_&#347;r&#243;dziemnomorsku"
  ]
  node [
    id 145
    label "filologia"
  ]
  node [
    id 146
    label "iberystyka"
  ]
  node [
    id 147
    label "flamenco"
  ]
  node [
    id 148
    label "partita"
  ]
  node [
    id 149
    label "utw&#243;r"
  ]
  node [
    id 150
    label "wariacja"
  ]
  node [
    id 151
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 152
    label "pierogi_ruskie"
  ]
  node [
    id 153
    label "oberek"
  ]
  node [
    id 154
    label "po_polsku"
  ]
  node [
    id 155
    label "goniony"
  ]
  node [
    id 156
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 157
    label "polsko"
  ]
  node [
    id 158
    label "Polish"
  ]
  node [
    id 159
    label "mazur"
  ]
  node [
    id 160
    label "skoczny"
  ]
  node [
    id 161
    label "chodzony"
  ]
  node [
    id 162
    label "krakowiak"
  ]
  node [
    id 163
    label "ryba_po_grecku"
  ]
  node [
    id 164
    label "polak"
  ]
  node [
    id 165
    label "lacki"
  ]
  node [
    id 166
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 167
    label "sztajer"
  ]
  node [
    id 168
    label "drabant"
  ]
  node [
    id 169
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 170
    label "langosz"
  ]
  node [
    id 171
    label "discipline"
  ]
  node [
    id 172
    label "zboczy&#263;"
  ]
  node [
    id 173
    label "w&#261;tek"
  ]
  node [
    id 174
    label "kultura"
  ]
  node [
    id 175
    label "entity"
  ]
  node [
    id 176
    label "sponiewiera&#263;"
  ]
  node [
    id 177
    label "zboczenie"
  ]
  node [
    id 178
    label "zbaczanie"
  ]
  node [
    id 179
    label "charakter"
  ]
  node [
    id 180
    label "thing"
  ]
  node [
    id 181
    label "om&#243;wi&#263;"
  ]
  node [
    id 182
    label "tre&#347;&#263;"
  ]
  node [
    id 183
    label "element"
  ]
  node [
    id 184
    label "kr&#261;&#380;enie"
  ]
  node [
    id 185
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 186
    label "istota"
  ]
  node [
    id 187
    label "zbacza&#263;"
  ]
  node [
    id 188
    label "om&#243;wienie"
  ]
  node [
    id 189
    label "rzecz"
  ]
  node [
    id 190
    label "tematyka"
  ]
  node [
    id 191
    label "omawianie"
  ]
  node [
    id 192
    label "omawia&#263;"
  ]
  node [
    id 193
    label "robienie"
  ]
  node [
    id 194
    label "program_nauczania"
  ]
  node [
    id 195
    label "sponiewieranie"
  ]
  node [
    id 196
    label "gwardzista"
  ]
  node [
    id 197
    label "&#347;redniowieczny"
  ]
  node [
    id 198
    label "rytmiczny"
  ]
  node [
    id 199
    label "sprawny"
  ]
  node [
    id 200
    label "skocznie"
  ]
  node [
    id 201
    label "weso&#322;y"
  ]
  node [
    id 202
    label "energiczny"
  ]
  node [
    id 203
    label "specjalny"
  ]
  node [
    id 204
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 205
    label "austriacki"
  ]
  node [
    id 206
    label "lendler"
  ]
  node [
    id 207
    label "polka"
  ]
  node [
    id 208
    label "przytup"
  ]
  node [
    id 209
    label "wodzi&#263;"
  ]
  node [
    id 210
    label "ho&#322;ubiec"
  ]
  node [
    id 211
    label "ludowy"
  ]
  node [
    id 212
    label "krakauer"
  ]
  node [
    id 213
    label "lalka"
  ]
  node [
    id 214
    label "mieszkaniec"
  ]
  node [
    id 215
    label "centu&#347;"
  ]
  node [
    id 216
    label "Ma&#322;opolanin"
  ]
  node [
    id 217
    label "pie&#347;&#324;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
]
