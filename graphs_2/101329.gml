graph [
  node [
    id 0
    label "hamulec"
    origin "text"
  ]
  node [
    id 1
    label "elektrodynamiczny"
    origin "text"
  ]
  node [
    id 2
    label "pow&#347;ci&#261;g"
  ]
  node [
    id 3
    label "uk&#322;ad_hamulcowy"
  ]
  node [
    id 4
    label "czuwak"
  ]
  node [
    id 5
    label "przeszkoda"
  ]
  node [
    id 6
    label "szcz&#281;ka"
  ]
  node [
    id 7
    label "pojazd"
  ]
  node [
    id 8
    label "brake"
  ]
  node [
    id 9
    label "luzownik"
  ]
  node [
    id 10
    label "urz&#261;dzenie"
  ]
  node [
    id 11
    label "dzielenie"
  ]
  node [
    id 12
    label "je&#378;dziectwo"
  ]
  node [
    id 13
    label "obstruction"
  ]
  node [
    id 14
    label "trudno&#347;&#263;"
  ]
  node [
    id 15
    label "bieg_prze&#322;ajowy"
  ]
  node [
    id 16
    label "podzielenie"
  ]
  node [
    id 17
    label "przedmiot"
  ]
  node [
    id 18
    label "kom&#243;rka"
  ]
  node [
    id 19
    label "furnishing"
  ]
  node [
    id 20
    label "zabezpieczenie"
  ]
  node [
    id 21
    label "zrobienie"
  ]
  node [
    id 22
    label "wyrz&#261;dzenie"
  ]
  node [
    id 23
    label "zagospodarowanie"
  ]
  node [
    id 24
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 25
    label "ig&#322;a"
  ]
  node [
    id 26
    label "narz&#281;dzie"
  ]
  node [
    id 27
    label "wirnik"
  ]
  node [
    id 28
    label "aparatura"
  ]
  node [
    id 29
    label "system_energetyczny"
  ]
  node [
    id 30
    label "impulsator"
  ]
  node [
    id 31
    label "mechanizm"
  ]
  node [
    id 32
    label "sprz&#281;t"
  ]
  node [
    id 33
    label "czynno&#347;&#263;"
  ]
  node [
    id 34
    label "blokowanie"
  ]
  node [
    id 35
    label "set"
  ]
  node [
    id 36
    label "zablokowanie"
  ]
  node [
    id 37
    label "przygotowanie"
  ]
  node [
    id 38
    label "komora"
  ]
  node [
    id 39
    label "j&#281;zyk"
  ]
  node [
    id 40
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 41
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 42
    label "antybodziec"
  ]
  node [
    id 43
    label "&#322;&#281;kotka"
  ]
  node [
    id 44
    label "artykulator"
  ]
  node [
    id 45
    label "trzewioczaszka"
  ]
  node [
    id 46
    label "wi&#261;zanie"
  ]
  node [
    id 47
    label "imad&#322;o"
  ]
  node [
    id 48
    label "szczena"
  ]
  node [
    id 49
    label "guzowato&#347;&#263;_br&#243;dkowa"
  ]
  node [
    id 50
    label "dzi&#261;s&#322;o"
  ]
  node [
    id 51
    label "jama_ustna"
  ]
  node [
    id 52
    label "ko&#347;&#263;_z&#281;bowa"
  ]
  node [
    id 53
    label "z&#261;b"
  ]
  node [
    id 54
    label "z&#281;bod&#243;&#322;"
  ]
  node [
    id 55
    label "ko&#347;&#263;"
  ]
  node [
    id 56
    label "artykulacja"
  ]
  node [
    id 57
    label "czaszka"
  ]
  node [
    id 58
    label "kram"
  ]
  node [
    id 59
    label "odholowa&#263;"
  ]
  node [
    id 60
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 61
    label "tabor"
  ]
  node [
    id 62
    label "przyholowywanie"
  ]
  node [
    id 63
    label "przyholowa&#263;"
  ]
  node [
    id 64
    label "przyholowanie"
  ]
  node [
    id 65
    label "fukni&#281;cie"
  ]
  node [
    id 66
    label "l&#261;d"
  ]
  node [
    id 67
    label "zielona_karta"
  ]
  node [
    id 68
    label "fukanie"
  ]
  node [
    id 69
    label "przyholowywa&#263;"
  ]
  node [
    id 70
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 71
    label "woda"
  ]
  node [
    id 72
    label "przeszklenie"
  ]
  node [
    id 73
    label "test_zderzeniowy"
  ]
  node [
    id 74
    label "powietrze"
  ]
  node [
    id 75
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 76
    label "odzywka"
  ]
  node [
    id 77
    label "nadwozie"
  ]
  node [
    id 78
    label "odholowanie"
  ]
  node [
    id 79
    label "prowadzenie_si&#281;"
  ]
  node [
    id 80
    label "odholowywa&#263;"
  ]
  node [
    id 81
    label "pod&#322;oga"
  ]
  node [
    id 82
    label "odholowywanie"
  ]
  node [
    id 83
    label "podwozie"
  ]
  node [
    id 84
    label "pojazd_szynowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
]
