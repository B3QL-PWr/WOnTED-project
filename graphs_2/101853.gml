graph [
  node [
    id 0
    label "lawrence"
    origin "text"
  ]
  node [
    id 1
    label "lessig"
    origin "text"
  ]
  node [
    id 2
    label "swoje"
    origin "text"
  ]
  node [
    id 3
    label "blog"
    origin "text"
  ]
  node [
    id 4
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 6
    label "krytyka"
    origin "text"
  ]
  node [
    id 7
    label "ksi&#261;&#380;ka"
    origin "text"
  ]
  node [
    id 8
    label "keena"
    origin "text"
  ]
  node [
    id 9
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 10
    label "niedziela"
    origin "text"
  ]
  node [
    id 11
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "mirek"
    origin "text"
  ]
  node [
    id 13
    label "poprzedza&#263;"
    origin "text"
  ]
  node [
    id 14
    label "wst&#281;p"
    origin "text"
  ]
  node [
    id 15
    label "zauwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 16
    label "kwestia"
    origin "text"
  ]
  node [
    id 17
    label "paradoksalny"
    origin "text"
  ]
  node [
    id 18
    label "ksi&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 19
    label "krytykowa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "niechlujno&#347;&#263;"
    origin "text"
  ]
  node [
    id 21
    label "ignorancja"
    origin "text"
  ]
  node [
    id 22
    label "internetowy"
    origin "text"
  ]
  node [
    id 23
    label "kultura"
    origin "text"
  ]
  node [
    id 24
    label "partycypacyjnej"
    origin "text"
  ]
  node [
    id 25
    label "sam"
    origin "text"
  ]
  node [
    id 26
    label "pope&#322;nia&#263;"
    origin "text"
  ]
  node [
    id 27
    label "liczny"
    origin "text"
  ]
  node [
    id 28
    label "b&#322;&#261;d"
    origin "text"
  ]
  node [
    id 29
    label "mimo"
    origin "text"
  ]
  node [
    id 30
    label "weryfikacja"
    origin "text"
  ]
  node [
    id 31
    label "przefiltrowa&#263;"
    origin "text"
  ]
  node [
    id 32
    label "tekst"
    origin "text"
  ]
  node [
    id 33
    label "przez"
    origin "text"
  ]
  node [
    id 34
    label "szereg"
    origin "text"
  ]
  node [
    id 35
    label "instytucja"
    origin "text"
  ]
  node [
    id 36
    label "komcio"
  ]
  node [
    id 37
    label "blogosfera"
  ]
  node [
    id 38
    label "pami&#281;tnik"
  ]
  node [
    id 39
    label "strona"
  ]
  node [
    id 40
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 41
    label "pami&#261;tka"
  ]
  node [
    id 42
    label "notes"
  ]
  node [
    id 43
    label "zapiski"
  ]
  node [
    id 44
    label "raptularz"
  ]
  node [
    id 45
    label "album"
  ]
  node [
    id 46
    label "utw&#243;r_epicki"
  ]
  node [
    id 47
    label "kartka"
  ]
  node [
    id 48
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 49
    label "logowanie"
  ]
  node [
    id 50
    label "plik"
  ]
  node [
    id 51
    label "s&#261;d"
  ]
  node [
    id 52
    label "adres_internetowy"
  ]
  node [
    id 53
    label "linia"
  ]
  node [
    id 54
    label "serwis_internetowy"
  ]
  node [
    id 55
    label "posta&#263;"
  ]
  node [
    id 56
    label "bok"
  ]
  node [
    id 57
    label "skr&#281;canie"
  ]
  node [
    id 58
    label "skr&#281;ca&#263;"
  ]
  node [
    id 59
    label "orientowanie"
  ]
  node [
    id 60
    label "skr&#281;ci&#263;"
  ]
  node [
    id 61
    label "uj&#281;cie"
  ]
  node [
    id 62
    label "zorientowanie"
  ]
  node [
    id 63
    label "ty&#322;"
  ]
  node [
    id 64
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 65
    label "fragment"
  ]
  node [
    id 66
    label "layout"
  ]
  node [
    id 67
    label "obiekt"
  ]
  node [
    id 68
    label "zorientowa&#263;"
  ]
  node [
    id 69
    label "pagina"
  ]
  node [
    id 70
    label "podmiot"
  ]
  node [
    id 71
    label "g&#243;ra"
  ]
  node [
    id 72
    label "orientowa&#263;"
  ]
  node [
    id 73
    label "voice"
  ]
  node [
    id 74
    label "orientacja"
  ]
  node [
    id 75
    label "prz&#243;d"
  ]
  node [
    id 76
    label "internet"
  ]
  node [
    id 77
    label "powierzchnia"
  ]
  node [
    id 78
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 79
    label "forma"
  ]
  node [
    id 80
    label "skr&#281;cenie"
  ]
  node [
    id 81
    label "zbi&#243;r"
  ]
  node [
    id 82
    label "komentarz"
  ]
  node [
    id 83
    label "upubliczni&#263;"
  ]
  node [
    id 84
    label "picture"
  ]
  node [
    id 85
    label "wydawnictwo"
  ]
  node [
    id 86
    label "wprowadzi&#263;"
  ]
  node [
    id 87
    label "rynek"
  ]
  node [
    id 88
    label "doprowadzi&#263;"
  ]
  node [
    id 89
    label "testify"
  ]
  node [
    id 90
    label "insert"
  ]
  node [
    id 91
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 92
    label "wpisa&#263;"
  ]
  node [
    id 93
    label "zapozna&#263;"
  ]
  node [
    id 94
    label "zrobi&#263;"
  ]
  node [
    id 95
    label "wej&#347;&#263;"
  ]
  node [
    id 96
    label "spowodowa&#263;"
  ]
  node [
    id 97
    label "zej&#347;&#263;"
  ]
  node [
    id 98
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 99
    label "umie&#347;ci&#263;"
  ]
  node [
    id 100
    label "zacz&#261;&#263;"
  ]
  node [
    id 101
    label "indicate"
  ]
  node [
    id 102
    label "udost&#281;pni&#263;"
  ]
  node [
    id 103
    label "debit"
  ]
  node [
    id 104
    label "redaktor"
  ]
  node [
    id 105
    label "druk"
  ]
  node [
    id 106
    label "publikacja"
  ]
  node [
    id 107
    label "redakcja"
  ]
  node [
    id 108
    label "szata_graficzna"
  ]
  node [
    id 109
    label "firma"
  ]
  node [
    id 110
    label "wydawa&#263;"
  ]
  node [
    id 111
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 112
    label "wyda&#263;"
  ]
  node [
    id 113
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 114
    label "poster"
  ]
  node [
    id 115
    label "daleki"
  ]
  node [
    id 116
    label "ruch"
  ]
  node [
    id 117
    label "d&#322;ugo"
  ]
  node [
    id 118
    label "mechanika"
  ]
  node [
    id 119
    label "utrzymywanie"
  ]
  node [
    id 120
    label "move"
  ]
  node [
    id 121
    label "poruszenie"
  ]
  node [
    id 122
    label "movement"
  ]
  node [
    id 123
    label "myk"
  ]
  node [
    id 124
    label "utrzyma&#263;"
  ]
  node [
    id 125
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 126
    label "zjawisko"
  ]
  node [
    id 127
    label "utrzymanie"
  ]
  node [
    id 128
    label "travel"
  ]
  node [
    id 129
    label "kanciasty"
  ]
  node [
    id 130
    label "commercial_enterprise"
  ]
  node [
    id 131
    label "model"
  ]
  node [
    id 132
    label "strumie&#324;"
  ]
  node [
    id 133
    label "proces"
  ]
  node [
    id 134
    label "aktywno&#347;&#263;"
  ]
  node [
    id 135
    label "kr&#243;tki"
  ]
  node [
    id 136
    label "taktyka"
  ]
  node [
    id 137
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 138
    label "apraksja"
  ]
  node [
    id 139
    label "natural_process"
  ]
  node [
    id 140
    label "utrzymywa&#263;"
  ]
  node [
    id 141
    label "wydarzenie"
  ]
  node [
    id 142
    label "dyssypacja_energii"
  ]
  node [
    id 143
    label "tumult"
  ]
  node [
    id 144
    label "stopek"
  ]
  node [
    id 145
    label "czynno&#347;&#263;"
  ]
  node [
    id 146
    label "zmiana"
  ]
  node [
    id 147
    label "manewr"
  ]
  node [
    id 148
    label "lokomocja"
  ]
  node [
    id 149
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 150
    label "komunikacja"
  ]
  node [
    id 151
    label "drift"
  ]
  node [
    id 152
    label "dawny"
  ]
  node [
    id 153
    label "ogl&#281;dny"
  ]
  node [
    id 154
    label "du&#380;y"
  ]
  node [
    id 155
    label "daleko"
  ]
  node [
    id 156
    label "odleg&#322;y"
  ]
  node [
    id 157
    label "zwi&#261;zany"
  ]
  node [
    id 158
    label "r&#243;&#380;ny"
  ]
  node [
    id 159
    label "s&#322;aby"
  ]
  node [
    id 160
    label "odlegle"
  ]
  node [
    id 161
    label "oddalony"
  ]
  node [
    id 162
    label "g&#322;&#281;boki"
  ]
  node [
    id 163
    label "obcy"
  ]
  node [
    id 164
    label "nieobecny"
  ]
  node [
    id 165
    label "przysz&#322;y"
  ]
  node [
    id 166
    label "streszczenie"
  ]
  node [
    id 167
    label "publicystyka"
  ]
  node [
    id 168
    label "criticism"
  ]
  node [
    id 169
    label "publiczno&#347;&#263;"
  ]
  node [
    id 170
    label "cenzura"
  ]
  node [
    id 171
    label "diatryba"
  ]
  node [
    id 172
    label "review"
  ]
  node [
    id 173
    label "ocena"
  ]
  node [
    id 174
    label "krytyka_literacka"
  ]
  node [
    id 175
    label "ekscerpcja"
  ]
  node [
    id 176
    label "j&#281;zykowo"
  ]
  node [
    id 177
    label "wypowied&#378;"
  ]
  node [
    id 178
    label "wytw&#243;r"
  ]
  node [
    id 179
    label "pomini&#281;cie"
  ]
  node [
    id 180
    label "dzie&#322;o"
  ]
  node [
    id 181
    label "preparacja"
  ]
  node [
    id 182
    label "odmianka"
  ]
  node [
    id 183
    label "opu&#347;ci&#263;"
  ]
  node [
    id 184
    label "koniektura"
  ]
  node [
    id 185
    label "obelga"
  ]
  node [
    id 186
    label "pogl&#261;d"
  ]
  node [
    id 187
    label "decyzja"
  ]
  node [
    id 188
    label "sofcik"
  ]
  node [
    id 189
    label "kryterium"
  ]
  node [
    id 190
    label "informacja"
  ]
  node [
    id 191
    label "appraisal"
  ]
  node [
    id 192
    label "odbiorca"
  ]
  node [
    id 193
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 194
    label "audience"
  ]
  node [
    id 195
    label "publiczka"
  ]
  node [
    id 196
    label "widzownia"
  ]
  node [
    id 197
    label "literatura"
  ]
  node [
    id 198
    label "skr&#243;t"
  ]
  node [
    id 199
    label "podsumowanie"
  ]
  node [
    id 200
    label "przed&#322;o&#380;enie"
  ]
  node [
    id 201
    label "overview"
  ]
  node [
    id 202
    label "urz&#261;d"
  ]
  node [
    id 203
    label "&#347;wiadectwo"
  ]
  node [
    id 204
    label "drugi_obieg"
  ]
  node [
    id 205
    label "zdejmowanie"
  ]
  node [
    id 206
    label "bell_ringer"
  ]
  node [
    id 207
    label "crisscross"
  ]
  node [
    id 208
    label "p&#243;&#322;kownik"
  ]
  node [
    id 209
    label "ekskomunikowa&#263;"
  ]
  node [
    id 210
    label "kontrola"
  ]
  node [
    id 211
    label "mark"
  ]
  node [
    id 212
    label "zdejmowa&#263;"
  ]
  node [
    id 213
    label "zdj&#281;cie"
  ]
  node [
    id 214
    label "zdj&#261;&#263;"
  ]
  node [
    id 215
    label "kara"
  ]
  node [
    id 216
    label "ekskomunikowanie"
  ]
  node [
    id 217
    label "harangue"
  ]
  node [
    id 218
    label "rozmowa"
  ]
  node [
    id 219
    label "utw&#243;r"
  ]
  node [
    id 220
    label "pamflet"
  ]
  node [
    id 221
    label "sprzeciw"
  ]
  node [
    id 222
    label "przem&#243;wienie"
  ]
  node [
    id 223
    label "egzemplarz"
  ]
  node [
    id 224
    label "rozdzia&#322;"
  ]
  node [
    id 225
    label "wk&#322;ad"
  ]
  node [
    id 226
    label "tytu&#322;"
  ]
  node [
    id 227
    label "zak&#322;adka"
  ]
  node [
    id 228
    label "nomina&#322;"
  ]
  node [
    id 229
    label "ok&#322;adka"
  ]
  node [
    id 230
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 231
    label "ekslibris"
  ]
  node [
    id 232
    label "przek&#322;adacz"
  ]
  node [
    id 233
    label "bibliofilstwo"
  ]
  node [
    id 234
    label "falc"
  ]
  node [
    id 235
    label "zw&#243;j"
  ]
  node [
    id 236
    label "czynnik_biotyczny"
  ]
  node [
    id 237
    label "wyewoluowanie"
  ]
  node [
    id 238
    label "reakcja"
  ]
  node [
    id 239
    label "individual"
  ]
  node [
    id 240
    label "przyswoi&#263;"
  ]
  node [
    id 241
    label "starzenie_si&#281;"
  ]
  node [
    id 242
    label "wyewoluowa&#263;"
  ]
  node [
    id 243
    label "okaz"
  ]
  node [
    id 244
    label "part"
  ]
  node [
    id 245
    label "przyswojenie"
  ]
  node [
    id 246
    label "ewoluowanie"
  ]
  node [
    id 247
    label "ewoluowa&#263;"
  ]
  node [
    id 248
    label "sztuka"
  ]
  node [
    id 249
    label "agent"
  ]
  node [
    id 250
    label "przyswaja&#263;"
  ]
  node [
    id 251
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 252
    label "nicpo&#324;"
  ]
  node [
    id 253
    label "przyswajanie"
  ]
  node [
    id 254
    label "nadtytu&#322;"
  ]
  node [
    id 255
    label "tytulatura"
  ]
  node [
    id 256
    label "elevation"
  ]
  node [
    id 257
    label "mianowaniec"
  ]
  node [
    id 258
    label "nazwa"
  ]
  node [
    id 259
    label "podtytu&#322;"
  ]
  node [
    id 260
    label "faza"
  ]
  node [
    id 261
    label "interruption"
  ]
  node [
    id 262
    label "podzia&#322;"
  ]
  node [
    id 263
    label "podrozdzia&#322;"
  ]
  node [
    id 264
    label "pagination"
  ]
  node [
    id 265
    label "numer"
  ]
  node [
    id 266
    label "kwota"
  ]
  node [
    id 267
    label "uczestnictwo"
  ]
  node [
    id 268
    label "element"
  ]
  node [
    id 269
    label "input"
  ]
  node [
    id 270
    label "czasopismo"
  ]
  node [
    id 271
    label "lokata"
  ]
  node [
    id 272
    label "zeszyt"
  ]
  node [
    id 273
    label "blok"
  ]
  node [
    id 274
    label "oprawa"
  ]
  node [
    id 275
    label "boarding"
  ]
  node [
    id 276
    label "oprawianie"
  ]
  node [
    id 277
    label "os&#322;ona"
  ]
  node [
    id 278
    label "oprawia&#263;"
  ]
  node [
    id 279
    label "blacha"
  ]
  node [
    id 280
    label "z&#322;&#261;czenie"
  ]
  node [
    id 281
    label "grzbiet"
  ]
  node [
    id 282
    label "kszta&#322;t"
  ]
  node [
    id 283
    label "wrench"
  ]
  node [
    id 284
    label "m&#243;zg"
  ]
  node [
    id 285
    label "kink"
  ]
  node [
    id 286
    label "manuskrypt"
  ]
  node [
    id 287
    label "rolka"
  ]
  node [
    id 288
    label "warto&#347;&#263;"
  ]
  node [
    id 289
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 290
    label "pieni&#261;dz"
  ]
  node [
    id 291
    label "par_value"
  ]
  node [
    id 292
    label "cena"
  ]
  node [
    id 293
    label "znaczek"
  ]
  node [
    id 294
    label "kolekcjonerstwo"
  ]
  node [
    id 295
    label "bibliomania"
  ]
  node [
    id 296
    label "t&#322;umacz"
  ]
  node [
    id 297
    label "urz&#261;dzenie"
  ]
  node [
    id 298
    label "cz&#322;owiek"
  ]
  node [
    id 299
    label "bookmark"
  ]
  node [
    id 300
    label "fa&#322;da"
  ]
  node [
    id 301
    label "znacznik"
  ]
  node [
    id 302
    label "sp&#243;&#322;g&#322;oska"
  ]
  node [
    id 303
    label "widok"
  ]
  node [
    id 304
    label "program"
  ]
  node [
    id 305
    label "oznaczenie"
  ]
  node [
    id 306
    label "tydzie&#324;"
  ]
  node [
    id 307
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 308
    label "Niedziela_Palmowa"
  ]
  node [
    id 309
    label "dzie&#324;_&#347;wi&#281;ty"
  ]
  node [
    id 310
    label "Wielkanoc"
  ]
  node [
    id 311
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 312
    label "weekend"
  ]
  node [
    id 313
    label "Zes&#322;anie_Ducha_&#346;wi&#281;tego"
  ]
  node [
    id 314
    label "niedziela_przewodnia"
  ]
  node [
    id 315
    label "bia&#322;a_niedziela"
  ]
  node [
    id 316
    label "doba"
  ]
  node [
    id 317
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 318
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 319
    label "czas"
  ]
  node [
    id 320
    label "miesi&#261;c"
  ]
  node [
    id 321
    label "wiosna"
  ]
  node [
    id 322
    label "Poniedzia&#322;ek_Wielkonocny"
  ]
  node [
    id 323
    label "&#347;niadanie_wielkanocne"
  ]
  node [
    id 324
    label "rezurekcja"
  ]
  node [
    id 325
    label "sobota"
  ]
  node [
    id 326
    label "formu&#322;owa&#263;"
  ]
  node [
    id 327
    label "ozdabia&#263;"
  ]
  node [
    id 328
    label "stawia&#263;"
  ]
  node [
    id 329
    label "spell"
  ]
  node [
    id 330
    label "styl"
  ]
  node [
    id 331
    label "skryba"
  ]
  node [
    id 332
    label "read"
  ]
  node [
    id 333
    label "donosi&#263;"
  ]
  node [
    id 334
    label "code"
  ]
  node [
    id 335
    label "dysgrafia"
  ]
  node [
    id 336
    label "dysortografia"
  ]
  node [
    id 337
    label "tworzy&#263;"
  ]
  node [
    id 338
    label "prasa"
  ]
  node [
    id 339
    label "robi&#263;"
  ]
  node [
    id 340
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 341
    label "wytwarza&#263;"
  ]
  node [
    id 342
    label "get"
  ]
  node [
    id 343
    label "consist"
  ]
  node [
    id 344
    label "stanowi&#263;"
  ]
  node [
    id 345
    label "raise"
  ]
  node [
    id 346
    label "spill_the_beans"
  ]
  node [
    id 347
    label "przeby&#263;"
  ]
  node [
    id 348
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 349
    label "zanosi&#263;"
  ]
  node [
    id 350
    label "inform"
  ]
  node [
    id 351
    label "give"
  ]
  node [
    id 352
    label "zu&#380;y&#263;"
  ]
  node [
    id 353
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 354
    label "introduce"
  ]
  node [
    id 355
    label "render"
  ]
  node [
    id 356
    label "ci&#261;&#380;a"
  ]
  node [
    id 357
    label "informowa&#263;"
  ]
  node [
    id 358
    label "komunikowa&#263;"
  ]
  node [
    id 359
    label "convey"
  ]
  node [
    id 360
    label "pozostawia&#263;"
  ]
  node [
    id 361
    label "czyni&#263;"
  ]
  node [
    id 362
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 363
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 364
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 365
    label "przewidywa&#263;"
  ]
  node [
    id 366
    label "przyznawa&#263;"
  ]
  node [
    id 367
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 368
    label "go"
  ]
  node [
    id 369
    label "obstawia&#263;"
  ]
  node [
    id 370
    label "umieszcza&#263;"
  ]
  node [
    id 371
    label "ocenia&#263;"
  ]
  node [
    id 372
    label "zastawia&#263;"
  ]
  node [
    id 373
    label "stanowisko"
  ]
  node [
    id 374
    label "znak"
  ]
  node [
    id 375
    label "wskazywa&#263;"
  ]
  node [
    id 376
    label "uruchamia&#263;"
  ]
  node [
    id 377
    label "fundowa&#263;"
  ]
  node [
    id 378
    label "zmienia&#263;"
  ]
  node [
    id 379
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 380
    label "deliver"
  ]
  node [
    id 381
    label "powodowa&#263;"
  ]
  node [
    id 382
    label "wyznacza&#263;"
  ]
  node [
    id 383
    label "przedstawia&#263;"
  ]
  node [
    id 384
    label "wydobywa&#263;"
  ]
  node [
    id 385
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 386
    label "trim"
  ]
  node [
    id 387
    label "gryzipi&#243;rek"
  ]
  node [
    id 388
    label "pisarz"
  ]
  node [
    id 389
    label "zesp&#243;&#322;"
  ]
  node [
    id 390
    label "t&#322;oczysko"
  ]
  node [
    id 391
    label "depesza"
  ]
  node [
    id 392
    label "maszyna"
  ]
  node [
    id 393
    label "media"
  ]
  node [
    id 394
    label "napisa&#263;"
  ]
  node [
    id 395
    label "dziennikarz_prasowy"
  ]
  node [
    id 396
    label "kiosk"
  ]
  node [
    id 397
    label "maszyna_rolnicza"
  ]
  node [
    id 398
    label "gazeta"
  ]
  node [
    id 399
    label "trzonek"
  ]
  node [
    id 400
    label "narz&#281;dzie"
  ]
  node [
    id 401
    label "spos&#243;b"
  ]
  node [
    id 402
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 403
    label "zachowanie"
  ]
  node [
    id 404
    label "stylik"
  ]
  node [
    id 405
    label "dyscyplina_sportowa"
  ]
  node [
    id 406
    label "handle"
  ]
  node [
    id 407
    label "stroke"
  ]
  node [
    id 408
    label "line"
  ]
  node [
    id 409
    label "charakter"
  ]
  node [
    id 410
    label "natural_language"
  ]
  node [
    id 411
    label "kanon"
  ]
  node [
    id 412
    label "behawior"
  ]
  node [
    id 413
    label "dysleksja"
  ]
  node [
    id 414
    label "pisanie"
  ]
  node [
    id 415
    label "dysgraphia"
  ]
  node [
    id 416
    label "opatrywa&#263;"
  ]
  node [
    id 417
    label "poprzedzanie"
  ]
  node [
    id 418
    label "czasoprzestrze&#324;"
  ]
  node [
    id 419
    label "laba"
  ]
  node [
    id 420
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 421
    label "chronometria"
  ]
  node [
    id 422
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 423
    label "rachuba_czasu"
  ]
  node [
    id 424
    label "przep&#322;ywanie"
  ]
  node [
    id 425
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 426
    label "czasokres"
  ]
  node [
    id 427
    label "odczyt"
  ]
  node [
    id 428
    label "chwila"
  ]
  node [
    id 429
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 430
    label "dzieje"
  ]
  node [
    id 431
    label "kategoria_gramatyczna"
  ]
  node [
    id 432
    label "poprzedzenie"
  ]
  node [
    id 433
    label "trawienie"
  ]
  node [
    id 434
    label "pochodzi&#263;"
  ]
  node [
    id 435
    label "period"
  ]
  node [
    id 436
    label "okres_czasu"
  ]
  node [
    id 437
    label "schy&#322;ek"
  ]
  node [
    id 438
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 439
    label "odwlekanie_si&#281;"
  ]
  node [
    id 440
    label "zegar"
  ]
  node [
    id 441
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 442
    label "czwarty_wymiar"
  ]
  node [
    id 443
    label "pochodzenie"
  ]
  node [
    id 444
    label "koniugacja"
  ]
  node [
    id 445
    label "Zeitgeist"
  ]
  node [
    id 446
    label "trawi&#263;"
  ]
  node [
    id 447
    label "pogoda"
  ]
  node [
    id 448
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 449
    label "poprzedzi&#263;"
  ]
  node [
    id 450
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 451
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 452
    label "time_period"
  ]
  node [
    id 453
    label "oznacza&#263;"
  ]
  node [
    id 454
    label "bandage"
  ]
  node [
    id 455
    label "dopowiada&#263;"
  ]
  node [
    id 456
    label "revise"
  ]
  node [
    id 457
    label "zabezpiecza&#263;"
  ]
  node [
    id 458
    label "przywraca&#263;"
  ]
  node [
    id 459
    label "dress"
  ]
  node [
    id 460
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 461
    label "zapowied&#378;"
  ]
  node [
    id 462
    label "pocz&#261;tek"
  ]
  node [
    id 463
    label "g&#322;oska"
  ]
  node [
    id 464
    label "wymowa"
  ]
  node [
    id 465
    label "podstawy"
  ]
  node [
    id 466
    label "evocation"
  ]
  node [
    id 467
    label "doj&#347;cie"
  ]
  node [
    id 468
    label "Rzym_Zachodni"
  ]
  node [
    id 469
    label "whole"
  ]
  node [
    id 470
    label "ilo&#347;&#263;"
  ]
  node [
    id 471
    label "Rzym_Wschodni"
  ]
  node [
    id 472
    label "signal"
  ]
  node [
    id 473
    label "przewidywanie"
  ]
  node [
    id 474
    label "oznaka"
  ]
  node [
    id 475
    label "zawiadomienie"
  ]
  node [
    id 476
    label "declaration"
  ]
  node [
    id 477
    label "pierworodztwo"
  ]
  node [
    id 478
    label "miejsce"
  ]
  node [
    id 479
    label "upgrade"
  ]
  node [
    id 480
    label "nast&#281;pstwo"
  ]
  node [
    id 481
    label "dochodzenie"
  ]
  node [
    id 482
    label "uzyskanie"
  ]
  node [
    id 483
    label "skill"
  ]
  node [
    id 484
    label "dochrapanie_si&#281;"
  ]
  node [
    id 485
    label "znajomo&#347;ci"
  ]
  node [
    id 486
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 487
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 488
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 489
    label "powi&#261;zanie"
  ]
  node [
    id 490
    label "entrance"
  ]
  node [
    id 491
    label "affiliation"
  ]
  node [
    id 492
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 493
    label "dor&#281;czenie"
  ]
  node [
    id 494
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 495
    label "spowodowanie"
  ]
  node [
    id 496
    label "bodziec"
  ]
  node [
    id 497
    label "dost&#281;p"
  ]
  node [
    id 498
    label "przesy&#322;ka"
  ]
  node [
    id 499
    label "gotowy"
  ]
  node [
    id 500
    label "avenue"
  ]
  node [
    id 501
    label "postrzeganie"
  ]
  node [
    id 502
    label "dodatek"
  ]
  node [
    id 503
    label "doznanie"
  ]
  node [
    id 504
    label "dojrza&#322;y"
  ]
  node [
    id 505
    label "dojechanie"
  ]
  node [
    id 506
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 507
    label "ingress"
  ]
  node [
    id 508
    label "strzelenie"
  ]
  node [
    id 509
    label "orzekni&#281;cie"
  ]
  node [
    id 510
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 511
    label "orgazm"
  ]
  node [
    id 512
    label "dolecenie"
  ]
  node [
    id 513
    label "rozpowszechnienie"
  ]
  node [
    id 514
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 515
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 516
    label "stanie_si&#281;"
  ]
  node [
    id 517
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 518
    label "dop&#322;ata"
  ]
  node [
    id 519
    label "zrobienie"
  ]
  node [
    id 520
    label "wiedza"
  ]
  node [
    id 521
    label "detail"
  ]
  node [
    id 522
    label "obrazowanie"
  ]
  node [
    id 523
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 524
    label "organ"
  ]
  node [
    id 525
    label "tre&#347;&#263;"
  ]
  node [
    id 526
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 527
    label "element_anatomiczny"
  ]
  node [
    id 528
    label "komunikat"
  ]
  node [
    id 529
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 530
    label "sylaba"
  ]
  node [
    id 531
    label "morfem"
  ]
  node [
    id 532
    label "zasymilowanie"
  ]
  node [
    id 533
    label "zasymilowa&#263;"
  ]
  node [
    id 534
    label "phone"
  ]
  node [
    id 535
    label "asymilowanie"
  ]
  node [
    id 536
    label "nast&#281;p"
  ]
  node [
    id 537
    label "d&#378;wi&#281;k"
  ]
  node [
    id 538
    label "asymilowa&#263;"
  ]
  node [
    id 539
    label "akcent"
  ]
  node [
    id 540
    label "chironomia"
  ]
  node [
    id 541
    label "efekt"
  ]
  node [
    id 542
    label "implozja"
  ]
  node [
    id 543
    label "stress"
  ]
  node [
    id 544
    label "znaczenie"
  ]
  node [
    id 545
    label "elokwencja"
  ]
  node [
    id 546
    label "plozja"
  ]
  node [
    id 547
    label "intonacja"
  ]
  node [
    id 548
    label "elokucja"
  ]
  node [
    id 549
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 550
    label "obacza&#263;"
  ]
  node [
    id 551
    label "widzie&#263;"
  ]
  node [
    id 552
    label "notice"
  ]
  node [
    id 553
    label "perceive"
  ]
  node [
    id 554
    label "postrzega&#263;"
  ]
  node [
    id 555
    label "aprobowa&#263;"
  ]
  node [
    id 556
    label "wzrok"
  ]
  node [
    id 557
    label "zmale&#263;"
  ]
  node [
    id 558
    label "punkt_widzenia"
  ]
  node [
    id 559
    label "male&#263;"
  ]
  node [
    id 560
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 561
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 562
    label "spotka&#263;"
  ]
  node [
    id 563
    label "ogl&#261;da&#263;"
  ]
  node [
    id 564
    label "dostrzega&#263;"
  ]
  node [
    id 565
    label "go_steady"
  ]
  node [
    id 566
    label "reagowa&#263;"
  ]
  node [
    id 567
    label "os&#261;dza&#263;"
  ]
  node [
    id 568
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 569
    label "sprawa"
  ]
  node [
    id 570
    label "dialog"
  ]
  node [
    id 571
    label "problemat"
  ]
  node [
    id 572
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 573
    label "problematyka"
  ]
  node [
    id 574
    label "subject"
  ]
  node [
    id 575
    label "kognicja"
  ]
  node [
    id 576
    label "object"
  ]
  node [
    id 577
    label "rozprawa"
  ]
  node [
    id 578
    label "temat"
  ]
  node [
    id 579
    label "szczeg&#243;&#322;"
  ]
  node [
    id 580
    label "proposition"
  ]
  node [
    id 581
    label "przes&#322;anka"
  ]
  node [
    id 582
    label "rzecz"
  ]
  node [
    id 583
    label "idea"
  ]
  node [
    id 584
    label "pos&#322;uchanie"
  ]
  node [
    id 585
    label "sparafrazowanie"
  ]
  node [
    id 586
    label "strawestowa&#263;"
  ]
  node [
    id 587
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 588
    label "trawestowa&#263;"
  ]
  node [
    id 589
    label "sparafrazowa&#263;"
  ]
  node [
    id 590
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 591
    label "sformu&#322;owanie"
  ]
  node [
    id 592
    label "parafrazowanie"
  ]
  node [
    id 593
    label "ozdobnik"
  ]
  node [
    id 594
    label "delimitacja"
  ]
  node [
    id 595
    label "parafrazowa&#263;"
  ]
  node [
    id 596
    label "stylizacja"
  ]
  node [
    id 597
    label "trawestowanie"
  ]
  node [
    id 598
    label "strawestowanie"
  ]
  node [
    id 599
    label "rezultat"
  ]
  node [
    id 600
    label "problem"
  ]
  node [
    id 601
    label "cisza"
  ]
  node [
    id 602
    label "odpowied&#378;"
  ]
  node [
    id 603
    label "rozhowor"
  ]
  node [
    id 604
    label "discussion"
  ]
  node [
    id 605
    label "porozumienie"
  ]
  node [
    id 606
    label "rola"
  ]
  node [
    id 607
    label "sprzeczny"
  ]
  node [
    id 608
    label "paradoksalnie"
  ]
  node [
    id 609
    label "niezgodny"
  ]
  node [
    id 610
    label "sprzecznie"
  ]
  node [
    id 611
    label "przeciwstawnie"
  ]
  node [
    id 612
    label "paradoxically"
  ]
  node [
    id 613
    label "strike"
  ]
  node [
    id 614
    label "rap"
  ]
  node [
    id 615
    label "opiniowa&#263;"
  ]
  node [
    id 616
    label "stwierdza&#263;"
  ]
  node [
    id 617
    label "s&#261;dzi&#263;"
  ]
  node [
    id 618
    label "znajdowa&#263;"
  ]
  node [
    id 619
    label "hold"
  ]
  node [
    id 620
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 621
    label "muzyka_rozrywkowa"
  ]
  node [
    id 622
    label "karpiowate"
  ]
  node [
    id 623
    label "ryba"
  ]
  node [
    id 624
    label "czarna_muzyka"
  ]
  node [
    id 625
    label "drapie&#380;nik"
  ]
  node [
    id 626
    label "asp"
  ]
  node [
    id 627
    label "wada"
  ]
  node [
    id 628
    label "niedba&#322;o&#347;&#263;"
  ]
  node [
    id 629
    label "nastawienie"
  ]
  node [
    id 630
    label "bylejako&#347;&#263;"
  ]
  node [
    id 631
    label "faintness"
  ]
  node [
    id 632
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 633
    label "defect"
  ]
  node [
    id 634
    label "schorzenie"
  ]
  node [
    id 635
    label "cecha"
  ]
  node [
    id 636
    label "imperfection"
  ]
  node [
    id 637
    label "stan"
  ]
  node [
    id 638
    label "brak"
  ]
  node [
    id 639
    label "niewiadomo&#347;&#263;"
  ]
  node [
    id 640
    label "bia&#322;e_plamy"
  ]
  node [
    id 641
    label "Ohio"
  ]
  node [
    id 642
    label "wci&#281;cie"
  ]
  node [
    id 643
    label "Nowy_York"
  ]
  node [
    id 644
    label "warstwa"
  ]
  node [
    id 645
    label "samopoczucie"
  ]
  node [
    id 646
    label "Illinois"
  ]
  node [
    id 647
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 648
    label "state"
  ]
  node [
    id 649
    label "Jukatan"
  ]
  node [
    id 650
    label "Kalifornia"
  ]
  node [
    id 651
    label "Wirginia"
  ]
  node [
    id 652
    label "wektor"
  ]
  node [
    id 653
    label "by&#263;"
  ]
  node [
    id 654
    label "Teksas"
  ]
  node [
    id 655
    label "Goa"
  ]
  node [
    id 656
    label "Waszyngton"
  ]
  node [
    id 657
    label "Massachusetts"
  ]
  node [
    id 658
    label "Alaska"
  ]
  node [
    id 659
    label "Arakan"
  ]
  node [
    id 660
    label "Hawaje"
  ]
  node [
    id 661
    label "Maryland"
  ]
  node [
    id 662
    label "punkt"
  ]
  node [
    id 663
    label "Michigan"
  ]
  node [
    id 664
    label "Arizona"
  ]
  node [
    id 665
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 666
    label "Georgia"
  ]
  node [
    id 667
    label "poziom"
  ]
  node [
    id 668
    label "Pensylwania"
  ]
  node [
    id 669
    label "shape"
  ]
  node [
    id 670
    label "Luizjana"
  ]
  node [
    id 671
    label "Nowy_Meksyk"
  ]
  node [
    id 672
    label "Alabama"
  ]
  node [
    id 673
    label "Kansas"
  ]
  node [
    id 674
    label "Oregon"
  ]
  node [
    id 675
    label "Floryda"
  ]
  node [
    id 676
    label "Oklahoma"
  ]
  node [
    id 677
    label "jednostka_administracyjna"
  ]
  node [
    id 678
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 679
    label "nieistnienie"
  ]
  node [
    id 680
    label "odej&#347;cie"
  ]
  node [
    id 681
    label "gap"
  ]
  node [
    id 682
    label "odej&#347;&#263;"
  ]
  node [
    id 683
    label "odchodzi&#263;"
  ]
  node [
    id 684
    label "wyr&#243;b"
  ]
  node [
    id 685
    label "odchodzenie"
  ]
  node [
    id 686
    label "prywatywny"
  ]
  node [
    id 687
    label "niewiedza"
  ]
  node [
    id 688
    label "elektroniczny"
  ]
  node [
    id 689
    label "internetowo"
  ]
  node [
    id 690
    label "nowoczesny"
  ]
  node [
    id 691
    label "netowy"
  ]
  node [
    id 692
    label "sieciowo"
  ]
  node [
    id 693
    label "elektronicznie"
  ]
  node [
    id 694
    label "siatkowy"
  ]
  node [
    id 695
    label "sieciowy"
  ]
  node [
    id 696
    label "nowy"
  ]
  node [
    id 697
    label "nowo&#380;ytny"
  ]
  node [
    id 698
    label "otwarty"
  ]
  node [
    id 699
    label "nowocze&#347;nie"
  ]
  node [
    id 700
    label "elektrycznie"
  ]
  node [
    id 701
    label "asymilowanie_si&#281;"
  ]
  node [
    id 702
    label "Wsch&#243;d"
  ]
  node [
    id 703
    label "przedmiot"
  ]
  node [
    id 704
    label "praca_rolnicza"
  ]
  node [
    id 705
    label "przejmowanie"
  ]
  node [
    id 706
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 707
    label "makrokosmos"
  ]
  node [
    id 708
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 709
    label "konwencja"
  ]
  node [
    id 710
    label "propriety"
  ]
  node [
    id 711
    label "przejmowa&#263;"
  ]
  node [
    id 712
    label "brzoskwiniarnia"
  ]
  node [
    id 713
    label "zwyczaj"
  ]
  node [
    id 714
    label "jako&#347;&#263;"
  ]
  node [
    id 715
    label "kuchnia"
  ]
  node [
    id 716
    label "tradycja"
  ]
  node [
    id 717
    label "populace"
  ]
  node [
    id 718
    label "hodowla"
  ]
  node [
    id 719
    label "religia"
  ]
  node [
    id 720
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 721
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 722
    label "przej&#281;cie"
  ]
  node [
    id 723
    label "przej&#261;&#263;"
  ]
  node [
    id 724
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 725
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 726
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 727
    label "quality"
  ]
  node [
    id 728
    label "co&#347;"
  ]
  node [
    id 729
    label "syf"
  ]
  node [
    id 730
    label "absolutorium"
  ]
  node [
    id 731
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 732
    label "dzia&#322;anie"
  ]
  node [
    id 733
    label "activity"
  ]
  node [
    id 734
    label "boski"
  ]
  node [
    id 735
    label "krajobraz"
  ]
  node [
    id 736
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 737
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 738
    label "przywidzenie"
  ]
  node [
    id 739
    label "presence"
  ]
  node [
    id 740
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 741
    label "potrzymanie"
  ]
  node [
    id 742
    label "rolnictwo"
  ]
  node [
    id 743
    label "pod&#243;j"
  ]
  node [
    id 744
    label "filiacja"
  ]
  node [
    id 745
    label "licencjonowanie"
  ]
  node [
    id 746
    label "opasa&#263;"
  ]
  node [
    id 747
    label "ch&#243;w"
  ]
  node [
    id 748
    label "licencja"
  ]
  node [
    id 749
    label "sokolarnia"
  ]
  node [
    id 750
    label "potrzyma&#263;"
  ]
  node [
    id 751
    label "rozp&#322;&#243;d"
  ]
  node [
    id 752
    label "grupa_organizm&#243;w"
  ]
  node [
    id 753
    label "wypas"
  ]
  node [
    id 754
    label "wychowalnia"
  ]
  node [
    id 755
    label "pstr&#261;garnia"
  ]
  node [
    id 756
    label "krzy&#380;owanie"
  ]
  node [
    id 757
    label "licencjonowa&#263;"
  ]
  node [
    id 758
    label "odch&#243;w"
  ]
  node [
    id 759
    label "tucz"
  ]
  node [
    id 760
    label "ud&#243;j"
  ]
  node [
    id 761
    label "klatka"
  ]
  node [
    id 762
    label "opasienie"
  ]
  node [
    id 763
    label "wych&#243;w"
  ]
  node [
    id 764
    label "obrz&#261;dek"
  ]
  node [
    id 765
    label "opasanie"
  ]
  node [
    id 766
    label "polish"
  ]
  node [
    id 767
    label "akwarium"
  ]
  node [
    id 768
    label "biotechnika"
  ]
  node [
    id 769
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 770
    label "uk&#322;ad"
  ]
  node [
    id 771
    label "zjazd"
  ]
  node [
    id 772
    label "charakterystyka"
  ]
  node [
    id 773
    label "m&#322;ot"
  ]
  node [
    id 774
    label "drzewo"
  ]
  node [
    id 775
    label "pr&#243;ba"
  ]
  node [
    id 776
    label "attribute"
  ]
  node [
    id 777
    label "marka"
  ]
  node [
    id 778
    label "biom"
  ]
  node [
    id 779
    label "szata_ro&#347;linna"
  ]
  node [
    id 780
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 781
    label "formacja_ro&#347;linna"
  ]
  node [
    id 782
    label "przyroda"
  ]
  node [
    id 783
    label "zielono&#347;&#263;"
  ]
  node [
    id 784
    label "pi&#281;tro"
  ]
  node [
    id 785
    label "plant"
  ]
  node [
    id 786
    label "ro&#347;lina"
  ]
  node [
    id 787
    label "geosystem"
  ]
  node [
    id 788
    label "kult"
  ]
  node [
    id 789
    label "mitologia"
  ]
  node [
    id 790
    label "wyznanie"
  ]
  node [
    id 791
    label "ideologia"
  ]
  node [
    id 792
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 793
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 794
    label "nawracanie_si&#281;"
  ]
  node [
    id 795
    label "duchowny"
  ]
  node [
    id 796
    label "rela"
  ]
  node [
    id 797
    label "kultura_duchowa"
  ]
  node [
    id 798
    label "kosmologia"
  ]
  node [
    id 799
    label "kosmogonia"
  ]
  node [
    id 800
    label "nawraca&#263;"
  ]
  node [
    id 801
    label "mistyka"
  ]
  node [
    id 802
    label "pr&#243;bowanie"
  ]
  node [
    id 803
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 804
    label "realizacja"
  ]
  node [
    id 805
    label "scena"
  ]
  node [
    id 806
    label "didaskalia"
  ]
  node [
    id 807
    label "czyn"
  ]
  node [
    id 808
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 809
    label "environment"
  ]
  node [
    id 810
    label "head"
  ]
  node [
    id 811
    label "scenariusz"
  ]
  node [
    id 812
    label "jednostka"
  ]
  node [
    id 813
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 814
    label "fortel"
  ]
  node [
    id 815
    label "theatrical_performance"
  ]
  node [
    id 816
    label "ambala&#380;"
  ]
  node [
    id 817
    label "sprawno&#347;&#263;"
  ]
  node [
    id 818
    label "kobieta"
  ]
  node [
    id 819
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 820
    label "Faust"
  ]
  node [
    id 821
    label "scenografia"
  ]
  node [
    id 822
    label "ods&#322;ona"
  ]
  node [
    id 823
    label "turn"
  ]
  node [
    id 824
    label "pokaz"
  ]
  node [
    id 825
    label "przedstawienie"
  ]
  node [
    id 826
    label "przedstawi&#263;"
  ]
  node [
    id 827
    label "Apollo"
  ]
  node [
    id 828
    label "przedstawianie"
  ]
  node [
    id 829
    label "towar"
  ]
  node [
    id 830
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 831
    label "ceremony"
  ]
  node [
    id 832
    label "dorobek"
  ]
  node [
    id 833
    label "tworzenie"
  ]
  node [
    id 834
    label "kreacja"
  ]
  node [
    id 835
    label "creation"
  ]
  node [
    id 836
    label "staro&#347;cina_weselna"
  ]
  node [
    id 837
    label "folklor"
  ]
  node [
    id 838
    label "objawienie"
  ]
  node [
    id 839
    label "zaj&#281;cie"
  ]
  node [
    id 840
    label "tajniki"
  ]
  node [
    id 841
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 842
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 843
    label "jedzenie"
  ]
  node [
    id 844
    label "zaplecze"
  ]
  node [
    id 845
    label "pomieszczenie"
  ]
  node [
    id 846
    label "zlewozmywak"
  ]
  node [
    id 847
    label "gotowa&#263;"
  ]
  node [
    id 848
    label "ciemna_materia"
  ]
  node [
    id 849
    label "planeta"
  ]
  node [
    id 850
    label "mikrokosmos"
  ]
  node [
    id 851
    label "ekosfera"
  ]
  node [
    id 852
    label "przestrze&#324;"
  ]
  node [
    id 853
    label "czarna_dziura"
  ]
  node [
    id 854
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 855
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 856
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 857
    label "kosmos"
  ]
  node [
    id 858
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 859
    label "poprawno&#347;&#263;"
  ]
  node [
    id 860
    label "og&#322;ada"
  ]
  node [
    id 861
    label "service"
  ]
  node [
    id 862
    label "stosowno&#347;&#263;"
  ]
  node [
    id 863
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 864
    label "Ukraina"
  ]
  node [
    id 865
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 866
    label "blok_wschodni"
  ]
  node [
    id 867
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 868
    label "wsch&#243;d"
  ]
  node [
    id 869
    label "Europa_Wschodnia"
  ]
  node [
    id 870
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 871
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 872
    label "wra&#380;enie"
  ]
  node [
    id 873
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 874
    label "interception"
  ]
  node [
    id 875
    label "wzbudzenie"
  ]
  node [
    id 876
    label "emotion"
  ]
  node [
    id 877
    label "zaczerpni&#281;cie"
  ]
  node [
    id 878
    label "wzi&#281;cie"
  ]
  node [
    id 879
    label "bang"
  ]
  node [
    id 880
    label "wzi&#261;&#263;"
  ]
  node [
    id 881
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 882
    label "stimulate"
  ]
  node [
    id 883
    label "ogarn&#261;&#263;"
  ]
  node [
    id 884
    label "wzbudzi&#263;"
  ]
  node [
    id 885
    label "thrill"
  ]
  node [
    id 886
    label "treat"
  ]
  node [
    id 887
    label "czerpa&#263;"
  ]
  node [
    id 888
    label "bra&#263;"
  ]
  node [
    id 889
    label "wzbudza&#263;"
  ]
  node [
    id 890
    label "ogarnia&#263;"
  ]
  node [
    id 891
    label "czerpanie"
  ]
  node [
    id 892
    label "acquisition"
  ]
  node [
    id 893
    label "branie"
  ]
  node [
    id 894
    label "caparison"
  ]
  node [
    id 895
    label "wzbudzanie"
  ]
  node [
    id 896
    label "ogarnianie"
  ]
  node [
    id 897
    label "wpadni&#281;cie"
  ]
  node [
    id 898
    label "mienie"
  ]
  node [
    id 899
    label "istota"
  ]
  node [
    id 900
    label "wpa&#347;&#263;"
  ]
  node [
    id 901
    label "wpadanie"
  ]
  node [
    id 902
    label "wpada&#263;"
  ]
  node [
    id 903
    label "zboczenie"
  ]
  node [
    id 904
    label "om&#243;wienie"
  ]
  node [
    id 905
    label "sponiewieranie"
  ]
  node [
    id 906
    label "discipline"
  ]
  node [
    id 907
    label "omawia&#263;"
  ]
  node [
    id 908
    label "kr&#261;&#380;enie"
  ]
  node [
    id 909
    label "robienie"
  ]
  node [
    id 910
    label "sponiewiera&#263;"
  ]
  node [
    id 911
    label "entity"
  ]
  node [
    id 912
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 913
    label "tematyka"
  ]
  node [
    id 914
    label "w&#261;tek"
  ]
  node [
    id 915
    label "zbaczanie"
  ]
  node [
    id 916
    label "program_nauczania"
  ]
  node [
    id 917
    label "om&#243;wi&#263;"
  ]
  node [
    id 918
    label "omawianie"
  ]
  node [
    id 919
    label "thing"
  ]
  node [
    id 920
    label "zbacza&#263;"
  ]
  node [
    id 921
    label "zboczy&#263;"
  ]
  node [
    id 922
    label "uprawa"
  ]
  node [
    id 923
    label "sklep"
  ]
  node [
    id 924
    label "p&#243;&#322;ka"
  ]
  node [
    id 925
    label "stoisko"
  ]
  node [
    id 926
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 927
    label "sk&#322;ad"
  ]
  node [
    id 928
    label "obiekt_handlowy"
  ]
  node [
    id 929
    label "witryna"
  ]
  node [
    id 930
    label "organizowa&#263;"
  ]
  node [
    id 931
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 932
    label "stylizowa&#263;"
  ]
  node [
    id 933
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 934
    label "falowa&#263;"
  ]
  node [
    id 935
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 936
    label "peddle"
  ]
  node [
    id 937
    label "praca"
  ]
  node [
    id 938
    label "wydala&#263;"
  ]
  node [
    id 939
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 940
    label "tentegowa&#263;"
  ]
  node [
    id 941
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 942
    label "urz&#261;dza&#263;"
  ]
  node [
    id 943
    label "oszukiwa&#263;"
  ]
  node [
    id 944
    label "work"
  ]
  node [
    id 945
    label "ukazywa&#263;"
  ]
  node [
    id 946
    label "przerabia&#263;"
  ]
  node [
    id 947
    label "act"
  ]
  node [
    id 948
    label "post&#281;powa&#263;"
  ]
  node [
    id 949
    label "cz&#281;sty"
  ]
  node [
    id 950
    label "licznie"
  ]
  node [
    id 951
    label "rojenie_si&#281;"
  ]
  node [
    id 952
    label "cz&#281;sto"
  ]
  node [
    id 953
    label "pomyli&#263;_si&#281;"
  ]
  node [
    id 954
    label "error"
  ]
  node [
    id 955
    label "pomylenie_si&#281;"
  ]
  node [
    id 956
    label "baseball"
  ]
  node [
    id 957
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 958
    label "mniemanie"
  ]
  node [
    id 959
    label "byk"
  ]
  node [
    id 960
    label "treatment"
  ]
  node [
    id 961
    label "my&#347;lenie"
  ]
  node [
    id 962
    label "typ"
  ]
  node [
    id 963
    label "event"
  ]
  node [
    id 964
    label "przyczyna"
  ]
  node [
    id 965
    label "nies&#322;uszno&#347;&#263;"
  ]
  node [
    id 966
    label "sytuacja"
  ]
  node [
    id 967
    label "niedopasowanie"
  ]
  node [
    id 968
    label "funkcja"
  ]
  node [
    id 969
    label "g&#243;ra_mi&#281;sa"
  ]
  node [
    id 970
    label "bydl&#281;"
  ]
  node [
    id 971
    label "strategia_byka"
  ]
  node [
    id 972
    label "si&#322;acz"
  ]
  node [
    id 973
    label "gie&#322;da_papier&#243;w_warto&#347;ciowych"
  ]
  node [
    id 974
    label "brat"
  ]
  node [
    id 975
    label "cios"
  ]
  node [
    id 976
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 977
    label "symbol"
  ]
  node [
    id 978
    label "bull"
  ]
  node [
    id 979
    label "gie&#322;da"
  ]
  node [
    id 980
    label "inwestor"
  ]
  node [
    id 981
    label "samiec"
  ]
  node [
    id 982
    label "optymista"
  ]
  node [
    id 983
    label "olbrzym"
  ]
  node [
    id 984
    label "kij_baseballowy"
  ]
  node [
    id 985
    label "r&#281;kawica_baseballowa"
  ]
  node [
    id 986
    label "gra"
  ]
  node [
    id 987
    label "sport"
  ]
  node [
    id 988
    label "sport_zespo&#322;owy"
  ]
  node [
    id 989
    label "&#322;apacz"
  ]
  node [
    id 990
    label "baza"
  ]
  node [
    id 991
    label "analiza"
  ]
  node [
    id 992
    label "postkomunizm"
  ]
  node [
    id 993
    label "k&#322;amca_lustracyjny"
  ]
  node [
    id 994
    label "misja_weryfikacyjna"
  ]
  node [
    id 995
    label "post&#281;powanie"
  ]
  node [
    id 996
    label "legalizacja_ponowna"
  ]
  node [
    id 997
    label "w&#322;adza"
  ]
  node [
    id 998
    label "perlustracja"
  ]
  node [
    id 999
    label "legalizacja_pierwotna"
  ]
  node [
    id 1000
    label "examination"
  ]
  node [
    id 1001
    label "campaign"
  ]
  node [
    id 1002
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1003
    label "fashion"
  ]
  node [
    id 1004
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1005
    label "zmierzanie"
  ]
  node [
    id 1006
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 1007
    label "kazanie"
  ]
  node [
    id 1008
    label "badanie"
  ]
  node [
    id 1009
    label "opis"
  ]
  node [
    id 1010
    label "analysis"
  ]
  node [
    id 1011
    label "dissection"
  ]
  node [
    id 1012
    label "metoda"
  ]
  node [
    id 1013
    label "reakcja_chemiczna"
  ]
  node [
    id 1014
    label "komunizm"
  ]
  node [
    id 1015
    label "filter"
  ]
  node [
    id 1016
    label "wyczy&#347;ci&#263;"
  ]
  node [
    id 1017
    label "usun&#261;&#263;"
  ]
  node [
    id 1018
    label "nerka"
  ]
  node [
    id 1019
    label "przepu&#347;ci&#263;"
  ]
  node [
    id 1020
    label "withdraw"
  ]
  node [
    id 1021
    label "motivate"
  ]
  node [
    id 1022
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 1023
    label "wyrugowa&#263;"
  ]
  node [
    id 1024
    label "undo"
  ]
  node [
    id 1025
    label "zabi&#263;"
  ]
  node [
    id 1026
    label "przenie&#347;&#263;"
  ]
  node [
    id 1027
    label "przesun&#261;&#263;"
  ]
  node [
    id 1028
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1029
    label "roztrwoni&#263;"
  ]
  node [
    id 1030
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 1031
    label "obrobi&#263;"
  ]
  node [
    id 1032
    label "ust&#261;pi&#263;"
  ]
  node [
    id 1033
    label "darowa&#263;"
  ]
  node [
    id 1034
    label "pu&#347;ci&#263;"
  ]
  node [
    id 1035
    label "przeoczy&#263;"
  ]
  node [
    id 1036
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 1037
    label "blue"
  ]
  node [
    id 1038
    label "authorize"
  ]
  node [
    id 1039
    label "omin&#261;&#263;"
  ]
  node [
    id 1040
    label "correct"
  ]
  node [
    id 1041
    label "zabra&#263;"
  ]
  node [
    id 1042
    label "okra&#347;&#263;"
  ]
  node [
    id 1043
    label "poprawi&#263;"
  ]
  node [
    id 1044
    label "oczy&#347;ci&#263;"
  ]
  node [
    id 1045
    label "clarify"
  ]
  node [
    id 1046
    label "&#322;oso&#347;"
  ]
  node [
    id 1047
    label "saszetka"
  ]
  node [
    id 1048
    label "filtrowa&#263;"
  ]
  node [
    id 1049
    label "przefiltrowywa&#263;"
  ]
  node [
    id 1050
    label "przefiltrowywanie"
  ]
  node [
    id 1051
    label "narz&#261;d_wydalniczy"
  ]
  node [
    id 1052
    label "ektopia_nerki"
  ]
  node [
    id 1053
    label "podroby"
  ]
  node [
    id 1054
    label "t&#281;tnica_&#322;ukowata"
  ]
  node [
    id 1055
    label "przefiltrowanie"
  ]
  node [
    id 1056
    label "dializa"
  ]
  node [
    id 1057
    label "nefrostomia"
  ]
  node [
    id 1058
    label "dysplazja_nerek"
  ]
  node [
    id 1059
    label "agenezja_nerki"
  ]
  node [
    id 1060
    label "piramida_nerkowa"
  ]
  node [
    id 1061
    label "uk&#322;ad_moczowy"
  ]
  node [
    id 1062
    label "wielotorbielowato&#347;&#263;_nerek"
  ]
  node [
    id 1063
    label "gruczo&#322;_nadnerczowy"
  ]
  node [
    id 1064
    label "p&#322;&#243;d"
  ]
  node [
    id 1065
    label "retrospektywa"
  ]
  node [
    id 1066
    label "works"
  ]
  node [
    id 1067
    label "tetralogia"
  ]
  node [
    id 1068
    label "cholera"
  ]
  node [
    id 1069
    label "ubliga"
  ]
  node [
    id 1070
    label "niedorobek"
  ]
  node [
    id 1071
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 1072
    label "chuj"
  ]
  node [
    id 1073
    label "bluzg"
  ]
  node [
    id 1074
    label "wyzwisko"
  ]
  node [
    id 1075
    label "indignation"
  ]
  node [
    id 1076
    label "pies"
  ]
  node [
    id 1077
    label "wrzuta"
  ]
  node [
    id 1078
    label "chujowy"
  ]
  node [
    id 1079
    label "krzywda"
  ]
  node [
    id 1080
    label "szmata"
  ]
  node [
    id 1081
    label "odmiana"
  ]
  node [
    id 1082
    label "preparation"
  ]
  node [
    id 1083
    label "proces_technologiczny"
  ]
  node [
    id 1084
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 1085
    label "pozostawi&#263;"
  ]
  node [
    id 1086
    label "obni&#380;y&#263;"
  ]
  node [
    id 1087
    label "zostawi&#263;"
  ]
  node [
    id 1088
    label "przesta&#263;"
  ]
  node [
    id 1089
    label "potani&#263;"
  ]
  node [
    id 1090
    label "drop"
  ]
  node [
    id 1091
    label "evacuate"
  ]
  node [
    id 1092
    label "humiliate"
  ]
  node [
    id 1093
    label "leave"
  ]
  node [
    id 1094
    label "straci&#263;"
  ]
  node [
    id 1095
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1096
    label "przypuszczenie"
  ]
  node [
    id 1097
    label "conjecture"
  ]
  node [
    id 1098
    label "obr&#243;bka"
  ]
  node [
    id 1099
    label "wniosek"
  ]
  node [
    id 1100
    label "radio"
  ]
  node [
    id 1101
    label "siedziba"
  ]
  node [
    id 1102
    label "composition"
  ]
  node [
    id 1103
    label "redaction"
  ]
  node [
    id 1104
    label "telewizja"
  ]
  node [
    id 1105
    label "wyb&#243;r"
  ]
  node [
    id 1106
    label "dokumentacja"
  ]
  node [
    id 1107
    label "u&#380;ytkownik"
  ]
  node [
    id 1108
    label "komunikacyjnie"
  ]
  node [
    id 1109
    label "ellipsis"
  ]
  node [
    id 1110
    label "wykluczenie"
  ]
  node [
    id 1111
    label "figura_my&#347;li"
  ]
  node [
    id 1112
    label "szpaler"
  ]
  node [
    id 1113
    label "column"
  ]
  node [
    id 1114
    label "uporz&#261;dkowanie"
  ]
  node [
    id 1115
    label "mn&#243;stwo"
  ]
  node [
    id 1116
    label "unit"
  ]
  node [
    id 1117
    label "koniec"
  ]
  node [
    id 1118
    label "rozmieszczenie"
  ]
  node [
    id 1119
    label "tract"
  ]
  node [
    id 1120
    label "wyra&#380;enie"
  ]
  node [
    id 1121
    label "struktura"
  ]
  node [
    id 1122
    label "ustalenie"
  ]
  node [
    id 1123
    label "structure"
  ]
  node [
    id 1124
    label "sequence"
  ]
  node [
    id 1125
    label "succession"
  ]
  node [
    id 1126
    label "series"
  ]
  node [
    id 1127
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1128
    label "uprawianie"
  ]
  node [
    id 1129
    label "collection"
  ]
  node [
    id 1130
    label "dane"
  ]
  node [
    id 1131
    label "pakiet_klimatyczny"
  ]
  node [
    id 1132
    label "poj&#281;cie"
  ]
  node [
    id 1133
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1134
    label "sum"
  ]
  node [
    id 1135
    label "gathering"
  ]
  node [
    id 1136
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 1137
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1138
    label "porozmieszczanie"
  ]
  node [
    id 1139
    label "wyst&#281;powanie"
  ]
  node [
    id 1140
    label "umieszczenie"
  ]
  node [
    id 1141
    label "enormousness"
  ]
  node [
    id 1142
    label "leksem"
  ]
  node [
    id 1143
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1144
    label "poinformowanie"
  ]
  node [
    id 1145
    label "wording"
  ]
  node [
    id 1146
    label "kompozycja"
  ]
  node [
    id 1147
    label "znak_j&#281;zykowy"
  ]
  node [
    id 1148
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1149
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 1150
    label "grupa_imienna"
  ]
  node [
    id 1151
    label "jednostka_leksykalna"
  ]
  node [
    id 1152
    label "term"
  ]
  node [
    id 1153
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 1154
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 1155
    label "ujawnienie"
  ]
  node [
    id 1156
    label "affirmation"
  ]
  node [
    id 1157
    label "zapisanie"
  ]
  node [
    id 1158
    label "rzucenie"
  ]
  node [
    id 1159
    label "przej&#347;cie"
  ]
  node [
    id 1160
    label "espalier"
  ]
  node [
    id 1161
    label "aleja"
  ]
  node [
    id 1162
    label "szyk"
  ]
  node [
    id 1163
    label "rz&#261;d"
  ]
  node [
    id 1164
    label "ostatnie_podrygi"
  ]
  node [
    id 1165
    label "visitation"
  ]
  node [
    id 1166
    label "agonia"
  ]
  node [
    id 1167
    label "defenestracja"
  ]
  node [
    id 1168
    label "kres"
  ]
  node [
    id 1169
    label "mogi&#322;a"
  ]
  node [
    id 1170
    label "kres_&#380;ycia"
  ]
  node [
    id 1171
    label "szeol"
  ]
  node [
    id 1172
    label "pogrzebanie"
  ]
  node [
    id 1173
    label "&#380;a&#322;oba"
  ]
  node [
    id 1174
    label "zabicie"
  ]
  node [
    id 1175
    label "osoba_prawna"
  ]
  node [
    id 1176
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1177
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1178
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1179
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1180
    label "biuro"
  ]
  node [
    id 1181
    label "organizacja"
  ]
  node [
    id 1182
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1183
    label "Fundusze_Unijne"
  ]
  node [
    id 1184
    label "zamyka&#263;"
  ]
  node [
    id 1185
    label "establishment"
  ]
  node [
    id 1186
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1187
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1188
    label "afiliowa&#263;"
  ]
  node [
    id 1189
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1190
    label "standard"
  ]
  node [
    id 1191
    label "zamykanie"
  ]
  node [
    id 1192
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1193
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1194
    label "skumanie"
  ]
  node [
    id 1195
    label "teoria"
  ]
  node [
    id 1196
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1197
    label "clasp"
  ]
  node [
    id 1198
    label "jednostka_organizacyjna"
  ]
  node [
    id 1199
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1200
    label "TOPR"
  ]
  node [
    id 1201
    label "endecki"
  ]
  node [
    id 1202
    label "przedstawicielstwo"
  ]
  node [
    id 1203
    label "od&#322;am"
  ]
  node [
    id 1204
    label "Cepelia"
  ]
  node [
    id 1205
    label "ZBoWiD"
  ]
  node [
    id 1206
    label "organization"
  ]
  node [
    id 1207
    label "centrala"
  ]
  node [
    id 1208
    label "GOPR"
  ]
  node [
    id 1209
    label "ZOMO"
  ]
  node [
    id 1210
    label "ZMP"
  ]
  node [
    id 1211
    label "komitet_koordynacyjny"
  ]
  node [
    id 1212
    label "przybud&#243;wka"
  ]
  node [
    id 1213
    label "boj&#243;wka"
  ]
  node [
    id 1214
    label "ordinariness"
  ]
  node [
    id 1215
    label "zorganizowa&#263;"
  ]
  node [
    id 1216
    label "taniec_towarzyski"
  ]
  node [
    id 1217
    label "organizowanie"
  ]
  node [
    id 1218
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 1219
    label "criterion"
  ]
  node [
    id 1220
    label "zorganizowanie"
  ]
  node [
    id 1221
    label "biurko"
  ]
  node [
    id 1222
    label "boks"
  ]
  node [
    id 1223
    label "palestra"
  ]
  node [
    id 1224
    label "Biuro_Lustracyjne"
  ]
  node [
    id 1225
    label "agency"
  ]
  node [
    id 1226
    label "board"
  ]
  node [
    id 1227
    label "dzia&#322;"
  ]
  node [
    id 1228
    label "position"
  ]
  node [
    id 1229
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 1230
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1231
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1232
    label "okienko"
  ]
  node [
    id 1233
    label "consort"
  ]
  node [
    id 1234
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 1235
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 1236
    label "umieszczanie"
  ]
  node [
    id 1237
    label "powodowanie"
  ]
  node [
    id 1238
    label "zamykanie_si&#281;"
  ]
  node [
    id 1239
    label "ko&#324;czenie"
  ]
  node [
    id 1240
    label "extinction"
  ]
  node [
    id 1241
    label "unieruchamianie"
  ]
  node [
    id 1242
    label "ukrywanie"
  ]
  node [
    id 1243
    label "sk&#322;adanie"
  ]
  node [
    id 1244
    label "locking"
  ]
  node [
    id 1245
    label "zawieranie"
  ]
  node [
    id 1246
    label "wy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 1247
    label "closing"
  ]
  node [
    id 1248
    label "blocking"
  ]
  node [
    id 1249
    label "przytrzaskiwanie"
  ]
  node [
    id 1250
    label "blokowanie"
  ]
  node [
    id 1251
    label "occlusion"
  ]
  node [
    id 1252
    label "rozwi&#261;zywanie"
  ]
  node [
    id 1253
    label "przygaszanie"
  ]
  node [
    id 1254
    label "lock"
  ]
  node [
    id 1255
    label "ujmowanie"
  ]
  node [
    id 1256
    label "zawiera&#263;"
  ]
  node [
    id 1257
    label "suspend"
  ]
  node [
    id 1258
    label "ujmowa&#263;"
  ]
  node [
    id 1259
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 1260
    label "unieruchamia&#263;"
  ]
  node [
    id 1261
    label "sk&#322;ada&#263;"
  ]
  node [
    id 1262
    label "ko&#324;czy&#263;"
  ]
  node [
    id 1263
    label "blokowa&#263;"
  ]
  node [
    id 1264
    label "ukrywa&#263;"
  ]
  node [
    id 1265
    label "exsert"
  ]
  node [
    id 1266
    label "elite"
  ]
  node [
    id 1267
    label "&#347;rodowisko"
  ]
  node [
    id 1268
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1269
    label "Lawrence"
  ]
  node [
    id 1270
    label "Lessig"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 32
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 319
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 433
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 32
  ]
  edge [
    source 14
    target 219
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 78
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 467
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 469
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 268
  ]
  edge [
    source 14
    target 471
  ]
  edge [
    source 14
    target 297
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 472
  ]
  edge [
    source 14
    target 473
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 260
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 420
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 502
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 505
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 14
    target 507
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 509
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 511
  ]
  edge [
    source 14
    target 512
  ]
  edge [
    source 14
    target 513
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 522
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 244
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 14
    target 529
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 531
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 248
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 549
  ]
  edge [
    source 15
    target 550
  ]
  edge [
    source 15
    target 551
  ]
  edge [
    source 15
    target 552
  ]
  edge [
    source 15
    target 553
  ]
  edge [
    source 15
    target 554
  ]
  edge [
    source 15
    target 555
  ]
  edge [
    source 15
    target 556
  ]
  edge [
    source 15
    target 557
  ]
  edge [
    source 15
    target 558
  ]
  edge [
    source 15
    target 559
  ]
  edge [
    source 15
    target 560
  ]
  edge [
    source 15
    target 561
  ]
  edge [
    source 15
    target 562
  ]
  edge [
    source 15
    target 362
  ]
  edge [
    source 15
    target 563
  ]
  edge [
    source 15
    target 564
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 565
  ]
  edge [
    source 15
    target 566
  ]
  edge [
    source 15
    target 567
  ]
  edge [
    source 15
    target 568
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 569
  ]
  edge [
    source 16
    target 570
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 571
  ]
  edge [
    source 16
    target 572
  ]
  edge [
    source 16
    target 573
  ]
  edge [
    source 16
    target 574
  ]
  edge [
    source 16
    target 575
  ]
  edge [
    source 16
    target 576
  ]
  edge [
    source 16
    target 577
  ]
  edge [
    source 16
    target 578
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 579
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 51
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 586
  ]
  edge [
    source 16
    target 587
  ]
  edge [
    source 16
    target 588
  ]
  edge [
    source 16
    target 589
  ]
  edge [
    source 16
    target 590
  ]
  edge [
    source 16
    target 591
  ]
  edge [
    source 16
    target 592
  ]
  edge [
    source 16
    target 593
  ]
  edge [
    source 16
    target 594
  ]
  edge [
    source 16
    target 595
  ]
  edge [
    source 16
    target 596
  ]
  edge [
    source 16
    target 528
  ]
  edge [
    source 16
    target 597
  ]
  edge [
    source 16
    target 598
  ]
  edge [
    source 16
    target 599
  ]
  edge [
    source 16
    target 600
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 601
  ]
  edge [
    source 16
    target 602
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 603
  ]
  edge [
    source 16
    target 604
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 605
  ]
  edge [
    source 16
    target 606
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 607
  ]
  edge [
    source 17
    target 608
  ]
  edge [
    source 17
    target 609
  ]
  edge [
    source 17
    target 610
  ]
  edge [
    source 17
    target 611
  ]
  edge [
    source 17
    target 612
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 613
  ]
  edge [
    source 19
    target 614
  ]
  edge [
    source 19
    target 567
  ]
  edge [
    source 19
    target 615
  ]
  edge [
    source 19
    target 616
  ]
  edge [
    source 19
    target 339
  ]
  edge [
    source 19
    target 617
  ]
  edge [
    source 19
    target 381
  ]
  edge [
    source 19
    target 618
  ]
  edge [
    source 19
    target 619
  ]
  edge [
    source 19
    target 620
  ]
  edge [
    source 19
    target 621
  ]
  edge [
    source 19
    target 622
  ]
  edge [
    source 19
    target 623
  ]
  edge [
    source 19
    target 624
  ]
  edge [
    source 19
    target 625
  ]
  edge [
    source 19
    target 626
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 627
  ]
  edge [
    source 20
    target 628
  ]
  edge [
    source 20
    target 629
  ]
  edge [
    source 20
    target 630
  ]
  edge [
    source 20
    target 631
  ]
  edge [
    source 20
    target 632
  ]
  edge [
    source 20
    target 633
  ]
  edge [
    source 20
    target 634
  ]
  edge [
    source 20
    target 635
  ]
  edge [
    source 20
    target 39
  ]
  edge [
    source 20
    target 636
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 637
  ]
  edge [
    source 21
    target 638
  ]
  edge [
    source 21
    target 639
  ]
  edge [
    source 21
    target 640
  ]
  edge [
    source 21
    target 641
  ]
  edge [
    source 21
    target 642
  ]
  edge [
    source 21
    target 643
  ]
  edge [
    source 21
    target 644
  ]
  edge [
    source 21
    target 645
  ]
  edge [
    source 21
    target 646
  ]
  edge [
    source 21
    target 647
  ]
  edge [
    source 21
    target 648
  ]
  edge [
    source 21
    target 649
  ]
  edge [
    source 21
    target 650
  ]
  edge [
    source 21
    target 651
  ]
  edge [
    source 21
    target 652
  ]
  edge [
    source 21
    target 653
  ]
  edge [
    source 21
    target 654
  ]
  edge [
    source 21
    target 655
  ]
  edge [
    source 21
    target 656
  ]
  edge [
    source 21
    target 478
  ]
  edge [
    source 21
    target 657
  ]
  edge [
    source 21
    target 658
  ]
  edge [
    source 21
    target 659
  ]
  edge [
    source 21
    target 660
  ]
  edge [
    source 21
    target 661
  ]
  edge [
    source 21
    target 662
  ]
  edge [
    source 21
    target 663
  ]
  edge [
    source 21
    target 664
  ]
  edge [
    source 21
    target 665
  ]
  edge [
    source 21
    target 666
  ]
  edge [
    source 21
    target 667
  ]
  edge [
    source 21
    target 668
  ]
  edge [
    source 21
    target 669
  ]
  edge [
    source 21
    target 670
  ]
  edge [
    source 21
    target 671
  ]
  edge [
    source 21
    target 672
  ]
  edge [
    source 21
    target 470
  ]
  edge [
    source 21
    target 673
  ]
  edge [
    source 21
    target 674
  ]
  edge [
    source 21
    target 675
  ]
  edge [
    source 21
    target 676
  ]
  edge [
    source 21
    target 677
  ]
  edge [
    source 21
    target 678
  ]
  edge [
    source 21
    target 679
  ]
  edge [
    source 21
    target 680
  ]
  edge [
    source 21
    target 633
  ]
  edge [
    source 21
    target 681
  ]
  edge [
    source 21
    target 682
  ]
  edge [
    source 21
    target 135
  ]
  edge [
    source 21
    target 627
  ]
  edge [
    source 21
    target 683
  ]
  edge [
    source 21
    target 684
  ]
  edge [
    source 21
    target 685
  ]
  edge [
    source 21
    target 686
  ]
  edge [
    source 21
    target 687
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 688
  ]
  edge [
    source 22
    target 689
  ]
  edge [
    source 22
    target 690
  ]
  edge [
    source 22
    target 691
  ]
  edge [
    source 22
    target 692
  ]
  edge [
    source 22
    target 693
  ]
  edge [
    source 22
    target 694
  ]
  edge [
    source 22
    target 695
  ]
  edge [
    source 22
    target 696
  ]
  edge [
    source 22
    target 697
  ]
  edge [
    source 22
    target 698
  ]
  edge [
    source 22
    target 699
  ]
  edge [
    source 22
    target 700
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 35
  ]
  edge [
    source 23
    target 701
  ]
  edge [
    source 23
    target 523
  ]
  edge [
    source 23
    target 702
  ]
  edge [
    source 23
    target 703
  ]
  edge [
    source 23
    target 704
  ]
  edge [
    source 23
    target 705
  ]
  edge [
    source 23
    target 126
  ]
  edge [
    source 23
    target 635
  ]
  edge [
    source 23
    target 706
  ]
  edge [
    source 23
    target 707
  ]
  edge [
    source 23
    target 708
  ]
  edge [
    source 23
    target 709
  ]
  edge [
    source 23
    target 582
  ]
  edge [
    source 23
    target 710
  ]
  edge [
    source 23
    target 711
  ]
  edge [
    source 23
    target 712
  ]
  edge [
    source 23
    target 137
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 713
  ]
  edge [
    source 23
    target 714
  ]
  edge [
    source 23
    target 715
  ]
  edge [
    source 23
    target 716
  ]
  edge [
    source 23
    target 717
  ]
  edge [
    source 23
    target 718
  ]
  edge [
    source 23
    target 719
  ]
  edge [
    source 23
    target 720
  ]
  edge [
    source 23
    target 721
  ]
  edge [
    source 23
    target 722
  ]
  edge [
    source 23
    target 723
  ]
  edge [
    source 23
    target 724
  ]
  edge [
    source 23
    target 725
  ]
  edge [
    source 23
    target 726
  ]
  edge [
    source 23
    target 288
  ]
  edge [
    source 23
    target 727
  ]
  edge [
    source 23
    target 728
  ]
  edge [
    source 23
    target 648
  ]
  edge [
    source 23
    target 729
  ]
  edge [
    source 23
    target 730
  ]
  edge [
    source 23
    target 731
  ]
  edge [
    source 23
    target 732
  ]
  edge [
    source 23
    target 733
  ]
  edge [
    source 23
    target 133
  ]
  edge [
    source 23
    target 734
  ]
  edge [
    source 23
    target 735
  ]
  edge [
    source 23
    target 736
  ]
  edge [
    source 23
    target 737
  ]
  edge [
    source 23
    target 738
  ]
  edge [
    source 23
    target 739
  ]
  edge [
    source 23
    target 409
  ]
  edge [
    source 23
    target 740
  ]
  edge [
    source 23
    target 741
  ]
  edge [
    source 23
    target 742
  ]
  edge [
    source 23
    target 743
  ]
  edge [
    source 23
    target 744
  ]
  edge [
    source 23
    target 745
  ]
  edge [
    source 23
    target 746
  ]
  edge [
    source 23
    target 747
  ]
  edge [
    source 23
    target 748
  ]
  edge [
    source 23
    target 749
  ]
  edge [
    source 23
    target 750
  ]
  edge [
    source 23
    target 751
  ]
  edge [
    source 23
    target 752
  ]
  edge [
    source 23
    target 753
  ]
  edge [
    source 23
    target 754
  ]
  edge [
    source 23
    target 755
  ]
  edge [
    source 23
    target 756
  ]
  edge [
    source 23
    target 757
  ]
  edge [
    source 23
    target 758
  ]
  edge [
    source 23
    target 759
  ]
  edge [
    source 23
    target 760
  ]
  edge [
    source 23
    target 761
  ]
  edge [
    source 23
    target 762
  ]
  edge [
    source 23
    target 763
  ]
  edge [
    source 23
    target 764
  ]
  edge [
    source 23
    target 765
  ]
  edge [
    source 23
    target 766
  ]
  edge [
    source 23
    target 767
  ]
  edge [
    source 23
    target 768
  ]
  edge [
    source 23
    target 769
  ]
  edge [
    source 23
    target 81
  ]
  edge [
    source 23
    target 770
  ]
  edge [
    source 23
    target 330
  ]
  edge [
    source 23
    target 408
  ]
  edge [
    source 23
    target 411
  ]
  edge [
    source 23
    target 771
  ]
  edge [
    source 23
    target 772
  ]
  edge [
    source 23
    target 773
  ]
  edge [
    source 23
    target 374
  ]
  edge [
    source 23
    target 774
  ]
  edge [
    source 23
    target 775
  ]
  edge [
    source 23
    target 776
  ]
  edge [
    source 23
    target 777
  ]
  edge [
    source 23
    target 778
  ]
  edge [
    source 23
    target 779
  ]
  edge [
    source 23
    target 780
  ]
  edge [
    source 23
    target 781
  ]
  edge [
    source 23
    target 782
  ]
  edge [
    source 23
    target 783
  ]
  edge [
    source 23
    target 784
  ]
  edge [
    source 23
    target 785
  ]
  edge [
    source 23
    target 786
  ]
  edge [
    source 23
    target 787
  ]
  edge [
    source 23
    target 788
  ]
  edge [
    source 23
    target 789
  ]
  edge [
    source 23
    target 790
  ]
  edge [
    source 23
    target 791
  ]
  edge [
    source 23
    target 792
  ]
  edge [
    source 23
    target 793
  ]
  edge [
    source 23
    target 794
  ]
  edge [
    source 23
    target 795
  ]
  edge [
    source 23
    target 796
  ]
  edge [
    source 23
    target 797
  ]
  edge [
    source 23
    target 798
  ]
  edge [
    source 23
    target 799
  ]
  edge [
    source 23
    target 800
  ]
  edge [
    source 23
    target 801
  ]
  edge [
    source 23
    target 802
  ]
  edge [
    source 23
    target 606
  ]
  edge [
    source 23
    target 298
  ]
  edge [
    source 23
    target 803
  ]
  edge [
    source 23
    target 804
  ]
  edge [
    source 23
    target 805
  ]
  edge [
    source 23
    target 806
  ]
  edge [
    source 23
    target 807
  ]
  edge [
    source 23
    target 808
  ]
  edge [
    source 23
    target 809
  ]
  edge [
    source 23
    target 810
  ]
  edge [
    source 23
    target 811
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 812
  ]
  edge [
    source 23
    target 813
  ]
  edge [
    source 23
    target 219
  ]
  edge [
    source 23
    target 814
  ]
  edge [
    source 23
    target 815
  ]
  edge [
    source 23
    target 816
  ]
  edge [
    source 23
    target 817
  ]
  edge [
    source 23
    target 818
  ]
  edge [
    source 23
    target 819
  ]
  edge [
    source 23
    target 820
  ]
  edge [
    source 23
    target 821
  ]
  edge [
    source 23
    target 822
  ]
  edge [
    source 23
    target 823
  ]
  edge [
    source 23
    target 824
  ]
  edge [
    source 23
    target 470
  ]
  edge [
    source 23
    target 825
  ]
  edge [
    source 23
    target 826
  ]
  edge [
    source 23
    target 827
  ]
  edge [
    source 23
    target 828
  ]
  edge [
    source 23
    target 383
  ]
  edge [
    source 23
    target 829
  ]
  edge [
    source 23
    target 830
  ]
  edge [
    source 23
    target 403
  ]
  edge [
    source 23
    target 831
  ]
  edge [
    source 23
    target 832
  ]
  edge [
    source 23
    target 833
  ]
  edge [
    source 23
    target 834
  ]
  edge [
    source 23
    target 835
  ]
  edge [
    source 23
    target 836
  ]
  edge [
    source 23
    target 837
  ]
  edge [
    source 23
    target 838
  ]
  edge [
    source 23
    target 839
  ]
  edge [
    source 23
    target 840
  ]
  edge [
    source 23
    target 841
  ]
  edge [
    source 23
    target 842
  ]
  edge [
    source 23
    target 843
  ]
  edge [
    source 23
    target 844
  ]
  edge [
    source 23
    target 845
  ]
  edge [
    source 23
    target 846
  ]
  edge [
    source 23
    target 847
  ]
  edge [
    source 23
    target 848
  ]
  edge [
    source 23
    target 849
  ]
  edge [
    source 23
    target 850
  ]
  edge [
    source 23
    target 851
  ]
  edge [
    source 23
    target 852
  ]
  edge [
    source 23
    target 853
  ]
  edge [
    source 23
    target 854
  ]
  edge [
    source 23
    target 855
  ]
  edge [
    source 23
    target 856
  ]
  edge [
    source 23
    target 857
  ]
  edge [
    source 23
    target 858
  ]
  edge [
    source 23
    target 859
  ]
  edge [
    source 23
    target 860
  ]
  edge [
    source 23
    target 861
  ]
  edge [
    source 23
    target 862
  ]
  edge [
    source 23
    target 863
  ]
  edge [
    source 23
    target 864
  ]
  edge [
    source 23
    target 865
  ]
  edge [
    source 23
    target 866
  ]
  edge [
    source 23
    target 867
  ]
  edge [
    source 23
    target 868
  ]
  edge [
    source 23
    target 869
  ]
  edge [
    source 23
    target 870
  ]
  edge [
    source 23
    target 871
  ]
  edge [
    source 23
    target 872
  ]
  edge [
    source 23
    target 873
  ]
  edge [
    source 23
    target 874
  ]
  edge [
    source 23
    target 875
  ]
  edge [
    source 23
    target 876
  ]
  edge [
    source 23
    target 122
  ]
  edge [
    source 23
    target 877
  ]
  edge [
    source 23
    target 878
  ]
  edge [
    source 23
    target 879
  ]
  edge [
    source 23
    target 880
  ]
  edge [
    source 23
    target 881
  ]
  edge [
    source 23
    target 882
  ]
  edge [
    source 23
    target 883
  ]
  edge [
    source 23
    target 884
  ]
  edge [
    source 23
    target 885
  ]
  edge [
    source 23
    target 886
  ]
  edge [
    source 23
    target 887
  ]
  edge [
    source 23
    target 888
  ]
  edge [
    source 23
    target 368
  ]
  edge [
    source 23
    target 406
  ]
  edge [
    source 23
    target 889
  ]
  edge [
    source 23
    target 890
  ]
  edge [
    source 23
    target 891
  ]
  edge [
    source 23
    target 892
  ]
  edge [
    source 23
    target 893
  ]
  edge [
    source 23
    target 894
  ]
  edge [
    source 23
    target 895
  ]
  edge [
    source 23
    target 145
  ]
  edge [
    source 23
    target 896
  ]
  edge [
    source 23
    target 576
  ]
  edge [
    source 23
    target 578
  ]
  edge [
    source 23
    target 897
  ]
  edge [
    source 23
    target 898
  ]
  edge [
    source 23
    target 899
  ]
  edge [
    source 23
    target 67
  ]
  edge [
    source 23
    target 900
  ]
  edge [
    source 23
    target 901
  ]
  edge [
    source 23
    target 902
  ]
  edge [
    source 23
    target 903
  ]
  edge [
    source 23
    target 904
  ]
  edge [
    source 23
    target 905
  ]
  edge [
    source 23
    target 906
  ]
  edge [
    source 23
    target 907
  ]
  edge [
    source 23
    target 908
  ]
  edge [
    source 23
    target 525
  ]
  edge [
    source 23
    target 909
  ]
  edge [
    source 23
    target 910
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 911
  ]
  edge [
    source 23
    target 912
  ]
  edge [
    source 23
    target 913
  ]
  edge [
    source 23
    target 914
  ]
  edge [
    source 23
    target 915
  ]
  edge [
    source 23
    target 916
  ]
  edge [
    source 23
    target 917
  ]
  edge [
    source 23
    target 918
  ]
  edge [
    source 23
    target 919
  ]
  edge [
    source 23
    target 920
  ]
  edge [
    source 23
    target 921
  ]
  edge [
    source 23
    target 478
  ]
  edge [
    source 23
    target 922
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 923
  ]
  edge [
    source 25
    target 924
  ]
  edge [
    source 25
    target 109
  ]
  edge [
    source 25
    target 925
  ]
  edge [
    source 25
    target 926
  ]
  edge [
    source 25
    target 927
  ]
  edge [
    source 25
    target 928
  ]
  edge [
    source 25
    target 844
  ]
  edge [
    source 25
    target 929
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 337
  ]
  edge [
    source 26
    target 339
  ]
  edge [
    source 26
    target 930
  ]
  edge [
    source 26
    target 931
  ]
  edge [
    source 26
    target 361
  ]
  edge [
    source 26
    target 351
  ]
  edge [
    source 26
    target 932
  ]
  edge [
    source 26
    target 933
  ]
  edge [
    source 26
    target 934
  ]
  edge [
    source 26
    target 935
  ]
  edge [
    source 26
    target 936
  ]
  edge [
    source 26
    target 937
  ]
  edge [
    source 26
    target 938
  ]
  edge [
    source 26
    target 939
  ]
  edge [
    source 26
    target 940
  ]
  edge [
    source 26
    target 941
  ]
  edge [
    source 26
    target 942
  ]
  edge [
    source 26
    target 943
  ]
  edge [
    source 26
    target 944
  ]
  edge [
    source 26
    target 945
  ]
  edge [
    source 26
    target 946
  ]
  edge [
    source 26
    target 947
  ]
  edge [
    source 26
    target 948
  ]
  edge [
    source 26
    target 340
  ]
  edge [
    source 26
    target 341
  ]
  edge [
    source 26
    target 342
  ]
  edge [
    source 26
    target 343
  ]
  edge [
    source 26
    target 344
  ]
  edge [
    source 26
    target 345
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 949
  ]
  edge [
    source 27
    target 950
  ]
  edge [
    source 27
    target 951
  ]
  edge [
    source 27
    target 952
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 953
  ]
  edge [
    source 28
    target 954
  ]
  edge [
    source 28
    target 955
  ]
  edge [
    source 28
    target 807
  ]
  edge [
    source 28
    target 956
  ]
  edge [
    source 28
    target 957
  ]
  edge [
    source 28
    target 958
  ]
  edge [
    source 28
    target 959
  ]
  edge [
    source 28
    target 599
  ]
  edge [
    source 28
    target 960
  ]
  edge [
    source 28
    target 186
  ]
  edge [
    source 28
    target 961
  ]
  edge [
    source 28
    target 732
  ]
  edge [
    source 28
    target 962
  ]
  edge [
    source 28
    target 963
  ]
  edge [
    source 28
    target 964
  ]
  edge [
    source 28
    target 965
  ]
  edge [
    source 28
    target 966
  ]
  edge [
    source 28
    target 967
  ]
  edge [
    source 28
    target 968
  ]
  edge [
    source 28
    target 947
  ]
  edge [
    source 28
    target 969
  ]
  edge [
    source 28
    target 970
  ]
  edge [
    source 28
    target 298
  ]
  edge [
    source 28
    target 971
  ]
  edge [
    source 28
    target 972
  ]
  edge [
    source 28
    target 973
  ]
  edge [
    source 28
    target 974
  ]
  edge [
    source 28
    target 975
  ]
  edge [
    source 28
    target 976
  ]
  edge [
    source 28
    target 977
  ]
  edge [
    source 28
    target 978
  ]
  edge [
    source 28
    target 979
  ]
  edge [
    source 28
    target 980
  ]
  edge [
    source 28
    target 981
  ]
  edge [
    source 28
    target 982
  ]
  edge [
    source 28
    target 983
  ]
  edge [
    source 28
    target 984
  ]
  edge [
    source 28
    target 985
  ]
  edge [
    source 28
    target 986
  ]
  edge [
    source 28
    target 987
  ]
  edge [
    source 28
    target 988
  ]
  edge [
    source 28
    target 989
  ]
  edge [
    source 28
    target 990
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 991
  ]
  edge [
    source 30
    target 992
  ]
  edge [
    source 30
    target 993
  ]
  edge [
    source 30
    target 210
  ]
  edge [
    source 30
    target 994
  ]
  edge [
    source 30
    target 995
  ]
  edge [
    source 30
    target 996
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 997
  ]
  edge [
    source 30
    target 998
  ]
  edge [
    source 30
    target 145
  ]
  edge [
    source 30
    target 999
  ]
  edge [
    source 30
    target 1000
  ]
  edge [
    source 30
    target 51
  ]
  edge [
    source 30
    target 575
  ]
  edge [
    source 30
    target 1001
  ]
  edge [
    source 30
    target 577
  ]
  edge [
    source 30
    target 403
  ]
  edge [
    source 30
    target 141
  ]
  edge [
    source 30
    target 1002
  ]
  edge [
    source 30
    target 1003
  ]
  edge [
    source 30
    target 909
  ]
  edge [
    source 30
    target 1004
  ]
  edge [
    source 30
    target 1005
  ]
  edge [
    source 30
    target 581
  ]
  edge [
    source 30
    target 1006
  ]
  edge [
    source 30
    target 1007
  ]
  edge [
    source 30
    target 1008
  ]
  edge [
    source 30
    target 1009
  ]
  edge [
    source 30
    target 1010
  ]
  edge [
    source 30
    target 1011
  ]
  edge [
    source 30
    target 1012
  ]
  edge [
    source 30
    target 1013
  ]
  edge [
    source 30
    target 319
  ]
  edge [
    source 30
    target 1014
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1015
  ]
  edge [
    source 31
    target 1016
  ]
  edge [
    source 31
    target 1017
  ]
  edge [
    source 31
    target 1018
  ]
  edge [
    source 31
    target 1019
  ]
  edge [
    source 31
    target 1020
  ]
  edge [
    source 31
    target 1021
  ]
  edge [
    source 31
    target 1022
  ]
  edge [
    source 31
    target 1023
  ]
  edge [
    source 31
    target 368
  ]
  edge [
    source 31
    target 1024
  ]
  edge [
    source 31
    target 1025
  ]
  edge [
    source 31
    target 96
  ]
  edge [
    source 31
    target 1026
  ]
  edge [
    source 31
    target 1027
  ]
  edge [
    source 31
    target 1028
  ]
  edge [
    source 31
    target 1029
  ]
  edge [
    source 31
    target 1030
  ]
  edge [
    source 31
    target 1031
  ]
  edge [
    source 31
    target 1032
  ]
  edge [
    source 31
    target 1033
  ]
  edge [
    source 31
    target 1034
  ]
  edge [
    source 31
    target 1035
  ]
  edge [
    source 31
    target 1036
  ]
  edge [
    source 31
    target 1037
  ]
  edge [
    source 31
    target 1038
  ]
  edge [
    source 31
    target 1039
  ]
  edge [
    source 31
    target 1040
  ]
  edge [
    source 31
    target 1041
  ]
  edge [
    source 31
    target 1042
  ]
  edge [
    source 31
    target 1043
  ]
  edge [
    source 31
    target 1044
  ]
  edge [
    source 31
    target 1045
  ]
  edge [
    source 31
    target 1046
  ]
  edge [
    source 31
    target 1047
  ]
  edge [
    source 31
    target 1048
  ]
  edge [
    source 31
    target 1049
  ]
  edge [
    source 31
    target 1050
  ]
  edge [
    source 31
    target 1051
  ]
  edge [
    source 31
    target 1052
  ]
  edge [
    source 31
    target 1053
  ]
  edge [
    source 31
    target 1054
  ]
  edge [
    source 31
    target 1055
  ]
  edge [
    source 31
    target 1056
  ]
  edge [
    source 31
    target 1057
  ]
  edge [
    source 31
    target 1058
  ]
  edge [
    source 31
    target 1059
  ]
  edge [
    source 31
    target 1060
  ]
  edge [
    source 31
    target 1061
  ]
  edge [
    source 31
    target 1062
  ]
  edge [
    source 31
    target 1063
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 175
  ]
  edge [
    source 32
    target 176
  ]
  edge [
    source 32
    target 177
  ]
  edge [
    source 32
    target 107
  ]
  edge [
    source 32
    target 178
  ]
  edge [
    source 32
    target 179
  ]
  edge [
    source 32
    target 180
  ]
  edge [
    source 32
    target 181
  ]
  edge [
    source 32
    target 182
  ]
  edge [
    source 32
    target 183
  ]
  edge [
    source 32
    target 184
  ]
  edge [
    source 32
    target 185
  ]
  edge [
    source 32
    target 703
  ]
  edge [
    source 32
    target 1064
  ]
  edge [
    source 32
    target 944
  ]
  edge [
    source 32
    target 599
  ]
  edge [
    source 32
    target 522
  ]
  edge [
    source 32
    target 523
  ]
  edge [
    source 32
    target 832
  ]
  edge [
    source 32
    target 79
  ]
  edge [
    source 32
    target 525
  ]
  edge [
    source 32
    target 526
  ]
  edge [
    source 32
    target 1065
  ]
  edge [
    source 32
    target 1066
  ]
  edge [
    source 32
    target 835
  ]
  edge [
    source 32
    target 1067
  ]
  edge [
    source 32
    target 528
  ]
  edge [
    source 32
    target 529
  ]
  edge [
    source 32
    target 937
  ]
  edge [
    source 32
    target 584
  ]
  edge [
    source 32
    target 51
  ]
  edge [
    source 32
    target 585
  ]
  edge [
    source 32
    target 587
  ]
  edge [
    source 32
    target 586
  ]
  edge [
    source 32
    target 589
  ]
  edge [
    source 32
    target 590
  ]
  edge [
    source 32
    target 588
  ]
  edge [
    source 32
    target 591
  ]
  edge [
    source 32
    target 592
  ]
  edge [
    source 32
    target 593
  ]
  edge [
    source 32
    target 594
  ]
  edge [
    source 32
    target 595
  ]
  edge [
    source 32
    target 596
  ]
  edge [
    source 32
    target 597
  ]
  edge [
    source 32
    target 598
  ]
  edge [
    source 32
    target 1068
  ]
  edge [
    source 32
    target 1069
  ]
  edge [
    source 32
    target 1070
  ]
  edge [
    source 32
    target 1071
  ]
  edge [
    source 32
    target 1072
  ]
  edge [
    source 32
    target 1073
  ]
  edge [
    source 32
    target 1074
  ]
  edge [
    source 32
    target 1075
  ]
  edge [
    source 32
    target 1076
  ]
  edge [
    source 32
    target 1077
  ]
  edge [
    source 32
    target 1078
  ]
  edge [
    source 32
    target 1079
  ]
  edge [
    source 32
    target 1080
  ]
  edge [
    source 32
    target 1081
  ]
  edge [
    source 32
    target 326
  ]
  edge [
    source 32
    target 327
  ]
  edge [
    source 32
    target 328
  ]
  edge [
    source 32
    target 329
  ]
  edge [
    source 32
    target 330
  ]
  edge [
    source 32
    target 331
  ]
  edge [
    source 32
    target 332
  ]
  edge [
    source 32
    target 333
  ]
  edge [
    source 32
    target 334
  ]
  edge [
    source 32
    target 335
  ]
  edge [
    source 32
    target 336
  ]
  edge [
    source 32
    target 337
  ]
  edge [
    source 32
    target 338
  ]
  edge [
    source 32
    target 1082
  ]
  edge [
    source 32
    target 1083
  ]
  edge [
    source 32
    target 61
  ]
  edge [
    source 32
    target 1084
  ]
  edge [
    source 32
    target 1085
  ]
  edge [
    source 32
    target 1086
  ]
  edge [
    source 32
    target 1087
  ]
  edge [
    source 32
    target 1088
  ]
  edge [
    source 32
    target 1089
  ]
  edge [
    source 32
    target 1090
  ]
  edge [
    source 32
    target 1091
  ]
  edge [
    source 32
    target 1092
  ]
  edge [
    source 32
    target 1093
  ]
  edge [
    source 32
    target 1094
  ]
  edge [
    source 32
    target 1095
  ]
  edge [
    source 32
    target 1038
  ]
  edge [
    source 32
    target 1039
  ]
  edge [
    source 32
    target 1096
  ]
  edge [
    source 32
    target 1097
  ]
  edge [
    source 32
    target 1098
  ]
  edge [
    source 32
    target 1099
  ]
  edge [
    source 32
    target 104
  ]
  edge [
    source 32
    target 1100
  ]
  edge [
    source 32
    target 389
  ]
  edge [
    source 32
    target 1101
  ]
  edge [
    source 32
    target 1102
  ]
  edge [
    source 32
    target 85
  ]
  edge [
    source 32
    target 1103
  ]
  edge [
    source 32
    target 1104
  ]
  edge [
    source 32
    target 1105
  ]
  edge [
    source 32
    target 1106
  ]
  edge [
    source 32
    target 1107
  ]
  edge [
    source 32
    target 1108
  ]
  edge [
    source 32
    target 1109
  ]
  edge [
    source 32
    target 1110
  ]
  edge [
    source 32
    target 1111
  ]
  edge [
    source 32
    target 519
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1112
  ]
  edge [
    source 34
    target 81
  ]
  edge [
    source 34
    target 1113
  ]
  edge [
    source 34
    target 1114
  ]
  edge [
    source 34
    target 1115
  ]
  edge [
    source 34
    target 1116
  ]
  edge [
    source 34
    target 1117
  ]
  edge [
    source 34
    target 1118
  ]
  edge [
    source 34
    target 1119
  ]
  edge [
    source 34
    target 1120
  ]
  edge [
    source 34
    target 1121
  ]
  edge [
    source 34
    target 1122
  ]
  edge [
    source 34
    target 495
  ]
  edge [
    source 34
    target 1123
  ]
  edge [
    source 34
    target 145
  ]
  edge [
    source 34
    target 1124
  ]
  edge [
    source 34
    target 1125
  ]
  edge [
    source 34
    target 223
  ]
  edge [
    source 34
    target 1126
  ]
  edge [
    source 34
    target 1127
  ]
  edge [
    source 34
    target 1128
  ]
  edge [
    source 34
    target 704
  ]
  edge [
    source 34
    target 1129
  ]
  edge [
    source 34
    target 1130
  ]
  edge [
    source 34
    target 1131
  ]
  edge [
    source 34
    target 1132
  ]
  edge [
    source 34
    target 1133
  ]
  edge [
    source 34
    target 1134
  ]
  edge [
    source 34
    target 1135
  ]
  edge [
    source 34
    target 1136
  ]
  edge [
    source 34
    target 45
  ]
  edge [
    source 34
    target 1137
  ]
  edge [
    source 34
    target 1138
  ]
  edge [
    source 34
    target 1139
  ]
  edge [
    source 34
    target 770
  ]
  edge [
    source 34
    target 66
  ]
  edge [
    source 34
    target 1140
  ]
  edge [
    source 34
    target 470
  ]
  edge [
    source 34
    target 1141
  ]
  edge [
    source 34
    target 1142
  ]
  edge [
    source 34
    target 591
  ]
  edge [
    source 34
    target 1143
  ]
  edge [
    source 34
    target 1144
  ]
  edge [
    source 34
    target 1145
  ]
  edge [
    source 34
    target 1146
  ]
  edge [
    source 34
    target 305
  ]
  edge [
    source 34
    target 1147
  ]
  edge [
    source 34
    target 1148
  ]
  edge [
    source 34
    target 593
  ]
  edge [
    source 34
    target 1149
  ]
  edge [
    source 34
    target 1150
  ]
  edge [
    source 34
    target 1151
  ]
  edge [
    source 34
    target 1152
  ]
  edge [
    source 34
    target 1153
  ]
  edge [
    source 34
    target 1154
  ]
  edge [
    source 34
    target 1155
  ]
  edge [
    source 34
    target 1156
  ]
  edge [
    source 34
    target 1157
  ]
  edge [
    source 34
    target 1158
  ]
  edge [
    source 34
    target 1159
  ]
  edge [
    source 34
    target 1160
  ]
  edge [
    source 34
    target 1161
  ]
  edge [
    source 34
    target 1162
  ]
  edge [
    source 34
    target 1163
  ]
  edge [
    source 34
    target 1164
  ]
  edge [
    source 34
    target 1165
  ]
  edge [
    source 34
    target 1166
  ]
  edge [
    source 34
    target 1167
  ]
  edge [
    source 34
    target 662
  ]
  edge [
    source 34
    target 732
  ]
  edge [
    source 34
    target 1168
  ]
  edge [
    source 34
    target 141
  ]
  edge [
    source 34
    target 1169
  ]
  edge [
    source 34
    target 1170
  ]
  edge [
    source 34
    target 1171
  ]
  edge [
    source 34
    target 1172
  ]
  edge [
    source 34
    target 478
  ]
  edge [
    source 34
    target 428
  ]
  edge [
    source 34
    target 1173
  ]
  edge [
    source 34
    target 78
  ]
  edge [
    source 34
    target 1174
  ]
  edge [
    source 35
    target 1175
  ]
  edge [
    source 35
    target 1176
  ]
  edge [
    source 35
    target 1177
  ]
  edge [
    source 35
    target 1132
  ]
  edge [
    source 35
    target 1178
  ]
  edge [
    source 35
    target 1179
  ]
  edge [
    source 35
    target 1180
  ]
  edge [
    source 35
    target 1181
  ]
  edge [
    source 35
    target 1182
  ]
  edge [
    source 35
    target 1183
  ]
  edge [
    source 35
    target 1184
  ]
  edge [
    source 35
    target 1185
  ]
  edge [
    source 35
    target 1186
  ]
  edge [
    source 35
    target 202
  ]
  edge [
    source 35
    target 1187
  ]
  edge [
    source 35
    target 1188
  ]
  edge [
    source 35
    target 1189
  ]
  edge [
    source 35
    target 1190
  ]
  edge [
    source 35
    target 1191
  ]
  edge [
    source 35
    target 1192
  ]
  edge [
    source 35
    target 1193
  ]
  edge [
    source 35
    target 584
  ]
  edge [
    source 35
    target 1194
  ]
  edge [
    source 35
    target 74
  ]
  edge [
    source 35
    target 178
  ]
  edge [
    source 35
    target 1195
  ]
  edge [
    source 35
    target 1196
  ]
  edge [
    source 35
    target 1197
  ]
  edge [
    source 35
    target 222
  ]
  edge [
    source 35
    target 79
  ]
  edge [
    source 35
    target 62
  ]
  edge [
    source 35
    target 70
  ]
  edge [
    source 35
    target 1198
  ]
  edge [
    source 35
    target 1121
  ]
  edge [
    source 35
    target 1199
  ]
  edge [
    source 35
    target 1200
  ]
  edge [
    source 35
    target 1201
  ]
  edge [
    source 35
    target 389
  ]
  edge [
    source 35
    target 1202
  ]
  edge [
    source 35
    target 1203
  ]
  edge [
    source 35
    target 1204
  ]
  edge [
    source 35
    target 1004
  ]
  edge [
    source 35
    target 1205
  ]
  edge [
    source 35
    target 1206
  ]
  edge [
    source 35
    target 1207
  ]
  edge [
    source 35
    target 1208
  ]
  edge [
    source 35
    target 1209
  ]
  edge [
    source 35
    target 1210
  ]
  edge [
    source 35
    target 1211
  ]
  edge [
    source 35
    target 1212
  ]
  edge [
    source 35
    target 1213
  ]
  edge [
    source 35
    target 131
  ]
  edge [
    source 35
    target 930
  ]
  edge [
    source 35
    target 1214
  ]
  edge [
    source 35
    target 1215
  ]
  edge [
    source 35
    target 1216
  ]
  edge [
    source 35
    target 1217
  ]
  edge [
    source 35
    target 1218
  ]
  edge [
    source 35
    target 1219
  ]
  edge [
    source 35
    target 1220
  ]
  edge [
    source 35
    target 51
  ]
  edge [
    source 35
    target 1221
  ]
  edge [
    source 35
    target 1222
  ]
  edge [
    source 35
    target 1223
  ]
  edge [
    source 35
    target 1224
  ]
  edge [
    source 35
    target 1225
  ]
  edge [
    source 35
    target 1226
  ]
  edge [
    source 35
    target 1227
  ]
  edge [
    source 35
    target 845
  ]
  edge [
    source 35
    target 373
  ]
  edge [
    source 35
    target 1228
  ]
  edge [
    source 35
    target 1101
  ]
  edge [
    source 35
    target 524
  ]
  edge [
    source 35
    target 1229
  ]
  edge [
    source 35
    target 1230
  ]
  edge [
    source 35
    target 1231
  ]
  edge [
    source 35
    target 257
  ]
  edge [
    source 35
    target 1232
  ]
  edge [
    source 35
    target 997
  ]
  edge [
    source 35
    target 1233
  ]
  edge [
    source 35
    target 1234
  ]
  edge [
    source 35
    target 1235
  ]
  edge [
    source 35
    target 1236
  ]
  edge [
    source 35
    target 1237
  ]
  edge [
    source 35
    target 1238
  ]
  edge [
    source 35
    target 1239
  ]
  edge [
    source 35
    target 1240
  ]
  edge [
    source 35
    target 909
  ]
  edge [
    source 35
    target 1241
  ]
  edge [
    source 35
    target 1242
  ]
  edge [
    source 35
    target 1243
  ]
  edge [
    source 35
    target 1244
  ]
  edge [
    source 35
    target 1245
  ]
  edge [
    source 35
    target 1246
  ]
  edge [
    source 35
    target 1247
  ]
  edge [
    source 35
    target 1248
  ]
  edge [
    source 35
    target 1249
  ]
  edge [
    source 35
    target 145
  ]
  edge [
    source 35
    target 1250
  ]
  edge [
    source 35
    target 1251
  ]
  edge [
    source 35
    target 1252
  ]
  edge [
    source 35
    target 1253
  ]
  edge [
    source 35
    target 1254
  ]
  edge [
    source 35
    target 1255
  ]
  edge [
    source 35
    target 1256
  ]
  edge [
    source 35
    target 1257
  ]
  edge [
    source 35
    target 1258
  ]
  edge [
    source 35
    target 1259
  ]
  edge [
    source 35
    target 1260
  ]
  edge [
    source 35
    target 1261
  ]
  edge [
    source 35
    target 1262
  ]
  edge [
    source 35
    target 1263
  ]
  edge [
    source 35
    target 370
  ]
  edge [
    source 35
    target 1264
  ]
  edge [
    source 35
    target 1265
  ]
  edge [
    source 35
    target 1266
  ]
  edge [
    source 35
    target 1267
  ]
  edge [
    source 35
    target 1268
  ]
  edge [
    source 1269
    target 1270
  ]
]
