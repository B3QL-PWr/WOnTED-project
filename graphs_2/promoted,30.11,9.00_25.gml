graph [
  node [
    id 0
    label "historia"
    origin "text"
  ]
  node [
    id 1
    label "praca"
    origin "text"
  ]
  node [
    id 2
    label "automatyk"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "mama"
    origin "text"
  ]
  node [
    id 5
    label "nadzieja"
    origin "text"
  ]
  node [
    id 6
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 7
    label "co&#347;"
    origin "text"
  ]
  node [
    id 8
    label "nauczy&#263;"
    origin "text"
  ]
  node [
    id 9
    label "historiografia"
  ]
  node [
    id 10
    label "nauka_humanistyczna"
  ]
  node [
    id 11
    label "nautologia"
  ]
  node [
    id 12
    label "przedmiot"
  ]
  node [
    id 13
    label "epigrafika"
  ]
  node [
    id 14
    label "muzealnictwo"
  ]
  node [
    id 15
    label "report"
  ]
  node [
    id 16
    label "hista"
  ]
  node [
    id 17
    label "przebiec"
  ]
  node [
    id 18
    label "zabytkoznawstwo"
  ]
  node [
    id 19
    label "historia_gospodarcza"
  ]
  node [
    id 20
    label "motyw"
  ]
  node [
    id 21
    label "kierunek"
  ]
  node [
    id 22
    label "varsavianistyka"
  ]
  node [
    id 23
    label "filigranistyka"
  ]
  node [
    id 24
    label "neografia"
  ]
  node [
    id 25
    label "prezentyzm"
  ]
  node [
    id 26
    label "genealogia"
  ]
  node [
    id 27
    label "ikonografia"
  ]
  node [
    id 28
    label "bizantynistyka"
  ]
  node [
    id 29
    label "epoka"
  ]
  node [
    id 30
    label "historia_sztuki"
  ]
  node [
    id 31
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 32
    label "ruralistyka"
  ]
  node [
    id 33
    label "annalistyka"
  ]
  node [
    id 34
    label "charakter"
  ]
  node [
    id 35
    label "papirologia"
  ]
  node [
    id 36
    label "heraldyka"
  ]
  node [
    id 37
    label "archiwistyka"
  ]
  node [
    id 38
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 39
    label "dyplomatyka"
  ]
  node [
    id 40
    label "czynno&#347;&#263;"
  ]
  node [
    id 41
    label "numizmatyka"
  ]
  node [
    id 42
    label "chronologia"
  ]
  node [
    id 43
    label "wypowied&#378;"
  ]
  node [
    id 44
    label "historyka"
  ]
  node [
    id 45
    label "prozopografia"
  ]
  node [
    id 46
    label "sfragistyka"
  ]
  node [
    id 47
    label "weksylologia"
  ]
  node [
    id 48
    label "paleografia"
  ]
  node [
    id 49
    label "mediewistyka"
  ]
  node [
    id 50
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 51
    label "przebiegni&#281;cie"
  ]
  node [
    id 52
    label "fabu&#322;a"
  ]
  node [
    id 53
    label "koleje_losu"
  ]
  node [
    id 54
    label "&#380;ycie"
  ]
  node [
    id 55
    label "czas"
  ]
  node [
    id 56
    label "zboczenie"
  ]
  node [
    id 57
    label "om&#243;wienie"
  ]
  node [
    id 58
    label "sponiewieranie"
  ]
  node [
    id 59
    label "discipline"
  ]
  node [
    id 60
    label "rzecz"
  ]
  node [
    id 61
    label "omawia&#263;"
  ]
  node [
    id 62
    label "kr&#261;&#380;enie"
  ]
  node [
    id 63
    label "tre&#347;&#263;"
  ]
  node [
    id 64
    label "robienie"
  ]
  node [
    id 65
    label "sponiewiera&#263;"
  ]
  node [
    id 66
    label "element"
  ]
  node [
    id 67
    label "entity"
  ]
  node [
    id 68
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 69
    label "tematyka"
  ]
  node [
    id 70
    label "w&#261;tek"
  ]
  node [
    id 71
    label "zbaczanie"
  ]
  node [
    id 72
    label "program_nauczania"
  ]
  node [
    id 73
    label "om&#243;wi&#263;"
  ]
  node [
    id 74
    label "omawianie"
  ]
  node [
    id 75
    label "thing"
  ]
  node [
    id 76
    label "kultura"
  ]
  node [
    id 77
    label "istota"
  ]
  node [
    id 78
    label "zbacza&#263;"
  ]
  node [
    id 79
    label "zboczy&#263;"
  ]
  node [
    id 80
    label "pos&#322;uchanie"
  ]
  node [
    id 81
    label "s&#261;d"
  ]
  node [
    id 82
    label "sparafrazowanie"
  ]
  node [
    id 83
    label "strawestowa&#263;"
  ]
  node [
    id 84
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 85
    label "trawestowa&#263;"
  ]
  node [
    id 86
    label "sparafrazowa&#263;"
  ]
  node [
    id 87
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 88
    label "sformu&#322;owanie"
  ]
  node [
    id 89
    label "parafrazowanie"
  ]
  node [
    id 90
    label "ozdobnik"
  ]
  node [
    id 91
    label "delimitacja"
  ]
  node [
    id 92
    label "parafrazowa&#263;"
  ]
  node [
    id 93
    label "stylizacja"
  ]
  node [
    id 94
    label "komunikat"
  ]
  node [
    id 95
    label "trawestowanie"
  ]
  node [
    id 96
    label "strawestowanie"
  ]
  node [
    id 97
    label "rezultat"
  ]
  node [
    id 98
    label "przebieg"
  ]
  node [
    id 99
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 100
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 101
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 102
    label "praktyka"
  ]
  node [
    id 103
    label "system"
  ]
  node [
    id 104
    label "przeorientowywanie"
  ]
  node [
    id 105
    label "studia"
  ]
  node [
    id 106
    label "linia"
  ]
  node [
    id 107
    label "bok"
  ]
  node [
    id 108
    label "skr&#281;canie"
  ]
  node [
    id 109
    label "skr&#281;ca&#263;"
  ]
  node [
    id 110
    label "przeorientowywa&#263;"
  ]
  node [
    id 111
    label "orientowanie"
  ]
  node [
    id 112
    label "skr&#281;ci&#263;"
  ]
  node [
    id 113
    label "przeorientowanie"
  ]
  node [
    id 114
    label "zorientowanie"
  ]
  node [
    id 115
    label "przeorientowa&#263;"
  ]
  node [
    id 116
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 117
    label "metoda"
  ]
  node [
    id 118
    label "ty&#322;"
  ]
  node [
    id 119
    label "zorientowa&#263;"
  ]
  node [
    id 120
    label "g&#243;ra"
  ]
  node [
    id 121
    label "orientowa&#263;"
  ]
  node [
    id 122
    label "spos&#243;b"
  ]
  node [
    id 123
    label "ideologia"
  ]
  node [
    id 124
    label "orientacja"
  ]
  node [
    id 125
    label "prz&#243;d"
  ]
  node [
    id 126
    label "bearing"
  ]
  node [
    id 127
    label "skr&#281;cenie"
  ]
  node [
    id 128
    label "aalen"
  ]
  node [
    id 129
    label "jura_wczesna"
  ]
  node [
    id 130
    label "holocen"
  ]
  node [
    id 131
    label "pliocen"
  ]
  node [
    id 132
    label "plejstocen"
  ]
  node [
    id 133
    label "paleocen"
  ]
  node [
    id 134
    label "dzieje"
  ]
  node [
    id 135
    label "bajos"
  ]
  node [
    id 136
    label "kelowej"
  ]
  node [
    id 137
    label "eocen"
  ]
  node [
    id 138
    label "jednostka_geologiczna"
  ]
  node [
    id 139
    label "okres"
  ]
  node [
    id 140
    label "schy&#322;ek"
  ]
  node [
    id 141
    label "miocen"
  ]
  node [
    id 142
    label "&#347;rodkowy_trias"
  ]
  node [
    id 143
    label "term"
  ]
  node [
    id 144
    label "Zeitgeist"
  ]
  node [
    id 145
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 146
    label "wczesny_trias"
  ]
  node [
    id 147
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 148
    label "jura_&#347;rodkowa"
  ]
  node [
    id 149
    label "oligocen"
  ]
  node [
    id 150
    label "w&#281;ze&#322;"
  ]
  node [
    id 151
    label "perypetia"
  ]
  node [
    id 152
    label "opowiadanie"
  ]
  node [
    id 153
    label "datacja"
  ]
  node [
    id 154
    label "dendrochronologia"
  ]
  node [
    id 155
    label "kolejno&#347;&#263;"
  ]
  node [
    id 156
    label "plastyka"
  ]
  node [
    id 157
    label "&#347;redniowiecze"
  ]
  node [
    id 158
    label "descendencja"
  ]
  node [
    id 159
    label "drzewo_genealogiczne"
  ]
  node [
    id 160
    label "procedencja"
  ]
  node [
    id 161
    label "pochodzenie"
  ]
  node [
    id 162
    label "medal"
  ]
  node [
    id 163
    label "kolekcjonerstwo"
  ]
  node [
    id 164
    label "numismatics"
  ]
  node [
    id 165
    label "archeologia"
  ]
  node [
    id 166
    label "archiwoznawstwo"
  ]
  node [
    id 167
    label "Byzantine_Empire"
  ]
  node [
    id 168
    label "pismo"
  ]
  node [
    id 169
    label "brachygrafia"
  ]
  node [
    id 170
    label "architektura"
  ]
  node [
    id 171
    label "nauka"
  ]
  node [
    id 172
    label "oksza"
  ]
  node [
    id 173
    label "pas"
  ]
  node [
    id 174
    label "s&#322;up"
  ]
  node [
    id 175
    label "barwa_heraldyczna"
  ]
  node [
    id 176
    label "herb"
  ]
  node [
    id 177
    label "or&#281;&#380;"
  ]
  node [
    id 178
    label "museum"
  ]
  node [
    id 179
    label "bibliologia"
  ]
  node [
    id 180
    label "historiography"
  ]
  node [
    id 181
    label "pi&#347;miennictwo"
  ]
  node [
    id 182
    label "metodologia"
  ]
  node [
    id 183
    label "fraza"
  ]
  node [
    id 184
    label "temat"
  ]
  node [
    id 185
    label "wydarzenie"
  ]
  node [
    id 186
    label "melodia"
  ]
  node [
    id 187
    label "cecha"
  ]
  node [
    id 188
    label "przyczyna"
  ]
  node [
    id 189
    label "sytuacja"
  ]
  node [
    id 190
    label "ozdoba"
  ]
  node [
    id 191
    label "umowa"
  ]
  node [
    id 192
    label "cover"
  ]
  node [
    id 193
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 194
    label "zbi&#243;r"
  ]
  node [
    id 195
    label "cz&#322;owiek"
  ]
  node [
    id 196
    label "osobowo&#347;&#263;"
  ]
  node [
    id 197
    label "psychika"
  ]
  node [
    id 198
    label "posta&#263;"
  ]
  node [
    id 199
    label "kompleksja"
  ]
  node [
    id 200
    label "fizjonomia"
  ]
  node [
    id 201
    label "zjawisko"
  ]
  node [
    id 202
    label "activity"
  ]
  node [
    id 203
    label "bezproblemowy"
  ]
  node [
    id 204
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 205
    label "przeby&#263;"
  ]
  node [
    id 206
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 207
    label "run"
  ]
  node [
    id 208
    label "proceed"
  ]
  node [
    id 209
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 210
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 211
    label "przemierzy&#263;"
  ]
  node [
    id 212
    label "fly"
  ]
  node [
    id 213
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 214
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 215
    label "przesun&#261;&#263;"
  ]
  node [
    id 216
    label "przemkni&#281;cie"
  ]
  node [
    id 217
    label "zabrzmienie"
  ]
  node [
    id 218
    label "przebycie"
  ]
  node [
    id 219
    label "zdarzenie_si&#281;"
  ]
  node [
    id 220
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 221
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 222
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 223
    label "najem"
  ]
  node [
    id 224
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 225
    label "zak&#322;ad"
  ]
  node [
    id 226
    label "stosunek_pracy"
  ]
  node [
    id 227
    label "benedykty&#324;ski"
  ]
  node [
    id 228
    label "poda&#380;_pracy"
  ]
  node [
    id 229
    label "pracowanie"
  ]
  node [
    id 230
    label "tyrka"
  ]
  node [
    id 231
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 232
    label "wytw&#243;r"
  ]
  node [
    id 233
    label "miejsce"
  ]
  node [
    id 234
    label "zaw&#243;d"
  ]
  node [
    id 235
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 236
    label "tynkarski"
  ]
  node [
    id 237
    label "pracowa&#263;"
  ]
  node [
    id 238
    label "zmiana"
  ]
  node [
    id 239
    label "czynnik_produkcji"
  ]
  node [
    id 240
    label "zobowi&#261;zanie"
  ]
  node [
    id 241
    label "kierownictwo"
  ]
  node [
    id 242
    label "siedziba"
  ]
  node [
    id 243
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 244
    label "p&#322;&#243;d"
  ]
  node [
    id 245
    label "work"
  ]
  node [
    id 246
    label "warunek_lokalowy"
  ]
  node [
    id 247
    label "plac"
  ]
  node [
    id 248
    label "location"
  ]
  node [
    id 249
    label "uwaga"
  ]
  node [
    id 250
    label "przestrze&#324;"
  ]
  node [
    id 251
    label "status"
  ]
  node [
    id 252
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 253
    label "chwila"
  ]
  node [
    id 254
    label "cia&#322;o"
  ]
  node [
    id 255
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 256
    label "rz&#261;d"
  ]
  node [
    id 257
    label "stosunek_prawny"
  ]
  node [
    id 258
    label "oblig"
  ]
  node [
    id 259
    label "uregulowa&#263;"
  ]
  node [
    id 260
    label "oddzia&#322;anie"
  ]
  node [
    id 261
    label "occupation"
  ]
  node [
    id 262
    label "duty"
  ]
  node [
    id 263
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 264
    label "zapowied&#378;"
  ]
  node [
    id 265
    label "obowi&#261;zek"
  ]
  node [
    id 266
    label "statement"
  ]
  node [
    id 267
    label "zapewnienie"
  ]
  node [
    id 268
    label "miejsce_pracy"
  ]
  node [
    id 269
    label "&#321;ubianka"
  ]
  node [
    id 270
    label "dzia&#322;_personalny"
  ]
  node [
    id 271
    label "Kreml"
  ]
  node [
    id 272
    label "Bia&#322;y_Dom"
  ]
  node [
    id 273
    label "budynek"
  ]
  node [
    id 274
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 275
    label "sadowisko"
  ]
  node [
    id 276
    label "zak&#322;adka"
  ]
  node [
    id 277
    label "jednostka_organizacyjna"
  ]
  node [
    id 278
    label "instytucja"
  ]
  node [
    id 279
    label "wyko&#324;czenie"
  ]
  node [
    id 280
    label "firma"
  ]
  node [
    id 281
    label "czyn"
  ]
  node [
    id 282
    label "company"
  ]
  node [
    id 283
    label "instytut"
  ]
  node [
    id 284
    label "cierpliwy"
  ]
  node [
    id 285
    label "mozolny"
  ]
  node [
    id 286
    label "wytrwa&#322;y"
  ]
  node [
    id 287
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 288
    label "benedykty&#324;sko"
  ]
  node [
    id 289
    label "typowy"
  ]
  node [
    id 290
    label "po_benedykty&#324;sku"
  ]
  node [
    id 291
    label "rewizja"
  ]
  node [
    id 292
    label "passage"
  ]
  node [
    id 293
    label "oznaka"
  ]
  node [
    id 294
    label "change"
  ]
  node [
    id 295
    label "ferment"
  ]
  node [
    id 296
    label "komplet"
  ]
  node [
    id 297
    label "anatomopatolog"
  ]
  node [
    id 298
    label "zmianka"
  ]
  node [
    id 299
    label "amendment"
  ]
  node [
    id 300
    label "odmienianie"
  ]
  node [
    id 301
    label "tura"
  ]
  node [
    id 302
    label "przepracowanie_si&#281;"
  ]
  node [
    id 303
    label "zarz&#261;dzanie"
  ]
  node [
    id 304
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 305
    label "podlizanie_si&#281;"
  ]
  node [
    id 306
    label "dopracowanie"
  ]
  node [
    id 307
    label "podlizywanie_si&#281;"
  ]
  node [
    id 308
    label "uruchamianie"
  ]
  node [
    id 309
    label "dzia&#322;anie"
  ]
  node [
    id 310
    label "d&#261;&#380;enie"
  ]
  node [
    id 311
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 312
    label "uruchomienie"
  ]
  node [
    id 313
    label "nakr&#281;canie"
  ]
  node [
    id 314
    label "funkcjonowanie"
  ]
  node [
    id 315
    label "tr&#243;jstronny"
  ]
  node [
    id 316
    label "postaranie_si&#281;"
  ]
  node [
    id 317
    label "odpocz&#281;cie"
  ]
  node [
    id 318
    label "nakr&#281;cenie"
  ]
  node [
    id 319
    label "zatrzymanie"
  ]
  node [
    id 320
    label "spracowanie_si&#281;"
  ]
  node [
    id 321
    label "skakanie"
  ]
  node [
    id 322
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 323
    label "podtrzymywanie"
  ]
  node [
    id 324
    label "w&#322;&#261;czanie"
  ]
  node [
    id 325
    label "zaprz&#281;ganie"
  ]
  node [
    id 326
    label "podejmowanie"
  ]
  node [
    id 327
    label "maszyna"
  ]
  node [
    id 328
    label "wyrabianie"
  ]
  node [
    id 329
    label "dzianie_si&#281;"
  ]
  node [
    id 330
    label "use"
  ]
  node [
    id 331
    label "przepracowanie"
  ]
  node [
    id 332
    label "poruszanie_si&#281;"
  ]
  node [
    id 333
    label "funkcja"
  ]
  node [
    id 334
    label "impact"
  ]
  node [
    id 335
    label "przepracowywanie"
  ]
  node [
    id 336
    label "awansowanie"
  ]
  node [
    id 337
    label "courtship"
  ]
  node [
    id 338
    label "zapracowanie"
  ]
  node [
    id 339
    label "wyrobienie"
  ]
  node [
    id 340
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 341
    label "w&#322;&#261;czenie"
  ]
  node [
    id 342
    label "zawodoznawstwo"
  ]
  node [
    id 343
    label "emocja"
  ]
  node [
    id 344
    label "office"
  ]
  node [
    id 345
    label "kwalifikacje"
  ]
  node [
    id 346
    label "craft"
  ]
  node [
    id 347
    label "transakcja"
  ]
  node [
    id 348
    label "endeavor"
  ]
  node [
    id 349
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 350
    label "mie&#263;_miejsce"
  ]
  node [
    id 351
    label "podejmowa&#263;"
  ]
  node [
    id 352
    label "dziama&#263;"
  ]
  node [
    id 353
    label "do"
  ]
  node [
    id 354
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 355
    label "bangla&#263;"
  ]
  node [
    id 356
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 357
    label "dzia&#322;a&#263;"
  ]
  node [
    id 358
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 359
    label "tryb"
  ]
  node [
    id 360
    label "funkcjonowa&#263;"
  ]
  node [
    id 361
    label "biuro"
  ]
  node [
    id 362
    label "lead"
  ]
  node [
    id 363
    label "zesp&#243;&#322;"
  ]
  node [
    id 364
    label "w&#322;adza"
  ]
  node [
    id 365
    label "in&#380;ynier"
  ]
  node [
    id 366
    label "technik"
  ]
  node [
    id 367
    label "inteligent"
  ]
  node [
    id 368
    label "tytu&#322;"
  ]
  node [
    id 369
    label "Tesla"
  ]
  node [
    id 370
    label "fachowiec"
  ]
  node [
    id 371
    label "Pierre-&#201;mile_Martin"
  ]
  node [
    id 372
    label "praktyk"
  ]
  node [
    id 373
    label "przodkini"
  ]
  node [
    id 374
    label "matka_zast&#281;pcza"
  ]
  node [
    id 375
    label "matczysko"
  ]
  node [
    id 376
    label "rodzice"
  ]
  node [
    id 377
    label "stara"
  ]
  node [
    id 378
    label "macierz"
  ]
  node [
    id 379
    label "rodzic"
  ]
  node [
    id 380
    label "Matka_Boska"
  ]
  node [
    id 381
    label "macocha"
  ]
  node [
    id 382
    label "starzy"
  ]
  node [
    id 383
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 384
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 385
    label "pokolenie"
  ]
  node [
    id 386
    label "wapniaki"
  ]
  node [
    id 387
    label "krewna"
  ]
  node [
    id 388
    label "opiekun"
  ]
  node [
    id 389
    label "wapniak"
  ]
  node [
    id 390
    label "rodzic_chrzestny"
  ]
  node [
    id 391
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 392
    label "matka"
  ]
  node [
    id 393
    label "&#380;ona"
  ]
  node [
    id 394
    label "kobieta"
  ]
  node [
    id 395
    label "partnerka"
  ]
  node [
    id 396
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 397
    label "matuszka"
  ]
  node [
    id 398
    label "parametryzacja"
  ]
  node [
    id 399
    label "pa&#324;stwo"
  ]
  node [
    id 400
    label "poj&#281;cie"
  ]
  node [
    id 401
    label "mod"
  ]
  node [
    id 402
    label "patriota"
  ]
  node [
    id 403
    label "m&#281;&#380;atka"
  ]
  node [
    id 404
    label "szansa"
  ]
  node [
    id 405
    label "spoczywa&#263;"
  ]
  node [
    id 406
    label "&#347;wiate&#322;ko_w_tunelu"
  ]
  node [
    id 407
    label "oczekiwanie"
  ]
  node [
    id 408
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 409
    label "wierzy&#263;"
  ]
  node [
    id 410
    label "posiada&#263;"
  ]
  node [
    id 411
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 412
    label "egzekutywa"
  ]
  node [
    id 413
    label "potencja&#322;"
  ]
  node [
    id 414
    label "wyb&#243;r"
  ]
  node [
    id 415
    label "prospect"
  ]
  node [
    id 416
    label "ability"
  ]
  node [
    id 417
    label "obliczeniowo"
  ]
  node [
    id 418
    label "alternatywa"
  ]
  node [
    id 419
    label "operator_modalny"
  ]
  node [
    id 420
    label "wytrzymanie"
  ]
  node [
    id 421
    label "czekanie"
  ]
  node [
    id 422
    label "spodziewanie_si&#281;"
  ]
  node [
    id 423
    label "anticipation"
  ]
  node [
    id 424
    label "przewidywanie"
  ]
  node [
    id 425
    label "wytrzymywanie"
  ]
  node [
    id 426
    label "spotykanie"
  ]
  node [
    id 427
    label "wait"
  ]
  node [
    id 428
    label "wierza&#263;"
  ]
  node [
    id 429
    label "trust"
  ]
  node [
    id 430
    label "powierzy&#263;"
  ]
  node [
    id 431
    label "wyznawa&#263;"
  ]
  node [
    id 432
    label "czu&#263;"
  ]
  node [
    id 433
    label "faith"
  ]
  node [
    id 434
    label "chowa&#263;"
  ]
  node [
    id 435
    label "powierza&#263;"
  ]
  node [
    id 436
    label "uznawa&#263;"
  ]
  node [
    id 437
    label "by&#263;"
  ]
  node [
    id 438
    label "lie"
  ]
  node [
    id 439
    label "odpoczywa&#263;"
  ]
  node [
    id 440
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 441
    label "gr&#243;b"
  ]
  node [
    id 442
    label "cosik"
  ]
  node [
    id 443
    label "instruct"
  ]
  node [
    id 444
    label "wyszkoli&#263;"
  ]
  node [
    id 445
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 446
    label "pom&#243;c"
  ]
  node [
    id 447
    label "train"
  ]
  node [
    id 448
    label "o&#347;wieci&#263;"
  ]
  node [
    id 449
    label "chemia"
  ]
  node [
    id 450
    label "spowodowa&#263;"
  ]
  node [
    id 451
    label "reakcja_chemiczna"
  ]
  node [
    id 452
    label "act"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
]
