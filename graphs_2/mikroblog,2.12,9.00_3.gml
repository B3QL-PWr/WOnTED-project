graph [
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ten"
    origin "text"
  ]
  node [
    id 3
    label "historia"
    origin "text"
  ]
  node [
    id 4
    label "przeczyta&#263;"
    origin "text"
  ]
  node [
    id 5
    label "tutaj"
    origin "text"
  ]
  node [
    id 6
    label "godzina"
  ]
  node [
    id 7
    label "time"
  ]
  node [
    id 8
    label "doba"
  ]
  node [
    id 9
    label "p&#243;&#322;godzina"
  ]
  node [
    id 10
    label "jednostka_czasu"
  ]
  node [
    id 11
    label "czas"
  ]
  node [
    id 12
    label "minuta"
  ]
  node [
    id 13
    label "kwadrans"
  ]
  node [
    id 14
    label "Rzym_Zachodni"
  ]
  node [
    id 15
    label "whole"
  ]
  node [
    id 16
    label "ilo&#347;&#263;"
  ]
  node [
    id 17
    label "element"
  ]
  node [
    id 18
    label "Rzym_Wschodni"
  ]
  node [
    id 19
    label "urz&#261;dzenie"
  ]
  node [
    id 20
    label "r&#243;&#380;niczka"
  ]
  node [
    id 21
    label "&#347;rodowisko"
  ]
  node [
    id 22
    label "przedmiot"
  ]
  node [
    id 23
    label "materia"
  ]
  node [
    id 24
    label "szambo"
  ]
  node [
    id 25
    label "aspo&#322;eczny"
  ]
  node [
    id 26
    label "component"
  ]
  node [
    id 27
    label "szkodnik"
  ]
  node [
    id 28
    label "gangsterski"
  ]
  node [
    id 29
    label "poj&#281;cie"
  ]
  node [
    id 30
    label "underworld"
  ]
  node [
    id 31
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 32
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 33
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 34
    label "rozmiar"
  ]
  node [
    id 35
    label "part"
  ]
  node [
    id 36
    label "kom&#243;rka"
  ]
  node [
    id 37
    label "furnishing"
  ]
  node [
    id 38
    label "zabezpieczenie"
  ]
  node [
    id 39
    label "zrobienie"
  ]
  node [
    id 40
    label "wyrz&#261;dzenie"
  ]
  node [
    id 41
    label "zagospodarowanie"
  ]
  node [
    id 42
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 43
    label "ig&#322;a"
  ]
  node [
    id 44
    label "narz&#281;dzie"
  ]
  node [
    id 45
    label "wirnik"
  ]
  node [
    id 46
    label "aparatura"
  ]
  node [
    id 47
    label "system_energetyczny"
  ]
  node [
    id 48
    label "impulsator"
  ]
  node [
    id 49
    label "mechanizm"
  ]
  node [
    id 50
    label "sprz&#281;t"
  ]
  node [
    id 51
    label "czynno&#347;&#263;"
  ]
  node [
    id 52
    label "blokowanie"
  ]
  node [
    id 53
    label "set"
  ]
  node [
    id 54
    label "zablokowanie"
  ]
  node [
    id 55
    label "przygotowanie"
  ]
  node [
    id 56
    label "komora"
  ]
  node [
    id 57
    label "j&#281;zyk"
  ]
  node [
    id 58
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 59
    label "okre&#347;lony"
  ]
  node [
    id 60
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 61
    label "wiadomy"
  ]
  node [
    id 62
    label "historiografia"
  ]
  node [
    id 63
    label "nauka_humanistyczna"
  ]
  node [
    id 64
    label "nautologia"
  ]
  node [
    id 65
    label "epigrafika"
  ]
  node [
    id 66
    label "muzealnictwo"
  ]
  node [
    id 67
    label "report"
  ]
  node [
    id 68
    label "hista"
  ]
  node [
    id 69
    label "przebiec"
  ]
  node [
    id 70
    label "zabytkoznawstwo"
  ]
  node [
    id 71
    label "historia_gospodarcza"
  ]
  node [
    id 72
    label "motyw"
  ]
  node [
    id 73
    label "kierunek"
  ]
  node [
    id 74
    label "varsavianistyka"
  ]
  node [
    id 75
    label "filigranistyka"
  ]
  node [
    id 76
    label "neografia"
  ]
  node [
    id 77
    label "prezentyzm"
  ]
  node [
    id 78
    label "genealogia"
  ]
  node [
    id 79
    label "ikonografia"
  ]
  node [
    id 80
    label "bizantynistyka"
  ]
  node [
    id 81
    label "epoka"
  ]
  node [
    id 82
    label "historia_sztuki"
  ]
  node [
    id 83
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 84
    label "ruralistyka"
  ]
  node [
    id 85
    label "annalistyka"
  ]
  node [
    id 86
    label "charakter"
  ]
  node [
    id 87
    label "papirologia"
  ]
  node [
    id 88
    label "heraldyka"
  ]
  node [
    id 89
    label "archiwistyka"
  ]
  node [
    id 90
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 91
    label "dyplomatyka"
  ]
  node [
    id 92
    label "numizmatyka"
  ]
  node [
    id 93
    label "chronologia"
  ]
  node [
    id 94
    label "wypowied&#378;"
  ]
  node [
    id 95
    label "historyka"
  ]
  node [
    id 96
    label "prozopografia"
  ]
  node [
    id 97
    label "sfragistyka"
  ]
  node [
    id 98
    label "weksylologia"
  ]
  node [
    id 99
    label "paleografia"
  ]
  node [
    id 100
    label "mediewistyka"
  ]
  node [
    id 101
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 102
    label "przebiegni&#281;cie"
  ]
  node [
    id 103
    label "fabu&#322;a"
  ]
  node [
    id 104
    label "koleje_losu"
  ]
  node [
    id 105
    label "&#380;ycie"
  ]
  node [
    id 106
    label "zboczenie"
  ]
  node [
    id 107
    label "om&#243;wienie"
  ]
  node [
    id 108
    label "sponiewieranie"
  ]
  node [
    id 109
    label "discipline"
  ]
  node [
    id 110
    label "rzecz"
  ]
  node [
    id 111
    label "omawia&#263;"
  ]
  node [
    id 112
    label "kr&#261;&#380;enie"
  ]
  node [
    id 113
    label "tre&#347;&#263;"
  ]
  node [
    id 114
    label "robienie"
  ]
  node [
    id 115
    label "sponiewiera&#263;"
  ]
  node [
    id 116
    label "entity"
  ]
  node [
    id 117
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 118
    label "tematyka"
  ]
  node [
    id 119
    label "w&#261;tek"
  ]
  node [
    id 120
    label "zbaczanie"
  ]
  node [
    id 121
    label "program_nauczania"
  ]
  node [
    id 122
    label "om&#243;wi&#263;"
  ]
  node [
    id 123
    label "omawianie"
  ]
  node [
    id 124
    label "thing"
  ]
  node [
    id 125
    label "kultura"
  ]
  node [
    id 126
    label "istota"
  ]
  node [
    id 127
    label "zbacza&#263;"
  ]
  node [
    id 128
    label "zboczy&#263;"
  ]
  node [
    id 129
    label "pos&#322;uchanie"
  ]
  node [
    id 130
    label "s&#261;d"
  ]
  node [
    id 131
    label "sparafrazowanie"
  ]
  node [
    id 132
    label "strawestowa&#263;"
  ]
  node [
    id 133
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 134
    label "trawestowa&#263;"
  ]
  node [
    id 135
    label "sparafrazowa&#263;"
  ]
  node [
    id 136
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 137
    label "sformu&#322;owanie"
  ]
  node [
    id 138
    label "parafrazowanie"
  ]
  node [
    id 139
    label "ozdobnik"
  ]
  node [
    id 140
    label "delimitacja"
  ]
  node [
    id 141
    label "parafrazowa&#263;"
  ]
  node [
    id 142
    label "stylizacja"
  ]
  node [
    id 143
    label "komunikat"
  ]
  node [
    id 144
    label "trawestowanie"
  ]
  node [
    id 145
    label "strawestowanie"
  ]
  node [
    id 146
    label "rezultat"
  ]
  node [
    id 147
    label "przebieg"
  ]
  node [
    id 148
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 149
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 150
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 151
    label "praktyka"
  ]
  node [
    id 152
    label "system"
  ]
  node [
    id 153
    label "przeorientowywanie"
  ]
  node [
    id 154
    label "studia"
  ]
  node [
    id 155
    label "linia"
  ]
  node [
    id 156
    label "bok"
  ]
  node [
    id 157
    label "skr&#281;canie"
  ]
  node [
    id 158
    label "skr&#281;ca&#263;"
  ]
  node [
    id 159
    label "przeorientowywa&#263;"
  ]
  node [
    id 160
    label "orientowanie"
  ]
  node [
    id 161
    label "skr&#281;ci&#263;"
  ]
  node [
    id 162
    label "przeorientowanie"
  ]
  node [
    id 163
    label "zorientowanie"
  ]
  node [
    id 164
    label "przeorientowa&#263;"
  ]
  node [
    id 165
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 166
    label "metoda"
  ]
  node [
    id 167
    label "ty&#322;"
  ]
  node [
    id 168
    label "zorientowa&#263;"
  ]
  node [
    id 169
    label "g&#243;ra"
  ]
  node [
    id 170
    label "orientowa&#263;"
  ]
  node [
    id 171
    label "spos&#243;b"
  ]
  node [
    id 172
    label "ideologia"
  ]
  node [
    id 173
    label "orientacja"
  ]
  node [
    id 174
    label "prz&#243;d"
  ]
  node [
    id 175
    label "bearing"
  ]
  node [
    id 176
    label "skr&#281;cenie"
  ]
  node [
    id 177
    label "aalen"
  ]
  node [
    id 178
    label "jura_wczesna"
  ]
  node [
    id 179
    label "holocen"
  ]
  node [
    id 180
    label "pliocen"
  ]
  node [
    id 181
    label "plejstocen"
  ]
  node [
    id 182
    label "paleocen"
  ]
  node [
    id 183
    label "dzieje"
  ]
  node [
    id 184
    label "bajos"
  ]
  node [
    id 185
    label "kelowej"
  ]
  node [
    id 186
    label "eocen"
  ]
  node [
    id 187
    label "jednostka_geologiczna"
  ]
  node [
    id 188
    label "okres"
  ]
  node [
    id 189
    label "schy&#322;ek"
  ]
  node [
    id 190
    label "miocen"
  ]
  node [
    id 191
    label "&#347;rodkowy_trias"
  ]
  node [
    id 192
    label "term"
  ]
  node [
    id 193
    label "Zeitgeist"
  ]
  node [
    id 194
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 195
    label "wczesny_trias"
  ]
  node [
    id 196
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 197
    label "jura_&#347;rodkowa"
  ]
  node [
    id 198
    label "oligocen"
  ]
  node [
    id 199
    label "w&#281;ze&#322;"
  ]
  node [
    id 200
    label "perypetia"
  ]
  node [
    id 201
    label "opowiadanie"
  ]
  node [
    id 202
    label "datacja"
  ]
  node [
    id 203
    label "dendrochronologia"
  ]
  node [
    id 204
    label "kolejno&#347;&#263;"
  ]
  node [
    id 205
    label "plastyka"
  ]
  node [
    id 206
    label "&#347;redniowiecze"
  ]
  node [
    id 207
    label "descendencja"
  ]
  node [
    id 208
    label "drzewo_genealogiczne"
  ]
  node [
    id 209
    label "procedencja"
  ]
  node [
    id 210
    label "pochodzenie"
  ]
  node [
    id 211
    label "medal"
  ]
  node [
    id 212
    label "kolekcjonerstwo"
  ]
  node [
    id 213
    label "numismatics"
  ]
  node [
    id 214
    label "archeologia"
  ]
  node [
    id 215
    label "archiwoznawstwo"
  ]
  node [
    id 216
    label "Byzantine_Empire"
  ]
  node [
    id 217
    label "pismo"
  ]
  node [
    id 218
    label "brachygrafia"
  ]
  node [
    id 219
    label "architektura"
  ]
  node [
    id 220
    label "nauka"
  ]
  node [
    id 221
    label "oksza"
  ]
  node [
    id 222
    label "pas"
  ]
  node [
    id 223
    label "s&#322;up"
  ]
  node [
    id 224
    label "barwa_heraldyczna"
  ]
  node [
    id 225
    label "herb"
  ]
  node [
    id 226
    label "or&#281;&#380;"
  ]
  node [
    id 227
    label "museum"
  ]
  node [
    id 228
    label "bibliologia"
  ]
  node [
    id 229
    label "historiography"
  ]
  node [
    id 230
    label "pi&#347;miennictwo"
  ]
  node [
    id 231
    label "metodologia"
  ]
  node [
    id 232
    label "fraza"
  ]
  node [
    id 233
    label "temat"
  ]
  node [
    id 234
    label "wydarzenie"
  ]
  node [
    id 235
    label "melodia"
  ]
  node [
    id 236
    label "cecha"
  ]
  node [
    id 237
    label "przyczyna"
  ]
  node [
    id 238
    label "sytuacja"
  ]
  node [
    id 239
    label "ozdoba"
  ]
  node [
    id 240
    label "umowa"
  ]
  node [
    id 241
    label "cover"
  ]
  node [
    id 242
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 243
    label "zbi&#243;r"
  ]
  node [
    id 244
    label "cz&#322;owiek"
  ]
  node [
    id 245
    label "osobowo&#347;&#263;"
  ]
  node [
    id 246
    label "psychika"
  ]
  node [
    id 247
    label "posta&#263;"
  ]
  node [
    id 248
    label "kompleksja"
  ]
  node [
    id 249
    label "fizjonomia"
  ]
  node [
    id 250
    label "zjawisko"
  ]
  node [
    id 251
    label "activity"
  ]
  node [
    id 252
    label "bezproblemowy"
  ]
  node [
    id 253
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 254
    label "przeby&#263;"
  ]
  node [
    id 255
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 256
    label "run"
  ]
  node [
    id 257
    label "proceed"
  ]
  node [
    id 258
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 259
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 260
    label "przemierzy&#263;"
  ]
  node [
    id 261
    label "fly"
  ]
  node [
    id 262
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 263
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 264
    label "przesun&#261;&#263;"
  ]
  node [
    id 265
    label "przemkni&#281;cie"
  ]
  node [
    id 266
    label "zabrzmienie"
  ]
  node [
    id 267
    label "przebycie"
  ]
  node [
    id 268
    label "zdarzenie_si&#281;"
  ]
  node [
    id 269
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 270
    label "pozna&#263;"
  ]
  node [
    id 271
    label "wywr&#243;&#380;y&#263;"
  ]
  node [
    id 272
    label "zaobserwowa&#263;"
  ]
  node [
    id 273
    label "read"
  ]
  node [
    id 274
    label "odczyta&#263;"
  ]
  node [
    id 275
    label "przetworzy&#263;"
  ]
  node [
    id 276
    label "przewidzie&#263;"
  ]
  node [
    id 277
    label "bode"
  ]
  node [
    id 278
    label "wywnioskowa&#263;"
  ]
  node [
    id 279
    label "przepowiedzie&#263;"
  ]
  node [
    id 280
    label "watch"
  ]
  node [
    id 281
    label "zobaczy&#263;"
  ]
  node [
    id 282
    label "zinterpretowa&#263;"
  ]
  node [
    id 283
    label "sprawdzi&#263;"
  ]
  node [
    id 284
    label "poda&#263;"
  ]
  node [
    id 285
    label "wyzyska&#263;"
  ]
  node [
    id 286
    label "opracowa&#263;"
  ]
  node [
    id 287
    label "convert"
  ]
  node [
    id 288
    label "stworzy&#263;"
  ]
  node [
    id 289
    label "zrozumie&#263;"
  ]
  node [
    id 290
    label "feel"
  ]
  node [
    id 291
    label "topographic_point"
  ]
  node [
    id 292
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 293
    label "visualize"
  ]
  node [
    id 294
    label "przyswoi&#263;"
  ]
  node [
    id 295
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 296
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 297
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 298
    label "teach"
  ]
  node [
    id 299
    label "experience"
  ]
  node [
    id 300
    label "tam"
  ]
  node [
    id 301
    label "tu"
  ]
  node [
    id 302
    label "2"
  ]
  node [
    id 303
    label "J&#243;zef"
  ]
  node [
    id 304
    label "k"
  ]
  node [
    id 305
    label "sp&#243;&#322;dzielnia"
  ]
  node [
    id 306
    label "k&#243;&#322;ko"
  ]
  node [
    id 307
    label "rolniczy"
  ]
  node [
    id 308
    label "Star"
  ]
  node [
    id 309
    label "klapa"
  ]
  node [
    id 310
    label "d&#261;browa"
  ]
  node [
    id 311
    label "tarnowski"
  ]
  node [
    id 312
    label "Leszek"
  ]
  node [
    id 313
    label "Miller"
  ]
  node [
    id 314
    label "Tankpol"
  ]
  node [
    id 315
    label "Roman"
  ]
  node [
    id 316
    label "M"
  ]
  node [
    id 317
    label "Andrzej"
  ]
  node [
    id 318
    label "&#321;"
  ]
  node [
    id 319
    label "komenda"
  ]
  node [
    id 320
    label "powiatowy"
  ]
  node [
    id 321
    label "policja"
  ]
  node [
    id 322
    label "Bogus&#322;aw"
  ]
  node [
    id 323
    label "P"
  ]
  node [
    id 324
    label "Renata"
  ]
  node [
    id 325
    label "gram"
  ]
  node [
    id 326
    label "D"
  ]
  node [
    id 327
    label "stra&#380;"
  ]
  node [
    id 328
    label "graniczny"
  ]
  node [
    id 329
    label "Szczucina"
  ]
  node [
    id 330
    label "Witaszek"
  ]
  node [
    id 331
    label "Grzegorz"
  ]
  node [
    id 332
    label "J"
  ]
  node [
    id 333
    label "Jerzy"
  ]
  node [
    id 334
    label "sekunda"
  ]
  node [
    id 335
    label "Jacek"
  ]
  node [
    id 336
    label "Waldemar"
  ]
  node [
    id 337
    label "marek"
  ]
  node [
    id 338
    label "Pawe&#322;"
  ]
  node [
    id 339
    label "w"
  ]
  node [
    id 340
    label "longin"
  ]
  node [
    id 341
    label "F"
  ]
  node [
    id 342
    label "Maciej"
  ]
  node [
    id 343
    label "C"
  ]
  node [
    id 344
    label "Krzysztof"
  ]
  node [
    id 345
    label "bit"
  ]
  node [
    id 346
    label "Robert"
  ]
  node [
    id 347
    label "N"
  ]
  node [
    id 348
    label "Boles&#322;aw"
  ]
  node [
    id 349
    label "postscriptum"
  ]
  node [
    id 350
    label "Papu&#347;ny"
  ]
  node [
    id 351
    label "zarzuci&#263;"
  ]
  node [
    id 352
    label "Ryszard"
  ]
  node [
    id 353
    label "Witaszka"
  ]
  node [
    id 354
    label "Tadeusz"
  ]
  node [
    id 355
    label "drab"
  ]
  node [
    id 356
    label "archiwum"
  ]
  node [
    id 357
    label "X"
  ]
  node [
    id 358
    label "kapela"
  ]
  node [
    id 359
    label "Iwona"
  ]
  node [
    id 360
    label "cygan"
  ]
  node [
    id 361
    label "okr&#281;gowy"
  ]
  node [
    id 362
    label "prokuratura"
  ]
  node [
    id 363
    label "krajowy"
  ]
  node [
    id 364
    label "Rzesz&#243;w"
  ]
  node [
    id 365
    label "Roberto"
  ]
  node [
    id 366
    label "m&#322;ody"
  ]
  node [
    id 367
    label "oko&#322;o"
  ]
  node [
    id 368
    label "obok"
  ]
  node [
    id 369
    label "u"
  ]
  node [
    id 370
    label "trabant"
  ]
  node [
    id 371
    label "wywie&#378;&#263;"
  ]
  node [
    id 372
    label "Witaszkiem"
  ]
  node [
    id 373
    label "Stany"
  ]
  node [
    id 374
    label "zjednoczy&#263;"
  ]
  node [
    id 375
    label "do"
  ]
  node [
    id 376
    label "klasa"
  ]
  node [
    id 377
    label "Renat"
  ]
  node [
    id 378
    label "Barbar"
  ]
  node [
    id 379
    label "Piwnik"
  ]
  node [
    id 380
    label "Piotr"
  ]
  node [
    id 381
    label "Krupi&#324;ski"
  ]
  node [
    id 382
    label "ma&#322;opolski"
  ]
  node [
    id 383
    label "wydzia&#322;"
  ]
  node [
    id 384
    label "sprawa"
  ]
  node [
    id 385
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 386
    label "zorganizowa&#263;"
  ]
  node [
    id 387
    label "i"
  ]
  node [
    id 388
    label "korupcja"
  ]
  node [
    id 389
    label "apelacyjny"
  ]
  node [
    id 390
    label "TVN"
  ]
  node [
    id 391
    label "info"
  ]
  node [
    id 392
    label "rejestr"
  ]
  node [
    id 393
    label "sprawca"
  ]
  node [
    id 394
    label "przest&#281;pstwo"
  ]
  node [
    id 395
    label "t&#322;o"
  ]
  node [
    id 396
    label "seksualny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 130
    target 361
  ]
  edge [
    source 130
    target 339
  ]
  edge [
    source 130
    target 364
  ]
  edge [
    source 130
    target 389
  ]
  edge [
    source 300
    target 366
  ]
  edge [
    source 300
    target 309
  ]
  edge [
    source 303
    target 304
  ]
  edge [
    source 304
    target 317
  ]
  edge [
    source 304
    target 337
  ]
  edge [
    source 304
    target 338
  ]
  edge [
    source 304
    target 365
  ]
  edge [
    source 304
    target 346
  ]
  edge [
    source 304
    target 368
  ]
  edge [
    source 304
    target 371
  ]
  edge [
    source 304
    target 359
  ]
  edge [
    source 304
    target 349
  ]
  edge [
    source 304
    target 370
  ]
  edge [
    source 304
    target 375
  ]
  edge [
    source 304
    target 376
  ]
  edge [
    source 305
    target 306
  ]
  edge [
    source 305
    target 307
  ]
  edge [
    source 306
    target 307
  ]
  edge [
    source 308
    target 309
  ]
  edge [
    source 309
    target 366
  ]
  edge [
    source 310
    target 311
  ]
  edge [
    source 312
    target 313
  ]
  edge [
    source 312
    target 329
  ]
  edge [
    source 312
    target 330
  ]
  edge [
    source 312
    target 353
  ]
  edge [
    source 312
    target 372
  ]
  edge [
    source 314
    target 315
  ]
  edge [
    source 314
    target 316
  ]
  edge [
    source 315
    target 316
  ]
  edge [
    source 316
    target 335
  ]
  edge [
    source 316
    target 351
  ]
  edge [
    source 317
    target 318
  ]
  edge [
    source 317
    target 332
  ]
  edge [
    source 319
    target 320
  ]
  edge [
    source 319
    target 321
  ]
  edge [
    source 320
    target 321
  ]
  edge [
    source 322
    target 323
  ]
  edge [
    source 323
    target 348
  ]
  edge [
    source 323
    target 349
  ]
  edge [
    source 323
    target 350
  ]
  edge [
    source 324
    target 325
  ]
  edge [
    source 324
    target 326
  ]
  edge [
    source 325
    target 326
  ]
  edge [
    source 325
    target 336
  ]
  edge [
    source 325
    target 377
  ]
  edge [
    source 326
    target 377
  ]
  edge [
    source 327
    target 328
  ]
  edge [
    source 329
    target 330
  ]
  edge [
    source 331
    target 332
  ]
  edge [
    source 331
    target 367
  ]
  edge [
    source 332
    target 367
  ]
  edge [
    source 333
    target 334
  ]
  edge [
    source 334
    target 352
  ]
  edge [
    source 335
    target 351
  ]
  edge [
    source 337
    target 358
  ]
  edge [
    source 338
    target 339
  ]
  edge [
    source 338
    target 368
  ]
  edge [
    source 339
    target 361
  ]
  edge [
    source 339
    target 364
  ]
  edge [
    source 340
    target 341
  ]
  edge [
    source 342
    target 343
  ]
  edge [
    source 344
    target 345
  ]
  edge [
    source 346
    target 347
  ]
  edge [
    source 346
    target 371
  ]
  edge [
    source 346
    target 359
  ]
  edge [
    source 346
    target 349
  ]
  edge [
    source 346
    target 370
  ]
  edge [
    source 348
    target 349
  ]
  edge [
    source 348
    target 350
  ]
  edge [
    source 349
    target 350
  ]
  edge [
    source 349
    target 370
  ]
  edge [
    source 354
    target 355
  ]
  edge [
    source 356
    target 357
  ]
  edge [
    source 359
    target 360
  ]
  edge [
    source 359
    target 371
  ]
  edge [
    source 361
    target 364
  ]
  edge [
    source 362
    target 363
  ]
  edge [
    source 365
    target 375
  ]
  edge [
    source 365
    target 376
  ]
  edge [
    source 369
    target 370
  ]
  edge [
    source 373
    target 374
  ]
  edge [
    source 375
    target 376
  ]
  edge [
    source 375
    target 382
  ]
  edge [
    source 375
    target 383
  ]
  edge [
    source 375
    target 384
  ]
  edge [
    source 375
    target 385
  ]
  edge [
    source 375
    target 386
  ]
  edge [
    source 375
    target 387
  ]
  edge [
    source 375
    target 388
  ]
  edge [
    source 378
    target 379
  ]
  edge [
    source 380
    target 381
  ]
  edge [
    source 382
    target 383
  ]
  edge [
    source 382
    target 384
  ]
  edge [
    source 382
    target 385
  ]
  edge [
    source 382
    target 386
  ]
  edge [
    source 383
    target 384
  ]
  edge [
    source 383
    target 385
  ]
  edge [
    source 383
    target 386
  ]
  edge [
    source 383
    target 387
  ]
  edge [
    source 383
    target 388
  ]
  edge [
    source 384
    target 385
  ]
  edge [
    source 384
    target 386
  ]
  edge [
    source 384
    target 387
  ]
  edge [
    source 384
    target 388
  ]
  edge [
    source 385
    target 386
  ]
  edge [
    source 385
    target 387
  ]
  edge [
    source 385
    target 388
  ]
  edge [
    source 386
    target 387
  ]
  edge [
    source 386
    target 388
  ]
  edge [
    source 387
    target 388
  ]
  edge [
    source 390
    target 391
  ]
  edge [
    source 392
    target 393
  ]
  edge [
    source 392
    target 394
  ]
  edge [
    source 393
    target 394
  ]
  edge [
    source 395
    target 396
  ]
]
