graph [
  node [
    id 0
    label "dla"
    origin "text"
  ]
  node [
    id 1
    label "klient"
    origin "text"
  ]
  node [
    id 2
    label "galeria"
    origin "text"
  ]
  node [
    id 3
    label "forum"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zni&#380;ka"
    origin "text"
  ]
  node [
    id 6
    label "taxi"
    origin "text"
  ]
  node [
    id 7
    label "podeszlam"
    origin "text"
  ]
  node [
    id 8
    label "taks&#243;wka"
    origin "text"
  ]
  node [
    id 9
    label "spyta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "ile"
    origin "text"
  ]
  node [
    id 11
    label "zawie&#378;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "wrzeszcz"
    origin "text"
  ]
  node [
    id 13
    label "agent_rozliczeniowy"
  ]
  node [
    id 14
    label "komputer_cyfrowy"
  ]
  node [
    id 15
    label "us&#322;ugobiorca"
  ]
  node [
    id 16
    label "cz&#322;owiek"
  ]
  node [
    id 17
    label "Rzymianin"
  ]
  node [
    id 18
    label "szlachcic"
  ]
  node [
    id 19
    label "obywatel"
  ]
  node [
    id 20
    label "klientela"
  ]
  node [
    id 21
    label "bratek"
  ]
  node [
    id 22
    label "program"
  ]
  node [
    id 23
    label "szlachciura"
  ]
  node [
    id 24
    label "przedstawiciel"
  ]
  node [
    id 25
    label "szlachta"
  ]
  node [
    id 26
    label "notabl"
  ]
  node [
    id 27
    label "miastowy"
  ]
  node [
    id 28
    label "mieszkaniec"
  ]
  node [
    id 29
    label "pa&#324;stwo"
  ]
  node [
    id 30
    label "Cyceron"
  ]
  node [
    id 31
    label "Horacy"
  ]
  node [
    id 32
    label "W&#322;och"
  ]
  node [
    id 33
    label "ludzko&#347;&#263;"
  ]
  node [
    id 34
    label "asymilowanie"
  ]
  node [
    id 35
    label "wapniak"
  ]
  node [
    id 36
    label "asymilowa&#263;"
  ]
  node [
    id 37
    label "os&#322;abia&#263;"
  ]
  node [
    id 38
    label "posta&#263;"
  ]
  node [
    id 39
    label "hominid"
  ]
  node [
    id 40
    label "podw&#322;adny"
  ]
  node [
    id 41
    label "os&#322;abianie"
  ]
  node [
    id 42
    label "g&#322;owa"
  ]
  node [
    id 43
    label "figura"
  ]
  node [
    id 44
    label "portrecista"
  ]
  node [
    id 45
    label "dwun&#243;g"
  ]
  node [
    id 46
    label "profanum"
  ]
  node [
    id 47
    label "mikrokosmos"
  ]
  node [
    id 48
    label "nasada"
  ]
  node [
    id 49
    label "duch"
  ]
  node [
    id 50
    label "antropochoria"
  ]
  node [
    id 51
    label "osoba"
  ]
  node [
    id 52
    label "wz&#243;r"
  ]
  node [
    id 53
    label "senior"
  ]
  node [
    id 54
    label "oddzia&#322;ywanie"
  ]
  node [
    id 55
    label "Adam"
  ]
  node [
    id 56
    label "homo_sapiens"
  ]
  node [
    id 57
    label "polifag"
  ]
  node [
    id 58
    label "podmiot"
  ]
  node [
    id 59
    label "instalowa&#263;"
  ]
  node [
    id 60
    label "oprogramowanie"
  ]
  node [
    id 61
    label "odinstalowywa&#263;"
  ]
  node [
    id 62
    label "spis"
  ]
  node [
    id 63
    label "zaprezentowanie"
  ]
  node [
    id 64
    label "podprogram"
  ]
  node [
    id 65
    label "ogranicznik_referencyjny"
  ]
  node [
    id 66
    label "course_of_study"
  ]
  node [
    id 67
    label "booklet"
  ]
  node [
    id 68
    label "dzia&#322;"
  ]
  node [
    id 69
    label "odinstalowanie"
  ]
  node [
    id 70
    label "broszura"
  ]
  node [
    id 71
    label "wytw&#243;r"
  ]
  node [
    id 72
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 73
    label "kana&#322;"
  ]
  node [
    id 74
    label "teleferie"
  ]
  node [
    id 75
    label "zainstalowanie"
  ]
  node [
    id 76
    label "struktura_organizacyjna"
  ]
  node [
    id 77
    label "pirat"
  ]
  node [
    id 78
    label "zaprezentowa&#263;"
  ]
  node [
    id 79
    label "prezentowanie"
  ]
  node [
    id 80
    label "prezentowa&#263;"
  ]
  node [
    id 81
    label "interfejs"
  ]
  node [
    id 82
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 83
    label "okno"
  ]
  node [
    id 84
    label "blok"
  ]
  node [
    id 85
    label "punkt"
  ]
  node [
    id 86
    label "folder"
  ]
  node [
    id 87
    label "zainstalowa&#263;"
  ]
  node [
    id 88
    label "za&#322;o&#380;enie"
  ]
  node [
    id 89
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 90
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 91
    label "ram&#243;wka"
  ]
  node [
    id 92
    label "tryb"
  ]
  node [
    id 93
    label "emitowa&#263;"
  ]
  node [
    id 94
    label "emitowanie"
  ]
  node [
    id 95
    label "odinstalowywanie"
  ]
  node [
    id 96
    label "instrukcja"
  ]
  node [
    id 97
    label "informatyka"
  ]
  node [
    id 98
    label "deklaracja"
  ]
  node [
    id 99
    label "menu"
  ]
  node [
    id 100
    label "sekcja_krytyczna"
  ]
  node [
    id 101
    label "furkacja"
  ]
  node [
    id 102
    label "podstawa"
  ]
  node [
    id 103
    label "instalowanie"
  ]
  node [
    id 104
    label "oferta"
  ]
  node [
    id 105
    label "odinstalowa&#263;"
  ]
  node [
    id 106
    label "kochanek"
  ]
  node [
    id 107
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 108
    label "fio&#322;ek"
  ]
  node [
    id 109
    label "facet"
  ]
  node [
    id 110
    label "brat"
  ]
  node [
    id 111
    label "clientele"
  ]
  node [
    id 112
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 113
    label "balkon"
  ]
  node [
    id 114
    label "eskalator"
  ]
  node [
    id 115
    label "zbi&#243;r"
  ]
  node [
    id 116
    label "&#322;&#261;cznik"
  ]
  node [
    id 117
    label "wystawa"
  ]
  node [
    id 118
    label "sala"
  ]
  node [
    id 119
    label "publiczno&#347;&#263;"
  ]
  node [
    id 120
    label "Galeria_Arsena&#322;"
  ]
  node [
    id 121
    label "muzeum"
  ]
  node [
    id 122
    label "sklep"
  ]
  node [
    id 123
    label "centrum_handlowe"
  ]
  node [
    id 124
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 125
    label "ekspozycja"
  ]
  node [
    id 126
    label "instytucja"
  ]
  node [
    id 127
    label "kuratorstwo"
  ]
  node [
    id 128
    label "budynek"
  ]
  node [
    id 129
    label "widownia"
  ]
  node [
    id 130
    label "balustrada"
  ]
  node [
    id 131
    label "p&#243;&#322;ka"
  ]
  node [
    id 132
    label "firma"
  ]
  node [
    id 133
    label "stoisko"
  ]
  node [
    id 134
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 135
    label "sk&#322;ad"
  ]
  node [
    id 136
    label "obiekt_handlowy"
  ]
  node [
    id 137
    label "zaplecze"
  ]
  node [
    id 138
    label "witryna"
  ]
  node [
    id 139
    label "odbiorca"
  ]
  node [
    id 140
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 141
    label "audience"
  ]
  node [
    id 142
    label "publiczka"
  ]
  node [
    id 143
    label "widzownia"
  ]
  node [
    id 144
    label "droga"
  ]
  node [
    id 145
    label "kreska"
  ]
  node [
    id 146
    label "zwiadowca"
  ]
  node [
    id 147
    label "znak_pisarski"
  ]
  node [
    id 148
    label "czynnik"
  ]
  node [
    id 149
    label "fuga"
  ]
  node [
    id 150
    label "przew&#243;d_wiertniczy"
  ]
  node [
    id 151
    label "znak_muzyczny"
  ]
  node [
    id 152
    label "pogoniec"
  ]
  node [
    id 153
    label "S&#261;deczanka"
  ]
  node [
    id 154
    label "ligature"
  ]
  node [
    id 155
    label "orzeczenie"
  ]
  node [
    id 156
    label "rakieta"
  ]
  node [
    id 157
    label "napastnik"
  ]
  node [
    id 158
    label "po&#347;rednik"
  ]
  node [
    id 159
    label "hyphen"
  ]
  node [
    id 160
    label "kontakt"
  ]
  node [
    id 161
    label "dobud&#243;wka"
  ]
  node [
    id 162
    label "egzemplarz"
  ]
  node [
    id 163
    label "series"
  ]
  node [
    id 164
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 165
    label "uprawianie"
  ]
  node [
    id 166
    label "praca_rolnicza"
  ]
  node [
    id 167
    label "collection"
  ]
  node [
    id 168
    label "dane"
  ]
  node [
    id 169
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 170
    label "pakiet_klimatyczny"
  ]
  node [
    id 171
    label "poj&#281;cie"
  ]
  node [
    id 172
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 173
    label "sum"
  ]
  node [
    id 174
    label "gathering"
  ]
  node [
    id 175
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 176
    label "album"
  ]
  node [
    id 177
    label "zgromadzenie"
  ]
  node [
    id 178
    label "pomieszczenie"
  ]
  node [
    id 179
    label "szyba"
  ]
  node [
    id 180
    label "kolekcja"
  ]
  node [
    id 181
    label "impreza"
  ]
  node [
    id 182
    label "kustosz"
  ]
  node [
    id 183
    label "miejsce"
  ]
  node [
    id 184
    label "kurator"
  ]
  node [
    id 185
    label "Agropromocja"
  ]
  node [
    id 186
    label "wernisa&#380;"
  ]
  node [
    id 187
    label "Arsena&#322;"
  ]
  node [
    id 188
    label "schody"
  ]
  node [
    id 189
    label "grupa_dyskusyjna"
  ]
  node [
    id 190
    label "s&#261;d"
  ]
  node [
    id 191
    label "plac"
  ]
  node [
    id 192
    label "bazylika"
  ]
  node [
    id 193
    label "przestrze&#324;"
  ]
  node [
    id 194
    label "portal"
  ]
  node [
    id 195
    label "konferencja"
  ]
  node [
    id 196
    label "agora"
  ]
  node [
    id 197
    label "grupa"
  ]
  node [
    id 198
    label "strona"
  ]
  node [
    id 199
    label "odm&#322;adzanie"
  ]
  node [
    id 200
    label "liga"
  ]
  node [
    id 201
    label "jednostka_systematyczna"
  ]
  node [
    id 202
    label "gromada"
  ]
  node [
    id 203
    label "Entuzjastki"
  ]
  node [
    id 204
    label "kompozycja"
  ]
  node [
    id 205
    label "Terranie"
  ]
  node [
    id 206
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 207
    label "category"
  ]
  node [
    id 208
    label "oddzia&#322;"
  ]
  node [
    id 209
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 210
    label "cz&#261;steczka"
  ]
  node [
    id 211
    label "stage_set"
  ]
  node [
    id 212
    label "type"
  ]
  node [
    id 213
    label "specgrupa"
  ]
  node [
    id 214
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 215
    label "&#346;wietliki"
  ]
  node [
    id 216
    label "odm&#322;odzenie"
  ]
  node [
    id 217
    label "Eurogrupa"
  ]
  node [
    id 218
    label "odm&#322;adza&#263;"
  ]
  node [
    id 219
    label "formacja_geologiczna"
  ]
  node [
    id 220
    label "harcerze_starsi"
  ]
  node [
    id 221
    label "warunek_lokalowy"
  ]
  node [
    id 222
    label "location"
  ]
  node [
    id 223
    label "uwaga"
  ]
  node [
    id 224
    label "status"
  ]
  node [
    id 225
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 226
    label "chwila"
  ]
  node [
    id 227
    label "cia&#322;o"
  ]
  node [
    id 228
    label "cecha"
  ]
  node [
    id 229
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 230
    label "praca"
  ]
  node [
    id 231
    label "rz&#261;d"
  ]
  node [
    id 232
    label "&#321;ubianka"
  ]
  node [
    id 233
    label "area"
  ]
  node [
    id 234
    label "Majdan"
  ]
  node [
    id 235
    label "pole_bitwy"
  ]
  node [
    id 236
    label "obszar"
  ]
  node [
    id 237
    label "pierzeja"
  ]
  node [
    id 238
    label "miasto"
  ]
  node [
    id 239
    label "targowica"
  ]
  node [
    id 240
    label "kram"
  ]
  node [
    id 241
    label "rozdzielanie"
  ]
  node [
    id 242
    label "bezbrze&#380;e"
  ]
  node [
    id 243
    label "czasoprzestrze&#324;"
  ]
  node [
    id 244
    label "niezmierzony"
  ]
  node [
    id 245
    label "przedzielenie"
  ]
  node [
    id 246
    label "nielito&#347;ciwy"
  ]
  node [
    id 247
    label "rozdziela&#263;"
  ]
  node [
    id 248
    label "oktant"
  ]
  node [
    id 249
    label "przedzieli&#263;"
  ]
  node [
    id 250
    label "przestw&#243;r"
  ]
  node [
    id 251
    label "kartka"
  ]
  node [
    id 252
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 253
    label "logowanie"
  ]
  node [
    id 254
    label "plik"
  ]
  node [
    id 255
    label "adres_internetowy"
  ]
  node [
    id 256
    label "linia"
  ]
  node [
    id 257
    label "serwis_internetowy"
  ]
  node [
    id 258
    label "bok"
  ]
  node [
    id 259
    label "skr&#281;canie"
  ]
  node [
    id 260
    label "skr&#281;ca&#263;"
  ]
  node [
    id 261
    label "orientowanie"
  ]
  node [
    id 262
    label "skr&#281;ci&#263;"
  ]
  node [
    id 263
    label "uj&#281;cie"
  ]
  node [
    id 264
    label "zorientowanie"
  ]
  node [
    id 265
    label "ty&#322;"
  ]
  node [
    id 266
    label "fragment"
  ]
  node [
    id 267
    label "layout"
  ]
  node [
    id 268
    label "obiekt"
  ]
  node [
    id 269
    label "zorientowa&#263;"
  ]
  node [
    id 270
    label "pagina"
  ]
  node [
    id 271
    label "g&#243;ra"
  ]
  node [
    id 272
    label "orientowa&#263;"
  ]
  node [
    id 273
    label "voice"
  ]
  node [
    id 274
    label "orientacja"
  ]
  node [
    id 275
    label "prz&#243;d"
  ]
  node [
    id 276
    label "internet"
  ]
  node [
    id 277
    label "powierzchnia"
  ]
  node [
    id 278
    label "forma"
  ]
  node [
    id 279
    label "skr&#281;cenie"
  ]
  node [
    id 280
    label "Ja&#322;ta"
  ]
  node [
    id 281
    label "spotkanie"
  ]
  node [
    id 282
    label "konferencyjka"
  ]
  node [
    id 283
    label "conference"
  ]
  node [
    id 284
    label "grusza_pospolita"
  ]
  node [
    id 285
    label "Poczdam"
  ]
  node [
    id 286
    label "obramienie"
  ]
  node [
    id 287
    label "Onet"
  ]
  node [
    id 288
    label "archiwolta"
  ]
  node [
    id 289
    label "wej&#347;cie"
  ]
  node [
    id 290
    label "hala"
  ]
  node [
    id 291
    label "westwerk"
  ]
  node [
    id 292
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 293
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 294
    label "zesp&#243;&#322;"
  ]
  node [
    id 295
    label "podejrzany"
  ]
  node [
    id 296
    label "s&#261;downictwo"
  ]
  node [
    id 297
    label "system"
  ]
  node [
    id 298
    label "biuro"
  ]
  node [
    id 299
    label "court"
  ]
  node [
    id 300
    label "bronienie"
  ]
  node [
    id 301
    label "urz&#261;d"
  ]
  node [
    id 302
    label "wydarzenie"
  ]
  node [
    id 303
    label "oskar&#380;yciel"
  ]
  node [
    id 304
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 305
    label "skazany"
  ]
  node [
    id 306
    label "post&#281;powanie"
  ]
  node [
    id 307
    label "broni&#263;"
  ]
  node [
    id 308
    label "my&#347;l"
  ]
  node [
    id 309
    label "pods&#261;dny"
  ]
  node [
    id 310
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 311
    label "obrona"
  ]
  node [
    id 312
    label "wypowied&#378;"
  ]
  node [
    id 313
    label "antylogizm"
  ]
  node [
    id 314
    label "konektyw"
  ]
  node [
    id 315
    label "&#347;wiadek"
  ]
  node [
    id 316
    label "procesowicz"
  ]
  node [
    id 317
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 318
    label "mie&#263;_miejsce"
  ]
  node [
    id 319
    label "equal"
  ]
  node [
    id 320
    label "trwa&#263;"
  ]
  node [
    id 321
    label "chodzi&#263;"
  ]
  node [
    id 322
    label "si&#281;ga&#263;"
  ]
  node [
    id 323
    label "stan"
  ]
  node [
    id 324
    label "obecno&#347;&#263;"
  ]
  node [
    id 325
    label "stand"
  ]
  node [
    id 326
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 327
    label "uczestniczy&#263;"
  ]
  node [
    id 328
    label "participate"
  ]
  node [
    id 329
    label "robi&#263;"
  ]
  node [
    id 330
    label "istnie&#263;"
  ]
  node [
    id 331
    label "pozostawa&#263;"
  ]
  node [
    id 332
    label "zostawa&#263;"
  ]
  node [
    id 333
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 334
    label "adhere"
  ]
  node [
    id 335
    label "compass"
  ]
  node [
    id 336
    label "korzysta&#263;"
  ]
  node [
    id 337
    label "appreciation"
  ]
  node [
    id 338
    label "osi&#261;ga&#263;"
  ]
  node [
    id 339
    label "dociera&#263;"
  ]
  node [
    id 340
    label "get"
  ]
  node [
    id 341
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 342
    label "mierzy&#263;"
  ]
  node [
    id 343
    label "u&#380;ywa&#263;"
  ]
  node [
    id 344
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 345
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 346
    label "exsert"
  ]
  node [
    id 347
    label "being"
  ]
  node [
    id 348
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 349
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 350
    label "p&#322;ywa&#263;"
  ]
  node [
    id 351
    label "run"
  ]
  node [
    id 352
    label "bangla&#263;"
  ]
  node [
    id 353
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 354
    label "przebiega&#263;"
  ]
  node [
    id 355
    label "wk&#322;ada&#263;"
  ]
  node [
    id 356
    label "proceed"
  ]
  node [
    id 357
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 358
    label "carry"
  ]
  node [
    id 359
    label "bywa&#263;"
  ]
  node [
    id 360
    label "dziama&#263;"
  ]
  node [
    id 361
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 362
    label "stara&#263;_si&#281;"
  ]
  node [
    id 363
    label "para"
  ]
  node [
    id 364
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 365
    label "str&#243;j"
  ]
  node [
    id 366
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 367
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 368
    label "krok"
  ]
  node [
    id 369
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 370
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 371
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 372
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 373
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 374
    label "continue"
  ]
  node [
    id 375
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 376
    label "Ohio"
  ]
  node [
    id 377
    label "wci&#281;cie"
  ]
  node [
    id 378
    label "Nowy_York"
  ]
  node [
    id 379
    label "warstwa"
  ]
  node [
    id 380
    label "samopoczucie"
  ]
  node [
    id 381
    label "Illinois"
  ]
  node [
    id 382
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 383
    label "state"
  ]
  node [
    id 384
    label "Jukatan"
  ]
  node [
    id 385
    label "Kalifornia"
  ]
  node [
    id 386
    label "Wirginia"
  ]
  node [
    id 387
    label "wektor"
  ]
  node [
    id 388
    label "Teksas"
  ]
  node [
    id 389
    label "Goa"
  ]
  node [
    id 390
    label "Waszyngton"
  ]
  node [
    id 391
    label "Massachusetts"
  ]
  node [
    id 392
    label "Alaska"
  ]
  node [
    id 393
    label "Arakan"
  ]
  node [
    id 394
    label "Hawaje"
  ]
  node [
    id 395
    label "Maryland"
  ]
  node [
    id 396
    label "Michigan"
  ]
  node [
    id 397
    label "Arizona"
  ]
  node [
    id 398
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 399
    label "Georgia"
  ]
  node [
    id 400
    label "poziom"
  ]
  node [
    id 401
    label "Pensylwania"
  ]
  node [
    id 402
    label "shape"
  ]
  node [
    id 403
    label "Luizjana"
  ]
  node [
    id 404
    label "Nowy_Meksyk"
  ]
  node [
    id 405
    label "Alabama"
  ]
  node [
    id 406
    label "ilo&#347;&#263;"
  ]
  node [
    id 407
    label "Kansas"
  ]
  node [
    id 408
    label "Oregon"
  ]
  node [
    id 409
    label "Floryda"
  ]
  node [
    id 410
    label "Oklahoma"
  ]
  node [
    id 411
    label "jednostka_administracyjna"
  ]
  node [
    id 412
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 413
    label "spadek"
  ]
  node [
    id 414
    label "ton"
  ]
  node [
    id 415
    label "p&#322;aszczyzna"
  ]
  node [
    id 416
    label "zachowek"
  ]
  node [
    id 417
    label "wydziedziczy&#263;"
  ]
  node [
    id 418
    label "mienie"
  ]
  node [
    id 419
    label "wydziedziczenie"
  ]
  node [
    id 420
    label "ruch"
  ]
  node [
    id 421
    label "scheda_spadkowa"
  ]
  node [
    id 422
    label "sukcesja"
  ]
  node [
    id 423
    label "camber"
  ]
  node [
    id 424
    label "zmiana"
  ]
  node [
    id 425
    label "transport"
  ]
  node [
    id 426
    label "sa&#322;ata"
  ]
  node [
    id 427
    label "roz&#322;adunek"
  ]
  node [
    id 428
    label "sprz&#281;t"
  ]
  node [
    id 429
    label "cedu&#322;a"
  ]
  node [
    id 430
    label "jednoszynowy"
  ]
  node [
    id 431
    label "unos"
  ]
  node [
    id 432
    label "traffic"
  ]
  node [
    id 433
    label "prze&#322;adunek"
  ]
  node [
    id 434
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 435
    label "us&#322;uga"
  ]
  node [
    id 436
    label "komunikacja"
  ]
  node [
    id 437
    label "tyfon"
  ]
  node [
    id 438
    label "zawarto&#347;&#263;"
  ]
  node [
    id 439
    label "towar"
  ]
  node [
    id 440
    label "gospodarka"
  ]
  node [
    id 441
    label "za&#322;adunek"
  ]
  node [
    id 442
    label "pieni&#261;dze"
  ]
  node [
    id 443
    label "zielenina"
  ]
  node [
    id 444
    label "warzywo"
  ]
  node [
    id 445
    label "lettuce"
  ]
  node [
    id 446
    label "ro&#347;lina"
  ]
  node [
    id 447
    label "astrowate"
  ]
  node [
    id 448
    label "dostarczy&#263;"
  ]
  node [
    id 449
    label "take"
  ]
  node [
    id 450
    label "wytworzy&#263;"
  ]
  node [
    id 451
    label "spowodowa&#263;"
  ]
  node [
    id 452
    label "give"
  ]
  node [
    id 453
    label "picture"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
]
