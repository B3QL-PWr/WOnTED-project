graph [
  node [
    id 0
    label "sos"
    origin "text"
  ]
  node [
    id 1
    label "spaghetti"
    origin "text"
  ]
  node [
    id 2
    label "r&#243;&#380;norodno&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "spo&#322;ecze&#324;stwo"
    origin "text"
  ]
  node [
    id 4
    label "mieszanina"
  ]
  node [
    id 5
    label "zaklepka"
  ]
  node [
    id 6
    label "gulasz"
  ]
  node [
    id 7
    label "jedzenie"
  ]
  node [
    id 8
    label "zatruwanie_si&#281;"
  ]
  node [
    id 9
    label "przejadanie_si&#281;"
  ]
  node [
    id 10
    label "szama"
  ]
  node [
    id 11
    label "koryto"
  ]
  node [
    id 12
    label "rzecz"
  ]
  node [
    id 13
    label "odpasanie_si&#281;"
  ]
  node [
    id 14
    label "eating"
  ]
  node [
    id 15
    label "jadanie"
  ]
  node [
    id 16
    label "posilenie"
  ]
  node [
    id 17
    label "wpieprzanie"
  ]
  node [
    id 18
    label "wmuszanie"
  ]
  node [
    id 19
    label "robienie"
  ]
  node [
    id 20
    label "wiwenda"
  ]
  node [
    id 21
    label "polowanie"
  ]
  node [
    id 22
    label "ufetowanie_si&#281;"
  ]
  node [
    id 23
    label "wyjadanie"
  ]
  node [
    id 24
    label "smakowanie"
  ]
  node [
    id 25
    label "przejedzenie"
  ]
  node [
    id 26
    label "jad&#322;o"
  ]
  node [
    id 27
    label "mlaskanie"
  ]
  node [
    id 28
    label "papusianie"
  ]
  node [
    id 29
    label "podawa&#263;"
  ]
  node [
    id 30
    label "poda&#263;"
  ]
  node [
    id 31
    label "posilanie"
  ]
  node [
    id 32
    label "czynno&#347;&#263;"
  ]
  node [
    id 33
    label "podawanie"
  ]
  node [
    id 34
    label "przejedzenie_si&#281;"
  ]
  node [
    id 35
    label "&#380;arcie"
  ]
  node [
    id 36
    label "odpasienie_si&#281;"
  ]
  node [
    id 37
    label "podanie"
  ]
  node [
    id 38
    label "wyjedzenie"
  ]
  node [
    id 39
    label "przejadanie"
  ]
  node [
    id 40
    label "objadanie"
  ]
  node [
    id 41
    label "zbi&#243;r"
  ]
  node [
    id 42
    label "frakcja"
  ]
  node [
    id 43
    label "substancja"
  ]
  node [
    id 44
    label "synteza"
  ]
  node [
    id 45
    label "potrawa"
  ]
  node [
    id 46
    label "zupa"
  ]
  node [
    id 47
    label "zag&#281;stnik"
  ]
  node [
    id 48
    label "zaprawa"
  ]
  node [
    id 49
    label "pasta"
  ]
  node [
    id 50
    label "masa"
  ]
  node [
    id 51
    label "przetw&#243;r"
  ]
  node [
    id 52
    label "porcja"
  ]
  node [
    id 53
    label "historyjka"
  ]
  node [
    id 54
    label "makaron"
  ]
  node [
    id 55
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 56
    label "rozpraszanie_si&#281;"
  ]
  node [
    id 57
    label "discrimination"
  ]
  node [
    id 58
    label "diverseness"
  ]
  node [
    id 59
    label "eklektyk"
  ]
  node [
    id 60
    label "rozproszenie_si&#281;"
  ]
  node [
    id 61
    label "differentiation"
  ]
  node [
    id 62
    label "bogactwo"
  ]
  node [
    id 63
    label "cecha"
  ]
  node [
    id 64
    label "multikulturalizm"
  ]
  node [
    id 65
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 66
    label "rozdzielenie"
  ]
  node [
    id 67
    label "nadanie"
  ]
  node [
    id 68
    label "podzielenie"
  ]
  node [
    id 69
    label "zrobienie"
  ]
  node [
    id 70
    label "cywilizacja"
  ]
  node [
    id 71
    label "pole"
  ]
  node [
    id 72
    label "elita"
  ]
  node [
    id 73
    label "status"
  ]
  node [
    id 74
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 75
    label "aspo&#322;eczny"
  ]
  node [
    id 76
    label "ludzie_pracy"
  ]
  node [
    id 77
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 78
    label "pozaklasowy"
  ]
  node [
    id 79
    label "uwarstwienie"
  ]
  node [
    id 80
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 81
    label "community"
  ]
  node [
    id 82
    label "klasa"
  ]
  node [
    id 83
    label "kastowo&#347;&#263;"
  ]
  node [
    id 84
    label "facylitacja"
  ]
  node [
    id 85
    label "elite"
  ]
  node [
    id 86
    label "&#347;rodowisko"
  ]
  node [
    id 87
    label "wagon"
  ]
  node [
    id 88
    label "mecz_mistrzowski"
  ]
  node [
    id 89
    label "przedmiot"
  ]
  node [
    id 90
    label "arrangement"
  ]
  node [
    id 91
    label "class"
  ]
  node [
    id 92
    label "&#322;awka"
  ]
  node [
    id 93
    label "wykrzyknik"
  ]
  node [
    id 94
    label "zaleta"
  ]
  node [
    id 95
    label "jednostka_systematyczna"
  ]
  node [
    id 96
    label "programowanie_obiektowe"
  ]
  node [
    id 97
    label "tablica"
  ]
  node [
    id 98
    label "warstwa"
  ]
  node [
    id 99
    label "rezerwa"
  ]
  node [
    id 100
    label "gromada"
  ]
  node [
    id 101
    label "Ekwici"
  ]
  node [
    id 102
    label "szko&#322;a"
  ]
  node [
    id 103
    label "organizacja"
  ]
  node [
    id 104
    label "sala"
  ]
  node [
    id 105
    label "pomoc"
  ]
  node [
    id 106
    label "form"
  ]
  node [
    id 107
    label "grupa"
  ]
  node [
    id 108
    label "przepisa&#263;"
  ]
  node [
    id 109
    label "jako&#347;&#263;"
  ]
  node [
    id 110
    label "znak_jako&#347;ci"
  ]
  node [
    id 111
    label "poziom"
  ]
  node [
    id 112
    label "type"
  ]
  node [
    id 113
    label "promocja"
  ]
  node [
    id 114
    label "przepisanie"
  ]
  node [
    id 115
    label "kurs"
  ]
  node [
    id 116
    label "obiekt"
  ]
  node [
    id 117
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 118
    label "dziennik_lekcyjny"
  ]
  node [
    id 119
    label "typ"
  ]
  node [
    id 120
    label "fakcja"
  ]
  node [
    id 121
    label "obrona"
  ]
  node [
    id 122
    label "atak"
  ]
  node [
    id 123
    label "botanika"
  ]
  node [
    id 124
    label "uprawienie"
  ]
  node [
    id 125
    label "u&#322;o&#380;enie"
  ]
  node [
    id 126
    label "p&#322;osa"
  ]
  node [
    id 127
    label "ziemia"
  ]
  node [
    id 128
    label "t&#322;o"
  ]
  node [
    id 129
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 130
    label "gospodarstwo"
  ]
  node [
    id 131
    label "uprawi&#263;"
  ]
  node [
    id 132
    label "room"
  ]
  node [
    id 133
    label "dw&#243;r"
  ]
  node [
    id 134
    label "okazja"
  ]
  node [
    id 135
    label "rozmiar"
  ]
  node [
    id 136
    label "irygowanie"
  ]
  node [
    id 137
    label "compass"
  ]
  node [
    id 138
    label "square"
  ]
  node [
    id 139
    label "zmienna"
  ]
  node [
    id 140
    label "irygowa&#263;"
  ]
  node [
    id 141
    label "socjologia"
  ]
  node [
    id 142
    label "boisko"
  ]
  node [
    id 143
    label "dziedzina"
  ]
  node [
    id 144
    label "baza_danych"
  ]
  node [
    id 145
    label "region"
  ]
  node [
    id 146
    label "przestrze&#324;"
  ]
  node [
    id 147
    label "zagon"
  ]
  node [
    id 148
    label "obszar"
  ]
  node [
    id 149
    label "sk&#322;ad"
  ]
  node [
    id 150
    label "powierzchnia"
  ]
  node [
    id 151
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 152
    label "plane"
  ]
  node [
    id 153
    label "radlina"
  ]
  node [
    id 154
    label "Fremeni"
  ]
  node [
    id 155
    label "niskogatunkowy"
  ]
  node [
    id 156
    label "condition"
  ]
  node [
    id 157
    label "awansowa&#263;"
  ]
  node [
    id 158
    label "znaczenie"
  ]
  node [
    id 159
    label "stan"
  ]
  node [
    id 160
    label "awans"
  ]
  node [
    id 161
    label "podmiotowo"
  ]
  node [
    id 162
    label "awansowanie"
  ]
  node [
    id 163
    label "sytuacja"
  ]
  node [
    id 164
    label "niekorzystny"
  ]
  node [
    id 165
    label "aspo&#322;ecznie"
  ]
  node [
    id 166
    label "typowy"
  ]
  node [
    id 167
    label "niech&#281;tny"
  ]
  node [
    id 168
    label "asymilowanie_si&#281;"
  ]
  node [
    id 169
    label "Wsch&#243;d"
  ]
  node [
    id 170
    label "przejmowanie"
  ]
  node [
    id 171
    label "zjawisko"
  ]
  node [
    id 172
    label "makrokosmos"
  ]
  node [
    id 173
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 174
    label "civilization"
  ]
  node [
    id 175
    label "przejmowa&#263;"
  ]
  node [
    id 176
    label "faza"
  ]
  node [
    id 177
    label "technika"
  ]
  node [
    id 178
    label "kuchnia"
  ]
  node [
    id 179
    label "rozw&#243;j"
  ]
  node [
    id 180
    label "populace"
  ]
  node [
    id 181
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 182
    label "przej&#281;cie"
  ]
  node [
    id 183
    label "przej&#261;&#263;"
  ]
  node [
    id 184
    label "cywilizowanie"
  ]
  node [
    id 185
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 186
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 187
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 188
    label "struktura"
  ]
  node [
    id 189
    label "stratification"
  ]
  node [
    id 190
    label "lamination"
  ]
  node [
    id 191
    label "podzia&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
]
