graph [
  node [
    id 0
    label "tendencja"
    origin "text"
  ]
  node [
    id 1
    label "uwa&#380;anie"
    origin "text"
  ]
  node [
    id 2
    label "niski"
    origin "text"
  ]
  node [
    id 3
    label "przedstawicielka"
    origin "text"
  ]
  node [
    id 4
    label "p&#322;e&#263;"
    origin "text"
  ]
  node [
    id 5
    label "pi&#281;kny"
    origin "text"
  ]
  node [
    id 6
    label "bardzo"
    origin "text"
  ]
  node [
    id 7
    label "kobiecy"
    origin "text"
  ]
  node [
    id 8
    label "dawa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "zauwa&#380;y&#263;"
    origin "text"
  ]
  node [
    id 11
    label "go&#322;y"
    origin "text"
  ]
  node [
    id 12
    label "oko"
    origin "text"
  ]
  node [
    id 13
    label "wysoki"
    origin "text"
  ]
  node [
    id 14
    label "wzrost"
    origin "text"
  ]
  node [
    id 15
    label "by&#263;"
    origin "text"
  ]
  node [
    id 16
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 17
    label "cecha"
    origin "text"
  ]
  node [
    id 18
    label "charakterystyczny"
    origin "text"
  ]
  node [
    id 19
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 20
    label "ideologia"
  ]
  node [
    id 21
    label "podatno&#347;&#263;"
  ]
  node [
    id 22
    label "metoda"
  ]
  node [
    id 23
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 24
    label "praktyka"
  ]
  node [
    id 25
    label "idea"
  ]
  node [
    id 26
    label "system"
  ]
  node [
    id 27
    label "byt"
  ]
  node [
    id 28
    label "intelekt"
  ]
  node [
    id 29
    label "Kant"
  ]
  node [
    id 30
    label "p&#322;&#243;d"
  ]
  node [
    id 31
    label "cel"
  ]
  node [
    id 32
    label "poj&#281;cie"
  ]
  node [
    id 33
    label "istota"
  ]
  node [
    id 34
    label "pomys&#322;"
  ]
  node [
    id 35
    label "ideacja"
  ]
  node [
    id 36
    label "practice"
  ]
  node [
    id 37
    label "wiedza"
  ]
  node [
    id 38
    label "znawstwo"
  ]
  node [
    id 39
    label "skill"
  ]
  node [
    id 40
    label "czyn"
  ]
  node [
    id 41
    label "nauka"
  ]
  node [
    id 42
    label "zwyczaj"
  ]
  node [
    id 43
    label "eksperiencja"
  ]
  node [
    id 44
    label "praca"
  ]
  node [
    id 45
    label "j&#261;dro"
  ]
  node [
    id 46
    label "systemik"
  ]
  node [
    id 47
    label "rozprz&#261;c"
  ]
  node [
    id 48
    label "oprogramowanie"
  ]
  node [
    id 49
    label "systemat"
  ]
  node [
    id 50
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 51
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 52
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 53
    label "model"
  ]
  node [
    id 54
    label "struktura"
  ]
  node [
    id 55
    label "usenet"
  ]
  node [
    id 56
    label "s&#261;d"
  ]
  node [
    id 57
    label "zbi&#243;r"
  ]
  node [
    id 58
    label "porz&#261;dek"
  ]
  node [
    id 59
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 60
    label "przyn&#281;ta"
  ]
  node [
    id 61
    label "net"
  ]
  node [
    id 62
    label "w&#281;dkarstwo"
  ]
  node [
    id 63
    label "eratem"
  ]
  node [
    id 64
    label "oddzia&#322;"
  ]
  node [
    id 65
    label "doktryna"
  ]
  node [
    id 66
    label "pulpit"
  ]
  node [
    id 67
    label "konstelacja"
  ]
  node [
    id 68
    label "jednostka_geologiczna"
  ]
  node [
    id 69
    label "o&#347;"
  ]
  node [
    id 70
    label "podsystem"
  ]
  node [
    id 71
    label "ryba"
  ]
  node [
    id 72
    label "Leopard"
  ]
  node [
    id 73
    label "spos&#243;b"
  ]
  node [
    id 74
    label "Android"
  ]
  node [
    id 75
    label "zachowanie"
  ]
  node [
    id 76
    label "cybernetyk"
  ]
  node [
    id 77
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 78
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 79
    label "method"
  ]
  node [
    id 80
    label "sk&#322;ad"
  ]
  node [
    id 81
    label "podstawa"
  ]
  node [
    id 82
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 83
    label "zasada"
  ]
  node [
    id 84
    label "relacja"
  ]
  node [
    id 85
    label "political_orientation"
  ]
  node [
    id 86
    label "szko&#322;a"
  ]
  node [
    id 87
    label "obserwowanie"
  ]
  node [
    id 88
    label "eskortowanie"
  ]
  node [
    id 89
    label "treatment"
  ]
  node [
    id 90
    label "robienie"
  ]
  node [
    id 91
    label "guard_duty"
  ]
  node [
    id 92
    label "pilnowanie"
  ]
  node [
    id 93
    label "ocenianie"
  ]
  node [
    id 94
    label "czynno&#347;&#263;"
  ]
  node [
    id 95
    label "str&#243;&#380;owanie"
  ]
  node [
    id 96
    label "my&#347;lenie"
  ]
  node [
    id 97
    label "skupianie_si&#281;"
  ]
  node [
    id 98
    label "proces_my&#347;lowy"
  ]
  node [
    id 99
    label "domy&#347;lanie_si&#281;"
  ]
  node [
    id 100
    label "zjawisko"
  ]
  node [
    id 101
    label "s&#261;dzenie"
  ]
  node [
    id 102
    label "judgment"
  ]
  node [
    id 103
    label "troskanie_si&#281;"
  ]
  node [
    id 104
    label "wnioskowanie"
  ]
  node [
    id 105
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 106
    label "proces"
  ]
  node [
    id 107
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 108
    label "walczenie"
  ]
  node [
    id 109
    label "zinterpretowanie"
  ]
  node [
    id 110
    label "walczy&#263;"
  ]
  node [
    id 111
    label "reflection"
  ]
  node [
    id 112
    label "wzlecie&#263;"
  ]
  node [
    id 113
    label "wzlecenie"
  ]
  node [
    id 114
    label "fabrication"
  ]
  node [
    id 115
    label "porobienie"
  ]
  node [
    id 116
    label "przedmiot"
  ]
  node [
    id 117
    label "bycie"
  ]
  node [
    id 118
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 119
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 120
    label "creation"
  ]
  node [
    id 121
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 122
    label "act"
  ]
  node [
    id 123
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 124
    label "tentegowanie"
  ]
  node [
    id 125
    label "activity"
  ]
  node [
    id 126
    label "bezproblemowy"
  ]
  node [
    id 127
    label "wydarzenie"
  ]
  node [
    id 128
    label "patrzenie"
  ]
  node [
    id 129
    label "badanie"
  ]
  node [
    id 130
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 131
    label "doszukanie_si&#281;"
  ]
  node [
    id 132
    label "dostrzeganie"
  ]
  node [
    id 133
    label "poobserwowanie"
  ]
  node [
    id 134
    label "observation"
  ]
  node [
    id 135
    label "bocianie_gniazdo"
  ]
  node [
    id 136
    label "evaluation"
  ]
  node [
    id 137
    label "zawyrokowanie"
  ]
  node [
    id 138
    label "przeszacowanie"
  ]
  node [
    id 139
    label "wyrokowanie"
  ]
  node [
    id 140
    label "przewidywanie"
  ]
  node [
    id 141
    label "przyznawanie"
  ]
  node [
    id 142
    label "cyrklowanie"
  ]
  node [
    id 143
    label "dostawanie"
  ]
  node [
    id 144
    label "traktowanie"
  ]
  node [
    id 145
    label "wycyrklowanie"
  ]
  node [
    id 146
    label "categorization"
  ]
  node [
    id 147
    label "znajdowanie"
  ]
  node [
    id 148
    label "appraisal"
  ]
  node [
    id 149
    label "convoy"
  ]
  node [
    id 150
    label "towarzyszenie"
  ]
  node [
    id 151
    label "dostarczanie"
  ]
  node [
    id 152
    label "nieznaczny"
  ]
  node [
    id 153
    label "nisko"
  ]
  node [
    id 154
    label "pomierny"
  ]
  node [
    id 155
    label "wstydliwy"
  ]
  node [
    id 156
    label "bliski"
  ]
  node [
    id 157
    label "s&#322;aby"
  ]
  node [
    id 158
    label "obni&#380;anie"
  ]
  node [
    id 159
    label "uni&#380;ony"
  ]
  node [
    id 160
    label "po&#347;ledni"
  ]
  node [
    id 161
    label "marny"
  ]
  node [
    id 162
    label "obni&#380;enie"
  ]
  node [
    id 163
    label "n&#281;dznie"
  ]
  node [
    id 164
    label "gorszy"
  ]
  node [
    id 165
    label "ma&#322;y"
  ]
  node [
    id 166
    label "pospolity"
  ]
  node [
    id 167
    label "nieznacznie"
  ]
  node [
    id 168
    label "drobnostkowy"
  ]
  node [
    id 169
    label "niewa&#380;ny"
  ]
  node [
    id 170
    label "pospolicie"
  ]
  node [
    id 171
    label "zwyczajny"
  ]
  node [
    id 172
    label "wsp&#243;lny"
  ]
  node [
    id 173
    label "jak_ps&#243;w"
  ]
  node [
    id 174
    label "niewyszukany"
  ]
  node [
    id 175
    label "pogorszenie_si&#281;"
  ]
  node [
    id 176
    label "pogorszenie"
  ]
  node [
    id 177
    label "pogarszanie_si&#281;"
  ]
  node [
    id 178
    label "uni&#380;enie"
  ]
  node [
    id 179
    label "skromny"
  ]
  node [
    id 180
    label "grzeczny"
  ]
  node [
    id 181
    label "wstydliwie"
  ]
  node [
    id 182
    label "g&#322;upi"
  ]
  node [
    id 183
    label "nie&#347;mia&#322;y"
  ]
  node [
    id 184
    label "blisko"
  ]
  node [
    id 185
    label "cz&#322;owiek"
  ]
  node [
    id 186
    label "znajomy"
  ]
  node [
    id 187
    label "zwi&#261;zany"
  ]
  node [
    id 188
    label "przesz&#322;y"
  ]
  node [
    id 189
    label "silny"
  ]
  node [
    id 190
    label "zbli&#380;enie"
  ]
  node [
    id 191
    label "kr&#243;tki"
  ]
  node [
    id 192
    label "oddalony"
  ]
  node [
    id 193
    label "dok&#322;adny"
  ]
  node [
    id 194
    label "nieodleg&#322;y"
  ]
  node [
    id 195
    label "przysz&#322;y"
  ]
  node [
    id 196
    label "gotowy"
  ]
  node [
    id 197
    label "ja&#322;owy"
  ]
  node [
    id 198
    label "marnie"
  ]
  node [
    id 199
    label "nieskuteczny"
  ]
  node [
    id 200
    label "kiepski"
  ]
  node [
    id 201
    label "kiepsko"
  ]
  node [
    id 202
    label "nadaremnie"
  ]
  node [
    id 203
    label "z&#322;y"
  ]
  node [
    id 204
    label "nietrwa&#322;y"
  ]
  node [
    id 205
    label "mizerny"
  ]
  node [
    id 206
    label "delikatny"
  ]
  node [
    id 207
    label "niezdrowy"
  ]
  node [
    id 208
    label "nieumiej&#281;tny"
  ]
  node [
    id 209
    label "s&#322;abo"
  ]
  node [
    id 210
    label "lura"
  ]
  node [
    id 211
    label "nieudany"
  ]
  node [
    id 212
    label "s&#322;abowity"
  ]
  node [
    id 213
    label "zawodny"
  ]
  node [
    id 214
    label "&#322;agodny"
  ]
  node [
    id 215
    label "md&#322;y"
  ]
  node [
    id 216
    label "niedoskona&#322;y"
  ]
  node [
    id 217
    label "przemijaj&#261;cy"
  ]
  node [
    id 218
    label "niemocny"
  ]
  node [
    id 219
    label "niefajny"
  ]
  node [
    id 220
    label "szybki"
  ]
  node [
    id 221
    label "przeci&#281;tny"
  ]
  node [
    id 222
    label "ch&#322;opiec"
  ]
  node [
    id 223
    label "m&#322;ody"
  ]
  node [
    id 224
    label "ma&#322;o"
  ]
  node [
    id 225
    label "nieliczny"
  ]
  node [
    id 226
    label "po&#347;lednio"
  ]
  node [
    id 227
    label "vilely"
  ]
  node [
    id 228
    label "despicably"
  ]
  node [
    id 229
    label "n&#281;dzny"
  ]
  node [
    id 230
    label "sm&#281;tnie"
  ]
  node [
    id 231
    label "biednie"
  ]
  node [
    id 232
    label "powodowanie"
  ]
  node [
    id 233
    label "zmniejszanie"
  ]
  node [
    id 234
    label "ni&#380;szy"
  ]
  node [
    id 235
    label "brzmienie"
  ]
  node [
    id 236
    label "zabrzmienie"
  ]
  node [
    id 237
    label "kszta&#322;t"
  ]
  node [
    id 238
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 239
    label "zmniejszenie"
  ]
  node [
    id 240
    label "spowodowanie"
  ]
  node [
    id 241
    label "miejsce"
  ]
  node [
    id 242
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 243
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 244
    label "suspension"
  ]
  node [
    id 245
    label "pad&#243;&#322;"
  ]
  node [
    id 246
    label "snub"
  ]
  node [
    id 247
    label "mierny"
  ]
  node [
    id 248
    label "&#347;redni"
  ]
  node [
    id 249
    label "lichy"
  ]
  node [
    id 250
    label "pomiernie"
  ]
  node [
    id 251
    label "pomiarowy"
  ]
  node [
    id 252
    label "sex"
  ]
  node [
    id 253
    label "transseksualizm"
  ]
  node [
    id 254
    label "organ"
  ]
  node [
    id 255
    label "sk&#243;ra"
  ]
  node [
    id 256
    label "twarz"
  ]
  node [
    id 257
    label "charakterystyka"
  ]
  node [
    id 258
    label "m&#322;ot"
  ]
  node [
    id 259
    label "znak"
  ]
  node [
    id 260
    label "drzewo"
  ]
  node [
    id 261
    label "pr&#243;ba"
  ]
  node [
    id 262
    label "attribute"
  ]
  node [
    id 263
    label "marka"
  ]
  node [
    id 264
    label "szczupak"
  ]
  node [
    id 265
    label "coating"
  ]
  node [
    id 266
    label "krupon"
  ]
  node [
    id 267
    label "harleyowiec"
  ]
  node [
    id 268
    label "tkanka_podsk&#243;rna"
  ]
  node [
    id 269
    label "kurtka"
  ]
  node [
    id 270
    label "metal"
  ]
  node [
    id 271
    label "p&#322;aszcz"
  ]
  node [
    id 272
    label "&#322;upa"
  ]
  node [
    id 273
    label "wyprze&#263;"
  ]
  node [
    id 274
    label "okrywa"
  ]
  node [
    id 275
    label "gruczo&#322;_kuprowy"
  ]
  node [
    id 276
    label "&#380;ycie"
  ]
  node [
    id 277
    label "gruczo&#322;_potowy"
  ]
  node [
    id 278
    label "lico"
  ]
  node [
    id 279
    label "wi&#243;rkownik"
  ]
  node [
    id 280
    label "mizdra"
  ]
  node [
    id 281
    label "dupa"
  ]
  node [
    id 282
    label "rockers"
  ]
  node [
    id 283
    label "&#322;uskowato&#347;&#263;"
  ]
  node [
    id 284
    label "surowiec"
  ]
  node [
    id 285
    label "czerniak_z&#322;o&#347;liwy"
  ]
  node [
    id 286
    label "gruczo&#322;_&#322;ojowy"
  ]
  node [
    id 287
    label "pow&#322;oka"
  ]
  node [
    id 288
    label "zdrowie"
  ]
  node [
    id 289
    label "wyprawa"
  ]
  node [
    id 290
    label "sk&#243;ra_w&#322;a&#347;ciwa"
  ]
  node [
    id 291
    label "hardrockowiec"
  ]
  node [
    id 292
    label "nask&#243;rek"
  ]
  node [
    id 293
    label "gestapowiec"
  ]
  node [
    id 294
    label "funkcja"
  ]
  node [
    id 295
    label "cia&#322;o"
  ]
  node [
    id 296
    label "shell"
  ]
  node [
    id 297
    label "tkanka"
  ]
  node [
    id 298
    label "jednostka_organizacyjna"
  ]
  node [
    id 299
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 300
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 301
    label "tw&#243;r"
  ]
  node [
    id 302
    label "organogeneza"
  ]
  node [
    id 303
    label "zesp&#243;&#322;"
  ]
  node [
    id 304
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 305
    label "struktura_anatomiczna"
  ]
  node [
    id 306
    label "uk&#322;ad"
  ]
  node [
    id 307
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 308
    label "dekortykacja"
  ]
  node [
    id 309
    label "Izba_Konsyliarska"
  ]
  node [
    id 310
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 311
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 312
    label "stomia"
  ]
  node [
    id 313
    label "budowa"
  ]
  node [
    id 314
    label "okolica"
  ]
  node [
    id 315
    label "Komitet_Region&#243;w"
  ]
  node [
    id 316
    label "egzemplarz"
  ]
  node [
    id 317
    label "series"
  ]
  node [
    id 318
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 319
    label "uprawianie"
  ]
  node [
    id 320
    label "praca_rolnicza"
  ]
  node [
    id 321
    label "collection"
  ]
  node [
    id 322
    label "dane"
  ]
  node [
    id 323
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 324
    label "pakiet_klimatyczny"
  ]
  node [
    id 325
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 326
    label "sum"
  ]
  node [
    id 327
    label "gathering"
  ]
  node [
    id 328
    label "album"
  ]
  node [
    id 329
    label "transsexualism"
  ]
  node [
    id 330
    label "zaburzenie_identyfikacji_p&#322;ciowej"
  ]
  node [
    id 331
    label "tranzycja"
  ]
  node [
    id 332
    label "cera"
  ]
  node [
    id 333
    label "wielko&#347;&#263;"
  ]
  node [
    id 334
    label "rys"
  ]
  node [
    id 335
    label "przedstawiciel"
  ]
  node [
    id 336
    label "profil"
  ]
  node [
    id 337
    label "posta&#263;"
  ]
  node [
    id 338
    label "zas&#322;ona"
  ]
  node [
    id 339
    label "p&#243;&#322;profil"
  ]
  node [
    id 340
    label "policzek"
  ]
  node [
    id 341
    label "brew"
  ]
  node [
    id 342
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 343
    label "uj&#281;cie"
  ]
  node [
    id 344
    label "micha"
  ]
  node [
    id 345
    label "reputacja"
  ]
  node [
    id 346
    label "wyraz_twarzy"
  ]
  node [
    id 347
    label "powieka"
  ]
  node [
    id 348
    label "czo&#322;o"
  ]
  node [
    id 349
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 350
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 351
    label "twarzyczka"
  ]
  node [
    id 352
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 353
    label "ucho"
  ]
  node [
    id 354
    label "usta"
  ]
  node [
    id 355
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 356
    label "dzi&#243;b"
  ]
  node [
    id 357
    label "prz&#243;d"
  ]
  node [
    id 358
    label "nos"
  ]
  node [
    id 359
    label "podbr&#243;dek"
  ]
  node [
    id 360
    label "liczko"
  ]
  node [
    id 361
    label "pysk"
  ]
  node [
    id 362
    label "maskowato&#347;&#263;"
  ]
  node [
    id 363
    label "promiskuityzm"
  ]
  node [
    id 364
    label "sztos"
  ]
  node [
    id 365
    label "sexual_activity"
  ]
  node [
    id 366
    label "niedopasowanie_seksualne"
  ]
  node [
    id 367
    label "akt_p&#322;ciowy"
  ]
  node [
    id 368
    label "petting"
  ]
  node [
    id 369
    label "dopasowanie_seksualne"
  ]
  node [
    id 370
    label "wypi&#281;knienie"
  ]
  node [
    id 371
    label "skandaliczny"
  ]
  node [
    id 372
    label "wspania&#322;y"
  ]
  node [
    id 373
    label "szlachetnie"
  ]
  node [
    id 374
    label "gor&#261;cy"
  ]
  node [
    id 375
    label "pi&#281;knie"
  ]
  node [
    id 376
    label "pi&#281;knienie"
  ]
  node [
    id 377
    label "wzruszaj&#261;cy"
  ]
  node [
    id 378
    label "po&#380;&#261;dany"
  ]
  node [
    id 379
    label "cudowny"
  ]
  node [
    id 380
    label "okaza&#322;y"
  ]
  node [
    id 381
    label "dobry"
  ]
  node [
    id 382
    label "stresogenny"
  ]
  node [
    id 383
    label "szczery"
  ]
  node [
    id 384
    label "rozpalenie_si&#281;"
  ]
  node [
    id 385
    label "zdecydowany"
  ]
  node [
    id 386
    label "sensacyjny"
  ]
  node [
    id 387
    label "na_gor&#261;co"
  ]
  node [
    id 388
    label "rozpalanie_si&#281;"
  ]
  node [
    id 389
    label "&#380;arki"
  ]
  node [
    id 390
    label "serdeczny"
  ]
  node [
    id 391
    label "ciep&#322;y"
  ]
  node [
    id 392
    label "g&#322;&#281;boki"
  ]
  node [
    id 393
    label "gor&#261;co"
  ]
  node [
    id 394
    label "seksowny"
  ]
  node [
    id 395
    label "&#347;wie&#380;y"
  ]
  node [
    id 396
    label "wzruszaj&#261;co"
  ]
  node [
    id 397
    label "przejmuj&#261;cy"
  ]
  node [
    id 398
    label "dobroczynny"
  ]
  node [
    id 399
    label "czw&#243;rka"
  ]
  node [
    id 400
    label "spokojny"
  ]
  node [
    id 401
    label "skuteczny"
  ]
  node [
    id 402
    label "&#347;mieszny"
  ]
  node [
    id 403
    label "mi&#322;y"
  ]
  node [
    id 404
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 405
    label "powitanie"
  ]
  node [
    id 406
    label "dobrze"
  ]
  node [
    id 407
    label "ca&#322;y"
  ]
  node [
    id 408
    label "zwrot"
  ]
  node [
    id 409
    label "pomy&#347;lny"
  ]
  node [
    id 410
    label "moralny"
  ]
  node [
    id 411
    label "drogi"
  ]
  node [
    id 412
    label "pozytywny"
  ]
  node [
    id 413
    label "odpowiedni"
  ]
  node [
    id 414
    label "korzystny"
  ]
  node [
    id 415
    label "pos&#322;uszny"
  ]
  node [
    id 416
    label "wspaniale"
  ]
  node [
    id 417
    label "&#347;wietnie"
  ]
  node [
    id 418
    label "spania&#322;y"
  ]
  node [
    id 419
    label "och&#281;do&#380;ny"
  ]
  node [
    id 420
    label "warto&#347;ciowy"
  ]
  node [
    id 421
    label "zajebisty"
  ]
  node [
    id 422
    label "bogato"
  ]
  node [
    id 423
    label "okazale"
  ]
  node [
    id 424
    label "imponuj&#261;cy"
  ]
  node [
    id 425
    label "bogaty"
  ]
  node [
    id 426
    label "poka&#378;ny"
  ]
  node [
    id 427
    label "cudownie"
  ]
  node [
    id 428
    label "wyj&#261;tkowy"
  ]
  node [
    id 429
    label "fantastyczny"
  ]
  node [
    id 430
    label "cudnie"
  ]
  node [
    id 431
    label "&#347;wietny"
  ]
  node [
    id 432
    label "przewspania&#322;y"
  ]
  node [
    id 433
    label "niezwyk&#322;y"
  ]
  node [
    id 434
    label "straszny"
  ]
  node [
    id 435
    label "skandalicznie"
  ]
  node [
    id 436
    label "gorsz&#261;cy"
  ]
  node [
    id 437
    label "pieski"
  ]
  node [
    id 438
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 439
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 440
    label "niekorzystny"
  ]
  node [
    id 441
    label "z&#322;oszczenie"
  ]
  node [
    id 442
    label "sierdzisty"
  ]
  node [
    id 443
    label "niegrzeczny"
  ]
  node [
    id 444
    label "zez&#322;oszczenie"
  ]
  node [
    id 445
    label "zdenerwowany"
  ]
  node [
    id 446
    label "negatywny"
  ]
  node [
    id 447
    label "rozgniewanie"
  ]
  node [
    id 448
    label "gniewanie"
  ]
  node [
    id 449
    label "niemoralny"
  ]
  node [
    id 450
    label "&#378;le"
  ]
  node [
    id 451
    label "niepomy&#347;lny"
  ]
  node [
    id 452
    label "syf"
  ]
  node [
    id 453
    label "szlachetny"
  ]
  node [
    id 454
    label "beautifully"
  ]
  node [
    id 455
    label "stanie_si&#281;"
  ]
  node [
    id 456
    label "stawanie_si&#281;"
  ]
  node [
    id 457
    label "zacnie"
  ]
  node [
    id 458
    label "estetycznie"
  ]
  node [
    id 459
    label "stylowy"
  ]
  node [
    id 460
    label "harmonijnie"
  ]
  node [
    id 461
    label "gatunkowo"
  ]
  node [
    id 462
    label "w_chuj"
  ]
  node [
    id 463
    label "odr&#281;bny"
  ]
  node [
    id 464
    label "bia&#322;og&#322;owski"
  ]
  node [
    id 465
    label "stosowny"
  ]
  node [
    id 466
    label "typowy"
  ]
  node [
    id 467
    label "uroczy"
  ]
  node [
    id 468
    label "prawdziwy"
  ]
  node [
    id 469
    label "kobieco"
  ]
  node [
    id 470
    label "damsko"
  ]
  node [
    id 471
    label "podobny"
  ]
  node [
    id 472
    label "po_damsku"
  ]
  node [
    id 473
    label "&#380;e&#324;sko"
  ]
  node [
    id 474
    label "&#380;e&#324;ski"
  ]
  node [
    id 475
    label "przypominanie"
  ]
  node [
    id 476
    label "podobnie"
  ]
  node [
    id 477
    label "asymilowanie"
  ]
  node [
    id 478
    label "upodabnianie_si&#281;"
  ]
  node [
    id 479
    label "upodobnienie"
  ]
  node [
    id 480
    label "drugi"
  ]
  node [
    id 481
    label "taki"
  ]
  node [
    id 482
    label "upodobnienie_si&#281;"
  ]
  node [
    id 483
    label "zasymilowanie"
  ]
  node [
    id 484
    label "typowo"
  ]
  node [
    id 485
    label "cz&#281;sty"
  ]
  node [
    id 486
    label "zwyk&#322;y"
  ]
  node [
    id 487
    label "&#322;adny"
  ]
  node [
    id 488
    label "uroczny"
  ]
  node [
    id 489
    label "uroczo"
  ]
  node [
    id 490
    label "sympatyczny"
  ]
  node [
    id 491
    label "powabny"
  ]
  node [
    id 492
    label "podniecaj&#261;cy"
  ]
  node [
    id 493
    label "atrakcyjny"
  ]
  node [
    id 494
    label "seksownie"
  ]
  node [
    id 495
    label "&#380;ywny"
  ]
  node [
    id 496
    label "naturalny"
  ]
  node [
    id 497
    label "naprawd&#281;"
  ]
  node [
    id 498
    label "realnie"
  ]
  node [
    id 499
    label "zgodny"
  ]
  node [
    id 500
    label "m&#261;dry"
  ]
  node [
    id 501
    label "prawdziwie"
  ]
  node [
    id 502
    label "wyodr&#281;bnienie_si&#281;"
  ]
  node [
    id 503
    label "kolejny"
  ]
  node [
    id 504
    label "wydzielenie"
  ]
  node [
    id 505
    label "osobno"
  ]
  node [
    id 506
    label "inszy"
  ]
  node [
    id 507
    label "wyodr&#281;bnianie"
  ]
  node [
    id 508
    label "inny"
  ]
  node [
    id 509
    label "wyodr&#281;bnianie_si&#281;"
  ]
  node [
    id 510
    label "odr&#281;bnie"
  ]
  node [
    id 511
    label "osobny"
  ]
  node [
    id 512
    label "nale&#380;yty"
  ]
  node [
    id 513
    label "stosownie"
  ]
  node [
    id 514
    label "damski"
  ]
  node [
    id 515
    label "po_bia&#322;og&#322;owsku"
  ]
  node [
    id 516
    label "przekazywa&#263;"
  ]
  node [
    id 517
    label "dostarcza&#263;"
  ]
  node [
    id 518
    label "robi&#263;"
  ]
  node [
    id 519
    label "mie&#263;_miejsce"
  ]
  node [
    id 520
    label "&#322;adowa&#263;"
  ]
  node [
    id 521
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 522
    label "give"
  ]
  node [
    id 523
    label "przeznacza&#263;"
  ]
  node [
    id 524
    label "surrender"
  ]
  node [
    id 525
    label "traktowa&#263;"
  ]
  node [
    id 526
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 527
    label "obiecywa&#263;"
  ]
  node [
    id 528
    label "odst&#281;powa&#263;"
  ]
  node [
    id 529
    label "tender"
  ]
  node [
    id 530
    label "rap"
  ]
  node [
    id 531
    label "umieszcza&#263;"
  ]
  node [
    id 532
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 533
    label "t&#322;uc"
  ]
  node [
    id 534
    label "powierza&#263;"
  ]
  node [
    id 535
    label "render"
  ]
  node [
    id 536
    label "wpiernicza&#263;"
  ]
  node [
    id 537
    label "exsert"
  ]
  node [
    id 538
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 539
    label "train"
  ]
  node [
    id 540
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 541
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 542
    label "p&#322;aci&#263;"
  ]
  node [
    id 543
    label "hold_out"
  ]
  node [
    id 544
    label "nalewa&#263;"
  ]
  node [
    id 545
    label "zezwala&#263;"
  ]
  node [
    id 546
    label "hold"
  ]
  node [
    id 547
    label "harbinger"
  ]
  node [
    id 548
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 549
    label "pledge"
  ]
  node [
    id 550
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 551
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 552
    label "poddawa&#263;"
  ]
  node [
    id 553
    label "dotyczy&#263;"
  ]
  node [
    id 554
    label "use"
  ]
  node [
    id 555
    label "perform"
  ]
  node [
    id 556
    label "wychodzi&#263;"
  ]
  node [
    id 557
    label "seclude"
  ]
  node [
    id 558
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 559
    label "nak&#322;ania&#263;"
  ]
  node [
    id 560
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 561
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 562
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 563
    label "dzia&#322;a&#263;"
  ]
  node [
    id 564
    label "appear"
  ]
  node [
    id 565
    label "unwrap"
  ]
  node [
    id 566
    label "rezygnowa&#263;"
  ]
  node [
    id 567
    label "overture"
  ]
  node [
    id 568
    label "uczestniczy&#263;"
  ]
  node [
    id 569
    label "organizowa&#263;"
  ]
  node [
    id 570
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 571
    label "czyni&#263;"
  ]
  node [
    id 572
    label "stylizowa&#263;"
  ]
  node [
    id 573
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 574
    label "falowa&#263;"
  ]
  node [
    id 575
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 576
    label "peddle"
  ]
  node [
    id 577
    label "wydala&#263;"
  ]
  node [
    id 578
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 579
    label "tentegowa&#263;"
  ]
  node [
    id 580
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 581
    label "urz&#261;dza&#263;"
  ]
  node [
    id 582
    label "oszukiwa&#263;"
  ]
  node [
    id 583
    label "work"
  ]
  node [
    id 584
    label "ukazywa&#263;"
  ]
  node [
    id 585
    label "przerabia&#263;"
  ]
  node [
    id 586
    label "post&#281;powa&#263;"
  ]
  node [
    id 587
    label "plasowa&#263;"
  ]
  node [
    id 588
    label "umie&#347;ci&#263;"
  ]
  node [
    id 589
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 590
    label "pomieszcza&#263;"
  ]
  node [
    id 591
    label "accommodate"
  ]
  node [
    id 592
    label "zmienia&#263;"
  ]
  node [
    id 593
    label "powodowa&#263;"
  ]
  node [
    id 594
    label "venture"
  ]
  node [
    id 595
    label "okre&#347;la&#263;"
  ]
  node [
    id 596
    label "wyznawa&#263;"
  ]
  node [
    id 597
    label "oddawa&#263;"
  ]
  node [
    id 598
    label "confide"
  ]
  node [
    id 599
    label "zleca&#263;"
  ]
  node [
    id 600
    label "ufa&#263;"
  ]
  node [
    id 601
    label "command"
  ]
  node [
    id 602
    label "grant"
  ]
  node [
    id 603
    label "wydawa&#263;"
  ]
  node [
    id 604
    label "pay"
  ]
  node [
    id 605
    label "osi&#261;ga&#263;"
  ]
  node [
    id 606
    label "buli&#263;"
  ]
  node [
    id 607
    label "get"
  ]
  node [
    id 608
    label "wytwarza&#263;"
  ]
  node [
    id 609
    label "odsuwa&#263;_si&#281;"
  ]
  node [
    id 610
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 611
    label "odwr&#243;t"
  ]
  node [
    id 612
    label "zrzeka&#263;_si&#281;"
  ]
  node [
    id 613
    label "impart"
  ]
  node [
    id 614
    label "uznawa&#263;"
  ]
  node [
    id 615
    label "authorize"
  ]
  node [
    id 616
    label "ustala&#263;"
  ]
  node [
    id 617
    label "indicate"
  ]
  node [
    id 618
    label "wysy&#322;a&#263;"
  ]
  node [
    id 619
    label "podawa&#263;"
  ]
  node [
    id 620
    label "wp&#322;aca&#263;"
  ]
  node [
    id 621
    label "sygna&#322;"
  ]
  node [
    id 622
    label "muzyka_rozrywkowa"
  ]
  node [
    id 623
    label "karpiowate"
  ]
  node [
    id 624
    label "czarna_muzyka"
  ]
  node [
    id 625
    label "drapie&#380;nik"
  ]
  node [
    id 626
    label "asp"
  ]
  node [
    id 627
    label "pojazd_kolejowy"
  ]
  node [
    id 628
    label "wagon"
  ]
  node [
    id 629
    label "poci&#261;g"
  ]
  node [
    id 630
    label "statek"
  ]
  node [
    id 631
    label "parow&#243;z_tendrowy"
  ]
  node [
    id 632
    label "okr&#281;t"
  ]
  node [
    id 633
    label "applaud"
  ]
  node [
    id 634
    label "wk&#322;ada&#263;"
  ]
  node [
    id 635
    label "zasila&#263;"
  ]
  node [
    id 636
    label "charge"
  ]
  node [
    id 637
    label "nabija&#263;"
  ]
  node [
    id 638
    label "bi&#263;"
  ]
  node [
    id 639
    label "bro&#324;_palna"
  ]
  node [
    id 640
    label "wype&#322;nia&#263;"
  ]
  node [
    id 641
    label "piure"
  ]
  node [
    id 642
    label "butcher"
  ]
  node [
    id 643
    label "murder"
  ]
  node [
    id 644
    label "produkowa&#263;"
  ]
  node [
    id 645
    label "napierdziela&#263;"
  ]
  node [
    id 646
    label "fight"
  ]
  node [
    id 647
    label "mia&#380;d&#380;y&#263;"
  ]
  node [
    id 648
    label "rozdrabnia&#263;"
  ]
  node [
    id 649
    label "wystukiwa&#263;"
  ]
  node [
    id 650
    label "rzn&#261;&#263;"
  ]
  node [
    id 651
    label "plu&#263;"
  ]
  node [
    id 652
    label "uderza&#263;"
  ]
  node [
    id 653
    label "gra&#263;"
  ]
  node [
    id 654
    label "odpala&#263;"
  ]
  node [
    id 655
    label "r&#380;n&#261;&#263;"
  ]
  node [
    id 656
    label "zabija&#263;"
  ]
  node [
    id 657
    label "powtarza&#263;"
  ]
  node [
    id 658
    label "stuka&#263;"
  ]
  node [
    id 659
    label "niszczy&#263;"
  ]
  node [
    id 660
    label "write_out"
  ]
  node [
    id 661
    label "wpl&#261;tywa&#263;"
  ]
  node [
    id 662
    label "je&#347;&#263;"
  ]
  node [
    id 663
    label "z&#322;o&#347;ci&#263;"
  ]
  node [
    id 664
    label "wpycha&#263;"
  ]
  node [
    id 665
    label "zalewa&#263;"
  ]
  node [
    id 666
    label "inculcate"
  ]
  node [
    id 667
    label "pour"
  ]
  node [
    id 668
    label "la&#263;"
  ]
  node [
    id 669
    label "notice"
  ]
  node [
    id 670
    label "zobaczy&#263;"
  ]
  node [
    id 671
    label "cognizance"
  ]
  node [
    id 672
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 673
    label "u&#347;wiadomi&#263;_sobie"
  ]
  node [
    id 674
    label "spoziera&#263;"
  ]
  node [
    id 675
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 676
    label "peek"
  ]
  node [
    id 677
    label "postrzec"
  ]
  node [
    id 678
    label "popatrze&#263;"
  ]
  node [
    id 679
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 680
    label "pojrze&#263;"
  ]
  node [
    id 681
    label "dostrzec"
  ]
  node [
    id 682
    label "spot"
  ]
  node [
    id 683
    label "przekona&#263;_si&#281;"
  ]
  node [
    id 684
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 685
    label "go_steady"
  ]
  node [
    id 686
    label "zinterpretowa&#263;"
  ]
  node [
    id 687
    label "spotka&#263;"
  ]
  node [
    id 688
    label "obejrze&#263;"
  ]
  node [
    id 689
    label "znale&#378;&#263;"
  ]
  node [
    id 690
    label "see"
  ]
  node [
    id 691
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 692
    label "wyczyszczenie_si&#281;"
  ]
  node [
    id 693
    label "go&#322;o"
  ]
  node [
    id 694
    label "do_go&#322;a"
  ]
  node [
    id 695
    label "biedny"
  ]
  node [
    id 696
    label "sam"
  ]
  node [
    id 697
    label "do_naga"
  ]
  node [
    id 698
    label "wyczyszczanie_si&#281;"
  ]
  node [
    id 699
    label "sklep"
  ]
  node [
    id 700
    label "bankrutowanie"
  ]
  node [
    id 701
    label "ubo&#380;enie"
  ]
  node [
    id 702
    label "go&#322;odupiec"
  ]
  node [
    id 703
    label "nieszcz&#281;&#347;liwy"
  ]
  node [
    id 704
    label "zubo&#380;enie"
  ]
  node [
    id 705
    label "raw_material"
  ]
  node [
    id 706
    label "zubo&#380;anie"
  ]
  node [
    id 707
    label "ho&#322;ysz"
  ]
  node [
    id 708
    label "zbiednienie"
  ]
  node [
    id 709
    label "proletariusz"
  ]
  node [
    id 710
    label "sytuowany"
  ]
  node [
    id 711
    label "biedota"
  ]
  node [
    id 712
    label "sp&#322;ukany"
  ]
  node [
    id 713
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 714
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 715
    label "rzecz"
  ]
  node [
    id 716
    label "oczy"
  ]
  node [
    id 717
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 718
    label "&#378;renica"
  ]
  node [
    id 719
    label "uwaga"
  ]
  node [
    id 720
    label "spojrzenie"
  ]
  node [
    id 721
    label "&#347;lepko"
  ]
  node [
    id 722
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 723
    label "siniec"
  ]
  node [
    id 724
    label "wzrok"
  ]
  node [
    id 725
    label "kaprawie&#263;"
  ]
  node [
    id 726
    label "spoj&#243;wka"
  ]
  node [
    id 727
    label "ga&#322;ka_oczna"
  ]
  node [
    id 728
    label "kaprawienie"
  ]
  node [
    id 729
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 730
    label "ros&#243;&#322;"
  ]
  node [
    id 731
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 732
    label "wypowied&#378;"
  ]
  node [
    id 733
    label "&#347;lepie"
  ]
  node [
    id 734
    label "nerw_wzrokowy"
  ]
  node [
    id 735
    label "coloboma"
  ]
  node [
    id 736
    label "m&#281;tnienie"
  ]
  node [
    id 737
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 738
    label "widzenie"
  ]
  node [
    id 739
    label "okulista"
  ]
  node [
    id 740
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 741
    label "zmys&#322;"
  ]
  node [
    id 742
    label "expression"
  ]
  node [
    id 743
    label "widzie&#263;"
  ]
  node [
    id 744
    label "m&#281;tnie&#263;"
  ]
  node [
    id 745
    label "kontakt"
  ]
  node [
    id 746
    label "stan"
  ]
  node [
    id 747
    label "nagana"
  ]
  node [
    id 748
    label "tekst"
  ]
  node [
    id 749
    label "upomnienie"
  ]
  node [
    id 750
    label "dzienniczek"
  ]
  node [
    id 751
    label "wzgl&#261;d"
  ]
  node [
    id 752
    label "gossip"
  ]
  node [
    id 753
    label "patrze&#263;"
  ]
  node [
    id 754
    label "expectation"
  ]
  node [
    id 755
    label "popatrzenie"
  ]
  node [
    id 756
    label "wytw&#243;r"
  ]
  node [
    id 757
    label "pojmowanie"
  ]
  node [
    id 758
    label "stare"
  ]
  node [
    id 759
    label "decentracja"
  ]
  node [
    id 760
    label "object"
  ]
  node [
    id 761
    label "temat"
  ]
  node [
    id 762
    label "wpadni&#281;cie"
  ]
  node [
    id 763
    label "mienie"
  ]
  node [
    id 764
    label "przyroda"
  ]
  node [
    id 765
    label "obiekt"
  ]
  node [
    id 766
    label "kultura"
  ]
  node [
    id 767
    label "wpa&#347;&#263;"
  ]
  node [
    id 768
    label "wpadanie"
  ]
  node [
    id 769
    label "wpada&#263;"
  ]
  node [
    id 770
    label "pos&#322;uchanie"
  ]
  node [
    id 771
    label "sparafrazowanie"
  ]
  node [
    id 772
    label "strawestowa&#263;"
  ]
  node [
    id 773
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 774
    label "trawestowa&#263;"
  ]
  node [
    id 775
    label "sparafrazowa&#263;"
  ]
  node [
    id 776
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 777
    label "sformu&#322;owanie"
  ]
  node [
    id 778
    label "parafrazowanie"
  ]
  node [
    id 779
    label "ozdobnik"
  ]
  node [
    id 780
    label "delimitacja"
  ]
  node [
    id 781
    label "parafrazowa&#263;"
  ]
  node [
    id 782
    label "stylizacja"
  ]
  node [
    id 783
    label "komunikat"
  ]
  node [
    id 784
    label "trawestowanie"
  ]
  node [
    id 785
    label "strawestowanie"
  ]
  node [
    id 786
    label "rezultat"
  ]
  node [
    id 787
    label "para"
  ]
  node [
    id 788
    label "eyeliner"
  ]
  node [
    id 789
    label "ga&#322;y"
  ]
  node [
    id 790
    label "zupa"
  ]
  node [
    id 791
    label "consomme"
  ]
  node [
    id 792
    label "mruganie"
  ]
  node [
    id 793
    label "tarczka"
  ]
  node [
    id 794
    label "sk&#243;rzak"
  ]
  node [
    id 795
    label "mruga&#263;"
  ]
  node [
    id 796
    label "entropion"
  ]
  node [
    id 797
    label "ptoza"
  ]
  node [
    id 798
    label "mrugni&#281;cie"
  ]
  node [
    id 799
    label "mrugn&#261;&#263;"
  ]
  node [
    id 800
    label "wrodzone_zw&#281;&#380;enie_szpary_powiekowej"
  ]
  node [
    id 801
    label "grad&#243;wka"
  ]
  node [
    id 802
    label "j&#281;czmie&#324;"
  ]
  node [
    id 803
    label "rz&#281;sa"
  ]
  node [
    id 804
    label "ektropion"
  ]
  node [
    id 805
    label "&#347;luz&#243;wka"
  ]
  node [
    id 806
    label "ropie&#263;"
  ]
  node [
    id 807
    label "ropienie"
  ]
  node [
    id 808
    label "kr&#281;gi_pod_oczami"
  ]
  node [
    id 809
    label "st&#322;uczenie"
  ]
  node [
    id 810
    label "effusion"
  ]
  node [
    id 811
    label "oznaka"
  ]
  node [
    id 812
    label "obw&#243;dka"
  ]
  node [
    id 813
    label "przebarwienie"
  ]
  node [
    id 814
    label "zm&#281;czenie"
  ]
  node [
    id 815
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 816
    label "zmiana"
  ]
  node [
    id 817
    label "szczelina"
  ]
  node [
    id 818
    label "wada_wrodzona"
  ]
  node [
    id 819
    label "provider"
  ]
  node [
    id 820
    label "b&#322;&#261;d"
  ]
  node [
    id 821
    label "hipertekst"
  ]
  node [
    id 822
    label "cyberprzestrze&#324;"
  ]
  node [
    id 823
    label "mem"
  ]
  node [
    id 824
    label "gra_sieciowa"
  ]
  node [
    id 825
    label "grooming"
  ]
  node [
    id 826
    label "media"
  ]
  node [
    id 827
    label "biznes_elektroniczny"
  ]
  node [
    id 828
    label "sie&#263;_komputerowa"
  ]
  node [
    id 829
    label "punkt_dost&#281;pu"
  ]
  node [
    id 830
    label "us&#322;uga_internetowa"
  ]
  node [
    id 831
    label "netbook"
  ]
  node [
    id 832
    label "e-hazard"
  ]
  node [
    id 833
    label "podcast"
  ]
  node [
    id 834
    label "strona"
  ]
  node [
    id 835
    label "wyrafinowany"
  ]
  node [
    id 836
    label "niepo&#347;ledni"
  ]
  node [
    id 837
    label "du&#380;y"
  ]
  node [
    id 838
    label "chwalebny"
  ]
  node [
    id 839
    label "z_wysoka"
  ]
  node [
    id 840
    label "wznios&#322;y"
  ]
  node [
    id 841
    label "daleki"
  ]
  node [
    id 842
    label "wysoce"
  ]
  node [
    id 843
    label "szczytnie"
  ]
  node [
    id 844
    label "znaczny"
  ]
  node [
    id 845
    label "wysoko"
  ]
  node [
    id 846
    label "uprzywilejowany"
  ]
  node [
    id 847
    label "doros&#322;y"
  ]
  node [
    id 848
    label "niema&#322;o"
  ]
  node [
    id 849
    label "wiele"
  ]
  node [
    id 850
    label "rozwini&#281;ty"
  ]
  node [
    id 851
    label "dorodny"
  ]
  node [
    id 852
    label "wa&#380;ny"
  ]
  node [
    id 853
    label "du&#380;o"
  ]
  node [
    id 854
    label "szczeg&#243;lny"
  ]
  node [
    id 855
    label "lekki"
  ]
  node [
    id 856
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 857
    label "znacznie"
  ]
  node [
    id 858
    label "zauwa&#380;alny"
  ]
  node [
    id 859
    label "niez&#322;y"
  ]
  node [
    id 860
    label "niepo&#347;lednio"
  ]
  node [
    id 861
    label "pochwalny"
  ]
  node [
    id 862
    label "powa&#380;ny"
  ]
  node [
    id 863
    label "chwalebnie"
  ]
  node [
    id 864
    label "podnios&#322;y"
  ]
  node [
    id 865
    label "wznio&#347;le"
  ]
  node [
    id 866
    label "oderwany"
  ]
  node [
    id 867
    label "rewaluowanie"
  ]
  node [
    id 868
    label "warto&#347;ciowo"
  ]
  node [
    id 869
    label "u&#380;yteczny"
  ]
  node [
    id 870
    label "zrewaluowanie"
  ]
  node [
    id 871
    label "obyty"
  ]
  node [
    id 872
    label "wykwintny"
  ]
  node [
    id 873
    label "wyrafinowanie"
  ]
  node [
    id 874
    label "wymy&#347;lny"
  ]
  node [
    id 875
    label "dawny"
  ]
  node [
    id 876
    label "ogl&#281;dny"
  ]
  node [
    id 877
    label "d&#322;ugi"
  ]
  node [
    id 878
    label "daleko"
  ]
  node [
    id 879
    label "odleg&#322;y"
  ]
  node [
    id 880
    label "r&#243;&#380;ny"
  ]
  node [
    id 881
    label "odlegle"
  ]
  node [
    id 882
    label "obcy"
  ]
  node [
    id 883
    label "nieobecny"
  ]
  node [
    id 884
    label "g&#243;rno"
  ]
  node [
    id 885
    label "szczytny"
  ]
  node [
    id 886
    label "intensywnie"
  ]
  node [
    id 887
    label "wielki"
  ]
  node [
    id 888
    label "niezmiernie"
  ]
  node [
    id 889
    label "increase"
  ]
  node [
    id 890
    label "rozw&#243;j"
  ]
  node [
    id 891
    label "wegetacja"
  ]
  node [
    id 892
    label "wysoko&#347;&#263;"
  ]
  node [
    id 893
    label "tallness"
  ]
  node [
    id 894
    label "altitude"
  ]
  node [
    id 895
    label "rozmiar"
  ]
  node [
    id 896
    label "degree"
  ]
  node [
    id 897
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 898
    label "odcinek"
  ]
  node [
    id 899
    label "k&#261;t"
  ]
  node [
    id 900
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 901
    label "procedura"
  ]
  node [
    id 902
    label "proces_biologiczny"
  ]
  node [
    id 903
    label "z&#322;ote_czasy"
  ]
  node [
    id 904
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 905
    label "process"
  ]
  node [
    id 906
    label "cycle"
  ]
  node [
    id 907
    label "rewizja"
  ]
  node [
    id 908
    label "passage"
  ]
  node [
    id 909
    label "change"
  ]
  node [
    id 910
    label "ferment"
  ]
  node [
    id 911
    label "komplet"
  ]
  node [
    id 912
    label "anatomopatolog"
  ]
  node [
    id 913
    label "zmianka"
  ]
  node [
    id 914
    label "czas"
  ]
  node [
    id 915
    label "amendment"
  ]
  node [
    id 916
    label "odmienianie"
  ]
  node [
    id 917
    label "tura"
  ]
  node [
    id 918
    label "faza"
  ]
  node [
    id 919
    label "rozkwit"
  ]
  node [
    id 920
    label "rostowy"
  ]
  node [
    id 921
    label "vegetation"
  ]
  node [
    id 922
    label "ro&#347;lina"
  ]
  node [
    id 923
    label "chtoniczny"
  ]
  node [
    id 924
    label "cebula_przybyszowa"
  ]
  node [
    id 925
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 926
    label "equal"
  ]
  node [
    id 927
    label "trwa&#263;"
  ]
  node [
    id 928
    label "chodzi&#263;"
  ]
  node [
    id 929
    label "si&#281;ga&#263;"
  ]
  node [
    id 930
    label "obecno&#347;&#263;"
  ]
  node [
    id 931
    label "stand"
  ]
  node [
    id 932
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 933
    label "participate"
  ]
  node [
    id 934
    label "istnie&#263;"
  ]
  node [
    id 935
    label "pozostawa&#263;"
  ]
  node [
    id 936
    label "zostawa&#263;"
  ]
  node [
    id 937
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 938
    label "adhere"
  ]
  node [
    id 939
    label "compass"
  ]
  node [
    id 940
    label "korzysta&#263;"
  ]
  node [
    id 941
    label "appreciation"
  ]
  node [
    id 942
    label "dociera&#263;"
  ]
  node [
    id 943
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 944
    label "mierzy&#263;"
  ]
  node [
    id 945
    label "u&#380;ywa&#263;"
  ]
  node [
    id 946
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 947
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 948
    label "being"
  ]
  node [
    id 949
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 950
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 951
    label "p&#322;ywa&#263;"
  ]
  node [
    id 952
    label "run"
  ]
  node [
    id 953
    label "bangla&#263;"
  ]
  node [
    id 954
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 955
    label "przebiega&#263;"
  ]
  node [
    id 956
    label "proceed"
  ]
  node [
    id 957
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 958
    label "carry"
  ]
  node [
    id 959
    label "bywa&#263;"
  ]
  node [
    id 960
    label "dziama&#263;"
  ]
  node [
    id 961
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 962
    label "stara&#263;_si&#281;"
  ]
  node [
    id 963
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 964
    label "str&#243;j"
  ]
  node [
    id 965
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 966
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 967
    label "krok"
  ]
  node [
    id 968
    label "tryb"
  ]
  node [
    id 969
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 970
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 971
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 972
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 973
    label "continue"
  ]
  node [
    id 974
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 975
    label "Ohio"
  ]
  node [
    id 976
    label "wci&#281;cie"
  ]
  node [
    id 977
    label "Nowy_York"
  ]
  node [
    id 978
    label "warstwa"
  ]
  node [
    id 979
    label "samopoczucie"
  ]
  node [
    id 980
    label "Illinois"
  ]
  node [
    id 981
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 982
    label "state"
  ]
  node [
    id 983
    label "Jukatan"
  ]
  node [
    id 984
    label "Kalifornia"
  ]
  node [
    id 985
    label "Wirginia"
  ]
  node [
    id 986
    label "wektor"
  ]
  node [
    id 987
    label "Teksas"
  ]
  node [
    id 988
    label "Goa"
  ]
  node [
    id 989
    label "Waszyngton"
  ]
  node [
    id 990
    label "Massachusetts"
  ]
  node [
    id 991
    label "Alaska"
  ]
  node [
    id 992
    label "Arakan"
  ]
  node [
    id 993
    label "Hawaje"
  ]
  node [
    id 994
    label "Maryland"
  ]
  node [
    id 995
    label "punkt"
  ]
  node [
    id 996
    label "Michigan"
  ]
  node [
    id 997
    label "Arizona"
  ]
  node [
    id 998
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 999
    label "Georgia"
  ]
  node [
    id 1000
    label "poziom"
  ]
  node [
    id 1001
    label "Pensylwania"
  ]
  node [
    id 1002
    label "shape"
  ]
  node [
    id 1003
    label "Luizjana"
  ]
  node [
    id 1004
    label "Nowy_Meksyk"
  ]
  node [
    id 1005
    label "Alabama"
  ]
  node [
    id 1006
    label "ilo&#347;&#263;"
  ]
  node [
    id 1007
    label "Kansas"
  ]
  node [
    id 1008
    label "Oregon"
  ]
  node [
    id 1009
    label "Floryda"
  ]
  node [
    id 1010
    label "Oklahoma"
  ]
  node [
    id 1011
    label "jednostka_administracyjna"
  ]
  node [
    id 1012
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1013
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 1014
    label "pilnowa&#263;"
  ]
  node [
    id 1015
    label "my&#347;le&#263;"
  ]
  node [
    id 1016
    label "consider"
  ]
  node [
    id 1017
    label "deliver"
  ]
  node [
    id 1018
    label "obserwowa&#263;"
  ]
  node [
    id 1019
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 1020
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 1021
    label "take_care"
  ]
  node [
    id 1022
    label "troska&#263;_si&#281;"
  ]
  node [
    id 1023
    label "rozpatrywa&#263;"
  ]
  node [
    id 1024
    label "zamierza&#263;"
  ]
  node [
    id 1025
    label "argue"
  ]
  node [
    id 1026
    label "os&#261;dza&#263;"
  ]
  node [
    id 1027
    label "stwierdza&#263;"
  ]
  node [
    id 1028
    label "przyznawa&#263;"
  ]
  node [
    id 1029
    label "zachowywa&#263;"
  ]
  node [
    id 1030
    label "dostrzega&#263;"
  ]
  node [
    id 1031
    label "look"
  ]
  node [
    id 1032
    label "cover"
  ]
  node [
    id 1033
    label "dow&#243;d"
  ]
  node [
    id 1034
    label "oznakowanie"
  ]
  node [
    id 1035
    label "fakt"
  ]
  node [
    id 1036
    label "stawia&#263;"
  ]
  node [
    id 1037
    label "point"
  ]
  node [
    id 1038
    label "kodzik"
  ]
  node [
    id 1039
    label "postawi&#263;"
  ]
  node [
    id 1040
    label "mark"
  ]
  node [
    id 1041
    label "herb"
  ]
  node [
    id 1042
    label "implikowa&#263;"
  ]
  node [
    id 1043
    label "rzut_m&#322;otem"
  ]
  node [
    id 1044
    label "r&#261;b"
  ]
  node [
    id 1045
    label "zbicie"
  ]
  node [
    id 1046
    label "kowad&#322;o"
  ]
  node [
    id 1047
    label "m&#322;otowate"
  ]
  node [
    id 1048
    label "rekin"
  ]
  node [
    id 1049
    label "obuch"
  ]
  node [
    id 1050
    label "narz&#281;dzie"
  ]
  node [
    id 1051
    label "lekkoatletyka"
  ]
  node [
    id 1052
    label "konkurencja"
  ]
  node [
    id 1053
    label "prostak"
  ]
  node [
    id 1054
    label "klepanie"
  ]
  node [
    id 1055
    label "obrabiarka"
  ]
  node [
    id 1056
    label "kula"
  ]
  node [
    id 1057
    label "klepa&#263;"
  ]
  node [
    id 1058
    label "t&#281;pak"
  ]
  node [
    id 1059
    label "ciemniak"
  ]
  node [
    id 1060
    label "maszyna"
  ]
  node [
    id 1061
    label "bijak"
  ]
  node [
    id 1062
    label "ku&#378;nica"
  ]
  node [
    id 1063
    label "m&#322;otownia"
  ]
  node [
    id 1064
    label "do&#347;wiadczenie"
  ]
  node [
    id 1065
    label "spotkanie"
  ]
  node [
    id 1066
    label "pobiera&#263;"
  ]
  node [
    id 1067
    label "metal_szlachetny"
  ]
  node [
    id 1068
    label "pobranie"
  ]
  node [
    id 1069
    label "usi&#322;owanie"
  ]
  node [
    id 1070
    label "pobra&#263;"
  ]
  node [
    id 1071
    label "pobieranie"
  ]
  node [
    id 1072
    label "effort"
  ]
  node [
    id 1073
    label "analiza_chemiczna"
  ]
  node [
    id 1074
    label "item"
  ]
  node [
    id 1075
    label "sytuacja"
  ]
  node [
    id 1076
    label "probiernictwo"
  ]
  node [
    id 1077
    label "test"
  ]
  node [
    id 1078
    label "opis"
  ]
  node [
    id 1079
    label "parametr"
  ]
  node [
    id 1080
    label "analiza"
  ]
  node [
    id 1081
    label "specyfikacja"
  ]
  node [
    id 1082
    label "wykres"
  ]
  node [
    id 1083
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 1084
    label "charakter"
  ]
  node [
    id 1085
    label "interpretacja"
  ]
  node [
    id 1086
    label "pier&#347;nica"
  ]
  node [
    id 1087
    label "parzelnia"
  ]
  node [
    id 1088
    label "zadrzewienie"
  ]
  node [
    id 1089
    label "&#380;ywica"
  ]
  node [
    id 1090
    label "fanerofit"
  ]
  node [
    id 1091
    label "zacios"
  ]
  node [
    id 1092
    label "graf"
  ]
  node [
    id 1093
    label "las"
  ]
  node [
    id 1094
    label "karczowa&#263;"
  ]
  node [
    id 1095
    label "wykarczowa&#263;"
  ]
  node [
    id 1096
    label "karczowanie"
  ]
  node [
    id 1097
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 1098
    label "&#322;yko"
  ]
  node [
    id 1099
    label "szpaler"
  ]
  node [
    id 1100
    label "chodnik"
  ]
  node [
    id 1101
    label "wykarczowanie"
  ]
  node [
    id 1102
    label "skupina"
  ]
  node [
    id 1103
    label "pie&#324;"
  ]
  node [
    id 1104
    label "kora"
  ]
  node [
    id 1105
    label "drzewostan"
  ]
  node [
    id 1106
    label "brodaczka"
  ]
  node [
    id 1107
    label "korona"
  ]
  node [
    id 1108
    label "stamp"
  ]
  node [
    id 1109
    label "Honda"
  ]
  node [
    id 1110
    label "Intel"
  ]
  node [
    id 1111
    label "Harley-Davidson"
  ]
  node [
    id 1112
    label "oznaczenie"
  ]
  node [
    id 1113
    label "Coca-Cola"
  ]
  node [
    id 1114
    label "Niemcy"
  ]
  node [
    id 1115
    label "Snickers"
  ]
  node [
    id 1116
    label "Pepsi-Cola"
  ]
  node [
    id 1117
    label "jako&#347;&#263;"
  ]
  node [
    id 1118
    label "jednostka_monetarna"
  ]
  node [
    id 1119
    label "Cessna"
  ]
  node [
    id 1120
    label "znak_jako&#347;ci"
  ]
  node [
    id 1121
    label "Inka"
  ]
  node [
    id 1122
    label "funt"
  ]
  node [
    id 1123
    label "jednostka_avoirdupois"
  ]
  node [
    id 1124
    label "Tymbark"
  ]
  node [
    id 1125
    label "Daewoo"
  ]
  node [
    id 1126
    label "Romet"
  ]
  node [
    id 1127
    label "firm&#243;wka"
  ]
  node [
    id 1128
    label "znaczek_pocztowy"
  ]
  node [
    id 1129
    label "branding"
  ]
  node [
    id 1130
    label "fenig"
  ]
  node [
    id 1131
    label "charakterystycznie"
  ]
  node [
    id 1132
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 1133
    label "wyj&#261;tkowo"
  ]
  node [
    id 1134
    label "szczeg&#243;lnie"
  ]
  node [
    id 1135
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 1136
    label "ojciec"
  ]
  node [
    id 1137
    label "jegomo&#347;&#263;"
  ]
  node [
    id 1138
    label "andropauza"
  ]
  node [
    id 1139
    label "pa&#324;stwo"
  ]
  node [
    id 1140
    label "bratek"
  ]
  node [
    id 1141
    label "samiec"
  ]
  node [
    id 1142
    label "ch&#322;opina"
  ]
  node [
    id 1143
    label "twardziel"
  ]
  node [
    id 1144
    label "androlog"
  ]
  node [
    id 1145
    label "m&#261;&#380;"
  ]
  node [
    id 1146
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1147
    label "Katar"
  ]
  node [
    id 1148
    label "Libia"
  ]
  node [
    id 1149
    label "Gwatemala"
  ]
  node [
    id 1150
    label "Ekwador"
  ]
  node [
    id 1151
    label "Afganistan"
  ]
  node [
    id 1152
    label "Tad&#380;ykistan"
  ]
  node [
    id 1153
    label "Bhutan"
  ]
  node [
    id 1154
    label "Argentyna"
  ]
  node [
    id 1155
    label "D&#380;ibuti"
  ]
  node [
    id 1156
    label "Wenezuela"
  ]
  node [
    id 1157
    label "Gabon"
  ]
  node [
    id 1158
    label "Ukraina"
  ]
  node [
    id 1159
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1160
    label "Rwanda"
  ]
  node [
    id 1161
    label "Liechtenstein"
  ]
  node [
    id 1162
    label "organizacja"
  ]
  node [
    id 1163
    label "Sri_Lanka"
  ]
  node [
    id 1164
    label "Madagaskar"
  ]
  node [
    id 1165
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1166
    label "Kongo"
  ]
  node [
    id 1167
    label "Tonga"
  ]
  node [
    id 1168
    label "Bangladesz"
  ]
  node [
    id 1169
    label "Kanada"
  ]
  node [
    id 1170
    label "Wehrlen"
  ]
  node [
    id 1171
    label "Algieria"
  ]
  node [
    id 1172
    label "Uganda"
  ]
  node [
    id 1173
    label "Surinam"
  ]
  node [
    id 1174
    label "Sahara_Zachodnia"
  ]
  node [
    id 1175
    label "Chile"
  ]
  node [
    id 1176
    label "W&#281;gry"
  ]
  node [
    id 1177
    label "Birma"
  ]
  node [
    id 1178
    label "Kazachstan"
  ]
  node [
    id 1179
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1180
    label "Armenia"
  ]
  node [
    id 1181
    label "Tuwalu"
  ]
  node [
    id 1182
    label "Timor_Wschodni"
  ]
  node [
    id 1183
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1184
    label "Izrael"
  ]
  node [
    id 1185
    label "Estonia"
  ]
  node [
    id 1186
    label "Komory"
  ]
  node [
    id 1187
    label "Kamerun"
  ]
  node [
    id 1188
    label "Haiti"
  ]
  node [
    id 1189
    label "Belize"
  ]
  node [
    id 1190
    label "Sierra_Leone"
  ]
  node [
    id 1191
    label "Luksemburg"
  ]
  node [
    id 1192
    label "USA"
  ]
  node [
    id 1193
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1194
    label "Barbados"
  ]
  node [
    id 1195
    label "San_Marino"
  ]
  node [
    id 1196
    label "Bu&#322;garia"
  ]
  node [
    id 1197
    label "Indonezja"
  ]
  node [
    id 1198
    label "Wietnam"
  ]
  node [
    id 1199
    label "Malawi"
  ]
  node [
    id 1200
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1201
    label "Francja"
  ]
  node [
    id 1202
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1203
    label "partia"
  ]
  node [
    id 1204
    label "Zambia"
  ]
  node [
    id 1205
    label "Angola"
  ]
  node [
    id 1206
    label "Grenada"
  ]
  node [
    id 1207
    label "Nepal"
  ]
  node [
    id 1208
    label "Panama"
  ]
  node [
    id 1209
    label "Rumunia"
  ]
  node [
    id 1210
    label "Czarnog&#243;ra"
  ]
  node [
    id 1211
    label "Malediwy"
  ]
  node [
    id 1212
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1213
    label "S&#322;owacja"
  ]
  node [
    id 1214
    label "Egipt"
  ]
  node [
    id 1215
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1216
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1217
    label "Mozambik"
  ]
  node [
    id 1218
    label "Kolumbia"
  ]
  node [
    id 1219
    label "Laos"
  ]
  node [
    id 1220
    label "Burundi"
  ]
  node [
    id 1221
    label "Suazi"
  ]
  node [
    id 1222
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1223
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1224
    label "Czechy"
  ]
  node [
    id 1225
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1226
    label "Wyspy_Marshalla"
  ]
  node [
    id 1227
    label "Dominika"
  ]
  node [
    id 1228
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1229
    label "Syria"
  ]
  node [
    id 1230
    label "Palau"
  ]
  node [
    id 1231
    label "Gwinea_Bissau"
  ]
  node [
    id 1232
    label "Liberia"
  ]
  node [
    id 1233
    label "Jamajka"
  ]
  node [
    id 1234
    label "Zimbabwe"
  ]
  node [
    id 1235
    label "Polska"
  ]
  node [
    id 1236
    label "Dominikana"
  ]
  node [
    id 1237
    label "Senegal"
  ]
  node [
    id 1238
    label "Togo"
  ]
  node [
    id 1239
    label "Gujana"
  ]
  node [
    id 1240
    label "Gruzja"
  ]
  node [
    id 1241
    label "Albania"
  ]
  node [
    id 1242
    label "Zair"
  ]
  node [
    id 1243
    label "Meksyk"
  ]
  node [
    id 1244
    label "Macedonia"
  ]
  node [
    id 1245
    label "Chorwacja"
  ]
  node [
    id 1246
    label "Kambod&#380;a"
  ]
  node [
    id 1247
    label "Monako"
  ]
  node [
    id 1248
    label "Mauritius"
  ]
  node [
    id 1249
    label "Gwinea"
  ]
  node [
    id 1250
    label "Mali"
  ]
  node [
    id 1251
    label "Nigeria"
  ]
  node [
    id 1252
    label "Kostaryka"
  ]
  node [
    id 1253
    label "Hanower"
  ]
  node [
    id 1254
    label "Paragwaj"
  ]
  node [
    id 1255
    label "W&#322;ochy"
  ]
  node [
    id 1256
    label "Seszele"
  ]
  node [
    id 1257
    label "Wyspy_Salomona"
  ]
  node [
    id 1258
    label "Hiszpania"
  ]
  node [
    id 1259
    label "Boliwia"
  ]
  node [
    id 1260
    label "Kirgistan"
  ]
  node [
    id 1261
    label "Irlandia"
  ]
  node [
    id 1262
    label "Czad"
  ]
  node [
    id 1263
    label "Irak"
  ]
  node [
    id 1264
    label "Lesoto"
  ]
  node [
    id 1265
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1266
    label "Malta"
  ]
  node [
    id 1267
    label "Andora"
  ]
  node [
    id 1268
    label "Chiny"
  ]
  node [
    id 1269
    label "Filipiny"
  ]
  node [
    id 1270
    label "Antarktis"
  ]
  node [
    id 1271
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1272
    label "Pakistan"
  ]
  node [
    id 1273
    label "terytorium"
  ]
  node [
    id 1274
    label "Nikaragua"
  ]
  node [
    id 1275
    label "Brazylia"
  ]
  node [
    id 1276
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1277
    label "Maroko"
  ]
  node [
    id 1278
    label "Portugalia"
  ]
  node [
    id 1279
    label "Niger"
  ]
  node [
    id 1280
    label "Kenia"
  ]
  node [
    id 1281
    label "Botswana"
  ]
  node [
    id 1282
    label "Fid&#380;i"
  ]
  node [
    id 1283
    label "Tunezja"
  ]
  node [
    id 1284
    label "Australia"
  ]
  node [
    id 1285
    label "Tajlandia"
  ]
  node [
    id 1286
    label "Burkina_Faso"
  ]
  node [
    id 1287
    label "interior"
  ]
  node [
    id 1288
    label "Tanzania"
  ]
  node [
    id 1289
    label "Benin"
  ]
  node [
    id 1290
    label "Indie"
  ]
  node [
    id 1291
    label "&#321;otwa"
  ]
  node [
    id 1292
    label "Kiribati"
  ]
  node [
    id 1293
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1294
    label "Rodezja"
  ]
  node [
    id 1295
    label "Cypr"
  ]
  node [
    id 1296
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1297
    label "Peru"
  ]
  node [
    id 1298
    label "Austria"
  ]
  node [
    id 1299
    label "Urugwaj"
  ]
  node [
    id 1300
    label "Jordania"
  ]
  node [
    id 1301
    label "Grecja"
  ]
  node [
    id 1302
    label "Azerbejd&#380;an"
  ]
  node [
    id 1303
    label "Turcja"
  ]
  node [
    id 1304
    label "Samoa"
  ]
  node [
    id 1305
    label "Sudan"
  ]
  node [
    id 1306
    label "Oman"
  ]
  node [
    id 1307
    label "ziemia"
  ]
  node [
    id 1308
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1309
    label "Uzbekistan"
  ]
  node [
    id 1310
    label "Portoryko"
  ]
  node [
    id 1311
    label "Honduras"
  ]
  node [
    id 1312
    label "Mongolia"
  ]
  node [
    id 1313
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1314
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1315
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1316
    label "Serbia"
  ]
  node [
    id 1317
    label "Tajwan"
  ]
  node [
    id 1318
    label "Wielka_Brytania"
  ]
  node [
    id 1319
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1320
    label "Liban"
  ]
  node [
    id 1321
    label "Japonia"
  ]
  node [
    id 1322
    label "Ghana"
  ]
  node [
    id 1323
    label "Belgia"
  ]
  node [
    id 1324
    label "Bahrajn"
  ]
  node [
    id 1325
    label "Mikronezja"
  ]
  node [
    id 1326
    label "Etiopia"
  ]
  node [
    id 1327
    label "Kuwejt"
  ]
  node [
    id 1328
    label "grupa"
  ]
  node [
    id 1329
    label "Bahamy"
  ]
  node [
    id 1330
    label "Rosja"
  ]
  node [
    id 1331
    label "Mo&#322;dawia"
  ]
  node [
    id 1332
    label "Litwa"
  ]
  node [
    id 1333
    label "S&#322;owenia"
  ]
  node [
    id 1334
    label "Szwajcaria"
  ]
  node [
    id 1335
    label "Erytrea"
  ]
  node [
    id 1336
    label "Arabia_Saudyjska"
  ]
  node [
    id 1337
    label "Kuba"
  ]
  node [
    id 1338
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1339
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1340
    label "Malezja"
  ]
  node [
    id 1341
    label "Korea"
  ]
  node [
    id 1342
    label "Jemen"
  ]
  node [
    id 1343
    label "Nowa_Zelandia"
  ]
  node [
    id 1344
    label "Namibia"
  ]
  node [
    id 1345
    label "Nauru"
  ]
  node [
    id 1346
    label "holoarktyka"
  ]
  node [
    id 1347
    label "Brunei"
  ]
  node [
    id 1348
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1349
    label "Khitai"
  ]
  node [
    id 1350
    label "Mauretania"
  ]
  node [
    id 1351
    label "Iran"
  ]
  node [
    id 1352
    label "Gambia"
  ]
  node [
    id 1353
    label "Somalia"
  ]
  node [
    id 1354
    label "Holandia"
  ]
  node [
    id 1355
    label "Turkmenistan"
  ]
  node [
    id 1356
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1357
    label "Salwador"
  ]
  node [
    id 1358
    label "wydoro&#347;lenie"
  ]
  node [
    id 1359
    label "doro&#347;lenie"
  ]
  node [
    id 1360
    label "&#378;ra&#322;y"
  ]
  node [
    id 1361
    label "doro&#347;le"
  ]
  node [
    id 1362
    label "senior"
  ]
  node [
    id 1363
    label "dojrzale"
  ]
  node [
    id 1364
    label "wapniak"
  ]
  node [
    id 1365
    label "dojrza&#322;y"
  ]
  node [
    id 1366
    label "doletni"
  ]
  node [
    id 1367
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1368
    label "asymilowa&#263;"
  ]
  node [
    id 1369
    label "os&#322;abia&#263;"
  ]
  node [
    id 1370
    label "hominid"
  ]
  node [
    id 1371
    label "podw&#322;adny"
  ]
  node [
    id 1372
    label "os&#322;abianie"
  ]
  node [
    id 1373
    label "g&#322;owa"
  ]
  node [
    id 1374
    label "figura"
  ]
  node [
    id 1375
    label "portrecista"
  ]
  node [
    id 1376
    label "dwun&#243;g"
  ]
  node [
    id 1377
    label "profanum"
  ]
  node [
    id 1378
    label "mikrokosmos"
  ]
  node [
    id 1379
    label "nasada"
  ]
  node [
    id 1380
    label "duch"
  ]
  node [
    id 1381
    label "antropochoria"
  ]
  node [
    id 1382
    label "osoba"
  ]
  node [
    id 1383
    label "wz&#243;r"
  ]
  node [
    id 1384
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1385
    label "Adam"
  ]
  node [
    id 1386
    label "homo_sapiens"
  ]
  node [
    id 1387
    label "polifag"
  ]
  node [
    id 1388
    label "pami&#281;&#263;"
  ]
  node [
    id 1389
    label "powierzchnia_zr&#243;wnania"
  ]
  node [
    id 1390
    label "zapalenie"
  ]
  node [
    id 1391
    label "drewno_wt&#243;rne"
  ]
  node [
    id 1392
    label "heartwood"
  ]
  node [
    id 1393
    label "monolit"
  ]
  node [
    id 1394
    label "formacja_skalna"
  ]
  node [
    id 1395
    label "klaster_dyskowy"
  ]
  node [
    id 1396
    label "komputer"
  ]
  node [
    id 1397
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 1398
    label "hard_disc"
  ]
  node [
    id 1399
    label "&#347;mia&#322;ek"
  ]
  node [
    id 1400
    label "kszta&#322;ciciel"
  ]
  node [
    id 1401
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 1402
    label "kuwada"
  ]
  node [
    id 1403
    label "tworzyciel"
  ]
  node [
    id 1404
    label "rodzice"
  ]
  node [
    id 1405
    label "&#347;w"
  ]
  node [
    id 1406
    label "pomys&#322;odawca"
  ]
  node [
    id 1407
    label "rodzic"
  ]
  node [
    id 1408
    label "wykonawca"
  ]
  node [
    id 1409
    label "ojczym"
  ]
  node [
    id 1410
    label "przodek"
  ]
  node [
    id 1411
    label "papa"
  ]
  node [
    id 1412
    label "zakonnik"
  ]
  node [
    id 1413
    label "stary"
  ]
  node [
    id 1414
    label "kochanek"
  ]
  node [
    id 1415
    label "fio&#322;ek"
  ]
  node [
    id 1416
    label "facet"
  ]
  node [
    id 1417
    label "brat"
  ]
  node [
    id 1418
    label "zwierz&#281;"
  ]
  node [
    id 1419
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 1420
    label "ma&#322;&#380;onek"
  ]
  node [
    id 1421
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1422
    label "m&#243;j"
  ]
  node [
    id 1423
    label "ch&#322;op"
  ]
  node [
    id 1424
    label "pan_m&#322;ody"
  ]
  node [
    id 1425
    label "&#347;lubny"
  ]
  node [
    id 1426
    label "pan_domu"
  ]
  node [
    id 1427
    label "pan_i_w&#322;adca"
  ]
  node [
    id 1428
    label "mo&#347;&#263;"
  ]
  node [
    id 1429
    label "specjalista"
  ]
  node [
    id 1430
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 1431
    label "Micha&#322;"
  ]
  node [
    id 1432
    label "Parzuchowski"
  ]
  node [
    id 1433
    label "psychologia"
  ]
  node [
    id 1434
    label "spo&#322;eczny"
  ]
  node [
    id 1435
    label "SPSP"
  ]
  node [
    id 1436
    label "APS"
  ]
  node [
    id 1437
    label "european"
  ]
  node [
    id 1438
    label "Journal"
  ]
  node [
    id 1439
    label "of"
  ]
  node [
    id 1440
    label "Social"
  ]
  node [
    id 1441
    label "Psychology"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 256
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 254
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 305
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 12
    target 309
  ]
  edge [
    source 12
    target 310
  ]
  edge [
    source 12
    target 311
  ]
  edge [
    source 12
    target 312
  ]
  edge [
    source 12
    target 313
  ]
  edge [
    source 12
    target 314
  ]
  edge [
    source 12
    target 238
  ]
  edge [
    source 12
    target 315
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 337
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 33
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 56
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 335
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 1433
  ]
  edge [
    source 13
    target 1434
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 889
  ]
  edge [
    source 14
    target 890
  ]
  edge [
    source 14
    target 891
  ]
  edge [
    source 14
    target 892
  ]
  edge [
    source 14
    target 816
  ]
  edge [
    source 14
    target 893
  ]
  edge [
    source 14
    target 894
  ]
  edge [
    source 14
    target 895
  ]
  edge [
    source 14
    target 896
  ]
  edge [
    source 14
    target 897
  ]
  edge [
    source 14
    target 898
  ]
  edge [
    source 14
    target 899
  ]
  edge [
    source 14
    target 333
  ]
  edge [
    source 14
    target 235
  ]
  edge [
    source 14
    target 326
  ]
  edge [
    source 14
    target 900
  ]
  edge [
    source 14
    target 901
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 276
  ]
  edge [
    source 14
    target 902
  ]
  edge [
    source 14
    target 903
  ]
  edge [
    source 14
    target 904
  ]
  edge [
    source 14
    target 905
  ]
  edge [
    source 14
    target 906
  ]
  edge [
    source 14
    target 907
  ]
  edge [
    source 14
    target 908
  ]
  edge [
    source 14
    target 811
  ]
  edge [
    source 14
    target 909
  ]
  edge [
    source 14
    target 910
  ]
  edge [
    source 14
    target 911
  ]
  edge [
    source 14
    target 912
  ]
  edge [
    source 14
    target 913
  ]
  edge [
    source 14
    target 914
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 915
  ]
  edge [
    source 14
    target 44
  ]
  edge [
    source 14
    target 916
  ]
  edge [
    source 14
    target 917
  ]
  edge [
    source 14
    target 918
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 920
  ]
  edge [
    source 14
    target 921
  ]
  edge [
    source 14
    target 922
  ]
  edge [
    source 14
    target 923
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 925
  ]
  edge [
    source 15
    target 519
  ]
  edge [
    source 15
    target 926
  ]
  edge [
    source 15
    target 927
  ]
  edge [
    source 15
    target 928
  ]
  edge [
    source 15
    target 929
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 930
  ]
  edge [
    source 15
    target 931
  ]
  edge [
    source 15
    target 932
  ]
  edge [
    source 15
    target 568
  ]
  edge [
    source 15
    target 933
  ]
  edge [
    source 15
    target 518
  ]
  edge [
    source 15
    target 934
  ]
  edge [
    source 15
    target 935
  ]
  edge [
    source 15
    target 936
  ]
  edge [
    source 15
    target 937
  ]
  edge [
    source 15
    target 938
  ]
  edge [
    source 15
    target 939
  ]
  edge [
    source 15
    target 940
  ]
  edge [
    source 15
    target 941
  ]
  edge [
    source 15
    target 605
  ]
  edge [
    source 15
    target 942
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 943
  ]
  edge [
    source 15
    target 944
  ]
  edge [
    source 15
    target 945
  ]
  edge [
    source 15
    target 946
  ]
  edge [
    source 15
    target 947
  ]
  edge [
    source 15
    target 537
  ]
  edge [
    source 15
    target 948
  ]
  edge [
    source 15
    target 562
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 949
  ]
  edge [
    source 15
    target 950
  ]
  edge [
    source 15
    target 951
  ]
  edge [
    source 15
    target 952
  ]
  edge [
    source 15
    target 953
  ]
  edge [
    source 15
    target 954
  ]
  edge [
    source 15
    target 955
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 956
  ]
  edge [
    source 15
    target 957
  ]
  edge [
    source 15
    target 958
  ]
  edge [
    source 15
    target 959
  ]
  edge [
    source 15
    target 960
  ]
  edge [
    source 15
    target 961
  ]
  edge [
    source 15
    target 962
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 963
  ]
  edge [
    source 15
    target 964
  ]
  edge [
    source 15
    target 965
  ]
  edge [
    source 15
    target 966
  ]
  edge [
    source 15
    target 967
  ]
  edge [
    source 15
    target 968
  ]
  edge [
    source 15
    target 969
  ]
  edge [
    source 15
    target 970
  ]
  edge [
    source 15
    target 971
  ]
  edge [
    source 15
    target 540
  ]
  edge [
    source 15
    target 972
  ]
  edge [
    source 15
    target 973
  ]
  edge [
    source 15
    target 974
  ]
  edge [
    source 15
    target 975
  ]
  edge [
    source 15
    target 976
  ]
  edge [
    source 15
    target 977
  ]
  edge [
    source 15
    target 978
  ]
  edge [
    source 15
    target 979
  ]
  edge [
    source 15
    target 980
  ]
  edge [
    source 15
    target 981
  ]
  edge [
    source 15
    target 982
  ]
  edge [
    source 15
    target 983
  ]
  edge [
    source 15
    target 984
  ]
  edge [
    source 15
    target 985
  ]
  edge [
    source 15
    target 986
  ]
  edge [
    source 15
    target 987
  ]
  edge [
    source 15
    target 988
  ]
  edge [
    source 15
    target 989
  ]
  edge [
    source 15
    target 241
  ]
  edge [
    source 15
    target 990
  ]
  edge [
    source 15
    target 991
  ]
  edge [
    source 15
    target 992
  ]
  edge [
    source 15
    target 993
  ]
  edge [
    source 15
    target 994
  ]
  edge [
    source 15
    target 995
  ]
  edge [
    source 15
    target 996
  ]
  edge [
    source 15
    target 997
  ]
  edge [
    source 15
    target 998
  ]
  edge [
    source 15
    target 999
  ]
  edge [
    source 15
    target 1000
  ]
  edge [
    source 15
    target 1001
  ]
  edge [
    source 15
    target 1002
  ]
  edge [
    source 15
    target 1003
  ]
  edge [
    source 15
    target 1004
  ]
  edge [
    source 15
    target 1005
  ]
  edge [
    source 15
    target 1006
  ]
  edge [
    source 15
    target 1007
  ]
  edge [
    source 15
    target 1008
  ]
  edge [
    source 15
    target 1009
  ]
  edge [
    source 15
    target 1010
  ]
  edge [
    source 15
    target 1011
  ]
  edge [
    source 15
    target 1012
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1013
  ]
  edge [
    source 16
    target 1014
  ]
  edge [
    source 16
    target 518
  ]
  edge [
    source 16
    target 1015
  ]
  edge [
    source 16
    target 973
  ]
  edge [
    source 16
    target 1016
  ]
  edge [
    source 16
    target 1017
  ]
  edge [
    source 16
    target 1018
  ]
  edge [
    source 16
    target 1019
  ]
  edge [
    source 16
    target 614
  ]
  edge [
    source 16
    target 1020
  ]
  edge [
    source 16
    target 569
  ]
  edge [
    source 16
    target 570
  ]
  edge [
    source 16
    target 571
  ]
  edge [
    source 16
    target 522
  ]
  edge [
    source 16
    target 572
  ]
  edge [
    source 16
    target 573
  ]
  edge [
    source 16
    target 574
  ]
  edge [
    source 16
    target 575
  ]
  edge [
    source 16
    target 576
  ]
  edge [
    source 16
    target 44
  ]
  edge [
    source 16
    target 577
  ]
  edge [
    source 16
    target 578
  ]
  edge [
    source 16
    target 579
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 586
  ]
  edge [
    source 16
    target 1021
  ]
  edge [
    source 16
    target 1022
  ]
  edge [
    source 16
    target 1023
  ]
  edge [
    source 16
    target 1024
  ]
  edge [
    source 16
    target 1025
  ]
  edge [
    source 16
    target 1026
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 1027
  ]
  edge [
    source 16
    target 1028
  ]
  edge [
    source 16
    target 1029
  ]
  edge [
    source 16
    target 1030
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 1031
  ]
  edge [
    source 16
    target 1032
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 257
  ]
  edge [
    source 17
    target 258
  ]
  edge [
    source 17
    target 259
  ]
  edge [
    source 17
    target 260
  ]
  edge [
    source 17
    target 261
  ]
  edge [
    source 17
    target 262
  ]
  edge [
    source 17
    target 263
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 1041
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 1043
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 17
    target 1052
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 1055
  ]
  edge [
    source 17
    target 1056
  ]
  edge [
    source 17
    target 1057
  ]
  edge [
    source 17
    target 1058
  ]
  edge [
    source 17
    target 1059
  ]
  edge [
    source 17
    target 1060
  ]
  edge [
    source 17
    target 1061
  ]
  edge [
    source 17
    target 1062
  ]
  edge [
    source 17
    target 1063
  ]
  edge [
    source 17
    target 1064
  ]
  edge [
    source 17
    target 1065
  ]
  edge [
    source 17
    target 1066
  ]
  edge [
    source 17
    target 1067
  ]
  edge [
    source 17
    target 1068
  ]
  edge [
    source 17
    target 57
  ]
  edge [
    source 17
    target 1069
  ]
  edge [
    source 17
    target 1070
  ]
  edge [
    source 17
    target 1071
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 1072
  ]
  edge [
    source 17
    target 1073
  ]
  edge [
    source 17
    target 1074
  ]
  edge [
    source 17
    target 94
  ]
  edge [
    source 17
    target 1075
  ]
  edge [
    source 17
    target 1076
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1077
  ]
  edge [
    source 17
    target 238
  ]
  edge [
    source 17
    target 1078
  ]
  edge [
    source 17
    target 1079
  ]
  edge [
    source 17
    target 1080
  ]
  edge [
    source 17
    target 1081
  ]
  edge [
    source 17
    target 1082
  ]
  edge [
    source 17
    target 1083
  ]
  edge [
    source 17
    target 337
  ]
  edge [
    source 17
    target 1084
  ]
  edge [
    source 17
    target 1085
  ]
  edge [
    source 17
    target 1086
  ]
  edge [
    source 17
    target 1087
  ]
  edge [
    source 17
    target 1088
  ]
  edge [
    source 17
    target 1089
  ]
  edge [
    source 17
    target 1090
  ]
  edge [
    source 17
    target 1091
  ]
  edge [
    source 17
    target 1092
  ]
  edge [
    source 17
    target 1093
  ]
  edge [
    source 17
    target 1094
  ]
  edge [
    source 17
    target 1095
  ]
  edge [
    source 17
    target 1096
  ]
  edge [
    source 17
    target 284
  ]
  edge [
    source 17
    target 1097
  ]
  edge [
    source 17
    target 1098
  ]
  edge [
    source 17
    target 1099
  ]
  edge [
    source 17
    target 1100
  ]
  edge [
    source 17
    target 1101
  ]
  edge [
    source 17
    target 1102
  ]
  edge [
    source 17
    target 1103
  ]
  edge [
    source 17
    target 1104
  ]
  edge [
    source 17
    target 1105
  ]
  edge [
    source 17
    target 1106
  ]
  edge [
    source 17
    target 1107
  ]
  edge [
    source 17
    target 1108
  ]
  edge [
    source 17
    target 333
  ]
  edge [
    source 17
    target 1109
  ]
  edge [
    source 17
    target 1110
  ]
  edge [
    source 17
    target 1111
  ]
  edge [
    source 17
    target 1112
  ]
  edge [
    source 17
    target 1113
  ]
  edge [
    source 17
    target 1114
  ]
  edge [
    source 17
    target 342
  ]
  edge [
    source 17
    target 1115
  ]
  edge [
    source 17
    target 1116
  ]
  edge [
    source 17
    target 345
  ]
  edge [
    source 17
    target 1117
  ]
  edge [
    source 17
    target 1118
  ]
  edge [
    source 17
    target 1119
  ]
  edge [
    source 17
    target 1120
  ]
  edge [
    source 17
    target 349
  ]
  edge [
    source 17
    target 350
  ]
  edge [
    source 17
    target 1121
  ]
  edge [
    source 17
    target 1122
  ]
  edge [
    source 17
    target 1123
  ]
  edge [
    source 17
    target 352
  ]
  edge [
    source 17
    target 1124
  ]
  edge [
    source 17
    target 1125
  ]
  edge [
    source 17
    target 1126
  ]
  edge [
    source 17
    target 355
  ]
  edge [
    source 17
    target 1127
  ]
  edge [
    source 17
    target 1128
  ]
  edge [
    source 17
    target 1129
  ]
  edge [
    source 17
    target 1130
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1131
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 428
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 1132
  ]
  edge [
    source 18
    target 471
  ]
  edge [
    source 18
    target 1133
  ]
  edge [
    source 18
    target 508
  ]
  edge [
    source 18
    target 1134
  ]
  edge [
    source 18
    target 404
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 484
  ]
  edge [
    source 18
    target 485
  ]
  edge [
    source 18
    target 486
  ]
  edge [
    source 18
    target 475
  ]
  edge [
    source 18
    target 476
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 478
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 480
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 482
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 19
    target 847
  ]
  edge [
    source 19
    target 1135
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 1136
  ]
  edge [
    source 19
    target 1137
  ]
  edge [
    source 19
    target 1138
  ]
  edge [
    source 19
    target 1139
  ]
  edge [
    source 19
    target 1140
  ]
  edge [
    source 19
    target 1141
  ]
  edge [
    source 19
    target 1142
  ]
  edge [
    source 19
    target 1143
  ]
  edge [
    source 19
    target 1144
  ]
  edge [
    source 19
    target 1145
  ]
  edge [
    source 19
    target 1146
  ]
  edge [
    source 19
    target 1147
  ]
  edge [
    source 19
    target 1148
  ]
  edge [
    source 19
    target 1149
  ]
  edge [
    source 19
    target 1150
  ]
  edge [
    source 19
    target 1151
  ]
  edge [
    source 19
    target 1152
  ]
  edge [
    source 19
    target 1153
  ]
  edge [
    source 19
    target 1154
  ]
  edge [
    source 19
    target 1155
  ]
  edge [
    source 19
    target 1156
  ]
  edge [
    source 19
    target 1157
  ]
  edge [
    source 19
    target 1158
  ]
  edge [
    source 19
    target 1159
  ]
  edge [
    source 19
    target 1160
  ]
  edge [
    source 19
    target 1161
  ]
  edge [
    source 19
    target 1162
  ]
  edge [
    source 19
    target 1163
  ]
  edge [
    source 19
    target 1164
  ]
  edge [
    source 19
    target 1165
  ]
  edge [
    source 19
    target 1166
  ]
  edge [
    source 19
    target 1167
  ]
  edge [
    source 19
    target 1168
  ]
  edge [
    source 19
    target 1169
  ]
  edge [
    source 19
    target 1170
  ]
  edge [
    source 19
    target 1171
  ]
  edge [
    source 19
    target 1172
  ]
  edge [
    source 19
    target 1173
  ]
  edge [
    source 19
    target 1174
  ]
  edge [
    source 19
    target 1175
  ]
  edge [
    source 19
    target 1176
  ]
  edge [
    source 19
    target 1177
  ]
  edge [
    source 19
    target 1178
  ]
  edge [
    source 19
    target 1179
  ]
  edge [
    source 19
    target 1180
  ]
  edge [
    source 19
    target 1181
  ]
  edge [
    source 19
    target 1182
  ]
  edge [
    source 19
    target 1183
  ]
  edge [
    source 19
    target 1184
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 1186
  ]
  edge [
    source 19
    target 1187
  ]
  edge [
    source 19
    target 1188
  ]
  edge [
    source 19
    target 1189
  ]
  edge [
    source 19
    target 1190
  ]
  edge [
    source 19
    target 1191
  ]
  edge [
    source 19
    target 1192
  ]
  edge [
    source 19
    target 1193
  ]
  edge [
    source 19
    target 1194
  ]
  edge [
    source 19
    target 1195
  ]
  edge [
    source 19
    target 1196
  ]
  edge [
    source 19
    target 1197
  ]
  edge [
    source 19
    target 1198
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 1200
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 1202
  ]
  edge [
    source 19
    target 1203
  ]
  edge [
    source 19
    target 1204
  ]
  edge [
    source 19
    target 1205
  ]
  edge [
    source 19
    target 1206
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1208
  ]
  edge [
    source 19
    target 1209
  ]
  edge [
    source 19
    target 1210
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 1212
  ]
  edge [
    source 19
    target 1213
  ]
  edge [
    source 19
    target 787
  ]
  edge [
    source 19
    target 1214
  ]
  edge [
    source 19
    target 408
  ]
  edge [
    source 19
    target 1215
  ]
  edge [
    source 19
    target 1216
  ]
  edge [
    source 19
    target 1217
  ]
  edge [
    source 19
    target 1218
  ]
  edge [
    source 19
    target 1219
  ]
  edge [
    source 19
    target 1220
  ]
  edge [
    source 19
    target 1221
  ]
  edge [
    source 19
    target 1222
  ]
  edge [
    source 19
    target 1223
  ]
  edge [
    source 19
    target 1224
  ]
  edge [
    source 19
    target 1225
  ]
  edge [
    source 19
    target 1226
  ]
  edge [
    source 19
    target 1227
  ]
  edge [
    source 19
    target 1228
  ]
  edge [
    source 19
    target 1229
  ]
  edge [
    source 19
    target 1230
  ]
  edge [
    source 19
    target 1231
  ]
  edge [
    source 19
    target 1232
  ]
  edge [
    source 19
    target 1233
  ]
  edge [
    source 19
    target 1234
  ]
  edge [
    source 19
    target 1235
  ]
  edge [
    source 19
    target 1236
  ]
  edge [
    source 19
    target 1237
  ]
  edge [
    source 19
    target 1238
  ]
  edge [
    source 19
    target 1239
  ]
  edge [
    source 19
    target 1240
  ]
  edge [
    source 19
    target 1241
  ]
  edge [
    source 19
    target 1242
  ]
  edge [
    source 19
    target 1243
  ]
  edge [
    source 19
    target 1244
  ]
  edge [
    source 19
    target 1245
  ]
  edge [
    source 19
    target 1246
  ]
  edge [
    source 19
    target 1247
  ]
  edge [
    source 19
    target 1248
  ]
  edge [
    source 19
    target 1249
  ]
  edge [
    source 19
    target 1250
  ]
  edge [
    source 19
    target 1251
  ]
  edge [
    source 19
    target 1252
  ]
  edge [
    source 19
    target 1253
  ]
  edge [
    source 19
    target 1254
  ]
  edge [
    source 19
    target 1255
  ]
  edge [
    source 19
    target 1256
  ]
  edge [
    source 19
    target 1257
  ]
  edge [
    source 19
    target 1258
  ]
  edge [
    source 19
    target 1259
  ]
  edge [
    source 19
    target 1260
  ]
  edge [
    source 19
    target 1261
  ]
  edge [
    source 19
    target 1262
  ]
  edge [
    source 19
    target 1263
  ]
  edge [
    source 19
    target 1264
  ]
  edge [
    source 19
    target 1265
  ]
  edge [
    source 19
    target 1266
  ]
  edge [
    source 19
    target 1267
  ]
  edge [
    source 19
    target 1268
  ]
  edge [
    source 19
    target 1269
  ]
  edge [
    source 19
    target 1270
  ]
  edge [
    source 19
    target 1114
  ]
  edge [
    source 19
    target 1271
  ]
  edge [
    source 19
    target 1272
  ]
  edge [
    source 19
    target 1273
  ]
  edge [
    source 19
    target 1274
  ]
  edge [
    source 19
    target 1275
  ]
  edge [
    source 19
    target 1276
  ]
  edge [
    source 19
    target 1277
  ]
  edge [
    source 19
    target 1278
  ]
  edge [
    source 19
    target 1279
  ]
  edge [
    source 19
    target 1280
  ]
  edge [
    source 19
    target 1281
  ]
  edge [
    source 19
    target 1282
  ]
  edge [
    source 19
    target 1283
  ]
  edge [
    source 19
    target 1284
  ]
  edge [
    source 19
    target 1285
  ]
  edge [
    source 19
    target 1286
  ]
  edge [
    source 19
    target 1287
  ]
  edge [
    source 19
    target 1288
  ]
  edge [
    source 19
    target 1289
  ]
  edge [
    source 19
    target 1290
  ]
  edge [
    source 19
    target 1291
  ]
  edge [
    source 19
    target 1292
  ]
  edge [
    source 19
    target 1293
  ]
  edge [
    source 19
    target 1294
  ]
  edge [
    source 19
    target 1295
  ]
  edge [
    source 19
    target 1296
  ]
  edge [
    source 19
    target 1297
  ]
  edge [
    source 19
    target 1298
  ]
  edge [
    source 19
    target 1299
  ]
  edge [
    source 19
    target 1300
  ]
  edge [
    source 19
    target 1301
  ]
  edge [
    source 19
    target 1302
  ]
  edge [
    source 19
    target 1303
  ]
  edge [
    source 19
    target 1304
  ]
  edge [
    source 19
    target 1305
  ]
  edge [
    source 19
    target 1306
  ]
  edge [
    source 19
    target 1307
  ]
  edge [
    source 19
    target 1308
  ]
  edge [
    source 19
    target 1309
  ]
  edge [
    source 19
    target 1310
  ]
  edge [
    source 19
    target 1311
  ]
  edge [
    source 19
    target 1312
  ]
  edge [
    source 19
    target 1313
  ]
  edge [
    source 19
    target 1314
  ]
  edge [
    source 19
    target 1315
  ]
  edge [
    source 19
    target 1316
  ]
  edge [
    source 19
    target 1317
  ]
  edge [
    source 19
    target 1318
  ]
  edge [
    source 19
    target 1319
  ]
  edge [
    source 19
    target 1320
  ]
  edge [
    source 19
    target 1321
  ]
  edge [
    source 19
    target 1322
  ]
  edge [
    source 19
    target 1323
  ]
  edge [
    source 19
    target 1324
  ]
  edge [
    source 19
    target 1325
  ]
  edge [
    source 19
    target 1326
  ]
  edge [
    source 19
    target 1327
  ]
  edge [
    source 19
    target 1328
  ]
  edge [
    source 19
    target 1329
  ]
  edge [
    source 19
    target 1330
  ]
  edge [
    source 19
    target 1331
  ]
  edge [
    source 19
    target 1332
  ]
  edge [
    source 19
    target 1333
  ]
  edge [
    source 19
    target 1334
  ]
  edge [
    source 19
    target 1335
  ]
  edge [
    source 19
    target 1336
  ]
  edge [
    source 19
    target 1337
  ]
  edge [
    source 19
    target 1338
  ]
  edge [
    source 19
    target 1339
  ]
  edge [
    source 19
    target 1340
  ]
  edge [
    source 19
    target 1341
  ]
  edge [
    source 19
    target 1342
  ]
  edge [
    source 19
    target 1343
  ]
  edge [
    source 19
    target 1344
  ]
  edge [
    source 19
    target 1345
  ]
  edge [
    source 19
    target 1346
  ]
  edge [
    source 19
    target 1347
  ]
  edge [
    source 19
    target 1348
  ]
  edge [
    source 19
    target 1349
  ]
  edge [
    source 19
    target 1350
  ]
  edge [
    source 19
    target 1351
  ]
  edge [
    source 19
    target 1352
  ]
  edge [
    source 19
    target 1353
  ]
  edge [
    source 19
    target 1354
  ]
  edge [
    source 19
    target 1355
  ]
  edge [
    source 19
    target 1356
  ]
  edge [
    source 19
    target 1357
  ]
  edge [
    source 19
    target 1358
  ]
  edge [
    source 19
    target 837
  ]
  edge [
    source 19
    target 404
  ]
  edge [
    source 19
    target 1359
  ]
  edge [
    source 19
    target 1360
  ]
  edge [
    source 19
    target 1361
  ]
  edge [
    source 19
    target 1362
  ]
  edge [
    source 19
    target 1363
  ]
  edge [
    source 19
    target 1364
  ]
  edge [
    source 19
    target 1365
  ]
  edge [
    source 19
    target 500
  ]
  edge [
    source 19
    target 1366
  ]
  edge [
    source 19
    target 1367
  ]
  edge [
    source 19
    target 477
  ]
  edge [
    source 19
    target 1368
  ]
  edge [
    source 19
    target 1369
  ]
  edge [
    source 19
    target 337
  ]
  edge [
    source 19
    target 1370
  ]
  edge [
    source 19
    target 1371
  ]
  edge [
    source 19
    target 1372
  ]
  edge [
    source 19
    target 1373
  ]
  edge [
    source 19
    target 1374
  ]
  edge [
    source 19
    target 1375
  ]
  edge [
    source 19
    target 1376
  ]
  edge [
    source 19
    target 1377
  ]
  edge [
    source 19
    target 1378
  ]
  edge [
    source 19
    target 1379
  ]
  edge [
    source 19
    target 1380
  ]
  edge [
    source 19
    target 1381
  ]
  edge [
    source 19
    target 1382
  ]
  edge [
    source 19
    target 1383
  ]
  edge [
    source 19
    target 1384
  ]
  edge [
    source 19
    target 1385
  ]
  edge [
    source 19
    target 1386
  ]
  edge [
    source 19
    target 1387
  ]
  edge [
    source 19
    target 1388
  ]
  edge [
    source 19
    target 1389
  ]
  edge [
    source 19
    target 1390
  ]
  edge [
    source 19
    target 1391
  ]
  edge [
    source 19
    target 1392
  ]
  edge [
    source 19
    target 1393
  ]
  edge [
    source 19
    target 1394
  ]
  edge [
    source 19
    target 1395
  ]
  edge [
    source 19
    target 1396
  ]
  edge [
    source 19
    target 1397
  ]
  edge [
    source 19
    target 1398
  ]
  edge [
    source 19
    target 1399
  ]
  edge [
    source 19
    target 1400
  ]
  edge [
    source 19
    target 1401
  ]
  edge [
    source 19
    target 1402
  ]
  edge [
    source 19
    target 1403
  ]
  edge [
    source 19
    target 1404
  ]
  edge [
    source 19
    target 1405
  ]
  edge [
    source 19
    target 1406
  ]
  edge [
    source 19
    target 1407
  ]
  edge [
    source 19
    target 1408
  ]
  edge [
    source 19
    target 1409
  ]
  edge [
    source 19
    target 1410
  ]
  edge [
    source 19
    target 1411
  ]
  edge [
    source 19
    target 1412
  ]
  edge [
    source 19
    target 1413
  ]
  edge [
    source 19
    target 1414
  ]
  edge [
    source 19
    target 1415
  ]
  edge [
    source 19
    target 1416
  ]
  edge [
    source 19
    target 1417
  ]
  edge [
    source 19
    target 1418
  ]
  edge [
    source 19
    target 1419
  ]
  edge [
    source 19
    target 1420
  ]
  edge [
    source 19
    target 1421
  ]
  edge [
    source 19
    target 1422
  ]
  edge [
    source 19
    target 1423
  ]
  edge [
    source 19
    target 1424
  ]
  edge [
    source 19
    target 1425
  ]
  edge [
    source 19
    target 1426
  ]
  edge [
    source 19
    target 1427
  ]
  edge [
    source 19
    target 1428
  ]
  edge [
    source 19
    target 1429
  ]
  edge [
    source 19
    target 276
  ]
  edge [
    source 19
    target 1430
  ]
  edge [
    source 86
    target 1433
  ]
  edge [
    source 86
    target 1434
  ]
  edge [
    source 1431
    target 1432
  ]
  edge [
    source 1433
    target 1434
  ]
  edge [
    source 1435
    target 1436
  ]
  edge [
    source 1437
    target 1438
  ]
  edge [
    source 1437
    target 1439
  ]
  edge [
    source 1437
    target 1440
  ]
  edge [
    source 1437
    target 1441
  ]
  edge [
    source 1438
    target 1439
  ]
  edge [
    source 1438
    target 1440
  ]
  edge [
    source 1438
    target 1441
  ]
  edge [
    source 1439
    target 1440
  ]
  edge [
    source 1439
    target 1441
  ]
  edge [
    source 1440
    target 1441
  ]
]
