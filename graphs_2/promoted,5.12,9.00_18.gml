graph [
  node [
    id 0
    label "trzeba"
    origin "text"
  ]
  node [
    id 1
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "necessity"
  ]
  node [
    id 3
    label "trza"
  ]
  node [
    id 4
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 5
    label "pilnowa&#263;"
  ]
  node [
    id 6
    label "robi&#263;"
  ]
  node [
    id 7
    label "my&#347;le&#263;"
  ]
  node [
    id 8
    label "continue"
  ]
  node [
    id 9
    label "consider"
  ]
  node [
    id 10
    label "deliver"
  ]
  node [
    id 11
    label "obserwowa&#263;"
  ]
  node [
    id 12
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 13
    label "uznawa&#263;"
  ]
  node [
    id 14
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 15
    label "organizowa&#263;"
  ]
  node [
    id 16
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 17
    label "czyni&#263;"
  ]
  node [
    id 18
    label "give"
  ]
  node [
    id 19
    label "stylizowa&#263;"
  ]
  node [
    id 20
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 21
    label "falowa&#263;"
  ]
  node [
    id 22
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 23
    label "peddle"
  ]
  node [
    id 24
    label "praca"
  ]
  node [
    id 25
    label "wydala&#263;"
  ]
  node [
    id 26
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 27
    label "tentegowa&#263;"
  ]
  node [
    id 28
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 29
    label "urz&#261;dza&#263;"
  ]
  node [
    id 30
    label "oszukiwa&#263;"
  ]
  node [
    id 31
    label "work"
  ]
  node [
    id 32
    label "ukazywa&#263;"
  ]
  node [
    id 33
    label "przerabia&#263;"
  ]
  node [
    id 34
    label "act"
  ]
  node [
    id 35
    label "post&#281;powa&#263;"
  ]
  node [
    id 36
    label "take_care"
  ]
  node [
    id 37
    label "troska&#263;_si&#281;"
  ]
  node [
    id 38
    label "rozpatrywa&#263;"
  ]
  node [
    id 39
    label "zamierza&#263;"
  ]
  node [
    id 40
    label "argue"
  ]
  node [
    id 41
    label "os&#261;dza&#263;"
  ]
  node [
    id 42
    label "notice"
  ]
  node [
    id 43
    label "stwierdza&#263;"
  ]
  node [
    id 44
    label "przyznawa&#263;"
  ]
  node [
    id 45
    label "zachowywa&#263;"
  ]
  node [
    id 46
    label "dostrzega&#263;"
  ]
  node [
    id 47
    label "patrze&#263;"
  ]
  node [
    id 48
    label "look"
  ]
  node [
    id 49
    label "cover"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
]
