graph [
  node [
    id 0
    label "oskar"
    origin "text"
  ]
  node [
    id 1
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "&#347;mia&#263;"
    origin "text"
  ]
  node [
    id 4
    label "irytowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "jarek"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "nagle"
    origin "text"
  ]
  node [
    id 8
    label "podchodzi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "fachowy"
    origin "text"
  ]
  node [
    id 10
    label "ruch"
    origin "text"
  ]
  node [
    id 11
    label "wykr&#281;ca&#263;"
    origin "text"
  ]
  node [
    id 12
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 13
    label "zmusza&#263;"
    origin "text"
  ]
  node [
    id 14
    label "ukl&#281;kn&#261;&#263;"
    origin "text"
  ]
  node [
    id 15
    label "pod&#322;oga"
    origin "text"
  ]
  node [
    id 16
    label "mimo"
    origin "text"
  ]
  node [
    id 17
    label "przestawa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "bankrupt"
  ]
  node [
    id 19
    label "open"
  ]
  node [
    id 20
    label "post&#281;powa&#263;"
  ]
  node [
    id 21
    label "odejmowa&#263;"
  ]
  node [
    id 22
    label "set_about"
  ]
  node [
    id 23
    label "begin"
  ]
  node [
    id 24
    label "mie&#263;_miejsce"
  ]
  node [
    id 25
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 26
    label "take"
  ]
  node [
    id 27
    label "ujemny"
  ]
  node [
    id 28
    label "abstract"
  ]
  node [
    id 29
    label "liczy&#263;"
  ]
  node [
    id 30
    label "zabiera&#263;"
  ]
  node [
    id 31
    label "oddziela&#263;"
  ]
  node [
    id 32
    label "oddala&#263;"
  ]
  node [
    id 33
    label "reduce"
  ]
  node [
    id 34
    label "robi&#263;"
  ]
  node [
    id 35
    label "przybiera&#263;"
  ]
  node [
    id 36
    label "act"
  ]
  node [
    id 37
    label "i&#347;&#263;"
  ]
  node [
    id 38
    label "go"
  ]
  node [
    id 39
    label "use"
  ]
  node [
    id 40
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 41
    label "dzia&#322;a&#263;_na_nerwy"
  ]
  node [
    id 42
    label "harass"
  ]
  node [
    id 43
    label "raptowny"
  ]
  node [
    id 44
    label "nieprzewidzianie"
  ]
  node [
    id 45
    label "szybko"
  ]
  node [
    id 46
    label "sprawnie"
  ]
  node [
    id 47
    label "dynamicznie"
  ]
  node [
    id 48
    label "szybciochem"
  ]
  node [
    id 49
    label "szybciej"
  ]
  node [
    id 50
    label "bezpo&#347;rednio"
  ]
  node [
    id 51
    label "quicker"
  ]
  node [
    id 52
    label "quickest"
  ]
  node [
    id 53
    label "prosto"
  ]
  node [
    id 54
    label "promptly"
  ]
  node [
    id 55
    label "szybki"
  ]
  node [
    id 56
    label "niespodziewanie"
  ]
  node [
    id 57
    label "nieoczekiwany"
  ]
  node [
    id 58
    label "raptownie"
  ]
  node [
    id 59
    label "zawrzenie"
  ]
  node [
    id 60
    label "gwa&#322;towny"
  ]
  node [
    id 61
    label "zawrze&#263;"
  ]
  node [
    id 62
    label "oszukiwa&#263;"
  ]
  node [
    id 63
    label "dochodzi&#263;"
  ]
  node [
    id 64
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 65
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 66
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 67
    label "odpowiada&#263;"
  ]
  node [
    id 68
    label "ciecz"
  ]
  node [
    id 69
    label "wype&#322;nia&#263;"
  ]
  node [
    id 70
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 71
    label "sprawdzian"
  ]
  node [
    id 72
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 73
    label "dociera&#263;"
  ]
  node [
    id 74
    label "wspina&#263;_si&#281;"
  ]
  node [
    id 75
    label "approach"
  ]
  node [
    id 76
    label "traktowa&#263;"
  ]
  node [
    id 77
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 78
    label "trze&#263;"
  ]
  node [
    id 79
    label "get"
  ]
  node [
    id 80
    label "boost"
  ]
  node [
    id 81
    label "g&#322;adzi&#263;"
  ]
  node [
    id 82
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 83
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 84
    label "silnik"
  ]
  node [
    id 85
    label "znajdowa&#263;"
  ]
  node [
    id 86
    label "dopasowywa&#263;"
  ]
  node [
    id 87
    label "dorabia&#263;"
  ]
  node [
    id 88
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 89
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 90
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 91
    label "oszwabia&#263;"
  ]
  node [
    id 92
    label "orzyna&#263;"
  ]
  node [
    id 93
    label "cheat"
  ]
  node [
    id 94
    label "submit"
  ]
  node [
    id 95
    label "wchodzi&#263;"
  ]
  node [
    id 96
    label "react"
  ]
  node [
    id 97
    label "pytanie"
  ]
  node [
    id 98
    label "equate"
  ]
  node [
    id 99
    label "tone"
  ]
  node [
    id 100
    label "answer"
  ]
  node [
    id 101
    label "report"
  ]
  node [
    id 102
    label "contend"
  ]
  node [
    id 103
    label "powodowa&#263;"
  ]
  node [
    id 104
    label "dawa&#263;"
  ]
  node [
    id 105
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 106
    label "by&#263;"
  ]
  node [
    id 107
    label "impart"
  ]
  node [
    id 108
    label "reagowa&#263;"
  ]
  node [
    id 109
    label "ponosi&#263;"
  ]
  node [
    id 110
    label "dolatywa&#263;"
  ]
  node [
    id 111
    label "przesy&#322;ka"
  ]
  node [
    id 112
    label "doznawa&#263;"
  ]
  node [
    id 113
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 114
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 115
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 116
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 117
    label "postrzega&#263;"
  ]
  node [
    id 118
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 119
    label "zachodzi&#263;"
  ]
  node [
    id 120
    label "orgazm"
  ]
  node [
    id 121
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 122
    label "doczeka&#263;"
  ]
  node [
    id 123
    label "osi&#261;ga&#263;"
  ]
  node [
    id 124
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 125
    label "ripen"
  ]
  node [
    id 126
    label "uzyskiwa&#263;"
  ]
  node [
    id 127
    label "claim"
  ]
  node [
    id 128
    label "supervene"
  ]
  node [
    id 129
    label "dokoptowywa&#263;"
  ]
  node [
    id 130
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 131
    label "reach"
  ]
  node [
    id 132
    label "dotyczy&#263;"
  ]
  node [
    id 133
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 134
    label "poddawa&#263;"
  ]
  node [
    id 135
    label "perform"
  ]
  node [
    id 136
    label "meet"
  ]
  node [
    id 137
    label "zajmowa&#263;"
  ]
  node [
    id 138
    label "do"
  ]
  node [
    id 139
    label "umieszcza&#263;"
  ]
  node [
    id 140
    label "close"
  ]
  node [
    id 141
    label "istnie&#263;"
  ]
  node [
    id 142
    label "charge"
  ]
  node [
    id 143
    label "praca_pisemna"
  ]
  node [
    id 144
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 145
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 146
    label "examination"
  ]
  node [
    id 147
    label "pr&#243;ba"
  ]
  node [
    id 148
    label "&#263;wiczenie"
  ]
  node [
    id 149
    label "dydaktyka"
  ]
  node [
    id 150
    label "faza"
  ]
  node [
    id 151
    label "kontrola"
  ]
  node [
    id 152
    label "ciek&#322;y"
  ]
  node [
    id 153
    label "podbiec"
  ]
  node [
    id 154
    label "&#347;ciekni&#281;cie"
  ]
  node [
    id 155
    label "w&#322;oskowato&#347;&#263;"
  ]
  node [
    id 156
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 157
    label "baniak"
  ]
  node [
    id 158
    label "podbiega&#263;"
  ]
  node [
    id 159
    label "nieprzejrzysty"
  ]
  node [
    id 160
    label "wpadanie"
  ]
  node [
    id 161
    label "chlupa&#263;"
  ]
  node [
    id 162
    label "p&#322;ywa&#263;"
  ]
  node [
    id 163
    label "odp&#322;yn&#261;&#263;"
  ]
  node [
    id 164
    label "stan_skupienia"
  ]
  node [
    id 165
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 166
    label "odp&#322;ywanie"
  ]
  node [
    id 167
    label "zachlupa&#263;"
  ]
  node [
    id 168
    label "wpadni&#281;cie"
  ]
  node [
    id 169
    label "cia&#322;o"
  ]
  node [
    id 170
    label "wytoczenie"
  ]
  node [
    id 171
    label "substancja"
  ]
  node [
    id 172
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 173
    label "umiej&#281;tny"
  ]
  node [
    id 174
    label "zawodowy"
  ]
  node [
    id 175
    label "dobry"
  ]
  node [
    id 176
    label "profesjonalny"
  ]
  node [
    id 177
    label "fachowo"
  ]
  node [
    id 178
    label "specjalistyczny"
  ]
  node [
    id 179
    label "trained"
  ]
  node [
    id 180
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 181
    label "porz&#261;dny"
  ]
  node [
    id 182
    label "rzetelny"
  ]
  node [
    id 183
    label "ch&#322;odny"
  ]
  node [
    id 184
    label "specjalny"
  ]
  node [
    id 185
    label "zawodowo"
  ]
  node [
    id 186
    label "profesjonalnie"
  ]
  node [
    id 187
    label "kompetentny"
  ]
  node [
    id 188
    label "skuteczny"
  ]
  node [
    id 189
    label "ca&#322;y"
  ]
  node [
    id 190
    label "czw&#243;rka"
  ]
  node [
    id 191
    label "spokojny"
  ]
  node [
    id 192
    label "pos&#322;uszny"
  ]
  node [
    id 193
    label "korzystny"
  ]
  node [
    id 194
    label "drogi"
  ]
  node [
    id 195
    label "pozytywny"
  ]
  node [
    id 196
    label "moralny"
  ]
  node [
    id 197
    label "pomy&#347;lny"
  ]
  node [
    id 198
    label "powitanie"
  ]
  node [
    id 199
    label "grzeczny"
  ]
  node [
    id 200
    label "&#347;mieszny"
  ]
  node [
    id 201
    label "odpowiedni"
  ]
  node [
    id 202
    label "zwrot"
  ]
  node [
    id 203
    label "dobrze"
  ]
  node [
    id 204
    label "dobroczynny"
  ]
  node [
    id 205
    label "mi&#322;y"
  ]
  node [
    id 206
    label "umny"
  ]
  node [
    id 207
    label "umiej&#281;tnie"
  ]
  node [
    id 208
    label "udany"
  ]
  node [
    id 209
    label "s&#322;u&#380;bowo"
  ]
  node [
    id 210
    label "czadowy"
  ]
  node [
    id 211
    label "fajny"
  ]
  node [
    id 212
    label "zawo&#322;any"
  ]
  node [
    id 213
    label "formalny"
  ]
  node [
    id 214
    label "klawy"
  ]
  node [
    id 215
    label "specjalistycznie"
  ]
  node [
    id 216
    label "move"
  ]
  node [
    id 217
    label "zmiana"
  ]
  node [
    id 218
    label "model"
  ]
  node [
    id 219
    label "aktywno&#347;&#263;"
  ]
  node [
    id 220
    label "utrzymywanie"
  ]
  node [
    id 221
    label "utrzymywa&#263;"
  ]
  node [
    id 222
    label "taktyka"
  ]
  node [
    id 223
    label "d&#322;ugi"
  ]
  node [
    id 224
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 225
    label "natural_process"
  ]
  node [
    id 226
    label "kanciasty"
  ]
  node [
    id 227
    label "utrzyma&#263;"
  ]
  node [
    id 228
    label "myk"
  ]
  node [
    id 229
    label "manewr"
  ]
  node [
    id 230
    label "utrzymanie"
  ]
  node [
    id 231
    label "wydarzenie"
  ]
  node [
    id 232
    label "tumult"
  ]
  node [
    id 233
    label "stopek"
  ]
  node [
    id 234
    label "movement"
  ]
  node [
    id 235
    label "strumie&#324;"
  ]
  node [
    id 236
    label "czynno&#347;&#263;"
  ]
  node [
    id 237
    label "komunikacja"
  ]
  node [
    id 238
    label "lokomocja"
  ]
  node [
    id 239
    label "drift"
  ]
  node [
    id 240
    label "commercial_enterprise"
  ]
  node [
    id 241
    label "zjawisko"
  ]
  node [
    id 242
    label "apraksja"
  ]
  node [
    id 243
    label "proces"
  ]
  node [
    id 244
    label "poruszenie"
  ]
  node [
    id 245
    label "mechanika"
  ]
  node [
    id 246
    label "travel"
  ]
  node [
    id 247
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 248
    label "dyssypacja_energii"
  ]
  node [
    id 249
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 250
    label "kr&#243;tki"
  ]
  node [
    id 251
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 252
    label "przebieg"
  ]
  node [
    id 253
    label "rozprawa"
  ]
  node [
    id 254
    label "kognicja"
  ]
  node [
    id 255
    label "przes&#322;anka"
  ]
  node [
    id 256
    label "legislacyjnie"
  ]
  node [
    id 257
    label "nast&#281;pstwo"
  ]
  node [
    id 258
    label "charakter"
  ]
  node [
    id 259
    label "przebiegni&#281;cie"
  ]
  node [
    id 260
    label "przebiec"
  ]
  node [
    id 261
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 262
    label "motyw"
  ]
  node [
    id 263
    label "fabu&#322;a"
  ]
  node [
    id 264
    label "postawa"
  ]
  node [
    id 265
    label "stan"
  ]
  node [
    id 266
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 267
    label "action"
  ]
  node [
    id 268
    label "maneuver"
  ]
  node [
    id 269
    label "posuni&#281;cie"
  ]
  node [
    id 270
    label "activity"
  ]
  node [
    id 271
    label "absolutorium"
  ]
  node [
    id 272
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 273
    label "dzia&#322;anie"
  ]
  node [
    id 274
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 275
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 276
    label "przywidzenie"
  ]
  node [
    id 277
    label "boski"
  ]
  node [
    id 278
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 279
    label "krajobraz"
  ]
  node [
    id 280
    label "presence"
  ]
  node [
    id 281
    label "transportation_system"
  ]
  node [
    id 282
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 283
    label "implicite"
  ]
  node [
    id 284
    label "explicite"
  ]
  node [
    id 285
    label "wydeptanie"
  ]
  node [
    id 286
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 287
    label "miejsce"
  ]
  node [
    id 288
    label "wydeptywanie"
  ]
  node [
    id 289
    label "ekspedytor"
  ]
  node [
    id 290
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 291
    label "bezproblemowy"
  ]
  node [
    id 292
    label "oznaka"
  ]
  node [
    id 293
    label "odmienianie"
  ]
  node [
    id 294
    label "zmianka"
  ]
  node [
    id 295
    label "amendment"
  ]
  node [
    id 296
    label "passage"
  ]
  node [
    id 297
    label "praca"
  ]
  node [
    id 298
    label "rewizja"
  ]
  node [
    id 299
    label "komplet"
  ]
  node [
    id 300
    label "tura"
  ]
  node [
    id 301
    label "change"
  ]
  node [
    id 302
    label "ferment"
  ]
  node [
    id 303
    label "czas"
  ]
  node [
    id 304
    label "anatomopatolog"
  ]
  node [
    id 305
    label "woda_powierzchniowa"
  ]
  node [
    id 306
    label "fala"
  ]
  node [
    id 307
    label "mn&#243;stwo"
  ]
  node [
    id 308
    label "Ajgospotamoj"
  ]
  node [
    id 309
    label "ciek_wodny"
  ]
  node [
    id 310
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 311
    label "ha&#322;as"
  ]
  node [
    id 312
    label "disquiet"
  ]
  node [
    id 313
    label "struktura"
  ]
  node [
    id 314
    label "mechanika_klasyczna"
  ]
  node [
    id 315
    label "hydromechanika"
  ]
  node [
    id 316
    label "telemechanika"
  ]
  node [
    id 317
    label "elektromechanika"
  ]
  node [
    id 318
    label "cecha"
  ]
  node [
    id 319
    label "mechanika_o&#347;rodk&#243;w_ci&#261;g&#322;ych"
  ]
  node [
    id 320
    label "mechanika_g&#243;rotworu"
  ]
  node [
    id 321
    label "fizyka"
  ]
  node [
    id 322
    label "mechanika_gruntu"
  ]
  node [
    id 323
    label "aeromechanika"
  ]
  node [
    id 324
    label "mechanika_teoretyczna"
  ]
  node [
    id 325
    label "nauka"
  ]
  node [
    id 326
    label "d&#322;ugo"
  ]
  node [
    id 327
    label "daleki"
  ]
  node [
    id 328
    label "brak"
  ]
  node [
    id 329
    label "z&#322;y"
  ]
  node [
    id 330
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 331
    label "jednowyrazowy"
  ]
  node [
    id 332
    label "bliski"
  ]
  node [
    id 333
    label "drobny"
  ]
  node [
    id 334
    label "kr&#243;tko"
  ]
  node [
    id 335
    label "s&#322;aby"
  ]
  node [
    id 336
    label "panowa&#263;"
  ]
  node [
    id 337
    label "zachowywa&#263;"
  ]
  node [
    id 338
    label "twierdzi&#263;"
  ]
  node [
    id 339
    label "argue"
  ]
  node [
    id 340
    label "sprawowa&#263;"
  ]
  node [
    id 341
    label "s&#261;dzi&#263;"
  ]
  node [
    id 342
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 343
    label "byt"
  ]
  node [
    id 344
    label "podtrzymywa&#263;"
  ]
  node [
    id 345
    label "corroborate"
  ]
  node [
    id 346
    label "cope"
  ]
  node [
    id 347
    label "trzyma&#263;"
  ]
  node [
    id 348
    label "broni&#263;"
  ]
  node [
    id 349
    label "defy"
  ]
  node [
    id 350
    label "zapewnia&#263;"
  ]
  node [
    id 351
    label "zdo&#322;a&#263;"
  ]
  node [
    id 352
    label "op&#322;aci&#263;"
  ]
  node [
    id 353
    label "zrobi&#263;"
  ]
  node [
    id 354
    label "zapewni&#263;"
  ]
  node [
    id 355
    label "zachowa&#263;"
  ]
  node [
    id 356
    label "feed"
  ]
  node [
    id 357
    label "foster"
  ]
  node [
    id 358
    label "preserve"
  ]
  node [
    id 359
    label "potrzyma&#263;"
  ]
  node [
    id 360
    label "przetrzyma&#263;"
  ]
  node [
    id 361
    label "podtrzyma&#263;"
  ]
  node [
    id 362
    label "unie&#347;&#263;"
  ]
  node [
    id 363
    label "obroni&#263;"
  ]
  node [
    id 364
    label "bycie"
  ]
  node [
    id 365
    label "zachowywanie"
  ]
  node [
    id 366
    label "panowanie"
  ]
  node [
    id 367
    label "podtrzymywanie"
  ]
  node [
    id 368
    label "zapewnianie"
  ]
  node [
    id 369
    label "preservation"
  ]
  node [
    id 370
    label "trzymanie"
  ]
  node [
    id 371
    label "zapewnienie"
  ]
  node [
    id 372
    label "bronienie"
  ]
  node [
    id 373
    label "chowanie"
  ]
  node [
    id 374
    label "potrzymanie"
  ]
  node [
    id 375
    label "twierdzenie"
  ]
  node [
    id 376
    label "wychowywanie"
  ]
  node [
    id 377
    label "retention"
  ]
  node [
    id 378
    label "s&#261;dzenie"
  ]
  node [
    id 379
    label "op&#322;acanie"
  ]
  node [
    id 380
    label "bearing"
  ]
  node [
    id 381
    label "wy&#380;ywienie"
  ]
  node [
    id 382
    label "zachowanie"
  ]
  node [
    id 383
    label "zdo&#322;anie"
  ]
  node [
    id 384
    label "uniesienie"
  ]
  node [
    id 385
    label "przetrzymanie"
  ]
  node [
    id 386
    label "obronienie"
  ]
  node [
    id 387
    label "zap&#322;acenie"
  ]
  node [
    id 388
    label "zrobienie"
  ]
  node [
    id 389
    label "subsystencja"
  ]
  node [
    id 390
    label "podtrzymanie"
  ]
  node [
    id 391
    label "wychowanie"
  ]
  node [
    id 392
    label "spowodowanie"
  ]
  node [
    id 393
    label "zdarzenie_si&#281;"
  ]
  node [
    id 394
    label "wzbudzenie"
  ]
  node [
    id 395
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 396
    label "gesture"
  ]
  node [
    id 397
    label "poruszanie_si&#281;"
  ]
  node [
    id 398
    label "nietaktowny"
  ]
  node [
    id 399
    label "niezgrabny"
  ]
  node [
    id 400
    label "szorstki"
  ]
  node [
    id 401
    label "kanciasto"
  ]
  node [
    id 402
    label "kanciaty"
  ]
  node [
    id 403
    label "niesk&#322;adny"
  ]
  node [
    id 404
    label "szko&#322;a"
  ]
  node [
    id 405
    label "opiekun"
  ]
  node [
    id 406
    label "stra&#380;nik"
  ]
  node [
    id 407
    label "przedszkole"
  ]
  node [
    id 408
    label "cz&#322;owiek"
  ]
  node [
    id 409
    label "spos&#243;b"
  ]
  node [
    id 410
    label "matryca"
  ]
  node [
    id 411
    label "facet"
  ]
  node [
    id 412
    label "zi&#243;&#322;ko"
  ]
  node [
    id 413
    label "mildew"
  ]
  node [
    id 414
    label "miniatura"
  ]
  node [
    id 415
    label "ideal"
  ]
  node [
    id 416
    label "adaptation"
  ]
  node [
    id 417
    label "typ"
  ]
  node [
    id 418
    label "imitacja"
  ]
  node [
    id 419
    label "pozowa&#263;"
  ]
  node [
    id 420
    label "orygina&#322;"
  ]
  node [
    id 421
    label "wz&#243;r"
  ]
  node [
    id 422
    label "motif"
  ]
  node [
    id 423
    label "prezenter"
  ]
  node [
    id 424
    label "pozowanie"
  ]
  node [
    id 425
    label "zaburzenie"
  ]
  node [
    id 426
    label "apraxia"
  ]
  node [
    id 427
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 428
    label "sport_motorowy"
  ]
  node [
    id 429
    label "jazda"
  ]
  node [
    id 430
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 431
    label "pocz&#261;tki"
  ]
  node [
    id 432
    label "metoda"
  ]
  node [
    id 433
    label "wrinkle"
  ]
  node [
    id 434
    label "zwiad"
  ]
  node [
    id 435
    label "Michnik"
  ]
  node [
    id 436
    label "solidarno&#347;ciowiec"
  ]
  node [
    id 437
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 438
    label "Sierpie&#324;"
  ]
  node [
    id 439
    label "wyjmowa&#263;"
  ]
  node [
    id 440
    label "deformowa&#263;"
  ]
  node [
    id 441
    label "corrupt"
  ]
  node [
    id 442
    label "wy&#380;yma&#263;"
  ]
  node [
    id 443
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 444
    label "odwraca&#263;"
  ]
  node [
    id 445
    label "wrench"
  ]
  node [
    id 446
    label "kierunek"
  ]
  node [
    id 447
    label "kierowa&#263;"
  ]
  node [
    id 448
    label "turn"
  ]
  node [
    id 449
    label "zmienia&#263;"
  ]
  node [
    id 450
    label "flip"
  ]
  node [
    id 451
    label "psychika"
  ]
  node [
    id 452
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 453
    label "express"
  ]
  node [
    id 454
    label "wyciska&#263;"
  ]
  node [
    id 455
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 456
    label "expand"
  ]
  node [
    id 457
    label "przemieszcza&#263;"
  ]
  node [
    id 458
    label "wyklucza&#263;"
  ]
  node [
    id 459
    label "produce"
  ]
  node [
    id 460
    label "linia"
  ]
  node [
    id 461
    label "zorientowa&#263;"
  ]
  node [
    id 462
    label "orientowa&#263;"
  ]
  node [
    id 463
    label "praktyka"
  ]
  node [
    id 464
    label "skr&#281;cenie"
  ]
  node [
    id 465
    label "skr&#281;ci&#263;"
  ]
  node [
    id 466
    label "przeorientowanie"
  ]
  node [
    id 467
    label "g&#243;ra"
  ]
  node [
    id 468
    label "orientowanie"
  ]
  node [
    id 469
    label "zorientowanie"
  ]
  node [
    id 470
    label "ty&#322;"
  ]
  node [
    id 471
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 472
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 473
    label "przeorientowywanie"
  ]
  node [
    id 474
    label "bok"
  ]
  node [
    id 475
    label "ideologia"
  ]
  node [
    id 476
    label "skr&#281;canie"
  ]
  node [
    id 477
    label "orientacja"
  ]
  node [
    id 478
    label "studia"
  ]
  node [
    id 479
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 480
    label "przeorientowa&#263;"
  ]
  node [
    id 481
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 482
    label "prz&#243;d"
  ]
  node [
    id 483
    label "skr&#281;ca&#263;"
  ]
  node [
    id 484
    label "system"
  ]
  node [
    id 485
    label "przeorientowywa&#263;"
  ]
  node [
    id 486
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 487
    label "pi&#322;ka"
  ]
  node [
    id 488
    label "r&#261;czyna"
  ]
  node [
    id 489
    label "paw"
  ]
  node [
    id 490
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 491
    label "bramkarz"
  ]
  node [
    id 492
    label "chwytanie"
  ]
  node [
    id 493
    label "chwyta&#263;"
  ]
  node [
    id 494
    label "rami&#281;"
  ]
  node [
    id 495
    label "k&#322;&#261;b"
  ]
  node [
    id 496
    label "gestykulowa&#263;"
  ]
  node [
    id 497
    label "cmoknonsens"
  ]
  node [
    id 498
    label "&#322;okie&#263;"
  ]
  node [
    id 499
    label "czerwona_kartka"
  ]
  node [
    id 500
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 501
    label "krzy&#380;"
  ]
  node [
    id 502
    label "gestykulowanie"
  ]
  node [
    id 503
    label "wykroczenie"
  ]
  node [
    id 504
    label "zagrywka"
  ]
  node [
    id 505
    label "nadgarstek"
  ]
  node [
    id 506
    label "obietnica"
  ]
  node [
    id 507
    label "kroki"
  ]
  node [
    id 508
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 509
    label "hasta"
  ]
  node [
    id 510
    label "pracownik"
  ]
  node [
    id 511
    label "hazena"
  ]
  node [
    id 512
    label "pomocnik"
  ]
  node [
    id 513
    label "handwriting"
  ]
  node [
    id 514
    label "hand"
  ]
  node [
    id 515
    label "graba"
  ]
  node [
    id 516
    label "palec"
  ]
  node [
    id 517
    label "przedrami&#281;"
  ]
  node [
    id 518
    label "d&#322;o&#324;"
  ]
  node [
    id 519
    label "&#347;wieca"
  ]
  node [
    id 520
    label "aut"
  ]
  node [
    id 521
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 522
    label "kula"
  ]
  node [
    id 523
    label "sport"
  ]
  node [
    id 524
    label "odbicie"
  ]
  node [
    id 525
    label "gra"
  ]
  node [
    id 526
    label "do&#347;rodkowywanie"
  ]
  node [
    id 527
    label "sport_zespo&#322;owy"
  ]
  node [
    id 528
    label "musket_ball"
  ]
  node [
    id 529
    label "zaserwowa&#263;"
  ]
  node [
    id 530
    label "serwowa&#263;"
  ]
  node [
    id 531
    label "rzucanka"
  ]
  node [
    id 532
    label "serwowanie"
  ]
  node [
    id 533
    label "orb"
  ]
  node [
    id 534
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 535
    label "zaserwowanie"
  ]
  node [
    id 536
    label "charakterystyka"
  ]
  node [
    id 537
    label "m&#322;ot"
  ]
  node [
    id 538
    label "marka"
  ]
  node [
    id 539
    label "attribute"
  ]
  node [
    id 540
    label "drzewo"
  ]
  node [
    id 541
    label "znak"
  ]
  node [
    id 542
    label "zbi&#243;r"
  ]
  node [
    id 543
    label "narz&#281;dzie"
  ]
  node [
    id 544
    label "nature"
  ]
  node [
    id 545
    label "tryb"
  ]
  node [
    id 546
    label "discourtesy"
  ]
  node [
    id 547
    label "post&#281;pek"
  ]
  node [
    id 548
    label "transgresja"
  ]
  node [
    id 549
    label "uderzenie"
  ]
  node [
    id 550
    label "mecz"
  ]
  node [
    id 551
    label "rozgrywka"
  ]
  node [
    id 552
    label "gambit"
  ]
  node [
    id 553
    label "gra_w_karty"
  ]
  node [
    id 554
    label "statement"
  ]
  node [
    id 555
    label "zapowied&#378;"
  ]
  node [
    id 556
    label "koszyk&#243;wka"
  ]
  node [
    id 557
    label "pi&#322;ka_r&#281;czna"
  ]
  node [
    id 558
    label "d&#378;wi&#281;k"
  ]
  node [
    id 559
    label "gracz"
  ]
  node [
    id 560
    label "zawodnik"
  ]
  node [
    id 561
    label "obrona"
  ]
  node [
    id 562
    label "hokej"
  ]
  node [
    id 563
    label "wykidaj&#322;o"
  ]
  node [
    id 564
    label "bileter"
  ]
  node [
    id 565
    label "ogarnia&#263;"
  ]
  node [
    id 566
    label "perceive"
  ]
  node [
    id 567
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 568
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 569
    label "doj&#347;&#263;"
  ]
  node [
    id 570
    label "rozumie&#263;"
  ]
  node [
    id 571
    label "bra&#263;"
  ]
  node [
    id 572
    label "ujmowa&#263;"
  ]
  node [
    id 573
    label "kompozycja"
  ]
  node [
    id 574
    label "w&#322;&#243;cznia"
  ]
  node [
    id 575
    label "triarius"
  ]
  node [
    id 576
    label "porywanie"
  ]
  node [
    id 577
    label "branie"
  ]
  node [
    id 578
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 579
    label "przyp&#322;ywanie"
  ]
  node [
    id 580
    label "perception"
  ]
  node [
    id 581
    label "ogarnianie"
  ]
  node [
    id 582
    label "rozumienie"
  ]
  node [
    id 583
    label "doj&#347;cie"
  ]
  node [
    id 584
    label "catch"
  ]
  node [
    id 585
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 586
    label "dochodzenie"
  ]
  node [
    id 587
    label "przedmiot"
  ]
  node [
    id 588
    label "traverse"
  ]
  node [
    id 589
    label "biblizm"
  ]
  node [
    id 590
    label "order"
  ]
  node [
    id 591
    label "gest"
  ]
  node [
    id 592
    label "kara_&#347;mierci"
  ]
  node [
    id 593
    label "cierpienie"
  ]
  node [
    id 594
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 595
    label "kszta&#322;t"
  ]
  node [
    id 596
    label "symbol"
  ]
  node [
    id 597
    label "ca&#322;us"
  ]
  node [
    id 598
    label "gesticulate"
  ]
  node [
    id 599
    label "rusza&#263;"
  ]
  node [
    id 600
    label "pokazywanie"
  ]
  node [
    id 601
    label "pokazanie"
  ]
  node [
    id 602
    label "ruszanie"
  ]
  node [
    id 603
    label "linia_&#380;ycia"
  ]
  node [
    id 604
    label "klepanie"
  ]
  node [
    id 605
    label "poduszka"
  ]
  node [
    id 606
    label "wyklepanie"
  ]
  node [
    id 607
    label "chiromancja"
  ]
  node [
    id 608
    label "linia_mi&#322;o&#347;ci"
  ]
  node [
    id 609
    label "dotykanie"
  ]
  node [
    id 610
    label "szeroko&#347;&#263;_d&#322;oni"
  ]
  node [
    id 611
    label "linia_rozumu"
  ]
  node [
    id 612
    label "klepa&#263;"
  ]
  node [
    id 613
    label "dotyka&#263;"
  ]
  node [
    id 614
    label "wyklepa&#263;"
  ]
  node [
    id 615
    label "polidaktylia"
  ]
  node [
    id 616
    label "zap&#322;on"
  ]
  node [
    id 617
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 618
    label "palpacja"
  ]
  node [
    id 619
    label "koniuszek_palca"
  ]
  node [
    id 620
    label "paznokie&#263;"
  ]
  node [
    id 621
    label "element_anatomiczny"
  ]
  node [
    id 622
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 623
    label "knykie&#263;"
  ]
  node [
    id 624
    label "pazur"
  ]
  node [
    id 625
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 626
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 627
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 628
    label "powerball"
  ]
  node [
    id 629
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 630
    label "kostka"
  ]
  node [
    id 631
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 632
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 633
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 634
    label "triceps"
  ]
  node [
    id 635
    label "robot_przemys&#322;owy"
  ]
  node [
    id 636
    label "narz&#261;d_ruchu"
  ]
  node [
    id 637
    label "element"
  ]
  node [
    id 638
    label "ko&#347;&#263;_ramieniowa"
  ]
  node [
    id 639
    label "maszyna"
  ]
  node [
    id 640
    label "mi&#281;sie&#324;_&#322;okciowy"
  ]
  node [
    id 641
    label "biceps"
  ]
  node [
    id 642
    label "oberwanie_si&#281;"
  ]
  node [
    id 643
    label "grzbiet"
  ]
  node [
    id 644
    label "p&#281;d"
  ]
  node [
    id 645
    label "pl&#261;tanina"
  ]
  node [
    id 646
    label "ob&#322;ok_Oorta"
  ]
  node [
    id 647
    label "cloud"
  ]
  node [
    id 648
    label "burza"
  ]
  node [
    id 649
    label "skupienie"
  ]
  node [
    id 650
    label "oberwa&#263;_si&#281;"
  ]
  node [
    id 651
    label "chmura"
  ]
  node [
    id 652
    label "powderpuff"
  ]
  node [
    id 653
    label "miara"
  ]
  node [
    id 654
    label "zgi&#281;cie_&#322;okciowe"
  ]
  node [
    id 655
    label "r&#281;kaw"
  ]
  node [
    id 656
    label "listewka"
  ]
  node [
    id 657
    label "d&#243;&#322;_&#322;okciowy"
  ]
  node [
    id 658
    label "ko&#347;&#263;_czworoboczna_mniejsza"
  ]
  node [
    id 659
    label "ko&#347;&#263;_czworoboczna_wi&#281;ksza"
  ]
  node [
    id 660
    label "metacarpus"
  ]
  node [
    id 661
    label "ko&#347;&#263;_&#322;okciowa"
  ]
  node [
    id 662
    label "ko&#347;&#263;_promieniowa"
  ]
  node [
    id 663
    label "r&#261;cz&#281;ta"
  ]
  node [
    id 664
    label "wrzosowate"
  ]
  node [
    id 665
    label "pomagacz"
  ]
  node [
    id 666
    label "bylina"
  ]
  node [
    id 667
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 668
    label "pomoc"
  ]
  node [
    id 669
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 670
    label "kredens"
  ]
  node [
    id 671
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 672
    label "delegowa&#263;"
  ]
  node [
    id 673
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 674
    label "salariat"
  ]
  node [
    id 675
    label "pracu&#347;"
  ]
  node [
    id 676
    label "delegowanie"
  ]
  node [
    id 677
    label "wymiociny"
  ]
  node [
    id 678
    label "ptak"
  ]
  node [
    id 679
    label "korona"
  ]
  node [
    id 680
    label "ba&#380;anty"
  ]
  node [
    id 681
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 682
    label "sandbag"
  ]
  node [
    id 683
    label "reakcja_chemiczna"
  ]
  node [
    id 684
    label "determine"
  ]
  node [
    id 685
    label "work"
  ]
  node [
    id 686
    label "motywowa&#263;"
  ]
  node [
    id 687
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 688
    label "zmieni&#263;"
  ]
  node [
    id 689
    label "pokl&#281;kn&#261;&#263;"
  ]
  node [
    id 690
    label "przyj&#261;&#263;"
  ]
  node [
    id 691
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 692
    label "kneel"
  ]
  node [
    id 693
    label "come_up"
  ]
  node [
    id 694
    label "sprawi&#263;"
  ]
  node [
    id 695
    label "zyska&#263;"
  ]
  node [
    id 696
    label "straci&#263;"
  ]
  node [
    id 697
    label "zast&#261;pi&#263;"
  ]
  node [
    id 698
    label "przej&#347;&#263;"
  ]
  node [
    id 699
    label "absorb"
  ]
  node [
    id 700
    label "swallow"
  ]
  node [
    id 701
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 702
    label "przybra&#263;"
  ]
  node [
    id 703
    label "odebra&#263;"
  ]
  node [
    id 704
    label "fall"
  ]
  node [
    id 705
    label "undertake"
  ]
  node [
    id 706
    label "umie&#347;ci&#263;"
  ]
  node [
    id 707
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 708
    label "draw"
  ]
  node [
    id 709
    label "obra&#263;"
  ]
  node [
    id 710
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 711
    label "dostarczy&#263;"
  ]
  node [
    id 712
    label "receive"
  ]
  node [
    id 713
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 714
    label "strike"
  ]
  node [
    id 715
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 716
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 717
    label "uzna&#263;"
  ]
  node [
    id 718
    label "przyj&#281;cie"
  ]
  node [
    id 719
    label "wzi&#261;&#263;"
  ]
  node [
    id 720
    label "posadzka"
  ]
  node [
    id 721
    label "budynek"
  ]
  node [
    id 722
    label "p&#322;aszczyzna"
  ]
  node [
    id 723
    label "zapadnia"
  ]
  node [
    id 724
    label "pojazd"
  ]
  node [
    id 725
    label "pomieszczenie"
  ]
  node [
    id 726
    label "odholowywa&#263;"
  ]
  node [
    id 727
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 728
    label "powietrze"
  ]
  node [
    id 729
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 730
    label "l&#261;d"
  ]
  node [
    id 731
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 732
    label "test_zderzeniowy"
  ]
  node [
    id 733
    label "nadwozie"
  ]
  node [
    id 734
    label "odholowa&#263;"
  ]
  node [
    id 735
    label "przeszklenie"
  ]
  node [
    id 736
    label "fukni&#281;cie"
  ]
  node [
    id 737
    label "tabor"
  ]
  node [
    id 738
    label "odzywka"
  ]
  node [
    id 739
    label "podwozie"
  ]
  node [
    id 740
    label "przyholowywanie"
  ]
  node [
    id 741
    label "przyholowanie"
  ]
  node [
    id 742
    label "zielona_karta"
  ]
  node [
    id 743
    label "przyholowywa&#263;"
  ]
  node [
    id 744
    label "przyholowa&#263;"
  ]
  node [
    id 745
    label "odholowywanie"
  ]
  node [
    id 746
    label "prowadzenie_si&#281;"
  ]
  node [
    id 747
    label "woda"
  ]
  node [
    id 748
    label "odholowanie"
  ]
  node [
    id 749
    label "hamulec"
  ]
  node [
    id 750
    label "fukanie"
  ]
  node [
    id 751
    label "kondygnacja"
  ]
  node [
    id 752
    label "skrzyd&#322;o"
  ]
  node [
    id 753
    label "klatka_schodowa"
  ]
  node [
    id 754
    label "front"
  ]
  node [
    id 755
    label "budowla"
  ]
  node [
    id 756
    label "alkierz"
  ]
  node [
    id 757
    label "strop"
  ]
  node [
    id 758
    label "przedpro&#380;e"
  ]
  node [
    id 759
    label "dach"
  ]
  node [
    id 760
    label "Pentagon"
  ]
  node [
    id 761
    label "balkon"
  ]
  node [
    id 762
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 763
    label "zakamarek"
  ]
  node [
    id 764
    label "amfilada"
  ]
  node [
    id 765
    label "sklepienie"
  ]
  node [
    id 766
    label "apartment"
  ]
  node [
    id 767
    label "udost&#281;pnienie"
  ]
  node [
    id 768
    label "umieszczenie"
  ]
  node [
    id 769
    label "sufit"
  ]
  node [
    id 770
    label "p&#322;aszczak"
  ]
  node [
    id 771
    label "zakres"
  ]
  node [
    id 772
    label "&#347;ciana"
  ]
  node [
    id 773
    label "ukszta&#322;towanie"
  ]
  node [
    id 774
    label "degree"
  ]
  node [
    id 775
    label "wymiar"
  ]
  node [
    id 776
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 777
    label "powierzchnia"
  ]
  node [
    id 778
    label "kwadrant"
  ]
  node [
    id 779
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 780
    label "surface"
  ]
  node [
    id 781
    label "pu&#322;apka"
  ]
  node [
    id 782
    label "&#380;y&#263;"
  ]
  node [
    id 783
    label "coating"
  ]
  node [
    id 784
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 785
    label "ko&#324;czy&#263;"
  ]
  node [
    id 786
    label "finish_up"
  ]
  node [
    id 787
    label "przebywa&#263;"
  ]
  node [
    id 788
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 789
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 790
    label "pause"
  ]
  node [
    id 791
    label "consist"
  ]
  node [
    id 792
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 793
    label "stay"
  ]
  node [
    id 794
    label "stanowi&#263;"
  ]
  node [
    id 795
    label "zako&#324;cza&#263;"
  ]
  node [
    id 796
    label "satisfy"
  ]
  node [
    id 797
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 798
    label "hesitate"
  ]
  node [
    id 799
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 800
    label "tkwi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 216
  ]
  edge [
    source 12
    target 228
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 229
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 246
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 371
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 241
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 34
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 36
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 353
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 301
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 719
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 287
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 684
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 34
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 443
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
]
