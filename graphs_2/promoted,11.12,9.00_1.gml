graph [
  node [
    id 0
    label "fizyk"
    origin "text"
  ]
  node [
    id 1
    label "belgia"
    origin "text"
  ]
  node [
    id 2
    label "stany"
    origin "text"
  ]
  node [
    id 3
    label "zjednoczy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zbudowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "miniaturowy"
    origin "text"
  ]
  node [
    id 6
    label "model"
    origin "text"
  ]
  node [
    id 7
    label "elektrownia"
    origin "text"
  ]
  node [
    id 8
    label "wiatrowy"
    origin "text"
  ]
  node [
    id 9
    label "sk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "sto"
    origin "text"
  ]
  node [
    id 12
    label "turbina"
    origin "text"
  ]
  node [
    id 13
    label "nauczyciel"
  ]
  node [
    id 14
    label "Kartezjusz"
  ]
  node [
    id 15
    label "Biot"
  ]
  node [
    id 16
    label "Einstein"
  ]
  node [
    id 17
    label "Galvani"
  ]
  node [
    id 18
    label "Culomb"
  ]
  node [
    id 19
    label "Doppler"
  ]
  node [
    id 20
    label "Lorentz"
  ]
  node [
    id 21
    label "naukowiec"
  ]
  node [
    id 22
    label "William_Nicol"
  ]
  node [
    id 23
    label "Faraday"
  ]
  node [
    id 24
    label "Weber"
  ]
  node [
    id 25
    label "Gilbert"
  ]
  node [
    id 26
    label "Newton"
  ]
  node [
    id 27
    label "Pascal"
  ]
  node [
    id 28
    label "Maxwell"
  ]
  node [
    id 29
    label "belfer"
  ]
  node [
    id 30
    label "kszta&#322;ciciel"
  ]
  node [
    id 31
    label "preceptor"
  ]
  node [
    id 32
    label "pedagog"
  ]
  node [
    id 33
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 34
    label "szkolnik"
  ]
  node [
    id 35
    label "profesor"
  ]
  node [
    id 36
    label "popularyzator"
  ]
  node [
    id 37
    label "&#347;ledziciel"
  ]
  node [
    id 38
    label "uczony"
  ]
  node [
    id 39
    label "Miczurin"
  ]
  node [
    id 40
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 41
    label "j&#281;zyk_programowania"
  ]
  node [
    id 42
    label "filozofia"
  ]
  node [
    id 43
    label "wyra&#378;no&#347;&#263;"
  ]
  node [
    id 44
    label "rozci&#261;g&#322;y"
  ]
  node [
    id 45
    label "pogl&#261;dy"
  ]
  node [
    id 46
    label "teoria_wzgl&#281;dno&#347;ci"
  ]
  node [
    id 47
    label "consort"
  ]
  node [
    id 48
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 49
    label "stworzy&#263;"
  ]
  node [
    id 50
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 51
    label "incorporate"
  ]
  node [
    id 52
    label "zrobi&#263;"
  ]
  node [
    id 53
    label "connect"
  ]
  node [
    id 54
    label "spowodowa&#263;"
  ]
  node [
    id 55
    label "relate"
  ]
  node [
    id 56
    label "po&#322;&#261;czenie"
  ]
  node [
    id 57
    label "budowla"
  ]
  node [
    id 58
    label "establish"
  ]
  node [
    id 59
    label "evolve"
  ]
  node [
    id 60
    label "zaplanowa&#263;"
  ]
  node [
    id 61
    label "wytworzy&#263;"
  ]
  node [
    id 62
    label "cause"
  ]
  node [
    id 63
    label "manufacture"
  ]
  node [
    id 64
    label "przemy&#347;le&#263;"
  ]
  node [
    id 65
    label "line_up"
  ]
  node [
    id 66
    label "opracowa&#263;"
  ]
  node [
    id 67
    label "map"
  ]
  node [
    id 68
    label "pomy&#347;le&#263;"
  ]
  node [
    id 69
    label "create"
  ]
  node [
    id 70
    label "specjalista_od_public_relations"
  ]
  node [
    id 71
    label "wizerunek"
  ]
  node [
    id 72
    label "przygotowa&#263;"
  ]
  node [
    id 73
    label "obudowanie"
  ]
  node [
    id 74
    label "obudowywa&#263;"
  ]
  node [
    id 75
    label "obudowa&#263;"
  ]
  node [
    id 76
    label "kolumnada"
  ]
  node [
    id 77
    label "korpus"
  ]
  node [
    id 78
    label "Sukiennice"
  ]
  node [
    id 79
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 80
    label "fundament"
  ]
  node [
    id 81
    label "postanie"
  ]
  node [
    id 82
    label "obudowywanie"
  ]
  node [
    id 83
    label "zbudowanie"
  ]
  node [
    id 84
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 85
    label "stan_surowy"
  ]
  node [
    id 86
    label "konstrukcja"
  ]
  node [
    id 87
    label "rzecz"
  ]
  node [
    id 88
    label "miniaturowo"
  ]
  node [
    id 89
    label "male&#324;ki"
  ]
  node [
    id 90
    label "ma&#322;y"
  ]
  node [
    id 91
    label "ma&#322;o"
  ]
  node [
    id 92
    label "szybki"
  ]
  node [
    id 93
    label "nieznaczny"
  ]
  node [
    id 94
    label "przeci&#281;tny"
  ]
  node [
    id 95
    label "wstydliwy"
  ]
  node [
    id 96
    label "s&#322;aby"
  ]
  node [
    id 97
    label "niewa&#380;ny"
  ]
  node [
    id 98
    label "ch&#322;opiec"
  ]
  node [
    id 99
    label "m&#322;ody"
  ]
  node [
    id 100
    label "marny"
  ]
  node [
    id 101
    label "nieliczny"
  ]
  node [
    id 102
    label "n&#281;dznie"
  ]
  node [
    id 103
    label "male&#324;ko"
  ]
  node [
    id 104
    label "szybciute&#324;ki"
  ]
  node [
    id 105
    label "spos&#243;b"
  ]
  node [
    id 106
    label "cz&#322;owiek"
  ]
  node [
    id 107
    label "prezenter"
  ]
  node [
    id 108
    label "typ"
  ]
  node [
    id 109
    label "mildew"
  ]
  node [
    id 110
    label "zi&#243;&#322;ko"
  ]
  node [
    id 111
    label "motif"
  ]
  node [
    id 112
    label "pozowanie"
  ]
  node [
    id 113
    label "ideal"
  ]
  node [
    id 114
    label "wz&#243;r"
  ]
  node [
    id 115
    label "matryca"
  ]
  node [
    id 116
    label "adaptation"
  ]
  node [
    id 117
    label "ruch"
  ]
  node [
    id 118
    label "pozowa&#263;"
  ]
  node [
    id 119
    label "imitacja"
  ]
  node [
    id 120
    label "orygina&#322;"
  ]
  node [
    id 121
    label "facet"
  ]
  node [
    id 122
    label "miniatura"
  ]
  node [
    id 123
    label "narz&#281;dzie"
  ]
  node [
    id 124
    label "gablotka"
  ]
  node [
    id 125
    label "pokaz"
  ]
  node [
    id 126
    label "szkatu&#322;ka"
  ]
  node [
    id 127
    label "pude&#322;ko"
  ]
  node [
    id 128
    label "bran&#380;owiec"
  ]
  node [
    id 129
    label "prowadz&#261;cy"
  ]
  node [
    id 130
    label "ludzko&#347;&#263;"
  ]
  node [
    id 131
    label "asymilowanie"
  ]
  node [
    id 132
    label "wapniak"
  ]
  node [
    id 133
    label "asymilowa&#263;"
  ]
  node [
    id 134
    label "os&#322;abia&#263;"
  ]
  node [
    id 135
    label "posta&#263;"
  ]
  node [
    id 136
    label "hominid"
  ]
  node [
    id 137
    label "podw&#322;adny"
  ]
  node [
    id 138
    label "os&#322;abianie"
  ]
  node [
    id 139
    label "g&#322;owa"
  ]
  node [
    id 140
    label "figura"
  ]
  node [
    id 141
    label "portrecista"
  ]
  node [
    id 142
    label "dwun&#243;g"
  ]
  node [
    id 143
    label "profanum"
  ]
  node [
    id 144
    label "mikrokosmos"
  ]
  node [
    id 145
    label "nasada"
  ]
  node [
    id 146
    label "duch"
  ]
  node [
    id 147
    label "antropochoria"
  ]
  node [
    id 148
    label "osoba"
  ]
  node [
    id 149
    label "senior"
  ]
  node [
    id 150
    label "oddzia&#322;ywanie"
  ]
  node [
    id 151
    label "Adam"
  ]
  node [
    id 152
    label "homo_sapiens"
  ]
  node [
    id 153
    label "polifag"
  ]
  node [
    id 154
    label "kszta&#322;t"
  ]
  node [
    id 155
    label "przedmiot"
  ]
  node [
    id 156
    label "kopia"
  ]
  node [
    id 157
    label "utw&#243;r"
  ]
  node [
    id 158
    label "obraz"
  ]
  node [
    id 159
    label "obiekt"
  ]
  node [
    id 160
    label "ilustracja"
  ]
  node [
    id 161
    label "miniature"
  ]
  node [
    id 162
    label "zapis"
  ]
  node [
    id 163
    label "figure"
  ]
  node [
    id 164
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 165
    label "rule"
  ]
  node [
    id 166
    label "dekal"
  ]
  node [
    id 167
    label "motyw"
  ]
  node [
    id 168
    label "projekt"
  ]
  node [
    id 169
    label "technika"
  ]
  node [
    id 170
    label "praktyka"
  ]
  node [
    id 171
    label "na&#347;ladownictwo"
  ]
  node [
    id 172
    label "zbi&#243;r"
  ]
  node [
    id 173
    label "tryb"
  ]
  node [
    id 174
    label "nature"
  ]
  node [
    id 175
    label "bratek"
  ]
  node [
    id 176
    label "kod_genetyczny"
  ]
  node [
    id 177
    label "t&#322;ocznik"
  ]
  node [
    id 178
    label "aparat_cyfrowy"
  ]
  node [
    id 179
    label "detector"
  ]
  node [
    id 180
    label "forma"
  ]
  node [
    id 181
    label "jednostka_systematyczna"
  ]
  node [
    id 182
    label "kr&#243;lestwo"
  ]
  node [
    id 183
    label "autorament"
  ]
  node [
    id 184
    label "variety"
  ]
  node [
    id 185
    label "antycypacja"
  ]
  node [
    id 186
    label "przypuszczenie"
  ]
  node [
    id 187
    label "cynk"
  ]
  node [
    id 188
    label "obstawia&#263;"
  ]
  node [
    id 189
    label "gromada"
  ]
  node [
    id 190
    label "sztuka"
  ]
  node [
    id 191
    label "rezultat"
  ]
  node [
    id 192
    label "design"
  ]
  node [
    id 193
    label "sit"
  ]
  node [
    id 194
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 195
    label "robi&#263;"
  ]
  node [
    id 196
    label "dally"
  ]
  node [
    id 197
    label "sfotografowanie_si&#281;"
  ]
  node [
    id 198
    label "na&#347;ladowanie"
  ]
  node [
    id 199
    label "robienie"
  ]
  node [
    id 200
    label "fotografowanie_si&#281;"
  ]
  node [
    id 201
    label "czynno&#347;&#263;"
  ]
  node [
    id 202
    label "pretense"
  ]
  node [
    id 203
    label "mechanika"
  ]
  node [
    id 204
    label "utrzymywanie"
  ]
  node [
    id 205
    label "move"
  ]
  node [
    id 206
    label "poruszenie"
  ]
  node [
    id 207
    label "movement"
  ]
  node [
    id 208
    label "myk"
  ]
  node [
    id 209
    label "utrzyma&#263;"
  ]
  node [
    id 210
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 211
    label "zjawisko"
  ]
  node [
    id 212
    label "utrzymanie"
  ]
  node [
    id 213
    label "travel"
  ]
  node [
    id 214
    label "kanciasty"
  ]
  node [
    id 215
    label "commercial_enterprise"
  ]
  node [
    id 216
    label "strumie&#324;"
  ]
  node [
    id 217
    label "proces"
  ]
  node [
    id 218
    label "aktywno&#347;&#263;"
  ]
  node [
    id 219
    label "kr&#243;tki"
  ]
  node [
    id 220
    label "taktyka"
  ]
  node [
    id 221
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 222
    label "apraksja"
  ]
  node [
    id 223
    label "natural_process"
  ]
  node [
    id 224
    label "utrzymywa&#263;"
  ]
  node [
    id 225
    label "d&#322;ugi"
  ]
  node [
    id 226
    label "wydarzenie"
  ]
  node [
    id 227
    label "dyssypacja_energii"
  ]
  node [
    id 228
    label "tumult"
  ]
  node [
    id 229
    label "stopek"
  ]
  node [
    id 230
    label "zmiana"
  ]
  node [
    id 231
    label "manewr"
  ]
  node [
    id 232
    label "lokomocja"
  ]
  node [
    id 233
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 234
    label "komunikacja"
  ]
  node [
    id 235
    label "drift"
  ]
  node [
    id 236
    label "nicpo&#324;"
  ]
  node [
    id 237
    label "agent"
  ]
  node [
    id 238
    label "zak&#322;ad_komunalny"
  ]
  node [
    id 239
    label "przekazywa&#263;"
  ]
  node [
    id 240
    label "zbiera&#263;"
  ]
  node [
    id 241
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 242
    label "przywraca&#263;"
  ]
  node [
    id 243
    label "dawa&#263;"
  ]
  node [
    id 244
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 245
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 246
    label "convey"
  ]
  node [
    id 247
    label "publicize"
  ]
  node [
    id 248
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 249
    label "render"
  ]
  node [
    id 250
    label "uk&#322;ada&#263;"
  ]
  node [
    id 251
    label "opracowywa&#263;"
  ]
  node [
    id 252
    label "set"
  ]
  node [
    id 253
    label "oddawa&#263;"
  ]
  node [
    id 254
    label "train"
  ]
  node [
    id 255
    label "zmienia&#263;"
  ]
  node [
    id 256
    label "dzieli&#263;"
  ]
  node [
    id 257
    label "scala&#263;"
  ]
  node [
    id 258
    label "zestaw"
  ]
  node [
    id 259
    label "divide"
  ]
  node [
    id 260
    label "posiada&#263;"
  ]
  node [
    id 261
    label "deal"
  ]
  node [
    id 262
    label "cover"
  ]
  node [
    id 263
    label "liczy&#263;"
  ]
  node [
    id 264
    label "assign"
  ]
  node [
    id 265
    label "korzysta&#263;"
  ]
  node [
    id 266
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 267
    label "digest"
  ]
  node [
    id 268
    label "powodowa&#263;"
  ]
  node [
    id 269
    label "share"
  ]
  node [
    id 270
    label "iloraz"
  ]
  node [
    id 271
    label "sk&#322;&#243;ca&#263;"
  ]
  node [
    id 272
    label "rozdawa&#263;"
  ]
  node [
    id 273
    label "sprawowa&#263;"
  ]
  node [
    id 274
    label "dostarcza&#263;"
  ]
  node [
    id 275
    label "sacrifice"
  ]
  node [
    id 276
    label "odst&#281;powa&#263;"
  ]
  node [
    id 277
    label "sprzedawa&#263;"
  ]
  node [
    id 278
    label "give"
  ]
  node [
    id 279
    label "reflect"
  ]
  node [
    id 280
    label "surrender"
  ]
  node [
    id 281
    label "deliver"
  ]
  node [
    id 282
    label "odpowiada&#263;"
  ]
  node [
    id 283
    label "umieszcza&#263;"
  ]
  node [
    id 284
    label "blurt_out"
  ]
  node [
    id 285
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 286
    label "przedstawia&#263;"
  ]
  node [
    id 287
    label "impart"
  ]
  node [
    id 288
    label "przejmowa&#263;"
  ]
  node [
    id 289
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 290
    label "gromadzi&#263;"
  ]
  node [
    id 291
    label "mie&#263;_miejsce"
  ]
  node [
    id 292
    label "bra&#263;"
  ]
  node [
    id 293
    label "pozyskiwa&#263;"
  ]
  node [
    id 294
    label "poci&#261;ga&#263;"
  ]
  node [
    id 295
    label "wzbiera&#263;"
  ]
  node [
    id 296
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 297
    label "meet"
  ]
  node [
    id 298
    label "dostawa&#263;"
  ]
  node [
    id 299
    label "consolidate"
  ]
  node [
    id 300
    label "congregate"
  ]
  node [
    id 301
    label "postpone"
  ]
  node [
    id 302
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 303
    label "znosi&#263;"
  ]
  node [
    id 304
    label "chroni&#263;"
  ]
  node [
    id 305
    label "darowywa&#263;"
  ]
  node [
    id 306
    label "preserve"
  ]
  node [
    id 307
    label "zachowywa&#263;"
  ]
  node [
    id 308
    label "gospodarowa&#263;"
  ]
  node [
    id 309
    label "dispose"
  ]
  node [
    id 310
    label "uczy&#263;"
  ]
  node [
    id 311
    label "kszta&#322;ci&#263;"
  ]
  node [
    id 312
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 313
    label "porz&#261;dkowa&#263;"
  ]
  node [
    id 314
    label "przygotowywa&#263;"
  ]
  node [
    id 315
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 316
    label "tworzy&#263;"
  ]
  node [
    id 317
    label "treser"
  ]
  node [
    id 318
    label "raise"
  ]
  node [
    id 319
    label "pozostawia&#263;"
  ]
  node [
    id 320
    label "zaczyna&#263;"
  ]
  node [
    id 321
    label "psu&#263;"
  ]
  node [
    id 322
    label "wzbudza&#263;"
  ]
  node [
    id 323
    label "wk&#322;ada&#263;"
  ]
  node [
    id 324
    label "go"
  ]
  node [
    id 325
    label "inspirowa&#263;"
  ]
  node [
    id 326
    label "wpaja&#263;"
  ]
  node [
    id 327
    label "znak"
  ]
  node [
    id 328
    label "seat"
  ]
  node [
    id 329
    label "wygrywa&#263;"
  ]
  node [
    id 330
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 331
    label "go&#347;ci&#263;"
  ]
  node [
    id 332
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 333
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 334
    label "pour"
  ]
  node [
    id 335
    label "elaborate"
  ]
  node [
    id 336
    label "pokrywa&#263;"
  ]
  node [
    id 337
    label "traci&#263;"
  ]
  node [
    id 338
    label "alternate"
  ]
  node [
    id 339
    label "change"
  ]
  node [
    id 340
    label "reengineering"
  ]
  node [
    id 341
    label "zast&#281;powa&#263;"
  ]
  node [
    id 342
    label "sprawia&#263;"
  ]
  node [
    id 343
    label "zyskiwa&#263;"
  ]
  node [
    id 344
    label "przechodzi&#263;"
  ]
  node [
    id 345
    label "jednoczy&#263;"
  ]
  node [
    id 346
    label "work"
  ]
  node [
    id 347
    label "wysy&#322;a&#263;"
  ]
  node [
    id 348
    label "podawa&#263;"
  ]
  node [
    id 349
    label "wp&#322;aca&#263;"
  ]
  node [
    id 350
    label "sygna&#322;"
  ]
  node [
    id 351
    label "doprowadza&#263;"
  ]
  node [
    id 352
    label "&#322;adowa&#263;"
  ]
  node [
    id 353
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 354
    label "przeznacza&#263;"
  ]
  node [
    id 355
    label "traktowa&#263;"
  ]
  node [
    id 356
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 357
    label "obiecywa&#263;"
  ]
  node [
    id 358
    label "tender"
  ]
  node [
    id 359
    label "rap"
  ]
  node [
    id 360
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 361
    label "t&#322;uc"
  ]
  node [
    id 362
    label "powierza&#263;"
  ]
  node [
    id 363
    label "wpiernicza&#263;"
  ]
  node [
    id 364
    label "exsert"
  ]
  node [
    id 365
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 366
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 367
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 368
    label "p&#322;aci&#263;"
  ]
  node [
    id 369
    label "hold_out"
  ]
  node [
    id 370
    label "nalewa&#263;"
  ]
  node [
    id 371
    label "zezwala&#263;"
  ]
  node [
    id 372
    label "hold"
  ]
  node [
    id 373
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 374
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 375
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 376
    label "dopieprza&#263;"
  ]
  node [
    id 377
    label "press"
  ]
  node [
    id 378
    label "urge"
  ]
  node [
    id 379
    label "zbli&#380;a&#263;"
  ]
  node [
    id 380
    label "przykrochmala&#263;"
  ]
  node [
    id 381
    label "uderza&#263;"
  ]
  node [
    id 382
    label "gem"
  ]
  node [
    id 383
    label "kompozycja"
  ]
  node [
    id 384
    label "runda"
  ]
  node [
    id 385
    label "muzyka"
  ]
  node [
    id 386
    label "struktura"
  ]
  node [
    id 387
    label "stage_set"
  ]
  node [
    id 388
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 389
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 390
    label "maszyna_hydrauliczna"
  ]
  node [
    id 391
    label "stopie&#324;_turbiny"
  ]
  node [
    id 392
    label "turbozesp&#243;&#322;"
  ]
  node [
    id 393
    label "silnik_przep&#322;ywowy"
  ]
  node [
    id 394
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 395
    label "spr&#281;&#380;arka"
  ]
  node [
    id 396
    label "turbogenerator"
  ]
  node [
    id 397
    label "Stany"
  ]
  node [
    id 398
    label "Physical"
  ]
  node [
    id 399
    label "Review"
  ]
  node [
    id 400
    label "Fluids"
  ]
  node [
    id 401
    label "en"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 398
    target 399
  ]
  edge [
    source 398
    target 400
  ]
  edge [
    source 398
    target 401
  ]
  edge [
    source 399
    target 400
  ]
  edge [
    source 399
    target 401
  ]
  edge [
    source 400
    target 401
  ]
]
