graph [
  node [
    id 0
    label "jaki"
    origin "text"
  ]
  node [
    id 1
    label "kosmita"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 3
    label "uniwersum"
    origin "text"
  ]
  node [
    id 4
    label "kapitan"
    origin "text"
  ]
  node [
    id 5
    label "bomba"
    origin "text"
  ]
  node [
    id 6
    label "ostatni"
    origin "text"
  ]
  node [
    id 7
    label "cyfra"
    origin "text"
  ]
  node [
    id 8
    label "liczba"
    origin "text"
  ]
  node [
    id 9
    label "plus"
    origin "text"
  ]
  node [
    id 10
    label "prawda"
    origin "text"
  ]
  node [
    id 11
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 12
    label "osoba"
  ]
  node [
    id 13
    label "istota_&#380;ywa"
  ]
  node [
    id 14
    label "Chocho&#322;"
  ]
  node [
    id 15
    label "Herkules_Poirot"
  ]
  node [
    id 16
    label "Edyp"
  ]
  node [
    id 17
    label "ludzko&#347;&#263;"
  ]
  node [
    id 18
    label "parali&#380;owa&#263;"
  ]
  node [
    id 19
    label "Harry_Potter"
  ]
  node [
    id 20
    label "Casanova"
  ]
  node [
    id 21
    label "Zgredek"
  ]
  node [
    id 22
    label "Gargantua"
  ]
  node [
    id 23
    label "Winnetou"
  ]
  node [
    id 24
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 25
    label "posta&#263;"
  ]
  node [
    id 26
    label "Dulcynea"
  ]
  node [
    id 27
    label "kategoria_gramatyczna"
  ]
  node [
    id 28
    label "g&#322;owa"
  ]
  node [
    id 29
    label "figura"
  ]
  node [
    id 30
    label "portrecista"
  ]
  node [
    id 31
    label "person"
  ]
  node [
    id 32
    label "Plastu&#347;"
  ]
  node [
    id 33
    label "Quasimodo"
  ]
  node [
    id 34
    label "Sherlock_Holmes"
  ]
  node [
    id 35
    label "Faust"
  ]
  node [
    id 36
    label "Wallenrod"
  ]
  node [
    id 37
    label "Dwukwiat"
  ]
  node [
    id 38
    label "Don_Juan"
  ]
  node [
    id 39
    label "profanum"
  ]
  node [
    id 40
    label "koniugacja"
  ]
  node [
    id 41
    label "Don_Kiszot"
  ]
  node [
    id 42
    label "mikrokosmos"
  ]
  node [
    id 43
    label "duch"
  ]
  node [
    id 44
    label "antropochoria"
  ]
  node [
    id 45
    label "oddzia&#322;ywanie"
  ]
  node [
    id 46
    label "Hamlet"
  ]
  node [
    id 47
    label "Werter"
  ]
  node [
    id 48
    label "istota"
  ]
  node [
    id 49
    label "Szwejk"
  ]
  node [
    id 50
    label "homo_sapiens"
  ]
  node [
    id 51
    label "ciemna_materia"
  ]
  node [
    id 52
    label "planeta"
  ]
  node [
    id 53
    label "ekosfera"
  ]
  node [
    id 54
    label "przestrze&#324;"
  ]
  node [
    id 55
    label "czarna_dziura"
  ]
  node [
    id 56
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 57
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 58
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 59
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 60
    label "poj&#281;cie"
  ]
  node [
    id 61
    label "makrokosmos"
  ]
  node [
    id 62
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 63
    label "rozdzielanie"
  ]
  node [
    id 64
    label "bezbrze&#380;e"
  ]
  node [
    id 65
    label "punkt"
  ]
  node [
    id 66
    label "czasoprzestrze&#324;"
  ]
  node [
    id 67
    label "zbi&#243;r"
  ]
  node [
    id 68
    label "niezmierzony"
  ]
  node [
    id 69
    label "przedzielenie"
  ]
  node [
    id 70
    label "nielito&#347;ciwy"
  ]
  node [
    id 71
    label "rozdziela&#263;"
  ]
  node [
    id 72
    label "oktant"
  ]
  node [
    id 73
    label "miejsce"
  ]
  node [
    id 74
    label "przedzieli&#263;"
  ]
  node [
    id 75
    label "przestw&#243;r"
  ]
  node [
    id 76
    label "pos&#322;uchanie"
  ]
  node [
    id 77
    label "skumanie"
  ]
  node [
    id 78
    label "orientacja"
  ]
  node [
    id 79
    label "wytw&#243;r"
  ]
  node [
    id 80
    label "zorientowanie"
  ]
  node [
    id 81
    label "teoria"
  ]
  node [
    id 82
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 83
    label "clasp"
  ]
  node [
    id 84
    label "forma"
  ]
  node [
    id 85
    label "przem&#243;wienie"
  ]
  node [
    id 86
    label "strefa"
  ]
  node [
    id 87
    label "kosmos"
  ]
  node [
    id 88
    label "cz&#322;owiek"
  ]
  node [
    id 89
    label "atom"
  ]
  node [
    id 90
    label "odbicie"
  ]
  node [
    id 91
    label "przyroda"
  ]
  node [
    id 92
    label "Ziemia"
  ]
  node [
    id 93
    label "miniatura"
  ]
  node [
    id 94
    label "Jowisz"
  ]
  node [
    id 95
    label "syzygia"
  ]
  node [
    id 96
    label "atmosfera"
  ]
  node [
    id 97
    label "Saturn"
  ]
  node [
    id 98
    label "Uran"
  ]
  node [
    id 99
    label "aspekt"
  ]
  node [
    id 100
    label "kultura"
  ]
  node [
    id 101
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 102
    label "kontekst"
  ]
  node [
    id 103
    label "message"
  ]
  node [
    id 104
    label "&#347;wiat"
  ]
  node [
    id 105
    label "dar"
  ]
  node [
    id 106
    label "zjawisko"
  ]
  node [
    id 107
    label "cecha"
  ]
  node [
    id 108
    label "real"
  ]
  node [
    id 109
    label "pilot"
  ]
  node [
    id 110
    label "zawodnik"
  ]
  node [
    id 111
    label "dow&#243;dca"
  ]
  node [
    id 112
    label "oficer_&#380;eglugi"
  ]
  node [
    id 113
    label "gracz"
  ]
  node [
    id 114
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 115
    label "oficer"
  ]
  node [
    id 116
    label "zwierzchnik"
  ]
  node [
    id 117
    label "dow&#243;dztwo"
  ]
  node [
    id 118
    label "podchor&#261;&#380;y"
  ]
  node [
    id 119
    label "podoficer"
  ]
  node [
    id 120
    label "mundurowy"
  ]
  node [
    id 121
    label "lotnik"
  ]
  node [
    id 122
    label "przewodnik"
  ]
  node [
    id 123
    label "delfin"
  ]
  node [
    id 124
    label "briefing"
  ]
  node [
    id 125
    label "wycieczka"
  ]
  node [
    id 126
    label "nawigator"
  ]
  node [
    id 127
    label "delfinowate"
  ]
  node [
    id 128
    label "zwiastun"
  ]
  node [
    id 129
    label "urz&#261;dzenie"
  ]
  node [
    id 130
    label "zi&#243;&#322;ko"
  ]
  node [
    id 131
    label "czo&#322;&#243;wka"
  ]
  node [
    id 132
    label "uczestnik"
  ]
  node [
    id 133
    label "lista_startowa"
  ]
  node [
    id 134
    label "sportowiec"
  ]
  node [
    id 135
    label "orygina&#322;"
  ]
  node [
    id 136
    label "facet"
  ]
  node [
    id 137
    label "zwierz&#281;"
  ]
  node [
    id 138
    label "bohater"
  ]
  node [
    id 139
    label "spryciarz"
  ]
  node [
    id 140
    label "rozdawa&#263;_karty"
  ]
  node [
    id 141
    label "czerep"
  ]
  node [
    id 142
    label "nab&#243;j"
  ]
  node [
    id 143
    label "pocisk"
  ]
  node [
    id 144
    label "novum"
  ]
  node [
    id 145
    label "niedostateczny"
  ]
  node [
    id 146
    label "bombowiec"
  ]
  node [
    id 147
    label "zapalnik"
  ]
  node [
    id 148
    label "strza&#322;"
  ]
  node [
    id 149
    label "materia&#322;_piroklastyczny"
  ]
  node [
    id 150
    label "pa&#322;a"
  ]
  node [
    id 151
    label "jedynka"
  ]
  node [
    id 152
    label "niezadowalaj&#261;cy"
  ]
  node [
    id 153
    label "dw&#243;jka"
  ]
  node [
    id 154
    label "trafny"
  ]
  node [
    id 155
    label "shot"
  ]
  node [
    id 156
    label "przykro&#347;&#263;"
  ]
  node [
    id 157
    label "huk"
  ]
  node [
    id 158
    label "bum-bum"
  ]
  node [
    id 159
    label "pi&#322;ka"
  ]
  node [
    id 160
    label "uderzenie"
  ]
  node [
    id 161
    label "eksplozja"
  ]
  node [
    id 162
    label "wyrzut"
  ]
  node [
    id 163
    label "usi&#322;owanie"
  ]
  node [
    id 164
    label "przypadek"
  ]
  node [
    id 165
    label "shooting"
  ]
  node [
    id 166
    label "odgadywanie"
  ]
  node [
    id 167
    label "proch_bezdymny"
  ]
  node [
    id 168
    label "amunicja"
  ]
  node [
    id 169
    label "&#322;uska"
  ]
  node [
    id 170
    label "o&#322;&#243;w"
  ]
  node [
    id 171
    label "kartuza"
  ]
  node [
    id 172
    label "prochownia"
  ]
  node [
    id 173
    label "musket_ball"
  ]
  node [
    id 174
    label "charge"
  ]
  node [
    id 175
    label "komora_nabojowa"
  ]
  node [
    id 176
    label "ta&#347;ma"
  ]
  node [
    id 177
    label "samoch&#243;d_pu&#322;apka"
  ]
  node [
    id 178
    label "patron"
  ]
  node [
    id 179
    label "knickknack"
  ]
  node [
    id 180
    label "przedmiot"
  ]
  node [
    id 181
    label "nowo&#347;&#263;"
  ]
  node [
    id 182
    label "g&#322;owica"
  ]
  node [
    id 183
    label "trafienie"
  ]
  node [
    id 184
    label "trafianie"
  ]
  node [
    id 185
    label "kulka"
  ]
  node [
    id 186
    label "rdze&#324;"
  ]
  node [
    id 187
    label "przeniesienie"
  ]
  node [
    id 188
    label "&#322;adunek_bojowy"
  ]
  node [
    id 189
    label "trafi&#263;"
  ]
  node [
    id 190
    label "przenoszenie"
  ]
  node [
    id 191
    label "przenie&#347;&#263;"
  ]
  node [
    id 192
    label "trafia&#263;"
  ]
  node [
    id 193
    label "przenosi&#263;"
  ]
  node [
    id 194
    label "bro&#324;"
  ]
  node [
    id 195
    label "szew_kostny"
  ]
  node [
    id 196
    label "wiedza"
  ]
  node [
    id 197
    label "kawa&#322;ek"
  ]
  node [
    id 198
    label "trzewioczaszka"
  ]
  node [
    id 199
    label "siode&#322;ko_tureckie"
  ]
  node [
    id 200
    label "wa&#322;_nadoczodo&#322;owy"
  ]
  node [
    id 201
    label "m&#243;zgoczaszka"
  ]
  node [
    id 202
    label "&#347;mie&#263;"
  ]
  node [
    id 203
    label "ko&#347;&#263;_czo&#322;owa"
  ]
  node [
    id 204
    label "dynia"
  ]
  node [
    id 205
    label "ko&#347;&#263;_podniebienna"
  ]
  node [
    id 206
    label "rozszczep_czaszki"
  ]
  node [
    id 207
    label "korpus"
  ]
  node [
    id 208
    label "szew_strza&#322;kowy"
  ]
  node [
    id 209
    label "mak&#243;wka"
  ]
  node [
    id 210
    label "skorupa"
  ]
  node [
    id 211
    label "&#322;uk_jarzmowy"
  ]
  node [
    id 212
    label "ko&#347;&#263;_klinowa"
  ]
  node [
    id 213
    label "noosfera"
  ]
  node [
    id 214
    label "naczynie"
  ]
  node [
    id 215
    label "szkielet"
  ]
  node [
    id 216
    label "fragment"
  ]
  node [
    id 217
    label "czaszka"
  ]
  node [
    id 218
    label "zatoka"
  ]
  node [
    id 219
    label "ma&#322;og&#322;owie"
  ]
  node [
    id 220
    label "potylica"
  ]
  node [
    id 221
    label "oczod&#243;&#322;"
  ]
  node [
    id 222
    label "czapa"
  ]
  node [
    id 223
    label "lemiesz"
  ]
  node [
    id 224
    label "szczelina_oczodo&#322;owa"
  ]
  node [
    id 225
    label "&#380;uchwa"
  ]
  node [
    id 226
    label "p&#243;&#322;kula"
  ]
  node [
    id 227
    label "po&#347;redniog&#322;owo&#347;&#263;"
  ]
  node [
    id 228
    label "diafanoskopia"
  ]
  node [
    id 229
    label "umys&#322;"
  ]
  node [
    id 230
    label "&#322;eb"
  ]
  node [
    id 231
    label "granat"
  ]
  node [
    id 232
    label "ciemi&#281;"
  ]
  node [
    id 233
    label "mina"
  ]
  node [
    id 234
    label "op&#243;&#378;niacz"
  ]
  node [
    id 235
    label "mechanizm"
  ]
  node [
    id 236
    label "ogie&#324;"
  ]
  node [
    id 237
    label "kabina"
  ]
  node [
    id 238
    label "eskadra_bombowa"
  ]
  node [
    id 239
    label "silnik"
  ]
  node [
    id 240
    label "&#347;mig&#322;o"
  ]
  node [
    id 241
    label "samolot_bojowy"
  ]
  node [
    id 242
    label "dywizjon_bombowy"
  ]
  node [
    id 243
    label "podwozie"
  ]
  node [
    id 244
    label "kolejny"
  ]
  node [
    id 245
    label "niedawno"
  ]
  node [
    id 246
    label "poprzedni"
  ]
  node [
    id 247
    label "pozosta&#322;y"
  ]
  node [
    id 248
    label "ostatnio"
  ]
  node [
    id 249
    label "sko&#324;czony"
  ]
  node [
    id 250
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 251
    label "aktualny"
  ]
  node [
    id 252
    label "najgorszy"
  ]
  node [
    id 253
    label "w&#261;tpliwy"
  ]
  node [
    id 254
    label "nast&#281;pnie"
  ]
  node [
    id 255
    label "inny"
  ]
  node [
    id 256
    label "nastopny"
  ]
  node [
    id 257
    label "kolejno"
  ]
  node [
    id 258
    label "kt&#243;ry&#347;"
  ]
  node [
    id 259
    label "przesz&#322;y"
  ]
  node [
    id 260
    label "wcze&#347;niejszy"
  ]
  node [
    id 261
    label "poprzednio"
  ]
  node [
    id 262
    label "w&#261;tpliwie"
  ]
  node [
    id 263
    label "pozorny"
  ]
  node [
    id 264
    label "&#380;ywy"
  ]
  node [
    id 265
    label "ostateczny"
  ]
  node [
    id 266
    label "wa&#380;ny"
  ]
  node [
    id 267
    label "asymilowanie"
  ]
  node [
    id 268
    label "wapniak"
  ]
  node [
    id 269
    label "asymilowa&#263;"
  ]
  node [
    id 270
    label "os&#322;abia&#263;"
  ]
  node [
    id 271
    label "hominid"
  ]
  node [
    id 272
    label "podw&#322;adny"
  ]
  node [
    id 273
    label "os&#322;abianie"
  ]
  node [
    id 274
    label "dwun&#243;g"
  ]
  node [
    id 275
    label "nasada"
  ]
  node [
    id 276
    label "wz&#243;r"
  ]
  node [
    id 277
    label "senior"
  ]
  node [
    id 278
    label "Adam"
  ]
  node [
    id 279
    label "polifag"
  ]
  node [
    id 280
    label "wykszta&#322;cony"
  ]
  node [
    id 281
    label "dyplomowany"
  ]
  node [
    id 282
    label "wykwalifikowany"
  ]
  node [
    id 283
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 284
    label "kompletny"
  ]
  node [
    id 285
    label "sko&#324;czenie"
  ]
  node [
    id 286
    label "okre&#347;lony"
  ]
  node [
    id 287
    label "wielki"
  ]
  node [
    id 288
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 289
    label "aktualnie"
  ]
  node [
    id 290
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 291
    label "aktualizowanie"
  ]
  node [
    id 292
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 293
    label "uaktualnienie"
  ]
  node [
    id 294
    label "znak_pisarski"
  ]
  node [
    id 295
    label "inicja&#322;"
  ]
  node [
    id 296
    label "zapis"
  ]
  node [
    id 297
    label "figure"
  ]
  node [
    id 298
    label "typ"
  ]
  node [
    id 299
    label "spos&#243;b"
  ]
  node [
    id 300
    label "mildew"
  ]
  node [
    id 301
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 302
    label "ideal"
  ]
  node [
    id 303
    label "rule"
  ]
  node [
    id 304
    label "ruch"
  ]
  node [
    id 305
    label "dekal"
  ]
  node [
    id 306
    label "motyw"
  ]
  node [
    id 307
    label "projekt"
  ]
  node [
    id 308
    label "cyfrowa&#263;"
  ]
  node [
    id 309
    label "cyfrowanie"
  ]
  node [
    id 310
    label "litera"
  ]
  node [
    id 311
    label "ilustracja"
  ]
  node [
    id 312
    label "kategoria"
  ]
  node [
    id 313
    label "pierwiastek"
  ]
  node [
    id 314
    label "rozmiar"
  ]
  node [
    id 315
    label "number"
  ]
  node [
    id 316
    label "grupa"
  ]
  node [
    id 317
    label "kwadrat_magiczny"
  ]
  node [
    id 318
    label "wyra&#380;enie"
  ]
  node [
    id 319
    label "type"
  ]
  node [
    id 320
    label "klasa"
  ]
  node [
    id 321
    label "odm&#322;adzanie"
  ]
  node [
    id 322
    label "liga"
  ]
  node [
    id 323
    label "jednostka_systematyczna"
  ]
  node [
    id 324
    label "gromada"
  ]
  node [
    id 325
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 326
    label "egzemplarz"
  ]
  node [
    id 327
    label "Entuzjastki"
  ]
  node [
    id 328
    label "kompozycja"
  ]
  node [
    id 329
    label "Terranie"
  ]
  node [
    id 330
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 331
    label "category"
  ]
  node [
    id 332
    label "pakiet_klimatyczny"
  ]
  node [
    id 333
    label "oddzia&#322;"
  ]
  node [
    id 334
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 335
    label "cz&#261;steczka"
  ]
  node [
    id 336
    label "stage_set"
  ]
  node [
    id 337
    label "specgrupa"
  ]
  node [
    id 338
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 339
    label "&#346;wietliki"
  ]
  node [
    id 340
    label "odm&#322;odzenie"
  ]
  node [
    id 341
    label "Eurogrupa"
  ]
  node [
    id 342
    label "odm&#322;adza&#263;"
  ]
  node [
    id 343
    label "formacja_geologiczna"
  ]
  node [
    id 344
    label "harcerze_starsi"
  ]
  node [
    id 345
    label "charakterystyka"
  ]
  node [
    id 346
    label "m&#322;ot"
  ]
  node [
    id 347
    label "znak"
  ]
  node [
    id 348
    label "drzewo"
  ]
  node [
    id 349
    label "pr&#243;ba"
  ]
  node [
    id 350
    label "attribute"
  ]
  node [
    id 351
    label "marka"
  ]
  node [
    id 352
    label "warunek_lokalowy"
  ]
  node [
    id 353
    label "circumference"
  ]
  node [
    id 354
    label "odzie&#380;"
  ]
  node [
    id 355
    label "ilo&#347;&#263;"
  ]
  node [
    id 356
    label "znaczenie"
  ]
  node [
    id 357
    label "dymensja"
  ]
  node [
    id 358
    label "fleksja"
  ]
  node [
    id 359
    label "coupling"
  ]
  node [
    id 360
    label "tryb"
  ]
  node [
    id 361
    label "czas"
  ]
  node [
    id 362
    label "czasownik"
  ]
  node [
    id 363
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 364
    label "orz&#281;sek"
  ]
  node [
    id 365
    label "leksem"
  ]
  node [
    id 366
    label "sformu&#322;owanie"
  ]
  node [
    id 367
    label "zdarzenie_si&#281;"
  ]
  node [
    id 368
    label "poinformowanie"
  ]
  node [
    id 369
    label "wording"
  ]
  node [
    id 370
    label "oznaczenie"
  ]
  node [
    id 371
    label "znak_j&#281;zykowy"
  ]
  node [
    id 372
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 373
    label "ozdobnik"
  ]
  node [
    id 374
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 375
    label "grupa_imienna"
  ]
  node [
    id 376
    label "jednostka_leksykalna"
  ]
  node [
    id 377
    label "term"
  ]
  node [
    id 378
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 379
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 380
    label "ujawnienie"
  ]
  node [
    id 381
    label "affirmation"
  ]
  node [
    id 382
    label "zapisanie"
  ]
  node [
    id 383
    label "rzucenie"
  ]
  node [
    id 384
    label "substancja_chemiczna"
  ]
  node [
    id 385
    label "morfem"
  ]
  node [
    id 386
    label "sk&#322;adnik"
  ]
  node [
    id 387
    label "root"
  ]
  node [
    id 388
    label "warto&#347;&#263;"
  ]
  node [
    id 389
    label "rewaluowa&#263;"
  ]
  node [
    id 390
    label "zrewaluowa&#263;"
  ]
  node [
    id 391
    label "rewaluowanie"
  ]
  node [
    id 392
    label "znak_matematyczny"
  ]
  node [
    id 393
    label "korzy&#347;&#263;"
  ]
  node [
    id 394
    label "stopie&#324;"
  ]
  node [
    id 395
    label "zrewaluowanie"
  ]
  node [
    id 396
    label "dodawanie"
  ]
  node [
    id 397
    label "ocena"
  ]
  node [
    id 398
    label "wabik"
  ]
  node [
    id 399
    label "strona"
  ]
  node [
    id 400
    label "pogl&#261;d"
  ]
  node [
    id 401
    label "decyzja"
  ]
  node [
    id 402
    label "sofcik"
  ]
  node [
    id 403
    label "kryterium"
  ]
  node [
    id 404
    label "informacja"
  ]
  node [
    id 405
    label "appraisal"
  ]
  node [
    id 406
    label "kszta&#322;t"
  ]
  node [
    id 407
    label "podstopie&#324;"
  ]
  node [
    id 408
    label "wielko&#347;&#263;"
  ]
  node [
    id 409
    label "rank"
  ]
  node [
    id 410
    label "minuta"
  ]
  node [
    id 411
    label "d&#378;wi&#281;k"
  ]
  node [
    id 412
    label "wschodek"
  ]
  node [
    id 413
    label "przymiotnik"
  ]
  node [
    id 414
    label "gama"
  ]
  node [
    id 415
    label "jednostka"
  ]
  node [
    id 416
    label "podzia&#322;"
  ]
  node [
    id 417
    label "element"
  ]
  node [
    id 418
    label "schody"
  ]
  node [
    id 419
    label "poziom"
  ]
  node [
    id 420
    label "przys&#322;&#243;wek"
  ]
  node [
    id 421
    label "degree"
  ]
  node [
    id 422
    label "szczebel"
  ]
  node [
    id 423
    label "podn&#243;&#380;ek"
  ]
  node [
    id 424
    label "zaleta"
  ]
  node [
    id 425
    label "dobro"
  ]
  node [
    id 426
    label "kartka"
  ]
  node [
    id 427
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 428
    label "logowanie"
  ]
  node [
    id 429
    label "plik"
  ]
  node [
    id 430
    label "s&#261;d"
  ]
  node [
    id 431
    label "adres_internetowy"
  ]
  node [
    id 432
    label "linia"
  ]
  node [
    id 433
    label "serwis_internetowy"
  ]
  node [
    id 434
    label "bok"
  ]
  node [
    id 435
    label "skr&#281;canie"
  ]
  node [
    id 436
    label "skr&#281;ca&#263;"
  ]
  node [
    id 437
    label "orientowanie"
  ]
  node [
    id 438
    label "skr&#281;ci&#263;"
  ]
  node [
    id 439
    label "uj&#281;cie"
  ]
  node [
    id 440
    label "ty&#322;"
  ]
  node [
    id 441
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 442
    label "layout"
  ]
  node [
    id 443
    label "obiekt"
  ]
  node [
    id 444
    label "zorientowa&#263;"
  ]
  node [
    id 445
    label "pagina"
  ]
  node [
    id 446
    label "podmiot"
  ]
  node [
    id 447
    label "g&#243;ra"
  ]
  node [
    id 448
    label "orientowa&#263;"
  ]
  node [
    id 449
    label "voice"
  ]
  node [
    id 450
    label "prz&#243;d"
  ]
  node [
    id 451
    label "internet"
  ]
  node [
    id 452
    label "powierzchnia"
  ]
  node [
    id 453
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 454
    label "skr&#281;cenie"
  ]
  node [
    id 455
    label "zmienna"
  ]
  node [
    id 456
    label "wskazywanie"
  ]
  node [
    id 457
    label "cel"
  ]
  node [
    id 458
    label "wskazywa&#263;"
  ]
  node [
    id 459
    label "worth"
  ]
  node [
    id 460
    label "do&#322;&#261;czanie"
  ]
  node [
    id 461
    label "addition"
  ]
  node [
    id 462
    label "liczenie"
  ]
  node [
    id 463
    label "dop&#322;acanie"
  ]
  node [
    id 464
    label "summation"
  ]
  node [
    id 465
    label "rozdzielno&#347;&#263;_mno&#380;enia_wzgl&#281;dem_dodawania"
  ]
  node [
    id 466
    label "uzupe&#322;nianie"
  ]
  node [
    id 467
    label "dokupowanie"
  ]
  node [
    id 468
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 469
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 470
    label "do&#347;wietlanie"
  ]
  node [
    id 471
    label "suma"
  ]
  node [
    id 472
    label "wspominanie"
  ]
  node [
    id 473
    label "podniesienie"
  ]
  node [
    id 474
    label "warto&#347;ciowy"
  ]
  node [
    id 475
    label "przewarto&#347;ciowanie"
  ]
  node [
    id 476
    label "czynnik"
  ]
  node [
    id 477
    label "magnes"
  ]
  node [
    id 478
    label "appreciate"
  ]
  node [
    id 479
    label "podnosi&#263;"
  ]
  node [
    id 480
    label "podnie&#347;&#263;"
  ]
  node [
    id 481
    label "przewarto&#347;ciowywanie"
  ]
  node [
    id 482
    label "podnoszenie"
  ]
  node [
    id 483
    label "za&#322;o&#380;enie"
  ]
  node [
    id 484
    label "nieprawdziwy"
  ]
  node [
    id 485
    label "prawdziwy"
  ]
  node [
    id 486
    label "truth"
  ]
  node [
    id 487
    label "realia"
  ]
  node [
    id 488
    label "podwini&#281;cie"
  ]
  node [
    id 489
    label "zap&#322;acenie"
  ]
  node [
    id 490
    label "przyodzianie"
  ]
  node [
    id 491
    label "budowla"
  ]
  node [
    id 492
    label "pokrycie"
  ]
  node [
    id 493
    label "rozebranie"
  ]
  node [
    id 494
    label "zak&#322;adka"
  ]
  node [
    id 495
    label "struktura"
  ]
  node [
    id 496
    label "poubieranie"
  ]
  node [
    id 497
    label "infliction"
  ]
  node [
    id 498
    label "spowodowanie"
  ]
  node [
    id 499
    label "pozak&#322;adanie"
  ]
  node [
    id 500
    label "program"
  ]
  node [
    id 501
    label "przebranie"
  ]
  node [
    id 502
    label "przywdzianie"
  ]
  node [
    id 503
    label "obleczenie_si&#281;"
  ]
  node [
    id 504
    label "utworzenie"
  ]
  node [
    id 505
    label "str&#243;j"
  ]
  node [
    id 506
    label "twierdzenie"
  ]
  node [
    id 507
    label "obleczenie"
  ]
  node [
    id 508
    label "umieszczenie"
  ]
  node [
    id 509
    label "czynno&#347;&#263;"
  ]
  node [
    id 510
    label "przygotowywanie"
  ]
  node [
    id 511
    label "przymierzenie"
  ]
  node [
    id 512
    label "wyko&#324;czenie"
  ]
  node [
    id 513
    label "point"
  ]
  node [
    id 514
    label "przygotowanie"
  ]
  node [
    id 515
    label "proposition"
  ]
  node [
    id 516
    label "przewidzenie"
  ]
  node [
    id 517
    label "zrobienie"
  ]
  node [
    id 518
    label "zesp&#243;&#322;"
  ]
  node [
    id 519
    label "podejrzany"
  ]
  node [
    id 520
    label "s&#261;downictwo"
  ]
  node [
    id 521
    label "system"
  ]
  node [
    id 522
    label "biuro"
  ]
  node [
    id 523
    label "court"
  ]
  node [
    id 524
    label "forum"
  ]
  node [
    id 525
    label "bronienie"
  ]
  node [
    id 526
    label "urz&#261;d"
  ]
  node [
    id 527
    label "wydarzenie"
  ]
  node [
    id 528
    label "oskar&#380;yciel"
  ]
  node [
    id 529
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 530
    label "skazany"
  ]
  node [
    id 531
    label "post&#281;powanie"
  ]
  node [
    id 532
    label "broni&#263;"
  ]
  node [
    id 533
    label "my&#347;l"
  ]
  node [
    id 534
    label "pods&#261;dny"
  ]
  node [
    id 535
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 536
    label "obrona"
  ]
  node [
    id 537
    label "wypowied&#378;"
  ]
  node [
    id 538
    label "instytucja"
  ]
  node [
    id 539
    label "antylogizm"
  ]
  node [
    id 540
    label "konektyw"
  ]
  node [
    id 541
    label "&#347;wiadek"
  ]
  node [
    id 542
    label "procesowicz"
  ]
  node [
    id 543
    label "&#380;ywny"
  ]
  node [
    id 544
    label "szczery"
  ]
  node [
    id 545
    label "naturalny"
  ]
  node [
    id 546
    label "naprawd&#281;"
  ]
  node [
    id 547
    label "realnie"
  ]
  node [
    id 548
    label "podobny"
  ]
  node [
    id 549
    label "zgodny"
  ]
  node [
    id 550
    label "m&#261;dry"
  ]
  node [
    id 551
    label "prawdziwie"
  ]
  node [
    id 552
    label "nieprawdziwie"
  ]
  node [
    id 553
    label "niezgodny"
  ]
  node [
    id 554
    label "nierzeczywi&#347;cie"
  ]
  node [
    id 555
    label "udawany"
  ]
  node [
    id 556
    label "nieszczery"
  ]
  node [
    id 557
    label "niehistoryczny"
  ]
  node [
    id 558
    label "discover"
  ]
  node [
    id 559
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 560
    label "wydoby&#263;"
  ]
  node [
    id 561
    label "okre&#347;li&#263;"
  ]
  node [
    id 562
    label "poda&#263;"
  ]
  node [
    id 563
    label "express"
  ]
  node [
    id 564
    label "wyrazi&#263;"
  ]
  node [
    id 565
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 566
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 567
    label "rzekn&#261;&#263;"
  ]
  node [
    id 568
    label "unwrap"
  ]
  node [
    id 569
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 570
    label "convey"
  ]
  node [
    id 571
    label "tenis"
  ]
  node [
    id 572
    label "supply"
  ]
  node [
    id 573
    label "da&#263;"
  ]
  node [
    id 574
    label "ustawi&#263;"
  ]
  node [
    id 575
    label "siatk&#243;wka"
  ]
  node [
    id 576
    label "give"
  ]
  node [
    id 577
    label "zagra&#263;"
  ]
  node [
    id 578
    label "jedzenie"
  ]
  node [
    id 579
    label "poinformowa&#263;"
  ]
  node [
    id 580
    label "introduce"
  ]
  node [
    id 581
    label "nafaszerowa&#263;"
  ]
  node [
    id 582
    label "zaserwowa&#263;"
  ]
  node [
    id 583
    label "draw"
  ]
  node [
    id 584
    label "doby&#263;"
  ]
  node [
    id 585
    label "g&#243;rnictwo"
  ]
  node [
    id 586
    label "wyeksploatowa&#263;"
  ]
  node [
    id 587
    label "extract"
  ]
  node [
    id 588
    label "obtain"
  ]
  node [
    id 589
    label "wyj&#261;&#263;"
  ]
  node [
    id 590
    label "ocali&#263;"
  ]
  node [
    id 591
    label "uzyska&#263;"
  ]
  node [
    id 592
    label "wyda&#263;"
  ]
  node [
    id 593
    label "wydosta&#263;"
  ]
  node [
    id 594
    label "uwydatni&#263;"
  ]
  node [
    id 595
    label "distill"
  ]
  node [
    id 596
    label "raise"
  ]
  node [
    id 597
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 598
    label "testify"
  ]
  node [
    id 599
    label "zakomunikowa&#263;"
  ]
  node [
    id 600
    label "oznaczy&#263;"
  ]
  node [
    id 601
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 602
    label "vent"
  ]
  node [
    id 603
    label "zdecydowa&#263;"
  ]
  node [
    id 604
    label "zrobi&#263;"
  ]
  node [
    id 605
    label "spowodowa&#263;"
  ]
  node [
    id 606
    label "situate"
  ]
  node [
    id 607
    label "nominate"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
]
