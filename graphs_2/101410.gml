graph [
  node [
    id 0
    label "efekt"
    origin "text"
  ]
  node [
    id 1
    label "dochodowy"
    origin "text"
  ]
  node [
    id 2
    label "wra&#380;enie"
  ]
  node [
    id 3
    label "&#347;rodek"
  ]
  node [
    id 4
    label "impression"
  ]
  node [
    id 5
    label "dzia&#322;anie"
  ]
  node [
    id 6
    label "typ"
  ]
  node [
    id 7
    label "robienie_wra&#380;enia"
  ]
  node [
    id 8
    label "zrobienie_wra&#380;enia"
  ]
  node [
    id 9
    label "zrobi&#263;_wra&#380;enie"
  ]
  node [
    id 10
    label "robi&#263;_wra&#380;enie"
  ]
  node [
    id 11
    label "rezultat"
  ]
  node [
    id 12
    label "event"
  ]
  node [
    id 13
    label "przyczyna"
  ]
  node [
    id 14
    label "punkt"
  ]
  node [
    id 15
    label "spos&#243;b"
  ]
  node [
    id 16
    label "miejsce"
  ]
  node [
    id 17
    label "abstrakcja"
  ]
  node [
    id 18
    label "czas"
  ]
  node [
    id 19
    label "chemikalia"
  ]
  node [
    id 20
    label "substancja"
  ]
  node [
    id 21
    label "odczucia"
  ]
  node [
    id 22
    label "proces"
  ]
  node [
    id 23
    label "zmys&#322;"
  ]
  node [
    id 24
    label "przeczulica"
  ]
  node [
    id 25
    label "zjawisko"
  ]
  node [
    id 26
    label "czucie"
  ]
  node [
    id 27
    label "poczucie"
  ]
  node [
    id 28
    label "reakcja"
  ]
  node [
    id 29
    label "infimum"
  ]
  node [
    id 30
    label "powodowanie"
  ]
  node [
    id 31
    label "liczenie"
  ]
  node [
    id 32
    label "cz&#322;owiek"
  ]
  node [
    id 33
    label "skutek"
  ]
  node [
    id 34
    label "podzia&#322;anie"
  ]
  node [
    id 35
    label "supremum"
  ]
  node [
    id 36
    label "kampania"
  ]
  node [
    id 37
    label "uruchamianie"
  ]
  node [
    id 38
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 39
    label "operacja"
  ]
  node [
    id 40
    label "jednostka"
  ]
  node [
    id 41
    label "hipnotyzowanie"
  ]
  node [
    id 42
    label "robienie"
  ]
  node [
    id 43
    label "uruchomienie"
  ]
  node [
    id 44
    label "nakr&#281;canie"
  ]
  node [
    id 45
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 46
    label "matematyka"
  ]
  node [
    id 47
    label "reakcja_chemiczna"
  ]
  node [
    id 48
    label "tr&#243;jstronny"
  ]
  node [
    id 49
    label "natural_process"
  ]
  node [
    id 50
    label "nakr&#281;cenie"
  ]
  node [
    id 51
    label "zatrzymanie"
  ]
  node [
    id 52
    label "wp&#322;yw"
  ]
  node [
    id 53
    label "rzut"
  ]
  node [
    id 54
    label "podtrzymywanie"
  ]
  node [
    id 55
    label "w&#322;&#261;czanie"
  ]
  node [
    id 56
    label "liczy&#263;"
  ]
  node [
    id 57
    label "operation"
  ]
  node [
    id 58
    label "czynno&#347;&#263;"
  ]
  node [
    id 59
    label "dzianie_si&#281;"
  ]
  node [
    id 60
    label "zadzia&#322;anie"
  ]
  node [
    id 61
    label "priorytet"
  ]
  node [
    id 62
    label "bycie"
  ]
  node [
    id 63
    label "kres"
  ]
  node [
    id 64
    label "rozpocz&#281;cie"
  ]
  node [
    id 65
    label "docieranie"
  ]
  node [
    id 66
    label "funkcja"
  ]
  node [
    id 67
    label "czynny"
  ]
  node [
    id 68
    label "impact"
  ]
  node [
    id 69
    label "oferta"
  ]
  node [
    id 70
    label "zako&#324;czenie"
  ]
  node [
    id 71
    label "act"
  ]
  node [
    id 72
    label "wdzieranie_si&#281;"
  ]
  node [
    id 73
    label "w&#322;&#261;czenie"
  ]
  node [
    id 74
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 75
    label "subject"
  ]
  node [
    id 76
    label "czynnik"
  ]
  node [
    id 77
    label "matuszka"
  ]
  node [
    id 78
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 79
    label "geneza"
  ]
  node [
    id 80
    label "poci&#261;ganie"
  ]
  node [
    id 81
    label "facet"
  ]
  node [
    id 82
    label "jednostka_systematyczna"
  ]
  node [
    id 83
    label "kr&#243;lestwo"
  ]
  node [
    id 84
    label "autorament"
  ]
  node [
    id 85
    label "variety"
  ]
  node [
    id 86
    label "antycypacja"
  ]
  node [
    id 87
    label "przypuszczenie"
  ]
  node [
    id 88
    label "cynk"
  ]
  node [
    id 89
    label "obstawia&#263;"
  ]
  node [
    id 90
    label "gromada"
  ]
  node [
    id 91
    label "sztuka"
  ]
  node [
    id 92
    label "design"
  ]
  node [
    id 93
    label "wydarzenie"
  ]
  node [
    id 94
    label "dochodowo"
  ]
  node [
    id 95
    label "korzystny"
  ]
  node [
    id 96
    label "finansowo"
  ]
  node [
    id 97
    label "korzystnie"
  ]
  node [
    id 98
    label "financially"
  ]
  node [
    id 99
    label "fiscally"
  ]
  node [
    id 100
    label "dobry"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
]
