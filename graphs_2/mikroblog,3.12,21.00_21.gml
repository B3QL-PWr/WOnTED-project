graph [
  node [
    id 0
    label "zagadka"
    origin "text"
  ]
  node [
    id 1
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 2
    label "enigmat"
  ]
  node [
    id 3
    label "mystery"
  ]
  node [
    id 4
    label "&#322;amig&#322;&#243;wka"
  ]
  node [
    id 5
    label "po&#322;udnica"
  ]
  node [
    id 6
    label "rzecz"
  ]
  node [
    id 7
    label "taj&#324;"
  ]
  node [
    id 8
    label "object"
  ]
  node [
    id 9
    label "przedmiot"
  ]
  node [
    id 10
    label "temat"
  ]
  node [
    id 11
    label "wpadni&#281;cie"
  ]
  node [
    id 12
    label "mienie"
  ]
  node [
    id 13
    label "przyroda"
  ]
  node [
    id 14
    label "istota"
  ]
  node [
    id 15
    label "obiekt"
  ]
  node [
    id 16
    label "kultura"
  ]
  node [
    id 17
    label "wpa&#347;&#263;"
  ]
  node [
    id 18
    label "wpadanie"
  ]
  node [
    id 19
    label "wpada&#263;"
  ]
  node [
    id 20
    label "rozrywka"
  ]
  node [
    id 21
    label "zadanie"
  ]
  node [
    id 22
    label "tajemnica"
  ]
  node [
    id 23
    label "riddle"
  ]
  node [
    id 24
    label "motyl_dzienny"
  ]
  node [
    id 25
    label "duch"
  ]
  node [
    id 26
    label "polewik"
  ]
  node [
    id 27
    label "istota_fantastyczna"
  ]
  node [
    id 28
    label "doba"
  ]
  node [
    id 29
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 30
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 31
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 32
    label "teraz"
  ]
  node [
    id 33
    label "czas"
  ]
  node [
    id 34
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 35
    label "jednocze&#347;nie"
  ]
  node [
    id 36
    label "tydzie&#324;"
  ]
  node [
    id 37
    label "noc"
  ]
  node [
    id 38
    label "dzie&#324;"
  ]
  node [
    id 39
    label "godzina"
  ]
  node [
    id 40
    label "long_time"
  ]
  node [
    id 41
    label "jednostka_geologiczna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
]
