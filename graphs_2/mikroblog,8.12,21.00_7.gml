graph [
  node [
    id 0
    label "koga"
    origin "text"
  ]
  node [
    id 1
    label "zawo&#322;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "lato"
    origin "text"
  ]
  node [
    id 3
    label "hulk"
  ]
  node [
    id 4
    label "&#380;aglowiec"
  ]
  node [
    id 5
    label "kasztel"
  ]
  node [
    id 6
    label "statek_handlowy"
  ]
  node [
    id 7
    label "nef"
  ]
  node [
    id 8
    label "piel&#281;gnicowate"
  ]
  node [
    id 9
    label "takielunek"
  ]
  node [
    id 10
    label "ko&#322;kownica"
  ]
  node [
    id 11
    label "statek"
  ]
  node [
    id 12
    label "ryba"
  ]
  node [
    id 13
    label "scalar"
  ]
  node [
    id 14
    label "sztagownik"
  ]
  node [
    id 15
    label "sejzing"
  ]
  node [
    id 16
    label "fluita"
  ]
  node [
    id 17
    label "nadbud&#243;wka"
  ]
  node [
    id 18
    label "zamek"
  ]
  node [
    id 19
    label "karaka"
  ]
  node [
    id 20
    label "jednomasztowiec"
  ]
  node [
    id 21
    label "ster_wios&#322;owy"
  ]
  node [
    id 22
    label "poszycie_zak&#322;adkowe"
  ]
  node [
    id 23
    label "frachtowiec"
  ]
  node [
    id 24
    label "poszycie_klepkowe"
  ]
  node [
    id 25
    label "port"
  ]
  node [
    id 26
    label "nakaza&#263;"
  ]
  node [
    id 27
    label "invite"
  ]
  node [
    id 28
    label "cry"
  ]
  node [
    id 29
    label "powiedzie&#263;"
  ]
  node [
    id 30
    label "krzykn&#261;&#263;"
  ]
  node [
    id 31
    label "przewo&#322;a&#263;"
  ]
  node [
    id 32
    label "wydoby&#263;"
  ]
  node [
    id 33
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 34
    label "shout"
  ]
  node [
    id 35
    label "discover"
  ]
  node [
    id 36
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 37
    label "poda&#263;"
  ]
  node [
    id 38
    label "okre&#347;li&#263;"
  ]
  node [
    id 39
    label "express"
  ]
  node [
    id 40
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 41
    label "wyrazi&#263;"
  ]
  node [
    id 42
    label "rzekn&#261;&#263;"
  ]
  node [
    id 43
    label "unwrap"
  ]
  node [
    id 44
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 45
    label "convey"
  ]
  node [
    id 46
    label "poleci&#263;"
  ]
  node [
    id 47
    label "order"
  ]
  node [
    id 48
    label "zapakowa&#263;"
  ]
  node [
    id 49
    label "wezwa&#263;"
  ]
  node [
    id 50
    label "zepsu&#263;"
  ]
  node [
    id 51
    label "przywo&#322;a&#263;"
  ]
  node [
    id 52
    label "wywo&#322;a&#263;"
  ]
  node [
    id 53
    label "pora_roku"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 2
    target 53
  ]
]
