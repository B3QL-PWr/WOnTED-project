graph [
  node [
    id 0
    label "pa&#322;ac"
    origin "text"
  ]
  node [
    id 1
    label "august"
    origin "text"
  ]
  node [
    id 2
    label "haertiga"
    origin "text"
  ]
  node [
    id 3
    label "rezydencja"
  ]
  node [
    id 4
    label "budynek"
  ]
  node [
    id 5
    label "Wersal"
  ]
  node [
    id 6
    label "Belweder"
  ]
  node [
    id 7
    label "w&#322;adza"
  ]
  node [
    id 8
    label "cz&#322;owiek"
  ]
  node [
    id 9
    label "struktura"
  ]
  node [
    id 10
    label "panowanie"
  ]
  node [
    id 11
    label "wydolno&#347;&#263;"
  ]
  node [
    id 12
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 13
    label "rz&#261;d"
  ]
  node [
    id 14
    label "prawo"
  ]
  node [
    id 15
    label "grupa"
  ]
  node [
    id 16
    label "rz&#261;dzenie"
  ]
  node [
    id 17
    label "Kreml"
  ]
  node [
    id 18
    label "kondygnacja"
  ]
  node [
    id 19
    label "skrzyd&#322;o"
  ]
  node [
    id 20
    label "klatka_schodowa"
  ]
  node [
    id 21
    label "front"
  ]
  node [
    id 22
    label "budowla"
  ]
  node [
    id 23
    label "przedpro&#380;e"
  ]
  node [
    id 24
    label "dach"
  ]
  node [
    id 25
    label "alkierz"
  ]
  node [
    id 26
    label "strop"
  ]
  node [
    id 27
    label "pod&#322;oga"
  ]
  node [
    id 28
    label "Pentagon"
  ]
  node [
    id 29
    label "balkon"
  ]
  node [
    id 30
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 31
    label "siedziba"
  ]
  node [
    id 32
    label "dom"
  ]
  node [
    id 33
    label "tytu&#322;"
  ]
  node [
    id 34
    label "druk"
  ]
  node [
    id 35
    label "mianowaniec"
  ]
  node [
    id 36
    label "podtytu&#322;"
  ]
  node [
    id 37
    label "poster"
  ]
  node [
    id 38
    label "publikacja"
  ]
  node [
    id 39
    label "nadtytu&#322;"
  ]
  node [
    id 40
    label "redaktor"
  ]
  node [
    id 41
    label "nazwa"
  ]
  node [
    id 42
    label "szata_graficzna"
  ]
  node [
    id 43
    label "debit"
  ]
  node [
    id 44
    label "tytulatura"
  ]
  node [
    id 45
    label "wyda&#263;"
  ]
  node [
    id 46
    label "elevation"
  ]
  node [
    id 47
    label "wydawa&#263;"
  ]
  node [
    id 48
    label "Haertiga"
  ]
  node [
    id 49
    label "Franciszka"
  ]
  node [
    id 50
    label "che&#322;mi&#324;ski"
  ]
  node [
    id 51
    label "towarzystwo"
  ]
  node [
    id 52
    label "ubezpieczenie"
  ]
  node [
    id 53
    label "i"
  ]
  node [
    id 54
    label "reasekuracja"
  ]
  node [
    id 55
    label "warta&#263;"
  ]
  node [
    id 56
    label "zabytek"
  ]
  node [
    id 57
    label "zadba&#263;"
  ]
  node [
    id 58
    label "krajowy"
  ]
  node [
    id 59
    label "o&#347;rodek"
  ]
  node [
    id 60
    label "badanie"
  ]
  node [
    id 61
    label "dokumentacja"
  ]
  node [
    id 62
    label "fundacja"
  ]
  node [
    id 63
    label "ulica"
  ]
  node [
    id 64
    label "piotrkowski"
  ]
  node [
    id 65
    label "dobry"
  ]
  node [
    id 66
    label "wn&#281;trze"
  ]
  node [
    id 67
    label "rok"
  ]
  node [
    id 68
    label "2005"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 58
  ]
  edge [
    source 53
    target 59
  ]
  edge [
    source 53
    target 60
  ]
  edge [
    source 53
    target 61
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 56
    target 59
  ]
  edge [
    source 56
    target 60
  ]
  edge [
    source 56
    target 61
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 58
    target 61
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 67
  ]
  edge [
    source 65
    target 68
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 68
  ]
  edge [
    source 67
    target 68
  ]
]
