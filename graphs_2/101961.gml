graph [
  node [
    id 0
    label "zach&#281;ci&#263;"
    origin "text"
  ]
  node [
    id 1
    label "prze&#322;omowy"
    origin "text"
  ]
  node [
    id 2
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 3
    label "dystrybutor"
    origin "text"
  ]
  node [
    id 4
    label "dvd"
    origin "text"
  ]
  node [
    id 5
    label "niekonwencjonalny"
    origin "text"
  ]
  node [
    id 6
    label "wreszcie"
    origin "text"
  ]
  node [
    id 7
    label "skuteczny"
    origin "text"
  ]
  node [
    id 8
    label "walka"
    origin "text"
  ]
  node [
    id 9
    label "piractwo"
    origin "text"
  ]
  node [
    id 10
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "w&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 14
    label "inny"
    origin "text"
  ]
  node [
    id 15
    label "posiadacz"
    origin "text"
  ]
  node [
    id 16
    label "prawy"
    origin "text"
  ]
  node [
    id 17
    label "autorski"
    origin "text"
  ]
  node [
    id 18
    label "&#347;lad"
    origin "text"
  ]
  node [
    id 19
    label "filip"
    origin "text"
  ]
  node [
    id 20
    label "rzuci&#263;"
    origin "text"
  ]
  node [
    id 21
    label "wszystek"
    origin "text"
  ]
  node [
    id 22
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 23
    label "invite"
  ]
  node [
    id 24
    label "pozyska&#263;"
  ]
  node [
    id 25
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 26
    label "stage"
  ]
  node [
    id 27
    label "uzyska&#263;"
  ]
  node [
    id 28
    label "wytworzy&#263;"
  ]
  node [
    id 29
    label "give_birth"
  ]
  node [
    id 30
    label "prze&#322;omowo"
  ]
  node [
    id 31
    label "donios&#322;y"
  ]
  node [
    id 32
    label "wa&#380;ny"
  ]
  node [
    id 33
    label "innowacyjny"
  ]
  node [
    id 34
    label "wynios&#322;y"
  ]
  node [
    id 35
    label "dono&#347;ny"
  ]
  node [
    id 36
    label "silny"
  ]
  node [
    id 37
    label "wa&#380;nie"
  ]
  node [
    id 38
    label "istotnie"
  ]
  node [
    id 39
    label "znaczny"
  ]
  node [
    id 40
    label "eksponowany"
  ]
  node [
    id 41
    label "dobry"
  ]
  node [
    id 42
    label "nowatorski"
  ]
  node [
    id 43
    label "innowacyjnie"
  ]
  node [
    id 44
    label "g&#322;o&#347;ny"
  ]
  node [
    id 45
    label "dono&#347;nie"
  ]
  node [
    id 46
    label "donio&#347;le"
  ]
  node [
    id 47
    label "gromowy"
  ]
  node [
    id 48
    label "nowatorsko"
  ]
  node [
    id 49
    label "wytw&#243;r"
  ]
  node [
    id 50
    label "pocz&#261;tki"
  ]
  node [
    id 51
    label "ukra&#347;&#263;"
  ]
  node [
    id 52
    label "ukradzenie"
  ]
  node [
    id 53
    label "idea"
  ]
  node [
    id 54
    label "system"
  ]
  node [
    id 55
    label "przedmiot"
  ]
  node [
    id 56
    label "p&#322;&#243;d"
  ]
  node [
    id 57
    label "work"
  ]
  node [
    id 58
    label "rezultat"
  ]
  node [
    id 59
    label "strategia"
  ]
  node [
    id 60
    label "background"
  ]
  node [
    id 61
    label "dzieci&#281;ctwo"
  ]
  node [
    id 62
    label "podpierdoli&#263;"
  ]
  node [
    id 63
    label "dash_off"
  ]
  node [
    id 64
    label "zw&#281;dzi&#263;"
  ]
  node [
    id 65
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 66
    label "zabra&#263;"
  ]
  node [
    id 67
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 68
    label "overcharge"
  ]
  node [
    id 69
    label "podpierdolenie"
  ]
  node [
    id 70
    label "zgini&#281;cie"
  ]
  node [
    id 71
    label "przyw&#322;aszczenie"
  ]
  node [
    id 72
    label "larceny"
  ]
  node [
    id 73
    label "zaczerpni&#281;cie"
  ]
  node [
    id 74
    label "zw&#281;dzenie"
  ]
  node [
    id 75
    label "okradzenie"
  ]
  node [
    id 76
    label "nakradzenie"
  ]
  node [
    id 77
    label "ideologia"
  ]
  node [
    id 78
    label "byt"
  ]
  node [
    id 79
    label "intelekt"
  ]
  node [
    id 80
    label "Kant"
  ]
  node [
    id 81
    label "cel"
  ]
  node [
    id 82
    label "poj&#281;cie"
  ]
  node [
    id 83
    label "istota"
  ]
  node [
    id 84
    label "ideacja"
  ]
  node [
    id 85
    label "j&#261;dro"
  ]
  node [
    id 86
    label "systemik"
  ]
  node [
    id 87
    label "rozprz&#261;c"
  ]
  node [
    id 88
    label "oprogramowanie"
  ]
  node [
    id 89
    label "systemat"
  ]
  node [
    id 90
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 91
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 92
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 93
    label "model"
  ]
  node [
    id 94
    label "struktura"
  ]
  node [
    id 95
    label "usenet"
  ]
  node [
    id 96
    label "s&#261;d"
  ]
  node [
    id 97
    label "zbi&#243;r"
  ]
  node [
    id 98
    label "porz&#261;dek"
  ]
  node [
    id 99
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 100
    label "przyn&#281;ta"
  ]
  node [
    id 101
    label "net"
  ]
  node [
    id 102
    label "w&#281;dkarstwo"
  ]
  node [
    id 103
    label "eratem"
  ]
  node [
    id 104
    label "oddzia&#322;"
  ]
  node [
    id 105
    label "doktryna"
  ]
  node [
    id 106
    label "pulpit"
  ]
  node [
    id 107
    label "konstelacja"
  ]
  node [
    id 108
    label "jednostka_geologiczna"
  ]
  node [
    id 109
    label "o&#347;"
  ]
  node [
    id 110
    label "podsystem"
  ]
  node [
    id 111
    label "metoda"
  ]
  node [
    id 112
    label "ryba"
  ]
  node [
    id 113
    label "Leopard"
  ]
  node [
    id 114
    label "spos&#243;b"
  ]
  node [
    id 115
    label "Android"
  ]
  node [
    id 116
    label "zachowanie"
  ]
  node [
    id 117
    label "cybernetyk"
  ]
  node [
    id 118
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 119
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 120
    label "method"
  ]
  node [
    id 121
    label "sk&#322;ad"
  ]
  node [
    id 122
    label "podstawa"
  ]
  node [
    id 123
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 124
    label "dozownik"
  ]
  node [
    id 125
    label "po&#347;rednik"
  ]
  node [
    id 126
    label "urz&#261;dzenie"
  ]
  node [
    id 127
    label "dostawca"
  ]
  node [
    id 128
    label "przyrz&#261;d_mechaniczny"
  ]
  node [
    id 129
    label "kom&#243;rka"
  ]
  node [
    id 130
    label "furnishing"
  ]
  node [
    id 131
    label "zabezpieczenie"
  ]
  node [
    id 132
    label "zrobienie"
  ]
  node [
    id 133
    label "wyrz&#261;dzenie"
  ]
  node [
    id 134
    label "zagospodarowanie"
  ]
  node [
    id 135
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 136
    label "ig&#322;a"
  ]
  node [
    id 137
    label "narz&#281;dzie"
  ]
  node [
    id 138
    label "wirnik"
  ]
  node [
    id 139
    label "aparatura"
  ]
  node [
    id 140
    label "system_energetyczny"
  ]
  node [
    id 141
    label "impulsator"
  ]
  node [
    id 142
    label "mechanizm"
  ]
  node [
    id 143
    label "sprz&#281;t"
  ]
  node [
    id 144
    label "czynno&#347;&#263;"
  ]
  node [
    id 145
    label "blokowanie"
  ]
  node [
    id 146
    label "set"
  ]
  node [
    id 147
    label "zablokowanie"
  ]
  node [
    id 148
    label "przygotowanie"
  ]
  node [
    id 149
    label "komora"
  ]
  node [
    id 150
    label "j&#281;zyk"
  ]
  node [
    id 151
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 152
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 153
    label "transportowiec"
  ]
  node [
    id 154
    label "podmiot_gospodarczy"
  ]
  node [
    id 155
    label "us&#322;ugodawca"
  ]
  node [
    id 156
    label "handlowiec"
  ]
  node [
    id 157
    label "firma"
  ]
  node [
    id 158
    label "pomocnik"
  ]
  node [
    id 159
    label "faktor"
  ]
  node [
    id 160
    label "pomoc"
  ]
  node [
    id 161
    label "psychopomp"
  ]
  node [
    id 162
    label "us&#322;ugowiec"
  ]
  node [
    id 163
    label "niekonwencjonalnie"
  ]
  node [
    id 164
    label "bezpretensjonalny"
  ]
  node [
    id 165
    label "niestandardowy"
  ]
  node [
    id 166
    label "oryginalny"
  ]
  node [
    id 167
    label "niepospolity"
  ]
  node [
    id 168
    label "niestandardowo"
  ]
  node [
    id 169
    label "nietypowy"
  ]
  node [
    id 170
    label "niepospolicie"
  ]
  node [
    id 171
    label "rzadki"
  ]
  node [
    id 172
    label "bezpretensjonalnie"
  ]
  node [
    id 173
    label "naturalny"
  ]
  node [
    id 174
    label "prosty"
  ]
  node [
    id 175
    label "niespotykany"
  ]
  node [
    id 176
    label "o&#380;ywczy"
  ]
  node [
    id 177
    label "ekscentryczny"
  ]
  node [
    id 178
    label "nowy"
  ]
  node [
    id 179
    label "oryginalnie"
  ]
  node [
    id 180
    label "pierwotny"
  ]
  node [
    id 181
    label "prawdziwy"
  ]
  node [
    id 182
    label "warto&#347;ciowy"
  ]
  node [
    id 183
    label "w&#380;dy"
  ]
  node [
    id 184
    label "poskutkowanie"
  ]
  node [
    id 185
    label "sprawny"
  ]
  node [
    id 186
    label "skutecznie"
  ]
  node [
    id 187
    label "skutkowanie"
  ]
  node [
    id 188
    label "szybki"
  ]
  node [
    id 189
    label "letki"
  ]
  node [
    id 190
    label "umiej&#281;tny"
  ]
  node [
    id 191
    label "zdrowy"
  ]
  node [
    id 192
    label "dzia&#322;alny"
  ]
  node [
    id 193
    label "sprawnie"
  ]
  node [
    id 194
    label "dobroczynny"
  ]
  node [
    id 195
    label "czw&#243;rka"
  ]
  node [
    id 196
    label "spokojny"
  ]
  node [
    id 197
    label "&#347;mieszny"
  ]
  node [
    id 198
    label "mi&#322;y"
  ]
  node [
    id 199
    label "grzeczny"
  ]
  node [
    id 200
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 201
    label "powitanie"
  ]
  node [
    id 202
    label "dobrze"
  ]
  node [
    id 203
    label "ca&#322;y"
  ]
  node [
    id 204
    label "zwrot"
  ]
  node [
    id 205
    label "pomy&#347;lny"
  ]
  node [
    id 206
    label "moralny"
  ]
  node [
    id 207
    label "drogi"
  ]
  node [
    id 208
    label "pozytywny"
  ]
  node [
    id 209
    label "odpowiedni"
  ]
  node [
    id 210
    label "korzystny"
  ]
  node [
    id 211
    label "pos&#322;uszny"
  ]
  node [
    id 212
    label "zdarzenie_si&#281;"
  ]
  node [
    id 213
    label "dzianie_si&#281;"
  ]
  node [
    id 214
    label "skutek_prawny"
  ]
  node [
    id 215
    label "wydarzenie"
  ]
  node [
    id 216
    label "obrona"
  ]
  node [
    id 217
    label "zaatakowanie"
  ]
  node [
    id 218
    label "konfrontacyjny"
  ]
  node [
    id 219
    label "contest"
  ]
  node [
    id 220
    label "action"
  ]
  node [
    id 221
    label "sambo"
  ]
  node [
    id 222
    label "czyn"
  ]
  node [
    id 223
    label "rywalizacja"
  ]
  node [
    id 224
    label "trudno&#347;&#263;"
  ]
  node [
    id 225
    label "sp&#243;r"
  ]
  node [
    id 226
    label "wrestle"
  ]
  node [
    id 227
    label "military_action"
  ]
  node [
    id 228
    label "przebiec"
  ]
  node [
    id 229
    label "charakter"
  ]
  node [
    id 230
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 231
    label "motyw"
  ]
  node [
    id 232
    label "przebiegni&#281;cie"
  ]
  node [
    id 233
    label "fabu&#322;a"
  ]
  node [
    id 234
    label "konflikt"
  ]
  node [
    id 235
    label "clash"
  ]
  node [
    id 236
    label "wsp&#243;r"
  ]
  node [
    id 237
    label "funkcja"
  ]
  node [
    id 238
    label "act"
  ]
  node [
    id 239
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 240
    label "napotka&#263;"
  ]
  node [
    id 241
    label "subiekcja"
  ]
  node [
    id 242
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 243
    label "k&#322;opotliwy"
  ]
  node [
    id 244
    label "napotkanie"
  ]
  node [
    id 245
    label "poziom"
  ]
  node [
    id 246
    label "difficulty"
  ]
  node [
    id 247
    label "obstacle"
  ]
  node [
    id 248
    label "cecha"
  ]
  node [
    id 249
    label "sytuacja"
  ]
  node [
    id 250
    label "skrytykowanie"
  ]
  node [
    id 251
    label "time"
  ]
  node [
    id 252
    label "manewr"
  ]
  node [
    id 253
    label "nast&#261;pienie"
  ]
  node [
    id 254
    label "oddzia&#322;anie"
  ]
  node [
    id 255
    label "przebycie"
  ]
  node [
    id 256
    label "upolowanie"
  ]
  node [
    id 257
    label "wdarcie_si&#281;"
  ]
  node [
    id 258
    label "wyskoczenie_z_g&#281;b&#261;"
  ]
  node [
    id 259
    label "sport"
  ]
  node [
    id 260
    label "progress"
  ]
  node [
    id 261
    label "spr&#243;bowanie"
  ]
  node [
    id 262
    label "powiedzenie"
  ]
  node [
    id 263
    label "rozegranie"
  ]
  node [
    id 264
    label "egzamin"
  ]
  node [
    id 265
    label "liga"
  ]
  node [
    id 266
    label "gracz"
  ]
  node [
    id 267
    label "protection"
  ]
  node [
    id 268
    label "poparcie"
  ]
  node [
    id 269
    label "mecz"
  ]
  node [
    id 270
    label "reakcja"
  ]
  node [
    id 271
    label "defense"
  ]
  node [
    id 272
    label "auspices"
  ]
  node [
    id 273
    label "gra"
  ]
  node [
    id 274
    label "ochrona"
  ]
  node [
    id 275
    label "post&#281;powanie"
  ]
  node [
    id 276
    label "wojsko"
  ]
  node [
    id 277
    label "defensive_structure"
  ]
  node [
    id 278
    label "guard_duty"
  ]
  node [
    id 279
    label "strona"
  ]
  node [
    id 280
    label "agresywny"
  ]
  node [
    id 281
    label "konfrontacyjnie"
  ]
  node [
    id 282
    label "zapasy"
  ]
  node [
    id 283
    label "professional_wrestling"
  ]
  node [
    id 284
    label "przest&#281;pstwo"
  ]
  node [
    id 285
    label "reprodukcja"
  ]
  node [
    id 286
    label "bandytyzm"
  ]
  node [
    id 287
    label "nielegalno&#347;&#263;"
  ]
  node [
    id 288
    label "bootleg"
  ]
  node [
    id 289
    label "transmisja"
  ]
  node [
    id 290
    label "plagiarism"
  ]
  node [
    id 291
    label "terroryzm"
  ]
  node [
    id 292
    label "brudny"
  ]
  node [
    id 293
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 294
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 295
    label "crime"
  ]
  node [
    id 296
    label "sprawstwo"
  ]
  node [
    id 297
    label "barbarzy&#324;stwo"
  ]
  node [
    id 298
    label "nieprawno&#347;&#263;"
  ]
  node [
    id 299
    label "terrorism"
  ]
  node [
    id 300
    label "przekaz"
  ]
  node [
    id 301
    label "program"
  ]
  node [
    id 302
    label "zjawisko"
  ]
  node [
    id 303
    label "proces"
  ]
  node [
    id 304
    label "nagranie"
  ]
  node [
    id 305
    label "powielanie"
  ]
  node [
    id 306
    label "impression"
  ]
  node [
    id 307
    label "picture"
  ]
  node [
    id 308
    label "kopia"
  ]
  node [
    id 309
    label "hodowla"
  ]
  node [
    id 310
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 311
    label "produkcja"
  ]
  node [
    id 312
    label "reproduction"
  ]
  node [
    id 313
    label "podj&#261;&#263;"
  ]
  node [
    id 314
    label "sta&#263;_si&#281;"
  ]
  node [
    id 315
    label "determine"
  ]
  node [
    id 316
    label "zrobi&#263;"
  ]
  node [
    id 317
    label "zareagowa&#263;"
  ]
  node [
    id 318
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 319
    label "draw"
  ]
  node [
    id 320
    label "allude"
  ]
  node [
    id 321
    label "zmieni&#263;"
  ]
  node [
    id 322
    label "zacz&#261;&#263;"
  ]
  node [
    id 323
    label "raise"
  ]
  node [
    id 324
    label "post&#261;pi&#263;"
  ]
  node [
    id 325
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 326
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 327
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 328
    label "zorganizowa&#263;"
  ]
  node [
    id 329
    label "appoint"
  ]
  node [
    id 330
    label "wystylizowa&#263;"
  ]
  node [
    id 331
    label "cause"
  ]
  node [
    id 332
    label "przerobi&#263;"
  ]
  node [
    id 333
    label "nabra&#263;"
  ]
  node [
    id 334
    label "make"
  ]
  node [
    id 335
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 336
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 337
    label "wydali&#263;"
  ]
  node [
    id 338
    label "nastawi&#263;"
  ]
  node [
    id 339
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 340
    label "incorporate"
  ]
  node [
    id 341
    label "obejrze&#263;"
  ]
  node [
    id 342
    label "impersonate"
  ]
  node [
    id 343
    label "dokoptowa&#263;"
  ]
  node [
    id 344
    label "prosecute"
  ]
  node [
    id 345
    label "uruchomi&#263;"
  ]
  node [
    id 346
    label "umie&#347;ci&#263;"
  ]
  node [
    id 347
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 348
    label "spowodowa&#263;"
  ]
  node [
    id 349
    label "begin"
  ]
  node [
    id 350
    label "trip"
  ]
  node [
    id 351
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 352
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 353
    label "odj&#261;&#263;"
  ]
  node [
    id 354
    label "introduce"
  ]
  node [
    id 355
    label "do"
  ]
  node [
    id 356
    label "put"
  ]
  node [
    id 357
    label "uplasowa&#263;"
  ]
  node [
    id 358
    label "wpierniczy&#263;"
  ]
  node [
    id 359
    label "okre&#347;li&#263;"
  ]
  node [
    id 360
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 361
    label "umieszcza&#263;"
  ]
  node [
    id 362
    label "z&#322;amanie"
  ]
  node [
    id 363
    label "plant"
  ]
  node [
    id 364
    label "ustawi&#263;"
  ]
  node [
    id 365
    label "direct"
  ]
  node [
    id 366
    label "aim"
  ]
  node [
    id 367
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 368
    label "poprawi&#263;"
  ]
  node [
    id 369
    label "przyrz&#261;dzi&#263;"
  ]
  node [
    id 370
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 371
    label "wyznaczy&#263;"
  ]
  node [
    id 372
    label "visualize"
  ]
  node [
    id 373
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 374
    label "dokooptowa&#263;"
  ]
  node [
    id 375
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 376
    label "kolejny"
  ]
  node [
    id 377
    label "osobno"
  ]
  node [
    id 378
    label "r&#243;&#380;ny"
  ]
  node [
    id 379
    label "inszy"
  ]
  node [
    id 380
    label "inaczej"
  ]
  node [
    id 381
    label "odr&#281;bny"
  ]
  node [
    id 382
    label "nast&#281;pnie"
  ]
  node [
    id 383
    label "nastopny"
  ]
  node [
    id 384
    label "kolejno"
  ]
  node [
    id 385
    label "kt&#243;ry&#347;"
  ]
  node [
    id 386
    label "jaki&#347;"
  ]
  node [
    id 387
    label "r&#243;&#380;nie"
  ]
  node [
    id 388
    label "individually"
  ]
  node [
    id 389
    label "udzielnie"
  ]
  node [
    id 390
    label "osobnie"
  ]
  node [
    id 391
    label "odr&#281;bnie"
  ]
  node [
    id 392
    label "osobny"
  ]
  node [
    id 393
    label "podmiot"
  ]
  node [
    id 394
    label "wykupienie"
  ]
  node [
    id 395
    label "bycie_w_posiadaniu"
  ]
  node [
    id 396
    label "wykupywanie"
  ]
  node [
    id 397
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 398
    label "cz&#322;owiek"
  ]
  node [
    id 399
    label "osobowo&#347;&#263;"
  ]
  node [
    id 400
    label "organizacja"
  ]
  node [
    id 401
    label "prawo"
  ]
  node [
    id 402
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 403
    label "nauka_prawa"
  ]
  node [
    id 404
    label "kupowanie"
  ]
  node [
    id 405
    label "odkupywanie"
  ]
  node [
    id 406
    label "wyczerpywanie"
  ]
  node [
    id 407
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 408
    label "wyswobadzanie"
  ]
  node [
    id 409
    label "wyczerpanie"
  ]
  node [
    id 410
    label "odkupienie"
  ]
  node [
    id 411
    label "wyswobodzenie"
  ]
  node [
    id 412
    label "kupienie"
  ]
  node [
    id 413
    label "ransom"
  ]
  node [
    id 414
    label "w_prawo"
  ]
  node [
    id 415
    label "s&#322;uszny"
  ]
  node [
    id 416
    label "chwalebny"
  ]
  node [
    id 417
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 418
    label "zgodnie_z_prawem"
  ]
  node [
    id 419
    label "zacny"
  ]
  node [
    id 420
    label "prawicowy"
  ]
  node [
    id 421
    label "na_prawo"
  ]
  node [
    id 422
    label "cnotliwy"
  ]
  node [
    id 423
    label "legalny"
  ]
  node [
    id 424
    label "z_prawa"
  ]
  node [
    id 425
    label "gajny"
  ]
  node [
    id 426
    label "legalnie"
  ]
  node [
    id 427
    label "s&#322;usznie"
  ]
  node [
    id 428
    label "zasadny"
  ]
  node [
    id 429
    label "nale&#380;yty"
  ]
  node [
    id 430
    label "solidny"
  ]
  node [
    id 431
    label "moralnie"
  ]
  node [
    id 432
    label "etycznie"
  ]
  node [
    id 433
    label "pochwalny"
  ]
  node [
    id 434
    label "wspania&#322;y"
  ]
  node [
    id 435
    label "szlachetny"
  ]
  node [
    id 436
    label "powa&#380;ny"
  ]
  node [
    id 437
    label "chwalebnie"
  ]
  node [
    id 438
    label "zacnie"
  ]
  node [
    id 439
    label "skromny"
  ]
  node [
    id 440
    label "niewinny"
  ]
  node [
    id 441
    label "cny"
  ]
  node [
    id 442
    label "cnotliwie"
  ]
  node [
    id 443
    label "prostolinijny"
  ]
  node [
    id 444
    label "szczery"
  ]
  node [
    id 445
    label "zrozumia&#322;y"
  ]
  node [
    id 446
    label "immanentny"
  ]
  node [
    id 447
    label "zwyczajny"
  ]
  node [
    id 448
    label "bezsporny"
  ]
  node [
    id 449
    label "organicznie"
  ]
  node [
    id 450
    label "neutralny"
  ]
  node [
    id 451
    label "normalny"
  ]
  node [
    id 452
    label "rzeczywisty"
  ]
  node [
    id 453
    label "naturalnie"
  ]
  node [
    id 454
    label "prawicowo"
  ]
  node [
    id 455
    label "prawoskr&#281;tny"
  ]
  node [
    id 456
    label "konserwatywny"
  ]
  node [
    id 457
    label "w&#322;asny"
  ]
  node [
    id 458
    label "autorsko"
  ]
  node [
    id 459
    label "samodzielny"
  ]
  node [
    id 460
    label "zwi&#261;zany"
  ]
  node [
    id 461
    label "czyj&#347;"
  ]
  node [
    id 462
    label "swoisty"
  ]
  node [
    id 463
    label "prawnie"
  ]
  node [
    id 464
    label "indywidualnie"
  ]
  node [
    id 465
    label "sznurowanie"
  ]
  node [
    id 466
    label "odrobina"
  ]
  node [
    id 467
    label "skutek"
  ]
  node [
    id 468
    label "sznurowa&#263;"
  ]
  node [
    id 469
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 470
    label "attribute"
  ]
  node [
    id 471
    label "odcisk"
  ]
  node [
    id 472
    label "wp&#322;yw"
  ]
  node [
    id 473
    label "dash"
  ]
  node [
    id 474
    label "ilo&#347;&#263;"
  ]
  node [
    id 475
    label "grain"
  ]
  node [
    id 476
    label "intensywno&#347;&#263;"
  ]
  node [
    id 477
    label "reszta"
  ]
  node [
    id 478
    label "trace"
  ]
  node [
    id 479
    label "obiekt"
  ]
  node [
    id 480
    label "&#347;wiadectwo"
  ]
  node [
    id 481
    label "zgrubienie"
  ]
  node [
    id 482
    label "zmiana"
  ]
  node [
    id 483
    label "odbicie"
  ]
  node [
    id 484
    label "rozrost"
  ]
  node [
    id 485
    label "kwota"
  ]
  node [
    id 486
    label "lobbysta"
  ]
  node [
    id 487
    label "doch&#243;d_narodowy"
  ]
  node [
    id 488
    label "biegni&#281;cie"
  ]
  node [
    id 489
    label "wi&#261;zanie"
  ]
  node [
    id 490
    label "zawi&#261;zywanie"
  ]
  node [
    id 491
    label "sk&#322;adanie"
  ]
  node [
    id 492
    label "lace"
  ]
  node [
    id 493
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 494
    label "biec"
  ]
  node [
    id 495
    label "sk&#322;ada&#263;"
  ]
  node [
    id 496
    label "bind"
  ]
  node [
    id 497
    label "wi&#261;za&#263;"
  ]
  node [
    id 498
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 499
    label "konwulsja"
  ]
  node [
    id 500
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 501
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 502
    label "ruszy&#263;"
  ]
  node [
    id 503
    label "powiedzie&#263;"
  ]
  node [
    id 504
    label "majdn&#261;&#263;"
  ]
  node [
    id 505
    label "most"
  ]
  node [
    id 506
    label "poruszy&#263;"
  ]
  node [
    id 507
    label "wyzwanie"
  ]
  node [
    id 508
    label "da&#263;"
  ]
  node [
    id 509
    label "peddle"
  ]
  node [
    id 510
    label "rush"
  ]
  node [
    id 511
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 512
    label "bewilder"
  ]
  node [
    id 513
    label "przeznaczenie"
  ]
  node [
    id 514
    label "skonstruowa&#263;"
  ]
  node [
    id 515
    label "sygn&#261;&#263;"
  ]
  node [
    id 516
    label "&#347;wiat&#322;o"
  ]
  node [
    id 517
    label "wywo&#322;a&#263;"
  ]
  node [
    id 518
    label "frame"
  ]
  node [
    id 519
    label "podejrzenie"
  ]
  node [
    id 520
    label "czar"
  ]
  node [
    id 521
    label "project"
  ]
  node [
    id 522
    label "odej&#347;&#263;"
  ]
  node [
    id 523
    label "zdecydowa&#263;"
  ]
  node [
    id 524
    label "cie&#324;"
  ]
  node [
    id 525
    label "opu&#347;ci&#263;"
  ]
  node [
    id 526
    label "atak"
  ]
  node [
    id 527
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 528
    label "towar"
  ]
  node [
    id 529
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 530
    label "pozostawi&#263;"
  ]
  node [
    id 531
    label "obni&#380;y&#263;"
  ]
  node [
    id 532
    label "zostawi&#263;"
  ]
  node [
    id 533
    label "przesta&#263;"
  ]
  node [
    id 534
    label "potani&#263;"
  ]
  node [
    id 535
    label "drop"
  ]
  node [
    id 536
    label "evacuate"
  ]
  node [
    id 537
    label "humiliate"
  ]
  node [
    id 538
    label "tekst"
  ]
  node [
    id 539
    label "leave"
  ]
  node [
    id 540
    label "straci&#263;"
  ]
  node [
    id 541
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 542
    label "authorize"
  ]
  node [
    id 543
    label "omin&#261;&#263;"
  ]
  node [
    id 544
    label "odwr&#243;ci&#263;"
  ]
  node [
    id 545
    label "przerzuci&#263;"
  ]
  node [
    id 546
    label "upset"
  ]
  node [
    id 547
    label "sabotage"
  ]
  node [
    id 548
    label "motivate"
  ]
  node [
    id 549
    label "go"
  ]
  node [
    id 550
    label "cut"
  ]
  node [
    id 551
    label "stimulate"
  ]
  node [
    id 552
    label "wzbudzi&#263;"
  ]
  node [
    id 553
    label "travel"
  ]
  node [
    id 554
    label "chemia"
  ]
  node [
    id 555
    label "reakcja_chemiczna"
  ]
  node [
    id 556
    label "evolve"
  ]
  node [
    id 557
    label "stworzy&#263;"
  ]
  node [
    id 558
    label "powierzy&#263;"
  ]
  node [
    id 559
    label "give"
  ]
  node [
    id 560
    label "obieca&#263;"
  ]
  node [
    id 561
    label "pozwoli&#263;"
  ]
  node [
    id 562
    label "odst&#261;pi&#263;"
  ]
  node [
    id 563
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 564
    label "przywali&#263;"
  ]
  node [
    id 565
    label "wyrzec_si&#281;"
  ]
  node [
    id 566
    label "sztachn&#261;&#263;"
  ]
  node [
    id 567
    label "rap"
  ]
  node [
    id 568
    label "feed"
  ]
  node [
    id 569
    label "convey"
  ]
  node [
    id 570
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 571
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 572
    label "testify"
  ]
  node [
    id 573
    label "udost&#281;pni&#263;"
  ]
  node [
    id 574
    label "przeznaczy&#263;"
  ]
  node [
    id 575
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 576
    label "zada&#263;"
  ]
  node [
    id 577
    label "dress"
  ]
  node [
    id 578
    label "dostarczy&#263;"
  ]
  node [
    id 579
    label "przekaza&#263;"
  ]
  node [
    id 580
    label "supply"
  ]
  node [
    id 581
    label "doda&#263;"
  ]
  node [
    id 582
    label "zap&#322;aci&#263;"
  ]
  node [
    id 583
    label "decide"
  ]
  node [
    id 584
    label "odrzut"
  ]
  node [
    id 585
    label "proceed"
  ]
  node [
    id 586
    label "zrezygnowa&#263;"
  ]
  node [
    id 587
    label "min&#261;&#263;"
  ]
  node [
    id 588
    label "leave_office"
  ]
  node [
    id 589
    label "die"
  ]
  node [
    id 590
    label "retract"
  ]
  node [
    id 591
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 592
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 593
    label "accommodate"
  ]
  node [
    id 594
    label "poleci&#263;"
  ]
  node [
    id 595
    label "train"
  ]
  node [
    id 596
    label "wezwa&#263;"
  ]
  node [
    id 597
    label "oznajmi&#263;"
  ]
  node [
    id 598
    label "revolutionize"
  ]
  node [
    id 599
    label "przetworzy&#263;"
  ]
  node [
    id 600
    label "arouse"
  ]
  node [
    id 601
    label "discover"
  ]
  node [
    id 602
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 603
    label "wydoby&#263;"
  ]
  node [
    id 604
    label "poda&#263;"
  ]
  node [
    id 605
    label "express"
  ]
  node [
    id 606
    label "wyrazi&#263;"
  ]
  node [
    id 607
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 608
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 609
    label "rzekn&#261;&#263;"
  ]
  node [
    id 610
    label "unwrap"
  ]
  node [
    id 611
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 612
    label "sprawi&#263;"
  ]
  node [
    id 613
    label "change"
  ]
  node [
    id 614
    label "zast&#261;pi&#263;"
  ]
  node [
    id 615
    label "come_up"
  ]
  node [
    id 616
    label "przej&#347;&#263;"
  ]
  node [
    id 617
    label "zyska&#263;"
  ]
  node [
    id 618
    label "przy&#263;miewa&#263;"
  ]
  node [
    id 619
    label "energia"
  ]
  node [
    id 620
    label "&#347;wieci&#263;"
  ]
  node [
    id 621
    label "odst&#281;p"
  ]
  node [
    id 622
    label "wpadni&#281;cie"
  ]
  node [
    id 623
    label "interpretacja"
  ]
  node [
    id 624
    label "fotokataliza"
  ]
  node [
    id 625
    label "promieniowanie_elektromagnetyczne"
  ]
  node [
    id 626
    label "wpa&#347;&#263;"
  ]
  node [
    id 627
    label "rzuca&#263;"
  ]
  node [
    id 628
    label "obsadnik"
  ]
  node [
    id 629
    label "promieniowanie_optyczne"
  ]
  node [
    id 630
    label "lampa"
  ]
  node [
    id 631
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 632
    label "ja&#347;nia"
  ]
  node [
    id 633
    label "light"
  ]
  node [
    id 634
    label "przeb&#322;yskiwa&#263;"
  ]
  node [
    id 635
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 636
    label "wpada&#263;"
  ]
  node [
    id 637
    label "o&#347;wietlenie"
  ]
  node [
    id 638
    label "punkt_widzenia"
  ]
  node [
    id 639
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 640
    label "przy&#263;mienie"
  ]
  node [
    id 641
    label "instalacja"
  ]
  node [
    id 642
    label "&#347;wiecenie"
  ]
  node [
    id 643
    label "radiance"
  ]
  node [
    id 644
    label "przeb&#322;yskiwanie"
  ]
  node [
    id 645
    label "przy&#263;mi&#263;"
  ]
  node [
    id 646
    label "b&#322;ysk"
  ]
  node [
    id 647
    label "&#347;wiat&#322;y"
  ]
  node [
    id 648
    label "promie&#324;"
  ]
  node [
    id 649
    label "m&#261;drze"
  ]
  node [
    id 650
    label "rozja&#347;nia&#263;"
  ]
  node [
    id 651
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 652
    label "lighting"
  ]
  node [
    id 653
    label "lighter"
  ]
  node [
    id 654
    label "rzucenie"
  ]
  node [
    id 655
    label "plama"
  ]
  node [
    id 656
    label "&#347;rednica"
  ]
  node [
    id 657
    label "wpadanie"
  ]
  node [
    id 658
    label "przy&#263;miewanie"
  ]
  node [
    id 659
    label "rzucanie"
  ]
  node [
    id 660
    label "kr&#281;gi_pod_oczami"
  ]
  node [
    id 661
    label "Ereb"
  ]
  node [
    id 662
    label "kszta&#322;t"
  ]
  node [
    id 663
    label "oznaka"
  ]
  node [
    id 664
    label "ciemnota"
  ]
  node [
    id 665
    label "zm&#281;czenie"
  ]
  node [
    id 666
    label "nekromancja"
  ]
  node [
    id 667
    label "zacienie"
  ]
  node [
    id 668
    label "wycieniowa&#263;"
  ]
  node [
    id 669
    label "zjawa"
  ]
  node [
    id 670
    label "miejsce"
  ]
  node [
    id 671
    label "zmar&#322;y"
  ]
  node [
    id 672
    label "noktowizja"
  ]
  node [
    id 673
    label "kosmetyk_kolorowy"
  ]
  node [
    id 674
    label "sylwetka"
  ]
  node [
    id 675
    label "cloud"
  ]
  node [
    id 676
    label "shade"
  ]
  node [
    id 677
    label "&#263;ma"
  ]
  node [
    id 678
    label "cieniowa&#263;"
  ]
  node [
    id 679
    label "eyeshadow"
  ]
  node [
    id 680
    label "archetyp"
  ]
  node [
    id 681
    label "duch"
  ]
  node [
    id 682
    label "obw&#243;dka"
  ]
  node [
    id 683
    label "bearing"
  ]
  node [
    id 684
    label "oko"
  ]
  node [
    id 685
    label "sowie_oczy"
  ]
  node [
    id 686
    label "przebarwienie"
  ]
  node [
    id 687
    label "pomrok"
  ]
  node [
    id 688
    label "obra&#380;enie"
  ]
  node [
    id 689
    label "wypowied&#378;"
  ]
  node [
    id 690
    label "zaproponowanie"
  ]
  node [
    id 691
    label "gauntlet"
  ]
  node [
    id 692
    label "pojedynek"
  ]
  node [
    id 693
    label "pojedynkowanie_si&#281;"
  ]
  node [
    id 694
    label "zadanie"
  ]
  node [
    id 695
    label "pr&#243;ba"
  ]
  node [
    id 696
    label "challenge"
  ]
  node [
    id 697
    label "zakl&#281;cie"
  ]
  node [
    id 698
    label "niecodzienno&#347;&#263;"
  ]
  node [
    id 699
    label "attraction"
  ]
  node [
    id 700
    label "agreeableness"
  ]
  node [
    id 701
    label "oskar&#380;enie"
  ]
  node [
    id 702
    label "assumption"
  ]
  node [
    id 703
    label "przyjrzenie_si&#281;"
  ]
  node [
    id 704
    label "imputation"
  ]
  node [
    id 705
    label "przypuszczenie"
  ]
  node [
    id 706
    label "pos&#261;d"
  ]
  node [
    id 707
    label "prz&#281;s&#322;o"
  ]
  node [
    id 708
    label "m&#243;zg"
  ]
  node [
    id 709
    label "trasa"
  ]
  node [
    id 710
    label "jarzmo_mostowe"
  ]
  node [
    id 711
    label "pylon"
  ]
  node [
    id 712
    label "zam&#243;zgowie"
  ]
  node [
    id 713
    label "obiekt_mostowy"
  ]
  node [
    id 714
    label "samoch&#243;d"
  ]
  node [
    id 715
    label "szczelina_dylatacyjna"
  ]
  node [
    id 716
    label "bridge"
  ]
  node [
    id 717
    label "suwnica"
  ]
  node [
    id 718
    label "porozumienie"
  ]
  node [
    id 719
    label "nap&#281;d"
  ]
  node [
    id 720
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 721
    label "destiny"
  ]
  node [
    id 722
    label "ustalenie"
  ]
  node [
    id 723
    label "przymus"
  ]
  node [
    id 724
    label "przydzielenie"
  ]
  node [
    id 725
    label "p&#243;j&#347;cie"
  ]
  node [
    id 726
    label "oblat"
  ]
  node [
    id 727
    label "obowi&#261;zek"
  ]
  node [
    id 728
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 729
    label "wybranie"
  ]
  node [
    id 730
    label "metka"
  ]
  node [
    id 731
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 732
    label "szprycowa&#263;"
  ]
  node [
    id 733
    label "naszprycowa&#263;"
  ]
  node [
    id 734
    label "tandeta"
  ]
  node [
    id 735
    label "obr&#243;t_handlowy"
  ]
  node [
    id 736
    label "wyr&#243;b"
  ]
  node [
    id 737
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 738
    label "naszprycowanie"
  ]
  node [
    id 739
    label "tkanina"
  ]
  node [
    id 740
    label "szprycowanie"
  ]
  node [
    id 741
    label "za&#322;adownia"
  ]
  node [
    id 742
    label "asortyment"
  ]
  node [
    id 743
    label "&#322;&#243;dzki"
  ]
  node [
    id 744
    label "narkobiznes"
  ]
  node [
    id 745
    label "pogorszenie"
  ]
  node [
    id 746
    label "przemoc"
  ]
  node [
    id 747
    label "krytyka"
  ]
  node [
    id 748
    label "bat"
  ]
  node [
    id 749
    label "kaszel"
  ]
  node [
    id 750
    label "fit"
  ]
  node [
    id 751
    label "spasm"
  ]
  node [
    id 752
    label "zagrywka"
  ]
  node [
    id 753
    label "&#380;&#261;danie"
  ]
  node [
    id 754
    label "przyp&#322;yw"
  ]
  node [
    id 755
    label "ofensywa"
  ]
  node [
    id 756
    label "pogoda"
  ]
  node [
    id 757
    label "stroke"
  ]
  node [
    id 758
    label "pozycja"
  ]
  node [
    id 759
    label "knock"
  ]
  node [
    id 760
    label "ostatnie_podrygi"
  ]
  node [
    id 761
    label "kurcz"
  ]
  node [
    id 762
    label "machn&#261;&#263;"
  ]
  node [
    id 763
    label "jedyny"
  ]
  node [
    id 764
    label "du&#380;y"
  ]
  node [
    id 765
    label "zdr&#243;w"
  ]
  node [
    id 766
    label "calu&#347;ko"
  ]
  node [
    id 767
    label "kompletny"
  ]
  node [
    id 768
    label "&#380;ywy"
  ]
  node [
    id 769
    label "pe&#322;ny"
  ]
  node [
    id 770
    label "podobny"
  ]
  node [
    id 771
    label "ca&#322;o"
  ]
  node [
    id 772
    label "parametr"
  ]
  node [
    id 773
    label "rozwi&#261;zanie"
  ]
  node [
    id 774
    label "wuchta"
  ]
  node [
    id 775
    label "zaleta"
  ]
  node [
    id 776
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 777
    label "moment_si&#322;y"
  ]
  node [
    id 778
    label "mn&#243;stwo"
  ]
  node [
    id 779
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 780
    label "zdolno&#347;&#263;"
  ]
  node [
    id 781
    label "capacity"
  ]
  node [
    id 782
    label "magnitude"
  ]
  node [
    id 783
    label "potencja"
  ]
  node [
    id 784
    label "boski"
  ]
  node [
    id 785
    label "krajobraz"
  ]
  node [
    id 786
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 787
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 788
    label "przywidzenie"
  ]
  node [
    id 789
    label "presence"
  ]
  node [
    id 790
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 791
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 792
    label "egzergia"
  ]
  node [
    id 793
    label "emitowa&#263;"
  ]
  node [
    id 794
    label "kwant_energii"
  ]
  node [
    id 795
    label "szwung"
  ]
  node [
    id 796
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 797
    label "power"
  ]
  node [
    id 798
    label "emitowanie"
  ]
  node [
    id 799
    label "energy"
  ]
  node [
    id 800
    label "wymiar"
  ]
  node [
    id 801
    label "zmienna"
  ]
  node [
    id 802
    label "charakterystyka"
  ]
  node [
    id 803
    label "wielko&#347;&#263;"
  ]
  node [
    id 804
    label "patologia"
  ]
  node [
    id 805
    label "agresja"
  ]
  node [
    id 806
    label "przewaga"
  ]
  node [
    id 807
    label "drastyczny"
  ]
  node [
    id 808
    label "po&#322;&#243;g"
  ]
  node [
    id 809
    label "spe&#322;nienie"
  ]
  node [
    id 810
    label "dula"
  ]
  node [
    id 811
    label "usuni&#281;cie"
  ]
  node [
    id 812
    label "wymy&#347;lenie"
  ]
  node [
    id 813
    label "po&#322;o&#380;na"
  ]
  node [
    id 814
    label "wyj&#347;cie"
  ]
  node [
    id 815
    label "uniewa&#380;nienie"
  ]
  node [
    id 816
    label "proces_fizjologiczny"
  ]
  node [
    id 817
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 818
    label "szok_poporodowy"
  ]
  node [
    id 819
    label "event"
  ]
  node [
    id 820
    label "marc&#243;wka"
  ]
  node [
    id 821
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 822
    label "birth"
  ]
  node [
    id 823
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 824
    label "wynik"
  ]
  node [
    id 825
    label "przestanie"
  ]
  node [
    id 826
    label "enormousness"
  ]
  node [
    id 827
    label "posiada&#263;"
  ]
  node [
    id 828
    label "potencja&#322;"
  ]
  node [
    id 829
    label "zapomina&#263;"
  ]
  node [
    id 830
    label "zapomnienie"
  ]
  node [
    id 831
    label "zapominanie"
  ]
  node [
    id 832
    label "ability"
  ]
  node [
    id 833
    label "obliczeniowo"
  ]
  node [
    id 834
    label "zapomnie&#263;"
  ]
  node [
    id 835
    label "facylitacja"
  ]
  node [
    id 836
    label "warto&#347;&#263;"
  ]
  node [
    id 837
    label "zrewaluowa&#263;"
  ]
  node [
    id 838
    label "rewaluowanie"
  ]
  node [
    id 839
    label "korzy&#347;&#263;"
  ]
  node [
    id 840
    label "zrewaluowanie"
  ]
  node [
    id 841
    label "rewaluowa&#263;"
  ]
  node [
    id 842
    label "wabik"
  ]
  node [
    id 843
    label "m&#322;ot"
  ]
  node [
    id 844
    label "znak"
  ]
  node [
    id 845
    label "drzewo"
  ]
  node [
    id 846
    label "marka"
  ]
  node [
    id 847
    label "moc"
  ]
  node [
    id 848
    label "potency"
  ]
  node [
    id 849
    label "tomizm"
  ]
  node [
    id 850
    label "wydolno&#347;&#263;"
  ]
  node [
    id 851
    label "arystotelizm"
  ]
  node [
    id 852
    label "gotowo&#347;&#263;"
  ]
  node [
    id 853
    label "zrejterowanie"
  ]
  node [
    id 854
    label "zmobilizowa&#263;"
  ]
  node [
    id 855
    label "dezerter"
  ]
  node [
    id 856
    label "oddzia&#322;_karny"
  ]
  node [
    id 857
    label "rezerwa"
  ]
  node [
    id 858
    label "tabor"
  ]
  node [
    id 859
    label "wermacht"
  ]
  node [
    id 860
    label "cofni&#281;cie"
  ]
  node [
    id 861
    label "fala"
  ]
  node [
    id 862
    label "szko&#322;a"
  ]
  node [
    id 863
    label "korpus"
  ]
  node [
    id 864
    label "soldateska"
  ]
  node [
    id 865
    label "ods&#322;ugiwanie"
  ]
  node [
    id 866
    label "werbowanie_si&#281;"
  ]
  node [
    id 867
    label "zdemobilizowanie"
  ]
  node [
    id 868
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 869
    label "s&#322;u&#380;ba"
  ]
  node [
    id 870
    label "or&#281;&#380;"
  ]
  node [
    id 871
    label "Legia_Cudzoziemska"
  ]
  node [
    id 872
    label "Armia_Czerwona"
  ]
  node [
    id 873
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 874
    label "rejterowanie"
  ]
  node [
    id 875
    label "Czerwona_Gwardia"
  ]
  node [
    id 876
    label "zrejterowa&#263;"
  ]
  node [
    id 877
    label "sztabslekarz"
  ]
  node [
    id 878
    label "zmobilizowanie"
  ]
  node [
    id 879
    label "wojo"
  ]
  node [
    id 880
    label "pospolite_ruszenie"
  ]
  node [
    id 881
    label "Eurokorpus"
  ]
  node [
    id 882
    label "mobilizowanie"
  ]
  node [
    id 883
    label "rejterowa&#263;"
  ]
  node [
    id 884
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 885
    label "mobilizowa&#263;"
  ]
  node [
    id 886
    label "Armia_Krajowa"
  ]
  node [
    id 887
    label "dryl"
  ]
  node [
    id 888
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 889
    label "petarda"
  ]
  node [
    id 890
    label "zdemobilizowa&#263;"
  ]
  node [
    id 891
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 892
    label "ii"
  ]
  node [
    id 893
    label "tv"
  ]
  node [
    id 894
    label "trwa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 25
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 376
  ]
  edge [
    source 14
    target 377
  ]
  edge [
    source 14
    target 378
  ]
  edge [
    source 14
    target 379
  ]
  edge [
    source 14
    target 380
  ]
  edge [
    source 14
    target 381
  ]
  edge [
    source 14
    target 382
  ]
  edge [
    source 14
    target 383
  ]
  edge [
    source 14
    target 384
  ]
  edge [
    source 14
    target 385
  ]
  edge [
    source 14
    target 386
  ]
  edge [
    source 14
    target 387
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 388
  ]
  edge [
    source 14
    target 389
  ]
  edge [
    source 14
    target 390
  ]
  edge [
    source 14
    target 391
  ]
  edge [
    source 14
    target 392
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 393
  ]
  edge [
    source 15
    target 394
  ]
  edge [
    source 15
    target 395
  ]
  edge [
    source 15
    target 396
  ]
  edge [
    source 15
    target 397
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 398
  ]
  edge [
    source 15
    target 399
  ]
  edge [
    source 15
    target 400
  ]
  edge [
    source 15
    target 401
  ]
  edge [
    source 15
    target 402
  ]
  edge [
    source 15
    target 403
  ]
  edge [
    source 15
    target 404
  ]
  edge [
    source 15
    target 405
  ]
  edge [
    source 15
    target 406
  ]
  edge [
    source 15
    target 407
  ]
  edge [
    source 15
    target 408
  ]
  edge [
    source 15
    target 409
  ]
  edge [
    source 15
    target 410
  ]
  edge [
    source 15
    target 411
  ]
  edge [
    source 15
    target 412
  ]
  edge [
    source 15
    target 413
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 414
  ]
  edge [
    source 16
    target 415
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 416
  ]
  edge [
    source 16
    target 417
  ]
  edge [
    source 16
    target 418
  ]
  edge [
    source 16
    target 419
  ]
  edge [
    source 16
    target 206
  ]
  edge [
    source 16
    target 420
  ]
  edge [
    source 16
    target 421
  ]
  edge [
    source 16
    target 422
  ]
  edge [
    source 16
    target 423
  ]
  edge [
    source 16
    target 424
  ]
  edge [
    source 16
    target 425
  ]
  edge [
    source 16
    target 426
  ]
  edge [
    source 16
    target 427
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 428
  ]
  edge [
    source 16
    target 429
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 430
  ]
  edge [
    source 16
    target 431
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 432
  ]
  edge [
    source 16
    target 41
  ]
  edge [
    source 16
    target 433
  ]
  edge [
    source 16
    target 434
  ]
  edge [
    source 16
    target 435
  ]
  edge [
    source 16
    target 436
  ]
  edge [
    source 16
    target 437
  ]
  edge [
    source 16
    target 438
  ]
  edge [
    source 16
    target 439
  ]
  edge [
    source 16
    target 440
  ]
  edge [
    source 16
    target 441
  ]
  edge [
    source 16
    target 442
  ]
  edge [
    source 16
    target 443
  ]
  edge [
    source 16
    target 444
  ]
  edge [
    source 16
    target 445
  ]
  edge [
    source 16
    target 446
  ]
  edge [
    source 16
    target 447
  ]
  edge [
    source 16
    target 448
  ]
  edge [
    source 16
    target 449
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 450
  ]
  edge [
    source 16
    target 451
  ]
  edge [
    source 16
    target 452
  ]
  edge [
    source 16
    target 453
  ]
  edge [
    source 16
    target 454
  ]
  edge [
    source 16
    target 455
  ]
  edge [
    source 16
    target 456
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 457
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 458
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 459
  ]
  edge [
    source 17
    target 460
  ]
  edge [
    source 17
    target 461
  ]
  edge [
    source 17
    target 462
  ]
  edge [
    source 17
    target 392
  ]
  edge [
    source 17
    target 463
  ]
  edge [
    source 17
    target 464
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 465
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 468
  ]
  edge [
    source 18
    target 469
  ]
  edge [
    source 18
    target 470
  ]
  edge [
    source 18
    target 471
  ]
  edge [
    source 18
    target 472
  ]
  edge [
    source 18
    target 473
  ]
  edge [
    source 18
    target 474
  ]
  edge [
    source 18
    target 475
  ]
  edge [
    source 18
    target 476
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 478
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 480
  ]
  edge [
    source 18
    target 58
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 482
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 484
  ]
  edge [
    source 18
    target 485
  ]
  edge [
    source 18
    target 302
  ]
  edge [
    source 18
    target 486
  ]
  edge [
    source 18
    target 487
  ]
  edge [
    source 18
    target 488
  ]
  edge [
    source 18
    target 489
  ]
  edge [
    source 18
    target 490
  ]
  edge [
    source 18
    target 491
  ]
  edge [
    source 18
    target 492
  ]
  edge [
    source 18
    target 493
  ]
  edge [
    source 18
    target 494
  ]
  edge [
    source 18
    target 495
  ]
  edge [
    source 18
    target 496
  ]
  edge [
    source 18
    target 497
  ]
  edge [
    source 19
    target 892
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 498
  ]
  edge [
    source 20
    target 499
  ]
  edge [
    source 20
    target 500
  ]
  edge [
    source 20
    target 501
  ]
  edge [
    source 20
    target 502
  ]
  edge [
    source 20
    target 503
  ]
  edge [
    source 20
    target 504
  ]
  edge [
    source 20
    target 505
  ]
  edge [
    source 20
    target 506
  ]
  edge [
    source 20
    target 507
  ]
  edge [
    source 20
    target 508
  ]
  edge [
    source 20
    target 509
  ]
  edge [
    source 20
    target 510
  ]
  edge [
    source 20
    target 511
  ]
  edge [
    source 20
    target 321
  ]
  edge [
    source 20
    target 512
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 20
    target 513
  ]
  edge [
    source 20
    target 514
  ]
  edge [
    source 20
    target 515
  ]
  edge [
    source 20
    target 516
  ]
  edge [
    source 20
    target 348
  ]
  edge [
    source 20
    target 517
  ]
  edge [
    source 20
    target 518
  ]
  edge [
    source 20
    target 519
  ]
  edge [
    source 20
    target 520
  ]
  edge [
    source 20
    target 521
  ]
  edge [
    source 20
    target 522
  ]
  edge [
    source 20
    target 523
  ]
  edge [
    source 20
    target 524
  ]
  edge [
    source 20
    target 525
  ]
  edge [
    source 20
    target 526
  ]
  edge [
    source 20
    target 527
  ]
  edge [
    source 20
    target 528
  ]
  edge [
    source 20
    target 529
  ]
  edge [
    source 20
    target 530
  ]
  edge [
    source 20
    target 531
  ]
  edge [
    source 20
    target 532
  ]
  edge [
    source 20
    target 533
  ]
  edge [
    source 20
    target 534
  ]
  edge [
    source 20
    target 535
  ]
  edge [
    source 20
    target 536
  ]
  edge [
    source 20
    target 537
  ]
  edge [
    source 20
    target 538
  ]
  edge [
    source 20
    target 539
  ]
  edge [
    source 20
    target 540
  ]
  edge [
    source 20
    target 541
  ]
  edge [
    source 20
    target 542
  ]
  edge [
    source 20
    target 543
  ]
  edge [
    source 20
    target 544
  ]
  edge [
    source 20
    target 545
  ]
  edge [
    source 20
    target 546
  ]
  edge [
    source 20
    target 547
  ]
  edge [
    source 20
    target 548
  ]
  edge [
    source 20
    target 318
  ]
  edge [
    source 20
    target 66
  ]
  edge [
    source 20
    target 549
  ]
  edge [
    source 20
    target 316
  ]
  edge [
    source 20
    target 320
  ]
  edge [
    source 20
    target 550
  ]
  edge [
    source 20
    target 551
  ]
  edge [
    source 20
    target 322
  ]
  edge [
    source 20
    target 552
  ]
  edge [
    source 20
    target 553
  ]
  edge [
    source 20
    target 347
  ]
  edge [
    source 20
    target 238
  ]
  edge [
    source 20
    target 57
  ]
  edge [
    source 20
    target 554
  ]
  edge [
    source 20
    target 555
  ]
  edge [
    source 20
    target 556
  ]
  edge [
    source 20
    target 557
  ]
  edge [
    source 20
    target 558
  ]
  edge [
    source 20
    target 559
  ]
  edge [
    source 20
    target 560
  ]
  edge [
    source 20
    target 561
  ]
  edge [
    source 20
    target 562
  ]
  edge [
    source 20
    target 563
  ]
  edge [
    source 20
    target 564
  ]
  edge [
    source 20
    target 565
  ]
  edge [
    source 20
    target 566
  ]
  edge [
    source 20
    target 567
  ]
  edge [
    source 20
    target 568
  ]
  edge [
    source 20
    target 569
  ]
  edge [
    source 20
    target 570
  ]
  edge [
    source 20
    target 571
  ]
  edge [
    source 20
    target 572
  ]
  edge [
    source 20
    target 573
  ]
  edge [
    source 20
    target 574
  ]
  edge [
    source 20
    target 575
  ]
  edge [
    source 20
    target 307
  ]
  edge [
    source 20
    target 576
  ]
  edge [
    source 20
    target 577
  ]
  edge [
    source 20
    target 578
  ]
  edge [
    source 20
    target 579
  ]
  edge [
    source 20
    target 580
  ]
  edge [
    source 20
    target 581
  ]
  edge [
    source 20
    target 582
  ]
  edge [
    source 20
    target 314
  ]
  edge [
    source 20
    target 313
  ]
  edge [
    source 20
    target 583
  ]
  edge [
    source 20
    target 315
  ]
  edge [
    source 20
    target 584
  ]
  edge [
    source 20
    target 585
  ]
  edge [
    source 20
    target 586
  ]
  edge [
    source 20
    target 587
  ]
  edge [
    source 20
    target 588
  ]
  edge [
    source 20
    target 589
  ]
  edge [
    source 20
    target 590
  ]
  edge [
    source 20
    target 591
  ]
  edge [
    source 20
    target 592
  ]
  edge [
    source 20
    target 593
  ]
  edge [
    source 20
    target 594
  ]
  edge [
    source 20
    target 595
  ]
  edge [
    source 20
    target 596
  ]
  edge [
    source 20
    target 350
  ]
  edge [
    source 20
    target 597
  ]
  edge [
    source 20
    target 598
  ]
  edge [
    source 20
    target 599
  ]
  edge [
    source 20
    target 337
  ]
  edge [
    source 20
    target 600
  ]
  edge [
    source 20
    target 601
  ]
  edge [
    source 20
    target 602
  ]
  edge [
    source 20
    target 603
  ]
  edge [
    source 20
    target 359
  ]
  edge [
    source 20
    target 604
  ]
  edge [
    source 20
    target 605
  ]
  edge [
    source 20
    target 606
  ]
  edge [
    source 20
    target 607
  ]
  edge [
    source 20
    target 608
  ]
  edge [
    source 20
    target 609
  ]
  edge [
    source 20
    target 610
  ]
  edge [
    source 20
    target 611
  ]
  edge [
    source 20
    target 612
  ]
  edge [
    source 20
    target 613
  ]
  edge [
    source 20
    target 614
  ]
  edge [
    source 20
    target 615
  ]
  edge [
    source 20
    target 616
  ]
  edge [
    source 20
    target 617
  ]
  edge [
    source 20
    target 618
  ]
  edge [
    source 20
    target 619
  ]
  edge [
    source 20
    target 620
  ]
  edge [
    source 20
    target 621
  ]
  edge [
    source 20
    target 622
  ]
  edge [
    source 20
    target 623
  ]
  edge [
    source 20
    target 302
  ]
  edge [
    source 20
    target 248
  ]
  edge [
    source 20
    target 624
  ]
  edge [
    source 20
    target 625
  ]
  edge [
    source 20
    target 626
  ]
  edge [
    source 20
    target 627
  ]
  edge [
    source 20
    target 628
  ]
  edge [
    source 20
    target 629
  ]
  edge [
    source 20
    target 630
  ]
  edge [
    source 20
    target 631
  ]
  edge [
    source 20
    target 632
  ]
  edge [
    source 20
    target 633
  ]
  edge [
    source 20
    target 634
  ]
  edge [
    source 20
    target 635
  ]
  edge [
    source 20
    target 636
  ]
  edge [
    source 20
    target 637
  ]
  edge [
    source 20
    target 638
  ]
  edge [
    source 20
    target 639
  ]
  edge [
    source 20
    target 640
  ]
  edge [
    source 20
    target 641
  ]
  edge [
    source 20
    target 642
  ]
  edge [
    source 20
    target 643
  ]
  edge [
    source 20
    target 644
  ]
  edge [
    source 20
    target 645
  ]
  edge [
    source 20
    target 646
  ]
  edge [
    source 20
    target 647
  ]
  edge [
    source 20
    target 648
  ]
  edge [
    source 20
    target 649
  ]
  edge [
    source 20
    target 650
  ]
  edge [
    source 20
    target 651
  ]
  edge [
    source 20
    target 652
  ]
  edge [
    source 20
    target 653
  ]
  edge [
    source 20
    target 654
  ]
  edge [
    source 20
    target 655
  ]
  edge [
    source 20
    target 656
  ]
  edge [
    source 20
    target 657
  ]
  edge [
    source 20
    target 658
  ]
  edge [
    source 20
    target 659
  ]
  edge [
    source 20
    target 660
  ]
  edge [
    source 20
    target 661
  ]
  edge [
    source 20
    target 662
  ]
  edge [
    source 20
    target 663
  ]
  edge [
    source 20
    target 664
  ]
  edge [
    source 20
    target 665
  ]
  edge [
    source 20
    target 666
  ]
  edge [
    source 20
    target 667
  ]
  edge [
    source 20
    target 668
  ]
  edge [
    source 20
    target 669
  ]
  edge [
    source 20
    target 670
  ]
  edge [
    source 20
    target 466
  ]
  edge [
    source 20
    target 671
  ]
  edge [
    source 20
    target 672
  ]
  edge [
    source 20
    target 673
  ]
  edge [
    source 20
    target 674
  ]
  edge [
    source 20
    target 675
  ]
  edge [
    source 20
    target 676
  ]
  edge [
    source 20
    target 677
  ]
  edge [
    source 20
    target 678
  ]
  edge [
    source 20
    target 679
  ]
  edge [
    source 20
    target 680
  ]
  edge [
    source 20
    target 681
  ]
  edge [
    source 20
    target 682
  ]
  edge [
    source 20
    target 683
  ]
  edge [
    source 20
    target 684
  ]
  edge [
    source 20
    target 685
  ]
  edge [
    source 20
    target 686
  ]
  edge [
    source 20
    target 687
  ]
  edge [
    source 20
    target 688
  ]
  edge [
    source 20
    target 689
  ]
  edge [
    source 20
    target 690
  ]
  edge [
    source 20
    target 691
  ]
  edge [
    source 20
    target 692
  ]
  edge [
    source 20
    target 693
  ]
  edge [
    source 20
    target 694
  ]
  edge [
    source 20
    target 695
  ]
  edge [
    source 20
    target 696
  ]
  edge [
    source 20
    target 697
  ]
  edge [
    source 20
    target 698
  ]
  edge [
    source 20
    target 699
  ]
  edge [
    source 20
    target 700
  ]
  edge [
    source 20
    target 701
  ]
  edge [
    source 20
    target 702
  ]
  edge [
    source 20
    target 703
  ]
  edge [
    source 20
    target 704
  ]
  edge [
    source 20
    target 705
  ]
  edge [
    source 20
    target 706
  ]
  edge [
    source 20
    target 707
  ]
  edge [
    source 20
    target 708
  ]
  edge [
    source 20
    target 709
  ]
  edge [
    source 20
    target 710
  ]
  edge [
    source 20
    target 711
  ]
  edge [
    source 20
    target 712
  ]
  edge [
    source 20
    target 713
  ]
  edge [
    source 20
    target 714
  ]
  edge [
    source 20
    target 715
  ]
  edge [
    source 20
    target 716
  ]
  edge [
    source 20
    target 717
  ]
  edge [
    source 20
    target 718
  ]
  edge [
    source 20
    target 719
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 720
  ]
  edge [
    source 20
    target 721
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 722
  ]
  edge [
    source 20
    target 723
  ]
  edge [
    source 20
    target 724
  ]
  edge [
    source 20
    target 725
  ]
  edge [
    source 20
    target 726
  ]
  edge [
    source 20
    target 727
  ]
  edge [
    source 20
    target 728
  ]
  edge [
    source 20
    target 729
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 730
  ]
  edge [
    source 20
    target 731
  ]
  edge [
    source 20
    target 398
  ]
  edge [
    source 20
    target 732
  ]
  edge [
    source 20
    target 733
  ]
  edge [
    source 20
    target 734
  ]
  edge [
    source 20
    target 735
  ]
  edge [
    source 20
    target 97
  ]
  edge [
    source 20
    target 736
  ]
  edge [
    source 20
    target 737
  ]
  edge [
    source 20
    target 738
  ]
  edge [
    source 20
    target 739
  ]
  edge [
    source 20
    target 740
  ]
  edge [
    source 20
    target 741
  ]
  edge [
    source 20
    target 742
  ]
  edge [
    source 20
    target 743
  ]
  edge [
    source 20
    target 744
  ]
  edge [
    source 20
    target 265
  ]
  edge [
    source 20
    target 745
  ]
  edge [
    source 20
    target 746
  ]
  edge [
    source 20
    target 747
  ]
  edge [
    source 20
    target 748
  ]
  edge [
    source 20
    target 749
  ]
  edge [
    source 20
    target 750
  ]
  edge [
    source 20
    target 751
  ]
  edge [
    source 20
    target 752
  ]
  edge [
    source 20
    target 753
  ]
  edge [
    source 20
    target 252
  ]
  edge [
    source 20
    target 754
  ]
  edge [
    source 20
    target 755
  ]
  edge [
    source 20
    target 756
  ]
  edge [
    source 20
    target 757
  ]
  edge [
    source 20
    target 758
  ]
  edge [
    source 20
    target 759
  ]
  edge [
    source 20
    target 760
  ]
  edge [
    source 20
    target 761
  ]
  edge [
    source 20
    target 762
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 203
  ]
  edge [
    source 21
    target 763
  ]
  edge [
    source 21
    target 764
  ]
  edge [
    source 21
    target 765
  ]
  edge [
    source 21
    target 766
  ]
  edge [
    source 21
    target 767
  ]
  edge [
    source 21
    target 768
  ]
  edge [
    source 21
    target 769
  ]
  edge [
    source 21
    target 770
  ]
  edge [
    source 21
    target 771
  ]
  edge [
    source 22
    target 619
  ]
  edge [
    source 22
    target 772
  ]
  edge [
    source 22
    target 773
  ]
  edge [
    source 22
    target 276
  ]
  edge [
    source 22
    target 248
  ]
  edge [
    source 22
    target 774
  ]
  edge [
    source 22
    target 775
  ]
  edge [
    source 22
    target 776
  ]
  edge [
    source 22
    target 777
  ]
  edge [
    source 22
    target 778
  ]
  edge [
    source 22
    target 779
  ]
  edge [
    source 22
    target 302
  ]
  edge [
    source 22
    target 780
  ]
  edge [
    source 22
    target 781
  ]
  edge [
    source 22
    target 782
  ]
  edge [
    source 22
    target 783
  ]
  edge [
    source 22
    target 746
  ]
  edge [
    source 22
    target 303
  ]
  edge [
    source 22
    target 784
  ]
  edge [
    source 22
    target 785
  ]
  edge [
    source 22
    target 786
  ]
  edge [
    source 22
    target 787
  ]
  edge [
    source 22
    target 788
  ]
  edge [
    source 22
    target 789
  ]
  edge [
    source 22
    target 229
  ]
  edge [
    source 22
    target 790
  ]
  edge [
    source 22
    target 791
  ]
  edge [
    source 22
    target 651
  ]
  edge [
    source 22
    target 792
  ]
  edge [
    source 22
    target 793
  ]
  edge [
    source 22
    target 794
  ]
  edge [
    source 22
    target 795
  ]
  edge [
    source 22
    target 796
  ]
  edge [
    source 22
    target 797
  ]
  edge [
    source 22
    target 798
  ]
  edge [
    source 22
    target 799
  ]
  edge [
    source 22
    target 800
  ]
  edge [
    source 22
    target 801
  ]
  edge [
    source 22
    target 802
  ]
  edge [
    source 22
    target 803
  ]
  edge [
    source 22
    target 804
  ]
  edge [
    source 22
    target 805
  ]
  edge [
    source 22
    target 806
  ]
  edge [
    source 22
    target 807
  ]
  edge [
    source 22
    target 808
  ]
  edge [
    source 22
    target 809
  ]
  edge [
    source 22
    target 810
  ]
  edge [
    source 22
    target 114
  ]
  edge [
    source 22
    target 811
  ]
  edge [
    source 22
    target 812
  ]
  edge [
    source 22
    target 813
  ]
  edge [
    source 22
    target 814
  ]
  edge [
    source 22
    target 815
  ]
  edge [
    source 22
    target 816
  ]
  edge [
    source 22
    target 817
  ]
  edge [
    source 22
    target 818
  ]
  edge [
    source 22
    target 819
  ]
  edge [
    source 22
    target 820
  ]
  edge [
    source 22
    target 821
  ]
  edge [
    source 22
    target 822
  ]
  edge [
    source 22
    target 823
  ]
  edge [
    source 22
    target 824
  ]
  edge [
    source 22
    target 825
  ]
  edge [
    source 22
    target 474
  ]
  edge [
    source 22
    target 826
  ]
  edge [
    source 22
    target 827
  ]
  edge [
    source 22
    target 828
  ]
  edge [
    source 22
    target 829
  ]
  edge [
    source 22
    target 830
  ]
  edge [
    source 22
    target 831
  ]
  edge [
    source 22
    target 832
  ]
  edge [
    source 22
    target 833
  ]
  edge [
    source 22
    target 834
  ]
  edge [
    source 22
    target 835
  ]
  edge [
    source 22
    target 97
  ]
  edge [
    source 22
    target 836
  ]
  edge [
    source 22
    target 837
  ]
  edge [
    source 22
    target 838
  ]
  edge [
    source 22
    target 839
  ]
  edge [
    source 22
    target 840
  ]
  edge [
    source 22
    target 841
  ]
  edge [
    source 22
    target 842
  ]
  edge [
    source 22
    target 279
  ]
  edge [
    source 22
    target 843
  ]
  edge [
    source 22
    target 844
  ]
  edge [
    source 22
    target 845
  ]
  edge [
    source 22
    target 695
  ]
  edge [
    source 22
    target 470
  ]
  edge [
    source 22
    target 846
  ]
  edge [
    source 22
    target 847
  ]
  edge [
    source 22
    target 848
  ]
  edge [
    source 22
    target 78
  ]
  edge [
    source 22
    target 849
  ]
  edge [
    source 22
    target 850
  ]
  edge [
    source 22
    target 82
  ]
  edge [
    source 22
    target 851
  ]
  edge [
    source 22
    target 852
  ]
  edge [
    source 22
    target 853
  ]
  edge [
    source 22
    target 854
  ]
  edge [
    source 22
    target 55
  ]
  edge [
    source 22
    target 855
  ]
  edge [
    source 22
    target 856
  ]
  edge [
    source 22
    target 857
  ]
  edge [
    source 22
    target 858
  ]
  edge [
    source 22
    target 859
  ]
  edge [
    source 22
    target 860
  ]
  edge [
    source 22
    target 861
  ]
  edge [
    source 22
    target 94
  ]
  edge [
    source 22
    target 862
  ]
  edge [
    source 22
    target 863
  ]
  edge [
    source 22
    target 864
  ]
  edge [
    source 22
    target 865
  ]
  edge [
    source 22
    target 866
  ]
  edge [
    source 22
    target 867
  ]
  edge [
    source 22
    target 104
  ]
  edge [
    source 22
    target 868
  ]
  edge [
    source 22
    target 869
  ]
  edge [
    source 22
    target 870
  ]
  edge [
    source 22
    target 871
  ]
  edge [
    source 22
    target 872
  ]
  edge [
    source 22
    target 873
  ]
  edge [
    source 22
    target 874
  ]
  edge [
    source 22
    target 875
  ]
  edge [
    source 22
    target 876
  ]
  edge [
    source 22
    target 877
  ]
  edge [
    source 22
    target 878
  ]
  edge [
    source 22
    target 879
  ]
  edge [
    source 22
    target 880
  ]
  edge [
    source 22
    target 881
  ]
  edge [
    source 22
    target 882
  ]
  edge [
    source 22
    target 883
  ]
  edge [
    source 22
    target 884
  ]
  edge [
    source 22
    target 885
  ]
  edge [
    source 22
    target 886
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 887
  ]
  edge [
    source 22
    target 888
  ]
  edge [
    source 22
    target 889
  ]
  edge [
    source 22
    target 758
  ]
  edge [
    source 22
    target 890
  ]
  edge [
    source 22
    target 891
  ]
  edge [
    source 893
    target 894
  ]
]
