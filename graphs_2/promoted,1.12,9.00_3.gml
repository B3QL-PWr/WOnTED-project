graph [
  node [
    id 0
    label "grupa"
    origin "text"
  ]
  node [
    id 1
    label "bia&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "prawnik"
    origin "text"
  ]
  node [
    id 3
    label "walcz&#261;ca"
    origin "text"
  ]
  node [
    id 4
    label "przeciwko"
    origin "text"
  ]
  node [
    id 5
    label "plan"
    origin "text"
  ]
  node [
    id 6
    label "oddanie"
    origin "text"
  ]
  node [
    id 7
    label "ziemia"
    origin "text"
  ]
  node [
    id 8
    label "czarna"
    origin "text"
  ]
  node [
    id 9
    label "po&#322;udniowoafrykanom"
    origin "text"
  ]
  node [
    id 10
    label "bez"
    origin "text"
  ]
  node [
    id 11
    label "odszkodowanie"
    origin "text"
  ]
  node [
    id 12
    label "przegra&#263;"
    origin "text"
  ]
  node [
    id 13
    label "przed"
    origin "text"
  ]
  node [
    id 14
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 15
    label "najwy&#380;szy"
    origin "text"
  ]
  node [
    id 16
    label "kraj"
    origin "text"
  ]
  node [
    id 17
    label "odm&#322;adzanie"
  ]
  node [
    id 18
    label "liga"
  ]
  node [
    id 19
    label "jednostka_systematyczna"
  ]
  node [
    id 20
    label "asymilowanie"
  ]
  node [
    id 21
    label "gromada"
  ]
  node [
    id 22
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 23
    label "asymilowa&#263;"
  ]
  node [
    id 24
    label "egzemplarz"
  ]
  node [
    id 25
    label "Entuzjastki"
  ]
  node [
    id 26
    label "zbi&#243;r"
  ]
  node [
    id 27
    label "kompozycja"
  ]
  node [
    id 28
    label "Terranie"
  ]
  node [
    id 29
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 30
    label "category"
  ]
  node [
    id 31
    label "pakiet_klimatyczny"
  ]
  node [
    id 32
    label "oddzia&#322;"
  ]
  node [
    id 33
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 34
    label "cz&#261;steczka"
  ]
  node [
    id 35
    label "stage_set"
  ]
  node [
    id 36
    label "type"
  ]
  node [
    id 37
    label "specgrupa"
  ]
  node [
    id 38
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 39
    label "&#346;wietliki"
  ]
  node [
    id 40
    label "odm&#322;odzenie"
  ]
  node [
    id 41
    label "Eurogrupa"
  ]
  node [
    id 42
    label "odm&#322;adza&#263;"
  ]
  node [
    id 43
    label "formacja_geologiczna"
  ]
  node [
    id 44
    label "harcerze_starsi"
  ]
  node [
    id 45
    label "konfiguracja"
  ]
  node [
    id 46
    label "cz&#261;stka"
  ]
  node [
    id 47
    label "masa_cz&#261;steczkowa"
  ]
  node [
    id 48
    label "diadochia"
  ]
  node [
    id 49
    label "substancja"
  ]
  node [
    id 50
    label "grupa_funkcyjna"
  ]
  node [
    id 51
    label "integer"
  ]
  node [
    id 52
    label "liczba"
  ]
  node [
    id 53
    label "zlewanie_si&#281;"
  ]
  node [
    id 54
    label "ilo&#347;&#263;"
  ]
  node [
    id 55
    label "uk&#322;ad"
  ]
  node [
    id 56
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 57
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 58
    label "pe&#322;ny"
  ]
  node [
    id 59
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 60
    label "series"
  ]
  node [
    id 61
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 62
    label "uprawianie"
  ]
  node [
    id 63
    label "praca_rolnicza"
  ]
  node [
    id 64
    label "collection"
  ]
  node [
    id 65
    label "dane"
  ]
  node [
    id 66
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 67
    label "poj&#281;cie"
  ]
  node [
    id 68
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 69
    label "sum"
  ]
  node [
    id 70
    label "gathering"
  ]
  node [
    id 71
    label "album"
  ]
  node [
    id 72
    label "zesp&#243;&#322;"
  ]
  node [
    id 73
    label "dzia&#322;"
  ]
  node [
    id 74
    label "system"
  ]
  node [
    id 75
    label "lias"
  ]
  node [
    id 76
    label "jednostka"
  ]
  node [
    id 77
    label "pi&#281;tro"
  ]
  node [
    id 78
    label "klasa"
  ]
  node [
    id 79
    label "jednostka_geologiczna"
  ]
  node [
    id 80
    label "filia"
  ]
  node [
    id 81
    label "malm"
  ]
  node [
    id 82
    label "whole"
  ]
  node [
    id 83
    label "dogger"
  ]
  node [
    id 84
    label "poziom"
  ]
  node [
    id 85
    label "promocja"
  ]
  node [
    id 86
    label "kurs"
  ]
  node [
    id 87
    label "bank"
  ]
  node [
    id 88
    label "formacja"
  ]
  node [
    id 89
    label "ajencja"
  ]
  node [
    id 90
    label "wojsko"
  ]
  node [
    id 91
    label "siedziba"
  ]
  node [
    id 92
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 93
    label "agencja"
  ]
  node [
    id 94
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 95
    label "szpital"
  ]
  node [
    id 96
    label "blend"
  ]
  node [
    id 97
    label "struktura"
  ]
  node [
    id 98
    label "prawo_karne"
  ]
  node [
    id 99
    label "leksem"
  ]
  node [
    id 100
    label "dzie&#322;o"
  ]
  node [
    id 101
    label "figuracja"
  ]
  node [
    id 102
    label "chwyt"
  ]
  node [
    id 103
    label "okup"
  ]
  node [
    id 104
    label "muzykologia"
  ]
  node [
    id 105
    label "&#347;redniowiecze"
  ]
  node [
    id 106
    label "czynnik_biotyczny"
  ]
  node [
    id 107
    label "wyewoluowanie"
  ]
  node [
    id 108
    label "reakcja"
  ]
  node [
    id 109
    label "individual"
  ]
  node [
    id 110
    label "przyswoi&#263;"
  ]
  node [
    id 111
    label "wytw&#243;r"
  ]
  node [
    id 112
    label "starzenie_si&#281;"
  ]
  node [
    id 113
    label "wyewoluowa&#263;"
  ]
  node [
    id 114
    label "okaz"
  ]
  node [
    id 115
    label "part"
  ]
  node [
    id 116
    label "przyswojenie"
  ]
  node [
    id 117
    label "ewoluowanie"
  ]
  node [
    id 118
    label "ewoluowa&#263;"
  ]
  node [
    id 119
    label "obiekt"
  ]
  node [
    id 120
    label "sztuka"
  ]
  node [
    id 121
    label "agent"
  ]
  node [
    id 122
    label "przyswaja&#263;"
  ]
  node [
    id 123
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 124
    label "nicpo&#324;"
  ]
  node [
    id 125
    label "przyswajanie"
  ]
  node [
    id 126
    label "feminizm"
  ]
  node [
    id 127
    label "Unia_Europejska"
  ]
  node [
    id 128
    label "odtwarzanie"
  ]
  node [
    id 129
    label "uatrakcyjnianie"
  ]
  node [
    id 130
    label "zast&#281;powanie"
  ]
  node [
    id 131
    label "odbudowywanie"
  ]
  node [
    id 132
    label "rejuvenation"
  ]
  node [
    id 133
    label "m&#322;odszy"
  ]
  node [
    id 134
    label "odbudowywa&#263;"
  ]
  node [
    id 135
    label "m&#322;odzi&#263;"
  ]
  node [
    id 136
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 137
    label "przewietrza&#263;"
  ]
  node [
    id 138
    label "wymienia&#263;"
  ]
  node [
    id 139
    label "odtwarza&#263;"
  ]
  node [
    id 140
    label "uatrakcyjni&#263;"
  ]
  node [
    id 141
    label "przewietrzy&#263;"
  ]
  node [
    id 142
    label "regenerate"
  ]
  node [
    id 143
    label "odtworzy&#263;"
  ]
  node [
    id 144
    label "wymieni&#263;"
  ]
  node [
    id 145
    label "odbudowa&#263;"
  ]
  node [
    id 146
    label "wymienienie"
  ]
  node [
    id 147
    label "uatrakcyjnienie"
  ]
  node [
    id 148
    label "odbudowanie"
  ]
  node [
    id 149
    label "odtworzenie"
  ]
  node [
    id 150
    label "asymilowanie_si&#281;"
  ]
  node [
    id 151
    label "absorption"
  ]
  node [
    id 152
    label "pobieranie"
  ]
  node [
    id 153
    label "czerpanie"
  ]
  node [
    id 154
    label "acquisition"
  ]
  node [
    id 155
    label "zmienianie"
  ]
  node [
    id 156
    label "cz&#322;owiek"
  ]
  node [
    id 157
    label "organizm"
  ]
  node [
    id 158
    label "assimilation"
  ]
  node [
    id 159
    label "upodabnianie"
  ]
  node [
    id 160
    label "g&#322;oska"
  ]
  node [
    id 161
    label "kultura"
  ]
  node [
    id 162
    label "podobny"
  ]
  node [
    id 163
    label "fonetyka"
  ]
  node [
    id 164
    label "mecz_mistrzowski"
  ]
  node [
    id 165
    label "&#347;rodowisko"
  ]
  node [
    id 166
    label "arrangement"
  ]
  node [
    id 167
    label "obrona"
  ]
  node [
    id 168
    label "pomoc"
  ]
  node [
    id 169
    label "organizacja"
  ]
  node [
    id 170
    label "rezerwa"
  ]
  node [
    id 171
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 172
    label "pr&#243;ba"
  ]
  node [
    id 173
    label "atak"
  ]
  node [
    id 174
    label "moneta"
  ]
  node [
    id 175
    label "union"
  ]
  node [
    id 176
    label "assimilate"
  ]
  node [
    id 177
    label "dostosowywa&#263;"
  ]
  node [
    id 178
    label "dostosowa&#263;"
  ]
  node [
    id 179
    label "przejmowa&#263;"
  ]
  node [
    id 180
    label "upodobni&#263;"
  ]
  node [
    id 181
    label "przej&#261;&#263;"
  ]
  node [
    id 182
    label "upodabnia&#263;"
  ]
  node [
    id 183
    label "pobiera&#263;"
  ]
  node [
    id 184
    label "pobra&#263;"
  ]
  node [
    id 185
    label "typ"
  ]
  node [
    id 186
    label "jednostka_administracyjna"
  ]
  node [
    id 187
    label "zoologia"
  ]
  node [
    id 188
    label "skupienie"
  ]
  node [
    id 189
    label "kr&#243;lestwo"
  ]
  node [
    id 190
    label "tribe"
  ]
  node [
    id 191
    label "hurma"
  ]
  node [
    id 192
    label "botanika"
  ]
  node [
    id 193
    label "carat"
  ]
  node [
    id 194
    label "bia&#322;y_murzyn"
  ]
  node [
    id 195
    label "Rosjanin"
  ]
  node [
    id 196
    label "bia&#322;e"
  ]
  node [
    id 197
    label "jasnosk&#243;ry"
  ]
  node [
    id 198
    label "bierka_szachowa"
  ]
  node [
    id 199
    label "bia&#322;y_taniec"
  ]
  node [
    id 200
    label "dzia&#322;acz"
  ]
  node [
    id 201
    label "bezbarwny"
  ]
  node [
    id 202
    label "dobry"
  ]
  node [
    id 203
    label "siwy"
  ]
  node [
    id 204
    label "bia&#322;a_n&#281;dza"
  ]
  node [
    id 205
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 206
    label "Polak"
  ]
  node [
    id 207
    label "medyczny"
  ]
  node [
    id 208
    label "bia&#322;o"
  ]
  node [
    id 209
    label "typ_orientalny"
  ]
  node [
    id 210
    label "libera&#322;"
  ]
  node [
    id 211
    label "czysty"
  ]
  node [
    id 212
    label "&#347;nie&#380;nie"
  ]
  node [
    id 213
    label "konserwatysta"
  ]
  node [
    id 214
    label "&#347;nie&#380;no"
  ]
  node [
    id 215
    label "bia&#322;as"
  ]
  node [
    id 216
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 217
    label "blady"
  ]
  node [
    id 218
    label "bia&#322;osk&#243;ry"
  ]
  node [
    id 219
    label "nacjonalista"
  ]
  node [
    id 220
    label "jasny"
  ]
  node [
    id 221
    label "zimowo"
  ]
  node [
    id 222
    label "&#347;nie&#380;ny"
  ]
  node [
    id 223
    label "&#347;niegowy"
  ]
  node [
    id 224
    label "jaskrawo"
  ]
  node [
    id 225
    label "&#347;nie&#380;nobia&#322;y"
  ]
  node [
    id 226
    label "leczniczy"
  ]
  node [
    id 227
    label "lekarsko"
  ]
  node [
    id 228
    label "medycznie"
  ]
  node [
    id 229
    label "paramedyczny"
  ]
  node [
    id 230
    label "profilowy"
  ]
  node [
    id 231
    label "praktyczny"
  ]
  node [
    id 232
    label "specjalistyczny"
  ]
  node [
    id 233
    label "zgodny"
  ]
  node [
    id 234
    label "specjalny"
  ]
  node [
    id 235
    label "monarchia"
  ]
  node [
    id 236
    label "blado"
  ]
  node [
    id 237
    label "jasno"
  ]
  node [
    id 238
    label "bezbarwnie"
  ]
  node [
    id 239
    label "o&#347;wietlenie"
  ]
  node [
    id 240
    label "szczery"
  ]
  node [
    id 241
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 242
    label "o&#347;wietlanie"
  ]
  node [
    id 243
    label "przytomny"
  ]
  node [
    id 244
    label "zrozumia&#322;y"
  ]
  node [
    id 245
    label "niezm&#261;cony"
  ]
  node [
    id 246
    label "jednoznaczny"
  ]
  node [
    id 247
    label "klarowny"
  ]
  node [
    id 248
    label "pogodny"
  ]
  node [
    id 249
    label "bia&#322;a_bierka"
  ]
  node [
    id 250
    label "strona"
  ]
  node [
    id 251
    label "radyka&#322;"
  ]
  node [
    id 252
    label "prawicowiec"
  ]
  node [
    id 253
    label "patriota"
  ]
  node [
    id 254
    label "konserwa"
  ]
  node [
    id 255
    label "zachowawca"
  ]
  node [
    id 256
    label "kacap"
  ]
  node [
    id 257
    label "Sto&#322;ypin"
  ]
  node [
    id 258
    label "Gorbaczow"
  ]
  node [
    id 259
    label "Miczurin"
  ]
  node [
    id 260
    label "Jesienin"
  ]
  node [
    id 261
    label "mieszkaniec"
  ]
  node [
    id 262
    label "Moskal"
  ]
  node [
    id 263
    label "Trocki"
  ]
  node [
    id 264
    label "Lenin"
  ]
  node [
    id 265
    label "Rusek"
  ]
  node [
    id 266
    label "Potiomkin"
  ]
  node [
    id 267
    label "Puszkin"
  ]
  node [
    id 268
    label "Wielkorusin"
  ]
  node [
    id 269
    label "Strawi&#324;ski"
  ]
  node [
    id 270
    label "S&#322;owianin"
  ]
  node [
    id 271
    label "Gogol"
  ]
  node [
    id 272
    label "To&#322;stoj"
  ]
  node [
    id 273
    label "Jelcyn"
  ]
  node [
    id 274
    label "niezabawny"
  ]
  node [
    id 275
    label "zblakni&#281;cie"
  ]
  node [
    id 276
    label "nieciekawy"
  ]
  node [
    id 277
    label "zwyczajny"
  ]
  node [
    id 278
    label "blakni&#281;cie"
  ]
  node [
    id 279
    label "wyburza&#322;y"
  ]
  node [
    id 280
    label "oboj&#281;tny"
  ]
  node [
    id 281
    label "zniszczony"
  ]
  node [
    id 282
    label "poszarzenie"
  ]
  node [
    id 283
    label "nudny"
  ]
  node [
    id 284
    label "szarzenie"
  ]
  node [
    id 285
    label "wyblak&#322;y"
  ]
  node [
    id 286
    label "pewny"
  ]
  node [
    id 287
    label "przezroczy&#347;cie"
  ]
  node [
    id 288
    label "nieemisyjny"
  ]
  node [
    id 289
    label "bezb&#322;&#281;dny"
  ]
  node [
    id 290
    label "kompletny"
  ]
  node [
    id 291
    label "umycie"
  ]
  node [
    id 292
    label "ekologiczny"
  ]
  node [
    id 293
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 294
    label "bezpieczny"
  ]
  node [
    id 295
    label "dopuszczalny"
  ]
  node [
    id 296
    label "ca&#322;y"
  ]
  node [
    id 297
    label "mycie"
  ]
  node [
    id 298
    label "jednolity"
  ]
  node [
    id 299
    label "udany"
  ]
  node [
    id 300
    label "czysto"
  ]
  node [
    id 301
    label "klarowanie"
  ]
  node [
    id 302
    label "bezchmurny"
  ]
  node [
    id 303
    label "ostry"
  ]
  node [
    id 304
    label "legalny"
  ]
  node [
    id 305
    label "wyczyszczenie_si&#281;"
  ]
  node [
    id 306
    label "prze&#378;roczy"
  ]
  node [
    id 307
    label "wolny"
  ]
  node [
    id 308
    label "czyszczenie_si&#281;"
  ]
  node [
    id 309
    label "uczciwy"
  ]
  node [
    id 310
    label "do_czysta"
  ]
  node [
    id 311
    label "klarowanie_si&#281;"
  ]
  node [
    id 312
    label "sklarowanie"
  ]
  node [
    id 313
    label "zdrowy"
  ]
  node [
    id 314
    label "prawdziwy"
  ]
  node [
    id 315
    label "przyjemny"
  ]
  node [
    id 316
    label "cnotliwy"
  ]
  node [
    id 317
    label "ewidentny"
  ]
  node [
    id 318
    label "wspinaczka"
  ]
  node [
    id 319
    label "porz&#261;dny"
  ]
  node [
    id 320
    label "schludny"
  ]
  node [
    id 321
    label "doskona&#322;y"
  ]
  node [
    id 322
    label "orze&#378;wiaj&#261;cy"
  ]
  node [
    id 323
    label "nieodrodny"
  ]
  node [
    id 324
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 325
    label "nieatrakcyjny"
  ]
  node [
    id 326
    label "mizerny"
  ]
  node [
    id 327
    label "nienasycony"
  ]
  node [
    id 328
    label "s&#322;aby"
  ]
  node [
    id 329
    label "niewa&#380;ny"
  ]
  node [
    id 330
    label "ch&#322;odny"
  ]
  node [
    id 331
    label "Mro&#380;ek"
  ]
  node [
    id 332
    label "Ko&#347;ciuszko"
  ]
  node [
    id 333
    label "Saba&#322;a"
  ]
  node [
    id 334
    label "Europejczyk"
  ]
  node [
    id 335
    label "Lach"
  ]
  node [
    id 336
    label "Anders"
  ]
  node [
    id 337
    label "Boles&#322;aw_Chrobry"
  ]
  node [
    id 338
    label "Jakub_Wujek"
  ]
  node [
    id 339
    label "Polaczek"
  ]
  node [
    id 340
    label "Kie&#347;lowski"
  ]
  node [
    id 341
    label "Daniel_Dubicki"
  ]
  node [
    id 342
    label "&#346;ledzi&#324;ski"
  ]
  node [
    id 343
    label "Pola&#324;ski"
  ]
  node [
    id 344
    label "Towia&#324;ski"
  ]
  node [
    id 345
    label "Zanussi"
  ]
  node [
    id 346
    label "Wajda"
  ]
  node [
    id 347
    label "Pi&#322;sudski"
  ]
  node [
    id 348
    label "Owsiak"
  ]
  node [
    id 349
    label "Asnyk"
  ]
  node [
    id 350
    label "Daniel_Olbrychski"
  ]
  node [
    id 351
    label "Conrad"
  ]
  node [
    id 352
    label "Ma&#322;ysz"
  ]
  node [
    id 353
    label "Wojciech_Mann"
  ]
  node [
    id 354
    label "farmazon"
  ]
  node [
    id 355
    label "Korwin"
  ]
  node [
    id 356
    label "Michnik"
  ]
  node [
    id 357
    label "cz&#322;onek"
  ]
  node [
    id 358
    label "dobroczynny"
  ]
  node [
    id 359
    label "czw&#243;rka"
  ]
  node [
    id 360
    label "spokojny"
  ]
  node [
    id 361
    label "skuteczny"
  ]
  node [
    id 362
    label "&#347;mieszny"
  ]
  node [
    id 363
    label "mi&#322;y"
  ]
  node [
    id 364
    label "grzeczny"
  ]
  node [
    id 365
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 366
    label "powitanie"
  ]
  node [
    id 367
    label "dobrze"
  ]
  node [
    id 368
    label "zwrot"
  ]
  node [
    id 369
    label "pomy&#347;lny"
  ]
  node [
    id 370
    label "moralny"
  ]
  node [
    id 371
    label "drogi"
  ]
  node [
    id 372
    label "pozytywny"
  ]
  node [
    id 373
    label "odpowiedni"
  ]
  node [
    id 374
    label "korzystny"
  ]
  node [
    id 375
    label "pos&#322;uszny"
  ]
  node [
    id 376
    label "ludzko&#347;&#263;"
  ]
  node [
    id 377
    label "wapniak"
  ]
  node [
    id 378
    label "os&#322;abia&#263;"
  ]
  node [
    id 379
    label "posta&#263;"
  ]
  node [
    id 380
    label "hominid"
  ]
  node [
    id 381
    label "podw&#322;adny"
  ]
  node [
    id 382
    label "os&#322;abianie"
  ]
  node [
    id 383
    label "g&#322;owa"
  ]
  node [
    id 384
    label "figura"
  ]
  node [
    id 385
    label "portrecista"
  ]
  node [
    id 386
    label "dwun&#243;g"
  ]
  node [
    id 387
    label "profanum"
  ]
  node [
    id 388
    label "mikrokosmos"
  ]
  node [
    id 389
    label "nasada"
  ]
  node [
    id 390
    label "duch"
  ]
  node [
    id 391
    label "antropochoria"
  ]
  node [
    id 392
    label "osoba"
  ]
  node [
    id 393
    label "wz&#243;r"
  ]
  node [
    id 394
    label "senior"
  ]
  node [
    id 395
    label "oddzia&#322;ywanie"
  ]
  node [
    id 396
    label "Adam"
  ]
  node [
    id 397
    label "homo_sapiens"
  ]
  node [
    id 398
    label "polifag"
  ]
  node [
    id 399
    label "siwienie"
  ]
  node [
    id 400
    label "go&#322;&#261;b"
  ]
  node [
    id 401
    label "posiwienie"
  ]
  node [
    id 402
    label "siwo"
  ]
  node [
    id 403
    label "szymel"
  ]
  node [
    id 404
    label "ko&#324;"
  ]
  node [
    id 405
    label "jasnoszary"
  ]
  node [
    id 406
    label "pobielenie"
  ]
  node [
    id 407
    label "nieprzejrzysty"
  ]
  node [
    id 408
    label "albinos"
  ]
  node [
    id 409
    label "prawnicy"
  ]
  node [
    id 410
    label "Machiavelli"
  ]
  node [
    id 411
    label "jurysta"
  ]
  node [
    id 412
    label "specjalista"
  ]
  node [
    id 413
    label "aplikant"
  ]
  node [
    id 414
    label "student"
  ]
  node [
    id 415
    label "indeks"
  ]
  node [
    id 416
    label "s&#322;uchacz"
  ]
  node [
    id 417
    label "immatrykulowanie"
  ]
  node [
    id 418
    label "absolwent"
  ]
  node [
    id 419
    label "immatrykulowa&#263;"
  ]
  node [
    id 420
    label "akademik"
  ]
  node [
    id 421
    label "tutor"
  ]
  node [
    id 422
    label "znawca"
  ]
  node [
    id 423
    label "wyspecjalizowanie_si&#281;"
  ]
  node [
    id 424
    label "lekarz"
  ]
  node [
    id 425
    label "spec"
  ]
  node [
    id 426
    label "renesans"
  ]
  node [
    id 427
    label "sta&#380;ysta"
  ]
  node [
    id 428
    label "model"
  ]
  node [
    id 429
    label "intencja"
  ]
  node [
    id 430
    label "punkt"
  ]
  node [
    id 431
    label "rysunek"
  ]
  node [
    id 432
    label "miejsce_pracy"
  ]
  node [
    id 433
    label "przestrze&#324;"
  ]
  node [
    id 434
    label "device"
  ]
  node [
    id 435
    label "pomys&#322;"
  ]
  node [
    id 436
    label "obraz"
  ]
  node [
    id 437
    label "reprezentacja"
  ]
  node [
    id 438
    label "agreement"
  ]
  node [
    id 439
    label "dekoracja"
  ]
  node [
    id 440
    label "perspektywa"
  ]
  node [
    id 441
    label "kreska"
  ]
  node [
    id 442
    label "kszta&#322;t"
  ]
  node [
    id 443
    label "picture"
  ]
  node [
    id 444
    label "teka"
  ]
  node [
    id 445
    label "photograph"
  ]
  node [
    id 446
    label "ilustracja"
  ]
  node [
    id 447
    label "grafika"
  ]
  node [
    id 448
    label "plastyka"
  ]
  node [
    id 449
    label "shape"
  ]
  node [
    id 450
    label "dru&#380;yna"
  ]
  node [
    id 451
    label "emblemat"
  ]
  node [
    id 452
    label "deputation"
  ]
  node [
    id 453
    label "spos&#243;b"
  ]
  node [
    id 454
    label "prezenter"
  ]
  node [
    id 455
    label "mildew"
  ]
  node [
    id 456
    label "zi&#243;&#322;ko"
  ]
  node [
    id 457
    label "motif"
  ]
  node [
    id 458
    label "pozowanie"
  ]
  node [
    id 459
    label "ideal"
  ]
  node [
    id 460
    label "matryca"
  ]
  node [
    id 461
    label "adaptation"
  ]
  node [
    id 462
    label "ruch"
  ]
  node [
    id 463
    label "pozowa&#263;"
  ]
  node [
    id 464
    label "imitacja"
  ]
  node [
    id 465
    label "orygina&#322;"
  ]
  node [
    id 466
    label "facet"
  ]
  node [
    id 467
    label "miniatura"
  ]
  node [
    id 468
    label "rozdzielanie"
  ]
  node [
    id 469
    label "bezbrze&#380;e"
  ]
  node [
    id 470
    label "czasoprzestrze&#324;"
  ]
  node [
    id 471
    label "niezmierzony"
  ]
  node [
    id 472
    label "przedzielenie"
  ]
  node [
    id 473
    label "nielito&#347;ciwy"
  ]
  node [
    id 474
    label "rozdziela&#263;"
  ]
  node [
    id 475
    label "oktant"
  ]
  node [
    id 476
    label "miejsce"
  ]
  node [
    id 477
    label "przedzieli&#263;"
  ]
  node [
    id 478
    label "przestw&#243;r"
  ]
  node [
    id 479
    label "representation"
  ]
  node [
    id 480
    label "effigy"
  ]
  node [
    id 481
    label "podobrazie"
  ]
  node [
    id 482
    label "scena"
  ]
  node [
    id 483
    label "human_body"
  ]
  node [
    id 484
    label "projekcja"
  ]
  node [
    id 485
    label "oprawia&#263;"
  ]
  node [
    id 486
    label "zjawisko"
  ]
  node [
    id 487
    label "postprodukcja"
  ]
  node [
    id 488
    label "t&#322;o"
  ]
  node [
    id 489
    label "inning"
  ]
  node [
    id 490
    label "pulment"
  ]
  node [
    id 491
    label "pogl&#261;d"
  ]
  node [
    id 492
    label "plama_barwna"
  ]
  node [
    id 493
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 494
    label "oprawianie"
  ]
  node [
    id 495
    label "sztafa&#380;"
  ]
  node [
    id 496
    label "parkiet"
  ]
  node [
    id 497
    label "opinion"
  ]
  node [
    id 498
    label "uj&#281;cie"
  ]
  node [
    id 499
    label "zaj&#347;cie"
  ]
  node [
    id 500
    label "persona"
  ]
  node [
    id 501
    label "filmoteka"
  ]
  node [
    id 502
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 503
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 504
    label "ziarno"
  ]
  node [
    id 505
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 506
    label "wypunktowa&#263;"
  ]
  node [
    id 507
    label "ostro&#347;&#263;"
  ]
  node [
    id 508
    label "malarz"
  ]
  node [
    id 509
    label "napisy"
  ]
  node [
    id 510
    label "przeplot"
  ]
  node [
    id 511
    label "punktowa&#263;"
  ]
  node [
    id 512
    label "anamorfoza"
  ]
  node [
    id 513
    label "przedstawienie"
  ]
  node [
    id 514
    label "ty&#322;&#243;wka"
  ]
  node [
    id 515
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 516
    label "widok"
  ]
  node [
    id 517
    label "czo&#322;&#243;wka"
  ]
  node [
    id 518
    label "rola"
  ]
  node [
    id 519
    label "thinking"
  ]
  node [
    id 520
    label "pocz&#261;tki"
  ]
  node [
    id 521
    label "ukra&#347;&#263;"
  ]
  node [
    id 522
    label "ukradzenie"
  ]
  node [
    id 523
    label "idea"
  ]
  node [
    id 524
    label "przedmiot"
  ]
  node [
    id 525
    label "p&#322;&#243;d"
  ]
  node [
    id 526
    label "work"
  ]
  node [
    id 527
    label "rezultat"
  ]
  node [
    id 528
    label "patrzenie"
  ]
  node [
    id 529
    label "figura_geometryczna"
  ]
  node [
    id 530
    label "dystans"
  ]
  node [
    id 531
    label "patrze&#263;"
  ]
  node [
    id 532
    label "decentracja"
  ]
  node [
    id 533
    label "anticipation"
  ]
  node [
    id 534
    label "krajobraz"
  ]
  node [
    id 535
    label "metoda"
  ]
  node [
    id 536
    label "expectation"
  ]
  node [
    id 537
    label "scene"
  ]
  node [
    id 538
    label "pojmowanie"
  ]
  node [
    id 539
    label "widzie&#263;"
  ]
  node [
    id 540
    label "prognoza"
  ]
  node [
    id 541
    label "tryb"
  ]
  node [
    id 542
    label "przestrzenno&#347;&#263;"
  ]
  node [
    id 543
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 544
    label "ferm"
  ]
  node [
    id 545
    label "upi&#281;kszanie"
  ]
  node [
    id 546
    label "adornment"
  ]
  node [
    id 547
    label "pi&#281;kniejszy"
  ]
  node [
    id 548
    label "sznurownia"
  ]
  node [
    id 549
    label "plan_zdj&#281;ciowy"
  ]
  node [
    id 550
    label "scenografia"
  ]
  node [
    id 551
    label "wystr&#243;j"
  ]
  node [
    id 552
    label "ozdoba"
  ]
  node [
    id 553
    label "po&#322;o&#380;enie"
  ]
  node [
    id 554
    label "sprawa"
  ]
  node [
    id 555
    label "ust&#281;p"
  ]
  node [
    id 556
    label "obiekt_matematyczny"
  ]
  node [
    id 557
    label "problemat"
  ]
  node [
    id 558
    label "plamka"
  ]
  node [
    id 559
    label "stopie&#324;_pisma"
  ]
  node [
    id 560
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 561
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 562
    label "mark"
  ]
  node [
    id 563
    label "chwila"
  ]
  node [
    id 564
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 565
    label "prosta"
  ]
  node [
    id 566
    label "problematyka"
  ]
  node [
    id 567
    label "zapunktowa&#263;"
  ]
  node [
    id 568
    label "podpunkt"
  ]
  node [
    id 569
    label "kres"
  ]
  node [
    id 570
    label "point"
  ]
  node [
    id 571
    label "pozycja"
  ]
  node [
    id 572
    label "commitment"
  ]
  node [
    id 573
    label "wierno&#347;&#263;"
  ]
  node [
    id 574
    label "ofiarno&#347;&#263;"
  ]
  node [
    id 575
    label "reciprocation"
  ]
  node [
    id 576
    label "odej&#347;cie"
  ]
  node [
    id 577
    label "dostarczenie"
  ]
  node [
    id 578
    label "prohibition"
  ]
  node [
    id 579
    label "za&#322;atwienie_si&#281;"
  ]
  node [
    id 580
    label "powr&#243;cenie"
  ]
  node [
    id 581
    label "doj&#347;cie"
  ]
  node [
    id 582
    label "danie"
  ]
  node [
    id 583
    label "przekazanie"
  ]
  node [
    id 584
    label "odst&#261;pienie"
  ]
  node [
    id 585
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 586
    label "prototype"
  ]
  node [
    id 587
    label "umieszczenie"
  ]
  node [
    id 588
    label "render"
  ]
  node [
    id 589
    label "pass"
  ]
  node [
    id 590
    label "odpowiedzenie"
  ]
  node [
    id 591
    label "sprzedanie"
  ]
  node [
    id 592
    label "zrobienie"
  ]
  node [
    id 593
    label "pr&#243;bowanie"
  ]
  node [
    id 594
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 595
    label "zademonstrowanie"
  ]
  node [
    id 596
    label "report"
  ]
  node [
    id 597
    label "obgadanie"
  ]
  node [
    id 598
    label "realizacja"
  ]
  node [
    id 599
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 600
    label "narration"
  ]
  node [
    id 601
    label "cyrk"
  ]
  node [
    id 602
    label "theatrical_performance"
  ]
  node [
    id 603
    label "opisanie"
  ]
  node [
    id 604
    label "malarstwo"
  ]
  node [
    id 605
    label "teatr"
  ]
  node [
    id 606
    label "ukazanie"
  ]
  node [
    id 607
    label "zapoznanie"
  ]
  node [
    id 608
    label "pokaz"
  ]
  node [
    id 609
    label "podanie"
  ]
  node [
    id 610
    label "ods&#322;ona"
  ]
  node [
    id 611
    label "exhibit"
  ]
  node [
    id 612
    label "pokazanie"
  ]
  node [
    id 613
    label "wyst&#261;pienie"
  ]
  node [
    id 614
    label "przedstawi&#263;"
  ]
  node [
    id 615
    label "przedstawianie"
  ]
  node [
    id 616
    label "przedstawia&#263;"
  ]
  node [
    id 617
    label "obiecanie"
  ]
  node [
    id 618
    label "zap&#322;acenie"
  ]
  node [
    id 619
    label "cios"
  ]
  node [
    id 620
    label "give"
  ]
  node [
    id 621
    label "udost&#281;pnienie"
  ]
  node [
    id 622
    label "rendition"
  ]
  node [
    id 623
    label "wymienienie_si&#281;"
  ]
  node [
    id 624
    label "eating"
  ]
  node [
    id 625
    label "coup"
  ]
  node [
    id 626
    label "hand"
  ]
  node [
    id 627
    label "uprawianie_seksu"
  ]
  node [
    id 628
    label "allow"
  ]
  node [
    id 629
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 630
    label "uderzenie"
  ]
  node [
    id 631
    label "zadanie"
  ]
  node [
    id 632
    label "powierzenie"
  ]
  node [
    id 633
    label "przeznaczenie"
  ]
  node [
    id 634
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 635
    label "dodanie"
  ]
  node [
    id 636
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 637
    label "wyposa&#380;enie"
  ]
  node [
    id 638
    label "czynno&#347;&#263;"
  ]
  node [
    id 639
    label "dostanie"
  ]
  node [
    id 640
    label "karta"
  ]
  node [
    id 641
    label "potrawa"
  ]
  node [
    id 642
    label "menu"
  ]
  node [
    id 643
    label "uderzanie"
  ]
  node [
    id 644
    label "jedzenie"
  ]
  node [
    id 645
    label "wyposa&#380;anie"
  ]
  node [
    id 646
    label "pobicie"
  ]
  node [
    id 647
    label "posi&#322;ek"
  ]
  node [
    id 648
    label "urz&#261;dzenie"
  ]
  node [
    id 649
    label "delivery"
  ]
  node [
    id 650
    label "spowodowanie"
  ]
  node [
    id 651
    label "nawodnienie"
  ]
  node [
    id 652
    label "przes&#322;anie"
  ]
  node [
    id 653
    label "wytworzenie"
  ]
  node [
    id 654
    label "dor&#281;czenie"
  ]
  node [
    id 655
    label "wys&#322;anie"
  ]
  node [
    id 656
    label "transfer"
  ]
  node [
    id 657
    label "wp&#322;acenie"
  ]
  node [
    id 658
    label "z&#322;o&#380;enie"
  ]
  node [
    id 659
    label "sygna&#322;"
  ]
  node [
    id 660
    label "defense"
  ]
  node [
    id 661
    label "cribbage"
  ]
  node [
    id 662
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 663
    label "bekni&#281;cie"
  ]
  node [
    id 664
    label "zareagowanie"
  ]
  node [
    id 665
    label "poniesienie"
  ]
  node [
    id 666
    label "narobienie"
  ]
  node [
    id 667
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 668
    label "creation"
  ]
  node [
    id 669
    label "porobienie"
  ]
  node [
    id 670
    label "poumieszczanie"
  ]
  node [
    id 671
    label "ustalenie"
  ]
  node [
    id 672
    label "uplasowanie"
  ]
  node [
    id 673
    label "ulokowanie_si&#281;"
  ]
  node [
    id 674
    label "prze&#322;adowanie"
  ]
  node [
    id 675
    label "layout"
  ]
  node [
    id 676
    label "pomieszczenie"
  ]
  node [
    id 677
    label "siedzenie"
  ]
  node [
    id 678
    label "zakrycie"
  ]
  node [
    id 679
    label "cession"
  ]
  node [
    id 680
    label "odsuni&#281;cie_si&#281;"
  ]
  node [
    id 681
    label "leave"
  ]
  node [
    id 682
    label "cesja"
  ]
  node [
    id 683
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 684
    label "odwr&#243;t"
  ]
  node [
    id 685
    label "wycofanie_si&#281;"
  ]
  node [
    id 686
    label "zdradzenie"
  ]
  node [
    id 687
    label "sprzedanie_si&#281;"
  ]
  node [
    id 688
    label "nastawienie"
  ]
  node [
    id 689
    label "Oblation"
  ]
  node [
    id 690
    label "pit"
  ]
  node [
    id 691
    label "nadanie"
  ]
  node [
    id 692
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 693
    label "przywi&#261;zanie"
  ]
  node [
    id 694
    label "dok&#322;adno&#347;&#263;"
  ]
  node [
    id 695
    label "baja"
  ]
  node [
    id 696
    label "postawa"
  ]
  node [
    id 697
    label "dochodzenie"
  ]
  node [
    id 698
    label "uzyskanie"
  ]
  node [
    id 699
    label "skill"
  ]
  node [
    id 700
    label "dochrapanie_si&#281;"
  ]
  node [
    id 701
    label "znajomo&#347;ci"
  ]
  node [
    id 702
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 703
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 704
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 705
    label "powi&#261;zanie"
  ]
  node [
    id 706
    label "entrance"
  ]
  node [
    id 707
    label "affiliation"
  ]
  node [
    id 708
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 709
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 710
    label "bodziec"
  ]
  node [
    id 711
    label "informacja"
  ]
  node [
    id 712
    label "dost&#281;p"
  ]
  node [
    id 713
    label "przesy&#322;ka"
  ]
  node [
    id 714
    label "gotowy"
  ]
  node [
    id 715
    label "avenue"
  ]
  node [
    id 716
    label "postrzeganie"
  ]
  node [
    id 717
    label "dodatek"
  ]
  node [
    id 718
    label "doznanie"
  ]
  node [
    id 719
    label "dojrza&#322;y"
  ]
  node [
    id 720
    label "dojechanie"
  ]
  node [
    id 721
    label "ingress"
  ]
  node [
    id 722
    label "strzelenie"
  ]
  node [
    id 723
    label "orzekni&#281;cie"
  ]
  node [
    id 724
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 725
    label "orgazm"
  ]
  node [
    id 726
    label "dolecenie"
  ]
  node [
    id 727
    label "rozpowszechnienie"
  ]
  node [
    id 728
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 729
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 730
    label "stanie_si&#281;"
  ]
  node [
    id 731
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 732
    label "dop&#322;ata"
  ]
  node [
    id 733
    label "przybycie"
  ]
  node [
    id 734
    label "ruszenie"
  ]
  node [
    id 735
    label "wr&#243;cenie"
  ]
  node [
    id 736
    label "zdarzenie_si&#281;"
  ]
  node [
    id 737
    label "odnowienie_si&#281;"
  ]
  node [
    id 738
    label "podj&#281;cie"
  ]
  node [
    id 739
    label "powracanie"
  ]
  node [
    id 740
    label "odzyskanie"
  ]
  node [
    id 741
    label "zawr&#243;cenie"
  ]
  node [
    id 742
    label "zostanie"
  ]
  node [
    id 743
    label "mini&#281;cie"
  ]
  node [
    id 744
    label "odumarcie"
  ]
  node [
    id 745
    label "dysponowanie_si&#281;"
  ]
  node [
    id 746
    label "ust&#261;pienie"
  ]
  node [
    id 747
    label "mogi&#322;a"
  ]
  node [
    id 748
    label "pomarcie"
  ]
  node [
    id 749
    label "opuszczenie"
  ]
  node [
    id 750
    label "zb&#281;dny"
  ]
  node [
    id 751
    label "spisanie_"
  ]
  node [
    id 752
    label "oddalenie_si&#281;"
  ]
  node [
    id 753
    label "defenestracja"
  ]
  node [
    id 754
    label "danie_sobie_spokoju"
  ]
  node [
    id 755
    label "&#380;ycie"
  ]
  node [
    id 756
    label "odrzut"
  ]
  node [
    id 757
    label "kres_&#380;ycia"
  ]
  node [
    id 758
    label "zwolnienie_si&#281;"
  ]
  node [
    id 759
    label "zdechni&#281;cie"
  ]
  node [
    id 760
    label "exit"
  ]
  node [
    id 761
    label "stracenie"
  ]
  node [
    id 762
    label "przestanie"
  ]
  node [
    id 763
    label "martwy"
  ]
  node [
    id 764
    label "szeol"
  ]
  node [
    id 765
    label "die"
  ]
  node [
    id 766
    label "oddzielenie_si&#281;"
  ]
  node [
    id 767
    label "deviation"
  ]
  node [
    id 768
    label "wydalenie"
  ]
  node [
    id 769
    label "&#380;a&#322;oba"
  ]
  node [
    id 770
    label "pogrzebanie"
  ]
  node [
    id 771
    label "sko&#324;czenie"
  ]
  node [
    id 772
    label "withdrawal"
  ]
  node [
    id 773
    label "porozchodzenie_si&#281;"
  ]
  node [
    id 774
    label "zabicie"
  ]
  node [
    id 775
    label "agonia"
  ]
  node [
    id 776
    label "po&#322;o&#380;enie_lachy"
  ]
  node [
    id 777
    label "usuni&#281;cie"
  ]
  node [
    id 778
    label "relinquishment"
  ]
  node [
    id 779
    label "p&#243;j&#347;cie"
  ]
  node [
    id 780
    label "poniechanie"
  ]
  node [
    id 781
    label "zako&#324;czenie"
  ]
  node [
    id 782
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 783
    label "wypisanie_si&#281;"
  ]
  node [
    id 784
    label "Mazowsze"
  ]
  node [
    id 785
    label "Anglia"
  ]
  node [
    id 786
    label "Amazonia"
  ]
  node [
    id 787
    label "Bordeaux"
  ]
  node [
    id 788
    label "Naddniestrze"
  ]
  node [
    id 789
    label "plantowa&#263;"
  ]
  node [
    id 790
    label "Europa_Zachodnia"
  ]
  node [
    id 791
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 792
    label "Armagnac"
  ]
  node [
    id 793
    label "zapadnia"
  ]
  node [
    id 794
    label "Zamojszczyzna"
  ]
  node [
    id 795
    label "Amhara"
  ]
  node [
    id 796
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 797
    label "budynek"
  ]
  node [
    id 798
    label "skorupa_ziemska"
  ]
  node [
    id 799
    label "Ma&#322;opolska"
  ]
  node [
    id 800
    label "Turkiestan"
  ]
  node [
    id 801
    label "Noworosja"
  ]
  node [
    id 802
    label "Mezoameryka"
  ]
  node [
    id 803
    label "glinowanie"
  ]
  node [
    id 804
    label "Lubelszczyzna"
  ]
  node [
    id 805
    label "Ba&#322;kany"
  ]
  node [
    id 806
    label "Kurdystan"
  ]
  node [
    id 807
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 808
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 809
    label "martwica"
  ]
  node [
    id 810
    label "Baszkiria"
  ]
  node [
    id 811
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 812
    label "Szkocja"
  ]
  node [
    id 813
    label "Tonkin"
  ]
  node [
    id 814
    label "Maghreb"
  ]
  node [
    id 815
    label "teren"
  ]
  node [
    id 816
    label "litosfera"
  ]
  node [
    id 817
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 818
    label "penetrator"
  ]
  node [
    id 819
    label "Nadrenia"
  ]
  node [
    id 820
    label "glinowa&#263;"
  ]
  node [
    id 821
    label "Wielkopolska"
  ]
  node [
    id 822
    label "Zabajkale"
  ]
  node [
    id 823
    label "Apulia"
  ]
  node [
    id 824
    label "domain"
  ]
  node [
    id 825
    label "Bojkowszczyzna"
  ]
  node [
    id 826
    label "podglebie"
  ]
  node [
    id 827
    label "kompleks_sorpcyjny"
  ]
  node [
    id 828
    label "Liguria"
  ]
  node [
    id 829
    label "Pamir"
  ]
  node [
    id 830
    label "Indochiny"
  ]
  node [
    id 831
    label "Podlasie"
  ]
  node [
    id 832
    label "Polinezja"
  ]
  node [
    id 833
    label "Kurpie"
  ]
  node [
    id 834
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 835
    label "S&#261;decczyzna"
  ]
  node [
    id 836
    label "Umbria"
  ]
  node [
    id 837
    label "Karaiby"
  ]
  node [
    id 838
    label "Ukraina_Zachodnia"
  ]
  node [
    id 839
    label "Kielecczyzna"
  ]
  node [
    id 840
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 841
    label "kort"
  ]
  node [
    id 842
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 843
    label "czynnik_produkcji"
  ]
  node [
    id 844
    label "Skandynawia"
  ]
  node [
    id 845
    label "Kujawy"
  ]
  node [
    id 846
    label "Tyrol"
  ]
  node [
    id 847
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 848
    label "Huculszczyzna"
  ]
  node [
    id 849
    label "pojazd"
  ]
  node [
    id 850
    label "Turyngia"
  ]
  node [
    id 851
    label "powierzchnia"
  ]
  node [
    id 852
    label "Toskania"
  ]
  node [
    id 853
    label "Podhale"
  ]
  node [
    id 854
    label "Bory_Tucholskie"
  ]
  node [
    id 855
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 856
    label "Kalabria"
  ]
  node [
    id 857
    label "pr&#243;chnica"
  ]
  node [
    id 858
    label "Hercegowina"
  ]
  node [
    id 859
    label "Lotaryngia"
  ]
  node [
    id 860
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 861
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 862
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 863
    label "Walia"
  ]
  node [
    id 864
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 865
    label "Opolskie"
  ]
  node [
    id 866
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 867
    label "Kampania"
  ]
  node [
    id 868
    label "Sand&#380;ak"
  ]
  node [
    id 869
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 870
    label "Syjon"
  ]
  node [
    id 871
    label "Kabylia"
  ]
  node [
    id 872
    label "ryzosfera"
  ]
  node [
    id 873
    label "Lombardia"
  ]
  node [
    id 874
    label "Warmia"
  ]
  node [
    id 875
    label "Kaszmir"
  ]
  node [
    id 876
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 877
    label "&#321;&#243;dzkie"
  ]
  node [
    id 878
    label "Kaukaz"
  ]
  node [
    id 879
    label "Europa_Wschodnia"
  ]
  node [
    id 880
    label "Biskupizna"
  ]
  node [
    id 881
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 882
    label "Afryka_Wschodnia"
  ]
  node [
    id 883
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 884
    label "Podkarpacie"
  ]
  node [
    id 885
    label "obszar"
  ]
  node [
    id 886
    label "Afryka_Zachodnia"
  ]
  node [
    id 887
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 888
    label "Bo&#347;nia"
  ]
  node [
    id 889
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 890
    label "p&#322;aszczyzna"
  ]
  node [
    id 891
    label "dotleni&#263;"
  ]
  node [
    id 892
    label "Oceania"
  ]
  node [
    id 893
    label "Pomorze_Zachodnie"
  ]
  node [
    id 894
    label "Powi&#347;le"
  ]
  node [
    id 895
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 896
    label "Podbeskidzie"
  ]
  node [
    id 897
    label "&#321;emkowszczyzna"
  ]
  node [
    id 898
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 899
    label "Opolszczyzna"
  ]
  node [
    id 900
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 901
    label "Kaszuby"
  ]
  node [
    id 902
    label "Ko&#322;yma"
  ]
  node [
    id 903
    label "Szlezwik"
  ]
  node [
    id 904
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 905
    label "glej"
  ]
  node [
    id 906
    label "Mikronezja"
  ]
  node [
    id 907
    label "pa&#324;stwo"
  ]
  node [
    id 908
    label "posadzka"
  ]
  node [
    id 909
    label "Polesie"
  ]
  node [
    id 910
    label "Kerala"
  ]
  node [
    id 911
    label "Mazury"
  ]
  node [
    id 912
    label "Palestyna"
  ]
  node [
    id 913
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 914
    label "Lauda"
  ]
  node [
    id 915
    label "Azja_Wschodnia"
  ]
  node [
    id 916
    label "Galicja"
  ]
  node [
    id 917
    label "Zakarpacie"
  ]
  node [
    id 918
    label "Lubuskie"
  ]
  node [
    id 919
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 920
    label "Laponia"
  ]
  node [
    id 921
    label "Yorkshire"
  ]
  node [
    id 922
    label "Bawaria"
  ]
  node [
    id 923
    label "Zag&#243;rze"
  ]
  node [
    id 924
    label "geosystem"
  ]
  node [
    id 925
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 926
    label "Andaluzja"
  ]
  node [
    id 927
    label "&#379;ywiecczyzna"
  ]
  node [
    id 928
    label "Oksytania"
  ]
  node [
    id 929
    label "Kociewie"
  ]
  node [
    id 930
    label "Lasko"
  ]
  node [
    id 931
    label "warunek_lokalowy"
  ]
  node [
    id 932
    label "plac"
  ]
  node [
    id 933
    label "location"
  ]
  node [
    id 934
    label "uwaga"
  ]
  node [
    id 935
    label "status"
  ]
  node [
    id 936
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 937
    label "cia&#322;o"
  ]
  node [
    id 938
    label "cecha"
  ]
  node [
    id 939
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 940
    label "praca"
  ]
  node [
    id 941
    label "rz&#261;d"
  ]
  node [
    id 942
    label "tkanina_we&#322;niana"
  ]
  node [
    id 943
    label "boisko"
  ]
  node [
    id 944
    label "siatka"
  ]
  node [
    id 945
    label "ubrani&#243;wka"
  ]
  node [
    id 946
    label "p&#243;&#322;noc"
  ]
  node [
    id 947
    label "Kosowo"
  ]
  node [
    id 948
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 949
    label "Zab&#322;ocie"
  ]
  node [
    id 950
    label "zach&#243;d"
  ]
  node [
    id 951
    label "po&#322;udnie"
  ]
  node [
    id 952
    label "Pow&#261;zki"
  ]
  node [
    id 953
    label "Piotrowo"
  ]
  node [
    id 954
    label "Olszanica"
  ]
  node [
    id 955
    label "holarktyka"
  ]
  node [
    id 956
    label "Ruda_Pabianicka"
  ]
  node [
    id 957
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 958
    label "Ludwin&#243;w"
  ]
  node [
    id 959
    label "Arktyka"
  ]
  node [
    id 960
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 961
    label "Zabu&#380;e"
  ]
  node [
    id 962
    label "antroposfera"
  ]
  node [
    id 963
    label "terytorium"
  ]
  node [
    id 964
    label "Neogea"
  ]
  node [
    id 965
    label "Syberia_Zachodnia"
  ]
  node [
    id 966
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 967
    label "zakres"
  ]
  node [
    id 968
    label "pas_planetoid"
  ]
  node [
    id 969
    label "Syberia_Wschodnia"
  ]
  node [
    id 970
    label "Antarktyka"
  ]
  node [
    id 971
    label "Rakowice"
  ]
  node [
    id 972
    label "akrecja"
  ]
  node [
    id 973
    label "wymiar"
  ]
  node [
    id 974
    label "&#321;&#281;g"
  ]
  node [
    id 975
    label "Kresy_Zachodnie"
  ]
  node [
    id 976
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 977
    label "wsch&#243;d"
  ]
  node [
    id 978
    label "Notogea"
  ]
  node [
    id 979
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 980
    label "mienie"
  ]
  node [
    id 981
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 982
    label "stan"
  ]
  node [
    id 983
    label "rzecz"
  ]
  node [
    id 984
    label "immoblizacja"
  ]
  node [
    id 985
    label "&#347;ciana"
  ]
  node [
    id 986
    label "surface"
  ]
  node [
    id 987
    label "kwadrant"
  ]
  node [
    id 988
    label "degree"
  ]
  node [
    id 989
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 990
    label "ukszta&#322;towanie"
  ]
  node [
    id 991
    label "p&#322;aszczak"
  ]
  node [
    id 992
    label "kontekst"
  ]
  node [
    id 993
    label "nation"
  ]
  node [
    id 994
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 995
    label "przyroda"
  ]
  node [
    id 996
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 997
    label "w&#322;adza"
  ]
  node [
    id 998
    label "rozmiar"
  ]
  node [
    id 999
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 1000
    label "zwierciad&#322;o"
  ]
  node [
    id 1001
    label "capacity"
  ]
  node [
    id 1002
    label "plane"
  ]
  node [
    id 1003
    label "gleba"
  ]
  node [
    id 1004
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 1005
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 1006
    label "warstwa"
  ]
  node [
    id 1007
    label "Ziemia"
  ]
  node [
    id 1008
    label "sialma"
  ]
  node [
    id 1009
    label "warstwa_perydotytowa"
  ]
  node [
    id 1010
    label "warstwa_granitowa"
  ]
  node [
    id 1011
    label "powietrze"
  ]
  node [
    id 1012
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1013
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 1014
    label "fauna"
  ]
  node [
    id 1015
    label "balkon"
  ]
  node [
    id 1016
    label "budowla"
  ]
  node [
    id 1017
    label "pod&#322;oga"
  ]
  node [
    id 1018
    label "kondygnacja"
  ]
  node [
    id 1019
    label "skrzyd&#322;o"
  ]
  node [
    id 1020
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 1021
    label "dach"
  ]
  node [
    id 1022
    label "strop"
  ]
  node [
    id 1023
    label "klatka_schodowa"
  ]
  node [
    id 1024
    label "przedpro&#380;e"
  ]
  node [
    id 1025
    label "Pentagon"
  ]
  node [
    id 1026
    label "alkierz"
  ]
  node [
    id 1027
    label "front"
  ]
  node [
    id 1028
    label "amfilada"
  ]
  node [
    id 1029
    label "apartment"
  ]
  node [
    id 1030
    label "sklepienie"
  ]
  node [
    id 1031
    label "sufit"
  ]
  node [
    id 1032
    label "zakamarek"
  ]
  node [
    id 1033
    label "odholowa&#263;"
  ]
  node [
    id 1034
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 1035
    label "tabor"
  ]
  node [
    id 1036
    label "przyholowywanie"
  ]
  node [
    id 1037
    label "przyholowa&#263;"
  ]
  node [
    id 1038
    label "przyholowanie"
  ]
  node [
    id 1039
    label "fukni&#281;cie"
  ]
  node [
    id 1040
    label "l&#261;d"
  ]
  node [
    id 1041
    label "zielona_karta"
  ]
  node [
    id 1042
    label "fukanie"
  ]
  node [
    id 1043
    label "przyholowywa&#263;"
  ]
  node [
    id 1044
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1045
    label "woda"
  ]
  node [
    id 1046
    label "przeszklenie"
  ]
  node [
    id 1047
    label "test_zderzeniowy"
  ]
  node [
    id 1048
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1049
    label "odzywka"
  ]
  node [
    id 1050
    label "nadwozie"
  ]
  node [
    id 1051
    label "odholowanie"
  ]
  node [
    id 1052
    label "prowadzenie_si&#281;"
  ]
  node [
    id 1053
    label "odholowywa&#263;"
  ]
  node [
    id 1054
    label "odholowywanie"
  ]
  node [
    id 1055
    label "hamulec"
  ]
  node [
    id 1056
    label "podwozie"
  ]
  node [
    id 1057
    label "wzbogacanie"
  ]
  node [
    id 1058
    label "zabezpieczanie"
  ]
  node [
    id 1059
    label "pokrywanie"
  ]
  node [
    id 1060
    label "aluminize"
  ]
  node [
    id 1061
    label "metalizowanie"
  ]
  node [
    id 1062
    label "metalizowa&#263;"
  ]
  node [
    id 1063
    label "wzbogaca&#263;"
  ]
  node [
    id 1064
    label "pokrywa&#263;"
  ]
  node [
    id 1065
    label "zabezpiecza&#263;"
  ]
  node [
    id 1066
    label "nasyci&#263;"
  ]
  node [
    id 1067
    label "od&#347;wie&#380;y&#263;_si&#281;"
  ]
  node [
    id 1068
    label "dostarczy&#263;"
  ]
  node [
    id 1069
    label "level"
  ]
  node [
    id 1070
    label "r&#243;wna&#263;"
  ]
  node [
    id 1071
    label "uprawia&#263;"
  ]
  node [
    id 1072
    label "Judea"
  ]
  node [
    id 1073
    label "moszaw"
  ]
  node [
    id 1074
    label "Kanaan"
  ]
  node [
    id 1075
    label "Algieria"
  ]
  node [
    id 1076
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1077
    label "Aruba"
  ]
  node [
    id 1078
    label "Jamajka"
  ]
  node [
    id 1079
    label "Kuba"
  ]
  node [
    id 1080
    label "Haiti"
  ]
  node [
    id 1081
    label "Kajmany"
  ]
  node [
    id 1082
    label "Portoryko"
  ]
  node [
    id 1083
    label "Anguilla"
  ]
  node [
    id 1084
    label "Bahamy"
  ]
  node [
    id 1085
    label "Antyle"
  ]
  node [
    id 1086
    label "Polska"
  ]
  node [
    id 1087
    label "Mogielnica"
  ]
  node [
    id 1088
    label "Indie"
  ]
  node [
    id 1089
    label "jezioro"
  ]
  node [
    id 1090
    label "Niemcy"
  ]
  node [
    id 1091
    label "Rumelia"
  ]
  node [
    id 1092
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1093
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1094
    label "Poprad"
  ]
  node [
    id 1095
    label "Tatry"
  ]
  node [
    id 1096
    label "Podtatrze"
  ]
  node [
    id 1097
    label "Podole"
  ]
  node [
    id 1098
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1099
    label "Hiszpania"
  ]
  node [
    id 1100
    label "Austro-W&#281;gry"
  ]
  node [
    id 1101
    label "W&#322;ochy"
  ]
  node [
    id 1102
    label "Biskupice"
  ]
  node [
    id 1103
    label "Iwanowice"
  ]
  node [
    id 1104
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1105
    label "Rogo&#378;nik"
  ]
  node [
    id 1106
    label "Ropa"
  ]
  node [
    id 1107
    label "Wietnam"
  ]
  node [
    id 1108
    label "Etiopia"
  ]
  node [
    id 1109
    label "Austria"
  ]
  node [
    id 1110
    label "Alpy"
  ]
  node [
    id 1111
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1112
    label "Francja"
  ]
  node [
    id 1113
    label "Wyspy_Marshalla"
  ]
  node [
    id 1114
    label "Nauru"
  ]
  node [
    id 1115
    label "Mariany"
  ]
  node [
    id 1116
    label "dolar"
  ]
  node [
    id 1117
    label "Karpaty"
  ]
  node [
    id 1118
    label "Samoa"
  ]
  node [
    id 1119
    label "Tonga"
  ]
  node [
    id 1120
    label "Tuwalu"
  ]
  node [
    id 1121
    label "Hawaje"
  ]
  node [
    id 1122
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1123
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1124
    label "Rosja"
  ]
  node [
    id 1125
    label "Beskid_Niski"
  ]
  node [
    id 1126
    label "Etruria"
  ]
  node [
    id 1127
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1128
    label "Bojanowo"
  ]
  node [
    id 1129
    label "Obra"
  ]
  node [
    id 1130
    label "Wilkowo_Polskie"
  ]
  node [
    id 1131
    label "Dobra"
  ]
  node [
    id 1132
    label "Buriacja"
  ]
  node [
    id 1133
    label "Rozewie"
  ]
  node [
    id 1134
    label "&#346;l&#261;sk"
  ]
  node [
    id 1135
    label "Czechy"
  ]
  node [
    id 1136
    label "Ukraina"
  ]
  node [
    id 1137
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1138
    label "Mo&#322;dawia"
  ]
  node [
    id 1139
    label "Norwegia"
  ]
  node [
    id 1140
    label "Szwecja"
  ]
  node [
    id 1141
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1142
    label "Finlandia"
  ]
  node [
    id 1143
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1144
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1145
    label "Wiktoria"
  ]
  node [
    id 1146
    label "Wielka_Brytania"
  ]
  node [
    id 1147
    label "Guernsey"
  ]
  node [
    id 1148
    label "funt_szterling"
  ]
  node [
    id 1149
    label "Portland"
  ]
  node [
    id 1150
    label "NATO"
  ]
  node [
    id 1151
    label "El&#380;bieta_I"
  ]
  node [
    id 1152
    label "Kornwalia"
  ]
  node [
    id 1153
    label "Amazonka"
  ]
  node [
    id 1154
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1155
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1156
    label "Libia"
  ]
  node [
    id 1157
    label "Maroko"
  ]
  node [
    id 1158
    label "Tunezja"
  ]
  node [
    id 1159
    label "Mauretania"
  ]
  node [
    id 1160
    label "Sahara_Zachodnia"
  ]
  node [
    id 1161
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1162
    label "Anglosas"
  ]
  node [
    id 1163
    label "Moza"
  ]
  node [
    id 1164
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1165
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1166
    label "Paj&#281;czno"
  ]
  node [
    id 1167
    label "Nowa_Zelandia"
  ]
  node [
    id 1168
    label "Ocean_Spokojny"
  ]
  node [
    id 1169
    label "Palau"
  ]
  node [
    id 1170
    label "Melanezja"
  ]
  node [
    id 1171
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1172
    label "Czarnog&#243;ra"
  ]
  node [
    id 1173
    label "Serbia"
  ]
  node [
    id 1174
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1175
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1176
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1177
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1178
    label "Gop&#322;o"
  ]
  node [
    id 1179
    label "Jerozolima"
  ]
  node [
    id 1180
    label "Dolna_Frankonia"
  ]
  node [
    id 1181
    label "funt_szkocki"
  ]
  node [
    id 1182
    label "Kaledonia"
  ]
  node [
    id 1183
    label "Czeczenia"
  ]
  node [
    id 1184
    label "Inguszetia"
  ]
  node [
    id 1185
    label "Abchazja"
  ]
  node [
    id 1186
    label "Sarmata"
  ]
  node [
    id 1187
    label "Dagestan"
  ]
  node [
    id 1188
    label "Eurazja"
  ]
  node [
    id 1189
    label "Warszawa"
  ]
  node [
    id 1190
    label "Mariensztat"
  ]
  node [
    id 1191
    label "Pakistan"
  ]
  node [
    id 1192
    label "Katar"
  ]
  node [
    id 1193
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1194
    label "Gwatemala"
  ]
  node [
    id 1195
    label "Afganistan"
  ]
  node [
    id 1196
    label "Ekwador"
  ]
  node [
    id 1197
    label "Tad&#380;ykistan"
  ]
  node [
    id 1198
    label "Bhutan"
  ]
  node [
    id 1199
    label "Argentyna"
  ]
  node [
    id 1200
    label "D&#380;ibuti"
  ]
  node [
    id 1201
    label "Wenezuela"
  ]
  node [
    id 1202
    label "Gabon"
  ]
  node [
    id 1203
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1204
    label "Rwanda"
  ]
  node [
    id 1205
    label "Liechtenstein"
  ]
  node [
    id 1206
    label "Sri_Lanka"
  ]
  node [
    id 1207
    label "Madagaskar"
  ]
  node [
    id 1208
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1209
    label "Kongo"
  ]
  node [
    id 1210
    label "Bangladesz"
  ]
  node [
    id 1211
    label "Kanada"
  ]
  node [
    id 1212
    label "Wehrlen"
  ]
  node [
    id 1213
    label "Surinam"
  ]
  node [
    id 1214
    label "Chile"
  ]
  node [
    id 1215
    label "Uganda"
  ]
  node [
    id 1216
    label "W&#281;gry"
  ]
  node [
    id 1217
    label "Birma"
  ]
  node [
    id 1218
    label "Kazachstan"
  ]
  node [
    id 1219
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1220
    label "Armenia"
  ]
  node [
    id 1221
    label "Timor_Wschodni"
  ]
  node [
    id 1222
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1223
    label "Izrael"
  ]
  node [
    id 1224
    label "Estonia"
  ]
  node [
    id 1225
    label "Komory"
  ]
  node [
    id 1226
    label "Kamerun"
  ]
  node [
    id 1227
    label "Belize"
  ]
  node [
    id 1228
    label "Sierra_Leone"
  ]
  node [
    id 1229
    label "Luksemburg"
  ]
  node [
    id 1230
    label "USA"
  ]
  node [
    id 1231
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1232
    label "Barbados"
  ]
  node [
    id 1233
    label "San_Marino"
  ]
  node [
    id 1234
    label "Bu&#322;garia"
  ]
  node [
    id 1235
    label "Indonezja"
  ]
  node [
    id 1236
    label "Malawi"
  ]
  node [
    id 1237
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1238
    label "partia"
  ]
  node [
    id 1239
    label "Zambia"
  ]
  node [
    id 1240
    label "Angola"
  ]
  node [
    id 1241
    label "Grenada"
  ]
  node [
    id 1242
    label "Nepal"
  ]
  node [
    id 1243
    label "Panama"
  ]
  node [
    id 1244
    label "Rumunia"
  ]
  node [
    id 1245
    label "Malediwy"
  ]
  node [
    id 1246
    label "S&#322;owacja"
  ]
  node [
    id 1247
    label "para"
  ]
  node [
    id 1248
    label "Egipt"
  ]
  node [
    id 1249
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1250
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1251
    label "Kolumbia"
  ]
  node [
    id 1252
    label "Mozambik"
  ]
  node [
    id 1253
    label "Laos"
  ]
  node [
    id 1254
    label "Burundi"
  ]
  node [
    id 1255
    label "Suazi"
  ]
  node [
    id 1256
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1257
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1258
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1259
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1260
    label "Dominika"
  ]
  node [
    id 1261
    label "Syria"
  ]
  node [
    id 1262
    label "Gwinea_Bissau"
  ]
  node [
    id 1263
    label "Liberia"
  ]
  node [
    id 1264
    label "Zimbabwe"
  ]
  node [
    id 1265
    label "Dominikana"
  ]
  node [
    id 1266
    label "Senegal"
  ]
  node [
    id 1267
    label "Gruzja"
  ]
  node [
    id 1268
    label "Togo"
  ]
  node [
    id 1269
    label "Chorwacja"
  ]
  node [
    id 1270
    label "Meksyk"
  ]
  node [
    id 1271
    label "Macedonia"
  ]
  node [
    id 1272
    label "Gujana"
  ]
  node [
    id 1273
    label "Zair"
  ]
  node [
    id 1274
    label "Albania"
  ]
  node [
    id 1275
    label "Kambod&#380;a"
  ]
  node [
    id 1276
    label "Mauritius"
  ]
  node [
    id 1277
    label "Monako"
  ]
  node [
    id 1278
    label "Gwinea"
  ]
  node [
    id 1279
    label "Mali"
  ]
  node [
    id 1280
    label "Nigeria"
  ]
  node [
    id 1281
    label "Kostaryka"
  ]
  node [
    id 1282
    label "Hanower"
  ]
  node [
    id 1283
    label "Paragwaj"
  ]
  node [
    id 1284
    label "Wyspy_Salomona"
  ]
  node [
    id 1285
    label "Seszele"
  ]
  node [
    id 1286
    label "Boliwia"
  ]
  node [
    id 1287
    label "Kirgistan"
  ]
  node [
    id 1288
    label "Irlandia"
  ]
  node [
    id 1289
    label "Czad"
  ]
  node [
    id 1290
    label "Irak"
  ]
  node [
    id 1291
    label "Lesoto"
  ]
  node [
    id 1292
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1293
    label "Malta"
  ]
  node [
    id 1294
    label "Andora"
  ]
  node [
    id 1295
    label "Chiny"
  ]
  node [
    id 1296
    label "Filipiny"
  ]
  node [
    id 1297
    label "Antarktis"
  ]
  node [
    id 1298
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1299
    label "Brazylia"
  ]
  node [
    id 1300
    label "Nikaragua"
  ]
  node [
    id 1301
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1302
    label "Kenia"
  ]
  node [
    id 1303
    label "Niger"
  ]
  node [
    id 1304
    label "Portugalia"
  ]
  node [
    id 1305
    label "Fid&#380;i"
  ]
  node [
    id 1306
    label "Botswana"
  ]
  node [
    id 1307
    label "Tajlandia"
  ]
  node [
    id 1308
    label "Australia"
  ]
  node [
    id 1309
    label "Burkina_Faso"
  ]
  node [
    id 1310
    label "interior"
  ]
  node [
    id 1311
    label "Benin"
  ]
  node [
    id 1312
    label "Tanzania"
  ]
  node [
    id 1313
    label "&#321;otwa"
  ]
  node [
    id 1314
    label "Kiribati"
  ]
  node [
    id 1315
    label "Rodezja"
  ]
  node [
    id 1316
    label "Cypr"
  ]
  node [
    id 1317
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1318
    label "Peru"
  ]
  node [
    id 1319
    label "Urugwaj"
  ]
  node [
    id 1320
    label "Jordania"
  ]
  node [
    id 1321
    label "Grecja"
  ]
  node [
    id 1322
    label "Azerbejd&#380;an"
  ]
  node [
    id 1323
    label "Turcja"
  ]
  node [
    id 1324
    label "Sudan"
  ]
  node [
    id 1325
    label "Oman"
  ]
  node [
    id 1326
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1327
    label "Uzbekistan"
  ]
  node [
    id 1328
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1329
    label "Honduras"
  ]
  node [
    id 1330
    label "Mongolia"
  ]
  node [
    id 1331
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1332
    label "Tajwan"
  ]
  node [
    id 1333
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1334
    label "Liban"
  ]
  node [
    id 1335
    label "Japonia"
  ]
  node [
    id 1336
    label "Ghana"
  ]
  node [
    id 1337
    label "Bahrajn"
  ]
  node [
    id 1338
    label "Belgia"
  ]
  node [
    id 1339
    label "Kuwejt"
  ]
  node [
    id 1340
    label "Litwa"
  ]
  node [
    id 1341
    label "S&#322;owenia"
  ]
  node [
    id 1342
    label "Szwajcaria"
  ]
  node [
    id 1343
    label "Erytrea"
  ]
  node [
    id 1344
    label "Arabia_Saudyjska"
  ]
  node [
    id 1345
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1346
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1347
    label "Malezja"
  ]
  node [
    id 1348
    label "Korea"
  ]
  node [
    id 1349
    label "Jemen"
  ]
  node [
    id 1350
    label "Namibia"
  ]
  node [
    id 1351
    label "holoarktyka"
  ]
  node [
    id 1352
    label "Brunei"
  ]
  node [
    id 1353
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1354
    label "Khitai"
  ]
  node [
    id 1355
    label "Iran"
  ]
  node [
    id 1356
    label "Gambia"
  ]
  node [
    id 1357
    label "Somalia"
  ]
  node [
    id 1358
    label "Holandia"
  ]
  node [
    id 1359
    label "Turkmenistan"
  ]
  node [
    id 1360
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1361
    label "Salwador"
  ]
  node [
    id 1362
    label "system_korzeniowy"
  ]
  node [
    id 1363
    label "bakteria"
  ]
  node [
    id 1364
    label "ubytek"
  ]
  node [
    id 1365
    label "fleczer"
  ]
  node [
    id 1366
    label "choroba_bakteryjna"
  ]
  node [
    id 1367
    label "schorzenie"
  ]
  node [
    id 1368
    label "kwas_huminowy"
  ]
  node [
    id 1369
    label "substancja_szara"
  ]
  node [
    id 1370
    label "tkanka"
  ]
  node [
    id 1371
    label "kom&#243;rka_glejowa"
  ]
  node [
    id 1372
    label "neuroglia"
  ]
  node [
    id 1373
    label "kamfenol"
  ]
  node [
    id 1374
    label "&#322;yko"
  ]
  node [
    id 1375
    label "necrosis"
  ]
  node [
    id 1376
    label "odle&#380;yna"
  ]
  node [
    id 1377
    label "zanikni&#281;cie"
  ]
  node [
    id 1378
    label "zmiana_wsteczna"
  ]
  node [
    id 1379
    label "ska&#322;a_osadowa"
  ]
  node [
    id 1380
    label "korek"
  ]
  node [
    id 1381
    label "pu&#322;apka"
  ]
  node [
    id 1382
    label "czarny"
  ]
  node [
    id 1383
    label "kawa"
  ]
  node [
    id 1384
    label "murzynek"
  ]
  node [
    id 1385
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 1386
    label "dripper"
  ]
  node [
    id 1387
    label "u&#380;ywka"
  ]
  node [
    id 1388
    label "egzotyk"
  ]
  node [
    id 1389
    label "marzanowate"
  ]
  node [
    id 1390
    label "nap&#243;j"
  ]
  node [
    id 1391
    label "produkt"
  ]
  node [
    id 1392
    label "pestkowiec"
  ]
  node [
    id 1393
    label "ro&#347;lina"
  ]
  node [
    id 1394
    label "porcja"
  ]
  node [
    id 1395
    label "kofeina"
  ]
  node [
    id 1396
    label "chemex"
  ]
  node [
    id 1397
    label "czarna_kawa"
  ]
  node [
    id 1398
    label "ma&#347;lak_pstry"
  ]
  node [
    id 1399
    label "ciasto"
  ]
  node [
    id 1400
    label "czarne"
  ]
  node [
    id 1401
    label "kolorowy"
  ]
  node [
    id 1402
    label "gorzki"
  ]
  node [
    id 1403
    label "murzy&#324;ski"
  ]
  node [
    id 1404
    label "ksi&#261;dz"
  ]
  node [
    id 1405
    label "przewrotny"
  ]
  node [
    id 1406
    label "ponury"
  ]
  node [
    id 1407
    label "beznadziejny"
  ]
  node [
    id 1408
    label "z&#322;y"
  ]
  node [
    id 1409
    label "wedel"
  ]
  node [
    id 1410
    label "czarnuch"
  ]
  node [
    id 1411
    label "granatowo"
  ]
  node [
    id 1412
    label "ciemny"
  ]
  node [
    id 1413
    label "negatywny"
  ]
  node [
    id 1414
    label "ciemnienie"
  ]
  node [
    id 1415
    label "czernienie"
  ]
  node [
    id 1416
    label "zaczernienie"
  ]
  node [
    id 1417
    label "pesymistycznie"
  ]
  node [
    id 1418
    label "abolicjonista"
  ]
  node [
    id 1419
    label "brudny"
  ]
  node [
    id 1420
    label "zaczernianie_si&#281;"
  ]
  node [
    id 1421
    label "kafar"
  ]
  node [
    id 1422
    label "czarnuchowaty"
  ]
  node [
    id 1423
    label "pessimistic"
  ]
  node [
    id 1424
    label "czarniawy"
  ]
  node [
    id 1425
    label "ciemnosk&#243;ry"
  ]
  node [
    id 1426
    label "okrutny"
  ]
  node [
    id 1427
    label "czarno"
  ]
  node [
    id 1428
    label "zaczernienie_si&#281;"
  ]
  node [
    id 1429
    label "niepomy&#347;lny"
  ]
  node [
    id 1430
    label "krzew"
  ]
  node [
    id 1431
    label "delfinidyna"
  ]
  node [
    id 1432
    label "pi&#380;maczkowate"
  ]
  node [
    id 1433
    label "ki&#347;&#263;"
  ]
  node [
    id 1434
    label "hy&#263;ka"
  ]
  node [
    id 1435
    label "kwiat"
  ]
  node [
    id 1436
    label "owoc"
  ]
  node [
    id 1437
    label "oliwkowate"
  ]
  node [
    id 1438
    label "lilac"
  ]
  node [
    id 1439
    label "kostka"
  ]
  node [
    id 1440
    label "kita"
  ]
  node [
    id 1441
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 1442
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 1443
    label "d&#322;o&#324;"
  ]
  node [
    id 1444
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 1445
    label "powerball"
  ]
  node [
    id 1446
    label "&#380;ubr"
  ]
  node [
    id 1447
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 1448
    label "p&#281;k"
  ]
  node [
    id 1449
    label "r&#281;ka"
  ]
  node [
    id 1450
    label "ogon"
  ]
  node [
    id 1451
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 1452
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 1453
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 1454
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 1455
    label "flakon"
  ]
  node [
    id 1456
    label "przykoronek"
  ]
  node [
    id 1457
    label "kielich"
  ]
  node [
    id 1458
    label "dno_kwiatowe"
  ]
  node [
    id 1459
    label "organ_ro&#347;linny"
  ]
  node [
    id 1460
    label "warga"
  ]
  node [
    id 1461
    label "korona"
  ]
  node [
    id 1462
    label "rurka"
  ]
  node [
    id 1463
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 1464
    label "karczowa&#263;"
  ]
  node [
    id 1465
    label "wykarczowanie"
  ]
  node [
    id 1466
    label "skupina"
  ]
  node [
    id 1467
    label "wykarczowa&#263;"
  ]
  node [
    id 1468
    label "karczowanie"
  ]
  node [
    id 1469
    label "fanerofit"
  ]
  node [
    id 1470
    label "zbiorowisko"
  ]
  node [
    id 1471
    label "ro&#347;liny"
  ]
  node [
    id 1472
    label "p&#281;d"
  ]
  node [
    id 1473
    label "wegetowanie"
  ]
  node [
    id 1474
    label "zadziorek"
  ]
  node [
    id 1475
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1476
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1477
    label "do&#322;owa&#263;"
  ]
  node [
    id 1478
    label "wegetacja"
  ]
  node [
    id 1479
    label "strzyc"
  ]
  node [
    id 1480
    label "w&#322;&#243;kno"
  ]
  node [
    id 1481
    label "g&#322;uszenie"
  ]
  node [
    id 1482
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1483
    label "fitotron"
  ]
  node [
    id 1484
    label "bulwka"
  ]
  node [
    id 1485
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1486
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1487
    label "epiderma"
  ]
  node [
    id 1488
    label "gumoza"
  ]
  node [
    id 1489
    label "strzy&#380;enie"
  ]
  node [
    id 1490
    label "wypotnik"
  ]
  node [
    id 1491
    label "flawonoid"
  ]
  node [
    id 1492
    label "wyro&#347;le"
  ]
  node [
    id 1493
    label "do&#322;owanie"
  ]
  node [
    id 1494
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1495
    label "pora&#380;a&#263;"
  ]
  node [
    id 1496
    label "fitocenoza"
  ]
  node [
    id 1497
    label "hodowla"
  ]
  node [
    id 1498
    label "fotoautotrof"
  ]
  node [
    id 1499
    label "nieuleczalnie_chory"
  ]
  node [
    id 1500
    label "wegetowa&#263;"
  ]
  node [
    id 1501
    label "pochewka"
  ]
  node [
    id 1502
    label "sok"
  ]
  node [
    id 1503
    label "zawi&#261;zek"
  ]
  node [
    id 1504
    label "pestka"
  ]
  node [
    id 1505
    label "mi&#261;&#380;sz"
  ]
  node [
    id 1506
    label "frukt"
  ]
  node [
    id 1507
    label "drylowanie"
  ]
  node [
    id 1508
    label "owocnia"
  ]
  node [
    id 1509
    label "fruktoza"
  ]
  node [
    id 1510
    label "gniazdo_nasienne"
  ]
  node [
    id 1511
    label "glukoza"
  ]
  node [
    id 1512
    label "antocyjanidyn"
  ]
  node [
    id 1513
    label "szczeciowce"
  ]
  node [
    id 1514
    label "jasnotowce"
  ]
  node [
    id 1515
    label "Oleaceae"
  ]
  node [
    id 1516
    label "wielkopolski"
  ]
  node [
    id 1517
    label "bez_czarny"
  ]
  node [
    id 1518
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 1519
    label "zap&#322;ata"
  ]
  node [
    id 1520
    label "refund"
  ]
  node [
    id 1521
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 1522
    label "return"
  ]
  node [
    id 1523
    label "kwota"
  ]
  node [
    id 1524
    label "konsekwencja"
  ]
  node [
    id 1525
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1526
    label "nagroda"
  ]
  node [
    id 1527
    label "compensate"
  ]
  node [
    id 1528
    label "wynagrodzenie"
  ]
  node [
    id 1529
    label "ponie&#347;&#263;"
  ]
  node [
    id 1530
    label "play"
  ]
  node [
    id 1531
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1532
    label "riot"
  ]
  node [
    id 1533
    label "carry"
  ]
  node [
    id 1534
    label "dozna&#263;"
  ]
  node [
    id 1535
    label "nak&#322;oni&#263;"
  ]
  node [
    id 1536
    label "wst&#261;pi&#263;"
  ]
  node [
    id 1537
    label "porwa&#263;"
  ]
  node [
    id 1538
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1539
    label "podejrzany"
  ]
  node [
    id 1540
    label "s&#261;downictwo"
  ]
  node [
    id 1541
    label "biuro"
  ]
  node [
    id 1542
    label "court"
  ]
  node [
    id 1543
    label "forum"
  ]
  node [
    id 1544
    label "bronienie"
  ]
  node [
    id 1545
    label "urz&#261;d"
  ]
  node [
    id 1546
    label "wydarzenie"
  ]
  node [
    id 1547
    label "oskar&#380;yciel"
  ]
  node [
    id 1548
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1549
    label "skazany"
  ]
  node [
    id 1550
    label "post&#281;powanie"
  ]
  node [
    id 1551
    label "broni&#263;"
  ]
  node [
    id 1552
    label "my&#347;l"
  ]
  node [
    id 1553
    label "pods&#261;dny"
  ]
  node [
    id 1554
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1555
    label "wypowied&#378;"
  ]
  node [
    id 1556
    label "instytucja"
  ]
  node [
    id 1557
    label "antylogizm"
  ]
  node [
    id 1558
    label "konektyw"
  ]
  node [
    id 1559
    label "&#347;wiadek"
  ]
  node [
    id 1560
    label "procesowicz"
  ]
  node [
    id 1561
    label "The_Beatles"
  ]
  node [
    id 1562
    label "zabudowania"
  ]
  node [
    id 1563
    label "group"
  ]
  node [
    id 1564
    label "zespolik"
  ]
  node [
    id 1565
    label "Depeche_Mode"
  ]
  node [
    id 1566
    label "batch"
  ]
  node [
    id 1567
    label "stanowisko"
  ]
  node [
    id 1568
    label "position"
  ]
  node [
    id 1569
    label "organ"
  ]
  node [
    id 1570
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 1571
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1572
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1573
    label "mianowaniec"
  ]
  node [
    id 1574
    label "okienko"
  ]
  node [
    id 1575
    label "przebiec"
  ]
  node [
    id 1576
    label "charakter"
  ]
  node [
    id 1577
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1578
    label "motyw"
  ]
  node [
    id 1579
    label "przebiegni&#281;cie"
  ]
  node [
    id 1580
    label "fabu&#322;a"
  ]
  node [
    id 1581
    label "osoba_prawna"
  ]
  node [
    id 1582
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1583
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1584
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1585
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1586
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1587
    label "Fundusze_Unijne"
  ]
  node [
    id 1588
    label "zamyka&#263;"
  ]
  node [
    id 1589
    label "establishment"
  ]
  node [
    id 1590
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1591
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1592
    label "afiliowa&#263;"
  ]
  node [
    id 1593
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1594
    label "standard"
  ]
  node [
    id 1595
    label "zamykanie"
  ]
  node [
    id 1596
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1597
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1598
    label "szko&#322;a"
  ]
  node [
    id 1599
    label "umys&#322;"
  ]
  node [
    id 1600
    label "political_orientation"
  ]
  node [
    id 1601
    label "istota"
  ]
  node [
    id 1602
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 1603
    label "fantomatyka"
  ]
  node [
    id 1604
    label "pos&#322;uchanie"
  ]
  node [
    id 1605
    label "sparafrazowanie"
  ]
  node [
    id 1606
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 1607
    label "strawestowa&#263;"
  ]
  node [
    id 1608
    label "sparafrazowa&#263;"
  ]
  node [
    id 1609
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 1610
    label "trawestowa&#263;"
  ]
  node [
    id 1611
    label "sformu&#322;owanie"
  ]
  node [
    id 1612
    label "parafrazowanie"
  ]
  node [
    id 1613
    label "ozdobnik"
  ]
  node [
    id 1614
    label "delimitacja"
  ]
  node [
    id 1615
    label "parafrazowa&#263;"
  ]
  node [
    id 1616
    label "stylizacja"
  ]
  node [
    id 1617
    label "komunikat"
  ]
  node [
    id 1618
    label "trawestowanie"
  ]
  node [
    id 1619
    label "strawestowanie"
  ]
  node [
    id 1620
    label "kognicja"
  ]
  node [
    id 1621
    label "campaign"
  ]
  node [
    id 1622
    label "rozprawa"
  ]
  node [
    id 1623
    label "zachowanie"
  ]
  node [
    id 1624
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1625
    label "fashion"
  ]
  node [
    id 1626
    label "robienie"
  ]
  node [
    id 1627
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1628
    label "zmierzanie"
  ]
  node [
    id 1629
    label "przes&#322;anka"
  ]
  node [
    id 1630
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 1631
    label "kazanie"
  ]
  node [
    id 1632
    label "funktor"
  ]
  node [
    id 1633
    label "ob&#380;a&#322;owany"
  ]
  node [
    id 1634
    label "skar&#380;yciel"
  ]
  node [
    id 1635
    label "dysponowanie"
  ]
  node [
    id 1636
    label "dysponowa&#263;"
  ]
  node [
    id 1637
    label "podejrzanie"
  ]
  node [
    id 1638
    label "podmiot"
  ]
  node [
    id 1639
    label "pos&#261;dzanie"
  ]
  node [
    id 1640
    label "niepewny"
  ]
  node [
    id 1641
    label "egzamin"
  ]
  node [
    id 1642
    label "walka"
  ]
  node [
    id 1643
    label "gracz"
  ]
  node [
    id 1644
    label "protection"
  ]
  node [
    id 1645
    label "poparcie"
  ]
  node [
    id 1646
    label "mecz"
  ]
  node [
    id 1647
    label "auspices"
  ]
  node [
    id 1648
    label "gra"
  ]
  node [
    id 1649
    label "ochrona"
  ]
  node [
    id 1650
    label "sp&#243;r"
  ]
  node [
    id 1651
    label "manewr"
  ]
  node [
    id 1652
    label "defensive_structure"
  ]
  node [
    id 1653
    label "guard_duty"
  ]
  node [
    id 1654
    label "uczestnik"
  ]
  node [
    id 1655
    label "dru&#380;ba"
  ]
  node [
    id 1656
    label "obserwator"
  ]
  node [
    id 1657
    label "osoba_fizyczna"
  ]
  node [
    id 1658
    label "niedost&#281;pny"
  ]
  node [
    id 1659
    label "obstawanie"
  ]
  node [
    id 1660
    label "adwokatowanie"
  ]
  node [
    id 1661
    label "zdawanie"
  ]
  node [
    id 1662
    label "walczenie"
  ]
  node [
    id 1663
    label "zabezpieczenie"
  ]
  node [
    id 1664
    label "t&#322;umaczenie"
  ]
  node [
    id 1665
    label "parry"
  ]
  node [
    id 1666
    label "or&#281;dowanie"
  ]
  node [
    id 1667
    label "granie"
  ]
  node [
    id 1668
    label "kartka"
  ]
  node [
    id 1669
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1670
    label "logowanie"
  ]
  node [
    id 1671
    label "plik"
  ]
  node [
    id 1672
    label "adres_internetowy"
  ]
  node [
    id 1673
    label "linia"
  ]
  node [
    id 1674
    label "serwis_internetowy"
  ]
  node [
    id 1675
    label "bok"
  ]
  node [
    id 1676
    label "skr&#281;canie"
  ]
  node [
    id 1677
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1678
    label "orientowanie"
  ]
  node [
    id 1679
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1680
    label "zorientowanie"
  ]
  node [
    id 1681
    label "ty&#322;"
  ]
  node [
    id 1682
    label "fragment"
  ]
  node [
    id 1683
    label "zorientowa&#263;"
  ]
  node [
    id 1684
    label "pagina"
  ]
  node [
    id 1685
    label "g&#243;ra"
  ]
  node [
    id 1686
    label "orientowa&#263;"
  ]
  node [
    id 1687
    label "voice"
  ]
  node [
    id 1688
    label "orientacja"
  ]
  node [
    id 1689
    label "prz&#243;d"
  ]
  node [
    id 1690
    label "internet"
  ]
  node [
    id 1691
    label "forma"
  ]
  node [
    id 1692
    label "skr&#281;cenie"
  ]
  node [
    id 1693
    label "fend"
  ]
  node [
    id 1694
    label "reprezentowa&#263;"
  ]
  node [
    id 1695
    label "robi&#263;"
  ]
  node [
    id 1696
    label "zdawa&#263;"
  ]
  node [
    id 1697
    label "czuwa&#263;"
  ]
  node [
    id 1698
    label "preach"
  ]
  node [
    id 1699
    label "chroni&#263;"
  ]
  node [
    id 1700
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 1701
    label "walczy&#263;"
  ]
  node [
    id 1702
    label "resist"
  ]
  node [
    id 1703
    label "adwokatowa&#263;"
  ]
  node [
    id 1704
    label "rebuff"
  ]
  node [
    id 1705
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1706
    label "udowadnia&#263;"
  ]
  node [
    id 1707
    label "gra&#263;"
  ]
  node [
    id 1708
    label "sprawowa&#263;"
  ]
  node [
    id 1709
    label "refuse"
  ]
  node [
    id 1710
    label "biurko"
  ]
  node [
    id 1711
    label "boks"
  ]
  node [
    id 1712
    label "palestra"
  ]
  node [
    id 1713
    label "Biuro_Lustracyjne"
  ]
  node [
    id 1714
    label "agency"
  ]
  node [
    id 1715
    label "board"
  ]
  node [
    id 1716
    label "j&#261;dro"
  ]
  node [
    id 1717
    label "systemik"
  ]
  node [
    id 1718
    label "rozprz&#261;c"
  ]
  node [
    id 1719
    label "oprogramowanie"
  ]
  node [
    id 1720
    label "systemat"
  ]
  node [
    id 1721
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1722
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1723
    label "usenet"
  ]
  node [
    id 1724
    label "porz&#261;dek"
  ]
  node [
    id 1725
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1726
    label "przyn&#281;ta"
  ]
  node [
    id 1727
    label "net"
  ]
  node [
    id 1728
    label "w&#281;dkarstwo"
  ]
  node [
    id 1729
    label "eratem"
  ]
  node [
    id 1730
    label "doktryna"
  ]
  node [
    id 1731
    label "pulpit"
  ]
  node [
    id 1732
    label "konstelacja"
  ]
  node [
    id 1733
    label "o&#347;"
  ]
  node [
    id 1734
    label "podsystem"
  ]
  node [
    id 1735
    label "ryba"
  ]
  node [
    id 1736
    label "Leopard"
  ]
  node [
    id 1737
    label "Android"
  ]
  node [
    id 1738
    label "cybernetyk"
  ]
  node [
    id 1739
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1740
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1741
    label "method"
  ]
  node [
    id 1742
    label "sk&#322;ad"
  ]
  node [
    id 1743
    label "podstawa"
  ]
  node [
    id 1744
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1745
    label "relacja_logiczna"
  ]
  node [
    id 1746
    label "judiciary"
  ]
  node [
    id 1747
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 1748
    label "grupa_dyskusyjna"
  ]
  node [
    id 1749
    label "bazylika"
  ]
  node [
    id 1750
    label "portal"
  ]
  node [
    id 1751
    label "konferencja"
  ]
  node [
    id 1752
    label "agora"
  ]
  node [
    id 1753
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 1754
    label "brzeg"
  ]
  node [
    id 1755
    label "ekoton"
  ]
  node [
    id 1756
    label "str&#261;d"
  ]
  node [
    id 1757
    label "koniec"
  ]
  node [
    id 1758
    label "jednostka_organizacyjna"
  ]
  node [
    id 1759
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1760
    label "TOPR"
  ]
  node [
    id 1761
    label "endecki"
  ]
  node [
    id 1762
    label "przedstawicielstwo"
  ]
  node [
    id 1763
    label "od&#322;am"
  ]
  node [
    id 1764
    label "Cepelia"
  ]
  node [
    id 1765
    label "ZBoWiD"
  ]
  node [
    id 1766
    label "organization"
  ]
  node [
    id 1767
    label "centrala"
  ]
  node [
    id 1768
    label "GOPR"
  ]
  node [
    id 1769
    label "ZOMO"
  ]
  node [
    id 1770
    label "ZMP"
  ]
  node [
    id 1771
    label "komitet_koordynacyjny"
  ]
  node [
    id 1772
    label "przybud&#243;wka"
  ]
  node [
    id 1773
    label "boj&#243;wka"
  ]
  node [
    id 1774
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1775
    label "Pend&#380;ab"
  ]
  node [
    id 1776
    label "funt_liba&#324;ski"
  ]
  node [
    id 1777
    label "strefa_euro"
  ]
  node [
    id 1778
    label "Pozna&#324;"
  ]
  node [
    id 1779
    label "lira_malta&#324;ska"
  ]
  node [
    id 1780
    label "Gozo"
  ]
  node [
    id 1781
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1782
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1783
    label "dolar_namibijski"
  ]
  node [
    id 1784
    label "milrejs"
  ]
  node [
    id 1785
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1786
    label "escudo_portugalskie"
  ]
  node [
    id 1787
    label "dolar_bahamski"
  ]
  node [
    id 1788
    label "Wielka_Bahama"
  ]
  node [
    id 1789
    label "dolar_liberyjski"
  ]
  node [
    id 1790
    label "riel"
  ]
  node [
    id 1791
    label "Karelia"
  ]
  node [
    id 1792
    label "Mari_El"
  ]
  node [
    id 1793
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1794
    label "Udmurcja"
  ]
  node [
    id 1795
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1796
    label "Newa"
  ]
  node [
    id 1797
    label "&#321;adoga"
  ]
  node [
    id 1798
    label "Anadyr"
  ]
  node [
    id 1799
    label "Syberia"
  ]
  node [
    id 1800
    label "Tatarstan"
  ]
  node [
    id 1801
    label "Wszechrosja"
  ]
  node [
    id 1802
    label "Azja"
  ]
  node [
    id 1803
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1804
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 1805
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 1806
    label "Kamczatka"
  ]
  node [
    id 1807
    label "Jama&#322;"
  ]
  node [
    id 1808
    label "Witim"
  ]
  node [
    id 1809
    label "Tuwa"
  ]
  node [
    id 1810
    label "car"
  ]
  node [
    id 1811
    label "Komi"
  ]
  node [
    id 1812
    label "Czuwaszja"
  ]
  node [
    id 1813
    label "Chakasja"
  ]
  node [
    id 1814
    label "Perm"
  ]
  node [
    id 1815
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 1816
    label "Ajon"
  ]
  node [
    id 1817
    label "Adygeja"
  ]
  node [
    id 1818
    label "Dniepr"
  ]
  node [
    id 1819
    label "rubel_rosyjski"
  ]
  node [
    id 1820
    label "Don"
  ]
  node [
    id 1821
    label "Mordowia"
  ]
  node [
    id 1822
    label "s&#322;owianofilstwo"
  ]
  node [
    id 1823
    label "lew"
  ]
  node [
    id 1824
    label "Dobrud&#380;a"
  ]
  node [
    id 1825
    label "lira_izraelska"
  ]
  node [
    id 1826
    label "szekel"
  ]
  node [
    id 1827
    label "Galilea"
  ]
  node [
    id 1828
    label "Luksemburgia"
  ]
  node [
    id 1829
    label "frank_belgijski"
  ]
  node [
    id 1830
    label "Limburgia"
  ]
  node [
    id 1831
    label "Walonia"
  ]
  node [
    id 1832
    label "Brabancja"
  ]
  node [
    id 1833
    label "Flandria"
  ]
  node [
    id 1834
    label "Niderlandy"
  ]
  node [
    id 1835
    label "dinar_iracki"
  ]
  node [
    id 1836
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1837
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1838
    label "szyling_ugandyjski"
  ]
  node [
    id 1839
    label "dolar_jamajski"
  ]
  node [
    id 1840
    label "ringgit"
  ]
  node [
    id 1841
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1842
    label "Borneo"
  ]
  node [
    id 1843
    label "dolar_surinamski"
  ]
  node [
    id 1844
    label "funt_suda&#324;ski"
  ]
  node [
    id 1845
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1846
    label "Manica"
  ]
  node [
    id 1847
    label "escudo_mozambickie"
  ]
  node [
    id 1848
    label "Cabo_Delgado"
  ]
  node [
    id 1849
    label "Inhambane"
  ]
  node [
    id 1850
    label "Maputo"
  ]
  node [
    id 1851
    label "Gaza"
  ]
  node [
    id 1852
    label "Niasa"
  ]
  node [
    id 1853
    label "Nampula"
  ]
  node [
    id 1854
    label "metical"
  ]
  node [
    id 1855
    label "Sahara"
  ]
  node [
    id 1856
    label "inti"
  ]
  node [
    id 1857
    label "sol"
  ]
  node [
    id 1858
    label "kip"
  ]
  node [
    id 1859
    label "Pireneje"
  ]
  node [
    id 1860
    label "euro"
  ]
  node [
    id 1861
    label "kwacha_zambijska"
  ]
  node [
    id 1862
    label "Buriaci"
  ]
  node [
    id 1863
    label "tugrik"
  ]
  node [
    id 1864
    label "ajmak"
  ]
  node [
    id 1865
    label "balboa"
  ]
  node [
    id 1866
    label "Ameryka_Centralna"
  ]
  node [
    id 1867
    label "gulden"
  ]
  node [
    id 1868
    label "Zelandia"
  ]
  node [
    id 1869
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1870
    label "dolar_Tuvalu"
  ]
  node [
    id 1871
    label "zair"
  ]
  node [
    id 1872
    label "Katanga"
  ]
  node [
    id 1873
    label "frank_szwajcarski"
  ]
  node [
    id 1874
    label "Jukatan"
  ]
  node [
    id 1875
    label "dolar_Belize"
  ]
  node [
    id 1876
    label "colon"
  ]
  node [
    id 1877
    label "Dyja"
  ]
  node [
    id 1878
    label "korona_czeska"
  ]
  node [
    id 1879
    label "Izera"
  ]
  node [
    id 1880
    label "ugija"
  ]
  node [
    id 1881
    label "szyling_kenijski"
  ]
  node [
    id 1882
    label "Nachiczewan"
  ]
  node [
    id 1883
    label "manat_azerski"
  ]
  node [
    id 1884
    label "Karabach"
  ]
  node [
    id 1885
    label "Bengal"
  ]
  node [
    id 1886
    label "taka"
  ]
  node [
    id 1887
    label "dolar_Kiribati"
  ]
  node [
    id 1888
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1889
    label "Cebu"
  ]
  node [
    id 1890
    label "Atlantyk"
  ]
  node [
    id 1891
    label "Ulster"
  ]
  node [
    id 1892
    label "funt_irlandzki"
  ]
  node [
    id 1893
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1894
    label "cedi"
  ]
  node [
    id 1895
    label "ariary"
  ]
  node [
    id 1896
    label "Ocean_Indyjski"
  ]
  node [
    id 1897
    label "frank_malgaski"
  ]
  node [
    id 1898
    label "Estremadura"
  ]
  node [
    id 1899
    label "Kastylia"
  ]
  node [
    id 1900
    label "Rzym_Zachodni"
  ]
  node [
    id 1901
    label "Aragonia"
  ]
  node [
    id 1902
    label "hacjender"
  ]
  node [
    id 1903
    label "Asturia"
  ]
  node [
    id 1904
    label "Baskonia"
  ]
  node [
    id 1905
    label "Majorka"
  ]
  node [
    id 1906
    label "Walencja"
  ]
  node [
    id 1907
    label "peseta"
  ]
  node [
    id 1908
    label "Katalonia"
  ]
  node [
    id 1909
    label "peso_chilijskie"
  ]
  node [
    id 1910
    label "Indie_Zachodnie"
  ]
  node [
    id 1911
    label "Sikkim"
  ]
  node [
    id 1912
    label "Asam"
  ]
  node [
    id 1913
    label "rupia_indyjska"
  ]
  node [
    id 1914
    label "Indie_Portugalskie"
  ]
  node [
    id 1915
    label "Indie_Wschodnie"
  ]
  node [
    id 1916
    label "Bollywood"
  ]
  node [
    id 1917
    label "jen"
  ]
  node [
    id 1918
    label "jinja"
  ]
  node [
    id 1919
    label "Okinawa"
  ]
  node [
    id 1920
    label "Japonica"
  ]
  node [
    id 1921
    label "Rugia"
  ]
  node [
    id 1922
    label "Saksonia"
  ]
  node [
    id 1923
    label "Dolna_Saksonia"
  ]
  node [
    id 1924
    label "Hesja"
  ]
  node [
    id 1925
    label "Wirtembergia"
  ]
  node [
    id 1926
    label "Po&#322;abie"
  ]
  node [
    id 1927
    label "Germania"
  ]
  node [
    id 1928
    label "Frankonia"
  ]
  node [
    id 1929
    label "Badenia"
  ]
  node [
    id 1930
    label "Holsztyn"
  ]
  node [
    id 1931
    label "marka"
  ]
  node [
    id 1932
    label "Brandenburgia"
  ]
  node [
    id 1933
    label "Szwabia"
  ]
  node [
    id 1934
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1935
    label "Westfalia"
  ]
  node [
    id 1936
    label "Helgoland"
  ]
  node [
    id 1937
    label "Karlsbad"
  ]
  node [
    id 1938
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1939
    label "Piemont"
  ]
  node [
    id 1940
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 1941
    label "Sardynia"
  ]
  node [
    id 1942
    label "Italia"
  ]
  node [
    id 1943
    label "Ok&#281;cie"
  ]
  node [
    id 1944
    label "Karyntia"
  ]
  node [
    id 1945
    label "Romania"
  ]
  node [
    id 1946
    label "lir"
  ]
  node [
    id 1947
    label "Sycylia"
  ]
  node [
    id 1948
    label "Dacja"
  ]
  node [
    id 1949
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1950
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1951
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1952
    label "funt_syryjski"
  ]
  node [
    id 1953
    label "alawizm"
  ]
  node [
    id 1954
    label "frank_rwandyjski"
  ]
  node [
    id 1955
    label "dinar_Bahrajnu"
  ]
  node [
    id 1956
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 1957
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1958
    label "frank_luksemburski"
  ]
  node [
    id 1959
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1960
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1961
    label "frank_monakijski"
  ]
  node [
    id 1962
    label "dinar_algierski"
  ]
  node [
    id 1963
    label "Wojwodina"
  ]
  node [
    id 1964
    label "dinar_serbski"
  ]
  node [
    id 1965
    label "Orinoko"
  ]
  node [
    id 1966
    label "boliwar"
  ]
  node [
    id 1967
    label "tenge"
  ]
  node [
    id 1968
    label "lek"
  ]
  node [
    id 1969
    label "frank_alba&#324;ski"
  ]
  node [
    id 1970
    label "dolar_Barbadosu"
  ]
  node [
    id 1971
    label "kyat"
  ]
  node [
    id 1972
    label "Arakan"
  ]
  node [
    id 1973
    label "c&#243;rdoba"
  ]
  node [
    id 1974
    label "Paros"
  ]
  node [
    id 1975
    label "Epir"
  ]
  node [
    id 1976
    label "panhellenizm"
  ]
  node [
    id 1977
    label "Eubea"
  ]
  node [
    id 1978
    label "Rodos"
  ]
  node [
    id 1979
    label "Achaja"
  ]
  node [
    id 1980
    label "Termopile"
  ]
  node [
    id 1981
    label "Attyka"
  ]
  node [
    id 1982
    label "Hellada"
  ]
  node [
    id 1983
    label "Etolia"
  ]
  node [
    id 1984
    label "Kreta"
  ]
  node [
    id 1985
    label "drachma"
  ]
  node [
    id 1986
    label "Olimp"
  ]
  node [
    id 1987
    label "Tesalia"
  ]
  node [
    id 1988
    label "Peloponez"
  ]
  node [
    id 1989
    label "Eolia"
  ]
  node [
    id 1990
    label "Beocja"
  ]
  node [
    id 1991
    label "Parnas"
  ]
  node [
    id 1992
    label "Lesbos"
  ]
  node [
    id 1993
    label "Salzburg"
  ]
  node [
    id 1994
    label "Rakuzy"
  ]
  node [
    id 1995
    label "konsulent"
  ]
  node [
    id 1996
    label "szyling_austryjacki"
  ]
  node [
    id 1997
    label "birr"
  ]
  node [
    id 1998
    label "negus"
  ]
  node [
    id 1999
    label "Jawa"
  ]
  node [
    id 2000
    label "Sumatra"
  ]
  node [
    id 2001
    label "rupia_indonezyjska"
  ]
  node [
    id 2002
    label "Nowa_Gwinea"
  ]
  node [
    id 2003
    label "Moluki"
  ]
  node [
    id 2004
    label "boliviano"
  ]
  node [
    id 2005
    label "Pikardia"
  ]
  node [
    id 2006
    label "Alzacja"
  ]
  node [
    id 2007
    label "Masyw_Centralny"
  ]
  node [
    id 2008
    label "Akwitania"
  ]
  node [
    id 2009
    label "Sekwana"
  ]
  node [
    id 2010
    label "Langwedocja"
  ]
  node [
    id 2011
    label "Martynika"
  ]
  node [
    id 2012
    label "Bretania"
  ]
  node [
    id 2013
    label "Sabaudia"
  ]
  node [
    id 2014
    label "Korsyka"
  ]
  node [
    id 2015
    label "Normandia"
  ]
  node [
    id 2016
    label "Gaskonia"
  ]
  node [
    id 2017
    label "Burgundia"
  ]
  node [
    id 2018
    label "frank_francuski"
  ]
  node [
    id 2019
    label "Wandea"
  ]
  node [
    id 2020
    label "Prowansja"
  ]
  node [
    id 2021
    label "Gwadelupa"
  ]
  node [
    id 2022
    label "somoni"
  ]
  node [
    id 2023
    label "dolar_Fid&#380;i"
  ]
  node [
    id 2024
    label "funt_cypryjski"
  ]
  node [
    id 2025
    label "Afrodyzje"
  ]
  node [
    id 2026
    label "peso_dominika&#324;skie"
  ]
  node [
    id 2027
    label "Fryburg"
  ]
  node [
    id 2028
    label "Bazylea"
  ]
  node [
    id 2029
    label "Helwecja"
  ]
  node [
    id 2030
    label "Berno"
  ]
  node [
    id 2031
    label "Karaka&#322;pacja"
  ]
  node [
    id 2032
    label "Windawa"
  ]
  node [
    id 2033
    label "&#322;at"
  ]
  node [
    id 2034
    label "Kurlandia"
  ]
  node [
    id 2035
    label "Liwonia"
  ]
  node [
    id 2036
    label "rubel_&#322;otewski"
  ]
  node [
    id 2037
    label "Inflanty"
  ]
  node [
    id 2038
    label "Wile&#324;szczyzna"
  ]
  node [
    id 2039
    label "&#379;mud&#378;"
  ]
  node [
    id 2040
    label "lit"
  ]
  node [
    id 2041
    label "frank_tunezyjski"
  ]
  node [
    id 2042
    label "dinar_tunezyjski"
  ]
  node [
    id 2043
    label "lempira"
  ]
  node [
    id 2044
    label "korona_w&#281;gierska"
  ]
  node [
    id 2045
    label "forint"
  ]
  node [
    id 2046
    label "Lipt&#243;w"
  ]
  node [
    id 2047
    label "dong"
  ]
  node [
    id 2048
    label "Annam"
  ]
  node [
    id 2049
    label "lud"
  ]
  node [
    id 2050
    label "frank_kongijski"
  ]
  node [
    id 2051
    label "szyling_somalijski"
  ]
  node [
    id 2052
    label "cruzado"
  ]
  node [
    id 2053
    label "real"
  ]
  node [
    id 2054
    label "Wsch&#243;d"
  ]
  node [
    id 2055
    label "Naddnieprze"
  ]
  node [
    id 2056
    label "Ma&#322;orosja"
  ]
  node [
    id 2057
    label "Wo&#322;y&#324;"
  ]
  node [
    id 2058
    label "Nadbu&#380;e"
  ]
  node [
    id 2059
    label "hrywna"
  ]
  node [
    id 2060
    label "Zaporo&#380;e"
  ]
  node [
    id 2061
    label "Krym"
  ]
  node [
    id 2062
    label "Dniestr"
  ]
  node [
    id 2063
    label "Przykarpacie"
  ]
  node [
    id 2064
    label "Kozaczyzna"
  ]
  node [
    id 2065
    label "karbowaniec"
  ]
  node [
    id 2066
    label "Tasmania"
  ]
  node [
    id 2067
    label "dolar_australijski"
  ]
  node [
    id 2068
    label "gourde"
  ]
  node [
    id 2069
    label "escudo_angolskie"
  ]
  node [
    id 2070
    label "kwanza"
  ]
  node [
    id 2071
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 2072
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 2073
    label "Ad&#380;aria"
  ]
  node [
    id 2074
    label "lari"
  ]
  node [
    id 2075
    label "naira"
  ]
  node [
    id 2076
    label "Ohio"
  ]
  node [
    id 2077
    label "P&#243;&#322;noc"
  ]
  node [
    id 2078
    label "Nowy_York"
  ]
  node [
    id 2079
    label "Illinois"
  ]
  node [
    id 2080
    label "Po&#322;udnie"
  ]
  node [
    id 2081
    label "Kalifornia"
  ]
  node [
    id 2082
    label "Wirginia"
  ]
  node [
    id 2083
    label "Teksas"
  ]
  node [
    id 2084
    label "Waszyngton"
  ]
  node [
    id 2085
    label "Massachusetts"
  ]
  node [
    id 2086
    label "Alaska"
  ]
  node [
    id 2087
    label "Maryland"
  ]
  node [
    id 2088
    label "Michigan"
  ]
  node [
    id 2089
    label "Arizona"
  ]
  node [
    id 2090
    label "Georgia"
  ]
  node [
    id 2091
    label "stan_wolny"
  ]
  node [
    id 2092
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 2093
    label "Pensylwania"
  ]
  node [
    id 2094
    label "Luizjana"
  ]
  node [
    id 2095
    label "Nowy_Meksyk"
  ]
  node [
    id 2096
    label "Wuj_Sam"
  ]
  node [
    id 2097
    label "Alabama"
  ]
  node [
    id 2098
    label "Kansas"
  ]
  node [
    id 2099
    label "Oregon"
  ]
  node [
    id 2100
    label "Zach&#243;d"
  ]
  node [
    id 2101
    label "Floryda"
  ]
  node [
    id 2102
    label "Oklahoma"
  ]
  node [
    id 2103
    label "Hudson"
  ]
  node [
    id 2104
    label "som"
  ]
  node [
    id 2105
    label "peso_urugwajskie"
  ]
  node [
    id 2106
    label "denar_macedo&#324;ski"
  ]
  node [
    id 2107
    label "dolar_Brunei"
  ]
  node [
    id 2108
    label "rial_ira&#324;ski"
  ]
  node [
    id 2109
    label "mu&#322;&#322;a"
  ]
  node [
    id 2110
    label "Persja"
  ]
  node [
    id 2111
    label "d&#380;amahirijja"
  ]
  node [
    id 2112
    label "dinar_libijski"
  ]
  node [
    id 2113
    label "nakfa"
  ]
  node [
    id 2114
    label "rial_katarski"
  ]
  node [
    id 2115
    label "quetzal"
  ]
  node [
    id 2116
    label "won"
  ]
  node [
    id 2117
    label "rial_jeme&#324;ski"
  ]
  node [
    id 2118
    label "peso_argenty&#324;skie"
  ]
  node [
    id 2119
    label "guarani"
  ]
  node [
    id 2120
    label "perper"
  ]
  node [
    id 2121
    label "dinar_kuwejcki"
  ]
  node [
    id 2122
    label "dalasi"
  ]
  node [
    id 2123
    label "dolar_Zimbabwe"
  ]
  node [
    id 2124
    label "Szantung"
  ]
  node [
    id 2125
    label "Chiny_Zachodnie"
  ]
  node [
    id 2126
    label "Kuantung"
  ]
  node [
    id 2127
    label "D&#380;ungaria"
  ]
  node [
    id 2128
    label "yuan"
  ]
  node [
    id 2129
    label "Hongkong"
  ]
  node [
    id 2130
    label "Chiny_Wschodnie"
  ]
  node [
    id 2131
    label "Guangdong"
  ]
  node [
    id 2132
    label "Junnan"
  ]
  node [
    id 2133
    label "Mand&#380;uria"
  ]
  node [
    id 2134
    label "Syczuan"
  ]
  node [
    id 2135
    label "Pa&#322;uki"
  ]
  node [
    id 2136
    label "Wolin"
  ]
  node [
    id 2137
    label "z&#322;oty"
  ]
  node [
    id 2138
    label "So&#322;a"
  ]
  node [
    id 2139
    label "Suwalszczyzna"
  ]
  node [
    id 2140
    label "Krajna"
  ]
  node [
    id 2141
    label "barwy_polskie"
  ]
  node [
    id 2142
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 2143
    label "Kaczawa"
  ]
  node [
    id 2144
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 2145
    label "Wis&#322;a"
  ]
  node [
    id 2146
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 2147
    label "Ujgur"
  ]
  node [
    id 2148
    label "Azja_Mniejsza"
  ]
  node [
    id 2149
    label "lira_turecka"
  ]
  node [
    id 2150
    label "kuna"
  ]
  node [
    id 2151
    label "dram"
  ]
  node [
    id 2152
    label "tala"
  ]
  node [
    id 2153
    label "korona_s&#322;owacka"
  ]
  node [
    id 2154
    label "Turiec"
  ]
  node [
    id 2155
    label "Himalaje"
  ]
  node [
    id 2156
    label "rupia_nepalska"
  ]
  node [
    id 2157
    label "frank_gwinejski"
  ]
  node [
    id 2158
    label "korona_esto&#324;ska"
  ]
  node [
    id 2159
    label "marka_esto&#324;ska"
  ]
  node [
    id 2160
    label "Quebec"
  ]
  node [
    id 2161
    label "dolar_kanadyjski"
  ]
  node [
    id 2162
    label "Nowa_Fundlandia"
  ]
  node [
    id 2163
    label "Zanzibar"
  ]
  node [
    id 2164
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 2165
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 2166
    label "&#346;wite&#378;"
  ]
  node [
    id 2167
    label "peso_kolumbijskie"
  ]
  node [
    id 2168
    label "Synaj"
  ]
  node [
    id 2169
    label "paraszyt"
  ]
  node [
    id 2170
    label "funt_egipski"
  ]
  node [
    id 2171
    label "szach"
  ]
  node [
    id 2172
    label "Baktria"
  ]
  node [
    id 2173
    label "afgani"
  ]
  node [
    id 2174
    label "baht"
  ]
  node [
    id 2175
    label "tolar"
  ]
  node [
    id 2176
    label "lej_mo&#322;dawski"
  ]
  node [
    id 2177
    label "Gagauzja"
  ]
  node [
    id 2178
    label "Po&#322;udniowoafrykanom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 1
    target 393
  ]
  edge [
    source 1
    target 394
  ]
  edge [
    source 1
    target 395
  ]
  edge [
    source 1
    target 396
  ]
  edge [
    source 1
    target 397
  ]
  edge [
    source 1
    target 398
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 400
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 1
    target 402
  ]
  edge [
    source 1
    target 403
  ]
  edge [
    source 1
    target 404
  ]
  edge [
    source 1
    target 405
  ]
  edge [
    source 1
    target 406
  ]
  edge [
    source 1
    target 407
  ]
  edge [
    source 1
    target 408
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 560
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 563
  ]
  edge [
    source 5
    target 564
  ]
  edge [
    source 5
    target 565
  ]
  edge [
    source 5
    target 566
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 567
  ]
  edge [
    source 5
    target 568
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 569
  ]
  edge [
    source 5
    target 570
  ]
  edge [
    source 5
    target 571
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 6
    target 690
  ]
  edge [
    source 6
    target 691
  ]
  edge [
    source 6
    target 692
  ]
  edge [
    source 6
    target 693
  ]
  edge [
    source 6
    target 694
  ]
  edge [
    source 6
    target 695
  ]
  edge [
    source 6
    target 696
  ]
  edge [
    source 6
    target 697
  ]
  edge [
    source 6
    target 698
  ]
  edge [
    source 6
    target 699
  ]
  edge [
    source 6
    target 700
  ]
  edge [
    source 6
    target 701
  ]
  edge [
    source 6
    target 702
  ]
  edge [
    source 6
    target 703
  ]
  edge [
    source 6
    target 704
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 722
  ]
  edge [
    source 6
    target 723
  ]
  edge [
    source 6
    target 724
  ]
  edge [
    source 6
    target 725
  ]
  edge [
    source 6
    target 726
  ]
  edge [
    source 6
    target 727
  ]
  edge [
    source 6
    target 728
  ]
  edge [
    source 6
    target 729
  ]
  edge [
    source 6
    target 730
  ]
  edge [
    source 6
    target 731
  ]
  edge [
    source 6
    target 732
  ]
  edge [
    source 6
    target 733
  ]
  edge [
    source 6
    target 734
  ]
  edge [
    source 6
    target 735
  ]
  edge [
    source 6
    target 736
  ]
  edge [
    source 6
    target 737
  ]
  edge [
    source 6
    target 738
  ]
  edge [
    source 6
    target 739
  ]
  edge [
    source 6
    target 740
  ]
  edge [
    source 6
    target 741
  ]
  edge [
    source 6
    target 742
  ]
  edge [
    source 6
    target 743
  ]
  edge [
    source 6
    target 744
  ]
  edge [
    source 6
    target 745
  ]
  edge [
    source 6
    target 746
  ]
  edge [
    source 6
    target 747
  ]
  edge [
    source 6
    target 748
  ]
  edge [
    source 6
    target 749
  ]
  edge [
    source 6
    target 750
  ]
  edge [
    source 6
    target 751
  ]
  edge [
    source 6
    target 752
  ]
  edge [
    source 6
    target 753
  ]
  edge [
    source 6
    target 754
  ]
  edge [
    source 6
    target 755
  ]
  edge [
    source 6
    target 756
  ]
  edge [
    source 6
    target 757
  ]
  edge [
    source 6
    target 758
  ]
  edge [
    source 6
    target 759
  ]
  edge [
    source 6
    target 760
  ]
  edge [
    source 6
    target 761
  ]
  edge [
    source 6
    target 762
  ]
  edge [
    source 6
    target 763
  ]
  edge [
    source 6
    target 764
  ]
  edge [
    source 6
    target 765
  ]
  edge [
    source 6
    target 766
  ]
  edge [
    source 6
    target 767
  ]
  edge [
    source 6
    target 768
  ]
  edge [
    source 6
    target 769
  ]
  edge [
    source 6
    target 770
  ]
  edge [
    source 6
    target 771
  ]
  edge [
    source 6
    target 772
  ]
  edge [
    source 6
    target 773
  ]
  edge [
    source 6
    target 774
  ]
  edge [
    source 6
    target 775
  ]
  edge [
    source 6
    target 776
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 777
  ]
  edge [
    source 6
    target 778
  ]
  edge [
    source 6
    target 779
  ]
  edge [
    source 6
    target 780
  ]
  edge [
    source 6
    target 781
  ]
  edge [
    source 6
    target 782
  ]
  edge [
    source 6
    target 783
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 784
  ]
  edge [
    source 7
    target 785
  ]
  edge [
    source 7
    target 786
  ]
  edge [
    source 7
    target 787
  ]
  edge [
    source 7
    target 788
  ]
  edge [
    source 7
    target 789
  ]
  edge [
    source 7
    target 790
  ]
  edge [
    source 7
    target 791
  ]
  edge [
    source 7
    target 792
  ]
  edge [
    source 7
    target 793
  ]
  edge [
    source 7
    target 794
  ]
  edge [
    source 7
    target 795
  ]
  edge [
    source 7
    target 796
  ]
  edge [
    source 7
    target 797
  ]
  edge [
    source 7
    target 798
  ]
  edge [
    source 7
    target 799
  ]
  edge [
    source 7
    target 800
  ]
  edge [
    source 7
    target 801
  ]
  edge [
    source 7
    target 802
  ]
  edge [
    source 7
    target 803
  ]
  edge [
    source 7
    target 804
  ]
  edge [
    source 7
    target 805
  ]
  edge [
    source 7
    target 806
  ]
  edge [
    source 7
    target 807
  ]
  edge [
    source 7
    target 808
  ]
  edge [
    source 7
    target 809
  ]
  edge [
    source 7
    target 810
  ]
  edge [
    source 7
    target 811
  ]
  edge [
    source 7
    target 812
  ]
  edge [
    source 7
    target 813
  ]
  edge [
    source 7
    target 814
  ]
  edge [
    source 7
    target 815
  ]
  edge [
    source 7
    target 816
  ]
  edge [
    source 7
    target 817
  ]
  edge [
    source 7
    target 818
  ]
  edge [
    source 7
    target 819
  ]
  edge [
    source 7
    target 820
  ]
  edge [
    source 7
    target 821
  ]
  edge [
    source 7
    target 822
  ]
  edge [
    source 7
    target 823
  ]
  edge [
    source 7
    target 824
  ]
  edge [
    source 7
    target 825
  ]
  edge [
    source 7
    target 826
  ]
  edge [
    source 7
    target 827
  ]
  edge [
    source 7
    target 828
  ]
  edge [
    source 7
    target 829
  ]
  edge [
    source 7
    target 830
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 831
  ]
  edge [
    source 7
    target 832
  ]
  edge [
    source 7
    target 833
  ]
  edge [
    source 7
    target 834
  ]
  edge [
    source 7
    target 835
  ]
  edge [
    source 7
    target 836
  ]
  edge [
    source 7
    target 837
  ]
  edge [
    source 7
    target 838
  ]
  edge [
    source 7
    target 839
  ]
  edge [
    source 7
    target 840
  ]
  edge [
    source 7
    target 841
  ]
  edge [
    source 7
    target 842
  ]
  edge [
    source 7
    target 843
  ]
  edge [
    source 7
    target 844
  ]
  edge [
    source 7
    target 845
  ]
  edge [
    source 7
    target 846
  ]
  edge [
    source 7
    target 847
  ]
  edge [
    source 7
    target 848
  ]
  edge [
    source 7
    target 849
  ]
  edge [
    source 7
    target 850
  ]
  edge [
    source 7
    target 851
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 852
  ]
  edge [
    source 7
    target 853
  ]
  edge [
    source 7
    target 854
  ]
  edge [
    source 7
    target 855
  ]
  edge [
    source 7
    target 856
  ]
  edge [
    source 7
    target 857
  ]
  edge [
    source 7
    target 858
  ]
  edge [
    source 7
    target 859
  ]
  edge [
    source 7
    target 860
  ]
  edge [
    source 7
    target 861
  ]
  edge [
    source 7
    target 862
  ]
  edge [
    source 7
    target 863
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 864
  ]
  edge [
    source 7
    target 865
  ]
  edge [
    source 7
    target 866
  ]
  edge [
    source 7
    target 867
  ]
  edge [
    source 7
    target 868
  ]
  edge [
    source 7
    target 869
  ]
  edge [
    source 7
    target 870
  ]
  edge [
    source 7
    target 871
  ]
  edge [
    source 7
    target 872
  ]
  edge [
    source 7
    target 873
  ]
  edge [
    source 7
    target 874
  ]
  edge [
    source 7
    target 875
  ]
  edge [
    source 7
    target 876
  ]
  edge [
    source 7
    target 877
  ]
  edge [
    source 7
    target 878
  ]
  edge [
    source 7
    target 879
  ]
  edge [
    source 7
    target 880
  ]
  edge [
    source 7
    target 881
  ]
  edge [
    source 7
    target 882
  ]
  edge [
    source 7
    target 883
  ]
  edge [
    source 7
    target 884
  ]
  edge [
    source 7
    target 885
  ]
  edge [
    source 7
    target 886
  ]
  edge [
    source 7
    target 887
  ]
  edge [
    source 7
    target 888
  ]
  edge [
    source 7
    target 889
  ]
  edge [
    source 7
    target 890
  ]
  edge [
    source 7
    target 891
  ]
  edge [
    source 7
    target 892
  ]
  edge [
    source 7
    target 893
  ]
  edge [
    source 7
    target 894
  ]
  edge [
    source 7
    target 895
  ]
  edge [
    source 7
    target 896
  ]
  edge [
    source 7
    target 897
  ]
  edge [
    source 7
    target 898
  ]
  edge [
    source 7
    target 899
  ]
  edge [
    source 7
    target 900
  ]
  edge [
    source 7
    target 901
  ]
  edge [
    source 7
    target 902
  ]
  edge [
    source 7
    target 903
  ]
  edge [
    source 7
    target 904
  ]
  edge [
    source 7
    target 905
  ]
  edge [
    source 7
    target 906
  ]
  edge [
    source 7
    target 907
  ]
  edge [
    source 7
    target 908
  ]
  edge [
    source 7
    target 909
  ]
  edge [
    source 7
    target 910
  ]
  edge [
    source 7
    target 911
  ]
  edge [
    source 7
    target 912
  ]
  edge [
    source 7
    target 913
  ]
  edge [
    source 7
    target 914
  ]
  edge [
    source 7
    target 915
  ]
  edge [
    source 7
    target 916
  ]
  edge [
    source 7
    target 917
  ]
  edge [
    source 7
    target 918
  ]
  edge [
    source 7
    target 919
  ]
  edge [
    source 7
    target 920
  ]
  edge [
    source 7
    target 921
  ]
  edge [
    source 7
    target 922
  ]
  edge [
    source 7
    target 923
  ]
  edge [
    source 7
    target 924
  ]
  edge [
    source 7
    target 925
  ]
  edge [
    source 7
    target 926
  ]
  edge [
    source 7
    target 927
  ]
  edge [
    source 7
    target 928
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 929
  ]
  edge [
    source 7
    target 930
  ]
  edge [
    source 7
    target 931
  ]
  edge [
    source 7
    target 932
  ]
  edge [
    source 7
    target 933
  ]
  edge [
    source 7
    target 934
  ]
  edge [
    source 7
    target 935
  ]
  edge [
    source 7
    target 936
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 937
  ]
  edge [
    source 7
    target 938
  ]
  edge [
    source 7
    target 939
  ]
  edge [
    source 7
    target 940
  ]
  edge [
    source 7
    target 941
  ]
  edge [
    source 7
    target 942
  ]
  edge [
    source 7
    target 943
  ]
  edge [
    source 7
    target 944
  ]
  edge [
    source 7
    target 945
  ]
  edge [
    source 7
    target 946
  ]
  edge [
    source 7
    target 947
  ]
  edge [
    source 7
    target 948
  ]
  edge [
    source 7
    target 949
  ]
  edge [
    source 7
    target 950
  ]
  edge [
    source 7
    target 951
  ]
  edge [
    source 7
    target 952
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 953
  ]
  edge [
    source 7
    target 954
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 955
  ]
  edge [
    source 7
    target 956
  ]
  edge [
    source 7
    target 957
  ]
  edge [
    source 7
    target 958
  ]
  edge [
    source 7
    target 959
  ]
  edge [
    source 7
    target 960
  ]
  edge [
    source 7
    target 961
  ]
  edge [
    source 7
    target 962
  ]
  edge [
    source 7
    target 963
  ]
  edge [
    source 7
    target 964
  ]
  edge [
    source 7
    target 965
  ]
  edge [
    source 7
    target 966
  ]
  edge [
    source 7
    target 967
  ]
  edge [
    source 7
    target 968
  ]
  edge [
    source 7
    target 969
  ]
  edge [
    source 7
    target 970
  ]
  edge [
    source 7
    target 971
  ]
  edge [
    source 7
    target 972
  ]
  edge [
    source 7
    target 973
  ]
  edge [
    source 7
    target 974
  ]
  edge [
    source 7
    target 975
  ]
  edge [
    source 7
    target 976
  ]
  edge [
    source 7
    target 977
  ]
  edge [
    source 7
    target 978
  ]
  edge [
    source 7
    target 979
  ]
  edge [
    source 7
    target 980
  ]
  edge [
    source 7
    target 981
  ]
  edge [
    source 7
    target 982
  ]
  edge [
    source 7
    target 983
  ]
  edge [
    source 7
    target 984
  ]
  edge [
    source 7
    target 985
  ]
  edge [
    source 7
    target 986
  ]
  edge [
    source 7
    target 987
  ]
  edge [
    source 7
    target 988
  ]
  edge [
    source 7
    target 989
  ]
  edge [
    source 7
    target 990
  ]
  edge [
    source 7
    target 991
  ]
  edge [
    source 7
    target 992
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 993
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 994
  ]
  edge [
    source 7
    target 995
  ]
  edge [
    source 7
    target 996
  ]
  edge [
    source 7
    target 997
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 998
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 999
  ]
  edge [
    source 7
    target 1000
  ]
  edge [
    source 7
    target 1001
  ]
  edge [
    source 7
    target 1002
  ]
  edge [
    source 7
    target 1003
  ]
  edge [
    source 7
    target 1004
  ]
  edge [
    source 7
    target 1005
  ]
  edge [
    source 7
    target 1006
  ]
  edge [
    source 7
    target 1007
  ]
  edge [
    source 7
    target 1008
  ]
  edge [
    source 7
    target 1009
  ]
  edge [
    source 7
    target 1010
  ]
  edge [
    source 7
    target 1011
  ]
  edge [
    source 7
    target 1012
  ]
  edge [
    source 7
    target 1013
  ]
  edge [
    source 7
    target 1014
  ]
  edge [
    source 7
    target 1015
  ]
  edge [
    source 7
    target 1016
  ]
  edge [
    source 7
    target 1017
  ]
  edge [
    source 7
    target 1018
  ]
  edge [
    source 7
    target 1019
  ]
  edge [
    source 7
    target 1020
  ]
  edge [
    source 7
    target 1021
  ]
  edge [
    source 7
    target 1022
  ]
  edge [
    source 7
    target 1023
  ]
  edge [
    source 7
    target 1024
  ]
  edge [
    source 7
    target 1025
  ]
  edge [
    source 7
    target 1026
  ]
  edge [
    source 7
    target 1027
  ]
  edge [
    source 7
    target 1028
  ]
  edge [
    source 7
    target 1029
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 1030
  ]
  edge [
    source 7
    target 1031
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 1032
  ]
  edge [
    source 7
    target 1033
  ]
  edge [
    source 7
    target 1034
  ]
  edge [
    source 7
    target 1035
  ]
  edge [
    source 7
    target 1036
  ]
  edge [
    source 7
    target 1037
  ]
  edge [
    source 7
    target 1038
  ]
  edge [
    source 7
    target 1039
  ]
  edge [
    source 7
    target 1040
  ]
  edge [
    source 7
    target 1041
  ]
  edge [
    source 7
    target 1042
  ]
  edge [
    source 7
    target 1043
  ]
  edge [
    source 7
    target 1044
  ]
  edge [
    source 7
    target 1045
  ]
  edge [
    source 7
    target 1046
  ]
  edge [
    source 7
    target 1047
  ]
  edge [
    source 7
    target 1048
  ]
  edge [
    source 7
    target 1049
  ]
  edge [
    source 7
    target 1050
  ]
  edge [
    source 7
    target 1051
  ]
  edge [
    source 7
    target 1052
  ]
  edge [
    source 7
    target 1053
  ]
  edge [
    source 7
    target 1054
  ]
  edge [
    source 7
    target 1055
  ]
  edge [
    source 7
    target 1056
  ]
  edge [
    source 7
    target 1057
  ]
  edge [
    source 7
    target 1058
  ]
  edge [
    source 7
    target 1059
  ]
  edge [
    source 7
    target 1060
  ]
  edge [
    source 7
    target 1061
  ]
  edge [
    source 7
    target 1062
  ]
  edge [
    source 7
    target 1063
  ]
  edge [
    source 7
    target 1064
  ]
  edge [
    source 7
    target 1065
  ]
  edge [
    source 7
    target 1066
  ]
  edge [
    source 7
    target 1067
  ]
  edge [
    source 7
    target 1068
  ]
  edge [
    source 7
    target 1069
  ]
  edge [
    source 7
    target 1070
  ]
  edge [
    source 7
    target 1071
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 1072
  ]
  edge [
    source 7
    target 1073
  ]
  edge [
    source 7
    target 1074
  ]
  edge [
    source 7
    target 1075
  ]
  edge [
    source 7
    target 1076
  ]
  edge [
    source 7
    target 1077
  ]
  edge [
    source 7
    target 1078
  ]
  edge [
    source 7
    target 1079
  ]
  edge [
    source 7
    target 1080
  ]
  edge [
    source 7
    target 1081
  ]
  edge [
    source 7
    target 1082
  ]
  edge [
    source 7
    target 1083
  ]
  edge [
    source 7
    target 1084
  ]
  edge [
    source 7
    target 1085
  ]
  edge [
    source 7
    target 1086
  ]
  edge [
    source 7
    target 1087
  ]
  edge [
    source 7
    target 1088
  ]
  edge [
    source 7
    target 1089
  ]
  edge [
    source 7
    target 1090
  ]
  edge [
    source 7
    target 1091
  ]
  edge [
    source 7
    target 1092
  ]
  edge [
    source 7
    target 1093
  ]
  edge [
    source 7
    target 1094
  ]
  edge [
    source 7
    target 1095
  ]
  edge [
    source 7
    target 1096
  ]
  edge [
    source 7
    target 1097
  ]
  edge [
    source 7
    target 1098
  ]
  edge [
    source 7
    target 1099
  ]
  edge [
    source 7
    target 1100
  ]
  edge [
    source 7
    target 1101
  ]
  edge [
    source 7
    target 1102
  ]
  edge [
    source 7
    target 1103
  ]
  edge [
    source 7
    target 1104
  ]
  edge [
    source 7
    target 1105
  ]
  edge [
    source 7
    target 1106
  ]
  edge [
    source 7
    target 1107
  ]
  edge [
    source 7
    target 1108
  ]
  edge [
    source 7
    target 1109
  ]
  edge [
    source 7
    target 1110
  ]
  edge [
    source 7
    target 1111
  ]
  edge [
    source 7
    target 1112
  ]
  edge [
    source 7
    target 1113
  ]
  edge [
    source 7
    target 1114
  ]
  edge [
    source 7
    target 1115
  ]
  edge [
    source 7
    target 1116
  ]
  edge [
    source 7
    target 1117
  ]
  edge [
    source 7
    target 1118
  ]
  edge [
    source 7
    target 1119
  ]
  edge [
    source 7
    target 1120
  ]
  edge [
    source 7
    target 1121
  ]
  edge [
    source 7
    target 1122
  ]
  edge [
    source 7
    target 1123
  ]
  edge [
    source 7
    target 1124
  ]
  edge [
    source 7
    target 1125
  ]
  edge [
    source 7
    target 1126
  ]
  edge [
    source 7
    target 1127
  ]
  edge [
    source 7
    target 1128
  ]
  edge [
    source 7
    target 1129
  ]
  edge [
    source 7
    target 1130
  ]
  edge [
    source 7
    target 1131
  ]
  edge [
    source 7
    target 1132
  ]
  edge [
    source 7
    target 1133
  ]
  edge [
    source 7
    target 1134
  ]
  edge [
    source 7
    target 1135
  ]
  edge [
    source 7
    target 1136
  ]
  edge [
    source 7
    target 1137
  ]
  edge [
    source 7
    target 1138
  ]
  edge [
    source 7
    target 1139
  ]
  edge [
    source 7
    target 1140
  ]
  edge [
    source 7
    target 1141
  ]
  edge [
    source 7
    target 1142
  ]
  edge [
    source 7
    target 1143
  ]
  edge [
    source 7
    target 1144
  ]
  edge [
    source 7
    target 1145
  ]
  edge [
    source 7
    target 1146
  ]
  edge [
    source 7
    target 1147
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 1148
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 1149
  ]
  edge [
    source 7
    target 1150
  ]
  edge [
    source 7
    target 1151
  ]
  edge [
    source 7
    target 1152
  ]
  edge [
    source 7
    target 1153
  ]
  edge [
    source 7
    target 1154
  ]
  edge [
    source 7
    target 1155
  ]
  edge [
    source 7
    target 1156
  ]
  edge [
    source 7
    target 1157
  ]
  edge [
    source 7
    target 1158
  ]
  edge [
    source 7
    target 1159
  ]
  edge [
    source 7
    target 1160
  ]
  edge [
    source 7
    target 1161
  ]
  edge [
    source 7
    target 1162
  ]
  edge [
    source 7
    target 1163
  ]
  edge [
    source 7
    target 1164
  ]
  edge [
    source 7
    target 1165
  ]
  edge [
    source 7
    target 1166
  ]
  edge [
    source 7
    target 1167
  ]
  edge [
    source 7
    target 1168
  ]
  edge [
    source 7
    target 1169
  ]
  edge [
    source 7
    target 1170
  ]
  edge [
    source 7
    target 1171
  ]
  edge [
    source 7
    target 1172
  ]
  edge [
    source 7
    target 1173
  ]
  edge [
    source 7
    target 1174
  ]
  edge [
    source 7
    target 1175
  ]
  edge [
    source 7
    target 1176
  ]
  edge [
    source 7
    target 1177
  ]
  edge [
    source 7
    target 1178
  ]
  edge [
    source 7
    target 1179
  ]
  edge [
    source 7
    target 1180
  ]
  edge [
    source 7
    target 1181
  ]
  edge [
    source 7
    target 1182
  ]
  edge [
    source 7
    target 1183
  ]
  edge [
    source 7
    target 1184
  ]
  edge [
    source 7
    target 1185
  ]
  edge [
    source 7
    target 1186
  ]
  edge [
    source 7
    target 1187
  ]
  edge [
    source 7
    target 1188
  ]
  edge [
    source 7
    target 1189
  ]
  edge [
    source 7
    target 1190
  ]
  edge [
    source 7
    target 1191
  ]
  edge [
    source 7
    target 1192
  ]
  edge [
    source 7
    target 1193
  ]
  edge [
    source 7
    target 1194
  ]
  edge [
    source 7
    target 1195
  ]
  edge [
    source 7
    target 1196
  ]
  edge [
    source 7
    target 1197
  ]
  edge [
    source 7
    target 1198
  ]
  edge [
    source 7
    target 1199
  ]
  edge [
    source 7
    target 1200
  ]
  edge [
    source 7
    target 1201
  ]
  edge [
    source 7
    target 1202
  ]
  edge [
    source 7
    target 1203
  ]
  edge [
    source 7
    target 1204
  ]
  edge [
    source 7
    target 1205
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 1206
  ]
  edge [
    source 7
    target 1207
  ]
  edge [
    source 7
    target 1208
  ]
  edge [
    source 7
    target 1209
  ]
  edge [
    source 7
    target 1210
  ]
  edge [
    source 7
    target 1211
  ]
  edge [
    source 7
    target 1212
  ]
  edge [
    source 7
    target 1213
  ]
  edge [
    source 7
    target 1214
  ]
  edge [
    source 7
    target 1215
  ]
  edge [
    source 7
    target 1216
  ]
  edge [
    source 7
    target 1217
  ]
  edge [
    source 7
    target 1218
  ]
  edge [
    source 7
    target 1219
  ]
  edge [
    source 7
    target 1220
  ]
  edge [
    source 7
    target 1221
  ]
  edge [
    source 7
    target 1222
  ]
  edge [
    source 7
    target 1223
  ]
  edge [
    source 7
    target 1224
  ]
  edge [
    source 7
    target 1225
  ]
  edge [
    source 7
    target 1226
  ]
  edge [
    source 7
    target 1227
  ]
  edge [
    source 7
    target 1228
  ]
  edge [
    source 7
    target 1229
  ]
  edge [
    source 7
    target 1230
  ]
  edge [
    source 7
    target 1231
  ]
  edge [
    source 7
    target 1232
  ]
  edge [
    source 7
    target 1233
  ]
  edge [
    source 7
    target 1234
  ]
  edge [
    source 7
    target 1235
  ]
  edge [
    source 7
    target 1236
  ]
  edge [
    source 7
    target 1237
  ]
  edge [
    source 7
    target 1238
  ]
  edge [
    source 7
    target 1239
  ]
  edge [
    source 7
    target 1240
  ]
  edge [
    source 7
    target 1241
  ]
  edge [
    source 7
    target 1242
  ]
  edge [
    source 7
    target 1243
  ]
  edge [
    source 7
    target 1244
  ]
  edge [
    source 7
    target 1245
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 1246
  ]
  edge [
    source 7
    target 1247
  ]
  edge [
    source 7
    target 1248
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 1249
  ]
  edge [
    source 7
    target 1250
  ]
  edge [
    source 7
    target 1251
  ]
  edge [
    source 7
    target 1252
  ]
  edge [
    source 7
    target 1253
  ]
  edge [
    source 7
    target 1254
  ]
  edge [
    source 7
    target 1255
  ]
  edge [
    source 7
    target 1256
  ]
  edge [
    source 7
    target 1257
  ]
  edge [
    source 7
    target 1258
  ]
  edge [
    source 7
    target 1259
  ]
  edge [
    source 7
    target 1260
  ]
  edge [
    source 7
    target 1261
  ]
  edge [
    source 7
    target 1262
  ]
  edge [
    source 7
    target 1263
  ]
  edge [
    source 7
    target 1264
  ]
  edge [
    source 7
    target 1265
  ]
  edge [
    source 7
    target 1266
  ]
  edge [
    source 7
    target 1267
  ]
  edge [
    source 7
    target 1268
  ]
  edge [
    source 7
    target 1269
  ]
  edge [
    source 7
    target 1270
  ]
  edge [
    source 7
    target 1271
  ]
  edge [
    source 7
    target 1272
  ]
  edge [
    source 7
    target 1273
  ]
  edge [
    source 7
    target 1274
  ]
  edge [
    source 7
    target 1275
  ]
  edge [
    source 7
    target 1276
  ]
  edge [
    source 7
    target 1277
  ]
  edge [
    source 7
    target 1278
  ]
  edge [
    source 7
    target 1279
  ]
  edge [
    source 7
    target 1280
  ]
  edge [
    source 7
    target 1281
  ]
  edge [
    source 7
    target 1282
  ]
  edge [
    source 7
    target 1283
  ]
  edge [
    source 7
    target 1284
  ]
  edge [
    source 7
    target 1285
  ]
  edge [
    source 7
    target 1286
  ]
  edge [
    source 7
    target 1287
  ]
  edge [
    source 7
    target 1288
  ]
  edge [
    source 7
    target 1289
  ]
  edge [
    source 7
    target 1290
  ]
  edge [
    source 7
    target 1291
  ]
  edge [
    source 7
    target 1292
  ]
  edge [
    source 7
    target 1293
  ]
  edge [
    source 7
    target 1294
  ]
  edge [
    source 7
    target 1295
  ]
  edge [
    source 7
    target 1296
  ]
  edge [
    source 7
    target 1297
  ]
  edge [
    source 7
    target 1298
  ]
  edge [
    source 7
    target 1299
  ]
  edge [
    source 7
    target 1300
  ]
  edge [
    source 7
    target 1301
  ]
  edge [
    source 7
    target 1302
  ]
  edge [
    source 7
    target 1303
  ]
  edge [
    source 7
    target 1304
  ]
  edge [
    source 7
    target 1305
  ]
  edge [
    source 7
    target 1306
  ]
  edge [
    source 7
    target 1307
  ]
  edge [
    source 7
    target 1308
  ]
  edge [
    source 7
    target 1309
  ]
  edge [
    source 7
    target 1310
  ]
  edge [
    source 7
    target 1311
  ]
  edge [
    source 7
    target 1312
  ]
  edge [
    source 7
    target 1313
  ]
  edge [
    source 7
    target 1314
  ]
  edge [
    source 7
    target 1315
  ]
  edge [
    source 7
    target 1316
  ]
  edge [
    source 7
    target 1317
  ]
  edge [
    source 7
    target 1318
  ]
  edge [
    source 7
    target 1319
  ]
  edge [
    source 7
    target 1320
  ]
  edge [
    source 7
    target 1321
  ]
  edge [
    source 7
    target 1322
  ]
  edge [
    source 7
    target 1323
  ]
  edge [
    source 7
    target 1324
  ]
  edge [
    source 7
    target 1325
  ]
  edge [
    source 7
    target 1326
  ]
  edge [
    source 7
    target 1327
  ]
  edge [
    source 7
    target 1328
  ]
  edge [
    source 7
    target 1329
  ]
  edge [
    source 7
    target 1330
  ]
  edge [
    source 7
    target 1331
  ]
  edge [
    source 7
    target 1332
  ]
  edge [
    source 7
    target 1333
  ]
  edge [
    source 7
    target 1334
  ]
  edge [
    source 7
    target 1335
  ]
  edge [
    source 7
    target 1336
  ]
  edge [
    source 7
    target 1337
  ]
  edge [
    source 7
    target 1338
  ]
  edge [
    source 7
    target 1339
  ]
  edge [
    source 7
    target 1340
  ]
  edge [
    source 7
    target 1341
  ]
  edge [
    source 7
    target 1342
  ]
  edge [
    source 7
    target 1343
  ]
  edge [
    source 7
    target 1344
  ]
  edge [
    source 7
    target 1345
  ]
  edge [
    source 7
    target 1346
  ]
  edge [
    source 7
    target 1347
  ]
  edge [
    source 7
    target 1348
  ]
  edge [
    source 7
    target 1349
  ]
  edge [
    source 7
    target 1350
  ]
  edge [
    source 7
    target 1351
  ]
  edge [
    source 7
    target 1352
  ]
  edge [
    source 7
    target 1353
  ]
  edge [
    source 7
    target 1354
  ]
  edge [
    source 7
    target 1355
  ]
  edge [
    source 7
    target 1356
  ]
  edge [
    source 7
    target 1357
  ]
  edge [
    source 7
    target 1358
  ]
  edge [
    source 7
    target 1359
  ]
  edge [
    source 7
    target 1360
  ]
  edge [
    source 7
    target 1361
  ]
  edge [
    source 7
    target 1362
  ]
  edge [
    source 7
    target 1363
  ]
  edge [
    source 7
    target 1364
  ]
  edge [
    source 7
    target 1365
  ]
  edge [
    source 7
    target 1366
  ]
  edge [
    source 7
    target 1367
  ]
  edge [
    source 7
    target 1368
  ]
  edge [
    source 7
    target 1369
  ]
  edge [
    source 7
    target 1370
  ]
  edge [
    source 7
    target 1371
  ]
  edge [
    source 7
    target 1372
  ]
  edge [
    source 7
    target 1373
  ]
  edge [
    source 7
    target 1374
  ]
  edge [
    source 7
    target 1375
  ]
  edge [
    source 7
    target 1376
  ]
  edge [
    source 7
    target 1377
  ]
  edge [
    source 7
    target 1378
  ]
  edge [
    source 7
    target 1379
  ]
  edge [
    source 7
    target 1380
  ]
  edge [
    source 7
    target 1381
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 1382
  ]
  edge [
    source 8
    target 1383
  ]
  edge [
    source 8
    target 1384
  ]
  edge [
    source 8
    target 1385
  ]
  edge [
    source 8
    target 1386
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 1387
  ]
  edge [
    source 8
    target 1388
  ]
  edge [
    source 8
    target 1389
  ]
  edge [
    source 8
    target 1390
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 1391
  ]
  edge [
    source 8
    target 1392
  ]
  edge [
    source 8
    target 1393
  ]
  edge [
    source 8
    target 1394
  ]
  edge [
    source 8
    target 1395
  ]
  edge [
    source 8
    target 1396
  ]
  edge [
    source 8
    target 1397
  ]
  edge [
    source 8
    target 1398
  ]
  edge [
    source 8
    target 1399
  ]
  edge [
    source 8
    target 1400
  ]
  edge [
    source 8
    target 1401
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 1402
  ]
  edge [
    source 8
    target 1403
  ]
  edge [
    source 8
    target 1404
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 1405
  ]
  edge [
    source 8
    target 1406
  ]
  edge [
    source 8
    target 1407
  ]
  edge [
    source 8
    target 1408
  ]
  edge [
    source 8
    target 1409
  ]
  edge [
    source 8
    target 1410
  ]
  edge [
    source 8
    target 1411
  ]
  edge [
    source 8
    target 1412
  ]
  edge [
    source 8
    target 1413
  ]
  edge [
    source 8
    target 1414
  ]
  edge [
    source 8
    target 1415
  ]
  edge [
    source 8
    target 1416
  ]
  edge [
    source 8
    target 1417
  ]
  edge [
    source 8
    target 1418
  ]
  edge [
    source 8
    target 1419
  ]
  edge [
    source 8
    target 1420
  ]
  edge [
    source 8
    target 1421
  ]
  edge [
    source 8
    target 1422
  ]
  edge [
    source 8
    target 1423
  ]
  edge [
    source 8
    target 1424
  ]
  edge [
    source 8
    target 1425
  ]
  edge [
    source 8
    target 1426
  ]
  edge [
    source 8
    target 1427
  ]
  edge [
    source 8
    target 1428
  ]
  edge [
    source 8
    target 1429
  ]
  edge [
    source 8
    target 2178
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 1385
  ]
  edge [
    source 10
    target 1430
  ]
  edge [
    source 10
    target 1431
  ]
  edge [
    source 10
    target 1432
  ]
  edge [
    source 10
    target 1433
  ]
  edge [
    source 10
    target 1434
  ]
  edge [
    source 10
    target 1392
  ]
  edge [
    source 10
    target 1435
  ]
  edge [
    source 10
    target 1393
  ]
  edge [
    source 10
    target 1436
  ]
  edge [
    source 10
    target 1437
  ]
  edge [
    source 10
    target 1438
  ]
  edge [
    source 10
    target 1439
  ]
  edge [
    source 10
    target 1440
  ]
  edge [
    source 10
    target 1441
  ]
  edge [
    source 10
    target 1442
  ]
  edge [
    source 10
    target 1443
  ]
  edge [
    source 10
    target 1444
  ]
  edge [
    source 10
    target 1445
  ]
  edge [
    source 10
    target 1446
  ]
  edge [
    source 10
    target 1447
  ]
  edge [
    source 10
    target 1448
  ]
  edge [
    source 10
    target 1449
  ]
  edge [
    source 10
    target 1450
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 1451
  ]
  edge [
    source 10
    target 1452
  ]
  edge [
    source 10
    target 1453
  ]
  edge [
    source 10
    target 1454
  ]
  edge [
    source 10
    target 1455
  ]
  edge [
    source 10
    target 1456
  ]
  edge [
    source 10
    target 1457
  ]
  edge [
    source 10
    target 1458
  ]
  edge [
    source 10
    target 1459
  ]
  edge [
    source 10
    target 1460
  ]
  edge [
    source 10
    target 1461
  ]
  edge [
    source 10
    target 1462
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 1374
  ]
  edge [
    source 10
    target 1463
  ]
  edge [
    source 10
    target 1464
  ]
  edge [
    source 10
    target 1465
  ]
  edge [
    source 10
    target 1466
  ]
  edge [
    source 10
    target 1467
  ]
  edge [
    source 10
    target 1468
  ]
  edge [
    source 10
    target 1469
  ]
  edge [
    source 10
    target 1470
  ]
  edge [
    source 10
    target 1471
  ]
  edge [
    source 10
    target 1472
  ]
  edge [
    source 10
    target 1473
  ]
  edge [
    source 10
    target 1474
  ]
  edge [
    source 10
    target 1475
  ]
  edge [
    source 10
    target 1476
  ]
  edge [
    source 10
    target 1477
  ]
  edge [
    source 10
    target 1478
  ]
  edge [
    source 10
    target 1012
  ]
  edge [
    source 10
    target 1479
  ]
  edge [
    source 10
    target 1480
  ]
  edge [
    source 10
    target 1481
  ]
  edge [
    source 10
    target 1482
  ]
  edge [
    source 10
    target 1483
  ]
  edge [
    source 10
    target 1484
  ]
  edge [
    source 10
    target 1485
  ]
  edge [
    source 10
    target 1486
  ]
  edge [
    source 10
    target 1487
  ]
  edge [
    source 10
    target 1488
  ]
  edge [
    source 10
    target 1489
  ]
  edge [
    source 10
    target 1490
  ]
  edge [
    source 10
    target 1491
  ]
  edge [
    source 10
    target 1492
  ]
  edge [
    source 10
    target 1493
  ]
  edge [
    source 10
    target 1494
  ]
  edge [
    source 10
    target 1495
  ]
  edge [
    source 10
    target 1496
  ]
  edge [
    source 10
    target 1497
  ]
  edge [
    source 10
    target 1498
  ]
  edge [
    source 10
    target 1499
  ]
  edge [
    source 10
    target 1500
  ]
  edge [
    source 10
    target 1501
  ]
  edge [
    source 10
    target 1502
  ]
  edge [
    source 10
    target 1362
  ]
  edge [
    source 10
    target 1503
  ]
  edge [
    source 10
    target 1504
  ]
  edge [
    source 10
    target 1505
  ]
  edge [
    source 10
    target 1506
  ]
  edge [
    source 10
    target 1507
  ]
  edge [
    source 10
    target 1391
  ]
  edge [
    source 10
    target 1508
  ]
  edge [
    source 10
    target 1509
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 1510
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 1511
  ]
  edge [
    source 10
    target 1512
  ]
  edge [
    source 10
    target 1513
  ]
  edge [
    source 10
    target 1514
  ]
  edge [
    source 10
    target 1515
  ]
  edge [
    source 10
    target 1516
  ]
  edge [
    source 10
    target 1517
  ]
  edge [
    source 10
    target 2178
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 1518
  ]
  edge [
    source 11
    target 1519
  ]
  edge [
    source 11
    target 1520
  ]
  edge [
    source 11
    target 1521
  ]
  edge [
    source 11
    target 1522
  ]
  edge [
    source 11
    target 1523
  ]
  edge [
    source 11
    target 1524
  ]
  edge [
    source 11
    target 1525
  ]
  edge [
    source 11
    target 1526
  ]
  edge [
    source 11
    target 1527
  ]
  edge [
    source 11
    target 1528
  ]
  edge [
    source 11
    target 2178
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1529
  ]
  edge [
    source 12
    target 1530
  ]
  edge [
    source 12
    target 1531
  ]
  edge [
    source 12
    target 1532
  ]
  edge [
    source 12
    target 1533
  ]
  edge [
    source 12
    target 1534
  ]
  edge [
    source 12
    target 1535
  ]
  edge [
    source 12
    target 1536
  ]
  edge [
    source 12
    target 1537
  ]
  edge [
    source 12
    target 1538
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 72
  ]
  edge [
    source 14
    target 1539
  ]
  edge [
    source 14
    target 1540
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 1541
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 1542
  ]
  edge [
    source 14
    target 1543
  ]
  edge [
    source 14
    target 1544
  ]
  edge [
    source 14
    target 1545
  ]
  edge [
    source 14
    target 1546
  ]
  edge [
    source 14
    target 1547
  ]
  edge [
    source 14
    target 1548
  ]
  edge [
    source 14
    target 1549
  ]
  edge [
    source 14
    target 1550
  ]
  edge [
    source 14
    target 1551
  ]
  edge [
    source 14
    target 1552
  ]
  edge [
    source 14
    target 1553
  ]
  edge [
    source 14
    target 1554
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 1555
  ]
  edge [
    source 14
    target 1556
  ]
  edge [
    source 14
    target 1557
  ]
  edge [
    source 14
    target 1558
  ]
  edge [
    source 14
    target 1559
  ]
  edge [
    source 14
    target 1560
  ]
  edge [
    source 14
    target 250
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 784
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 39
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 1561
  ]
  edge [
    source 14
    target 42
  ]
  edge [
    source 14
    target 29
  ]
  edge [
    source 14
    target 1562
  ]
  edge [
    source 14
    target 1563
  ]
  edge [
    source 14
    target 1564
  ]
  edge [
    source 14
    target 1367
  ]
  edge [
    source 14
    target 1393
  ]
  edge [
    source 14
    target 1565
  ]
  edge [
    source 14
    target 1566
  ]
  edge [
    source 14
    target 40
  ]
  edge [
    source 14
    target 1567
  ]
  edge [
    source 14
    target 1568
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 1569
  ]
  edge [
    source 14
    target 1570
  ]
  edge [
    source 14
    target 1571
  ]
  edge [
    source 14
    target 1572
  ]
  edge [
    source 14
    target 1573
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 1574
  ]
  edge [
    source 14
    target 997
  ]
  edge [
    source 14
    target 1575
  ]
  edge [
    source 14
    target 1576
  ]
  edge [
    source 14
    target 638
  ]
  edge [
    source 14
    target 1577
  ]
  edge [
    source 14
    target 1578
  ]
  edge [
    source 14
    target 1579
  ]
  edge [
    source 14
    target 1580
  ]
  edge [
    source 14
    target 1581
  ]
  edge [
    source 14
    target 1582
  ]
  edge [
    source 14
    target 1583
  ]
  edge [
    source 14
    target 67
  ]
  edge [
    source 14
    target 1584
  ]
  edge [
    source 14
    target 1585
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 1586
  ]
  edge [
    source 14
    target 1587
  ]
  edge [
    source 14
    target 1588
  ]
  edge [
    source 14
    target 1589
  ]
  edge [
    source 14
    target 1590
  ]
  edge [
    source 14
    target 1591
  ]
  edge [
    source 14
    target 1592
  ]
  edge [
    source 14
    target 1593
  ]
  edge [
    source 14
    target 1594
  ]
  edge [
    source 14
    target 1595
  ]
  edge [
    source 14
    target 1596
  ]
  edge [
    source 14
    target 1597
  ]
  edge [
    source 14
    target 1598
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 1599
  ]
  edge [
    source 14
    target 1600
  ]
  edge [
    source 14
    target 1601
  ]
  edge [
    source 14
    target 435
  ]
  edge [
    source 14
    target 1602
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 1603
  ]
  edge [
    source 14
    target 1604
  ]
  edge [
    source 14
    target 1605
  ]
  edge [
    source 14
    target 1606
  ]
  edge [
    source 14
    target 1607
  ]
  edge [
    source 14
    target 1608
  ]
  edge [
    source 14
    target 1609
  ]
  edge [
    source 14
    target 1610
  ]
  edge [
    source 14
    target 1611
  ]
  edge [
    source 14
    target 1612
  ]
  edge [
    source 14
    target 1613
  ]
  edge [
    source 14
    target 1614
  ]
  edge [
    source 14
    target 1615
  ]
  edge [
    source 14
    target 1616
  ]
  edge [
    source 14
    target 1617
  ]
  edge [
    source 14
    target 1618
  ]
  edge [
    source 14
    target 1619
  ]
  edge [
    source 14
    target 1620
  ]
  edge [
    source 14
    target 1621
  ]
  edge [
    source 14
    target 1622
  ]
  edge [
    source 14
    target 1623
  ]
  edge [
    source 14
    target 1624
  ]
  edge [
    source 14
    target 1625
  ]
  edge [
    source 14
    target 1626
  ]
  edge [
    source 14
    target 1627
  ]
  edge [
    source 14
    target 1628
  ]
  edge [
    source 14
    target 1629
  ]
  edge [
    source 14
    target 1630
  ]
  edge [
    source 14
    target 1631
  ]
  edge [
    source 14
    target 1632
  ]
  edge [
    source 14
    target 1633
  ]
  edge [
    source 14
    target 1634
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 1635
  ]
  edge [
    source 14
    target 1636
  ]
  edge [
    source 14
    target 1637
  ]
  edge [
    source 14
    target 1638
  ]
  edge [
    source 14
    target 1639
  ]
  edge [
    source 14
    target 407
  ]
  edge [
    source 14
    target 1640
  ]
  edge [
    source 14
    target 1408
  ]
  edge [
    source 14
    target 1641
  ]
  edge [
    source 14
    target 1642
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 1643
  ]
  edge [
    source 14
    target 1644
  ]
  edge [
    source 14
    target 1645
  ]
  edge [
    source 14
    target 1646
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 660
  ]
  edge [
    source 14
    target 1647
  ]
  edge [
    source 14
    target 1648
  ]
  edge [
    source 14
    target 1649
  ]
  edge [
    source 14
    target 1650
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 1651
  ]
  edge [
    source 14
    target 1652
  ]
  edge [
    source 14
    target 1653
  ]
  edge [
    source 14
    target 1654
  ]
  edge [
    source 14
    target 1655
  ]
  edge [
    source 14
    target 1656
  ]
  edge [
    source 14
    target 1657
  ]
  edge [
    source 14
    target 1658
  ]
  edge [
    source 14
    target 1659
  ]
  edge [
    source 14
    target 1660
  ]
  edge [
    source 14
    target 1661
  ]
  edge [
    source 14
    target 1662
  ]
  edge [
    source 14
    target 1663
  ]
  edge [
    source 14
    target 1664
  ]
  edge [
    source 14
    target 1665
  ]
  edge [
    source 14
    target 1666
  ]
  edge [
    source 14
    target 1667
  ]
  edge [
    source 14
    target 1668
  ]
  edge [
    source 14
    target 1669
  ]
  edge [
    source 14
    target 1670
  ]
  edge [
    source 14
    target 1671
  ]
  edge [
    source 14
    target 1672
  ]
  edge [
    source 14
    target 1673
  ]
  edge [
    source 14
    target 1674
  ]
  edge [
    source 14
    target 379
  ]
  edge [
    source 14
    target 1675
  ]
  edge [
    source 14
    target 1676
  ]
  edge [
    source 14
    target 1677
  ]
  edge [
    source 14
    target 1678
  ]
  edge [
    source 14
    target 1679
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 1680
  ]
  edge [
    source 14
    target 1681
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 1682
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 1683
  ]
  edge [
    source 14
    target 1684
  ]
  edge [
    source 14
    target 1685
  ]
  edge [
    source 14
    target 1686
  ]
  edge [
    source 14
    target 1687
  ]
  edge [
    source 14
    target 1688
  ]
  edge [
    source 14
    target 1689
  ]
  edge [
    source 14
    target 1690
  ]
  edge [
    source 14
    target 851
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 1691
  ]
  edge [
    source 14
    target 1692
  ]
  edge [
    source 14
    target 1693
  ]
  edge [
    source 14
    target 1694
  ]
  edge [
    source 14
    target 1695
  ]
  edge [
    source 14
    target 1696
  ]
  edge [
    source 14
    target 1697
  ]
  edge [
    source 14
    target 1698
  ]
  edge [
    source 14
    target 1699
  ]
  edge [
    source 14
    target 1700
  ]
  edge [
    source 14
    target 1701
  ]
  edge [
    source 14
    target 1702
  ]
  edge [
    source 14
    target 1703
  ]
  edge [
    source 14
    target 1704
  ]
  edge [
    source 14
    target 1705
  ]
  edge [
    source 14
    target 1706
  ]
  edge [
    source 14
    target 1707
  ]
  edge [
    source 14
    target 1708
  ]
  edge [
    source 14
    target 1709
  ]
  edge [
    source 14
    target 1710
  ]
  edge [
    source 14
    target 1711
  ]
  edge [
    source 14
    target 1712
  ]
  edge [
    source 14
    target 1713
  ]
  edge [
    source 14
    target 1714
  ]
  edge [
    source 14
    target 1715
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 1716
  ]
  edge [
    source 14
    target 1717
  ]
  edge [
    source 14
    target 1718
  ]
  edge [
    source 14
    target 1719
  ]
  edge [
    source 14
    target 1720
  ]
  edge [
    source 14
    target 1721
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 14
    target 1722
  ]
  edge [
    source 14
    target 428
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 1723
  ]
  edge [
    source 14
    target 1724
  ]
  edge [
    source 14
    target 1725
  ]
  edge [
    source 14
    target 1726
  ]
  edge [
    source 14
    target 1727
  ]
  edge [
    source 14
    target 1728
  ]
  edge [
    source 14
    target 1729
  ]
  edge [
    source 14
    target 32
  ]
  edge [
    source 14
    target 1730
  ]
  edge [
    source 14
    target 1731
  ]
  edge [
    source 14
    target 1732
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 1733
  ]
  edge [
    source 14
    target 1734
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 1735
  ]
  edge [
    source 14
    target 1736
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 1737
  ]
  edge [
    source 14
    target 1738
  ]
  edge [
    source 14
    target 1739
  ]
  edge [
    source 14
    target 1740
  ]
  edge [
    source 14
    target 1741
  ]
  edge [
    source 14
    target 1742
  ]
  edge [
    source 14
    target 1743
  ]
  edge [
    source 14
    target 1744
  ]
  edge [
    source 14
    target 1745
  ]
  edge [
    source 14
    target 1746
  ]
  edge [
    source 14
    target 1747
  ]
  edge [
    source 14
    target 1748
  ]
  edge [
    source 14
    target 932
  ]
  edge [
    source 14
    target 1749
  ]
  edge [
    source 14
    target 433
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 1750
  ]
  edge [
    source 14
    target 1751
  ]
  edge [
    source 14
    target 1752
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 1193
  ]
  edge [
    source 16
    target 1192
  ]
  edge [
    source 16
    target 784
  ]
  edge [
    source 16
    target 1156
  ]
  edge [
    source 16
    target 1753
  ]
  edge [
    source 16
    target 1194
  ]
  edge [
    source 16
    target 785
  ]
  edge [
    source 16
    target 786
  ]
  edge [
    source 16
    target 1196
  ]
  edge [
    source 16
    target 1195
  ]
  edge [
    source 16
    target 787
  ]
  edge [
    source 16
    target 1197
  ]
  edge [
    source 16
    target 1198
  ]
  edge [
    source 16
    target 1199
  ]
  edge [
    source 16
    target 1200
  ]
  edge [
    source 16
    target 1201
  ]
  edge [
    source 16
    target 1202
  ]
  edge [
    source 16
    target 1136
  ]
  edge [
    source 16
    target 788
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 1203
  ]
  edge [
    source 16
    target 1204
  ]
  edge [
    source 16
    target 1205
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 1206
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 1207
  ]
  edge [
    source 16
    target 1209
  ]
  edge [
    source 16
    target 1119
  ]
  edge [
    source 16
    target 1210
  ]
  edge [
    source 16
    target 1211
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 1212
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 1075
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 1215
  ]
  edge [
    source 16
    target 1213
  ]
  edge [
    source 16
    target 1160
  ]
  edge [
    source 16
    target 1214
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 1216
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 1217
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 1218
  ]
  edge [
    source 16
    target 1219
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 1220
  ]
  edge [
    source 16
    target 1120
  ]
  edge [
    source 16
    target 1221
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 1222
  ]
  edge [
    source 16
    target 1223
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 1224
  ]
  edge [
    source 16
    target 1225
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 1226
  ]
  edge [
    source 16
    target 1080
  ]
  edge [
    source 16
    target 1227
  ]
  edge [
    source 16
    target 1228
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 1229
  ]
  edge [
    source 16
    target 1754
  ]
  edge [
    source 16
    target 1230
  ]
  edge [
    source 16
    target 1231
  ]
  edge [
    source 16
    target 1232
  ]
  edge [
    source 16
    target 1233
  ]
  edge [
    source 16
    target 1234
  ]
  edge [
    source 16
    target 1235
  ]
  edge [
    source 16
    target 1107
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 1236
  ]
  edge [
    source 16
    target 1112
  ]
  edge [
    source 16
    target 1239
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 1240
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 1241
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 1242
  ]
  edge [
    source 16
    target 1243
  ]
  edge [
    source 16
    target 1244
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 1172
  ]
  edge [
    source 16
    target 1245
  ]
  edge [
    source 16
    target 205
  ]
  edge [
    source 16
    target 1246
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 1248
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 1251
  ]
  edge [
    source 16
    target 1252
  ]
  edge [
    source 16
    target 1249
  ]
  edge [
    source 16
    target 1253
  ]
  edge [
    source 16
    target 1254
  ]
  edge [
    source 16
    target 1255
  ]
  edge [
    source 16
    target 1257
  ]
  edge [
    source 16
    target 1135
  ]
  edge [
    source 16
    target 1258
  ]
  edge [
    source 16
    target 1113
  ]
  edge [
    source 16
    target 1260
  ]
  edge [
    source 16
    target 1259
  ]
  edge [
    source 16
    target 1261
  ]
  edge [
    source 16
    target 1169
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 1262
  ]
  edge [
    source 16
    target 1263
  ]
  edge [
    source 16
    target 1078
  ]
  edge [
    source 16
    target 1264
  ]
  edge [
    source 16
    target 1086
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 1265
  ]
  edge [
    source 16
    target 1266
  ]
  edge [
    source 16
    target 1268
  ]
  edge [
    source 16
    target 1272
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 1274
  ]
  edge [
    source 16
    target 1273
  ]
  edge [
    source 16
    target 1270
  ]
  edge [
    source 16
    target 1267
  ]
  edge [
    source 16
    target 1271
  ]
  edge [
    source 16
    target 1275
  ]
  edge [
    source 16
    target 1269
  ]
  edge [
    source 16
    target 1277
  ]
  edge [
    source 16
    target 1276
  ]
  edge [
    source 16
    target 1278
  ]
  edge [
    source 16
    target 1279
  ]
  edge [
    source 16
    target 1280
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 858
  ]
  edge [
    source 16
    target 1281
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 16
    target 860
  ]
  edge [
    source 16
    target 861
  ]
  edge [
    source 16
    target 1282
  ]
  edge [
    source 16
    target 1283
  ]
  edge [
    source 16
    target 1101
  ]
  edge [
    source 16
    target 1285
  ]
  edge [
    source 16
    target 1284
  ]
  edge [
    source 16
    target 1099
  ]
  edge [
    source 16
    target 862
  ]
  edge [
    source 16
    target 863
  ]
  edge [
    source 16
    target 1286
  ]
  edge [
    source 16
    target 864
  ]
  edge [
    source 16
    target 865
  ]
  edge [
    source 16
    target 1287
  ]
  edge [
    source 16
    target 866
  ]
  edge [
    source 16
    target 1288
  ]
  edge [
    source 16
    target 867
  ]
  edge [
    source 16
    target 1289
  ]
  edge [
    source 16
    target 1290
  ]
  edge [
    source 16
    target 1291
  ]
  edge [
    source 16
    target 1293
  ]
  edge [
    source 16
    target 1294
  ]
  edge [
    source 16
    target 868
  ]
  edge [
    source 16
    target 1295
  ]
  edge [
    source 16
    target 1296
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 1090
  ]
  edge [
    source 16
    target 871
  ]
  edge [
    source 16
    target 873
  ]
  edge [
    source 16
    target 874
  ]
  edge [
    source 16
    target 1300
  ]
  edge [
    source 16
    target 1191
  ]
  edge [
    source 16
    target 1299
  ]
  edge [
    source 16
    target 1301
  ]
  edge [
    source 16
    target 875
  ]
  edge [
    source 16
    target 1157
  ]
  edge [
    source 16
    target 1304
  ]
  edge [
    source 16
    target 1303
  ]
  edge [
    source 16
    target 1302
  ]
  edge [
    source 16
    target 1306
  ]
  edge [
    source 16
    target 1305
  ]
  edge [
    source 16
    target 1158
  ]
  edge [
    source 16
    target 1308
  ]
  edge [
    source 16
    target 1307
  ]
  edge [
    source 16
    target 876
  ]
  edge [
    source 16
    target 877
  ]
  edge [
    source 16
    target 878
  ]
  edge [
    source 16
    target 1309
  ]
  edge [
    source 16
    target 1312
  ]
  edge [
    source 16
    target 1311
  ]
  edge [
    source 16
    target 879
  ]
  edge [
    source 16
    target 1310
  ]
  edge [
    source 16
    target 1088
  ]
  edge [
    source 16
    target 1313
  ]
  edge [
    source 16
    target 881
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 1314
  ]
  edge [
    source 16
    target 1076
  ]
  edge [
    source 16
    target 1315
  ]
  edge [
    source 16
    target 882
  ]
  edge [
    source 16
    target 1316
  ]
  edge [
    source 16
    target 1317
  ]
  edge [
    source 16
    target 884
  ]
  edge [
    source 16
    target 883
  ]
  edge [
    source 16
    target 885
  ]
  edge [
    source 16
    target 1318
  ]
  edge [
    source 16
    target 886
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 1109
  ]
  edge [
    source 16
    target 887
  ]
  edge [
    source 16
    target 1319
  ]
  edge [
    source 16
    target 896
  ]
  edge [
    source 16
    target 1320
  ]
  edge [
    source 16
    target 888
  ]
  edge [
    source 16
    target 889
  ]
  edge [
    source 16
    target 1321
  ]
  edge [
    source 16
    target 1322
  ]
  edge [
    source 16
    target 892
  ]
  edge [
    source 16
    target 1323
  ]
  edge [
    source 16
    target 893
  ]
  edge [
    source 16
    target 1118
  ]
  edge [
    source 16
    target 894
  ]
  edge [
    source 16
    target 895
  ]
  edge [
    source 16
    target 1324
  ]
  edge [
    source 16
    target 1325
  ]
  edge [
    source 16
    target 897
  ]
  edge [
    source 16
    target 898
  ]
  edge [
    source 16
    target 1328
  ]
  edge [
    source 16
    target 1327
  ]
  edge [
    source 16
    target 1082
  ]
  edge [
    source 16
    target 1329
  ]
  edge [
    source 16
    target 1330
  ]
  edge [
    source 16
    target 1174
  ]
  edge [
    source 16
    target 901
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 900
  ]
  edge [
    source 16
    target 903
  ]
  edge [
    source 16
    target 1331
  ]
  edge [
    source 16
    target 1173
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 1332
  ]
  edge [
    source 16
    target 1146
  ]
  edge [
    source 16
    target 1333
  ]
  edge [
    source 16
    target 1334
  ]
  edge [
    source 16
    target 1335
  ]
  edge [
    source 16
    target 1336
  ]
  edge [
    source 16
    target 1338
  ]
  edge [
    source 16
    target 1337
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 1108
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 1339
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 1084
  ]
  edge [
    source 16
    target 1124
  ]
  edge [
    source 16
    target 1138
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 1340
  ]
  edge [
    source 16
    target 1341
  ]
  edge [
    source 16
    target 1342
  ]
  edge [
    source 16
    target 1343
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 1344
  ]
  edge [
    source 16
    target 1079
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 1345
  ]
  edge [
    source 16
    target 1347
  ]
  edge [
    source 16
    target 1348
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 1349
  ]
  edge [
    source 16
    target 1167
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 1350
  ]
  edge [
    source 16
    target 1114
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 1352
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 899
  ]
  edge [
    source 16
    target 1353
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 16
    target 1354
  ]
  edge [
    source 16
    target 1159
  ]
  edge [
    source 16
    target 1355
  ]
  edge [
    source 16
    target 1356
  ]
  edge [
    source 16
    target 1357
  ]
  edge [
    source 16
    target 1358
  ]
  edge [
    source 16
    target 930
  ]
  edge [
    source 16
    target 1359
  ]
  edge [
    source 16
    target 1360
  ]
  edge [
    source 16
    target 1361
  ]
  edge [
    source 16
    target 1045
  ]
  edge [
    source 16
    target 1673
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 16
    target 1755
  ]
  edge [
    source 16
    target 1756
  ]
  edge [
    source 16
    target 1757
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 476
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 16
    target 890
  ]
  edge [
    source 16
    target 891
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 433
  ]
  edge [
    source 16
    target 1638
  ]
  edge [
    source 16
    target 1758
  ]
  edge [
    source 16
    target 97
  ]
  edge [
    source 16
    target 1759
  ]
  edge [
    source 16
    target 1760
  ]
  edge [
    source 16
    target 1761
  ]
  edge [
    source 16
    target 72
  ]
  edge [
    source 16
    target 1762
  ]
  edge [
    source 16
    target 1763
  ]
  edge [
    source 16
    target 1764
  ]
  edge [
    source 16
    target 1627
  ]
  edge [
    source 16
    target 1765
  ]
  edge [
    source 16
    target 1766
  ]
  edge [
    source 16
    target 1767
  ]
  edge [
    source 16
    target 1768
  ]
  edge [
    source 16
    target 1769
  ]
  edge [
    source 16
    target 1770
  ]
  edge [
    source 16
    target 1771
  ]
  edge [
    source 16
    target 1772
  ]
  edge [
    source 16
    target 1773
  ]
  edge [
    source 16
    target 946
  ]
  edge [
    source 16
    target 947
  ]
  edge [
    source 16
    target 948
  ]
  edge [
    source 16
    target 949
  ]
  edge [
    source 16
    target 950
  ]
  edge [
    source 16
    target 951
  ]
  edge [
    source 16
    target 952
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 953
  ]
  edge [
    source 16
    target 954
  ]
  edge [
    source 16
    target 956
  ]
  edge [
    source 16
    target 955
  ]
  edge [
    source 16
    target 957
  ]
  edge [
    source 16
    target 958
  ]
  edge [
    source 16
    target 959
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 961
  ]
  edge [
    source 16
    target 962
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 963
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 966
  ]
  edge [
    source 16
    target 967
  ]
  edge [
    source 16
    target 968
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 16
    target 971
  ]
  edge [
    source 16
    target 972
  ]
  edge [
    source 16
    target 973
  ]
  edge [
    source 16
    target 974
  ]
  edge [
    source 16
    target 975
  ]
  edge [
    source 16
    target 976
  ]
  edge [
    source 16
    target 977
  ]
  edge [
    source 16
    target 978
  ]
  edge [
    source 16
    target 1144
  ]
  edge [
    source 16
    target 1774
  ]
  edge [
    source 16
    target 1775
  ]
  edge [
    source 16
    target 1776
  ]
  edge [
    source 16
    target 1777
  ]
  edge [
    source 16
    target 1778
  ]
  edge [
    source 16
    target 1779
  ]
  edge [
    source 16
    target 1780
  ]
  edge [
    source 16
    target 1781
  ]
  edge [
    source 16
    target 1782
  ]
  edge [
    source 16
    target 1783
  ]
  edge [
    source 16
    target 1784
  ]
  edge [
    source 16
    target 1785
  ]
  edge [
    source 16
    target 1150
  ]
  edge [
    source 16
    target 1786
  ]
  edge [
    source 16
    target 1787
  ]
  edge [
    source 16
    target 1788
  ]
  edge [
    source 16
    target 1789
  ]
  edge [
    source 16
    target 1177
  ]
  edge [
    source 16
    target 1790
  ]
  edge [
    source 16
    target 1791
  ]
  edge [
    source 16
    target 1792
  ]
  edge [
    source 16
    target 1184
  ]
  edge [
    source 16
    target 1793
  ]
  edge [
    source 16
    target 1794
  ]
  edge [
    source 16
    target 1795
  ]
  edge [
    source 16
    target 1796
  ]
  edge [
    source 16
    target 1797
  ]
  edge [
    source 16
    target 1183
  ]
  edge [
    source 16
    target 1798
  ]
  edge [
    source 16
    target 1799
  ]
  edge [
    source 16
    target 1800
  ]
  edge [
    source 16
    target 1801
  ]
  edge [
    source 16
    target 1802
  ]
  edge [
    source 16
    target 1803
  ]
  edge [
    source 16
    target 1804
  ]
  edge [
    source 16
    target 1805
  ]
  edge [
    source 16
    target 1806
  ]
  edge [
    source 16
    target 1807
  ]
  edge [
    source 16
    target 1187
  ]
  edge [
    source 16
    target 1808
  ]
  edge [
    source 16
    target 1809
  ]
  edge [
    source 16
    target 1810
  ]
  edge [
    source 16
    target 1811
  ]
  edge [
    source 16
    target 1812
  ]
  edge [
    source 16
    target 1813
  ]
  edge [
    source 16
    target 1814
  ]
  edge [
    source 16
    target 1815
  ]
  edge [
    source 16
    target 1816
  ]
  edge [
    source 16
    target 1817
  ]
  edge [
    source 16
    target 1818
  ]
  edge [
    source 16
    target 1819
  ]
  edge [
    source 16
    target 1820
  ]
  edge [
    source 16
    target 1821
  ]
  edge [
    source 16
    target 1822
  ]
  edge [
    source 16
    target 1823
  ]
  edge [
    source 16
    target 1092
  ]
  edge [
    source 16
    target 1824
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 1825
  ]
  edge [
    source 16
    target 1826
  ]
  edge [
    source 16
    target 1827
  ]
  edge [
    source 16
    target 1072
  ]
  edge [
    source 16
    target 1828
  ]
  edge [
    source 16
    target 1829
  ]
  edge [
    source 16
    target 1830
  ]
  edge [
    source 16
    target 1831
  ]
  edge [
    source 16
    target 1832
  ]
  edge [
    source 16
    target 1833
  ]
  edge [
    source 16
    target 1834
  ]
  edge [
    source 16
    target 1835
  ]
  edge [
    source 16
    target 1836
  ]
  edge [
    source 16
    target 1837
  ]
  edge [
    source 16
    target 1838
  ]
  edge [
    source 16
    target 1839
  ]
  edge [
    source 16
    target 1421
  ]
  edge [
    source 16
    target 1840
  ]
  edge [
    source 16
    target 1841
  ]
  edge [
    source 16
    target 1842
  ]
  edge [
    source 16
    target 1154
  ]
  edge [
    source 16
    target 1843
  ]
  edge [
    source 16
    target 1844
  ]
  edge [
    source 16
    target 1845
  ]
  edge [
    source 16
    target 1846
  ]
  edge [
    source 16
    target 1847
  ]
  edge [
    source 16
    target 1848
  ]
  edge [
    source 16
    target 1849
  ]
  edge [
    source 16
    target 1850
  ]
  edge [
    source 16
    target 1851
  ]
  edge [
    source 16
    target 1852
  ]
  edge [
    source 16
    target 1853
  ]
  edge [
    source 16
    target 1854
  ]
  edge [
    source 16
    target 1855
  ]
  edge [
    source 16
    target 1856
  ]
  edge [
    source 16
    target 1857
  ]
  edge [
    source 16
    target 1858
  ]
  edge [
    source 16
    target 1859
  ]
  edge [
    source 16
    target 1860
  ]
  edge [
    source 16
    target 1861
  ]
  edge [
    source 16
    target 1862
  ]
  edge [
    source 16
    target 1863
  ]
  edge [
    source 16
    target 1864
  ]
  edge [
    source 16
    target 1865
  ]
  edge [
    source 16
    target 1866
  ]
  edge [
    source 16
    target 1116
  ]
  edge [
    source 16
    target 1867
  ]
  edge [
    source 16
    target 1868
  ]
  edge [
    source 16
    target 1869
  ]
  edge [
    source 16
    target 1870
  ]
  edge [
    source 16
    target 1871
  ]
  edge [
    source 16
    target 1872
  ]
  edge [
    source 16
    target 1873
  ]
  edge [
    source 16
    target 1874
  ]
  edge [
    source 16
    target 1875
  ]
  edge [
    source 16
    target 1876
  ]
  edge [
    source 16
    target 1877
  ]
  edge [
    source 16
    target 1878
  ]
  edge [
    source 16
    target 1879
  ]
  edge [
    source 16
    target 1880
  ]
  edge [
    source 16
    target 1881
  ]
  edge [
    source 16
    target 1882
  ]
  edge [
    source 16
    target 1883
  ]
  edge [
    source 16
    target 1884
  ]
  edge [
    source 16
    target 1885
  ]
  edge [
    source 16
    target 1886
  ]
  edge [
    source 16
    target 1168
  ]
  edge [
    source 16
    target 1887
  ]
  edge [
    source 16
    target 1888
  ]
  edge [
    source 16
    target 1889
  ]
  edge [
    source 16
    target 1890
  ]
  edge [
    source 16
    target 1143
  ]
  edge [
    source 16
    target 1891
  ]
  edge [
    source 16
    target 1892
  ]
  edge [
    source 16
    target 1893
  ]
  edge [
    source 16
    target 1894
  ]
  edge [
    source 16
    target 1895
  ]
  edge [
    source 16
    target 1896
  ]
  edge [
    source 16
    target 1897
  ]
  edge [
    source 16
    target 1898
  ]
  edge [
    source 16
    target 1899
  ]
  edge [
    source 16
    target 1900
  ]
  edge [
    source 16
    target 1901
  ]
  edge [
    source 16
    target 1902
  ]
  edge [
    source 16
    target 1903
  ]
  edge [
    source 16
    target 1904
  ]
  edge [
    source 16
    target 1905
  ]
  edge [
    source 16
    target 1906
  ]
  edge [
    source 16
    target 1907
  ]
  edge [
    source 16
    target 1908
  ]
  edge [
    source 16
    target 1909
  ]
  edge [
    source 16
    target 1910
  ]
  edge [
    source 16
    target 1911
  ]
  edge [
    source 16
    target 1912
  ]
  edge [
    source 16
    target 1913
  ]
  edge [
    source 16
    target 1914
  ]
  edge [
    source 16
    target 1915
  ]
  edge [
    source 16
    target 1916
  ]
  edge [
    source 16
    target 1917
  ]
  edge [
    source 16
    target 1918
  ]
  edge [
    source 16
    target 1919
  ]
  edge [
    source 16
    target 1920
  ]
  edge [
    source 16
    target 1921
  ]
  edge [
    source 16
    target 1922
  ]
  edge [
    source 16
    target 1923
  ]
  edge [
    source 16
    target 1162
  ]
  edge [
    source 16
    target 1924
  ]
  edge [
    source 16
    target 1925
  ]
  edge [
    source 16
    target 1926
  ]
  edge [
    source 16
    target 1927
  ]
  edge [
    source 16
    target 1928
  ]
  edge [
    source 16
    target 1929
  ]
  edge [
    source 16
    target 1930
  ]
  edge [
    source 16
    target 1931
  ]
  edge [
    source 16
    target 1932
  ]
  edge [
    source 16
    target 1933
  ]
  edge [
    source 16
    target 1934
  ]
  edge [
    source 16
    target 1935
  ]
  edge [
    source 16
    target 1936
  ]
  edge [
    source 16
    target 1937
  ]
  edge [
    source 16
    target 1938
  ]
  edge [
    source 16
    target 1939
  ]
  edge [
    source 16
    target 1940
  ]
  edge [
    source 16
    target 1941
  ]
  edge [
    source 16
    target 1942
  ]
  edge [
    source 16
    target 1943
  ]
  edge [
    source 16
    target 1944
  ]
  edge [
    source 16
    target 1945
  ]
  edge [
    source 16
    target 1189
  ]
  edge [
    source 16
    target 1946
  ]
  edge [
    source 16
    target 1947
  ]
  edge [
    source 16
    target 1948
  ]
  edge [
    source 16
    target 1949
  ]
  edge [
    source 16
    target 1950
  ]
  edge [
    source 16
    target 1951
  ]
  edge [
    source 16
    target 1952
  ]
  edge [
    source 16
    target 1953
  ]
  edge [
    source 16
    target 1954
  ]
  edge [
    source 16
    target 1955
  ]
  edge [
    source 16
    target 1956
  ]
  edge [
    source 16
    target 1957
  ]
  edge [
    source 16
    target 1958
  ]
  edge [
    source 16
    target 1959
  ]
  edge [
    source 16
    target 1960
  ]
  edge [
    source 16
    target 1961
  ]
  edge [
    source 16
    target 1962
  ]
  edge [
    source 16
    target 1963
  ]
  edge [
    source 16
    target 1964
  ]
  edge [
    source 16
    target 1965
  ]
  edge [
    source 16
    target 1966
  ]
  edge [
    source 16
    target 1967
  ]
  edge [
    source 16
    target 1247
  ]
  edge [
    source 16
    target 1968
  ]
  edge [
    source 16
    target 1969
  ]
  edge [
    source 16
    target 1970
  ]
  edge [
    source 16
    target 1085
  ]
  edge [
    source 16
    target 1971
  ]
  edge [
    source 16
    target 1972
  ]
  edge [
    source 16
    target 1973
  ]
  edge [
    source 16
    target 1974
  ]
  edge [
    source 16
    target 1975
  ]
  edge [
    source 16
    target 1976
  ]
  edge [
    source 16
    target 1977
  ]
  edge [
    source 16
    target 1978
  ]
  edge [
    source 16
    target 1979
  ]
  edge [
    source 16
    target 1980
  ]
  edge [
    source 16
    target 1981
  ]
  edge [
    source 16
    target 1982
  ]
  edge [
    source 16
    target 1983
  ]
  edge [
    source 16
    target 1712
  ]
  edge [
    source 16
    target 1984
  ]
  edge [
    source 16
    target 1985
  ]
  edge [
    source 16
    target 1986
  ]
  edge [
    source 16
    target 1987
  ]
  edge [
    source 16
    target 1988
  ]
  edge [
    source 16
    target 1989
  ]
  edge [
    source 16
    target 1990
  ]
  edge [
    source 16
    target 1991
  ]
  edge [
    source 16
    target 1992
  ]
  edge [
    source 16
    target 1115
  ]
  edge [
    source 16
    target 1993
  ]
  edge [
    source 16
    target 1994
  ]
  edge [
    source 16
    target 1995
  ]
  edge [
    source 16
    target 1996
  ]
  edge [
    source 16
    target 1997
  ]
  edge [
    source 16
    target 1998
  ]
  edge [
    source 16
    target 1999
  ]
  edge [
    source 16
    target 2000
  ]
  edge [
    source 16
    target 2001
  ]
  edge [
    source 16
    target 2002
  ]
  edge [
    source 16
    target 2003
  ]
  edge [
    source 16
    target 2004
  ]
  edge [
    source 16
    target 2005
  ]
  edge [
    source 16
    target 2006
  ]
  edge [
    source 16
    target 2007
  ]
  edge [
    source 16
    target 2008
  ]
  edge [
    source 16
    target 2009
  ]
  edge [
    source 16
    target 2010
  ]
  edge [
    source 16
    target 2011
  ]
  edge [
    source 16
    target 2012
  ]
  edge [
    source 16
    target 2013
  ]
  edge [
    source 16
    target 2014
  ]
  edge [
    source 16
    target 2015
  ]
  edge [
    source 16
    target 2016
  ]
  edge [
    source 16
    target 2017
  ]
  edge [
    source 16
    target 2018
  ]
  edge [
    source 16
    target 2019
  ]
  edge [
    source 16
    target 2020
  ]
  edge [
    source 16
    target 2021
  ]
  edge [
    source 16
    target 2022
  ]
  edge [
    source 16
    target 1170
  ]
  edge [
    source 16
    target 2023
  ]
  edge [
    source 16
    target 2024
  ]
  edge [
    source 16
    target 2025
  ]
  edge [
    source 16
    target 2026
  ]
  edge [
    source 16
    target 2027
  ]
  edge [
    source 16
    target 2028
  ]
  edge [
    source 16
    target 1110
  ]
  edge [
    source 16
    target 2029
  ]
  edge [
    source 16
    target 2030
  ]
  edge [
    source 16
    target 69
  ]
  edge [
    source 16
    target 2031
  ]
  edge [
    source 16
    target 2032
  ]
  edge [
    source 16
    target 2033
  ]
  edge [
    source 16
    target 2034
  ]
  edge [
    source 16
    target 2035
  ]
  edge [
    source 16
    target 2036
  ]
  edge [
    source 16
    target 2037
  ]
  edge [
    source 16
    target 2038
  ]
  edge [
    source 16
    target 2039
  ]
  edge [
    source 16
    target 2040
  ]
  edge [
    source 16
    target 2041
  ]
  edge [
    source 16
    target 2042
  ]
  edge [
    source 16
    target 2043
  ]
  edge [
    source 16
    target 2044
  ]
  edge [
    source 16
    target 2045
  ]
  edge [
    source 16
    target 2046
  ]
  edge [
    source 16
    target 2047
  ]
  edge [
    source 16
    target 2048
  ]
  edge [
    source 16
    target 2049
  ]
  edge [
    source 16
    target 2050
  ]
  edge [
    source 16
    target 2051
  ]
  edge [
    source 16
    target 2052
  ]
  edge [
    source 16
    target 2053
  ]
  edge [
    source 16
    target 1097
  ]
  edge [
    source 16
    target 2054
  ]
  edge [
    source 16
    target 2055
  ]
  edge [
    source 16
    target 2056
  ]
  edge [
    source 16
    target 2057
  ]
  edge [
    source 16
    target 2058
  ]
  edge [
    source 16
    target 2059
  ]
  edge [
    source 16
    target 2060
  ]
  edge [
    source 16
    target 2061
  ]
  edge [
    source 16
    target 2062
  ]
  edge [
    source 16
    target 2063
  ]
  edge [
    source 16
    target 2064
  ]
  edge [
    source 16
    target 2065
  ]
  edge [
    source 16
    target 2066
  ]
  edge [
    source 16
    target 1171
  ]
  edge [
    source 16
    target 2067
  ]
  edge [
    source 16
    target 2068
  ]
  edge [
    source 16
    target 2069
  ]
  edge [
    source 16
    target 2070
  ]
  edge [
    source 16
    target 2071
  ]
  edge [
    source 16
    target 2072
  ]
  edge [
    source 16
    target 2073
  ]
  edge [
    source 16
    target 2074
  ]
  edge [
    source 16
    target 2075
  ]
  edge [
    source 16
    target 2076
  ]
  edge [
    source 16
    target 2077
  ]
  edge [
    source 16
    target 2078
  ]
  edge [
    source 16
    target 2079
  ]
  edge [
    source 16
    target 2080
  ]
  edge [
    source 16
    target 2081
  ]
  edge [
    source 16
    target 2082
  ]
  edge [
    source 16
    target 2083
  ]
  edge [
    source 16
    target 2084
  ]
  edge [
    source 16
    target 1041
  ]
  edge [
    source 16
    target 2085
  ]
  edge [
    source 16
    target 2086
  ]
  edge [
    source 16
    target 1121
  ]
  edge [
    source 16
    target 2087
  ]
  edge [
    source 16
    target 2088
  ]
  edge [
    source 16
    target 2089
  ]
  edge [
    source 16
    target 2090
  ]
  edge [
    source 16
    target 2091
  ]
  edge [
    source 16
    target 2092
  ]
  edge [
    source 16
    target 2093
  ]
  edge [
    source 16
    target 2094
  ]
  edge [
    source 16
    target 2095
  ]
  edge [
    source 16
    target 2096
  ]
  edge [
    source 16
    target 2097
  ]
  edge [
    source 16
    target 2098
  ]
  edge [
    source 16
    target 2099
  ]
  edge [
    source 16
    target 2100
  ]
  edge [
    source 16
    target 2101
  ]
  edge [
    source 16
    target 2102
  ]
  edge [
    source 16
    target 2103
  ]
  edge [
    source 16
    target 2104
  ]
  edge [
    source 16
    target 2105
  ]
  edge [
    source 16
    target 2106
  ]
  edge [
    source 16
    target 2107
  ]
  edge [
    source 16
    target 2108
  ]
  edge [
    source 16
    target 2109
  ]
  edge [
    source 16
    target 2110
  ]
  edge [
    source 16
    target 2111
  ]
  edge [
    source 16
    target 2112
  ]
  edge [
    source 16
    target 2113
  ]
  edge [
    source 16
    target 2114
  ]
  edge [
    source 16
    target 2115
  ]
  edge [
    source 16
    target 2116
  ]
  edge [
    source 16
    target 2117
  ]
  edge [
    source 16
    target 2118
  ]
  edge [
    source 16
    target 2119
  ]
  edge [
    source 16
    target 2120
  ]
  edge [
    source 16
    target 2121
  ]
  edge [
    source 16
    target 2122
  ]
  edge [
    source 16
    target 2123
  ]
  edge [
    source 16
    target 2124
  ]
  edge [
    source 16
    target 2125
  ]
  edge [
    source 16
    target 2126
  ]
  edge [
    source 16
    target 2127
  ]
  edge [
    source 16
    target 2128
  ]
  edge [
    source 16
    target 2129
  ]
  edge [
    source 16
    target 2130
  ]
  edge [
    source 16
    target 2131
  ]
  edge [
    source 16
    target 2132
  ]
  edge [
    source 16
    target 2133
  ]
  edge [
    source 16
    target 2134
  ]
  edge [
    source 16
    target 2135
  ]
  edge [
    source 16
    target 2136
  ]
  edge [
    source 16
    target 2137
  ]
  edge [
    source 16
    target 2138
  ]
  edge [
    source 16
    target 2139
  ]
  edge [
    source 16
    target 2140
  ]
  edge [
    source 16
    target 2141
  ]
  edge [
    source 16
    target 2142
  ]
  edge [
    source 16
    target 2143
  ]
  edge [
    source 16
    target 2144
  ]
  edge [
    source 16
    target 2145
  ]
  edge [
    source 16
    target 2146
  ]
  edge [
    source 16
    target 2147
  ]
  edge [
    source 16
    target 2148
  ]
  edge [
    source 16
    target 2149
  ]
  edge [
    source 16
    target 2150
  ]
  edge [
    source 16
    target 2151
  ]
  edge [
    source 16
    target 2152
  ]
  edge [
    source 16
    target 2153
  ]
  edge [
    source 16
    target 2154
  ]
  edge [
    source 16
    target 2155
  ]
  edge [
    source 16
    target 2156
  ]
  edge [
    source 16
    target 2157
  ]
  edge [
    source 16
    target 2158
  ]
  edge [
    source 16
    target 2159
  ]
  edge [
    source 16
    target 2160
  ]
  edge [
    source 16
    target 2161
  ]
  edge [
    source 16
    target 2162
  ]
  edge [
    source 16
    target 2163
  ]
  edge [
    source 16
    target 2164
  ]
  edge [
    source 16
    target 2165
  ]
  edge [
    source 16
    target 2166
  ]
  edge [
    source 16
    target 2167
  ]
  edge [
    source 16
    target 2168
  ]
  edge [
    source 16
    target 2169
  ]
  edge [
    source 16
    target 2170
  ]
  edge [
    source 16
    target 2171
  ]
  edge [
    source 16
    target 2172
  ]
  edge [
    source 16
    target 2173
  ]
  edge [
    source 16
    target 2174
  ]
  edge [
    source 16
    target 2175
  ]
  edge [
    source 16
    target 2176
  ]
  edge [
    source 16
    target 2177
  ]
  edge [
    source 16
    target 1073
  ]
  edge [
    source 16
    target 1074
  ]
  edge [
    source 16
    target 1141
  ]
  edge [
    source 16
    target 1179
  ]
  edge [
    source 16
    target 1122
  ]
  edge [
    source 16
    target 1145
  ]
  edge [
    source 16
    target 1147
  ]
  edge [
    source 16
    target 351
  ]
  edge [
    source 16
    target 1148
  ]
  edge [
    source 16
    target 1149
  ]
  edge [
    source 16
    target 1151
  ]
  edge [
    source 16
    target 1152
  ]
  edge [
    source 16
    target 1180
  ]
  edge [
    source 16
    target 1117
  ]
  edge [
    source 16
    target 1125
  ]
  edge [
    source 16
    target 1190
  ]
  edge [
    source 16
    target 1164
  ]
  edge [
    source 16
    target 1165
  ]
  edge [
    source 16
    target 1166
  ]
  edge [
    source 16
    target 1087
  ]
  edge [
    source 16
    target 1178
  ]
  edge [
    source 16
    target 1163
  ]
  edge [
    source 16
    target 1094
  ]
  edge [
    source 16
    target 1111
  ]
  edge [
    source 16
    target 1127
  ]
  edge [
    source 16
    target 1128
  ]
  edge [
    source 16
    target 1129
  ]
  edge [
    source 16
    target 1130
  ]
  edge [
    source 16
    target 1131
  ]
  edge [
    source 16
    target 1123
  ]
  edge [
    source 16
    target 1093
  ]
  edge [
    source 16
    target 1126
  ]
  edge [
    source 16
    target 1091
  ]
  edge [
    source 16
    target 1175
  ]
  edge [
    source 16
    target 1176
  ]
  edge [
    source 16
    target 1185
  ]
  edge [
    source 16
    target 1186
  ]
  edge [
    source 16
    target 1188
  ]
  edge [
    source 16
    target 1095
  ]
  edge [
    source 16
    target 1096
  ]
  edge [
    source 16
    target 1161
  ]
  edge [
    source 16
    target 1089
  ]
  edge [
    source 16
    target 1134
  ]
  edge [
    source 16
    target 1155
  ]
  edge [
    source 16
    target 1137
  ]
  edge [
    source 16
    target 1098
  ]
  edge [
    source 16
    target 1100
  ]
  edge [
    source 16
    target 1181
  ]
  edge [
    source 16
    target 1182
  ]
  edge [
    source 16
    target 1102
  ]
  edge [
    source 16
    target 1103
  ]
  edge [
    source 16
    target 1104
  ]
  edge [
    source 16
    target 1105
  ]
  edge [
    source 16
    target 1106
  ]
  edge [
    source 16
    target 1132
  ]
  edge [
    source 16
    target 1133
  ]
  edge [
    source 16
    target 1139
  ]
  edge [
    source 16
    target 1140
  ]
  edge [
    source 16
    target 1142
  ]
  edge [
    source 16
    target 1077
  ]
  edge [
    source 16
    target 1081
  ]
  edge [
    source 16
    target 1083
  ]
  edge [
    source 16
    target 1153
  ]
]
