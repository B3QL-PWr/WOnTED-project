graph [
  node [
    id 0
    label "tried"
    origin "text"
  ]
  node [
    id 1
    label "somebody's"
    origin "text"
  ]
  node [
    id 2
    label "work"
    origin "text"
  ]
  node [
    id 3
    label "after"
    origin "text"
  ]
  node [
    id 4
    label "interview"
    origin "text"
  ]
  node [
    id 5
    label "autoryzowanie"
  ]
  node [
    id 6
    label "rozmowa"
  ]
  node [
    id 7
    label "autoryzowa&#263;"
  ]
  node [
    id 8
    label "consultation"
  ]
  node [
    id 9
    label "cisza"
  ]
  node [
    id 10
    label "odpowied&#378;"
  ]
  node [
    id 11
    label "rozhowor"
  ]
  node [
    id 12
    label "discussion"
  ]
  node [
    id 13
    label "czynno&#347;&#263;"
  ]
  node [
    id 14
    label "zezwalanie"
  ]
  node [
    id 15
    label "wywiad"
  ]
  node [
    id 16
    label "upowa&#380;nianie"
  ]
  node [
    id 17
    label "pozwolenie"
  ]
  node [
    id 18
    label "authority"
  ]
  node [
    id 19
    label "zatwierdzanie"
  ]
  node [
    id 20
    label "upowa&#380;nienie"
  ]
  node [
    id 21
    label "mandate"
  ]
  node [
    id 22
    label "zatwierdzenie"
  ]
  node [
    id 23
    label "zatwierdzi&#263;"
  ]
  node [
    id 24
    label "zatwierdza&#263;"
  ]
  node [
    id 25
    label "pozwoli&#263;"
  ]
  node [
    id 26
    label "zezwala&#263;"
  ]
  node [
    id 27
    label "upowa&#380;nia&#263;"
  ]
  node [
    id 28
    label "upowa&#380;ni&#263;"
  ]
  node [
    id 29
    label "authorize"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
]
