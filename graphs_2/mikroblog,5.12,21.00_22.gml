graph [
  node [
    id 0
    label "historia"
    origin "text"
  ]
  node [
    id 1
    label "edycja"
    origin "text"
  ]
  node [
    id 2
    label "post"
    origin "text"
  ]
  node [
    id 3
    label "leszke"
    origin "text"
  ]
  node [
    id 4
    label "xdddd"
    origin "text"
  ]
  node [
    id 5
    label "historiografia"
  ]
  node [
    id 6
    label "nauka_humanistyczna"
  ]
  node [
    id 7
    label "nautologia"
  ]
  node [
    id 8
    label "przedmiot"
  ]
  node [
    id 9
    label "epigrafika"
  ]
  node [
    id 10
    label "muzealnictwo"
  ]
  node [
    id 11
    label "report"
  ]
  node [
    id 12
    label "hista"
  ]
  node [
    id 13
    label "przebiec"
  ]
  node [
    id 14
    label "zabytkoznawstwo"
  ]
  node [
    id 15
    label "historia_gospodarcza"
  ]
  node [
    id 16
    label "motyw"
  ]
  node [
    id 17
    label "kierunek"
  ]
  node [
    id 18
    label "varsavianistyka"
  ]
  node [
    id 19
    label "filigranistyka"
  ]
  node [
    id 20
    label "neografia"
  ]
  node [
    id 21
    label "prezentyzm"
  ]
  node [
    id 22
    label "bizantynistyka"
  ]
  node [
    id 23
    label "ikonografia"
  ]
  node [
    id 24
    label "genealogia"
  ]
  node [
    id 25
    label "epoka"
  ]
  node [
    id 26
    label "historia_sztuki"
  ]
  node [
    id 27
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 28
    label "ruralistyka"
  ]
  node [
    id 29
    label "annalistyka"
  ]
  node [
    id 30
    label "charakter"
  ]
  node [
    id 31
    label "papirologia"
  ]
  node [
    id 32
    label "heraldyka"
  ]
  node [
    id 33
    label "archiwistyka"
  ]
  node [
    id 34
    label "dyplomatyka"
  ]
  node [
    id 35
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 36
    label "czynno&#347;&#263;"
  ]
  node [
    id 37
    label "numizmatyka"
  ]
  node [
    id 38
    label "chronologia"
  ]
  node [
    id 39
    label "wypowied&#378;"
  ]
  node [
    id 40
    label "historyka"
  ]
  node [
    id 41
    label "prozopografia"
  ]
  node [
    id 42
    label "sfragistyka"
  ]
  node [
    id 43
    label "weksylologia"
  ]
  node [
    id 44
    label "paleografia"
  ]
  node [
    id 45
    label "mediewistyka"
  ]
  node [
    id 46
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 47
    label "przebiegni&#281;cie"
  ]
  node [
    id 48
    label "fabu&#322;a"
  ]
  node [
    id 49
    label "koleje_losu"
  ]
  node [
    id 50
    label "&#380;ycie"
  ]
  node [
    id 51
    label "czas"
  ]
  node [
    id 52
    label "zboczenie"
  ]
  node [
    id 53
    label "om&#243;wienie"
  ]
  node [
    id 54
    label "sponiewieranie"
  ]
  node [
    id 55
    label "discipline"
  ]
  node [
    id 56
    label "rzecz"
  ]
  node [
    id 57
    label "omawia&#263;"
  ]
  node [
    id 58
    label "kr&#261;&#380;enie"
  ]
  node [
    id 59
    label "tre&#347;&#263;"
  ]
  node [
    id 60
    label "robienie"
  ]
  node [
    id 61
    label "sponiewiera&#263;"
  ]
  node [
    id 62
    label "element"
  ]
  node [
    id 63
    label "entity"
  ]
  node [
    id 64
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 65
    label "tematyka"
  ]
  node [
    id 66
    label "w&#261;tek"
  ]
  node [
    id 67
    label "zbaczanie"
  ]
  node [
    id 68
    label "program_nauczania"
  ]
  node [
    id 69
    label "om&#243;wi&#263;"
  ]
  node [
    id 70
    label "omawianie"
  ]
  node [
    id 71
    label "thing"
  ]
  node [
    id 72
    label "kultura"
  ]
  node [
    id 73
    label "istota"
  ]
  node [
    id 74
    label "zbacza&#263;"
  ]
  node [
    id 75
    label "zboczy&#263;"
  ]
  node [
    id 76
    label "pos&#322;uchanie"
  ]
  node [
    id 77
    label "s&#261;d"
  ]
  node [
    id 78
    label "sparafrazowanie"
  ]
  node [
    id 79
    label "strawestowa&#263;"
  ]
  node [
    id 80
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 81
    label "trawestowa&#263;"
  ]
  node [
    id 82
    label "sparafrazowa&#263;"
  ]
  node [
    id 83
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 84
    label "sformu&#322;owanie"
  ]
  node [
    id 85
    label "parafrazowanie"
  ]
  node [
    id 86
    label "ozdobnik"
  ]
  node [
    id 87
    label "delimitacja"
  ]
  node [
    id 88
    label "parafrazowa&#263;"
  ]
  node [
    id 89
    label "stylizacja"
  ]
  node [
    id 90
    label "komunikat"
  ]
  node [
    id 91
    label "trawestowanie"
  ]
  node [
    id 92
    label "strawestowanie"
  ]
  node [
    id 93
    label "rezultat"
  ]
  node [
    id 94
    label "przebieg"
  ]
  node [
    id 95
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 96
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 97
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 98
    label "praktyka"
  ]
  node [
    id 99
    label "system"
  ]
  node [
    id 100
    label "przeorientowywanie"
  ]
  node [
    id 101
    label "studia"
  ]
  node [
    id 102
    label "linia"
  ]
  node [
    id 103
    label "bok"
  ]
  node [
    id 104
    label "skr&#281;canie"
  ]
  node [
    id 105
    label "skr&#281;ca&#263;"
  ]
  node [
    id 106
    label "przeorientowywa&#263;"
  ]
  node [
    id 107
    label "orientowanie"
  ]
  node [
    id 108
    label "skr&#281;ci&#263;"
  ]
  node [
    id 109
    label "przeorientowanie"
  ]
  node [
    id 110
    label "zorientowanie"
  ]
  node [
    id 111
    label "przeorientowa&#263;"
  ]
  node [
    id 112
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 113
    label "metoda"
  ]
  node [
    id 114
    label "ty&#322;"
  ]
  node [
    id 115
    label "zorientowa&#263;"
  ]
  node [
    id 116
    label "g&#243;ra"
  ]
  node [
    id 117
    label "orientowa&#263;"
  ]
  node [
    id 118
    label "spos&#243;b"
  ]
  node [
    id 119
    label "ideologia"
  ]
  node [
    id 120
    label "orientacja"
  ]
  node [
    id 121
    label "prz&#243;d"
  ]
  node [
    id 122
    label "bearing"
  ]
  node [
    id 123
    label "skr&#281;cenie"
  ]
  node [
    id 124
    label "aalen"
  ]
  node [
    id 125
    label "jura_wczesna"
  ]
  node [
    id 126
    label "holocen"
  ]
  node [
    id 127
    label "pliocen"
  ]
  node [
    id 128
    label "plejstocen"
  ]
  node [
    id 129
    label "paleocen"
  ]
  node [
    id 130
    label "dzieje"
  ]
  node [
    id 131
    label "bajos"
  ]
  node [
    id 132
    label "kelowej"
  ]
  node [
    id 133
    label "eocen"
  ]
  node [
    id 134
    label "jednostka_geologiczna"
  ]
  node [
    id 135
    label "okres"
  ]
  node [
    id 136
    label "schy&#322;ek"
  ]
  node [
    id 137
    label "miocen"
  ]
  node [
    id 138
    label "&#347;rodkowy_trias"
  ]
  node [
    id 139
    label "term"
  ]
  node [
    id 140
    label "Zeitgeist"
  ]
  node [
    id 141
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 142
    label "wczesny_trias"
  ]
  node [
    id 143
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 144
    label "jura_&#347;rodkowa"
  ]
  node [
    id 145
    label "oligocen"
  ]
  node [
    id 146
    label "w&#281;ze&#322;"
  ]
  node [
    id 147
    label "perypetia"
  ]
  node [
    id 148
    label "opowiadanie"
  ]
  node [
    id 149
    label "Byzantine_Empire"
  ]
  node [
    id 150
    label "metodologia"
  ]
  node [
    id 151
    label "datacja"
  ]
  node [
    id 152
    label "dendrochronologia"
  ]
  node [
    id 153
    label "kolejno&#347;&#263;"
  ]
  node [
    id 154
    label "bibliologia"
  ]
  node [
    id 155
    label "pismo"
  ]
  node [
    id 156
    label "brachygrafia"
  ]
  node [
    id 157
    label "architektura"
  ]
  node [
    id 158
    label "nauka"
  ]
  node [
    id 159
    label "museum"
  ]
  node [
    id 160
    label "archiwoznawstwo"
  ]
  node [
    id 161
    label "historiography"
  ]
  node [
    id 162
    label "pi&#347;miennictwo"
  ]
  node [
    id 163
    label "archeologia"
  ]
  node [
    id 164
    label "plastyka"
  ]
  node [
    id 165
    label "oksza"
  ]
  node [
    id 166
    label "pas"
  ]
  node [
    id 167
    label "s&#322;up"
  ]
  node [
    id 168
    label "barwa_heraldyczna"
  ]
  node [
    id 169
    label "herb"
  ]
  node [
    id 170
    label "or&#281;&#380;"
  ]
  node [
    id 171
    label "&#347;redniowiecze"
  ]
  node [
    id 172
    label "descendencja"
  ]
  node [
    id 173
    label "drzewo_genealogiczne"
  ]
  node [
    id 174
    label "procedencja"
  ]
  node [
    id 175
    label "pochodzenie"
  ]
  node [
    id 176
    label "medal"
  ]
  node [
    id 177
    label "kolekcjonerstwo"
  ]
  node [
    id 178
    label "numismatics"
  ]
  node [
    id 179
    label "fraza"
  ]
  node [
    id 180
    label "temat"
  ]
  node [
    id 181
    label "wydarzenie"
  ]
  node [
    id 182
    label "melodia"
  ]
  node [
    id 183
    label "cecha"
  ]
  node [
    id 184
    label "przyczyna"
  ]
  node [
    id 185
    label "sytuacja"
  ]
  node [
    id 186
    label "ozdoba"
  ]
  node [
    id 187
    label "umowa"
  ]
  node [
    id 188
    label "cover"
  ]
  node [
    id 189
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 190
    label "przeby&#263;"
  ]
  node [
    id 191
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 192
    label "run"
  ]
  node [
    id 193
    label "proceed"
  ]
  node [
    id 194
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 195
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 196
    label "przemierzy&#263;"
  ]
  node [
    id 197
    label "fly"
  ]
  node [
    id 198
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 199
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 200
    label "przesun&#261;&#263;"
  ]
  node [
    id 201
    label "activity"
  ]
  node [
    id 202
    label "bezproblemowy"
  ]
  node [
    id 203
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 204
    label "zbi&#243;r"
  ]
  node [
    id 205
    label "cz&#322;owiek"
  ]
  node [
    id 206
    label "osobowo&#347;&#263;"
  ]
  node [
    id 207
    label "psychika"
  ]
  node [
    id 208
    label "posta&#263;"
  ]
  node [
    id 209
    label "kompleksja"
  ]
  node [
    id 210
    label "fizjonomia"
  ]
  node [
    id 211
    label "zjawisko"
  ]
  node [
    id 212
    label "przemkni&#281;cie"
  ]
  node [
    id 213
    label "zabrzmienie"
  ]
  node [
    id 214
    label "przebycie"
  ]
  node [
    id 215
    label "zdarzenie_si&#281;"
  ]
  node [
    id 216
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 217
    label "egzemplarz"
  ]
  node [
    id 218
    label "impression"
  ]
  node [
    id 219
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 220
    label "odmiana"
  ]
  node [
    id 221
    label "cykl"
  ]
  node [
    id 222
    label "notification"
  ]
  node [
    id 223
    label "zmiana"
  ]
  node [
    id 224
    label "produkcja"
  ]
  node [
    id 225
    label "mutant"
  ]
  node [
    id 226
    label "rewizja"
  ]
  node [
    id 227
    label "gramatyka"
  ]
  node [
    id 228
    label "typ"
  ]
  node [
    id 229
    label "paradygmat"
  ]
  node [
    id 230
    label "jednostka_systematyczna"
  ]
  node [
    id 231
    label "change"
  ]
  node [
    id 232
    label "podgatunek"
  ]
  node [
    id 233
    label "ferment"
  ]
  node [
    id 234
    label "rasa"
  ]
  node [
    id 235
    label "impreza"
  ]
  node [
    id 236
    label "realizacja"
  ]
  node [
    id 237
    label "tingel-tangel"
  ]
  node [
    id 238
    label "wydawa&#263;"
  ]
  node [
    id 239
    label "numer"
  ]
  node [
    id 240
    label "monta&#380;"
  ]
  node [
    id 241
    label "wyda&#263;"
  ]
  node [
    id 242
    label "postprodukcja"
  ]
  node [
    id 243
    label "performance"
  ]
  node [
    id 244
    label "fabrication"
  ]
  node [
    id 245
    label "product"
  ]
  node [
    id 246
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 247
    label "uzysk"
  ]
  node [
    id 248
    label "rozw&#243;j"
  ]
  node [
    id 249
    label "odtworzenie"
  ]
  node [
    id 250
    label "dorobek"
  ]
  node [
    id 251
    label "kreacja"
  ]
  node [
    id 252
    label "trema"
  ]
  node [
    id 253
    label "creation"
  ]
  node [
    id 254
    label "kooperowa&#263;"
  ]
  node [
    id 255
    label "czynnik_biotyczny"
  ]
  node [
    id 256
    label "wyewoluowanie"
  ]
  node [
    id 257
    label "reakcja"
  ]
  node [
    id 258
    label "individual"
  ]
  node [
    id 259
    label "przyswoi&#263;"
  ]
  node [
    id 260
    label "wytw&#243;r"
  ]
  node [
    id 261
    label "starzenie_si&#281;"
  ]
  node [
    id 262
    label "wyewoluowa&#263;"
  ]
  node [
    id 263
    label "okaz"
  ]
  node [
    id 264
    label "part"
  ]
  node [
    id 265
    label "ewoluowa&#263;"
  ]
  node [
    id 266
    label "przyswojenie"
  ]
  node [
    id 267
    label "ewoluowanie"
  ]
  node [
    id 268
    label "obiekt"
  ]
  node [
    id 269
    label "sztuka"
  ]
  node [
    id 270
    label "agent"
  ]
  node [
    id 271
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 272
    label "przyswaja&#263;"
  ]
  node [
    id 273
    label "nicpo&#324;"
  ]
  node [
    id 274
    label "przyswajanie"
  ]
  node [
    id 275
    label "passage"
  ]
  node [
    id 276
    label "oznaka"
  ]
  node [
    id 277
    label "komplet"
  ]
  node [
    id 278
    label "anatomopatolog"
  ]
  node [
    id 279
    label "zmianka"
  ]
  node [
    id 280
    label "amendment"
  ]
  node [
    id 281
    label "praca"
  ]
  node [
    id 282
    label "odmienianie"
  ]
  node [
    id 283
    label "tura"
  ]
  node [
    id 284
    label "set"
  ]
  node [
    id 285
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 286
    label "miesi&#261;czka"
  ]
  node [
    id 287
    label "owulacja"
  ]
  node [
    id 288
    label "sekwencja"
  ]
  node [
    id 289
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 290
    label "cycle"
  ]
  node [
    id 291
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 292
    label "zachowanie"
  ]
  node [
    id 293
    label "zachowywanie"
  ]
  node [
    id 294
    label "rok_ko&#347;cielny"
  ]
  node [
    id 295
    label "tekst"
  ]
  node [
    id 296
    label "zachowa&#263;"
  ]
  node [
    id 297
    label "zachowywa&#263;"
  ]
  node [
    id 298
    label "practice"
  ]
  node [
    id 299
    label "wiedza"
  ]
  node [
    id 300
    label "znawstwo"
  ]
  node [
    id 301
    label "skill"
  ]
  node [
    id 302
    label "czyn"
  ]
  node [
    id 303
    label "zwyczaj"
  ]
  node [
    id 304
    label "eksperiencja"
  ]
  node [
    id 305
    label "poprzedzanie"
  ]
  node [
    id 306
    label "czasoprzestrze&#324;"
  ]
  node [
    id 307
    label "laba"
  ]
  node [
    id 308
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 309
    label "chronometria"
  ]
  node [
    id 310
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 311
    label "rachuba_czasu"
  ]
  node [
    id 312
    label "przep&#322;ywanie"
  ]
  node [
    id 313
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 314
    label "czasokres"
  ]
  node [
    id 315
    label "odczyt"
  ]
  node [
    id 316
    label "chwila"
  ]
  node [
    id 317
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 318
    label "kategoria_gramatyczna"
  ]
  node [
    id 319
    label "poprzedzenie"
  ]
  node [
    id 320
    label "trawienie"
  ]
  node [
    id 321
    label "pochodzi&#263;"
  ]
  node [
    id 322
    label "period"
  ]
  node [
    id 323
    label "okres_czasu"
  ]
  node [
    id 324
    label "poprzedza&#263;"
  ]
  node [
    id 325
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 326
    label "odwlekanie_si&#281;"
  ]
  node [
    id 327
    label "zegar"
  ]
  node [
    id 328
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 329
    label "czwarty_wymiar"
  ]
  node [
    id 330
    label "koniugacja"
  ]
  node [
    id 331
    label "trawi&#263;"
  ]
  node [
    id 332
    label "pogoda"
  ]
  node [
    id 333
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 334
    label "poprzedzi&#263;"
  ]
  node [
    id 335
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 336
    label "time_period"
  ]
  node [
    id 337
    label "continence"
  ]
  node [
    id 338
    label "ekscerpcja"
  ]
  node [
    id 339
    label "j&#281;zykowo"
  ]
  node [
    id 340
    label "redakcja"
  ]
  node [
    id 341
    label "pomini&#281;cie"
  ]
  node [
    id 342
    label "dzie&#322;o"
  ]
  node [
    id 343
    label "preparacja"
  ]
  node [
    id 344
    label "odmianka"
  ]
  node [
    id 345
    label "opu&#347;ci&#263;"
  ]
  node [
    id 346
    label "koniektura"
  ]
  node [
    id 347
    label "pisa&#263;"
  ]
  node [
    id 348
    label "obelga"
  ]
  node [
    id 349
    label "post&#261;pi&#263;"
  ]
  node [
    id 350
    label "tajemnica"
  ]
  node [
    id 351
    label "pami&#281;&#263;"
  ]
  node [
    id 352
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 353
    label "zdyscyplinowanie"
  ]
  node [
    id 354
    label "zrobi&#263;"
  ]
  node [
    id 355
    label "przechowa&#263;"
  ]
  node [
    id 356
    label "preserve"
  ]
  node [
    id 357
    label "dieta"
  ]
  node [
    id 358
    label "bury"
  ]
  node [
    id 359
    label "podtrzyma&#263;"
  ]
  node [
    id 360
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 361
    label "struktura"
  ]
  node [
    id 362
    label "pochowanie"
  ]
  node [
    id 363
    label "post&#261;pienie"
  ]
  node [
    id 364
    label "zwierz&#281;"
  ]
  node [
    id 365
    label "behawior"
  ]
  node [
    id 366
    label "observation"
  ]
  node [
    id 367
    label "podtrzymanie"
  ]
  node [
    id 368
    label "etolog"
  ]
  node [
    id 369
    label "przechowanie"
  ]
  node [
    id 370
    label "zrobienie"
  ]
  node [
    id 371
    label "robi&#263;"
  ]
  node [
    id 372
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 373
    label "podtrzymywa&#263;"
  ]
  node [
    id 374
    label "control"
  ]
  node [
    id 375
    label "przechowywa&#263;"
  ]
  node [
    id 376
    label "behave"
  ]
  node [
    id 377
    label "hold"
  ]
  node [
    id 378
    label "post&#281;powa&#263;"
  ]
  node [
    id 379
    label "podtrzymywanie"
  ]
  node [
    id 380
    label "podporz&#261;dkowywanie_si&#281;"
  ]
  node [
    id 381
    label "conservation"
  ]
  node [
    id 382
    label "post&#281;powanie"
  ]
  node [
    id 383
    label "pami&#281;tanie"
  ]
  node [
    id 384
    label "przechowywanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 3
    target 4
  ]
]
