graph [
  node [
    id 0
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 1
    label "wej&#347;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 3
    label "raz"
    origin "text"
  ]
  node [
    id 4
    label "ent"
    origin "text"
  ]
  node [
    id 5
    label "moi"
    origin "text"
  ]
  node [
    id 6
    label "wsp&#243;&#322;lokator"
    origin "text"
  ]
  node [
    id 7
    label "mimo"
    origin "text"
  ]
  node [
    id 8
    label "uprzejmy"
    origin "text"
  ]
  node [
    id 9
    label "pro&#347;ba"
    origin "text"
  ]
  node [
    id 10
    label "nie"
    origin "text"
  ]
  node [
    id 11
    label "dok&#322;adnie"
  ]
  node [
    id 12
    label "punctiliously"
  ]
  node [
    id 13
    label "meticulously"
  ]
  node [
    id 14
    label "precyzyjnie"
  ]
  node [
    id 15
    label "dok&#322;adny"
  ]
  node [
    id 16
    label "rzetelnie"
  ]
  node [
    id 17
    label "adjustment"
  ]
  node [
    id 18
    label "panowanie"
  ]
  node [
    id 19
    label "przebywanie"
  ]
  node [
    id 20
    label "animation"
  ]
  node [
    id 21
    label "kwadrat"
  ]
  node [
    id 22
    label "stanie"
  ]
  node [
    id 23
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 24
    label "pomieszkanie"
  ]
  node [
    id 25
    label "lokal"
  ]
  node [
    id 26
    label "dom"
  ]
  node [
    id 27
    label "zajmowanie"
  ]
  node [
    id 28
    label "sprawowanie"
  ]
  node [
    id 29
    label "bycie"
  ]
  node [
    id 30
    label "kierowanie"
  ]
  node [
    id 31
    label "w&#322;adca"
  ]
  node [
    id 32
    label "dominowanie"
  ]
  node [
    id 33
    label "przewaga"
  ]
  node [
    id 34
    label "przewa&#380;anie"
  ]
  node [
    id 35
    label "znaczenie"
  ]
  node [
    id 36
    label "laterality"
  ]
  node [
    id 37
    label "control"
  ]
  node [
    id 38
    label "dominance"
  ]
  node [
    id 39
    label "kontrolowanie"
  ]
  node [
    id 40
    label "temper"
  ]
  node [
    id 41
    label "rule"
  ]
  node [
    id 42
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 43
    label "prym"
  ]
  node [
    id 44
    label "w&#322;adza"
  ]
  node [
    id 45
    label "ocieranie_si&#281;"
  ]
  node [
    id 46
    label "otoczenie_si&#281;"
  ]
  node [
    id 47
    label "posiedzenie"
  ]
  node [
    id 48
    label "otarcie_si&#281;"
  ]
  node [
    id 49
    label "atakowanie"
  ]
  node [
    id 50
    label "otaczanie_si&#281;"
  ]
  node [
    id 51
    label "wyj&#347;cie"
  ]
  node [
    id 52
    label "zmierzanie"
  ]
  node [
    id 53
    label "residency"
  ]
  node [
    id 54
    label "sojourn"
  ]
  node [
    id 55
    label "wychodzenie"
  ]
  node [
    id 56
    label "tkwienie"
  ]
  node [
    id 57
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 58
    label "powodowanie"
  ]
  node [
    id 59
    label "lokowanie_si&#281;"
  ]
  node [
    id 60
    label "schorzenie"
  ]
  node [
    id 61
    label "zajmowanie_si&#281;"
  ]
  node [
    id 62
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 63
    label "stosowanie"
  ]
  node [
    id 64
    label "anektowanie"
  ]
  node [
    id 65
    label "ciekawy"
  ]
  node [
    id 66
    label "zabieranie"
  ]
  node [
    id 67
    label "robienie"
  ]
  node [
    id 68
    label "sytuowanie_si&#281;"
  ]
  node [
    id 69
    label "wype&#322;nianie"
  ]
  node [
    id 70
    label "obejmowanie"
  ]
  node [
    id 71
    label "klasyfikacja"
  ]
  node [
    id 72
    label "czynno&#347;&#263;"
  ]
  node [
    id 73
    label "dzianie_si&#281;"
  ]
  node [
    id 74
    label "branie"
  ]
  node [
    id 75
    label "rz&#261;dzenie"
  ]
  node [
    id 76
    label "occupation"
  ]
  node [
    id 77
    label "zadawanie"
  ]
  node [
    id 78
    label "zaj&#281;ty"
  ]
  node [
    id 79
    label "miejsce"
  ]
  node [
    id 80
    label "gastronomia"
  ]
  node [
    id 81
    label "zak&#322;ad"
  ]
  node [
    id 82
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 83
    label "rodzina"
  ]
  node [
    id 84
    label "substancja_mieszkaniowa"
  ]
  node [
    id 85
    label "instytucja"
  ]
  node [
    id 86
    label "siedziba"
  ]
  node [
    id 87
    label "dom_rodzinny"
  ]
  node [
    id 88
    label "budynek"
  ]
  node [
    id 89
    label "grupa"
  ]
  node [
    id 90
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 91
    label "poj&#281;cie"
  ]
  node [
    id 92
    label "stead"
  ]
  node [
    id 93
    label "garderoba"
  ]
  node [
    id 94
    label "wiecha"
  ]
  node [
    id 95
    label "fratria"
  ]
  node [
    id 96
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 97
    label "trwanie"
  ]
  node [
    id 98
    label "ustanie"
  ]
  node [
    id 99
    label "wystanie"
  ]
  node [
    id 100
    label "postanie"
  ]
  node [
    id 101
    label "wystawanie"
  ]
  node [
    id 102
    label "kosztowanie"
  ]
  node [
    id 103
    label "przestanie"
  ]
  node [
    id 104
    label "pot&#281;ga"
  ]
  node [
    id 105
    label "wielok&#261;t_foremny"
  ]
  node [
    id 106
    label "stopie&#324;_pisma"
  ]
  node [
    id 107
    label "prostok&#261;t"
  ]
  node [
    id 108
    label "square"
  ]
  node [
    id 109
    label "romb"
  ]
  node [
    id 110
    label "justunek"
  ]
  node [
    id 111
    label "dzielnica"
  ]
  node [
    id 112
    label "poletko"
  ]
  node [
    id 113
    label "ekologia"
  ]
  node [
    id 114
    label "tango"
  ]
  node [
    id 115
    label "figura_taneczna"
  ]
  node [
    id 116
    label "time"
  ]
  node [
    id 117
    label "cios"
  ]
  node [
    id 118
    label "chwila"
  ]
  node [
    id 119
    label "uderzenie"
  ]
  node [
    id 120
    label "blok"
  ]
  node [
    id 121
    label "shot"
  ]
  node [
    id 122
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 123
    label "struktura_geologiczna"
  ]
  node [
    id 124
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 125
    label "pr&#243;ba"
  ]
  node [
    id 126
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 127
    label "coup"
  ]
  node [
    id 128
    label "siekacz"
  ]
  node [
    id 129
    label "instrumentalizacja"
  ]
  node [
    id 130
    label "trafienie"
  ]
  node [
    id 131
    label "walka"
  ]
  node [
    id 132
    label "zdarzenie_si&#281;"
  ]
  node [
    id 133
    label "wdarcie_si&#281;"
  ]
  node [
    id 134
    label "pogorszenie"
  ]
  node [
    id 135
    label "d&#378;wi&#281;k"
  ]
  node [
    id 136
    label "poczucie"
  ]
  node [
    id 137
    label "reakcja"
  ]
  node [
    id 138
    label "contact"
  ]
  node [
    id 139
    label "stukni&#281;cie"
  ]
  node [
    id 140
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 141
    label "bat"
  ]
  node [
    id 142
    label "spowodowanie"
  ]
  node [
    id 143
    label "rush"
  ]
  node [
    id 144
    label "odbicie"
  ]
  node [
    id 145
    label "dawka"
  ]
  node [
    id 146
    label "zadanie"
  ]
  node [
    id 147
    label "&#347;ci&#281;cie"
  ]
  node [
    id 148
    label "st&#322;uczenie"
  ]
  node [
    id 149
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 150
    label "odbicie_si&#281;"
  ]
  node [
    id 151
    label "dotkni&#281;cie"
  ]
  node [
    id 152
    label "charge"
  ]
  node [
    id 153
    label "dostanie"
  ]
  node [
    id 154
    label "skrytykowanie"
  ]
  node [
    id 155
    label "zagrywka"
  ]
  node [
    id 156
    label "manewr"
  ]
  node [
    id 157
    label "nast&#261;pienie"
  ]
  node [
    id 158
    label "uderzanie"
  ]
  node [
    id 159
    label "pogoda"
  ]
  node [
    id 160
    label "stroke"
  ]
  node [
    id 161
    label "pobicie"
  ]
  node [
    id 162
    label "ruch"
  ]
  node [
    id 163
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 164
    label "flap"
  ]
  node [
    id 165
    label "dotyk"
  ]
  node [
    id 166
    label "zrobienie"
  ]
  node [
    id 167
    label "czas"
  ]
  node [
    id 168
    label "wsp&#243;&#322;mieszkaniec"
  ]
  node [
    id 169
    label "mieszkaniec"
  ]
  node [
    id 170
    label "mi&#322;y"
  ]
  node [
    id 171
    label "sk&#322;onny"
  ]
  node [
    id 172
    label "grzeczny"
  ]
  node [
    id 173
    label "uprzejmie"
  ]
  node [
    id 174
    label "dobrze_wychowany"
  ]
  node [
    id 175
    label "grzecznie"
  ]
  node [
    id 176
    label "spokojny"
  ]
  node [
    id 177
    label "stosowny"
  ]
  node [
    id 178
    label "niewinny"
  ]
  node [
    id 179
    label "konserwatywny"
  ]
  node [
    id 180
    label "pos&#322;uszny"
  ]
  node [
    id 181
    label "przyjemny"
  ]
  node [
    id 182
    label "nijaki"
  ]
  node [
    id 183
    label "kochanek"
  ]
  node [
    id 184
    label "wybranek"
  ]
  node [
    id 185
    label "umi&#322;owany"
  ]
  node [
    id 186
    label "przyjemnie"
  ]
  node [
    id 187
    label "drogi"
  ]
  node [
    id 188
    label "mi&#322;o"
  ]
  node [
    id 189
    label "kochanie"
  ]
  node [
    id 190
    label "dyplomata"
  ]
  node [
    id 191
    label "dobry"
  ]
  node [
    id 192
    label "podatnie"
  ]
  node [
    id 193
    label "gotowy"
  ]
  node [
    id 194
    label "podda&#263;_si&#281;"
  ]
  node [
    id 195
    label "wypowied&#378;"
  ]
  node [
    id 196
    label "solicitation"
  ]
  node [
    id 197
    label "pos&#322;uchanie"
  ]
  node [
    id 198
    label "s&#261;d"
  ]
  node [
    id 199
    label "sparafrazowanie"
  ]
  node [
    id 200
    label "strawestowa&#263;"
  ]
  node [
    id 201
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 202
    label "trawestowa&#263;"
  ]
  node [
    id 203
    label "sparafrazowa&#263;"
  ]
  node [
    id 204
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 205
    label "sformu&#322;owanie"
  ]
  node [
    id 206
    label "parafrazowanie"
  ]
  node [
    id 207
    label "ozdobnik"
  ]
  node [
    id 208
    label "delimitacja"
  ]
  node [
    id 209
    label "parafrazowa&#263;"
  ]
  node [
    id 210
    label "stylizacja"
  ]
  node [
    id 211
    label "komunikat"
  ]
  node [
    id 212
    label "trawestowanie"
  ]
  node [
    id 213
    label "strawestowanie"
  ]
  node [
    id 214
    label "rezultat"
  ]
  node [
    id 215
    label "sprzeciw"
  ]
  node [
    id 216
    label "czerwona_kartka"
  ]
  node [
    id 217
    label "protestacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 137
  ]
]
