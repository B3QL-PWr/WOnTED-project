graph [
  node [
    id 0
    label "cze&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "drogi"
    origin "text"
  ]
  node [
    id 2
    label "mirka"
    origin "text"
  ]
  node [
    id 3
    label "honorowa&#263;"
  ]
  node [
    id 4
    label "uhonorowanie"
  ]
  node [
    id 5
    label "zaimponowanie"
  ]
  node [
    id 6
    label "honorowanie"
  ]
  node [
    id 7
    label "uszanowa&#263;"
  ]
  node [
    id 8
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 9
    label "uszanowanie"
  ]
  node [
    id 10
    label "szacuneczek"
  ]
  node [
    id 11
    label "rewerencja"
  ]
  node [
    id 12
    label "uhonorowa&#263;"
  ]
  node [
    id 13
    label "dobro"
  ]
  node [
    id 14
    label "szanowa&#263;"
  ]
  node [
    id 15
    label "respect"
  ]
  node [
    id 16
    label "postawa"
  ]
  node [
    id 17
    label "imponowanie"
  ]
  node [
    id 18
    label "warto&#347;&#263;"
  ]
  node [
    id 19
    label "u&#380;yteczno&#347;&#263;_ca&#322;kowita"
  ]
  node [
    id 20
    label "dobro&#263;"
  ]
  node [
    id 21
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 22
    label "krzywa_Engla"
  ]
  node [
    id 23
    label "cel"
  ]
  node [
    id 24
    label "dobra"
  ]
  node [
    id 25
    label "go&#322;&#261;bek"
  ]
  node [
    id 26
    label "despond"
  ]
  node [
    id 27
    label "litera"
  ]
  node [
    id 28
    label "kalokagatia"
  ]
  node [
    id 29
    label "rzecz"
  ]
  node [
    id 30
    label "g&#322;agolica"
  ]
  node [
    id 31
    label "ekstraspekcja"
  ]
  node [
    id 32
    label "feeling"
  ]
  node [
    id 33
    label "wiedza"
  ]
  node [
    id 34
    label "zemdle&#263;"
  ]
  node [
    id 35
    label "psychika"
  ]
  node [
    id 36
    label "stan"
  ]
  node [
    id 37
    label "Freud"
  ]
  node [
    id 38
    label "psychoanaliza"
  ]
  node [
    id 39
    label "conscience"
  ]
  node [
    id 40
    label "nastawienie"
  ]
  node [
    id 41
    label "pozycja"
  ]
  node [
    id 42
    label "attitude"
  ]
  node [
    id 43
    label "wypowied&#378;"
  ]
  node [
    id 44
    label "powa&#380;anie"
  ]
  node [
    id 45
    label "nagrodzi&#263;"
  ]
  node [
    id 46
    label "uczci&#263;"
  ]
  node [
    id 47
    label "wyp&#322;aci&#263;"
  ]
  node [
    id 48
    label "wzbudzanie"
  ]
  node [
    id 49
    label "szanowanie"
  ]
  node [
    id 50
    label "treasure"
  ]
  node [
    id 51
    label "czu&#263;"
  ]
  node [
    id 52
    label "respektowa&#263;"
  ]
  node [
    id 53
    label "wyra&#380;a&#263;"
  ]
  node [
    id 54
    label "chowa&#263;"
  ]
  node [
    id 55
    label "wyra&#380;enie"
  ]
  node [
    id 56
    label "wzbudzenie"
  ]
  node [
    id 57
    label "uznawanie"
  ]
  node [
    id 58
    label "p&#322;acenie"
  ]
  node [
    id 59
    label "honor"
  ]
  node [
    id 60
    label "okazywanie"
  ]
  node [
    id 61
    label "wyrazi&#263;"
  ]
  node [
    id 62
    label "spare_part"
  ]
  node [
    id 63
    label "czci&#263;"
  ]
  node [
    id 64
    label "acknowledge"
  ]
  node [
    id 65
    label "wyp&#322;aca&#263;"
  ]
  node [
    id 66
    label "notice"
  ]
  node [
    id 67
    label "fit"
  ]
  node [
    id 68
    label "uznawa&#263;"
  ]
  node [
    id 69
    label "nagrodzenie"
  ]
  node [
    id 70
    label "zap&#322;acenie"
  ]
  node [
    id 71
    label "drogo"
  ]
  node [
    id 72
    label "mi&#322;y"
  ]
  node [
    id 73
    label "cz&#322;owiek"
  ]
  node [
    id 74
    label "bliski"
  ]
  node [
    id 75
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 76
    label "przyjaciel"
  ]
  node [
    id 77
    label "warto&#347;ciowy"
  ]
  node [
    id 78
    label "blisko"
  ]
  node [
    id 79
    label "znajomy"
  ]
  node [
    id 80
    label "zwi&#261;zany"
  ]
  node [
    id 81
    label "przesz&#322;y"
  ]
  node [
    id 82
    label "silny"
  ]
  node [
    id 83
    label "zbli&#380;enie"
  ]
  node [
    id 84
    label "kr&#243;tki"
  ]
  node [
    id 85
    label "oddalony"
  ]
  node [
    id 86
    label "dok&#322;adny"
  ]
  node [
    id 87
    label "nieodleg&#322;y"
  ]
  node [
    id 88
    label "przysz&#322;y"
  ]
  node [
    id 89
    label "gotowy"
  ]
  node [
    id 90
    label "ma&#322;y"
  ]
  node [
    id 91
    label "ludzko&#347;&#263;"
  ]
  node [
    id 92
    label "asymilowanie"
  ]
  node [
    id 93
    label "wapniak"
  ]
  node [
    id 94
    label "asymilowa&#263;"
  ]
  node [
    id 95
    label "os&#322;abia&#263;"
  ]
  node [
    id 96
    label "posta&#263;"
  ]
  node [
    id 97
    label "hominid"
  ]
  node [
    id 98
    label "podw&#322;adny"
  ]
  node [
    id 99
    label "os&#322;abianie"
  ]
  node [
    id 100
    label "g&#322;owa"
  ]
  node [
    id 101
    label "figura"
  ]
  node [
    id 102
    label "portrecista"
  ]
  node [
    id 103
    label "dwun&#243;g"
  ]
  node [
    id 104
    label "profanum"
  ]
  node [
    id 105
    label "mikrokosmos"
  ]
  node [
    id 106
    label "nasada"
  ]
  node [
    id 107
    label "duch"
  ]
  node [
    id 108
    label "antropochoria"
  ]
  node [
    id 109
    label "osoba"
  ]
  node [
    id 110
    label "wz&#243;r"
  ]
  node [
    id 111
    label "senior"
  ]
  node [
    id 112
    label "oddzia&#322;ywanie"
  ]
  node [
    id 113
    label "Adam"
  ]
  node [
    id 114
    label "homo_sapiens"
  ]
  node [
    id 115
    label "polifag"
  ]
  node [
    id 116
    label "droga"
  ]
  node [
    id 117
    label "ukochanie"
  ]
  node [
    id 118
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 119
    label "feblik"
  ]
  node [
    id 120
    label "podnieci&#263;"
  ]
  node [
    id 121
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 122
    label "numer"
  ]
  node [
    id 123
    label "po&#380;ycie"
  ]
  node [
    id 124
    label "tendency"
  ]
  node [
    id 125
    label "podniecenie"
  ]
  node [
    id 126
    label "afekt"
  ]
  node [
    id 127
    label "zakochanie"
  ]
  node [
    id 128
    label "zajawka"
  ]
  node [
    id 129
    label "seks"
  ]
  node [
    id 130
    label "podniecanie"
  ]
  node [
    id 131
    label "imisja"
  ]
  node [
    id 132
    label "love"
  ]
  node [
    id 133
    label "rozmna&#380;anie"
  ]
  node [
    id 134
    label "ruch_frykcyjny"
  ]
  node [
    id 135
    label "na_pieska"
  ]
  node [
    id 136
    label "serce"
  ]
  node [
    id 137
    label "pozycja_misjonarska"
  ]
  node [
    id 138
    label "wi&#281;&#378;"
  ]
  node [
    id 139
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 140
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 141
    label "z&#322;&#261;czenie"
  ]
  node [
    id 142
    label "czynno&#347;&#263;"
  ]
  node [
    id 143
    label "gra_wst&#281;pna"
  ]
  node [
    id 144
    label "erotyka"
  ]
  node [
    id 145
    label "emocja"
  ]
  node [
    id 146
    label "baraszki"
  ]
  node [
    id 147
    label "po&#380;&#261;danie"
  ]
  node [
    id 148
    label "wzw&#243;d"
  ]
  node [
    id 149
    label "podnieca&#263;"
  ]
  node [
    id 150
    label "kochanek"
  ]
  node [
    id 151
    label "kum"
  ]
  node [
    id 152
    label "amikus"
  ]
  node [
    id 153
    label "zaprzyja&#378;nianie_si&#281;"
  ]
  node [
    id 154
    label "pobratymiec"
  ]
  node [
    id 155
    label "zaprzyja&#378;nienie_si&#281;"
  ]
  node [
    id 156
    label "sympatyk"
  ]
  node [
    id 157
    label "bratnia_dusza"
  ]
  node [
    id 158
    label "przyja&#378;nienie_si&#281;"
  ]
  node [
    id 159
    label "sk&#322;onny"
  ]
  node [
    id 160
    label "wybranek"
  ]
  node [
    id 161
    label "umi&#322;owany"
  ]
  node [
    id 162
    label "przyjemnie"
  ]
  node [
    id 163
    label "mi&#322;o"
  ]
  node [
    id 164
    label "kochanie"
  ]
  node [
    id 165
    label "dyplomata"
  ]
  node [
    id 166
    label "dobry"
  ]
  node [
    id 167
    label "dro&#380;ej"
  ]
  node [
    id 168
    label "p&#322;atnie"
  ]
  node [
    id 169
    label "rewaluowanie"
  ]
  node [
    id 170
    label "warto&#347;ciowo"
  ]
  node [
    id 171
    label "u&#380;yteczny"
  ]
  node [
    id 172
    label "zrewaluowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
]
