graph [
  node [
    id 0
    label "amerykanin"
    origin "text"
  ]
  node [
    id 1
    label "coraz"
    origin "text"
  ]
  node [
    id 2
    label "bardziej"
    origin "text"
  ]
  node [
    id 3
    label "u&#347;wiadamia&#263;"
    origin "text"
  ]
  node [
    id 4
    label "siebie"
    origin "text"
  ]
  node [
    id 5
    label "okienko"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "zamyka&#263;"
    origin "text"
  ]
  node [
    id 8
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 9
    label "informowa&#263;"
  ]
  node [
    id 10
    label "explain"
  ]
  node [
    id 11
    label "powiada&#263;"
  ]
  node [
    id 12
    label "komunikowa&#263;"
  ]
  node [
    id 13
    label "inform"
  ]
  node [
    id 14
    label "poja&#347;nia&#263;"
  ]
  node [
    id 15
    label "robi&#263;"
  ]
  node [
    id 16
    label "u&#322;atwia&#263;"
  ]
  node [
    id 17
    label "elaborate"
  ]
  node [
    id 18
    label "give"
  ]
  node [
    id 19
    label "suplikowa&#263;"
  ]
  node [
    id 20
    label "przek&#322;ada&#263;"
  ]
  node [
    id 21
    label "przekonywa&#263;"
  ]
  node [
    id 22
    label "interpretowa&#263;"
  ]
  node [
    id 23
    label "broni&#263;"
  ]
  node [
    id 24
    label "j&#281;zyk"
  ]
  node [
    id 25
    label "przedstawia&#263;"
  ]
  node [
    id 26
    label "sprawowa&#263;"
  ]
  node [
    id 27
    label "uzasadnia&#263;"
  ]
  node [
    id 28
    label "ekran"
  ]
  node [
    id 29
    label "interfejs"
  ]
  node [
    id 30
    label "okno"
  ]
  node [
    id 31
    label "urz&#261;d"
  ]
  node [
    id 32
    label "tabela"
  ]
  node [
    id 33
    label "poczta"
  ]
  node [
    id 34
    label "rubryka"
  ]
  node [
    id 35
    label "pozycja"
  ]
  node [
    id 36
    label "ram&#243;wka"
  ]
  node [
    id 37
    label "czas_wolny"
  ]
  node [
    id 38
    label "wype&#322;nianie"
  ]
  node [
    id 39
    label "program"
  ]
  node [
    id 40
    label "wype&#322;nienie"
  ]
  node [
    id 41
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 42
    label "pulpit"
  ]
  node [
    id 43
    label "menad&#380;er_okien"
  ]
  node [
    id 44
    label "otw&#243;r"
  ]
  node [
    id 45
    label "heading"
  ]
  node [
    id 46
    label "artyku&#322;"
  ]
  node [
    id 47
    label "dzia&#322;"
  ]
  node [
    id 48
    label "parapet"
  ]
  node [
    id 49
    label "szyba"
  ]
  node [
    id 50
    label "okiennica"
  ]
  node [
    id 51
    label "prze&#347;wit"
  ]
  node [
    id 52
    label "transenna"
  ]
  node [
    id 53
    label "kwatera_okienna"
  ]
  node [
    id 54
    label "inspekt"
  ]
  node [
    id 55
    label "nora"
  ]
  node [
    id 56
    label "futryna"
  ]
  node [
    id 57
    label "nadokiennik"
  ]
  node [
    id 58
    label "skrzyd&#322;o"
  ]
  node [
    id 59
    label "lufcik"
  ]
  node [
    id 60
    label "casement"
  ]
  node [
    id 61
    label "przestrze&#324;"
  ]
  node [
    id 62
    label "wyd&#322;uba&#263;"
  ]
  node [
    id 63
    label "wybicie"
  ]
  node [
    id 64
    label "wyd&#322;ubanie"
  ]
  node [
    id 65
    label "przerwa"
  ]
  node [
    id 66
    label "powybijanie"
  ]
  node [
    id 67
    label "wybijanie"
  ]
  node [
    id 68
    label "wiercenie"
  ]
  node [
    id 69
    label "po&#322;o&#380;enie"
  ]
  node [
    id 70
    label "debit"
  ]
  node [
    id 71
    label "druk"
  ]
  node [
    id 72
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 73
    label "szata_graficzna"
  ]
  node [
    id 74
    label "wydawa&#263;"
  ]
  node [
    id 75
    label "szermierka"
  ]
  node [
    id 76
    label "spis"
  ]
  node [
    id 77
    label "wyda&#263;"
  ]
  node [
    id 78
    label "ustawienie"
  ]
  node [
    id 79
    label "publikacja"
  ]
  node [
    id 80
    label "status"
  ]
  node [
    id 81
    label "miejsce"
  ]
  node [
    id 82
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 83
    label "adres"
  ]
  node [
    id 84
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 85
    label "rozmieszczenie"
  ]
  node [
    id 86
    label "sytuacja"
  ]
  node [
    id 87
    label "rz&#261;d"
  ]
  node [
    id 88
    label "redaktor"
  ]
  node [
    id 89
    label "awansowa&#263;"
  ]
  node [
    id 90
    label "wojsko"
  ]
  node [
    id 91
    label "bearing"
  ]
  node [
    id 92
    label "znaczenie"
  ]
  node [
    id 93
    label "awans"
  ]
  node [
    id 94
    label "awansowanie"
  ]
  node [
    id 95
    label "poster"
  ]
  node [
    id 96
    label "le&#380;e&#263;"
  ]
  node [
    id 97
    label "p&#322;aszczyzna"
  ]
  node [
    id 98
    label "naszywka"
  ]
  node [
    id 99
    label "kominek"
  ]
  node [
    id 100
    label "zas&#322;ona"
  ]
  node [
    id 101
    label "os&#322;ona"
  ]
  node [
    id 102
    label "urz&#261;dzenie"
  ]
  node [
    id 103
    label "blok"
  ]
  node [
    id 104
    label "scheduling"
  ]
  node [
    id 105
    label "plan"
  ]
  node [
    id 106
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 107
    label "skrytka_pocztowa"
  ]
  node [
    id 108
    label "zbi&#243;r"
  ]
  node [
    id 109
    label "list"
  ]
  node [
    id 110
    label "miejscownik"
  ]
  node [
    id 111
    label "instytucja"
  ]
  node [
    id 112
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 113
    label "mail"
  ]
  node [
    id 114
    label "plac&#243;wka"
  ]
  node [
    id 115
    label "poczta_elektroniczna"
  ]
  node [
    id 116
    label "szybkow&#243;z"
  ]
  node [
    id 117
    label "stanowisko"
  ]
  node [
    id 118
    label "position"
  ]
  node [
    id 119
    label "siedziba"
  ]
  node [
    id 120
    label "organ"
  ]
  node [
    id 121
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 122
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 123
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 124
    label "mianowaniec"
  ]
  node [
    id 125
    label "w&#322;adza"
  ]
  node [
    id 126
    label "rozmiar&#243;wka"
  ]
  node [
    id 127
    label "chart"
  ]
  node [
    id 128
    label "klasyfikacja"
  ]
  node [
    id 129
    label "szachownica_Punnetta"
  ]
  node [
    id 130
    label "instalowa&#263;"
  ]
  node [
    id 131
    label "oprogramowanie"
  ]
  node [
    id 132
    label "odinstalowywa&#263;"
  ]
  node [
    id 133
    label "zaprezentowanie"
  ]
  node [
    id 134
    label "podprogram"
  ]
  node [
    id 135
    label "ogranicznik_referencyjny"
  ]
  node [
    id 136
    label "course_of_study"
  ]
  node [
    id 137
    label "booklet"
  ]
  node [
    id 138
    label "odinstalowanie"
  ]
  node [
    id 139
    label "broszura"
  ]
  node [
    id 140
    label "wytw&#243;r"
  ]
  node [
    id 141
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 142
    label "kana&#322;"
  ]
  node [
    id 143
    label "teleferie"
  ]
  node [
    id 144
    label "zainstalowanie"
  ]
  node [
    id 145
    label "struktura_organizacyjna"
  ]
  node [
    id 146
    label "pirat"
  ]
  node [
    id 147
    label "zaprezentowa&#263;"
  ]
  node [
    id 148
    label "prezentowanie"
  ]
  node [
    id 149
    label "prezentowa&#263;"
  ]
  node [
    id 150
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 151
    label "punkt"
  ]
  node [
    id 152
    label "folder"
  ]
  node [
    id 153
    label "zainstalowa&#263;"
  ]
  node [
    id 154
    label "za&#322;o&#380;enie"
  ]
  node [
    id 155
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 156
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 157
    label "tryb"
  ]
  node [
    id 158
    label "emitowa&#263;"
  ]
  node [
    id 159
    label "emitowanie"
  ]
  node [
    id 160
    label "odinstalowywanie"
  ]
  node [
    id 161
    label "instrukcja"
  ]
  node [
    id 162
    label "informatyka"
  ]
  node [
    id 163
    label "deklaracja"
  ]
  node [
    id 164
    label "sekcja_krytyczna"
  ]
  node [
    id 165
    label "menu"
  ]
  node [
    id 166
    label "furkacja"
  ]
  node [
    id 167
    label "podstawa"
  ]
  node [
    id 168
    label "instalowanie"
  ]
  node [
    id 169
    label "oferta"
  ]
  node [
    id 170
    label "odinstalowa&#263;"
  ]
  node [
    id 171
    label "blat"
  ]
  node [
    id 172
    label "obszar"
  ]
  node [
    id 173
    label "ikona"
  ]
  node [
    id 174
    label "system_operacyjny"
  ]
  node [
    id 175
    label "mebel"
  ]
  node [
    id 176
    label "umieszczanie"
  ]
  node [
    id 177
    label "powodowanie"
  ]
  node [
    id 178
    label "filling"
  ]
  node [
    id 179
    label "fugowanie"
  ]
  node [
    id 180
    label "nadp&#322;ywanie"
  ]
  node [
    id 181
    label "przepe&#322;nianie"
  ]
  node [
    id 182
    label "robienie"
  ]
  node [
    id 183
    label "rojenie_si&#281;"
  ]
  node [
    id 184
    label "urzeczywistnianie"
  ]
  node [
    id 185
    label "realization"
  ]
  node [
    id 186
    label "czucie"
  ]
  node [
    id 187
    label "rise"
  ]
  node [
    id 188
    label "nadp&#322;yni&#281;cie"
  ]
  node [
    id 189
    label "czynno&#347;&#263;"
  ]
  node [
    id 190
    label "dzianie_si&#281;"
  ]
  node [
    id 191
    label "bycie"
  ]
  node [
    id 192
    label "uzupe&#322;nianie"
  ]
  node [
    id 193
    label "occupation"
  ]
  node [
    id 194
    label "t&#281;&#380;enie"
  ]
  node [
    id 195
    label "przepe&#322;nienie"
  ]
  node [
    id 196
    label "uzupe&#322;nienie"
  ]
  node [
    id 197
    label "woof"
  ]
  node [
    id 198
    label "activity"
  ]
  node [
    id 199
    label "spowodowanie"
  ]
  node [
    id 200
    label "control"
  ]
  node [
    id 201
    label "pe&#322;ny"
  ]
  node [
    id 202
    label "zdarzenie_si&#281;"
  ]
  node [
    id 203
    label "znalezienie_si&#281;"
  ]
  node [
    id 204
    label "element"
  ]
  node [
    id 205
    label "completion"
  ]
  node [
    id 206
    label "bash"
  ]
  node [
    id 207
    label "umieszczenie"
  ]
  node [
    id 208
    label "ziszczenie_si&#281;"
  ]
  node [
    id 209
    label "nasilenie_si&#281;"
  ]
  node [
    id 210
    label "poczucie"
  ]
  node [
    id 211
    label "performance"
  ]
  node [
    id 212
    label "zrobienie"
  ]
  node [
    id 213
    label "zawiera&#263;"
  ]
  node [
    id 214
    label "suspend"
  ]
  node [
    id 215
    label "ujmowa&#263;"
  ]
  node [
    id 216
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 217
    label "unieruchamia&#263;"
  ]
  node [
    id 218
    label "sk&#322;ada&#263;"
  ]
  node [
    id 219
    label "ko&#324;czy&#263;"
  ]
  node [
    id 220
    label "blokowa&#263;"
  ]
  node [
    id 221
    label "lock"
  ]
  node [
    id 222
    label "umieszcza&#263;"
  ]
  node [
    id 223
    label "ukrywa&#263;"
  ]
  node [
    id 224
    label "exsert"
  ]
  node [
    id 225
    label "poznawa&#263;"
  ]
  node [
    id 226
    label "fold"
  ]
  node [
    id 227
    label "obejmowa&#263;"
  ]
  node [
    id 228
    label "mie&#263;"
  ]
  node [
    id 229
    label "make"
  ]
  node [
    id 230
    label "ustala&#263;"
  ]
  node [
    id 231
    label "zabiera&#263;"
  ]
  node [
    id 232
    label "zgarnia&#263;"
  ]
  node [
    id 233
    label "chwyta&#263;"
  ]
  node [
    id 234
    label "&#322;apa&#263;"
  ]
  node [
    id 235
    label "reduce"
  ]
  node [
    id 236
    label "allude"
  ]
  node [
    id 237
    label "wzbudza&#263;"
  ]
  node [
    id 238
    label "convey"
  ]
  node [
    id 239
    label "raise"
  ]
  node [
    id 240
    label "throng"
  ]
  node [
    id 241
    label "powodowa&#263;"
  ]
  node [
    id 242
    label "wstrzymywa&#263;"
  ]
  node [
    id 243
    label "przeszkadza&#263;"
  ]
  node [
    id 244
    label "zajmowa&#263;"
  ]
  node [
    id 245
    label "kiblowa&#263;"
  ]
  node [
    id 246
    label "walczy&#263;"
  ]
  node [
    id 247
    label "zablokowywa&#263;"
  ]
  node [
    id 248
    label "zatrzymywa&#263;"
  ]
  node [
    id 249
    label "interlock"
  ]
  node [
    id 250
    label "parry"
  ]
  node [
    id 251
    label "suppress"
  ]
  node [
    id 252
    label "report"
  ]
  node [
    id 253
    label "meliniarz"
  ]
  node [
    id 254
    label "cache"
  ]
  node [
    id 255
    label "zachowywa&#263;"
  ]
  node [
    id 256
    label "plasowa&#263;"
  ]
  node [
    id 257
    label "umie&#347;ci&#263;"
  ]
  node [
    id 258
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 259
    label "pomieszcza&#263;"
  ]
  node [
    id 260
    label "accommodate"
  ]
  node [
    id 261
    label "zmienia&#263;"
  ]
  node [
    id 262
    label "venture"
  ]
  node [
    id 263
    label "wpiernicza&#263;"
  ]
  node [
    id 264
    label "okre&#347;la&#263;"
  ]
  node [
    id 265
    label "przekazywa&#263;"
  ]
  node [
    id 266
    label "zbiera&#263;"
  ]
  node [
    id 267
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 268
    label "przywraca&#263;"
  ]
  node [
    id 269
    label "dawa&#263;"
  ]
  node [
    id 270
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 271
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 272
    label "publicize"
  ]
  node [
    id 273
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 274
    label "render"
  ]
  node [
    id 275
    label "uk&#322;ada&#263;"
  ]
  node [
    id 276
    label "opracowywa&#263;"
  ]
  node [
    id 277
    label "set"
  ]
  node [
    id 278
    label "oddawa&#263;"
  ]
  node [
    id 279
    label "train"
  ]
  node [
    id 280
    label "dzieli&#263;"
  ]
  node [
    id 281
    label "scala&#263;"
  ]
  node [
    id 282
    label "zestaw"
  ]
  node [
    id 283
    label "usuwa&#263;"
  ]
  node [
    id 284
    label "odkrywa&#263;"
  ]
  node [
    id 285
    label "urzeczywistnia&#263;"
  ]
  node [
    id 286
    label "undo"
  ]
  node [
    id 287
    label "przestawa&#263;"
  ]
  node [
    id 288
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 289
    label "cope"
  ]
  node [
    id 290
    label "satisfy"
  ]
  node [
    id 291
    label "close"
  ]
  node [
    id 292
    label "determine"
  ]
  node [
    id 293
    label "zako&#324;cza&#263;"
  ]
  node [
    id 294
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 295
    label "stanowi&#263;"
  ]
  node [
    id 296
    label "osoba_prawna"
  ]
  node [
    id 297
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 298
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 299
    label "poj&#281;cie"
  ]
  node [
    id 300
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 301
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 302
    label "biuro"
  ]
  node [
    id 303
    label "organizacja"
  ]
  node [
    id 304
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 305
    label "Fundusze_Unijne"
  ]
  node [
    id 306
    label "establishment"
  ]
  node [
    id 307
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 308
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 309
    label "afiliowa&#263;"
  ]
  node [
    id 310
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 311
    label "standard"
  ]
  node [
    id 312
    label "zamykanie"
  ]
  node [
    id 313
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 314
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 315
    label "bogdan"
  ]
  node [
    id 316
    label "g&#243;ralczyk"
  ]
  node [
    id 317
    label "Grzegorz"
  ]
  node [
    id 318
    label "Sroczy&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 315
    target 316
  ]
  edge [
    source 317
    target 318
  ]
]
