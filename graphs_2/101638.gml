graph [
  node [
    id 0
    label "przechodzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "g&#322;osowanie"
    origin "text"
  ]
  node [
    id 2
    label "nad"
    origin "text"
  ]
  node [
    id 3
    label "ca&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "projekt"
    origin "text"
  ]
  node [
    id 5
    label "apel"
    origin "text"
  ]
  node [
    id 6
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 7
    label "mie&#263;_miejsce"
  ]
  node [
    id 8
    label "move"
  ]
  node [
    id 9
    label "zaczyna&#263;"
  ]
  node [
    id 10
    label "przebywa&#263;"
  ]
  node [
    id 11
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 12
    label "conflict"
  ]
  node [
    id 13
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 14
    label "mija&#263;"
  ]
  node [
    id 15
    label "proceed"
  ]
  node [
    id 16
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 17
    label "go"
  ]
  node [
    id 18
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 19
    label "saturate"
  ]
  node [
    id 20
    label "i&#347;&#263;"
  ]
  node [
    id 21
    label "doznawa&#263;"
  ]
  node [
    id 22
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 23
    label "przestawa&#263;"
  ]
  node [
    id 24
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 25
    label "pass"
  ]
  node [
    id 26
    label "zalicza&#263;"
  ]
  node [
    id 27
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 28
    label "zmienia&#263;"
  ]
  node [
    id 29
    label "test"
  ]
  node [
    id 30
    label "podlega&#263;"
  ]
  node [
    id 31
    label "przerabia&#263;"
  ]
  node [
    id 32
    label "continue"
  ]
  node [
    id 33
    label "traci&#263;"
  ]
  node [
    id 34
    label "alternate"
  ]
  node [
    id 35
    label "change"
  ]
  node [
    id 36
    label "reengineering"
  ]
  node [
    id 37
    label "zast&#281;powa&#263;"
  ]
  node [
    id 38
    label "sprawia&#263;"
  ]
  node [
    id 39
    label "zyskiwa&#263;"
  ]
  node [
    id 40
    label "&#380;y&#263;"
  ]
  node [
    id 41
    label "coating"
  ]
  node [
    id 42
    label "determine"
  ]
  node [
    id 43
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 44
    label "ko&#324;czy&#263;"
  ]
  node [
    id 45
    label "finish_up"
  ]
  node [
    id 46
    label "lecie&#263;"
  ]
  node [
    id 47
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 48
    label "bangla&#263;"
  ]
  node [
    id 49
    label "trace"
  ]
  node [
    id 50
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 51
    label "impart"
  ]
  node [
    id 52
    label "by&#263;"
  ]
  node [
    id 53
    label "try"
  ]
  node [
    id 54
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 55
    label "boost"
  ]
  node [
    id 56
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 57
    label "dziama&#263;"
  ]
  node [
    id 58
    label "blend"
  ]
  node [
    id 59
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 60
    label "draw"
  ]
  node [
    id 61
    label "wyrusza&#263;"
  ]
  node [
    id 62
    label "bie&#380;e&#263;"
  ]
  node [
    id 63
    label "dzia&#263;_si&#281;"
  ]
  node [
    id 64
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 65
    label "tryb"
  ]
  node [
    id 66
    label "czas"
  ]
  node [
    id 67
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 68
    label "atakowa&#263;"
  ]
  node [
    id 69
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 70
    label "describe"
  ]
  node [
    id 71
    label "post&#281;powa&#263;"
  ]
  node [
    id 72
    label "tkwi&#263;"
  ]
  node [
    id 73
    label "istnie&#263;"
  ]
  node [
    id 74
    label "pause"
  ]
  node [
    id 75
    label "hesitate"
  ]
  node [
    id 76
    label "trwa&#263;"
  ]
  node [
    id 77
    label "base_on_balls"
  ]
  node [
    id 78
    label "omija&#263;"
  ]
  node [
    id 79
    label "czu&#263;"
  ]
  node [
    id 80
    label "zale&#380;e&#263;"
  ]
  node [
    id 81
    label "hurt"
  ]
  node [
    id 82
    label "wywi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 83
    label "bra&#263;"
  ]
  node [
    id 84
    label "mark"
  ]
  node [
    id 85
    label "number"
  ]
  node [
    id 86
    label "stwierdza&#263;"
  ]
  node [
    id 87
    label "odb&#281;bnia&#263;"
  ]
  node [
    id 88
    label "wlicza&#263;"
  ]
  node [
    id 89
    label "przyporz&#261;dkowywa&#263;"
  ]
  node [
    id 90
    label "odejmowa&#263;"
  ]
  node [
    id 91
    label "bankrupt"
  ]
  node [
    id 92
    label "open"
  ]
  node [
    id 93
    label "set_about"
  ]
  node [
    id 94
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 95
    label "begin"
  ]
  node [
    id 96
    label "goban"
  ]
  node [
    id 97
    label "gra_planszowa"
  ]
  node [
    id 98
    label "sport_umys&#322;owy"
  ]
  node [
    id 99
    label "chi&#324;ski"
  ]
  node [
    id 100
    label "cover"
  ]
  node [
    id 101
    label "robi&#263;"
  ]
  node [
    id 102
    label "wytwarza&#263;"
  ]
  node [
    id 103
    label "amend"
  ]
  node [
    id 104
    label "overwork"
  ]
  node [
    id 105
    label "convert"
  ]
  node [
    id 106
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 107
    label "zamienia&#263;"
  ]
  node [
    id 108
    label "modyfikowa&#263;"
  ]
  node [
    id 109
    label "radzi&#263;_sobie"
  ]
  node [
    id 110
    label "pracowa&#263;"
  ]
  node [
    id 111
    label "przetwarza&#263;"
  ]
  node [
    id 112
    label "sp&#281;dza&#263;"
  ]
  node [
    id 113
    label "badanie"
  ]
  node [
    id 114
    label "do&#347;wiadczenie"
  ]
  node [
    id 115
    label "narz&#281;dzie"
  ]
  node [
    id 116
    label "przechodzenie"
  ]
  node [
    id 117
    label "quiz"
  ]
  node [
    id 118
    label "sprawdzian"
  ]
  node [
    id 119
    label "arkusz"
  ]
  node [
    id 120
    label "sytuacja"
  ]
  node [
    id 121
    label "reasumowa&#263;"
  ]
  node [
    id 122
    label "przeg&#322;osowanie"
  ]
  node [
    id 123
    label "powodowanie"
  ]
  node [
    id 124
    label "reasumowanie"
  ]
  node [
    id 125
    label "akcja"
  ]
  node [
    id 126
    label "wybieranie"
  ]
  node [
    id 127
    label "poll"
  ]
  node [
    id 128
    label "vote"
  ]
  node [
    id 129
    label "decydowanie"
  ]
  node [
    id 130
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 131
    label "przeg&#322;osowywanie"
  ]
  node [
    id 132
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 133
    label "wi&#281;kszo&#347;&#263;_bezwzgl&#281;dna"
  ]
  node [
    id 134
    label "wybranie"
  ]
  node [
    id 135
    label "dywidenda"
  ]
  node [
    id 136
    label "przebieg"
  ]
  node [
    id 137
    label "operacja"
  ]
  node [
    id 138
    label "zagrywka"
  ]
  node [
    id 139
    label "wydarzenie"
  ]
  node [
    id 140
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 141
    label "udzia&#322;"
  ]
  node [
    id 142
    label "commotion"
  ]
  node [
    id 143
    label "occupation"
  ]
  node [
    id 144
    label "gra"
  ]
  node [
    id 145
    label "jazda"
  ]
  node [
    id 146
    label "czyn"
  ]
  node [
    id 147
    label "stock"
  ]
  node [
    id 148
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 149
    label "w&#281;ze&#322;"
  ]
  node [
    id 150
    label "wysoko&#347;&#263;"
  ]
  node [
    id 151
    label "czynno&#347;&#263;"
  ]
  node [
    id 152
    label "instrument_strunowy"
  ]
  node [
    id 153
    label "cause"
  ]
  node [
    id 154
    label "causal_agent"
  ]
  node [
    id 155
    label "zarz&#261;dzanie"
  ]
  node [
    id 156
    label "rz&#261;dzenie"
  ]
  node [
    id 157
    label "robienie"
  ]
  node [
    id 158
    label "gospodarowanie"
  ]
  node [
    id 159
    label "oddzia&#322;ywanie"
  ]
  node [
    id 160
    label "podejmowanie"
  ]
  node [
    id 161
    label "rozstrzyganie_si&#281;"
  ]
  node [
    id 162
    label "liquidation"
  ]
  node [
    id 163
    label "zu&#380;ycie"
  ]
  node [
    id 164
    label "powo&#322;anie"
  ]
  node [
    id 165
    label "powybieranie"
  ]
  node [
    id 166
    label "ustalenie"
  ]
  node [
    id 167
    label "sie&#263;_rybacka"
  ]
  node [
    id 168
    label "wyj&#281;cie"
  ]
  node [
    id 169
    label "optowanie"
  ]
  node [
    id 170
    label "kotwica"
  ]
  node [
    id 171
    label "wygrywanie"
  ]
  node [
    id 172
    label "powo&#322;ywanie"
  ]
  node [
    id 173
    label "election"
  ]
  node [
    id 174
    label "wyiskanie"
  ]
  node [
    id 175
    label "wyjmowanie"
  ]
  node [
    id 176
    label "iskanie"
  ]
  node [
    id 177
    label "chosen"
  ]
  node [
    id 178
    label "zu&#380;ywanie"
  ]
  node [
    id 179
    label "okre&#347;lanie"
  ]
  node [
    id 180
    label "uchwalenie"
  ]
  node [
    id 181
    label "przewa&#380;enie"
  ]
  node [
    id 182
    label "podsumowywanie"
  ]
  node [
    id 183
    label "streszczenie"
  ]
  node [
    id 184
    label "powtarzanie"
  ]
  node [
    id 185
    label "summarization"
  ]
  node [
    id 186
    label "podsumowanie"
  ]
  node [
    id 187
    label "powt&#243;rzenie"
  ]
  node [
    id 188
    label "streszczanie"
  ]
  node [
    id 189
    label "powtarza&#263;"
  ]
  node [
    id 190
    label "powt&#243;rzy&#263;"
  ]
  node [
    id 191
    label "podsumowa&#263;"
  ]
  node [
    id 192
    label "sum_up"
  ]
  node [
    id 193
    label "reprise"
  ]
  node [
    id 194
    label "stre&#347;ci&#263;"
  ]
  node [
    id 195
    label "podsumowywa&#263;"
  ]
  node [
    id 196
    label "streszcza&#263;"
  ]
  node [
    id 197
    label "integer"
  ]
  node [
    id 198
    label "liczba"
  ]
  node [
    id 199
    label "zlewanie_si&#281;"
  ]
  node [
    id 200
    label "ilo&#347;&#263;"
  ]
  node [
    id 201
    label "uk&#322;ad"
  ]
  node [
    id 202
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 203
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 204
    label "pe&#322;ny"
  ]
  node [
    id 205
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 206
    label "kategoria"
  ]
  node [
    id 207
    label "pierwiastek"
  ]
  node [
    id 208
    label "rozmiar"
  ]
  node [
    id 209
    label "poj&#281;cie"
  ]
  node [
    id 210
    label "cecha"
  ]
  node [
    id 211
    label "kategoria_gramatyczna"
  ]
  node [
    id 212
    label "grupa"
  ]
  node [
    id 213
    label "kwadrat_magiczny"
  ]
  node [
    id 214
    label "wyra&#380;enie"
  ]
  node [
    id 215
    label "koniugacja"
  ]
  node [
    id 216
    label "rozprz&#261;c"
  ]
  node [
    id 217
    label "treaty"
  ]
  node [
    id 218
    label "systemat"
  ]
  node [
    id 219
    label "system"
  ]
  node [
    id 220
    label "umowa"
  ]
  node [
    id 221
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 222
    label "struktura"
  ]
  node [
    id 223
    label "usenet"
  ]
  node [
    id 224
    label "przestawi&#263;"
  ]
  node [
    id 225
    label "zbi&#243;r"
  ]
  node [
    id 226
    label "alliance"
  ]
  node [
    id 227
    label "ONZ"
  ]
  node [
    id 228
    label "NATO"
  ]
  node [
    id 229
    label "konstelacja"
  ]
  node [
    id 230
    label "o&#347;"
  ]
  node [
    id 231
    label "podsystem"
  ]
  node [
    id 232
    label "zawarcie"
  ]
  node [
    id 233
    label "zawrze&#263;"
  ]
  node [
    id 234
    label "organ"
  ]
  node [
    id 235
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 236
    label "wi&#281;&#378;"
  ]
  node [
    id 237
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 238
    label "zachowanie"
  ]
  node [
    id 239
    label "cybernetyk"
  ]
  node [
    id 240
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 241
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 242
    label "sk&#322;ad"
  ]
  node [
    id 243
    label "traktat_wersalski"
  ]
  node [
    id 244
    label "cia&#322;o"
  ]
  node [
    id 245
    label "toni&#281;cie"
  ]
  node [
    id 246
    label "zatoni&#281;cie"
  ]
  node [
    id 247
    label "part"
  ]
  node [
    id 248
    label "nieograniczony"
  ]
  node [
    id 249
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 250
    label "satysfakcja"
  ]
  node [
    id 251
    label "bezwzgl&#281;dny"
  ]
  node [
    id 252
    label "ca&#322;y"
  ]
  node [
    id 253
    label "otwarty"
  ]
  node [
    id 254
    label "wype&#322;nienie"
  ]
  node [
    id 255
    label "kompletny"
  ]
  node [
    id 256
    label "pe&#322;no"
  ]
  node [
    id 257
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 258
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 259
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 260
    label "zupe&#322;ny"
  ]
  node [
    id 261
    label "r&#243;wny"
  ]
  node [
    id 262
    label "intencja"
  ]
  node [
    id 263
    label "plan"
  ]
  node [
    id 264
    label "device"
  ]
  node [
    id 265
    label "program_u&#380;ytkowy"
  ]
  node [
    id 266
    label "pomys&#322;"
  ]
  node [
    id 267
    label "dokumentacja"
  ]
  node [
    id 268
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 269
    label "agreement"
  ]
  node [
    id 270
    label "dokument"
  ]
  node [
    id 271
    label "wytw&#243;r"
  ]
  node [
    id 272
    label "thinking"
  ]
  node [
    id 273
    label "model"
  ]
  node [
    id 274
    label "punkt"
  ]
  node [
    id 275
    label "rysunek"
  ]
  node [
    id 276
    label "miejsce_pracy"
  ]
  node [
    id 277
    label "przestrze&#324;"
  ]
  node [
    id 278
    label "obraz"
  ]
  node [
    id 279
    label "reprezentacja"
  ]
  node [
    id 280
    label "dekoracja"
  ]
  node [
    id 281
    label "perspektywa"
  ]
  node [
    id 282
    label "ekscerpcja"
  ]
  node [
    id 283
    label "materia&#322;"
  ]
  node [
    id 284
    label "operat"
  ]
  node [
    id 285
    label "kosztorys"
  ]
  node [
    id 286
    label "zapis"
  ]
  node [
    id 287
    label "&#347;wiadectwo"
  ]
  node [
    id 288
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 289
    label "parafa"
  ]
  node [
    id 290
    label "plik"
  ]
  node [
    id 291
    label "raport&#243;wka"
  ]
  node [
    id 292
    label "utw&#243;r"
  ]
  node [
    id 293
    label "record"
  ]
  node [
    id 294
    label "fascyku&#322;"
  ]
  node [
    id 295
    label "registratura"
  ]
  node [
    id 296
    label "artyku&#322;"
  ]
  node [
    id 297
    label "writing"
  ]
  node [
    id 298
    label "sygnatariusz"
  ]
  node [
    id 299
    label "pocz&#261;tki"
  ]
  node [
    id 300
    label "ukra&#347;&#263;"
  ]
  node [
    id 301
    label "ukradzenie"
  ]
  node [
    id 302
    label "idea"
  ]
  node [
    id 303
    label "pro&#347;ba"
  ]
  node [
    id 304
    label "znak"
  ]
  node [
    id 305
    label "zdyscyplinowanie"
  ]
  node [
    id 306
    label "recoil"
  ]
  node [
    id 307
    label "spotkanie"
  ]
  node [
    id 308
    label "uderzenie"
  ]
  node [
    id 309
    label "pies_my&#347;liwski"
  ]
  node [
    id 310
    label "bid"
  ]
  node [
    id 311
    label "sygna&#322;"
  ]
  node [
    id 312
    label "szermierka"
  ]
  node [
    id 313
    label "zbi&#243;rka"
  ]
  node [
    id 314
    label "doznanie"
  ]
  node [
    id 315
    label "gathering"
  ]
  node [
    id 316
    label "znajomy"
  ]
  node [
    id 317
    label "powitanie"
  ]
  node [
    id 318
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 319
    label "spowodowanie"
  ]
  node [
    id 320
    label "zdarzenie_si&#281;"
  ]
  node [
    id 321
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 322
    label "znalezienie"
  ]
  node [
    id 323
    label "match"
  ]
  node [
    id 324
    label "employment"
  ]
  node [
    id 325
    label "po&#380;egnanie"
  ]
  node [
    id 326
    label "gather"
  ]
  node [
    id 327
    label "spotykanie"
  ]
  node [
    id 328
    label "spotkanie_si&#281;"
  ]
  node [
    id 329
    label "instrumentalizacja"
  ]
  node [
    id 330
    label "trafienie"
  ]
  node [
    id 331
    label "walka"
  ]
  node [
    id 332
    label "cios"
  ]
  node [
    id 333
    label "wdarcie_si&#281;"
  ]
  node [
    id 334
    label "pogorszenie"
  ]
  node [
    id 335
    label "d&#378;wi&#281;k"
  ]
  node [
    id 336
    label "poczucie"
  ]
  node [
    id 337
    label "coup"
  ]
  node [
    id 338
    label "reakcja"
  ]
  node [
    id 339
    label "contact"
  ]
  node [
    id 340
    label "stukni&#281;cie"
  ]
  node [
    id 341
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 342
    label "bat"
  ]
  node [
    id 343
    label "rush"
  ]
  node [
    id 344
    label "odbicie"
  ]
  node [
    id 345
    label "dawka"
  ]
  node [
    id 346
    label "zadanie"
  ]
  node [
    id 347
    label "&#347;ci&#281;cie"
  ]
  node [
    id 348
    label "st&#322;uczenie"
  ]
  node [
    id 349
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 350
    label "time"
  ]
  node [
    id 351
    label "odbicie_si&#281;"
  ]
  node [
    id 352
    label "dotkni&#281;cie"
  ]
  node [
    id 353
    label "charge"
  ]
  node [
    id 354
    label "dostanie"
  ]
  node [
    id 355
    label "skrytykowanie"
  ]
  node [
    id 356
    label "manewr"
  ]
  node [
    id 357
    label "nast&#261;pienie"
  ]
  node [
    id 358
    label "uderzanie"
  ]
  node [
    id 359
    label "pogoda"
  ]
  node [
    id 360
    label "stroke"
  ]
  node [
    id 361
    label "pobicie"
  ]
  node [
    id 362
    label "ruch"
  ]
  node [
    id 363
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 364
    label "flap"
  ]
  node [
    id 365
    label "dotyk"
  ]
  node [
    id 366
    label "zrobienie"
  ]
  node [
    id 367
    label "podporz&#261;dkowanie"
  ]
  node [
    id 368
    label "porz&#261;dek"
  ]
  node [
    id 369
    label "zachowywanie"
  ]
  node [
    id 370
    label "mores"
  ]
  node [
    id 371
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 372
    label "zachowa&#263;"
  ]
  node [
    id 373
    label "zachowywa&#263;"
  ]
  node [
    id 374
    label "nauczenie"
  ]
  node [
    id 375
    label "wypowied&#378;"
  ]
  node [
    id 376
    label "solicitation"
  ]
  node [
    id 377
    label "dow&#243;d"
  ]
  node [
    id 378
    label "oznakowanie"
  ]
  node [
    id 379
    label "fakt"
  ]
  node [
    id 380
    label "stawia&#263;"
  ]
  node [
    id 381
    label "point"
  ]
  node [
    id 382
    label "kodzik"
  ]
  node [
    id 383
    label "postawi&#263;"
  ]
  node [
    id 384
    label "herb"
  ]
  node [
    id 385
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 386
    label "attribute"
  ]
  node [
    id 387
    label "implikowa&#263;"
  ]
  node [
    id 388
    label "przekazywa&#263;"
  ]
  node [
    id 389
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 390
    label "pulsation"
  ]
  node [
    id 391
    label "przekazywanie"
  ]
  node [
    id 392
    label "przewodzenie"
  ]
  node [
    id 393
    label "po&#322;&#261;czenie"
  ]
  node [
    id 394
    label "fala"
  ]
  node [
    id 395
    label "doj&#347;cie"
  ]
  node [
    id 396
    label "przekazanie"
  ]
  node [
    id 397
    label "przewodzi&#263;"
  ]
  node [
    id 398
    label "zapowied&#378;"
  ]
  node [
    id 399
    label "medium_transmisyjne"
  ]
  node [
    id 400
    label "demodulacja"
  ]
  node [
    id 401
    label "doj&#347;&#263;"
  ]
  node [
    id 402
    label "przekaza&#263;"
  ]
  node [
    id 403
    label "czynnik"
  ]
  node [
    id 404
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 405
    label "aliasing"
  ]
  node [
    id 406
    label "wizja"
  ]
  node [
    id 407
    label "modulacja"
  ]
  node [
    id 408
    label "drift"
  ]
  node [
    id 409
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 410
    label "plansza"
  ]
  node [
    id 411
    label "wi&#261;zanie"
  ]
  node [
    id 412
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 413
    label "ripostowa&#263;"
  ]
  node [
    id 414
    label "czerwona_kartka"
  ]
  node [
    id 415
    label "czarna_kartka"
  ]
  node [
    id 416
    label "pozycja"
  ]
  node [
    id 417
    label "fight"
  ]
  node [
    id 418
    label "sztuka"
  ]
  node [
    id 419
    label "sport_walki"
  ]
  node [
    id 420
    label "manszeta"
  ]
  node [
    id 421
    label "ripostowanie"
  ]
  node [
    id 422
    label "tusz"
  ]
  node [
    id 423
    label "kwestarz"
  ]
  node [
    id 424
    label "kwestowanie"
  ]
  node [
    id 425
    label "collection"
  ]
  node [
    id 426
    label "koszyk&#243;wka"
  ]
  node [
    id 427
    label "chwyt"
  ]
  node [
    id 428
    label "komisja"
  ]
  node [
    id 429
    label "ii"
  ]
  node [
    id 430
    label "iii"
  ]
  node [
    id 431
    label "sejm"
  ]
  node [
    id 432
    label "dziecko"
  ]
  node [
    id 433
    label "i"
  ]
  node [
    id 434
    label "m&#322;odzie&#380;"
  ]
  node [
    id 435
    label "Charles"
  ]
  node [
    id 436
    label "&#8217;"
  ]
  node [
    id 437
    label "albo"
  ]
  node [
    id 438
    label "Karolina"
  ]
  node [
    id 439
    label "Pankiewicz"
  ]
  node [
    id 440
    label "de"
  ]
  node [
    id 441
    label "Gaulle"
  ]
  node [
    id 442
    label "gimnazjum"
  ]
  node [
    id 443
    label "nr"
  ]
  node [
    id 444
    label "29"
  ]
  node [
    id 445
    label "zeszyt"
  ]
  node [
    id 446
    label "oddzia&#322;"
  ]
  node [
    id 447
    label "dwuj&#281;zyczny"
  ]
  node [
    id 448
    label "on"
  ]
  node [
    id 449
    label "daniel"
  ]
  node [
    id 450
    label "Milewski"
  ]
  node [
    id 451
    label "sesja"
  ]
  node [
    id 452
    label "rzeczpospolita"
  ]
  node [
    id 453
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 428
    target 429
  ]
  edge [
    source 428
    target 430
  ]
  edge [
    source 431
    target 432
  ]
  edge [
    source 431
    target 433
  ]
  edge [
    source 431
    target 434
  ]
  edge [
    source 431
    target 451
  ]
  edge [
    source 432
    target 433
  ]
  edge [
    source 432
    target 434
  ]
  edge [
    source 432
    target 451
  ]
  edge [
    source 433
    target 434
  ]
  edge [
    source 433
    target 451
  ]
  edge [
    source 434
    target 451
  ]
  edge [
    source 435
    target 436
  ]
  edge [
    source 435
    target 437
  ]
  edge [
    source 435
    target 440
  ]
  edge [
    source 435
    target 441
  ]
  edge [
    source 435
    target 442
  ]
  edge [
    source 435
    target 443
  ]
  edge [
    source 435
    target 444
  ]
  edge [
    source 435
    target 445
  ]
  edge [
    source 435
    target 446
  ]
  edge [
    source 435
    target 447
  ]
  edge [
    source 435
    target 448
  ]
  edge [
    source 436
    target 437
  ]
  edge [
    source 436
    target 440
  ]
  edge [
    source 436
    target 441
  ]
  edge [
    source 436
    target 436
  ]
  edge [
    source 436
    target 442
  ]
  edge [
    source 436
    target 443
  ]
  edge [
    source 436
    target 444
  ]
  edge [
    source 436
    target 445
  ]
  edge [
    source 436
    target 446
  ]
  edge [
    source 436
    target 447
  ]
  edge [
    source 436
    target 448
  ]
  edge [
    source 437
    target 440
  ]
  edge [
    source 437
    target 441
  ]
  edge [
    source 437
    target 437
  ]
  edge [
    source 437
    target 442
  ]
  edge [
    source 437
    target 443
  ]
  edge [
    source 437
    target 444
  ]
  edge [
    source 437
    target 445
  ]
  edge [
    source 437
    target 446
  ]
  edge [
    source 437
    target 447
  ]
  edge [
    source 437
    target 448
  ]
  edge [
    source 438
    target 439
  ]
  edge [
    source 440
    target 441
  ]
  edge [
    source 440
    target 442
  ]
  edge [
    source 440
    target 443
  ]
  edge [
    source 440
    target 444
  ]
  edge [
    source 440
    target 445
  ]
  edge [
    source 440
    target 446
  ]
  edge [
    source 440
    target 447
  ]
  edge [
    source 440
    target 448
  ]
  edge [
    source 441
    target 442
  ]
  edge [
    source 441
    target 443
  ]
  edge [
    source 441
    target 444
  ]
  edge [
    source 441
    target 445
  ]
  edge [
    source 441
    target 446
  ]
  edge [
    source 441
    target 447
  ]
  edge [
    source 441
    target 448
  ]
  edge [
    source 442
    target 443
  ]
  edge [
    source 442
    target 444
  ]
  edge [
    source 442
    target 445
  ]
  edge [
    source 442
    target 446
  ]
  edge [
    source 442
    target 447
  ]
  edge [
    source 442
    target 448
  ]
  edge [
    source 443
    target 444
  ]
  edge [
    source 443
    target 445
  ]
  edge [
    source 443
    target 446
  ]
  edge [
    source 443
    target 447
  ]
  edge [
    source 443
    target 448
  ]
  edge [
    source 444
    target 445
  ]
  edge [
    source 444
    target 446
  ]
  edge [
    source 444
    target 447
  ]
  edge [
    source 444
    target 448
  ]
  edge [
    source 445
    target 446
  ]
  edge [
    source 445
    target 447
  ]
  edge [
    source 445
    target 448
  ]
  edge [
    source 446
    target 447
  ]
  edge [
    source 446
    target 448
  ]
  edge [
    source 447
    target 448
  ]
  edge [
    source 449
    target 450
  ]
  edge [
    source 452
    target 453
  ]
]
