graph [
  node [
    id 0
    label "urz&#281;dnik"
    origin "text"
  ]
  node [
    id 1
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 2
    label "wyda&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pozwolenie"
    origin "text"
  ]
  node [
    id 4
    label "manifestacja"
    origin "text"
  ]
  node [
    id 5
    label "antyaborcyjny"
    origin "text"
  ]
  node [
    id 6
    label "miko&#322;ajek"
    origin "text"
  ]
  node [
    id 7
    label "sam"
    origin "text"
  ]
  node [
    id 8
    label "centrum"
    origin "text"
  ]
  node [
    id 9
    label "jarmark"
    origin "text"
  ]
  node [
    id 10
    label "&#347;wi&#261;teczny"
    origin "text"
  ]
  node [
    id 11
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "przychodzi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "dziecko"
    origin "text"
  ]
  node [
    id 14
    label "pracownik"
  ]
  node [
    id 15
    label "korpus_urz&#281;dniczy"
  ]
  node [
    id 16
    label "pragmatyka"
  ]
  node [
    id 17
    label "salariat"
  ]
  node [
    id 18
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 19
    label "cz&#322;owiek"
  ]
  node [
    id 20
    label "delegowanie"
  ]
  node [
    id 21
    label "pracu&#347;"
  ]
  node [
    id 22
    label "r&#281;ka"
  ]
  node [
    id 23
    label "delegowa&#263;"
  ]
  node [
    id 24
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 25
    label "prawo_pracy"
  ]
  node [
    id 26
    label "semiotyka"
  ]
  node [
    id 27
    label "powierzy&#263;"
  ]
  node [
    id 28
    label "pieni&#261;dze"
  ]
  node [
    id 29
    label "plon"
  ]
  node [
    id 30
    label "give"
  ]
  node [
    id 31
    label "skojarzy&#263;"
  ]
  node [
    id 32
    label "d&#378;wi&#281;k"
  ]
  node [
    id 33
    label "zadenuncjowa&#263;"
  ]
  node [
    id 34
    label "impart"
  ]
  node [
    id 35
    label "da&#263;"
  ]
  node [
    id 36
    label "reszta"
  ]
  node [
    id 37
    label "zapach"
  ]
  node [
    id 38
    label "wydawnictwo"
  ]
  node [
    id 39
    label "zrobi&#263;"
  ]
  node [
    id 40
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 41
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 42
    label "wiano"
  ]
  node [
    id 43
    label "produkcja"
  ]
  node [
    id 44
    label "translate"
  ]
  node [
    id 45
    label "picture"
  ]
  node [
    id 46
    label "poda&#263;"
  ]
  node [
    id 47
    label "wprowadzi&#263;"
  ]
  node [
    id 48
    label "wytworzy&#263;"
  ]
  node [
    id 49
    label "dress"
  ]
  node [
    id 50
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 51
    label "tajemnica"
  ]
  node [
    id 52
    label "panna_na_wydaniu"
  ]
  node [
    id 53
    label "supply"
  ]
  node [
    id 54
    label "ujawni&#263;"
  ]
  node [
    id 55
    label "rynek"
  ]
  node [
    id 56
    label "doprowadzi&#263;"
  ]
  node [
    id 57
    label "testify"
  ]
  node [
    id 58
    label "insert"
  ]
  node [
    id 59
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 60
    label "wpisa&#263;"
  ]
  node [
    id 61
    label "zapozna&#263;"
  ]
  node [
    id 62
    label "wej&#347;&#263;"
  ]
  node [
    id 63
    label "spowodowa&#263;"
  ]
  node [
    id 64
    label "zej&#347;&#263;"
  ]
  node [
    id 65
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 66
    label "umie&#347;ci&#263;"
  ]
  node [
    id 67
    label "zacz&#261;&#263;"
  ]
  node [
    id 68
    label "indicate"
  ]
  node [
    id 69
    label "post&#261;pi&#263;"
  ]
  node [
    id 70
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 71
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 72
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 73
    label "zorganizowa&#263;"
  ]
  node [
    id 74
    label "appoint"
  ]
  node [
    id 75
    label "wystylizowa&#263;"
  ]
  node [
    id 76
    label "cause"
  ]
  node [
    id 77
    label "przerobi&#263;"
  ]
  node [
    id 78
    label "nabra&#263;"
  ]
  node [
    id 79
    label "make"
  ]
  node [
    id 80
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 81
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 82
    label "wydali&#263;"
  ]
  node [
    id 83
    label "manufacture"
  ]
  node [
    id 84
    label "tenis"
  ]
  node [
    id 85
    label "ustawi&#263;"
  ]
  node [
    id 86
    label "siatk&#243;wka"
  ]
  node [
    id 87
    label "zagra&#263;"
  ]
  node [
    id 88
    label "jedzenie"
  ]
  node [
    id 89
    label "poinformowa&#263;"
  ]
  node [
    id 90
    label "introduce"
  ]
  node [
    id 91
    label "nafaszerowa&#263;"
  ]
  node [
    id 92
    label "zaserwowa&#263;"
  ]
  node [
    id 93
    label "discover"
  ]
  node [
    id 94
    label "objawi&#263;"
  ]
  node [
    id 95
    label "dostrzec"
  ]
  node [
    id 96
    label "denounce"
  ]
  node [
    id 97
    label "donie&#347;&#263;"
  ]
  node [
    id 98
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 99
    label "obieca&#263;"
  ]
  node [
    id 100
    label "pozwoli&#263;"
  ]
  node [
    id 101
    label "odst&#261;pi&#263;"
  ]
  node [
    id 102
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 103
    label "przywali&#263;"
  ]
  node [
    id 104
    label "wyrzec_si&#281;"
  ]
  node [
    id 105
    label "sztachn&#261;&#263;"
  ]
  node [
    id 106
    label "rap"
  ]
  node [
    id 107
    label "feed"
  ]
  node [
    id 108
    label "convey"
  ]
  node [
    id 109
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 110
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 111
    label "udost&#281;pni&#263;"
  ]
  node [
    id 112
    label "przeznaczy&#263;"
  ]
  node [
    id 113
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 114
    label "zada&#263;"
  ]
  node [
    id 115
    label "dostarczy&#263;"
  ]
  node [
    id 116
    label "przekaza&#263;"
  ]
  node [
    id 117
    label "doda&#263;"
  ]
  node [
    id 118
    label "zap&#322;aci&#263;"
  ]
  node [
    id 119
    label "consort"
  ]
  node [
    id 120
    label "powi&#261;za&#263;"
  ]
  node [
    id 121
    label "swat"
  ]
  node [
    id 122
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 123
    label "confide"
  ]
  node [
    id 124
    label "charge"
  ]
  node [
    id 125
    label "ufa&#263;"
  ]
  node [
    id 126
    label "odda&#263;"
  ]
  node [
    id 127
    label "entrust"
  ]
  node [
    id 128
    label "wyzna&#263;"
  ]
  node [
    id 129
    label "zleci&#263;"
  ]
  node [
    id 130
    label "consign"
  ]
  node [
    id 131
    label "phone"
  ]
  node [
    id 132
    label "wpadni&#281;cie"
  ]
  node [
    id 133
    label "wydawa&#263;"
  ]
  node [
    id 134
    label "zjawisko"
  ]
  node [
    id 135
    label "intonacja"
  ]
  node [
    id 136
    label "wpa&#347;&#263;"
  ]
  node [
    id 137
    label "note"
  ]
  node [
    id 138
    label "onomatopeja"
  ]
  node [
    id 139
    label "modalizm"
  ]
  node [
    id 140
    label "nadlecenie"
  ]
  node [
    id 141
    label "sound"
  ]
  node [
    id 142
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 143
    label "wpada&#263;"
  ]
  node [
    id 144
    label "solmizacja"
  ]
  node [
    id 145
    label "seria"
  ]
  node [
    id 146
    label "dobiec"
  ]
  node [
    id 147
    label "transmiter"
  ]
  node [
    id 148
    label "heksachord"
  ]
  node [
    id 149
    label "akcent"
  ]
  node [
    id 150
    label "wydanie"
  ]
  node [
    id 151
    label "repetycja"
  ]
  node [
    id 152
    label "brzmienie"
  ]
  node [
    id 153
    label "wpadanie"
  ]
  node [
    id 154
    label "liczba_kwantowa"
  ]
  node [
    id 155
    label "kosmetyk"
  ]
  node [
    id 156
    label "ciasto"
  ]
  node [
    id 157
    label "aromat"
  ]
  node [
    id 158
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 159
    label "puff"
  ]
  node [
    id 160
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 161
    label "przyprawa"
  ]
  node [
    id 162
    label "upojno&#347;&#263;"
  ]
  node [
    id 163
    label "owiewanie"
  ]
  node [
    id 164
    label "smak"
  ]
  node [
    id 165
    label "impreza"
  ]
  node [
    id 166
    label "realizacja"
  ]
  node [
    id 167
    label "tingel-tangel"
  ]
  node [
    id 168
    label "numer"
  ]
  node [
    id 169
    label "monta&#380;"
  ]
  node [
    id 170
    label "postprodukcja"
  ]
  node [
    id 171
    label "performance"
  ]
  node [
    id 172
    label "fabrication"
  ]
  node [
    id 173
    label "zbi&#243;r"
  ]
  node [
    id 174
    label "product"
  ]
  node [
    id 175
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 176
    label "uzysk"
  ]
  node [
    id 177
    label "rozw&#243;j"
  ]
  node [
    id 178
    label "odtworzenie"
  ]
  node [
    id 179
    label "dorobek"
  ]
  node [
    id 180
    label "kreacja"
  ]
  node [
    id 181
    label "trema"
  ]
  node [
    id 182
    label "creation"
  ]
  node [
    id 183
    label "kooperowa&#263;"
  ]
  node [
    id 184
    label "debit"
  ]
  node [
    id 185
    label "redaktor"
  ]
  node [
    id 186
    label "druk"
  ]
  node [
    id 187
    label "publikacja"
  ]
  node [
    id 188
    label "redakcja"
  ]
  node [
    id 189
    label "szata_graficzna"
  ]
  node [
    id 190
    label "firma"
  ]
  node [
    id 191
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 192
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 193
    label "poster"
  ]
  node [
    id 194
    label "return"
  ]
  node [
    id 195
    label "metr"
  ]
  node [
    id 196
    label "rezultat"
  ]
  node [
    id 197
    label "naturalia"
  ]
  node [
    id 198
    label "wypaplanie"
  ]
  node [
    id 199
    label "enigmat"
  ]
  node [
    id 200
    label "spos&#243;b"
  ]
  node [
    id 201
    label "wiedza"
  ]
  node [
    id 202
    label "zachowanie"
  ]
  node [
    id 203
    label "zachowywanie"
  ]
  node [
    id 204
    label "secret"
  ]
  node [
    id 205
    label "obowi&#261;zek"
  ]
  node [
    id 206
    label "dyskrecja"
  ]
  node [
    id 207
    label "informacja"
  ]
  node [
    id 208
    label "rzecz"
  ]
  node [
    id 209
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 210
    label "taj&#324;"
  ]
  node [
    id 211
    label "zachowa&#263;"
  ]
  node [
    id 212
    label "zachowywa&#263;"
  ]
  node [
    id 213
    label "portfel"
  ]
  node [
    id 214
    label "kwota"
  ]
  node [
    id 215
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 216
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 217
    label "forsa"
  ]
  node [
    id 218
    label "kapa&#263;"
  ]
  node [
    id 219
    label "kapn&#261;&#263;"
  ]
  node [
    id 220
    label "kapanie"
  ]
  node [
    id 221
    label "kapita&#322;"
  ]
  node [
    id 222
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 223
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 224
    label "kapni&#281;cie"
  ]
  node [
    id 225
    label "hajs"
  ]
  node [
    id 226
    label "dydki"
  ]
  node [
    id 227
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 228
    label "remainder"
  ]
  node [
    id 229
    label "pozosta&#322;y"
  ]
  node [
    id 230
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 231
    label "posa&#380;ek"
  ]
  node [
    id 232
    label "mienie"
  ]
  node [
    id 233
    label "wydawa&#263;_za_m&#261;&#380;"
  ]
  node [
    id 234
    label "wyda&#263;_za_m&#261;&#380;"
  ]
  node [
    id 235
    label "decyzja"
  ]
  node [
    id 236
    label "zwalnianie_si&#281;"
  ]
  node [
    id 237
    label "authorization"
  ]
  node [
    id 238
    label "koncesjonowanie"
  ]
  node [
    id 239
    label "zwolnienie_si&#281;"
  ]
  node [
    id 240
    label "pozwole&#324;stwo"
  ]
  node [
    id 241
    label "bycie_w_stanie"
  ]
  node [
    id 242
    label "odwieszenie"
  ]
  node [
    id 243
    label "odpowied&#378;"
  ]
  node [
    id 244
    label "pofolgowanie"
  ]
  node [
    id 245
    label "license"
  ]
  node [
    id 246
    label "franchise"
  ]
  node [
    id 247
    label "umo&#380;liwienie"
  ]
  node [
    id 248
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 249
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 250
    label "dokument"
  ]
  node [
    id 251
    label "uznanie"
  ]
  node [
    id 252
    label "zrobienie"
  ]
  node [
    id 253
    label "zawieszenie"
  ]
  node [
    id 254
    label "wznowienie"
  ]
  node [
    id 255
    label "przywr&#243;cenie"
  ]
  node [
    id 256
    label "powieszenie"
  ]
  node [
    id 257
    label "narobienie"
  ]
  node [
    id 258
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 259
    label "porobienie"
  ]
  node [
    id 260
    label "czynno&#347;&#263;"
  ]
  node [
    id 261
    label "zaimponowanie"
  ]
  node [
    id 262
    label "honorowanie"
  ]
  node [
    id 263
    label "uszanowanie"
  ]
  node [
    id 264
    label "uhonorowa&#263;"
  ]
  node [
    id 265
    label "oznajmienie"
  ]
  node [
    id 266
    label "imponowanie"
  ]
  node [
    id 267
    label "uhonorowanie"
  ]
  node [
    id 268
    label "spowodowanie"
  ]
  node [
    id 269
    label "honorowa&#263;"
  ]
  node [
    id 270
    label "uszanowa&#263;"
  ]
  node [
    id 271
    label "mniemanie"
  ]
  node [
    id 272
    label "szacuneczek"
  ]
  node [
    id 273
    label "recognition"
  ]
  node [
    id 274
    label "rewerencja"
  ]
  node [
    id 275
    label "szanowa&#263;"
  ]
  node [
    id 276
    label "postawa"
  ]
  node [
    id 277
    label "acclaim"
  ]
  node [
    id 278
    label "przej&#347;cie"
  ]
  node [
    id 279
    label "przechodzenie"
  ]
  node [
    id 280
    label "ocenienie"
  ]
  node [
    id 281
    label "zachwyt"
  ]
  node [
    id 282
    label "respect"
  ]
  node [
    id 283
    label "fame"
  ]
  node [
    id 284
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 285
    label "management"
  ]
  node [
    id 286
    label "resolution"
  ]
  node [
    id 287
    label "wytw&#243;r"
  ]
  node [
    id 288
    label "zdecydowanie"
  ]
  node [
    id 289
    label "react"
  ]
  node [
    id 290
    label "replica"
  ]
  node [
    id 291
    label "rozmowa"
  ]
  node [
    id 292
    label "wyj&#347;cie"
  ]
  node [
    id 293
    label "respondent"
  ]
  node [
    id 294
    label "reakcja"
  ]
  node [
    id 295
    label "mo&#380;liwy"
  ]
  node [
    id 296
    label "upowa&#380;nienie"
  ]
  node [
    id 297
    label "zapis"
  ]
  node [
    id 298
    label "&#347;wiadectwo"
  ]
  node [
    id 299
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 300
    label "parafa"
  ]
  node [
    id 301
    label "plik"
  ]
  node [
    id 302
    label "raport&#243;wka"
  ]
  node [
    id 303
    label "utw&#243;r"
  ]
  node [
    id 304
    label "record"
  ]
  node [
    id 305
    label "fascyku&#322;"
  ]
  node [
    id 306
    label "dokumentacja"
  ]
  node [
    id 307
    label "registratura"
  ]
  node [
    id 308
    label "artyku&#322;"
  ]
  node [
    id 309
    label "writing"
  ]
  node [
    id 310
    label "sygnatariusz"
  ]
  node [
    id 311
    label "folgowanie"
  ]
  node [
    id 312
    label "cognition"
  ]
  node [
    id 313
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 314
    label "intelekt"
  ]
  node [
    id 315
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 316
    label "zaawansowanie"
  ]
  node [
    id 317
    label "wykszta&#322;cenie"
  ]
  node [
    id 318
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 319
    label "zezwalanie"
  ]
  node [
    id 320
    label "powodowanie"
  ]
  node [
    id 321
    label "koncesja"
  ]
  node [
    id 322
    label "pokaz"
  ]
  node [
    id 323
    label "show"
  ]
  node [
    id 324
    label "zgromadzenie"
  ]
  node [
    id 325
    label "exhibition"
  ]
  node [
    id 326
    label "grupa"
  ]
  node [
    id 327
    label "odm&#322;adzanie"
  ]
  node [
    id 328
    label "liga"
  ]
  node [
    id 329
    label "jednostka_systematyczna"
  ]
  node [
    id 330
    label "asymilowanie"
  ]
  node [
    id 331
    label "gromada"
  ]
  node [
    id 332
    label "asymilowa&#263;"
  ]
  node [
    id 333
    label "egzemplarz"
  ]
  node [
    id 334
    label "Entuzjastki"
  ]
  node [
    id 335
    label "kompozycja"
  ]
  node [
    id 336
    label "Terranie"
  ]
  node [
    id 337
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 338
    label "category"
  ]
  node [
    id 339
    label "pakiet_klimatyczny"
  ]
  node [
    id 340
    label "oddzia&#322;"
  ]
  node [
    id 341
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 342
    label "cz&#261;steczka"
  ]
  node [
    id 343
    label "stage_set"
  ]
  node [
    id 344
    label "type"
  ]
  node [
    id 345
    label "specgrupa"
  ]
  node [
    id 346
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 347
    label "&#346;wietliki"
  ]
  node [
    id 348
    label "odm&#322;odzenie"
  ]
  node [
    id 349
    label "Eurogrupa"
  ]
  node [
    id 350
    label "odm&#322;adza&#263;"
  ]
  node [
    id 351
    label "formacja_geologiczna"
  ]
  node [
    id 352
    label "harcerze_starsi"
  ]
  node [
    id 353
    label "pokaz&#243;wka"
  ]
  node [
    id 354
    label "prezenter"
  ]
  node [
    id 355
    label "wydarzenie"
  ]
  node [
    id 356
    label "wyraz"
  ]
  node [
    id 357
    label "concourse"
  ]
  node [
    id 358
    label "gathering"
  ]
  node [
    id 359
    label "skupienie"
  ]
  node [
    id 360
    label "wsp&#243;lnota"
  ]
  node [
    id 361
    label "spotkanie"
  ]
  node [
    id 362
    label "organ"
  ]
  node [
    id 363
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 364
    label "gromadzenie"
  ]
  node [
    id 365
    label "templum"
  ]
  node [
    id 366
    label "konwentykiel"
  ]
  node [
    id 367
    label "klasztor"
  ]
  node [
    id 368
    label "caucus"
  ]
  node [
    id 369
    label "pozyskanie"
  ]
  node [
    id 370
    label "kongregacja"
  ]
  node [
    id 371
    label "przedstawienie"
  ]
  node [
    id 372
    label "przeciwny"
  ]
  node [
    id 373
    label "odwrotno&#347;&#263;"
  ]
  node [
    id 374
    label "odmienny"
  ]
  node [
    id 375
    label "inny"
  ]
  node [
    id 376
    label "odwrotnie"
  ]
  node [
    id 377
    label "po_przeciwnej_stronie"
  ]
  node [
    id 378
    label "przeciwnie"
  ]
  node [
    id 379
    label "niech&#281;tny"
  ]
  node [
    id 380
    label "bylina"
  ]
  node [
    id 381
    label "selerowate"
  ]
  node [
    id 382
    label "ludowy"
  ]
  node [
    id 383
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 384
    label "utw&#243;r_epicki"
  ]
  node [
    id 385
    label "pie&#347;&#324;"
  ]
  node [
    id 386
    label "selerowce"
  ]
  node [
    id 387
    label "sklep"
  ]
  node [
    id 388
    label "p&#243;&#322;ka"
  ]
  node [
    id 389
    label "stoisko"
  ]
  node [
    id 390
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 391
    label "sk&#322;ad"
  ]
  node [
    id 392
    label "obiekt_handlowy"
  ]
  node [
    id 393
    label "zaplecze"
  ]
  node [
    id 394
    label "witryna"
  ]
  node [
    id 395
    label "blok"
  ]
  node [
    id 396
    label "punkt"
  ]
  node [
    id 397
    label "Hollywood"
  ]
  node [
    id 398
    label "centrolew"
  ]
  node [
    id 399
    label "miejsce"
  ]
  node [
    id 400
    label "sejm"
  ]
  node [
    id 401
    label "o&#347;rodek"
  ]
  node [
    id 402
    label "centroprawica"
  ]
  node [
    id 403
    label "core"
  ]
  node [
    id 404
    label "&#347;rodek"
  ]
  node [
    id 405
    label "skupisko"
  ]
  node [
    id 406
    label "zal&#261;&#380;ek"
  ]
  node [
    id 407
    label "instytucja"
  ]
  node [
    id 408
    label "otoczenie"
  ]
  node [
    id 409
    label "warunki"
  ]
  node [
    id 410
    label "center"
  ]
  node [
    id 411
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 412
    label "bajt"
  ]
  node [
    id 413
    label "bloking"
  ]
  node [
    id 414
    label "j&#261;kanie"
  ]
  node [
    id 415
    label "przeszkoda"
  ]
  node [
    id 416
    label "zesp&#243;&#322;"
  ]
  node [
    id 417
    label "blokada"
  ]
  node [
    id 418
    label "bry&#322;a"
  ]
  node [
    id 419
    label "dzia&#322;"
  ]
  node [
    id 420
    label "kontynent"
  ]
  node [
    id 421
    label "nastawnia"
  ]
  node [
    id 422
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 423
    label "blockage"
  ]
  node [
    id 424
    label "block"
  ]
  node [
    id 425
    label "organizacja"
  ]
  node [
    id 426
    label "budynek"
  ]
  node [
    id 427
    label "start"
  ]
  node [
    id 428
    label "skorupa_ziemska"
  ]
  node [
    id 429
    label "program"
  ]
  node [
    id 430
    label "zeszyt"
  ]
  node [
    id 431
    label "blokowisko"
  ]
  node [
    id 432
    label "barak"
  ]
  node [
    id 433
    label "stok_kontynentalny"
  ]
  node [
    id 434
    label "whole"
  ]
  node [
    id 435
    label "square"
  ]
  node [
    id 436
    label "kr&#261;g"
  ]
  node [
    id 437
    label "ram&#243;wka"
  ]
  node [
    id 438
    label "zamek"
  ]
  node [
    id 439
    label "obrona"
  ]
  node [
    id 440
    label "ok&#322;adka"
  ]
  node [
    id 441
    label "bie&#380;nia"
  ]
  node [
    id 442
    label "referat"
  ]
  node [
    id 443
    label "dom_wielorodzinny"
  ]
  node [
    id 444
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 445
    label "po&#322;o&#380;enie"
  ]
  node [
    id 446
    label "sprawa"
  ]
  node [
    id 447
    label "ust&#281;p"
  ]
  node [
    id 448
    label "plan"
  ]
  node [
    id 449
    label "obiekt_matematyczny"
  ]
  node [
    id 450
    label "problemat"
  ]
  node [
    id 451
    label "plamka"
  ]
  node [
    id 452
    label "stopie&#324;_pisma"
  ]
  node [
    id 453
    label "jednostka"
  ]
  node [
    id 454
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 455
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 456
    label "mark"
  ]
  node [
    id 457
    label "chwila"
  ]
  node [
    id 458
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 459
    label "prosta"
  ]
  node [
    id 460
    label "problematyka"
  ]
  node [
    id 461
    label "obiekt"
  ]
  node [
    id 462
    label "zapunktowa&#263;"
  ]
  node [
    id 463
    label "podpunkt"
  ]
  node [
    id 464
    label "wojsko"
  ]
  node [
    id 465
    label "kres"
  ]
  node [
    id 466
    label "przestrze&#324;"
  ]
  node [
    id 467
    label "point"
  ]
  node [
    id 468
    label "pozycja"
  ]
  node [
    id 469
    label "warunek_lokalowy"
  ]
  node [
    id 470
    label "plac"
  ]
  node [
    id 471
    label "location"
  ]
  node [
    id 472
    label "uwaga"
  ]
  node [
    id 473
    label "status"
  ]
  node [
    id 474
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 475
    label "cia&#322;o"
  ]
  node [
    id 476
    label "cecha"
  ]
  node [
    id 477
    label "praca"
  ]
  node [
    id 478
    label "rz&#261;d"
  ]
  node [
    id 479
    label "koalicja"
  ]
  node [
    id 480
    label "parlament"
  ]
  node [
    id 481
    label "izba_ni&#380;sza"
  ]
  node [
    id 482
    label "lewica"
  ]
  node [
    id 483
    label "siedziba"
  ]
  node [
    id 484
    label "parliament"
  ]
  node [
    id 485
    label "obrady"
  ]
  node [
    id 486
    label "prawica"
  ]
  node [
    id 487
    label "Los_Angeles"
  ]
  node [
    id 488
    label "market"
  ]
  node [
    id 489
    label "targ"
  ]
  node [
    id 490
    label "targowica"
  ]
  node [
    id 491
    label "kram"
  ]
  node [
    id 492
    label "&#321;ubianka"
  ]
  node [
    id 493
    label "area"
  ]
  node [
    id 494
    label "Majdan"
  ]
  node [
    id 495
    label "pole_bitwy"
  ]
  node [
    id 496
    label "obszar"
  ]
  node [
    id 497
    label "pierzeja"
  ]
  node [
    id 498
    label "miasto"
  ]
  node [
    id 499
    label "sprzeda&#380;"
  ]
  node [
    id 500
    label "szmartuz"
  ]
  node [
    id 501
    label "kramnica"
  ]
  node [
    id 502
    label "zdrada"
  ]
  node [
    id 503
    label "od&#347;wi&#281;tny"
  ]
  node [
    id 504
    label "obrz&#281;dowy"
  ]
  node [
    id 505
    label "&#347;wi&#261;tecznie"
  ]
  node [
    id 506
    label "dzie&#324;_wolny"
  ]
  node [
    id 507
    label "wyj&#261;tkowy"
  ]
  node [
    id 508
    label "&#347;wi&#281;tny"
  ]
  node [
    id 509
    label "uroczysty"
  ]
  node [
    id 510
    label "od&#347;wi&#281;tnie"
  ]
  node [
    id 511
    label "specjalnie"
  ]
  node [
    id 512
    label "&#347;wi&#261;teczno"
  ]
  node [
    id 513
    label "specjalny"
  ]
  node [
    id 514
    label "obrz&#281;dowo"
  ]
  node [
    id 515
    label "tradycyjny"
  ]
  node [
    id 516
    label "powierzchowny"
  ]
  node [
    id 517
    label "niezwyczajny"
  ]
  node [
    id 518
    label "powa&#380;ny"
  ]
  node [
    id 519
    label "formalny"
  ]
  node [
    id 520
    label "podnios&#322;y"
  ]
  node [
    id 521
    label "uroczy&#347;cie"
  ]
  node [
    id 522
    label "wyj&#261;tkowo"
  ]
  node [
    id 523
    label "&#347;wi&#281;ty"
  ]
  node [
    id 524
    label "przybywa&#263;"
  ]
  node [
    id 525
    label "dochodzi&#263;"
  ]
  node [
    id 526
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 527
    label "robi&#263;"
  ]
  node [
    id 528
    label "uzyskiwa&#263;"
  ]
  node [
    id 529
    label "claim"
  ]
  node [
    id 530
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 531
    label "osi&#261;ga&#263;"
  ]
  node [
    id 532
    label "ripen"
  ]
  node [
    id 533
    label "supervene"
  ]
  node [
    id 534
    label "doczeka&#263;"
  ]
  node [
    id 535
    label "przesy&#322;ka"
  ]
  node [
    id 536
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 537
    label "doznawa&#263;"
  ]
  node [
    id 538
    label "reach"
  ]
  node [
    id 539
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 540
    label "dociera&#263;"
  ]
  node [
    id 541
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 542
    label "zachodzi&#263;"
  ]
  node [
    id 543
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 544
    label "postrzega&#263;"
  ]
  node [
    id 545
    label "orgazm"
  ]
  node [
    id 546
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 547
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 548
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 549
    label "dokoptowywa&#263;"
  ]
  node [
    id 550
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 551
    label "dolatywa&#263;"
  ]
  node [
    id 552
    label "powodowa&#263;"
  ]
  node [
    id 553
    label "submit"
  ]
  node [
    id 554
    label "get"
  ]
  node [
    id 555
    label "zyskiwa&#263;"
  ]
  node [
    id 556
    label "utulenie"
  ]
  node [
    id 557
    label "pediatra"
  ]
  node [
    id 558
    label "dzieciak"
  ]
  node [
    id 559
    label "utulanie"
  ]
  node [
    id 560
    label "dzieciarnia"
  ]
  node [
    id 561
    label "niepe&#322;noletni"
  ]
  node [
    id 562
    label "organizm"
  ]
  node [
    id 563
    label "utula&#263;"
  ]
  node [
    id 564
    label "cz&#322;owieczek"
  ]
  node [
    id 565
    label "fledgling"
  ]
  node [
    id 566
    label "zwierz&#281;"
  ]
  node [
    id 567
    label "utuli&#263;"
  ]
  node [
    id 568
    label "m&#322;odzik"
  ]
  node [
    id 569
    label "pedofil"
  ]
  node [
    id 570
    label "m&#322;odziak"
  ]
  node [
    id 571
    label "potomek"
  ]
  node [
    id 572
    label "entliczek-pentliczek"
  ]
  node [
    id 573
    label "potomstwo"
  ]
  node [
    id 574
    label "sraluch"
  ]
  node [
    id 575
    label "czeladka"
  ]
  node [
    id 576
    label "dzietno&#347;&#263;"
  ]
  node [
    id 577
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 578
    label "bawienie_si&#281;"
  ]
  node [
    id 579
    label "pomiot"
  ]
  node [
    id 580
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 581
    label "kinderbal"
  ]
  node [
    id 582
    label "krewny"
  ]
  node [
    id 583
    label "ludzko&#347;&#263;"
  ]
  node [
    id 584
    label "wapniak"
  ]
  node [
    id 585
    label "os&#322;abia&#263;"
  ]
  node [
    id 586
    label "posta&#263;"
  ]
  node [
    id 587
    label "hominid"
  ]
  node [
    id 588
    label "podw&#322;adny"
  ]
  node [
    id 589
    label "os&#322;abianie"
  ]
  node [
    id 590
    label "g&#322;owa"
  ]
  node [
    id 591
    label "figura"
  ]
  node [
    id 592
    label "portrecista"
  ]
  node [
    id 593
    label "dwun&#243;g"
  ]
  node [
    id 594
    label "profanum"
  ]
  node [
    id 595
    label "mikrokosmos"
  ]
  node [
    id 596
    label "nasada"
  ]
  node [
    id 597
    label "duch"
  ]
  node [
    id 598
    label "antropochoria"
  ]
  node [
    id 599
    label "osoba"
  ]
  node [
    id 600
    label "wz&#243;r"
  ]
  node [
    id 601
    label "senior"
  ]
  node [
    id 602
    label "oddzia&#322;ywanie"
  ]
  node [
    id 603
    label "Adam"
  ]
  node [
    id 604
    label "homo_sapiens"
  ]
  node [
    id 605
    label "polifag"
  ]
  node [
    id 606
    label "ma&#322;oletny"
  ]
  node [
    id 607
    label "m&#322;ody"
  ]
  node [
    id 608
    label "p&#322;aszczyzna"
  ]
  node [
    id 609
    label "odwadnia&#263;"
  ]
  node [
    id 610
    label "przyswoi&#263;"
  ]
  node [
    id 611
    label "sk&#243;ra"
  ]
  node [
    id 612
    label "odwodni&#263;"
  ]
  node [
    id 613
    label "ewoluowanie"
  ]
  node [
    id 614
    label "staw"
  ]
  node [
    id 615
    label "ow&#322;osienie"
  ]
  node [
    id 616
    label "unerwienie"
  ]
  node [
    id 617
    label "wyewoluowanie"
  ]
  node [
    id 618
    label "przyswajanie"
  ]
  node [
    id 619
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 620
    label "wyewoluowa&#263;"
  ]
  node [
    id 621
    label "biorytm"
  ]
  node [
    id 622
    label "ewoluowa&#263;"
  ]
  node [
    id 623
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 624
    label "istota_&#380;ywa"
  ]
  node [
    id 625
    label "otworzy&#263;"
  ]
  node [
    id 626
    label "otwiera&#263;"
  ]
  node [
    id 627
    label "czynnik_biotyczny"
  ]
  node [
    id 628
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 629
    label "otworzenie"
  ]
  node [
    id 630
    label "otwieranie"
  ]
  node [
    id 631
    label "individual"
  ]
  node [
    id 632
    label "szkielet"
  ]
  node [
    id 633
    label "ty&#322;"
  ]
  node [
    id 634
    label "przyswaja&#263;"
  ]
  node [
    id 635
    label "przyswojenie"
  ]
  node [
    id 636
    label "odwadnianie"
  ]
  node [
    id 637
    label "odwodnienie"
  ]
  node [
    id 638
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 639
    label "starzenie_si&#281;"
  ]
  node [
    id 640
    label "prz&#243;d"
  ]
  node [
    id 641
    label "uk&#322;ad"
  ]
  node [
    id 642
    label "temperatura"
  ]
  node [
    id 643
    label "l&#281;d&#378;wie"
  ]
  node [
    id 644
    label "cz&#322;onek"
  ]
  node [
    id 645
    label "degenerat"
  ]
  node [
    id 646
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 647
    label "zwyrol"
  ]
  node [
    id 648
    label "czerniak"
  ]
  node [
    id 649
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 650
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 651
    label "paszcza"
  ]
  node [
    id 652
    label "popapraniec"
  ]
  node [
    id 653
    label "skuba&#263;"
  ]
  node [
    id 654
    label "skubanie"
  ]
  node [
    id 655
    label "skubni&#281;cie"
  ]
  node [
    id 656
    label "agresja"
  ]
  node [
    id 657
    label "zwierz&#281;ta"
  ]
  node [
    id 658
    label "fukni&#281;cie"
  ]
  node [
    id 659
    label "farba"
  ]
  node [
    id 660
    label "fukanie"
  ]
  node [
    id 661
    label "gad"
  ]
  node [
    id 662
    label "siedzie&#263;"
  ]
  node [
    id 663
    label "oswaja&#263;"
  ]
  node [
    id 664
    label "tresowa&#263;"
  ]
  node [
    id 665
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 666
    label "poligamia"
  ]
  node [
    id 667
    label "oz&#243;r"
  ]
  node [
    id 668
    label "skubn&#261;&#263;"
  ]
  node [
    id 669
    label "wios&#322;owa&#263;"
  ]
  node [
    id 670
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 671
    label "le&#380;enie"
  ]
  node [
    id 672
    label "niecz&#322;owiek"
  ]
  node [
    id 673
    label "wios&#322;owanie"
  ]
  node [
    id 674
    label "napasienie_si&#281;"
  ]
  node [
    id 675
    label "wiwarium"
  ]
  node [
    id 676
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 677
    label "animalista"
  ]
  node [
    id 678
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 679
    label "budowa"
  ]
  node [
    id 680
    label "hodowla"
  ]
  node [
    id 681
    label "pasienie_si&#281;"
  ]
  node [
    id 682
    label "sodomita"
  ]
  node [
    id 683
    label "monogamia"
  ]
  node [
    id 684
    label "przyssawka"
  ]
  node [
    id 685
    label "budowa_cia&#322;a"
  ]
  node [
    id 686
    label "okrutnik"
  ]
  node [
    id 687
    label "grzbiet"
  ]
  node [
    id 688
    label "weterynarz"
  ]
  node [
    id 689
    label "&#322;eb"
  ]
  node [
    id 690
    label "wylinka"
  ]
  node [
    id 691
    label "bestia"
  ]
  node [
    id 692
    label "poskramia&#263;"
  ]
  node [
    id 693
    label "fauna"
  ]
  node [
    id 694
    label "treser"
  ]
  node [
    id 695
    label "siedzenie"
  ]
  node [
    id 696
    label "le&#380;e&#263;"
  ]
  node [
    id 697
    label "uspokojenie"
  ]
  node [
    id 698
    label "utulenie_si&#281;"
  ]
  node [
    id 699
    label "u&#347;pienie"
  ]
  node [
    id 700
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 701
    label "uspokoi&#263;"
  ]
  node [
    id 702
    label "utulanie_si&#281;"
  ]
  node [
    id 703
    label "usypianie"
  ]
  node [
    id 704
    label "pocieszanie"
  ]
  node [
    id 705
    label "uspokajanie"
  ]
  node [
    id 706
    label "usypia&#263;"
  ]
  node [
    id 707
    label "uspokaja&#263;"
  ]
  node [
    id 708
    label "wyliczanka"
  ]
  node [
    id 709
    label "specjalista"
  ]
  node [
    id 710
    label "harcerz"
  ]
  node [
    id 711
    label "ch&#322;opta&#347;"
  ]
  node [
    id 712
    label "zawodnik"
  ]
  node [
    id 713
    label "go&#322;ow&#261;s"
  ]
  node [
    id 714
    label "m&#322;ode"
  ]
  node [
    id 715
    label "stopie&#324;_harcerski"
  ]
  node [
    id 716
    label "g&#243;wniarz"
  ]
  node [
    id 717
    label "beniaminek"
  ]
  node [
    id 718
    label "dewiant"
  ]
  node [
    id 719
    label "istotka"
  ]
  node [
    id 720
    label "bech"
  ]
  node [
    id 721
    label "dziecinny"
  ]
  node [
    id 722
    label "naiwniak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 330
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 318
  ]
  edge [
    source 13
    target 294
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 722
  ]
]
