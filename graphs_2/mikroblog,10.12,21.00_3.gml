graph [
  node [
    id 0
    label "black"
    origin "text"
  ]
  node [
    id 1
    label "dove"
    origin "text"
  ]
  node [
    id 2
    label "ten"
    origin "text"
  ]
  node [
    id 3
    label "sam"
    origin "text"
  ]
  node [
    id 4
    label "szablon"
    origin "text"
  ]
  node [
    id 5
    label "stocka"
    origin "text"
  ]
  node [
    id 6
    label "okre&#347;lony"
  ]
  node [
    id 7
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 8
    label "wiadomy"
  ]
  node [
    id 9
    label "sklep"
  ]
  node [
    id 10
    label "p&#243;&#322;ka"
  ]
  node [
    id 11
    label "firma"
  ]
  node [
    id 12
    label "stoisko"
  ]
  node [
    id 13
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 14
    label "sk&#322;ad"
  ]
  node [
    id 15
    label "obiekt_handlowy"
  ]
  node [
    id 16
    label "zaplecze"
  ]
  node [
    id 17
    label "witryna"
  ]
  node [
    id 18
    label "model"
  ]
  node [
    id 19
    label "struktura"
  ]
  node [
    id 20
    label "mildew"
  ]
  node [
    id 21
    label "jig"
  ]
  node [
    id 22
    label "drabina_analgetyczna"
  ]
  node [
    id 23
    label "wz&#243;r"
  ]
  node [
    id 24
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 25
    label "C"
  ]
  node [
    id 26
    label "D"
  ]
  node [
    id 27
    label "exemplar"
  ]
  node [
    id 28
    label "mechanika"
  ]
  node [
    id 29
    label "o&#347;"
  ]
  node [
    id 30
    label "usenet"
  ]
  node [
    id 31
    label "rozprz&#261;c"
  ]
  node [
    id 32
    label "zachowanie"
  ]
  node [
    id 33
    label "cybernetyk"
  ]
  node [
    id 34
    label "podsystem"
  ]
  node [
    id 35
    label "system"
  ]
  node [
    id 36
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 37
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 38
    label "systemat"
  ]
  node [
    id 39
    label "cecha"
  ]
  node [
    id 40
    label "konstrukcja"
  ]
  node [
    id 41
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 42
    label "konstelacja"
  ]
  node [
    id 43
    label "zapis"
  ]
  node [
    id 44
    label "figure"
  ]
  node [
    id 45
    label "typ"
  ]
  node [
    id 46
    label "spos&#243;b"
  ]
  node [
    id 47
    label "cz&#322;owiek"
  ]
  node [
    id 48
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 49
    label "ideal"
  ]
  node [
    id 50
    label "rule"
  ]
  node [
    id 51
    label "ruch"
  ]
  node [
    id 52
    label "dekal"
  ]
  node [
    id 53
    label "motyw"
  ]
  node [
    id 54
    label "projekt"
  ]
  node [
    id 55
    label "prezenter"
  ]
  node [
    id 56
    label "zi&#243;&#322;ko"
  ]
  node [
    id 57
    label "motif"
  ]
  node [
    id 58
    label "pozowanie"
  ]
  node [
    id 59
    label "matryca"
  ]
  node [
    id 60
    label "adaptation"
  ]
  node [
    id 61
    label "pozowa&#263;"
  ]
  node [
    id 62
    label "imitacja"
  ]
  node [
    id 63
    label "orygina&#322;"
  ]
  node [
    id 64
    label "facet"
  ]
  node [
    id 65
    label "miniatura"
  ]
  node [
    id 66
    label "cyfra_rzymska"
  ]
  node [
    id 67
    label "j&#281;zyk_programowania"
  ]
  node [
    id 68
    label "fulleren"
  ]
  node [
    id 69
    label "niemetal"
  ]
  node [
    id 70
    label "jednostka"
  ]
  node [
    id 71
    label "carbon"
  ]
  node [
    id 72
    label "jednostka_&#322;adunku_elektrycznego"
  ]
  node [
    id 73
    label "stopie&#324;"
  ]
  node [
    id 74
    label "c"
  ]
  node [
    id 75
    label "makroelement"
  ]
  node [
    id 76
    label "skala_Celsjusza"
  ]
  node [
    id 77
    label "w&#281;glowiec"
  ]
  node [
    id 78
    label "format_pomocniczy"
  ]
  node [
    id 79
    label "haczyk"
  ]
  node [
    id 80
    label "ludowy"
  ]
  node [
    id 81
    label "melodia"
  ]
  node [
    id 82
    label "taniec"
  ]
  node [
    id 83
    label "taniec_ludowy"
  ]
  node [
    id 84
    label "si&#243;demka"
  ]
  node [
    id 85
    label "tr&#243;jka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
]
