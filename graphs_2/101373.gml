graph [
  node [
    id 0
    label "international"
    origin "text"
  ]
  node [
    id 1
    label "mobil"
    origin "text"
  ]
  node [
    id 2
    label "subscriber"
    origin "text"
  ]
  node [
    id 3
    label "identity"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
  ]
  node [
    id 5
    label "CB_radio"
  ]
  node [
    id 6
    label "rze&#378;ba"
  ]
  node [
    id 7
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 8
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 9
    label "rze&#378;biarstwo"
  ]
  node [
    id 10
    label "planacja"
  ]
  node [
    id 11
    label "plastyka"
  ]
  node [
    id 12
    label "relief"
  ]
  node [
    id 13
    label "bozzetto"
  ]
  node [
    id 14
    label "Ziemia"
  ]
  node [
    id 15
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 16
    label "asymilowa&#263;"
  ]
  node [
    id 17
    label "nasada"
  ]
  node [
    id 18
    label "profanum"
  ]
  node [
    id 19
    label "wz&#243;r"
  ]
  node [
    id 20
    label "senior"
  ]
  node [
    id 21
    label "asymilowanie"
  ]
  node [
    id 22
    label "os&#322;abia&#263;"
  ]
  node [
    id 23
    label "homo_sapiens"
  ]
  node [
    id 24
    label "osoba"
  ]
  node [
    id 25
    label "ludzko&#347;&#263;"
  ]
  node [
    id 26
    label "Adam"
  ]
  node [
    id 27
    label "hominid"
  ]
  node [
    id 28
    label "posta&#263;"
  ]
  node [
    id 29
    label "portrecista"
  ]
  node [
    id 30
    label "polifag"
  ]
  node [
    id 31
    label "podw&#322;adny"
  ]
  node [
    id 32
    label "dwun&#243;g"
  ]
  node [
    id 33
    label "wapniak"
  ]
  node [
    id 34
    label "duch"
  ]
  node [
    id 35
    label "os&#322;abianie"
  ]
  node [
    id 36
    label "antropochoria"
  ]
  node [
    id 37
    label "figura"
  ]
  node [
    id 38
    label "g&#322;owa"
  ]
  node [
    id 39
    label "mikrokosmos"
  ]
  node [
    id 40
    label "oddzia&#322;ywanie"
  ]
  node [
    id 41
    label "Subscriber"
  ]
  node [
    id 42
    label "Identity"
  ]
  node [
    id 43
    label "ITU"
  ]
  node [
    id 44
    label "tom"
  ]
  node [
    id 45
    label "Recommendation"
  ]
  node [
    id 46
    label "e"
  ]
  node [
    id 47
    label "212"
  ]
  node [
    id 48
    label "05"
  ]
  node [
    id 49
    label "08"
  ]
  node [
    id 50
    label "The"
  ]
  node [
    id 51
    label "identification"
  ]
  node [
    id 52
    label "plan"
  ]
  node [
    id 53
    label "for"
  ]
  node [
    id 54
    label "public"
  ]
  node [
    id 55
    label "networks"
  ]
  node [
    id 56
    label "Anda"
  ]
  node [
    id 57
    label "subscriptions"
  ]
  node [
    id 58
    label "country"
  ]
  node [
    id 59
    label "Code"
  ]
  node [
    id 60
    label "Network"
  ]
  node [
    id 61
    label "Identification"
  ]
  node [
    id 62
    label "Number"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 61
  ]
  edge [
    source 41
    target 62
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 43
    target 47
  ]
  edge [
    source 43
    target 48
  ]
  edge [
    source 43
    target 49
  ]
  edge [
    source 43
    target 50
  ]
  edge [
    source 43
    target 51
  ]
  edge [
    source 43
    target 52
  ]
  edge [
    source 43
    target 53
  ]
  edge [
    source 43
    target 54
  ]
  edge [
    source 43
    target 55
  ]
  edge [
    source 43
    target 56
  ]
  edge [
    source 43
    target 57
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 44
    target 48
  ]
  edge [
    source 44
    target 49
  ]
  edge [
    source 44
    target 50
  ]
  edge [
    source 44
    target 51
  ]
  edge [
    source 44
    target 52
  ]
  edge [
    source 44
    target 53
  ]
  edge [
    source 44
    target 54
  ]
  edge [
    source 44
    target 55
  ]
  edge [
    source 44
    target 56
  ]
  edge [
    source 44
    target 57
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 45
    target 49
  ]
  edge [
    source 45
    target 50
  ]
  edge [
    source 45
    target 51
  ]
  edge [
    source 45
    target 52
  ]
  edge [
    source 45
    target 53
  ]
  edge [
    source 45
    target 54
  ]
  edge [
    source 45
    target 55
  ]
  edge [
    source 45
    target 56
  ]
  edge [
    source 45
    target 57
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 46
    target 50
  ]
  edge [
    source 46
    target 51
  ]
  edge [
    source 46
    target 52
  ]
  edge [
    source 46
    target 53
  ]
  edge [
    source 46
    target 54
  ]
  edge [
    source 46
    target 55
  ]
  edge [
    source 46
    target 56
  ]
  edge [
    source 46
    target 57
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 47
    target 51
  ]
  edge [
    source 47
    target 52
  ]
  edge [
    source 47
    target 53
  ]
  edge [
    source 47
    target 54
  ]
  edge [
    source 47
    target 55
  ]
  edge [
    source 47
    target 56
  ]
  edge [
    source 47
    target 57
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 51
  ]
  edge [
    source 48
    target 52
  ]
  edge [
    source 48
    target 53
  ]
  edge [
    source 48
    target 54
  ]
  edge [
    source 48
    target 55
  ]
  edge [
    source 48
    target 56
  ]
  edge [
    source 48
    target 57
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 49
    target 52
  ]
  edge [
    source 49
    target 53
  ]
  edge [
    source 49
    target 54
  ]
  edge [
    source 49
    target 55
  ]
  edge [
    source 49
    target 56
  ]
  edge [
    source 49
    target 57
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 50
    target 53
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 50
    target 55
  ]
  edge [
    source 50
    target 56
  ]
  edge [
    source 50
    target 57
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 51
    target 56
  ]
  edge [
    source 51
    target 57
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 52
    target 56
  ]
  edge [
    source 52
    target 57
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 53
    target 57
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 61
    target 62
  ]
]
