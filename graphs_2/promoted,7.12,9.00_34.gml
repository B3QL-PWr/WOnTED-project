graph [
  node [
    id 0
    label "recent"
    origin "text"
  ]
  node [
    id 1
    label "return"
    origin "text"
  ]
  node [
    id 2
    label "leipzig"
    origin "text"
  ]
  node [
    id 3
    label "reminded"
    origin "text"
  ]
  node [
    id 4
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 5
    label "why"
    origin "text"
  ]
  node [
    id 6
    label "green"
    origin "text"
  ]
  node [
    id 7
    label "has"
    origin "text"
  ]
  node [
    id 8
    label "been"
    origin "text"
  ]
  node [
    id 9
    label "lot"
    origin "text"
  ]
  node [
    id 10
    label "difficult"
    origin "text"
  ]
  node [
    id 11
    label "than"
    origin "text"
  ]
  node [
    id 12
    label "the"
    origin "text"
  ]
  node [
    id 13
    label "rhetoric"
    origin "text"
  ]
  node [
    id 14
    label "would"
    origin "text"
  ]
  node [
    id 15
    label "sometimes"
    origin "text"
  ]
  node [
    id 16
    label "suggest"
    origin "text"
  ]
  node [
    id 17
    label "czyj&#347;"
  ]
  node [
    id 18
    label "m&#261;&#380;"
  ]
  node [
    id 19
    label "prywatny"
  ]
  node [
    id 20
    label "ma&#322;&#380;onek"
  ]
  node [
    id 21
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 22
    label "ch&#322;op"
  ]
  node [
    id 23
    label "cz&#322;owiek"
  ]
  node [
    id 24
    label "pan_m&#322;ody"
  ]
  node [
    id 25
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 26
    label "&#347;lubny"
  ]
  node [
    id 27
    label "pan_domu"
  ]
  node [
    id 28
    label "pan_i_w&#322;adca"
  ]
  node [
    id 29
    label "stary"
  ]
  node [
    id 30
    label "pole_golfowe"
  ]
  node [
    id 31
    label "obszar"
  ]
  node [
    id 32
    label "p&#243;&#322;noc"
  ]
  node [
    id 33
    label "Kosowo"
  ]
  node [
    id 34
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 35
    label "Zab&#322;ocie"
  ]
  node [
    id 36
    label "zach&#243;d"
  ]
  node [
    id 37
    label "po&#322;udnie"
  ]
  node [
    id 38
    label "Pow&#261;zki"
  ]
  node [
    id 39
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 40
    label "Piotrowo"
  ]
  node [
    id 41
    label "Olszanica"
  ]
  node [
    id 42
    label "zbi&#243;r"
  ]
  node [
    id 43
    label "holarktyka"
  ]
  node [
    id 44
    label "Ruda_Pabianicka"
  ]
  node [
    id 45
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 46
    label "Ludwin&#243;w"
  ]
  node [
    id 47
    label "Arktyka"
  ]
  node [
    id 48
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 49
    label "Zabu&#380;e"
  ]
  node [
    id 50
    label "miejsce"
  ]
  node [
    id 51
    label "antroposfera"
  ]
  node [
    id 52
    label "terytorium"
  ]
  node [
    id 53
    label "Neogea"
  ]
  node [
    id 54
    label "Syberia_Zachodnia"
  ]
  node [
    id 55
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 56
    label "zakres"
  ]
  node [
    id 57
    label "pas_planetoid"
  ]
  node [
    id 58
    label "Syberia_Wschodnia"
  ]
  node [
    id 59
    label "Antarktyka"
  ]
  node [
    id 60
    label "Rakowice"
  ]
  node [
    id 61
    label "akrecja"
  ]
  node [
    id 62
    label "wymiar"
  ]
  node [
    id 63
    label "&#321;&#281;g"
  ]
  node [
    id 64
    label "Kresy_Zachodnie"
  ]
  node [
    id 65
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 66
    label "przestrze&#324;"
  ]
  node [
    id 67
    label "wsch&#243;d"
  ]
  node [
    id 68
    label "Notogea"
  ]
  node [
    id 69
    label "&#380;elazowiec"
  ]
  node [
    id 70
    label "transuranowiec"
  ]
  node [
    id 71
    label "hassium"
  ]
  node [
    id 72
    label "metal"
  ]
  node [
    id 73
    label "pierwiastek"
  ]
  node [
    id 74
    label "uranowiec"
  ]
  node [
    id 75
    label "chronometra&#380;ysta"
  ]
  node [
    id 76
    label "odlot"
  ]
  node [
    id 77
    label "l&#261;dowanie"
  ]
  node [
    id 78
    label "start"
  ]
  node [
    id 79
    label "podr&#243;&#380;"
  ]
  node [
    id 80
    label "ruch"
  ]
  node [
    id 81
    label "ci&#261;g"
  ]
  node [
    id 82
    label "flight"
  ]
  node [
    id 83
    label "ekskursja"
  ]
  node [
    id 84
    label "bezsilnikowy"
  ]
  node [
    id 85
    label "ekwipunek"
  ]
  node [
    id 86
    label "journey"
  ]
  node [
    id 87
    label "zbior&#243;wka"
  ]
  node [
    id 88
    label "rajza"
  ]
  node [
    id 89
    label "zmiana"
  ]
  node [
    id 90
    label "turystyka"
  ]
  node [
    id 91
    label "mechanika"
  ]
  node [
    id 92
    label "utrzymywanie"
  ]
  node [
    id 93
    label "move"
  ]
  node [
    id 94
    label "poruszenie"
  ]
  node [
    id 95
    label "movement"
  ]
  node [
    id 96
    label "myk"
  ]
  node [
    id 97
    label "utrzyma&#263;"
  ]
  node [
    id 98
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 99
    label "zjawisko"
  ]
  node [
    id 100
    label "utrzymanie"
  ]
  node [
    id 101
    label "travel"
  ]
  node [
    id 102
    label "kanciasty"
  ]
  node [
    id 103
    label "commercial_enterprise"
  ]
  node [
    id 104
    label "model"
  ]
  node [
    id 105
    label "strumie&#324;"
  ]
  node [
    id 106
    label "proces"
  ]
  node [
    id 107
    label "aktywno&#347;&#263;"
  ]
  node [
    id 108
    label "kr&#243;tki"
  ]
  node [
    id 109
    label "taktyka"
  ]
  node [
    id 110
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 111
    label "apraksja"
  ]
  node [
    id 112
    label "natural_process"
  ]
  node [
    id 113
    label "utrzymywa&#263;"
  ]
  node [
    id 114
    label "d&#322;ugi"
  ]
  node [
    id 115
    label "wydarzenie"
  ]
  node [
    id 116
    label "dyssypacja_energii"
  ]
  node [
    id 117
    label "tumult"
  ]
  node [
    id 118
    label "stopek"
  ]
  node [
    id 119
    label "czynno&#347;&#263;"
  ]
  node [
    id 120
    label "manewr"
  ]
  node [
    id 121
    label "lokomocja"
  ]
  node [
    id 122
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 123
    label "komunikacja"
  ]
  node [
    id 124
    label "drift"
  ]
  node [
    id 125
    label "descent"
  ]
  node [
    id 126
    label "trafienie"
  ]
  node [
    id 127
    label "trafianie"
  ]
  node [
    id 128
    label "przybycie"
  ]
  node [
    id 129
    label "radzenie_sobie"
  ]
  node [
    id 130
    label "poradzenie_sobie"
  ]
  node [
    id 131
    label "dobijanie"
  ]
  node [
    id 132
    label "skok"
  ]
  node [
    id 133
    label "podr&#243;&#380;owanie"
  ]
  node [
    id 134
    label "lecenie"
  ]
  node [
    id 135
    label "przybywanie"
  ]
  node [
    id 136
    label "dobicie"
  ]
  node [
    id 137
    label "grogginess"
  ]
  node [
    id 138
    label "jazda"
  ]
  node [
    id 139
    label "odurzenie"
  ]
  node [
    id 140
    label "zamroczenie"
  ]
  node [
    id 141
    label "odkrycie"
  ]
  node [
    id 142
    label "rozpocz&#281;cie"
  ]
  node [
    id 143
    label "uczestnictwo"
  ]
  node [
    id 144
    label "okno_startowe"
  ]
  node [
    id 145
    label "pocz&#261;tek"
  ]
  node [
    id 146
    label "blok_startowy"
  ]
  node [
    id 147
    label "wy&#347;cig"
  ]
  node [
    id 148
    label "ekspedytor"
  ]
  node [
    id 149
    label "kontroler"
  ]
  node [
    id 150
    label "pr&#261;d"
  ]
  node [
    id 151
    label "przebieg"
  ]
  node [
    id 152
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 153
    label "k&#322;us"
  ]
  node [
    id 154
    label "si&#322;a"
  ]
  node [
    id 155
    label "cable"
  ]
  node [
    id 156
    label "lina"
  ]
  node [
    id 157
    label "way"
  ]
  node [
    id 158
    label "stan"
  ]
  node [
    id 159
    label "ch&#243;d"
  ]
  node [
    id 160
    label "current"
  ]
  node [
    id 161
    label "trasa"
  ]
  node [
    id 162
    label "progression"
  ]
  node [
    id 163
    label "rz&#261;d"
  ]
  node [
    id 164
    label "Just"
  ]
  node [
    id 165
    label "after"
  ]
  node [
    id 166
    label "reunification"
  ]
  node [
    id 167
    label "of"
  ]
  node [
    id 168
    label "german"
  ]
  node [
    id 169
    label "in"
  ]
  node [
    id 170
    label "1990"
  ]
  node [
    id 171
    label "Anda"
  ]
  node [
    id 172
    label "Leipzig"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 164
    target 165
  ]
  edge [
    source 164
    target 166
  ]
  edge [
    source 164
    target 167
  ]
  edge [
    source 164
    target 168
  ]
  edge [
    source 164
    target 169
  ]
  edge [
    source 164
    target 170
  ]
  edge [
    source 164
    target 171
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 165
    target 167
  ]
  edge [
    source 165
    target 168
  ]
  edge [
    source 165
    target 169
  ]
  edge [
    source 165
    target 170
  ]
  edge [
    source 165
    target 171
  ]
  edge [
    source 166
    target 167
  ]
  edge [
    source 166
    target 168
  ]
  edge [
    source 166
    target 169
  ]
  edge [
    source 166
    target 170
  ]
  edge [
    source 166
    target 171
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 167
    target 169
  ]
  edge [
    source 167
    target 170
  ]
  edge [
    source 167
    target 171
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 168
    target 170
  ]
  edge [
    source 168
    target 171
  ]
  edge [
    source 169
    target 170
  ]
  edge [
    source 169
    target 171
  ]
  edge [
    source 170
    target 171
  ]
  edge [
    source 171
    target 172
  ]
]
