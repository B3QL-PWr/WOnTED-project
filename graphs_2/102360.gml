graph [
  node [
    id 0
    label "rusza"
    origin "text"
  ]
  node [
    id 1
    label "wypowied&#378;"
    origin "text"
  ]
  node [
    id 2
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 3
    label "organizacja"
    origin "text"
  ]
  node [
    id 4
    label "obro&#324;ca"
    origin "text"
  ]
  node [
    id 5
    label "prawy"
    origin "text"
  ]
  node [
    id 6
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 7
    label "pojawia&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "stwierdzenie"
    origin "text"
  ]
  node [
    id 10
    label "jakoby"
    origin "text"
  ]
  node [
    id 11
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 12
    label "rozpoczyna&#263;"
    origin "text"
  ]
  node [
    id 13
    label "igrzyska"
    origin "text"
  ]
  node [
    id 14
    label "pekin"
    origin "text"
  ]
  node [
    id 15
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 16
    label "jaskrawy"
    origin "text"
  ]
  node [
    id 17
    label "zaprzeczenie"
    origin "text"
  ]
  node [
    id 18
    label "idea"
    origin "text"
  ]
  node [
    id 19
    label "olimpijski"
    origin "text"
  ]
  node [
    id 20
    label "mie&#263;by"
    origin "text"
  ]
  node [
    id 21
    label "promowa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "uczciwy"
    origin "text"
  ]
  node [
    id 23
    label "rywalizacja"
    origin "text"
  ]
  node [
    id 24
    label "oraz"
    origin "text"
  ]
  node [
    id 25
    label "wsp&#243;&#322;istnienie"
    origin "text"
  ]
  node [
    id 26
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 27
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 28
    label "pos&#322;uchanie"
  ]
  node [
    id 29
    label "s&#261;d"
  ]
  node [
    id 30
    label "sparafrazowanie"
  ]
  node [
    id 31
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 32
    label "strawestowa&#263;"
  ]
  node [
    id 33
    label "sparafrazowa&#263;"
  ]
  node [
    id 34
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 35
    label "trawestowa&#263;"
  ]
  node [
    id 36
    label "sformu&#322;owanie"
  ]
  node [
    id 37
    label "parafrazowanie"
  ]
  node [
    id 38
    label "ozdobnik"
  ]
  node [
    id 39
    label "delimitacja"
  ]
  node [
    id 40
    label "parafrazowa&#263;"
  ]
  node [
    id 41
    label "stylizacja"
  ]
  node [
    id 42
    label "komunikat"
  ]
  node [
    id 43
    label "trawestowanie"
  ]
  node [
    id 44
    label "strawestowanie"
  ]
  node [
    id 45
    label "rezultat"
  ]
  node [
    id 46
    label "dzia&#322;anie"
  ]
  node [
    id 47
    label "typ"
  ]
  node [
    id 48
    label "event"
  ]
  node [
    id 49
    label "przyczyna"
  ]
  node [
    id 50
    label "communication"
  ]
  node [
    id 51
    label "kreacjonista"
  ]
  node [
    id 52
    label "roi&#263;_si&#281;"
  ]
  node [
    id 53
    label "wytw&#243;r"
  ]
  node [
    id 54
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 55
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 56
    label "wygl&#261;d"
  ]
  node [
    id 57
    label "na&#347;ladownictwo"
  ]
  node [
    id 58
    label "styl"
  ]
  node [
    id 59
    label "stylization"
  ]
  node [
    id 60
    label "otoczka"
  ]
  node [
    id 61
    label "modyfikacja"
  ]
  node [
    id 62
    label "imitacja"
  ]
  node [
    id 63
    label "zestawienie"
  ]
  node [
    id 64
    label "szafiarka"
  ]
  node [
    id 65
    label "wording"
  ]
  node [
    id 66
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 67
    label "statement"
  ]
  node [
    id 68
    label "zapisanie"
  ]
  node [
    id 69
    label "rzucenie"
  ]
  node [
    id 70
    label "poinformowanie"
  ]
  node [
    id 71
    label "przedmiot"
  ]
  node [
    id 72
    label "dekor"
  ]
  node [
    id 73
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 74
    label "okre&#347;lenie"
  ]
  node [
    id 75
    label "ornamentyka"
  ]
  node [
    id 76
    label "ilustracja"
  ]
  node [
    id 77
    label "d&#378;wi&#281;k"
  ]
  node [
    id 78
    label "wyra&#380;enie"
  ]
  node [
    id 79
    label "dekoracja"
  ]
  node [
    id 80
    label "boundary_line"
  ]
  node [
    id 81
    label "podzia&#322;"
  ]
  node [
    id 82
    label "satyra"
  ]
  node [
    id 83
    label "parodiowanie"
  ]
  node [
    id 84
    label "rozwlec_si&#281;"
  ]
  node [
    id 85
    label "rozwlekanie_si&#281;"
  ]
  node [
    id 86
    label "rozwleczenie_si&#281;"
  ]
  node [
    id 87
    label "zmodyfikowa&#263;"
  ]
  node [
    id 88
    label "powt&#243;rzy&#263;"
  ]
  node [
    id 89
    label "rozwleka&#263;_si&#281;"
  ]
  node [
    id 90
    label "przetworzy&#263;"
  ]
  node [
    id 91
    label "powtarza&#263;"
  ]
  node [
    id 92
    label "modyfikowa&#263;"
  ]
  node [
    id 93
    label "paraphrase"
  ]
  node [
    id 94
    label "przetwarza&#263;"
  ]
  node [
    id 95
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 96
    label "spotkanie"
  ]
  node [
    id 97
    label "wys&#322;uchanie"
  ]
  node [
    id 98
    label "porobienie"
  ]
  node [
    id 99
    label "audience"
  ]
  node [
    id 100
    label "w&#322;&#261;czenie"
  ]
  node [
    id 101
    label "zrobienie"
  ]
  node [
    id 102
    label "zesp&#243;&#322;"
  ]
  node [
    id 103
    label "podejrzany"
  ]
  node [
    id 104
    label "s&#261;downictwo"
  ]
  node [
    id 105
    label "system"
  ]
  node [
    id 106
    label "biuro"
  ]
  node [
    id 107
    label "court"
  ]
  node [
    id 108
    label "forum"
  ]
  node [
    id 109
    label "bronienie"
  ]
  node [
    id 110
    label "urz&#261;d"
  ]
  node [
    id 111
    label "wydarzenie"
  ]
  node [
    id 112
    label "oskar&#380;yciel"
  ]
  node [
    id 113
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 114
    label "skazany"
  ]
  node [
    id 115
    label "post&#281;powanie"
  ]
  node [
    id 116
    label "broni&#263;"
  ]
  node [
    id 117
    label "my&#347;l"
  ]
  node [
    id 118
    label "pods&#261;dny"
  ]
  node [
    id 119
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 120
    label "obrona"
  ]
  node [
    id 121
    label "instytucja"
  ]
  node [
    id 122
    label "antylogizm"
  ]
  node [
    id 123
    label "konektyw"
  ]
  node [
    id 124
    label "&#347;wiadek"
  ]
  node [
    id 125
    label "procesowicz"
  ]
  node [
    id 126
    label "strona"
  ]
  node [
    id 127
    label "return"
  ]
  node [
    id 128
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 129
    label "sta&#263;_si&#281;"
  ]
  node [
    id 130
    label "zrobi&#263;"
  ]
  node [
    id 131
    label "porobi&#263;"
  ]
  node [
    id 132
    label "sparodiowanie"
  ]
  node [
    id 133
    label "parodiowa&#263;"
  ]
  node [
    id 134
    label "farce"
  ]
  node [
    id 135
    label "przetworzenie"
  ]
  node [
    id 136
    label "powt&#243;rzenie"
  ]
  node [
    id 137
    label "sparodiowa&#263;"
  ]
  node [
    id 138
    label "powtarzanie"
  ]
  node [
    id 139
    label "przetwarzanie"
  ]
  node [
    id 140
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 141
    label "cz&#322;onek"
  ]
  node [
    id 142
    label "przyk&#322;ad"
  ]
  node [
    id 143
    label "substytuowa&#263;"
  ]
  node [
    id 144
    label "substytuowanie"
  ]
  node [
    id 145
    label "zast&#281;pca"
  ]
  node [
    id 146
    label "ludzko&#347;&#263;"
  ]
  node [
    id 147
    label "asymilowanie"
  ]
  node [
    id 148
    label "wapniak"
  ]
  node [
    id 149
    label "asymilowa&#263;"
  ]
  node [
    id 150
    label "os&#322;abia&#263;"
  ]
  node [
    id 151
    label "posta&#263;"
  ]
  node [
    id 152
    label "hominid"
  ]
  node [
    id 153
    label "podw&#322;adny"
  ]
  node [
    id 154
    label "os&#322;abianie"
  ]
  node [
    id 155
    label "g&#322;owa"
  ]
  node [
    id 156
    label "figura"
  ]
  node [
    id 157
    label "portrecista"
  ]
  node [
    id 158
    label "dwun&#243;g"
  ]
  node [
    id 159
    label "profanum"
  ]
  node [
    id 160
    label "mikrokosmos"
  ]
  node [
    id 161
    label "nasada"
  ]
  node [
    id 162
    label "duch"
  ]
  node [
    id 163
    label "antropochoria"
  ]
  node [
    id 164
    label "osoba"
  ]
  node [
    id 165
    label "wz&#243;r"
  ]
  node [
    id 166
    label "senior"
  ]
  node [
    id 167
    label "oddzia&#322;ywanie"
  ]
  node [
    id 168
    label "Adam"
  ]
  node [
    id 169
    label "homo_sapiens"
  ]
  node [
    id 170
    label "polifag"
  ]
  node [
    id 171
    label "podmiot"
  ]
  node [
    id 172
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 173
    label "organ"
  ]
  node [
    id 174
    label "ptaszek"
  ]
  node [
    id 175
    label "element_anatomiczny"
  ]
  node [
    id 176
    label "cia&#322;o"
  ]
  node [
    id 177
    label "przyrodzenie"
  ]
  node [
    id 178
    label "fiut"
  ]
  node [
    id 179
    label "shaft"
  ]
  node [
    id 180
    label "wchodzenie"
  ]
  node [
    id 181
    label "grupa"
  ]
  node [
    id 182
    label "wej&#347;cie"
  ]
  node [
    id 183
    label "wskaza&#263;"
  ]
  node [
    id 184
    label "podstawi&#263;"
  ]
  node [
    id 185
    label "pe&#322;nomocnik"
  ]
  node [
    id 186
    label "wskazywa&#263;"
  ]
  node [
    id 187
    label "zast&#261;pi&#263;"
  ]
  node [
    id 188
    label "zast&#281;powa&#263;"
  ]
  node [
    id 189
    label "podstawia&#263;"
  ]
  node [
    id 190
    label "protezowa&#263;"
  ]
  node [
    id 191
    label "wskazywanie"
  ]
  node [
    id 192
    label "podstawienie"
  ]
  node [
    id 193
    label "wskazanie"
  ]
  node [
    id 194
    label "podstawianie"
  ]
  node [
    id 195
    label "fakt"
  ]
  node [
    id 196
    label "czyn"
  ]
  node [
    id 197
    label "jednostka_organizacyjna"
  ]
  node [
    id 198
    label "struktura"
  ]
  node [
    id 199
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 200
    label "TOPR"
  ]
  node [
    id 201
    label "endecki"
  ]
  node [
    id 202
    label "przedstawicielstwo"
  ]
  node [
    id 203
    label "od&#322;am"
  ]
  node [
    id 204
    label "Cepelia"
  ]
  node [
    id 205
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 206
    label "ZBoWiD"
  ]
  node [
    id 207
    label "organization"
  ]
  node [
    id 208
    label "centrala"
  ]
  node [
    id 209
    label "GOPR"
  ]
  node [
    id 210
    label "ZOMO"
  ]
  node [
    id 211
    label "ZMP"
  ]
  node [
    id 212
    label "komitet_koordynacyjny"
  ]
  node [
    id 213
    label "przybud&#243;wka"
  ]
  node [
    id 214
    label "boj&#243;wka"
  ]
  node [
    id 215
    label "mechanika"
  ]
  node [
    id 216
    label "o&#347;"
  ]
  node [
    id 217
    label "usenet"
  ]
  node [
    id 218
    label "rozprz&#261;c"
  ]
  node [
    id 219
    label "zachowanie"
  ]
  node [
    id 220
    label "cybernetyk"
  ]
  node [
    id 221
    label "podsystem"
  ]
  node [
    id 222
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 223
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 224
    label "sk&#322;ad"
  ]
  node [
    id 225
    label "systemat"
  ]
  node [
    id 226
    label "cecha"
  ]
  node [
    id 227
    label "konstrukcja"
  ]
  node [
    id 228
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 229
    label "konstelacja"
  ]
  node [
    id 230
    label "Mazowsze"
  ]
  node [
    id 231
    label "odm&#322;adzanie"
  ]
  node [
    id 232
    label "&#346;wietliki"
  ]
  node [
    id 233
    label "zbi&#243;r"
  ]
  node [
    id 234
    label "whole"
  ]
  node [
    id 235
    label "skupienie"
  ]
  node [
    id 236
    label "The_Beatles"
  ]
  node [
    id 237
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 238
    label "odm&#322;adza&#263;"
  ]
  node [
    id 239
    label "zabudowania"
  ]
  node [
    id 240
    label "group"
  ]
  node [
    id 241
    label "zespolik"
  ]
  node [
    id 242
    label "schorzenie"
  ]
  node [
    id 243
    label "ro&#347;lina"
  ]
  node [
    id 244
    label "Depeche_Mode"
  ]
  node [
    id 245
    label "batch"
  ]
  node [
    id 246
    label "odm&#322;odzenie"
  ]
  node [
    id 247
    label "ajencja"
  ]
  node [
    id 248
    label "siedziba"
  ]
  node [
    id 249
    label "agencja"
  ]
  node [
    id 250
    label "bank"
  ]
  node [
    id 251
    label "filia"
  ]
  node [
    id 252
    label "kawa&#322;"
  ]
  node [
    id 253
    label "bry&#322;a"
  ]
  node [
    id 254
    label "fragment"
  ]
  node [
    id 255
    label "struktura_geologiczna"
  ]
  node [
    id 256
    label "dzia&#322;"
  ]
  node [
    id 257
    label "section"
  ]
  node [
    id 258
    label "budynek"
  ]
  node [
    id 259
    label "b&#281;ben_wielki"
  ]
  node [
    id 260
    label "Bruksela"
  ]
  node [
    id 261
    label "administration"
  ]
  node [
    id 262
    label "miejsce"
  ]
  node [
    id 263
    label "zarz&#261;d"
  ]
  node [
    id 264
    label "stopa"
  ]
  node [
    id 265
    label "o&#347;rodek"
  ]
  node [
    id 266
    label "urz&#261;dzenie"
  ]
  node [
    id 267
    label "w&#322;adza"
  ]
  node [
    id 268
    label "ratownictwo"
  ]
  node [
    id 269
    label "milicja_obywatelska"
  ]
  node [
    id 270
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 271
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 272
    label "byt"
  ]
  node [
    id 273
    label "osobowo&#347;&#263;"
  ]
  node [
    id 274
    label "prawo"
  ]
  node [
    id 275
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 276
    label "nauka_prawa"
  ]
  node [
    id 277
    label "rzecznik"
  ]
  node [
    id 278
    label "tarcza"
  ]
  node [
    id 279
    label "prawnik"
  ]
  node [
    id 280
    label "zawodnik"
  ]
  node [
    id 281
    label "palestrant"
  ]
  node [
    id 282
    label "palestra"
  ]
  node [
    id 283
    label "gracz"
  ]
  node [
    id 284
    label "papuga"
  ]
  node [
    id 285
    label "obroniciel"
  ]
  node [
    id 286
    label "przyjaciel"
  ]
  node [
    id 287
    label "doradca"
  ]
  node [
    id 288
    label "kartka"
  ]
  node [
    id 289
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 290
    label "logowanie"
  ]
  node [
    id 291
    label "plik"
  ]
  node [
    id 292
    label "adres_internetowy"
  ]
  node [
    id 293
    label "linia"
  ]
  node [
    id 294
    label "serwis_internetowy"
  ]
  node [
    id 295
    label "bok"
  ]
  node [
    id 296
    label "skr&#281;canie"
  ]
  node [
    id 297
    label "skr&#281;ca&#263;"
  ]
  node [
    id 298
    label "orientowanie"
  ]
  node [
    id 299
    label "skr&#281;ci&#263;"
  ]
  node [
    id 300
    label "uj&#281;cie"
  ]
  node [
    id 301
    label "zorientowanie"
  ]
  node [
    id 302
    label "ty&#322;"
  ]
  node [
    id 303
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 304
    label "layout"
  ]
  node [
    id 305
    label "obiekt"
  ]
  node [
    id 306
    label "zorientowa&#263;"
  ]
  node [
    id 307
    label "pagina"
  ]
  node [
    id 308
    label "g&#243;ra"
  ]
  node [
    id 309
    label "orientowa&#263;"
  ]
  node [
    id 310
    label "voice"
  ]
  node [
    id 311
    label "orientacja"
  ]
  node [
    id 312
    label "prz&#243;d"
  ]
  node [
    id 313
    label "internet"
  ]
  node [
    id 314
    label "powierzchnia"
  ]
  node [
    id 315
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 316
    label "forma"
  ]
  node [
    id 317
    label "skr&#281;cenie"
  ]
  node [
    id 318
    label "prawnicy"
  ]
  node [
    id 319
    label "Machiavelli"
  ]
  node [
    id 320
    label "jurysta"
  ]
  node [
    id 321
    label "specjalista"
  ]
  node [
    id 322
    label "aplikant"
  ]
  node [
    id 323
    label "student"
  ]
  node [
    id 324
    label "zi&#243;&#322;ko"
  ]
  node [
    id 325
    label "czo&#322;&#243;wka"
  ]
  node [
    id 326
    label "uczestnik"
  ]
  node [
    id 327
    label "lista_startowa"
  ]
  node [
    id 328
    label "sportowiec"
  ]
  node [
    id 329
    label "orygina&#322;"
  ]
  node [
    id 330
    label "facet"
  ]
  node [
    id 331
    label "zwierz&#281;"
  ]
  node [
    id 332
    label "bohater"
  ]
  node [
    id 333
    label "spryciarz"
  ]
  node [
    id 334
    label "rozdawa&#263;_karty"
  ]
  node [
    id 335
    label "naszywka"
  ]
  node [
    id 336
    label "kszta&#322;t"
  ]
  node [
    id 337
    label "wskaz&#243;wka"
  ]
  node [
    id 338
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 339
    label "bro&#324;_ochronna"
  ]
  node [
    id 340
    label "odznaka"
  ]
  node [
    id 341
    label "bro&#324;"
  ]
  node [
    id 342
    label "denture"
  ]
  node [
    id 343
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 344
    label "telefon"
  ]
  node [
    id 345
    label "or&#281;&#380;"
  ]
  node [
    id 346
    label "ochrona"
  ]
  node [
    id 347
    label "target"
  ]
  node [
    id 348
    label "cel"
  ]
  node [
    id 349
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 350
    label "maszyna"
  ]
  node [
    id 351
    label "obszar"
  ]
  node [
    id 352
    label "ucze&#324;"
  ]
  node [
    id 353
    label "tablica"
  ]
  node [
    id 354
    label "gadu&#322;a"
  ]
  node [
    id 355
    label "adwokat"
  ]
  node [
    id 356
    label "ptak"
  ]
  node [
    id 357
    label "ptak_egzotyczny"
  ]
  node [
    id 358
    label "na&#347;ladowca"
  ]
  node [
    id 359
    label "parrot"
  ]
  node [
    id 360
    label "papugowe"
  ]
  node [
    id 361
    label "egzamin"
  ]
  node [
    id 362
    label "walka"
  ]
  node [
    id 363
    label "liga"
  ]
  node [
    id 364
    label "poj&#281;cie"
  ]
  node [
    id 365
    label "protection"
  ]
  node [
    id 366
    label "poparcie"
  ]
  node [
    id 367
    label "mecz"
  ]
  node [
    id 368
    label "reakcja"
  ]
  node [
    id 369
    label "defense"
  ]
  node [
    id 370
    label "auspices"
  ]
  node [
    id 371
    label "gra"
  ]
  node [
    id 372
    label "sp&#243;r"
  ]
  node [
    id 373
    label "wojsko"
  ]
  node [
    id 374
    label "manewr"
  ]
  node [
    id 375
    label "defensive_structure"
  ]
  node [
    id 376
    label "guard_duty"
  ]
  node [
    id 377
    label "Grecja"
  ]
  node [
    id 378
    label "regent"
  ]
  node [
    id 379
    label "legal_profession"
  ]
  node [
    id 380
    label "chancellery"
  ]
  node [
    id 381
    label "w_prawo"
  ]
  node [
    id 382
    label "s&#322;uszny"
  ]
  node [
    id 383
    label "naturalny"
  ]
  node [
    id 384
    label "chwalebny"
  ]
  node [
    id 385
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 386
    label "zgodnie_z_prawem"
  ]
  node [
    id 387
    label "zacny"
  ]
  node [
    id 388
    label "moralny"
  ]
  node [
    id 389
    label "prawicowy"
  ]
  node [
    id 390
    label "na_prawo"
  ]
  node [
    id 391
    label "cnotliwy"
  ]
  node [
    id 392
    label "legalny"
  ]
  node [
    id 393
    label "z_prawa"
  ]
  node [
    id 394
    label "gajny"
  ]
  node [
    id 395
    label "legalnie"
  ]
  node [
    id 396
    label "s&#322;usznie"
  ]
  node [
    id 397
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 398
    label "zasadny"
  ]
  node [
    id 399
    label "nale&#380;yty"
  ]
  node [
    id 400
    label "prawdziwy"
  ]
  node [
    id 401
    label "solidny"
  ]
  node [
    id 402
    label "moralnie"
  ]
  node [
    id 403
    label "warto&#347;ciowy"
  ]
  node [
    id 404
    label "etycznie"
  ]
  node [
    id 405
    label "dobry"
  ]
  node [
    id 406
    label "pochwalny"
  ]
  node [
    id 407
    label "wspania&#322;y"
  ]
  node [
    id 408
    label "szlachetny"
  ]
  node [
    id 409
    label "powa&#380;ny"
  ]
  node [
    id 410
    label "chwalebnie"
  ]
  node [
    id 411
    label "zacnie"
  ]
  node [
    id 412
    label "skromny"
  ]
  node [
    id 413
    label "niewinny"
  ]
  node [
    id 414
    label "cny"
  ]
  node [
    id 415
    label "cnotliwie"
  ]
  node [
    id 416
    label "prostolinijny"
  ]
  node [
    id 417
    label "szczery"
  ]
  node [
    id 418
    label "zrozumia&#322;y"
  ]
  node [
    id 419
    label "immanentny"
  ]
  node [
    id 420
    label "zwyczajny"
  ]
  node [
    id 421
    label "bezsporny"
  ]
  node [
    id 422
    label "organicznie"
  ]
  node [
    id 423
    label "pierwotny"
  ]
  node [
    id 424
    label "neutralny"
  ]
  node [
    id 425
    label "normalny"
  ]
  node [
    id 426
    label "rzeczywisty"
  ]
  node [
    id 427
    label "naturalnie"
  ]
  node [
    id 428
    label "prawicowo"
  ]
  node [
    id 429
    label "prawoskr&#281;tny"
  ]
  node [
    id 430
    label "konserwatywny"
  ]
  node [
    id 431
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 432
    label "cz&#322;owiekowate"
  ]
  node [
    id 433
    label "konsument"
  ]
  node [
    id 434
    label "istota_&#380;ywa"
  ]
  node [
    id 435
    label "pracownik"
  ]
  node [
    id 436
    label "Chocho&#322;"
  ]
  node [
    id 437
    label "Herkules_Poirot"
  ]
  node [
    id 438
    label "Edyp"
  ]
  node [
    id 439
    label "parali&#380;owa&#263;"
  ]
  node [
    id 440
    label "Harry_Potter"
  ]
  node [
    id 441
    label "Casanova"
  ]
  node [
    id 442
    label "Zgredek"
  ]
  node [
    id 443
    label "Gargantua"
  ]
  node [
    id 444
    label "Winnetou"
  ]
  node [
    id 445
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 446
    label "Dulcynea"
  ]
  node [
    id 447
    label "kategoria_gramatyczna"
  ]
  node [
    id 448
    label "person"
  ]
  node [
    id 449
    label "Plastu&#347;"
  ]
  node [
    id 450
    label "Quasimodo"
  ]
  node [
    id 451
    label "Sherlock_Holmes"
  ]
  node [
    id 452
    label "Faust"
  ]
  node [
    id 453
    label "Wallenrod"
  ]
  node [
    id 454
    label "Dwukwiat"
  ]
  node [
    id 455
    label "Don_Juan"
  ]
  node [
    id 456
    label "koniugacja"
  ]
  node [
    id 457
    label "Don_Kiszot"
  ]
  node [
    id 458
    label "Hamlet"
  ]
  node [
    id 459
    label "Werter"
  ]
  node [
    id 460
    label "istota"
  ]
  node [
    id 461
    label "Szwejk"
  ]
  node [
    id 462
    label "doros&#322;y"
  ]
  node [
    id 463
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 464
    label "jajko"
  ]
  node [
    id 465
    label "rodzic"
  ]
  node [
    id 466
    label "wapniaki"
  ]
  node [
    id 467
    label "zwierzchnik"
  ]
  node [
    id 468
    label "feuda&#322;"
  ]
  node [
    id 469
    label "starzec"
  ]
  node [
    id 470
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 471
    label "komendancja"
  ]
  node [
    id 472
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 473
    label "de-escalation"
  ]
  node [
    id 474
    label "powodowanie"
  ]
  node [
    id 475
    label "os&#322;abienie"
  ]
  node [
    id 476
    label "kondycja_fizyczna"
  ]
  node [
    id 477
    label "os&#322;abi&#263;"
  ]
  node [
    id 478
    label "debilitation"
  ]
  node [
    id 479
    label "zdrowie"
  ]
  node [
    id 480
    label "zmniejszanie"
  ]
  node [
    id 481
    label "s&#322;abszy"
  ]
  node [
    id 482
    label "pogarszanie"
  ]
  node [
    id 483
    label "suppress"
  ]
  node [
    id 484
    label "robi&#263;"
  ]
  node [
    id 485
    label "powodowa&#263;"
  ]
  node [
    id 486
    label "zmniejsza&#263;"
  ]
  node [
    id 487
    label "bate"
  ]
  node [
    id 488
    label "asymilowanie_si&#281;"
  ]
  node [
    id 489
    label "absorption"
  ]
  node [
    id 490
    label "pobieranie"
  ]
  node [
    id 491
    label "czerpanie"
  ]
  node [
    id 492
    label "acquisition"
  ]
  node [
    id 493
    label "zmienianie"
  ]
  node [
    id 494
    label "organizm"
  ]
  node [
    id 495
    label "assimilation"
  ]
  node [
    id 496
    label "upodabnianie"
  ]
  node [
    id 497
    label "g&#322;oska"
  ]
  node [
    id 498
    label "kultura"
  ]
  node [
    id 499
    label "podobny"
  ]
  node [
    id 500
    label "fonetyka"
  ]
  node [
    id 501
    label "assimilate"
  ]
  node [
    id 502
    label "dostosowywa&#263;"
  ]
  node [
    id 503
    label "dostosowa&#263;"
  ]
  node [
    id 504
    label "przejmowa&#263;"
  ]
  node [
    id 505
    label "upodobni&#263;"
  ]
  node [
    id 506
    label "przej&#261;&#263;"
  ]
  node [
    id 507
    label "upodabnia&#263;"
  ]
  node [
    id 508
    label "pobiera&#263;"
  ]
  node [
    id 509
    label "pobra&#263;"
  ]
  node [
    id 510
    label "charakterystyka"
  ]
  node [
    id 511
    label "zaistnie&#263;"
  ]
  node [
    id 512
    label "Osjan"
  ]
  node [
    id 513
    label "kto&#347;"
  ]
  node [
    id 514
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 515
    label "trim"
  ]
  node [
    id 516
    label "poby&#263;"
  ]
  node [
    id 517
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 518
    label "Aspazja"
  ]
  node [
    id 519
    label "punkt_widzenia"
  ]
  node [
    id 520
    label "kompleksja"
  ]
  node [
    id 521
    label "wytrzyma&#263;"
  ]
  node [
    id 522
    label "budowa"
  ]
  node [
    id 523
    label "formacja"
  ]
  node [
    id 524
    label "pozosta&#263;"
  ]
  node [
    id 525
    label "point"
  ]
  node [
    id 526
    label "przedstawienie"
  ]
  node [
    id 527
    label "go&#347;&#263;"
  ]
  node [
    id 528
    label "zapis"
  ]
  node [
    id 529
    label "figure"
  ]
  node [
    id 530
    label "spos&#243;b"
  ]
  node [
    id 531
    label "mildew"
  ]
  node [
    id 532
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 533
    label "ideal"
  ]
  node [
    id 534
    label "rule"
  ]
  node [
    id 535
    label "ruch"
  ]
  node [
    id 536
    label "dekal"
  ]
  node [
    id 537
    label "motyw"
  ]
  node [
    id 538
    label "projekt"
  ]
  node [
    id 539
    label "pryncypa&#322;"
  ]
  node [
    id 540
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 541
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 542
    label "wiedza"
  ]
  node [
    id 543
    label "kierowa&#263;"
  ]
  node [
    id 544
    label "alkohol"
  ]
  node [
    id 545
    label "zdolno&#347;&#263;"
  ]
  node [
    id 546
    label "&#380;ycie"
  ]
  node [
    id 547
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 548
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 549
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 550
    label "sztuka"
  ]
  node [
    id 551
    label "dekiel"
  ]
  node [
    id 552
    label "&#347;ci&#281;cie"
  ]
  node [
    id 553
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 554
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 555
    label "&#347;ci&#281;gno"
  ]
  node [
    id 556
    label "noosfera"
  ]
  node [
    id 557
    label "byd&#322;o"
  ]
  node [
    id 558
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 559
    label "makrocefalia"
  ]
  node [
    id 560
    label "ucho"
  ]
  node [
    id 561
    label "m&#243;zg"
  ]
  node [
    id 562
    label "kierownictwo"
  ]
  node [
    id 563
    label "fryzura"
  ]
  node [
    id 564
    label "umys&#322;"
  ]
  node [
    id 565
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 566
    label "czaszka"
  ]
  node [
    id 567
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 568
    label "dziedzina"
  ]
  node [
    id 569
    label "hipnotyzowanie"
  ]
  node [
    id 570
    label "&#347;lad"
  ]
  node [
    id 571
    label "docieranie"
  ]
  node [
    id 572
    label "natural_process"
  ]
  node [
    id 573
    label "reakcja_chemiczna"
  ]
  node [
    id 574
    label "wdzieranie_si&#281;"
  ]
  node [
    id 575
    label "zjawisko"
  ]
  node [
    id 576
    label "act"
  ]
  node [
    id 577
    label "lobbysta"
  ]
  node [
    id 578
    label "allochoria"
  ]
  node [
    id 579
    label "fotograf"
  ]
  node [
    id 580
    label "malarz"
  ]
  node [
    id 581
    label "artysta"
  ]
  node [
    id 582
    label "p&#322;aszczyzna"
  ]
  node [
    id 583
    label "bierka_szachowa"
  ]
  node [
    id 584
    label "obiekt_matematyczny"
  ]
  node [
    id 585
    label "gestaltyzm"
  ]
  node [
    id 586
    label "obraz"
  ]
  node [
    id 587
    label "rzecz"
  ]
  node [
    id 588
    label "character"
  ]
  node [
    id 589
    label "rze&#378;ba"
  ]
  node [
    id 590
    label "stylistyka"
  ]
  node [
    id 591
    label "antycypacja"
  ]
  node [
    id 592
    label "informacja"
  ]
  node [
    id 593
    label "popis"
  ]
  node [
    id 594
    label "wiersz"
  ]
  node [
    id 595
    label "symetria"
  ]
  node [
    id 596
    label "lingwistyka_kognitywna"
  ]
  node [
    id 597
    label "karta"
  ]
  node [
    id 598
    label "shape"
  ]
  node [
    id 599
    label "podzbi&#243;r"
  ]
  node [
    id 600
    label "perspektywa"
  ]
  node [
    id 601
    label "nak&#322;adka"
  ]
  node [
    id 602
    label "li&#347;&#263;"
  ]
  node [
    id 603
    label "jama_gard&#322;owa"
  ]
  node [
    id 604
    label "rezonator"
  ]
  node [
    id 605
    label "podstawa"
  ]
  node [
    id 606
    label "base"
  ]
  node [
    id 607
    label "piek&#322;o"
  ]
  node [
    id 608
    label "human_body"
  ]
  node [
    id 609
    label "ofiarowywanie"
  ]
  node [
    id 610
    label "sfera_afektywna"
  ]
  node [
    id 611
    label "nekromancja"
  ]
  node [
    id 612
    label "Po&#347;wist"
  ]
  node [
    id 613
    label "podekscytowanie"
  ]
  node [
    id 614
    label "deformowanie"
  ]
  node [
    id 615
    label "sumienie"
  ]
  node [
    id 616
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 617
    label "deformowa&#263;"
  ]
  node [
    id 618
    label "psychika"
  ]
  node [
    id 619
    label "zjawa"
  ]
  node [
    id 620
    label "zmar&#322;y"
  ]
  node [
    id 621
    label "istota_nadprzyrodzona"
  ]
  node [
    id 622
    label "power"
  ]
  node [
    id 623
    label "entity"
  ]
  node [
    id 624
    label "ofiarowywa&#263;"
  ]
  node [
    id 625
    label "oddech"
  ]
  node [
    id 626
    label "seksualno&#347;&#263;"
  ]
  node [
    id 627
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 628
    label "si&#322;a"
  ]
  node [
    id 629
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 630
    label "ego"
  ]
  node [
    id 631
    label "ofiarowanie"
  ]
  node [
    id 632
    label "charakter"
  ]
  node [
    id 633
    label "fizjonomia"
  ]
  node [
    id 634
    label "kompleks"
  ]
  node [
    id 635
    label "zapalno&#347;&#263;"
  ]
  node [
    id 636
    label "T&#281;sknica"
  ]
  node [
    id 637
    label "ofiarowa&#263;"
  ]
  node [
    id 638
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 639
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 640
    label "passion"
  ]
  node [
    id 641
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 642
    label "atom"
  ]
  node [
    id 643
    label "odbicie"
  ]
  node [
    id 644
    label "przyroda"
  ]
  node [
    id 645
    label "Ziemia"
  ]
  node [
    id 646
    label "kosmos"
  ]
  node [
    id 647
    label "miniatura"
  ]
  node [
    id 648
    label "ustalenie"
  ]
  node [
    id 649
    label "claim"
  ]
  node [
    id 650
    label "oznajmienie"
  ]
  node [
    id 651
    label "wypowiedzenie"
  ]
  node [
    id 652
    label "manifesto"
  ]
  node [
    id 653
    label "zwiastowanie"
  ]
  node [
    id 654
    label "announcement"
  ]
  node [
    id 655
    label "apel"
  ]
  node [
    id 656
    label "Manifest_lipcowy"
  ]
  node [
    id 657
    label "decyzja"
  ]
  node [
    id 658
    label "umocnienie"
  ]
  node [
    id 659
    label "appointment"
  ]
  node [
    id 660
    label "spowodowanie"
  ]
  node [
    id 661
    label "localization"
  ]
  node [
    id 662
    label "czynno&#347;&#263;"
  ]
  node [
    id 663
    label "zdecydowanie"
  ]
  node [
    id 664
    label "doba"
  ]
  node [
    id 665
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 666
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 667
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 668
    label "teraz"
  ]
  node [
    id 669
    label "czas"
  ]
  node [
    id 670
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 671
    label "jednocze&#347;nie"
  ]
  node [
    id 672
    label "tydzie&#324;"
  ]
  node [
    id 673
    label "noc"
  ]
  node [
    id 674
    label "dzie&#324;"
  ]
  node [
    id 675
    label "godzina"
  ]
  node [
    id 676
    label "long_time"
  ]
  node [
    id 677
    label "jednostka_geologiczna"
  ]
  node [
    id 678
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 679
    label "start"
  ]
  node [
    id 680
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 681
    label "begin"
  ]
  node [
    id 682
    label "sprawowa&#263;"
  ]
  node [
    id 683
    label "prosecute"
  ]
  node [
    id 684
    label "by&#263;"
  ]
  node [
    id 685
    label "lot"
  ]
  node [
    id 686
    label "rozpocz&#281;cie"
  ]
  node [
    id 687
    label "uczestnictwo"
  ]
  node [
    id 688
    label "okno_startowe"
  ]
  node [
    id 689
    label "pocz&#261;tek"
  ]
  node [
    id 690
    label "blok_startowy"
  ]
  node [
    id 691
    label "wy&#347;cig"
  ]
  node [
    id 692
    label "festiwal"
  ]
  node [
    id 693
    label "zawody"
  ]
  node [
    id 694
    label "arena"
  ]
  node [
    id 695
    label "game"
  ]
  node [
    id 696
    label "impreza"
  ]
  node [
    id 697
    label "contest"
  ]
  node [
    id 698
    label "walczy&#263;"
  ]
  node [
    id 699
    label "champion"
  ]
  node [
    id 700
    label "walczenie"
  ]
  node [
    id 701
    label "tysi&#281;cznik"
  ]
  node [
    id 702
    label "spadochroniarstwo"
  ]
  node [
    id 703
    label "kategoria_open"
  ]
  node [
    id 704
    label "Przystanek_Woodstock"
  ]
  node [
    id 705
    label "Woodstock"
  ]
  node [
    id 706
    label "Opole"
  ]
  node [
    id 707
    label "Eurowizja"
  ]
  node [
    id 708
    label "Open'er"
  ]
  node [
    id 709
    label "Metalmania"
  ]
  node [
    id 710
    label "Brutal"
  ]
  node [
    id 711
    label "FAMA"
  ]
  node [
    id 712
    label "Interwizja"
  ]
  node [
    id 713
    label "Nowe_Horyzonty"
  ]
  node [
    id 714
    label "teren"
  ]
  node [
    id 715
    label "plac"
  ]
  node [
    id 716
    label "publiczno&#347;&#263;"
  ]
  node [
    id 717
    label "korrida"
  ]
  node [
    id 718
    label "widzownia"
  ]
  node [
    id 719
    label "dawny"
  ]
  node [
    id 720
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 721
    label "eksprezydent"
  ]
  node [
    id 722
    label "partner"
  ]
  node [
    id 723
    label "rozw&#243;d"
  ]
  node [
    id 724
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 725
    label "wcze&#347;niejszy"
  ]
  node [
    id 726
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 727
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 728
    label "przedsi&#281;biorca"
  ]
  node [
    id 729
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 730
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 731
    label "kolaborator"
  ]
  node [
    id 732
    label "prowadzi&#263;"
  ]
  node [
    id 733
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 734
    label "sp&#243;lnik"
  ]
  node [
    id 735
    label "aktor"
  ]
  node [
    id 736
    label "uczestniczenie"
  ]
  node [
    id 737
    label "przestarza&#322;y"
  ]
  node [
    id 738
    label "odleg&#322;y"
  ]
  node [
    id 739
    label "przesz&#322;y"
  ]
  node [
    id 740
    label "od_dawna"
  ]
  node [
    id 741
    label "poprzedni"
  ]
  node [
    id 742
    label "dawno"
  ]
  node [
    id 743
    label "d&#322;ugoletni"
  ]
  node [
    id 744
    label "anachroniczny"
  ]
  node [
    id 745
    label "dawniej"
  ]
  node [
    id 746
    label "niegdysiejszy"
  ]
  node [
    id 747
    label "kombatant"
  ]
  node [
    id 748
    label "stary"
  ]
  node [
    id 749
    label "wcze&#347;niej"
  ]
  node [
    id 750
    label "rozstanie"
  ]
  node [
    id 751
    label "ekspartner"
  ]
  node [
    id 752
    label "rozbita_rodzina"
  ]
  node [
    id 753
    label "uniewa&#380;nienie"
  ]
  node [
    id 754
    label "separation"
  ]
  node [
    id 755
    label "prezydent"
  ]
  node [
    id 756
    label "krzykliwy"
  ]
  node [
    id 757
    label "oczojebny"
  ]
  node [
    id 758
    label "jaskrawo"
  ]
  node [
    id 759
    label "dosadny"
  ]
  node [
    id 760
    label "nasycony"
  ]
  node [
    id 761
    label "oczojebkowy"
  ]
  node [
    id 762
    label "ostry"
  ]
  node [
    id 763
    label "ewidentny"
  ]
  node [
    id 764
    label "ra&#380;&#261;cy"
  ]
  node [
    id 765
    label "jasny"
  ]
  node [
    id 766
    label "oczywisty"
  ]
  node [
    id 767
    label "pewny"
  ]
  node [
    id 768
    label "wyra&#378;ny"
  ]
  node [
    id 769
    label "ewidentnie"
  ]
  node [
    id 770
    label "jednoznaczny"
  ]
  node [
    id 771
    label "dosadnie"
  ]
  node [
    id 772
    label "rabelaisowski"
  ]
  node [
    id 773
    label "mocny"
  ]
  node [
    id 774
    label "bezpo&#347;redni"
  ]
  node [
    id 775
    label "ekstrawagancki"
  ]
  node [
    id 776
    label "krzykliwie"
  ]
  node [
    id 777
    label "sensacyjny"
  ]
  node [
    id 778
    label "zapami&#281;ta&#322;y"
  ]
  node [
    id 779
    label "trudny"
  ]
  node [
    id 780
    label "nieneutralny"
  ]
  node [
    id 781
    label "porywczy"
  ]
  node [
    id 782
    label "dynamiczny"
  ]
  node [
    id 783
    label "nieprzyjazny"
  ]
  node [
    id 784
    label "skuteczny"
  ]
  node [
    id 785
    label "kategoryczny"
  ]
  node [
    id 786
    label "surowy"
  ]
  node [
    id 787
    label "silny"
  ]
  node [
    id 788
    label "bystro"
  ]
  node [
    id 789
    label "raptowny"
  ]
  node [
    id 790
    label "szorstki"
  ]
  node [
    id 791
    label "energiczny"
  ]
  node [
    id 792
    label "intensywny"
  ]
  node [
    id 793
    label "dramatyczny"
  ]
  node [
    id 794
    label "zdecydowany"
  ]
  node [
    id 795
    label "nieoboj&#281;tny"
  ]
  node [
    id 796
    label "widoczny"
  ]
  node [
    id 797
    label "ostrzenie"
  ]
  node [
    id 798
    label "k&#322;uj&#261;cy"
  ]
  node [
    id 799
    label "ci&#281;&#380;ki"
  ]
  node [
    id 800
    label "naostrzenie"
  ]
  node [
    id 801
    label "gryz&#261;cy"
  ]
  node [
    id 802
    label "dokuczliwy"
  ]
  node [
    id 803
    label "dotkliwy"
  ]
  node [
    id 804
    label "ostro"
  ]
  node [
    id 805
    label "za&#380;arcie"
  ]
  node [
    id 806
    label "nieobyczajny"
  ]
  node [
    id 807
    label "niebezpieczny"
  ]
  node [
    id 808
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 809
    label "podniecaj&#261;cy"
  ]
  node [
    id 810
    label "osch&#322;y"
  ]
  node [
    id 811
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 812
    label "agresywny"
  ]
  node [
    id 813
    label "gro&#378;ny"
  ]
  node [
    id 814
    label "dziki"
  ]
  node [
    id 815
    label "g&#322;&#281;boki"
  ]
  node [
    id 816
    label "&#380;ywo"
  ]
  node [
    id 817
    label "ra&#380;&#261;co"
  ]
  node [
    id 818
    label "o&#347;wietlenie"
  ]
  node [
    id 819
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 820
    label "jasno"
  ]
  node [
    id 821
    label "o&#347;wietlanie"
  ]
  node [
    id 822
    label "przytomny"
  ]
  node [
    id 823
    label "niezm&#261;cony"
  ]
  node [
    id 824
    label "bia&#322;y"
  ]
  node [
    id 825
    label "klarowny"
  ]
  node [
    id 826
    label "pogodny"
  ]
  node [
    id 827
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 828
    label "rejection"
  ]
  node [
    id 829
    label "negacja"
  ]
  node [
    id 830
    label "zdementowanie"
  ]
  node [
    id 831
    label "zdarzenie_si&#281;"
  ]
  node [
    id 832
    label "negation"
  ]
  node [
    id 833
    label "denial"
  ]
  node [
    id 834
    label "ocenienie"
  ]
  node [
    id 835
    label "zakwestionowanie"
  ]
  node [
    id 836
    label "challenge"
  ]
  node [
    id 837
    label "follow-up"
  ]
  node [
    id 838
    label "przewarto&#347;ciowanie"
  ]
  node [
    id 839
    label "appraisal"
  ]
  node [
    id 840
    label "potraktowanie"
  ]
  node [
    id 841
    label "przyznanie"
  ]
  node [
    id 842
    label "dostanie"
  ]
  node [
    id 843
    label "wywy&#380;szenie"
  ]
  node [
    id 844
    label "przewidzenie"
  ]
  node [
    id 845
    label "sprostowanie"
  ]
  node [
    id 846
    label "ideologia"
  ]
  node [
    id 847
    label "intelekt"
  ]
  node [
    id 848
    label "Kant"
  ]
  node [
    id 849
    label "p&#322;&#243;d"
  ]
  node [
    id 850
    label "pomys&#322;"
  ]
  node [
    id 851
    label "ideacja"
  ]
  node [
    id 852
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 853
    label "moczownik"
  ]
  node [
    id 854
    label "embryo"
  ]
  node [
    id 855
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 856
    label "zarodek"
  ]
  node [
    id 857
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 858
    label "latawiec"
  ]
  node [
    id 859
    label "mentalno&#347;&#263;"
  ]
  node [
    id 860
    label "superego"
  ]
  node [
    id 861
    label "znaczenie"
  ]
  node [
    id 862
    label "wn&#281;trze"
  ]
  node [
    id 863
    label "punkt"
  ]
  node [
    id 864
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 865
    label "thing"
  ]
  node [
    id 866
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 867
    label "skumanie"
  ]
  node [
    id 868
    label "teoria"
  ]
  node [
    id 869
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 870
    label "clasp"
  ]
  node [
    id 871
    label "przem&#243;wienie"
  ]
  node [
    id 872
    label "utrzymywanie"
  ]
  node [
    id 873
    label "bycie"
  ]
  node [
    id 874
    label "subsystencja"
  ]
  node [
    id 875
    label "utrzyma&#263;"
  ]
  node [
    id 876
    label "egzystencja"
  ]
  node [
    id 877
    label "wy&#380;ywienie"
  ]
  node [
    id 878
    label "ontologicznie"
  ]
  node [
    id 879
    label "utrzymanie"
  ]
  node [
    id 880
    label "potencja"
  ]
  node [
    id 881
    label "utrzymywa&#263;"
  ]
  node [
    id 882
    label "pocz&#261;tki"
  ]
  node [
    id 883
    label "ukra&#347;&#263;"
  ]
  node [
    id 884
    label "ukradzenie"
  ]
  node [
    id 885
    label "czysty_rozum"
  ]
  node [
    id 886
    label "noumenon"
  ]
  node [
    id 887
    label "filozofia"
  ]
  node [
    id 888
    label "kantysta"
  ]
  node [
    id 889
    label "fenomenologia"
  ]
  node [
    id 890
    label "political_orientation"
  ]
  node [
    id 891
    label "szko&#322;a"
  ]
  node [
    id 892
    label "nadawa&#263;"
  ]
  node [
    id 893
    label "wypromowywa&#263;"
  ]
  node [
    id 894
    label "nada&#263;"
  ]
  node [
    id 895
    label "rozpowszechnia&#263;"
  ]
  node [
    id 896
    label "zach&#281;ca&#263;"
  ]
  node [
    id 897
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 898
    label "promocja"
  ]
  node [
    id 899
    label "advance"
  ]
  node [
    id 900
    label "udzieli&#263;"
  ]
  node [
    id 901
    label "udziela&#263;"
  ]
  node [
    id 902
    label "reklama"
  ]
  node [
    id 903
    label "doprowadza&#263;"
  ]
  node [
    id 904
    label "pomaga&#263;"
  ]
  node [
    id 905
    label "pozyskiwa&#263;"
  ]
  node [
    id 906
    label "generalize"
  ]
  node [
    id 907
    label "sprawia&#263;"
  ]
  node [
    id 908
    label "rig"
  ]
  node [
    id 909
    label "message"
  ]
  node [
    id 910
    label "wykonywa&#263;"
  ]
  node [
    id 911
    label "deliver"
  ]
  node [
    id 912
    label "wzbudza&#263;"
  ]
  node [
    id 913
    label "moderate"
  ]
  node [
    id 914
    label "wprowadza&#263;"
  ]
  node [
    id 915
    label "da&#263;"
  ]
  node [
    id 916
    label "udost&#281;pni&#263;"
  ]
  node [
    id 917
    label "przyzna&#263;"
  ]
  node [
    id 918
    label "picture"
  ]
  node [
    id 919
    label "give"
  ]
  node [
    id 920
    label "odst&#261;pi&#263;"
  ]
  node [
    id 921
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 922
    label "odst&#281;powa&#263;"
  ]
  node [
    id 923
    label "dawa&#263;"
  ]
  node [
    id 924
    label "assign"
  ]
  node [
    id 925
    label "render"
  ]
  node [
    id 926
    label "accord"
  ]
  node [
    id 927
    label "zezwala&#263;"
  ]
  node [
    id 928
    label "przyznawa&#263;"
  ]
  node [
    id 929
    label "mie&#263;_miejsce"
  ]
  node [
    id 930
    label "aid"
  ]
  node [
    id 931
    label "u&#322;atwia&#263;"
  ]
  node [
    id 932
    label "concur"
  ]
  node [
    id 933
    label "sprzyja&#263;"
  ]
  node [
    id 934
    label "skutkowa&#263;"
  ]
  node [
    id 935
    label "digest"
  ]
  node [
    id 936
    label "Warszawa"
  ]
  node [
    id 937
    label "back"
  ]
  node [
    id 938
    label "za&#322;atwi&#263;"
  ]
  node [
    id 939
    label "zarekomendowa&#263;"
  ]
  node [
    id 940
    label "spowodowa&#263;"
  ]
  node [
    id 941
    label "przes&#322;a&#263;"
  ]
  node [
    id 942
    label "donie&#347;&#263;"
  ]
  node [
    id 943
    label "gada&#263;"
  ]
  node [
    id 944
    label "donosi&#263;"
  ]
  node [
    id 945
    label "rekomendowa&#263;"
  ]
  node [
    id 946
    label "za&#322;atwia&#263;"
  ]
  node [
    id 947
    label "obgadywa&#263;"
  ]
  node [
    id 948
    label "przesy&#322;a&#263;"
  ]
  node [
    id 949
    label "damka"
  ]
  node [
    id 950
    label "warcaby"
  ]
  node [
    id 951
    label "promotion"
  ]
  node [
    id 952
    label "sprzeda&#380;"
  ]
  node [
    id 953
    label "zamiana"
  ]
  node [
    id 954
    label "brief"
  ]
  node [
    id 955
    label "&#347;wiadectwo"
  ]
  node [
    id 956
    label "akcja"
  ]
  node [
    id 957
    label "bran&#380;a"
  ]
  node [
    id 958
    label "commencement"
  ]
  node [
    id 959
    label "okazja"
  ]
  node [
    id 960
    label "klasa"
  ]
  node [
    id 961
    label "graduacja"
  ]
  node [
    id 962
    label "nominacja"
  ]
  node [
    id 963
    label "szachy"
  ]
  node [
    id 964
    label "popularyzacja"
  ]
  node [
    id 965
    label "wypromowa&#263;"
  ]
  node [
    id 966
    label "gradation"
  ]
  node [
    id 967
    label "uzyska&#263;"
  ]
  node [
    id 968
    label "copywriting"
  ]
  node [
    id 969
    label "samplowanie"
  ]
  node [
    id 970
    label "tekst"
  ]
  node [
    id 971
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 972
    label "porz&#261;dnie"
  ]
  node [
    id 973
    label "uczciwie"
  ]
  node [
    id 974
    label "zgodny"
  ]
  node [
    id 975
    label "rzetelny"
  ]
  node [
    id 976
    label "solidnie"
  ]
  node [
    id 977
    label "porz&#261;dny"
  ]
  node [
    id 978
    label "niez&#322;y"
  ]
  node [
    id 979
    label "&#322;adny"
  ]
  node [
    id 980
    label "obowi&#261;zkowy"
  ]
  node [
    id 981
    label "zadowalaj&#261;cy"
  ]
  node [
    id 982
    label "nale&#380;ycie"
  ]
  node [
    id 983
    label "przystojny"
  ]
  node [
    id 984
    label "rzetelnie"
  ]
  node [
    id 985
    label "przekonuj&#261;cy"
  ]
  node [
    id 986
    label "zgodnie"
  ]
  node [
    id 987
    label "zbie&#380;ny"
  ]
  node [
    id 988
    label "spokojny"
  ]
  node [
    id 989
    label "szybki"
  ]
  node [
    id 990
    label "znacz&#261;cy"
  ]
  node [
    id 991
    label "zwarty"
  ]
  node [
    id 992
    label "efektywny"
  ]
  node [
    id 993
    label "ogrodnictwo"
  ]
  node [
    id 994
    label "pe&#322;ny"
  ]
  node [
    id 995
    label "intensywnie"
  ]
  node [
    id 996
    label "nieproporcjonalny"
  ]
  node [
    id 997
    label "specjalny"
  ]
  node [
    id 998
    label "&#380;ywny"
  ]
  node [
    id 999
    label "naprawd&#281;"
  ]
  node [
    id 1000
    label "realnie"
  ]
  node [
    id 1001
    label "m&#261;dry"
  ]
  node [
    id 1002
    label "prawdziwie"
  ]
  node [
    id 1003
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1004
    label "zasadnie"
  ]
  node [
    id 1005
    label "szczodry"
  ]
  node [
    id 1006
    label "prostoduszny"
  ]
  node [
    id 1007
    label "szczyry"
  ]
  node [
    id 1008
    label "szczerze"
  ]
  node [
    id 1009
    label "czysty"
  ]
  node [
    id 1010
    label "rz&#261;dnie"
  ]
  node [
    id 1011
    label "fair"
  ]
  node [
    id 1012
    label "dobrze"
  ]
  node [
    id 1013
    label "ch&#281;dogo"
  ]
  node [
    id 1014
    label "schludnie"
  ]
  node [
    id 1015
    label "przebiec"
  ]
  node [
    id 1016
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1017
    label "przebiegni&#281;cie"
  ]
  node [
    id 1018
    label "fabu&#322;a"
  ]
  node [
    id 1019
    label "wsp&#243;&#322;wyst&#281;powanie"
  ]
  node [
    id 1020
    label "coexistence"
  ]
  node [
    id 1021
    label "Irokezi"
  ]
  node [
    id 1022
    label "ludno&#347;&#263;"
  ]
  node [
    id 1023
    label "Syngalezi"
  ]
  node [
    id 1024
    label "Apacze"
  ]
  node [
    id 1025
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 1026
    label "t&#322;um"
  ]
  node [
    id 1027
    label "Mohikanie"
  ]
  node [
    id 1028
    label "Komancze"
  ]
  node [
    id 1029
    label "lud"
  ]
  node [
    id 1030
    label "Samojedzi"
  ]
  node [
    id 1031
    label "Siuksowie"
  ]
  node [
    id 1032
    label "Buriaci"
  ]
  node [
    id 1033
    label "Czejenowie"
  ]
  node [
    id 1034
    label "Wotiacy"
  ]
  node [
    id 1035
    label "Aztekowie"
  ]
  node [
    id 1036
    label "nacja"
  ]
  node [
    id 1037
    label "Baszkirzy"
  ]
  node [
    id 1038
    label "innowierstwo"
  ]
  node [
    id 1039
    label "ch&#322;opstwo"
  ]
  node [
    id 1040
    label "najazd"
  ]
  node [
    id 1041
    label "demofobia"
  ]
  node [
    id 1042
    label "Tagalowie"
  ]
  node [
    id 1043
    label "Ugrowie"
  ]
  node [
    id 1044
    label "Retowie"
  ]
  node [
    id 1045
    label "Negryci"
  ]
  node [
    id 1046
    label "Ladynowie"
  ]
  node [
    id 1047
    label "Wizygoci"
  ]
  node [
    id 1048
    label "Dogonowie"
  ]
  node [
    id 1049
    label "chamstwo"
  ]
  node [
    id 1050
    label "Do&#322;ganie"
  ]
  node [
    id 1051
    label "Indoira&#324;czycy"
  ]
  node [
    id 1052
    label "gmin"
  ]
  node [
    id 1053
    label "Kozacy"
  ]
  node [
    id 1054
    label "Indoariowie"
  ]
  node [
    id 1055
    label "Maroni"
  ]
  node [
    id 1056
    label "Po&#322;owcy"
  ]
  node [
    id 1057
    label "Kumbrowie"
  ]
  node [
    id 1058
    label "Nogajowie"
  ]
  node [
    id 1059
    label "Nawahowie"
  ]
  node [
    id 1060
    label "Wenedowie"
  ]
  node [
    id 1061
    label "Majowie"
  ]
  node [
    id 1062
    label "Kipczacy"
  ]
  node [
    id 1063
    label "Frygijczycy"
  ]
  node [
    id 1064
    label "Paleoazjaci"
  ]
  node [
    id 1065
    label "Tocharowie"
  ]
  node [
    id 1066
    label "etnogeneza"
  ]
  node [
    id 1067
    label "nationality"
  ]
  node [
    id 1068
    label "Sri_Lanka"
  ]
  node [
    id 1069
    label "Mongolia"
  ]
  node [
    id 1070
    label "Stary_&#346;wiat"
  ]
  node [
    id 1071
    label "p&#243;&#322;noc"
  ]
  node [
    id 1072
    label "Wsch&#243;d"
  ]
  node [
    id 1073
    label "class"
  ]
  node [
    id 1074
    label "geosfera"
  ]
  node [
    id 1075
    label "obiekt_naturalny"
  ]
  node [
    id 1076
    label "przejmowanie"
  ]
  node [
    id 1077
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1078
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 1079
    label "po&#322;udnie"
  ]
  node [
    id 1080
    label "makrokosmos"
  ]
  node [
    id 1081
    label "huczek"
  ]
  node [
    id 1082
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 1083
    label "environment"
  ]
  node [
    id 1084
    label "morze"
  ]
  node [
    id 1085
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1086
    label "hydrosfera"
  ]
  node [
    id 1087
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 1088
    label "ciemna_materia"
  ]
  node [
    id 1089
    label "ekosystem"
  ]
  node [
    id 1090
    label "biota"
  ]
  node [
    id 1091
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 1092
    label "planeta"
  ]
  node [
    id 1093
    label "geotermia"
  ]
  node [
    id 1094
    label "ekosfera"
  ]
  node [
    id 1095
    label "ozonosfera"
  ]
  node [
    id 1096
    label "wszechstworzenie"
  ]
  node [
    id 1097
    label "woda"
  ]
  node [
    id 1098
    label "kuchnia"
  ]
  node [
    id 1099
    label "biosfera"
  ]
  node [
    id 1100
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 1101
    label "populace"
  ]
  node [
    id 1102
    label "magnetosfera"
  ]
  node [
    id 1103
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1104
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 1105
    label "universe"
  ]
  node [
    id 1106
    label "biegun"
  ]
  node [
    id 1107
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1108
    label "litosfera"
  ]
  node [
    id 1109
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 1110
    label "przestrze&#324;"
  ]
  node [
    id 1111
    label "stw&#243;r"
  ]
  node [
    id 1112
    label "p&#243;&#322;kula"
  ]
  node [
    id 1113
    label "przej&#281;cie"
  ]
  node [
    id 1114
    label "barysfera"
  ]
  node [
    id 1115
    label "czarna_dziura"
  ]
  node [
    id 1116
    label "atmosfera"
  ]
  node [
    id 1117
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 1118
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 1119
    label "geoida"
  ]
  node [
    id 1120
    label "zagranica"
  ]
  node [
    id 1121
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 1122
    label "fauna"
  ]
  node [
    id 1123
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 1124
    label "jednostka_systematyczna"
  ]
  node [
    id 1125
    label "gromada"
  ]
  node [
    id 1126
    label "egzemplarz"
  ]
  node [
    id 1127
    label "Entuzjastki"
  ]
  node [
    id 1128
    label "kompozycja"
  ]
  node [
    id 1129
    label "Terranie"
  ]
  node [
    id 1130
    label "category"
  ]
  node [
    id 1131
    label "pakiet_klimatyczny"
  ]
  node [
    id 1132
    label "oddzia&#322;"
  ]
  node [
    id 1133
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1134
    label "cz&#261;steczka"
  ]
  node [
    id 1135
    label "stage_set"
  ]
  node [
    id 1136
    label "type"
  ]
  node [
    id 1137
    label "specgrupa"
  ]
  node [
    id 1138
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1139
    label "Eurogrupa"
  ]
  node [
    id 1140
    label "formacja_geologiczna"
  ]
  node [
    id 1141
    label "harcerze_starsi"
  ]
  node [
    id 1142
    label "Kosowo"
  ]
  node [
    id 1143
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1144
    label "Zab&#322;ocie"
  ]
  node [
    id 1145
    label "zach&#243;d"
  ]
  node [
    id 1146
    label "Pow&#261;zki"
  ]
  node [
    id 1147
    label "Piotrowo"
  ]
  node [
    id 1148
    label "Olszanica"
  ]
  node [
    id 1149
    label "holarktyka"
  ]
  node [
    id 1150
    label "Ruda_Pabianicka"
  ]
  node [
    id 1151
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1152
    label "Ludwin&#243;w"
  ]
  node [
    id 1153
    label "Arktyka"
  ]
  node [
    id 1154
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1155
    label "Zabu&#380;e"
  ]
  node [
    id 1156
    label "antroposfera"
  ]
  node [
    id 1157
    label "terytorium"
  ]
  node [
    id 1158
    label "Neogea"
  ]
  node [
    id 1159
    label "Syberia_Zachodnia"
  ]
  node [
    id 1160
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1161
    label "zakres"
  ]
  node [
    id 1162
    label "pas_planetoid"
  ]
  node [
    id 1163
    label "Syberia_Wschodnia"
  ]
  node [
    id 1164
    label "Antarktyka"
  ]
  node [
    id 1165
    label "Rakowice"
  ]
  node [
    id 1166
    label "akrecja"
  ]
  node [
    id 1167
    label "wymiar"
  ]
  node [
    id 1168
    label "&#321;&#281;g"
  ]
  node [
    id 1169
    label "Kresy_Zachodnie"
  ]
  node [
    id 1170
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1171
    label "wsch&#243;d"
  ]
  node [
    id 1172
    label "Notogea"
  ]
  node [
    id 1173
    label "integer"
  ]
  node [
    id 1174
    label "liczba"
  ]
  node [
    id 1175
    label "zlewanie_si&#281;"
  ]
  node [
    id 1176
    label "ilo&#347;&#263;"
  ]
  node [
    id 1177
    label "uk&#322;ad"
  ]
  node [
    id 1178
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1179
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1180
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1181
    label "proces"
  ]
  node [
    id 1182
    label "boski"
  ]
  node [
    id 1183
    label "krajobraz"
  ]
  node [
    id 1184
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1185
    label "przywidzenie"
  ]
  node [
    id 1186
    label "presence"
  ]
  node [
    id 1187
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1188
    label "rozdzielanie"
  ]
  node [
    id 1189
    label "bezbrze&#380;e"
  ]
  node [
    id 1190
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1191
    label "niezmierzony"
  ]
  node [
    id 1192
    label "przedzielenie"
  ]
  node [
    id 1193
    label "nielito&#347;ciwy"
  ]
  node [
    id 1194
    label "rozdziela&#263;"
  ]
  node [
    id 1195
    label "oktant"
  ]
  node [
    id 1196
    label "przedzieli&#263;"
  ]
  node [
    id 1197
    label "przestw&#243;r"
  ]
  node [
    id 1198
    label "&#347;rodowisko"
  ]
  node [
    id 1199
    label "rura"
  ]
  node [
    id 1200
    label "grzebiuszka"
  ]
  node [
    id 1201
    label "smok_wawelski"
  ]
  node [
    id 1202
    label "niecz&#322;owiek"
  ]
  node [
    id 1203
    label "monster"
  ]
  node [
    id 1204
    label "potw&#243;r"
  ]
  node [
    id 1205
    label "istota_fantastyczna"
  ]
  node [
    id 1206
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 1207
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 1208
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 1209
    label "aspekt"
  ]
  node [
    id 1210
    label "troposfera"
  ]
  node [
    id 1211
    label "klimat"
  ]
  node [
    id 1212
    label "metasfera"
  ]
  node [
    id 1213
    label "atmosferyki"
  ]
  node [
    id 1214
    label "homosfera"
  ]
  node [
    id 1215
    label "powietrznia"
  ]
  node [
    id 1216
    label "jonosfera"
  ]
  node [
    id 1217
    label "termosfera"
  ]
  node [
    id 1218
    label "egzosfera"
  ]
  node [
    id 1219
    label "heterosfera"
  ]
  node [
    id 1220
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 1221
    label "tropopauza"
  ]
  node [
    id 1222
    label "kwas"
  ]
  node [
    id 1223
    label "powietrze"
  ]
  node [
    id 1224
    label "stratosfera"
  ]
  node [
    id 1225
    label "pow&#322;oka"
  ]
  node [
    id 1226
    label "mezosfera"
  ]
  node [
    id 1227
    label "mezopauza"
  ]
  node [
    id 1228
    label "atmosphere"
  ]
  node [
    id 1229
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 1230
    label "ciep&#322;o"
  ]
  node [
    id 1231
    label "energia_termiczna"
  ]
  node [
    id 1232
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 1233
    label "sferoida"
  ]
  node [
    id 1234
    label "object"
  ]
  node [
    id 1235
    label "temat"
  ]
  node [
    id 1236
    label "wpadni&#281;cie"
  ]
  node [
    id 1237
    label "mienie"
  ]
  node [
    id 1238
    label "wpa&#347;&#263;"
  ]
  node [
    id 1239
    label "wpadanie"
  ]
  node [
    id 1240
    label "wpada&#263;"
  ]
  node [
    id 1241
    label "wra&#380;enie"
  ]
  node [
    id 1242
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 1243
    label "interception"
  ]
  node [
    id 1244
    label "wzbudzenie"
  ]
  node [
    id 1245
    label "emotion"
  ]
  node [
    id 1246
    label "movement"
  ]
  node [
    id 1247
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1248
    label "wzi&#281;cie"
  ]
  node [
    id 1249
    label "bang"
  ]
  node [
    id 1250
    label "wzi&#261;&#263;"
  ]
  node [
    id 1251
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1252
    label "stimulate"
  ]
  node [
    id 1253
    label "ogarn&#261;&#263;"
  ]
  node [
    id 1254
    label "wzbudzi&#263;"
  ]
  node [
    id 1255
    label "thrill"
  ]
  node [
    id 1256
    label "treat"
  ]
  node [
    id 1257
    label "czerpa&#263;"
  ]
  node [
    id 1258
    label "bra&#263;"
  ]
  node [
    id 1259
    label "go"
  ]
  node [
    id 1260
    label "handle"
  ]
  node [
    id 1261
    label "ogarnia&#263;"
  ]
  node [
    id 1262
    label "branie"
  ]
  node [
    id 1263
    label "caparison"
  ]
  node [
    id 1264
    label "wzbudzanie"
  ]
  node [
    id 1265
    label "ogarnianie"
  ]
  node [
    id 1266
    label "zboczenie"
  ]
  node [
    id 1267
    label "om&#243;wienie"
  ]
  node [
    id 1268
    label "sponiewieranie"
  ]
  node [
    id 1269
    label "discipline"
  ]
  node [
    id 1270
    label "omawia&#263;"
  ]
  node [
    id 1271
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1272
    label "tre&#347;&#263;"
  ]
  node [
    id 1273
    label "robienie"
  ]
  node [
    id 1274
    label "sponiewiera&#263;"
  ]
  node [
    id 1275
    label "element"
  ]
  node [
    id 1276
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1277
    label "tematyka"
  ]
  node [
    id 1278
    label "w&#261;tek"
  ]
  node [
    id 1279
    label "zbaczanie"
  ]
  node [
    id 1280
    label "program_nauczania"
  ]
  node [
    id 1281
    label "om&#243;wi&#263;"
  ]
  node [
    id 1282
    label "omawianie"
  ]
  node [
    id 1283
    label "zbacza&#263;"
  ]
  node [
    id 1284
    label "zboczy&#263;"
  ]
  node [
    id 1285
    label "performance"
  ]
  node [
    id 1286
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1287
    label "Boreasz"
  ]
  node [
    id 1288
    label "p&#243;&#322;nocek"
  ]
  node [
    id 1289
    label "strona_&#347;wiata"
  ]
  node [
    id 1290
    label "&#347;rodek"
  ]
  node [
    id 1291
    label "dwunasta"
  ]
  node [
    id 1292
    label "pora"
  ]
  node [
    id 1293
    label "brzeg"
  ]
  node [
    id 1294
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 1295
    label "p&#322;oza"
  ]
  node [
    id 1296
    label "zawiasy"
  ]
  node [
    id 1297
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 1298
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 1299
    label "reda"
  ]
  node [
    id 1300
    label "zbiornik_wodny"
  ]
  node [
    id 1301
    label "przymorze"
  ]
  node [
    id 1302
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 1303
    label "bezmiar"
  ]
  node [
    id 1304
    label "pe&#322;ne_morze"
  ]
  node [
    id 1305
    label "latarnia_morska"
  ]
  node [
    id 1306
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1307
    label "nereida"
  ]
  node [
    id 1308
    label "okeanida"
  ]
  node [
    id 1309
    label "marina"
  ]
  node [
    id 1310
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 1311
    label "Morze_Czerwone"
  ]
  node [
    id 1312
    label "talasoterapia"
  ]
  node [
    id 1313
    label "Morze_Bia&#322;e"
  ]
  node [
    id 1314
    label "paliszcze"
  ]
  node [
    id 1315
    label "Neptun"
  ]
  node [
    id 1316
    label "Morze_Czarne"
  ]
  node [
    id 1317
    label "laguna"
  ]
  node [
    id 1318
    label "Morze_Egejskie"
  ]
  node [
    id 1319
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 1320
    label "Morze_Adriatyckie"
  ]
  node [
    id 1321
    label "rze&#378;biarstwo"
  ]
  node [
    id 1322
    label "planacja"
  ]
  node [
    id 1323
    label "relief"
  ]
  node [
    id 1324
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1325
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1326
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 1327
    label "bozzetto"
  ]
  node [
    id 1328
    label "plastyka"
  ]
  node [
    id 1329
    label "sfera"
  ]
  node [
    id 1330
    label "gleba"
  ]
  node [
    id 1331
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 1332
    label "warstwa"
  ]
  node [
    id 1333
    label "sialma"
  ]
  node [
    id 1334
    label "skorupa_ziemska"
  ]
  node [
    id 1335
    label "warstwa_perydotytowa"
  ]
  node [
    id 1336
    label "warstwa_granitowa"
  ]
  node [
    id 1337
    label "kriosfera"
  ]
  node [
    id 1338
    label "j&#261;dro"
  ]
  node [
    id 1339
    label "lej_polarny"
  ]
  node [
    id 1340
    label "kula"
  ]
  node [
    id 1341
    label "kresom&#243;zgowie"
  ]
  node [
    id 1342
    label "ozon"
  ]
  node [
    id 1343
    label "przyra"
  ]
  node [
    id 1344
    label "kontekst"
  ]
  node [
    id 1345
    label "miejsce_pracy"
  ]
  node [
    id 1346
    label "nation"
  ]
  node [
    id 1347
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 1348
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 1349
    label "iglak"
  ]
  node [
    id 1350
    label "cyprysowate"
  ]
  node [
    id 1351
    label "biom"
  ]
  node [
    id 1352
    label "szata_ro&#347;linna"
  ]
  node [
    id 1353
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1354
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1355
    label "zielono&#347;&#263;"
  ]
  node [
    id 1356
    label "pi&#281;tro"
  ]
  node [
    id 1357
    label "plant"
  ]
  node [
    id 1358
    label "geosystem"
  ]
  node [
    id 1359
    label "dotleni&#263;"
  ]
  node [
    id 1360
    label "spi&#281;trza&#263;"
  ]
  node [
    id 1361
    label "spi&#281;trzenie"
  ]
  node [
    id 1362
    label "utylizator"
  ]
  node [
    id 1363
    label "p&#322;ycizna"
  ]
  node [
    id 1364
    label "nabranie"
  ]
  node [
    id 1365
    label "Waruna"
  ]
  node [
    id 1366
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 1367
    label "przybieranie"
  ]
  node [
    id 1368
    label "uci&#261;g"
  ]
  node [
    id 1369
    label "bombast"
  ]
  node [
    id 1370
    label "fala"
  ]
  node [
    id 1371
    label "kryptodepresja"
  ]
  node [
    id 1372
    label "water"
  ]
  node [
    id 1373
    label "wysi&#281;k"
  ]
  node [
    id 1374
    label "pustka"
  ]
  node [
    id 1375
    label "ciecz"
  ]
  node [
    id 1376
    label "przybrze&#380;e"
  ]
  node [
    id 1377
    label "nap&#243;j"
  ]
  node [
    id 1378
    label "spi&#281;trzanie"
  ]
  node [
    id 1379
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 1380
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 1381
    label "bicie"
  ]
  node [
    id 1382
    label "klarownik"
  ]
  node [
    id 1383
    label "chlastanie"
  ]
  node [
    id 1384
    label "woda_s&#322;odka"
  ]
  node [
    id 1385
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 1386
    label "nabra&#263;"
  ]
  node [
    id 1387
    label "chlasta&#263;"
  ]
  node [
    id 1388
    label "uj&#281;cie_wody"
  ]
  node [
    id 1389
    label "zrzut"
  ]
  node [
    id 1390
    label "wodnik"
  ]
  node [
    id 1391
    label "pojazd"
  ]
  node [
    id 1392
    label "l&#243;d"
  ]
  node [
    id 1393
    label "wybrze&#380;e"
  ]
  node [
    id 1394
    label "deklamacja"
  ]
  node [
    id 1395
    label "tlenek"
  ]
  node [
    id 1396
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 1397
    label "biotop"
  ]
  node [
    id 1398
    label "biocenoza"
  ]
  node [
    id 1399
    label "awifauna"
  ]
  node [
    id 1400
    label "ichtiofauna"
  ]
  node [
    id 1401
    label "zaj&#281;cie"
  ]
  node [
    id 1402
    label "tajniki"
  ]
  node [
    id 1403
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 1404
    label "jedzenie"
  ]
  node [
    id 1405
    label "zaplecze"
  ]
  node [
    id 1406
    label "pomieszczenie"
  ]
  node [
    id 1407
    label "zlewozmywak"
  ]
  node [
    id 1408
    label "gotowa&#263;"
  ]
  node [
    id 1409
    label "Jowisz"
  ]
  node [
    id 1410
    label "syzygia"
  ]
  node [
    id 1411
    label "Saturn"
  ]
  node [
    id 1412
    label "Uran"
  ]
  node [
    id 1413
    label "strefa"
  ]
  node [
    id 1414
    label "dar"
  ]
  node [
    id 1415
    label "real"
  ]
  node [
    id 1416
    label "Ukraina"
  ]
  node [
    id 1417
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1418
    label "blok_wschodni"
  ]
  node [
    id 1419
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1420
    label "Europa_Wschodnia"
  ]
  node [
    id 1421
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 1422
    label "Daleki_Wsch&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 305
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 435
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 771
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 16
    target 777
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 779
  ]
  edge [
    source 16
    target 780
  ]
  edge [
    source 16
    target 781
  ]
  edge [
    source 16
    target 782
  ]
  edge [
    source 16
    target 783
  ]
  edge [
    source 16
    target 784
  ]
  edge [
    source 16
    target 785
  ]
  edge [
    source 16
    target 786
  ]
  edge [
    source 16
    target 787
  ]
  edge [
    source 16
    target 788
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 409
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 417
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 418
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 405
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 272
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 348
  ]
  edge [
    source 18
    target 364
  ]
  edge [
    source 18
    target 460
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 53
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 618
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 632
  ]
  edge [
    source 18
    target 226
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 262
  ]
  edge [
    source 18
    target 45
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 587
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 311
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 316
  ]
  edge [
    source 18
    target 301
  ]
  edge [
    source 18
    target 564
  ]
  edge [
    source 18
    target 542
  ]
  edge [
    source 18
    target 556
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 623
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 228
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 105
  ]
  edge [
    source 18
    target 55
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 545
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 892
  ]
  edge [
    source 21
    target 893
  ]
  edge [
    source 21
    target 894
  ]
  edge [
    source 21
    target 895
  ]
  edge [
    source 21
    target 896
  ]
  edge [
    source 21
    target 897
  ]
  edge [
    source 21
    target 898
  ]
  edge [
    source 21
    target 899
  ]
  edge [
    source 21
    target 900
  ]
  edge [
    source 21
    target 901
  ]
  edge [
    source 21
    target 902
  ]
  edge [
    source 21
    target 903
  ]
  edge [
    source 21
    target 904
  ]
  edge [
    source 21
    target 905
  ]
  edge [
    source 21
    target 576
  ]
  edge [
    source 21
    target 906
  ]
  edge [
    source 21
    target 907
  ]
  edge [
    source 21
    target 908
  ]
  edge [
    source 21
    target 909
  ]
  edge [
    source 21
    target 910
  ]
  edge [
    source 21
    target 732
  ]
  edge [
    source 21
    target 911
  ]
  edge [
    source 21
    target 485
  ]
  edge [
    source 21
    target 912
  ]
  edge [
    source 21
    target 913
  ]
  edge [
    source 21
    target 914
  ]
  edge [
    source 21
    target 915
  ]
  edge [
    source 21
    target 916
  ]
  edge [
    source 21
    target 917
  ]
  edge [
    source 21
    target 918
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 921
  ]
  edge [
    source 21
    target 922
  ]
  edge [
    source 21
    target 923
  ]
  edge [
    source 21
    target 924
  ]
  edge [
    source 21
    target 925
  ]
  edge [
    source 21
    target 926
  ]
  edge [
    source 21
    target 927
  ]
  edge [
    source 21
    target 928
  ]
  edge [
    source 21
    target 929
  ]
  edge [
    source 21
    target 484
  ]
  edge [
    source 21
    target 930
  ]
  edge [
    source 21
    target 931
  ]
  edge [
    source 21
    target 932
  ]
  edge [
    source 21
    target 933
  ]
  edge [
    source 21
    target 934
  ]
  edge [
    source 21
    target 935
  ]
  edge [
    source 21
    target 936
  ]
  edge [
    source 21
    target 937
  ]
  edge [
    source 21
    target 938
  ]
  edge [
    source 21
    target 939
  ]
  edge [
    source 21
    target 940
  ]
  edge [
    source 21
    target 941
  ]
  edge [
    source 21
    target 942
  ]
  edge [
    source 21
    target 943
  ]
  edge [
    source 21
    target 944
  ]
  edge [
    source 21
    target 945
  ]
  edge [
    source 21
    target 946
  ]
  edge [
    source 21
    target 947
  ]
  edge [
    source 21
    target 948
  ]
  edge [
    source 21
    target 949
  ]
  edge [
    source 21
    target 950
  ]
  edge [
    source 21
    target 951
  ]
  edge [
    source 21
    target 696
  ]
  edge [
    source 21
    target 952
  ]
  edge [
    source 21
    target 953
  ]
  edge [
    source 21
    target 954
  ]
  edge [
    source 21
    target 657
  ]
  edge [
    source 21
    target 955
  ]
  edge [
    source 21
    target 956
  ]
  edge [
    source 21
    target 957
  ]
  edge [
    source 21
    target 958
  ]
  edge [
    source 21
    target 959
  ]
  edge [
    source 21
    target 592
  ]
  edge [
    source 21
    target 960
  ]
  edge [
    source 21
    target 961
  ]
  edge [
    source 21
    target 962
  ]
  edge [
    source 21
    target 963
  ]
  edge [
    source 21
    target 964
  ]
  edge [
    source 21
    target 965
  ]
  edge [
    source 21
    target 966
  ]
  edge [
    source 21
    target 967
  ]
  edge [
    source 21
    target 968
  ]
  edge [
    source 21
    target 969
  ]
  edge [
    source 21
    target 970
  ]
  edge [
    source 21
    target 971
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 792
  ]
  edge [
    source 22
    target 417
  ]
  edge [
    source 22
    target 382
  ]
  edge [
    source 22
    target 396
  ]
  edge [
    source 22
    target 399
  ]
  edge [
    source 22
    target 388
  ]
  edge [
    source 22
    target 972
  ]
  edge [
    source 22
    target 973
  ]
  edge [
    source 22
    target 400
  ]
  edge [
    source 22
    target 974
  ]
  edge [
    source 22
    target 401
  ]
  edge [
    source 22
    target 975
  ]
  edge [
    source 22
    target 976
  ]
  edge [
    source 22
    target 977
  ]
  edge [
    source 22
    target 978
  ]
  edge [
    source 22
    target 979
  ]
  edge [
    source 22
    target 980
  ]
  edge [
    source 22
    target 981
  ]
  edge [
    source 22
    target 982
  ]
  edge [
    source 22
    target 983
  ]
  edge [
    source 22
    target 984
  ]
  edge [
    source 22
    target 985
  ]
  edge [
    source 22
    target 405
  ]
  edge [
    source 22
    target 986
  ]
  edge [
    source 22
    target 987
  ]
  edge [
    source 22
    target 988
  ]
  edge [
    source 22
    target 989
  ]
  edge [
    source 22
    target 990
  ]
  edge [
    source 22
    target 991
  ]
  edge [
    source 22
    target 992
  ]
  edge [
    source 22
    target 993
  ]
  edge [
    source 22
    target 782
  ]
  edge [
    source 22
    target 994
  ]
  edge [
    source 22
    target 995
  ]
  edge [
    source 22
    target 996
  ]
  edge [
    source 22
    target 997
  ]
  edge [
    source 22
    target 998
  ]
  edge [
    source 22
    target 383
  ]
  edge [
    source 22
    target 999
  ]
  edge [
    source 22
    target 1000
  ]
  edge [
    source 22
    target 499
  ]
  edge [
    source 22
    target 1001
  ]
  edge [
    source 22
    target 1002
  ]
  edge [
    source 22
    target 402
  ]
  edge [
    source 22
    target 403
  ]
  edge [
    source 22
    target 404
  ]
  edge [
    source 22
    target 397
  ]
  edge [
    source 22
    target 398
  ]
  edge [
    source 22
    target 1003
  ]
  edge [
    source 22
    target 1004
  ]
  edge [
    source 22
    target 1005
  ]
  edge [
    source 22
    target 1006
  ]
  edge [
    source 22
    target 1007
  ]
  edge [
    source 22
    target 1008
  ]
  edge [
    source 22
    target 1009
  ]
  edge [
    source 22
    target 1010
  ]
  edge [
    source 22
    target 1011
  ]
  edge [
    source 22
    target 1012
  ]
  edge [
    source 22
    target 1013
  ]
  edge [
    source 22
    target 1014
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 697
  ]
  edge [
    source 23
    target 111
  ]
  edge [
    source 23
    target 1015
  ]
  edge [
    source 23
    target 632
  ]
  edge [
    source 23
    target 662
  ]
  edge [
    source 23
    target 1016
  ]
  edge [
    source 23
    target 537
  ]
  edge [
    source 23
    target 1017
  ]
  edge [
    source 23
    target 1018
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1019
  ]
  edge [
    source 25
    target 272
  ]
  edge [
    source 25
    target 1020
  ]
  edge [
    source 25
    target 872
  ]
  edge [
    source 25
    target 873
  ]
  edge [
    source 25
    target 623
  ]
  edge [
    source 25
    target 874
  ]
  edge [
    source 25
    target 875
  ]
  edge [
    source 25
    target 876
  ]
  edge [
    source 25
    target 877
  ]
  edge [
    source 25
    target 878
  ]
  edge [
    source 25
    target 879
  ]
  edge [
    source 25
    target 228
  ]
  edge [
    source 25
    target 880
  ]
  edge [
    source 25
    target 881
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1021
  ]
  edge [
    source 26
    target 1022
  ]
  edge [
    source 26
    target 1023
  ]
  edge [
    source 26
    target 1024
  ]
  edge [
    source 26
    target 1025
  ]
  edge [
    source 26
    target 1026
  ]
  edge [
    source 26
    target 1027
  ]
  edge [
    source 26
    target 1028
  ]
  edge [
    source 26
    target 1029
  ]
  edge [
    source 26
    target 1030
  ]
  edge [
    source 26
    target 1031
  ]
  edge [
    source 26
    target 1032
  ]
  edge [
    source 26
    target 1033
  ]
  edge [
    source 26
    target 1034
  ]
  edge [
    source 26
    target 1035
  ]
  edge [
    source 26
    target 1036
  ]
  edge [
    source 26
    target 1037
  ]
  edge [
    source 26
    target 641
  ]
  edge [
    source 26
    target 1038
  ]
  edge [
    source 26
    target 1039
  ]
  edge [
    source 26
    target 181
  ]
  edge [
    source 26
    target 1040
  ]
  edge [
    source 26
    target 1041
  ]
  edge [
    source 26
    target 1042
  ]
  edge [
    source 26
    target 1043
  ]
  edge [
    source 26
    target 1044
  ]
  edge [
    source 26
    target 1045
  ]
  edge [
    source 26
    target 1046
  ]
  edge [
    source 26
    target 1047
  ]
  edge [
    source 26
    target 1048
  ]
  edge [
    source 26
    target 1049
  ]
  edge [
    source 26
    target 1050
  ]
  edge [
    source 26
    target 1051
  ]
  edge [
    source 26
    target 1052
  ]
  edge [
    source 26
    target 1053
  ]
  edge [
    source 26
    target 1054
  ]
  edge [
    source 26
    target 1055
  ]
  edge [
    source 26
    target 1056
  ]
  edge [
    source 26
    target 1057
  ]
  edge [
    source 26
    target 1058
  ]
  edge [
    source 26
    target 1059
  ]
  edge [
    source 26
    target 1060
  ]
  edge [
    source 26
    target 1061
  ]
  edge [
    source 26
    target 1062
  ]
  edge [
    source 26
    target 1063
  ]
  edge [
    source 26
    target 1064
  ]
  edge [
    source 26
    target 1065
  ]
  edge [
    source 26
    target 1066
  ]
  edge [
    source 26
    target 1067
  ]
  edge [
    source 26
    target 1068
  ]
  edge [
    source 26
    target 1069
  ]
  edge [
    source 27
    target 1070
  ]
  edge [
    source 27
    target 488
  ]
  edge [
    source 27
    target 1071
  ]
  edge [
    source 27
    target 71
  ]
  edge [
    source 27
    target 1072
  ]
  edge [
    source 27
    target 1073
  ]
  edge [
    source 27
    target 1074
  ]
  edge [
    source 27
    target 1075
  ]
  edge [
    source 27
    target 1076
  ]
  edge [
    source 27
    target 1077
  ]
  edge [
    source 27
    target 644
  ]
  edge [
    source 27
    target 1078
  ]
  edge [
    source 27
    target 1079
  ]
  edge [
    source 27
    target 575
  ]
  edge [
    source 27
    target 587
  ]
  edge [
    source 27
    target 1080
  ]
  edge [
    source 27
    target 1081
  ]
  edge [
    source 27
    target 1082
  ]
  edge [
    source 27
    target 228
  ]
  edge [
    source 27
    target 1083
  ]
  edge [
    source 27
    target 1084
  ]
  edge [
    source 27
    target 589
  ]
  edge [
    source 27
    target 1085
  ]
  edge [
    source 27
    target 504
  ]
  edge [
    source 27
    target 1086
  ]
  edge [
    source 27
    target 1087
  ]
  edge [
    source 27
    target 1088
  ]
  edge [
    source 27
    target 1089
  ]
  edge [
    source 27
    target 1090
  ]
  edge [
    source 27
    target 1091
  ]
  edge [
    source 27
    target 1092
  ]
  edge [
    source 27
    target 1093
  ]
  edge [
    source 27
    target 1094
  ]
  edge [
    source 27
    target 1095
  ]
  edge [
    source 27
    target 1096
  ]
  edge [
    source 27
    target 181
  ]
  edge [
    source 27
    target 1097
  ]
  edge [
    source 27
    target 1098
  ]
  edge [
    source 27
    target 1099
  ]
  edge [
    source 27
    target 349
  ]
  edge [
    source 27
    target 1100
  ]
  edge [
    source 27
    target 1101
  ]
  edge [
    source 27
    target 1102
  ]
  edge [
    source 27
    target 1103
  ]
  edge [
    source 27
    target 1104
  ]
  edge [
    source 27
    target 1105
  ]
  edge [
    source 27
    target 1106
  ]
  edge [
    source 27
    target 1107
  ]
  edge [
    source 27
    target 1108
  ]
  edge [
    source 27
    target 714
  ]
  edge [
    source 27
    target 160
  ]
  edge [
    source 27
    target 1109
  ]
  edge [
    source 27
    target 1110
  ]
  edge [
    source 27
    target 1111
  ]
  edge [
    source 27
    target 1112
  ]
  edge [
    source 27
    target 1113
  ]
  edge [
    source 27
    target 1114
  ]
  edge [
    source 27
    target 351
  ]
  edge [
    source 27
    target 1115
  ]
  edge [
    source 27
    target 1116
  ]
  edge [
    source 27
    target 506
  ]
  edge [
    source 27
    target 1117
  ]
  edge [
    source 27
    target 645
  ]
  edge [
    source 27
    target 1118
  ]
  edge [
    source 27
    target 1119
  ]
  edge [
    source 27
    target 1120
  ]
  edge [
    source 27
    target 1121
  ]
  edge [
    source 27
    target 1122
  ]
  edge [
    source 27
    target 1123
  ]
  edge [
    source 27
    target 231
  ]
  edge [
    source 27
    target 363
  ]
  edge [
    source 27
    target 1124
  ]
  edge [
    source 27
    target 147
  ]
  edge [
    source 27
    target 1125
  ]
  edge [
    source 27
    target 149
  ]
  edge [
    source 27
    target 1126
  ]
  edge [
    source 27
    target 1127
  ]
  edge [
    source 27
    target 233
  ]
  edge [
    source 27
    target 1128
  ]
  edge [
    source 27
    target 1129
  ]
  edge [
    source 27
    target 237
  ]
  edge [
    source 27
    target 1130
  ]
  edge [
    source 27
    target 1131
  ]
  edge [
    source 27
    target 1132
  ]
  edge [
    source 27
    target 1133
  ]
  edge [
    source 27
    target 1134
  ]
  edge [
    source 27
    target 1135
  ]
  edge [
    source 27
    target 1136
  ]
  edge [
    source 27
    target 1137
  ]
  edge [
    source 27
    target 1138
  ]
  edge [
    source 27
    target 232
  ]
  edge [
    source 27
    target 246
  ]
  edge [
    source 27
    target 1139
  ]
  edge [
    source 27
    target 238
  ]
  edge [
    source 27
    target 1140
  ]
  edge [
    source 27
    target 1141
  ]
  edge [
    source 27
    target 1142
  ]
  edge [
    source 27
    target 1143
  ]
  edge [
    source 27
    target 1144
  ]
  edge [
    source 27
    target 1145
  ]
  edge [
    source 27
    target 1146
  ]
  edge [
    source 27
    target 1147
  ]
  edge [
    source 27
    target 1148
  ]
  edge [
    source 27
    target 1149
  ]
  edge [
    source 27
    target 1150
  ]
  edge [
    source 27
    target 1151
  ]
  edge [
    source 27
    target 1152
  ]
  edge [
    source 27
    target 1153
  ]
  edge [
    source 27
    target 1154
  ]
  edge [
    source 27
    target 1155
  ]
  edge [
    source 27
    target 262
  ]
  edge [
    source 27
    target 1156
  ]
  edge [
    source 27
    target 1157
  ]
  edge [
    source 27
    target 1158
  ]
  edge [
    source 27
    target 1159
  ]
  edge [
    source 27
    target 1160
  ]
  edge [
    source 27
    target 1161
  ]
  edge [
    source 27
    target 1162
  ]
  edge [
    source 27
    target 1163
  ]
  edge [
    source 27
    target 1164
  ]
  edge [
    source 27
    target 1165
  ]
  edge [
    source 27
    target 1166
  ]
  edge [
    source 27
    target 1167
  ]
  edge [
    source 27
    target 1168
  ]
  edge [
    source 27
    target 1169
  ]
  edge [
    source 27
    target 1170
  ]
  edge [
    source 27
    target 1171
  ]
  edge [
    source 27
    target 1172
  ]
  edge [
    source 27
    target 1173
  ]
  edge [
    source 27
    target 1174
  ]
  edge [
    source 27
    target 1175
  ]
  edge [
    source 27
    target 1176
  ]
  edge [
    source 27
    target 1177
  ]
  edge [
    source 27
    target 1178
  ]
  edge [
    source 27
    target 1179
  ]
  edge [
    source 27
    target 994
  ]
  edge [
    source 27
    target 1180
  ]
  edge [
    source 27
    target 1181
  ]
  edge [
    source 27
    target 1182
  ]
  edge [
    source 27
    target 1183
  ]
  edge [
    source 27
    target 1184
  ]
  edge [
    source 27
    target 1185
  ]
  edge [
    source 27
    target 1186
  ]
  edge [
    source 27
    target 632
  ]
  edge [
    source 27
    target 1187
  ]
  edge [
    source 27
    target 1188
  ]
  edge [
    source 27
    target 1189
  ]
  edge [
    source 27
    target 863
  ]
  edge [
    source 27
    target 1190
  ]
  edge [
    source 27
    target 1191
  ]
  edge [
    source 27
    target 1192
  ]
  edge [
    source 27
    target 1193
  ]
  edge [
    source 27
    target 1194
  ]
  edge [
    source 27
    target 1195
  ]
  edge [
    source 27
    target 1196
  ]
  edge [
    source 27
    target 1197
  ]
  edge [
    source 27
    target 1198
  ]
  edge [
    source 27
    target 1199
  ]
  edge [
    source 27
    target 1200
  ]
  edge [
    source 27
    target 642
  ]
  edge [
    source 27
    target 643
  ]
  edge [
    source 27
    target 646
  ]
  edge [
    source 27
    target 647
  ]
  edge [
    source 27
    target 1201
  ]
  edge [
    source 27
    target 1202
  ]
  edge [
    source 27
    target 1203
  ]
  edge [
    source 27
    target 434
  ]
  edge [
    source 27
    target 1204
  ]
  edge [
    source 27
    target 1205
  ]
  edge [
    source 27
    target 498
  ]
  edge [
    source 27
    target 1206
  ]
  edge [
    source 27
    target 1207
  ]
  edge [
    source 27
    target 1208
  ]
  edge [
    source 27
    target 1209
  ]
  edge [
    source 27
    target 1210
  ]
  edge [
    source 27
    target 1211
  ]
  edge [
    source 27
    target 1212
  ]
  edge [
    source 27
    target 1213
  ]
  edge [
    source 27
    target 1214
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 27
    target 1215
  ]
  edge [
    source 27
    target 1216
  ]
  edge [
    source 27
    target 1217
  ]
  edge [
    source 27
    target 1218
  ]
  edge [
    source 27
    target 1219
  ]
  edge [
    source 27
    target 1220
  ]
  edge [
    source 27
    target 1221
  ]
  edge [
    source 27
    target 1222
  ]
  edge [
    source 27
    target 1223
  ]
  edge [
    source 27
    target 1224
  ]
  edge [
    source 27
    target 1225
  ]
  edge [
    source 27
    target 1226
  ]
  edge [
    source 27
    target 1227
  ]
  edge [
    source 27
    target 1228
  ]
  edge [
    source 27
    target 1229
  ]
  edge [
    source 27
    target 1230
  ]
  edge [
    source 27
    target 1231
  ]
  edge [
    source 27
    target 1232
  ]
  edge [
    source 27
    target 1233
  ]
  edge [
    source 27
    target 1234
  ]
  edge [
    source 27
    target 1235
  ]
  edge [
    source 27
    target 1236
  ]
  edge [
    source 27
    target 1237
  ]
  edge [
    source 27
    target 460
  ]
  edge [
    source 27
    target 305
  ]
  edge [
    source 27
    target 1238
  ]
  edge [
    source 27
    target 1239
  ]
  edge [
    source 27
    target 1240
  ]
  edge [
    source 27
    target 1241
  ]
  edge [
    source 27
    target 1242
  ]
  edge [
    source 27
    target 1243
  ]
  edge [
    source 27
    target 1244
  ]
  edge [
    source 27
    target 1245
  ]
  edge [
    source 27
    target 1246
  ]
  edge [
    source 27
    target 1247
  ]
  edge [
    source 27
    target 1248
  ]
  edge [
    source 27
    target 1249
  ]
  edge [
    source 27
    target 1250
  ]
  edge [
    source 27
    target 1251
  ]
  edge [
    source 27
    target 1252
  ]
  edge [
    source 27
    target 1253
  ]
  edge [
    source 27
    target 1254
  ]
  edge [
    source 27
    target 1255
  ]
  edge [
    source 27
    target 1256
  ]
  edge [
    source 27
    target 1257
  ]
  edge [
    source 27
    target 1258
  ]
  edge [
    source 27
    target 1259
  ]
  edge [
    source 27
    target 1260
  ]
  edge [
    source 27
    target 912
  ]
  edge [
    source 27
    target 1261
  ]
  edge [
    source 27
    target 491
  ]
  edge [
    source 27
    target 492
  ]
  edge [
    source 27
    target 1262
  ]
  edge [
    source 27
    target 1263
  ]
  edge [
    source 27
    target 1264
  ]
  edge [
    source 27
    target 662
  ]
  edge [
    source 27
    target 1265
  ]
  edge [
    source 27
    target 1266
  ]
  edge [
    source 27
    target 1267
  ]
  edge [
    source 27
    target 1268
  ]
  edge [
    source 27
    target 1269
  ]
  edge [
    source 27
    target 1270
  ]
  edge [
    source 27
    target 1271
  ]
  edge [
    source 27
    target 1272
  ]
  edge [
    source 27
    target 1273
  ]
  edge [
    source 27
    target 1274
  ]
  edge [
    source 27
    target 1275
  ]
  edge [
    source 27
    target 623
  ]
  edge [
    source 27
    target 1276
  ]
  edge [
    source 27
    target 1277
  ]
  edge [
    source 27
    target 1278
  ]
  edge [
    source 27
    target 1279
  ]
  edge [
    source 27
    target 1280
  ]
  edge [
    source 27
    target 1281
  ]
  edge [
    source 27
    target 1282
  ]
  edge [
    source 27
    target 865
  ]
  edge [
    source 27
    target 1283
  ]
  edge [
    source 27
    target 1284
  ]
  edge [
    source 27
    target 1285
  ]
  edge [
    source 27
    target 550
  ]
  edge [
    source 27
    target 1286
  ]
  edge [
    source 27
    target 1287
  ]
  edge [
    source 27
    target 673
  ]
  edge [
    source 27
    target 1288
  ]
  edge [
    source 27
    target 1289
  ]
  edge [
    source 27
    target 675
  ]
  edge [
    source 27
    target 315
  ]
  edge [
    source 27
    target 1290
  ]
  edge [
    source 27
    target 674
  ]
  edge [
    source 27
    target 1291
  ]
  edge [
    source 27
    target 1292
  ]
  edge [
    source 27
    target 1293
  ]
  edge [
    source 27
    target 1294
  ]
  edge [
    source 27
    target 1295
  ]
  edge [
    source 27
    target 1296
  ]
  edge [
    source 27
    target 1297
  ]
  edge [
    source 27
    target 173
  ]
  edge [
    source 27
    target 175
  ]
  edge [
    source 27
    target 1298
  ]
  edge [
    source 27
    target 1299
  ]
  edge [
    source 27
    target 1300
  ]
  edge [
    source 27
    target 1301
  ]
  edge [
    source 27
    target 1302
  ]
  edge [
    source 27
    target 1303
  ]
  edge [
    source 27
    target 1304
  ]
  edge [
    source 27
    target 1305
  ]
  edge [
    source 27
    target 1306
  ]
  edge [
    source 27
    target 1307
  ]
  edge [
    source 27
    target 1308
  ]
  edge [
    source 27
    target 1309
  ]
  edge [
    source 27
    target 1310
  ]
  edge [
    source 27
    target 1311
  ]
  edge [
    source 27
    target 1312
  ]
  edge [
    source 27
    target 1313
  ]
  edge [
    source 27
    target 1314
  ]
  edge [
    source 27
    target 1315
  ]
  edge [
    source 27
    target 1316
  ]
  edge [
    source 27
    target 1317
  ]
  edge [
    source 27
    target 1318
  ]
  edge [
    source 27
    target 1319
  ]
  edge [
    source 27
    target 1320
  ]
  edge [
    source 27
    target 1321
  ]
  edge [
    source 27
    target 1322
  ]
  edge [
    source 27
    target 1323
  ]
  edge [
    source 27
    target 1324
  ]
  edge [
    source 27
    target 1325
  ]
  edge [
    source 27
    target 1326
  ]
  edge [
    source 27
    target 1327
  ]
  edge [
    source 27
    target 1328
  ]
  edge [
    source 27
    target 1329
  ]
  edge [
    source 27
    target 1330
  ]
  edge [
    source 27
    target 1331
  ]
  edge [
    source 27
    target 1332
  ]
  edge [
    source 27
    target 1333
  ]
  edge [
    source 27
    target 1334
  ]
  edge [
    source 27
    target 1335
  ]
  edge [
    source 27
    target 1336
  ]
  edge [
    source 27
    target 1337
  ]
  edge [
    source 27
    target 1338
  ]
  edge [
    source 27
    target 1339
  ]
  edge [
    source 27
    target 1340
  ]
  edge [
    source 27
    target 1341
  ]
  edge [
    source 27
    target 1342
  ]
  edge [
    source 27
    target 1343
  ]
  edge [
    source 27
    target 1344
  ]
  edge [
    source 27
    target 1345
  ]
  edge [
    source 27
    target 1346
  ]
  edge [
    source 27
    target 1347
  ]
  edge [
    source 27
    target 1348
  ]
  edge [
    source 27
    target 267
  ]
  edge [
    source 27
    target 1349
  ]
  edge [
    source 27
    target 1350
  ]
  edge [
    source 27
    target 1351
  ]
  edge [
    source 27
    target 1352
  ]
  edge [
    source 27
    target 1353
  ]
  edge [
    source 27
    target 1354
  ]
  edge [
    source 27
    target 1355
  ]
  edge [
    source 27
    target 1356
  ]
  edge [
    source 27
    target 1357
  ]
  edge [
    source 27
    target 243
  ]
  edge [
    source 27
    target 1358
  ]
  edge [
    source 27
    target 1359
  ]
  edge [
    source 27
    target 1360
  ]
  edge [
    source 27
    target 1361
  ]
  edge [
    source 27
    target 1362
  ]
  edge [
    source 27
    target 1363
  ]
  edge [
    source 27
    target 1364
  ]
  edge [
    source 27
    target 1365
  ]
  edge [
    source 27
    target 1366
  ]
  edge [
    source 27
    target 1367
  ]
  edge [
    source 27
    target 1368
  ]
  edge [
    source 27
    target 1369
  ]
  edge [
    source 27
    target 1370
  ]
  edge [
    source 27
    target 1371
  ]
  edge [
    source 27
    target 1372
  ]
  edge [
    source 27
    target 1373
  ]
  edge [
    source 27
    target 1374
  ]
  edge [
    source 27
    target 1375
  ]
  edge [
    source 27
    target 1376
  ]
  edge [
    source 27
    target 1377
  ]
  edge [
    source 27
    target 1378
  ]
  edge [
    source 27
    target 1379
  ]
  edge [
    source 27
    target 1380
  ]
  edge [
    source 27
    target 1381
  ]
  edge [
    source 27
    target 1382
  ]
  edge [
    source 27
    target 1383
  ]
  edge [
    source 27
    target 1384
  ]
  edge [
    source 27
    target 1385
  ]
  edge [
    source 27
    target 1386
  ]
  edge [
    source 27
    target 1387
  ]
  edge [
    source 27
    target 1388
  ]
  edge [
    source 27
    target 1389
  ]
  edge [
    source 27
    target 1390
  ]
  edge [
    source 27
    target 1391
  ]
  edge [
    source 27
    target 1392
  ]
  edge [
    source 27
    target 1393
  ]
  edge [
    source 27
    target 1394
  ]
  edge [
    source 27
    target 1395
  ]
  edge [
    source 27
    target 1396
  ]
  edge [
    source 27
    target 1397
  ]
  edge [
    source 27
    target 1398
  ]
  edge [
    source 27
    target 1399
  ]
  edge [
    source 27
    target 1400
  ]
  edge [
    source 27
    target 1401
  ]
  edge [
    source 27
    target 121
  ]
  edge [
    source 27
    target 1402
  ]
  edge [
    source 27
    target 1403
  ]
  edge [
    source 27
    target 1404
  ]
  edge [
    source 27
    target 1405
  ]
  edge [
    source 27
    target 1406
  ]
  edge [
    source 27
    target 1407
  ]
  edge [
    source 27
    target 1408
  ]
  edge [
    source 27
    target 1409
  ]
  edge [
    source 27
    target 1410
  ]
  edge [
    source 27
    target 1411
  ]
  edge [
    source 27
    target 1412
  ]
  edge [
    source 27
    target 1413
  ]
  edge [
    source 27
    target 909
  ]
  edge [
    source 27
    target 1414
  ]
  edge [
    source 27
    target 1415
  ]
  edge [
    source 27
    target 1416
  ]
  edge [
    source 27
    target 1417
  ]
  edge [
    source 27
    target 1418
  ]
  edge [
    source 27
    target 1419
  ]
  edge [
    source 27
    target 1420
  ]
  edge [
    source 27
    target 1421
  ]
  edge [
    source 27
    target 1422
  ]
]
