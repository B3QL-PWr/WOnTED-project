graph [
  node [
    id 0
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "rozpocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "xxiv"
    origin "text"
  ]
  node [
    id 4
    label "mi&#281;dzynarodowy"
    origin "text"
  ]
  node [
    id 5
    label "turniej"
    origin "text"
  ]
  node [
    id 6
    label "puchar"
    origin "text"
  ]
  node [
    id 7
    label "syrenka"
    origin "text"
  ]
  node [
    id 8
    label "m&#322;odzie&#380;owy"
    origin "text"
  ]
  node [
    id 9
    label "impreza"
    origin "text"
  ]
  node [
    id 10
    label "pi&#322;karski"
    origin "text"
  ]
  node [
    id 11
    label "bez"
    origin "text"
  ]
  node [
    id 12
    label "precedens"
    origin "text"
  ]
  node [
    id 13
    label "tylko"
    origin "text"
  ]
  node [
    id 14
    label "nasi"
    origin "text"
  ]
  node [
    id 15
    label "ale"
    origin "text"
  ]
  node [
    id 16
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 17
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 18
    label "europejski"
    origin "text"
  ]
  node [
    id 19
    label "futbol"
    origin "text"
  ]
  node [
    id 20
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 21
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 22
    label "mecz"
    origin "text"
  ]
  node [
    id 23
    label "miejsce"
    origin "text"
  ]
  node [
    id 24
    label "miesi&#261;c"
  ]
  node [
    id 25
    label "tydzie&#324;"
  ]
  node [
    id 26
    label "miech"
  ]
  node [
    id 27
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 28
    label "czas"
  ]
  node [
    id 29
    label "rok"
  ]
  node [
    id 30
    label "kalendy"
  ]
  node [
    id 31
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 32
    label "zrobi&#263;"
  ]
  node [
    id 33
    label "cause"
  ]
  node [
    id 34
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 35
    label "do"
  ]
  node [
    id 36
    label "zacz&#261;&#263;"
  ]
  node [
    id 37
    label "post&#261;pi&#263;"
  ]
  node [
    id 38
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 39
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 40
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 41
    label "zorganizowa&#263;"
  ]
  node [
    id 42
    label "appoint"
  ]
  node [
    id 43
    label "wystylizowa&#263;"
  ]
  node [
    id 44
    label "przerobi&#263;"
  ]
  node [
    id 45
    label "nabra&#263;"
  ]
  node [
    id 46
    label "make"
  ]
  node [
    id 47
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 48
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 49
    label "wydali&#263;"
  ]
  node [
    id 50
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 51
    label "odj&#261;&#263;"
  ]
  node [
    id 52
    label "introduce"
  ]
  node [
    id 53
    label "begin"
  ]
  node [
    id 54
    label "ut"
  ]
  node [
    id 55
    label "d&#378;wi&#281;k"
  ]
  node [
    id 56
    label "C"
  ]
  node [
    id 57
    label "his"
  ]
  node [
    id 58
    label "umi&#281;dzynaradawianie"
  ]
  node [
    id 59
    label "umi&#281;dzynarodowienie"
  ]
  node [
    id 60
    label "mi&#281;dzynarodowo"
  ]
  node [
    id 61
    label "transgraniczny"
  ]
  node [
    id 62
    label "zbiorowo"
  ]
  node [
    id 63
    label "internationalization"
  ]
  node [
    id 64
    label "uwsp&#243;lnienie"
  ]
  node [
    id 65
    label "udost&#281;pnienie"
  ]
  node [
    id 66
    label "udost&#281;pnianie"
  ]
  node [
    id 67
    label "Wielki_Szlem"
  ]
  node [
    id 68
    label "pojedynek"
  ]
  node [
    id 69
    label "drive"
  ]
  node [
    id 70
    label "runda"
  ]
  node [
    id 71
    label "rywalizacja"
  ]
  node [
    id 72
    label "zawody"
  ]
  node [
    id 73
    label "eliminacje"
  ]
  node [
    id 74
    label "tournament"
  ]
  node [
    id 75
    label "impra"
  ]
  node [
    id 76
    label "rozrywka"
  ]
  node [
    id 77
    label "przyj&#281;cie"
  ]
  node [
    id 78
    label "okazja"
  ]
  node [
    id 79
    label "party"
  ]
  node [
    id 80
    label "contest"
  ]
  node [
    id 81
    label "wydarzenie"
  ]
  node [
    id 82
    label "walczy&#263;"
  ]
  node [
    id 83
    label "champion"
  ]
  node [
    id 84
    label "walczenie"
  ]
  node [
    id 85
    label "tysi&#281;cznik"
  ]
  node [
    id 86
    label "spadochroniarstwo"
  ]
  node [
    id 87
    label "kategoria_open"
  ]
  node [
    id 88
    label "engagement"
  ]
  node [
    id 89
    label "walka"
  ]
  node [
    id 90
    label "wyzwanie"
  ]
  node [
    id 91
    label "wyzwa&#263;"
  ]
  node [
    id 92
    label "odyniec"
  ]
  node [
    id 93
    label "wyzywa&#263;"
  ]
  node [
    id 94
    label "sekundant"
  ]
  node [
    id 95
    label "competitiveness"
  ]
  node [
    id 96
    label "sp&#243;r"
  ]
  node [
    id 97
    label "bout"
  ]
  node [
    id 98
    label "wyzywanie"
  ]
  node [
    id 99
    label "konkurs"
  ]
  node [
    id 100
    label "faza"
  ]
  node [
    id 101
    label "retirement"
  ]
  node [
    id 102
    label "rozgrywka"
  ]
  node [
    id 103
    label "seria"
  ]
  node [
    id 104
    label "rhythm"
  ]
  node [
    id 105
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 106
    label "okr&#261;&#380;enie"
  ]
  node [
    id 107
    label "naczynie"
  ]
  node [
    id 108
    label "nagroda"
  ]
  node [
    id 109
    label "zwyci&#281;stwo"
  ]
  node [
    id 110
    label "zawarto&#347;&#263;"
  ]
  node [
    id 111
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 112
    label "vessel"
  ]
  node [
    id 113
    label "sprz&#281;t"
  ]
  node [
    id 114
    label "statki"
  ]
  node [
    id 115
    label "rewaskularyzacja"
  ]
  node [
    id 116
    label "ceramika"
  ]
  node [
    id 117
    label "drewno"
  ]
  node [
    id 118
    label "przew&#243;d"
  ]
  node [
    id 119
    label "unaczyni&#263;"
  ]
  node [
    id 120
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 121
    label "receptacle"
  ]
  node [
    id 122
    label "oskar"
  ]
  node [
    id 123
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 124
    label "return"
  ]
  node [
    id 125
    label "konsekwencja"
  ]
  node [
    id 126
    label "temat"
  ]
  node [
    id 127
    label "ilo&#347;&#263;"
  ]
  node [
    id 128
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 129
    label "wn&#281;trze"
  ]
  node [
    id 130
    label "informacja"
  ]
  node [
    id 131
    label "beat"
  ]
  node [
    id 132
    label "poradzenie_sobie"
  ]
  node [
    id 133
    label "sukces"
  ]
  node [
    id 134
    label "conquest"
  ]
  node [
    id 135
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 136
    label "samoch&#243;d"
  ]
  node [
    id 137
    label "Syrena"
  ]
  node [
    id 138
    label "pojazd_drogowy"
  ]
  node [
    id 139
    label "spryskiwacz"
  ]
  node [
    id 140
    label "most"
  ]
  node [
    id 141
    label "baga&#380;nik"
  ]
  node [
    id 142
    label "silnik"
  ]
  node [
    id 143
    label "dachowanie"
  ]
  node [
    id 144
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 145
    label "pompa_wodna"
  ]
  node [
    id 146
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 147
    label "poduszka_powietrzna"
  ]
  node [
    id 148
    label "tempomat"
  ]
  node [
    id 149
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 150
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 151
    label "deska_rozdzielcza"
  ]
  node [
    id 152
    label "immobilizer"
  ]
  node [
    id 153
    label "t&#322;umik"
  ]
  node [
    id 154
    label "kierownica"
  ]
  node [
    id 155
    label "ABS"
  ]
  node [
    id 156
    label "bak"
  ]
  node [
    id 157
    label "dwu&#347;lad"
  ]
  node [
    id 158
    label "poci&#261;g_drogowy"
  ]
  node [
    id 159
    label "wycieraczka"
  ]
  node [
    id 160
    label "m&#322;odzie&#380;owo"
  ]
  node [
    id 161
    label "podw&#243;zka"
  ]
  node [
    id 162
    label "okazka"
  ]
  node [
    id 163
    label "oferta"
  ]
  node [
    id 164
    label "autostop"
  ]
  node [
    id 165
    label "atrakcyjny"
  ]
  node [
    id 166
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 167
    label "sytuacja"
  ]
  node [
    id 168
    label "adeptness"
  ]
  node [
    id 169
    label "spotkanie"
  ]
  node [
    id 170
    label "wpuszczenie"
  ]
  node [
    id 171
    label "credence"
  ]
  node [
    id 172
    label "zobowi&#261;zanie_si&#281;"
  ]
  node [
    id 173
    label "dopuszczenie"
  ]
  node [
    id 174
    label "zareagowanie"
  ]
  node [
    id 175
    label "uznanie"
  ]
  node [
    id 176
    label "presumption"
  ]
  node [
    id 177
    label "wzi&#281;cie"
  ]
  node [
    id 178
    label "entertainment"
  ]
  node [
    id 179
    label "przyj&#261;&#263;"
  ]
  node [
    id 180
    label "reception"
  ]
  node [
    id 181
    label "umieszczenie"
  ]
  node [
    id 182
    label "poch&#322;oni&#281;cie"
  ]
  node [
    id 183
    label "zgodzenie_si&#281;"
  ]
  node [
    id 184
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 185
    label "stanie_si&#281;"
  ]
  node [
    id 186
    label "w&#322;&#261;czenie"
  ]
  node [
    id 187
    label "zrobienie"
  ]
  node [
    id 188
    label "czasoumilacz"
  ]
  node [
    id 189
    label "odpoczynek"
  ]
  node [
    id 190
    label "game"
  ]
  node [
    id 191
    label "pi&#322;karsko"
  ]
  node [
    id 192
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 193
    label "typowy"
  ]
  node [
    id 194
    label "sportowy"
  ]
  node [
    id 195
    label "po_pi&#322;karsku"
  ]
  node [
    id 196
    label "specjalny"
  ]
  node [
    id 197
    label "intencjonalny"
  ]
  node [
    id 198
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 199
    label "niedorozw&#243;j"
  ]
  node [
    id 200
    label "szczeg&#243;lny"
  ]
  node [
    id 201
    label "specjalnie"
  ]
  node [
    id 202
    label "nieetatowy"
  ]
  node [
    id 203
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 204
    label "nienormalny"
  ]
  node [
    id 205
    label "umy&#347;lnie"
  ]
  node [
    id 206
    label "odpowiedni"
  ]
  node [
    id 207
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 208
    label "zwyczajny"
  ]
  node [
    id 209
    label "typowo"
  ]
  node [
    id 210
    label "cz&#281;sty"
  ]
  node [
    id 211
    label "zwyk&#322;y"
  ]
  node [
    id 212
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 213
    label "nale&#380;ny"
  ]
  node [
    id 214
    label "nale&#380;yty"
  ]
  node [
    id 215
    label "uprawniony"
  ]
  node [
    id 216
    label "zasadniczy"
  ]
  node [
    id 217
    label "stosownie"
  ]
  node [
    id 218
    label "taki"
  ]
  node [
    id 219
    label "charakterystyczny"
  ]
  node [
    id 220
    label "prawdziwy"
  ]
  node [
    id 221
    label "ten"
  ]
  node [
    id 222
    label "dobry"
  ]
  node [
    id 223
    label "sportowo"
  ]
  node [
    id 224
    label "uczciwy"
  ]
  node [
    id 225
    label "wygodny"
  ]
  node [
    id 226
    label "na_sportowo"
  ]
  node [
    id 227
    label "pe&#322;ny"
  ]
  node [
    id 228
    label "pe&#322;no"
  ]
  node [
    id 229
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 230
    label "krzew"
  ]
  node [
    id 231
    label "delfinidyna"
  ]
  node [
    id 232
    label "pi&#380;maczkowate"
  ]
  node [
    id 233
    label "ki&#347;&#263;"
  ]
  node [
    id 234
    label "hy&#263;ka"
  ]
  node [
    id 235
    label "pestkowiec"
  ]
  node [
    id 236
    label "kwiat"
  ]
  node [
    id 237
    label "ro&#347;lina"
  ]
  node [
    id 238
    label "owoc"
  ]
  node [
    id 239
    label "oliwkowate"
  ]
  node [
    id 240
    label "lilac"
  ]
  node [
    id 241
    label "kostka"
  ]
  node [
    id 242
    label "kita"
  ]
  node [
    id 243
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 244
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 245
    label "d&#322;o&#324;"
  ]
  node [
    id 246
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 247
    label "powerball"
  ]
  node [
    id 248
    label "&#380;ubr"
  ]
  node [
    id 249
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 250
    label "p&#281;k"
  ]
  node [
    id 251
    label "r&#281;ka"
  ]
  node [
    id 252
    label "ogon"
  ]
  node [
    id 253
    label "zako&#324;czenie"
  ]
  node [
    id 254
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 255
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 256
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 257
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 258
    label "flakon"
  ]
  node [
    id 259
    label "przykoronek"
  ]
  node [
    id 260
    label "kielich"
  ]
  node [
    id 261
    label "dno_kwiatowe"
  ]
  node [
    id 262
    label "organ_ro&#347;linny"
  ]
  node [
    id 263
    label "warga"
  ]
  node [
    id 264
    label "korona"
  ]
  node [
    id 265
    label "rurka"
  ]
  node [
    id 266
    label "ozdoba"
  ]
  node [
    id 267
    label "&#322;yko"
  ]
  node [
    id 268
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 269
    label "karczowa&#263;"
  ]
  node [
    id 270
    label "wykarczowanie"
  ]
  node [
    id 271
    label "skupina"
  ]
  node [
    id 272
    label "wykarczowa&#263;"
  ]
  node [
    id 273
    label "karczowanie"
  ]
  node [
    id 274
    label "fanerofit"
  ]
  node [
    id 275
    label "zbiorowisko"
  ]
  node [
    id 276
    label "ro&#347;liny"
  ]
  node [
    id 277
    label "p&#281;d"
  ]
  node [
    id 278
    label "wegetowanie"
  ]
  node [
    id 279
    label "zadziorek"
  ]
  node [
    id 280
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 281
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 282
    label "do&#322;owa&#263;"
  ]
  node [
    id 283
    label "wegetacja"
  ]
  node [
    id 284
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 285
    label "strzyc"
  ]
  node [
    id 286
    label "w&#322;&#243;kno"
  ]
  node [
    id 287
    label "g&#322;uszenie"
  ]
  node [
    id 288
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 289
    label "fitotron"
  ]
  node [
    id 290
    label "bulwka"
  ]
  node [
    id 291
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 292
    label "odn&#243;&#380;ka"
  ]
  node [
    id 293
    label "epiderma"
  ]
  node [
    id 294
    label "gumoza"
  ]
  node [
    id 295
    label "strzy&#380;enie"
  ]
  node [
    id 296
    label "wypotnik"
  ]
  node [
    id 297
    label "flawonoid"
  ]
  node [
    id 298
    label "wyro&#347;le"
  ]
  node [
    id 299
    label "do&#322;owanie"
  ]
  node [
    id 300
    label "g&#322;uszy&#263;"
  ]
  node [
    id 301
    label "pora&#380;a&#263;"
  ]
  node [
    id 302
    label "fitocenoza"
  ]
  node [
    id 303
    label "hodowla"
  ]
  node [
    id 304
    label "fotoautotrof"
  ]
  node [
    id 305
    label "nieuleczalnie_chory"
  ]
  node [
    id 306
    label "wegetowa&#263;"
  ]
  node [
    id 307
    label "pochewka"
  ]
  node [
    id 308
    label "sok"
  ]
  node [
    id 309
    label "system_korzeniowy"
  ]
  node [
    id 310
    label "zawi&#261;zek"
  ]
  node [
    id 311
    label "pestka"
  ]
  node [
    id 312
    label "mi&#261;&#380;sz"
  ]
  node [
    id 313
    label "frukt"
  ]
  node [
    id 314
    label "drylowanie"
  ]
  node [
    id 315
    label "produkt"
  ]
  node [
    id 316
    label "owocnia"
  ]
  node [
    id 317
    label "fruktoza"
  ]
  node [
    id 318
    label "obiekt"
  ]
  node [
    id 319
    label "gniazdo_nasienne"
  ]
  node [
    id 320
    label "rezultat"
  ]
  node [
    id 321
    label "glukoza"
  ]
  node [
    id 322
    label "antocyjanidyn"
  ]
  node [
    id 323
    label "szczeciowce"
  ]
  node [
    id 324
    label "jasnotowce"
  ]
  node [
    id 325
    label "Oleaceae"
  ]
  node [
    id 326
    label "wielkopolski"
  ]
  node [
    id 327
    label "bez_czarny"
  ]
  node [
    id 328
    label "precedent"
  ]
  node [
    id 329
    label "przypadek"
  ]
  node [
    id 330
    label "orzeczenie"
  ]
  node [
    id 331
    label "przebiec"
  ]
  node [
    id 332
    label "charakter"
  ]
  node [
    id 333
    label "czynno&#347;&#263;"
  ]
  node [
    id 334
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 335
    label "motyw"
  ]
  node [
    id 336
    label "przebiegni&#281;cie"
  ]
  node [
    id 337
    label "fabu&#322;a"
  ]
  node [
    id 338
    label "pacjent"
  ]
  node [
    id 339
    label "happening"
  ]
  node [
    id 340
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 341
    label "schorzenie"
  ]
  node [
    id 342
    label "przyk&#322;ad"
  ]
  node [
    id 343
    label "kategoria_gramatyczna"
  ]
  node [
    id 344
    label "przeznaczenie"
  ]
  node [
    id 345
    label "wypowied&#378;"
  ]
  node [
    id 346
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 347
    label "predykatywno&#347;&#263;"
  ]
  node [
    id 348
    label "decyzja"
  ]
  node [
    id 349
    label "piwo"
  ]
  node [
    id 350
    label "uwarzenie"
  ]
  node [
    id 351
    label "warzenie"
  ]
  node [
    id 352
    label "alkohol"
  ]
  node [
    id 353
    label "nap&#243;j"
  ]
  node [
    id 354
    label "bacik"
  ]
  node [
    id 355
    label "wyj&#347;cie"
  ]
  node [
    id 356
    label "uwarzy&#263;"
  ]
  node [
    id 357
    label "birofilia"
  ]
  node [
    id 358
    label "warzy&#263;"
  ]
  node [
    id 359
    label "nawarzy&#263;"
  ]
  node [
    id 360
    label "browarnia"
  ]
  node [
    id 361
    label "nawarzenie"
  ]
  node [
    id 362
    label "anta&#322;"
  ]
  node [
    id 363
    label "jedyny"
  ]
  node [
    id 364
    label "du&#380;y"
  ]
  node [
    id 365
    label "zdr&#243;w"
  ]
  node [
    id 366
    label "calu&#347;ko"
  ]
  node [
    id 367
    label "kompletny"
  ]
  node [
    id 368
    label "&#380;ywy"
  ]
  node [
    id 369
    label "podobny"
  ]
  node [
    id 370
    label "ca&#322;o"
  ]
  node [
    id 371
    label "kompletnie"
  ]
  node [
    id 372
    label "zupe&#322;ny"
  ]
  node [
    id 373
    label "w_pizdu"
  ]
  node [
    id 374
    label "przypominanie"
  ]
  node [
    id 375
    label "podobnie"
  ]
  node [
    id 376
    label "upodabnianie_si&#281;"
  ]
  node [
    id 377
    label "asymilowanie"
  ]
  node [
    id 378
    label "upodobnienie"
  ]
  node [
    id 379
    label "drugi"
  ]
  node [
    id 380
    label "upodobnienie_si&#281;"
  ]
  node [
    id 381
    label "zasymilowanie"
  ]
  node [
    id 382
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 383
    label "ukochany"
  ]
  node [
    id 384
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 385
    label "najlepszy"
  ]
  node [
    id 386
    label "optymalnie"
  ]
  node [
    id 387
    label "doros&#322;y"
  ]
  node [
    id 388
    label "znaczny"
  ]
  node [
    id 389
    label "niema&#322;o"
  ]
  node [
    id 390
    label "wiele"
  ]
  node [
    id 391
    label "rozwini&#281;ty"
  ]
  node [
    id 392
    label "dorodny"
  ]
  node [
    id 393
    label "wa&#380;ny"
  ]
  node [
    id 394
    label "du&#380;o"
  ]
  node [
    id 395
    label "zdrowy"
  ]
  node [
    id 396
    label "ciekawy"
  ]
  node [
    id 397
    label "szybki"
  ]
  node [
    id 398
    label "&#380;ywotny"
  ]
  node [
    id 399
    label "naturalny"
  ]
  node [
    id 400
    label "&#380;ywo"
  ]
  node [
    id 401
    label "cz&#322;owiek"
  ]
  node [
    id 402
    label "o&#380;ywianie"
  ]
  node [
    id 403
    label "&#380;ycie"
  ]
  node [
    id 404
    label "silny"
  ]
  node [
    id 405
    label "g&#322;&#281;boki"
  ]
  node [
    id 406
    label "wyra&#378;ny"
  ]
  node [
    id 407
    label "czynny"
  ]
  node [
    id 408
    label "aktualny"
  ]
  node [
    id 409
    label "zgrabny"
  ]
  node [
    id 410
    label "realistyczny"
  ]
  node [
    id 411
    label "energiczny"
  ]
  node [
    id 412
    label "nieograniczony"
  ]
  node [
    id 413
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 414
    label "satysfakcja"
  ]
  node [
    id 415
    label "bezwzgl&#281;dny"
  ]
  node [
    id 416
    label "otwarty"
  ]
  node [
    id 417
    label "wype&#322;nienie"
  ]
  node [
    id 418
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 419
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 420
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 421
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 422
    label "r&#243;wny"
  ]
  node [
    id 423
    label "nieuszkodzony"
  ]
  node [
    id 424
    label "odpowiednio"
  ]
  node [
    id 425
    label "po_europejsku"
  ]
  node [
    id 426
    label "European"
  ]
  node [
    id 427
    label "europejsko"
  ]
  node [
    id 428
    label "charakterystycznie"
  ]
  node [
    id 429
    label "wyj&#261;tkowy"
  ]
  node [
    id 430
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 431
    label "dogrywa&#263;"
  ]
  node [
    id 432
    label "bezbramkowy"
  ]
  node [
    id 433
    label "faulowa&#263;"
  ]
  node [
    id 434
    label "zamurowanie"
  ]
  node [
    id 435
    label "d&#322;uga_pi&#322;ka"
  ]
  node [
    id 436
    label "ekstraklasa"
  ]
  node [
    id 437
    label "sfaulowa&#263;"
  ]
  node [
    id 438
    label "lobowanie"
  ]
  node [
    id 439
    label "dogrywanie"
  ]
  node [
    id 440
    label "dublet"
  ]
  node [
    id 441
    label "sfaulowanie"
  ]
  node [
    id 442
    label "lobowa&#263;"
  ]
  node [
    id 443
    label "faulowanie"
  ]
  node [
    id 444
    label "bramkarz"
  ]
  node [
    id 445
    label "zamurowywanie"
  ]
  node [
    id 446
    label "kopni&#281;cie"
  ]
  node [
    id 447
    label "kopn&#261;&#263;"
  ]
  node [
    id 448
    label "dogranie"
  ]
  node [
    id 449
    label "kopanie"
  ]
  node [
    id 450
    label "pi&#322;ka"
  ]
  node [
    id 451
    label "przelobowa&#263;"
  ]
  node [
    id 452
    label "mundial"
  ]
  node [
    id 453
    label "kopa&#263;"
  ]
  node [
    id 454
    label "catenaccio"
  ]
  node [
    id 455
    label "dogra&#263;"
  ]
  node [
    id 456
    label "tackle"
  ]
  node [
    id 457
    label "zamurowywa&#263;"
  ]
  node [
    id 458
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 459
    label "interliga"
  ]
  node [
    id 460
    label "przelobowanie"
  ]
  node [
    id 461
    label "czerwona_kartka"
  ]
  node [
    id 462
    label "Wis&#322;a"
  ]
  node [
    id 463
    label "zamurowa&#263;"
  ]
  node [
    id 464
    label "jedenastka"
  ]
  node [
    id 465
    label "kula"
  ]
  node [
    id 466
    label "zaserwowa&#263;"
  ]
  node [
    id 467
    label "zagrywka"
  ]
  node [
    id 468
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 469
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 470
    label "do&#347;rodkowywanie"
  ]
  node [
    id 471
    label "odbicie"
  ]
  node [
    id 472
    label "gra"
  ]
  node [
    id 473
    label "musket_ball"
  ]
  node [
    id 474
    label "aut"
  ]
  node [
    id 475
    label "sport"
  ]
  node [
    id 476
    label "sport_zespo&#322;owy"
  ]
  node [
    id 477
    label "serwowanie"
  ]
  node [
    id 478
    label "orb"
  ]
  node [
    id 479
    label "&#347;wieca"
  ]
  node [
    id 480
    label "zaserwowanie"
  ]
  node [
    id 481
    label "serwowa&#263;"
  ]
  node [
    id 482
    label "rzucanka"
  ]
  node [
    id 483
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 484
    label "remisowy"
  ]
  node [
    id 485
    label "bezbramkowo"
  ]
  node [
    id 486
    label "hokej"
  ]
  node [
    id 487
    label "utworzenie"
  ]
  node [
    id 488
    label "zabudowanie"
  ]
  node [
    id 489
    label "zamkni&#281;cie"
  ]
  node [
    id 490
    label "uciszenie"
  ]
  node [
    id 491
    label "rozegranie"
  ]
  node [
    id 492
    label "siatk&#243;wka"
  ]
  node [
    id 493
    label "koszyk&#243;wka"
  ]
  node [
    id 494
    label "przelecie&#263;"
  ]
  node [
    id 495
    label "przebi&#263;"
  ]
  node [
    id 496
    label "tenis"
  ]
  node [
    id 497
    label "przerzucanie"
  ]
  node [
    id 498
    label "lecie&#263;"
  ]
  node [
    id 499
    label "lob"
  ]
  node [
    id 500
    label "przebija&#263;"
  ]
  node [
    id 501
    label "zabudowywa&#263;"
  ]
  node [
    id 502
    label "rozgrywa&#263;"
  ]
  node [
    id 503
    label "zamyka&#263;"
  ]
  node [
    id 504
    label "murowa&#263;"
  ]
  node [
    id 505
    label "porozumiewanie_si&#281;"
  ]
  node [
    id 506
    label "nagrywanie"
  ]
  node [
    id 507
    label "podawanie"
  ]
  node [
    id 508
    label "ko&#324;czenie"
  ]
  node [
    id 509
    label "hack"
  ]
  node [
    id 510
    label "spulchnia&#263;"
  ]
  node [
    id 511
    label "chow"
  ]
  node [
    id 512
    label "robi&#263;"
  ]
  node [
    id 513
    label "dig"
  ]
  node [
    id 514
    label "d&#243;&#322;"
  ]
  node [
    id 515
    label "krytykowa&#263;"
  ]
  node [
    id 516
    label "wykopywa&#263;"
  ]
  node [
    id 517
    label "uderza&#263;"
  ]
  node [
    id 518
    label "przeszukiwa&#263;"
  ]
  node [
    id 519
    label "foul"
  ]
  node [
    id 520
    label "wygrywa&#263;"
  ]
  node [
    id 521
    label "narusza&#263;"
  ]
  node [
    id 522
    label "podawa&#263;"
  ]
  node [
    id 523
    label "ko&#324;czy&#263;"
  ]
  node [
    id 524
    label "umawia&#263;_si&#281;"
  ]
  node [
    id 525
    label "nagrywa&#263;"
  ]
  node [
    id 526
    label "naruszy&#263;"
  ]
  node [
    id 527
    label "wygra&#263;"
  ]
  node [
    id 528
    label "obrona"
  ]
  node [
    id 529
    label "zawodnik"
  ]
  node [
    id 530
    label "gracz"
  ]
  node [
    id 531
    label "pi&#322;ka_r&#281;czna"
  ]
  node [
    id 532
    label "bileter"
  ]
  node [
    id 533
    label "wykidaj&#322;o"
  ]
  node [
    id 534
    label "extraklasa"
  ]
  node [
    id 535
    label "zbi&#243;r"
  ]
  node [
    id 536
    label "liga"
  ]
  node [
    id 537
    label "grupa"
  ]
  node [
    id 538
    label "krzy&#380;"
  ]
  node [
    id 539
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 540
    label "handwriting"
  ]
  node [
    id 541
    label "gestykulowa&#263;"
  ]
  node [
    id 542
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 543
    label "palec"
  ]
  node [
    id 544
    label "przedrami&#281;"
  ]
  node [
    id 545
    label "cecha"
  ]
  node [
    id 546
    label "hand"
  ]
  node [
    id 547
    label "&#322;okie&#263;"
  ]
  node [
    id 548
    label "hazena"
  ]
  node [
    id 549
    label "nadgarstek"
  ]
  node [
    id 550
    label "graba"
  ]
  node [
    id 551
    label "pracownik"
  ]
  node [
    id 552
    label "r&#261;czyna"
  ]
  node [
    id 553
    label "k&#322;&#261;b"
  ]
  node [
    id 554
    label "chwyta&#263;"
  ]
  node [
    id 555
    label "cmoknonsens"
  ]
  node [
    id 556
    label "pomocnik"
  ]
  node [
    id 557
    label "gestykulowanie"
  ]
  node [
    id 558
    label "chwytanie"
  ]
  node [
    id 559
    label "obietnica"
  ]
  node [
    id 560
    label "spos&#243;b"
  ]
  node [
    id 561
    label "kroki"
  ]
  node [
    id 562
    label "hasta"
  ]
  node [
    id 563
    label "wykroczenie"
  ]
  node [
    id 564
    label "paw"
  ]
  node [
    id 565
    label "rami&#281;"
  ]
  node [
    id 566
    label "dru&#380;yna"
  ]
  node [
    id 567
    label "egzemplarz"
  ]
  node [
    id 568
    label "uk&#322;ad_optyczny"
  ]
  node [
    id 569
    label "kamie&#324;_jubilerski"
  ]
  node [
    id 570
    label "kaftan"
  ]
  node [
    id 571
    label "San"
  ]
  node [
    id 572
    label "Brda"
  ]
  node [
    id 573
    label "Pilica"
  ]
  node [
    id 574
    label "Drw&#281;ca"
  ]
  node [
    id 575
    label "Narew"
  ]
  node [
    id 576
    label "Polska"
  ]
  node [
    id 577
    label "Mot&#322;awa"
  ]
  node [
    id 578
    label "Wis&#322;oka"
  ]
  node [
    id 579
    label "Bzura"
  ]
  node [
    id 580
    label "Wieprz"
  ]
  node [
    id 581
    label "Nida"
  ]
  node [
    id 582
    label "Wda"
  ]
  node [
    id 583
    label "Dunajec"
  ]
  node [
    id 584
    label "Kamienna"
  ]
  node [
    id 585
    label "wygrywanie"
  ]
  node [
    id 586
    label "naruszanie"
  ]
  node [
    id 587
    label "wygranie"
  ]
  node [
    id 588
    label "naruszenie"
  ]
  node [
    id 589
    label "mistrzostwa"
  ]
  node [
    id 590
    label "strategia"
  ]
  node [
    id 591
    label "rozgrywanie"
  ]
  node [
    id 592
    label "murowanie"
  ]
  node [
    id 593
    label "zamykanie"
  ]
  node [
    id 594
    label "tworzenie"
  ]
  node [
    id 595
    label "zabudowywanie"
  ]
  node [
    id 596
    label "sko&#324;czy&#263;"
  ]
  node [
    id 597
    label "poda&#263;"
  ]
  node [
    id 598
    label "nagra&#263;"
  ]
  node [
    id 599
    label "um&#243;wi&#263;_si&#281;"
  ]
  node [
    id 600
    label "porazi&#263;"
  ]
  node [
    id 601
    label "uderzy&#263;"
  ]
  node [
    id 602
    label "przerzucenie"
  ]
  node [
    id 603
    label "podanie"
  ]
  node [
    id 604
    label "nagranie"
  ]
  node [
    id 605
    label "dogadanie_si&#281;"
  ]
  node [
    id 606
    label "liczba"
  ]
  node [
    id 607
    label "rzut_karny"
  ]
  node [
    id 608
    label "odkopanie"
  ]
  node [
    id 609
    label "przeszukiwanie"
  ]
  node [
    id 610
    label "skopywanie"
  ]
  node [
    id 611
    label "pora&#380;anie"
  ]
  node [
    id 612
    label "rozkopanie_si&#281;"
  ]
  node [
    id 613
    label "rozkopywanie_si&#281;"
  ]
  node [
    id 614
    label "excavation"
  ]
  node [
    id 615
    label "rozkopywanie"
  ]
  node [
    id 616
    label "robienie"
  ]
  node [
    id 617
    label "podkopanie_si&#281;"
  ]
  node [
    id 618
    label "odkopanie_si&#281;"
  ]
  node [
    id 619
    label "wykopanie"
  ]
  node [
    id 620
    label "podkopywanie"
  ]
  node [
    id 621
    label "wkopywanie_si&#281;"
  ]
  node [
    id 622
    label "wykopywanie"
  ]
  node [
    id 623
    label "skopanie"
  ]
  node [
    id 624
    label "pokopanie"
  ]
  node [
    id 625
    label "podkopywanie_si&#281;"
  ]
  node [
    id 626
    label "wkopywanie"
  ]
  node [
    id 627
    label "wkopanie"
  ]
  node [
    id 628
    label "wkopanie_si&#281;"
  ]
  node [
    id 629
    label "podkopanie"
  ]
  node [
    id 630
    label "krytykowanie"
  ]
  node [
    id 631
    label "recoil"
  ]
  node [
    id 632
    label "uderzanie"
  ]
  node [
    id 633
    label "odkopywanie_si&#281;"
  ]
  node [
    id 634
    label "odbijanie"
  ]
  node [
    id 635
    label "odkopywanie"
  ]
  node [
    id 636
    label "rozkopanie"
  ]
  node [
    id 637
    label "spulchnianie"
  ]
  node [
    id 638
    label "zabudowa&#263;"
  ]
  node [
    id 639
    label "uciszy&#263;"
  ]
  node [
    id 640
    label "wmurowa&#263;"
  ]
  node [
    id 641
    label "utworzy&#263;"
  ]
  node [
    id 642
    label "rozegra&#263;"
  ]
  node [
    id 643
    label "zamkn&#261;&#263;"
  ]
  node [
    id 644
    label "pora&#380;enie"
  ]
  node [
    id 645
    label "cios"
  ]
  node [
    id 646
    label "uderzenie"
  ]
  node [
    id 647
    label "kopniak"
  ]
  node [
    id 648
    label "kick"
  ]
  node [
    id 649
    label "reserve"
  ]
  node [
    id 650
    label "przej&#347;&#263;"
  ]
  node [
    id 651
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 652
    label "ustawa"
  ]
  node [
    id 653
    label "podlec"
  ]
  node [
    id 654
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 655
    label "min&#261;&#263;"
  ]
  node [
    id 656
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 657
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 658
    label "zaliczy&#263;"
  ]
  node [
    id 659
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 660
    label "zmieni&#263;"
  ]
  node [
    id 661
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 662
    label "przeby&#263;"
  ]
  node [
    id 663
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 664
    label "die"
  ]
  node [
    id 665
    label "dozna&#263;"
  ]
  node [
    id 666
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 667
    label "happen"
  ]
  node [
    id 668
    label "pass"
  ]
  node [
    id 669
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 670
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 671
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 672
    label "mienie"
  ]
  node [
    id 673
    label "absorb"
  ]
  node [
    id 674
    label "pique"
  ]
  node [
    id 675
    label "przesta&#263;"
  ]
  node [
    id 676
    label "serw"
  ]
  node [
    id 677
    label "dwumecz"
  ]
  node [
    id 678
    label "zmienno&#347;&#263;"
  ]
  node [
    id 679
    label "play"
  ]
  node [
    id 680
    label "apparent_motion"
  ]
  node [
    id 681
    label "akcja"
  ]
  node [
    id 682
    label "komplet"
  ]
  node [
    id 683
    label "zabawa"
  ]
  node [
    id 684
    label "zasada"
  ]
  node [
    id 685
    label "zbijany"
  ]
  node [
    id 686
    label "post&#281;powanie"
  ]
  node [
    id 687
    label "odg&#322;os"
  ]
  node [
    id 688
    label "Pok&#233;mon"
  ]
  node [
    id 689
    label "synteza"
  ]
  node [
    id 690
    label "odtworzenie"
  ]
  node [
    id 691
    label "rekwizyt_do_gry"
  ]
  node [
    id 692
    label "egzamin"
  ]
  node [
    id 693
    label "poj&#281;cie"
  ]
  node [
    id 694
    label "protection"
  ]
  node [
    id 695
    label "poparcie"
  ]
  node [
    id 696
    label "reakcja"
  ]
  node [
    id 697
    label "defense"
  ]
  node [
    id 698
    label "s&#261;d"
  ]
  node [
    id 699
    label "auspices"
  ]
  node [
    id 700
    label "ochrona"
  ]
  node [
    id 701
    label "wojsko"
  ]
  node [
    id 702
    label "manewr"
  ]
  node [
    id 703
    label "defensive_structure"
  ]
  node [
    id 704
    label "guard_duty"
  ]
  node [
    id 705
    label "strona"
  ]
  node [
    id 706
    label "warunek_lokalowy"
  ]
  node [
    id 707
    label "plac"
  ]
  node [
    id 708
    label "location"
  ]
  node [
    id 709
    label "uwaga"
  ]
  node [
    id 710
    label "przestrze&#324;"
  ]
  node [
    id 711
    label "status"
  ]
  node [
    id 712
    label "chwila"
  ]
  node [
    id 713
    label "cia&#322;o"
  ]
  node [
    id 714
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 715
    label "praca"
  ]
  node [
    id 716
    label "rz&#261;d"
  ]
  node [
    id 717
    label "charakterystyka"
  ]
  node [
    id 718
    label "m&#322;ot"
  ]
  node [
    id 719
    label "znak"
  ]
  node [
    id 720
    label "drzewo"
  ]
  node [
    id 721
    label "pr&#243;ba"
  ]
  node [
    id 722
    label "attribute"
  ]
  node [
    id 723
    label "marka"
  ]
  node [
    id 724
    label "Rzym_Zachodni"
  ]
  node [
    id 725
    label "whole"
  ]
  node [
    id 726
    label "element"
  ]
  node [
    id 727
    label "Rzym_Wschodni"
  ]
  node [
    id 728
    label "urz&#261;dzenie"
  ]
  node [
    id 729
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 730
    label "stan"
  ]
  node [
    id 731
    label "nagana"
  ]
  node [
    id 732
    label "tekst"
  ]
  node [
    id 733
    label "upomnienie"
  ]
  node [
    id 734
    label "dzienniczek"
  ]
  node [
    id 735
    label "wzgl&#261;d"
  ]
  node [
    id 736
    label "gossip"
  ]
  node [
    id 737
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 738
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 739
    label "najem"
  ]
  node [
    id 740
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 741
    label "zak&#322;ad"
  ]
  node [
    id 742
    label "stosunek_pracy"
  ]
  node [
    id 743
    label "benedykty&#324;ski"
  ]
  node [
    id 744
    label "poda&#380;_pracy"
  ]
  node [
    id 745
    label "pracowanie"
  ]
  node [
    id 746
    label "tyrka"
  ]
  node [
    id 747
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 748
    label "wytw&#243;r"
  ]
  node [
    id 749
    label "zaw&#243;d"
  ]
  node [
    id 750
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 751
    label "tynkarski"
  ]
  node [
    id 752
    label "pracowa&#263;"
  ]
  node [
    id 753
    label "zmiana"
  ]
  node [
    id 754
    label "czynnik_produkcji"
  ]
  node [
    id 755
    label "zobowi&#261;zanie"
  ]
  node [
    id 756
    label "kierownictwo"
  ]
  node [
    id 757
    label "siedziba"
  ]
  node [
    id 758
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 759
    label "rozdzielanie"
  ]
  node [
    id 760
    label "bezbrze&#380;e"
  ]
  node [
    id 761
    label "punkt"
  ]
  node [
    id 762
    label "czasoprzestrze&#324;"
  ]
  node [
    id 763
    label "niezmierzony"
  ]
  node [
    id 764
    label "przedzielenie"
  ]
  node [
    id 765
    label "nielito&#347;ciwy"
  ]
  node [
    id 766
    label "rozdziela&#263;"
  ]
  node [
    id 767
    label "oktant"
  ]
  node [
    id 768
    label "przedzieli&#263;"
  ]
  node [
    id 769
    label "przestw&#243;r"
  ]
  node [
    id 770
    label "condition"
  ]
  node [
    id 771
    label "awansowa&#263;"
  ]
  node [
    id 772
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 773
    label "znaczenie"
  ]
  node [
    id 774
    label "awans"
  ]
  node [
    id 775
    label "podmiotowo"
  ]
  node [
    id 776
    label "awansowanie"
  ]
  node [
    id 777
    label "time"
  ]
  node [
    id 778
    label "rozmiar"
  ]
  node [
    id 779
    label "circumference"
  ]
  node [
    id 780
    label "leksem"
  ]
  node [
    id 781
    label "cyrkumferencja"
  ]
  node [
    id 782
    label "ekshumowanie"
  ]
  node [
    id 783
    label "jednostka_organizacyjna"
  ]
  node [
    id 784
    label "p&#322;aszczyzna"
  ]
  node [
    id 785
    label "odwadnia&#263;"
  ]
  node [
    id 786
    label "zabalsamowanie"
  ]
  node [
    id 787
    label "zesp&#243;&#322;"
  ]
  node [
    id 788
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 789
    label "odwodni&#263;"
  ]
  node [
    id 790
    label "sk&#243;ra"
  ]
  node [
    id 791
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 792
    label "staw"
  ]
  node [
    id 793
    label "ow&#322;osienie"
  ]
  node [
    id 794
    label "mi&#281;so"
  ]
  node [
    id 795
    label "zabalsamowa&#263;"
  ]
  node [
    id 796
    label "Izba_Konsyliarska"
  ]
  node [
    id 797
    label "unerwienie"
  ]
  node [
    id 798
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 799
    label "kremacja"
  ]
  node [
    id 800
    label "biorytm"
  ]
  node [
    id 801
    label "sekcja"
  ]
  node [
    id 802
    label "istota_&#380;ywa"
  ]
  node [
    id 803
    label "otworzy&#263;"
  ]
  node [
    id 804
    label "otwiera&#263;"
  ]
  node [
    id 805
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 806
    label "otworzenie"
  ]
  node [
    id 807
    label "materia"
  ]
  node [
    id 808
    label "pochowanie"
  ]
  node [
    id 809
    label "otwieranie"
  ]
  node [
    id 810
    label "ty&#322;"
  ]
  node [
    id 811
    label "szkielet"
  ]
  node [
    id 812
    label "tanatoplastyk"
  ]
  node [
    id 813
    label "odwadnianie"
  ]
  node [
    id 814
    label "Komitet_Region&#243;w"
  ]
  node [
    id 815
    label "odwodnienie"
  ]
  node [
    id 816
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 817
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 818
    label "nieumar&#322;y"
  ]
  node [
    id 819
    label "pochowa&#263;"
  ]
  node [
    id 820
    label "balsamowa&#263;"
  ]
  node [
    id 821
    label "tanatoplastyka"
  ]
  node [
    id 822
    label "temperatura"
  ]
  node [
    id 823
    label "ekshumowa&#263;"
  ]
  node [
    id 824
    label "balsamowanie"
  ]
  node [
    id 825
    label "uk&#322;ad"
  ]
  node [
    id 826
    label "prz&#243;d"
  ]
  node [
    id 827
    label "l&#281;d&#378;wie"
  ]
  node [
    id 828
    label "cz&#322;onek"
  ]
  node [
    id 829
    label "pogrzeb"
  ]
  node [
    id 830
    label "&#321;ubianka"
  ]
  node [
    id 831
    label "area"
  ]
  node [
    id 832
    label "Majdan"
  ]
  node [
    id 833
    label "pole_bitwy"
  ]
  node [
    id 834
    label "stoisko"
  ]
  node [
    id 835
    label "obszar"
  ]
  node [
    id 836
    label "pierzeja"
  ]
  node [
    id 837
    label "obiekt_handlowy"
  ]
  node [
    id 838
    label "zgromadzenie"
  ]
  node [
    id 839
    label "miasto"
  ]
  node [
    id 840
    label "targowica"
  ]
  node [
    id 841
    label "kram"
  ]
  node [
    id 842
    label "przybli&#380;enie"
  ]
  node [
    id 843
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 844
    label "kategoria"
  ]
  node [
    id 845
    label "szpaler"
  ]
  node [
    id 846
    label "lon&#380;a"
  ]
  node [
    id 847
    label "uporz&#261;dkowanie"
  ]
  node [
    id 848
    label "egzekutywa"
  ]
  node [
    id 849
    label "jednostka_systematyczna"
  ]
  node [
    id 850
    label "instytucja"
  ]
  node [
    id 851
    label "premier"
  ]
  node [
    id 852
    label "Londyn"
  ]
  node [
    id 853
    label "gabinet_cieni"
  ]
  node [
    id 854
    label "gromada"
  ]
  node [
    id 855
    label "number"
  ]
  node [
    id 856
    label "Konsulat"
  ]
  node [
    id 857
    label "tract"
  ]
  node [
    id 858
    label "klasa"
  ]
  node [
    id 859
    label "w&#322;adza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 246
  ]
  edge [
    source 11
    target 247
  ]
  edge [
    source 11
    target 248
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 250
  ]
  edge [
    source 11
    target 251
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 260
  ]
  edge [
    source 11
    target 261
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 285
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 11
    target 287
  ]
  edge [
    source 11
    target 288
  ]
  edge [
    source 11
    target 289
  ]
  edge [
    source 11
    target 290
  ]
  edge [
    source 11
    target 291
  ]
  edge [
    source 11
    target 292
  ]
  edge [
    source 11
    target 293
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 295
  ]
  edge [
    source 11
    target 296
  ]
  edge [
    source 11
    target 297
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 300
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 11
    target 309
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 11
    target 311
  ]
  edge [
    source 11
    target 312
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 331
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 335
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 337
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 349
  ]
  edge [
    source 15
    target 350
  ]
  edge [
    source 15
    target 351
  ]
  edge [
    source 15
    target 352
  ]
  edge [
    source 15
    target 353
  ]
  edge [
    source 15
    target 354
  ]
  edge [
    source 15
    target 355
  ]
  edge [
    source 15
    target 356
  ]
  edge [
    source 15
    target 357
  ]
  edge [
    source 15
    target 358
  ]
  edge [
    source 15
    target 359
  ]
  edge [
    source 15
    target 360
  ]
  edge [
    source 15
    target 361
  ]
  edge [
    source 15
    target 362
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 363
  ]
  edge [
    source 17
    target 364
  ]
  edge [
    source 17
    target 365
  ]
  edge [
    source 17
    target 366
  ]
  edge [
    source 17
    target 367
  ]
  edge [
    source 17
    target 368
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 369
  ]
  edge [
    source 17
    target 370
  ]
  edge [
    source 17
    target 371
  ]
  edge [
    source 17
    target 372
  ]
  edge [
    source 17
    target 373
  ]
  edge [
    source 17
    target 374
  ]
  edge [
    source 17
    target 375
  ]
  edge [
    source 17
    target 376
  ]
  edge [
    source 17
    target 377
  ]
  edge [
    source 17
    target 378
  ]
  edge [
    source 17
    target 379
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 219
  ]
  edge [
    source 17
    target 380
  ]
  edge [
    source 17
    target 381
  ]
  edge [
    source 17
    target 382
  ]
  edge [
    source 17
    target 383
  ]
  edge [
    source 17
    target 384
  ]
  edge [
    source 17
    target 385
  ]
  edge [
    source 17
    target 386
  ]
  edge [
    source 17
    target 387
  ]
  edge [
    source 17
    target 388
  ]
  edge [
    source 17
    target 389
  ]
  edge [
    source 17
    target 390
  ]
  edge [
    source 17
    target 391
  ]
  edge [
    source 17
    target 392
  ]
  edge [
    source 17
    target 393
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 394
  ]
  edge [
    source 17
    target 395
  ]
  edge [
    source 17
    target 396
  ]
  edge [
    source 17
    target 397
  ]
  edge [
    source 17
    target 398
  ]
  edge [
    source 17
    target 399
  ]
  edge [
    source 17
    target 400
  ]
  edge [
    source 17
    target 401
  ]
  edge [
    source 17
    target 402
  ]
  edge [
    source 17
    target 403
  ]
  edge [
    source 17
    target 404
  ]
  edge [
    source 17
    target 405
  ]
  edge [
    source 17
    target 406
  ]
  edge [
    source 17
    target 407
  ]
  edge [
    source 17
    target 408
  ]
  edge [
    source 17
    target 409
  ]
  edge [
    source 17
    target 410
  ]
  edge [
    source 17
    target 411
  ]
  edge [
    source 17
    target 412
  ]
  edge [
    source 17
    target 413
  ]
  edge [
    source 17
    target 414
  ]
  edge [
    source 17
    target 415
  ]
  edge [
    source 17
    target 416
  ]
  edge [
    source 17
    target 417
  ]
  edge [
    source 17
    target 418
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 419
  ]
  edge [
    source 17
    target 420
  ]
  edge [
    source 17
    target 421
  ]
  edge [
    source 17
    target 422
  ]
  edge [
    source 17
    target 423
  ]
  edge [
    source 17
    target 424
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 425
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 426
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 18
    target 219
  ]
  edge [
    source 18
    target 427
  ]
  edge [
    source 18
    target 428
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 429
  ]
  edge [
    source 18
    target 430
  ]
  edge [
    source 18
    target 369
  ]
  edge [
    source 18
    target 208
  ]
  edge [
    source 18
    target 209
  ]
  edge [
    source 18
    target 210
  ]
  edge [
    source 18
    target 211
  ]
  edge [
    source 18
    target 212
  ]
  edge [
    source 18
    target 213
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 18
    target 216
  ]
  edge [
    source 18
    target 217
  ]
  edge [
    source 18
    target 218
  ]
  edge [
    source 18
    target 220
  ]
  edge [
    source 18
    target 221
  ]
  edge [
    source 18
    target 222
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 431
  ]
  edge [
    source 19
    target 432
  ]
  edge [
    source 19
    target 433
  ]
  edge [
    source 19
    target 434
  ]
  edge [
    source 19
    target 435
  ]
  edge [
    source 19
    target 436
  ]
  edge [
    source 19
    target 437
  ]
  edge [
    source 19
    target 438
  ]
  edge [
    source 19
    target 439
  ]
  edge [
    source 19
    target 440
  ]
  edge [
    source 19
    target 441
  ]
  edge [
    source 19
    target 442
  ]
  edge [
    source 19
    target 443
  ]
  edge [
    source 19
    target 444
  ]
  edge [
    source 19
    target 445
  ]
  edge [
    source 19
    target 446
  ]
  edge [
    source 19
    target 447
  ]
  edge [
    source 19
    target 448
  ]
  edge [
    source 19
    target 449
  ]
  edge [
    source 19
    target 450
  ]
  edge [
    source 19
    target 451
  ]
  edge [
    source 19
    target 452
  ]
  edge [
    source 19
    target 453
  ]
  edge [
    source 19
    target 251
  ]
  edge [
    source 19
    target 454
  ]
  edge [
    source 19
    target 455
  ]
  edge [
    source 19
    target 456
  ]
  edge [
    source 19
    target 457
  ]
  edge [
    source 19
    target 458
  ]
  edge [
    source 19
    target 459
  ]
  edge [
    source 19
    target 460
  ]
  edge [
    source 19
    target 461
  ]
  edge [
    source 19
    target 462
  ]
  edge [
    source 19
    target 463
  ]
  edge [
    source 19
    target 464
  ]
  edge [
    source 19
    target 465
  ]
  edge [
    source 19
    target 466
  ]
  edge [
    source 19
    target 467
  ]
  edge [
    source 19
    target 468
  ]
  edge [
    source 19
    target 469
  ]
  edge [
    source 19
    target 470
  ]
  edge [
    source 19
    target 471
  ]
  edge [
    source 19
    target 472
  ]
  edge [
    source 19
    target 473
  ]
  edge [
    source 19
    target 474
  ]
  edge [
    source 19
    target 475
  ]
  edge [
    source 19
    target 476
  ]
  edge [
    source 19
    target 477
  ]
  edge [
    source 19
    target 478
  ]
  edge [
    source 19
    target 479
  ]
  edge [
    source 19
    target 480
  ]
  edge [
    source 19
    target 481
  ]
  edge [
    source 19
    target 482
  ]
  edge [
    source 19
    target 483
  ]
  edge [
    source 19
    target 484
  ]
  edge [
    source 19
    target 485
  ]
  edge [
    source 19
    target 486
  ]
  edge [
    source 19
    target 487
  ]
  edge [
    source 19
    target 488
  ]
  edge [
    source 19
    target 489
  ]
  edge [
    source 19
    target 490
  ]
  edge [
    source 19
    target 491
  ]
  edge [
    source 19
    target 492
  ]
  edge [
    source 19
    target 493
  ]
  edge [
    source 19
    target 494
  ]
  edge [
    source 19
    target 495
  ]
  edge [
    source 19
    target 496
  ]
  edge [
    source 19
    target 497
  ]
  edge [
    source 19
    target 498
  ]
  edge [
    source 19
    target 499
  ]
  edge [
    source 19
    target 500
  ]
  edge [
    source 19
    target 501
  ]
  edge [
    source 19
    target 502
  ]
  edge [
    source 19
    target 503
  ]
  edge [
    source 19
    target 504
  ]
  edge [
    source 19
    target 505
  ]
  edge [
    source 19
    target 506
  ]
  edge [
    source 19
    target 507
  ]
  edge [
    source 19
    target 508
  ]
  edge [
    source 19
    target 509
  ]
  edge [
    source 19
    target 510
  ]
  edge [
    source 19
    target 511
  ]
  edge [
    source 19
    target 512
  ]
  edge [
    source 19
    target 513
  ]
  edge [
    source 19
    target 514
  ]
  edge [
    source 19
    target 515
  ]
  edge [
    source 19
    target 301
  ]
  edge [
    source 19
    target 516
  ]
  edge [
    source 19
    target 517
  ]
  edge [
    source 19
    target 518
  ]
  edge [
    source 19
    target 519
  ]
  edge [
    source 19
    target 520
  ]
  edge [
    source 19
    target 521
  ]
  edge [
    source 19
    target 522
  ]
  edge [
    source 19
    target 523
  ]
  edge [
    source 19
    target 524
  ]
  edge [
    source 19
    target 525
  ]
  edge [
    source 19
    target 526
  ]
  edge [
    source 19
    target 527
  ]
  edge [
    source 19
    target 528
  ]
  edge [
    source 19
    target 529
  ]
  edge [
    source 19
    target 530
  ]
  edge [
    source 19
    target 531
  ]
  edge [
    source 19
    target 532
  ]
  edge [
    source 19
    target 533
  ]
  edge [
    source 19
    target 534
  ]
  edge [
    source 19
    target 535
  ]
  edge [
    source 19
    target 536
  ]
  edge [
    source 19
    target 537
  ]
  edge [
    source 19
    target 538
  ]
  edge [
    source 19
    target 539
  ]
  edge [
    source 19
    target 540
  ]
  edge [
    source 19
    target 245
  ]
  edge [
    source 19
    target 541
  ]
  edge [
    source 19
    target 542
  ]
  edge [
    source 19
    target 543
  ]
  edge [
    source 19
    target 544
  ]
  edge [
    source 19
    target 545
  ]
  edge [
    source 19
    target 546
  ]
  edge [
    source 19
    target 547
  ]
  edge [
    source 19
    target 548
  ]
  edge [
    source 19
    target 549
  ]
  edge [
    source 19
    target 550
  ]
  edge [
    source 19
    target 551
  ]
  edge [
    source 19
    target 552
  ]
  edge [
    source 19
    target 553
  ]
  edge [
    source 19
    target 554
  ]
  edge [
    source 19
    target 555
  ]
  edge [
    source 19
    target 556
  ]
  edge [
    source 19
    target 557
  ]
  edge [
    source 19
    target 558
  ]
  edge [
    source 19
    target 559
  ]
  edge [
    source 19
    target 560
  ]
  edge [
    source 19
    target 561
  ]
  edge [
    source 19
    target 562
  ]
  edge [
    source 19
    target 563
  ]
  edge [
    source 19
    target 564
  ]
  edge [
    source 19
    target 565
  ]
  edge [
    source 19
    target 566
  ]
  edge [
    source 19
    target 567
  ]
  edge [
    source 19
    target 568
  ]
  edge [
    source 19
    target 569
  ]
  edge [
    source 19
    target 570
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 571
  ]
  edge [
    source 19
    target 572
  ]
  edge [
    source 19
    target 573
  ]
  edge [
    source 19
    target 574
  ]
  edge [
    source 19
    target 575
  ]
  edge [
    source 19
    target 576
  ]
  edge [
    source 19
    target 577
  ]
  edge [
    source 19
    target 578
  ]
  edge [
    source 19
    target 579
  ]
  edge [
    source 19
    target 580
  ]
  edge [
    source 19
    target 581
  ]
  edge [
    source 19
    target 582
  ]
  edge [
    source 19
    target 583
  ]
  edge [
    source 19
    target 584
  ]
  edge [
    source 19
    target 585
  ]
  edge [
    source 19
    target 586
  ]
  edge [
    source 19
    target 587
  ]
  edge [
    source 19
    target 588
  ]
  edge [
    source 19
    target 589
  ]
  edge [
    source 19
    target 590
  ]
  edge [
    source 19
    target 591
  ]
  edge [
    source 19
    target 592
  ]
  edge [
    source 19
    target 593
  ]
  edge [
    source 19
    target 594
  ]
  edge [
    source 19
    target 595
  ]
  edge [
    source 19
    target 596
  ]
  edge [
    source 19
    target 597
  ]
  edge [
    source 19
    target 598
  ]
  edge [
    source 19
    target 599
  ]
  edge [
    source 19
    target 600
  ]
  edge [
    source 19
    target 601
  ]
  edge [
    source 19
    target 602
  ]
  edge [
    source 19
    target 603
  ]
  edge [
    source 19
    target 604
  ]
  edge [
    source 19
    target 253
  ]
  edge [
    source 19
    target 605
  ]
  edge [
    source 19
    target 606
  ]
  edge [
    source 19
    target 607
  ]
  edge [
    source 19
    target 318
  ]
  edge [
    source 19
    target 608
  ]
  edge [
    source 19
    target 609
  ]
  edge [
    source 19
    target 610
  ]
  edge [
    source 19
    target 611
  ]
  edge [
    source 19
    target 612
  ]
  edge [
    source 19
    target 613
  ]
  edge [
    source 19
    target 614
  ]
  edge [
    source 19
    target 615
  ]
  edge [
    source 19
    target 616
  ]
  edge [
    source 19
    target 617
  ]
  edge [
    source 19
    target 618
  ]
  edge [
    source 19
    target 619
  ]
  edge [
    source 19
    target 620
  ]
  edge [
    source 19
    target 621
  ]
  edge [
    source 19
    target 622
  ]
  edge [
    source 19
    target 623
  ]
  edge [
    source 19
    target 624
  ]
  edge [
    source 19
    target 625
  ]
  edge [
    source 19
    target 626
  ]
  edge [
    source 19
    target 627
  ]
  edge [
    source 19
    target 628
  ]
  edge [
    source 19
    target 629
  ]
  edge [
    source 19
    target 630
  ]
  edge [
    source 19
    target 631
  ]
  edge [
    source 19
    target 632
  ]
  edge [
    source 19
    target 633
  ]
  edge [
    source 19
    target 634
  ]
  edge [
    source 19
    target 635
  ]
  edge [
    source 19
    target 636
  ]
  edge [
    source 19
    target 637
  ]
  edge [
    source 19
    target 638
  ]
  edge [
    source 19
    target 639
  ]
  edge [
    source 19
    target 640
  ]
  edge [
    source 19
    target 641
  ]
  edge [
    source 19
    target 642
  ]
  edge [
    source 19
    target 643
  ]
  edge [
    source 19
    target 644
  ]
  edge [
    source 19
    target 645
  ]
  edge [
    source 19
    target 646
  ]
  edge [
    source 19
    target 647
  ]
  edge [
    source 19
    target 648
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 649
  ]
  edge [
    source 21
    target 650
  ]
  edge [
    source 21
    target 651
  ]
  edge [
    source 21
    target 652
  ]
  edge [
    source 21
    target 653
  ]
  edge [
    source 21
    target 654
  ]
  edge [
    source 21
    target 655
  ]
  edge [
    source 21
    target 656
  ]
  edge [
    source 21
    target 657
  ]
  edge [
    source 21
    target 658
  ]
  edge [
    source 21
    target 659
  ]
  edge [
    source 21
    target 660
  ]
  edge [
    source 21
    target 661
  ]
  edge [
    source 21
    target 662
  ]
  edge [
    source 21
    target 663
  ]
  edge [
    source 21
    target 664
  ]
  edge [
    source 21
    target 665
  ]
  edge [
    source 21
    target 666
  ]
  edge [
    source 21
    target 36
  ]
  edge [
    source 21
    target 667
  ]
  edge [
    source 21
    target 668
  ]
  edge [
    source 21
    target 669
  ]
  edge [
    source 21
    target 670
  ]
  edge [
    source 21
    target 671
  ]
  edge [
    source 21
    target 131
  ]
  edge [
    source 21
    target 672
  ]
  edge [
    source 21
    target 673
  ]
  edge [
    source 21
    target 44
  ]
  edge [
    source 21
    target 674
  ]
  edge [
    source 21
    target 675
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 528
  ]
  edge [
    source 22
    target 472
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 676
  ]
  edge [
    source 22
    target 677
  ]
  edge [
    source 22
    target 678
  ]
  edge [
    source 22
    target 679
  ]
  edge [
    source 22
    target 102
  ]
  edge [
    source 22
    target 680
  ]
  edge [
    source 22
    target 81
  ]
  edge [
    source 22
    target 80
  ]
  edge [
    source 22
    target 681
  ]
  edge [
    source 22
    target 682
  ]
  edge [
    source 22
    target 683
  ]
  edge [
    source 22
    target 684
  ]
  edge [
    source 22
    target 71
  ]
  edge [
    source 22
    target 685
  ]
  edge [
    source 22
    target 686
  ]
  edge [
    source 22
    target 687
  ]
  edge [
    source 22
    target 688
  ]
  edge [
    source 22
    target 333
  ]
  edge [
    source 22
    target 689
  ]
  edge [
    source 22
    target 690
  ]
  edge [
    source 22
    target 691
  ]
  edge [
    source 22
    target 692
  ]
  edge [
    source 22
    target 89
  ]
  edge [
    source 22
    target 536
  ]
  edge [
    source 22
    target 530
  ]
  edge [
    source 22
    target 693
  ]
  edge [
    source 22
    target 694
  ]
  edge [
    source 22
    target 695
  ]
  edge [
    source 22
    target 696
  ]
  edge [
    source 22
    target 697
  ]
  edge [
    source 22
    target 698
  ]
  edge [
    source 22
    target 699
  ]
  edge [
    source 22
    target 700
  ]
  edge [
    source 22
    target 96
  ]
  edge [
    source 22
    target 701
  ]
  edge [
    source 22
    target 702
  ]
  edge [
    source 22
    target 703
  ]
  edge [
    source 22
    target 704
  ]
  edge [
    source 22
    target 705
  ]
  edge [
    source 22
    target 646
  ]
  edge [
    source 23
    target 706
  ]
  edge [
    source 23
    target 707
  ]
  edge [
    source 23
    target 708
  ]
  edge [
    source 23
    target 709
  ]
  edge [
    source 23
    target 710
  ]
  edge [
    source 23
    target 711
  ]
  edge [
    source 23
    target 128
  ]
  edge [
    source 23
    target 712
  ]
  edge [
    source 23
    target 713
  ]
  edge [
    source 23
    target 545
  ]
  edge [
    source 23
    target 714
  ]
  edge [
    source 23
    target 715
  ]
  edge [
    source 23
    target 716
  ]
  edge [
    source 23
    target 717
  ]
  edge [
    source 23
    target 718
  ]
  edge [
    source 23
    target 719
  ]
  edge [
    source 23
    target 720
  ]
  edge [
    source 23
    target 721
  ]
  edge [
    source 23
    target 722
  ]
  edge [
    source 23
    target 723
  ]
  edge [
    source 23
    target 724
  ]
  edge [
    source 23
    target 725
  ]
  edge [
    source 23
    target 127
  ]
  edge [
    source 23
    target 726
  ]
  edge [
    source 23
    target 727
  ]
  edge [
    source 23
    target 728
  ]
  edge [
    source 23
    target 345
  ]
  edge [
    source 23
    target 729
  ]
  edge [
    source 23
    target 730
  ]
  edge [
    source 23
    target 731
  ]
  edge [
    source 23
    target 732
  ]
  edge [
    source 23
    target 733
  ]
  edge [
    source 23
    target 734
  ]
  edge [
    source 23
    target 735
  ]
  edge [
    source 23
    target 736
  ]
  edge [
    source 23
    target 737
  ]
  edge [
    source 23
    target 738
  ]
  edge [
    source 23
    target 739
  ]
  edge [
    source 23
    target 740
  ]
  edge [
    source 23
    target 741
  ]
  edge [
    source 23
    target 742
  ]
  edge [
    source 23
    target 743
  ]
  edge [
    source 23
    target 744
  ]
  edge [
    source 23
    target 745
  ]
  edge [
    source 23
    target 746
  ]
  edge [
    source 23
    target 747
  ]
  edge [
    source 23
    target 748
  ]
  edge [
    source 23
    target 749
  ]
  edge [
    source 23
    target 750
  ]
  edge [
    source 23
    target 751
  ]
  edge [
    source 23
    target 752
  ]
  edge [
    source 23
    target 333
  ]
  edge [
    source 23
    target 753
  ]
  edge [
    source 23
    target 754
  ]
  edge [
    source 23
    target 755
  ]
  edge [
    source 23
    target 756
  ]
  edge [
    source 23
    target 757
  ]
  edge [
    source 23
    target 758
  ]
  edge [
    source 23
    target 759
  ]
  edge [
    source 23
    target 760
  ]
  edge [
    source 23
    target 761
  ]
  edge [
    source 23
    target 762
  ]
  edge [
    source 23
    target 535
  ]
  edge [
    source 23
    target 763
  ]
  edge [
    source 23
    target 764
  ]
  edge [
    source 23
    target 765
  ]
  edge [
    source 23
    target 766
  ]
  edge [
    source 23
    target 767
  ]
  edge [
    source 23
    target 768
  ]
  edge [
    source 23
    target 769
  ]
  edge [
    source 23
    target 770
  ]
  edge [
    source 23
    target 771
  ]
  edge [
    source 23
    target 772
  ]
  edge [
    source 23
    target 773
  ]
  edge [
    source 23
    target 774
  ]
  edge [
    source 23
    target 775
  ]
  edge [
    source 23
    target 776
  ]
  edge [
    source 23
    target 167
  ]
  edge [
    source 23
    target 777
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 778
  ]
  edge [
    source 23
    target 606
  ]
  edge [
    source 23
    target 779
  ]
  edge [
    source 23
    target 780
  ]
  edge [
    source 23
    target 781
  ]
  edge [
    source 23
    target 705
  ]
  edge [
    source 23
    target 782
  ]
  edge [
    source 23
    target 783
  ]
  edge [
    source 23
    target 784
  ]
  edge [
    source 23
    target 785
  ]
  edge [
    source 23
    target 786
  ]
  edge [
    source 23
    target 787
  ]
  edge [
    source 23
    target 788
  ]
  edge [
    source 23
    target 789
  ]
  edge [
    source 23
    target 790
  ]
  edge [
    source 23
    target 791
  ]
  edge [
    source 23
    target 792
  ]
  edge [
    source 23
    target 793
  ]
  edge [
    source 23
    target 794
  ]
  edge [
    source 23
    target 795
  ]
  edge [
    source 23
    target 796
  ]
  edge [
    source 23
    target 797
  ]
  edge [
    source 23
    target 798
  ]
  edge [
    source 23
    target 799
  ]
  edge [
    source 23
    target 800
  ]
  edge [
    source 23
    target 801
  ]
  edge [
    source 23
    target 802
  ]
  edge [
    source 23
    target 803
  ]
  edge [
    source 23
    target 804
  ]
  edge [
    source 23
    target 805
  ]
  edge [
    source 23
    target 806
  ]
  edge [
    source 23
    target 807
  ]
  edge [
    source 23
    target 808
  ]
  edge [
    source 23
    target 809
  ]
  edge [
    source 23
    target 810
  ]
  edge [
    source 23
    target 811
  ]
  edge [
    source 23
    target 812
  ]
  edge [
    source 23
    target 813
  ]
  edge [
    source 23
    target 814
  ]
  edge [
    source 23
    target 815
  ]
  edge [
    source 23
    target 816
  ]
  edge [
    source 23
    target 817
  ]
  edge [
    source 23
    target 818
  ]
  edge [
    source 23
    target 819
  ]
  edge [
    source 23
    target 820
  ]
  edge [
    source 23
    target 821
  ]
  edge [
    source 23
    target 822
  ]
  edge [
    source 23
    target 823
  ]
  edge [
    source 23
    target 824
  ]
  edge [
    source 23
    target 825
  ]
  edge [
    source 23
    target 826
  ]
  edge [
    source 23
    target 827
  ]
  edge [
    source 23
    target 828
  ]
  edge [
    source 23
    target 829
  ]
  edge [
    source 23
    target 830
  ]
  edge [
    source 23
    target 831
  ]
  edge [
    source 23
    target 832
  ]
  edge [
    source 23
    target 833
  ]
  edge [
    source 23
    target 834
  ]
  edge [
    source 23
    target 835
  ]
  edge [
    source 23
    target 836
  ]
  edge [
    source 23
    target 837
  ]
  edge [
    source 23
    target 838
  ]
  edge [
    source 23
    target 839
  ]
  edge [
    source 23
    target 840
  ]
  edge [
    source 23
    target 841
  ]
  edge [
    source 23
    target 842
  ]
  edge [
    source 23
    target 843
  ]
  edge [
    source 23
    target 844
  ]
  edge [
    source 23
    target 845
  ]
  edge [
    source 23
    target 846
  ]
  edge [
    source 23
    target 847
  ]
  edge [
    source 23
    target 848
  ]
  edge [
    source 23
    target 849
  ]
  edge [
    source 23
    target 850
  ]
  edge [
    source 23
    target 851
  ]
  edge [
    source 23
    target 852
  ]
  edge [
    source 23
    target 853
  ]
  edge [
    source 23
    target 854
  ]
  edge [
    source 23
    target 855
  ]
  edge [
    source 23
    target 856
  ]
  edge [
    source 23
    target 857
  ]
  edge [
    source 23
    target 858
  ]
  edge [
    source 23
    target 859
  ]
]
