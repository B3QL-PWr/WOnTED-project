graph [
  node [
    id 0
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "w&#261;tpliwy"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "autor"
    origin "text"
  ]
  node [
    id 4
    label "pseudo"
    origin "text"
  ]
  node [
    id 5
    label "graffiti"
    origin "text"
  ]
  node [
    id 6
    label "polak"
    origin "text"
  ]
  node [
    id 7
    label "w&#261;tpliwie"
  ]
  node [
    id 8
    label "pozorny"
  ]
  node [
    id 9
    label "pozornie"
  ]
  node [
    id 10
    label "nieprawdziwy"
  ]
  node [
    id 11
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 12
    label "mie&#263;_miejsce"
  ]
  node [
    id 13
    label "equal"
  ]
  node [
    id 14
    label "trwa&#263;"
  ]
  node [
    id 15
    label "chodzi&#263;"
  ]
  node [
    id 16
    label "si&#281;ga&#263;"
  ]
  node [
    id 17
    label "stan"
  ]
  node [
    id 18
    label "obecno&#347;&#263;"
  ]
  node [
    id 19
    label "stand"
  ]
  node [
    id 20
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 21
    label "uczestniczy&#263;"
  ]
  node [
    id 22
    label "participate"
  ]
  node [
    id 23
    label "robi&#263;"
  ]
  node [
    id 24
    label "istnie&#263;"
  ]
  node [
    id 25
    label "pozostawa&#263;"
  ]
  node [
    id 26
    label "zostawa&#263;"
  ]
  node [
    id 27
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 28
    label "adhere"
  ]
  node [
    id 29
    label "compass"
  ]
  node [
    id 30
    label "korzysta&#263;"
  ]
  node [
    id 31
    label "appreciation"
  ]
  node [
    id 32
    label "osi&#261;ga&#263;"
  ]
  node [
    id 33
    label "dociera&#263;"
  ]
  node [
    id 34
    label "get"
  ]
  node [
    id 35
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 36
    label "mierzy&#263;"
  ]
  node [
    id 37
    label "u&#380;ywa&#263;"
  ]
  node [
    id 38
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 39
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 40
    label "exsert"
  ]
  node [
    id 41
    label "being"
  ]
  node [
    id 42
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 43
    label "cecha"
  ]
  node [
    id 44
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 45
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 46
    label "p&#322;ywa&#263;"
  ]
  node [
    id 47
    label "run"
  ]
  node [
    id 48
    label "bangla&#263;"
  ]
  node [
    id 49
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 50
    label "przebiega&#263;"
  ]
  node [
    id 51
    label "wk&#322;ada&#263;"
  ]
  node [
    id 52
    label "proceed"
  ]
  node [
    id 53
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 54
    label "carry"
  ]
  node [
    id 55
    label "bywa&#263;"
  ]
  node [
    id 56
    label "dziama&#263;"
  ]
  node [
    id 57
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 58
    label "stara&#263;_si&#281;"
  ]
  node [
    id 59
    label "para"
  ]
  node [
    id 60
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 61
    label "str&#243;j"
  ]
  node [
    id 62
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 63
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 64
    label "krok"
  ]
  node [
    id 65
    label "tryb"
  ]
  node [
    id 66
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 67
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 68
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 69
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 70
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 71
    label "continue"
  ]
  node [
    id 72
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 73
    label "Ohio"
  ]
  node [
    id 74
    label "wci&#281;cie"
  ]
  node [
    id 75
    label "Nowy_York"
  ]
  node [
    id 76
    label "warstwa"
  ]
  node [
    id 77
    label "samopoczucie"
  ]
  node [
    id 78
    label "Illinois"
  ]
  node [
    id 79
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 80
    label "state"
  ]
  node [
    id 81
    label "Jukatan"
  ]
  node [
    id 82
    label "Kalifornia"
  ]
  node [
    id 83
    label "Wirginia"
  ]
  node [
    id 84
    label "wektor"
  ]
  node [
    id 85
    label "Teksas"
  ]
  node [
    id 86
    label "Goa"
  ]
  node [
    id 87
    label "Waszyngton"
  ]
  node [
    id 88
    label "miejsce"
  ]
  node [
    id 89
    label "Massachusetts"
  ]
  node [
    id 90
    label "Alaska"
  ]
  node [
    id 91
    label "Arakan"
  ]
  node [
    id 92
    label "Hawaje"
  ]
  node [
    id 93
    label "Maryland"
  ]
  node [
    id 94
    label "punkt"
  ]
  node [
    id 95
    label "Michigan"
  ]
  node [
    id 96
    label "Arizona"
  ]
  node [
    id 97
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 98
    label "Georgia"
  ]
  node [
    id 99
    label "poziom"
  ]
  node [
    id 100
    label "Pensylwania"
  ]
  node [
    id 101
    label "shape"
  ]
  node [
    id 102
    label "Luizjana"
  ]
  node [
    id 103
    label "Nowy_Meksyk"
  ]
  node [
    id 104
    label "Alabama"
  ]
  node [
    id 105
    label "ilo&#347;&#263;"
  ]
  node [
    id 106
    label "Kansas"
  ]
  node [
    id 107
    label "Oregon"
  ]
  node [
    id 108
    label "Floryda"
  ]
  node [
    id 109
    label "Oklahoma"
  ]
  node [
    id 110
    label "jednostka_administracyjna"
  ]
  node [
    id 111
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 112
    label "kszta&#322;ciciel"
  ]
  node [
    id 113
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 114
    label "tworzyciel"
  ]
  node [
    id 115
    label "wykonawca"
  ]
  node [
    id 116
    label "pomys&#322;odawca"
  ]
  node [
    id 117
    label "&#347;w"
  ]
  node [
    id 118
    label "inicjator"
  ]
  node [
    id 119
    label "podmiot_gospodarczy"
  ]
  node [
    id 120
    label "artysta"
  ]
  node [
    id 121
    label "cz&#322;owiek"
  ]
  node [
    id 122
    label "muzyk"
  ]
  node [
    id 123
    label "nauczyciel"
  ]
  node [
    id 124
    label "mural"
  ]
  node [
    id 125
    label "malowid&#322;o"
  ]
  node [
    id 126
    label "graffito"
  ]
  node [
    id 127
    label "picture"
  ]
  node [
    id 128
    label "obraz"
  ]
  node [
    id 129
    label "malarstwo_na&#347;cienne"
  ]
  node [
    id 130
    label "polski"
  ]
  node [
    id 131
    label "przedmiot"
  ]
  node [
    id 132
    label "Polish"
  ]
  node [
    id 133
    label "goniony"
  ]
  node [
    id 134
    label "oberek"
  ]
  node [
    id 135
    label "ryba_po_grecku"
  ]
  node [
    id 136
    label "sztajer"
  ]
  node [
    id 137
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 138
    label "krakowiak"
  ]
  node [
    id 139
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 140
    label "pierogi_ruskie"
  ]
  node [
    id 141
    label "lacki"
  ]
  node [
    id 142
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 143
    label "chodzony"
  ]
  node [
    id 144
    label "po_polsku"
  ]
  node [
    id 145
    label "mazur"
  ]
  node [
    id 146
    label "polsko"
  ]
  node [
    id 147
    label "skoczny"
  ]
  node [
    id 148
    label "drabant"
  ]
  node [
    id 149
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 150
    label "j&#281;zyk"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
]
