graph [
  node [
    id 0
    label "wchodzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "autobus"
    origin "text"
  ]
  node [
    id 2
    label "pks"
    origin "text"
  ]
  node [
    id 3
    label "bilet"
    origin "text"
  ]
  node [
    id 4
    label "miejscowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 6
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 7
    label "zaziera&#263;"
  ]
  node [
    id 8
    label "move"
  ]
  node [
    id 9
    label "zaczyna&#263;"
  ]
  node [
    id 10
    label "spotyka&#263;"
  ]
  node [
    id 11
    label "przenika&#263;"
  ]
  node [
    id 12
    label "osi&#261;ga&#263;"
  ]
  node [
    id 13
    label "nast&#281;powa&#263;"
  ]
  node [
    id 14
    label "mount"
  ]
  node [
    id 15
    label "bra&#263;"
  ]
  node [
    id 16
    label "go"
  ]
  node [
    id 17
    label "&#322;oi&#263;"
  ]
  node [
    id 18
    label "intervene"
  ]
  node [
    id 19
    label "scale"
  ]
  node [
    id 20
    label "poznawa&#263;"
  ]
  node [
    id 21
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 22
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 23
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 24
    label "dochodzi&#263;"
  ]
  node [
    id 25
    label "przekracza&#263;"
  ]
  node [
    id 26
    label "wnika&#263;"
  ]
  node [
    id 27
    label "atakowa&#263;"
  ]
  node [
    id 28
    label "invade"
  ]
  node [
    id 29
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 30
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 31
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 32
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 33
    label "strike"
  ]
  node [
    id 34
    label "robi&#263;"
  ]
  node [
    id 35
    label "schorzenie"
  ]
  node [
    id 36
    label "dzia&#322;a&#263;"
  ]
  node [
    id 37
    label "ofensywny"
  ]
  node [
    id 38
    label "przewaga"
  ]
  node [
    id 39
    label "sport"
  ]
  node [
    id 40
    label "epidemia"
  ]
  node [
    id 41
    label "attack"
  ]
  node [
    id 42
    label "rozgrywa&#263;"
  ]
  node [
    id 43
    label "krytykowa&#263;"
  ]
  node [
    id 44
    label "walczy&#263;"
  ]
  node [
    id 45
    label "aim"
  ]
  node [
    id 46
    label "trouble_oneself"
  ]
  node [
    id 47
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 48
    label "napada&#263;"
  ]
  node [
    id 49
    label "m&#243;wi&#263;"
  ]
  node [
    id 50
    label "usi&#322;owa&#263;"
  ]
  node [
    id 51
    label "ograniczenie"
  ]
  node [
    id 52
    label "przebywa&#263;"
  ]
  node [
    id 53
    label "conflict"
  ]
  node [
    id 54
    label "transgress"
  ]
  node [
    id 55
    label "appear"
  ]
  node [
    id 56
    label "mija&#263;"
  ]
  node [
    id 57
    label "zawiera&#263;"
  ]
  node [
    id 58
    label "cognizance"
  ]
  node [
    id 59
    label "szkoli&#263;_si&#281;"
  ]
  node [
    id 60
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 61
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 62
    label "go_steady"
  ]
  node [
    id 63
    label "detect"
  ]
  node [
    id 64
    label "make"
  ]
  node [
    id 65
    label "rozr&#243;&#380;nia&#263;"
  ]
  node [
    id 66
    label "hurt"
  ]
  node [
    id 67
    label "styka&#263;_si&#281;"
  ]
  node [
    id 68
    label "trespass"
  ]
  node [
    id 69
    label "transpire"
  ]
  node [
    id 70
    label "naciska&#263;"
  ]
  node [
    id 71
    label "mie&#263;_miejsce"
  ]
  node [
    id 72
    label "alternate"
  ]
  node [
    id 73
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 74
    label "chance"
  ]
  node [
    id 75
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 76
    label "uzyskiwa&#263;"
  ]
  node [
    id 77
    label "dociera&#263;"
  ]
  node [
    id 78
    label "mark"
  ]
  node [
    id 79
    label "get"
  ]
  node [
    id 80
    label "claim"
  ]
  node [
    id 81
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 82
    label "ripen"
  ]
  node [
    id 83
    label "supervene"
  ]
  node [
    id 84
    label "doczeka&#263;"
  ]
  node [
    id 85
    label "przesy&#322;ka"
  ]
  node [
    id 86
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 87
    label "doznawa&#263;"
  ]
  node [
    id 88
    label "reach"
  ]
  node [
    id 89
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 90
    label "zachodzi&#263;"
  ]
  node [
    id 91
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 92
    label "postrzega&#263;"
  ]
  node [
    id 93
    label "orgazm"
  ]
  node [
    id 94
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 95
    label "dokoptowywa&#263;"
  ]
  node [
    id 96
    label "dolatywa&#263;"
  ]
  node [
    id 97
    label "powodowa&#263;"
  ]
  node [
    id 98
    label "submit"
  ]
  node [
    id 99
    label "odejmowa&#263;"
  ]
  node [
    id 100
    label "bankrupt"
  ]
  node [
    id 101
    label "open"
  ]
  node [
    id 102
    label "set_about"
  ]
  node [
    id 103
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 104
    label "begin"
  ]
  node [
    id 105
    label "post&#281;powa&#263;"
  ]
  node [
    id 106
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 107
    label "fall"
  ]
  node [
    id 108
    label "znajdowa&#263;"
  ]
  node [
    id 109
    label "happen"
  ]
  node [
    id 110
    label "goban"
  ]
  node [
    id 111
    label "gra_planszowa"
  ]
  node [
    id 112
    label "sport_umys&#322;owy"
  ]
  node [
    id 113
    label "chi&#324;ski"
  ]
  node [
    id 114
    label "zagl&#261;da&#263;"
  ]
  node [
    id 115
    label "wpada&#263;"
  ]
  node [
    id 116
    label "pokonywa&#263;"
  ]
  node [
    id 117
    label "nat&#322;uszcza&#263;"
  ]
  node [
    id 118
    label "je&#378;dzi&#263;"
  ]
  node [
    id 119
    label "peddle"
  ]
  node [
    id 120
    label "obgadywa&#263;"
  ]
  node [
    id 121
    label "bi&#263;"
  ]
  node [
    id 122
    label "wspina&#263;_si&#281;"
  ]
  node [
    id 123
    label "naci&#261;ga&#263;"
  ]
  node [
    id 124
    label "gra&#263;"
  ]
  node [
    id 125
    label "tankowa&#263;"
  ]
  node [
    id 126
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 127
    label "bang"
  ]
  node [
    id 128
    label "domy&#347;la&#263;_si&#281;"
  ]
  node [
    id 129
    label "drench"
  ]
  node [
    id 130
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 131
    label "meet"
  ]
  node [
    id 132
    label "substancja"
  ]
  node [
    id 133
    label "saturate"
  ]
  node [
    id 134
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 135
    label "tworzy&#263;"
  ]
  node [
    id 136
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 137
    label "porywa&#263;"
  ]
  node [
    id 138
    label "korzysta&#263;"
  ]
  node [
    id 139
    label "take"
  ]
  node [
    id 140
    label "poczytywa&#263;"
  ]
  node [
    id 141
    label "levy"
  ]
  node [
    id 142
    label "wk&#322;ada&#263;"
  ]
  node [
    id 143
    label "raise"
  ]
  node [
    id 144
    label "by&#263;"
  ]
  node [
    id 145
    label "przyjmowa&#263;"
  ]
  node [
    id 146
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 147
    label "rucha&#263;"
  ]
  node [
    id 148
    label "prowadzi&#263;"
  ]
  node [
    id 149
    label "za&#380;ywa&#263;"
  ]
  node [
    id 150
    label "otrzymywa&#263;"
  ]
  node [
    id 151
    label "&#263;pa&#263;"
  ]
  node [
    id 152
    label "interpretowa&#263;"
  ]
  node [
    id 153
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 154
    label "dostawa&#263;"
  ]
  node [
    id 155
    label "rusza&#263;"
  ]
  node [
    id 156
    label "chwyta&#263;"
  ]
  node [
    id 157
    label "grza&#263;"
  ]
  node [
    id 158
    label "wch&#322;ania&#263;"
  ]
  node [
    id 159
    label "wygrywa&#263;"
  ]
  node [
    id 160
    label "u&#380;ywa&#263;"
  ]
  node [
    id 161
    label "ucieka&#263;"
  ]
  node [
    id 162
    label "arise"
  ]
  node [
    id 163
    label "uprawia&#263;_seks"
  ]
  node [
    id 164
    label "abstract"
  ]
  node [
    id 165
    label "towarzystwo"
  ]
  node [
    id 166
    label "branie"
  ]
  node [
    id 167
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 168
    label "zalicza&#263;"
  ]
  node [
    id 169
    label "wzi&#261;&#263;"
  ]
  node [
    id 170
    label "&#322;apa&#263;"
  ]
  node [
    id 171
    label "przewa&#380;a&#263;"
  ]
  node [
    id 172
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 173
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 174
    label "samoch&#243;d"
  ]
  node [
    id 175
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 176
    label "pojazd_drogowy"
  ]
  node [
    id 177
    label "spryskiwacz"
  ]
  node [
    id 178
    label "most"
  ]
  node [
    id 179
    label "baga&#380;nik"
  ]
  node [
    id 180
    label "silnik"
  ]
  node [
    id 181
    label "dachowanie"
  ]
  node [
    id 182
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 183
    label "pompa_wodna"
  ]
  node [
    id 184
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 185
    label "poduszka_powietrzna"
  ]
  node [
    id 186
    label "tempomat"
  ]
  node [
    id 187
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 188
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 189
    label "deska_rozdzielcza"
  ]
  node [
    id 190
    label "immobilizer"
  ]
  node [
    id 191
    label "t&#322;umik"
  ]
  node [
    id 192
    label "ABS"
  ]
  node [
    id 193
    label "kierownica"
  ]
  node [
    id 194
    label "bak"
  ]
  node [
    id 195
    label "dwu&#347;lad"
  ]
  node [
    id 196
    label "poci&#261;g_drogowy"
  ]
  node [
    id 197
    label "wycieraczka"
  ]
  node [
    id 198
    label "karta_wst&#281;pu"
  ]
  node [
    id 199
    label "konik"
  ]
  node [
    id 200
    label "passe-partout"
  ]
  node [
    id 201
    label "cedu&#322;a"
  ]
  node [
    id 202
    label "bordiura"
  ]
  node [
    id 203
    label "kolej"
  ]
  node [
    id 204
    label "raport"
  ]
  node [
    id 205
    label "transport"
  ]
  node [
    id 206
    label "kurs"
  ]
  node [
    id 207
    label "spis"
  ]
  node [
    id 208
    label "zaj&#281;cie"
  ]
  node [
    id 209
    label "cyka&#263;"
  ]
  node [
    id 210
    label "zabawka"
  ]
  node [
    id 211
    label "figura"
  ]
  node [
    id 212
    label "szara&#324;czak"
  ]
  node [
    id 213
    label "grasshopper"
  ]
  node [
    id 214
    label "fondness"
  ]
  node [
    id 215
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 216
    label "mechanizm"
  ]
  node [
    id 217
    label "za&#322;atwiacz"
  ]
  node [
    id 218
    label "Aurignac"
  ]
  node [
    id 219
    label "Sabaudia"
  ]
  node [
    id 220
    label "Cecora"
  ]
  node [
    id 221
    label "Saint-Acheul"
  ]
  node [
    id 222
    label "Boulogne"
  ]
  node [
    id 223
    label "Opat&#243;wek"
  ]
  node [
    id 224
    label "osiedle"
  ]
  node [
    id 225
    label "Levallois-Perret"
  ]
  node [
    id 226
    label "Szcz&#281;&#347;liwice"
  ]
  node [
    id 227
    label "Jelcz"
  ]
  node [
    id 228
    label "Kaw&#281;czyn"
  ]
  node [
    id 229
    label "Br&#243;dno"
  ]
  node [
    id 230
    label "Marysin"
  ]
  node [
    id 231
    label "Ochock"
  ]
  node [
    id 232
    label "Kabaty"
  ]
  node [
    id 233
    label "Paw&#322;owice"
  ]
  node [
    id 234
    label "Falenica"
  ]
  node [
    id 235
    label "Osobowice"
  ]
  node [
    id 236
    label "Wielopole"
  ]
  node [
    id 237
    label "Boryszew"
  ]
  node [
    id 238
    label "Chojny"
  ]
  node [
    id 239
    label "Szack"
  ]
  node [
    id 240
    label "Powsin"
  ]
  node [
    id 241
    label "Bielice"
  ]
  node [
    id 242
    label "Wi&#347;niowiec"
  ]
  node [
    id 243
    label "Branice"
  ]
  node [
    id 244
    label "Rej&#243;w"
  ]
  node [
    id 245
    label "Zerze&#324;"
  ]
  node [
    id 246
    label "Rakowiec"
  ]
  node [
    id 247
    label "osadnictwo"
  ]
  node [
    id 248
    label "Jelonki"
  ]
  node [
    id 249
    label "Gronik"
  ]
  node [
    id 250
    label "Horodyszcze"
  ]
  node [
    id 251
    label "S&#281;polno"
  ]
  node [
    id 252
    label "Salwator"
  ]
  node [
    id 253
    label "Mariensztat"
  ]
  node [
    id 254
    label "Lubiesz&#243;w"
  ]
  node [
    id 255
    label "Izborsk"
  ]
  node [
    id 256
    label "Orunia"
  ]
  node [
    id 257
    label "Opor&#243;w"
  ]
  node [
    id 258
    label "Miedzeszyn"
  ]
  node [
    id 259
    label "Nadodrze"
  ]
  node [
    id 260
    label "Natolin"
  ]
  node [
    id 261
    label "Wi&#347;niewo"
  ]
  node [
    id 262
    label "Wojn&#243;w"
  ]
  node [
    id 263
    label "Ujazd&#243;w"
  ]
  node [
    id 264
    label "Solec"
  ]
  node [
    id 265
    label "Biskupin"
  ]
  node [
    id 266
    label "G&#243;rce"
  ]
  node [
    id 267
    label "Siersza"
  ]
  node [
    id 268
    label "Wawrzyszew"
  ]
  node [
    id 269
    label "&#321;agiewniki"
  ]
  node [
    id 270
    label "Azory"
  ]
  node [
    id 271
    label "&#379;erniki"
  ]
  node [
    id 272
    label "jednostka_administracyjna"
  ]
  node [
    id 273
    label "Goc&#322;aw"
  ]
  node [
    id 274
    label "Latycz&#243;w"
  ]
  node [
    id 275
    label "Micha&#322;owo"
  ]
  node [
    id 276
    label "zesp&#243;&#322;"
  ]
  node [
    id 277
    label "Broch&#243;w"
  ]
  node [
    id 278
    label "jednostka_osadnicza"
  ]
  node [
    id 279
    label "M&#322;ociny"
  ]
  node [
    id 280
    label "Groch&#243;w"
  ]
  node [
    id 281
    label "dzielnica"
  ]
  node [
    id 282
    label "Laskowice_O&#322;awskie"
  ]
  node [
    id 283
    label "Marysin_Wawerski"
  ]
  node [
    id 284
    label "Le&#347;nica"
  ]
  node [
    id 285
    label "Kortowo"
  ]
  node [
    id 286
    label "G&#322;uszyna"
  ]
  node [
    id 287
    label "Kar&#322;owice"
  ]
  node [
    id 288
    label "Kujbyszewe"
  ]
  node [
    id 289
    label "Tarchomin"
  ]
  node [
    id 290
    label "&#379;era&#324;"
  ]
  node [
    id 291
    label "Jasienica"
  ]
  node [
    id 292
    label "Ok&#281;cie"
  ]
  node [
    id 293
    label "Zakrz&#243;w"
  ]
  node [
    id 294
    label "G&#243;rczyn"
  ]
  node [
    id 295
    label "Powi&#347;le"
  ]
  node [
    id 296
    label "Lewin&#243;w"
  ]
  node [
    id 297
    label "Gutkowo"
  ]
  node [
    id 298
    label "Wad&#243;w"
  ]
  node [
    id 299
    label "grupa"
  ]
  node [
    id 300
    label "Gr&#281;ba&#322;&#243;w"
  ]
  node [
    id 301
    label "Dojlidy"
  ]
  node [
    id 302
    label "Marymont"
  ]
  node [
    id 303
    label "Rataje"
  ]
  node [
    id 304
    label "Grabiszyn"
  ]
  node [
    id 305
    label "Szczytniki"
  ]
  node [
    id 306
    label "Anin"
  ]
  node [
    id 307
    label "Imielin"
  ]
  node [
    id 308
    label "siedziba"
  ]
  node [
    id 309
    label "Zalesie"
  ]
  node [
    id 310
    label "Arsk"
  ]
  node [
    id 311
    label "Bogucice"
  ]
  node [
    id 312
    label "kultura_aszelska"
  ]
  node [
    id 313
    label "Francja"
  ]
  node [
    id 314
    label "jednostka_monetarna"
  ]
  node [
    id 315
    label "wspania&#322;y"
  ]
  node [
    id 316
    label "metaliczny"
  ]
  node [
    id 317
    label "Polska"
  ]
  node [
    id 318
    label "szlachetny"
  ]
  node [
    id 319
    label "kochany"
  ]
  node [
    id 320
    label "doskona&#322;y"
  ]
  node [
    id 321
    label "grosz"
  ]
  node [
    id 322
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 323
    label "poz&#322;ocenie"
  ]
  node [
    id 324
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 325
    label "utytu&#322;owany"
  ]
  node [
    id 326
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 327
    label "z&#322;ocenie"
  ]
  node [
    id 328
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 329
    label "prominentny"
  ]
  node [
    id 330
    label "znany"
  ]
  node [
    id 331
    label "wybitny"
  ]
  node [
    id 332
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 333
    label "naj"
  ]
  node [
    id 334
    label "&#347;wietny"
  ]
  node [
    id 335
    label "pe&#322;ny"
  ]
  node [
    id 336
    label "doskonale"
  ]
  node [
    id 337
    label "szlachetnie"
  ]
  node [
    id 338
    label "uczciwy"
  ]
  node [
    id 339
    label "zacny"
  ]
  node [
    id 340
    label "harmonijny"
  ]
  node [
    id 341
    label "gatunkowy"
  ]
  node [
    id 342
    label "pi&#281;kny"
  ]
  node [
    id 343
    label "dobry"
  ]
  node [
    id 344
    label "typowy"
  ]
  node [
    id 345
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 346
    label "metaloplastyczny"
  ]
  node [
    id 347
    label "metalicznie"
  ]
  node [
    id 348
    label "kochanek"
  ]
  node [
    id 349
    label "wybranek"
  ]
  node [
    id 350
    label "umi&#322;owany"
  ]
  node [
    id 351
    label "drogi"
  ]
  node [
    id 352
    label "kochanie"
  ]
  node [
    id 353
    label "wspaniale"
  ]
  node [
    id 354
    label "pomy&#347;lny"
  ]
  node [
    id 355
    label "pozytywny"
  ]
  node [
    id 356
    label "&#347;wietnie"
  ]
  node [
    id 357
    label "spania&#322;y"
  ]
  node [
    id 358
    label "och&#281;do&#380;ny"
  ]
  node [
    id 359
    label "warto&#347;ciowy"
  ]
  node [
    id 360
    label "zajebisty"
  ]
  node [
    id 361
    label "bogato"
  ]
  node [
    id 362
    label "typ_mongoloidalny"
  ]
  node [
    id 363
    label "kolorowy"
  ]
  node [
    id 364
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 365
    label "ciep&#322;y"
  ]
  node [
    id 366
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 367
    label "jasny"
  ]
  node [
    id 368
    label "kwota"
  ]
  node [
    id 369
    label "groszak"
  ]
  node [
    id 370
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 371
    label "szyling_austryjacki"
  ]
  node [
    id 372
    label "moneta"
  ]
  node [
    id 373
    label "Mazowsze"
  ]
  node [
    id 374
    label "Pa&#322;uki"
  ]
  node [
    id 375
    label "Pomorze_Zachodnie"
  ]
  node [
    id 376
    label "Wolin"
  ]
  node [
    id 377
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 378
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 379
    label "So&#322;a"
  ]
  node [
    id 380
    label "Unia_Europejska"
  ]
  node [
    id 381
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 382
    label "Opolskie"
  ]
  node [
    id 383
    label "Suwalszczyzna"
  ]
  node [
    id 384
    label "Krajna"
  ]
  node [
    id 385
    label "barwy_polskie"
  ]
  node [
    id 386
    label "Nadbu&#380;e"
  ]
  node [
    id 387
    label "Podlasie"
  ]
  node [
    id 388
    label "Izera"
  ]
  node [
    id 389
    label "Ma&#322;opolska"
  ]
  node [
    id 390
    label "Warmia"
  ]
  node [
    id 391
    label "Mazury"
  ]
  node [
    id 392
    label "NATO"
  ]
  node [
    id 393
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 394
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 395
    label "Lubelszczyzna"
  ]
  node [
    id 396
    label "Kaczawa"
  ]
  node [
    id 397
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 398
    label "Kielecczyzna"
  ]
  node [
    id 399
    label "Lubuskie"
  ]
  node [
    id 400
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 401
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 402
    label "&#321;&#243;dzkie"
  ]
  node [
    id 403
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 404
    label "Kujawy"
  ]
  node [
    id 405
    label "Podkarpacie"
  ]
  node [
    id 406
    label "Wielkopolska"
  ]
  node [
    id 407
    label "Wis&#322;a"
  ]
  node [
    id 408
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 409
    label "Bory_Tucholskie"
  ]
  node [
    id 410
    label "platerowanie"
  ]
  node [
    id 411
    label "z&#322;ocisty"
  ]
  node [
    id 412
    label "barwienie"
  ]
  node [
    id 413
    label "gilt"
  ]
  node [
    id 414
    label "plating"
  ]
  node [
    id 415
    label "zdobienie"
  ]
  node [
    id 416
    label "club"
  ]
  node [
    id 417
    label "powleczenie"
  ]
  node [
    id 418
    label "zabarwienie"
  ]
  node [
    id 419
    label "PKS"
  ]
  node [
    id 420
    label "do"
  ]
  node [
    id 421
    label "X"
  ]
  node [
    id 422
    label "9"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 419
    target 420
  ]
  edge [
    source 419
    target 421
  ]
  edge [
    source 419
    target 422
  ]
  edge [
    source 420
    target 421
  ]
  edge [
    source 420
    target 422
  ]
  edge [
    source 421
    target 422
  ]
]
