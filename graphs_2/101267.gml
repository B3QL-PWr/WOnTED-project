graph [
  node [
    id 0
    label "szaniec"
    origin "text"
  ]
  node [
    id 1
    label "kresowy"
    origin "text"
  ]
  node [
    id 2
    label "wa&#322;"
  ]
  node [
    id 3
    label "r&#243;w"
  ]
  node [
    id 4
    label "redoubt"
  ]
  node [
    id 5
    label "fortyfikacja"
  ]
  node [
    id 6
    label "budowa"
  ]
  node [
    id 7
    label "zrzutowy"
  ]
  node [
    id 8
    label "odk&#322;ad"
  ]
  node [
    id 9
    label "chody"
  ]
  node [
    id 10
    label "budowla"
  ]
  node [
    id 11
    label "obni&#380;enie"
  ]
  node [
    id 12
    label "przedpiersie"
  ]
  node [
    id 13
    label "formacja_geologiczna"
  ]
  node [
    id 14
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 15
    label "odwa&#322;"
  ]
  node [
    id 16
    label "grodzisko"
  ]
  node [
    id 17
    label "blinda&#380;"
  ]
  node [
    id 18
    label "roller"
  ]
  node [
    id 19
    label "narz&#281;dzie"
  ]
  node [
    id 20
    label "przewa&#322;"
  ]
  node [
    id 21
    label "usypisko"
  ]
  node [
    id 22
    label "naiwniak"
  ]
  node [
    id 23
    label "chutzpa"
  ]
  node [
    id 24
    label "pojazd_mechaniczny"
  ]
  node [
    id 25
    label "maszyna_robocza"
  ]
  node [
    id 26
    label "pojazd_budowlany"
  ]
  node [
    id 27
    label "emblemat"
  ]
  node [
    id 28
    label "maszyna"
  ]
  node [
    id 29
    label "rotating_shaft"
  ]
  node [
    id 30
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 31
    label "post&#281;pek"
  ]
  node [
    id 32
    label "obwa&#322;owanie"
  ]
  node [
    id 33
    label "narys_bastionowy"
  ]
  node [
    id 34
    label "fosa"
  ]
  node [
    id 35
    label "fort"
  ]
  node [
    id 36
    label "machiku&#322;"
  ]
  node [
    id 37
    label "kazamata"
  ]
  node [
    id 38
    label "bastion"
  ]
  node [
    id 39
    label "kurtyna"
  ]
  node [
    id 40
    label "barykada"
  ]
  node [
    id 41
    label "przedbramie"
  ]
  node [
    id 42
    label "baszta"
  ]
  node [
    id 43
    label "transzeja"
  ]
  node [
    id 44
    label "zamek"
  ]
  node [
    id 45
    label "palisada"
  ]
  node [
    id 46
    label "in&#380;ynieria"
  ]
  node [
    id 47
    label "okop"
  ]
  node [
    id 48
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 49
    label "&#347;r&#243;dszaniec"
  ]
  node [
    id 50
    label "warenik"
  ]
  node [
    id 51
    label "wschodni"
  ]
  node [
    id 52
    label "blin"
  ]
  node [
    id 53
    label "syrniki"
  ]
  node [
    id 54
    label "piero&#380;ek"
  ]
  node [
    id 55
    label "placek"
  ]
  node [
    id 56
    label "dro&#380;d&#380;owy"
  ]
  node [
    id 57
    label "m&#261;czny"
  ]
  node [
    id 58
    label "okr&#281;g"
  ]
  node [
    id 59
    label "XIV"
  ]
  node [
    id 60
    label "lwowski"
  ]
  node [
    id 61
    label "narodowy"
  ]
  node [
    id 62
    label "si&#322;y"
  ]
  node [
    id 63
    label "zbrojny"
  ]
  node [
    id 64
    label "grupa"
  ]
  node [
    id 65
    label "sza&#324;c"
  ]
  node [
    id 66
    label "propaganda"
  ]
  node [
    id 67
    label "czyn"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 63
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 66
    target 67
  ]
]
