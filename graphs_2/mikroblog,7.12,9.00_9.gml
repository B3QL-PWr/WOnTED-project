graph [
  node [
    id 0
    label "zaproszenie"
    origin "text"
  ]
  node [
    id 1
    label "kilka"
    origin "text"
  ]
  node [
    id 2
    label "lasek"
    origin "text"
  ]
  node [
    id 3
    label "tindera"
    origin "text"
  ]
  node [
    id 4
    label "impreze"
    origin "text"
  ]
  node [
    id 5
    label "ktorej"
    origin "text"
  ]
  node [
    id 6
    label "gram"
    origin "text"
  ]
  node [
    id 7
    label "set"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "dobre"
    origin "text"
  ]
  node [
    id 10
    label "pomyslem"
    origin "text"
  ]
  node [
    id 11
    label "druk"
  ]
  node [
    id 12
    label "pro&#347;ba"
  ]
  node [
    id 13
    label "invitation"
  ]
  node [
    id 14
    label "karteczka"
  ]
  node [
    id 15
    label "zaproponowanie"
  ]
  node [
    id 16
    label "propozycja"
  ]
  node [
    id 17
    label "proposal"
  ]
  node [
    id 18
    label "pomys&#322;"
  ]
  node [
    id 19
    label "wypowied&#378;"
  ]
  node [
    id 20
    label "solicitation"
  ]
  node [
    id 21
    label "przed&#322;o&#380;enie"
  ]
  node [
    id 22
    label "announcement"
  ]
  node [
    id 23
    label "zach&#281;cenie"
  ]
  node [
    id 24
    label "poinformowanie"
  ]
  node [
    id 25
    label "kandydatura"
  ]
  node [
    id 26
    label "technika"
  ]
  node [
    id 27
    label "impression"
  ]
  node [
    id 28
    label "pismo"
  ]
  node [
    id 29
    label "publikacja"
  ]
  node [
    id 30
    label "glif"
  ]
  node [
    id 31
    label "dese&#324;"
  ]
  node [
    id 32
    label "prohibita"
  ]
  node [
    id 33
    label "cymelium"
  ]
  node [
    id 34
    label "wytw&#243;r"
  ]
  node [
    id 35
    label "tkanina"
  ]
  node [
    id 36
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 37
    label "tekst"
  ]
  node [
    id 38
    label "formatowa&#263;"
  ]
  node [
    id 39
    label "formatowanie"
  ]
  node [
    id 40
    label "zdobnik"
  ]
  node [
    id 41
    label "character"
  ]
  node [
    id 42
    label "printing"
  ]
  node [
    id 43
    label "ryba"
  ]
  node [
    id 44
    label "&#347;ledziowate"
  ]
  node [
    id 45
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 46
    label "kr&#281;gowiec"
  ]
  node [
    id 47
    label "cz&#322;owiek"
  ]
  node [
    id 48
    label "systemik"
  ]
  node [
    id 49
    label "doniczkowiec"
  ]
  node [
    id 50
    label "mi&#281;so"
  ]
  node [
    id 51
    label "system"
  ]
  node [
    id 52
    label "patroszy&#263;"
  ]
  node [
    id 53
    label "rakowato&#347;&#263;"
  ]
  node [
    id 54
    label "w&#281;dkarstwo"
  ]
  node [
    id 55
    label "ryby"
  ]
  node [
    id 56
    label "fish"
  ]
  node [
    id 57
    label "linia_boczna"
  ]
  node [
    id 58
    label "tar&#322;o"
  ]
  node [
    id 59
    label "wyrostek_filtracyjny"
  ]
  node [
    id 60
    label "m&#281;tnooki"
  ]
  node [
    id 61
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 62
    label "pokrywa_skrzelowa"
  ]
  node [
    id 63
    label "ikra"
  ]
  node [
    id 64
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 65
    label "szczelina_skrzelowa"
  ]
  node [
    id 66
    label "&#347;ledziokszta&#322;tne"
  ]
  node [
    id 67
    label "centygram"
  ]
  node [
    id 68
    label "megagram"
  ]
  node [
    id 69
    label "metryczna_jednostka_masy"
  ]
  node [
    id 70
    label "miligram"
  ]
  node [
    id 71
    label "dekagram"
  ]
  node [
    id 72
    label "decygram"
  ]
  node [
    id 73
    label "mikrogram"
  ]
  node [
    id 74
    label "hektogram"
  ]
  node [
    id 75
    label "kilogram"
  ]
  node [
    id 76
    label "gem"
  ]
  node [
    id 77
    label "kompozycja"
  ]
  node [
    id 78
    label "runda"
  ]
  node [
    id 79
    label "muzyka"
  ]
  node [
    id 80
    label "zestaw"
  ]
  node [
    id 81
    label "rozgrywka"
  ]
  node [
    id 82
    label "faza"
  ]
  node [
    id 83
    label "seria"
  ]
  node [
    id 84
    label "rhythm"
  ]
  node [
    id 85
    label "turniej"
  ]
  node [
    id 86
    label "czas"
  ]
  node [
    id 87
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 88
    label "okr&#261;&#380;enie"
  ]
  node [
    id 89
    label "struktura"
  ]
  node [
    id 90
    label "zbi&#243;r"
  ]
  node [
    id 91
    label "stage_set"
  ]
  node [
    id 92
    label "sk&#322;ada&#263;"
  ]
  node [
    id 93
    label "sygna&#322;"
  ]
  node [
    id 94
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 95
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 96
    label "blend"
  ]
  node [
    id 97
    label "prawo_karne"
  ]
  node [
    id 98
    label "leksem"
  ]
  node [
    id 99
    label "dzie&#322;o"
  ]
  node [
    id 100
    label "figuracja"
  ]
  node [
    id 101
    label "chwyt"
  ]
  node [
    id 102
    label "okup"
  ]
  node [
    id 103
    label "muzykologia"
  ]
  node [
    id 104
    label "&#347;redniowiecze"
  ]
  node [
    id 105
    label "tennis"
  ]
  node [
    id 106
    label "wokalistyka"
  ]
  node [
    id 107
    label "przedmiot"
  ]
  node [
    id 108
    label "wykonywanie"
  ]
  node [
    id 109
    label "muza"
  ]
  node [
    id 110
    label "wykonywa&#263;"
  ]
  node [
    id 111
    label "zjawisko"
  ]
  node [
    id 112
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 113
    label "beatbox"
  ]
  node [
    id 114
    label "komponowa&#263;"
  ]
  node [
    id 115
    label "szko&#322;a"
  ]
  node [
    id 116
    label "komponowanie"
  ]
  node [
    id 117
    label "pasa&#380;"
  ]
  node [
    id 118
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 119
    label "notacja_muzyczna"
  ]
  node [
    id 120
    label "kontrapunkt"
  ]
  node [
    id 121
    label "nauka"
  ]
  node [
    id 122
    label "sztuka"
  ]
  node [
    id 123
    label "instrumentalistyka"
  ]
  node [
    id 124
    label "harmonia"
  ]
  node [
    id 125
    label "wys&#322;uchanie"
  ]
  node [
    id 126
    label "kapela"
  ]
  node [
    id 127
    label "britpop"
  ]
  node [
    id 128
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 129
    label "mie&#263;_miejsce"
  ]
  node [
    id 130
    label "equal"
  ]
  node [
    id 131
    label "trwa&#263;"
  ]
  node [
    id 132
    label "chodzi&#263;"
  ]
  node [
    id 133
    label "si&#281;ga&#263;"
  ]
  node [
    id 134
    label "stan"
  ]
  node [
    id 135
    label "obecno&#347;&#263;"
  ]
  node [
    id 136
    label "stand"
  ]
  node [
    id 137
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 138
    label "uczestniczy&#263;"
  ]
  node [
    id 139
    label "participate"
  ]
  node [
    id 140
    label "robi&#263;"
  ]
  node [
    id 141
    label "istnie&#263;"
  ]
  node [
    id 142
    label "pozostawa&#263;"
  ]
  node [
    id 143
    label "zostawa&#263;"
  ]
  node [
    id 144
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 145
    label "adhere"
  ]
  node [
    id 146
    label "compass"
  ]
  node [
    id 147
    label "korzysta&#263;"
  ]
  node [
    id 148
    label "appreciation"
  ]
  node [
    id 149
    label "osi&#261;ga&#263;"
  ]
  node [
    id 150
    label "dociera&#263;"
  ]
  node [
    id 151
    label "get"
  ]
  node [
    id 152
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 153
    label "mierzy&#263;"
  ]
  node [
    id 154
    label "u&#380;ywa&#263;"
  ]
  node [
    id 155
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 156
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 157
    label "exsert"
  ]
  node [
    id 158
    label "being"
  ]
  node [
    id 159
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 160
    label "cecha"
  ]
  node [
    id 161
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 162
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 163
    label "p&#322;ywa&#263;"
  ]
  node [
    id 164
    label "run"
  ]
  node [
    id 165
    label "bangla&#263;"
  ]
  node [
    id 166
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 167
    label "przebiega&#263;"
  ]
  node [
    id 168
    label "wk&#322;ada&#263;"
  ]
  node [
    id 169
    label "proceed"
  ]
  node [
    id 170
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 171
    label "carry"
  ]
  node [
    id 172
    label "bywa&#263;"
  ]
  node [
    id 173
    label "dziama&#263;"
  ]
  node [
    id 174
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 175
    label "stara&#263;_si&#281;"
  ]
  node [
    id 176
    label "para"
  ]
  node [
    id 177
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 178
    label "str&#243;j"
  ]
  node [
    id 179
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 180
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 181
    label "krok"
  ]
  node [
    id 182
    label "tryb"
  ]
  node [
    id 183
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 184
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 185
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 186
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 187
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 188
    label "continue"
  ]
  node [
    id 189
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 190
    label "Ohio"
  ]
  node [
    id 191
    label "wci&#281;cie"
  ]
  node [
    id 192
    label "Nowy_York"
  ]
  node [
    id 193
    label "warstwa"
  ]
  node [
    id 194
    label "samopoczucie"
  ]
  node [
    id 195
    label "Illinois"
  ]
  node [
    id 196
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 197
    label "state"
  ]
  node [
    id 198
    label "Jukatan"
  ]
  node [
    id 199
    label "Kalifornia"
  ]
  node [
    id 200
    label "Wirginia"
  ]
  node [
    id 201
    label "wektor"
  ]
  node [
    id 202
    label "Teksas"
  ]
  node [
    id 203
    label "Goa"
  ]
  node [
    id 204
    label "Waszyngton"
  ]
  node [
    id 205
    label "miejsce"
  ]
  node [
    id 206
    label "Massachusetts"
  ]
  node [
    id 207
    label "Alaska"
  ]
  node [
    id 208
    label "Arakan"
  ]
  node [
    id 209
    label "Hawaje"
  ]
  node [
    id 210
    label "Maryland"
  ]
  node [
    id 211
    label "punkt"
  ]
  node [
    id 212
    label "Michigan"
  ]
  node [
    id 213
    label "Arizona"
  ]
  node [
    id 214
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 215
    label "Georgia"
  ]
  node [
    id 216
    label "poziom"
  ]
  node [
    id 217
    label "Pensylwania"
  ]
  node [
    id 218
    label "shape"
  ]
  node [
    id 219
    label "Luizjana"
  ]
  node [
    id 220
    label "Nowy_Meksyk"
  ]
  node [
    id 221
    label "Alabama"
  ]
  node [
    id 222
    label "ilo&#347;&#263;"
  ]
  node [
    id 223
    label "Kansas"
  ]
  node [
    id 224
    label "Oregon"
  ]
  node [
    id 225
    label "Floryda"
  ]
  node [
    id 226
    label "Oklahoma"
  ]
  node [
    id 227
    label "jednostka_administracyjna"
  ]
  node [
    id 228
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 9
    target 10
  ]
]
