graph [
  node [
    id 0
    label "ba&#324;ski"
    origin "text"
  ]
  node [
    id 1
    label "bystrzyca"
    origin "text"
  ]
  node [
    id 2
    label "jedno"
    origin "text"
  ]
  node [
    id 3
    label "&#322;adny"
    origin "text"
  ]
  node [
    id 4
    label "s&#322;owacki"
    origin "text"
  ]
  node [
    id 5
    label "nurt"
  ]
  node [
    id 6
    label "pr&#261;d"
  ]
  node [
    id 7
    label "linia"
  ]
  node [
    id 8
    label "procedura"
  ]
  node [
    id 9
    label "ideologia"
  ]
  node [
    id 10
    label "proces"
  ]
  node [
    id 11
    label "syfon"
  ]
  node [
    id 12
    label "metoda"
  ]
  node [
    id 13
    label "ciek_wodny"
  ]
  node [
    id 14
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 15
    label "praktyka"
  ]
  node [
    id 16
    label "system"
  ]
  node [
    id 17
    label "cycle"
  ]
  node [
    id 18
    label "przyzwoity"
  ]
  node [
    id 19
    label "g&#322;adki"
  ]
  node [
    id 20
    label "ch&#281;dogi"
  ]
  node [
    id 21
    label "obyczajny"
  ]
  node [
    id 22
    label "dobrze"
  ]
  node [
    id 23
    label "niez&#322;y"
  ]
  node [
    id 24
    label "ca&#322;y"
  ]
  node [
    id 25
    label "&#347;warny"
  ]
  node [
    id 26
    label "harny"
  ]
  node [
    id 27
    label "przyjemny"
  ]
  node [
    id 28
    label "po&#380;&#261;dany"
  ]
  node [
    id 29
    label "&#322;adnie"
  ]
  node [
    id 30
    label "dobry"
  ]
  node [
    id 31
    label "z&#322;y"
  ]
  node [
    id 32
    label "intensywny"
  ]
  node [
    id 33
    label "udolny"
  ]
  node [
    id 34
    label "skuteczny"
  ]
  node [
    id 35
    label "&#347;mieszny"
  ]
  node [
    id 36
    label "niczegowaty"
  ]
  node [
    id 37
    label "nieszpetny"
  ]
  node [
    id 38
    label "spory"
  ]
  node [
    id 39
    label "pozytywny"
  ]
  node [
    id 40
    label "korzystny"
  ]
  node [
    id 41
    label "nie&#378;le"
  ]
  node [
    id 42
    label "dobroczynny"
  ]
  node [
    id 43
    label "czw&#243;rka"
  ]
  node [
    id 44
    label "spokojny"
  ]
  node [
    id 45
    label "mi&#322;y"
  ]
  node [
    id 46
    label "grzeczny"
  ]
  node [
    id 47
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 48
    label "powitanie"
  ]
  node [
    id 49
    label "zwrot"
  ]
  node [
    id 50
    label "pomy&#347;lny"
  ]
  node [
    id 51
    label "moralny"
  ]
  node [
    id 52
    label "drogi"
  ]
  node [
    id 53
    label "odpowiedni"
  ]
  node [
    id 54
    label "pos&#322;uszny"
  ]
  node [
    id 55
    label "jedyny"
  ]
  node [
    id 56
    label "du&#380;y"
  ]
  node [
    id 57
    label "zdr&#243;w"
  ]
  node [
    id 58
    label "calu&#347;ko"
  ]
  node [
    id 59
    label "kompletny"
  ]
  node [
    id 60
    label "&#380;ywy"
  ]
  node [
    id 61
    label "pe&#322;ny"
  ]
  node [
    id 62
    label "podobny"
  ]
  node [
    id 63
    label "ca&#322;o"
  ]
  node [
    id 64
    label "skromny"
  ]
  node [
    id 65
    label "kulturalny"
  ]
  node [
    id 66
    label "stosowny"
  ]
  node [
    id 67
    label "przystojny"
  ]
  node [
    id 68
    label "nale&#380;yty"
  ]
  node [
    id 69
    label "przyzwoicie"
  ]
  node [
    id 70
    label "wystarczaj&#261;cy"
  ]
  node [
    id 71
    label "przyjemnie"
  ]
  node [
    id 72
    label "pieski"
  ]
  node [
    id 73
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 74
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 75
    label "niekorzystny"
  ]
  node [
    id 76
    label "z&#322;oszczenie"
  ]
  node [
    id 77
    label "sierdzisty"
  ]
  node [
    id 78
    label "niegrzeczny"
  ]
  node [
    id 79
    label "zez&#322;oszczenie"
  ]
  node [
    id 80
    label "zdenerwowany"
  ]
  node [
    id 81
    label "negatywny"
  ]
  node [
    id 82
    label "rozgniewanie"
  ]
  node [
    id 83
    label "gniewanie"
  ]
  node [
    id 84
    label "niemoralny"
  ]
  node [
    id 85
    label "&#378;le"
  ]
  node [
    id 86
    label "niepomy&#347;lny"
  ]
  node [
    id 87
    label "syf"
  ]
  node [
    id 88
    label "ch&#281;dogo"
  ]
  node [
    id 89
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 90
    label "odpowiednio"
  ]
  node [
    id 91
    label "dobroczynnie"
  ]
  node [
    id 92
    label "moralnie"
  ]
  node [
    id 93
    label "korzystnie"
  ]
  node [
    id 94
    label "pozytywnie"
  ]
  node [
    id 95
    label "lepiej"
  ]
  node [
    id 96
    label "wiele"
  ]
  node [
    id 97
    label "skutecznie"
  ]
  node [
    id 98
    label "pomy&#347;lnie"
  ]
  node [
    id 99
    label "zuchwa&#322;y"
  ]
  node [
    id 100
    label "bezproblemowy"
  ]
  node [
    id 101
    label "elegancki"
  ]
  node [
    id 102
    label "og&#243;lnikowy"
  ]
  node [
    id 103
    label "atrakcyjny"
  ]
  node [
    id 104
    label "g&#322;adzenie"
  ]
  node [
    id 105
    label "nieruchomy"
  ]
  node [
    id 106
    label "&#322;atwy"
  ]
  node [
    id 107
    label "r&#243;wny"
  ]
  node [
    id 108
    label "wyg&#322;adzanie_si&#281;"
  ]
  node [
    id 109
    label "jednobarwny"
  ]
  node [
    id 110
    label "przyg&#322;adzenie"
  ]
  node [
    id 111
    label "obtaczanie"
  ]
  node [
    id 112
    label "g&#322;adko"
  ]
  node [
    id 113
    label "prosty"
  ]
  node [
    id 114
    label "przyg&#322;adzanie"
  ]
  node [
    id 115
    label "cisza"
  ]
  node [
    id 116
    label "okr&#261;g&#322;y"
  ]
  node [
    id 117
    label "wyg&#322;adzenie_si&#281;"
  ]
  node [
    id 118
    label "wyg&#322;adzenie"
  ]
  node [
    id 119
    label "wyr&#243;wnanie"
  ]
  node [
    id 120
    label "smaczny"
  ]
  node [
    id 121
    label "porz&#261;dny"
  ]
  node [
    id 122
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 123
    label "Slovak"
  ]
  node [
    id 124
    label "j&#281;zyk_zachodnios&#322;owia&#324;ski"
  ]
  node [
    id 125
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 126
    label "j&#281;zyk"
  ]
  node [
    id 127
    label "po_s&#322;owacku"
  ]
  node [
    id 128
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 129
    label "langosz"
  ]
  node [
    id 130
    label "europejski"
  ]
  node [
    id 131
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 132
    label "wschodnioeuropejski"
  ]
  node [
    id 133
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 134
    label "poga&#324;ski"
  ]
  node [
    id 135
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 136
    label "topielec"
  ]
  node [
    id 137
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 138
    label "artykulator"
  ]
  node [
    id 139
    label "kod"
  ]
  node [
    id 140
    label "kawa&#322;ek"
  ]
  node [
    id 141
    label "przedmiot"
  ]
  node [
    id 142
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 143
    label "gramatyka"
  ]
  node [
    id 144
    label "stylik"
  ]
  node [
    id 145
    label "przet&#322;umaczenie"
  ]
  node [
    id 146
    label "formalizowanie"
  ]
  node [
    id 147
    label "ssa&#263;"
  ]
  node [
    id 148
    label "ssanie"
  ]
  node [
    id 149
    label "language"
  ]
  node [
    id 150
    label "liza&#263;"
  ]
  node [
    id 151
    label "napisa&#263;"
  ]
  node [
    id 152
    label "konsonantyzm"
  ]
  node [
    id 153
    label "wokalizm"
  ]
  node [
    id 154
    label "pisa&#263;"
  ]
  node [
    id 155
    label "fonetyka"
  ]
  node [
    id 156
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 157
    label "jeniec"
  ]
  node [
    id 158
    label "but"
  ]
  node [
    id 159
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 160
    label "po_koroniarsku"
  ]
  node [
    id 161
    label "kultura_duchowa"
  ]
  node [
    id 162
    label "t&#322;umaczenie"
  ]
  node [
    id 163
    label "m&#243;wienie"
  ]
  node [
    id 164
    label "pype&#263;"
  ]
  node [
    id 165
    label "lizanie"
  ]
  node [
    id 166
    label "pismo"
  ]
  node [
    id 167
    label "formalizowa&#263;"
  ]
  node [
    id 168
    label "rozumie&#263;"
  ]
  node [
    id 169
    label "organ"
  ]
  node [
    id 170
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 171
    label "rozumienie"
  ]
  node [
    id 172
    label "spos&#243;b"
  ]
  node [
    id 173
    label "makroglosja"
  ]
  node [
    id 174
    label "m&#243;wi&#263;"
  ]
  node [
    id 175
    label "jama_ustna"
  ]
  node [
    id 176
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 177
    label "formacja_geologiczna"
  ]
  node [
    id 178
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 179
    label "natural_language"
  ]
  node [
    id 180
    label "s&#322;ownictwo"
  ]
  node [
    id 181
    label "urz&#261;dzenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
]
