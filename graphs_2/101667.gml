graph [
  node [
    id 0
    label "sprawozdanie"
    origin "text"
  ]
  node [
    id 1
    label "komisja"
    origin "text"
  ]
  node [
    id 2
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "prawy"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 5
    label "stanowisko"
    origin "text"
  ]
  node [
    id 6
    label "senat"
    origin "text"
  ]
  node [
    id 7
    label "sprawa"
    origin "text"
  ]
  node [
    id 8
    label "ustawa"
    origin "text"
  ]
  node [
    id 9
    label "zmiana"
    origin "text"
  ]
  node [
    id 10
    label "prawo"
    origin "text"
  ]
  node [
    id 11
    label "adwokatura"
    origin "text"
  ]
  node [
    id 12
    label "radca"
    origin "text"
  ]
  node [
    id 13
    label "prawny"
    origin "text"
  ]
  node [
    id 14
    label "notariat"
    origin "text"
  ]
  node [
    id 15
    label "druk"
    origin "text"
  ]
  node [
    id 16
    label "wypowied&#378;"
  ]
  node [
    id 17
    label "message"
  ]
  node [
    id 18
    label "korespondent"
  ]
  node [
    id 19
    label "sprawko"
  ]
  node [
    id 20
    label "pos&#322;uchanie"
  ]
  node [
    id 21
    label "s&#261;d"
  ]
  node [
    id 22
    label "sparafrazowanie"
  ]
  node [
    id 23
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 24
    label "strawestowa&#263;"
  ]
  node [
    id 25
    label "sparafrazowa&#263;"
  ]
  node [
    id 26
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 27
    label "trawestowa&#263;"
  ]
  node [
    id 28
    label "sformu&#322;owanie"
  ]
  node [
    id 29
    label "parafrazowanie"
  ]
  node [
    id 30
    label "ozdobnik"
  ]
  node [
    id 31
    label "delimitacja"
  ]
  node [
    id 32
    label "parafrazowa&#263;"
  ]
  node [
    id 33
    label "stylizacja"
  ]
  node [
    id 34
    label "komunikat"
  ]
  node [
    id 35
    label "trawestowanie"
  ]
  node [
    id 36
    label "strawestowanie"
  ]
  node [
    id 37
    label "rezultat"
  ]
  node [
    id 38
    label "relacja"
  ]
  node [
    id 39
    label "reporter"
  ]
  node [
    id 40
    label "podkomisja"
  ]
  node [
    id 41
    label "zesp&#243;&#322;"
  ]
  node [
    id 42
    label "organ"
  ]
  node [
    id 43
    label "obrady"
  ]
  node [
    id 44
    label "Komisja_Europejska"
  ]
  node [
    id 45
    label "dyskusja"
  ]
  node [
    id 46
    label "conference"
  ]
  node [
    id 47
    label "konsylium"
  ]
  node [
    id 48
    label "Mazowsze"
  ]
  node [
    id 49
    label "odm&#322;adzanie"
  ]
  node [
    id 50
    label "&#346;wietliki"
  ]
  node [
    id 51
    label "zbi&#243;r"
  ]
  node [
    id 52
    label "whole"
  ]
  node [
    id 53
    label "skupienie"
  ]
  node [
    id 54
    label "The_Beatles"
  ]
  node [
    id 55
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 56
    label "odm&#322;adza&#263;"
  ]
  node [
    id 57
    label "zabudowania"
  ]
  node [
    id 58
    label "group"
  ]
  node [
    id 59
    label "zespolik"
  ]
  node [
    id 60
    label "schorzenie"
  ]
  node [
    id 61
    label "ro&#347;lina"
  ]
  node [
    id 62
    label "grupa"
  ]
  node [
    id 63
    label "Depeche_Mode"
  ]
  node [
    id 64
    label "batch"
  ]
  node [
    id 65
    label "odm&#322;odzenie"
  ]
  node [
    id 66
    label "tkanka"
  ]
  node [
    id 67
    label "jednostka_organizacyjna"
  ]
  node [
    id 68
    label "budowa"
  ]
  node [
    id 69
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 70
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 71
    label "tw&#243;r"
  ]
  node [
    id 72
    label "organogeneza"
  ]
  node [
    id 73
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 74
    label "struktura_anatomiczna"
  ]
  node [
    id 75
    label "uk&#322;ad"
  ]
  node [
    id 76
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 77
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 78
    label "Izba_Konsyliarska"
  ]
  node [
    id 79
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 80
    label "stomia"
  ]
  node [
    id 81
    label "dekortykacja"
  ]
  node [
    id 82
    label "okolica"
  ]
  node [
    id 83
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 84
    label "Komitet_Region&#243;w"
  ]
  node [
    id 85
    label "subcommittee"
  ]
  node [
    id 86
    label "nemezis"
  ]
  node [
    id 87
    label "konsekwencja"
  ]
  node [
    id 88
    label "punishment"
  ]
  node [
    id 89
    label "righteousness"
  ]
  node [
    id 90
    label "cecha"
  ]
  node [
    id 91
    label "roboty_przymusowe"
  ]
  node [
    id 92
    label "odczuwa&#263;"
  ]
  node [
    id 93
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 94
    label "skrupienie_si&#281;"
  ]
  node [
    id 95
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 96
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 97
    label "odczucie"
  ]
  node [
    id 98
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 99
    label "koszula_Dejaniry"
  ]
  node [
    id 100
    label "odczuwanie"
  ]
  node [
    id 101
    label "event"
  ]
  node [
    id 102
    label "skrupianie_si&#281;"
  ]
  node [
    id 103
    label "odczu&#263;"
  ]
  node [
    id 104
    label "charakterystyka"
  ]
  node [
    id 105
    label "m&#322;ot"
  ]
  node [
    id 106
    label "znak"
  ]
  node [
    id 107
    label "drzewo"
  ]
  node [
    id 108
    label "pr&#243;ba"
  ]
  node [
    id 109
    label "attribute"
  ]
  node [
    id 110
    label "marka"
  ]
  node [
    id 111
    label "kara"
  ]
  node [
    id 112
    label "w_prawo"
  ]
  node [
    id 113
    label "s&#322;uszny"
  ]
  node [
    id 114
    label "naturalny"
  ]
  node [
    id 115
    label "chwalebny"
  ]
  node [
    id 116
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 117
    label "zgodnie_z_prawem"
  ]
  node [
    id 118
    label "zacny"
  ]
  node [
    id 119
    label "moralny"
  ]
  node [
    id 120
    label "prawicowy"
  ]
  node [
    id 121
    label "na_prawo"
  ]
  node [
    id 122
    label "cnotliwy"
  ]
  node [
    id 123
    label "legalny"
  ]
  node [
    id 124
    label "z_prawa"
  ]
  node [
    id 125
    label "gajny"
  ]
  node [
    id 126
    label "legalnie"
  ]
  node [
    id 127
    label "s&#322;usznie"
  ]
  node [
    id 128
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 129
    label "zasadny"
  ]
  node [
    id 130
    label "nale&#380;yty"
  ]
  node [
    id 131
    label "prawdziwy"
  ]
  node [
    id 132
    label "solidny"
  ]
  node [
    id 133
    label "moralnie"
  ]
  node [
    id 134
    label "warto&#347;ciowy"
  ]
  node [
    id 135
    label "etycznie"
  ]
  node [
    id 136
    label "dobry"
  ]
  node [
    id 137
    label "pochwalny"
  ]
  node [
    id 138
    label "wspania&#322;y"
  ]
  node [
    id 139
    label "szlachetny"
  ]
  node [
    id 140
    label "powa&#380;ny"
  ]
  node [
    id 141
    label "chwalebnie"
  ]
  node [
    id 142
    label "zacnie"
  ]
  node [
    id 143
    label "skromny"
  ]
  node [
    id 144
    label "niewinny"
  ]
  node [
    id 145
    label "cny"
  ]
  node [
    id 146
    label "cnotliwie"
  ]
  node [
    id 147
    label "prostolinijny"
  ]
  node [
    id 148
    label "szczery"
  ]
  node [
    id 149
    label "zrozumia&#322;y"
  ]
  node [
    id 150
    label "immanentny"
  ]
  node [
    id 151
    label "zwyczajny"
  ]
  node [
    id 152
    label "bezsporny"
  ]
  node [
    id 153
    label "organicznie"
  ]
  node [
    id 154
    label "pierwotny"
  ]
  node [
    id 155
    label "neutralny"
  ]
  node [
    id 156
    label "normalny"
  ]
  node [
    id 157
    label "rzeczywisty"
  ]
  node [
    id 158
    label "naturalnie"
  ]
  node [
    id 159
    label "prawicowo"
  ]
  node [
    id 160
    label "prawoskr&#281;tny"
  ]
  node [
    id 161
    label "konserwatywny"
  ]
  node [
    id 162
    label "ludzko&#347;&#263;"
  ]
  node [
    id 163
    label "asymilowanie"
  ]
  node [
    id 164
    label "wapniak"
  ]
  node [
    id 165
    label "asymilowa&#263;"
  ]
  node [
    id 166
    label "os&#322;abia&#263;"
  ]
  node [
    id 167
    label "posta&#263;"
  ]
  node [
    id 168
    label "hominid"
  ]
  node [
    id 169
    label "podw&#322;adny"
  ]
  node [
    id 170
    label "os&#322;abianie"
  ]
  node [
    id 171
    label "g&#322;owa"
  ]
  node [
    id 172
    label "figura"
  ]
  node [
    id 173
    label "portrecista"
  ]
  node [
    id 174
    label "dwun&#243;g"
  ]
  node [
    id 175
    label "profanum"
  ]
  node [
    id 176
    label "mikrokosmos"
  ]
  node [
    id 177
    label "nasada"
  ]
  node [
    id 178
    label "duch"
  ]
  node [
    id 179
    label "antropochoria"
  ]
  node [
    id 180
    label "osoba"
  ]
  node [
    id 181
    label "wz&#243;r"
  ]
  node [
    id 182
    label "senior"
  ]
  node [
    id 183
    label "oddzia&#322;ywanie"
  ]
  node [
    id 184
    label "Adam"
  ]
  node [
    id 185
    label "homo_sapiens"
  ]
  node [
    id 186
    label "polifag"
  ]
  node [
    id 187
    label "konsument"
  ]
  node [
    id 188
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 189
    label "cz&#322;owiekowate"
  ]
  node [
    id 190
    label "istota_&#380;ywa"
  ]
  node [
    id 191
    label "pracownik"
  ]
  node [
    id 192
    label "Chocho&#322;"
  ]
  node [
    id 193
    label "Herkules_Poirot"
  ]
  node [
    id 194
    label "Edyp"
  ]
  node [
    id 195
    label "parali&#380;owa&#263;"
  ]
  node [
    id 196
    label "Harry_Potter"
  ]
  node [
    id 197
    label "Casanova"
  ]
  node [
    id 198
    label "Zgredek"
  ]
  node [
    id 199
    label "Gargantua"
  ]
  node [
    id 200
    label "Winnetou"
  ]
  node [
    id 201
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 202
    label "Dulcynea"
  ]
  node [
    id 203
    label "kategoria_gramatyczna"
  ]
  node [
    id 204
    label "person"
  ]
  node [
    id 205
    label "Plastu&#347;"
  ]
  node [
    id 206
    label "Quasimodo"
  ]
  node [
    id 207
    label "Sherlock_Holmes"
  ]
  node [
    id 208
    label "Faust"
  ]
  node [
    id 209
    label "Wallenrod"
  ]
  node [
    id 210
    label "Dwukwiat"
  ]
  node [
    id 211
    label "Don_Juan"
  ]
  node [
    id 212
    label "koniugacja"
  ]
  node [
    id 213
    label "Don_Kiszot"
  ]
  node [
    id 214
    label "Hamlet"
  ]
  node [
    id 215
    label "Werter"
  ]
  node [
    id 216
    label "istota"
  ]
  node [
    id 217
    label "Szwejk"
  ]
  node [
    id 218
    label "doros&#322;y"
  ]
  node [
    id 219
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 220
    label "jajko"
  ]
  node [
    id 221
    label "rodzic"
  ]
  node [
    id 222
    label "wapniaki"
  ]
  node [
    id 223
    label "zwierzchnik"
  ]
  node [
    id 224
    label "feuda&#322;"
  ]
  node [
    id 225
    label "starzec"
  ]
  node [
    id 226
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 227
    label "zawodnik"
  ]
  node [
    id 228
    label "komendancja"
  ]
  node [
    id 229
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 230
    label "asymilowanie_si&#281;"
  ]
  node [
    id 231
    label "absorption"
  ]
  node [
    id 232
    label "pobieranie"
  ]
  node [
    id 233
    label "czerpanie"
  ]
  node [
    id 234
    label "acquisition"
  ]
  node [
    id 235
    label "zmienianie"
  ]
  node [
    id 236
    label "organizm"
  ]
  node [
    id 237
    label "assimilation"
  ]
  node [
    id 238
    label "upodabnianie"
  ]
  node [
    id 239
    label "g&#322;oska"
  ]
  node [
    id 240
    label "kultura"
  ]
  node [
    id 241
    label "podobny"
  ]
  node [
    id 242
    label "fonetyka"
  ]
  node [
    id 243
    label "suppress"
  ]
  node [
    id 244
    label "robi&#263;"
  ]
  node [
    id 245
    label "os&#322;abienie"
  ]
  node [
    id 246
    label "kondycja_fizyczna"
  ]
  node [
    id 247
    label "os&#322;abi&#263;"
  ]
  node [
    id 248
    label "zdrowie"
  ]
  node [
    id 249
    label "powodowa&#263;"
  ]
  node [
    id 250
    label "zmniejsza&#263;"
  ]
  node [
    id 251
    label "bate"
  ]
  node [
    id 252
    label "de-escalation"
  ]
  node [
    id 253
    label "powodowanie"
  ]
  node [
    id 254
    label "debilitation"
  ]
  node [
    id 255
    label "zmniejszanie"
  ]
  node [
    id 256
    label "s&#322;abszy"
  ]
  node [
    id 257
    label "pogarszanie"
  ]
  node [
    id 258
    label "assimilate"
  ]
  node [
    id 259
    label "dostosowywa&#263;"
  ]
  node [
    id 260
    label "dostosowa&#263;"
  ]
  node [
    id 261
    label "przejmowa&#263;"
  ]
  node [
    id 262
    label "upodobni&#263;"
  ]
  node [
    id 263
    label "przej&#261;&#263;"
  ]
  node [
    id 264
    label "upodabnia&#263;"
  ]
  node [
    id 265
    label "pobiera&#263;"
  ]
  node [
    id 266
    label "pobra&#263;"
  ]
  node [
    id 267
    label "zapis"
  ]
  node [
    id 268
    label "figure"
  ]
  node [
    id 269
    label "typ"
  ]
  node [
    id 270
    label "spos&#243;b"
  ]
  node [
    id 271
    label "mildew"
  ]
  node [
    id 272
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 273
    label "ideal"
  ]
  node [
    id 274
    label "rule"
  ]
  node [
    id 275
    label "ruch"
  ]
  node [
    id 276
    label "dekal"
  ]
  node [
    id 277
    label "motyw"
  ]
  node [
    id 278
    label "projekt"
  ]
  node [
    id 279
    label "zaistnie&#263;"
  ]
  node [
    id 280
    label "Osjan"
  ]
  node [
    id 281
    label "kto&#347;"
  ]
  node [
    id 282
    label "wygl&#261;d"
  ]
  node [
    id 283
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 284
    label "osobowo&#347;&#263;"
  ]
  node [
    id 285
    label "wytw&#243;r"
  ]
  node [
    id 286
    label "trim"
  ]
  node [
    id 287
    label "poby&#263;"
  ]
  node [
    id 288
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 289
    label "Aspazja"
  ]
  node [
    id 290
    label "punkt_widzenia"
  ]
  node [
    id 291
    label "kompleksja"
  ]
  node [
    id 292
    label "wytrzyma&#263;"
  ]
  node [
    id 293
    label "formacja"
  ]
  node [
    id 294
    label "pozosta&#263;"
  ]
  node [
    id 295
    label "point"
  ]
  node [
    id 296
    label "przedstawienie"
  ]
  node [
    id 297
    label "go&#347;&#263;"
  ]
  node [
    id 298
    label "fotograf"
  ]
  node [
    id 299
    label "malarz"
  ]
  node [
    id 300
    label "artysta"
  ]
  node [
    id 301
    label "hipnotyzowanie"
  ]
  node [
    id 302
    label "&#347;lad"
  ]
  node [
    id 303
    label "docieranie"
  ]
  node [
    id 304
    label "natural_process"
  ]
  node [
    id 305
    label "reakcja_chemiczna"
  ]
  node [
    id 306
    label "wdzieranie_si&#281;"
  ]
  node [
    id 307
    label "zjawisko"
  ]
  node [
    id 308
    label "act"
  ]
  node [
    id 309
    label "lobbysta"
  ]
  node [
    id 310
    label "pryncypa&#322;"
  ]
  node [
    id 311
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 312
    label "kszta&#322;t"
  ]
  node [
    id 313
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 314
    label "wiedza"
  ]
  node [
    id 315
    label "kierowa&#263;"
  ]
  node [
    id 316
    label "alkohol"
  ]
  node [
    id 317
    label "zdolno&#347;&#263;"
  ]
  node [
    id 318
    label "&#380;ycie"
  ]
  node [
    id 319
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 320
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 321
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 322
    label "sztuka"
  ]
  node [
    id 323
    label "dekiel"
  ]
  node [
    id 324
    label "&#347;ci&#281;cie"
  ]
  node [
    id 325
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 326
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 327
    label "&#347;ci&#281;gno"
  ]
  node [
    id 328
    label "noosfera"
  ]
  node [
    id 329
    label "byd&#322;o"
  ]
  node [
    id 330
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 331
    label "makrocefalia"
  ]
  node [
    id 332
    label "obiekt"
  ]
  node [
    id 333
    label "ucho"
  ]
  node [
    id 334
    label "g&#243;ra"
  ]
  node [
    id 335
    label "m&#243;zg"
  ]
  node [
    id 336
    label "kierownictwo"
  ]
  node [
    id 337
    label "fryzura"
  ]
  node [
    id 338
    label "umys&#322;"
  ]
  node [
    id 339
    label "cia&#322;o"
  ]
  node [
    id 340
    label "cz&#322;onek"
  ]
  node [
    id 341
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 342
    label "czaszka"
  ]
  node [
    id 343
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 344
    label "allochoria"
  ]
  node [
    id 345
    label "p&#322;aszczyzna"
  ]
  node [
    id 346
    label "przedmiot"
  ]
  node [
    id 347
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 348
    label "bierka_szachowa"
  ]
  node [
    id 349
    label "obiekt_matematyczny"
  ]
  node [
    id 350
    label "gestaltyzm"
  ]
  node [
    id 351
    label "styl"
  ]
  node [
    id 352
    label "obraz"
  ]
  node [
    id 353
    label "rzecz"
  ]
  node [
    id 354
    label "d&#378;wi&#281;k"
  ]
  node [
    id 355
    label "character"
  ]
  node [
    id 356
    label "rze&#378;ba"
  ]
  node [
    id 357
    label "stylistyka"
  ]
  node [
    id 358
    label "miejsce"
  ]
  node [
    id 359
    label "antycypacja"
  ]
  node [
    id 360
    label "ornamentyka"
  ]
  node [
    id 361
    label "informacja"
  ]
  node [
    id 362
    label "facet"
  ]
  node [
    id 363
    label "popis"
  ]
  node [
    id 364
    label "wiersz"
  ]
  node [
    id 365
    label "symetria"
  ]
  node [
    id 366
    label "lingwistyka_kognitywna"
  ]
  node [
    id 367
    label "karta"
  ]
  node [
    id 368
    label "shape"
  ]
  node [
    id 369
    label "podzbi&#243;r"
  ]
  node [
    id 370
    label "perspektywa"
  ]
  node [
    id 371
    label "dziedzina"
  ]
  node [
    id 372
    label "nak&#322;adka"
  ]
  node [
    id 373
    label "li&#347;&#263;"
  ]
  node [
    id 374
    label "jama_gard&#322;owa"
  ]
  node [
    id 375
    label "rezonator"
  ]
  node [
    id 376
    label "podstawa"
  ]
  node [
    id 377
    label "base"
  ]
  node [
    id 378
    label "piek&#322;o"
  ]
  node [
    id 379
    label "human_body"
  ]
  node [
    id 380
    label "ofiarowywanie"
  ]
  node [
    id 381
    label "sfera_afektywna"
  ]
  node [
    id 382
    label "nekromancja"
  ]
  node [
    id 383
    label "Po&#347;wist"
  ]
  node [
    id 384
    label "podekscytowanie"
  ]
  node [
    id 385
    label "deformowanie"
  ]
  node [
    id 386
    label "sumienie"
  ]
  node [
    id 387
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 388
    label "deformowa&#263;"
  ]
  node [
    id 389
    label "psychika"
  ]
  node [
    id 390
    label "zjawa"
  ]
  node [
    id 391
    label "zmar&#322;y"
  ]
  node [
    id 392
    label "istota_nadprzyrodzona"
  ]
  node [
    id 393
    label "power"
  ]
  node [
    id 394
    label "entity"
  ]
  node [
    id 395
    label "ofiarowywa&#263;"
  ]
  node [
    id 396
    label "oddech"
  ]
  node [
    id 397
    label "seksualno&#347;&#263;"
  ]
  node [
    id 398
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 399
    label "byt"
  ]
  node [
    id 400
    label "si&#322;a"
  ]
  node [
    id 401
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 402
    label "ego"
  ]
  node [
    id 403
    label "ofiarowanie"
  ]
  node [
    id 404
    label "charakter"
  ]
  node [
    id 405
    label "fizjonomia"
  ]
  node [
    id 406
    label "kompleks"
  ]
  node [
    id 407
    label "zapalno&#347;&#263;"
  ]
  node [
    id 408
    label "T&#281;sknica"
  ]
  node [
    id 409
    label "ofiarowa&#263;"
  ]
  node [
    id 410
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 411
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 412
    label "passion"
  ]
  node [
    id 413
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 414
    label "odbicie"
  ]
  node [
    id 415
    label "atom"
  ]
  node [
    id 416
    label "przyroda"
  ]
  node [
    id 417
    label "Ziemia"
  ]
  node [
    id 418
    label "kosmos"
  ]
  node [
    id 419
    label "miniatura"
  ]
  node [
    id 420
    label "po&#322;o&#380;enie"
  ]
  node [
    id 421
    label "punkt"
  ]
  node [
    id 422
    label "pogl&#261;d"
  ]
  node [
    id 423
    label "wojsko"
  ]
  node [
    id 424
    label "awansowa&#263;"
  ]
  node [
    id 425
    label "stawia&#263;"
  ]
  node [
    id 426
    label "uprawianie"
  ]
  node [
    id 427
    label "wakowa&#263;"
  ]
  node [
    id 428
    label "powierzanie"
  ]
  node [
    id 429
    label "postawi&#263;"
  ]
  node [
    id 430
    label "awansowanie"
  ]
  node [
    id 431
    label "praca"
  ]
  node [
    id 432
    label "ust&#281;p"
  ]
  node [
    id 433
    label "plan"
  ]
  node [
    id 434
    label "problemat"
  ]
  node [
    id 435
    label "plamka"
  ]
  node [
    id 436
    label "stopie&#324;_pisma"
  ]
  node [
    id 437
    label "jednostka"
  ]
  node [
    id 438
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 439
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 440
    label "mark"
  ]
  node [
    id 441
    label "chwila"
  ]
  node [
    id 442
    label "prosta"
  ]
  node [
    id 443
    label "problematyka"
  ]
  node [
    id 444
    label "zapunktowa&#263;"
  ]
  node [
    id 445
    label "podpunkt"
  ]
  node [
    id 446
    label "kres"
  ]
  node [
    id 447
    label "przestrze&#324;"
  ]
  node [
    id 448
    label "pozycja"
  ]
  node [
    id 449
    label "przenocowanie"
  ]
  node [
    id 450
    label "pora&#380;ka"
  ]
  node [
    id 451
    label "nak&#322;adzenie"
  ]
  node [
    id 452
    label "pouk&#322;adanie"
  ]
  node [
    id 453
    label "pokrycie"
  ]
  node [
    id 454
    label "zepsucie"
  ]
  node [
    id 455
    label "ustawienie"
  ]
  node [
    id 456
    label "spowodowanie"
  ]
  node [
    id 457
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 458
    label "ugoszczenie"
  ]
  node [
    id 459
    label "le&#380;enie"
  ]
  node [
    id 460
    label "adres"
  ]
  node [
    id 461
    label "zbudowanie"
  ]
  node [
    id 462
    label "umieszczenie"
  ]
  node [
    id 463
    label "reading"
  ]
  node [
    id 464
    label "czynno&#347;&#263;"
  ]
  node [
    id 465
    label "sytuacja"
  ]
  node [
    id 466
    label "zabicie"
  ]
  node [
    id 467
    label "wygranie"
  ]
  node [
    id 468
    label "presentation"
  ]
  node [
    id 469
    label "le&#380;e&#263;"
  ]
  node [
    id 470
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 471
    label "najem"
  ]
  node [
    id 472
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 473
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 474
    label "zak&#322;ad"
  ]
  node [
    id 475
    label "stosunek_pracy"
  ]
  node [
    id 476
    label "benedykty&#324;ski"
  ]
  node [
    id 477
    label "poda&#380;_pracy"
  ]
  node [
    id 478
    label "pracowanie"
  ]
  node [
    id 479
    label "tyrka"
  ]
  node [
    id 480
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 481
    label "zaw&#243;d"
  ]
  node [
    id 482
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 483
    label "tynkarski"
  ]
  node [
    id 484
    label "pracowa&#263;"
  ]
  node [
    id 485
    label "czynnik_produkcji"
  ]
  node [
    id 486
    label "zobowi&#261;zanie"
  ]
  node [
    id 487
    label "siedziba"
  ]
  node [
    id 488
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 489
    label "warunek_lokalowy"
  ]
  node [
    id 490
    label "plac"
  ]
  node [
    id 491
    label "location"
  ]
  node [
    id 492
    label "uwaga"
  ]
  node [
    id 493
    label "status"
  ]
  node [
    id 494
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 495
    label "rz&#261;d"
  ]
  node [
    id 496
    label "teologicznie"
  ]
  node [
    id 497
    label "belief"
  ]
  node [
    id 498
    label "zderzenie_si&#281;"
  ]
  node [
    id 499
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 500
    label "teoria_Arrheniusa"
  ]
  node [
    id 501
    label "zrejterowanie"
  ]
  node [
    id 502
    label "zmobilizowa&#263;"
  ]
  node [
    id 503
    label "dezerter"
  ]
  node [
    id 504
    label "oddzia&#322;_karny"
  ]
  node [
    id 505
    label "rezerwa"
  ]
  node [
    id 506
    label "tabor"
  ]
  node [
    id 507
    label "wermacht"
  ]
  node [
    id 508
    label "cofni&#281;cie"
  ]
  node [
    id 509
    label "potencja"
  ]
  node [
    id 510
    label "fala"
  ]
  node [
    id 511
    label "struktura"
  ]
  node [
    id 512
    label "szko&#322;a"
  ]
  node [
    id 513
    label "korpus"
  ]
  node [
    id 514
    label "soldateska"
  ]
  node [
    id 515
    label "ods&#322;ugiwanie"
  ]
  node [
    id 516
    label "werbowanie_si&#281;"
  ]
  node [
    id 517
    label "zdemobilizowanie"
  ]
  node [
    id 518
    label "oddzia&#322;"
  ]
  node [
    id 519
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 520
    label "s&#322;u&#380;ba"
  ]
  node [
    id 521
    label "or&#281;&#380;"
  ]
  node [
    id 522
    label "Legia_Cudzoziemska"
  ]
  node [
    id 523
    label "Armia_Czerwona"
  ]
  node [
    id 524
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 525
    label "rejterowanie"
  ]
  node [
    id 526
    label "Czerwona_Gwardia"
  ]
  node [
    id 527
    label "zrejterowa&#263;"
  ]
  node [
    id 528
    label "sztabslekarz"
  ]
  node [
    id 529
    label "zmobilizowanie"
  ]
  node [
    id 530
    label "wojo"
  ]
  node [
    id 531
    label "pospolite_ruszenie"
  ]
  node [
    id 532
    label "Eurokorpus"
  ]
  node [
    id 533
    label "mobilizowanie"
  ]
  node [
    id 534
    label "rejterowa&#263;"
  ]
  node [
    id 535
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 536
    label "mobilizowa&#263;"
  ]
  node [
    id 537
    label "Armia_Krajowa"
  ]
  node [
    id 538
    label "obrona"
  ]
  node [
    id 539
    label "dryl"
  ]
  node [
    id 540
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 541
    label "petarda"
  ]
  node [
    id 542
    label "zdemobilizowa&#263;"
  ]
  node [
    id 543
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 544
    label "oddawanie"
  ]
  node [
    id 545
    label "zlecanie"
  ]
  node [
    id 546
    label "ufanie"
  ]
  node [
    id 547
    label "wyznawanie"
  ]
  node [
    id 548
    label "zadanie"
  ]
  node [
    id 549
    label "przej&#347;cie"
  ]
  node [
    id 550
    label "przechodzenie"
  ]
  node [
    id 551
    label "przeniesienie"
  ]
  node [
    id 552
    label "promowanie"
  ]
  node [
    id 553
    label "habilitowanie_si&#281;"
  ]
  node [
    id 554
    label "obj&#281;cie"
  ]
  node [
    id 555
    label "obejmowanie"
  ]
  node [
    id 556
    label "kariera"
  ]
  node [
    id 557
    label "przenoszenie"
  ]
  node [
    id 558
    label "pozyskiwanie"
  ]
  node [
    id 559
    label "pozyskanie"
  ]
  node [
    id 560
    label "pi&#281;cie_si&#281;"
  ]
  node [
    id 561
    label "raise"
  ]
  node [
    id 562
    label "pozyska&#263;"
  ]
  node [
    id 563
    label "obejmowa&#263;"
  ]
  node [
    id 564
    label "pozyskiwa&#263;"
  ]
  node [
    id 565
    label "dawa&#263;_awans"
  ]
  node [
    id 566
    label "obj&#261;&#263;"
  ]
  node [
    id 567
    label "przej&#347;&#263;"
  ]
  node [
    id 568
    label "pi&#261;&#263;_si&#281;"
  ]
  node [
    id 569
    label "da&#263;_awans"
  ]
  node [
    id 570
    label "przechodzi&#263;"
  ]
  node [
    id 571
    label "wolny"
  ]
  node [
    id 572
    label "by&#263;"
  ]
  node [
    id 573
    label "pozostawia&#263;"
  ]
  node [
    id 574
    label "czyni&#263;"
  ]
  node [
    id 575
    label "wydawa&#263;"
  ]
  node [
    id 576
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 577
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 578
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 579
    label "przewidywa&#263;"
  ]
  node [
    id 580
    label "przyznawa&#263;"
  ]
  node [
    id 581
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 582
    label "go"
  ]
  node [
    id 583
    label "obstawia&#263;"
  ]
  node [
    id 584
    label "umieszcza&#263;"
  ]
  node [
    id 585
    label "ocenia&#263;"
  ]
  node [
    id 586
    label "zastawia&#263;"
  ]
  node [
    id 587
    label "wskazywa&#263;"
  ]
  node [
    id 588
    label "introduce"
  ]
  node [
    id 589
    label "uruchamia&#263;"
  ]
  node [
    id 590
    label "wytwarza&#263;"
  ]
  node [
    id 591
    label "fundowa&#263;"
  ]
  node [
    id 592
    label "zmienia&#263;"
  ]
  node [
    id 593
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 594
    label "deliver"
  ]
  node [
    id 595
    label "wyznacza&#263;"
  ]
  node [
    id 596
    label "przedstawia&#263;"
  ]
  node [
    id 597
    label "wydobywa&#263;"
  ]
  node [
    id 598
    label "zafundowa&#263;"
  ]
  node [
    id 599
    label "budowla"
  ]
  node [
    id 600
    label "wyda&#263;"
  ]
  node [
    id 601
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 602
    label "plant"
  ]
  node [
    id 603
    label "uruchomi&#263;"
  ]
  node [
    id 604
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 605
    label "pozostawi&#263;"
  ]
  node [
    id 606
    label "obra&#263;"
  ]
  node [
    id 607
    label "peddle"
  ]
  node [
    id 608
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 609
    label "obstawi&#263;"
  ]
  node [
    id 610
    label "zmieni&#263;"
  ]
  node [
    id 611
    label "post"
  ]
  node [
    id 612
    label "wyznaczy&#263;"
  ]
  node [
    id 613
    label "oceni&#263;"
  ]
  node [
    id 614
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 615
    label "uczyni&#263;"
  ]
  node [
    id 616
    label "spowodowa&#263;"
  ]
  node [
    id 617
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 618
    label "wytworzy&#263;"
  ]
  node [
    id 619
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 620
    label "umie&#347;ci&#263;"
  ]
  node [
    id 621
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 622
    label "set"
  ]
  node [
    id 623
    label "wskaza&#263;"
  ]
  node [
    id 624
    label "przyzna&#263;"
  ]
  node [
    id 625
    label "wydoby&#263;"
  ]
  node [
    id 626
    label "przedstawi&#263;"
  ]
  node [
    id 627
    label "establish"
  ]
  node [
    id 628
    label "stawi&#263;"
  ]
  node [
    id 629
    label "pielenie"
  ]
  node [
    id 630
    label "culture"
  ]
  node [
    id 631
    label "sianie"
  ]
  node [
    id 632
    label "sadzenie"
  ]
  node [
    id 633
    label "oprysk"
  ]
  node [
    id 634
    label "szczepienie"
  ]
  node [
    id 635
    label "orka"
  ]
  node [
    id 636
    label "rolnictwo"
  ]
  node [
    id 637
    label "siew"
  ]
  node [
    id 638
    label "exercise"
  ]
  node [
    id 639
    label "koszenie"
  ]
  node [
    id 640
    label "obrabianie"
  ]
  node [
    id 641
    label "zajmowanie_si&#281;"
  ]
  node [
    id 642
    label "use"
  ]
  node [
    id 643
    label "biotechnika"
  ]
  node [
    id 644
    label "hodowanie"
  ]
  node [
    id 645
    label "parlament"
  ]
  node [
    id 646
    label "deputation"
  ]
  node [
    id 647
    label "kolegium"
  ]
  node [
    id 648
    label "izba_wy&#380;sza"
  ]
  node [
    id 649
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 650
    label "magistrat"
  ]
  node [
    id 651
    label "council"
  ]
  node [
    id 652
    label "zgromadzenie"
  ]
  node [
    id 653
    label "liga"
  ]
  node [
    id 654
    label "jednostka_systematyczna"
  ]
  node [
    id 655
    label "gromada"
  ]
  node [
    id 656
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 657
    label "egzemplarz"
  ]
  node [
    id 658
    label "Entuzjastki"
  ]
  node [
    id 659
    label "kompozycja"
  ]
  node [
    id 660
    label "Terranie"
  ]
  node [
    id 661
    label "category"
  ]
  node [
    id 662
    label "pakiet_klimatyczny"
  ]
  node [
    id 663
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 664
    label "cz&#261;steczka"
  ]
  node [
    id 665
    label "stage_set"
  ]
  node [
    id 666
    label "type"
  ]
  node [
    id 667
    label "specgrupa"
  ]
  node [
    id 668
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 669
    label "Eurogrupa"
  ]
  node [
    id 670
    label "formacja_geologiczna"
  ]
  node [
    id 671
    label "harcerze_starsi"
  ]
  node [
    id 672
    label "urz&#281;dnik"
  ]
  node [
    id 673
    label "rynek"
  ]
  node [
    id 674
    label "plac_ratuszowy"
  ]
  node [
    id 675
    label "urz&#261;d"
  ]
  node [
    id 676
    label "sekretariat"
  ]
  node [
    id 677
    label "urz&#281;dnik_miejski"
  ]
  node [
    id 678
    label "&#321;ubianka"
  ]
  node [
    id 679
    label "miejsce_pracy"
  ]
  node [
    id 680
    label "dzia&#322;_personalny"
  ]
  node [
    id 681
    label "Kreml"
  ]
  node [
    id 682
    label "Bia&#322;y_Dom"
  ]
  node [
    id 683
    label "budynek"
  ]
  node [
    id 684
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 685
    label "sadowisko"
  ]
  node [
    id 686
    label "europarlament"
  ]
  node [
    id 687
    label "plankton_polityczny"
  ]
  node [
    id 688
    label "grupa_bilateralna"
  ]
  node [
    id 689
    label "ustawodawca"
  ]
  node [
    id 690
    label "kognicja"
  ]
  node [
    id 691
    label "object"
  ]
  node [
    id 692
    label "rozprawa"
  ]
  node [
    id 693
    label "temat"
  ]
  node [
    id 694
    label "wydarzenie"
  ]
  node [
    id 695
    label "szczeg&#243;&#322;"
  ]
  node [
    id 696
    label "proposition"
  ]
  node [
    id 697
    label "przes&#322;anka"
  ]
  node [
    id 698
    label "idea"
  ]
  node [
    id 699
    label "przebiec"
  ]
  node [
    id 700
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 701
    label "przebiegni&#281;cie"
  ]
  node [
    id 702
    label "fabu&#322;a"
  ]
  node [
    id 703
    label "ideologia"
  ]
  node [
    id 704
    label "intelekt"
  ]
  node [
    id 705
    label "Kant"
  ]
  node [
    id 706
    label "p&#322;&#243;d"
  ]
  node [
    id 707
    label "cel"
  ]
  node [
    id 708
    label "poj&#281;cie"
  ]
  node [
    id 709
    label "pomys&#322;"
  ]
  node [
    id 710
    label "ideacja"
  ]
  node [
    id 711
    label "wpadni&#281;cie"
  ]
  node [
    id 712
    label "mienie"
  ]
  node [
    id 713
    label "wpa&#347;&#263;"
  ]
  node [
    id 714
    label "wpadanie"
  ]
  node [
    id 715
    label "wpada&#263;"
  ]
  node [
    id 716
    label "rozumowanie"
  ]
  node [
    id 717
    label "opracowanie"
  ]
  node [
    id 718
    label "proces"
  ]
  node [
    id 719
    label "cytat"
  ]
  node [
    id 720
    label "tekst"
  ]
  node [
    id 721
    label "obja&#347;nienie"
  ]
  node [
    id 722
    label "s&#261;dzenie"
  ]
  node [
    id 723
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 724
    label "niuansowa&#263;"
  ]
  node [
    id 725
    label "element"
  ]
  node [
    id 726
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 727
    label "sk&#322;adnik"
  ]
  node [
    id 728
    label "zniuansowa&#263;"
  ]
  node [
    id 729
    label "fakt"
  ]
  node [
    id 730
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 731
    label "przyczyna"
  ]
  node [
    id 732
    label "wnioskowanie"
  ]
  node [
    id 733
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 734
    label "wyraz_pochodny"
  ]
  node [
    id 735
    label "zboczenie"
  ]
  node [
    id 736
    label "om&#243;wienie"
  ]
  node [
    id 737
    label "omawia&#263;"
  ]
  node [
    id 738
    label "fraza"
  ]
  node [
    id 739
    label "tre&#347;&#263;"
  ]
  node [
    id 740
    label "forum"
  ]
  node [
    id 741
    label "topik"
  ]
  node [
    id 742
    label "tematyka"
  ]
  node [
    id 743
    label "w&#261;tek"
  ]
  node [
    id 744
    label "zbaczanie"
  ]
  node [
    id 745
    label "forma"
  ]
  node [
    id 746
    label "om&#243;wi&#263;"
  ]
  node [
    id 747
    label "omawianie"
  ]
  node [
    id 748
    label "melodia"
  ]
  node [
    id 749
    label "otoczka"
  ]
  node [
    id 750
    label "zbacza&#263;"
  ]
  node [
    id 751
    label "zboczy&#263;"
  ]
  node [
    id 752
    label "Karta_Nauczyciela"
  ]
  node [
    id 753
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 754
    label "akt"
  ]
  node [
    id 755
    label "charter"
  ]
  node [
    id 756
    label "marc&#243;wka"
  ]
  node [
    id 757
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 758
    label "podnieci&#263;"
  ]
  node [
    id 759
    label "scena"
  ]
  node [
    id 760
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 761
    label "numer"
  ]
  node [
    id 762
    label "po&#380;ycie"
  ]
  node [
    id 763
    label "podniecenie"
  ]
  node [
    id 764
    label "nago&#347;&#263;"
  ]
  node [
    id 765
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 766
    label "fascyku&#322;"
  ]
  node [
    id 767
    label "seks"
  ]
  node [
    id 768
    label "podniecanie"
  ]
  node [
    id 769
    label "imisja"
  ]
  node [
    id 770
    label "zwyczaj"
  ]
  node [
    id 771
    label "rozmna&#380;anie"
  ]
  node [
    id 772
    label "ruch_frykcyjny"
  ]
  node [
    id 773
    label "ontologia"
  ]
  node [
    id 774
    label "na_pieska"
  ]
  node [
    id 775
    label "pozycja_misjonarska"
  ]
  node [
    id 776
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 777
    label "fragment"
  ]
  node [
    id 778
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 779
    label "z&#322;&#261;czenie"
  ]
  node [
    id 780
    label "gra_wst&#281;pna"
  ]
  node [
    id 781
    label "erotyka"
  ]
  node [
    id 782
    label "urzeczywistnienie"
  ]
  node [
    id 783
    label "baraszki"
  ]
  node [
    id 784
    label "certificate"
  ]
  node [
    id 785
    label "po&#380;&#261;danie"
  ]
  node [
    id 786
    label "wzw&#243;d"
  ]
  node [
    id 787
    label "funkcja"
  ]
  node [
    id 788
    label "dokument"
  ]
  node [
    id 789
    label "arystotelizm"
  ]
  node [
    id 790
    label "podnieca&#263;"
  ]
  node [
    id 791
    label "zabory"
  ]
  node [
    id 792
    label "ci&#281;&#380;arna"
  ]
  node [
    id 793
    label "rozwi&#261;zanie"
  ]
  node [
    id 794
    label "podlec"
  ]
  node [
    id 795
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 796
    label "min&#261;&#263;"
  ]
  node [
    id 797
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 798
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 799
    label "zaliczy&#263;"
  ]
  node [
    id 800
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 801
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 802
    label "przeby&#263;"
  ]
  node [
    id 803
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 804
    label "die"
  ]
  node [
    id 805
    label "dozna&#263;"
  ]
  node [
    id 806
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 807
    label "zacz&#261;&#263;"
  ]
  node [
    id 808
    label "happen"
  ]
  node [
    id 809
    label "pass"
  ]
  node [
    id 810
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 811
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 812
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 813
    label "beat"
  ]
  node [
    id 814
    label "absorb"
  ]
  node [
    id 815
    label "przerobi&#263;"
  ]
  node [
    id 816
    label "pique"
  ]
  node [
    id 817
    label "przesta&#263;"
  ]
  node [
    id 818
    label "mini&#281;cie"
  ]
  node [
    id 819
    label "wymienienie"
  ]
  node [
    id 820
    label "zaliczenie"
  ]
  node [
    id 821
    label "traversal"
  ]
  node [
    id 822
    label "zdarzenie_si&#281;"
  ]
  node [
    id 823
    label "przewy&#380;szenie"
  ]
  node [
    id 824
    label "experience"
  ]
  node [
    id 825
    label "przepuszczenie"
  ]
  node [
    id 826
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 827
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 828
    label "strain"
  ]
  node [
    id 829
    label "faza"
  ]
  node [
    id 830
    label "przerobienie"
  ]
  node [
    id 831
    label "wydeptywanie"
  ]
  node [
    id 832
    label "crack"
  ]
  node [
    id 833
    label "wydeptanie"
  ]
  node [
    id 834
    label "wstawka"
  ]
  node [
    id 835
    label "prze&#380;ycie"
  ]
  node [
    id 836
    label "uznanie"
  ]
  node [
    id 837
    label "doznanie"
  ]
  node [
    id 838
    label "dostanie_si&#281;"
  ]
  node [
    id 839
    label "trwanie"
  ]
  node [
    id 840
    label "przebycie"
  ]
  node [
    id 841
    label "wytyczenie"
  ]
  node [
    id 842
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 843
    label "przepojenie"
  ]
  node [
    id 844
    label "nas&#261;czenie"
  ]
  node [
    id 845
    label "nale&#380;enie"
  ]
  node [
    id 846
    label "odmienienie"
  ]
  node [
    id 847
    label "przedostanie_si&#281;"
  ]
  node [
    id 848
    label "przemokni&#281;cie"
  ]
  node [
    id 849
    label "nasycenie_si&#281;"
  ]
  node [
    id 850
    label "zacz&#281;cie"
  ]
  node [
    id 851
    label "stanie_si&#281;"
  ]
  node [
    id 852
    label "offense"
  ]
  node [
    id 853
    label "przestanie"
  ]
  node [
    id 854
    label "odnaj&#281;cie"
  ]
  node [
    id 855
    label "naj&#281;cie"
  ]
  node [
    id 856
    label "rewizja"
  ]
  node [
    id 857
    label "passage"
  ]
  node [
    id 858
    label "oznaka"
  ]
  node [
    id 859
    label "change"
  ]
  node [
    id 860
    label "ferment"
  ]
  node [
    id 861
    label "komplet"
  ]
  node [
    id 862
    label "anatomopatolog"
  ]
  node [
    id 863
    label "zmianka"
  ]
  node [
    id 864
    label "czas"
  ]
  node [
    id 865
    label "amendment"
  ]
  node [
    id 866
    label "odmienianie"
  ]
  node [
    id 867
    label "tura"
  ]
  node [
    id 868
    label "boski"
  ]
  node [
    id 869
    label "krajobraz"
  ]
  node [
    id 870
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 871
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 872
    label "przywidzenie"
  ]
  node [
    id 873
    label "presence"
  ]
  node [
    id 874
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 875
    label "lekcja"
  ]
  node [
    id 876
    label "ensemble"
  ]
  node [
    id 877
    label "klasa"
  ]
  node [
    id 878
    label "zestaw"
  ]
  node [
    id 879
    label "poprzedzanie"
  ]
  node [
    id 880
    label "czasoprzestrze&#324;"
  ]
  node [
    id 881
    label "laba"
  ]
  node [
    id 882
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 883
    label "chronometria"
  ]
  node [
    id 884
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 885
    label "rachuba_czasu"
  ]
  node [
    id 886
    label "przep&#322;ywanie"
  ]
  node [
    id 887
    label "czasokres"
  ]
  node [
    id 888
    label "odczyt"
  ]
  node [
    id 889
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 890
    label "dzieje"
  ]
  node [
    id 891
    label "poprzedzenie"
  ]
  node [
    id 892
    label "trawienie"
  ]
  node [
    id 893
    label "pochodzi&#263;"
  ]
  node [
    id 894
    label "period"
  ]
  node [
    id 895
    label "okres_czasu"
  ]
  node [
    id 896
    label "poprzedza&#263;"
  ]
  node [
    id 897
    label "schy&#322;ek"
  ]
  node [
    id 898
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 899
    label "odwlekanie_si&#281;"
  ]
  node [
    id 900
    label "zegar"
  ]
  node [
    id 901
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 902
    label "czwarty_wymiar"
  ]
  node [
    id 903
    label "pochodzenie"
  ]
  node [
    id 904
    label "Zeitgeist"
  ]
  node [
    id 905
    label "trawi&#263;"
  ]
  node [
    id 906
    label "pogoda"
  ]
  node [
    id 907
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 908
    label "poprzedzi&#263;"
  ]
  node [
    id 909
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 910
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 911
    label "time_period"
  ]
  node [
    id 912
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 913
    label "implikowa&#263;"
  ]
  node [
    id 914
    label "signal"
  ]
  node [
    id 915
    label "symbol"
  ]
  node [
    id 916
    label "proces_my&#347;lowy"
  ]
  node [
    id 917
    label "dow&#243;d"
  ]
  node [
    id 918
    label "krytyka"
  ]
  node [
    id 919
    label "rekurs"
  ]
  node [
    id 920
    label "checkup"
  ]
  node [
    id 921
    label "kontrola"
  ]
  node [
    id 922
    label "odwo&#322;anie"
  ]
  node [
    id 923
    label "correction"
  ]
  node [
    id 924
    label "przegl&#261;d"
  ]
  node [
    id 925
    label "kipisz"
  ]
  node [
    id 926
    label "korekta"
  ]
  node [
    id 927
    label "bia&#322;ko"
  ]
  node [
    id 928
    label "immobilizowa&#263;"
  ]
  node [
    id 929
    label "poruszenie"
  ]
  node [
    id 930
    label "immobilizacja"
  ]
  node [
    id 931
    label "apoenzym"
  ]
  node [
    id 932
    label "zymaza"
  ]
  node [
    id 933
    label "enzyme"
  ]
  node [
    id 934
    label "immobilizowanie"
  ]
  node [
    id 935
    label "biokatalizator"
  ]
  node [
    id 936
    label "patolog"
  ]
  node [
    id 937
    label "anatom"
  ]
  node [
    id 938
    label "zamiana"
  ]
  node [
    id 939
    label "wymienianie"
  ]
  node [
    id 940
    label "Transfiguration"
  ]
  node [
    id 941
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 942
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 943
    label "umocowa&#263;"
  ]
  node [
    id 944
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 945
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 946
    label "procesualistyka"
  ]
  node [
    id 947
    label "regu&#322;a_Allena"
  ]
  node [
    id 948
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 949
    label "kryminalistyka"
  ]
  node [
    id 950
    label "kierunek"
  ]
  node [
    id 951
    label "zasada_d'Alemberta"
  ]
  node [
    id 952
    label "obserwacja"
  ]
  node [
    id 953
    label "normatywizm"
  ]
  node [
    id 954
    label "jurisprudence"
  ]
  node [
    id 955
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 956
    label "kultura_duchowa"
  ]
  node [
    id 957
    label "przepis"
  ]
  node [
    id 958
    label "prawo_karne_procesowe"
  ]
  node [
    id 959
    label "criterion"
  ]
  node [
    id 960
    label "kazuistyka"
  ]
  node [
    id 961
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 962
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 963
    label "kryminologia"
  ]
  node [
    id 964
    label "opis"
  ]
  node [
    id 965
    label "regu&#322;a_Glogera"
  ]
  node [
    id 966
    label "prawo_Mendla"
  ]
  node [
    id 967
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 968
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 969
    label "prawo_karne"
  ]
  node [
    id 970
    label "legislacyjnie"
  ]
  node [
    id 971
    label "twierdzenie"
  ]
  node [
    id 972
    label "cywilistyka"
  ]
  node [
    id 973
    label "judykatura"
  ]
  node [
    id 974
    label "kanonistyka"
  ]
  node [
    id 975
    label "standard"
  ]
  node [
    id 976
    label "nauka_prawa"
  ]
  node [
    id 977
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 978
    label "podmiot"
  ]
  node [
    id 979
    label "law"
  ]
  node [
    id 980
    label "qualification"
  ]
  node [
    id 981
    label "dominion"
  ]
  node [
    id 982
    label "wykonawczy"
  ]
  node [
    id 983
    label "zasada"
  ]
  node [
    id 984
    label "normalizacja"
  ]
  node [
    id 985
    label "exposition"
  ]
  node [
    id 986
    label "model"
  ]
  node [
    id 987
    label "organizowa&#263;"
  ]
  node [
    id 988
    label "ordinariness"
  ]
  node [
    id 989
    label "instytucja"
  ]
  node [
    id 990
    label "zorganizowa&#263;"
  ]
  node [
    id 991
    label "taniec_towarzyski"
  ]
  node [
    id 992
    label "organizowanie"
  ]
  node [
    id 993
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 994
    label "zorganizowanie"
  ]
  node [
    id 995
    label "mechanika"
  ]
  node [
    id 996
    label "o&#347;"
  ]
  node [
    id 997
    label "usenet"
  ]
  node [
    id 998
    label "rozprz&#261;c"
  ]
  node [
    id 999
    label "zachowanie"
  ]
  node [
    id 1000
    label "cybernetyk"
  ]
  node [
    id 1001
    label "podsystem"
  ]
  node [
    id 1002
    label "system"
  ]
  node [
    id 1003
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1004
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1005
    label "sk&#322;ad"
  ]
  node [
    id 1006
    label "systemat"
  ]
  node [
    id 1007
    label "konstrukcja"
  ]
  node [
    id 1008
    label "konstelacja"
  ]
  node [
    id 1009
    label "przebieg"
  ]
  node [
    id 1010
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1011
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1012
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1013
    label "praktyka"
  ]
  node [
    id 1014
    label "przeorientowywanie"
  ]
  node [
    id 1015
    label "studia"
  ]
  node [
    id 1016
    label "linia"
  ]
  node [
    id 1017
    label "bok"
  ]
  node [
    id 1018
    label "skr&#281;canie"
  ]
  node [
    id 1019
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1020
    label "przeorientowywa&#263;"
  ]
  node [
    id 1021
    label "orientowanie"
  ]
  node [
    id 1022
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1023
    label "przeorientowanie"
  ]
  node [
    id 1024
    label "zorientowanie"
  ]
  node [
    id 1025
    label "przeorientowa&#263;"
  ]
  node [
    id 1026
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1027
    label "metoda"
  ]
  node [
    id 1028
    label "ty&#322;"
  ]
  node [
    id 1029
    label "zorientowa&#263;"
  ]
  node [
    id 1030
    label "orientowa&#263;"
  ]
  node [
    id 1031
    label "orientacja"
  ]
  node [
    id 1032
    label "prz&#243;d"
  ]
  node [
    id 1033
    label "bearing"
  ]
  node [
    id 1034
    label "skr&#281;cenie"
  ]
  node [
    id 1035
    label "do&#347;wiadczenie"
  ]
  node [
    id 1036
    label "teren_szko&#322;y"
  ]
  node [
    id 1037
    label "Mickiewicz"
  ]
  node [
    id 1038
    label "kwalifikacje"
  ]
  node [
    id 1039
    label "podr&#281;cznik"
  ]
  node [
    id 1040
    label "absolwent"
  ]
  node [
    id 1041
    label "school"
  ]
  node [
    id 1042
    label "zda&#263;"
  ]
  node [
    id 1043
    label "gabinet"
  ]
  node [
    id 1044
    label "urszulanki"
  ]
  node [
    id 1045
    label "sztuba"
  ]
  node [
    id 1046
    label "&#322;awa_szkolna"
  ]
  node [
    id 1047
    label "nauka"
  ]
  node [
    id 1048
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 1049
    label "przepisa&#263;"
  ]
  node [
    id 1050
    label "muzyka"
  ]
  node [
    id 1051
    label "form"
  ]
  node [
    id 1052
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 1053
    label "przepisanie"
  ]
  node [
    id 1054
    label "skolaryzacja"
  ]
  node [
    id 1055
    label "zdanie"
  ]
  node [
    id 1056
    label "stopek"
  ]
  node [
    id 1057
    label "lesson"
  ]
  node [
    id 1058
    label "niepokalanki"
  ]
  node [
    id 1059
    label "szkolenie"
  ]
  node [
    id 1060
    label "tablica"
  ]
  node [
    id 1061
    label "posiada&#263;"
  ]
  node [
    id 1062
    label "egzekutywa"
  ]
  node [
    id 1063
    label "potencja&#322;"
  ]
  node [
    id 1064
    label "wyb&#243;r"
  ]
  node [
    id 1065
    label "prospect"
  ]
  node [
    id 1066
    label "ability"
  ]
  node [
    id 1067
    label "obliczeniowo"
  ]
  node [
    id 1068
    label "alternatywa"
  ]
  node [
    id 1069
    label "operator_modalny"
  ]
  node [
    id 1070
    label "badanie"
  ]
  node [
    id 1071
    label "remark"
  ]
  node [
    id 1072
    label "stwierdzenie"
  ]
  node [
    id 1073
    label "observation"
  ]
  node [
    id 1074
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 1075
    label "alternatywa_Fredholma"
  ]
  node [
    id 1076
    label "oznajmianie"
  ]
  node [
    id 1077
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 1078
    label "teoria"
  ]
  node [
    id 1079
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 1080
    label "paradoks_Leontiefa"
  ]
  node [
    id 1081
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 1082
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 1083
    label "teza"
  ]
  node [
    id 1084
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 1085
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 1086
    label "twierdzenie_Pettisa"
  ]
  node [
    id 1087
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 1088
    label "twierdzenie_Maya"
  ]
  node [
    id 1089
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 1090
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 1091
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 1092
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 1093
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 1094
    label "zapewnianie"
  ]
  node [
    id 1095
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 1096
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 1097
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 1098
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 1099
    label "twierdzenie_Stokesa"
  ]
  node [
    id 1100
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 1101
    label "twierdzenie_Cevy"
  ]
  node [
    id 1102
    label "twierdzenie_Pascala"
  ]
  node [
    id 1103
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 1104
    label "komunikowanie"
  ]
  node [
    id 1105
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 1106
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 1107
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 1108
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 1109
    label "calibration"
  ]
  node [
    id 1110
    label "operacja"
  ]
  node [
    id 1111
    label "porz&#261;dek"
  ]
  node [
    id 1112
    label "dominance"
  ]
  node [
    id 1113
    label "zabieg"
  ]
  node [
    id 1114
    label "standardization"
  ]
  node [
    id 1115
    label "orzecznictwo"
  ]
  node [
    id 1116
    label "wykonawczo"
  ]
  node [
    id 1117
    label "organizacja"
  ]
  node [
    id 1118
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 1119
    label "nada&#263;"
  ]
  node [
    id 1120
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 1121
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 1122
    label "cook"
  ]
  node [
    id 1123
    label "procedura"
  ]
  node [
    id 1124
    label "norma_prawna"
  ]
  node [
    id 1125
    label "przedawnienie_si&#281;"
  ]
  node [
    id 1126
    label "przedawnianie_si&#281;"
  ]
  node [
    id 1127
    label "porada"
  ]
  node [
    id 1128
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 1129
    label "regulation"
  ]
  node [
    id 1130
    label "recepta"
  ]
  node [
    id 1131
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 1132
    label "kodeks"
  ]
  node [
    id 1133
    label "umowa"
  ]
  node [
    id 1134
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1135
    label "moralno&#347;&#263;"
  ]
  node [
    id 1136
    label "occupation"
  ]
  node [
    id 1137
    label "substancja"
  ]
  node [
    id 1138
    label "prawid&#322;o"
  ]
  node [
    id 1139
    label "casuistry"
  ]
  node [
    id 1140
    label "manipulacja"
  ]
  node [
    id 1141
    label "probabilizm"
  ]
  node [
    id 1142
    label "dermatoglifika"
  ]
  node [
    id 1143
    label "mikro&#347;lad"
  ]
  node [
    id 1144
    label "technika_&#347;ledcza"
  ]
  node [
    id 1145
    label "dzia&#322;"
  ]
  node [
    id 1146
    label "prawnicy"
  ]
  node [
    id 1147
    label "legal_profession"
  ]
  node [
    id 1148
    label "&#347;rodowisko"
  ]
  node [
    id 1149
    label "korpus_urz&#281;dniczy"
  ]
  node [
    id 1150
    label "pragmatyka"
  ]
  node [
    id 1151
    label "konstytucyjnoprawny"
  ]
  node [
    id 1152
    label "prawniczo"
  ]
  node [
    id 1153
    label "prawnie"
  ]
  node [
    id 1154
    label "jurydyczny"
  ]
  node [
    id 1155
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1156
    label "urz&#281;dowo"
  ]
  node [
    id 1157
    label "prawniczy"
  ]
  node [
    id 1158
    label "kancelaria"
  ]
  node [
    id 1159
    label "position"
  ]
  node [
    id 1160
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 1161
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1162
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1163
    label "mianowaniec"
  ]
  node [
    id 1164
    label "okienko"
  ]
  node [
    id 1165
    label "w&#322;adza"
  ]
  node [
    id 1166
    label "biuro"
  ]
  node [
    id 1167
    label "boks"
  ]
  node [
    id 1168
    label "biurko"
  ]
  node [
    id 1169
    label "regent"
  ]
  node [
    id 1170
    label "palestra"
  ]
  node [
    id 1171
    label "chancellery"
  ]
  node [
    id 1172
    label "pomieszczenie"
  ]
  node [
    id 1173
    label "technika"
  ]
  node [
    id 1174
    label "impression"
  ]
  node [
    id 1175
    label "pismo"
  ]
  node [
    id 1176
    label "publikacja"
  ]
  node [
    id 1177
    label "glif"
  ]
  node [
    id 1178
    label "dese&#324;"
  ]
  node [
    id 1179
    label "prohibita"
  ]
  node [
    id 1180
    label "cymelium"
  ]
  node [
    id 1181
    label "tkanina"
  ]
  node [
    id 1182
    label "zaproszenie"
  ]
  node [
    id 1183
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 1184
    label "formatowa&#263;"
  ]
  node [
    id 1185
    label "formatowanie"
  ]
  node [
    id 1186
    label "zdobnik"
  ]
  node [
    id 1187
    label "printing"
  ]
  node [
    id 1188
    label "telekomunikacja"
  ]
  node [
    id 1189
    label "cywilizacja"
  ]
  node [
    id 1190
    label "sprawno&#347;&#263;"
  ]
  node [
    id 1191
    label "engineering"
  ]
  node [
    id 1192
    label "fotowoltaika"
  ]
  node [
    id 1193
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1194
    label "teletechnika"
  ]
  node [
    id 1195
    label "mechanika_precyzyjna"
  ]
  node [
    id 1196
    label "technologia"
  ]
  node [
    id 1197
    label "work"
  ]
  node [
    id 1198
    label "psychotest"
  ]
  node [
    id 1199
    label "wk&#322;ad"
  ]
  node [
    id 1200
    label "handwriting"
  ]
  node [
    id 1201
    label "przekaz"
  ]
  node [
    id 1202
    label "dzie&#322;o"
  ]
  node [
    id 1203
    label "paleograf"
  ]
  node [
    id 1204
    label "interpunkcja"
  ]
  node [
    id 1205
    label "grafia"
  ]
  node [
    id 1206
    label "communication"
  ]
  node [
    id 1207
    label "script"
  ]
  node [
    id 1208
    label "zajawka"
  ]
  node [
    id 1209
    label "list"
  ]
  node [
    id 1210
    label "Zwrotnica"
  ]
  node [
    id 1211
    label "czasopismo"
  ]
  node [
    id 1212
    label "ok&#322;adka"
  ]
  node [
    id 1213
    label "ortografia"
  ]
  node [
    id 1214
    label "letter"
  ]
  node [
    id 1215
    label "komunikacja"
  ]
  node [
    id 1216
    label "paleografia"
  ]
  node [
    id 1217
    label "j&#281;zyk"
  ]
  node [
    id 1218
    label "prasa"
  ]
  node [
    id 1219
    label "design"
  ]
  node [
    id 1220
    label "produkcja"
  ]
  node [
    id 1221
    label "notification"
  ]
  node [
    id 1222
    label "pru&#263;_si&#281;"
  ]
  node [
    id 1223
    label "materia&#322;"
  ]
  node [
    id 1224
    label "maglownia"
  ]
  node [
    id 1225
    label "opalarnia"
  ]
  node [
    id 1226
    label "prucie_si&#281;"
  ]
  node [
    id 1227
    label "splot"
  ]
  node [
    id 1228
    label "karbonizowa&#263;"
  ]
  node [
    id 1229
    label "rozpru&#263;_si&#281;"
  ]
  node [
    id 1230
    label "karbonizacja"
  ]
  node [
    id 1231
    label "rozprucie_si&#281;"
  ]
  node [
    id 1232
    label "towar"
  ]
  node [
    id 1233
    label "apretura"
  ]
  node [
    id 1234
    label "ekscerpcja"
  ]
  node [
    id 1235
    label "j&#281;zykowo"
  ]
  node [
    id 1236
    label "redakcja"
  ]
  node [
    id 1237
    label "pomini&#281;cie"
  ]
  node [
    id 1238
    label "preparacja"
  ]
  node [
    id 1239
    label "odmianka"
  ]
  node [
    id 1240
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1241
    label "koniektura"
  ]
  node [
    id 1242
    label "pisa&#263;"
  ]
  node [
    id 1243
    label "obelga"
  ]
  node [
    id 1244
    label "kr&#243;j"
  ]
  node [
    id 1245
    label "splay"
  ]
  node [
    id 1246
    label "czcionka"
  ]
  node [
    id 1247
    label "pro&#347;ba"
  ]
  node [
    id 1248
    label "invitation"
  ]
  node [
    id 1249
    label "karteczka"
  ]
  node [
    id 1250
    label "zaproponowanie"
  ]
  node [
    id 1251
    label "propozycja"
  ]
  node [
    id 1252
    label "wzajemno&#347;&#263;"
  ]
  node [
    id 1253
    label "podw&#243;jno&#347;&#263;"
  ]
  node [
    id 1254
    label "edytowa&#263;"
  ]
  node [
    id 1255
    label "sk&#322;ada&#263;"
  ]
  node [
    id 1256
    label "przygotowywa&#263;"
  ]
  node [
    id 1257
    label "format"
  ]
  node [
    id 1258
    label "przygotowywanie"
  ]
  node [
    id 1259
    label "sk&#322;adanie"
  ]
  node [
    id 1260
    label "edytowanie"
  ]
  node [
    id 1261
    label "dostosowywanie"
  ]
  node [
    id 1262
    label "rarytas"
  ]
  node [
    id 1263
    label "r&#281;kopis"
  ]
  node [
    id 1264
    label "ojciec"
  ]
  node [
    id 1265
    label "i"
  ]
  node [
    id 1266
    label "J&#243;zefa"
  ]
  node [
    id 1267
    label "Zych"
  ]
  node [
    id 1268
    label "polskie"
  ]
  node [
    id 1269
    label "stronnictwo"
  ]
  node [
    id 1270
    label "ludowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 1265
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 1265
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 1265
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 1265
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 5
    target 560
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 563
  ]
  edge [
    source 5
    target 564
  ]
  edge [
    source 5
    target 565
  ]
  edge [
    source 5
    target 566
  ]
  edge [
    source 5
    target 567
  ]
  edge [
    source 5
    target 568
  ]
  edge [
    source 5
    target 569
  ]
  edge [
    source 5
    target 570
  ]
  edge [
    source 5
    target 571
  ]
  edge [
    source 5
    target 572
  ]
  edge [
    source 5
    target 573
  ]
  edge [
    source 5
    target 574
  ]
  edge [
    source 5
    target 575
  ]
  edge [
    source 5
    target 576
  ]
  edge [
    source 5
    target 577
  ]
  edge [
    source 5
    target 578
  ]
  edge [
    source 5
    target 579
  ]
  edge [
    source 5
    target 580
  ]
  edge [
    source 5
    target 581
  ]
  edge [
    source 5
    target 582
  ]
  edge [
    source 5
    target 583
  ]
  edge [
    source 5
    target 584
  ]
  edge [
    source 5
    target 585
  ]
  edge [
    source 5
    target 586
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 587
  ]
  edge [
    source 5
    target 588
  ]
  edge [
    source 5
    target 589
  ]
  edge [
    source 5
    target 590
  ]
  edge [
    source 5
    target 591
  ]
  edge [
    source 5
    target 592
  ]
  edge [
    source 5
    target 593
  ]
  edge [
    source 5
    target 594
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 595
  ]
  edge [
    source 5
    target 596
  ]
  edge [
    source 5
    target 597
  ]
  edge [
    source 5
    target 598
  ]
  edge [
    source 5
    target 599
  ]
  edge [
    source 5
    target 600
  ]
  edge [
    source 5
    target 601
  ]
  edge [
    source 5
    target 602
  ]
  edge [
    source 5
    target 603
  ]
  edge [
    source 5
    target 604
  ]
  edge [
    source 5
    target 605
  ]
  edge [
    source 5
    target 606
  ]
  edge [
    source 5
    target 607
  ]
  edge [
    source 5
    target 608
  ]
  edge [
    source 5
    target 609
  ]
  edge [
    source 5
    target 610
  ]
  edge [
    source 5
    target 611
  ]
  edge [
    source 5
    target 612
  ]
  edge [
    source 5
    target 613
  ]
  edge [
    source 5
    target 614
  ]
  edge [
    source 5
    target 615
  ]
  edge [
    source 5
    target 616
  ]
  edge [
    source 5
    target 617
  ]
  edge [
    source 5
    target 618
  ]
  edge [
    source 5
    target 619
  ]
  edge [
    source 5
    target 620
  ]
  edge [
    source 5
    target 621
  ]
  edge [
    source 5
    target 622
  ]
  edge [
    source 5
    target 623
  ]
  edge [
    source 5
    target 624
  ]
  edge [
    source 5
    target 625
  ]
  edge [
    source 5
    target 626
  ]
  edge [
    source 5
    target 627
  ]
  edge [
    source 5
    target 628
  ]
  edge [
    source 5
    target 629
  ]
  edge [
    source 5
    target 630
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 631
  ]
  edge [
    source 5
    target 632
  ]
  edge [
    source 5
    target 633
  ]
  edge [
    source 5
    target 634
  ]
  edge [
    source 5
    target 635
  ]
  edge [
    source 5
    target 636
  ]
  edge [
    source 5
    target 637
  ]
  edge [
    source 5
    target 638
  ]
  edge [
    source 5
    target 639
  ]
  edge [
    source 5
    target 640
  ]
  edge [
    source 5
    target 641
  ]
  edge [
    source 5
    target 642
  ]
  edge [
    source 5
    target 643
  ]
  edge [
    source 5
    target 644
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 7
    target 702
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 704
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 706
  ]
  edge [
    source 7
    target 707
  ]
  edge [
    source 7
    target 708
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 709
  ]
  edge [
    source 7
    target 710
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 711
  ]
  edge [
    source 7
    target 712
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 713
  ]
  edge [
    source 7
    target 714
  ]
  edge [
    source 7
    target 715
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 716
  ]
  edge [
    source 7
    target 717
  ]
  edge [
    source 7
    target 718
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 719
  ]
  edge [
    source 7
    target 720
  ]
  edge [
    source 7
    target 721
  ]
  edge [
    source 7
    target 722
  ]
  edge [
    source 7
    target 723
  ]
  edge [
    source 7
    target 724
  ]
  edge [
    source 7
    target 725
  ]
  edge [
    source 7
    target 726
  ]
  edge [
    source 7
    target 727
  ]
  edge [
    source 7
    target 728
  ]
  edge [
    source 7
    target 729
  ]
  edge [
    source 7
    target 730
  ]
  edge [
    source 7
    target 731
  ]
  edge [
    source 7
    target 732
  ]
  edge [
    source 7
    target 733
  ]
  edge [
    source 7
    target 734
  ]
  edge [
    source 7
    target 735
  ]
  edge [
    source 7
    target 736
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 737
  ]
  edge [
    source 7
    target 738
  ]
  edge [
    source 7
    target 739
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 740
  ]
  edge [
    source 7
    target 741
  ]
  edge [
    source 7
    target 742
  ]
  edge [
    source 7
    target 743
  ]
  edge [
    source 7
    target 744
  ]
  edge [
    source 7
    target 745
  ]
  edge [
    source 7
    target 746
  ]
  edge [
    source 7
    target 747
  ]
  edge [
    source 7
    target 748
  ]
  edge [
    source 7
    target 749
  ]
  edge [
    source 7
    target 750
  ]
  edge [
    source 7
    target 751
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 760
  ]
  edge [
    source 8
    target 761
  ]
  edge [
    source 8
    target 762
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 763
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 769
  ]
  edge [
    source 8
    target 770
  ]
  edge [
    source 8
    target 771
  ]
  edge [
    source 8
    target 772
  ]
  edge [
    source 8
    target 773
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 774
  ]
  edge [
    source 8
    target 775
  ]
  edge [
    source 8
    target 776
  ]
  edge [
    source 8
    target 777
  ]
  edge [
    source 8
    target 778
  ]
  edge [
    source 8
    target 779
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 780
  ]
  edge [
    source 8
    target 781
  ]
  edge [
    source 8
    target 782
  ]
  edge [
    source 8
    target 783
  ]
  edge [
    source 8
    target 784
  ]
  edge [
    source 8
    target 785
  ]
  edge [
    source 8
    target 786
  ]
  edge [
    source 8
    target 787
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 788
  ]
  edge [
    source 8
    target 789
  ]
  edge [
    source 8
    target 790
  ]
  edge [
    source 8
    target 791
  ]
  edge [
    source 8
    target 792
  ]
  edge [
    source 8
    target 793
  ]
  edge [
    source 8
    target 794
  ]
  edge [
    source 8
    target 795
  ]
  edge [
    source 8
    target 796
  ]
  edge [
    source 8
    target 797
  ]
  edge [
    source 8
    target 798
  ]
  edge [
    source 8
    target 799
  ]
  edge [
    source 8
    target 800
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 801
  ]
  edge [
    source 8
    target 802
  ]
  edge [
    source 8
    target 803
  ]
  edge [
    source 8
    target 804
  ]
  edge [
    source 8
    target 805
  ]
  edge [
    source 8
    target 806
  ]
  edge [
    source 8
    target 807
  ]
  edge [
    source 8
    target 808
  ]
  edge [
    source 8
    target 809
  ]
  edge [
    source 8
    target 810
  ]
  edge [
    source 8
    target 811
  ]
  edge [
    source 8
    target 812
  ]
  edge [
    source 8
    target 813
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 814
  ]
  edge [
    source 8
    target 815
  ]
  edge [
    source 8
    target 816
  ]
  edge [
    source 8
    target 817
  ]
  edge [
    source 8
    target 818
  ]
  edge [
    source 8
    target 819
  ]
  edge [
    source 8
    target 820
  ]
  edge [
    source 8
    target 821
  ]
  edge [
    source 8
    target 822
  ]
  edge [
    source 8
    target 823
  ]
  edge [
    source 8
    target 824
  ]
  edge [
    source 8
    target 825
  ]
  edge [
    source 8
    target 826
  ]
  edge [
    source 8
    target 827
  ]
  edge [
    source 8
    target 828
  ]
  edge [
    source 8
    target 829
  ]
  edge [
    source 8
    target 830
  ]
  edge [
    source 8
    target 831
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 832
  ]
  edge [
    source 8
    target 833
  ]
  edge [
    source 8
    target 834
  ]
  edge [
    source 8
    target 835
  ]
  edge [
    source 8
    target 836
  ]
  edge [
    source 8
    target 837
  ]
  edge [
    source 8
    target 838
  ]
  edge [
    source 8
    target 839
  ]
  edge [
    source 8
    target 840
  ]
  edge [
    source 8
    target 841
  ]
  edge [
    source 8
    target 842
  ]
  edge [
    source 8
    target 843
  ]
  edge [
    source 8
    target 844
  ]
  edge [
    source 8
    target 845
  ]
  edge [
    source 8
    target 846
  ]
  edge [
    source 8
    target 847
  ]
  edge [
    source 8
    target 848
  ]
  edge [
    source 8
    target 849
  ]
  edge [
    source 8
    target 850
  ]
  edge [
    source 8
    target 851
  ]
  edge [
    source 8
    target 852
  ]
  edge [
    source 8
    target 853
  ]
  edge [
    source 8
    target 854
  ]
  edge [
    source 8
    target 855
  ]
  edge [
    source 8
    target 8
  ]
  edge [
    source 8
    target 1264
  ]
  edge [
    source 9
    target 856
  ]
  edge [
    source 9
    target 857
  ]
  edge [
    source 9
    target 858
  ]
  edge [
    source 9
    target 859
  ]
  edge [
    source 9
    target 860
  ]
  edge [
    source 9
    target 861
  ]
  edge [
    source 9
    target 862
  ]
  edge [
    source 9
    target 863
  ]
  edge [
    source 9
    target 864
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 865
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 866
  ]
  edge [
    source 9
    target 867
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 868
  ]
  edge [
    source 9
    target 869
  ]
  edge [
    source 9
    target 870
  ]
  edge [
    source 9
    target 871
  ]
  edge [
    source 9
    target 872
  ]
  edge [
    source 9
    target 873
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 874
  ]
  edge [
    source 9
    target 875
  ]
  edge [
    source 9
    target 876
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 877
  ]
  edge [
    source 9
    target 878
  ]
  edge [
    source 9
    target 879
  ]
  edge [
    source 9
    target 880
  ]
  edge [
    source 9
    target 881
  ]
  edge [
    source 9
    target 882
  ]
  edge [
    source 9
    target 883
  ]
  edge [
    source 9
    target 884
  ]
  edge [
    source 9
    target 885
  ]
  edge [
    source 9
    target 886
  ]
  edge [
    source 9
    target 826
  ]
  edge [
    source 9
    target 887
  ]
  edge [
    source 9
    target 888
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 889
  ]
  edge [
    source 9
    target 890
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 891
  ]
  edge [
    source 9
    target 892
  ]
  edge [
    source 9
    target 893
  ]
  edge [
    source 9
    target 894
  ]
  edge [
    source 9
    target 895
  ]
  edge [
    source 9
    target 896
  ]
  edge [
    source 9
    target 897
  ]
  edge [
    source 9
    target 898
  ]
  edge [
    source 9
    target 899
  ]
  edge [
    source 9
    target 900
  ]
  edge [
    source 9
    target 901
  ]
  edge [
    source 9
    target 902
  ]
  edge [
    source 9
    target 903
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 904
  ]
  edge [
    source 9
    target 905
  ]
  edge [
    source 9
    target 906
  ]
  edge [
    source 9
    target 907
  ]
  edge [
    source 9
    target 908
  ]
  edge [
    source 9
    target 909
  ]
  edge [
    source 9
    target 910
  ]
  edge [
    source 9
    target 911
  ]
  edge [
    source 9
    target 912
  ]
  edge [
    source 9
    target 913
  ]
  edge [
    source 9
    target 914
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 915
  ]
  edge [
    source 9
    target 916
  ]
  edge [
    source 9
    target 917
  ]
  edge [
    source 9
    target 918
  ]
  edge [
    source 9
    target 919
  ]
  edge [
    source 9
    target 920
  ]
  edge [
    source 9
    target 921
  ]
  edge [
    source 9
    target 922
  ]
  edge [
    source 9
    target 923
  ]
  edge [
    source 9
    target 924
  ]
  edge [
    source 9
    target 925
  ]
  edge [
    source 9
    target 926
  ]
  edge [
    source 9
    target 927
  ]
  edge [
    source 9
    target 928
  ]
  edge [
    source 9
    target 929
  ]
  edge [
    source 9
    target 930
  ]
  edge [
    source 9
    target 931
  ]
  edge [
    source 9
    target 932
  ]
  edge [
    source 9
    target 933
  ]
  edge [
    source 9
    target 934
  ]
  edge [
    source 9
    target 935
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 936
  ]
  edge [
    source 9
    target 937
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 938
  ]
  edge [
    source 9
    target 939
  ]
  edge [
    source 9
    target 940
  ]
  edge [
    source 9
    target 941
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 942
  ]
  edge [
    source 10
    target 943
  ]
  edge [
    source 10
    target 944
  ]
  edge [
    source 10
    target 945
  ]
  edge [
    source 10
    target 946
  ]
  edge [
    source 10
    target 947
  ]
  edge [
    source 10
    target 948
  ]
  edge [
    source 10
    target 949
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 950
  ]
  edge [
    source 10
    target 951
  ]
  edge [
    source 10
    target 952
  ]
  edge [
    source 10
    target 953
  ]
  edge [
    source 10
    target 954
  ]
  edge [
    source 10
    target 955
  ]
  edge [
    source 10
    target 956
  ]
  edge [
    source 10
    target 957
  ]
  edge [
    source 10
    target 958
  ]
  edge [
    source 10
    target 959
  ]
  edge [
    source 10
    target 960
  ]
  edge [
    source 10
    target 961
  ]
  edge [
    source 10
    target 962
  ]
  edge [
    source 10
    target 963
  ]
  edge [
    source 10
    target 964
  ]
  edge [
    source 10
    target 965
  ]
  edge [
    source 10
    target 966
  ]
  edge [
    source 10
    target 967
  ]
  edge [
    source 10
    target 968
  ]
  edge [
    source 10
    target 969
  ]
  edge [
    source 10
    target 970
  ]
  edge [
    source 10
    target 971
  ]
  edge [
    source 10
    target 972
  ]
  edge [
    source 10
    target 973
  ]
  edge [
    source 10
    target 974
  ]
  edge [
    source 10
    target 975
  ]
  edge [
    source 10
    target 976
  ]
  edge [
    source 10
    target 977
  ]
  edge [
    source 10
    target 978
  ]
  edge [
    source 10
    target 979
  ]
  edge [
    source 10
    target 980
  ]
  edge [
    source 10
    target 981
  ]
  edge [
    source 10
    target 982
  ]
  edge [
    source 10
    target 983
  ]
  edge [
    source 10
    target 984
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 985
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 986
  ]
  edge [
    source 10
    target 987
  ]
  edge [
    source 10
    target 988
  ]
  edge [
    source 10
    target 989
  ]
  edge [
    source 10
    target 990
  ]
  edge [
    source 10
    target 991
  ]
  edge [
    source 10
    target 992
  ]
  edge [
    source 10
    target 993
  ]
  edge [
    source 10
    target 994
  ]
  edge [
    source 10
    target 995
  ]
  edge [
    source 10
    target 996
  ]
  edge [
    source 10
    target 997
  ]
  edge [
    source 10
    target 998
  ]
  edge [
    source 10
    target 999
  ]
  edge [
    source 10
    target 1000
  ]
  edge [
    source 10
    target 1001
  ]
  edge [
    source 10
    target 1002
  ]
  edge [
    source 10
    target 1003
  ]
  edge [
    source 10
    target 1004
  ]
  edge [
    source 10
    target 1005
  ]
  edge [
    source 10
    target 1006
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 1007
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 1008
  ]
  edge [
    source 10
    target 1009
  ]
  edge [
    source 10
    target 1010
  ]
  edge [
    source 10
    target 1011
  ]
  edge [
    source 10
    target 1012
  ]
  edge [
    source 10
    target 1013
  ]
  edge [
    source 10
    target 1014
  ]
  edge [
    source 10
    target 1015
  ]
  edge [
    source 10
    target 1016
  ]
  edge [
    source 10
    target 1017
  ]
  edge [
    source 10
    target 1018
  ]
  edge [
    source 10
    target 1019
  ]
  edge [
    source 10
    target 1020
  ]
  edge [
    source 10
    target 1021
  ]
  edge [
    source 10
    target 1022
  ]
  edge [
    source 10
    target 1023
  ]
  edge [
    source 10
    target 1024
  ]
  edge [
    source 10
    target 1025
  ]
  edge [
    source 10
    target 1026
  ]
  edge [
    source 10
    target 1027
  ]
  edge [
    source 10
    target 1028
  ]
  edge [
    source 10
    target 1029
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 1030
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 1031
  ]
  edge [
    source 10
    target 1032
  ]
  edge [
    source 10
    target 1033
  ]
  edge [
    source 10
    target 1034
  ]
  edge [
    source 10
    target 1035
  ]
  edge [
    source 10
    target 1036
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 1037
  ]
  edge [
    source 10
    target 1038
  ]
  edge [
    source 10
    target 1039
  ]
  edge [
    source 10
    target 1040
  ]
  edge [
    source 10
    target 1041
  ]
  edge [
    source 10
    target 1042
  ]
  edge [
    source 10
    target 1043
  ]
  edge [
    source 10
    target 1044
  ]
  edge [
    source 10
    target 1045
  ]
  edge [
    source 10
    target 1046
  ]
  edge [
    source 10
    target 1047
  ]
  edge [
    source 10
    target 1048
  ]
  edge [
    source 10
    target 1049
  ]
  edge [
    source 10
    target 1050
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 1051
  ]
  edge [
    source 10
    target 877
  ]
  edge [
    source 10
    target 875
  ]
  edge [
    source 10
    target 1052
  ]
  edge [
    source 10
    target 1053
  ]
  edge [
    source 10
    target 864
  ]
  edge [
    source 10
    target 1054
  ]
  edge [
    source 10
    target 1055
  ]
  edge [
    source 10
    target 1056
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 1057
  ]
  edge [
    source 10
    target 1058
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 1059
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 1060
  ]
  edge [
    source 10
    target 1061
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 1062
  ]
  edge [
    source 10
    target 1063
  ]
  edge [
    source 10
    target 1064
  ]
  edge [
    source 10
    target 1065
  ]
  edge [
    source 10
    target 1066
  ]
  edge [
    source 10
    target 1067
  ]
  edge [
    source 10
    target 1068
  ]
  edge [
    source 10
    target 1069
  ]
  edge [
    source 10
    target 1070
  ]
  edge [
    source 10
    target 916
  ]
  edge [
    source 10
    target 1071
  ]
  edge [
    source 10
    target 1072
  ]
  edge [
    source 10
    target 1073
  ]
  edge [
    source 10
    target 1074
  ]
  edge [
    source 10
    target 1075
  ]
  edge [
    source 10
    target 1076
  ]
  edge [
    source 10
    target 1077
  ]
  edge [
    source 10
    target 1078
  ]
  edge [
    source 10
    target 1079
  ]
  edge [
    source 10
    target 1080
  ]
  edge [
    source 10
    target 21
  ]
  edge [
    source 10
    target 1081
  ]
  edge [
    source 10
    target 1082
  ]
  edge [
    source 10
    target 1083
  ]
  edge [
    source 10
    target 1084
  ]
  edge [
    source 10
    target 1085
  ]
  edge [
    source 10
    target 1086
  ]
  edge [
    source 10
    target 1087
  ]
  edge [
    source 10
    target 1088
  ]
  edge [
    source 10
    target 1089
  ]
  edge [
    source 10
    target 1090
  ]
  edge [
    source 10
    target 1091
  ]
  edge [
    source 10
    target 1092
  ]
  edge [
    source 10
    target 1093
  ]
  edge [
    source 10
    target 1094
  ]
  edge [
    source 10
    target 1095
  ]
  edge [
    source 10
    target 1096
  ]
  edge [
    source 10
    target 1097
  ]
  edge [
    source 10
    target 1098
  ]
  edge [
    source 10
    target 1099
  ]
  edge [
    source 10
    target 1100
  ]
  edge [
    source 10
    target 1101
  ]
  edge [
    source 10
    target 1102
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 1103
  ]
  edge [
    source 10
    target 1104
  ]
  edge [
    source 10
    target 1105
  ]
  edge [
    source 10
    target 1106
  ]
  edge [
    source 10
    target 1107
  ]
  edge [
    source 10
    target 1108
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 1109
  ]
  edge [
    source 10
    target 1110
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 1111
  ]
  edge [
    source 10
    target 1112
  ]
  edge [
    source 10
    target 1113
  ]
  edge [
    source 10
    target 1114
  ]
  edge [
    source 10
    target 1115
  ]
  edge [
    source 10
    target 1116
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 1117
  ]
  edge [
    source 10
    target 1118
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 1119
  ]
  edge [
    source 10
    target 1120
  ]
  edge [
    source 10
    target 1121
  ]
  edge [
    source 10
    target 1122
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 1123
  ]
  edge [
    source 10
    target 1124
  ]
  edge [
    source 10
    target 1125
  ]
  edge [
    source 10
    target 1126
  ]
  edge [
    source 10
    target 1127
  ]
  edge [
    source 10
    target 1128
  ]
  edge [
    source 10
    target 1129
  ]
  edge [
    source 10
    target 1130
  ]
  edge [
    source 10
    target 1131
  ]
  edge [
    source 10
    target 1132
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 1133
  ]
  edge [
    source 10
    target 1134
  ]
  edge [
    source 10
    target 1135
  ]
  edge [
    source 10
    target 1136
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 1137
  ]
  edge [
    source 10
    target 1138
  ]
  edge [
    source 10
    target 1139
  ]
  edge [
    source 10
    target 1140
  ]
  edge [
    source 10
    target 1141
  ]
  edge [
    source 10
    target 1142
  ]
  edge [
    source 10
    target 1143
  ]
  edge [
    source 10
    target 1144
  ]
  edge [
    source 10
    target 1145
  ]
  edge [
    source 10
    target 10
  ]
  edge [
    source 10
    target 1264
  ]
  edge [
    source 11
    target 1146
  ]
  edge [
    source 11
    target 1147
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 1148
  ]
  edge [
    source 11
    target 1264
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 1149
  ]
  edge [
    source 12
    target 1150
  ]
  edge [
    source 12
    target 1264
  ]
  edge [
    source 13
    target 1151
  ]
  edge [
    source 13
    target 1152
  ]
  edge [
    source 13
    target 1153
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 1154
  ]
  edge [
    source 13
    target 1155
  ]
  edge [
    source 13
    target 1156
  ]
  edge [
    source 13
    target 1157
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 1264
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1158
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 1159
  ]
  edge [
    source 14
    target 989
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 42
  ]
  edge [
    source 14
    target 1160
  ]
  edge [
    source 14
    target 1161
  ]
  edge [
    source 14
    target 1162
  ]
  edge [
    source 14
    target 1163
  ]
  edge [
    source 14
    target 1145
  ]
  edge [
    source 14
    target 1164
  ]
  edge [
    source 14
    target 1165
  ]
  edge [
    source 14
    target 1166
  ]
  edge [
    source 14
    target 1167
  ]
  edge [
    source 14
    target 1168
  ]
  edge [
    source 14
    target 21
  ]
  edge [
    source 14
    target 1169
  ]
  edge [
    source 14
    target 1170
  ]
  edge [
    source 14
    target 1171
  ]
  edge [
    source 14
    target 1172
  ]
  edge [
    source 14
    target 1264
  ]
  edge [
    source 15
    target 1173
  ]
  edge [
    source 15
    target 1174
  ]
  edge [
    source 15
    target 1175
  ]
  edge [
    source 15
    target 1176
  ]
  edge [
    source 15
    target 1177
  ]
  edge [
    source 15
    target 1178
  ]
  edge [
    source 15
    target 1179
  ]
  edge [
    source 15
    target 1180
  ]
  edge [
    source 15
    target 285
  ]
  edge [
    source 15
    target 1181
  ]
  edge [
    source 15
    target 1182
  ]
  edge [
    source 15
    target 1183
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 1184
  ]
  edge [
    source 15
    target 1185
  ]
  edge [
    source 15
    target 1186
  ]
  edge [
    source 15
    target 355
  ]
  edge [
    source 15
    target 1187
  ]
  edge [
    source 15
    target 1188
  ]
  edge [
    source 15
    target 1189
  ]
  edge [
    source 15
    target 346
  ]
  edge [
    source 15
    target 270
  ]
  edge [
    source 15
    target 314
  ]
  edge [
    source 15
    target 1190
  ]
  edge [
    source 15
    target 1191
  ]
  edge [
    source 15
    target 1192
  ]
  edge [
    source 15
    target 1193
  ]
  edge [
    source 15
    target 1194
  ]
  edge [
    source 15
    target 1195
  ]
  edge [
    source 15
    target 1196
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 1197
  ]
  edge [
    source 15
    target 37
  ]
  edge [
    source 15
    target 1198
  ]
  edge [
    source 15
    target 1199
  ]
  edge [
    source 15
    target 1200
  ]
  edge [
    source 15
    target 1201
  ]
  edge [
    source 15
    target 1202
  ]
  edge [
    source 15
    target 1203
  ]
  edge [
    source 15
    target 1204
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 1145
  ]
  edge [
    source 15
    target 1205
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 1206
  ]
  edge [
    source 15
    target 1207
  ]
  edge [
    source 15
    target 1208
  ]
  edge [
    source 15
    target 70
  ]
  edge [
    source 15
    target 1209
  ]
  edge [
    source 15
    target 460
  ]
  edge [
    source 15
    target 1210
  ]
  edge [
    source 15
    target 1211
  ]
  edge [
    source 15
    target 1212
  ]
  edge [
    source 15
    target 1213
  ]
  edge [
    source 15
    target 1214
  ]
  edge [
    source 15
    target 1215
  ]
  edge [
    source 15
    target 1216
  ]
  edge [
    source 15
    target 1217
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 1218
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 1219
  ]
  edge [
    source 15
    target 1220
  ]
  edge [
    source 15
    target 1221
  ]
  edge [
    source 15
    target 1222
  ]
  edge [
    source 15
    target 1223
  ]
  edge [
    source 15
    target 1224
  ]
  edge [
    source 15
    target 1225
  ]
  edge [
    source 15
    target 1226
  ]
  edge [
    source 15
    target 1227
  ]
  edge [
    source 15
    target 1228
  ]
  edge [
    source 15
    target 1229
  ]
  edge [
    source 15
    target 1230
  ]
  edge [
    source 15
    target 1231
  ]
  edge [
    source 15
    target 1232
  ]
  edge [
    source 15
    target 1233
  ]
  edge [
    source 15
    target 51
  ]
  edge [
    source 15
    target 1234
  ]
  edge [
    source 15
    target 1235
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1236
  ]
  edge [
    source 15
    target 1237
  ]
  edge [
    source 15
    target 1238
  ]
  edge [
    source 15
    target 1239
  ]
  edge [
    source 15
    target 1240
  ]
  edge [
    source 15
    target 1241
  ]
  edge [
    source 15
    target 1242
  ]
  edge [
    source 15
    target 1243
  ]
  edge [
    source 15
    target 1244
  ]
  edge [
    source 15
    target 1245
  ]
  edge [
    source 15
    target 1246
  ]
  edge [
    source 15
    target 915
  ]
  edge [
    source 15
    target 1247
  ]
  edge [
    source 15
    target 1248
  ]
  edge [
    source 15
    target 1249
  ]
  edge [
    source 15
    target 1250
  ]
  edge [
    source 15
    target 1251
  ]
  edge [
    source 15
    target 1252
  ]
  edge [
    source 15
    target 1253
  ]
  edge [
    source 15
    target 1254
  ]
  edge [
    source 15
    target 259
  ]
  edge [
    source 15
    target 1255
  ]
  edge [
    source 15
    target 1256
  ]
  edge [
    source 15
    target 1257
  ]
  edge [
    source 15
    target 235
  ]
  edge [
    source 15
    target 1258
  ]
  edge [
    source 15
    target 1259
  ]
  edge [
    source 15
    target 1260
  ]
  edge [
    source 15
    target 1261
  ]
  edge [
    source 15
    target 1262
  ]
  edge [
    source 15
    target 267
  ]
  edge [
    source 15
    target 1263
  ]
  edge [
    source 1266
    target 1267
  ]
  edge [
    source 1268
    target 1269
  ]
  edge [
    source 1268
    target 1270
  ]
  edge [
    source 1269
    target 1270
  ]
]
