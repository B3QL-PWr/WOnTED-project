graph [
  node [
    id 0
    label "pracabaza"
    origin "text"
  ]
  node [
    id 1
    label "pracbaza"
    origin "text"
  ]
  node [
    id 2
    label "polak"
    origin "text"
  ]
  node [
    id 3
    label "heheszki"
    origin "text"
  ]
  node [
    id 4
    label "szef"
    origin "text"
  ]
  node [
    id 5
    label "dzwoni&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pracownica"
    origin "text"
  ]
  node [
    id 7
    label "godzina"
    origin "text"
  ]
  node [
    id 8
    label "praca"
    origin "text"
  ]
  node [
    id 9
    label "m&#261;&#380;"
    origin "text"
  ]
  node [
    id 10
    label "odbiera&#263;"
    origin "text"
  ]
  node [
    id 11
    label "telefon"
    origin "text"
  ]
  node [
    id 12
    label "sekunda"
    origin "text"
  ]
  node [
    id 13
    label "yyy"
    origin "text"
  ]
  node [
    id 14
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 15
    label "rozmawia&#263;"
    origin "text"
  ]
  node [
    id 16
    label "xyz"
    origin "text"
  ]
  node [
    id 17
    label "metr"
    origin "text"
  ]
  node [
    id 18
    label "wyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "rano"
    origin "text"
  ]
  node [
    id 20
    label "polski"
  ]
  node [
    id 21
    label "przedmiot"
  ]
  node [
    id 22
    label "Polish"
  ]
  node [
    id 23
    label "goniony"
  ]
  node [
    id 24
    label "oberek"
  ]
  node [
    id 25
    label "ryba_po_grecku"
  ]
  node [
    id 26
    label "sztajer"
  ]
  node [
    id 27
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 28
    label "krakowiak"
  ]
  node [
    id 29
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 30
    label "pierogi_ruskie"
  ]
  node [
    id 31
    label "lacki"
  ]
  node [
    id 32
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 33
    label "chodzony"
  ]
  node [
    id 34
    label "po_polsku"
  ]
  node [
    id 35
    label "mazur"
  ]
  node [
    id 36
    label "polsko"
  ]
  node [
    id 37
    label "skoczny"
  ]
  node [
    id 38
    label "drabant"
  ]
  node [
    id 39
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 40
    label "j&#281;zyk"
  ]
  node [
    id 41
    label "pryncypa&#322;"
  ]
  node [
    id 42
    label "kierownictwo"
  ]
  node [
    id 43
    label "kierowa&#263;"
  ]
  node [
    id 44
    label "cz&#322;owiek"
  ]
  node [
    id 45
    label "zwrot"
  ]
  node [
    id 46
    label "punkt"
  ]
  node [
    id 47
    label "turn"
  ]
  node [
    id 48
    label "turning"
  ]
  node [
    id 49
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 50
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 51
    label "skr&#281;t"
  ]
  node [
    id 52
    label "obr&#243;t"
  ]
  node [
    id 53
    label "fraza_czasownikowa"
  ]
  node [
    id 54
    label "jednostka_leksykalna"
  ]
  node [
    id 55
    label "zmiana"
  ]
  node [
    id 56
    label "wyra&#380;enie"
  ]
  node [
    id 57
    label "ludzko&#347;&#263;"
  ]
  node [
    id 58
    label "asymilowanie"
  ]
  node [
    id 59
    label "wapniak"
  ]
  node [
    id 60
    label "asymilowa&#263;"
  ]
  node [
    id 61
    label "os&#322;abia&#263;"
  ]
  node [
    id 62
    label "posta&#263;"
  ]
  node [
    id 63
    label "hominid"
  ]
  node [
    id 64
    label "podw&#322;adny"
  ]
  node [
    id 65
    label "os&#322;abianie"
  ]
  node [
    id 66
    label "g&#322;owa"
  ]
  node [
    id 67
    label "figura"
  ]
  node [
    id 68
    label "portrecista"
  ]
  node [
    id 69
    label "dwun&#243;g"
  ]
  node [
    id 70
    label "profanum"
  ]
  node [
    id 71
    label "mikrokosmos"
  ]
  node [
    id 72
    label "nasada"
  ]
  node [
    id 73
    label "duch"
  ]
  node [
    id 74
    label "antropochoria"
  ]
  node [
    id 75
    label "osoba"
  ]
  node [
    id 76
    label "wz&#243;r"
  ]
  node [
    id 77
    label "senior"
  ]
  node [
    id 78
    label "oddzia&#322;ywanie"
  ]
  node [
    id 79
    label "Adam"
  ]
  node [
    id 80
    label "homo_sapiens"
  ]
  node [
    id 81
    label "polifag"
  ]
  node [
    id 82
    label "biuro"
  ]
  node [
    id 83
    label "lead"
  ]
  node [
    id 84
    label "zesp&#243;&#322;"
  ]
  node [
    id 85
    label "siedziba"
  ]
  node [
    id 86
    label "w&#322;adza"
  ]
  node [
    id 87
    label "g&#322;os"
  ]
  node [
    id 88
    label "zwierzchnik"
  ]
  node [
    id 89
    label "sterowa&#263;"
  ]
  node [
    id 90
    label "wysy&#322;a&#263;"
  ]
  node [
    id 91
    label "manipulate"
  ]
  node [
    id 92
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 93
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 94
    label "ustawia&#263;"
  ]
  node [
    id 95
    label "give"
  ]
  node [
    id 96
    label "przeznacza&#263;"
  ]
  node [
    id 97
    label "control"
  ]
  node [
    id 98
    label "match"
  ]
  node [
    id 99
    label "motywowa&#263;"
  ]
  node [
    id 100
    label "administrowa&#263;"
  ]
  node [
    id 101
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 102
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 103
    label "order"
  ]
  node [
    id 104
    label "indicate"
  ]
  node [
    id 105
    label "call"
  ]
  node [
    id 106
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 107
    label "dzwonek"
  ]
  node [
    id 108
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 109
    label "sound"
  ]
  node [
    id 110
    label "bi&#263;"
  ]
  node [
    id 111
    label "brzmie&#263;"
  ]
  node [
    id 112
    label "drynda&#263;"
  ]
  node [
    id 113
    label "brz&#281;cze&#263;"
  ]
  node [
    id 114
    label "zaczyna&#263;"
  ]
  node [
    id 115
    label "nastawia&#263;"
  ]
  node [
    id 116
    label "get_in_touch"
  ]
  node [
    id 117
    label "odkr&#281;ca&#263;_kurek"
  ]
  node [
    id 118
    label "dokoptowywa&#263;"
  ]
  node [
    id 119
    label "ogl&#261;da&#263;"
  ]
  node [
    id 120
    label "uruchamia&#263;"
  ]
  node [
    id 121
    label "involve"
  ]
  node [
    id 122
    label "umieszcza&#263;"
  ]
  node [
    id 123
    label "connect"
  ]
  node [
    id 124
    label "echo"
  ]
  node [
    id 125
    label "wydawa&#263;"
  ]
  node [
    id 126
    label "wyra&#380;a&#263;"
  ]
  node [
    id 127
    label "s&#322;ycha&#263;"
  ]
  node [
    id 128
    label "rattle"
  ]
  node [
    id 129
    label "robi&#263;"
  ]
  node [
    id 130
    label "strike"
  ]
  node [
    id 131
    label "&#322;adowa&#263;"
  ]
  node [
    id 132
    label "usuwa&#263;"
  ]
  node [
    id 133
    label "butcher"
  ]
  node [
    id 134
    label "murder"
  ]
  node [
    id 135
    label "take"
  ]
  node [
    id 136
    label "napierdziela&#263;"
  ]
  node [
    id 137
    label "t&#322;oczy&#263;"
  ]
  node [
    id 138
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 139
    label "rejestrowa&#263;"
  ]
  node [
    id 140
    label "traktowa&#263;"
  ]
  node [
    id 141
    label "skuwa&#263;"
  ]
  node [
    id 142
    label "przygotowywa&#263;"
  ]
  node [
    id 143
    label "funkcjonowa&#263;"
  ]
  node [
    id 144
    label "macha&#263;"
  ]
  node [
    id 145
    label "zag&#322;&#281;bia&#263;"
  ]
  node [
    id 146
    label "proceed"
  ]
  node [
    id 147
    label "dawa&#263;"
  ]
  node [
    id 148
    label "peddle"
  ]
  node [
    id 149
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 150
    label "&#380;&#322;obi&#263;"
  ]
  node [
    id 151
    label "rap"
  ]
  node [
    id 152
    label "emanowa&#263;"
  ]
  node [
    id 153
    label "nalewa&#263;"
  ]
  node [
    id 154
    label "balansjerka"
  ]
  node [
    id 155
    label "wygrywa&#263;"
  ]
  node [
    id 156
    label "t&#322;uc"
  ]
  node [
    id 157
    label "uderza&#263;"
  ]
  node [
    id 158
    label "wpiernicza&#263;"
  ]
  node [
    id 159
    label "zwalcza&#263;"
  ]
  node [
    id 160
    label "pra&#263;"
  ]
  node [
    id 161
    label "str&#261;ca&#263;"
  ]
  node [
    id 162
    label "zabija&#263;"
  ]
  node [
    id 163
    label "beat"
  ]
  node [
    id 164
    label "niszczy&#263;"
  ]
  node [
    id 165
    label "przerabia&#263;"
  ]
  node [
    id 166
    label "powodowa&#263;"
  ]
  node [
    id 167
    label "tug"
  ]
  node [
    id 168
    label "chop"
  ]
  node [
    id 169
    label "krzywdzi&#263;"
  ]
  node [
    id 170
    label "telefonowa&#263;"
  ]
  node [
    id 171
    label "zadzwoni&#263;"
  ]
  node [
    id 172
    label "&#322;&#261;cznik_instalacyjny"
  ]
  node [
    id 173
    label "ro&#347;lina_zielna"
  ]
  node [
    id 174
    label "kolor"
  ]
  node [
    id 175
    label "dzwonkowate"
  ]
  node [
    id 176
    label "karo"
  ]
  node [
    id 177
    label "sygna&#322;_d&#378;wi&#281;kowy"
  ]
  node [
    id 178
    label "dzwonienie"
  ]
  node [
    id 179
    label "przycisk"
  ]
  node [
    id 180
    label "campanula"
  ]
  node [
    id 181
    label "sygnalizator"
  ]
  node [
    id 182
    label "karta"
  ]
  node [
    id 183
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 184
    label "provider"
  ]
  node [
    id 185
    label "infrastruktura"
  ]
  node [
    id 186
    label "numer"
  ]
  node [
    id 187
    label "po&#322;&#261;czenie"
  ]
  node [
    id 188
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 189
    label "phreaker"
  ]
  node [
    id 190
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 191
    label "mikrotelefon"
  ]
  node [
    id 192
    label "billing"
  ]
  node [
    id 193
    label "instalacja"
  ]
  node [
    id 194
    label "kontakt"
  ]
  node [
    id 195
    label "coalescence"
  ]
  node [
    id 196
    label "wy&#347;wietlacz"
  ]
  node [
    id 197
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 198
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 199
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 200
    label "urz&#261;dzenie"
  ]
  node [
    id 201
    label "time"
  ]
  node [
    id 202
    label "doba"
  ]
  node [
    id 203
    label "p&#243;&#322;godzina"
  ]
  node [
    id 204
    label "jednostka_czasu"
  ]
  node [
    id 205
    label "czas"
  ]
  node [
    id 206
    label "minuta"
  ]
  node [
    id 207
    label "kwadrans"
  ]
  node [
    id 208
    label "poprzedzanie"
  ]
  node [
    id 209
    label "czasoprzestrze&#324;"
  ]
  node [
    id 210
    label "laba"
  ]
  node [
    id 211
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 212
    label "chronometria"
  ]
  node [
    id 213
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 214
    label "rachuba_czasu"
  ]
  node [
    id 215
    label "przep&#322;ywanie"
  ]
  node [
    id 216
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 217
    label "czasokres"
  ]
  node [
    id 218
    label "odczyt"
  ]
  node [
    id 219
    label "chwila"
  ]
  node [
    id 220
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 221
    label "dzieje"
  ]
  node [
    id 222
    label "kategoria_gramatyczna"
  ]
  node [
    id 223
    label "poprzedzenie"
  ]
  node [
    id 224
    label "trawienie"
  ]
  node [
    id 225
    label "pochodzi&#263;"
  ]
  node [
    id 226
    label "period"
  ]
  node [
    id 227
    label "okres_czasu"
  ]
  node [
    id 228
    label "poprzedza&#263;"
  ]
  node [
    id 229
    label "schy&#322;ek"
  ]
  node [
    id 230
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 231
    label "odwlekanie_si&#281;"
  ]
  node [
    id 232
    label "zegar"
  ]
  node [
    id 233
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 234
    label "czwarty_wymiar"
  ]
  node [
    id 235
    label "pochodzenie"
  ]
  node [
    id 236
    label "koniugacja"
  ]
  node [
    id 237
    label "Zeitgeist"
  ]
  node [
    id 238
    label "trawi&#263;"
  ]
  node [
    id 239
    label "pogoda"
  ]
  node [
    id 240
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 241
    label "poprzedzi&#263;"
  ]
  node [
    id 242
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 243
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 244
    label "time_period"
  ]
  node [
    id 245
    label "zapis"
  ]
  node [
    id 246
    label "jednostka"
  ]
  node [
    id 247
    label "stopie&#324;"
  ]
  node [
    id 248
    label "design"
  ]
  node [
    id 249
    label "tydzie&#324;"
  ]
  node [
    id 250
    label "noc"
  ]
  node [
    id 251
    label "dzie&#324;"
  ]
  node [
    id 252
    label "long_time"
  ]
  node [
    id 253
    label "jednostka_geologiczna"
  ]
  node [
    id 254
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 255
    label "najem"
  ]
  node [
    id 256
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 257
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 258
    label "zak&#322;ad"
  ]
  node [
    id 259
    label "stosunek_pracy"
  ]
  node [
    id 260
    label "benedykty&#324;ski"
  ]
  node [
    id 261
    label "poda&#380;_pracy"
  ]
  node [
    id 262
    label "pracowanie"
  ]
  node [
    id 263
    label "tyrka"
  ]
  node [
    id 264
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 265
    label "wytw&#243;r"
  ]
  node [
    id 266
    label "miejsce"
  ]
  node [
    id 267
    label "zaw&#243;d"
  ]
  node [
    id 268
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 269
    label "tynkarski"
  ]
  node [
    id 270
    label "pracowa&#263;"
  ]
  node [
    id 271
    label "czynno&#347;&#263;"
  ]
  node [
    id 272
    label "czynnik_produkcji"
  ]
  node [
    id 273
    label "zobowi&#261;zanie"
  ]
  node [
    id 274
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 275
    label "p&#322;&#243;d"
  ]
  node [
    id 276
    label "work"
  ]
  node [
    id 277
    label "rezultat"
  ]
  node [
    id 278
    label "activity"
  ]
  node [
    id 279
    label "bezproblemowy"
  ]
  node [
    id 280
    label "wydarzenie"
  ]
  node [
    id 281
    label "warunek_lokalowy"
  ]
  node [
    id 282
    label "plac"
  ]
  node [
    id 283
    label "location"
  ]
  node [
    id 284
    label "uwaga"
  ]
  node [
    id 285
    label "przestrze&#324;"
  ]
  node [
    id 286
    label "status"
  ]
  node [
    id 287
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 288
    label "cia&#322;o"
  ]
  node [
    id 289
    label "cecha"
  ]
  node [
    id 290
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 291
    label "rz&#261;d"
  ]
  node [
    id 292
    label "stosunek_prawny"
  ]
  node [
    id 293
    label "oblig"
  ]
  node [
    id 294
    label "uregulowa&#263;"
  ]
  node [
    id 295
    label "oddzia&#322;anie"
  ]
  node [
    id 296
    label "occupation"
  ]
  node [
    id 297
    label "duty"
  ]
  node [
    id 298
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 299
    label "zapowied&#378;"
  ]
  node [
    id 300
    label "obowi&#261;zek"
  ]
  node [
    id 301
    label "statement"
  ]
  node [
    id 302
    label "zapewnienie"
  ]
  node [
    id 303
    label "miejsce_pracy"
  ]
  node [
    id 304
    label "zak&#322;adka"
  ]
  node [
    id 305
    label "jednostka_organizacyjna"
  ]
  node [
    id 306
    label "instytucja"
  ]
  node [
    id 307
    label "wyko&#324;czenie"
  ]
  node [
    id 308
    label "firma"
  ]
  node [
    id 309
    label "czyn"
  ]
  node [
    id 310
    label "company"
  ]
  node [
    id 311
    label "instytut"
  ]
  node [
    id 312
    label "umowa"
  ]
  node [
    id 313
    label "&#321;ubianka"
  ]
  node [
    id 314
    label "dzia&#322;_personalny"
  ]
  node [
    id 315
    label "Kreml"
  ]
  node [
    id 316
    label "Bia&#322;y_Dom"
  ]
  node [
    id 317
    label "budynek"
  ]
  node [
    id 318
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 319
    label "sadowisko"
  ]
  node [
    id 320
    label "rewizja"
  ]
  node [
    id 321
    label "passage"
  ]
  node [
    id 322
    label "oznaka"
  ]
  node [
    id 323
    label "change"
  ]
  node [
    id 324
    label "ferment"
  ]
  node [
    id 325
    label "komplet"
  ]
  node [
    id 326
    label "anatomopatolog"
  ]
  node [
    id 327
    label "zmianka"
  ]
  node [
    id 328
    label "zjawisko"
  ]
  node [
    id 329
    label "amendment"
  ]
  node [
    id 330
    label "odmienianie"
  ]
  node [
    id 331
    label "tura"
  ]
  node [
    id 332
    label "cierpliwy"
  ]
  node [
    id 333
    label "mozolny"
  ]
  node [
    id 334
    label "wytrwa&#322;y"
  ]
  node [
    id 335
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 336
    label "benedykty&#324;sko"
  ]
  node [
    id 337
    label "typowy"
  ]
  node [
    id 338
    label "po_benedykty&#324;sku"
  ]
  node [
    id 339
    label "endeavor"
  ]
  node [
    id 340
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 341
    label "mie&#263;_miejsce"
  ]
  node [
    id 342
    label "podejmowa&#263;"
  ]
  node [
    id 343
    label "dziama&#263;"
  ]
  node [
    id 344
    label "do"
  ]
  node [
    id 345
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 346
    label "bangla&#263;"
  ]
  node [
    id 347
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 348
    label "maszyna"
  ]
  node [
    id 349
    label "dzia&#322;a&#263;"
  ]
  node [
    id 350
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 351
    label "tryb"
  ]
  node [
    id 352
    label "zawodoznawstwo"
  ]
  node [
    id 353
    label "emocja"
  ]
  node [
    id 354
    label "office"
  ]
  node [
    id 355
    label "kwalifikacje"
  ]
  node [
    id 356
    label "craft"
  ]
  node [
    id 357
    label "przepracowanie_si&#281;"
  ]
  node [
    id 358
    label "zarz&#261;dzanie"
  ]
  node [
    id 359
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 360
    label "podlizanie_si&#281;"
  ]
  node [
    id 361
    label "dopracowanie"
  ]
  node [
    id 362
    label "podlizywanie_si&#281;"
  ]
  node [
    id 363
    label "uruchamianie"
  ]
  node [
    id 364
    label "dzia&#322;anie"
  ]
  node [
    id 365
    label "d&#261;&#380;enie"
  ]
  node [
    id 366
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 367
    label "uruchomienie"
  ]
  node [
    id 368
    label "nakr&#281;canie"
  ]
  node [
    id 369
    label "funkcjonowanie"
  ]
  node [
    id 370
    label "tr&#243;jstronny"
  ]
  node [
    id 371
    label "postaranie_si&#281;"
  ]
  node [
    id 372
    label "odpocz&#281;cie"
  ]
  node [
    id 373
    label "nakr&#281;cenie"
  ]
  node [
    id 374
    label "zatrzymanie"
  ]
  node [
    id 375
    label "spracowanie_si&#281;"
  ]
  node [
    id 376
    label "skakanie"
  ]
  node [
    id 377
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 378
    label "podtrzymywanie"
  ]
  node [
    id 379
    label "w&#322;&#261;czanie"
  ]
  node [
    id 380
    label "zaprz&#281;ganie"
  ]
  node [
    id 381
    label "podejmowanie"
  ]
  node [
    id 382
    label "wyrabianie"
  ]
  node [
    id 383
    label "dzianie_si&#281;"
  ]
  node [
    id 384
    label "use"
  ]
  node [
    id 385
    label "przepracowanie"
  ]
  node [
    id 386
    label "poruszanie_si&#281;"
  ]
  node [
    id 387
    label "funkcja"
  ]
  node [
    id 388
    label "impact"
  ]
  node [
    id 389
    label "przepracowywanie"
  ]
  node [
    id 390
    label "awansowanie"
  ]
  node [
    id 391
    label "courtship"
  ]
  node [
    id 392
    label "zapracowanie"
  ]
  node [
    id 393
    label "wyrobienie"
  ]
  node [
    id 394
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 395
    label "w&#322;&#261;czenie"
  ]
  node [
    id 396
    label "transakcja"
  ]
  node [
    id 397
    label "ma&#322;&#380;onek"
  ]
  node [
    id 398
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 399
    label "m&#243;j"
  ]
  node [
    id 400
    label "ch&#322;op"
  ]
  node [
    id 401
    label "pan_m&#322;ody"
  ]
  node [
    id 402
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 403
    label "&#347;lubny"
  ]
  node [
    id 404
    label "pan_domu"
  ]
  node [
    id 405
    label "pan_i_w&#322;adca"
  ]
  node [
    id 406
    label "stary"
  ]
  node [
    id 407
    label "doros&#322;y"
  ]
  node [
    id 408
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 409
    label "ojciec"
  ]
  node [
    id 410
    label "jegomo&#347;&#263;"
  ]
  node [
    id 411
    label "andropauza"
  ]
  node [
    id 412
    label "pa&#324;stwo"
  ]
  node [
    id 413
    label "bratek"
  ]
  node [
    id 414
    label "ch&#322;opina"
  ]
  node [
    id 415
    label "samiec"
  ]
  node [
    id 416
    label "twardziel"
  ]
  node [
    id 417
    label "androlog"
  ]
  node [
    id 418
    label "partner"
  ]
  node [
    id 419
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 420
    label "stan_cywilny"
  ]
  node [
    id 421
    label "para"
  ]
  node [
    id 422
    label "matrymonialny"
  ]
  node [
    id 423
    label "lewirat"
  ]
  node [
    id 424
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 425
    label "sakrament"
  ]
  node [
    id 426
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 427
    label "zwi&#261;zek"
  ]
  node [
    id 428
    label "partia"
  ]
  node [
    id 429
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 430
    label "nienowoczesny"
  ]
  node [
    id 431
    label "gruba_ryba"
  ]
  node [
    id 432
    label "zestarzenie_si&#281;"
  ]
  node [
    id 433
    label "poprzedni"
  ]
  node [
    id 434
    label "dawno"
  ]
  node [
    id 435
    label "staro"
  ]
  node [
    id 436
    label "starzy"
  ]
  node [
    id 437
    label "dotychczasowy"
  ]
  node [
    id 438
    label "p&#243;&#378;ny"
  ]
  node [
    id 439
    label "d&#322;ugoletni"
  ]
  node [
    id 440
    label "charakterystyczny"
  ]
  node [
    id 441
    label "brat"
  ]
  node [
    id 442
    label "po_staro&#347;wiecku"
  ]
  node [
    id 443
    label "znajomy"
  ]
  node [
    id 444
    label "odleg&#322;y"
  ]
  node [
    id 445
    label "starzenie_si&#281;"
  ]
  node [
    id 446
    label "starczo"
  ]
  node [
    id 447
    label "dawniej"
  ]
  node [
    id 448
    label "niegdysiejszy"
  ]
  node [
    id 449
    label "dojrza&#322;y"
  ]
  node [
    id 450
    label "szlubny"
  ]
  node [
    id 451
    label "&#347;lubnie"
  ]
  node [
    id 452
    label "legalny"
  ]
  node [
    id 453
    label "czyj&#347;"
  ]
  node [
    id 454
    label "uw&#322;aszczy&#263;"
  ]
  node [
    id 455
    label "uw&#322;aszcza&#263;"
  ]
  node [
    id 456
    label "rolnik"
  ]
  node [
    id 457
    label "ch&#322;opstwo"
  ]
  node [
    id 458
    label "cham"
  ]
  node [
    id 459
    label "bamber"
  ]
  node [
    id 460
    label "uw&#322;aszczanie"
  ]
  node [
    id 461
    label "prawo_wychodu"
  ]
  node [
    id 462
    label "w&#322;o&#347;cianin"
  ]
  node [
    id 463
    label "przedstawiciel"
  ]
  node [
    id 464
    label "facet"
  ]
  node [
    id 465
    label "zabiera&#263;"
  ]
  node [
    id 466
    label "zlecenie"
  ]
  node [
    id 467
    label "odzyskiwa&#263;"
  ]
  node [
    id 468
    label "radio"
  ]
  node [
    id 469
    label "przyjmowa&#263;"
  ]
  node [
    id 470
    label "bra&#263;"
  ]
  node [
    id 471
    label "antena"
  ]
  node [
    id 472
    label "fall"
  ]
  node [
    id 473
    label "liszy&#263;"
  ]
  node [
    id 474
    label "pozbawia&#263;"
  ]
  node [
    id 475
    label "telewizor"
  ]
  node [
    id 476
    label "konfiskowa&#263;"
  ]
  node [
    id 477
    label "deprive"
  ]
  node [
    id 478
    label "accept"
  ]
  node [
    id 479
    label "doznawa&#263;"
  ]
  node [
    id 480
    label "dostarcza&#263;"
  ]
  node [
    id 481
    label "close"
  ]
  node [
    id 482
    label "wpuszcza&#263;"
  ]
  node [
    id 483
    label "wyprawia&#263;"
  ]
  node [
    id 484
    label "przyjmowanie"
  ]
  node [
    id 485
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 486
    label "poch&#322;ania&#263;"
  ]
  node [
    id 487
    label "swallow"
  ]
  node [
    id 488
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 489
    label "uznawa&#263;"
  ]
  node [
    id 490
    label "dopuszcza&#263;"
  ]
  node [
    id 491
    label "zgadza&#263;_si&#281;"
  ]
  node [
    id 492
    label "obiera&#263;"
  ]
  node [
    id 493
    label "admit"
  ]
  node [
    id 494
    label "undertake"
  ]
  node [
    id 495
    label "hurt"
  ]
  node [
    id 496
    label "recur"
  ]
  node [
    id 497
    label "przychodzi&#263;"
  ]
  node [
    id 498
    label "sum_up"
  ]
  node [
    id 499
    label "zajmowa&#263;"
  ]
  node [
    id 500
    label "poci&#261;ga&#263;"
  ]
  node [
    id 501
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 502
    label "&#322;apa&#263;"
  ]
  node [
    id 503
    label "przesuwa&#263;"
  ]
  node [
    id 504
    label "prowadzi&#263;"
  ]
  node [
    id 505
    label "blurt_out"
  ]
  node [
    id 506
    label "abstract"
  ]
  node [
    id 507
    label "przenosi&#263;"
  ]
  node [
    id 508
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 509
    label "porywa&#263;"
  ]
  node [
    id 510
    label "korzysta&#263;"
  ]
  node [
    id 511
    label "wchodzi&#263;"
  ]
  node [
    id 512
    label "poczytywa&#263;"
  ]
  node [
    id 513
    label "levy"
  ]
  node [
    id 514
    label "wk&#322;ada&#263;"
  ]
  node [
    id 515
    label "raise"
  ]
  node [
    id 516
    label "pokonywa&#263;"
  ]
  node [
    id 517
    label "by&#263;"
  ]
  node [
    id 518
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 519
    label "rucha&#263;"
  ]
  node [
    id 520
    label "za&#380;ywa&#263;"
  ]
  node [
    id 521
    label "get"
  ]
  node [
    id 522
    label "otrzymywa&#263;"
  ]
  node [
    id 523
    label "&#263;pa&#263;"
  ]
  node [
    id 524
    label "interpretowa&#263;"
  ]
  node [
    id 525
    label "dostawa&#263;"
  ]
  node [
    id 526
    label "rusza&#263;"
  ]
  node [
    id 527
    label "chwyta&#263;"
  ]
  node [
    id 528
    label "grza&#263;"
  ]
  node [
    id 529
    label "wch&#322;ania&#263;"
  ]
  node [
    id 530
    label "u&#380;ywa&#263;"
  ]
  node [
    id 531
    label "ucieka&#263;"
  ]
  node [
    id 532
    label "arise"
  ]
  node [
    id 533
    label "uprawia&#263;_seks"
  ]
  node [
    id 534
    label "towarzystwo"
  ]
  node [
    id 535
    label "atakowa&#263;"
  ]
  node [
    id 536
    label "branie"
  ]
  node [
    id 537
    label "zalicza&#263;"
  ]
  node [
    id 538
    label "open"
  ]
  node [
    id 539
    label "wzi&#261;&#263;"
  ]
  node [
    id 540
    label "przewa&#380;a&#263;"
  ]
  node [
    id 541
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 542
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 543
    label "spadni&#281;cie"
  ]
  node [
    id 544
    label "undertaking"
  ]
  node [
    id 545
    label "polecenie"
  ]
  node [
    id 546
    label "odebra&#263;"
  ]
  node [
    id 547
    label "odebranie"
  ]
  node [
    id 548
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 549
    label "zbiegni&#281;cie"
  ]
  node [
    id 550
    label "odbieranie"
  ]
  node [
    id 551
    label "decree"
  ]
  node [
    id 552
    label "szyk_antenowy"
  ]
  node [
    id 553
    label "odbiornik"
  ]
  node [
    id 554
    label "nadajnik"
  ]
  node [
    id 555
    label "stawon&#243;g"
  ]
  node [
    id 556
    label "czu&#322;ek"
  ]
  node [
    id 557
    label "ekran"
  ]
  node [
    id 558
    label "paj&#281;czarz"
  ]
  node [
    id 559
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 560
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 561
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 562
    label "radiola"
  ]
  node [
    id 563
    label "programowiec"
  ]
  node [
    id 564
    label "redakcja"
  ]
  node [
    id 565
    label "spot"
  ]
  node [
    id 566
    label "stacja"
  ]
  node [
    id 567
    label "uk&#322;ad"
  ]
  node [
    id 568
    label "eliminator"
  ]
  node [
    id 569
    label "radiolinia"
  ]
  node [
    id 570
    label "media"
  ]
  node [
    id 571
    label "fala_radiowa"
  ]
  node [
    id 572
    label "radiofonia"
  ]
  node [
    id 573
    label "studio"
  ]
  node [
    id 574
    label "dyskryminator"
  ]
  node [
    id 575
    label "milcze&#263;"
  ]
  node [
    id 576
    label "zostawia&#263;"
  ]
  node [
    id 577
    label "pozostawi&#263;"
  ]
  node [
    id 578
    label "liczba"
  ]
  node [
    id 579
    label "&#380;art"
  ]
  node [
    id 580
    label "zi&#243;&#322;ko"
  ]
  node [
    id 581
    label "publikacja"
  ]
  node [
    id 582
    label "manewr"
  ]
  node [
    id 583
    label "impression"
  ]
  node [
    id 584
    label "wyst&#281;p"
  ]
  node [
    id 585
    label "sztos"
  ]
  node [
    id 586
    label "oznaczenie"
  ]
  node [
    id 587
    label "hotel"
  ]
  node [
    id 588
    label "pok&#243;j"
  ]
  node [
    id 589
    label "czasopismo"
  ]
  node [
    id 590
    label "akt_p&#322;ciowy"
  ]
  node [
    id 591
    label "orygina&#322;"
  ]
  node [
    id 592
    label "kom&#243;rka"
  ]
  node [
    id 593
    label "furnishing"
  ]
  node [
    id 594
    label "zabezpieczenie"
  ]
  node [
    id 595
    label "zrobienie"
  ]
  node [
    id 596
    label "wyrz&#261;dzenie"
  ]
  node [
    id 597
    label "zagospodarowanie"
  ]
  node [
    id 598
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 599
    label "ig&#322;a"
  ]
  node [
    id 600
    label "narz&#281;dzie"
  ]
  node [
    id 601
    label "wirnik"
  ]
  node [
    id 602
    label "aparatura"
  ]
  node [
    id 603
    label "system_energetyczny"
  ]
  node [
    id 604
    label "impulsator"
  ]
  node [
    id 605
    label "mechanizm"
  ]
  node [
    id 606
    label "sprz&#281;t"
  ]
  node [
    id 607
    label "blokowanie"
  ]
  node [
    id 608
    label "set"
  ]
  node [
    id 609
    label "zablokowanie"
  ]
  node [
    id 610
    label "przygotowanie"
  ]
  node [
    id 611
    label "komora"
  ]
  node [
    id 612
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 613
    label "communication"
  ]
  node [
    id 614
    label "styk"
  ]
  node [
    id 615
    label "&#322;&#261;cznik_mechanizmowy"
  ]
  node [
    id 616
    label "association"
  ]
  node [
    id 617
    label "&#322;&#261;cznik"
  ]
  node [
    id 618
    label "katalizator"
  ]
  node [
    id 619
    label "socket"
  ]
  node [
    id 620
    label "instalacja_elektryczna"
  ]
  node [
    id 621
    label "soczewka"
  ]
  node [
    id 622
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 623
    label "formacja_geologiczna"
  ]
  node [
    id 624
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 625
    label "linkage"
  ]
  node [
    id 626
    label "regulator"
  ]
  node [
    id 627
    label "z&#322;&#261;czenie"
  ]
  node [
    id 628
    label "contact"
  ]
  node [
    id 629
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 630
    label "proces"
  ]
  node [
    id 631
    label "kompozycja"
  ]
  node [
    id 632
    label "uzbrajanie"
  ]
  node [
    id 633
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 634
    label "handset"
  ]
  node [
    id 635
    label "dodzwanianie_si&#281;"
  ]
  node [
    id 636
    label "jingle"
  ]
  node [
    id 637
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 638
    label "wydzwanianie"
  ]
  node [
    id 639
    label "naciskanie"
  ]
  node [
    id 640
    label "brzmienie"
  ]
  node [
    id 641
    label "wybijanie"
  ]
  node [
    id 642
    label "dryndanie"
  ]
  node [
    id 643
    label "dodzwonienie_si&#281;"
  ]
  node [
    id 644
    label "wydzwonienie"
  ]
  node [
    id 645
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 646
    label "zabrz&#281;cze&#263;"
  ]
  node [
    id 647
    label "zabi&#263;"
  ]
  node [
    id 648
    label "zadrynda&#263;"
  ]
  node [
    id 649
    label "zabrzmie&#263;"
  ]
  node [
    id 650
    label "nacisn&#261;&#263;"
  ]
  node [
    id 651
    label "zjednoczy&#263;"
  ]
  node [
    id 652
    label "stworzy&#263;"
  ]
  node [
    id 653
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 654
    label "incorporate"
  ]
  node [
    id 655
    label "zrobi&#263;"
  ]
  node [
    id 656
    label "spowodowa&#263;"
  ]
  node [
    id 657
    label "relate"
  ]
  node [
    id 658
    label "z&#322;odziej"
  ]
  node [
    id 659
    label "severance"
  ]
  node [
    id 660
    label "przerwanie"
  ]
  node [
    id 661
    label "od&#322;&#261;czenie"
  ]
  node [
    id 662
    label "oddzielenie"
  ]
  node [
    id 663
    label "prze&#322;&#261;czenie"
  ]
  node [
    id 664
    label "rozdzielenie"
  ]
  node [
    id 665
    label "dissociation"
  ]
  node [
    id 666
    label "spis"
  ]
  node [
    id 667
    label "biling"
  ]
  node [
    id 668
    label "rozdzieli&#263;"
  ]
  node [
    id 669
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 670
    label "detach"
  ]
  node [
    id 671
    label "oddzieli&#263;"
  ]
  node [
    id 672
    label "amputate"
  ]
  node [
    id 673
    label "przerwa&#263;"
  ]
  node [
    id 674
    label "rozdzielanie"
  ]
  node [
    id 675
    label "separation"
  ]
  node [
    id 676
    label "oddzielanie"
  ]
  node [
    id 677
    label "rozsuwanie"
  ]
  node [
    id 678
    label "od&#322;&#261;czanie"
  ]
  node [
    id 679
    label "przerywanie"
  ]
  node [
    id 680
    label "prze&#322;&#261;czanie"
  ]
  node [
    id 681
    label "stworzenie"
  ]
  node [
    id 682
    label "zespolenie"
  ]
  node [
    id 683
    label "dressing"
  ]
  node [
    id 684
    label "pomy&#347;lenie"
  ]
  node [
    id 685
    label "zjednoczenie"
  ]
  node [
    id 686
    label "spowodowanie"
  ]
  node [
    id 687
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 688
    label "element"
  ]
  node [
    id 689
    label "alliance"
  ]
  node [
    id 690
    label "joining"
  ]
  node [
    id 691
    label "umo&#380;liwienie"
  ]
  node [
    id 692
    label "mention"
  ]
  node [
    id 693
    label "zwi&#261;zany"
  ]
  node [
    id 694
    label "port"
  ]
  node [
    id 695
    label "komunikacja"
  ]
  node [
    id 696
    label "rzucenie"
  ]
  node [
    id 697
    label "zgrzeina"
  ]
  node [
    id 698
    label "zestawienie"
  ]
  node [
    id 699
    label "cover"
  ]
  node [
    id 700
    label "gulf"
  ]
  node [
    id 701
    label "part"
  ]
  node [
    id 702
    label "rozdziela&#263;"
  ]
  node [
    id 703
    label "przerywa&#263;"
  ]
  node [
    id 704
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 705
    label "oddziela&#263;"
  ]
  node [
    id 706
    label "internet"
  ]
  node [
    id 707
    label "dostawca"
  ]
  node [
    id 708
    label "telefonia"
  ]
  node [
    id 709
    label "zaplecze"
  ]
  node [
    id 710
    label "trasa"
  ]
  node [
    id 711
    label "mikrosekunda"
  ]
  node [
    id 712
    label "milisekunda"
  ]
  node [
    id 713
    label "tercja"
  ]
  node [
    id 714
    label "nanosekunda"
  ]
  node [
    id 715
    label "uk&#322;ad_SI"
  ]
  node [
    id 716
    label "przyswoi&#263;"
  ]
  node [
    id 717
    label "one"
  ]
  node [
    id 718
    label "poj&#281;cie"
  ]
  node [
    id 719
    label "ewoluowanie"
  ]
  node [
    id 720
    label "supremum"
  ]
  node [
    id 721
    label "skala"
  ]
  node [
    id 722
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 723
    label "przyswajanie"
  ]
  node [
    id 724
    label "wyewoluowanie"
  ]
  node [
    id 725
    label "reakcja"
  ]
  node [
    id 726
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 727
    label "przeliczy&#263;"
  ]
  node [
    id 728
    label "wyewoluowa&#263;"
  ]
  node [
    id 729
    label "ewoluowa&#263;"
  ]
  node [
    id 730
    label "matematyka"
  ]
  node [
    id 731
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 732
    label "rzut"
  ]
  node [
    id 733
    label "liczba_naturalna"
  ]
  node [
    id 734
    label "czynnik_biotyczny"
  ]
  node [
    id 735
    label "individual"
  ]
  node [
    id 736
    label "obiekt"
  ]
  node [
    id 737
    label "przyswaja&#263;"
  ]
  node [
    id 738
    label "przyswojenie"
  ]
  node [
    id 739
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 740
    label "przeliczanie"
  ]
  node [
    id 741
    label "przelicza&#263;"
  ]
  node [
    id 742
    label "infimum"
  ]
  node [
    id 743
    label "przeliczenie"
  ]
  node [
    id 744
    label "stopie&#324;_pisma"
  ]
  node [
    id 745
    label "pole"
  ]
  node [
    id 746
    label "godzina_kanoniczna"
  ]
  node [
    id 747
    label "hokej"
  ]
  node [
    id 748
    label "hokej_na_lodzie"
  ]
  node [
    id 749
    label "zamek"
  ]
  node [
    id 750
    label "interwa&#322;"
  ]
  node [
    id 751
    label "mecz"
  ]
  node [
    id 752
    label "gotowy"
  ]
  node [
    id 753
    label "might"
  ]
  node [
    id 754
    label "uprawi&#263;"
  ]
  node [
    id 755
    label "public_treasury"
  ]
  node [
    id 756
    label "obrobi&#263;"
  ]
  node [
    id 757
    label "nietrze&#378;wy"
  ]
  node [
    id 758
    label "czekanie"
  ]
  node [
    id 759
    label "martwy"
  ]
  node [
    id 760
    label "bliski"
  ]
  node [
    id 761
    label "gotowo"
  ]
  node [
    id 762
    label "przygotowywanie"
  ]
  node [
    id 763
    label "dyspozycyjny"
  ]
  node [
    id 764
    label "zalany"
  ]
  node [
    id 765
    label "nieuchronny"
  ]
  node [
    id 766
    label "doj&#347;cie"
  ]
  node [
    id 767
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 768
    label "equal"
  ]
  node [
    id 769
    label "trwa&#263;"
  ]
  node [
    id 770
    label "chodzi&#263;"
  ]
  node [
    id 771
    label "si&#281;ga&#263;"
  ]
  node [
    id 772
    label "stan"
  ]
  node [
    id 773
    label "obecno&#347;&#263;"
  ]
  node [
    id 774
    label "stand"
  ]
  node [
    id 775
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 776
    label "uczestniczy&#263;"
  ]
  node [
    id 777
    label "talk"
  ]
  node [
    id 778
    label "gaworzy&#263;"
  ]
  node [
    id 779
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 780
    label "chatter"
  ]
  node [
    id 781
    label "m&#243;wi&#263;"
  ]
  node [
    id 782
    label "niemowl&#281;"
  ]
  node [
    id 783
    label "kosmetyk"
  ]
  node [
    id 784
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 785
    label "nauczyciel"
  ]
  node [
    id 786
    label "kilometr_kwadratowy"
  ]
  node [
    id 787
    label "centymetr_kwadratowy"
  ]
  node [
    id 788
    label "dekametr"
  ]
  node [
    id 789
    label "gigametr"
  ]
  node [
    id 790
    label "plon"
  ]
  node [
    id 791
    label "meter"
  ]
  node [
    id 792
    label "miara"
  ]
  node [
    id 793
    label "wiersz"
  ]
  node [
    id 794
    label "jednostka_metryczna"
  ]
  node [
    id 795
    label "metrum"
  ]
  node [
    id 796
    label "decymetr"
  ]
  node [
    id 797
    label "megabyte"
  ]
  node [
    id 798
    label "literaturoznawstwo"
  ]
  node [
    id 799
    label "jednostka_powierzchni"
  ]
  node [
    id 800
    label "jednostka_masy"
  ]
  node [
    id 801
    label "proportion"
  ]
  node [
    id 802
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 803
    label "wielko&#347;&#263;"
  ]
  node [
    id 804
    label "continence"
  ]
  node [
    id 805
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 806
    label "odwiedziny"
  ]
  node [
    id 807
    label "granica"
  ]
  node [
    id 808
    label "zakres"
  ]
  node [
    id 809
    label "ilo&#347;&#263;"
  ]
  node [
    id 810
    label "dymensja"
  ]
  node [
    id 811
    label "belfer"
  ]
  node [
    id 812
    label "kszta&#322;ciciel"
  ]
  node [
    id 813
    label "preceptor"
  ]
  node [
    id 814
    label "pedagog"
  ]
  node [
    id 815
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 816
    label "szkolnik"
  ]
  node [
    id 817
    label "profesor"
  ]
  node [
    id 818
    label "popularyzator"
  ]
  node [
    id 819
    label "struktura"
  ]
  node [
    id 820
    label "standard"
  ]
  node [
    id 821
    label "rytm"
  ]
  node [
    id 822
    label "rytmika"
  ]
  node [
    id 823
    label "centymetr"
  ]
  node [
    id 824
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 825
    label "hektometr"
  ]
  node [
    id 826
    label "return"
  ]
  node [
    id 827
    label "wyda&#263;"
  ]
  node [
    id 828
    label "produkcja"
  ]
  node [
    id 829
    label "naturalia"
  ]
  node [
    id 830
    label "strofoida"
  ]
  node [
    id 831
    label "figura_stylistyczna"
  ]
  node [
    id 832
    label "wypowied&#378;"
  ]
  node [
    id 833
    label "podmiot_liryczny"
  ]
  node [
    id 834
    label "cezura"
  ]
  node [
    id 835
    label "zwrotka"
  ]
  node [
    id 836
    label "fragment"
  ]
  node [
    id 837
    label "refren"
  ]
  node [
    id 838
    label "tekst"
  ]
  node [
    id 839
    label "dzie&#322;o_poetyckie"
  ]
  node [
    id 840
    label "nauka_humanistyczna"
  ]
  node [
    id 841
    label "teoria_literatury"
  ]
  node [
    id 842
    label "historia_literatury"
  ]
  node [
    id 843
    label "s&#322;owo_kluczowe"
  ]
  node [
    id 844
    label "komparatystyka"
  ]
  node [
    id 845
    label "literature"
  ]
  node [
    id 846
    label "stylistyka"
  ]
  node [
    id 847
    label "krytyka_literacka"
  ]
  node [
    id 848
    label "ograniczenie"
  ]
  node [
    id 849
    label "drop"
  ]
  node [
    id 850
    label "ruszy&#263;"
  ]
  node [
    id 851
    label "zademonstrowa&#263;"
  ]
  node [
    id 852
    label "leave"
  ]
  node [
    id 853
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 854
    label "uko&#324;czy&#263;"
  ]
  node [
    id 855
    label "ukaza&#263;_si&#281;_drukiem"
  ]
  node [
    id 856
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 857
    label "mount"
  ]
  node [
    id 858
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 859
    label "moderate"
  ]
  node [
    id 860
    label "wywie&#347;&#263;_si&#281;"
  ]
  node [
    id 861
    label "sko&#324;czy&#263;"
  ]
  node [
    id 862
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 863
    label "zej&#347;&#263;"
  ]
  node [
    id 864
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 865
    label "wystarczy&#263;"
  ]
  node [
    id 866
    label "perform"
  ]
  node [
    id 867
    label "drive"
  ]
  node [
    id 868
    label "zagra&#263;"
  ]
  node [
    id 869
    label "uzyska&#263;"
  ]
  node [
    id 870
    label "opu&#347;ci&#263;"
  ]
  node [
    id 871
    label "wypa&#347;&#263;"
  ]
  node [
    id 872
    label "motivate"
  ]
  node [
    id 873
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 874
    label "zabra&#263;"
  ]
  node [
    id 875
    label "go"
  ]
  node [
    id 876
    label "allude"
  ]
  node [
    id 877
    label "cut"
  ]
  node [
    id 878
    label "stimulate"
  ]
  node [
    id 879
    label "zacz&#261;&#263;"
  ]
  node [
    id 880
    label "wzbudzi&#263;"
  ]
  node [
    id 881
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 882
    label "end"
  ]
  node [
    id 883
    label "zako&#324;czy&#263;"
  ]
  node [
    id 884
    label "communicate"
  ]
  node [
    id 885
    label "przesta&#263;"
  ]
  node [
    id 886
    label "pokaza&#263;"
  ]
  node [
    id 887
    label "przedstawi&#263;"
  ]
  node [
    id 888
    label "wyrazi&#263;"
  ]
  node [
    id 889
    label "attest"
  ]
  node [
    id 890
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 891
    label "realize"
  ]
  node [
    id 892
    label "promocja"
  ]
  node [
    id 893
    label "make"
  ]
  node [
    id 894
    label "wytworzy&#263;"
  ]
  node [
    id 895
    label "give_birth"
  ]
  node [
    id 896
    label "wynikn&#261;&#263;"
  ]
  node [
    id 897
    label "termin"
  ]
  node [
    id 898
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 899
    label "condescend"
  ]
  node [
    id 900
    label "become"
  ]
  node [
    id 901
    label "temat"
  ]
  node [
    id 902
    label "uby&#263;"
  ]
  node [
    id 903
    label "umrze&#263;"
  ]
  node [
    id 904
    label "za&#347;piewa&#263;"
  ]
  node [
    id 905
    label "obni&#380;y&#263;"
  ]
  node [
    id 906
    label "przenie&#347;&#263;_si&#281;"
  ]
  node [
    id 907
    label "distract"
  ]
  node [
    id 908
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 909
    label "wzej&#347;&#263;"
  ]
  node [
    id 910
    label "zu&#380;y&#263;_si&#281;"
  ]
  node [
    id 911
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 912
    label "obni&#380;y&#263;_si&#281;"
  ]
  node [
    id 913
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 914
    label "odpa&#347;&#263;"
  ]
  node [
    id 915
    label "die"
  ]
  node [
    id 916
    label "zboczy&#263;"
  ]
  node [
    id 917
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 918
    label "wprowadzi&#263;"
  ]
  node [
    id 919
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 920
    label "odej&#347;&#263;"
  ]
  node [
    id 921
    label "zgin&#261;&#263;"
  ]
  node [
    id 922
    label "write_down"
  ]
  node [
    id 923
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 924
    label "zostawi&#263;"
  ]
  node [
    id 925
    label "potani&#263;"
  ]
  node [
    id 926
    label "evacuate"
  ]
  node [
    id 927
    label "humiliate"
  ]
  node [
    id 928
    label "straci&#263;"
  ]
  node [
    id 929
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 930
    label "authorize"
  ]
  node [
    id 931
    label "omin&#261;&#263;"
  ]
  node [
    id 932
    label "suffice"
  ]
  node [
    id 933
    label "stan&#261;&#263;"
  ]
  node [
    id 934
    label "zaspokoi&#263;"
  ]
  node [
    id 935
    label "dosta&#263;"
  ]
  node [
    id 936
    label "play"
  ]
  node [
    id 937
    label "instrument_muzyczny"
  ]
  node [
    id 938
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 939
    label "flare"
  ]
  node [
    id 940
    label "rozegra&#263;"
  ]
  node [
    id 941
    label "zaszczeka&#263;"
  ]
  node [
    id 942
    label "represent"
  ]
  node [
    id 943
    label "wykorzysta&#263;"
  ]
  node [
    id 944
    label "zatokowa&#263;"
  ]
  node [
    id 945
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 946
    label "uda&#263;_si&#281;"
  ]
  node [
    id 947
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 948
    label "wykona&#263;"
  ]
  node [
    id 949
    label "typify"
  ]
  node [
    id 950
    label "rola"
  ]
  node [
    id 951
    label "profit"
  ]
  node [
    id 952
    label "score"
  ]
  node [
    id 953
    label "dotrze&#263;"
  ]
  node [
    id 954
    label "dropiowate"
  ]
  node [
    id 955
    label "kania"
  ]
  node [
    id 956
    label "bustard"
  ]
  node [
    id 957
    label "ptak"
  ]
  node [
    id 958
    label "g&#322;upstwo"
  ]
  node [
    id 959
    label "pomroczno&#347;&#263;_jasna"
  ]
  node [
    id 960
    label "prevention"
  ]
  node [
    id 961
    label "pomiarkowanie"
  ]
  node [
    id 962
    label "przeszkoda"
  ]
  node [
    id 963
    label "intelekt"
  ]
  node [
    id 964
    label "zmniejszenie"
  ]
  node [
    id 965
    label "reservation"
  ]
  node [
    id 966
    label "przekroczenie"
  ]
  node [
    id 967
    label "finlandyzacja"
  ]
  node [
    id 968
    label "otoczenie"
  ]
  node [
    id 969
    label "osielstwo"
  ]
  node [
    id 970
    label "zdyskryminowanie"
  ]
  node [
    id 971
    label "warunek"
  ]
  node [
    id 972
    label "limitation"
  ]
  node [
    id 973
    label "przekroczy&#263;"
  ]
  node [
    id 974
    label "przekraczanie"
  ]
  node [
    id 975
    label "przekracza&#263;"
  ]
  node [
    id 976
    label "barrier"
  ]
  node [
    id 977
    label "aurora"
  ]
  node [
    id 978
    label "wsch&#243;d"
  ]
  node [
    id 979
    label "pora"
  ]
  node [
    id 980
    label "boski"
  ]
  node [
    id 981
    label "krajobraz"
  ]
  node [
    id 982
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 983
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 984
    label "przywidzenie"
  ]
  node [
    id 985
    label "presence"
  ]
  node [
    id 986
    label "charakter"
  ]
  node [
    id 987
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 988
    label "run"
  ]
  node [
    id 989
    label "brzask"
  ]
  node [
    id 990
    label "obszar"
  ]
  node [
    id 991
    label "pocz&#261;tek"
  ]
  node [
    id 992
    label "strona_&#347;wiata"
  ]
  node [
    id 993
    label "szabas"
  ]
  node [
    id 994
    label "s&#322;o&#324;ce"
  ]
  node [
    id 995
    label "ranek"
  ]
  node [
    id 996
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 997
    label "podwiecz&#243;r"
  ]
  node [
    id 998
    label "po&#322;udnie"
  ]
  node [
    id 999
    label "przedpo&#322;udnie"
  ]
  node [
    id 1000
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 1001
    label "wiecz&#243;r"
  ]
  node [
    id 1002
    label "t&#322;usty_czwartek"
  ]
  node [
    id 1003
    label "popo&#322;udnie"
  ]
  node [
    id 1004
    label "walentynki"
  ]
  node [
    id 1005
    label "czynienie_si&#281;"
  ]
  node [
    id 1006
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 1007
    label "wzej&#347;cie"
  ]
  node [
    id 1008
    label "wsta&#263;"
  ]
  node [
    id 1009
    label "day"
  ]
  node [
    id 1010
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 1011
    label "wstanie"
  ]
  node [
    id 1012
    label "przedwiecz&#243;r"
  ]
  node [
    id 1013
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 1014
    label "Sylwester"
  ]
  node [
    id 1015
    label "XD"
  ]
  node [
    id 1016
    label "szach"
  ]
  node [
    id 1017
    label "mat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 195
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 197
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 200
  ]
  edge [
    source 11
    target 46
  ]
  edge [
    source 11
    target 47
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 11
    target 290
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 246
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 204
  ]
  edge [
    source 12
    target 206
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 57
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 387
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 290
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 205
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 245
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 248
  ]
  edge [
    source 12
    target 207
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 752
  ]
  edge [
    source 14
    target 753
  ]
  edge [
    source 14
    target 754
  ]
  edge [
    source 14
    target 755
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 757
  ]
  edge [
    source 14
    target 758
  ]
  edge [
    source 14
    target 759
  ]
  edge [
    source 14
    target 760
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 762
  ]
  edge [
    source 14
    target 763
  ]
  edge [
    source 14
    target 764
  ]
  edge [
    source 14
    target 765
  ]
  edge [
    source 14
    target 766
  ]
  edge [
    source 14
    target 767
  ]
  edge [
    source 14
    target 341
  ]
  edge [
    source 14
    target 768
  ]
  edge [
    source 14
    target 769
  ]
  edge [
    source 14
    target 770
  ]
  edge [
    source 14
    target 771
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 773
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 715
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 289
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 726
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 246
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 578
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 281
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 387
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 277
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 521
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 538
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 656
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 104
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 472
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 577
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 289
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 19
    target 977
  ]
  edge [
    source 19
    target 978
  ]
  edge [
    source 19
    target 251
  ]
  edge [
    source 19
    target 328
  ]
  edge [
    source 19
    target 979
  ]
  edge [
    source 19
    target 630
  ]
  edge [
    source 19
    target 980
  ]
  edge [
    source 19
    target 981
  ]
  edge [
    source 19
    target 982
  ]
  edge [
    source 19
    target 983
  ]
  edge [
    source 19
    target 984
  ]
  edge [
    source 19
    target 985
  ]
  edge [
    source 19
    target 986
  ]
  edge [
    source 19
    target 987
  ]
  edge [
    source 19
    target 988
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 989
  ]
  edge [
    source 19
    target 990
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 290
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 250
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 252
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 249
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 897
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 1015
    target 1016
  ]
  edge [
    source 1015
    target 1017
  ]
  edge [
    source 1016
    target 1017
  ]
]
