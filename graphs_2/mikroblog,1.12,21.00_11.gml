graph [
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 2
    label "dla"
    origin "text"
  ]
  node [
    id 3
    label "nasze"
    origin "text"
  ]
  node [
    id 4
    label "pierwsza"
    origin "text"
  ]
  node [
    id 5
    label "rozdajo"
    origin "text"
  ]
  node [
    id 6
    label "przodkini"
  ]
  node [
    id 7
    label "matka_zast&#281;pcza"
  ]
  node [
    id 8
    label "matczysko"
  ]
  node [
    id 9
    label "rodzice"
  ]
  node [
    id 10
    label "stara"
  ]
  node [
    id 11
    label "macierz"
  ]
  node [
    id 12
    label "rodzic"
  ]
  node [
    id 13
    label "Matka_Boska"
  ]
  node [
    id 14
    label "macocha"
  ]
  node [
    id 15
    label "starzy"
  ]
  node [
    id 16
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 17
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 18
    label "pokolenie"
  ]
  node [
    id 19
    label "wapniaki"
  ]
  node [
    id 20
    label "opiekun"
  ]
  node [
    id 21
    label "wapniak"
  ]
  node [
    id 22
    label "rodzic_chrzestny"
  ]
  node [
    id 23
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 24
    label "krewna"
  ]
  node [
    id 25
    label "matka"
  ]
  node [
    id 26
    label "&#380;ona"
  ]
  node [
    id 27
    label "kobieta"
  ]
  node [
    id 28
    label "partnerka"
  ]
  node [
    id 29
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 30
    label "matuszka"
  ]
  node [
    id 31
    label "parametryzacja"
  ]
  node [
    id 32
    label "pa&#324;stwo"
  ]
  node [
    id 33
    label "poj&#281;cie"
  ]
  node [
    id 34
    label "mod"
  ]
  node [
    id 35
    label "patriota"
  ]
  node [
    id 36
    label "m&#281;&#380;atka"
  ]
  node [
    id 37
    label "doba"
  ]
  node [
    id 38
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 39
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 40
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 41
    label "dzi&#347;"
  ]
  node [
    id 42
    label "teraz"
  ]
  node [
    id 43
    label "czas"
  ]
  node [
    id 44
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 45
    label "jednocze&#347;nie"
  ]
  node [
    id 46
    label "tydzie&#324;"
  ]
  node [
    id 47
    label "noc"
  ]
  node [
    id 48
    label "dzie&#324;"
  ]
  node [
    id 49
    label "godzina"
  ]
  node [
    id 50
    label "long_time"
  ]
  node [
    id 51
    label "jednostka_geologiczna"
  ]
  node [
    id 52
    label "time"
  ]
  node [
    id 53
    label "p&#243;&#322;godzina"
  ]
  node [
    id 54
    label "jednostka_czasu"
  ]
  node [
    id 55
    label "minuta"
  ]
  node [
    id 56
    label "kwadrans"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
]
