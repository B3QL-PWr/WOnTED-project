graph [
  node [
    id 0
    label "pocisk"
    origin "text"
  ]
  node [
    id 1
    label "miota&#263;"
    origin "text"
  ]
  node [
    id 2
    label "przez"
    origin "text"
  ]
  node [
    id 3
    label "katapulta"
    origin "text"
  ]
  node [
    id 4
    label "trebusze"
    origin "text"
  ]
  node [
    id 5
    label "amunicja"
  ]
  node [
    id 6
    label "g&#322;owica"
  ]
  node [
    id 7
    label "trafienie"
  ]
  node [
    id 8
    label "trafianie"
  ]
  node [
    id 9
    label "kulka"
  ]
  node [
    id 10
    label "rdze&#324;"
  ]
  node [
    id 11
    label "prochownia"
  ]
  node [
    id 12
    label "przeniesienie"
  ]
  node [
    id 13
    label "&#322;adunek_bojowy"
  ]
  node [
    id 14
    label "trafi&#263;"
  ]
  node [
    id 15
    label "przenoszenie"
  ]
  node [
    id 16
    label "przenie&#347;&#263;"
  ]
  node [
    id 17
    label "trafia&#263;"
  ]
  node [
    id 18
    label "przenosi&#263;"
  ]
  node [
    id 19
    label "bro&#324;"
  ]
  node [
    id 20
    label "wytw&#243;rnia"
  ]
  node [
    id 21
    label "magazyn"
  ]
  node [
    id 22
    label "karta_przetargowa"
  ]
  node [
    id 23
    label "rozbroi&#263;"
  ]
  node [
    id 24
    label "rozbrojenie"
  ]
  node [
    id 25
    label "osprz&#281;t"
  ]
  node [
    id 26
    label "uzbrojenie"
  ]
  node [
    id 27
    label "przyrz&#261;d"
  ]
  node [
    id 28
    label "rozbrajanie"
  ]
  node [
    id 29
    label "rozbraja&#263;"
  ]
  node [
    id 30
    label "or&#281;&#380;"
  ]
  node [
    id 31
    label "reaktor_j&#261;drowy"
  ]
  node [
    id 32
    label "magnes"
  ]
  node [
    id 33
    label "morfem"
  ]
  node [
    id 34
    label "spowalniacz"
  ]
  node [
    id 35
    label "transformator"
  ]
  node [
    id 36
    label "mi&#281;kisz"
  ]
  node [
    id 37
    label "marrow"
  ]
  node [
    id 38
    label "pr&#281;t_kontrolny"
  ]
  node [
    id 39
    label "wn&#281;trze"
  ]
  node [
    id 40
    label "istota"
  ]
  node [
    id 41
    label "procesor"
  ]
  node [
    id 42
    label "odlewnictwo"
  ]
  node [
    id 43
    label "ch&#322;odziwo"
  ]
  node [
    id 44
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 45
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 46
    label "forma"
  ]
  node [
    id 47
    label "surowiak"
  ]
  node [
    id 48
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 49
    label "core"
  ]
  node [
    id 50
    label "zwie&#324;czenie"
  ]
  node [
    id 51
    label "g&#322;owa"
  ]
  node [
    id 52
    label "choroba_wirusowa"
  ]
  node [
    id 53
    label "schorzenie"
  ]
  node [
    id 54
    label "abakus"
  ]
  node [
    id 55
    label "kolumna"
  ]
  node [
    id 56
    label "drumhead"
  ]
  node [
    id 57
    label "urz&#261;dzenie"
  ]
  node [
    id 58
    label "figura_zaszczytna"
  ]
  node [
    id 59
    label "ball"
  ]
  node [
    id 60
    label "kula"
  ]
  node [
    id 61
    label "czasza"
  ]
  node [
    id 62
    label "marmurki"
  ]
  node [
    id 63
    label "sphere"
  ]
  node [
    id 64
    label "bry&#322;a"
  ]
  node [
    id 65
    label "ma&#322;y_przedmiot"
  ]
  node [
    id 66
    label "porcja"
  ]
  node [
    id 67
    label "motivate"
  ]
  node [
    id 68
    label "dostosowa&#263;"
  ]
  node [
    id 69
    label "deepen"
  ]
  node [
    id 70
    label "relocate"
  ]
  node [
    id 71
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 72
    label "transfer"
  ]
  node [
    id 73
    label "rozpowszechni&#263;"
  ]
  node [
    id 74
    label "go"
  ]
  node [
    id 75
    label "shift"
  ]
  node [
    id 76
    label "skopiowa&#263;"
  ]
  node [
    id 77
    label "zmieni&#263;"
  ]
  node [
    id 78
    label "przelecie&#263;"
  ]
  node [
    id 79
    label "umie&#347;ci&#263;"
  ]
  node [
    id 80
    label "strzeli&#263;"
  ]
  node [
    id 81
    label "dosi&#281;ganie"
  ]
  node [
    id 82
    label "dopasowywanie_si&#281;"
  ]
  node [
    id 83
    label "zjawianie_si&#281;"
  ]
  node [
    id 84
    label "wpadanie"
  ]
  node [
    id 85
    label "pojawianie_si&#281;"
  ]
  node [
    id 86
    label "dostawanie"
  ]
  node [
    id 87
    label "docieranie"
  ]
  node [
    id 88
    label "aim"
  ]
  node [
    id 89
    label "dolatywanie"
  ]
  node [
    id 90
    label "znajdowanie"
  ]
  node [
    id 91
    label "dzianie_si&#281;"
  ]
  node [
    id 92
    label "dostawanie_si&#281;"
  ]
  node [
    id 93
    label "meeting"
  ]
  node [
    id 94
    label "zjawienie_si&#281;"
  ]
  node [
    id 95
    label "dolecenie"
  ]
  node [
    id 96
    label "punkt"
  ]
  node [
    id 97
    label "rozgrywka"
  ]
  node [
    id 98
    label "strike"
  ]
  node [
    id 99
    label "dostanie_si&#281;"
  ]
  node [
    id 100
    label "wpadni&#281;cie"
  ]
  node [
    id 101
    label "spowodowanie"
  ]
  node [
    id 102
    label "hit"
  ]
  node [
    id 103
    label "zdarzenie_si&#281;"
  ]
  node [
    id 104
    label "sukces"
  ]
  node [
    id 105
    label "znalezienie_si&#281;"
  ]
  node [
    id 106
    label "znalezienie"
  ]
  node [
    id 107
    label "dopasowanie_si&#281;"
  ]
  node [
    id 108
    label "dotarcie"
  ]
  node [
    id 109
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 110
    label "gather"
  ]
  node [
    id 111
    label "dostanie"
  ]
  node [
    id 112
    label "dostosowanie"
  ]
  node [
    id 113
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 114
    label "rozpowszechnienie"
  ]
  node [
    id 115
    label "skopiowanie"
  ]
  node [
    id 116
    label "move"
  ]
  node [
    id 117
    label "assignment"
  ]
  node [
    id 118
    label "przemieszczenie"
  ]
  node [
    id 119
    label "przelecenie"
  ]
  node [
    id 120
    label "mechanizm_obronny"
  ]
  node [
    id 121
    label "zmienienie"
  ]
  node [
    id 122
    label "umieszczenie"
  ]
  node [
    id 123
    label "strzelenie"
  ]
  node [
    id 124
    label "przesadzenie"
  ]
  node [
    id 125
    label "poprzesuwanie"
  ]
  node [
    id 126
    label "infection"
  ]
  node [
    id 127
    label "umieszczanie"
  ]
  node [
    id 128
    label "zmienianie"
  ]
  node [
    id 129
    label "strzelanie"
  ]
  node [
    id 130
    label "transportation"
  ]
  node [
    id 131
    label "przesadzanie"
  ]
  node [
    id 132
    label "kopiowanie"
  ]
  node [
    id 133
    label "transmission"
  ]
  node [
    id 134
    label "dostosowywanie"
  ]
  node [
    id 135
    label "drift"
  ]
  node [
    id 136
    label "przetrwanie"
  ]
  node [
    id 137
    label "przesuwanie_si&#281;"
  ]
  node [
    id 138
    label "przemieszczanie"
  ]
  node [
    id 139
    label "przelatywanie"
  ]
  node [
    id 140
    label "translation"
  ]
  node [
    id 141
    label "ponoszenie"
  ]
  node [
    id 142
    label "rozpowszechnianie"
  ]
  node [
    id 143
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 144
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 145
    label "indicate"
  ]
  node [
    id 146
    label "spotyka&#263;"
  ]
  node [
    id 147
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 148
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 149
    label "dociera&#263;"
  ]
  node [
    id 150
    label "dolatywa&#263;"
  ]
  node [
    id 151
    label "dopasowywa&#263;_si&#281;"
  ]
  node [
    id 152
    label "znajdowa&#263;"
  ]
  node [
    id 153
    label "happen"
  ]
  node [
    id 154
    label "wpada&#263;"
  ]
  node [
    id 155
    label "kopiowa&#263;"
  ]
  node [
    id 156
    label "dostosowywa&#263;"
  ]
  node [
    id 157
    label "estrange"
  ]
  node [
    id 158
    label "ponosi&#263;"
  ]
  node [
    id 159
    label "rozpowszechnia&#263;"
  ]
  node [
    id 160
    label "rozprzestrzenia&#263;"
  ]
  node [
    id 161
    label "circulate"
  ]
  node [
    id 162
    label "przemieszcza&#263;"
  ]
  node [
    id 163
    label "zmienia&#263;"
  ]
  node [
    id 164
    label "wytrzyma&#263;"
  ]
  node [
    id 165
    label "umieszcza&#263;"
  ]
  node [
    id 166
    label "przelatywa&#263;"
  ]
  node [
    id 167
    label "infest"
  ]
  node [
    id 168
    label "strzela&#263;"
  ]
  node [
    id 169
    label "dolecie&#263;"
  ]
  node [
    id 170
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 171
    label "spotka&#263;"
  ]
  node [
    id 172
    label "przypasowa&#263;"
  ]
  node [
    id 173
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 174
    label "stumble"
  ]
  node [
    id 175
    label "dotrze&#263;"
  ]
  node [
    id 176
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 177
    label "wpa&#347;&#263;"
  ]
  node [
    id 178
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 179
    label "znale&#378;&#263;"
  ]
  node [
    id 180
    label "grzmoci&#263;"
  ]
  node [
    id 181
    label "tug"
  ]
  node [
    id 182
    label "syga&#263;"
  ]
  node [
    id 183
    label "traci&#263;"
  ]
  node [
    id 184
    label "alternate"
  ]
  node [
    id 185
    label "change"
  ]
  node [
    id 186
    label "reengineering"
  ]
  node [
    id 187
    label "zast&#281;powa&#263;"
  ]
  node [
    id 188
    label "sprawia&#263;"
  ]
  node [
    id 189
    label "zyskiwa&#263;"
  ]
  node [
    id 190
    label "przechodzi&#263;"
  ]
  node [
    id 191
    label "rzuca&#263;"
  ]
  node [
    id 192
    label "beat"
  ]
  node [
    id 193
    label "bra&#263;"
  ]
  node [
    id 194
    label "fall"
  ]
  node [
    id 195
    label "hucze&#263;"
  ]
  node [
    id 196
    label "uderza&#263;"
  ]
  node [
    id 197
    label "machina_miotaj&#261;ca"
  ]
  node [
    id 198
    label "samolot"
  ]
  node [
    id 199
    label "machina_obl&#281;&#380;nicza"
  ]
  node [
    id 200
    label "artyleria_przedogniowa"
  ]
  node [
    id 201
    label "przedmiot"
  ]
  node [
    id 202
    label "kom&#243;rka"
  ]
  node [
    id 203
    label "furnishing"
  ]
  node [
    id 204
    label "zabezpieczenie"
  ]
  node [
    id 205
    label "zrobienie"
  ]
  node [
    id 206
    label "wyrz&#261;dzenie"
  ]
  node [
    id 207
    label "zagospodarowanie"
  ]
  node [
    id 208
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 209
    label "ig&#322;a"
  ]
  node [
    id 210
    label "narz&#281;dzie"
  ]
  node [
    id 211
    label "wirnik"
  ]
  node [
    id 212
    label "aparatura"
  ]
  node [
    id 213
    label "system_energetyczny"
  ]
  node [
    id 214
    label "impulsator"
  ]
  node [
    id 215
    label "mechanizm"
  ]
  node [
    id 216
    label "sprz&#281;t"
  ]
  node [
    id 217
    label "czynno&#347;&#263;"
  ]
  node [
    id 218
    label "blokowanie"
  ]
  node [
    id 219
    label "set"
  ]
  node [
    id 220
    label "zablokowanie"
  ]
  node [
    id 221
    label "przygotowanie"
  ]
  node [
    id 222
    label "komora"
  ]
  node [
    id 223
    label "j&#281;zyk"
  ]
  node [
    id 224
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 225
    label "spalin&#243;wka"
  ]
  node [
    id 226
    label "pilot_automatyczny"
  ]
  node [
    id 227
    label "kad&#322;ub"
  ]
  node [
    id 228
    label "wiatrochron"
  ]
  node [
    id 229
    label "kabina"
  ]
  node [
    id 230
    label "wylatywanie"
  ]
  node [
    id 231
    label "kapotowanie"
  ]
  node [
    id 232
    label "kapotowa&#263;"
  ]
  node [
    id 233
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 234
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 235
    label "skrzyd&#322;o"
  ]
  node [
    id 236
    label "pok&#322;ad"
  ]
  node [
    id 237
    label "kapota&#380;"
  ]
  node [
    id 238
    label "sta&#322;op&#322;at"
  ]
  node [
    id 239
    label "sterownica"
  ]
  node [
    id 240
    label "p&#322;atowiec"
  ]
  node [
    id 241
    label "wylecenie"
  ]
  node [
    id 242
    label "wylecie&#263;"
  ]
  node [
    id 243
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 244
    label "wylatywa&#263;"
  ]
  node [
    id 245
    label "gondola"
  ]
  node [
    id 246
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 247
    label "dzi&#243;b"
  ]
  node [
    id 248
    label "inhalator_tlenowy"
  ]
  node [
    id 249
    label "kapot"
  ]
  node [
    id 250
    label "kabinka"
  ]
  node [
    id 251
    label "&#380;yroskop"
  ]
  node [
    id 252
    label "czarna_skrzynka"
  ]
  node [
    id 253
    label "lecenie"
  ]
  node [
    id 254
    label "fotel_lotniczy"
  ]
  node [
    id 255
    label "wy&#347;lizg"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
]
