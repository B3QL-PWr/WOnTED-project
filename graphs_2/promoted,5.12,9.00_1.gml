graph [
  node [
    id 0
    label "chorowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "nowotw&#243;r"
    origin "text"
  ]
  node [
    id 2
    label "niedowidzie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 4
    label "poruszaj&#261;cy"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "w&#243;zek"
    origin "text"
  ]
  node [
    id 7
    label "inwalidzki"
    origin "text"
  ]
  node [
    id 8
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 9
    label "okra&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "swoje"
    origin "text"
  ]
  node [
    id 11
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 12
    label "pain"
  ]
  node [
    id 13
    label "garlic"
  ]
  node [
    id 14
    label "pragn&#261;&#263;"
  ]
  node [
    id 15
    label "cierpie&#263;"
  ]
  node [
    id 16
    label "czu&#263;"
  ]
  node [
    id 17
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 18
    label "&#322;akn&#261;&#263;"
  ]
  node [
    id 19
    label "t&#281;skni&#263;"
  ]
  node [
    id 20
    label "desire"
  ]
  node [
    id 21
    label "chcie&#263;"
  ]
  node [
    id 22
    label "martwi&#263;_si&#281;"
  ]
  node [
    id 23
    label "sting"
  ]
  node [
    id 24
    label "pochyla&#263;_si&#281;"
  ]
  node [
    id 25
    label "traci&#263;"
  ]
  node [
    id 26
    label "wytrzymywa&#263;"
  ]
  node [
    id 27
    label "represent"
  ]
  node [
    id 28
    label "j&#281;cze&#263;"
  ]
  node [
    id 29
    label "boryka&#263;_si&#281;"
  ]
  node [
    id 30
    label "narzeka&#263;"
  ]
  node [
    id 31
    label "hurt"
  ]
  node [
    id 32
    label "doznawa&#263;"
  ]
  node [
    id 33
    label "tkanka"
  ]
  node [
    id 34
    label "dysplazja"
  ]
  node [
    id 35
    label "rozrost"
  ]
  node [
    id 36
    label "badanie_histopatologiczne"
  ]
  node [
    id 37
    label "zmiana"
  ]
  node [
    id 38
    label "rozw&#243;j"
  ]
  node [
    id 39
    label "growth"
  ]
  node [
    id 40
    label "rewizja"
  ]
  node [
    id 41
    label "passage"
  ]
  node [
    id 42
    label "oznaka"
  ]
  node [
    id 43
    label "change"
  ]
  node [
    id 44
    label "ferment"
  ]
  node [
    id 45
    label "komplet"
  ]
  node [
    id 46
    label "anatomopatolog"
  ]
  node [
    id 47
    label "zmianka"
  ]
  node [
    id 48
    label "czas"
  ]
  node [
    id 49
    label "zjawisko"
  ]
  node [
    id 50
    label "amendment"
  ]
  node [
    id 51
    label "praca"
  ]
  node [
    id 52
    label "odmienianie"
  ]
  node [
    id 53
    label "tura"
  ]
  node [
    id 54
    label "odwarstwi&#263;_si&#281;"
  ]
  node [
    id 55
    label "odwarstwi&#263;"
  ]
  node [
    id 56
    label "tissue"
  ]
  node [
    id 57
    label "histochemia"
  ]
  node [
    id 58
    label "organ"
  ]
  node [
    id 59
    label "kom&#243;rka"
  ]
  node [
    id 60
    label "oddychanie_tkankowe"
  ]
  node [
    id 61
    label "wapnie&#263;"
  ]
  node [
    id 62
    label "odwarstwia&#263;"
  ]
  node [
    id 63
    label "trofika"
  ]
  node [
    id 64
    label "element_anatomiczny"
  ]
  node [
    id 65
    label "wapnienie"
  ]
  node [
    id 66
    label "zserowacie&#263;"
  ]
  node [
    id 67
    label "zserowacenie"
  ]
  node [
    id 68
    label "odwarstwia&#263;_si&#281;"
  ]
  node [
    id 69
    label "serowacenie"
  ]
  node [
    id 70
    label "serowacie&#263;"
  ]
  node [
    id 71
    label "makrocytoza"
  ]
  node [
    id 72
    label "wada_wrodzona"
  ]
  node [
    id 73
    label "belfer"
  ]
  node [
    id 74
    label "kszta&#322;ciciel"
  ]
  node [
    id 75
    label "preceptor"
  ]
  node [
    id 76
    label "pedagog"
  ]
  node [
    id 77
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 78
    label "szkolnik"
  ]
  node [
    id 79
    label "profesor"
  ]
  node [
    id 80
    label "popularyzator"
  ]
  node [
    id 81
    label "rozszerzyciel"
  ]
  node [
    id 82
    label "cz&#322;owiek"
  ]
  node [
    id 83
    label "stopie&#324;_naukowy"
  ]
  node [
    id 84
    label "nauczyciel_akademicki"
  ]
  node [
    id 85
    label "tytu&#322;"
  ]
  node [
    id 86
    label "profesura"
  ]
  node [
    id 87
    label "konsulent"
  ]
  node [
    id 88
    label "wirtuoz"
  ]
  node [
    id 89
    label "autor"
  ]
  node [
    id 90
    label "wyprawka"
  ]
  node [
    id 91
    label "mundurek"
  ]
  node [
    id 92
    label "szko&#322;a"
  ]
  node [
    id 93
    label "tarcza"
  ]
  node [
    id 94
    label "elew"
  ]
  node [
    id 95
    label "absolwent"
  ]
  node [
    id 96
    label "klasa"
  ]
  node [
    id 97
    label "zwierzchnik"
  ]
  node [
    id 98
    label "ekspert"
  ]
  node [
    id 99
    label "ochotnik"
  ]
  node [
    id 100
    label "pomocnik"
  ]
  node [
    id 101
    label "student"
  ]
  node [
    id 102
    label "nauczyciel_muzyki"
  ]
  node [
    id 103
    label "zakonnik"
  ]
  node [
    id 104
    label "J&#281;drzejewicz"
  ]
  node [
    id 105
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 106
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 107
    label "John_Dewey"
  ]
  node [
    id 108
    label "pojazd_niemechaniczny"
  ]
  node [
    id 109
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 110
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 111
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 112
    label "osta&#263;_si&#281;"
  ]
  node [
    id 113
    label "pozosta&#263;"
  ]
  node [
    id 114
    label "catch"
  ]
  node [
    id 115
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 116
    label "proceed"
  ]
  node [
    id 117
    label "support"
  ]
  node [
    id 118
    label "prze&#380;y&#263;"
  ]
  node [
    id 119
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 120
    label "obedrze&#263;"
  ]
  node [
    id 121
    label "treat"
  ]
  node [
    id 122
    label "zabra&#263;"
  ]
  node [
    id 123
    label "obskoczy&#263;"
  ]
  node [
    id 124
    label "withdraw"
  ]
  node [
    id 125
    label "doprowadzi&#263;"
  ]
  node [
    id 126
    label "z&#322;apa&#263;"
  ]
  node [
    id 127
    label "wzi&#261;&#263;"
  ]
  node [
    id 128
    label "zaj&#261;&#263;"
  ]
  node [
    id 129
    label "spowodowa&#263;"
  ]
  node [
    id 130
    label "consume"
  ]
  node [
    id 131
    label "deprive"
  ]
  node [
    id 132
    label "przenie&#347;&#263;"
  ]
  node [
    id 133
    label "abstract"
  ]
  node [
    id 134
    label "uda&#263;_si&#281;"
  ]
  node [
    id 135
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 136
    label "przesun&#261;&#263;"
  ]
  node [
    id 137
    label "poradzi&#263;_sobie"
  ]
  node [
    id 138
    label "osaczy&#263;"
  ]
  node [
    id 139
    label "zrobi&#263;"
  ]
  node [
    id 140
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 141
    label "posi&#261;&#347;&#263;"
  ]
  node [
    id 142
    label "obiec"
  ]
  node [
    id 143
    label "dosta&#263;"
  ]
  node [
    id 144
    label "wear"
  ]
  node [
    id 145
    label "zarobi&#263;"
  ]
  node [
    id 146
    label "&#347;ci&#261;gn&#261;&#263;"
  ]
  node [
    id 147
    label "cover"
  ]
  node [
    id 148
    label "adjustment"
  ]
  node [
    id 149
    label "panowanie"
  ]
  node [
    id 150
    label "przebywanie"
  ]
  node [
    id 151
    label "animation"
  ]
  node [
    id 152
    label "kwadrat"
  ]
  node [
    id 153
    label "stanie"
  ]
  node [
    id 154
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 155
    label "pomieszkanie"
  ]
  node [
    id 156
    label "lokal"
  ]
  node [
    id 157
    label "dom"
  ]
  node [
    id 158
    label "zajmowanie"
  ]
  node [
    id 159
    label "sprawowanie"
  ]
  node [
    id 160
    label "bycie"
  ]
  node [
    id 161
    label "kierowanie"
  ]
  node [
    id 162
    label "w&#322;adca"
  ]
  node [
    id 163
    label "dominowanie"
  ]
  node [
    id 164
    label "przewaga"
  ]
  node [
    id 165
    label "przewa&#380;anie"
  ]
  node [
    id 166
    label "znaczenie"
  ]
  node [
    id 167
    label "laterality"
  ]
  node [
    id 168
    label "control"
  ]
  node [
    id 169
    label "temper"
  ]
  node [
    id 170
    label "kontrolowanie"
  ]
  node [
    id 171
    label "dominance"
  ]
  node [
    id 172
    label "rule"
  ]
  node [
    id 173
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 174
    label "prym"
  ]
  node [
    id 175
    label "w&#322;adza"
  ]
  node [
    id 176
    label "ocieranie_si&#281;"
  ]
  node [
    id 177
    label "otoczenie_si&#281;"
  ]
  node [
    id 178
    label "posiedzenie"
  ]
  node [
    id 179
    label "otarcie_si&#281;"
  ]
  node [
    id 180
    label "atakowanie"
  ]
  node [
    id 181
    label "otaczanie_si&#281;"
  ]
  node [
    id 182
    label "wyj&#347;cie"
  ]
  node [
    id 183
    label "zmierzanie"
  ]
  node [
    id 184
    label "residency"
  ]
  node [
    id 185
    label "sojourn"
  ]
  node [
    id 186
    label "wychodzenie"
  ]
  node [
    id 187
    label "tkwienie"
  ]
  node [
    id 188
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 189
    label "powodowanie"
  ]
  node [
    id 190
    label "lokowanie_si&#281;"
  ]
  node [
    id 191
    label "schorzenie"
  ]
  node [
    id 192
    label "zajmowanie_si&#281;"
  ]
  node [
    id 193
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 194
    label "stosowanie"
  ]
  node [
    id 195
    label "anektowanie"
  ]
  node [
    id 196
    label "ciekawy"
  ]
  node [
    id 197
    label "zabieranie"
  ]
  node [
    id 198
    label "robienie"
  ]
  node [
    id 199
    label "sytuowanie_si&#281;"
  ]
  node [
    id 200
    label "wype&#322;nianie"
  ]
  node [
    id 201
    label "obejmowanie"
  ]
  node [
    id 202
    label "klasyfikacja"
  ]
  node [
    id 203
    label "czynno&#347;&#263;"
  ]
  node [
    id 204
    label "dzianie_si&#281;"
  ]
  node [
    id 205
    label "branie"
  ]
  node [
    id 206
    label "rz&#261;dzenie"
  ]
  node [
    id 207
    label "occupation"
  ]
  node [
    id 208
    label "zadawanie"
  ]
  node [
    id 209
    label "zaj&#281;ty"
  ]
  node [
    id 210
    label "miejsce"
  ]
  node [
    id 211
    label "gastronomia"
  ]
  node [
    id 212
    label "zak&#322;ad"
  ]
  node [
    id 213
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 214
    label "rodzina"
  ]
  node [
    id 215
    label "substancja_mieszkaniowa"
  ]
  node [
    id 216
    label "instytucja"
  ]
  node [
    id 217
    label "siedziba"
  ]
  node [
    id 218
    label "dom_rodzinny"
  ]
  node [
    id 219
    label "budynek"
  ]
  node [
    id 220
    label "grupa"
  ]
  node [
    id 221
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 222
    label "poj&#281;cie"
  ]
  node [
    id 223
    label "stead"
  ]
  node [
    id 224
    label "garderoba"
  ]
  node [
    id 225
    label "wiecha"
  ]
  node [
    id 226
    label "fratria"
  ]
  node [
    id 227
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 228
    label "trwanie"
  ]
  node [
    id 229
    label "ustanie"
  ]
  node [
    id 230
    label "wystanie"
  ]
  node [
    id 231
    label "postanie"
  ]
  node [
    id 232
    label "wystawanie"
  ]
  node [
    id 233
    label "kosztowanie"
  ]
  node [
    id 234
    label "przestanie"
  ]
  node [
    id 235
    label "pot&#281;ga"
  ]
  node [
    id 236
    label "wielok&#261;t_foremny"
  ]
  node [
    id 237
    label "stopie&#324;_pisma"
  ]
  node [
    id 238
    label "prostok&#261;t"
  ]
  node [
    id 239
    label "square"
  ]
  node [
    id 240
    label "romb"
  ]
  node [
    id 241
    label "justunek"
  ]
  node [
    id 242
    label "dzielnica"
  ]
  node [
    id 243
    label "poletko"
  ]
  node [
    id 244
    label "ekologia"
  ]
  node [
    id 245
    label "tango"
  ]
  node [
    id 246
    label "figura_taneczna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 172
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 195
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 197
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 200
  ]
  edge [
    source 11
    target 201
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 203
  ]
  edge [
    source 11
    target 204
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 11
    target 206
  ]
  edge [
    source 11
    target 207
  ]
  edge [
    source 11
    target 208
  ]
  edge [
    source 11
    target 209
  ]
  edge [
    source 11
    target 210
  ]
  edge [
    source 11
    target 211
  ]
  edge [
    source 11
    target 212
  ]
  edge [
    source 11
    target 213
  ]
  edge [
    source 11
    target 214
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 217
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 219
  ]
  edge [
    source 11
    target 220
  ]
  edge [
    source 11
    target 221
  ]
  edge [
    source 11
    target 222
  ]
  edge [
    source 11
    target 223
  ]
  edge [
    source 11
    target 224
  ]
  edge [
    source 11
    target 225
  ]
  edge [
    source 11
    target 226
  ]
  edge [
    source 11
    target 227
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 246
  ]
]
