graph [
  node [
    id 0
    label "uprawnienie"
    origin "text"
  ]
  node [
    id 1
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 2
    label "miasto"
    origin "text"
  ]
  node [
    id 3
    label "opole"
    origin "text"
  ]
  node [
    id 4
    label "organizacja"
    origin "text"
  ]
  node [
    id 5
    label "zakres"
    origin "text"
  ]
  node [
    id 6
    label "kierowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "rad"
    origin "text"
  ]
  node [
    id 8
    label "wniosek"
    origin "text"
  ]
  node [
    id 9
    label "podejmowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "uchwa&#322;a"
    origin "text"
  ]
  node [
    id 11
    label "przedmiot"
    origin "text"
  ]
  node [
    id 12
    label "zastrzec"
    origin "text"
  ]
  node [
    id 13
    label "w&#322;a&#347;ciwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 14
    label "inny"
    origin "text"
  ]
  node [
    id 15
    label "organ"
    origin "text"
  ]
  node [
    id 16
    label "podmiot"
    origin "text"
  ]
  node [
    id 17
    label "regulowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "xxvi"
    origin "text"
  ]
  node [
    id 19
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 20
    label "luty"
    origin "text"
  ]
  node [
    id 21
    label "rocznik"
    origin "text"
  ]
  node [
    id 22
    label "sprawa"
    origin "text"
  ]
  node [
    id 23
    label "uchwali&#263;"
    origin "text"
  ]
  node [
    id 24
    label "statut"
    origin "text"
  ]
  node [
    id 25
    label "p&#243;&#378;ny"
    origin "text"
  ]
  node [
    id 26
    label "zmiana"
    origin "text"
  ]
  node [
    id 27
    label "law"
  ]
  node [
    id 28
    label "authorization"
  ]
  node [
    id 29
    label "spowodowanie"
  ]
  node [
    id 30
    label "title"
  ]
  node [
    id 31
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 32
    label "czynno&#347;&#263;"
  ]
  node [
    id 33
    label "dokument"
  ]
  node [
    id 34
    label "activity"
  ]
  node [
    id 35
    label "bezproblemowy"
  ]
  node [
    id 36
    label "wydarzenie"
  ]
  node [
    id 37
    label "campaign"
  ]
  node [
    id 38
    label "causing"
  ]
  node [
    id 39
    label "posiada&#263;"
  ]
  node [
    id 40
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 41
    label "egzekutywa"
  ]
  node [
    id 42
    label "potencja&#322;"
  ]
  node [
    id 43
    label "wyb&#243;r"
  ]
  node [
    id 44
    label "prospect"
  ]
  node [
    id 45
    label "ability"
  ]
  node [
    id 46
    label "obliczeniowo"
  ]
  node [
    id 47
    label "alternatywa"
  ]
  node [
    id 48
    label "cecha"
  ]
  node [
    id 49
    label "operator_modalny"
  ]
  node [
    id 50
    label "zapis"
  ]
  node [
    id 51
    label "&#347;wiadectwo"
  ]
  node [
    id 52
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 53
    label "wytw&#243;r"
  ]
  node [
    id 54
    label "parafa"
  ]
  node [
    id 55
    label "plik"
  ]
  node [
    id 56
    label "raport&#243;wka"
  ]
  node [
    id 57
    label "utw&#243;r"
  ]
  node [
    id 58
    label "record"
  ]
  node [
    id 59
    label "fascyku&#322;"
  ]
  node [
    id 60
    label "dokumentacja"
  ]
  node [
    id 61
    label "registratura"
  ]
  node [
    id 62
    label "artyku&#322;"
  ]
  node [
    id 63
    label "writing"
  ]
  node [
    id 64
    label "sygnatariusz"
  ]
  node [
    id 65
    label "ludno&#347;&#263;"
  ]
  node [
    id 66
    label "zwierz&#281;"
  ]
  node [
    id 67
    label "cz&#322;owiek"
  ]
  node [
    id 68
    label "degenerat"
  ]
  node [
    id 69
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 70
    label "zwyrol"
  ]
  node [
    id 71
    label "czerniak"
  ]
  node [
    id 72
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 73
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 74
    label "paszcza"
  ]
  node [
    id 75
    label "popapraniec"
  ]
  node [
    id 76
    label "skuba&#263;"
  ]
  node [
    id 77
    label "skubanie"
  ]
  node [
    id 78
    label "agresja"
  ]
  node [
    id 79
    label "skubni&#281;cie"
  ]
  node [
    id 80
    label "zwierz&#281;ta"
  ]
  node [
    id 81
    label "fukni&#281;cie"
  ]
  node [
    id 82
    label "farba"
  ]
  node [
    id 83
    label "fukanie"
  ]
  node [
    id 84
    label "istota_&#380;ywa"
  ]
  node [
    id 85
    label "gad"
  ]
  node [
    id 86
    label "tresowa&#263;"
  ]
  node [
    id 87
    label "siedzie&#263;"
  ]
  node [
    id 88
    label "oswaja&#263;"
  ]
  node [
    id 89
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 90
    label "poligamia"
  ]
  node [
    id 91
    label "oz&#243;r"
  ]
  node [
    id 92
    label "skubn&#261;&#263;"
  ]
  node [
    id 93
    label "wios&#322;owa&#263;"
  ]
  node [
    id 94
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 95
    label "le&#380;enie"
  ]
  node [
    id 96
    label "niecz&#322;owiek"
  ]
  node [
    id 97
    label "wios&#322;owanie"
  ]
  node [
    id 98
    label "napasienie_si&#281;"
  ]
  node [
    id 99
    label "wiwarium"
  ]
  node [
    id 100
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 101
    label "animalista"
  ]
  node [
    id 102
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 103
    label "budowa"
  ]
  node [
    id 104
    label "hodowla"
  ]
  node [
    id 105
    label "pasienie_si&#281;"
  ]
  node [
    id 106
    label "sodomita"
  ]
  node [
    id 107
    label "monogamia"
  ]
  node [
    id 108
    label "przyssawka"
  ]
  node [
    id 109
    label "zachowanie"
  ]
  node [
    id 110
    label "budowa_cia&#322;a"
  ]
  node [
    id 111
    label "okrutnik"
  ]
  node [
    id 112
    label "grzbiet"
  ]
  node [
    id 113
    label "weterynarz"
  ]
  node [
    id 114
    label "&#322;eb"
  ]
  node [
    id 115
    label "wylinka"
  ]
  node [
    id 116
    label "bestia"
  ]
  node [
    id 117
    label "poskramia&#263;"
  ]
  node [
    id 118
    label "fauna"
  ]
  node [
    id 119
    label "treser"
  ]
  node [
    id 120
    label "siedzenie"
  ]
  node [
    id 121
    label "le&#380;e&#263;"
  ]
  node [
    id 122
    label "ludzko&#347;&#263;"
  ]
  node [
    id 123
    label "asymilowanie"
  ]
  node [
    id 124
    label "wapniak"
  ]
  node [
    id 125
    label "asymilowa&#263;"
  ]
  node [
    id 126
    label "os&#322;abia&#263;"
  ]
  node [
    id 127
    label "posta&#263;"
  ]
  node [
    id 128
    label "hominid"
  ]
  node [
    id 129
    label "podw&#322;adny"
  ]
  node [
    id 130
    label "os&#322;abianie"
  ]
  node [
    id 131
    label "g&#322;owa"
  ]
  node [
    id 132
    label "figura"
  ]
  node [
    id 133
    label "portrecista"
  ]
  node [
    id 134
    label "dwun&#243;g"
  ]
  node [
    id 135
    label "profanum"
  ]
  node [
    id 136
    label "mikrokosmos"
  ]
  node [
    id 137
    label "nasada"
  ]
  node [
    id 138
    label "duch"
  ]
  node [
    id 139
    label "antropochoria"
  ]
  node [
    id 140
    label "osoba"
  ]
  node [
    id 141
    label "wz&#243;r"
  ]
  node [
    id 142
    label "senior"
  ]
  node [
    id 143
    label "oddzia&#322;ywanie"
  ]
  node [
    id 144
    label "Adam"
  ]
  node [
    id 145
    label "homo_sapiens"
  ]
  node [
    id 146
    label "polifag"
  ]
  node [
    id 147
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 148
    label "innowierstwo"
  ]
  node [
    id 149
    label "ch&#322;opstwo"
  ]
  node [
    id 150
    label "Brunszwik"
  ]
  node [
    id 151
    label "Twer"
  ]
  node [
    id 152
    label "Marki"
  ]
  node [
    id 153
    label "Tarnopol"
  ]
  node [
    id 154
    label "Czerkiesk"
  ]
  node [
    id 155
    label "Johannesburg"
  ]
  node [
    id 156
    label "Nowogr&#243;d"
  ]
  node [
    id 157
    label "Heidelberg"
  ]
  node [
    id 158
    label "Korsze"
  ]
  node [
    id 159
    label "Chocim"
  ]
  node [
    id 160
    label "Lenzen"
  ]
  node [
    id 161
    label "Bie&#322;gorod"
  ]
  node [
    id 162
    label "Hebron"
  ]
  node [
    id 163
    label "Korynt"
  ]
  node [
    id 164
    label "Pemba"
  ]
  node [
    id 165
    label "Norfolk"
  ]
  node [
    id 166
    label "Tarragona"
  ]
  node [
    id 167
    label "Loreto"
  ]
  node [
    id 168
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 169
    label "Paczk&#243;w"
  ]
  node [
    id 170
    label "Krasnodar"
  ]
  node [
    id 171
    label "Hadziacz"
  ]
  node [
    id 172
    label "Cymlansk"
  ]
  node [
    id 173
    label "Efez"
  ]
  node [
    id 174
    label "Kandahar"
  ]
  node [
    id 175
    label "&#346;wiebodzice"
  ]
  node [
    id 176
    label "Antwerpia"
  ]
  node [
    id 177
    label "Baltimore"
  ]
  node [
    id 178
    label "Eger"
  ]
  node [
    id 179
    label "Cumana"
  ]
  node [
    id 180
    label "Kanton"
  ]
  node [
    id 181
    label "Sarat&#243;w"
  ]
  node [
    id 182
    label "Siena"
  ]
  node [
    id 183
    label "Dubno"
  ]
  node [
    id 184
    label "Tyl&#380;a"
  ]
  node [
    id 185
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 186
    label "Pi&#324;sk"
  ]
  node [
    id 187
    label "Toledo"
  ]
  node [
    id 188
    label "Piza"
  ]
  node [
    id 189
    label "Triest"
  ]
  node [
    id 190
    label "Struga"
  ]
  node [
    id 191
    label "Gettysburg"
  ]
  node [
    id 192
    label "Sierdobsk"
  ]
  node [
    id 193
    label "Xai-Xai"
  ]
  node [
    id 194
    label "Bristol"
  ]
  node [
    id 195
    label "Katania"
  ]
  node [
    id 196
    label "Parma"
  ]
  node [
    id 197
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 198
    label "Dniepropetrowsk"
  ]
  node [
    id 199
    label "Tours"
  ]
  node [
    id 200
    label "Mohylew"
  ]
  node [
    id 201
    label "Suzdal"
  ]
  node [
    id 202
    label "Samara"
  ]
  node [
    id 203
    label "Akerman"
  ]
  node [
    id 204
    label "Szk&#322;&#243;w"
  ]
  node [
    id 205
    label "Chimoio"
  ]
  node [
    id 206
    label "Perm"
  ]
  node [
    id 207
    label "Murma&#324;sk"
  ]
  node [
    id 208
    label "Z&#322;oczew"
  ]
  node [
    id 209
    label "Reda"
  ]
  node [
    id 210
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 211
    label "Aleksandria"
  ]
  node [
    id 212
    label "Kowel"
  ]
  node [
    id 213
    label "Hamburg"
  ]
  node [
    id 214
    label "Rudki"
  ]
  node [
    id 215
    label "O&#322;omuniec"
  ]
  node [
    id 216
    label "Kowno"
  ]
  node [
    id 217
    label "Luksor"
  ]
  node [
    id 218
    label "Cremona"
  ]
  node [
    id 219
    label "Suczawa"
  ]
  node [
    id 220
    label "M&#252;nster"
  ]
  node [
    id 221
    label "Peszawar"
  ]
  node [
    id 222
    label "Los_Angeles"
  ]
  node [
    id 223
    label "Szawle"
  ]
  node [
    id 224
    label "Winnica"
  ]
  node [
    id 225
    label "I&#322;awka"
  ]
  node [
    id 226
    label "Poniatowa"
  ]
  node [
    id 227
    label "Ko&#322;omyja"
  ]
  node [
    id 228
    label "Asy&#380;"
  ]
  node [
    id 229
    label "Tolkmicko"
  ]
  node [
    id 230
    label "Orlean"
  ]
  node [
    id 231
    label "Koper"
  ]
  node [
    id 232
    label "Le&#324;sk"
  ]
  node [
    id 233
    label "Rostock"
  ]
  node [
    id 234
    label "Mantua"
  ]
  node [
    id 235
    label "Barcelona"
  ]
  node [
    id 236
    label "Mo&#347;ciska"
  ]
  node [
    id 237
    label "Koluszki"
  ]
  node [
    id 238
    label "Stalingrad"
  ]
  node [
    id 239
    label "Fergana"
  ]
  node [
    id 240
    label "A&#322;czewsk"
  ]
  node [
    id 241
    label "Kaszyn"
  ]
  node [
    id 242
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 243
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 244
    label "D&#252;sseldorf"
  ]
  node [
    id 245
    label "Mozyrz"
  ]
  node [
    id 246
    label "Syrakuzy"
  ]
  node [
    id 247
    label "Peszt"
  ]
  node [
    id 248
    label "Lichinga"
  ]
  node [
    id 249
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 250
    label "Choroszcz"
  ]
  node [
    id 251
    label "Po&#322;ock"
  ]
  node [
    id 252
    label "Cherso&#324;"
  ]
  node [
    id 253
    label "Fryburg"
  ]
  node [
    id 254
    label "Izmir"
  ]
  node [
    id 255
    label "Jawor&#243;w"
  ]
  node [
    id 256
    label "Wenecja"
  ]
  node [
    id 257
    label "Kordoba"
  ]
  node [
    id 258
    label "Mrocza"
  ]
  node [
    id 259
    label "Solikamsk"
  ]
  node [
    id 260
    label "Be&#322;z"
  ]
  node [
    id 261
    label "Wo&#322;gograd"
  ]
  node [
    id 262
    label "&#379;ar&#243;w"
  ]
  node [
    id 263
    label "Brugia"
  ]
  node [
    id 264
    label "Radk&#243;w"
  ]
  node [
    id 265
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 266
    label "Harbin"
  ]
  node [
    id 267
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 268
    label "Zaporo&#380;e"
  ]
  node [
    id 269
    label "Smorgonie"
  ]
  node [
    id 270
    label "Nowa_D&#281;ba"
  ]
  node [
    id 271
    label "Aktobe"
  ]
  node [
    id 272
    label "Ussuryjsk"
  ]
  node [
    id 273
    label "Mo&#380;ajsk"
  ]
  node [
    id 274
    label "Tanger"
  ]
  node [
    id 275
    label "Nowogard"
  ]
  node [
    id 276
    label "Utrecht"
  ]
  node [
    id 277
    label "Czerniejewo"
  ]
  node [
    id 278
    label "Bazylea"
  ]
  node [
    id 279
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 280
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 281
    label "Tu&#322;a"
  ]
  node [
    id 282
    label "Al-Kufa"
  ]
  node [
    id 283
    label "Jutrosin"
  ]
  node [
    id 284
    label "Czelabi&#324;sk"
  ]
  node [
    id 285
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 286
    label "Split"
  ]
  node [
    id 287
    label "Czerniowce"
  ]
  node [
    id 288
    label "Majsur"
  ]
  node [
    id 289
    label "Poczdam"
  ]
  node [
    id 290
    label "Troick"
  ]
  node [
    id 291
    label "Minusi&#324;sk"
  ]
  node [
    id 292
    label "Kostroma"
  ]
  node [
    id 293
    label "Barwice"
  ]
  node [
    id 294
    label "U&#322;an_Ude"
  ]
  node [
    id 295
    label "Czeskie_Budziejowice"
  ]
  node [
    id 296
    label "Getynga"
  ]
  node [
    id 297
    label "Kercz"
  ]
  node [
    id 298
    label "B&#322;aszki"
  ]
  node [
    id 299
    label "Lipawa"
  ]
  node [
    id 300
    label "Bujnaksk"
  ]
  node [
    id 301
    label "Wittenberga"
  ]
  node [
    id 302
    label "Gorycja"
  ]
  node [
    id 303
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 304
    label "Swatowe"
  ]
  node [
    id 305
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 306
    label "Magadan"
  ]
  node [
    id 307
    label "Rzg&#243;w"
  ]
  node [
    id 308
    label "Bijsk"
  ]
  node [
    id 309
    label "Norylsk"
  ]
  node [
    id 310
    label "Mesyna"
  ]
  node [
    id 311
    label "Berezyna"
  ]
  node [
    id 312
    label "Stawropol"
  ]
  node [
    id 313
    label "Kircholm"
  ]
  node [
    id 314
    label "Hawana"
  ]
  node [
    id 315
    label "Pardubice"
  ]
  node [
    id 316
    label "Drezno"
  ]
  node [
    id 317
    label "Zaklik&#243;w"
  ]
  node [
    id 318
    label "Kozielsk"
  ]
  node [
    id 319
    label "Paw&#322;owo"
  ]
  node [
    id 320
    label "Kani&#243;w"
  ]
  node [
    id 321
    label "Adana"
  ]
  node [
    id 322
    label "Kleczew"
  ]
  node [
    id 323
    label "Rybi&#324;sk"
  ]
  node [
    id 324
    label "Dayton"
  ]
  node [
    id 325
    label "Nowy_Orlean"
  ]
  node [
    id 326
    label "Perejas&#322;aw"
  ]
  node [
    id 327
    label "Jenisejsk"
  ]
  node [
    id 328
    label "Bolonia"
  ]
  node [
    id 329
    label "Bir&#380;e"
  ]
  node [
    id 330
    label "Marsylia"
  ]
  node [
    id 331
    label "Workuta"
  ]
  node [
    id 332
    label "Sewilla"
  ]
  node [
    id 333
    label "Megara"
  ]
  node [
    id 334
    label "Gotha"
  ]
  node [
    id 335
    label "Kiejdany"
  ]
  node [
    id 336
    label "Zaleszczyki"
  ]
  node [
    id 337
    label "Ja&#322;ta"
  ]
  node [
    id 338
    label "Burgas"
  ]
  node [
    id 339
    label "Essen"
  ]
  node [
    id 340
    label "Czadca"
  ]
  node [
    id 341
    label "Manchester"
  ]
  node [
    id 342
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 343
    label "Schmalkalden"
  ]
  node [
    id 344
    label "Oleszyce"
  ]
  node [
    id 345
    label "Kie&#380;mark"
  ]
  node [
    id 346
    label "Kleck"
  ]
  node [
    id 347
    label "Suez"
  ]
  node [
    id 348
    label "Brack"
  ]
  node [
    id 349
    label "Symferopol"
  ]
  node [
    id 350
    label "Michalovce"
  ]
  node [
    id 351
    label "Tambow"
  ]
  node [
    id 352
    label "Turkmenbaszy"
  ]
  node [
    id 353
    label "Bogumin"
  ]
  node [
    id 354
    label "Sambor"
  ]
  node [
    id 355
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 356
    label "Milan&#243;wek"
  ]
  node [
    id 357
    label "Nachiczewan"
  ]
  node [
    id 358
    label "Cluny"
  ]
  node [
    id 359
    label "Stalinogorsk"
  ]
  node [
    id 360
    label "Lipsk"
  ]
  node [
    id 361
    label "Karlsbad"
  ]
  node [
    id 362
    label "Pietrozawodsk"
  ]
  node [
    id 363
    label "Bar"
  ]
  node [
    id 364
    label "Korfant&#243;w"
  ]
  node [
    id 365
    label "Nieftiegorsk"
  ]
  node [
    id 366
    label "Hanower"
  ]
  node [
    id 367
    label "Windawa"
  ]
  node [
    id 368
    label "&#346;niatyn"
  ]
  node [
    id 369
    label "Dalton"
  ]
  node [
    id 370
    label "tramwaj"
  ]
  node [
    id 371
    label "Kaszgar"
  ]
  node [
    id 372
    label "Berdia&#324;sk"
  ]
  node [
    id 373
    label "Koprzywnica"
  ]
  node [
    id 374
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 375
    label "Brno"
  ]
  node [
    id 376
    label "Wia&#378;ma"
  ]
  node [
    id 377
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 378
    label "Starobielsk"
  ]
  node [
    id 379
    label "Ostr&#243;g"
  ]
  node [
    id 380
    label "Oran"
  ]
  node [
    id 381
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 382
    label "Wyszehrad"
  ]
  node [
    id 383
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 384
    label "Trembowla"
  ]
  node [
    id 385
    label "Tobolsk"
  ]
  node [
    id 386
    label "Liberec"
  ]
  node [
    id 387
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 388
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 389
    label "G&#322;uszyca"
  ]
  node [
    id 390
    label "Akwileja"
  ]
  node [
    id 391
    label "Kar&#322;owice"
  ]
  node [
    id 392
    label "Borys&#243;w"
  ]
  node [
    id 393
    label "Stryj"
  ]
  node [
    id 394
    label "Czeski_Cieszyn"
  ]
  node [
    id 395
    label "Rydu&#322;towy"
  ]
  node [
    id 396
    label "Darmstadt"
  ]
  node [
    id 397
    label "Opawa"
  ]
  node [
    id 398
    label "Jerycho"
  ]
  node [
    id 399
    label "&#321;ohojsk"
  ]
  node [
    id 400
    label "Fatima"
  ]
  node [
    id 401
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 402
    label "Sara&#324;sk"
  ]
  node [
    id 403
    label "Lyon"
  ]
  node [
    id 404
    label "Wormacja"
  ]
  node [
    id 405
    label "Perwomajsk"
  ]
  node [
    id 406
    label "Lubeka"
  ]
  node [
    id 407
    label "Sura&#380;"
  ]
  node [
    id 408
    label "Karaganda"
  ]
  node [
    id 409
    label "Nazaret"
  ]
  node [
    id 410
    label "Poniewie&#380;"
  ]
  node [
    id 411
    label "Siewieromorsk"
  ]
  node [
    id 412
    label "Greifswald"
  ]
  node [
    id 413
    label "Trewir"
  ]
  node [
    id 414
    label "Nitra"
  ]
  node [
    id 415
    label "Karwina"
  ]
  node [
    id 416
    label "Houston"
  ]
  node [
    id 417
    label "Demmin"
  ]
  node [
    id 418
    label "Szamocin"
  ]
  node [
    id 419
    label "Kolkata"
  ]
  node [
    id 420
    label "Brasz&#243;w"
  ]
  node [
    id 421
    label "&#321;uck"
  ]
  node [
    id 422
    label "Peczora"
  ]
  node [
    id 423
    label "S&#322;onim"
  ]
  node [
    id 424
    label "Mekka"
  ]
  node [
    id 425
    label "Rzeczyca"
  ]
  node [
    id 426
    label "Konstancja"
  ]
  node [
    id 427
    label "Orenburg"
  ]
  node [
    id 428
    label "Pittsburgh"
  ]
  node [
    id 429
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 430
    label "Barabi&#324;sk"
  ]
  node [
    id 431
    label "Mory&#324;"
  ]
  node [
    id 432
    label "Hallstatt"
  ]
  node [
    id 433
    label "Mannheim"
  ]
  node [
    id 434
    label "Tarent"
  ]
  node [
    id 435
    label "Dortmund"
  ]
  node [
    id 436
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 437
    label "Dodona"
  ]
  node [
    id 438
    label "Trojan"
  ]
  node [
    id 439
    label "Nankin"
  ]
  node [
    id 440
    label "Weimar"
  ]
  node [
    id 441
    label "Brac&#322;aw"
  ]
  node [
    id 442
    label "Izbica_Kujawska"
  ]
  node [
    id 443
    label "Sankt_Florian"
  ]
  node [
    id 444
    label "Pilzno"
  ]
  node [
    id 445
    label "&#321;uga&#324;sk"
  ]
  node [
    id 446
    label "Sewastopol"
  ]
  node [
    id 447
    label "Poczaj&#243;w"
  ]
  node [
    id 448
    label "Pas&#322;&#281;k"
  ]
  node [
    id 449
    label "Sulech&#243;w"
  ]
  node [
    id 450
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 451
    label "ulica"
  ]
  node [
    id 452
    label "Norak"
  ]
  node [
    id 453
    label "Filadelfia"
  ]
  node [
    id 454
    label "Maribor"
  ]
  node [
    id 455
    label "Detroit"
  ]
  node [
    id 456
    label "Bobolice"
  ]
  node [
    id 457
    label "K&#322;odawa"
  ]
  node [
    id 458
    label "Radziech&#243;w"
  ]
  node [
    id 459
    label "Eleusis"
  ]
  node [
    id 460
    label "W&#322;odzimierz"
  ]
  node [
    id 461
    label "Tartu"
  ]
  node [
    id 462
    label "Drohobycz"
  ]
  node [
    id 463
    label "Saloniki"
  ]
  node [
    id 464
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 465
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 466
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 467
    label "Buchara"
  ]
  node [
    id 468
    label "P&#322;owdiw"
  ]
  node [
    id 469
    label "Koszyce"
  ]
  node [
    id 470
    label "Brema"
  ]
  node [
    id 471
    label "Wagram"
  ]
  node [
    id 472
    label "Czarnobyl"
  ]
  node [
    id 473
    label "Brze&#347;&#263;"
  ]
  node [
    id 474
    label "S&#232;vres"
  ]
  node [
    id 475
    label "Dubrownik"
  ]
  node [
    id 476
    label "Grenada"
  ]
  node [
    id 477
    label "Jekaterynburg"
  ]
  node [
    id 478
    label "zabudowa"
  ]
  node [
    id 479
    label "Inhambane"
  ]
  node [
    id 480
    label "Konstantyn&#243;wka"
  ]
  node [
    id 481
    label "Krajowa"
  ]
  node [
    id 482
    label "Norymberga"
  ]
  node [
    id 483
    label "Tarnogr&#243;d"
  ]
  node [
    id 484
    label "Beresteczko"
  ]
  node [
    id 485
    label "Chabarowsk"
  ]
  node [
    id 486
    label "Boden"
  ]
  node [
    id 487
    label "Bamberg"
  ]
  node [
    id 488
    label "Podhajce"
  ]
  node [
    id 489
    label "Lhasa"
  ]
  node [
    id 490
    label "Oszmiana"
  ]
  node [
    id 491
    label "Narbona"
  ]
  node [
    id 492
    label "Carrara"
  ]
  node [
    id 493
    label "Soleczniki"
  ]
  node [
    id 494
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 495
    label "Malin"
  ]
  node [
    id 496
    label "Gandawa"
  ]
  node [
    id 497
    label "burmistrz"
  ]
  node [
    id 498
    label "Lancaster"
  ]
  node [
    id 499
    label "S&#322;uck"
  ]
  node [
    id 500
    label "Kronsztad"
  ]
  node [
    id 501
    label "Mosty"
  ]
  node [
    id 502
    label "Budionnowsk"
  ]
  node [
    id 503
    label "Oksford"
  ]
  node [
    id 504
    label "Awinion"
  ]
  node [
    id 505
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 506
    label "Edynburg"
  ]
  node [
    id 507
    label "Zagorsk"
  ]
  node [
    id 508
    label "Kaspijsk"
  ]
  node [
    id 509
    label "Konotop"
  ]
  node [
    id 510
    label "Nantes"
  ]
  node [
    id 511
    label "Sydney"
  ]
  node [
    id 512
    label "Orsza"
  ]
  node [
    id 513
    label "Krzanowice"
  ]
  node [
    id 514
    label "Tiume&#324;"
  ]
  node [
    id 515
    label "Wyborg"
  ]
  node [
    id 516
    label "Nerczy&#324;sk"
  ]
  node [
    id 517
    label "Rost&#243;w"
  ]
  node [
    id 518
    label "Halicz"
  ]
  node [
    id 519
    label "Sumy"
  ]
  node [
    id 520
    label "Locarno"
  ]
  node [
    id 521
    label "Luboml"
  ]
  node [
    id 522
    label "Mariupol"
  ]
  node [
    id 523
    label "Bras&#322;aw"
  ]
  node [
    id 524
    label "Witnica"
  ]
  node [
    id 525
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 526
    label "Orneta"
  ]
  node [
    id 527
    label "Gr&#243;dek"
  ]
  node [
    id 528
    label "Go&#347;cino"
  ]
  node [
    id 529
    label "Cannes"
  ]
  node [
    id 530
    label "Lw&#243;w"
  ]
  node [
    id 531
    label "Ulm"
  ]
  node [
    id 532
    label "Aczy&#324;sk"
  ]
  node [
    id 533
    label "Stuttgart"
  ]
  node [
    id 534
    label "weduta"
  ]
  node [
    id 535
    label "Borowsk"
  ]
  node [
    id 536
    label "Niko&#322;ajewsk"
  ]
  node [
    id 537
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 538
    label "Worone&#380;"
  ]
  node [
    id 539
    label "Delhi"
  ]
  node [
    id 540
    label "Adrianopol"
  ]
  node [
    id 541
    label "Byczyna"
  ]
  node [
    id 542
    label "Obuch&#243;w"
  ]
  node [
    id 543
    label "Tyraspol"
  ]
  node [
    id 544
    label "Modena"
  ]
  node [
    id 545
    label "Rajgr&#243;d"
  ]
  node [
    id 546
    label "Wo&#322;kowysk"
  ]
  node [
    id 547
    label "&#379;ylina"
  ]
  node [
    id 548
    label "Zurych"
  ]
  node [
    id 549
    label "Vukovar"
  ]
  node [
    id 550
    label "Narwa"
  ]
  node [
    id 551
    label "Neapol"
  ]
  node [
    id 552
    label "Frydek-Mistek"
  ]
  node [
    id 553
    label "W&#322;adywostok"
  ]
  node [
    id 554
    label "Calais"
  ]
  node [
    id 555
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 556
    label "Trydent"
  ]
  node [
    id 557
    label "Magnitogorsk"
  ]
  node [
    id 558
    label "Padwa"
  ]
  node [
    id 559
    label "Isfahan"
  ]
  node [
    id 560
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 561
    label "grupa"
  ]
  node [
    id 562
    label "Marburg"
  ]
  node [
    id 563
    label "Homel"
  ]
  node [
    id 564
    label "Boston"
  ]
  node [
    id 565
    label "W&#252;rzburg"
  ]
  node [
    id 566
    label "Antiochia"
  ]
  node [
    id 567
    label "Wotki&#324;sk"
  ]
  node [
    id 568
    label "A&#322;apajewsk"
  ]
  node [
    id 569
    label "Lejda"
  ]
  node [
    id 570
    label "Nieder_Selters"
  ]
  node [
    id 571
    label "Nicea"
  ]
  node [
    id 572
    label "Dmitrow"
  ]
  node [
    id 573
    label "Taganrog"
  ]
  node [
    id 574
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 575
    label "Nowomoskowsk"
  ]
  node [
    id 576
    label "Koby&#322;ka"
  ]
  node [
    id 577
    label "Iwano-Frankowsk"
  ]
  node [
    id 578
    label "Kis&#322;owodzk"
  ]
  node [
    id 579
    label "Tomsk"
  ]
  node [
    id 580
    label "Ferrara"
  ]
  node [
    id 581
    label "Edam"
  ]
  node [
    id 582
    label "Suworow"
  ]
  node [
    id 583
    label "Turka"
  ]
  node [
    id 584
    label "Aralsk"
  ]
  node [
    id 585
    label "Kobry&#324;"
  ]
  node [
    id 586
    label "Rotterdam"
  ]
  node [
    id 587
    label "Bordeaux"
  ]
  node [
    id 588
    label "L&#252;neburg"
  ]
  node [
    id 589
    label "Akwizgran"
  ]
  node [
    id 590
    label "Liverpool"
  ]
  node [
    id 591
    label "Asuan"
  ]
  node [
    id 592
    label "Bonn"
  ]
  node [
    id 593
    label "Teby"
  ]
  node [
    id 594
    label "Szumsk"
  ]
  node [
    id 595
    label "Ku&#378;nieck"
  ]
  node [
    id 596
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 597
    label "Tyberiada"
  ]
  node [
    id 598
    label "Turkiestan"
  ]
  node [
    id 599
    label "Nanning"
  ]
  node [
    id 600
    label "G&#322;uch&#243;w"
  ]
  node [
    id 601
    label "Bajonna"
  ]
  node [
    id 602
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 603
    label "Orze&#322;"
  ]
  node [
    id 604
    label "Opalenica"
  ]
  node [
    id 605
    label "Buczacz"
  ]
  node [
    id 606
    label "Armenia"
  ]
  node [
    id 607
    label "Nowoku&#378;nieck"
  ]
  node [
    id 608
    label "Wuppertal"
  ]
  node [
    id 609
    label "Wuhan"
  ]
  node [
    id 610
    label "Betlejem"
  ]
  node [
    id 611
    label "Wi&#322;komierz"
  ]
  node [
    id 612
    label "Podiebrady"
  ]
  node [
    id 613
    label "Rawenna"
  ]
  node [
    id 614
    label "Haarlem"
  ]
  node [
    id 615
    label "Woskriesiensk"
  ]
  node [
    id 616
    label "Pyskowice"
  ]
  node [
    id 617
    label "Kilonia"
  ]
  node [
    id 618
    label "Ruciane-Nida"
  ]
  node [
    id 619
    label "Kursk"
  ]
  node [
    id 620
    label "Wolgast"
  ]
  node [
    id 621
    label "Stralsund"
  ]
  node [
    id 622
    label "Sydon"
  ]
  node [
    id 623
    label "Natal"
  ]
  node [
    id 624
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 625
    label "Baranowicze"
  ]
  node [
    id 626
    label "Stara_Zagora"
  ]
  node [
    id 627
    label "Regensburg"
  ]
  node [
    id 628
    label "Kapsztad"
  ]
  node [
    id 629
    label "Kemerowo"
  ]
  node [
    id 630
    label "Mi&#347;nia"
  ]
  node [
    id 631
    label "Stary_Sambor"
  ]
  node [
    id 632
    label "Soligorsk"
  ]
  node [
    id 633
    label "Ostaszk&#243;w"
  ]
  node [
    id 634
    label "T&#322;uszcz"
  ]
  node [
    id 635
    label "Uljanowsk"
  ]
  node [
    id 636
    label "Tuluza"
  ]
  node [
    id 637
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 638
    label "Chicago"
  ]
  node [
    id 639
    label "Kamieniec_Podolski"
  ]
  node [
    id 640
    label "Dijon"
  ]
  node [
    id 641
    label "Siedliszcze"
  ]
  node [
    id 642
    label "Haga"
  ]
  node [
    id 643
    label "Bobrujsk"
  ]
  node [
    id 644
    label "Kokand"
  ]
  node [
    id 645
    label "Windsor"
  ]
  node [
    id 646
    label "Chmielnicki"
  ]
  node [
    id 647
    label "Winchester"
  ]
  node [
    id 648
    label "Bria&#324;sk"
  ]
  node [
    id 649
    label "Uppsala"
  ]
  node [
    id 650
    label "Paw&#322;odar"
  ]
  node [
    id 651
    label "Canterbury"
  ]
  node [
    id 652
    label "Omsk"
  ]
  node [
    id 653
    label "Tyr"
  ]
  node [
    id 654
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 655
    label "Kolonia"
  ]
  node [
    id 656
    label "Nowa_Ruda"
  ]
  node [
    id 657
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 658
    label "Czerkasy"
  ]
  node [
    id 659
    label "Budziszyn"
  ]
  node [
    id 660
    label "Rohatyn"
  ]
  node [
    id 661
    label "Nowogr&#243;dek"
  ]
  node [
    id 662
    label "Buda"
  ]
  node [
    id 663
    label "Zbara&#380;"
  ]
  node [
    id 664
    label "Korzec"
  ]
  node [
    id 665
    label "Medyna"
  ]
  node [
    id 666
    label "Piatigorsk"
  ]
  node [
    id 667
    label "Monako"
  ]
  node [
    id 668
    label "Chark&#243;w"
  ]
  node [
    id 669
    label "Zadar"
  ]
  node [
    id 670
    label "Brandenburg"
  ]
  node [
    id 671
    label "&#379;ytawa"
  ]
  node [
    id 672
    label "Konstantynopol"
  ]
  node [
    id 673
    label "Wismar"
  ]
  node [
    id 674
    label "Wielsk"
  ]
  node [
    id 675
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 676
    label "Genewa"
  ]
  node [
    id 677
    label "Merseburg"
  ]
  node [
    id 678
    label "Lozanna"
  ]
  node [
    id 679
    label "Azow"
  ]
  node [
    id 680
    label "K&#322;ajpeda"
  ]
  node [
    id 681
    label "Angarsk"
  ]
  node [
    id 682
    label "Ostrawa"
  ]
  node [
    id 683
    label "Jastarnia"
  ]
  node [
    id 684
    label "Moguncja"
  ]
  node [
    id 685
    label "Siewsk"
  ]
  node [
    id 686
    label "Pasawa"
  ]
  node [
    id 687
    label "Penza"
  ]
  node [
    id 688
    label "Borys&#322;aw"
  ]
  node [
    id 689
    label "Osaka"
  ]
  node [
    id 690
    label "Eupatoria"
  ]
  node [
    id 691
    label "Kalmar"
  ]
  node [
    id 692
    label "Troki"
  ]
  node [
    id 693
    label "Mosina"
  ]
  node [
    id 694
    label "Orany"
  ]
  node [
    id 695
    label "Zas&#322;aw"
  ]
  node [
    id 696
    label "Dobrodzie&#324;"
  ]
  node [
    id 697
    label "Kars"
  ]
  node [
    id 698
    label "Poprad"
  ]
  node [
    id 699
    label "Sajgon"
  ]
  node [
    id 700
    label "Tulon"
  ]
  node [
    id 701
    label "Kro&#347;niewice"
  ]
  node [
    id 702
    label "Krzywi&#324;"
  ]
  node [
    id 703
    label "Batumi"
  ]
  node [
    id 704
    label "Werona"
  ]
  node [
    id 705
    label "&#379;migr&#243;d"
  ]
  node [
    id 706
    label "Ka&#322;uga"
  ]
  node [
    id 707
    label "Rakoniewice"
  ]
  node [
    id 708
    label "Trabzon"
  ]
  node [
    id 709
    label "Debreczyn"
  ]
  node [
    id 710
    label "Jena"
  ]
  node [
    id 711
    label "Strzelno"
  ]
  node [
    id 712
    label "Gwardiejsk"
  ]
  node [
    id 713
    label "Wersal"
  ]
  node [
    id 714
    label "Bych&#243;w"
  ]
  node [
    id 715
    label "Ba&#322;tijsk"
  ]
  node [
    id 716
    label "Trenczyn"
  ]
  node [
    id 717
    label "Walencja"
  ]
  node [
    id 718
    label "Warna"
  ]
  node [
    id 719
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 720
    label "Huma&#324;"
  ]
  node [
    id 721
    label "Wilejka"
  ]
  node [
    id 722
    label "Ochryda"
  ]
  node [
    id 723
    label "Berdycz&#243;w"
  ]
  node [
    id 724
    label "Krasnogorsk"
  ]
  node [
    id 725
    label "Bogus&#322;aw"
  ]
  node [
    id 726
    label "Trzyniec"
  ]
  node [
    id 727
    label "urz&#261;d"
  ]
  node [
    id 728
    label "Mariampol"
  ]
  node [
    id 729
    label "Ko&#322;omna"
  ]
  node [
    id 730
    label "Chanty-Mansyjsk"
  ]
  node [
    id 731
    label "Piast&#243;w"
  ]
  node [
    id 732
    label "Jastrowie"
  ]
  node [
    id 733
    label "Nampula"
  ]
  node [
    id 734
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 735
    label "Bor"
  ]
  node [
    id 736
    label "Lengyel"
  ]
  node [
    id 737
    label "Lubecz"
  ]
  node [
    id 738
    label "Wierchoja&#324;sk"
  ]
  node [
    id 739
    label "Barczewo"
  ]
  node [
    id 740
    label "Madras"
  ]
  node [
    id 741
    label "stanowisko"
  ]
  node [
    id 742
    label "position"
  ]
  node [
    id 743
    label "instytucja"
  ]
  node [
    id 744
    label "siedziba"
  ]
  node [
    id 745
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 746
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 747
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 748
    label "mianowaniec"
  ]
  node [
    id 749
    label "dzia&#322;"
  ]
  node [
    id 750
    label "okienko"
  ]
  node [
    id 751
    label "w&#322;adza"
  ]
  node [
    id 752
    label "odm&#322;adzanie"
  ]
  node [
    id 753
    label "liga"
  ]
  node [
    id 754
    label "jednostka_systematyczna"
  ]
  node [
    id 755
    label "gromada"
  ]
  node [
    id 756
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 757
    label "egzemplarz"
  ]
  node [
    id 758
    label "Entuzjastki"
  ]
  node [
    id 759
    label "zbi&#243;r"
  ]
  node [
    id 760
    label "kompozycja"
  ]
  node [
    id 761
    label "Terranie"
  ]
  node [
    id 762
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 763
    label "category"
  ]
  node [
    id 764
    label "pakiet_klimatyczny"
  ]
  node [
    id 765
    label "oddzia&#322;"
  ]
  node [
    id 766
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 767
    label "cz&#261;steczka"
  ]
  node [
    id 768
    label "stage_set"
  ]
  node [
    id 769
    label "type"
  ]
  node [
    id 770
    label "specgrupa"
  ]
  node [
    id 771
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 772
    label "&#346;wietliki"
  ]
  node [
    id 773
    label "odm&#322;odzenie"
  ]
  node [
    id 774
    label "Eurogrupa"
  ]
  node [
    id 775
    label "odm&#322;adza&#263;"
  ]
  node [
    id 776
    label "formacja_geologiczna"
  ]
  node [
    id 777
    label "harcerze_starsi"
  ]
  node [
    id 778
    label "Aurignac"
  ]
  node [
    id 779
    label "Sabaudia"
  ]
  node [
    id 780
    label "Cecora"
  ]
  node [
    id 781
    label "Saint-Acheul"
  ]
  node [
    id 782
    label "Boulogne"
  ]
  node [
    id 783
    label "Opat&#243;wek"
  ]
  node [
    id 784
    label "osiedle"
  ]
  node [
    id 785
    label "Levallois-Perret"
  ]
  node [
    id 786
    label "kompleks"
  ]
  node [
    id 787
    label "wyposa&#380;enie_wn&#281;trz"
  ]
  node [
    id 788
    label "droga"
  ]
  node [
    id 789
    label "korona_drogi"
  ]
  node [
    id 790
    label "pas_rozdzielczy"
  ]
  node [
    id 791
    label "&#347;rodowisko"
  ]
  node [
    id 792
    label "streetball"
  ]
  node [
    id 793
    label "miasteczko"
  ]
  node [
    id 794
    label "pas_ruchu"
  ]
  node [
    id 795
    label "chodnik"
  ]
  node [
    id 796
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 797
    label "pierzeja"
  ]
  node [
    id 798
    label "wysepka"
  ]
  node [
    id 799
    label "arteria"
  ]
  node [
    id 800
    label "Broadway"
  ]
  node [
    id 801
    label "autostrada"
  ]
  node [
    id 802
    label "jezdnia"
  ]
  node [
    id 803
    label "Brenna"
  ]
  node [
    id 804
    label "Szwajcaria"
  ]
  node [
    id 805
    label "Rosja"
  ]
  node [
    id 806
    label "archidiecezja"
  ]
  node [
    id 807
    label "wirus"
  ]
  node [
    id 808
    label "gor&#261;czka_krwotoczna"
  ]
  node [
    id 809
    label "filowirusy"
  ]
  node [
    id 810
    label "Niemcy"
  ]
  node [
    id 811
    label "Swierd&#322;owsk"
  ]
  node [
    id 812
    label "Skierniewice"
  ]
  node [
    id 813
    label "Monaster"
  ]
  node [
    id 814
    label "edam"
  ]
  node [
    id 815
    label "mury_Jerycha"
  ]
  node [
    id 816
    label "Mozambik"
  ]
  node [
    id 817
    label "Francja"
  ]
  node [
    id 818
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 819
    label "dram"
  ]
  node [
    id 820
    label "Dunajec"
  ]
  node [
    id 821
    label "Tatry"
  ]
  node [
    id 822
    label "S&#261;decczyzna"
  ]
  node [
    id 823
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 824
    label "Stanis&#322;aw&#243;w"
  ]
  node [
    id 825
    label "Budapeszt"
  ]
  node [
    id 826
    label "Ukraina"
  ]
  node [
    id 827
    label "Dzikie_Pola"
  ]
  node [
    id 828
    label "Sicz"
  ]
  node [
    id 829
    label "Psie_Pole"
  ]
  node [
    id 830
    label "Frysztat"
  ]
  node [
    id 831
    label "Azerbejd&#380;an"
  ]
  node [
    id 832
    label "Prusy"
  ]
  node [
    id 833
    label "Budionowsk"
  ]
  node [
    id 834
    label "woda_kolo&#324;ska"
  ]
  node [
    id 835
    label "The_Beatles"
  ]
  node [
    id 836
    label "harcerstwo"
  ]
  node [
    id 837
    label "frank_monakijski"
  ]
  node [
    id 838
    label "euro"
  ]
  node [
    id 839
    label "&#321;otwa"
  ]
  node [
    id 840
    label "Litwa"
  ]
  node [
    id 841
    label "Hiszpania"
  ]
  node [
    id 842
    label "Stambu&#322;"
  ]
  node [
    id 843
    label "Bizancjum"
  ]
  node [
    id 844
    label "Kalinin"
  ]
  node [
    id 845
    label "&#321;yczak&#243;w"
  ]
  node [
    id 846
    label "obraz"
  ]
  node [
    id 847
    label "dzie&#322;o"
  ]
  node [
    id 848
    label "wagon"
  ]
  node [
    id 849
    label "bimba"
  ]
  node [
    id 850
    label "pojazd_szynowy"
  ]
  node [
    id 851
    label "odbierak"
  ]
  node [
    id 852
    label "&#347;rodek_transportu_publicznego"
  ]
  node [
    id 853
    label "samorz&#261;dowiec"
  ]
  node [
    id 854
    label "ceklarz"
  ]
  node [
    id 855
    label "burmistrzyna"
  ]
  node [
    id 856
    label "wsp&#243;lnota"
  ]
  node [
    id 857
    label "jednostka"
  ]
  node [
    id 858
    label "przyswoi&#263;"
  ]
  node [
    id 859
    label "one"
  ]
  node [
    id 860
    label "poj&#281;cie"
  ]
  node [
    id 861
    label "ewoluowanie"
  ]
  node [
    id 862
    label "supremum"
  ]
  node [
    id 863
    label "skala"
  ]
  node [
    id 864
    label "przyswajanie"
  ]
  node [
    id 865
    label "wyewoluowanie"
  ]
  node [
    id 866
    label "reakcja"
  ]
  node [
    id 867
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 868
    label "przeliczy&#263;"
  ]
  node [
    id 869
    label "wyewoluowa&#263;"
  ]
  node [
    id 870
    label "ewoluowa&#263;"
  ]
  node [
    id 871
    label "matematyka"
  ]
  node [
    id 872
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 873
    label "rzut"
  ]
  node [
    id 874
    label "liczba_naturalna"
  ]
  node [
    id 875
    label "czynnik_biotyczny"
  ]
  node [
    id 876
    label "individual"
  ]
  node [
    id 877
    label "obiekt"
  ]
  node [
    id 878
    label "przyswaja&#263;"
  ]
  node [
    id 879
    label "przyswojenie"
  ]
  node [
    id 880
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 881
    label "starzenie_si&#281;"
  ]
  node [
    id 882
    label "przeliczanie"
  ]
  node [
    id 883
    label "funkcja"
  ]
  node [
    id 884
    label "przelicza&#263;"
  ]
  node [
    id 885
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 886
    label "infimum"
  ]
  node [
    id 887
    label "przeliczenie"
  ]
  node [
    id 888
    label "zwi&#261;zanie"
  ]
  node [
    id 889
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 890
    label "podobie&#324;stwo"
  ]
  node [
    id 891
    label "Skandynawia"
  ]
  node [
    id 892
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 893
    label "partnership"
  ]
  node [
    id 894
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 895
    label "wi&#261;zanie"
  ]
  node [
    id 896
    label "Ba&#322;kany"
  ]
  node [
    id 897
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 898
    label "society"
  ]
  node [
    id 899
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 900
    label "bratnia_dusza"
  ]
  node [
    id 901
    label "zwi&#261;zek"
  ]
  node [
    id 902
    label "zwi&#261;za&#263;"
  ]
  node [
    id 903
    label "marriage"
  ]
  node [
    id 904
    label "jednostka_organizacyjna"
  ]
  node [
    id 905
    label "struktura"
  ]
  node [
    id 906
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 907
    label "TOPR"
  ]
  node [
    id 908
    label "endecki"
  ]
  node [
    id 909
    label "zesp&#243;&#322;"
  ]
  node [
    id 910
    label "przedstawicielstwo"
  ]
  node [
    id 911
    label "od&#322;am"
  ]
  node [
    id 912
    label "Cepelia"
  ]
  node [
    id 913
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 914
    label "ZBoWiD"
  ]
  node [
    id 915
    label "organization"
  ]
  node [
    id 916
    label "centrala"
  ]
  node [
    id 917
    label "GOPR"
  ]
  node [
    id 918
    label "ZOMO"
  ]
  node [
    id 919
    label "ZMP"
  ]
  node [
    id 920
    label "komitet_koordynacyjny"
  ]
  node [
    id 921
    label "przybud&#243;wka"
  ]
  node [
    id 922
    label "boj&#243;wka"
  ]
  node [
    id 923
    label "mechanika"
  ]
  node [
    id 924
    label "o&#347;"
  ]
  node [
    id 925
    label "usenet"
  ]
  node [
    id 926
    label "rozprz&#261;c"
  ]
  node [
    id 927
    label "cybernetyk"
  ]
  node [
    id 928
    label "podsystem"
  ]
  node [
    id 929
    label "system"
  ]
  node [
    id 930
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 931
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 932
    label "sk&#322;ad"
  ]
  node [
    id 933
    label "systemat"
  ]
  node [
    id 934
    label "konstrukcja"
  ]
  node [
    id 935
    label "konstelacja"
  ]
  node [
    id 936
    label "Mazowsze"
  ]
  node [
    id 937
    label "whole"
  ]
  node [
    id 938
    label "skupienie"
  ]
  node [
    id 939
    label "zabudowania"
  ]
  node [
    id 940
    label "group"
  ]
  node [
    id 941
    label "zespolik"
  ]
  node [
    id 942
    label "schorzenie"
  ]
  node [
    id 943
    label "ro&#347;lina"
  ]
  node [
    id 944
    label "Depeche_Mode"
  ]
  node [
    id 945
    label "batch"
  ]
  node [
    id 946
    label "ajencja"
  ]
  node [
    id 947
    label "agencja"
  ]
  node [
    id 948
    label "bank"
  ]
  node [
    id 949
    label "filia"
  ]
  node [
    id 950
    label "kawa&#322;"
  ]
  node [
    id 951
    label "bry&#322;a"
  ]
  node [
    id 952
    label "fragment"
  ]
  node [
    id 953
    label "struktura_geologiczna"
  ]
  node [
    id 954
    label "section"
  ]
  node [
    id 955
    label "budynek"
  ]
  node [
    id 956
    label "b&#281;ben_wielki"
  ]
  node [
    id 957
    label "Bruksela"
  ]
  node [
    id 958
    label "administration"
  ]
  node [
    id 959
    label "miejsce"
  ]
  node [
    id 960
    label "zarz&#261;d"
  ]
  node [
    id 961
    label "stopa"
  ]
  node [
    id 962
    label "o&#347;rodek"
  ]
  node [
    id 963
    label "urz&#261;dzenie"
  ]
  node [
    id 964
    label "ratownictwo"
  ]
  node [
    id 965
    label "milicja_obywatelska"
  ]
  node [
    id 966
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 967
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 968
    label "byt"
  ]
  node [
    id 969
    label "osobowo&#347;&#263;"
  ]
  node [
    id 970
    label "prawo"
  ]
  node [
    id 971
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 972
    label "nauka_prawa"
  ]
  node [
    id 973
    label "granica"
  ]
  node [
    id 974
    label "sfera"
  ]
  node [
    id 975
    label "wielko&#347;&#263;"
  ]
  node [
    id 976
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 977
    label "podzakres"
  ]
  node [
    id 978
    label "dziedzina"
  ]
  node [
    id 979
    label "desygnat"
  ]
  node [
    id 980
    label "circle"
  ]
  node [
    id 981
    label "rozmiar"
  ]
  node [
    id 982
    label "zasi&#261;g"
  ]
  node [
    id 983
    label "izochronizm"
  ]
  node [
    id 984
    label "bridge"
  ]
  node [
    id 985
    label "distribution"
  ]
  node [
    id 986
    label "warunek_lokalowy"
  ]
  node [
    id 987
    label "liczba"
  ]
  node [
    id 988
    label "rzadko&#347;&#263;"
  ]
  node [
    id 989
    label "zaleta"
  ]
  node [
    id 990
    label "ilo&#347;&#263;"
  ]
  node [
    id 991
    label "measure"
  ]
  node [
    id 992
    label "znaczenie"
  ]
  node [
    id 993
    label "opinia"
  ]
  node [
    id 994
    label "dymensja"
  ]
  node [
    id 995
    label "zdolno&#347;&#263;"
  ]
  node [
    id 996
    label "potencja"
  ]
  node [
    id 997
    label "property"
  ]
  node [
    id 998
    label "series"
  ]
  node [
    id 999
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1000
    label "uprawianie"
  ]
  node [
    id 1001
    label "praca_rolnicza"
  ]
  node [
    id 1002
    label "collection"
  ]
  node [
    id 1003
    label "dane"
  ]
  node [
    id 1004
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1005
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1006
    label "sum"
  ]
  node [
    id 1007
    label "gathering"
  ]
  node [
    id 1008
    label "album"
  ]
  node [
    id 1009
    label "przej&#347;cie"
  ]
  node [
    id 1010
    label "kres"
  ]
  node [
    id 1011
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1012
    label "Ural"
  ]
  node [
    id 1013
    label "miara"
  ]
  node [
    id 1014
    label "end"
  ]
  node [
    id 1015
    label "pu&#322;ap"
  ]
  node [
    id 1016
    label "koniec"
  ]
  node [
    id 1017
    label "granice"
  ]
  node [
    id 1018
    label "frontier"
  ]
  node [
    id 1019
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1020
    label "bezdro&#380;e"
  ]
  node [
    id 1021
    label "poddzia&#322;"
  ]
  node [
    id 1022
    label "wymiar"
  ]
  node [
    id 1023
    label "strefa"
  ]
  node [
    id 1024
    label "kula"
  ]
  node [
    id 1025
    label "class"
  ]
  node [
    id 1026
    label "sector"
  ]
  node [
    id 1027
    label "przestrze&#324;"
  ]
  node [
    id 1028
    label "p&#243;&#322;kula"
  ]
  node [
    id 1029
    label "huczek"
  ]
  node [
    id 1030
    label "p&#243;&#322;sfera"
  ]
  node [
    id 1031
    label "powierzchnia"
  ]
  node [
    id 1032
    label "kolur"
  ]
  node [
    id 1033
    label "odpowiednik"
  ]
  node [
    id 1034
    label "designatum"
  ]
  node [
    id 1035
    label "nazwa_rzetelna"
  ]
  node [
    id 1036
    label "nazwa_pozorna"
  ]
  node [
    id 1037
    label "denotacja"
  ]
  node [
    id 1038
    label "sterowa&#263;"
  ]
  node [
    id 1039
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1040
    label "manipulate"
  ]
  node [
    id 1041
    label "zwierzchnik"
  ]
  node [
    id 1042
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1043
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1044
    label "ustawia&#263;"
  ]
  node [
    id 1045
    label "give"
  ]
  node [
    id 1046
    label "przeznacza&#263;"
  ]
  node [
    id 1047
    label "control"
  ]
  node [
    id 1048
    label "match"
  ]
  node [
    id 1049
    label "motywowa&#263;"
  ]
  node [
    id 1050
    label "administrowa&#263;"
  ]
  node [
    id 1051
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 1052
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1053
    label "order"
  ]
  node [
    id 1054
    label "indicate"
  ]
  node [
    id 1055
    label "trzyma&#263;"
  ]
  node [
    id 1056
    label "dysponowa&#263;"
  ]
  node [
    id 1057
    label "manipulowa&#263;"
  ]
  node [
    id 1058
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 1059
    label "robi&#263;"
  ]
  node [
    id 1060
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 1061
    label "ustala&#263;"
  ]
  node [
    id 1062
    label "nadawa&#263;"
  ]
  node [
    id 1063
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 1064
    label "peddle"
  ]
  node [
    id 1065
    label "go"
  ]
  node [
    id 1066
    label "porz&#261;dkowa&#263;"
  ]
  node [
    id 1067
    label "decydowa&#263;"
  ]
  node [
    id 1068
    label "umieszcza&#263;"
  ]
  node [
    id 1069
    label "wskazywa&#263;"
  ]
  node [
    id 1070
    label "zabezpiecza&#263;"
  ]
  node [
    id 1071
    label "train"
  ]
  node [
    id 1072
    label "poprawia&#263;"
  ]
  node [
    id 1073
    label "nak&#322;ania&#263;"
  ]
  node [
    id 1074
    label "range"
  ]
  node [
    id 1075
    label "powodowa&#263;"
  ]
  node [
    id 1076
    label "wyznacza&#263;"
  ]
  node [
    id 1077
    label "przyznawa&#263;"
  ]
  node [
    id 1078
    label "przekazywa&#263;"
  ]
  node [
    id 1079
    label "dispatch"
  ]
  node [
    id 1080
    label "wytwarza&#263;"
  ]
  node [
    id 1081
    label "nakazywa&#263;"
  ]
  node [
    id 1082
    label "grant"
  ]
  node [
    id 1083
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1084
    label "explain"
  ]
  node [
    id 1085
    label "pobudza&#263;"
  ]
  node [
    id 1086
    label "determine"
  ]
  node [
    id 1087
    label "work"
  ]
  node [
    id 1088
    label "reakcja_chemiczna"
  ]
  node [
    id 1089
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1090
    label "poleca&#263;"
  ]
  node [
    id 1091
    label "sprawowa&#263;"
  ]
  node [
    id 1092
    label "pryncypa&#322;"
  ]
  node [
    id 1093
    label "kierownictwo"
  ]
  node [
    id 1094
    label "klawisz"
  ]
  node [
    id 1095
    label "odznaka"
  ]
  node [
    id 1096
    label "kawaler"
  ]
  node [
    id 1097
    label "berylowiec"
  ]
  node [
    id 1098
    label "content"
  ]
  node [
    id 1099
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 1100
    label "jednostka_promieniowania"
  ]
  node [
    id 1101
    label "zadowolenie_si&#281;"
  ]
  node [
    id 1102
    label "miliradian"
  ]
  node [
    id 1103
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 1104
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 1105
    label "mikroradian"
  ]
  node [
    id 1106
    label "metal"
  ]
  node [
    id 1107
    label "nanoradian"
  ]
  node [
    id 1108
    label "radian"
  ]
  node [
    id 1109
    label "zadowolony"
  ]
  node [
    id 1110
    label "weso&#322;y"
  ]
  node [
    id 1111
    label "pismo"
  ]
  node [
    id 1112
    label "prayer"
  ]
  node [
    id 1113
    label "twierdzenie"
  ]
  node [
    id 1114
    label "propozycja"
  ]
  node [
    id 1115
    label "my&#347;l"
  ]
  node [
    id 1116
    label "motion"
  ]
  node [
    id 1117
    label "wnioskowanie"
  ]
  node [
    id 1118
    label "s&#261;d"
  ]
  node [
    id 1119
    label "szko&#322;a"
  ]
  node [
    id 1120
    label "p&#322;&#243;d"
  ]
  node [
    id 1121
    label "thinking"
  ]
  node [
    id 1122
    label "umys&#322;"
  ]
  node [
    id 1123
    label "political_orientation"
  ]
  node [
    id 1124
    label "istota"
  ]
  node [
    id 1125
    label "pomys&#322;"
  ]
  node [
    id 1126
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 1127
    label "idea"
  ]
  node [
    id 1128
    label "fantomatyka"
  ]
  node [
    id 1129
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 1130
    label "alternatywa_Fredholma"
  ]
  node [
    id 1131
    label "oznajmianie"
  ]
  node [
    id 1132
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 1133
    label "teoria"
  ]
  node [
    id 1134
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 1135
    label "paradoks_Leontiefa"
  ]
  node [
    id 1136
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 1137
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 1138
    label "teza"
  ]
  node [
    id 1139
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 1140
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 1141
    label "twierdzenie_Pettisa"
  ]
  node [
    id 1142
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 1143
    label "twierdzenie_Maya"
  ]
  node [
    id 1144
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 1145
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 1146
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 1147
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 1148
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 1149
    label "zapewnianie"
  ]
  node [
    id 1150
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 1151
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 1152
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 1153
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 1154
    label "twierdzenie_Stokesa"
  ]
  node [
    id 1155
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 1156
    label "twierdzenie_Cevy"
  ]
  node [
    id 1157
    label "twierdzenie_Pascala"
  ]
  node [
    id 1158
    label "proposition"
  ]
  node [
    id 1159
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 1160
    label "komunikowanie"
  ]
  node [
    id 1161
    label "zasada"
  ]
  node [
    id 1162
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 1163
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 1164
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 1165
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 1166
    label "psychotest"
  ]
  node [
    id 1167
    label "wk&#322;ad"
  ]
  node [
    id 1168
    label "handwriting"
  ]
  node [
    id 1169
    label "przekaz"
  ]
  node [
    id 1170
    label "paleograf"
  ]
  node [
    id 1171
    label "interpunkcja"
  ]
  node [
    id 1172
    label "grafia"
  ]
  node [
    id 1173
    label "communication"
  ]
  node [
    id 1174
    label "script"
  ]
  node [
    id 1175
    label "zajawka"
  ]
  node [
    id 1176
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1177
    label "list"
  ]
  node [
    id 1178
    label "adres"
  ]
  node [
    id 1179
    label "Zwrotnica"
  ]
  node [
    id 1180
    label "czasopismo"
  ]
  node [
    id 1181
    label "ok&#322;adka"
  ]
  node [
    id 1182
    label "ortografia"
  ]
  node [
    id 1183
    label "letter"
  ]
  node [
    id 1184
    label "komunikacja"
  ]
  node [
    id 1185
    label "paleografia"
  ]
  node [
    id 1186
    label "j&#281;zyk"
  ]
  node [
    id 1187
    label "prasa"
  ]
  node [
    id 1188
    label "proposal"
  ]
  node [
    id 1189
    label "proszenie"
  ]
  node [
    id 1190
    label "dochodzenie"
  ]
  node [
    id 1191
    label "proces_my&#347;lowy"
  ]
  node [
    id 1192
    label "lead"
  ]
  node [
    id 1193
    label "konkluzja"
  ]
  node [
    id 1194
    label "sk&#322;adanie"
  ]
  node [
    id 1195
    label "przes&#322;anka"
  ]
  node [
    id 1196
    label "podnosi&#263;"
  ]
  node [
    id 1197
    label "draw"
  ]
  node [
    id 1198
    label "drive"
  ]
  node [
    id 1199
    label "zmienia&#263;"
  ]
  node [
    id 1200
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 1201
    label "rise"
  ]
  node [
    id 1202
    label "admit"
  ]
  node [
    id 1203
    label "reagowa&#263;"
  ]
  node [
    id 1204
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 1205
    label "zaczyna&#263;"
  ]
  node [
    id 1206
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 1207
    label "escalate"
  ]
  node [
    id 1208
    label "pia&#263;"
  ]
  node [
    id 1209
    label "raise"
  ]
  node [
    id 1210
    label "przybli&#380;a&#263;"
  ]
  node [
    id 1211
    label "ulepsza&#263;"
  ]
  node [
    id 1212
    label "tire"
  ]
  node [
    id 1213
    label "pomaga&#263;"
  ]
  node [
    id 1214
    label "liczy&#263;"
  ]
  node [
    id 1215
    label "express"
  ]
  node [
    id 1216
    label "przemieszcza&#263;"
  ]
  node [
    id 1217
    label "chwali&#263;"
  ]
  node [
    id 1218
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 1219
    label "os&#322;awia&#263;"
  ]
  node [
    id 1220
    label "odbudowywa&#263;"
  ]
  node [
    id 1221
    label "enhance"
  ]
  node [
    id 1222
    label "za&#322;apywa&#263;"
  ]
  node [
    id 1223
    label "lift"
  ]
  node [
    id 1224
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 1225
    label "react"
  ]
  node [
    id 1226
    label "answer"
  ]
  node [
    id 1227
    label "odpowiada&#263;"
  ]
  node [
    id 1228
    label "uczestniczy&#263;"
  ]
  node [
    id 1229
    label "organizowa&#263;"
  ]
  node [
    id 1230
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 1231
    label "czyni&#263;"
  ]
  node [
    id 1232
    label "stylizowa&#263;"
  ]
  node [
    id 1233
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 1234
    label "falowa&#263;"
  ]
  node [
    id 1235
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 1236
    label "praca"
  ]
  node [
    id 1237
    label "wydala&#263;"
  ]
  node [
    id 1238
    label "tentegowa&#263;"
  ]
  node [
    id 1239
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 1240
    label "urz&#261;dza&#263;"
  ]
  node [
    id 1241
    label "oszukiwa&#263;"
  ]
  node [
    id 1242
    label "ukazywa&#263;"
  ]
  node [
    id 1243
    label "przerabia&#263;"
  ]
  node [
    id 1244
    label "act"
  ]
  node [
    id 1245
    label "post&#281;powa&#263;"
  ]
  node [
    id 1246
    label "traci&#263;"
  ]
  node [
    id 1247
    label "alternate"
  ]
  node [
    id 1248
    label "change"
  ]
  node [
    id 1249
    label "reengineering"
  ]
  node [
    id 1250
    label "zast&#281;powa&#263;"
  ]
  node [
    id 1251
    label "sprawia&#263;"
  ]
  node [
    id 1252
    label "zyskiwa&#263;"
  ]
  node [
    id 1253
    label "przechodzi&#263;"
  ]
  node [
    id 1254
    label "resolution"
  ]
  node [
    id 1255
    label "akt"
  ]
  node [
    id 1256
    label "podnieci&#263;"
  ]
  node [
    id 1257
    label "scena"
  ]
  node [
    id 1258
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 1259
    label "numer"
  ]
  node [
    id 1260
    label "po&#380;ycie"
  ]
  node [
    id 1261
    label "podniecenie"
  ]
  node [
    id 1262
    label "nago&#347;&#263;"
  ]
  node [
    id 1263
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 1264
    label "seks"
  ]
  node [
    id 1265
    label "podniecanie"
  ]
  node [
    id 1266
    label "imisja"
  ]
  node [
    id 1267
    label "zwyczaj"
  ]
  node [
    id 1268
    label "rozmna&#380;anie"
  ]
  node [
    id 1269
    label "ruch_frykcyjny"
  ]
  node [
    id 1270
    label "ontologia"
  ]
  node [
    id 1271
    label "na_pieska"
  ]
  node [
    id 1272
    label "pozycja_misjonarska"
  ]
  node [
    id 1273
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1274
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 1275
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1276
    label "gra_wst&#281;pna"
  ]
  node [
    id 1277
    label "erotyka"
  ]
  node [
    id 1278
    label "urzeczywistnienie"
  ]
  node [
    id 1279
    label "baraszki"
  ]
  node [
    id 1280
    label "certificate"
  ]
  node [
    id 1281
    label "po&#380;&#261;danie"
  ]
  node [
    id 1282
    label "wzw&#243;d"
  ]
  node [
    id 1283
    label "arystotelizm"
  ]
  node [
    id 1284
    label "podnieca&#263;"
  ]
  node [
    id 1285
    label "zboczenie"
  ]
  node [
    id 1286
    label "om&#243;wienie"
  ]
  node [
    id 1287
    label "sponiewieranie"
  ]
  node [
    id 1288
    label "discipline"
  ]
  node [
    id 1289
    label "rzecz"
  ]
  node [
    id 1290
    label "omawia&#263;"
  ]
  node [
    id 1291
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1292
    label "tre&#347;&#263;"
  ]
  node [
    id 1293
    label "robienie"
  ]
  node [
    id 1294
    label "sponiewiera&#263;"
  ]
  node [
    id 1295
    label "element"
  ]
  node [
    id 1296
    label "entity"
  ]
  node [
    id 1297
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1298
    label "tematyka"
  ]
  node [
    id 1299
    label "w&#261;tek"
  ]
  node [
    id 1300
    label "charakter"
  ]
  node [
    id 1301
    label "zbaczanie"
  ]
  node [
    id 1302
    label "program_nauczania"
  ]
  node [
    id 1303
    label "om&#243;wi&#263;"
  ]
  node [
    id 1304
    label "omawianie"
  ]
  node [
    id 1305
    label "thing"
  ]
  node [
    id 1306
    label "kultura"
  ]
  node [
    id 1307
    label "zbacza&#263;"
  ]
  node [
    id 1308
    label "zboczy&#263;"
  ]
  node [
    id 1309
    label "temat"
  ]
  node [
    id 1310
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 1311
    label "informacja"
  ]
  node [
    id 1312
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1313
    label "object"
  ]
  node [
    id 1314
    label "wpadni&#281;cie"
  ]
  node [
    id 1315
    label "mienie"
  ]
  node [
    id 1316
    label "przyroda"
  ]
  node [
    id 1317
    label "wpa&#347;&#263;"
  ]
  node [
    id 1318
    label "wpadanie"
  ]
  node [
    id 1319
    label "wpada&#263;"
  ]
  node [
    id 1320
    label "discussion"
  ]
  node [
    id 1321
    label "rozpatrywanie"
  ]
  node [
    id 1322
    label "dyskutowanie"
  ]
  node [
    id 1323
    label "swerve"
  ]
  node [
    id 1324
    label "kierunek"
  ]
  node [
    id 1325
    label "digress"
  ]
  node [
    id 1326
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1327
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 1328
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 1329
    label "odchodzi&#263;"
  ]
  node [
    id 1330
    label "twist"
  ]
  node [
    id 1331
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1332
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 1333
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 1334
    label "omowny"
  ]
  node [
    id 1335
    label "figura_stylistyczna"
  ]
  node [
    id 1336
    label "sformu&#322;owanie"
  ]
  node [
    id 1337
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1338
    label "odchodzenie"
  ]
  node [
    id 1339
    label "aberrance"
  ]
  node [
    id 1340
    label "dyskutowa&#263;"
  ]
  node [
    id 1341
    label "formu&#322;owa&#263;"
  ]
  node [
    id 1342
    label "discourse"
  ]
  node [
    id 1343
    label "perversion"
  ]
  node [
    id 1344
    label "death"
  ]
  node [
    id 1345
    label "odej&#347;cie"
  ]
  node [
    id 1346
    label "turn"
  ]
  node [
    id 1347
    label "k&#261;t"
  ]
  node [
    id 1348
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1349
    label "odchylenie_si&#281;"
  ]
  node [
    id 1350
    label "deviation"
  ]
  node [
    id 1351
    label "patologia"
  ]
  node [
    id 1352
    label "przedyskutowa&#263;"
  ]
  node [
    id 1353
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 1354
    label "publicize"
  ]
  node [
    id 1355
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 1356
    label "distract"
  ]
  node [
    id 1357
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1358
    label "odej&#347;&#263;"
  ]
  node [
    id 1359
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 1360
    label "zmieni&#263;"
  ]
  node [
    id 1361
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1362
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 1363
    label "kontrolowa&#263;"
  ]
  node [
    id 1364
    label "carry"
  ]
  node [
    id 1365
    label "sok"
  ]
  node [
    id 1366
    label "krew"
  ]
  node [
    id 1367
    label "wheel"
  ]
  node [
    id 1368
    label "zniszczenie"
  ]
  node [
    id 1369
    label "rotation"
  ]
  node [
    id 1370
    label "obieg"
  ]
  node [
    id 1371
    label "kontrolowanie"
  ]
  node [
    id 1372
    label "patrol"
  ]
  node [
    id 1373
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 1374
    label "lap"
  ]
  node [
    id 1375
    label "upi&#263;"
  ]
  node [
    id 1376
    label "zm&#281;czy&#263;"
  ]
  node [
    id 1377
    label "zniszczy&#263;"
  ]
  node [
    id 1378
    label "fabrication"
  ]
  node [
    id 1379
    label "porobienie"
  ]
  node [
    id 1380
    label "bycie"
  ]
  node [
    id 1381
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 1382
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 1383
    label "creation"
  ]
  node [
    id 1384
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 1385
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 1386
    label "tentegowanie"
  ]
  node [
    id 1387
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1388
    label "psychika"
  ]
  node [
    id 1389
    label "kompleksja"
  ]
  node [
    id 1390
    label "fizjonomia"
  ]
  node [
    id 1391
    label "zjawisko"
  ]
  node [
    id 1392
    label "asymilowanie_si&#281;"
  ]
  node [
    id 1393
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1394
    label "Wsch&#243;d"
  ]
  node [
    id 1395
    label "przejmowanie"
  ]
  node [
    id 1396
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1397
    label "makrokosmos"
  ]
  node [
    id 1398
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 1399
    label "konwencja"
  ]
  node [
    id 1400
    label "propriety"
  ]
  node [
    id 1401
    label "przejmowa&#263;"
  ]
  node [
    id 1402
    label "brzoskwiniarnia"
  ]
  node [
    id 1403
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1404
    label "sztuka"
  ]
  node [
    id 1405
    label "jako&#347;&#263;"
  ]
  node [
    id 1406
    label "kuchnia"
  ]
  node [
    id 1407
    label "tradycja"
  ]
  node [
    id 1408
    label "populace"
  ]
  node [
    id 1409
    label "religia"
  ]
  node [
    id 1410
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 1411
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 1412
    label "przej&#281;cie"
  ]
  node [
    id 1413
    label "przej&#261;&#263;"
  ]
  node [
    id 1414
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 1415
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 1416
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 1417
    label "forum"
  ]
  node [
    id 1418
    label "matter"
  ]
  node [
    id 1419
    label "topik"
  ]
  node [
    id 1420
    label "splot"
  ]
  node [
    id 1421
    label "ceg&#322;a"
  ]
  node [
    id 1422
    label "socket"
  ]
  node [
    id 1423
    label "rozmieszczenie"
  ]
  node [
    id 1424
    label "fabu&#322;a"
  ]
  node [
    id 1425
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1426
    label "superego"
  ]
  node [
    id 1427
    label "wn&#281;trze"
  ]
  node [
    id 1428
    label "r&#243;&#380;niczka"
  ]
  node [
    id 1429
    label "materia"
  ]
  node [
    id 1430
    label "szambo"
  ]
  node [
    id 1431
    label "aspo&#322;eczny"
  ]
  node [
    id 1432
    label "component"
  ]
  node [
    id 1433
    label "szkodnik"
  ]
  node [
    id 1434
    label "gangsterski"
  ]
  node [
    id 1435
    label "underworld"
  ]
  node [
    id 1436
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 1437
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 1438
    label "uprzedzi&#263;"
  ]
  node [
    id 1439
    label "wym&#243;wi&#263;"
  ]
  node [
    id 1440
    label "condition"
  ]
  node [
    id 1441
    label "zapewni&#263;"
  ]
  node [
    id 1442
    label "zrobi&#263;"
  ]
  node [
    id 1443
    label "poinformowa&#263;"
  ]
  node [
    id 1444
    label "og&#322;osi&#263;"
  ]
  node [
    id 1445
    label "overwhelm"
  ]
  node [
    id 1446
    label "announce"
  ]
  node [
    id 1447
    label "translate"
  ]
  node [
    id 1448
    label "spowodowa&#263;"
  ]
  node [
    id 1449
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 1450
    label "wydoby&#263;"
  ]
  node [
    id 1451
    label "reproach"
  ]
  node [
    id 1452
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1453
    label "rozwi&#261;za&#263;"
  ]
  node [
    id 1454
    label "zwerbalizowa&#263;"
  ]
  node [
    id 1455
    label "denounce"
  ]
  node [
    id 1456
    label "warto&#347;&#263;"
  ]
  node [
    id 1457
    label "charakterystyka"
  ]
  node [
    id 1458
    label "feature"
  ]
  node [
    id 1459
    label "wyregulowanie"
  ]
  node [
    id 1460
    label "kompetencja"
  ]
  node [
    id 1461
    label "wyregulowa&#263;"
  ]
  node [
    id 1462
    label "regulowanie"
  ]
  node [
    id 1463
    label "attribute"
  ]
  node [
    id 1464
    label "standard"
  ]
  node [
    id 1465
    label "gestia"
  ]
  node [
    id 1466
    label "sprawno&#347;&#263;"
  ]
  node [
    id 1467
    label "znawstwo"
  ]
  node [
    id 1468
    label "authority"
  ]
  node [
    id 1469
    label "zrewaluowa&#263;"
  ]
  node [
    id 1470
    label "rewaluowanie"
  ]
  node [
    id 1471
    label "korzy&#347;&#263;"
  ]
  node [
    id 1472
    label "strona"
  ]
  node [
    id 1473
    label "rewaluowa&#263;"
  ]
  node [
    id 1474
    label "wabik"
  ]
  node [
    id 1475
    label "zrewaluowanie"
  ]
  node [
    id 1476
    label "m&#322;ot"
  ]
  node [
    id 1477
    label "znak"
  ]
  node [
    id 1478
    label "drzewo"
  ]
  node [
    id 1479
    label "pr&#243;ba"
  ]
  node [
    id 1480
    label "marka"
  ]
  node [
    id 1481
    label "model"
  ]
  node [
    id 1482
    label "ordinariness"
  ]
  node [
    id 1483
    label "zorganizowa&#263;"
  ]
  node [
    id 1484
    label "taniec_towarzyski"
  ]
  node [
    id 1485
    label "organizowanie"
  ]
  node [
    id 1486
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 1487
    label "criterion"
  ]
  node [
    id 1488
    label "zorganizowanie"
  ]
  node [
    id 1489
    label "zmienna"
  ]
  node [
    id 1490
    label "wskazywanie"
  ]
  node [
    id 1491
    label "cel"
  ]
  node [
    id 1492
    label "worth"
  ]
  node [
    id 1493
    label "opis"
  ]
  node [
    id 1494
    label "parametr"
  ]
  node [
    id 1495
    label "analiza"
  ]
  node [
    id 1496
    label "specyfikacja"
  ]
  node [
    id 1497
    label "wykres"
  ]
  node [
    id 1498
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 1499
    label "interpretacja"
  ]
  node [
    id 1500
    label "nastawi&#263;"
  ]
  node [
    id 1501
    label "ulepszy&#263;"
  ]
  node [
    id 1502
    label "ustawi&#263;"
  ]
  node [
    id 1503
    label "proces_fizjologiczny"
  ]
  node [
    id 1504
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 1505
    label "usprawni&#263;"
  ]
  node [
    id 1506
    label "align"
  ]
  node [
    id 1507
    label "op&#322;aca&#263;"
  ]
  node [
    id 1508
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1509
    label "tune"
  ]
  node [
    id 1510
    label "nastawia&#263;"
  ]
  node [
    id 1511
    label "usprawnia&#263;"
  ]
  node [
    id 1512
    label "normalize"
  ]
  node [
    id 1513
    label "kszta&#322;towanie"
  ]
  node [
    id 1514
    label "alteration"
  ]
  node [
    id 1515
    label "nastawianie"
  ]
  node [
    id 1516
    label "op&#322;acanie"
  ]
  node [
    id 1517
    label "ustawianie"
  ]
  node [
    id 1518
    label "usprawnianie"
  ]
  node [
    id 1519
    label "ulepszanie"
  ]
  node [
    id 1520
    label "standardization"
  ]
  node [
    id 1521
    label "ustawienie"
  ]
  node [
    id 1522
    label "usprawnienie"
  ]
  node [
    id 1523
    label "nastawienie"
  ]
  node [
    id 1524
    label "ulepszenie"
  ]
  node [
    id 1525
    label "ukszta&#322;towanie"
  ]
  node [
    id 1526
    label "adjustment"
  ]
  node [
    id 1527
    label "kolejny"
  ]
  node [
    id 1528
    label "osobno"
  ]
  node [
    id 1529
    label "r&#243;&#380;ny"
  ]
  node [
    id 1530
    label "inszy"
  ]
  node [
    id 1531
    label "inaczej"
  ]
  node [
    id 1532
    label "odr&#281;bny"
  ]
  node [
    id 1533
    label "nast&#281;pnie"
  ]
  node [
    id 1534
    label "nastopny"
  ]
  node [
    id 1535
    label "kolejno"
  ]
  node [
    id 1536
    label "kt&#243;ry&#347;"
  ]
  node [
    id 1537
    label "jaki&#347;"
  ]
  node [
    id 1538
    label "r&#243;&#380;nie"
  ]
  node [
    id 1539
    label "niestandardowo"
  ]
  node [
    id 1540
    label "individually"
  ]
  node [
    id 1541
    label "udzielnie"
  ]
  node [
    id 1542
    label "osobnie"
  ]
  node [
    id 1543
    label "odr&#281;bnie"
  ]
  node [
    id 1544
    label "osobny"
  ]
  node [
    id 1545
    label "tkanka"
  ]
  node [
    id 1546
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1547
    label "tw&#243;r"
  ]
  node [
    id 1548
    label "organogeneza"
  ]
  node [
    id 1549
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 1550
    label "struktura_anatomiczna"
  ]
  node [
    id 1551
    label "uk&#322;ad"
  ]
  node [
    id 1552
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 1553
    label "dekortykacja"
  ]
  node [
    id 1554
    label "Izba_Konsyliarska"
  ]
  node [
    id 1555
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1556
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1557
    label "stomia"
  ]
  node [
    id 1558
    label "okolica"
  ]
  node [
    id 1559
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1560
    label "Rzym_Zachodni"
  ]
  node [
    id 1561
    label "Rzym_Wschodni"
  ]
  node [
    id 1562
    label "odwarstwi&#263;"
  ]
  node [
    id 1563
    label "tissue"
  ]
  node [
    id 1564
    label "histochemia"
  ]
  node [
    id 1565
    label "zserowacenie"
  ]
  node [
    id 1566
    label "kom&#243;rka"
  ]
  node [
    id 1567
    label "wapnienie"
  ]
  node [
    id 1568
    label "wapnie&#263;"
  ]
  node [
    id 1569
    label "odwarstwia&#263;"
  ]
  node [
    id 1570
    label "trofika"
  ]
  node [
    id 1571
    label "element_anatomiczny"
  ]
  node [
    id 1572
    label "zserowacie&#263;"
  ]
  node [
    id 1573
    label "badanie_histopatologiczne"
  ]
  node [
    id 1574
    label "oddychanie_tkankowe"
  ]
  node [
    id 1575
    label "odwarstwia&#263;_si&#281;"
  ]
  node [
    id 1576
    label "odwarstwi&#263;_si&#281;"
  ]
  node [
    id 1577
    label "serowacie&#263;"
  ]
  node [
    id 1578
    label "serowacenie"
  ]
  node [
    id 1579
    label "organizm"
  ]
  node [
    id 1580
    label "part"
  ]
  node [
    id 1581
    label "substance"
  ]
  node [
    id 1582
    label "cia&#322;o"
  ]
  node [
    id 1583
    label "rezultat"
  ]
  node [
    id 1584
    label "treaty"
  ]
  node [
    id 1585
    label "umowa"
  ]
  node [
    id 1586
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1587
    label "przestawi&#263;"
  ]
  node [
    id 1588
    label "alliance"
  ]
  node [
    id 1589
    label "ONZ"
  ]
  node [
    id 1590
    label "NATO"
  ]
  node [
    id 1591
    label "zawarcie"
  ]
  node [
    id 1592
    label "zawrze&#263;"
  ]
  node [
    id 1593
    label "wi&#281;&#378;"
  ]
  node [
    id 1594
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 1595
    label "traktat_wersalski"
  ]
  node [
    id 1596
    label "krajobraz"
  ]
  node [
    id 1597
    label "obszar"
  ]
  node [
    id 1598
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 1599
    label "po_s&#261;siedzku"
  ]
  node [
    id 1600
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 1601
    label "miejsce_pracy"
  ]
  node [
    id 1602
    label "kreacja"
  ]
  node [
    id 1603
    label "r&#243;w"
  ]
  node [
    id 1604
    label "posesja"
  ]
  node [
    id 1605
    label "wjazd"
  ]
  node [
    id 1606
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 1607
    label "constitution"
  ]
  node [
    id 1608
    label "proces_biologiczny"
  ]
  node [
    id 1609
    label "operacja"
  ]
  node [
    id 1610
    label "anastomoza_chirurgiczna"
  ]
  node [
    id 1611
    label "&#322;odyga"
  ]
  node [
    id 1612
    label "ziarno"
  ]
  node [
    id 1613
    label "zdejmowanie"
  ]
  node [
    id 1614
    label "usuwanie"
  ]
  node [
    id 1615
    label "utrzymywanie"
  ]
  node [
    id 1616
    label "subsystencja"
  ]
  node [
    id 1617
    label "utrzyma&#263;"
  ]
  node [
    id 1618
    label "egzystencja"
  ]
  node [
    id 1619
    label "wy&#380;ywienie"
  ]
  node [
    id 1620
    label "ontologicznie"
  ]
  node [
    id 1621
    label "utrzymanie"
  ]
  node [
    id 1622
    label "utrzymywa&#263;"
  ]
  node [
    id 1623
    label "wyj&#261;tkowy"
  ]
  node [
    id 1624
    label "self"
  ]
  node [
    id 1625
    label "status"
  ]
  node [
    id 1626
    label "umocowa&#263;"
  ]
  node [
    id 1627
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 1628
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1629
    label "procesualistyka"
  ]
  node [
    id 1630
    label "regu&#322;a_Allena"
  ]
  node [
    id 1631
    label "kryminalistyka"
  ]
  node [
    id 1632
    label "zasada_d'Alemberta"
  ]
  node [
    id 1633
    label "obserwacja"
  ]
  node [
    id 1634
    label "normatywizm"
  ]
  node [
    id 1635
    label "jurisprudence"
  ]
  node [
    id 1636
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 1637
    label "kultura_duchowa"
  ]
  node [
    id 1638
    label "przepis"
  ]
  node [
    id 1639
    label "prawo_karne_procesowe"
  ]
  node [
    id 1640
    label "kazuistyka"
  ]
  node [
    id 1641
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 1642
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 1643
    label "kryminologia"
  ]
  node [
    id 1644
    label "regu&#322;a_Glogera"
  ]
  node [
    id 1645
    label "prawo_Mendla"
  ]
  node [
    id 1646
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 1647
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 1648
    label "prawo_karne"
  ]
  node [
    id 1649
    label "legislacyjnie"
  ]
  node [
    id 1650
    label "cywilistyka"
  ]
  node [
    id 1651
    label "judykatura"
  ]
  node [
    id 1652
    label "kanonistyka"
  ]
  node [
    id 1653
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 1654
    label "qualification"
  ]
  node [
    id 1655
    label "dominion"
  ]
  node [
    id 1656
    label "wykonawczy"
  ]
  node [
    id 1657
    label "normalizacja"
  ]
  node [
    id 1658
    label "p&#322;aci&#263;"
  ]
  node [
    id 1659
    label "finance"
  ]
  node [
    id 1660
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1661
    label "rationalize"
  ]
  node [
    id 1662
    label "dostosowywa&#263;"
  ]
  node [
    id 1663
    label "shape"
  ]
  node [
    id 1664
    label "wstrzeliwa&#263;_si&#281;"
  ]
  node [
    id 1665
    label "better"
  ]
  node [
    id 1666
    label "pobudowa&#263;"
  ]
  node [
    id 1667
    label "z&#322;amanie"
  ]
  node [
    id 1668
    label "set"
  ]
  node [
    id 1669
    label "poumieszcza&#263;"
  ]
  node [
    id 1670
    label "narobi&#263;"
  ]
  node [
    id 1671
    label "sk&#322;ada&#263;"
  ]
  node [
    id 1672
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 1673
    label "marshal"
  ]
  node [
    id 1674
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 1675
    label "deliver"
  ]
  node [
    id 1676
    label "predispose"
  ]
  node [
    id 1677
    label "powyznacza&#263;"
  ]
  node [
    id 1678
    label "ranek"
  ]
  node [
    id 1679
    label "doba"
  ]
  node [
    id 1680
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 1681
    label "noc"
  ]
  node [
    id 1682
    label "podwiecz&#243;r"
  ]
  node [
    id 1683
    label "po&#322;udnie"
  ]
  node [
    id 1684
    label "godzina"
  ]
  node [
    id 1685
    label "przedpo&#322;udnie"
  ]
  node [
    id 1686
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 1687
    label "long_time"
  ]
  node [
    id 1688
    label "wiecz&#243;r"
  ]
  node [
    id 1689
    label "t&#322;usty_czwartek"
  ]
  node [
    id 1690
    label "popo&#322;udnie"
  ]
  node [
    id 1691
    label "walentynki"
  ]
  node [
    id 1692
    label "czynienie_si&#281;"
  ]
  node [
    id 1693
    label "s&#322;o&#324;ce"
  ]
  node [
    id 1694
    label "rano"
  ]
  node [
    id 1695
    label "tydzie&#324;"
  ]
  node [
    id 1696
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 1697
    label "wzej&#347;cie"
  ]
  node [
    id 1698
    label "czas"
  ]
  node [
    id 1699
    label "wsta&#263;"
  ]
  node [
    id 1700
    label "day"
  ]
  node [
    id 1701
    label "termin"
  ]
  node [
    id 1702
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 1703
    label "wstanie"
  ]
  node [
    id 1704
    label "przedwiecz&#243;r"
  ]
  node [
    id 1705
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 1706
    label "Sylwester"
  ]
  node [
    id 1707
    label "poprzedzanie"
  ]
  node [
    id 1708
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1709
    label "laba"
  ]
  node [
    id 1710
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1711
    label "chronometria"
  ]
  node [
    id 1712
    label "rachuba_czasu"
  ]
  node [
    id 1713
    label "przep&#322;ywanie"
  ]
  node [
    id 1714
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1715
    label "czasokres"
  ]
  node [
    id 1716
    label "odczyt"
  ]
  node [
    id 1717
    label "chwila"
  ]
  node [
    id 1718
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1719
    label "dzieje"
  ]
  node [
    id 1720
    label "kategoria_gramatyczna"
  ]
  node [
    id 1721
    label "poprzedzenie"
  ]
  node [
    id 1722
    label "trawienie"
  ]
  node [
    id 1723
    label "pochodzi&#263;"
  ]
  node [
    id 1724
    label "period"
  ]
  node [
    id 1725
    label "okres_czasu"
  ]
  node [
    id 1726
    label "poprzedza&#263;"
  ]
  node [
    id 1727
    label "schy&#322;ek"
  ]
  node [
    id 1728
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1729
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1730
    label "zegar"
  ]
  node [
    id 1731
    label "czwarty_wymiar"
  ]
  node [
    id 1732
    label "pochodzenie"
  ]
  node [
    id 1733
    label "koniugacja"
  ]
  node [
    id 1734
    label "Zeitgeist"
  ]
  node [
    id 1735
    label "trawi&#263;"
  ]
  node [
    id 1736
    label "pogoda"
  ]
  node [
    id 1737
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1738
    label "poprzedzi&#263;"
  ]
  node [
    id 1739
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1740
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1741
    label "time_period"
  ]
  node [
    id 1742
    label "nazewnictwo"
  ]
  node [
    id 1743
    label "term"
  ]
  node [
    id 1744
    label "przypadni&#281;cie"
  ]
  node [
    id 1745
    label "ekspiracja"
  ]
  node [
    id 1746
    label "przypa&#347;&#263;"
  ]
  node [
    id 1747
    label "chronogram"
  ]
  node [
    id 1748
    label "praktyka"
  ]
  node [
    id 1749
    label "nazwa"
  ]
  node [
    id 1750
    label "odwieczerz"
  ]
  node [
    id 1751
    label "pora"
  ]
  node [
    id 1752
    label "przyj&#281;cie"
  ]
  node [
    id 1753
    label "spotkanie"
  ]
  node [
    id 1754
    label "night"
  ]
  node [
    id 1755
    label "zach&#243;d"
  ]
  node [
    id 1756
    label "vesper"
  ]
  node [
    id 1757
    label "aurora"
  ]
  node [
    id 1758
    label "wsch&#243;d"
  ]
  node [
    id 1759
    label "&#347;rodek"
  ]
  node [
    id 1760
    label "Ziemia"
  ]
  node [
    id 1761
    label "dwunasta"
  ]
  node [
    id 1762
    label "strona_&#347;wiata"
  ]
  node [
    id 1763
    label "dopo&#322;udnie"
  ]
  node [
    id 1764
    label "blady_&#347;wit"
  ]
  node [
    id 1765
    label "podkurek"
  ]
  node [
    id 1766
    label "time"
  ]
  node [
    id 1767
    label "p&#243;&#322;godzina"
  ]
  node [
    id 1768
    label "jednostka_czasu"
  ]
  node [
    id 1769
    label "minuta"
  ]
  node [
    id 1770
    label "kwadrans"
  ]
  node [
    id 1771
    label "p&#243;&#322;noc"
  ]
  node [
    id 1772
    label "nokturn"
  ]
  node [
    id 1773
    label "jednostka_geologiczna"
  ]
  node [
    id 1774
    label "weekend"
  ]
  node [
    id 1775
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 1776
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 1777
    label "miesi&#261;c"
  ]
  node [
    id 1778
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 1779
    label "mount"
  ]
  node [
    id 1780
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 1781
    label "wzej&#347;&#263;"
  ]
  node [
    id 1782
    label "ascend"
  ]
  node [
    id 1783
    label "kuca&#263;"
  ]
  node [
    id 1784
    label "wyzdrowie&#263;"
  ]
  node [
    id 1785
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1786
    label "arise"
  ]
  node [
    id 1787
    label "stan&#261;&#263;"
  ]
  node [
    id 1788
    label "przesta&#263;"
  ]
  node [
    id 1789
    label "wyzdrowienie"
  ]
  node [
    id 1790
    label "kl&#281;czenie"
  ]
  node [
    id 1791
    label "opuszczenie"
  ]
  node [
    id 1792
    label "uniesienie_si&#281;"
  ]
  node [
    id 1793
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1794
    label "beginning"
  ]
  node [
    id 1795
    label "przestanie"
  ]
  node [
    id 1796
    label "S&#322;o&#324;ce"
  ]
  node [
    id 1797
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 1798
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1799
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 1800
    label "kochanie"
  ]
  node [
    id 1801
    label "sunlight"
  ]
  node [
    id 1802
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 1803
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 1804
    label "grudzie&#324;"
  ]
  node [
    id 1805
    label "Matka_Boska_Gromniczna"
  ]
  node [
    id 1806
    label "miech"
  ]
  node [
    id 1807
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 1808
    label "rok"
  ]
  node [
    id 1809
    label "kalendy"
  ]
  node [
    id 1810
    label "formacja"
  ]
  node [
    id 1811
    label "yearbook"
  ]
  node [
    id 1812
    label "kronika"
  ]
  node [
    id 1813
    label "Bund"
  ]
  node [
    id 1814
    label "PPR"
  ]
  node [
    id 1815
    label "Jakobici"
  ]
  node [
    id 1816
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1817
    label "leksem"
  ]
  node [
    id 1818
    label "SLD"
  ]
  node [
    id 1819
    label "Razem"
  ]
  node [
    id 1820
    label "PiS"
  ]
  node [
    id 1821
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1822
    label "partia"
  ]
  node [
    id 1823
    label "Kuomintang"
  ]
  node [
    id 1824
    label "ZSL"
  ]
  node [
    id 1825
    label "proces"
  ]
  node [
    id 1826
    label "rugby"
  ]
  node [
    id 1827
    label "AWS"
  ]
  node [
    id 1828
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1829
    label "blok"
  ]
  node [
    id 1830
    label "PO"
  ]
  node [
    id 1831
    label "si&#322;a"
  ]
  node [
    id 1832
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1833
    label "Federali&#347;ci"
  ]
  node [
    id 1834
    label "PSL"
  ]
  node [
    id 1835
    label "wojsko"
  ]
  node [
    id 1836
    label "Wigowie"
  ]
  node [
    id 1837
    label "ZChN"
  ]
  node [
    id 1838
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 1839
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1840
    label "unit"
  ]
  node [
    id 1841
    label "forma"
  ]
  node [
    id 1842
    label "chronograf"
  ]
  node [
    id 1843
    label "latopis"
  ]
  node [
    id 1844
    label "ksi&#281;ga"
  ]
  node [
    id 1845
    label "kognicja"
  ]
  node [
    id 1846
    label "rozprawa"
  ]
  node [
    id 1847
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1848
    label "przebiec"
  ]
  node [
    id 1849
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1850
    label "motyw"
  ]
  node [
    id 1851
    label "przebiegni&#281;cie"
  ]
  node [
    id 1852
    label "ideologia"
  ]
  node [
    id 1853
    label "intelekt"
  ]
  node [
    id 1854
    label "Kant"
  ]
  node [
    id 1855
    label "ideacja"
  ]
  node [
    id 1856
    label "rozumowanie"
  ]
  node [
    id 1857
    label "opracowanie"
  ]
  node [
    id 1858
    label "obrady"
  ]
  node [
    id 1859
    label "cytat"
  ]
  node [
    id 1860
    label "tekst"
  ]
  node [
    id 1861
    label "obja&#347;nienie"
  ]
  node [
    id 1862
    label "s&#261;dzenie"
  ]
  node [
    id 1863
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 1864
    label "niuansowa&#263;"
  ]
  node [
    id 1865
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 1866
    label "sk&#322;adnik"
  ]
  node [
    id 1867
    label "zniuansowa&#263;"
  ]
  node [
    id 1868
    label "fakt"
  ]
  node [
    id 1869
    label "przyczyna"
  ]
  node [
    id 1870
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 1871
    label "wyraz_pochodny"
  ]
  node [
    id 1872
    label "fraza"
  ]
  node [
    id 1873
    label "melodia"
  ]
  node [
    id 1874
    label "otoczka"
  ]
  node [
    id 1875
    label "ustanowi&#263;"
  ]
  node [
    id 1876
    label "resoluteness"
  ]
  node [
    id 1877
    label "wskaza&#263;"
  ]
  node [
    id 1878
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1879
    label "ustali&#263;"
  ]
  node [
    id 1880
    label "install"
  ]
  node [
    id 1881
    label "situate"
  ]
  node [
    id 1882
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 1883
    label "do_p&#243;&#378;na"
  ]
  node [
    id 1884
    label "p&#243;&#378;no"
  ]
  node [
    id 1885
    label "rewizja"
  ]
  node [
    id 1886
    label "passage"
  ]
  node [
    id 1887
    label "oznaka"
  ]
  node [
    id 1888
    label "ferment"
  ]
  node [
    id 1889
    label "komplet"
  ]
  node [
    id 1890
    label "anatomopatolog"
  ]
  node [
    id 1891
    label "zmianka"
  ]
  node [
    id 1892
    label "amendment"
  ]
  node [
    id 1893
    label "odmienianie"
  ]
  node [
    id 1894
    label "tura"
  ]
  node [
    id 1895
    label "boski"
  ]
  node [
    id 1896
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1897
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1898
    label "przywidzenie"
  ]
  node [
    id 1899
    label "presence"
  ]
  node [
    id 1900
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1901
    label "lekcja"
  ]
  node [
    id 1902
    label "ensemble"
  ]
  node [
    id 1903
    label "klasa"
  ]
  node [
    id 1904
    label "zestaw"
  ]
  node [
    id 1905
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1906
    label "implikowa&#263;"
  ]
  node [
    id 1907
    label "signal"
  ]
  node [
    id 1908
    label "symbol"
  ]
  node [
    id 1909
    label "bia&#322;ko"
  ]
  node [
    id 1910
    label "immobilizowa&#263;"
  ]
  node [
    id 1911
    label "poruszenie"
  ]
  node [
    id 1912
    label "immobilizacja"
  ]
  node [
    id 1913
    label "apoenzym"
  ]
  node [
    id 1914
    label "zymaza"
  ]
  node [
    id 1915
    label "enzyme"
  ]
  node [
    id 1916
    label "immobilizowanie"
  ]
  node [
    id 1917
    label "biokatalizator"
  ]
  node [
    id 1918
    label "dow&#243;d"
  ]
  node [
    id 1919
    label "krytyka"
  ]
  node [
    id 1920
    label "rekurs"
  ]
  node [
    id 1921
    label "checkup"
  ]
  node [
    id 1922
    label "kontrola"
  ]
  node [
    id 1923
    label "odwo&#322;anie"
  ]
  node [
    id 1924
    label "correction"
  ]
  node [
    id 1925
    label "przegl&#261;d"
  ]
  node [
    id 1926
    label "kipisz"
  ]
  node [
    id 1927
    label "korekta"
  ]
  node [
    id 1928
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1929
    label "najem"
  ]
  node [
    id 1930
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 1931
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 1932
    label "zak&#322;ad"
  ]
  node [
    id 1933
    label "stosunek_pracy"
  ]
  node [
    id 1934
    label "benedykty&#324;ski"
  ]
  node [
    id 1935
    label "poda&#380;_pracy"
  ]
  node [
    id 1936
    label "pracowanie"
  ]
  node [
    id 1937
    label "tyrka"
  ]
  node [
    id 1938
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 1939
    label "zaw&#243;d"
  ]
  node [
    id 1940
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1941
    label "tynkarski"
  ]
  node [
    id 1942
    label "pracowa&#263;"
  ]
  node [
    id 1943
    label "czynnik_produkcji"
  ]
  node [
    id 1944
    label "zobowi&#261;zanie"
  ]
  node [
    id 1945
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 1946
    label "patolog"
  ]
  node [
    id 1947
    label "anatom"
  ]
  node [
    id 1948
    label "sparafrazowanie"
  ]
  node [
    id 1949
    label "zmienianie"
  ]
  node [
    id 1950
    label "parafrazowanie"
  ]
  node [
    id 1951
    label "zamiana"
  ]
  node [
    id 1952
    label "wymienianie"
  ]
  node [
    id 1953
    label "Transfiguration"
  ]
  node [
    id 1954
    label "przeobra&#380;anie_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 2
    target 520
  ]
  edge [
    source 2
    target 521
  ]
  edge [
    source 2
    target 522
  ]
  edge [
    source 2
    target 523
  ]
  edge [
    source 2
    target 524
  ]
  edge [
    source 2
    target 525
  ]
  edge [
    source 2
    target 526
  ]
  edge [
    source 2
    target 527
  ]
  edge [
    source 2
    target 528
  ]
  edge [
    source 2
    target 529
  ]
  edge [
    source 2
    target 530
  ]
  edge [
    source 2
    target 531
  ]
  edge [
    source 2
    target 532
  ]
  edge [
    source 2
    target 533
  ]
  edge [
    source 2
    target 534
  ]
  edge [
    source 2
    target 535
  ]
  edge [
    source 2
    target 536
  ]
  edge [
    source 2
    target 537
  ]
  edge [
    source 2
    target 538
  ]
  edge [
    source 2
    target 539
  ]
  edge [
    source 2
    target 540
  ]
  edge [
    source 2
    target 541
  ]
  edge [
    source 2
    target 542
  ]
  edge [
    source 2
    target 543
  ]
  edge [
    source 2
    target 544
  ]
  edge [
    source 2
    target 545
  ]
  edge [
    source 2
    target 546
  ]
  edge [
    source 2
    target 547
  ]
  edge [
    source 2
    target 548
  ]
  edge [
    source 2
    target 549
  ]
  edge [
    source 2
    target 550
  ]
  edge [
    source 2
    target 551
  ]
  edge [
    source 2
    target 552
  ]
  edge [
    source 2
    target 553
  ]
  edge [
    source 2
    target 554
  ]
  edge [
    source 2
    target 555
  ]
  edge [
    source 2
    target 556
  ]
  edge [
    source 2
    target 557
  ]
  edge [
    source 2
    target 558
  ]
  edge [
    source 2
    target 559
  ]
  edge [
    source 2
    target 560
  ]
  edge [
    source 2
    target 561
  ]
  edge [
    source 2
    target 562
  ]
  edge [
    source 2
    target 563
  ]
  edge [
    source 2
    target 564
  ]
  edge [
    source 2
    target 565
  ]
  edge [
    source 2
    target 566
  ]
  edge [
    source 2
    target 567
  ]
  edge [
    source 2
    target 568
  ]
  edge [
    source 2
    target 569
  ]
  edge [
    source 2
    target 570
  ]
  edge [
    source 2
    target 571
  ]
  edge [
    source 2
    target 572
  ]
  edge [
    source 2
    target 573
  ]
  edge [
    source 2
    target 574
  ]
  edge [
    source 2
    target 575
  ]
  edge [
    source 2
    target 576
  ]
  edge [
    source 2
    target 577
  ]
  edge [
    source 2
    target 578
  ]
  edge [
    source 2
    target 579
  ]
  edge [
    source 2
    target 580
  ]
  edge [
    source 2
    target 581
  ]
  edge [
    source 2
    target 582
  ]
  edge [
    source 2
    target 583
  ]
  edge [
    source 2
    target 584
  ]
  edge [
    source 2
    target 585
  ]
  edge [
    source 2
    target 586
  ]
  edge [
    source 2
    target 587
  ]
  edge [
    source 2
    target 588
  ]
  edge [
    source 2
    target 589
  ]
  edge [
    source 2
    target 590
  ]
  edge [
    source 2
    target 591
  ]
  edge [
    source 2
    target 592
  ]
  edge [
    source 2
    target 593
  ]
  edge [
    source 2
    target 594
  ]
  edge [
    source 2
    target 595
  ]
  edge [
    source 2
    target 596
  ]
  edge [
    source 2
    target 597
  ]
  edge [
    source 2
    target 598
  ]
  edge [
    source 2
    target 599
  ]
  edge [
    source 2
    target 600
  ]
  edge [
    source 2
    target 601
  ]
  edge [
    source 2
    target 602
  ]
  edge [
    source 2
    target 603
  ]
  edge [
    source 2
    target 604
  ]
  edge [
    source 2
    target 605
  ]
  edge [
    source 2
    target 606
  ]
  edge [
    source 2
    target 607
  ]
  edge [
    source 2
    target 608
  ]
  edge [
    source 2
    target 609
  ]
  edge [
    source 2
    target 610
  ]
  edge [
    source 2
    target 611
  ]
  edge [
    source 2
    target 612
  ]
  edge [
    source 2
    target 613
  ]
  edge [
    source 2
    target 614
  ]
  edge [
    source 2
    target 615
  ]
  edge [
    source 2
    target 616
  ]
  edge [
    source 2
    target 617
  ]
  edge [
    source 2
    target 618
  ]
  edge [
    source 2
    target 619
  ]
  edge [
    source 2
    target 620
  ]
  edge [
    source 2
    target 621
  ]
  edge [
    source 2
    target 622
  ]
  edge [
    source 2
    target 623
  ]
  edge [
    source 2
    target 624
  ]
  edge [
    source 2
    target 625
  ]
  edge [
    source 2
    target 626
  ]
  edge [
    source 2
    target 627
  ]
  edge [
    source 2
    target 628
  ]
  edge [
    source 2
    target 629
  ]
  edge [
    source 2
    target 630
  ]
  edge [
    source 2
    target 631
  ]
  edge [
    source 2
    target 632
  ]
  edge [
    source 2
    target 633
  ]
  edge [
    source 2
    target 634
  ]
  edge [
    source 2
    target 635
  ]
  edge [
    source 2
    target 636
  ]
  edge [
    source 2
    target 637
  ]
  edge [
    source 2
    target 638
  ]
  edge [
    source 2
    target 639
  ]
  edge [
    source 2
    target 640
  ]
  edge [
    source 2
    target 641
  ]
  edge [
    source 2
    target 642
  ]
  edge [
    source 2
    target 643
  ]
  edge [
    source 2
    target 644
  ]
  edge [
    source 2
    target 645
  ]
  edge [
    source 2
    target 646
  ]
  edge [
    source 2
    target 647
  ]
  edge [
    source 2
    target 648
  ]
  edge [
    source 2
    target 649
  ]
  edge [
    source 2
    target 650
  ]
  edge [
    source 2
    target 651
  ]
  edge [
    source 2
    target 652
  ]
  edge [
    source 2
    target 653
  ]
  edge [
    source 2
    target 654
  ]
  edge [
    source 2
    target 655
  ]
  edge [
    source 2
    target 656
  ]
  edge [
    source 2
    target 657
  ]
  edge [
    source 2
    target 658
  ]
  edge [
    source 2
    target 659
  ]
  edge [
    source 2
    target 660
  ]
  edge [
    source 2
    target 661
  ]
  edge [
    source 2
    target 662
  ]
  edge [
    source 2
    target 663
  ]
  edge [
    source 2
    target 664
  ]
  edge [
    source 2
    target 665
  ]
  edge [
    source 2
    target 666
  ]
  edge [
    source 2
    target 667
  ]
  edge [
    source 2
    target 668
  ]
  edge [
    source 2
    target 669
  ]
  edge [
    source 2
    target 670
  ]
  edge [
    source 2
    target 671
  ]
  edge [
    source 2
    target 672
  ]
  edge [
    source 2
    target 673
  ]
  edge [
    source 2
    target 674
  ]
  edge [
    source 2
    target 675
  ]
  edge [
    source 2
    target 676
  ]
  edge [
    source 2
    target 677
  ]
  edge [
    source 2
    target 678
  ]
  edge [
    source 2
    target 679
  ]
  edge [
    source 2
    target 680
  ]
  edge [
    source 2
    target 681
  ]
  edge [
    source 2
    target 682
  ]
  edge [
    source 2
    target 683
  ]
  edge [
    source 2
    target 684
  ]
  edge [
    source 2
    target 685
  ]
  edge [
    source 2
    target 686
  ]
  edge [
    source 2
    target 687
  ]
  edge [
    source 2
    target 688
  ]
  edge [
    source 2
    target 689
  ]
  edge [
    source 2
    target 690
  ]
  edge [
    source 2
    target 691
  ]
  edge [
    source 2
    target 692
  ]
  edge [
    source 2
    target 693
  ]
  edge [
    source 2
    target 694
  ]
  edge [
    source 2
    target 695
  ]
  edge [
    source 2
    target 696
  ]
  edge [
    source 2
    target 697
  ]
  edge [
    source 2
    target 698
  ]
  edge [
    source 2
    target 699
  ]
  edge [
    source 2
    target 700
  ]
  edge [
    source 2
    target 701
  ]
  edge [
    source 2
    target 702
  ]
  edge [
    source 2
    target 703
  ]
  edge [
    source 2
    target 704
  ]
  edge [
    source 2
    target 705
  ]
  edge [
    source 2
    target 706
  ]
  edge [
    source 2
    target 707
  ]
  edge [
    source 2
    target 708
  ]
  edge [
    source 2
    target 709
  ]
  edge [
    source 2
    target 710
  ]
  edge [
    source 2
    target 711
  ]
  edge [
    source 2
    target 712
  ]
  edge [
    source 2
    target 713
  ]
  edge [
    source 2
    target 714
  ]
  edge [
    source 2
    target 715
  ]
  edge [
    source 2
    target 716
  ]
  edge [
    source 2
    target 717
  ]
  edge [
    source 2
    target 718
  ]
  edge [
    source 2
    target 719
  ]
  edge [
    source 2
    target 720
  ]
  edge [
    source 2
    target 721
  ]
  edge [
    source 2
    target 722
  ]
  edge [
    source 2
    target 723
  ]
  edge [
    source 2
    target 724
  ]
  edge [
    source 2
    target 725
  ]
  edge [
    source 2
    target 726
  ]
  edge [
    source 2
    target 727
  ]
  edge [
    source 2
    target 728
  ]
  edge [
    source 2
    target 729
  ]
  edge [
    source 2
    target 730
  ]
  edge [
    source 2
    target 731
  ]
  edge [
    source 2
    target 732
  ]
  edge [
    source 2
    target 733
  ]
  edge [
    source 2
    target 734
  ]
  edge [
    source 2
    target 735
  ]
  edge [
    source 2
    target 736
  ]
  edge [
    source 2
    target 737
  ]
  edge [
    source 2
    target 738
  ]
  edge [
    source 2
    target 739
  ]
  edge [
    source 2
    target 740
  ]
  edge [
    source 2
    target 741
  ]
  edge [
    source 2
    target 742
  ]
  edge [
    source 2
    target 743
  ]
  edge [
    source 2
    target 744
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 745
  ]
  edge [
    source 2
    target 746
  ]
  edge [
    source 2
    target 747
  ]
  edge [
    source 2
    target 748
  ]
  edge [
    source 2
    target 749
  ]
  edge [
    source 2
    target 750
  ]
  edge [
    source 2
    target 751
  ]
  edge [
    source 2
    target 752
  ]
  edge [
    source 2
    target 753
  ]
  edge [
    source 2
    target 754
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 755
  ]
  edge [
    source 2
    target 756
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 757
  ]
  edge [
    source 2
    target 758
  ]
  edge [
    source 2
    target 759
  ]
  edge [
    source 2
    target 760
  ]
  edge [
    source 2
    target 761
  ]
  edge [
    source 2
    target 762
  ]
  edge [
    source 2
    target 763
  ]
  edge [
    source 2
    target 764
  ]
  edge [
    source 2
    target 765
  ]
  edge [
    source 2
    target 766
  ]
  edge [
    source 2
    target 767
  ]
  edge [
    source 2
    target 768
  ]
  edge [
    source 2
    target 769
  ]
  edge [
    source 2
    target 770
  ]
  edge [
    source 2
    target 771
  ]
  edge [
    source 2
    target 772
  ]
  edge [
    source 2
    target 773
  ]
  edge [
    source 2
    target 774
  ]
  edge [
    source 2
    target 775
  ]
  edge [
    source 2
    target 776
  ]
  edge [
    source 2
    target 777
  ]
  edge [
    source 2
    target 778
  ]
  edge [
    source 2
    target 779
  ]
  edge [
    source 2
    target 780
  ]
  edge [
    source 2
    target 781
  ]
  edge [
    source 2
    target 782
  ]
  edge [
    source 2
    target 783
  ]
  edge [
    source 2
    target 784
  ]
  edge [
    source 2
    target 785
  ]
  edge [
    source 2
    target 786
  ]
  edge [
    source 2
    target 787
  ]
  edge [
    source 2
    target 788
  ]
  edge [
    source 2
    target 789
  ]
  edge [
    source 2
    target 790
  ]
  edge [
    source 2
    target 791
  ]
  edge [
    source 2
    target 792
  ]
  edge [
    source 2
    target 793
  ]
  edge [
    source 2
    target 794
  ]
  edge [
    source 2
    target 795
  ]
  edge [
    source 2
    target 796
  ]
  edge [
    source 2
    target 797
  ]
  edge [
    source 2
    target 798
  ]
  edge [
    source 2
    target 799
  ]
  edge [
    source 2
    target 800
  ]
  edge [
    source 2
    target 801
  ]
  edge [
    source 2
    target 802
  ]
  edge [
    source 2
    target 803
  ]
  edge [
    source 2
    target 804
  ]
  edge [
    source 2
    target 805
  ]
  edge [
    source 2
    target 806
  ]
  edge [
    source 2
    target 807
  ]
  edge [
    source 2
    target 808
  ]
  edge [
    source 2
    target 809
  ]
  edge [
    source 2
    target 810
  ]
  edge [
    source 2
    target 811
  ]
  edge [
    source 2
    target 812
  ]
  edge [
    source 2
    target 813
  ]
  edge [
    source 2
    target 814
  ]
  edge [
    source 2
    target 815
  ]
  edge [
    source 2
    target 816
  ]
  edge [
    source 2
    target 817
  ]
  edge [
    source 2
    target 818
  ]
  edge [
    source 2
    target 819
  ]
  edge [
    source 2
    target 820
  ]
  edge [
    source 2
    target 821
  ]
  edge [
    source 2
    target 822
  ]
  edge [
    source 2
    target 823
  ]
  edge [
    source 2
    target 824
  ]
  edge [
    source 2
    target 825
  ]
  edge [
    source 2
    target 826
  ]
  edge [
    source 2
    target 827
  ]
  edge [
    source 2
    target 828
  ]
  edge [
    source 2
    target 829
  ]
  edge [
    source 2
    target 830
  ]
  edge [
    source 2
    target 831
  ]
  edge [
    source 2
    target 832
  ]
  edge [
    source 2
    target 833
  ]
  edge [
    source 2
    target 834
  ]
  edge [
    source 2
    target 835
  ]
  edge [
    source 2
    target 836
  ]
  edge [
    source 2
    target 837
  ]
  edge [
    source 2
    target 838
  ]
  edge [
    source 2
    target 839
  ]
  edge [
    source 2
    target 840
  ]
  edge [
    source 2
    target 841
  ]
  edge [
    source 2
    target 842
  ]
  edge [
    source 2
    target 843
  ]
  edge [
    source 2
    target 844
  ]
  edge [
    source 2
    target 845
  ]
  edge [
    source 2
    target 846
  ]
  edge [
    source 2
    target 847
  ]
  edge [
    source 2
    target 848
  ]
  edge [
    source 2
    target 849
  ]
  edge [
    source 2
    target 850
  ]
  edge [
    source 2
    target 851
  ]
  edge [
    source 2
    target 852
  ]
  edge [
    source 2
    target 853
  ]
  edge [
    source 2
    target 854
  ]
  edge [
    source 2
    target 855
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 856
  ]
  edge [
    source 3
    target 857
  ]
  edge [
    source 3
    target 858
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 859
  ]
  edge [
    source 3
    target 860
  ]
  edge [
    source 3
    target 861
  ]
  edge [
    source 3
    target 862
  ]
  edge [
    source 3
    target 863
  ]
  edge [
    source 3
    target 756
  ]
  edge [
    source 3
    target 864
  ]
  edge [
    source 3
    target 865
  ]
  edge [
    source 3
    target 866
  ]
  edge [
    source 3
    target 867
  ]
  edge [
    source 3
    target 868
  ]
  edge [
    source 3
    target 869
  ]
  edge [
    source 3
    target 870
  ]
  edge [
    source 3
    target 871
  ]
  edge [
    source 3
    target 872
  ]
  edge [
    source 3
    target 873
  ]
  edge [
    source 3
    target 874
  ]
  edge [
    source 3
    target 875
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 876
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 877
  ]
  edge [
    source 3
    target 878
  ]
  edge [
    source 3
    target 879
  ]
  edge [
    source 3
    target 880
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 881
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 882
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 883
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 884
  ]
  edge [
    source 3
    target 885
  ]
  edge [
    source 3
    target 886
  ]
  edge [
    source 3
    target 887
  ]
  edge [
    source 3
    target 888
  ]
  edge [
    source 3
    target 889
  ]
  edge [
    source 3
    target 890
  ]
  edge [
    source 3
    target 891
  ]
  edge [
    source 3
    target 892
  ]
  edge [
    source 3
    target 893
  ]
  edge [
    source 3
    target 894
  ]
  edge [
    source 3
    target 895
  ]
  edge [
    source 3
    target 896
  ]
  edge [
    source 3
    target 897
  ]
  edge [
    source 3
    target 898
  ]
  edge [
    source 3
    target 796
  ]
  edge [
    source 3
    target 717
  ]
  edge [
    source 3
    target 899
  ]
  edge [
    source 3
    target 900
  ]
  edge [
    source 3
    target 901
  ]
  edge [
    source 3
    target 902
  ]
  edge [
    source 3
    target 903
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 904
  ]
  edge [
    source 4
    target 905
  ]
  edge [
    source 4
    target 906
  ]
  edge [
    source 4
    target 907
  ]
  edge [
    source 4
    target 908
  ]
  edge [
    source 4
    target 909
  ]
  edge [
    source 4
    target 910
  ]
  edge [
    source 4
    target 911
  ]
  edge [
    source 4
    target 912
  ]
  edge [
    source 4
    target 913
  ]
  edge [
    source 4
    target 914
  ]
  edge [
    source 4
    target 915
  ]
  edge [
    source 4
    target 916
  ]
  edge [
    source 4
    target 917
  ]
  edge [
    source 4
    target 918
  ]
  edge [
    source 4
    target 919
  ]
  edge [
    source 4
    target 920
  ]
  edge [
    source 4
    target 921
  ]
  edge [
    source 4
    target 922
  ]
  edge [
    source 4
    target 923
  ]
  edge [
    source 4
    target 924
  ]
  edge [
    source 4
    target 925
  ]
  edge [
    source 4
    target 926
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 927
  ]
  edge [
    source 4
    target 928
  ]
  edge [
    source 4
    target 929
  ]
  edge [
    source 4
    target 930
  ]
  edge [
    source 4
    target 931
  ]
  edge [
    source 4
    target 932
  ]
  edge [
    source 4
    target 933
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 934
  ]
  edge [
    source 4
    target 756
  ]
  edge [
    source 4
    target 935
  ]
  edge [
    source 4
    target 936
  ]
  edge [
    source 4
    target 752
  ]
  edge [
    source 4
    target 772
  ]
  edge [
    source 4
    target 759
  ]
  edge [
    source 4
    target 937
  ]
  edge [
    source 4
    target 938
  ]
  edge [
    source 4
    target 835
  ]
  edge [
    source 4
    target 762
  ]
  edge [
    source 4
    target 775
  ]
  edge [
    source 4
    target 939
  ]
  edge [
    source 4
    target 940
  ]
  edge [
    source 4
    target 941
  ]
  edge [
    source 4
    target 942
  ]
  edge [
    source 4
    target 943
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 944
  ]
  edge [
    source 4
    target 945
  ]
  edge [
    source 4
    target 773
  ]
  edge [
    source 4
    target 946
  ]
  edge [
    source 4
    target 744
  ]
  edge [
    source 4
    target 947
  ]
  edge [
    source 4
    target 948
  ]
  edge [
    source 4
    target 949
  ]
  edge [
    source 4
    target 950
  ]
  edge [
    source 4
    target 951
  ]
  edge [
    source 4
    target 952
  ]
  edge [
    source 4
    target 953
  ]
  edge [
    source 4
    target 749
  ]
  edge [
    source 4
    target 954
  ]
  edge [
    source 4
    target 955
  ]
  edge [
    source 4
    target 956
  ]
  edge [
    source 4
    target 957
  ]
  edge [
    source 4
    target 958
  ]
  edge [
    source 4
    target 959
  ]
  edge [
    source 4
    target 960
  ]
  edge [
    source 4
    target 961
  ]
  edge [
    source 4
    target 962
  ]
  edge [
    source 4
    target 963
  ]
  edge [
    source 4
    target 751
  ]
  edge [
    source 4
    target 964
  ]
  edge [
    source 4
    target 965
  ]
  edge [
    source 4
    target 966
  ]
  edge [
    source 4
    target 967
  ]
  edge [
    source 4
    target 968
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 969
  ]
  edge [
    source 4
    target 970
  ]
  edge [
    source 4
    target 971
  ]
  edge [
    source 4
    target 972
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 973
  ]
  edge [
    source 5
    target 759
  ]
  edge [
    source 5
    target 974
  ]
  edge [
    source 5
    target 975
  ]
  edge [
    source 5
    target 976
  ]
  edge [
    source 5
    target 977
  ]
  edge [
    source 5
    target 978
  ]
  edge [
    source 5
    target 979
  ]
  edge [
    source 5
    target 980
  ]
  edge [
    source 5
    target 981
  ]
  edge [
    source 5
    target 982
  ]
  edge [
    source 5
    target 983
  ]
  edge [
    source 5
    target 984
  ]
  edge [
    source 5
    target 985
  ]
  edge [
    source 5
    target 986
  ]
  edge [
    source 5
    target 987
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 988
  ]
  edge [
    source 5
    target 989
  ]
  edge [
    source 5
    target 990
  ]
  edge [
    source 5
    target 991
  ]
  edge [
    source 5
    target 992
  ]
  edge [
    source 5
    target 993
  ]
  edge [
    source 5
    target 994
  ]
  edge [
    source 5
    target 860
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 995
  ]
  edge [
    source 5
    target 996
  ]
  edge [
    source 5
    target 997
  ]
  edge [
    source 5
    target 757
  ]
  edge [
    source 5
    target 998
  ]
  edge [
    source 5
    target 999
  ]
  edge [
    source 5
    target 1000
  ]
  edge [
    source 5
    target 1001
  ]
  edge [
    source 5
    target 1002
  ]
  edge [
    source 5
    target 1003
  ]
  edge [
    source 5
    target 1004
  ]
  edge [
    source 5
    target 764
  ]
  edge [
    source 5
    target 1005
  ]
  edge [
    source 5
    target 1006
  ]
  edge [
    source 5
    target 1007
  ]
  edge [
    source 5
    target 756
  ]
  edge [
    source 5
    target 1008
  ]
  edge [
    source 5
    target 1009
  ]
  edge [
    source 5
    target 1010
  ]
  edge [
    source 5
    target 1011
  ]
  edge [
    source 5
    target 1012
  ]
  edge [
    source 5
    target 1013
  ]
  edge [
    source 5
    target 1014
  ]
  edge [
    source 5
    target 1015
  ]
  edge [
    source 5
    target 1016
  ]
  edge [
    source 5
    target 1017
  ]
  edge [
    source 5
    target 1018
  ]
  edge [
    source 5
    target 1019
  ]
  edge [
    source 5
    target 883
  ]
  edge [
    source 5
    target 1020
  ]
  edge [
    source 5
    target 1021
  ]
  edge [
    source 5
    target 1022
  ]
  edge [
    source 5
    target 1023
  ]
  edge [
    source 5
    target 1024
  ]
  edge [
    source 5
    target 1025
  ]
  edge [
    source 5
    target 1026
  ]
  edge [
    source 5
    target 1027
  ]
  edge [
    source 5
    target 1028
  ]
  edge [
    source 5
    target 1029
  ]
  edge [
    source 5
    target 1030
  ]
  edge [
    source 5
    target 1031
  ]
  edge [
    source 5
    target 1032
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 885
  ]
  edge [
    source 5
    target 1033
  ]
  edge [
    source 5
    target 1034
  ]
  edge [
    source 5
    target 1035
  ]
  edge [
    source 5
    target 1036
  ]
  edge [
    source 5
    target 1037
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 1038
  ]
  edge [
    source 6
    target 1039
  ]
  edge [
    source 6
    target 1040
  ]
  edge [
    source 6
    target 1041
  ]
  edge [
    source 6
    target 1042
  ]
  edge [
    source 6
    target 1043
  ]
  edge [
    source 6
    target 1044
  ]
  edge [
    source 6
    target 1045
  ]
  edge [
    source 6
    target 1046
  ]
  edge [
    source 6
    target 1047
  ]
  edge [
    source 6
    target 1048
  ]
  edge [
    source 6
    target 1049
  ]
  edge [
    source 6
    target 1050
  ]
  edge [
    source 6
    target 1051
  ]
  edge [
    source 6
    target 1052
  ]
  edge [
    source 6
    target 1053
  ]
  edge [
    source 6
    target 1054
  ]
  edge [
    source 6
    target 1055
  ]
  edge [
    source 6
    target 1056
  ]
  edge [
    source 6
    target 1057
  ]
  edge [
    source 6
    target 1058
  ]
  edge [
    source 6
    target 1059
  ]
  edge [
    source 6
    target 1060
  ]
  edge [
    source 6
    target 1061
  ]
  edge [
    source 6
    target 1062
  ]
  edge [
    source 6
    target 1063
  ]
  edge [
    source 6
    target 1064
  ]
  edge [
    source 6
    target 1065
  ]
  edge [
    source 6
    target 1066
  ]
  edge [
    source 6
    target 1067
  ]
  edge [
    source 6
    target 1068
  ]
  edge [
    source 6
    target 741
  ]
  edge [
    source 6
    target 1069
  ]
  edge [
    source 6
    target 1070
  ]
  edge [
    source 6
    target 1071
  ]
  edge [
    source 6
    target 1072
  ]
  edge [
    source 6
    target 1073
  ]
  edge [
    source 6
    target 1074
  ]
  edge [
    source 6
    target 1075
  ]
  edge [
    source 6
    target 1076
  ]
  edge [
    source 6
    target 1077
  ]
  edge [
    source 6
    target 1078
  ]
  edge [
    source 6
    target 1079
  ]
  edge [
    source 6
    target 1080
  ]
  edge [
    source 6
    target 1081
  ]
  edge [
    source 6
    target 1082
  ]
  edge [
    source 6
    target 1083
  ]
  edge [
    source 6
    target 1084
  ]
  edge [
    source 6
    target 1085
  ]
  edge [
    source 6
    target 1086
  ]
  edge [
    source 6
    target 1087
  ]
  edge [
    source 6
    target 1088
  ]
  edge [
    source 6
    target 1089
  ]
  edge [
    source 6
    target 1090
  ]
  edge [
    source 6
    target 1091
  ]
  edge [
    source 6
    target 1092
  ]
  edge [
    source 6
    target 1093
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 1094
  ]
  edge [
    source 6
    target 1095
  ]
  edge [
    source 6
    target 1096
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 1097
  ]
  edge [
    source 7
    target 857
  ]
  edge [
    source 7
    target 1098
  ]
  edge [
    source 7
    target 1099
  ]
  edge [
    source 7
    target 1100
  ]
  edge [
    source 7
    target 1101
  ]
  edge [
    source 7
    target 1102
  ]
  edge [
    source 7
    target 1103
  ]
  edge [
    source 7
    target 1104
  ]
  edge [
    source 7
    target 1105
  ]
  edge [
    source 7
    target 858
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 859
  ]
  edge [
    source 7
    target 860
  ]
  edge [
    source 7
    target 861
  ]
  edge [
    source 7
    target 862
  ]
  edge [
    source 7
    target 863
  ]
  edge [
    source 7
    target 756
  ]
  edge [
    source 7
    target 864
  ]
  edge [
    source 7
    target 865
  ]
  edge [
    source 7
    target 866
  ]
  edge [
    source 7
    target 867
  ]
  edge [
    source 7
    target 868
  ]
  edge [
    source 7
    target 869
  ]
  edge [
    source 7
    target 870
  ]
  edge [
    source 7
    target 871
  ]
  edge [
    source 7
    target 872
  ]
  edge [
    source 7
    target 873
  ]
  edge [
    source 7
    target 874
  ]
  edge [
    source 7
    target 875
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 876
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 877
  ]
  edge [
    source 7
    target 878
  ]
  edge [
    source 7
    target 879
  ]
  edge [
    source 7
    target 880
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 881
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 882
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 883
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 884
  ]
  edge [
    source 7
    target 885
  ]
  edge [
    source 7
    target 886
  ]
  edge [
    source 7
    target 887
  ]
  edge [
    source 7
    target 1106
  ]
  edge [
    source 7
    target 1107
  ]
  edge [
    source 7
    target 1108
  ]
  edge [
    source 7
    target 1109
  ]
  edge [
    source 7
    target 1110
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 1111
  ]
  edge [
    source 8
    target 1112
  ]
  edge [
    source 8
    target 1113
  ]
  edge [
    source 8
    target 1114
  ]
  edge [
    source 8
    target 1115
  ]
  edge [
    source 8
    target 1116
  ]
  edge [
    source 8
    target 1117
  ]
  edge [
    source 8
    target 1118
  ]
  edge [
    source 8
    target 1119
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 1120
  ]
  edge [
    source 8
    target 1121
  ]
  edge [
    source 8
    target 1122
  ]
  edge [
    source 8
    target 1123
  ]
  edge [
    source 8
    target 1124
  ]
  edge [
    source 8
    target 1125
  ]
  edge [
    source 8
    target 1126
  ]
  edge [
    source 8
    target 1127
  ]
  edge [
    source 8
    target 929
  ]
  edge [
    source 8
    target 1128
  ]
  edge [
    source 8
    target 1129
  ]
  edge [
    source 8
    target 1130
  ]
  edge [
    source 8
    target 1131
  ]
  edge [
    source 8
    target 1132
  ]
  edge [
    source 8
    target 1133
  ]
  edge [
    source 8
    target 1134
  ]
  edge [
    source 8
    target 1135
  ]
  edge [
    source 8
    target 1136
  ]
  edge [
    source 8
    target 1137
  ]
  edge [
    source 8
    target 1138
  ]
  edge [
    source 8
    target 1139
  ]
  edge [
    source 8
    target 1140
  ]
  edge [
    source 8
    target 1141
  ]
  edge [
    source 8
    target 1142
  ]
  edge [
    source 8
    target 1143
  ]
  edge [
    source 8
    target 1144
  ]
  edge [
    source 8
    target 1145
  ]
  edge [
    source 8
    target 1146
  ]
  edge [
    source 8
    target 1147
  ]
  edge [
    source 8
    target 1148
  ]
  edge [
    source 8
    target 1149
  ]
  edge [
    source 8
    target 1150
  ]
  edge [
    source 8
    target 1151
  ]
  edge [
    source 8
    target 1152
  ]
  edge [
    source 8
    target 1153
  ]
  edge [
    source 8
    target 1154
  ]
  edge [
    source 8
    target 1155
  ]
  edge [
    source 8
    target 1156
  ]
  edge [
    source 8
    target 1157
  ]
  edge [
    source 8
    target 1158
  ]
  edge [
    source 8
    target 1159
  ]
  edge [
    source 8
    target 1160
  ]
  edge [
    source 8
    target 1161
  ]
  edge [
    source 8
    target 1162
  ]
  edge [
    source 8
    target 1163
  ]
  edge [
    source 8
    target 1164
  ]
  edge [
    source 8
    target 1165
  ]
  edge [
    source 8
    target 1166
  ]
  edge [
    source 8
    target 1167
  ]
  edge [
    source 8
    target 1168
  ]
  edge [
    source 8
    target 1169
  ]
  edge [
    source 8
    target 847
  ]
  edge [
    source 8
    target 1170
  ]
  edge [
    source 8
    target 1171
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 1172
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 1173
  ]
  edge [
    source 8
    target 1174
  ]
  edge [
    source 8
    target 1175
  ]
  edge [
    source 8
    target 1176
  ]
  edge [
    source 8
    target 1177
  ]
  edge [
    source 8
    target 1178
  ]
  edge [
    source 8
    target 1179
  ]
  edge [
    source 8
    target 1180
  ]
  edge [
    source 8
    target 1181
  ]
  edge [
    source 8
    target 1182
  ]
  edge [
    source 8
    target 1183
  ]
  edge [
    source 8
    target 1184
  ]
  edge [
    source 8
    target 1185
  ]
  edge [
    source 8
    target 1186
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 1187
  ]
  edge [
    source 8
    target 1188
  ]
  edge [
    source 8
    target 1189
  ]
  edge [
    source 8
    target 1190
  ]
  edge [
    source 8
    target 1191
  ]
  edge [
    source 8
    target 1192
  ]
  edge [
    source 8
    target 1193
  ]
  edge [
    source 8
    target 1194
  ]
  edge [
    source 8
    target 1195
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 1196
  ]
  edge [
    source 9
    target 1059
  ]
  edge [
    source 9
    target 1197
  ]
  edge [
    source 9
    target 1198
  ]
  edge [
    source 9
    target 1199
  ]
  edge [
    source 9
    target 1200
  ]
  edge [
    source 9
    target 1201
  ]
  edge [
    source 9
    target 1202
  ]
  edge [
    source 9
    target 1203
  ]
  edge [
    source 9
    target 1204
  ]
  edge [
    source 9
    target 1205
  ]
  edge [
    source 9
    target 1206
  ]
  edge [
    source 9
    target 1207
  ]
  edge [
    source 9
    target 1208
  ]
  edge [
    source 9
    target 1209
  ]
  edge [
    source 9
    target 1210
  ]
  edge [
    source 9
    target 1211
  ]
  edge [
    source 9
    target 1212
  ]
  edge [
    source 9
    target 1213
  ]
  edge [
    source 9
    target 1214
  ]
  edge [
    source 9
    target 1215
  ]
  edge [
    source 9
    target 1216
  ]
  edge [
    source 9
    target 1217
  ]
  edge [
    source 9
    target 1218
  ]
  edge [
    source 9
    target 1219
  ]
  edge [
    source 9
    target 1220
  ]
  edge [
    source 9
    target 1221
  ]
  edge [
    source 9
    target 1222
  ]
  edge [
    source 9
    target 1223
  ]
  edge [
    source 9
    target 1224
  ]
  edge [
    source 9
    target 1225
  ]
  edge [
    source 9
    target 1226
  ]
  edge [
    source 9
    target 1227
  ]
  edge [
    source 9
    target 1228
  ]
  edge [
    source 9
    target 1229
  ]
  edge [
    source 9
    target 1230
  ]
  edge [
    source 9
    target 1231
  ]
  edge [
    source 9
    target 1045
  ]
  edge [
    source 9
    target 1232
  ]
  edge [
    source 9
    target 1233
  ]
  edge [
    source 9
    target 1234
  ]
  edge [
    source 9
    target 1235
  ]
  edge [
    source 9
    target 1064
  ]
  edge [
    source 9
    target 1236
  ]
  edge [
    source 9
    target 1237
  ]
  edge [
    source 9
    target 1089
  ]
  edge [
    source 9
    target 1238
  ]
  edge [
    source 9
    target 1239
  ]
  edge [
    source 9
    target 1240
  ]
  edge [
    source 9
    target 1241
  ]
  edge [
    source 9
    target 1087
  ]
  edge [
    source 9
    target 1242
  ]
  edge [
    source 9
    target 1243
  ]
  edge [
    source 9
    target 1244
  ]
  edge [
    source 9
    target 1245
  ]
  edge [
    source 9
    target 1246
  ]
  edge [
    source 9
    target 1247
  ]
  edge [
    source 9
    target 1248
  ]
  edge [
    source 9
    target 1249
  ]
  edge [
    source 9
    target 1250
  ]
  edge [
    source 9
    target 1251
  ]
  edge [
    source 9
    target 1252
  ]
  edge [
    source 9
    target 1253
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 1254
  ]
  edge [
    source 10
    target 1255
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 1256
  ]
  edge [
    source 10
    target 1257
  ]
  edge [
    source 10
    target 1258
  ]
  edge [
    source 10
    target 1259
  ]
  edge [
    source 10
    target 1260
  ]
  edge [
    source 10
    target 860
  ]
  edge [
    source 10
    target 1261
  ]
  edge [
    source 10
    target 1262
  ]
  edge [
    source 10
    target 1263
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 1264
  ]
  edge [
    source 10
    target 1265
  ]
  edge [
    source 10
    target 1266
  ]
  edge [
    source 10
    target 1267
  ]
  edge [
    source 10
    target 1268
  ]
  edge [
    source 10
    target 1269
  ]
  edge [
    source 10
    target 1270
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 1271
  ]
  edge [
    source 10
    target 1272
  ]
  edge [
    source 10
    target 1273
  ]
  edge [
    source 10
    target 952
  ]
  edge [
    source 10
    target 1274
  ]
  edge [
    source 10
    target 1275
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 1276
  ]
  edge [
    source 10
    target 1277
  ]
  edge [
    source 10
    target 1278
  ]
  edge [
    source 10
    target 1279
  ]
  edge [
    source 10
    target 1280
  ]
  edge [
    source 10
    target 1281
  ]
  edge [
    source 10
    target 1282
  ]
  edge [
    source 10
    target 883
  ]
  edge [
    source 10
    target 1244
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 1283
  ]
  edge [
    source 10
    target 1284
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 1285
  ]
  edge [
    source 11
    target 1286
  ]
  edge [
    source 11
    target 1287
  ]
  edge [
    source 11
    target 1288
  ]
  edge [
    source 11
    target 1289
  ]
  edge [
    source 11
    target 1290
  ]
  edge [
    source 11
    target 1291
  ]
  edge [
    source 11
    target 1292
  ]
  edge [
    source 11
    target 1293
  ]
  edge [
    source 11
    target 1294
  ]
  edge [
    source 11
    target 1295
  ]
  edge [
    source 11
    target 1296
  ]
  edge [
    source 11
    target 1297
  ]
  edge [
    source 11
    target 1298
  ]
  edge [
    source 11
    target 1299
  ]
  edge [
    source 11
    target 1300
  ]
  edge [
    source 11
    target 1301
  ]
  edge [
    source 11
    target 1302
  ]
  edge [
    source 11
    target 1303
  ]
  edge [
    source 11
    target 1304
  ]
  edge [
    source 11
    target 1305
  ]
  edge [
    source 11
    target 1306
  ]
  edge [
    source 11
    target 1124
  ]
  edge [
    source 11
    target 1307
  ]
  edge [
    source 11
    target 1308
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 1309
  ]
  edge [
    source 11
    target 1310
  ]
  edge [
    source 11
    target 1311
  ]
  edge [
    source 11
    target 1312
  ]
  edge [
    source 11
    target 1313
  ]
  edge [
    source 11
    target 1314
  ]
  edge [
    source 11
    target 1315
  ]
  edge [
    source 11
    target 1316
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 1317
  ]
  edge [
    source 11
    target 1318
  ]
  edge [
    source 11
    target 1319
  ]
  edge [
    source 11
    target 1320
  ]
  edge [
    source 11
    target 1321
  ]
  edge [
    source 11
    target 1322
  ]
  edge [
    source 11
    target 1323
  ]
  edge [
    source 11
    target 1324
  ]
  edge [
    source 11
    target 1325
  ]
  edge [
    source 11
    target 1326
  ]
  edge [
    source 11
    target 1327
  ]
  edge [
    source 11
    target 1328
  ]
  edge [
    source 11
    target 1329
  ]
  edge [
    source 11
    target 1330
  ]
  edge [
    source 11
    target 1331
  ]
  edge [
    source 11
    target 1332
  ]
  edge [
    source 11
    target 1333
  ]
  edge [
    source 11
    target 1334
  ]
  edge [
    source 11
    target 1335
  ]
  edge [
    source 11
    target 1336
  ]
  edge [
    source 11
    target 1337
  ]
  edge [
    source 11
    target 1338
  ]
  edge [
    source 11
    target 1339
  ]
  edge [
    source 11
    target 1340
  ]
  edge [
    source 11
    target 1341
  ]
  edge [
    source 11
    target 1342
  ]
  edge [
    source 11
    target 1343
  ]
  edge [
    source 11
    target 1344
  ]
  edge [
    source 11
    target 1345
  ]
  edge [
    source 11
    target 1346
  ]
  edge [
    source 11
    target 1347
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 1348
  ]
  edge [
    source 11
    target 1349
  ]
  edge [
    source 11
    target 1350
  ]
  edge [
    source 11
    target 1351
  ]
  edge [
    source 11
    target 1352
  ]
  edge [
    source 11
    target 1353
  ]
  edge [
    source 11
    target 1354
  ]
  edge [
    source 11
    target 1355
  ]
  edge [
    source 11
    target 1356
  ]
  edge [
    source 11
    target 1357
  ]
  edge [
    source 11
    target 1358
  ]
  edge [
    source 11
    target 1359
  ]
  edge [
    source 11
    target 1360
  ]
  edge [
    source 11
    target 1361
  ]
  edge [
    source 11
    target 1362
  ]
  edge [
    source 11
    target 1363
  ]
  edge [
    source 11
    target 1364
  ]
  edge [
    source 11
    target 1365
  ]
  edge [
    source 11
    target 1366
  ]
  edge [
    source 11
    target 1367
  ]
  edge [
    source 11
    target 1368
  ]
  edge [
    source 11
    target 1369
  ]
  edge [
    source 11
    target 1370
  ]
  edge [
    source 11
    target 1371
  ]
  edge [
    source 11
    target 1372
  ]
  edge [
    source 11
    target 1373
  ]
  edge [
    source 11
    target 1374
  ]
  edge [
    source 11
    target 1375
  ]
  edge [
    source 11
    target 1376
  ]
  edge [
    source 11
    target 1377
  ]
  edge [
    source 11
    target 1378
  ]
  edge [
    source 11
    target 1379
  ]
  edge [
    source 11
    target 1380
  ]
  edge [
    source 11
    target 1381
  ]
  edge [
    source 11
    target 1382
  ]
  edge [
    source 11
    target 1383
  ]
  edge [
    source 11
    target 1384
  ]
  edge [
    source 11
    target 1244
  ]
  edge [
    source 11
    target 1385
  ]
  edge [
    source 11
    target 32
  ]
  edge [
    source 11
    target 1386
  ]
  edge [
    source 11
    target 1387
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 11
    target 969
  ]
  edge [
    source 11
    target 1388
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 1389
  ]
  edge [
    source 11
    target 1390
  ]
  edge [
    source 11
    target 1391
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 1392
  ]
  edge [
    source 11
    target 1393
  ]
  edge [
    source 11
    target 1394
  ]
  edge [
    source 11
    target 1001
  ]
  edge [
    source 11
    target 1395
  ]
  edge [
    source 11
    target 1396
  ]
  edge [
    source 11
    target 1397
  ]
  edge [
    source 11
    target 1398
  ]
  edge [
    source 11
    target 1399
  ]
  edge [
    source 11
    target 1400
  ]
  edge [
    source 11
    target 1401
  ]
  edge [
    source 11
    target 1402
  ]
  edge [
    source 11
    target 1403
  ]
  edge [
    source 11
    target 1404
  ]
  edge [
    source 11
    target 1267
  ]
  edge [
    source 11
    target 1405
  ]
  edge [
    source 11
    target 1406
  ]
  edge [
    source 11
    target 1407
  ]
  edge [
    source 11
    target 1408
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 1409
  ]
  edge [
    source 11
    target 1410
  ]
  edge [
    source 11
    target 1411
  ]
  edge [
    source 11
    target 1412
  ]
  edge [
    source 11
    target 1413
  ]
  edge [
    source 11
    target 1414
  ]
  edge [
    source 11
    target 1415
  ]
  edge [
    source 11
    target 1416
  ]
  edge [
    source 11
    target 1417
  ]
  edge [
    source 11
    target 1418
  ]
  edge [
    source 11
    target 1419
  ]
  edge [
    source 11
    target 1420
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 1421
  ]
  edge [
    source 11
    target 1422
  ]
  edge [
    source 11
    target 1423
  ]
  edge [
    source 11
    target 1424
  ]
  edge [
    source 11
    target 1425
  ]
  edge [
    source 11
    target 1426
  ]
  edge [
    source 11
    target 992
  ]
  edge [
    source 11
    target 1427
  ]
  edge [
    source 11
    target 1428
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 1429
  ]
  edge [
    source 11
    target 1430
  ]
  edge [
    source 11
    target 1431
  ]
  edge [
    source 11
    target 1432
  ]
  edge [
    source 11
    target 1433
  ]
  edge [
    source 11
    target 1434
  ]
  edge [
    source 11
    target 1435
  ]
  edge [
    source 11
    target 1436
  ]
  edge [
    source 11
    target 1437
  ]
  edge [
    source 11
    target 885
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1438
  ]
  edge [
    source 12
    target 1439
  ]
  edge [
    source 12
    target 1440
  ]
  edge [
    source 12
    target 1441
  ]
  edge [
    source 12
    target 1442
  ]
  edge [
    source 12
    target 1443
  ]
  edge [
    source 12
    target 1444
  ]
  edge [
    source 12
    target 1445
  ]
  edge [
    source 12
    target 1446
  ]
  edge [
    source 12
    target 1045
  ]
  edge [
    source 12
    target 1447
  ]
  edge [
    source 12
    target 1448
  ]
  edge [
    source 12
    target 1449
  ]
  edge [
    source 12
    target 1450
  ]
  edge [
    source 12
    target 1451
  ]
  edge [
    source 12
    target 1452
  ]
  edge [
    source 12
    target 1215
  ]
  edge [
    source 12
    target 1453
  ]
  edge [
    source 12
    target 1454
  ]
  edge [
    source 12
    target 1455
  ]
  edge [
    source 12
    target 1053
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1456
  ]
  edge [
    source 13
    target 1457
  ]
  edge [
    source 13
    target 1458
  ]
  edge [
    source 13
    target 989
  ]
  edge [
    source 13
    target 1459
  ]
  edge [
    source 13
    target 1460
  ]
  edge [
    source 13
    target 1461
  ]
  edge [
    source 13
    target 1462
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 13
    target 48
  ]
  edge [
    source 13
    target 1463
  ]
  edge [
    source 13
    target 1464
  ]
  edge [
    source 13
    target 1465
  ]
  edge [
    source 13
    target 1466
  ]
  edge [
    source 13
    target 1467
  ]
  edge [
    source 13
    target 45
  ]
  edge [
    source 13
    target 995
  ]
  edge [
    source 13
    target 1468
  ]
  edge [
    source 13
    target 970
  ]
  edge [
    source 13
    target 1469
  ]
  edge [
    source 13
    target 1470
  ]
  edge [
    source 13
    target 1471
  ]
  edge [
    source 13
    target 1472
  ]
  edge [
    source 13
    target 1473
  ]
  edge [
    source 13
    target 1474
  ]
  edge [
    source 13
    target 1475
  ]
  edge [
    source 13
    target 1476
  ]
  edge [
    source 13
    target 1477
  ]
  edge [
    source 13
    target 1478
  ]
  edge [
    source 13
    target 1479
  ]
  edge [
    source 13
    target 1480
  ]
  edge [
    source 13
    target 1481
  ]
  edge [
    source 13
    target 1229
  ]
  edge [
    source 13
    target 1482
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 1483
  ]
  edge [
    source 13
    target 1484
  ]
  edge [
    source 13
    target 1485
  ]
  edge [
    source 13
    target 1486
  ]
  edge [
    source 13
    target 1487
  ]
  edge [
    source 13
    target 1488
  ]
  edge [
    source 13
    target 981
  ]
  edge [
    source 13
    target 1489
  ]
  edge [
    source 13
    target 1490
  ]
  edge [
    source 13
    target 1491
  ]
  edge [
    source 13
    target 1069
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 1492
  ]
  edge [
    source 13
    target 1493
  ]
  edge [
    source 13
    target 1494
  ]
  edge [
    source 13
    target 1495
  ]
  edge [
    source 13
    target 1496
  ]
  edge [
    source 13
    target 1497
  ]
  edge [
    source 13
    target 1498
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 1300
  ]
  edge [
    source 13
    target 1499
  ]
  edge [
    source 13
    target 1500
  ]
  edge [
    source 13
    target 1501
  ]
  edge [
    source 13
    target 1502
  ]
  edge [
    source 13
    target 1086
  ]
  edge [
    source 13
    target 1503
  ]
  edge [
    source 13
    target 1504
  ]
  edge [
    source 13
    target 1505
  ]
  edge [
    source 13
    target 1506
  ]
  edge [
    source 13
    target 1040
  ]
  edge [
    source 13
    target 1507
  ]
  edge [
    source 13
    target 1508
  ]
  edge [
    source 13
    target 1509
  ]
  edge [
    source 13
    target 1044
  ]
  edge [
    source 13
    target 1510
  ]
  edge [
    source 13
    target 1211
  ]
  edge [
    source 13
    target 1511
  ]
  edge [
    source 13
    target 1512
  ]
  edge [
    source 13
    target 1513
  ]
  edge [
    source 13
    target 1514
  ]
  edge [
    source 13
    target 1515
  ]
  edge [
    source 13
    target 1047
  ]
  edge [
    source 13
    target 1516
  ]
  edge [
    source 13
    target 1517
  ]
  edge [
    source 13
    target 1518
  ]
  edge [
    source 13
    target 1519
  ]
  edge [
    source 13
    target 1520
  ]
  edge [
    source 13
    target 1521
  ]
  edge [
    source 13
    target 1522
  ]
  edge [
    source 13
    target 1523
  ]
  edge [
    source 13
    target 1524
  ]
  edge [
    source 13
    target 1525
  ]
  edge [
    source 13
    target 1526
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1527
  ]
  edge [
    source 14
    target 1528
  ]
  edge [
    source 14
    target 1529
  ]
  edge [
    source 14
    target 1530
  ]
  edge [
    source 14
    target 1531
  ]
  edge [
    source 14
    target 1532
  ]
  edge [
    source 14
    target 1533
  ]
  edge [
    source 14
    target 1534
  ]
  edge [
    source 14
    target 1535
  ]
  edge [
    source 14
    target 1536
  ]
  edge [
    source 14
    target 1537
  ]
  edge [
    source 14
    target 1538
  ]
  edge [
    source 14
    target 1539
  ]
  edge [
    source 14
    target 1540
  ]
  edge [
    source 14
    target 1541
  ]
  edge [
    source 14
    target 1542
  ]
  edge [
    source 14
    target 1543
  ]
  edge [
    source 14
    target 1544
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1545
  ]
  edge [
    source 15
    target 904
  ]
  edge [
    source 15
    target 1546
  ]
  edge [
    source 15
    target 1176
  ]
  edge [
    source 15
    target 1547
  ]
  edge [
    source 15
    target 1548
  ]
  edge [
    source 15
    target 909
  ]
  edge [
    source 15
    target 1549
  ]
  edge [
    source 15
    target 1550
  ]
  edge [
    source 15
    target 1551
  ]
  edge [
    source 15
    target 1552
  ]
  edge [
    source 15
    target 1553
  ]
  edge [
    source 15
    target 1554
  ]
  edge [
    source 15
    target 1555
  ]
  edge [
    source 15
    target 1556
  ]
  edge [
    source 15
    target 1557
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 1558
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 1559
  ]
  edge [
    source 15
    target 1560
  ]
  edge [
    source 15
    target 937
  ]
  edge [
    source 15
    target 990
  ]
  edge [
    source 15
    target 1295
  ]
  edge [
    source 15
    target 1561
  ]
  edge [
    source 15
    target 963
  ]
  edge [
    source 15
    target 936
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 938
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 939
  ]
  edge [
    source 15
    target 940
  ]
  edge [
    source 15
    target 941
  ]
  edge [
    source 15
    target 942
  ]
  edge [
    source 15
    target 943
  ]
  edge [
    source 15
    target 561
  ]
  edge [
    source 15
    target 944
  ]
  edge [
    source 15
    target 945
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 1562
  ]
  edge [
    source 15
    target 1563
  ]
  edge [
    source 15
    target 1564
  ]
  edge [
    source 15
    target 1565
  ]
  edge [
    source 15
    target 1566
  ]
  edge [
    source 15
    target 1567
  ]
  edge [
    source 15
    target 1568
  ]
  edge [
    source 15
    target 1569
  ]
  edge [
    source 15
    target 1570
  ]
  edge [
    source 15
    target 1571
  ]
  edge [
    source 15
    target 1572
  ]
  edge [
    source 15
    target 1573
  ]
  edge [
    source 15
    target 1574
  ]
  edge [
    source 15
    target 1575
  ]
  edge [
    source 15
    target 1576
  ]
  edge [
    source 15
    target 1577
  ]
  edge [
    source 15
    target 1578
  ]
  edge [
    source 15
    target 1289
  ]
  edge [
    source 15
    target 1579
  ]
  edge [
    source 15
    target 1120
  ]
  edge [
    source 15
    target 1580
  ]
  edge [
    source 15
    target 1087
  ]
  edge [
    source 15
    target 1316
  ]
  edge [
    source 15
    target 1581
  ]
  edge [
    source 15
    target 1124
  ]
  edge [
    source 15
    target 1582
  ]
  edge [
    source 15
    target 1583
  ]
  edge [
    source 15
    target 926
  ]
  edge [
    source 15
    target 1584
  ]
  edge [
    source 15
    target 933
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 929
  ]
  edge [
    source 15
    target 1585
  ]
  edge [
    source 15
    target 1586
  ]
  edge [
    source 15
    target 905
  ]
  edge [
    source 15
    target 925
  ]
  edge [
    source 15
    target 1587
  ]
  edge [
    source 15
    target 1588
  ]
  edge [
    source 15
    target 1589
  ]
  edge [
    source 15
    target 1590
  ]
  edge [
    source 15
    target 935
  ]
  edge [
    source 15
    target 924
  ]
  edge [
    source 15
    target 928
  ]
  edge [
    source 15
    target 1591
  ]
  edge [
    source 15
    target 1592
  ]
  edge [
    source 15
    target 1498
  ]
  edge [
    source 15
    target 1593
  ]
  edge [
    source 15
    target 1594
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 927
  ]
  edge [
    source 15
    target 930
  ]
  edge [
    source 15
    target 931
  ]
  edge [
    source 15
    target 932
  ]
  edge [
    source 15
    target 1595
  ]
  edge [
    source 15
    target 1596
  ]
  edge [
    source 15
    target 1597
  ]
  edge [
    source 15
    target 1598
  ]
  edge [
    source 15
    target 959
  ]
  edge [
    source 15
    target 1599
  ]
  edge [
    source 15
    target 1600
  ]
  edge [
    source 15
    target 923
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 1601
  ]
  edge [
    source 15
    target 48
  ]
  edge [
    source 15
    target 1602
  ]
  edge [
    source 15
    target 66
  ]
  edge [
    source 15
    target 1603
  ]
  edge [
    source 15
    target 1604
  ]
  edge [
    source 15
    target 934
  ]
  edge [
    source 15
    target 1605
  ]
  edge [
    source 15
    target 1606
  ]
  edge [
    source 15
    target 1236
  ]
  edge [
    source 15
    target 1607
  ]
  edge [
    source 15
    target 1608
  ]
  edge [
    source 15
    target 1275
  ]
  edge [
    source 15
    target 1609
  ]
  edge [
    source 15
    target 1610
  ]
  edge [
    source 15
    target 1611
  ]
  edge [
    source 15
    target 1612
  ]
  edge [
    source 15
    target 1613
  ]
  edge [
    source 15
    target 1614
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 967
  ]
  edge [
    source 16
    target 968
  ]
  edge [
    source 16
    target 67
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 16
    target 971
  ]
  edge [
    source 16
    target 972
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 1615
  ]
  edge [
    source 16
    target 1380
  ]
  edge [
    source 16
    target 1296
  ]
  edge [
    source 16
    target 1616
  ]
  edge [
    source 16
    target 1617
  ]
  edge [
    source 16
    target 1618
  ]
  edge [
    source 16
    target 1619
  ]
  edge [
    source 16
    target 1620
  ]
  edge [
    source 16
    target 1621
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 996
  ]
  edge [
    source 16
    target 1622
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 1425
  ]
  edge [
    source 16
    target 1426
  ]
  edge [
    source 16
    target 1623
  ]
  edge [
    source 16
    target 1388
  ]
  edge [
    source 16
    target 1300
  ]
  edge [
    source 16
    target 1427
  ]
  edge [
    source 16
    target 48
  ]
  edge [
    source 16
    target 1624
  ]
  edge [
    source 16
    target 1625
  ]
  edge [
    source 16
    target 1626
  ]
  edge [
    source 16
    target 1627
  ]
  edge [
    source 16
    target 1628
  ]
  edge [
    source 16
    target 1629
  ]
  edge [
    source 16
    target 1630
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 16
    target 1631
  ]
  edge [
    source 16
    target 1119
  ]
  edge [
    source 16
    target 1324
  ]
  edge [
    source 16
    target 1632
  ]
  edge [
    source 16
    target 1633
  ]
  edge [
    source 16
    target 1634
  ]
  edge [
    source 16
    target 1635
  ]
  edge [
    source 16
    target 1636
  ]
  edge [
    source 16
    target 1637
  ]
  edge [
    source 16
    target 1638
  ]
  edge [
    source 16
    target 1639
  ]
  edge [
    source 16
    target 1487
  ]
  edge [
    source 16
    target 1640
  ]
  edge [
    source 16
    target 1641
  ]
  edge [
    source 16
    target 1642
  ]
  edge [
    source 16
    target 1643
  ]
  edge [
    source 16
    target 1493
  ]
  edge [
    source 16
    target 1644
  ]
  edge [
    source 16
    target 1645
  ]
  edge [
    source 16
    target 1646
  ]
  edge [
    source 16
    target 1647
  ]
  edge [
    source 16
    target 1648
  ]
  edge [
    source 16
    target 1649
  ]
  edge [
    source 16
    target 1113
  ]
  edge [
    source 16
    target 1650
  ]
  edge [
    source 16
    target 1651
  ]
  edge [
    source 16
    target 1652
  ]
  edge [
    source 16
    target 1464
  ]
  edge [
    source 16
    target 1653
  ]
  edge [
    source 16
    target 27
  ]
  edge [
    source 16
    target 1654
  ]
  edge [
    source 16
    target 1655
  ]
  edge [
    source 16
    target 1656
  ]
  edge [
    source 16
    target 1161
  ]
  edge [
    source 16
    target 1657
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 1507
  ]
  edge [
    source 17
    target 1508
  ]
  edge [
    source 17
    target 1509
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 1510
  ]
  edge [
    source 17
    target 1211
  ]
  edge [
    source 17
    target 1503
  ]
  edge [
    source 17
    target 1511
  ]
  edge [
    source 17
    target 1512
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 1658
  ]
  edge [
    source 17
    target 1659
  ]
  edge [
    source 17
    target 1660
  ]
  edge [
    source 17
    target 1661
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 1662
  ]
  edge [
    source 17
    target 1062
  ]
  edge [
    source 17
    target 1663
  ]
  edge [
    source 17
    target 1199
  ]
  edge [
    source 17
    target 1664
  ]
  edge [
    source 17
    target 1665
  ]
  edge [
    source 17
    target 1059
  ]
  edge [
    source 17
    target 1060
  ]
  edge [
    source 17
    target 1061
  ]
  edge [
    source 17
    target 1063
  ]
  edge [
    source 17
    target 1064
  ]
  edge [
    source 17
    target 1065
  ]
  edge [
    source 17
    target 1066
  ]
  edge [
    source 17
    target 1067
  ]
  edge [
    source 17
    target 1068
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 1069
  ]
  edge [
    source 17
    target 1070
  ]
  edge [
    source 17
    target 1071
  ]
  edge [
    source 17
    target 1072
  ]
  edge [
    source 17
    target 1073
  ]
  edge [
    source 17
    target 1074
  ]
  edge [
    source 17
    target 1075
  ]
  edge [
    source 17
    target 1076
  ]
  edge [
    source 17
    target 1077
  ]
  edge [
    source 17
    target 1666
  ]
  edge [
    source 17
    target 1667
  ]
  edge [
    source 17
    target 1668
  ]
  edge [
    source 17
    target 1669
  ]
  edge [
    source 17
    target 1670
  ]
  edge [
    source 17
    target 1671
  ]
  edge [
    source 17
    target 1672
  ]
  edge [
    source 17
    target 1673
  ]
  edge [
    source 17
    target 1674
  ]
  edge [
    source 17
    target 1675
  ]
  edge [
    source 17
    target 1676
  ]
  edge [
    source 17
    target 1677
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 1456
  ]
  edge [
    source 17
    target 1457
  ]
  edge [
    source 17
    target 1458
  ]
  edge [
    source 17
    target 989
  ]
  edge [
    source 17
    target 1459
  ]
  edge [
    source 17
    target 1460
  ]
  edge [
    source 17
    target 1461
  ]
  edge [
    source 17
    target 1462
  ]
  edge [
    source 17
    target 48
  ]
  edge [
    source 17
    target 1463
  ]
  edge [
    source 17
    target 1464
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1678
  ]
  edge [
    source 19
    target 1679
  ]
  edge [
    source 19
    target 1680
  ]
  edge [
    source 19
    target 1681
  ]
  edge [
    source 19
    target 1682
  ]
  edge [
    source 19
    target 1683
  ]
  edge [
    source 19
    target 1684
  ]
  edge [
    source 19
    target 1685
  ]
  edge [
    source 19
    target 1686
  ]
  edge [
    source 19
    target 1687
  ]
  edge [
    source 19
    target 1688
  ]
  edge [
    source 19
    target 1689
  ]
  edge [
    source 19
    target 1690
  ]
  edge [
    source 19
    target 1691
  ]
  edge [
    source 19
    target 1692
  ]
  edge [
    source 19
    target 1693
  ]
  edge [
    source 19
    target 1694
  ]
  edge [
    source 19
    target 1695
  ]
  edge [
    source 19
    target 1696
  ]
  edge [
    source 19
    target 1697
  ]
  edge [
    source 19
    target 1698
  ]
  edge [
    source 19
    target 1699
  ]
  edge [
    source 19
    target 1700
  ]
  edge [
    source 19
    target 1701
  ]
  edge [
    source 19
    target 1702
  ]
  edge [
    source 19
    target 1703
  ]
  edge [
    source 19
    target 1704
  ]
  edge [
    source 19
    target 1705
  ]
  edge [
    source 19
    target 1706
  ]
  edge [
    source 19
    target 1707
  ]
  edge [
    source 19
    target 1708
  ]
  edge [
    source 19
    target 1709
  ]
  edge [
    source 19
    target 1710
  ]
  edge [
    source 19
    target 1711
  ]
  edge [
    source 19
    target 1331
  ]
  edge [
    source 19
    target 1712
  ]
  edge [
    source 19
    target 1713
  ]
  edge [
    source 19
    target 1714
  ]
  edge [
    source 19
    target 1715
  ]
  edge [
    source 19
    target 1716
  ]
  edge [
    source 19
    target 1717
  ]
  edge [
    source 19
    target 1718
  ]
  edge [
    source 19
    target 1719
  ]
  edge [
    source 19
    target 1720
  ]
  edge [
    source 19
    target 1721
  ]
  edge [
    source 19
    target 1722
  ]
  edge [
    source 19
    target 1723
  ]
  edge [
    source 19
    target 1724
  ]
  edge [
    source 19
    target 1725
  ]
  edge [
    source 19
    target 1726
  ]
  edge [
    source 19
    target 1727
  ]
  edge [
    source 19
    target 1728
  ]
  edge [
    source 19
    target 1729
  ]
  edge [
    source 19
    target 1730
  ]
  edge [
    source 19
    target 1361
  ]
  edge [
    source 19
    target 1731
  ]
  edge [
    source 19
    target 1732
  ]
  edge [
    source 19
    target 1733
  ]
  edge [
    source 19
    target 1734
  ]
  edge [
    source 19
    target 1735
  ]
  edge [
    source 19
    target 1736
  ]
  edge [
    source 19
    target 1737
  ]
  edge [
    source 19
    target 1738
  ]
  edge [
    source 19
    target 1739
  ]
  edge [
    source 19
    target 1740
  ]
  edge [
    source 19
    target 1741
  ]
  edge [
    source 19
    target 1742
  ]
  edge [
    source 19
    target 1743
  ]
  edge [
    source 19
    target 1744
  ]
  edge [
    source 19
    target 1745
  ]
  edge [
    source 19
    target 1746
  ]
  edge [
    source 19
    target 1747
  ]
  edge [
    source 19
    target 1748
  ]
  edge [
    source 19
    target 1749
  ]
  edge [
    source 19
    target 1750
  ]
  edge [
    source 19
    target 1751
  ]
  edge [
    source 19
    target 1752
  ]
  edge [
    source 19
    target 1753
  ]
  edge [
    source 19
    target 1754
  ]
  edge [
    source 19
    target 1755
  ]
  edge [
    source 19
    target 1756
  ]
  edge [
    source 19
    target 1757
  ]
  edge [
    source 19
    target 1758
  ]
  edge [
    source 19
    target 1391
  ]
  edge [
    source 19
    target 1759
  ]
  edge [
    source 19
    target 1597
  ]
  edge [
    source 19
    target 1760
  ]
  edge [
    source 19
    target 1761
  ]
  edge [
    source 19
    target 1762
  ]
  edge [
    source 19
    target 885
  ]
  edge [
    source 19
    target 1763
  ]
  edge [
    source 19
    target 1764
  ]
  edge [
    source 19
    target 1765
  ]
  edge [
    source 19
    target 1766
  ]
  edge [
    source 19
    target 1767
  ]
  edge [
    source 19
    target 1768
  ]
  edge [
    source 19
    target 1769
  ]
  edge [
    source 19
    target 1770
  ]
  edge [
    source 19
    target 1771
  ]
  edge [
    source 19
    target 1772
  ]
  edge [
    source 19
    target 1773
  ]
  edge [
    source 19
    target 1774
  ]
  edge [
    source 19
    target 1775
  ]
  edge [
    source 19
    target 1776
  ]
  edge [
    source 19
    target 1777
  ]
  edge [
    source 19
    target 1778
  ]
  edge [
    source 19
    target 1779
  ]
  edge [
    source 19
    target 1780
  ]
  edge [
    source 19
    target 1781
  ]
  edge [
    source 19
    target 1782
  ]
  edge [
    source 19
    target 1783
  ]
  edge [
    source 19
    target 1784
  ]
  edge [
    source 19
    target 1785
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 1786
  ]
  edge [
    source 19
    target 1787
  ]
  edge [
    source 19
    target 1788
  ]
  edge [
    source 19
    target 1789
  ]
  edge [
    source 19
    target 95
  ]
  edge [
    source 19
    target 1790
  ]
  edge [
    source 19
    target 1791
  ]
  edge [
    source 19
    target 1792
  ]
  edge [
    source 19
    target 1793
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 1794
  ]
  edge [
    source 19
    target 1795
  ]
  edge [
    source 19
    target 1796
  ]
  edge [
    source 19
    target 1797
  ]
  edge [
    source 19
    target 1798
  ]
  edge [
    source 19
    target 1799
  ]
  edge [
    source 19
    target 1800
  ]
  edge [
    source 19
    target 1801
  ]
  edge [
    source 19
    target 1802
  ]
  edge [
    source 19
    target 1803
  ]
  edge [
    source 19
    target 1804
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1691
  ]
  edge [
    source 20
    target 1805
  ]
  edge [
    source 20
    target 1777
  ]
  edge [
    source 20
    target 1695
  ]
  edge [
    source 20
    target 1806
  ]
  edge [
    source 20
    target 1807
  ]
  edge [
    source 20
    target 1698
  ]
  edge [
    source 20
    target 1808
  ]
  edge [
    source 20
    target 1809
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1810
  ]
  edge [
    source 21
    target 1811
  ]
  edge [
    source 21
    target 1180
  ]
  edge [
    source 21
    target 1812
  ]
  edge [
    source 21
    target 1813
  ]
  edge [
    source 21
    target 936
  ]
  edge [
    source 21
    target 1814
  ]
  edge [
    source 21
    target 1815
  ]
  edge [
    source 21
    target 909
  ]
  edge [
    source 21
    target 1816
  ]
  edge [
    source 21
    target 1817
  ]
  edge [
    source 21
    target 1818
  ]
  edge [
    source 21
    target 941
  ]
  edge [
    source 21
    target 1819
  ]
  edge [
    source 21
    target 1820
  ]
  edge [
    source 21
    target 1391
  ]
  edge [
    source 21
    target 1821
  ]
  edge [
    source 21
    target 1822
  ]
  edge [
    source 21
    target 1823
  ]
  edge [
    source 21
    target 1824
  ]
  edge [
    source 21
    target 1119
  ]
  edge [
    source 21
    target 857
  ]
  edge [
    source 21
    target 1825
  ]
  edge [
    source 21
    target 147
  ]
  edge [
    source 21
    target 1826
  ]
  edge [
    source 21
    target 1827
  ]
  edge [
    source 21
    target 127
  ]
  edge [
    source 21
    target 1828
  ]
  edge [
    source 21
    target 1829
  ]
  edge [
    source 21
    target 1830
  ]
  edge [
    source 21
    target 1831
  ]
  edge [
    source 21
    target 1832
  ]
  edge [
    source 21
    target 1833
  ]
  edge [
    source 21
    target 1834
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 1835
  ]
  edge [
    source 21
    target 1836
  ]
  edge [
    source 21
    target 1837
  ]
  edge [
    source 21
    target 41
  ]
  edge [
    source 21
    target 835
  ]
  edge [
    source 21
    target 1838
  ]
  edge [
    source 21
    target 1839
  ]
  edge [
    source 21
    target 1840
  ]
  edge [
    source 21
    target 944
  ]
  edge [
    source 21
    target 1841
  ]
  edge [
    source 21
    target 50
  ]
  edge [
    source 21
    target 1842
  ]
  edge [
    source 21
    target 1843
  ]
  edge [
    source 21
    target 1844
  ]
  edge [
    source 21
    target 757
  ]
  edge [
    source 21
    target 1166
  ]
  edge [
    source 21
    target 1111
  ]
  edge [
    source 21
    target 1173
  ]
  edge [
    source 21
    target 1176
  ]
  edge [
    source 21
    target 1167
  ]
  edge [
    source 21
    target 1175
  ]
  edge [
    source 21
    target 1181
  ]
  edge [
    source 21
    target 1179
  ]
  edge [
    source 21
    target 749
  ]
  edge [
    source 21
    target 1187
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1845
  ]
  edge [
    source 22
    target 1313
  ]
  edge [
    source 22
    target 1846
  ]
  edge [
    source 22
    target 1309
  ]
  edge [
    source 22
    target 36
  ]
  edge [
    source 22
    target 1847
  ]
  edge [
    source 22
    target 1158
  ]
  edge [
    source 22
    target 1195
  ]
  edge [
    source 22
    target 1289
  ]
  edge [
    source 22
    target 1127
  ]
  edge [
    source 22
    target 1848
  ]
  edge [
    source 22
    target 1300
  ]
  edge [
    source 22
    target 32
  ]
  edge [
    source 22
    target 1849
  ]
  edge [
    source 22
    target 1850
  ]
  edge [
    source 22
    target 1851
  ]
  edge [
    source 22
    target 1424
  ]
  edge [
    source 22
    target 1852
  ]
  edge [
    source 22
    target 968
  ]
  edge [
    source 22
    target 1853
  ]
  edge [
    source 22
    target 1854
  ]
  edge [
    source 22
    target 1120
  ]
  edge [
    source 22
    target 1491
  ]
  edge [
    source 22
    target 860
  ]
  edge [
    source 22
    target 1124
  ]
  edge [
    source 22
    target 1125
  ]
  edge [
    source 22
    target 1855
  ]
  edge [
    source 22
    target 1314
  ]
  edge [
    source 22
    target 1315
  ]
  edge [
    source 22
    target 1316
  ]
  edge [
    source 22
    target 877
  ]
  edge [
    source 22
    target 1306
  ]
  edge [
    source 22
    target 1317
  ]
  edge [
    source 22
    target 1318
  ]
  edge [
    source 22
    target 1319
  ]
  edge [
    source 22
    target 1118
  ]
  edge [
    source 22
    target 1856
  ]
  edge [
    source 22
    target 1857
  ]
  edge [
    source 22
    target 1825
  ]
  edge [
    source 22
    target 1858
  ]
  edge [
    source 22
    target 1859
  ]
  edge [
    source 22
    target 1860
  ]
  edge [
    source 22
    target 1861
  ]
  edge [
    source 22
    target 1862
  ]
  edge [
    source 22
    target 1863
  ]
  edge [
    source 22
    target 1864
  ]
  edge [
    source 22
    target 1295
  ]
  edge [
    source 22
    target 1865
  ]
  edge [
    source 22
    target 1866
  ]
  edge [
    source 22
    target 1867
  ]
  edge [
    source 22
    target 1868
  ]
  edge [
    source 22
    target 40
  ]
  edge [
    source 22
    target 1869
  ]
  edge [
    source 22
    target 1117
  ]
  edge [
    source 22
    target 1870
  ]
  edge [
    source 22
    target 1871
  ]
  edge [
    source 22
    target 1285
  ]
  edge [
    source 22
    target 1286
  ]
  edge [
    source 22
    target 48
  ]
  edge [
    source 22
    target 1290
  ]
  edge [
    source 22
    target 1872
  ]
  edge [
    source 22
    target 1292
  ]
  edge [
    source 22
    target 1296
  ]
  edge [
    source 22
    target 1417
  ]
  edge [
    source 22
    target 1419
  ]
  edge [
    source 22
    target 1298
  ]
  edge [
    source 22
    target 1299
  ]
  edge [
    source 22
    target 1301
  ]
  edge [
    source 22
    target 1841
  ]
  edge [
    source 22
    target 1303
  ]
  edge [
    source 22
    target 1304
  ]
  edge [
    source 22
    target 1873
  ]
  edge [
    source 22
    target 1874
  ]
  edge [
    source 22
    target 1307
  ]
  edge [
    source 22
    target 1308
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1875
  ]
  edge [
    source 23
    target 1876
  ]
  edge [
    source 23
    target 1877
  ]
  edge [
    source 23
    target 1878
  ]
  edge [
    source 23
    target 1448
  ]
  edge [
    source 23
    target 1879
  ]
  edge [
    source 23
    target 1880
  ]
  edge [
    source 23
    target 1881
  ]
  edge [
    source 24
    target 1882
  ]
  edge [
    source 24
    target 759
  ]
  edge [
    source 24
    target 757
  ]
  edge [
    source 24
    target 998
  ]
  edge [
    source 24
    target 999
  ]
  edge [
    source 24
    target 1000
  ]
  edge [
    source 24
    target 1001
  ]
  edge [
    source 24
    target 1002
  ]
  edge [
    source 24
    target 1003
  ]
  edge [
    source 24
    target 1004
  ]
  edge [
    source 24
    target 764
  ]
  edge [
    source 24
    target 860
  ]
  edge [
    source 24
    target 1005
  ]
  edge [
    source 24
    target 1006
  ]
  edge [
    source 24
    target 1007
  ]
  edge [
    source 24
    target 756
  ]
  edge [
    source 24
    target 1008
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1883
  ]
  edge [
    source 25
    target 1884
  ]
  edge [
    source 26
    target 1885
  ]
  edge [
    source 26
    target 1886
  ]
  edge [
    source 26
    target 1887
  ]
  edge [
    source 26
    target 1248
  ]
  edge [
    source 26
    target 1888
  ]
  edge [
    source 26
    target 1889
  ]
  edge [
    source 26
    target 1890
  ]
  edge [
    source 26
    target 1891
  ]
  edge [
    source 26
    target 1698
  ]
  edge [
    source 26
    target 1391
  ]
  edge [
    source 26
    target 1892
  ]
  edge [
    source 26
    target 1236
  ]
  edge [
    source 26
    target 1893
  ]
  edge [
    source 26
    target 1894
  ]
  edge [
    source 26
    target 1825
  ]
  edge [
    source 26
    target 1895
  ]
  edge [
    source 26
    target 1596
  ]
  edge [
    source 26
    target 1896
  ]
  edge [
    source 26
    target 1897
  ]
  edge [
    source 26
    target 1898
  ]
  edge [
    source 26
    target 1899
  ]
  edge [
    source 26
    target 1300
  ]
  edge [
    source 26
    target 1900
  ]
  edge [
    source 26
    target 1901
  ]
  edge [
    source 26
    target 1902
  ]
  edge [
    source 26
    target 561
  ]
  edge [
    source 26
    target 1903
  ]
  edge [
    source 26
    target 1904
  ]
  edge [
    source 26
    target 1707
  ]
  edge [
    source 26
    target 1708
  ]
  edge [
    source 26
    target 1709
  ]
  edge [
    source 26
    target 1710
  ]
  edge [
    source 26
    target 1711
  ]
  edge [
    source 26
    target 1331
  ]
  edge [
    source 26
    target 1712
  ]
  edge [
    source 26
    target 1713
  ]
  edge [
    source 26
    target 1714
  ]
  edge [
    source 26
    target 1715
  ]
  edge [
    source 26
    target 1716
  ]
  edge [
    source 26
    target 1717
  ]
  edge [
    source 26
    target 1718
  ]
  edge [
    source 26
    target 1719
  ]
  edge [
    source 26
    target 1720
  ]
  edge [
    source 26
    target 1721
  ]
  edge [
    source 26
    target 1722
  ]
  edge [
    source 26
    target 1723
  ]
  edge [
    source 26
    target 1724
  ]
  edge [
    source 26
    target 1725
  ]
  edge [
    source 26
    target 1726
  ]
  edge [
    source 26
    target 1727
  ]
  edge [
    source 26
    target 1728
  ]
  edge [
    source 26
    target 1729
  ]
  edge [
    source 26
    target 1730
  ]
  edge [
    source 26
    target 1361
  ]
  edge [
    source 26
    target 1731
  ]
  edge [
    source 26
    target 1732
  ]
  edge [
    source 26
    target 1733
  ]
  edge [
    source 26
    target 1734
  ]
  edge [
    source 26
    target 1735
  ]
  edge [
    source 26
    target 1736
  ]
  edge [
    source 26
    target 1737
  ]
  edge [
    source 26
    target 1738
  ]
  edge [
    source 26
    target 1739
  ]
  edge [
    source 26
    target 1740
  ]
  edge [
    source 26
    target 1741
  ]
  edge [
    source 26
    target 1905
  ]
  edge [
    source 26
    target 1906
  ]
  edge [
    source 26
    target 1907
  ]
  edge [
    source 26
    target 1868
  ]
  edge [
    source 26
    target 1908
  ]
  edge [
    source 26
    target 1909
  ]
  edge [
    source 26
    target 1910
  ]
  edge [
    source 26
    target 1911
  ]
  edge [
    source 26
    target 1912
  ]
  edge [
    source 26
    target 1913
  ]
  edge [
    source 26
    target 1914
  ]
  edge [
    source 26
    target 1915
  ]
  edge [
    source 26
    target 1916
  ]
  edge [
    source 26
    target 1917
  ]
  edge [
    source 26
    target 1191
  ]
  edge [
    source 26
    target 1918
  ]
  edge [
    source 26
    target 1919
  ]
  edge [
    source 26
    target 1920
  ]
  edge [
    source 26
    target 1921
  ]
  edge [
    source 26
    target 1922
  ]
  edge [
    source 26
    target 1923
  ]
  edge [
    source 26
    target 1924
  ]
  edge [
    source 26
    target 1925
  ]
  edge [
    source 26
    target 1926
  ]
  edge [
    source 26
    target 1927
  ]
  edge [
    source 26
    target 1928
  ]
  edge [
    source 26
    target 1929
  ]
  edge [
    source 26
    target 1930
  ]
  edge [
    source 26
    target 1931
  ]
  edge [
    source 26
    target 1932
  ]
  edge [
    source 26
    target 1933
  ]
  edge [
    source 26
    target 1934
  ]
  edge [
    source 26
    target 1935
  ]
  edge [
    source 26
    target 1936
  ]
  edge [
    source 26
    target 1937
  ]
  edge [
    source 26
    target 1938
  ]
  edge [
    source 26
    target 53
  ]
  edge [
    source 26
    target 959
  ]
  edge [
    source 26
    target 1939
  ]
  edge [
    source 26
    target 1940
  ]
  edge [
    source 26
    target 1941
  ]
  edge [
    source 26
    target 1942
  ]
  edge [
    source 26
    target 32
  ]
  edge [
    source 26
    target 1943
  ]
  edge [
    source 26
    target 1944
  ]
  edge [
    source 26
    target 1093
  ]
  edge [
    source 26
    target 744
  ]
  edge [
    source 26
    target 1945
  ]
  edge [
    source 26
    target 1946
  ]
  edge [
    source 26
    target 1947
  ]
  edge [
    source 26
    target 1948
  ]
  edge [
    source 26
    target 1949
  ]
  edge [
    source 26
    target 1950
  ]
  edge [
    source 26
    target 1951
  ]
  edge [
    source 26
    target 1952
  ]
  edge [
    source 26
    target 1953
  ]
  edge [
    source 26
    target 1954
  ]
]
